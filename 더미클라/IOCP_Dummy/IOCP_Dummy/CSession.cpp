#include "StdAfx.h"
#include "CSession.h"


CSession::CSession(): m_lIOCount(0)
{
	
}

CSession::~CSession()
{

}
void CSession::InitSession()
{		

	if(m_lIOCount > 0)
	{
		int *p = NULL;
		*p = 0;
	}

	m_lSendCount = 0;
	
	m_bReleaseFlag = false;

	
	m_bLogoutFlag = false;

	m_bRecvFlag = false;


	ClearBuffer();

	
	
}

void CSession::SetIOHandle(HANDLE hHandle)
{
	m_hIOHandle = hHandle;
}


void CSession::ClearBuffer()
{
	CPacketBuffer* Packet = nullptr;
	while(m_SendPacketKeepQ.Dequeue(Packet))	
		Packet->Free();
	

	while(m_RecvCompletePacketQ.Dequeue(Packet))
		Packet->Free();

	while(m_SendQ.Dequeue(Packet))
		Packet->Free();

	m_RecvBuffer.ClearBuffer();

}
long CSession::IncrementIO()
{
	return InterlockedIncrement(&m_lIOCount);
}

long CSession::DecrementIO()
{
	int Num = InterlockedDecrement(&m_lIOCount);
	if(Num == 0)	
		Disconnect();
	
	return Num;
}

void CSession::SetIO(long lCount)
{
	m_lIOCount = lCount;
}

void CSession::SetLogout()
{
	m_bLogoutFlag = true;
}

bool CSession::SendPost()
{
	//센드 1회 제한
	if(InterlockedExchange(&m_lSendCount,1) == 1)
		return false;

	if(m_SendQ.GetSize() <= 0)
		return false;

	WSABUF wsaBuf[SEND_WSABUF_COUNT];
	
	int iWSABufCount = 0;
	CPacketBuffer* pPacket;
	while(m_SendQ.Dequeue(pPacket))
	{
		wsaBuf[iWSABufCount].buf = pPacket->GetBufferPtr();
		wsaBuf[iWSABufCount].len = pPacket->GetPacketSize();

		m_SendPacketKeepQ.Enqueue(pPacket);

		if(++iWSABufCount == SEND_WSABUF_COUNT)
			break;

	}

	ZeroMemory(&m_SendOverlapped,sizeof(OVERLAPPED));

	IncrementIO();
	
	DWORD dwFlags = 0;
	DWORD dwNumberOfBytesRecvd = 0;

	int result = WSASend(GetSocket(),wsaBuf,iWSABufCount,&dwNumberOfBytesRecvd,dwFlags,&m_SendOverlapped,NULL);
	if(result != 0)
	{
		int num = WSAGetLastError();
		if(num != ERROR_IO_PENDING)
		{
			//// 로그
			//int *p = NULL;
			//*p = 0;

			DecrementIO();

			SendPacketProcess();			

			return false;	

		}

			return true;

	}

	return true;


	/*if(!m_Sock.Send(wsaBuf,iWSABufCount,&m_RecvOverlapped))
	{
	if(DecrementIO() == 0)
	SetLogout();

	CPacketBuffer* pTempPacket;
	while(m_SendPacketKeepQ.Dequeue(pTempPacket))
	pTempPacket->Free();

	m_lSendCount = 0;

	return false;
	}	*/

	//return true;
	
}

bool CSession::RecvPost()
{

	WSABUF wsaBuf[2];

	int iWSABufCount = 0;

	wsaBuf[iWSABufCount].len = m_RecvBuffer.GetNotBrokenPutSize();
	wsaBuf[iWSABufCount++].buf = m_RecvBuffer.GetWriteBufferPtr();

	INT iLen = m_RecvBuffer.GetFreeSize() - m_RecvBuffer.GetNotBrokenPutSize();
	if(iLen > 0)
	{
		wsaBuf[iWSABufCount].len = iLen;
		wsaBuf[iWSABufCount++].buf = m_RecvBuffer.GetBufferPtr();
	}

	ZeroMemory(&m_RecvOverlapped,sizeof(OVERLAPPED));

	IncrementIO();
	m_bRecvFlag = true;


	DWORD dwFlags = 0;
	DWORD dwNumberOfBytesRecvd = 0;

	int result = WSARecv(GetSocket(),wsaBuf,iWSABufCount,&dwNumberOfBytesRecvd,&dwFlags,&m_RecvOverlapped,NULL);
	if(result != 0)
	{
		int num = WSAGetLastError();
		if(num != ERROR_IO_PENDING)
		{
			// 로그
			
			DecrementIO();

			return false;	

		}

			

	}

	return true;

	/*if(!m_Sock.Recv(wsaBuf,iWSABufCount,&m_RecvOverlapped))
	{
		if(DecrementIO() == 0)
			SetLogout();

			return false;
	}	

	return true;*/
}
void CSession::RecvBufferAddRear(DWORD Size)
{
	m_RecvBuffer.MoveWritePos(Size);

}
void CSession::RecvBufferRemoveData(DWORD Size)
{
	m_RecvBuffer.RemoveData(Size);

}


void CSession::ProcessPacket()
{
	while(true)
	{
	
		//-------------------------------
		// 패킷 헤더 검사
		//-------------------------------
		CPacketBuffer* Packet = CheckPacket();
		if(Packet == NULL)
			break;

		
		Packet->AddRef();
		m_RecvCompletePacketQ.Enqueue(Packet);

		Packet->Free();


	}

}


CPacketBuffer* CSession::CheckPacket()
{
	//헤더길이 확인
	if (m_RecvBuffer.GetUseSize() < 5)
		return NULL;


	char check[3];
	m_RecvBuffer.Peek(check, 3);

	const BYTE Pack = (BYTE)check[0];
	const WORD Len = *(WORD*)&check[1];



	do {

		//패킷 코드 비교 방어
		//if (Pack != 119)
		//{
		//	//로그
		//	break;
		//}


		////방어
		//if (Len > df_Packet_MAXSIZE)
		//{
		//	//로그
		//	break;
		//}



		//패킷 길이 확인 (헤더와 페이로드 길이 합보다 작은지 비교)
		if (m_RecvBuffer.GetUseSize() < Len + 5)
		{
			//	Packet->Free();
			return NULL;
		}


		CPacketBuffer *Packet = CPacketBuffer::Alloc();


		//Packet->MoveHeadPos(df_PACK_HEADER_NET_SIZE);
		//pSession->RecvQ.Peek((char*)Packet->GetBufferPtr(), df_PACK_HEADER_NET_SIZE);



		//패킷초기화시 이미 rear이 헤더 길이만큼 되있기에 패킷무브는 움직이지 않는다(리시브큐만 픽으로 뺏기때문에 헤더 데이터를 지워준다
		//Packet->MoveWritePos(df_PACK_HEADER_SIZE);
		//	pSession->RecvQ.RemoveData(df_PACK_HEADER_NET_SIZE);



		//패킷 길이만큼 복사
		m_RecvBuffer.Dequeue((char*)Packet->GetBufferPtr(), Len + 5);

		//넣을때는 모든 패킷 길이만큼 넣었지만 할당받을때 헤더값만큼 rear가 밀려있어서 패이로드 값만큼만 밀어준다
		Packet->MoveWritePos(Len);



		//길이와 페이로드가 다르거나, 체크섬 실패(복호화 실패)
		if (!Packet->Decode())
		{
			//로그
			break;
		}


		return Packet;


	} while (0);


	//실패시 //값초기화하여 루프 방지	
	m_RecvBuffer.ClearBuffer();

	Disconnect();

	//Packet->Free();

	return NULL;

}


void CSession::SendPacketProcess()
{
	CPacketBuffer *Packet = nullptr;
	while(m_SendPacketKeepQ.Dequeue(Packet))
	{
		Packet->Free();
	}

	//---------------------------
	// 일단 그냥 함 인터락 안쓰고
	//----------------------------
	m_lSendCount = 0;
}



void CSession::SendPacket(CPacketBuffer *Packet)
{
	Packet->Encode();

	Packet->AddRef();
	m_SendQ.Enqueue(Packet);

	IncrementIO();
	PostQueuedCompletionStatus(m_hIOHandle, 100, (ULONG_PTR)this, &m_SendPostOverlapped);

}


CPacketBuffer*  CSession::GetPacket()
{
	CPacketBuffer* Packet = nullptr;
	m_RecvCompletePacketQ.Dequeue(Packet);
	return Packet;
}



OVERLAPPED* CSession::GetSendOverlapped()
{
	return &m_SendOverlapped;
}

OVERLAPPED* CSession::GetRecvOverlapped()
{
	return &m_RecvOverlapped;
}

OVERLAPPED* CSession::GetSendPostOverlapped()
{
	return &m_SendPostOverlapped;
}


void CSession::Disconnect()
{
	m_bLogoutFlag = true;

	//m_Sock.Disconnect();

}

void CSession::Logout_FlagCheck()
{
	if(m_bLogoutFlag)
		SetStatus(WAIT_RELEASE);

}
void CSession:: SessionRelease()
{
	
		if(m_lSendCount == 0)
		{
			SetStatus(RELEASE);
			
			//m_ArraySession[i]->SocketClose();
		//	m_ArraySession[i]->InitSession();
		}
		
	
}

void CSession::SetStatus(int Type)
{
	StatusType = Type;
}
int CSession::GetStatus()
{
	return StatusType;
}
//
//void  CSession::SocketClose()
//{
//	m_Sock.Close();
//
//}