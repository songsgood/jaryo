#pragma once

#include "CSocket.h"

#include "RingBuffer.h"
#include "SerializationBuffer.h"
#include "ArrayQueue.h"

#define SEND_WSABUF_COUNT 10

enum ESessionState
{
	NOT_CONNECT, CONNECT, WAIT_RELEASE,RELEASE
};

class CSession : public CSocket
{
public:
	CSession();
	virtual ~CSession();

	
	void InitSession();

	void SetIOHandle(HANDLE hHandle);

	bool SendPost();
	bool RecvPost();
	void Disconnect();

	long IncrementIO();
	long DecrementIO();
		
	void SetIO(long Count);

	
	OVERLAPPED* GetSendOverlapped();
	OVERLAPPED* GetRecvOverlapped();
	OVERLAPPED* GetSendPostOverlapped();
	
	
	void RecvBufferAddRear(DWORD Size);
	void RecvBufferRemoveData(DWORD Size);

	void ProcessPacket();
	
	void SendPacketProcess();

	void SetStatus(int Type);
	int GetStatus();

	void SendPacket(CPacketBuffer *Packet);

	void SetLogout();
	void Logout_FlagCheck();
	void SessionRelease();

	
	//void SocketClose();

	virtual void OnAction() = 0;
	virtual void OnConnect() = 0;

	

	bool m_bRecvFlag;
	//SOCKET ssock;
	unsigned int LastTime;

	
protected:
	
	CPacketBuffer* GetPacket();

private:

	CPacketBuffer* CheckPacket();
	void ClearBuffer();

		
private:

	

	CSocket m_Sock;
	HANDLE  m_hIOHandle;

	CRingBuffer				    m_RecvBuffer;
	CArrayQueue<CPacketBuffer*> m_RecvCompletePacketQ;
	CArrayQueue<CPacketBuffer*> m_SendPacketKeepQ;

	CArrayQueue<CPacketBuffer*> m_SendQ;
	

	
	
	OVERLAPPED m_SendOverlapped;
	OVERLAPPED m_RecvOverlapped;
	OVERLAPPED m_SendPostOverlapped;

	
	int StatusType;
	
	
	long m_lIOCount;

	bool m_bReleaseFlag;

	long m_lSendCount;
	
	bool m_bLogoutFlag;
	



};