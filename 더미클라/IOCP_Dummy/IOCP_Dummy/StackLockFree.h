
#pragma once

template<typename DATA>
class CStackLockFree
{
private:

	struct SNODE
	{
		DATA Data;
		SNODE *pNext;
	};

	union TOP_NODE
	{
		LONG64 Aligment;
		struct
		{
			SNODE *pNode;
			UINT32 iUniqueNum;
			
		};

	};

	

public:
	CStackLockFree()
	{
		pTop = (TOP_NODE*)malloc(sizeof(TOP_NODE));
		pTop->pNode = NULL;
		pTop->iUniqueNum = 0;		

		iCurrentSize = 0;
	}
	~CStackLockFree()
	{
		//free(pTop);
	}

	bool Push(DATA data);
	bool Pop(DATA &data);
	int Size();
	void Clear();






private:




	TOP_NODE *pTop;

	long iCurrentSize;


};

template<typename DATA>
bool CStackLockFree<DATA>::Push(DATA data)
{

	SNODE *pNewNode = new SNODE();// pool.Alloc();

	pNewNode->Data = data;

	TOP_NODE CurTop;
	TOP_NODE NewTop;

	do
	{


		CurTop.Aligment = pTop->Aligment;
	

		NewTop.iUniqueNum = CurTop.iUniqueNum+1;		
		NewTop.pNode = pNewNode;

		pNewNode->pNext = pTop->pNode;


	} while (InterlockedCompareExchange64(&pTop->Aligment, NewTop.Aligment, CurTop.Aligment) != CurTop.Aligment);

	

	InterlockedIncrement(&iCurrentSize);

	return true;
}

template<typename DATA>
bool CStackLockFree<DATA>::Pop(DATA &data)
{
	int Count = InterlockedDecrement(&iCurrentSize);

	if (Count < 0)
	{
		InterlockedIncrement(&iCurrentSize);
		data = NULL;
		return false;
	}



	TOP_NODE CurTop;
	TOP_NODE NewTop;

	do
	{

		CurTop.Aligment = pTop->Aligment;


		NewTop.pNode = CurTop.pNode->pNext;		

		NewTop.iUniqueNum = CurTop.iUniqueNum + 1;
		
		

	} while (InterlockedCompareExchange64(&pTop->Aligment, NewTop.Aligment, CurTop.Aligment) != CurTop.Aligment);

	data = CurTop.pNode->Data;
	delete CurTop.pNode;





	return true;

}

template<typename DATA>
int CStackLockFree<DATA>::Size()
{
	return iCurrentSize;
}




template<typename DATA>
void CStackLockFree<DATA>::Clear()
{
	iCurrentSize = 0;
}


