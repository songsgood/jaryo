#pragma once
#include "CSession.h"


class CPlayer : public CSession
{
public:
	CPlayer()
	{
		Number = 0;
		SendFlag = 0;
	};
	~CPlayer(){};

	int Number;

	int SendFlag;

	void OnAction();
	void OnConnect();

};