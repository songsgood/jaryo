/*
 * queue.h
 *
 *  Created on: 2011. 4. 23.
 *      Author: Chwang
 */

#ifndef QUEUE_H_
#define QUEUE_H_
template<typename DATA>
class QueueOk{

public:
	QueueOk();

typedef struct node {
	DATA value;
	node* next;
};



void destroy_queue();
void add_rear(DATA value);
DATA remove_front();

int count;
node *front;
node *rear;

};


template<typename DATA>
QueueOk<DATA>::QueueOk(){
	//queue* new_queue=(queue*)malloc(sizeof(queue));
	count=0;
	front=NULL;
	rear=NULL;
	
}


template<typename DATA>
void QueueOk<DATA>::add_rear(DATA value){
	node* new_node=(node*)malloc(sizeof(node));
	new_node->value=value;
	new_node->next=NULL;

	if (count==0){
		 
		rear=new_node;
		front= new_node;
	}
	else{
		rear->next=new_node;
		rear=new_node;
	}

	count++;
}
template<typename DATA>
DATA QueueOk<DATA>::remove_front(){
	if (count==0){
		printf("There is no item in this queue.\n");
		return NULL;
	}
	else{
		node* tmp= front;
		front=tmp->next;
		DATA tmp_value=tmp->value;
		free(tmp);
		count--;
		return tmp_value;
	}
}



#endif /* QUEUE_H_ */
