#include "stdafx.h"

#include "CIOCP_Dummy.h"

#include "CPlayer.h"
CIOCP_Dummy::CIOCP_Dummy()
{
	
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		wprintf(L"WSAStartup failed: %d\n", iResult);

	}

	m_lMonitor_Connect = 0;
	m_lMonitor_ConnectTotal = 0;
	m_lMonitor_RecvTPS = 0;
	m_lMonitor_SendTPS = 0;

	CPacketBuffer::SetEncodeVar((BYTE)119,(BYTE)123,(BYTE)321);
}
CIOCP_Dummy::~CIOCP_Dummy()
{

}

bool CIOCP_Dummy::Start()
{

	UpdateThreadNumber = 0;

	m_MaxUser = MAX_USER;

	m_hIO = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	

	//세션 초기화
	InitSession();

	//스레드 생성
	CreateThreads();

	return true;
	
}

void CIOCP_Dummy::CreateThreads()
{
	m_hConnectThread = (HANDLE)_beginthreadex(NULL,0,ConnectThread,this,0,0);
	if(m_hConnectThread == 0)
	{
		   //처리
	}
	
	for(int i = 0;i<WORK_THREAD_COUNT;i++)
		m_hWorkThread = (HANDLE)_beginthreadex(NULL,0,WorkThread,this,0,0);
	if(m_hConnectThread == 0)
	{
		//처리
	}

	for(int i = 0;i<UPDATE_THREAD_COUNT;i++)
		m_hUpdateThread = (HANDLE)_beginthreadex(NULL,0,UpdateThread,this,0,0);
	if(m_hConnectThread == 0)
	{
		//처리
	}
	
	m_hLogThread = (HANDLE)_beginthreadex(NULL,0,LogThread,this,0,0);
	if(m_hConnectThread == 0)
	{
		//처리
	}
	

}

unsigned WINAPI CIOCP_Dummy::ConnectThread(LPVOID arg)
{
	CIOCP_Dummy *dummy = (CIOCP_Dummy*)arg;
	dummy->OnConnectThread();

	return 0;
}

unsigned WINAPI CIOCP_Dummy::UpdateThread(LPVOID arg)
{
	CIOCP_Dummy *dummy = (CIOCP_Dummy*)arg;
	dummy->OnUpdateThread();

	return 0;
}

unsigned WINAPI CIOCP_Dummy::WorkThread(LPVOID arg)
{
	CIOCP_Dummy *dummy = (CIOCP_Dummy*)arg;
	dummy->OnWorkThread();

	return 0;
}

unsigned WINAPI CIOCP_Dummy::LogThread(LPVOID arg)
{
	CIOCP_Dummy *dummy = (CIOCP_Dummy*)arg;
	dummy->OnLogThread();

	return 0;
}


void CIOCP_Dummy::InitSession()
{
	m_ArraySession = new CSession*[m_MaxUser];

	for(int i = m_MaxUser - 1;i >= 0;i--)
	{
		CPlayer *pPlayer = new CPlayer;
		m_ArraySession[i] = (CSession *)pPlayer;

		m_ArraySession[i]->SetSocketIp(IP);
		m_ArraySession[i]->SetSocketPort(PORT);
		m_ArraySession[i]->InitSession();
		m_ArraySession[i]->SetStatus(NOT_CONNECT);
		m_ArraySession[i]->SetIOHandle(m_hIO);
		//m_StackSession.Push(m_ArraySession[i]);
	}

}
//
//CSession* CIOCP_Dummy::NewSession()
//{
//	CSession *pSession;
//
//	//if(!m_StackSession.Pop(pSession))
//	//{
//	//	//로그 처리
//	//	return nullptr;
//	//}
//
//	pSession->InitSession();
//
//	pSession->IncrementIO();
//
//	return pSession;
//}
void CIOCP_Dummy::OnConnectThread()
{
	CSession* pSession;
	srand((unsigned int)time(NULL));

	
			/*	WSADATA wsaData;
				int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
				if (iResult != 0) {
					wprintf(L"WSAStartup failed: %d\n", iResult);

				}*/

	while(true){
		for(int i = 0;i<m_MaxUser;i++)
		{
			pSession = m_ArraySession[i];
			if(pSession->GetStatus() == NOT_CONNECT)
			{
				pSession->InitSocket();

			/*	if(pSession->IncrementIO() != 1)
				{
					int bb = 0;
				}*/
				//
   	//			pSession->m_hSocket = socket(AF_INET, SOCK_STREAM, 0);
				//if (pSession->m_hSocket == INVALID_SOCKET) {
				//	//	wprintf(L"Error at socket(): %ld\n", WSAGetLastError());
				//	WSACleanup();
				//	break;
				//}


				//SOCKADDR_IN m_SocketAddr;
				//
				//ZeroMemory(&m_SocketAddr, sizeof(SOCKADDR_IN));

				//m_SocketAddr.sin_family = AF_INET;
				//m_SocketAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
				//m_SocketAddr.sin_port = htons((u_short)8888);

				//SOCKET ClientSocket = connect(pSession->m_hSocket, (SOCKADDR*)&m_SocketAddr, sizeof(SOCKADDR_IN));
				//if (ClientSocket == INVALID_SOCKET)
				//{
				//	//wprintf(L"accept failed with error : %d\n", WSAGetLastError());
				//	continue;

				//}
				
				//if(ClientSocket!= INVALID_SOCKET)
				if(pSession->Connect())
				{
					//pSession->InitSession();
					pSession->IncrementIO();

					CreateIoCompletionPort((HANDLE)pSession->GetSocket(),m_hIO,(ULONG_PTR)pSession,0);

					pSession->OnConnect();

					if(!pSession->RecvPost())					
						pSession->DecrementIO();
					
					

					pSession->SetStatus(CONNECT);

					InterlockedIncrement(&m_lMonitor_Connect);
					++m_lMonitor_ConnectTotal;

					//pSession->ShutDownSock();

					pSession->DecrementIO();
					



				}
				else
				{
					//실패
				}
			}

		}
	}

}
void CIOCP_Dummy::OnWorkThread()
{

	int retval;

	while(true)
	{
		DWORD dwThransferred = 0;
		CSession *pSession = nullptr;
		OVERLAPPED *pOverlapped = nullptr;


		retval = GetQueuedCompletionStatus(m_hIO,&dwThransferred,(PULONG_PTR)&pSession,(LPOVERLAPPED*)&pOverlapped,INFINITE);

		if(!retval && pOverlapped == nullptr)
		{
			//에러 처리 로그
			break;
		}

		//종료 처리
		if(dwThransferred == 0 && pSession == nullptr && pOverlapped == NULL)
		{
			PostQueuedCompletionStatus(m_hIO,0,0,nullptr);
			break;
		}
		
		//세션 종료
		if(dwThransferred == 0)
		{
			if(pOverlapped == pSession->GetSendOverlapped())
				pSession->SendPacketProcess();

		//	pSession->ShutDownSock();		
		}			
		else
		//리시브 완료 통지 처리
		if(pOverlapped == pSession->GetSendOverlapped())
		{
			pSession->SendPacketProcess();
			InterlockedIncrement(&m_lMonitor_SendTPS);

			// pSession->RecvBufferAddRear(dwThransferred);
			// pSession->OnAction();
			 //패킷 처리부

			// pSession->RecvPost();
		}
		else
		//센드 완료 통지 처리
		if(pOverlapped == pSession->GetRecvOverlapped())
		{
				InterlockedIncrement(&m_lMonitor_RecvTPS);
			pSession->m_bRecvFlag = false;
			pSession->RecvBufferAddRear(dwThransferred);
			pSession->ProcessPacket();
			pSession->RecvPost();
		
		}
		else
		//센드 요청 처리
		if(pOverlapped == pSession->GetSendPostOverlapped())
		{
			pSession->SendPost();
			
		}

		pSession->DecrementIO();
		
	}

}

void CIOCP_Dummy::OnUpdateThread()
{

	int ThreadId = InterlockedIncrement(&UpdateThreadNumber);
	--ThreadId;

	int StartIndex =  (m_MaxUser / UPDATE_THREAD_COUNT)*ThreadId; 
	int EndIndex   =  StartIndex + (m_MaxUser / UPDATE_THREAD_COUNT);

	//-------------------------------------------
	// 마지막 업데이트 스레드가 나머지 모두 처리
	//-------------------------------------------
	if(UPDATE_THREAD_COUNT == ThreadId +1)
	{		
		EndIndex += m_MaxUser % UPDATE_THREAD_COUNT;
	}


	const int SleepTime = UPDATE_SLEEP;
	srand((unsigned int)time(NULL));

	while(true)
	{
		Sleep(SleepTime);

		for(int i = StartIndex;i<EndIndex;i++)
		{
			switch(m_ArraySession[i]->GetStatus())
			{
			case CONNECT:
				m_ArraySession[i]->OnAction();
				m_ArraySession[i]->Logout_FlagCheck();

				break;
			case WAIT_RELEASE:
				m_ArraySession[i]->SessionRelease();
				break;
			case RELEASE:
				
				m_ArraySession[i]->InitSession();
				//closesocket(m_ArraySession[i]->m_hSocket);
				m_ArraySession[i]->SocketClose();
				m_ArraySession[i]->SetStatus(NOT_CONNECT);
				InterlockedDecrement(&m_lMonitor_Connect);

				//StackSession.Push(m_ArraySession[i]);
				break;
			}		

		}
	


	}

}
void CIOCP_Dummy::OnLogThread()
{
	while(true)
	{
		printf("========================== DummyClient ==========================\n\n");
		printf("ConnectSession        : %d\n",m_lMonitor_Connect);
		printf("ConnectSession Total  : %d\n\n",m_lMonitor_ConnectTotal);
		printf("Recv TPS              : %d\n",m_lMonitor_RecvTPS);
		printf("Send TPS              : %d\n\n",m_lMonitor_SendTPS);
		printf("=====================================================================\n");
		
		InterlockedExchange(&m_lMonitor_RecvTPS,0);
		InterlockedExchange(&m_lMonitor_SendTPS,0);
		

		Sleep(1000);

		
		//PostQueuedCompletionStatus(m_hIO,0,NULL,NULL);
	}
}


//
//void CIOCP_Dummy::ReleaseSession(CSession *pSession)
//{
//
//	m_StackSession.Push(pSession);
//}