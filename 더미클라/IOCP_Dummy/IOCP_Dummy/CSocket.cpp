#include "StdAfx.h"

#include "CSocket.h"

CSocket::CSocket()
{
	


}
CSocket::~CSocket()
{

}


SOCKET CSocket::GetSocket()
{
	return m_hSocket;
}
void CSocket::SetSocketIp(char* Ip)
{
	memcpy(m_sAddr,Ip,sizeof(m_sAddr));
}

void CSocket::SetSocketPort(unsigned short usPort)
{
	m_usPort = usPort;
}


unsigned short CSocket::GetSocketPort()
{
	return m_usPort;
}

void CSocket::InitSocket()
{
	m_hSocket = socket(AF_INET,SOCK_STREAM,0);
	if(m_hSocket == INVALID_SOCKET)
	{
		int *p = NULL;
		*p = 0;
		
		// 로그 에러 처리
	}
}

bool CSocket::Connect()
{



	ZeroMemory(&m_SocketAddr, sizeof(SOCKADDR_IN));

	m_SocketAddr.sin_family = AF_INET;
	m_SocketAddr.sin_addr.s_addr = inet_addr(m_sAddr);
	m_SocketAddr.sin_port = htons((u_short)m_usPort);

	SOCKET socket = connect(m_hSocket, (SOCKADDR*)&m_SocketAddr, sizeof(SOCKADDR_IN));

	if(socket == SOCKET_ERROR)
	{
		DWORD dwErr = WSAGetLastError();
		//에러 처리 로그
		return false;
	}

	return true;
}


bool CSocket::Send(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped)
{
	DWORD dwFlags = 0;
	DWORD dwNumberOfBytesRecvd = 0;

	if(!WSASend(m_hSocket,buffer,Size,&dwNumberOfBytesRecvd,dwFlags,pOverlapped,NULL))
	{
		if(WSAGetLastError() != ERROR_IO_PENDING)
		{
			// 로그

		}

		return false;		

	}

	return true;

}
bool CSocket::Recv(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped)
{
	DWORD dwFlags = 0;
	DWORD dwNumberOfBytesRecvd = 0;

	if(!WSARecv(m_hSocket,buffer,Size,&dwNumberOfBytesRecvd,&dwFlags,pOverlapped,NULL))
	{
		if(WSAGetLastError() != ERROR_IO_PENDING)
		{
			// 로그
		
		}

		
		return false;		

	}
	
	return true;
}


void CSocket::ShutDownSock()
{	
	shutdown(m_hSocket,SD_BOTH);
}

void CSocket::SocketClose()
{
	closesocket(m_hSocket);
}