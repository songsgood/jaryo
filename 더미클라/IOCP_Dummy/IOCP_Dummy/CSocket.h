#pragma once

#include <Winsock2.h>

#pragma comment(lib, "Ws2_32.lib")


class CSocket
{
public:
	CSocket();
	virtual ~CSocket();

	SOCKET GetSocket();

	void InitSocket();
	
	bool Connect();
	

	bool Send(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped);
	bool Recv(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped);

	void SetSocketIp(char* Ip);
	void SetSocketPort(unsigned short usPort);
	unsigned short GetSocketPort();

	void ShutDownSock();
	void SocketClose();

	 
private:	

	//---------------------------
	// ���� ����
	//---------------------------
	SOCKADDR_IN		m_SocketAddr;

	char			m_sAddr[16];
	unsigned short  m_usPort;


	
	SOCKET			m_hSocket;

}; 