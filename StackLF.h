#ifndef _STACKLF_H_
#define _STACKLF_H_

#include "MemoryPoolLF.h"

template<typename T>
class CStackLF
{
private:

	struct st_Node
	{
		T Data;
		st_Node *pNext;
	};

	union TOP_NODE
	{
		LONG64 Aligment;
		struct
		{
			st_Node *pNode;
			UINT32 iUniqueNum;
			
		};

	};

	//struct st_Top_Node
	//{
	//	st_Node *pNode;
	//	//INT64 iUniqueNum;
	//	UINT32 iUniqueNum;

	//};

public:
	CStackLF()
	{
		pTop = (TOP_NODE*)malloc(sizeof(TOP_NODE));
		pTop->pNode = NULL;
		pTop->iUniqueNum = 0;	
		//pTop = (st_Top_Node*)_aligned_malloc(sizeof(st_Top_Node), 16);
		//pTop->pNode = NULL;
		//pTop->iUniqueNum = 0;

		iCurrentSize = 0;
	}
	~CStackLF()
	{
		//free(pTop);
	}

	bool Push(T t);
	bool Pop(T &t);
	int Size();
	void Clear();

	//테스트용으로 public
	//CMemoryPool<st_Node> pool;




private:




	TOP_NODE *pTop;


	//long iMaxSize;
	long iCurrentSize;


};

template<typename T>
bool CStackLF<T>::Push(T t)
{

	st_Node *pNewNode = new st_Node();// pool.Alloc();

	pNewNode->Data = t;

	TOP_NODE CurTop;
	TOP_NODE NewTop;

	do
	{


		CurTop.Aligment = pTop->Aligment;
	

		NewTop.iUniqueNum = CurTop.iUniqueNum+1;		
		NewTop.pNode = pNewNode;

		pNewNode->pNext = pTop->pNode;


	} while (InterlockedCompareExchange64(&pTop->Aligment, NewTop.Aligment, CurTop.Aligment) != CurTop.Aligment);

	

	InterlockedIncrement(&iCurrentSize);

	return true;
}

template<typename T>
bool CStackLF<T>::Pop(T &t)
{
	int Count = InterlockedDecrement(&iCurrentSize);

	if (Count < 0)
	{
		InterlockedIncrement(&iCurrentSize);
		t = NULL;
		return false;
	}



	TOP_NODE CurTop;
	TOP_NODE NewTop;

	do
	{

		CurTop.Aligment = pTop->Aligment;


		NewTop.pNode = CurTop.pNode->pNext;		

		NewTop.iUniqueNum = CurTop.iUniqueNum + 1;
		
		

	} while (InterlockedCompareExchange64(&pTop->Aligment, NewTop.Aligment, CurTop.Aligment) != CurTop.Aligment);

	t = CurTop.pNode->Data;
	delete CurTop.pNode;
	//pool.Free(CurTop.pNode);




	return true;

}

template<typename T>
int CStackLF<T>::Size()
{
	return iCurrentSize;
}




template<typename T>
void CStackLF<T>::Clear()
{
	iCurrentSize = 0;
}

#endif // !_STACKLF_H_


