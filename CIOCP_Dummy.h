pragma once

include <WinSock2.h>
include <WS2tcpip.h>

include <process.h>


//라이브러리
include "CSession.h"
include "StackLockFree.h"


class CIOCP_Dummy
{

public:
	CIOCP_Dummy();
	~CIOCP_Dummy();

	bool Start();
	void Stop();
	
private:

	void InitSession();
	CSession* NewSession();
	

	static unsigned WINAPI ConnectThread(LPVOID arg);
	static unsigned WINAPI UpdateThread(LPVOID arg);
	static unsigned WINAPI WorkThread(LPVOID arg);
	static unsigned WINAPI LogThread(LPVOID arg);

	void OnConnectThread();
	void OnUpdateThread();
	void OnWorkThread();
	void OnLogThread();


	void CreateThreads();
	
	//-------------------
	// 패킷 처리
	//-------------------
	void PacketProcedure(CSession *pSession);

	//CPacketBuffer* HeadCheck(CSession *pSession);

	void Disconnect(CSession *pSession);
	void ReleaseSession(CSession *pSession);


	//cSession* SessionIO_Lock()

private:

	CSession *m_ArraySession;
	CStackLockFree<CSession*> m_StackSession;


	int m_MaxUser;

	HANDLE m_hIO;

	int m_iWorkThreadCount;


	
	HANDLE m_hConnectThread;
	HANDLE m_hWorkThread;
	HANDLE m_hUpdateThread;
	HANDLE m_hLogThread;


};
