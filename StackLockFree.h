
pragma once

template<typename T>
class CStackLockFree
{
private:

	struct st_Node
	{
		T Data;
		st_Node *pNext;
	};

	union TOP_NODE
	{
		LONG64 Aligment;
		struct
		{
			st_Node *pNode;
			UINT32 iUniqueNum;
			
		};

	};

	

public:
	CStackLockFree()
	{
		pTop = (TOP_NODE*)malloc(sizeof(TOP_NODE));
		pTop->pNode = NULL;
		pTop->iUniqueNum = 0;		

		iCurrentSize = 0;
	}
	~CStackLockFree()
	{
		//free(pTop);
	}

	bool Push(T t);
	bool Pop(T &t);
	int Size();
	void Clear();






private:




	TOP_NODE *pTop;

	long iCurrentSize;


};

template<typename T>
bool CStackLockFree<T>::Push(T t)
{

	st_Node *pNewNode = new st_Node();// pool.Alloc();

	pNewNode->Data = t;

	TOP_NODE CurTop;
	TOP_NODE NewTop;

	do
	{


		CurTop.Aligment = pTop->Aligment;
	

		NewTop.iUniqueNum = CurTop.iUniqueNum+1;		
		NewTop.pNode = pNewNode;

		pNewNode->pNext = pTop->pNode;


	} while (InterlockedCompareExchange64(&pTop->Aligment, NewTop.Aligment, CurTop.Aligment) != CurTop.Aligment);

	

	InterlockedIncrement(&iCurrentSize);

	return true;
}

template<typename T>
bool CStackLockFree<T>::Pop(T &t)
{
	int Count = InterlockedDecrement(&iCurrentSize);

	if (Count < 0)
	{
		InterlockedIncrement(&iCurrentSize);
		t = NULL;
		return false;
	}



	TOP_NODE CurTop;
	TOP_NODE NewTop;

	do
	{

		CurTop.Aligment = pTop->Aligment;


		NewTop.pNode = CurTop.pNode->pNext;		

		NewTop.iUniqueNum = CurTop.iUniqueNum + 1;
		
		

	} while (InterlockedCompareExchange64(&pTop->Aligment, NewTop.Aligment, CurTop.Aligment) != CurTop.Aligment);

	t = CurTop.pNode->Data;
	delete CurTop.pNode;





	return true;

}

template<typename T>
int CStackLockFree<T>::Size()
{
	return iCurrentSize;
}




template<typename T>
void CStackLockFree<T>::Clear()
{
	iCurrentSize = 0;
}


