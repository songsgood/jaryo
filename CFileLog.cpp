
#include "stdafx.h"
#include "CFileLog.h"

void CFileLog::System_Log(const char* file, int Line, const char* Type, const char* fmt, ...)
{

	char wzFileName[100] = { 0, };

	InterlockedIncrement(&Number);

	time_t t = time(NULL);
	tm tm;
	localtime_s(&tm, &t);

	char Buf[2048] = { 0, };

	int nResult = _mkdir("SystemLog\\");

	va_list Marker;
	va_start(Marker, fmt);

	StringCbVPrintfA(Buf, sizeof(Buf), fmt, Marker);

	va_end(Marker);

	char fileName[1000];


	//-----------------------------------
	// 일단위로 새로 만든다
	//-----------------------------------
	StringCbPrintfA(fileName, sizeof(fileName), "SystemLog\\%d_%d_%d_%s.txt", (tm.tm_year + 1900), tm.tm_mon + 1, tm.tm_mday, Type);

	FILE *fp = nullptr;
	while (true)
	{
		if (0 == fopen_s(&fp, fileName, "a"))
			break;
	}


	char ResultBuf[2048];
	StringCbPrintfA(ResultBuf, sizeof(ResultBuf), "%llu] [%s : %d] [%s][%02d-%02d-%02d  %02d:%02d:%02d ] %s \n", Number, file, Line, Type, (tm.tm_year + 1900), tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, Buf);



	fwrite(ResultBuf, 1, strlen(ResultBuf), fp);


	fclose(fp);

}