include "StdAfx.h"
include "CSession.h"

CSession::CSession(): m_lIOCount(0)
{
	InitSession();
}


void CSession::InitSession()
{	
	m_bSendFlag = false;
	m_bRecvFlag = false;

	m_bReleaseFlag = false;
}


long CSession::IncrementIO()
{
	return InterlockedIncrement(&m_lIOCount);
}

long CSession::DecrementIO()
{
	return InterlockedDecrement(&m_lIOCount);
}

void CSession::SetIO(long lCount)
{
	m_lIOCount = lCount;
}

bool CSession::SendPost()
{


}

void CSession::RecvPost()
{

	WSABUF wsaBuf[2];

	int iWsaBufCount = 0;

	//wsaBuf[iWsaBufCount].len = 

}


OVERLAPPED* CSession::GetSendOverlapped()
{
	return &m_SendOverlapped;
}

OVERLAPPED* CSession::GetRecvOverlapped()
{
	return &m_RecvOverlapped;
}

OVERLAPPED* CSession::GetSendPostOverlapped()
{
	return &m_SendPostOverlapped;
}


void CSession::Disconnect()
{
	m_Sock.Disconnect();

}
