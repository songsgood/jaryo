qpragma once
qinclude "Lock.h"

class CFSGameUser;
class CFSGameUserNewbieBenefit
{
public:
	BOOL LoadNewbieBenefit(void);
	void SendNewbieBenefitInfoNot(void);
	void SendNewbieBenefitList(void);
	void SendNewbieBenefitRewardList(BYTE btTargetLv);
	void SendNewbieBenefitRewardList(SC2S_NEWBIE_BENEFIT_REWARD_INFO_REQ& rq);
	void GiveRewardNewbieBenefit(SC2S_NEWBIE_BENEFIT_RECEIVING_REQ& rq);

	void AllRemoveNewbieBenefit(void);
public:
	inline bool IsNewbie()
	{
		return m_bIsNewbie;
	}

public:
	CFSGameUserNewbieBenefit(CFSGameUser* pUser);
	~CFSGameUserNewbieBenefit(void);

private:
	bool	m_bIsNewbie;
	int		m_iCompleteLevel;
	CFSGameUser* m_pUser;
};

