qpragma once
qinclude "Lock.h"

class CFSGameUser;
class CFSODBCBase;
class CFSGameUserComebackBenefit
{
public:
	CFSGameUserComebackBenefit(CFSGameUser* pUser);
	~CFSGameUserComebackBenefit(void);

public:
	BOOL LoadComebackBenefit();
	void LoginAttendanceGiveBenefit();
	void SendComebackBenefitInfoNot(void);
	void SendComebackBenefitList(void);
	void SendComebackBenefitRewardList(BYTE btTargetLv);
	void SendComebackBenefitRewardList(SC2S_COMEBACK_BENEFIT_REWARD_INFO_REQ& rq);
	void GiveRewardComebackBenefit(SC2S_COMEBACK_BENEFIT_RECEIVING_REQ& rq);
	BOOL CheckAndGiveRewardComebackBenefitwithAttendance(BOOL bCheckMailPopup);

	void IncreaseGamePlay();
	void IncreaseAttendance();
	void DecreaseAttendance();
	void SaveComebackBenefit(BOOL bSaveDate, CFSODBCBase* pODBCBase = nullptr);
	void CheckAttendance(BOOL bCheckMailPopup);

public:
	inline bool IsComeback()
	{
		return m_bComeback;
	}

private:
	CFSGameUser* m_pUser;
	BOOL		m_bComeback;
	int			m_iGamePlayCount;
	int			m_iGaveIndex; // with playcount
	int			m_iAttendanceCount;
	time_t		m_tAttendanceDate;
	int			m_iGaveAttendance;

	BOOL		m_bDBSave;
};

