qpragma once
qinclude "CSocketIOCPThread.h"
qinclude "ThreadODBCManager.h"

class CGameCheat
{
public:
	CGameCheat(void);
	~CGameCheat(void);
	static CGameCheat*	m_pInstance;
	static CGameCheat*	GetInstance()		
	{ 
		if(!m_pInstance) 
			m_pInstance=new CGameCheat; 
		return CGameCheat::m_pInstance; 
	}

	bool Initialize();
	void CheatDivide(CFSGameClient * pClient);
	
	void CheatLevel(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatTrophy(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatChampCount(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatPoint(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatCash(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatTraining(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatSkillSlot(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatAllSkill(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatAllFreeStyle(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	void CheatOneManTeam(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	bool CheatLogin(CFSGameClient * pClient, CFSGameODBC* pGameODBC);
	
	void SetOneManTeamOn()	{m_bOneManTeam=TRUE;};
	void SetOneManTeamOff()	{m_bOneManTeam=FALSE;};
	BOOL GetOneManTeam()    {return m_bOneManTeam;};

private:
	BOOL m_IsCheatInit;
	BOOL m_bOneManTeam;
	
};
