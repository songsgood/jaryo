qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserRandomItemBoardEvent
{
public:
	CFSGameUserRandomItemBoardEvent(CFSGameUser* pUser);
	~CFSGameUserRandomItemBoardEvent(void);

	BOOL Load();
	void SendRandomItemBoardEventInfo();
	void SaveRandomItemBoard(SC2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ rq);
	void SaveRandomItemBoard();
	void UseBoardTicket(SC2S_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_REQ rq);
	void SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET eResult);

private:
	CFSGameUser* m_pUser;
	BOOL		m_bDataLoaded;

	int m_RandomItemBoardInfo[MAX_EVENT_RANDOMBOARD_COUNT];	// 뽑기 이벤트 아이템바 위치 정보
	BOOL m_bRandomItemBoardLoaded;
	BOOL m_bRandomItemBoardUpdated;
};

