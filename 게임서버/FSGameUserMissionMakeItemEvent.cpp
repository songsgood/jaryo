qinclude "stdafx.h"
qinclude "FSGameUserMissionMakeItemEvent.h"
qinclude "MissionMakeItemEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserMissionMakeItemEvent::CFSGameUserMissionMakeItemEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_tLastResetTime(-1)
	, m_tStartConnectTime(-1)
	, m_iConnectSecond(0)
	, m_iTodayMakeCnt(0)
{
	for(int i = 0; i < MAX_MATERIAL_TYPE; ++i)
		m_iMaterial[i] = 0;
}


CFSGameUserMissionMakeItemEvent::~CFSGameUserMissionMakeItemEvent(void)
{
}

BOOL			CFSGameUserMissionMakeItemEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == MISSIONMAKEITEM.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_MISSIONMAKEITEM_GetUserData(m_pUser->GetUserIDIndex(), m_tLastResetTime, m_iConnectSecond, m_iTodayMakeCnt, m_iMaterial))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_MISSIONMAKEITEM_GetUserData failed");
		return FALSE;	
	}

	m_bDataLoaded = TRUE;
	m_tStartConnectTime = _time64(NULL);

	CheckResetDate(m_tStartConnectTime, TRUE);

	return TRUE;
}

BOOL			CFSGameUserMissionMakeItemEvent::CheckResetDate(time_t tCurrentTime/* = _time64(NULL)*/, BOOL bIsLogin/* = FALSE*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	BOOL	bResult = FALSE;

	if(-1 == m_tLastResetTime)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tLastResetTime, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= EVENT_RESET_HOUR;

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tLastResetTime <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_MISSIONMAKEITEM_ResetUserData(m_pUser->GetUserIDIndex(), 0, tCurrentTime))
		{			
			m_iConnectSecond = 0;
			m_iTodayMakeCnt = 0;
			m_tLastResetTime = tCurrentTime;
			m_tStartConnectTime = tCurrentTime; 

			if(FALSE == bIsLogin)	// 접속하고 있다가 리셋이 된 경우, 접속 시작시각을  그 날 09시부터로 설정.
			{
				TIMESTAMP_STRUCT	RecentResetHourDate;
				TimetToTimeStruct(m_tLastResetTime, RecentResetHourDate);
				RecentResetHourDate.hour = EVENT_RESET_HOUR;
				RecentResetHourDate.minute = 0;
				RecentResetHourDate.second = 0;

				m_tStartConnectTime = TimeStructToTimet(RecentResetHourDate);
			}

			for(int i = 0; i < MAX_MATERIAL_TYPE; ++i)
			{
				if(MATERIAL_BUY_EVENT_ITEM == i)
					continue;

				m_iMaterial[i] = 0;
			}
		}
	}

	return bResult;
}

void			CFSGameUserMissionMakeItemEvent::SendEventProductInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONMAKEITEM.IsOpen());

	CPacketComposer* pPacket = MISSIONMAKEITEM.GetPacketEventProductInfo();
	m_pUser->Send(pPacket);
}

void			CFSGameUserMissionMakeItemEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONMAKEITEM.IsOpen());

	BYTE btMaterialType = 0;
	int iRewardCnt = 0;
	int iClearValue = 0;
	time_t tCurrentTime = _time64(NULL);
	CheckAndUpdateMission(MISSION_CONDITION_TYPE_CONNECT_TIME_MIN, btMaterialType, iRewardCnt, tCurrentTime);

	SS2C_MISSION_MAKEITEM_USER_INFO_RES	ss;

	ss.iTodayMakeCnt = m_iTodayMakeCnt;

	for(int i = 0; i < MAX_MATERIAL_TYPE; ++i)
		ss.iMaterialCnt[i] = m_iMaterial[i];
	
	MISSIONMAKEITEM.GetRewardMaterialCnt(MISSION_CONDITION_TYPE_CONNECT_TIME_MIN, iClearValue, btMaterialType, iRewardCnt);

	ss.iRemainSec = (iClearValue * 60) - static_cast<int>(tCurrentTime - m_tStartConnectTime + m_iConnectSecond);

	if(ss.iRemainSec < 0)
		ss.iRemainSec = 0;

	m_pUser->Send(S2C_MISSION_MAKEITEM_USER_INFO_RES, &ss, sizeof(SS2C_MISSION_MAKEITEM_USER_INFO_RES));
}

BOOL			CFSGameUserMissionMakeItemEvent::CheckAndUpdateMission(BYTE btMissionConditionType, BYTE& btMaterialType, int& iRewardCnt, int iAddValue/* = 1*/, time_t tCurrentTime /*= _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MISSIONMAKEITEM.IsOpen(), FALSE);

	CheckResetDate(tCurrentTime);
	
	btMaterialType = 0;
	iRewardCnt = 0;

	int iClearValue = 0;
	int iConnectSecond = 0;
	int iClearCnt = 0;
	time_t tStartConnectTime = m_tStartConnectTime;

	CHECK_CONDITION_RETURN(FALSE == MISSIONMAKEITEM.GetRewardMaterialCnt(btMissionConditionType, iClearValue, btMaterialType, iRewardCnt), FALSE);
	CHECK_CONDITION_RETURN(0 == iRewardCnt || 0 == iClearValue, FALSE);

	switch(btMissionConditionType)
	{
	case MISSION_CONDITION_TYPE_PLAY_CNT:
	case MISSION_CONDITION_TYPE_PLAY_WIN:
	case MISSION_CONDITION_TYPE_BUY_EVENT_ITEM:
		{
			iClearCnt = iAddValue;
			iConnectSecond = m_iConnectSecond;
		}
		break;

	case MISSION_CONDITION_TYPE_CONNECT_TIME_MIN:
		{
			iClearCnt = ((tCurrentTime - m_tStartConnectTime + m_iConnectSecond) / 60) / iClearValue;

			CHECK_CONDITION_RETURN(iClearCnt <= 0, FALSE);

			iConnectSecond = (tCurrentTime - m_tStartConnectTime + m_iConnectSecond) - (iClearCnt * 60 * iClearValue);
			tStartConnectTime = tCurrentTime;
		}
		break;

	default:
		return FALSE;
	}

	int iMaterial[MAX_MATERIAL_TYPE] = {0,};
	ZeroMemory(iMaterial, sizeof(int) * MAX_MATERIAL_TYPE);
	iMaterial[btMaterialType] = iRewardCnt * iClearCnt;

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_MISSIONMAKEITEM_GiveMaterial(m_pUser->GetUserIDIndex(), iMaterial), FALSE);

	m_iMaterial[btMaterialType] += iMaterial[btMaterialType];
	m_iConnectSecond = iConnectSecond;
	m_tStartConnectTime = tStartConnectTime;

	return TRUE;
}

void			CFSGameUserMissionMakeItemEvent::MakeItem(BYTE btProductIndex)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	
	SS2C_MISSION_MAKEITEM_MAKE_ITEM_RES	ss;
	ss.btProductIndex = btProductIndex;

	int iRewardIndex = 0;
	int iNeedMaterial[MAX_MATERIAL_TYPE] = {0,};

	if(CLOSED == MISSIONMAKEITEM.IsOpen())
	{
		ss.btResult = RESULT_MAKE_ITEM_FAIL_EVENT_CLOSED;
	}
	else if(m_iTodayMakeCnt >= MISSIONMAKEITEM_DAILY_MAX_CNT)
	{
		ss.btResult = RESULT_MAKE_ITEM_FAIL_OVER_TODAY_MAKE_CNT;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_MAKE_ITEM_FAIL_FULL_MAILBOX;
	}
	else if(FALSE == MISSIONMAKEITEM.GetNeedMaterial(btProductIndex, iNeedMaterial))
	{
		ss.btResult = RESULT_MAKE_ITEM_FAIL;
	}
	else if(FALSE == MISSIONMAKEITEM.GetRandomReward(btProductIndex, ss.btRandomIndex, iRewardIndex))
	{
		ss.btResult = RESULT_MAKE_ITEM_FAIL;
	}
	else
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);

		BOOL bIsMaterialLack = FALSE;
		for(int i = 0; i < MAX_MATERIAL_TYPE; ++i)
		{
			if(m_iMaterial[i] < iNeedMaterial[i])
			{
				bIsMaterialLack = TRUE;
				break;
			}
		}

		if(bIsMaterialLack)
		{
			ss.btResult = RESULT_MAKE_ITEM_FAIL_LACK_MATERIAL;
		}
		else if(ODBC_RETURN_SUCCESS == pODBC->EVENT_MISSIONMAKEITEM_MakeItem(m_pUser->GetUserIDIndex(), ss.btProductIndex, ss.btRandomIndex, iNeedMaterial))
		{			
			ss.btResult = RESULT_MAKE_ITEM_SUCCESS;

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(NULL == pReward)
			{
				ss.btResult = RESULT_MAKE_ITEM_FAIL;
				WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_MISSIONMAKEITEM_MakeItem GiveReward Fail, UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), iRewardIndex);


		for(int i = 0; i < MAX_MATERIAL_TYPE; ++i)
				m_iMaterial[i] -= iNeedMaterial[i];

			if(++m_iTodayMakeCnt >= MISSIONMAKEITEM_DAILY_MAX_CNT)
			{
				if(ODBC_RETURN_SUCCESS == pODBC->EVENT_MISSIONMAKEITEM_ResetUserData(m_pUser->GetUserIDIndex(), MISSIONMAKEITEM_DAILY_MAX_CNT, m_tLastResetTime))
				{
					m_iTodayMakeCnt = MISSIONMAKEITEM_DAILY_MAX_CNT;

					for(int i = 0; i < MAX_MATERIAL_TYPE; ++i)
					{
						if(MATERIAL_BUY_EVENT_ITEM == i)
							continue;

						m_iMaterial[i] = 0;
					}
				}
			}
		}
	}

	m_pUser->Send(S2C_MISSION_MAKEITEM_MAKE_ITEM_RES, &ss, sizeof(SS2C_MISSION_MAKEITEM_MAKE_ITEM_RES));
}

BOOL			CFSGameUserMissionMakeItemEvent::BuyCashItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INVALED_DATA, CHECK_FAIL, "BuyCashItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	int iPropertyKind = pBillingInfo->iParam0;

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	SS2C_BUY_EVENT_ITEM_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_BUY_EVENT_ITEM_NOT));

	ss.iEventKind = EVENT_KIND_MISSION_MAKEITEM;
	ss.iPropertyKind = iPropertyKind;

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int	iBuyCount = pBillingInfo->iParam1;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	int iMaterial[MAX_MATERIAL_TYPE] = {0,};
	ZeroMemory(iMaterial, sizeof(int) * MAX_MATERIAL_TYPE);
	iMaterial[MATERIAL_BUY_EVENT_ITEM] = iBuyCount;
	
	BYTE btMaterialType = 0;
	int iRewardCnt = 0;

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_BuyEventCashItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, iBuyCount, iPrevCash, iPostCash, pBillingInfo->iItemPrice))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, NONE, "EVENT_BuyEventCashItem Fail, User:10752790, itemCode:0, BuyCount:7106560", m_pUser->GetUserIDIndex(), iItemCode, iBuyCount);
ult = RESULT_BUY_EVENT_ITEM_FAIL;
	}
	else if(TRUE == CheckAndUpdateMission(MISSION_CONDITION_TYPE_BUY_EVENT_ITEM, btMaterialType, iRewardCnt, iBuyCount))
	{
		ss.btResult = RESULT_BUY_EVENT_ITEM_SUCCESS;
		ss.iResultCount = m_iMaterial[MATERIAL_BUY_EVENT_ITEM];

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);

	m_pUser->Send(S2C_BUY_EVENT_ITEM_NOT, &ss, sizeof(SS2C_BUY_EVENT_ITEM_NOT));

	int iCostType = pBillingInfo->iSellType;
	m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
	m_pUser->SendUserGold();

	return (ss.btResult == RESULT_BUY_EVENT_ITEM_SUCCESS);
}

BOOL			CFSGameUserMissionMakeItemEvent::SaveStartConnectTime()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MISSIONMAKEITEM.IsOpen(), FALSE);

	BYTE btMaterialType = 0;
	int iRewardCnt = 0;
	CHECK_CONDITION_RETURN(TRUE == CheckAndUpdateMission(MISSION_CONDITION_TYPE_CONNECT_TIME_MIN, btMaterialType, iRewardCnt), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_MISSIONMAKEITEM_SaveStartConnectTime(m_pUser->GetUserIDIndex(), _time64(NULL) - m_tStartConnectTime + m_iConnectSecond), FALSE);

	return TRUE;
}

void			CFSGameUserMissionMakeItemEvent::SetMaterialEventItem(int iCount)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONMAKEITEM.IsOpen());

	m_iMaterial[MATERIAL_BUY_EVENT_ITEM] = iCount;
}