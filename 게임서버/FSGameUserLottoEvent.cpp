qinclude "stdafx.h"
qinclude "LottoEventManager.h"
qinclude "FSGameUserLottoEvent.h"
qinclude "TUser.h"
qinclude "FSServer.h"
qinclude "RewardManager.h"

CFSGameUserLottoEvent::CFSGameUserLottoEvent(CUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserLottoEvent::~CFSGameUserLottoEvent(void)
{
}

BOOL CFSGameUserLottoEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<SODBCLottoEventUserStatus> vStatus;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_LOTTO_GetUserNumber(m_pUser->GetUserIDIndex(), m_UserLottoNumber._iLotteryNumber1, m_UserLottoNumber._iLotteryNumber2, m_UserLottoNumber._iUpdateDate) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_LOTTO_GetUserStatus(m_pUser->GetUserIDIndex(), vStatus))
	{
		WRITE_LOG_NEW(LOG_TYPE_LOTTOEVENT, INITIALIZE_DATA, FAIL, "EVENT_LOTTO_GetUserNumber error");
		return FALSE;
	}

	for(int i = 0; i < vStatus.size(); ++i)
	{
		SetUserStatus(vStatus[i]._iSlotNumber, vStatus[i]._iRandomNumber, vStatus[i]._iRewardIndex, vStatus[i]._btRewardStatus);
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserLottoEvent::SetUserStatus(int iSlotNumber, int iRandomNumber, int iRewardIndex, BYTE btRewardStatus)
{
	BYTE btSlotStatus = EVENT_LOTTO_SLOT_STATUS_CLOSED;
	if(iRandomNumber > 0 && iRewardIndex > 0)
	{
		if(iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
			iRandomNumber == m_UserLottoNumber._iLotteryNumber2)
			btSlotStatus = EVENT_LOTTO_SLOT_STATUS_WIN;
		else
			btSlotStatus = EVENT_LOTTO_SLOT_STATUS_BED;
	}

	SLottoEventUserStatus Status;
	Status.SetUserStatus(iSlotNumber, btSlotStatus, iRandomNumber, iRewardIndex, btRewardStatus);
	m_mapUserLottoStatus[iSlotNumber] = Status;
}

void CFSGameUserLottoEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == LOTTOEVENT.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_EVENT_LOTTO_INFO_RES rs;
	rs.btOpenStatus = LOTTOEVENT.IsOpenTime();
	rs.iLotteryNumber1 = 0;
	rs.iLotteryNumber2 = 0;
	rs.iCount = 0;

	CPacketComposer Packet(S2C_EVENT_LOTTO_INFO_RES);
	if(OPEN == rs.btOpenStatus)
	{
		int iYYMMDD = GetYYYYMMDD();
		if(0 == m_UserLottoNumber._iUpdateDate ||
			iYYMMDD > m_UserLottoNumber._iUpdateDate)
		{
			CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
			CHECK_NULL_POINTER_VOID(pODBC);

			if (ODBC_RETURN_SUCCESS == pODBC->EVENT_LOTTO_ClearUserData(m_pUser->GetUserIDIndex(), iYYMMDD))
			{
				m_UserLottoNumber._iLotteryNumber1 = rs.iLotteryNumber1;
				m_UserLottoNumber._iLotteryNumber2 = rs.iLotteryNumber2;
				m_UserLottoNumber._iUpdateDate = iYYMMDD;

				m_mapUserLottoStatus.clear();
			}
		}

		rs.iLotteryNumber1 = m_UserLottoNumber._iLotteryNumber1;
		rs.iLotteryNumber2 = m_UserLottoNumber._iLotteryNumber2;
		rs.iCount = LOTTOEVENT.GetMaxSlotNumber();
		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_LOTTO_INFO_RES));

		SEVENT_LOTTO_SLOT info;
		for(int iSlot = 0; iSlot < rs.iCount; ++iSlot)
		{
			info.btRewardType = 0;	
			info.iItemCode = 0;	
			info.iPropertyKind = 0;	
			info.iPropertyType = 0;	
			info.iPropertyValue = 0;	

			EVENT_LOTTO_USER_STATUS_MAP::iterator iter = m_mapUserLottoStatus.find(iSlot);
			if(iter != m_mapUserLottoStatus.end())
			{
				info.btSlotStatus = iter->second._btSlotStatus;
				info.iSlotNumber = iter->second._iSlotNumber;
				info.iRandomNumber = iter->second._iRandomNumber;
				info.iRewardIndex = iter->second._iRewardIndex;
				LOTTOEVENT.MakeRewardInfo(info);
			}
			else
			{
				info.btSlotStatus = EVENT_LOTTO_SLOT_STATUS_CLOSED;
				info.iSlotNumber = iSlot;
				info.iRandomNumber = 0;
				info.iRewardIndex = 0;
			}
			Packet.Add((PBYTE)&info, sizeof(SEVENT_LOTTO_SLOT));
		}
	}
	else
	{
		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_LOTTO_INFO_RES));
	}

	m_pUser->Send(&Packet);
}

BOOL CFSGameUserLottoEvent::CheckAndGiveReward()
{
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpenTime(), FALSE);

	CHECK_CONDITION_RETURN(m_mapUserLottoStatus.empty(), FALSE);

	map<int, SODBCLottoEventUserStatus> mapUserStatus;
	SODBCLottoEventUserStatus Status;

	EVENT_LOTTO_USER_STATUS_MAP::iterator iterStatus = m_mapUserLottoStatus.begin();
	for( ; iterStatus != m_mapUserLottoStatus.end(); ++iterStatus)
	{
		if(iterStatus->second._iRandomNumber > 0 &&
			(iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 || 
			iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber2) &&
			iterStatus->second._iRewardIndex > 0 &&
			iterStatus->second._btRewardStatus == EVENT_LOTTO_USER_REWARD_STATUS_NONE)
		{
			Status._iSlotNumber = iterStatus->second._iSlotNumber;
			Status._iRandomNumber = iterStatus->second._iRandomNumber;
			Status._iRewardIndex = iterStatus->second._iRewardIndex;
			Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED;		
			mapUserStatus.insert(map<int, SODBCLottoEventUserStatus>::value_type(Status._iSlotNumber, Status));
		}
	}

	CHECK_CONDITION_RETURN(mapUserStatus.empty(), FALSE);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+mapUserStatus.size() > MAX_PRESENT_LIST, FALSE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, FALSE);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_LOTTO_UpdateUserStatus(m_pUser->GetUserIDIndex(), LOTTOEVENT.GetMaxSlotNumber(), mapUserStatus), FALSE);

	map<int, SODBCLottoEventUserStatus>::iterator iter = mapUserStatus.begin();
	for( ; iter != mapUserStatus.end(); ++iter)
	{
		int iSlotNumber = iter->second._iSlotNumber;
		int iRandomNumer = iter->second._iRandomNumber;
		int iRewardIndex = iter->second._iRewardIndex;
		BYTE btRewardStatus = iter->second._btRewardStatus;
		BYTE btSlotStatus = EVENT_LOTTO_SLOT_STATUS_CLOSED;

		EVENT_LOTTO_USER_STATUS_MAP::iterator iterStatus = m_mapUserLottoStatus.find(iSlotNumber);
		if(iterStatus != m_mapUserLottoStatus.end())
		{
			if(iterStatus->second._iRandomNumber > 0 && iterStatus->second._iRewardIndex > 0)
			{
				if(EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED == iter->second._btRewardStatus &&
					EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED != iterStatus->second._btRewardStatus)
				{
					SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
					if (NULL == pReward)
					{
						WRITE_LOG_NEW(LOG_TYPE_LOTTOEVENT, SET_DATA, FAIL, "CheckAndGiveReward error - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
User->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
					}
				}	

				if(iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
					iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber2)
					btSlotStatus = EVENT_LOTTO_SLOT_STATUS_WIN;
				else
					btSlotStatus = EVENT_LOTTO_SLOT_STATUS_BED;

				iterStatus->second._btSlotStatus = btSlotStatus;
				iterStatus->second._btRewardStatus = btRewardStatus;
			}		
		}
	}

	return TRUE;
}

RESULT_EVENT_LOTTO_OPEN_LOTTERY_NUMBER CFSGameUserLottoEvent::OpenLotteryNumberReq(int iSlot, int& iLotteryNumber)
{
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpen() ||
		CLOSED == LOTTOEVENT.IsOpenTime(), RESULT_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_CLOSED_EVENT);

	CHECK_CONDITION_RETURN((iSlot != EVENT_LOTTO_LOTTERY_NUMBER_SLOT_TYPE_1 && iSlot != EVENT_LOTTO_LOTTERY_NUMBER_SLOT_TYPE_2) ||
		(EVENT_LOTTO_LOTTERY_NUMBER_SLOT_TYPE_1 == iSlot && 0 != m_UserLottoNumber._iLotteryNumber1) ||
		(EVENT_LOTTO_LOTTERY_NUMBER_SLOT_TYPE_2 == iSlot && 0 != m_UserLottoNumber._iLotteryNumber2), RESULT_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_FAIL);

	LOTTOEVENT.MakeLotteryNumber(iSlot, m_UserLottoNumber._iLotteryNumber1, m_UserLottoNumber._iLotteryNumber2);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_FAIL);

	int iYYMMDD = GetYYYYMMDD();
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_LOTTO_UpdateUserNumer(m_pUser->GetUserIDIndex(), m_UserLottoNumber._iLotteryNumber1, m_UserLottoNumber._iLotteryNumber2, iYYMMDD))
	{
		if(EVENT_LOTTO_LOTTERY_NUMBER_SLOT_TYPE_1 == iSlot)
			m_UserLottoNumber._iLotteryNumber1 = 0;
		else
			m_UserLottoNumber._iLotteryNumber2 = 0;
		
		return RESULT_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_FAIL;
	}

	m_UserLottoNumber._iUpdateDate = iYYMMDD;

	if(EVENT_LOTTO_LOTTERY_NUMBER_SLOT_TYPE_1 == iSlot)
		iLotteryNumber = m_UserLottoNumber._iLotteryNumber1;
	else
		iLotteryNumber = m_UserLottoNumber._iLotteryNumber2;

	CheckAndGiveReward();

	return RESULT_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_SUCCESS;
}

RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER CFSGameUserLottoEvent::OpenRandomNumberReq(SC2S_EVENT_LOTTO_OPEN_RANDOM_NUMBER_REQ rq)
{
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpen(), RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpenTime(), RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(EVENT_LOTTO_OPEN_SLOT_TYPE_NONE != rq.btOpenSlotType && EVENT_LOTTO_OPEN_SLOT_TYPE_ALL != rq.btOpenSlotType, RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL);

	int iYYMMDD = GetYYYYMMDD();
	if(0 == m_UserLottoNumber._iUpdateDate ||
		iYYMMDD > m_UserLottoNumber._iUpdateDate)
	{
		SendEventInfo();
		return RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL;
	}

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL);

	map<int, SODBCLottoEventUserStatus> mapUserStatus;
	SODBCLottoEventUserStatus Status;

	if(EVENT_LOTTO_OPEN_SLOT_TYPE_NONE == rq.btOpenSlotType)
	{
		CHECK_CONDITION_RETURN(rq.iSlotNumber < 0 || rq.iSlotNumber >= LOTTOEVENT.GetMaxSlotNumber(), RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL);

		Status._iSlotNumber = rq.iSlotNumber;
		Status._iRewardIndex = 0;
		Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_NONE;

		EVENT_LOTTO_USER_STATUS_MAP::iterator iter = m_mapUserLottoStatus.find(rq.iSlotNumber);
		if(iter != m_mapUserLottoStatus.end())
		{
			CHECK_CONDITION_RETURN(iter->second._iRandomNumber > 0, RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL_ALREADY_OPEN);

			if(iter->second._iRewardIndex > 0)
			{
				Status._iRewardIndex = iter->second._iRewardIndex;		
				Status._btRewardStatus = iter->second._btRewardStatus;
			}
		}

		Status._iRandomNumber = LOTTOEVENT.GetRandomNumber();
		CHECK_CONDITION_RETURN(0 >= Status._iRandomNumber || LOTTOEVENT.GetMaxRandomNumber() < Status._iRandomNumber, RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL);

		if(Status._iRewardIndex > 0 &&
			Status._btRewardStatus == EVENT_LOTTO_USER_REWARD_STATUS_NONE &&
			(Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
			Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber2))
		{
			Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED;
		}

		mapUserStatus.insert(map<int, SODBCLottoEventUserStatus>::value_type(rq.iSlotNumber, Status));
	}
	else
	{
		for(int iSlot = 0; iSlot < LOTTOEVENT.GetMaxSlotNumber(); ++iSlot)
		{
			Status._iSlotNumber = iSlot;
			Status._iRewardIndex = 0;
			Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_NONE;

			EVENT_LOTTO_USER_STATUS_MAP::iterator iter = m_mapUserLottoStatus.find(iSlot);
			if(iter != m_mapUserLottoStatus.end())
			{
				if(iter->second._iRandomNumber > 0)
					continue;

				if(iter->second._iRewardIndex > 0)
				{
					Status._iRewardIndex = iter->second._iRewardIndex;		
					Status._btRewardStatus = iter->second._btRewardStatus;
				}
			}

			Status._iRandomNumber = LOTTOEVENT.GetRandomNumber();
			CHECK_CONDITION_RETURN(0 >= Status._iRandomNumber  || LOTTOEVENT.GetMaxRandomNumber() < Status._iRandomNumber , RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL);

			if(Status._iRewardIndex > 0 &&
				Status._btRewardStatus == EVENT_LOTTO_USER_REWARD_STATUS_NONE &&
				(Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
				Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber2))
			{
				Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED;
			}

			mapUserStatus.insert(map<int, SODBCLottoEventUserStatus>::value_type(iSlot, Status));
		}
	}

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+mapUserStatus.size() > MAX_PRESENT_LIST, RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL_FULL_MAILBOX);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_LOTTO_UpdateUserStatus(m_pUser->GetUserIDIndex(), LOTTOEVENT.GetMaxSlotNumber(), mapUserStatus), RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_FAIL);

	SS2C_EVENT_LOTTO_OPEN_RANDOM_NUMBER_RES rs;
	rs.btResult = RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_SUCCESS;
	rs.iSlotCount = mapUserStatus.size();

	CPacketComposer Packet(S2C_EVENT_LOTTO_OPEN_RANDOM_NUMBER_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_LOTTO_OPEN_RANDOM_NUMBER_RES));

	SEVENT_LOTTO_RANDOM_NUBER info;
	map<int, SODBCLottoEventUserStatus>::iterator iter = mapUserStatus.begin();
	for( ; iter != mapUserStatus.end(); ++iter)
	{
		int iSlotNumber = iter->second._iSlotNumber;
		int iRandomNumer = iter->second._iRandomNumber;
		int iRewardIndex = iter->second._iRewardIndex;
		BYTE btRewardStatus = iter->second._btRewardStatus;
		BYTE btSlotStatus = EVENT_LOTTO_SLOT_STATUS_CLOSED;

		EVENT_LOTTO_USER_STATUS_MAP::iterator iterStatus = m_mapUserLottoStatus.find(iSlotNumber);
		if(iterStatus != m_mapUserLottoStatus.end())
		{
			iterStatus->second._iRandomNumber = iRandomNumer;

			if(iterStatus->second._iRandomNumber > 0 && iterStatus->second._iRewardIndex > 0)
			{
				if(EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED == iter->second._btRewardStatus &&
					EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED != iterStatus->second._btRewardStatus)
				{
					SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
					if (NULL == pReward)
					{
						WRITE_LOG_NEW(LOG_TYPE_LOTTOEVENT, SET_DATA, FAIL, "OpenRandomNumberReq error - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
User->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
					}
				}	

				if(iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
					iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber2)
					btSlotStatus = EVENT_LOTTO_SLOT_STATUS_WIN;
				else
					btSlotStatus = EVENT_LOTTO_SLOT_STATUS_BED;

				iterStatus->second._btSlotStatus = btSlotStatus;
				iterStatus->second._btRewardStatus = btRewardStatus;
			}		
		}
		else
		{
			SLottoEventUserStatus Status;
			Status.SetUserStatus(iSlotNumber, btSlotStatus, iRandomNumer, iRewardIndex, btRewardStatus);
			m_mapUserLottoStatus.insert(EVENT_LOTTO_USER_STATUS_MAP::value_type(iSlotNumber, Status));
		}

		info.btSlotStatus = btSlotStatus;
		info.iSlotNumber = iSlotNumber;
		info.iRandomNumber = iRandomNumer;
		Packet.Add((PBYTE)&info, sizeof(SEVENT_LOTTO_RANDOM_NUBER));
	}

	m_pUser->Send(&Packet);

	return RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_SUCCESS;
}

RESULT_EVENT_LOTTO_OPEN_REWARD CFSGameUserLottoEvent::OpenRewardReq(SC2S_EVENT_LOTTO_OPEN_REWARD_REQ rq)
{
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpen(), RESULT_EVENT_LOTTO_OPEN_REWARD_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(CLOSED == LOTTOEVENT.IsOpenTime(), RESULT_EVENT_LOTTO_OPEN_REWARD_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(EVENT_LOTTO_OPEN_SLOT_TYPE_NONE != rq.btOpenSlotType && EVENT_LOTTO_OPEN_SLOT_TYPE_ALL != rq.btOpenSlotType, RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL);

	int iYYMMDD = GetYYYYMMDD();
	if(0 == m_UserLottoNumber._iUpdateDate ||
		iYYMMDD > m_UserLottoNumber._iUpdateDate)
	{
		SendEventInfo();
		return RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL;
	}

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL);

	map<int, SODBCLottoEventUserStatus> mapUserStatus;
	SODBCLottoEventUserStatus Status;
	
	if(EVENT_LOTTO_OPEN_SLOT_TYPE_NONE == rq.btOpenSlotType)
	{
		CHECK_CONDITION_RETURN(rq.iSlotNumber < 0 || rq.iSlotNumber >= LOTTOEVENT.GetMaxSlotNumber(), RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL);

		Status._iSlotNumber = rq.iSlotNumber;
		Status._iRewardIndex = 0;
		Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_NONE;

		EVENT_LOTTO_USER_STATUS_MAP::iterator iter = m_mapUserLottoStatus.find(rq.iSlotNumber);
		if(iter != m_mapUserLottoStatus.end())
		{
			CHECK_CONDITION_RETURN(iter->second._iRewardIndex > 0, RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL_ALREADY_OPEN);

			if(iter->second._iRandomNumber > 0)
			{
				Status._iRandomNumber = iter->second._iRandomNumber;
				Status._btRewardStatus = iter->second._btRewardStatus;
			}
		}

		Status._iRewardIndex = LOTTOEVENT.GetRandomRewardIndex();
		CHECK_CONDITION_RETURN(0 >= Status._iRewardIndex, RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL);

		if(Status._iRewardIndex > 0 &&
			Status._btRewardStatus == EVENT_LOTTO_USER_REWARD_STATUS_NONE &&
			m_UserLottoNumber._iLotteryNumber1 > 0 && m_UserLottoNumber._iLotteryNumber1 > 0 &&
			(Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
			Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber2))
		{
			Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED;
		}

		mapUserStatus.insert(map<int, SODBCLottoEventUserStatus>::value_type(rq.iSlotNumber, Status));
	}
	else
	{
		for(int iSlot = 0; iSlot < LOTTOEVENT.GetMaxSlotNumber(); ++iSlot)
		{
			Status._iSlotNumber = iSlot;
			Status._iRandomNumber = 0;
			Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_NONE;

			EVENT_LOTTO_USER_STATUS_MAP::iterator iter = m_mapUserLottoStatus.find(iSlot);
			if(iter != m_mapUserLottoStatus.end())
			{
				if(iter->second._iRewardIndex > 0)
					continue;

				if(iter->second._iRandomNumber > 0)
				{
					Status._iRandomNumber = iter->second._iRandomNumber;
					Status._btRewardStatus = iter->second._btRewardStatus;
				}
			}

			Status._iRewardIndex = LOTTOEVENT.GetRandomRewardIndex();
			CHECK_CONDITION_RETURN(0 >= Status._iRewardIndex, RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL);

			if(Status._iRewardIndex > 0 &&
				Status._btRewardStatus == EVENT_LOTTO_USER_REWARD_STATUS_NONE &&
				m_UserLottoNumber._iLotteryNumber1 > 0 && m_UserLottoNumber._iLotteryNumber1 > 0 &&
				(Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
				Status._iRandomNumber == m_UserLottoNumber._iLotteryNumber2))
			{
				Status._btRewardStatus = EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED;
			}

			mapUserStatus.insert(map<int, SODBCLottoEventUserStatus>::value_type(iSlot, Status));
		}
	}

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+mapUserStatus.size() > MAX_PRESENT_LIST, RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL_FULL_MAILBOX);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_LOTTO_UpdateUserStatus(m_pUser->GetUserIDIndex(), LOTTOEVENT.GetMaxSlotNumber(), mapUserStatus), RESULT_EVENT_LOTTO_OPEN_REWARD_FAIL);

	SS2C_EVENT_LOTTO_OPEN_REWARD_RES rs;
	rs.btResult = RESULT_EVENT_LOTTO_OPEN_REWARD_SUCCESS;
	rs.iSlotCount = mapUserStatus.size();

	CPacketComposer Packet(S2C_EVENT_LOTTO_OPEN_REWARD_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_LOTTO_OPEN_REWARD_RES));

	SEVENT_LOTTO_RANDOM_REWARD info;
	map<int, SODBCLottoEventUserStatus>::iterator iter = mapUserStatus.begin();
	for( ; iter != mapUserStatus.end(); ++iter)
	{
		int iSlotNumber = iter->second._iSlotNumber;
		int iRandomNumer = iter->second._iRandomNumber;
		int iRewardIndex = iter->second._iRewardIndex;
		BYTE btRewardStatus = iter->second._btRewardStatus;
		BYTE btSlotStatus = EVENT_LOTTO_SLOT_STATUS_CLOSED;

		EVENT_LOTTO_USER_STATUS_MAP::iterator iterStatus = m_mapUserLottoStatus.find(iSlotNumber);
		if(iterStatus != m_mapUserLottoStatus.end())
		{
			iterStatus->second._iRewardIndex = iRewardIndex;

			if(iterStatus->second._iRandomNumber > 0 && iterStatus->second._iRewardIndex > 0)
			{
				if(EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED == iter->second._btRewardStatus &&
					EVENT_LOTTO_USER_REWARD_STATUS_COMPLETED != iterStatus->second._btRewardStatus)
				{
					SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
					if (NULL == pReward)
					{
						WRITE_LOG_NEW(LOG_TYPE_LOTTOEVENT, SET_DATA, FAIL, "OpenRewardReq error - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
User->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
					}
				}	

				if(iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber1 ||
					iterStatus->second._iRandomNumber == m_UserLottoNumber._iLotteryNumber2)
					btSlotStatus = EVENT_LOTTO_SLOT_STATUS_WIN;
				else
					btSlotStatus = EVENT_LOTTO_SLOT_STATUS_BED;

				iterStatus->second._btSlotStatus = btSlotStatus;
				iterStatus->second._btRewardStatus = btRewardStatus;
			}
		}
		else
		{
			SLottoEventUserStatus Status;
			Status.SetUserStatus(iSlotNumber, btSlotStatus, iRandomNumer, iRewardIndex, btRewardStatus);
			m_mapUserLottoStatus.insert(EVENT_LOTTO_USER_STATUS_MAP::value_type(iSlotNumber, Status));
		}

		info.btSlotStatus = btSlotStatus;
		info.iSlotNumber = iSlotNumber;
		info.iRewardIndex = iRewardIndex;
		LOTTOEVENT.MakeRewardInfo(info);
		Packet.Add((PBYTE)&info, sizeof(SEVENT_LOTTO_RANDOM_REWARD));
	}

	m_pUser->Send(&Packet);

	return RESULT_EVENT_LOTTO_OPEN_REWARD_SUCCESS;
}
