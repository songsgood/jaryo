qinclude "stdafx.h"
qinclude "ChatSvrProxy.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameServer.h"
qinclude "LobbyChatUserManager.h"

#define DEFINE_CHATPROXY_PROC_FUNC(x) void CChatSvrProxy::Proc_##x(CReceivePacketBuffer* rp)

CChatSvrProxy::CChatSvrProxy(void)
{
	SetState(FS_CHAT_SERVER_PROXY);
}

CChatSvrProxy::~CChatSvrProxy(void)
{
}

void CChatSvrProxy::SendFirstPacket()
{
	CPacketComposer PacketComposer(S2T_CLIENT_INFO);
	SS2T_CLIENT_INFO info;

	CFSGameServer* pServer = (CFSGameServer*)GetServer();

	info.iProcessID = pServer->GetProcessID();	
	info.iServerType = pServer->GetServerType();

	PacketComposer.Add((BYTE*)&info, sizeof(SS2T_CLIENT_INFO));

	Send(&PacketComposer);

	pServer->BroadCastChatServerStatus(SERVER_ON);
}

void CChatSvrProxy::ProcessPacket(CReceivePacketBuffer* recvPacket)
{
#ifndef CASE_CHAT_PROXY_PACKET_PROC
#define CASE_CHAT_PROXY_PACKET_PROC(x)	case x:		Proc_##x(recvPacket);	break;
#endif

	switch(recvPacket->GetCommand())
	{
		CASE_CHAT_PROXY_PACKET_PROC(T2S_ENTER_PRIVATE_CHAT_ROOM_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_EXIT_PRIVATE_CHAT_ROOM_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_PRIVATE_CHAT_USER_LIST_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_PRIVATE_CHAT_USER_INFO_NOT);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_CHAT);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_FACTION_CHAT);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_LOBBY_CHAT_ROOM_LIST_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_ENTER_LOBBY_CHAT_ROOM_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_AUTO_ENTER_LOBBY_CHAT_ROOM_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_LOBBY_CHAT_ROOM_USER_LIST_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_CHAT_ROOM_NAME_RES);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_WORLD_END_NOT);
		CASE_CHAT_PROXY_PACKET_PROC(T2S_CHAT_REPEAT_BAN_RES);
	}
}


DEFINE_CHATPROXY_PROC_FUNC(T2S_ENTER_PRIVATE_CHAT_ROOM_RES)
{
	ST2S_ENTER_PRIVATE_CHAT_ROOM_RES rs;
	rp->Read((PBYTE)&rs, sizeof(ST2S_ENTER_PRIVATE_CHAT_ROOM_RES));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	if (pUser != NULL)
	{
		CPacketComposer Packet(S2C_ENTER_PRIVATE_CHAT_ROOM_RES);
		Packet.Add(rs.iResult);
		if (RESULT_COMMON_SUCCESS == rs.iResult)
		{
			Packet.Add((PBYTE)rs.szChatName, MAX_PRIVATE_CHAT_ROOM_NAME_LEN+1);
		}

		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_EXIT_PRIVATE_CHAT_ROOM_RES)
{
	ST2S_BASE rs;
	rp->Read((PBYTE)&rs, sizeof(ST2S_BASE));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	if (pUser != NULL)
	{
		CPacketComposer Packet(S2C_EXIT_PRIVATE_CHAT_ROOM_RES);

		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_PRIVATE_CHAT_USER_LIST_RES)
{
	ST2S_BASE rs;
	rp->Read((PBYTE)&rs, sizeof(ST2S_BASE));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	if (pUser != NULL)
	{
		CPacketComposer Packet(S2C_PRIVATE_CHAT_USER_LIST_RES);
		Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());

		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_PRIVATE_CHAT_USER_INFO_NOT)
{
	ST2S_PRIVATE_CHAT_USER_INFO_NOT rs;
	rp->Read((PBYTE)&rs, sizeof(ST2S_PRIVATE_CHAT_USER_INFO_NOT));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	if (pUser != NULL)
	{
		CPacketComposer Packet(S2C_PRIVATE_CHAT_USER_INFO_NOT);
		Packet.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);
		Packet.Add(rs.btUserIOState);

		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_CHAT)
{
	ST2S_CHAT rs;
	char szText[MAX_CHATBUFF_LENGTH+1] = {0};

	rp->Read((PBYTE)&rs, sizeof(ST2S_CHAT));
	rp->Read((PBYTE)szText, rs.iChatSize);

	int iChatState = rs.iRoomState == CHAT_STATE_PRIVATE ? CHAT_TYPE_PRIVATE : CHAT_TYPE_LOBBY;

	CPacketComposer Packet(S2C_CHAT);

	Packet.Add((int)iChatState);
	Packet.Add(rs.btIsGM);
	Packet.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH + 1 );	
	Packet.Add(&rs.iChatSize);
	Packet.Add((PBYTE)szText, rs.iChatSize + 1) ;

	if(rs.iRoomState == CHAT_STATE_LOBBY)
	{
		LOBBYCHAT.Broadcast(Packet, rs.iRoomType, rs.iRoomIndex);
	}
	else
	{
		CScopedRefGameUser pUser(rs.iGameIDIndex);
		if (pUser != NULL)
		{
			pUser->Send(&Packet);
		}
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_FACTION_CHAT)
{
	ST2S_CHAT rs;
	char szText[MAX_CHATBUFF_LENGTH+1] = {0};
	int	iFactionIndex = 0;

	rp->Read((PBYTE)&rs, sizeof(ST2S_CHAT));
	rp->Read((PBYTE)&iFactionIndex, sizeof(int));
	rp->Read((PBYTE)szText, rs.iChatSize);
	
	CPacketComposer Packet(S2C_CHAT);

	Packet.Add((int)CHAT_TYPE_FACTION);
	Packet.Add(rs.btIsGM);
	Packet.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH + 1 );	
	Packet.Add(&rs.iChatSize);
	Packet.Add((PBYTE)szText, rs.iChatSize + 1) ;

	LOBBYCHAT.BroadcastFaction(Packet, iFactionIndex);
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_LOBBY_CHAT_ROOM_LIST_RES)
{
	ST2S_LOBBY_CHAT_ROOM_LIST_RES rs;
	rp->Read((PBYTE)&rs, sizeof(ST2S_LOBBY_CHAT_ROOM_LIST_RES));

	CScopedRefGameUser pUser(rs.iGameIDIndex);

	if(pUser != NULL && rs.iRoomCount > 0)
	{
		int iPacketState = 0;
		CPacketComposer	Packet(S2C_LOBBY_CHAT_ROOM_LIST_RES);
		
		Packet.Add(iPacketState);
		Packet.Add(rs.iRoomCount);
		SCHAT_LOBBY_ROOM_INFO	stRoomInfo;

		pUser->Send(&Packet);

		iPacketState = 1;
		for(int iCount = 0 ; iCount < rs.iRoomCount ; ++iCount)
		{
			Packet.Initialize(S2C_LOBBY_CHAT_ROOM_LIST_RES);
			Packet.Add(iPacketState);

			stRoomInfo.Initialize();
			rp->Read((PBYTE)&stRoomInfo, sizeof(SCHAT_LOBBY_ROOM_INFO));

			Packet.Add(stRoomInfo.iRoomIndex);
			Packet.Add(stRoomInfo.iRoomType);
			Packet.Add(stRoomInfo.iUserCount);
			Packet.Add(stRoomInfo.iMaxUserCount);
			Packet.Add((PBYTE)stRoomInfo.szRoomName, MAX_CHAT_LOBBYROOM_NAME_LENGTH+1);

			pUser->Send(&Packet);
		}
		iPacketState = 2;
		Packet.Initialize(S2C_LOBBY_CHAT_ROOM_LIST_RES);
		Packet.Add(iPacketState);
		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_ENTER_LOBBY_CHAT_ROOM_RES)
{
	ST2S_ENTER_LOBBY_CHAT_ROOM_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(ST2S_ENTER_LOBBY_CHAT_ROOM_RES));

	CScopedRefGameUser pUser(rs.iGameIDIndex);

	CPacketComposer Packet(S2C_ENTER_LOBBY_CHAT_ROOM_RES);

	if(pUser != NULL)
	{
		if(RESULT_COMMON_SUCCESS == rs.iResult)
		{
			LOBBYCHAT.AddUser(pUser.GetPointer(), rs.iRoomType, rs.iRoomIndex);
		}

		Packet.Add(rs.iResult);
		Packet.Add(rs.iRoomIndex);
		Packet.Add(rs.iRoomType);
		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_AUTO_ENTER_LOBBY_CHAT_ROOM_RES)
{
	ST2S_AUTO_ENTER_LOBBY_CHAT_ROOM_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(ST2S_AUTO_ENTER_LOBBY_CHAT_ROOM_RES));

	CScopedRefGameUser	pUser(rs.iGameIDIndex);

	CPacketComposer Packet(S2C_LOBBY_CHAT_ROOM_AUTO_ENTER_RES);

	if( pUser != NULL )
	{
		if(RESULT_COMMON_SUCCESS == rs.iResult)
		{
			LOBBYCHAT.AddUser(pUser.GetPointer(), rs.iRoomType, rs.iRoomIndex);
		}

		Packet.Add(rs.iResult);
		Packet.Add(rs.iRoomIndex);
		Packet.Add(rs.iRoomType);
		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_LOBBY_CHAT_ROOM_USER_LIST_RES)
{
	int iGameIDIndex = 0;
	rp->Read(&iGameIDIndex);
	CScopedRefGameUser	pUser(iGameIDIndex);

	if( pUser != NULL )
	{
		CPacketComposer Packet(S2C_LOBBY_CHAT_ROOM_USER_LIST_RES);
		Packet.Add((PBYTE)rp->GetRemainedData(), rp->GetRemainedDataSize());
		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_CHAT_ROOM_NAME_RES)
{
	ST2S_CHAT_ROOM_NAME_RES rs;

	rp->Read((PBYTE)&rs, sizeof(ST2S_CHAT_ROOM_NAME_RES));

	CScopedRefGameUser pUser(rs.iGameIDIndex);

	CPacketComposer Packet(S2C_CHAT_ROOM_NAME_RES);

	if( pUser != NULL )
	{
		Packet.Add((PBYTE)&rs.szRoomName, MAX_CHAT_LOBBYROOM_NAME_LENGTH+1);
		Packet.Add(rs.iRoomIndex);

		pUser->Send(&Packet);
	}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_WORLD_END_NOT)
{
	////fake log
	//int iAddress = rand() + 100000000;
00000;
	//int iUserAddress = rand() + 100000000;
00000;
	//WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, NONE, "CreateUser - address:00A41316, userAddress:00000000", iAddress, iUserAddress);
//{
	//	int 3;
	//}
}

DEFINE_CHATPROXY_PROC_FUNC(T2S_CHAT_REPEAT_BAN_RES)
{
	int iGameIDIndex = 0;
	rp->Read(&iGameIDIndex);
	CScopedRefGameUser	pUser(iGameIDIndex);

	if( pUser != NULL )
	{
		CPacketComposer Packet(S2C_CHAT_REPEAT_BAN_RES);
		Packet.Add((PBYTE)rp->GetRemainedData(), rp->GetRemainedDataSize());
		pUser->Send(&Packet);
	}
}