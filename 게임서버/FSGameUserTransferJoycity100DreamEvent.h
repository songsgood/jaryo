qpragma once

qinclude "CFSODBCBase.h"

class CFSGameUser;
class CFSODBCBase;

typedef map<int/*_iIndex*/, STransferJoycity100DreamEventUserRecord> TRANSFERJOYCITY_100DREAM_USER_RECORD_MAP;

class CFSGameUserTransferJoycity100DreamEvent
{
public:
	CFSGameUserTransferJoycity100DreamEvent(CFSGameUser* pUser);
	~CFSGameUserTransferJoycity100DreamEvent(void);

	BOOL	Load();
	void	SendEventInfo(BYTE btType);
	void	MakeGiveTicket(CPacketComposer& Packet, int& iCount, BYTE btMatchType, BYTE btMatchScaleType, BOOL bIsDisconnectedMatch, BOOL bRunAway);
	RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN TryWinReq(int& iGiveCash, int& iRankNumber);
	RESULT_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS WriteWordsReq(SC2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ& rq);
	void	CheckAndUpdateRankGameID();
	void	UpdateTryWinRes(SS2G_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES& rs);

protected:
	void	ResetTicket();
	
private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	int m_iGetDailyTicketCount;
	time_t m_tGetTicketTime;

	TRANSFERJOYCITY_100DREAM_USER_RECORD_MAP m_mapUserRecord;
};
