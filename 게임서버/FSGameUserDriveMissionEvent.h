qpragma once

class CFSGameUser;
class CFSGameUserDriveMissionEvent
{
public:
	CFSGameUserDriveMissionEvent(CFSGameUser* pUser);
	~CFSGameUserDriveMissionEvent(void);

	BOOL			Load();
	void			CheckResetDate(time_t tCurrentTime = _time64(NULL));
	void			SendEventMissionInfo();
	void			SendEventProductInfo();
	void			SendUserInfo();
	void			SendButtonStatus();	// 이벤트정보를 받기 전에 상태에 따라 툴팁을 노출함.
	void			ChoiceMission(BYTE btMissionIndex);
	void			GiveMoney();
	void			BuyProduct(BYTE btProductIndex);
	BOOL			CheckUpdateMission(BYTE btMissionConditType, int iAddValue = 1);

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;

	BYTE			m_btMissionStatus;
	BYTE			m_btCurrentChoiceMissionIndex;
	int				m_iCurrentValue;
	int				m_iJoyCityMissionCurrentValue;
	int				m_iMyMoney;
	int				m_iTodayGetMoney;
	time_t			m_tRecentCheckDate;
};

