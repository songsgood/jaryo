qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "MatchSvrProxy.h"

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_SPECIAL_PIECE_PAGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_ENTER_SPECIAL_PIECE_PAGE_RES rs;
	rs.btResult = RESULT_ENTER_SPECIAL_PIECE_PAGE_SUCCESS;

	if(FALSE == pFSClient->SetState(FS_SPECIAL_PIECE))
		rs.btResult = RESULT_ENTER_SPECIAL_PIECE_PAGE_FAIL;

	pUser->Send(S2C_ENTER_SPECIAL_PIECE_PAGE_RES, &rs, sizeof(SS2C_ENTER_SPECIAL_PIECE_PAGE_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_SPECIAL_PIECE_PAGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EXIT_SPECIAL_PIECE_PAGE_RES rs;
	rs.btResult = RESULT_EXIT_SPECIAL_PIECE_PAGE_SUCCESS;

	if(FALSE == pFSClient->SetState(NEXUS_LOBBY))
		rs.btResult = RESULT_EXIT_SPECIAL_PIECE_PAGE_FAIL;

	pUser->Send(S2C_EXIT_SPECIAL_PIECE_PAGE_RES, &rs, sizeof(SS2C_EXIT_SPECIAL_PIECE_PAGE_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_BOX_PAGE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialPiece()->SendBoxPageInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_BOX_KEY_AND_TINYPIECE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialPiece()->SendBoxKeyAndTinyPieceInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_MAKE_BOX_KEY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SP_MAKE_BOX_KEY_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SP_MAKE_BOX_KEY_REQ));

	SS2C_SP_MAKE_BOX_KEY_RES rs;
	rs.btIncompletedPieceType = rq.btIncompletedPieceType;
	rs.btMakeType = rq.btMakeType;
	rs.iUpdatedIncompletedPieceCount = 0;
	rs.btResult = pUser->GetUserSpecialPiece()->MakeBoxKey(rq, rs.iUpdatedIncompletedPieceCount, rs.iMakeKeyItemCode);
	pUser->Send(S2C_SP_MAKE_BOX_KEY_RES, &rs, sizeof(SS2C_SP_MAKE_BOX_KEY_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_OPEN_BOX_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SP_OPEN_BOX_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SP_OPEN_BOX_REQ));

	SS2C_SP_OPEN_BOX_RES rs;
	rs.btBoxType = rq.btBoxType;
	rs.btOpenType = rq.btOpenType;
	rs.iPieceCount = 0;
	rs.btResult = pUser->GetUserSpecialPiece()->OpenBox(rq, rs);
	if(RESULT_SP_OPEN_BOX_SUCCESS != rs.btResult)
		pUser->Send(S2C_SP_OPEN_BOX_RES, &rs, sizeof(SS2C_SP_OPEN_BOX_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_PIECE_PAGE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SP_PIECE_PAGE_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SP_PIECE_PAGE_INFO_REQ));

	pUser->GetUserSpecialPiece()->SendPiecePageInfo(rq.btPieceType);
}

DEFINE_GAMETHREAD_PROC_FUNC(CS2_SP_SKILL_UP_PAGE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialPiece()->SendSkillUpPageInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_SA_CREATE_ITEM_PAGE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialPiece()->SendSACreateItemPageInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_SA_ITEM_PAGE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialPiece()->SendSAItemPageInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_SPLIT_PIECE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SP_SPLIT_PIECE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SP_SPLIT_PIECE_REQ));

	SS2C_SP_SPLIT_PIECE_RES rs;
	rs.btPieceType = rq.btPieceType;
	rs.btResult = pUser->GetUserSpecialPiece()->SplitPiece(rq, rs.btUpdatedTinyPieceType, rs.iUpdatedTinyPieceCount);
	pUser->Send(S2C_SP_SPLIT_PIECE_RES, &rs, sizeof(SS2C_SP_SPLIT_PIECE_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(CS2_SP_SKILL_UP_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SCS2_SP_SKILL_UP_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SCS2_SP_SKILL_UP_REQ));

	SS2C_SP_SKILL_UP_RES rs;
	rs.btResult = pUser->GetUserSpecialPiece()->SkillUp(rq.iPieceCode, rs);
	pUser->Send(S2C_SP_SKILL_UP_RES, &rs, sizeof(SS2C_SP_SKILL_UP_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(CS2_SP_MAKE_SA_CREATE_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SCS2_SP_MAKE_SA_CREATE_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SCS2_SP_MAKE_SA_CREATE_ITEM_REQ));

	SS2C_SP_MAKE_SA_CREATE_ITEM_RES rs;
	rs.btResult = pUser->GetUserSpecialPiece()->MakeSACreateItem(rq.iPieceCode, rs);
	pUser->Send(S2C_SP_MAKE_SA_CREATE_ITEM_RES, &rs, sizeof(SS2C_SP_MAKE_SA_CREATE_ITEM_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(CS2_SP_MAKE_SA_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SCS2_SP_MAKE_SA_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SCS2_SP_MAKE_SA_ITEM_REQ));

	SS2C_SP_MAKE_SA_ITEM_RES rs;
	rs.btResult = pUser->GetUserSpecialPiece()->MakeSAItem(rq.iPieceCode, rs);
	pUser->Send(S2C_SP_MAKE_SA_ITEM_RES, &rs, sizeof(SS2C_SP_MAKE_SA_ITEM_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_SKILL_LV_PAGE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialPiece()->SendSkillLvPageInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SP_SELECT_SKILL_LV_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SP_SELECT_SKILL_LV_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SP_SELECT_SKILL_LV_REQ));

	SS2C_SP_SELECT_SKILL_LV_RES rs;
	rs.iSkillLvCode = rq.iSkillLvCode;
	rs.btResult = pUser->GetUserSpecialPiece()->SelectSkillLv(rq.iSkillLvCode);
	pUser->Send(S2C_SP_SELECT_SKILL_LV_RES, &rs, sizeof(SS2C_SP_SELECT_SKILL_LV_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SPECIAL_PIECE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialPiece()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SPECIAL_PIECE_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_SPECIAL_PIECE_GET_REWARD_RES rs;
	rs.btResult = pUser->GetUserSpecialPiece()->GetEventReward();
	pUser->Send(S2C_EVENT_SPECIAL_PIECE_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_SPECIAL_PIECE_GET_REWARD_RES));
}
