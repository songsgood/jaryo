qpragma once
qinclude "Lock.h"

typedef map<int /* ItemIndex */,SUserSpecialSkin> MAP_SPECIALSKIN;

class CFSGameUser;
class CFSItemShop;
class CFSGameUserSpecialSkin
{
public:
	CFSGameUserSpecialSkin(CFSGameUser* pUser);
	virtual ~CFSGameUserSpecialSkin(void);

public:

	BOOL Load(void);
	void MakePacketUserSpecialSkinList(CPacketComposer& Packet, int iGamePosition);
	void SendUserSpecialSkinInventoryList(int iGamePosition);
	void SendUserSpecialSkinEquipList(int iGamePosition, int iSpecialCharacterIndex);
	void EquipSpecailSkin(CPacketComposer& Packet, int iSpecialSkinCode, int iItemIndex);
	void Un_EquipSpecailSkin(CPacketComposer& Packet, int iSpecialSkinCode, int iInventoryIndex);
	void Un_EquipSpecailSkin(int iInventoryIndex);
	void CheckAndUnEquipSpecialSKin( int iSkillNo, int iKind );
	void Un_EquipSpecialSkin_Kind_All(int iKind);
	void MakePacketUser_EquipSpecialSKinList(CPacketComposer& Packet);

	BOOL BuySpecialSkinCard( int& iErrorCode, SC2S_SPECIALSKIN_BUY_REQ sBuyInfo, CFSItemShop* pItemShop );
	BOOL BuySpecialSkinCard_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	void GetUsedSpecialSkinAdditionalEffects( vector<SSpecialSkinAdditionaleffectsInfo>& vec );
	void CheckExpireSpecialSkin();
	void CheckUnEquip_AfterSkillUnEquip_All();

	BOOL btRetentionOfSkinCard(int iSkinCode);

	void GetUserRetentionOfSkilCode(list<int>& listRetentionSkinCode);

private:
	enum eCheckSkin
	{
		Mine = 0,
		Target = 1,
		MaxCheckSkin,
	};

	void SpecialSkinUserDataSet(vector<SUSERSPECIALSKIN> vecSkinInfo);
	void UpdateSpecialSkinStatus( BYTE btStatus , int iItemIndex , int iSpecialSkinCode, int iUnEquipIndex);
	void SpecialSkinCard_GiveItem(BYTE btRewardType, int iItemCode, int iItemIndex, int iComponentIndex, int iRelateIndex, int iPresentIndex);
	BOOL CheckAndGetOwnedSpecialSkin( int iSkinCode, int iSkinIndex, SUserSpecialSkin& sInfo );
	BOOL CheckOwnedSpecialSkin( int iSkinCode , int iSkinCardIndex, BYTE btSelectColor, int iComponentIndex, int iRelateIndex);
	int CheckSpecialSkin_OverLapAndGetInventoryIndex(int iSkillNo, int iKind);

private:
	LOCK_RESOURCE(m_SyncmapSpecialSkin); 

	CFSGameUser* m_pUser;
	MAP_SPECIALSKIN m_mapSpecialSkin;
};

