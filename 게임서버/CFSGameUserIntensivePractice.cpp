qinclude "stdafx.h"
qinclude "CFSGameUserIntensivePractice.h"
qinclude "CFSGameUser.h"
qinclude "CIntensivePracticeManager.h"
qinclude "GameProxyManager.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "RewardManager.h"
qinclude "ClubConfigManager.h"
qinclude "CFSGameUserSkill.h"
qinclude "CIntensivePracticeRank.h"
qinclude "CFSGameServer.h"
qinclude "SpecialSkinManager.h"

CFSGameUserIntensivePractice::CFSGameUserIntensivePractice(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
	m_bOneDayPopUpNotice	= FALSE;
	m_bReadyGiveReward		= FALSE;
	m_bGiveReward_Completion = TRUE;
}

CFSGameUserIntensivePractice::~CFSGameUserIntensivePractice(void)
{
}

BOOL CFSGameUserIntensivePractice::Load()
{
	CHECK_CONDITION_RETURN(TRUE == m_bDataLoaded, TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<SIntensivePracticeUserBestRecord> vecInfo;
	if( ODBC_RETURN_SUCCESS != pODBC->INTENSIVEPRACTICE_GetUserRecord(m_pUser->GetGameIDIndex(), vecInfo))
	{
		return FALSE;
	}

	for (int i = 0; i < MAX_INTENSIVEPRACTICE_TYPE_COUNT; ++i)
	{
		m_mapUserBestRecord[i].Initialize();
	}

	for (int i = 0; i < MAX_INTENSIVEPRACTICE_TYPE_COUNT; ++i)
	{
		m_mapUserBestRecord[i].iPracticeType = i;
	}

	for (int i = 0; i < vecInfo.size(); ++i)
	{
		m_mapUserBestRecord[vecInfo[i].iPracticeType] = vecInfo[i];
	}

	if( ODBC_RETURN_SUCCESS != pODBC->INTENSIVEPRACTICE_GetUserOneDayRecord(m_pUser->GetGameIDIndex(), m_UserOneDayRecord))
	{
		return FALSE;
	}

	if(TRUE != LoadRankList(INTENSIVEPRACTICE_RANK_TYPE_FRIEND, m_pUser->GetGameIDIndex()))
		return FALSE;

	CheckNoticeLog();

	m_bDataLoaded = TRUE;

	return TRUE;
}

BOOL CFSGameUserIntensivePractice::CheckNoticeLog()
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if( ODBC_RETURN_SUCCESS == pODBC->INTENSIVEPRACTICE_CheckNoticeLog(m_pUser->GetGameIDIndex()))
	{
		m_bGiveReward_Completion = FALSE;
		m_bOneDayPopUpNotice = TRUE;
	}

	return TRUE;
}

int CFSGameUserIntensivePractice::GetBestPoint()
{
	return m_mapUserBestRecord[m_UserCurrentRecord.iPracticeType].iBestPoint;
}

int CFSGameUserIntensivePractice::GetBestPoint(int iPracticeType)
{
	CHECK_CONDITION_RETURN(iPracticeType <= INTENSIVEPRACTICE_TYPE_NONE || iPracticeType >= MAX_INTENSIVEPRACTICE_TYPE_COUNT, 0);

	return m_mapUserBestRecord[iPracticeType].iBestPoint;
}

int CFSGameUserIntensivePractice::GetUpdateExp()
{
	int iExp = m_UserOneDayRecord.iOneDayExp[m_UserCurrentRecord.iPracticeType] + m_UserCurrentRecord.iAddExp;
	if(iExp >= INTENSIVEPRACTICEMANAGER.GetOneDayLimitedExp(m_UserCurrentRecord.iPracticeType))
		iExp = INTENSIVEPRACTICEMANAGER.GetOneDayLimitedExp(m_UserCurrentRecord.iPracticeType);

	return iExp;
}

int CFSGameUserIntensivePractice::GetUpdatePoint()
{
	int iPoint = m_UserOneDayRecord.iOneDayPoint[m_UserCurrentRecord.iPracticeType] + m_UserCurrentRecord.iAddPoint;
	if(iPoint >= INTENSIVEPRACTICEMANAGER.GetOneDayLimitedPoint(m_UserCurrentRecord.iPracticeType))
		iPoint = INTENSIVEPRACTICEMANAGER.GetOneDayLimitedPoint(m_UserCurrentRecord.iPracticeType);

	return iPoint;
}

void CFSGameUserIntensivePractice::SetGameStartInfo(SS2C_START_INTENSIVEPRACTICE_RES rs)
{
	m_UserCurrentRecord.iRoomMode = rs.iRoomMode;
	m_UserCurrentRecord.iLevel = rs.iLevel;
	m_UserCurrentRecord.iPracticeType = rs.iPracticeType;
	m_UserCurrentRecord.iScore = 0;
	m_UserCurrentRecord.iGameTime = 0;
	if(rs.iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_RANKING &&
		INTENSIVEPRACTICEMANAGER.GetScoreMode(m_UserCurrentRecord.iPracticeType) == INTENSIVEPRACTICE_SCORE_MODE_TIME)
	{
		m_UserCurrentRecord.iGameTime = INTENSIVEPRACTICEMANAGER.GetGameTime(m_UserCurrentRecord.iPracticeType);
	}

	m_UserCurrentRecord.iAddExp = 0;
	m_UserCurrentRecord.iAddPoint = 0;
	m_UserCurrentRecord.iTryCount = 0;
}

void CFSGameUserIntensivePractice::InitializeBestPoint()
{
	for (int iPracticeType = INTENSIVEPRACTICE_TYPE_REBOUND; iPracticeType < MAX_INTENSIVEPRACTICE_TYPE_COUNT; ++iPracticeType)
	{
		m_mapUserBestRecord[iPracticeType].iBestPoint = 0;
	}

	LoadRankList(INTENSIVEPRACTICE_RANK_TYPE_FRIEND, m_pUser->GetGameIDIndex());
}

void CFSGameUserIntensivePractice::InitializeGame()
{
	m_UserCurrentRecord.iScore = 0;
	m_UserCurrentRecord.iGameTime = 0;
}

void CFSGameUserIntensivePractice::MakeGameStartInfo(CPacketComposer& Packet, SS2C_START_INTENSIVEPRACTICE_RES& rs)
{
	rs.iScoreMode = INTENSIVEPRACTICEMANAGER.GetScoreMode(rs.iPracticeType);
	rs.iLimitedGameTime = INTENSIVEPRACTICEMANAGER.GetGameTime(m_pUser->GetUserIntensivePractice()->GetPracticeType());
	INTENSIVEPRACTICEMANAGER.MakeMaxSuccessCount(rs);

	int	iAvatarAddStat[MAX_STAT_NUM] = {0,};
	int	iLinkItemAddStat[MAX_STAT_NUM] = {0,};
	int iaStat[MAX_STAT_NUM] = {0};

	m_pUser->GetAvatarItemStatus(iAvatarAddStat);
	m_pUser->SetAvatarFameSkillStatus(iAvatarAddStat);
	m_pUser->GetUserCharacterCollection()->GetSentenceStatus(iAvatarAddStat);
	m_pUser->GetUserPowerupCapsule()->GetPowerupCapsuleStatus(iAvatarAddStat);
	m_pUser->GetLvIntervalBuffItemStatus(iAvatarAddStat);
	m_pUser->GetAvatarLinkItemStatus(iLinkItemAddStat);
	m_pUser->GetAvatarSpecialPieceProperty(iAvatarAddStat);
	m_pUser->GetCheerLeaderStatus(iAvatarAddStat);

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	iaStat[0]	= pAvatar->Status.iStatRun		+ iAvatarAddStat[0]	 + iLinkItemAddStat[0];
	iaStat[1]	= pAvatar->Status.iStatJump		+ iAvatarAddStat[1]	 + iLinkItemAddStat[1];
	iaStat[2]	= pAvatar->Status.iStatStr		+ iAvatarAddStat[2]	 + iLinkItemAddStat[2];
	iaStat[3]	= pAvatar->Status.iStatPass		+ iAvatarAddStat[3]	 + iLinkItemAddStat[3];
	iaStat[4]	= pAvatar->Status.iStatDribble	+ iAvatarAddStat[4]	 + iLinkItemAddStat[4];
	iaStat[5]	= pAvatar->Status.iStatRebound	+ iAvatarAddStat[5]	 + iLinkItemAddStat[5];
	iaStat[6]	= pAvatar->Status.iStatBlock	+ iAvatarAddStat[6]	 + iLinkItemAddStat[6];
	iaStat[7]	= pAvatar->Status.iStatSteal	+ iAvatarAddStat[7]	 + iLinkItemAddStat[7];
	iaStat[8]	= pAvatar->Status.iStat2Point	+ iAvatarAddStat[8]	 + iLinkItemAddStat[8];
	iaStat[9]	= pAvatar->Status.iStat3Point	+ iAvatarAddStat[9]	 + iLinkItemAddStat[9];
	iaStat[10]	= pAvatar->Status.iStatDrive	+ iAvatarAddStat[10] + iLinkItemAddStat[10];				
	iaStat[11]	= pAvatar->Status.iStatClose	+ iAvatarAddStat[11] + iLinkItemAddStat[11];

	memcpy(rs.iaStat, iaStat, sizeof(int)*MAX_STAT_NUM);
	ZeroMemory(rs.iaSkill, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	ZeroMemory(rs.iaFreeStyle, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();
	int iSlotCount = pAvatar->GetTotalSkillSlotCount(iBonusSkillSlotCount);

	for(int j=0; j<iSlotCount; j++)
	{
		if(-1 != pAvatar->Skill.iaUseSkill[j] &&
			TRUE == INTENSIVEPRACTICEMANAGER.CheckUseSkill(m_UserCurrentRecord.iPracticeType, m_pUser->GetCurUsedAvatarPosition(), pAvatar->Skill.iaUseSkill[j]))
		{
			if(32 > pAvatar->Skill.iaUseSkill[j])
			{
				rs.iaSkill[SKILL_STORAGE_0] |= (1 << pAvatar->Skill.iaUseSkill[j]);
			}
			else if( 64 > pAvatar->Skill.iaUseSkill[j] )
			{
				rs.iaSkill[SKILL_STORAGE_1] |= (1 << (pAvatar->Skill.iaUseSkill[j]-32));
			}
			else if ( 96 > pAvatar->Skill.iaUseSkill[j] )
			{
				rs.iaSkill[SKILL_STORAGE_2] |= (1 << (pAvatar->Skill.iaUseSkill[j]-64));
			}
			else if ( 128 > pAvatar->Skill.iaUseSkill[j] )
			{
				rs.iaSkill[SKILL_STORAGE_3] |= (1 << (pAvatar->Skill.iaUseSkill[j]-96));
			}
			else if ( 160 > pAvatar->Skill.iaUseSkill[j] )
			{
				rs.iaSkill[SKILL_STORAGE_4] |= (1 << (pAvatar->Skill.iaUseSkill[j]-128));
			}
		}
	}

	for(int j=0; j<iSlotCount; j++)
	{
		if(-1 != pAvatar->Skill.iaUseFreestyle[j])
		{
			if(32 > pAvatar->Skill.iaUseFreestyle[j])
			{
				rs.iaFreeStyle[SKILL_STORAGE_0] |= (1 << pAvatar->Skill.iaUseFreestyle[j]);
			}
			else if(64 > pAvatar->Skill.iaUseFreestyle[j])
			{
				rs.iaFreeStyle[SKILL_STORAGE_1] |= (1 << (pAvatar->Skill.iaUseFreestyle[j]-32));
			}
			else if(96 > pAvatar->Skill.iaUseFreestyle[j])
			{
				rs.iaFreeStyle[SKILL_STORAGE_2] |= (1 << (pAvatar->Skill.iaUseFreestyle[j]-64));
			}
			else if(128 > pAvatar->Skill.iaUseFreestyle[j])
			{
				rs.iaFreeStyle[SKILL_STORAGE_3] |= (1 << (pAvatar->Skill.iaUseFreestyle[j]-96));
			}
			else if (160 > pAvatar->Skill.iaUseFreestyle[j])
			{
				rs.iaFreeStyle[SKILL_STORAGE_4] |= (1 << (pAvatar->Skill.iaUseFreestyle[j]-128));
			}
		}
	}

	rs.btIsCheerLeader = 0;
	rs.iShootSuccessRate = 0;
	rs.iBlockSuccessRate = 0;
	if(m_pUser->IsClubMember() && m_pUser->GetCheerLeaderCode() > 0)
	{
		rs.btIsCheerLeader = 1;
		rs.iShootSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderShootSuccessRate(m_pUser->GetCheerLeaderCode());
		rs.iBlockSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderBlockSuccessRate(m_pUser->GetCheerLeaderCode());
	}

	list<SAvatarSpecialtySkill> listSpecialtySkill;
	m_pUser->GetUserSkill()->GetSpecialSkilllist(listSpecialtySkill, rs.btSpecialtySkillCount, pAvatar->Skill);
	rs.btNPCCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_START_INTENSIVEPRACTICE_RES));
	PBYTE pNPCCountLocation = Packet.GetTail() - sizeof(BYTE);	
	
	SS2C_INTENSIVEPRACTICE_AVATAR_SPECIALTY_SKILL_INFO info;
	list<SAvatarSpecialtySkill>::iterator iter = listSpecialtySkill.begin();
	for( ; iter != listSpecialtySkill.end() ; ++iter )
	{
		info.iKind = (*iter).iKind;
		info.iSkillNo = (*iter).iSkillNo;
		info.btSpecialtyNo = (*iter).btSpecialtyNo;
		Packet.Add((PBYTE)&info, sizeof(SS2C_INTENSIVEPRACTICE_AVATAR_SPECIALTY_SKILL_INFO));	
	}

	BYTE btCount = 0;
	PBYTE btCountLocation = Packet.GetTail();

	Packet.Add( btCount );
	vector<SSpecialSkinAdditionaleffectsInfo>	vecSpecialSkin;
	m_pUser->GetSpecialSkinAdditionalEffectsInfo(vecSpecialSkin);

	BOOL bTransformSkin = FALSE;
	for( size_t iIndex = 0 ; iIndex < vecSpecialSkin.size(); ++iIndex )
	{
		SSpecialSkinAdditionaleffectsInfo* pSkinInfo = &(vecSpecialSkin[iIndex]);

		Packet.Add(pSkinInfo->iKind);
		Packet.Add(pSkinInfo->iSkillNo);
		Packet.Add(pSkinInfo->iSkinCode);
		Packet.Add((BYTE)pSkinInfo->iRelateIndex);
		BYTE btTemprelate = 0;
		Packet.Add(btTemprelate); // dummy data
		Packet.Add(pSkinInfo->fValue);
		
		bTransformSkin = SPECIALSKINManager.IsTransformSkinBySkillNo(pSkinInfo->iSkillNo);
		Packet.Add(&bTransformSkin);

		++btCount;
	}
	memcpy(btCountLocation, &btCount, sizeof(btCount));

	float fSentenceShootSuccessRate, fSentenceBlockSuccessRate;
	m_pUser->GetUserCharacterCollection()->GetSentenceStatus(fSentenceShootSuccessRate, fSentenceBlockSuccessRate);
	Packet.Add(fSentenceShootSuccessRate);
	Packet.Add(fSentenceBlockSuccessRate);

	float fSpecialPartsShootSuccessRate = 0, fSpecialPartsBlockSuccessRate = 0;
	m_pUser->GetSpecialPartsOptionProperty(fSpecialPartsShootSuccessRate, fSpecialPartsBlockSuccessRate);
	Packet.Add(fSpecialPartsShootSuccessRate);
	Packet.Add(fSpecialPartsBlockSuccessRate);

	INTENSIVEPRACTICEMANAGER.MakeNPCInfoList(Packet, pNPCCountLocation, rs);
}

void CFSGameUserIntensivePractice::AddScore(int* iScoreType, int& iAddGameTime)
{
	SIntensivePracticeScoreConfig stConfig;

	iAddGameTime = 0;
	for(int i = 0; i < MAX_INTENSIVEPRACTICE_USE_SCORE_COUNT; i++)
	{
		if(iScoreType[i] > INTENSIVEPRACTICE_SCORE_TYPE_NONE && iScoreType[i] < MAX_INTENSIVEPRACTICE_SCORE_TYPE_COUNT)
		{
			INTENSIVEPRACTICEMANAGER.GetScoreConfig(iScoreType[i], stConfig);
			m_UserCurrentRecord.iAddExp += stConfig.iExp;
			m_UserCurrentRecord.iAddPoint += stConfig.iPoint;
			m_UserCurrentRecord.iScore += stConfig.iScore;

			if(iScoreType[i] == INTENSIVEPRACTICE_SCORE_TYPE_STEP_2SHOOT)
			{
				iAddGameTime += RANKING_MODE_ADD_TIME_VALUE_STEP_2SHOOT;
				m_UserCurrentRecord.iGameTime += RANKING_MODE_ADD_TIME_VALUE_STEP_2SHOOT;
			}
			else if(iScoreType[i] == INTENSIVEPRACTICE_SCORE_TYPE_NICEPASS_DUNK)
			{
				iAddGameTime += RANKING_MODE_ADD_TIME_VALUE_NICEPASS_DUNK;
				m_UserCurrentRecord.iGameTime += RANKING_MODE_ADD_TIME_VALUE_NICEPASS_DUNK;
			}
			else if(iScoreType[i] == INTENSIVEPRACTICE_SCORE_TYPE_NICEPASS_LAYUP)
			{
				iAddGameTime += RANKING_MODE_ADD_TIME_VALUE_NICEPASS_LAYUP;
				m_UserCurrentRecord.iGameTime += RANKING_MODE_ADD_TIME_VALUE_NICEPASS_LAYUP;
			}
		}		
	}

	CheckUpdateRank();
}

void CFSGameUserIntensivePractice::CheckUpdateRank()
{
	CHECK_CONDITION_RETURN_VOID(m_UserCurrentRecord.iRoomMode != INTENSIVEPRACTICE_ROOM_MODE_RANKING);
	CHECK_CONDITION_RETURN_VOID(m_UserCurrentRecord.iPracticeType <= INTENSIVEPRACTICE_TYPE_NONE || m_UserCurrentRecord.iPracticeType >= MAX_INTENSIVEPRACTICE_TYPE_COUNT);
	CHECK_CONDITION_RETURN_VOID(m_mapUserBestRecord[m_UserCurrentRecord.iPracticeType].iBestPoint >= m_UserCurrentRecord.iScore);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	time_t tUpdateDate;
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->INTENSIVEPRACTICE_UpdateUserRecord(m_pUser->GetGameIDIndex(), m_UserCurrentRecord.iPracticeType, m_UserCurrentRecord.iScore, tUpdateDate));

	m_mapUserBestRecord[m_UserCurrentRecord.iPracticeType].iBestPoint = m_UserCurrentRecord.iScore;

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	if(pCenter) 
	{
		SG2S_UPDATE_INTENSIVEPRACTICE_RANK_REQ info;
		info.iPracticeType = m_UserCurrentRecord.iPracticeType;
		info.iGamePosition = m_pUser->GetCurUsedAvatarPosition();
		info.iGameIDIndex = m_pUser->GetGameIDIndex();
		strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iBestPoint = m_UserCurrentRecord.iScore;
		info.tUpdateDate = tUpdateDate;
		pCenter->SendPacket(G2S_UPDATE_INTENSIVEPRACTICE_RANK_REQ, &info, sizeof(SG2S_UPDATE_INTENSIVEPRACTICE_RANK_REQ));
	}

	vector<LPTSTR> vGameID;
	m_pUser->GetFriendGameIDList(vGameID);
	if(vGameID.empty())
	{
		m_pUser->GetUserIntensivePractice()->UpdateRank(m_UserCurrentRecord.iPracticeType, m_pUser->GetGameIDIndex(), m_pUser->GetGameID(), m_pUser->GetCurUsedAvatarPosition(), m_UserCurrentRecord.iScore, tUpdateDate);
	}
	else
	{
		if(pCenter) 
		{
			for(int i = 0; i < vGameID.size(); i++)
			{
				SG2S_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ info;
				strncpy_s(info.GameID, _countof(info.GameID), vGameID[i], MAX_GAMEID_LENGTH);

				info.iPracticeType = m_UserCurrentRecord.iPracticeType;
				info.iGamePosition = m_pUser->GetCurUsedAvatarPosition();
				info.iUpdateGameIDIndex = m_pUser->GetGameIDIndex();
				strncpy_s(info.szUpdateGameID, _countof(info.szUpdateGameID), m_pUser->GetGameID(), MAX_GAMEID_LENGTH);
				info.iBestPoint = m_UserCurrentRecord.iScore;
				info.tUpdateDate = tUpdateDate;
				pCenter->SendPacket(G2S_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ, &info, sizeof(SG2S_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ));
			}
		}
	}	

	if(m_pUser->GetClubSN() > 0)
	{
		CClubSvrProxy* pClub = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
		if(pClub)
		{
			SG2CL_INTENSIVEPRACTICE_RANK_UPDATE_REQ rq;
			rq.iClubSN = m_pUser->GetClubSN();
			rq.iPracticeType = m_UserCurrentRecord.iPracticeType;
			rq.iGameIDIndex = m_pUser->GetGameIDIndex();
			strncpy_s(rq.szGameID, _countof(rq.szGameID), m_pUser->GetGameID(), MAX_GAMEID_LENGTH);
			rq.iGamePosition = m_pUser->GetCurUsedAvatarPosition();
			rq.iBestPoint = m_UserCurrentRecord.iScore;
			rq.tUpdateDate = tUpdateDate;
			pClub->SendPacket(G2CL_INTENSIVEPRACTICE_RANK_UPDATE_REQ, &rq, sizeof(SG2CL_INTENSIVEPRACTICE_RANK_UPDATE_REQ));
		}
	}
}

void CFSGameUserIntensivePractice::AddTryCount()	
{ 
	m_UserCurrentRecord.iTryCount++; 
	if(m_UserCurrentRecord.iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_NONE &&
		m_bReadyGiveReward == TRUE &&
		m_bGiveReward_Completion == FALSE &&
		m_UserCurrentRecord.iTryCount >= 5)
	{
		SIntensivePracticeRewardConfig stRewardConfig;
		INTENSIVEPRACTICEMANAGER.GetRewardConfig(stRewardConfig);

		SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, stRewardConfig.iRewardIndex);
		if (NULL != pReward)
		{
			m_bReadyGiveReward = FALSE;
			m_bGiveReward_Completion = TRUE; // 지급완료
			m_UserCurrentRecord.iTryCount = 0;

			CPacketComposer Packet(S2C_INTENSIVEPRACTICE_GIVE_REWARD_NOTICE);
			m_pUser->Send(&Packet);
		}
	}
}

void CFSGameUserIntensivePractice::SetRoomMode(int iRoomMode, int& iAddGameTime)
{
	iAddGameTime = 0;
	if(m_UserCurrentRecord.iRoomMode != iRoomMode)
	{
		InitializeGame();

		if(iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_RANKING &&
			INTENSIVEPRACTICEMANAGER.GetScoreMode(m_UserCurrentRecord.iPracticeType) == INTENSIVEPRACTICE_SCORE_MODE_TIME)
		{
			m_UserCurrentRecord.iGameTime = INTENSIVEPRACTICEMANAGER.GetGameTime(m_UserCurrentRecord.iPracticeType);
		}
	}	
	else if(m_UserCurrentRecord.iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_NONE && iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_NONE)
	{
		m_UserCurrentRecord.iScore = 0;
	}
	else if(m_UserCurrentRecord.iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_RANKING && iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_RANKING &&
		INTENSIVEPRACTICEMANAGER.GetScoreMode(m_UserCurrentRecord.iPracticeType) == INTENSIVEPRACTICE_SCORE_MODE_TIME)
	{
		m_UserCurrentRecord.iGameTime += 10;
		iAddGameTime = 10;
	}
	m_UserCurrentRecord.iRoomMode = iRoomMode;
}

void CFSGameUserIntensivePractice::SetLevel(int iLevel)
{
	m_UserCurrentRecord.iLevel = iLevel;
}

void CFSGameUserIntensivePractice::EndGame()
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	CFSGameODBC* pGameODBC = (CFSGameODBC*) ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC);

	int iExp = m_UserOneDayRecord.iOneDayExp[m_UserCurrentRecord.iPracticeType] + m_UserCurrentRecord.iAddExp;
	if(iExp >= INTENSIVEPRACTICEMANAGER.GetOneDayLimitedExp(m_UserCurrentRecord.iPracticeType))
		m_UserCurrentRecord.iAddExp = INTENSIVEPRACTICEMANAGER.GetOneDayLimitedExp(m_UserCurrentRecord.iPracticeType) - m_UserOneDayRecord.iOneDayExp[m_UserCurrentRecord.iPracticeType];

	int iPoint = m_UserOneDayRecord.iOneDayPoint[m_UserCurrentRecord.iPracticeType] + m_UserCurrentRecord.iAddPoint;
	if(iPoint >= INTENSIVEPRACTICEMANAGER.GetOneDayLimitedPoint(m_UserCurrentRecord.iPracticeType))
		m_UserCurrentRecord.iAddPoint = INTENSIVEPRACTICEMANAGER.GetOneDayLimitedPoint(m_UserCurrentRecord.iPracticeType) - m_UserOneDayRecord.iOneDayPoint[m_UserCurrentRecord.iPracticeType];

	int iRequireLv = 0, iBonusPoint = 0;
	if(ODBC_RETURN_SUCCESS == pODBC->INTENSIVEPRACTICE_UpdateUserOneDayRecord(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), m_UserCurrentRecord.iPracticeType, 
				m_UserCurrentRecord.iAddExp, m_UserCurrentRecord.iAddPoint, m_UserOneDayRecord.iOneDayExp[m_UserCurrentRecord.iPracticeType], m_UserOneDayRecord.iOneDayPoint[m_UserCurrentRecord.iPracticeType],
				iRequireLv, iBonusPoint))
	{
		int iPreLv = m_pUser->GetCurUsedAvtarLv();
		if(TRUE == m_pUser->CheckLvUp(m_UserCurrentRecord.iAddExp, pGameODBC))
		{
			m_pUser->ProcessStatUp(pGameODBC);

			vector<int> vLevel;
			vLevel.push_back(m_pUser->GetCurUsedAvtarLv());
			m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_LEVEL, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vLevel);
			m_pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_INTENSIVEPRACTICE, WORDPUZZLES_REWARDTYPE_LV_UP, m_pUser->GetCurUsedAvtarLv());
			m_pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_INTENSIVEPRACTICE, WORDPUZZLES_REWARDTYPE_LV_UP_CONTINUE);
			
			m_pUser->GetUserCharacterCollection()->CheckLvUp(m_pUser->GetCurUsedAvtarLv());
			m_pUser->GiveLvUpEventReward(iPreLv);
			m_pUser->UpdateAvatarExp(m_pUser->GetGameIDIndex(), m_pUser->GetCurUsedAvtarLv(), m_pUser->GetCurUsedAvtarExp());
		}
		m_pUser->SetUserSkillPoint(m_pUser->GetUserSkillPoint() + m_UserCurrentRecord.iAddPoint);
		m_pUser->SetBonusPointTerms(iRequireLv, iBonusPoint);				
		if(iBonusPoint > 0)
		{
			m_pUser->SetUserSkillPoint(m_pUser->GetUserSkillPoint() + iBonusPoint);
		}
	}

	m_UserCurrentRecord.iRoomMode = INTENSIVEPRACTICE_ROOM_MODE_NONE;
	m_UserCurrentRecord.iLevel = INTENSIVEPRACTICE_LEVEL_NONE;
	m_UserCurrentRecord.iPracticeType = INTENSIVEPRACTICE_TYPE_NONE;
	m_UserCurrentRecord.iScore = 0;
	m_UserCurrentRecord.iGameTime = 0;

	m_pUser->SendUserStat();		
	m_pUser->SendUserGold();
}

void CFSGameUserIntensivePractice::CheckGameTime()
{
	if(m_UserCurrentRecord.iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_RANKING &&
		INTENSIVEPRACTICEMANAGER.GetScoreMode(m_UserCurrentRecord.iPracticeType) == INTENSIVEPRACTICE_SCORE_MODE_TIME)
	{
		if(m_UserCurrentRecord.iGameTime > 0)
		{
			m_UserCurrentRecord.iGameTime--;
			if(m_UserCurrentRecord.iGameTime == 0)
			{
				CPacketComposer Packet(S2C_INTENSIVEPRACTICE_GAMETIME_OVER_NOTICE);
				m_pUser->Send(&Packet);
			}
		}
	}
}

void CFSGameUserIntensivePractice::ResetOneDayRecord()
{
	ZeroMemory(m_UserOneDayRecord.iOneDayExp, sizeof(int)*MAX_INTENSIVEPRACTICE_TYPE_COUNT);
	ZeroMemory(m_UserOneDayRecord.iOneDayPoint, sizeof(int)*MAX_INTENSIVEPRACTICE_TYPE_COUNT);
}

void CFSGameUserIntensivePractice::UpdateReadyGiveReward(int iLoseCount, int iScore)
{
	m_bReadyGiveReward = FALSE;
	if(iLoseCount <= -5 ) // 5연패 일 경우만
		m_bReadyGiveReward = TRUE;
}

void CFSGameUserIntensivePractice::CheckOneDayPopUpNotice()
{
	if(m_bOneDayPopUpNotice == TRUE && m_bReadyGiveReward == TRUE)
	{
		SIntensivePracticeRewardConfig stRewardConfig;
		INTENSIVEPRACTICEMANAGER.GetRewardConfig(stRewardConfig);
		SS2C_INTENSIVEPRACTICE_ONEDAY_POPUP_NOTICE info;
		info.iItemCode = stRewardConfig.iItemCode;
		info.iPropertyType = stRewardConfig.iPropertyType;
		info.iPropertyValue = stRewardConfig.iPropertyValue;
		m_pUser->Send(S2C_INTENSIVEPRACTICE_ONEDAY_POPUP_NOTICE, &info, sizeof(SS2C_INTENSIVEPRACTICE_ONEDAY_POPUP_NOTICE));

		CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if( ODBC_RETURN_SUCCESS == pODBC->INTENSIVEPRACTICE_InsertNoticeLog(m_pUser->GetGameIDIndex()))
		{
			m_bGiveReward_Completion = FALSE;
			m_bOneDayPopUpNotice = FALSE;
		}
	}
}

void CFSGameUserIntensivePractice::GiveAchievement()
{
	vector<int> vInfo;
	vInfo.push_back((int)INTENSIVEPRACTICE_TYPE_REBOUND);
	vInfo.push_back(m_pUser->GetGameIDIndex());
	m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_INTENSIVEPRACTICE_REBOUND_VICTORY, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vInfo);

	vInfo.clear();
	vInfo.push_back((int)INTENSIVEPRACTICE_TYPE_SHOOT);
	vInfo.push_back(m_pUser->GetGameIDIndex());
	m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_INTENSIVEPRACTICE_SHOOT_VICTORY, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vInfo);

	vInfo.clear();
	vInfo.push_back((int)INTENSIVEPRACTICE_TYPE_DIVING_CATCH);
	vInfo.push_back(m_pUser->GetGameIDIndex());
	m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_INTENSIVEPRACTICE_DIVING_CATCH_VICTORY, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vInfo);

	vInfo.clear();
	vInfo.push_back((int)INTENSIVEPRACTICE_TYPE_FREE_DUNK);
	vInfo.push_back(m_pUser->GetGameIDIndex());
	m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_INTENSIVEPRACTICE_FREE_DUNK_VICTORY, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vInfo);

	vInfo.clear();
	vInfo.push_back((int)INTENSIVEPRACTICE_TYPE_FREE_LAYUP);
	vInfo.push_back(m_pUser->GetGameIDIndex());
	m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_INTENSIVEPRACTICE_FREE_LAYUP_VICTORY, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vInfo);
}
