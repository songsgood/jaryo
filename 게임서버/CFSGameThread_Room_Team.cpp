qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameUserItem.h"
qinclude "CActionShop.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "ChatSvrProxy.h"
qinclude "CBillingManagerGame.h"
qinclude "LeagueSvrProxy.h"
qinclude "ClubConfigManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CFSGameThread::Process_FSTGetMyItemBag(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	pUser->SendMyItemBag( pItemShop);
}

void CFSGameThread::Process_FSApplyBagSkill(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSSkillShop* pSkillShop = pServer->GetSkillShop();
	CHECK_NULL_POINTER_VOID(pSkillShop);
	
	CFSGameUserSkill* pUserSkill = pUser->GetUserSkill();
	CHECK_NULL_POINTER_VOID(pUserSkill);

	SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_AVATAR_SKILL_INFO_UPDATE update;
	update.iGameIDIndex = pUser->GetGameIDIndex();
	update.btServerIndex = _GetServerIndex;

	BYTE byKind;
	short nSkillCnt = -1;
	short nFreeStyleCnt = -1;
	BYTE bySkill;
	int iaUseSkill[MAX_SKILL_SLOT];
	int iaUseFreeStyle[MAX_SKILL_SLOT];
	
	memset(iaUseSkill, -1, sizeof(iaUseSkill));
	memset(iaUseFreeStyle, -1, sizeof(iaUseFreeStyle));
	
	for(int i=0; i<2; ++i)
	{
		m_ReceivePacketBuffer.Read(&byKind);
		
		if(SKILL_TYPE_SKILL == byKind)
		{
			m_ReceivePacketBuffer.Read(&nSkillCnt);
			if( nSkillCnt > MAX_SKILL_SLOT)
			{
				return ;
			}
			if(0 <= nSkillCnt && nSkillCnt <= MAX_SKILL_SLOT )
			{
				for(int j=0; j<nSkillCnt; j++)
				{
					m_ReceivePacketBuffer.Read(&bySkill);
					iaUseSkill[j] = bySkill;
				}
			}
		}
		else if(SKILL_TYPE_FREESTYLE == byKind)
		{
			m_ReceivePacketBuffer.Read(&nFreeStyleCnt);
			if( nFreeStyleCnt > MAX_SKILL_SLOT)
			{
				return ;
			}
			if(0 <= nFreeStyleCnt && nFreeStyleCnt <= MAX_SKILL_SLOT)
			{
				for(int j=0; j<nFreeStyleCnt; j++)
				{
					m_ReceivePacketBuffer.Read(&bySkill);
					iaUseFreeStyle[j] = bySkill;
				}
			}
		}
	}
	
	if(0 <= nSkillCnt)
	{
		pUserSkill->ApplyBagSkill(pSkillShop, SKILL_TYPE_SKILL, nSkillCnt, iaUseSkill, nFreeStyleCnt, iaUseFreeStyle);
	}
	if(0 <= nFreeStyleCnt)
	{
		pUserSkill->ApplyBagSkill(pSkillShop, SKILL_TYPE_FREESTYLE, nFreeStyleCnt, iaUseFreeStyle);
	}

	memcpy(&update.Skill, &pAvatarInfo->Skill, sizeof(SAvatarSkill));

	CPacketComposer PacketComposer(G2M_AVATAR_SKILL_UPDATE);
	PacketComposer.Add((BYTE*)&update, sizeof(SG2M_AVATAR_SKILL_INFO_UPDATE));
	pUserSkill->WriteSpecialtySkill(PacketComposer, pAvatarInfo->Skill);

	pMatch->Send(&PacketComposer);


	SendCurAvatarSkillPoint(pFSClient, pUser->GetCurAvatarRemainSkillPoint(), pUser->GetCurAvatarUsedSkillPointAllType());
}

void CFSGameThread::Process_FSTGetMySkillBag(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSSkillShop* pSkillShop = pServer->GetSkillShop();
	CHECK_NULL_POINTER_VOID(pSkillShop);
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	pUser->SendMySkillBag(pSkillShop);
}

// Modify for Match Server
void CFSGameThread::Process_FSTMoveChief(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHIEF_ASSIGN_MOVE_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szName, MAX_GAMEID_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChiefAssignMoveReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTCheckChiefAssign(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHIEF_ASSIGN_CHECK_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szName, MAX_GAMEID_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChiefAssignCheckReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTChangeTeamName(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_TEAM_NAME_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szName, MAX_TEAM_NAME_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeTeamNameReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTReadyTeamList(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_READY_TEAM_LIST_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.iReadMode);

	if(1 != info.iReadMode)
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCH, RECV_DATA, CHECK_FAIL, "Process_FSTReadyTeamList - ReadMode 11866902", info.iReadMode);
rn;
	}

	m_ReceivePacketBuffer.Read(&info.iFindStartID);
	m_ReceivePacketBuffer.Read(&info.iDir);
	
	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendReadyTeamListReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTRequestTeamMemberList(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
		CHECK_NULL_POINTER_VOID(pODBCBase);

		SG2M_ACTION_SLOT_UPDATE info;
		SG2M_BASE infobase;

		infobase.iGameIDIndex = info.iGameIDIndex = pUser->GetGameIDIndex();
		infobase.btServerIndex = info.btServerIndex = _GetServerIndex;

		pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());
		pUser->CopyActionSlotPacket(pODBCBase, pServer->GetActionShop(), info);

		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ACTION_SLOT, &info, sizeof(SG2M_ACTION_SLOT_UPDATE));
		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TEAM_MEMBER_LIST_REQ, &infobase, sizeof(SG2M_BASE));		

	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTRequsetTeamInfo(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;
	
	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TEAM_INFO_IN_TEAM_REQ, &info,  sizeof(SG2M_BASE));
}

// Modify for Match Server
void CFSGameThread::Process_FSTRequestTeamMatch(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_REQUEST_TEAM_MATCH_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iMode); 

	if(0 == info.iMode)
	{
		m_ReceivePacketBuffer.Read(&info.iTeamIdx);
	}
	else if(2 == info.iMode)
	{
		m_ReceivePacketBuffer.Read(&info.iResult);
		m_ReceivePacketBuffer.Read(&info.iTeamIdx);
	}

	if(0 == info.iMode || 2 == info.iMode)
	{
		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		if(pMatch) 
		{
			pMatch->SendRequestTeamMatchReq(info);
		}
	}
}

// Modify for Match Server

void CFSGameThread::Process_FSTForceOutUser(CFSGameClient* pClient)
{
	if(TRUE == CFSGameServer::GetInstance()->IsNoWithdrawChannel())
	{
		int iErrorMsg = ERROR_MSG_TYPE_EXIT_BEGINNER;
		CPacketComposer PacketComposer(S2C_ERROR_MSG_RES);
		PacketComposer.Add(iErrorMsg);
		pClient->Send(&PacketComposer);

		return;
	}

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_FORCEOUT_USER_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szBannedGameID, MAX_GAMEID_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendForceOutUserReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTChanageTeamPassword(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_TEAM_PASSWORD_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szPassword, MAX_TEAM_PASS_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeTeamPasswordReq(info);
	}	
}

// Modify for Match Server
void CFSGameThread::Process_FSLEnterTeam(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_ENTER_TEAM_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info.teamInfo, sizeof(SC2S_ENTER_WAIT_TEAM_REQ));

	CHECK_CONDITION_RETURN_VOID(MATCH_TYPE_RATING != info.teamInfo.btMatchType && MATCH_TYPE_CLUB_LEAGUE != info.teamInfo.btMatchType &&
		MATCH_TYPE_FREE != info.teamInfo.btMatchType && MATCH_TYPE_AIPVP != info.teamInfo.btMatchType);
	CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TEAM_SCALE <= info.teamInfo.btMatchTeamScale);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.teamInfo.btMatchType, pUser->GetCurUsedAvtarLv());
	if(pMatch) 
	{
		pUser->GetMatchLoginUserInfo(info, pMatch->GetMatchType());
		pMatch->SendPacket(G2M_ENTER_TEAM_REQ, &info, sizeof(SG2M_ENTER_TEAM_REQ));
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTExitWaitTeam(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendTeamExitReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTRCancelTeamMatch(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CANCEL_TEAM_MATCH_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendCancelTeamMatchReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSTRSendTeamInfo(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TEAM_INFO_REQ,&info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSTChangeUserPosition(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_POSITION_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.shSlotNumber);	
	m_ReceivePacketBuffer.Read(&info.shNewPosition);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangePositionReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::ProcessFSTChallengeRegister(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_REG_TEAM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.iMode);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendRegTeamReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::ProcessFSTChangeAvoidTeam(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_AVOID_TEAM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.shAvoidTeam);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendAvoidTeamReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::ProcessFSTAutoRegister(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_AUTO_REG_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.shMode);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendAutoRegReq(info);
	}
}

void CFSGameThread::ProcessFSTAutoTeamRegister(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if(NULL == pUser->GetCurUsedAvatar())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, CHECK_FAIL, "ProcessFSTAutoTeamRegister - �4", pUser->GetGameID());
	return;
	}

	SG2M_AUTO_TEAM_REG_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info.regInfo, sizeof(SC2S_AUTO_TEAM_REG_REQ));

	if(TRUE == pUser->GetUserAppraisal()->CheckPenaltyUser())
	{
		SS2C_AUTO_TEAM_REG_RES rs;
		rs.btResult = RESULT_AUTO_TEAM_REG_PENALTY_USER;
		rs.btRegMode = info.regInfo.btRegMode;
		rs.tPenaltyRemainTime = pUser->GetUserAppraisal()->GetPenaltyRemainTime();
		rs.iNeedClubContributionPoint = 0;
		pUser->Send(S2C_AUTO_TEAM_REG_RES, &rs, sizeof(SS2C_AUTO_TEAM_REG_RES));
		return;
	}

	CMatchSvrProxy* pMatch = nullptr;
	CLeagueSvrProxy* pLeague = dynamic_cast<CLeagueSvrProxy*>(GAMEPROXY.GetProxy(FS_LEAGUE_SERVER_PROXY));
	
	if( MATCH_TYPE_RANKMATCH == info.regInfo.btMatchType )
	{
		CHECK_NULL_POINTER_VOID(pLeague);
		pUser->GetUserRankMatch()->SetAutoTeamRegInfo(info);

		if( REG_OK == info.regInfo.btRegMode)
		{
			CHECK_CONDITION_RETURN_VOID( FALSE == pUser->GetUserRankMatch()->CheckRankMatchTeamReg() );
//			CHECK_CONDITION_RETURN_VOID( FALSE == pUser->GetUserRankMatch()->CheckPing());
		}

		if( pUser->GetUserRankMatch()->CheckLimitMatchingUser())
		{
			pUser->GetUserRankMatch()->SendAutoTeamRegFail(RESULT_AUTO_TEAM_REG_LIMIT_MATCHING_USER);
			return;
		}

		pUser->GetUserRankMatch()->SendAutoTeamReg(pLeague);

		if( REG_CANCEL == info.regInfo.btRegMode)
		{
			CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
			CHECK_NULL_POINTER_VOID( pGameODBC );

			SS2C_JOIN_TEAM_CONFIG_INFO JoinTeamInfo;
			pUser->GetJoinTeamConfig()->Load(pUser->GetGameIDIndex(), pGameODBC, JoinTeamInfo);
			pUser->Send(S2C_JOIN_TEAM_CONFIG_INFO, &JoinTeamInfo, sizeof(SS2C_JOIN_TEAM_CONFIG_INFO));
		}
	}
	else
	{
		if( pLeague != nullptr && pLeague->GetProcessID() == pUser->GetMatchLocation() )
			return;
		
		if(MATCH_TYPE_CLUB_LEAGUE == info.regInfo.btMatchType)
		{
			if(MATCH_TEAM_SCALE_3ON3CLUB != info.regInfo.btMatchTeamScale)
			{
				SS2C_CREATE_TEAM_RES rs;
				rs.btResult = RESULT_CREATE_TEAM_FAILED;
				pUser->Send(S2C_CREATE_TEAM_RES, &rs, sizeof(SS2C_CREATE_TEAM_RES));
				return;
			}

			if(CLOSED == CLUBCONFIGMANAGER.GetClubLeagueOpenState())
			{
				pUser->GetUserRankMatch()->SendAutoTeamRegFail(RESULT_AUTO_TEAM_REG_CLOSED_CLUB_LEAGUE);
				return;
			}

			if(0 >= pUser->GetClubSN())
			{
				pUser->GetUserRankMatch()->SendAutoTeamRegFail(RESULT_AUTO_TEAM_REG_NOT_CLUB_USER);
				return;
			}

			SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
			CHECK_NULL_POINTER_VOID(pAvatarInfo);

			if(pAvatarInfo->iClubContributionPoint < CLUBCONFIGMANAGER.GetNeedContributionPoint())
			{
				SS2C_AUTO_TEAM_REG_RES rs;
				rs.btResult = RESULT_AUTO_TEAM_REG_ENOUGH_CONTRIBUTION_POINT;
				rs.btRegMode = info.regInfo.btRegMode;
				rs.tPenaltyRemainTime = 0;
				rs.iNeedClubContributionPoint = CLUBCONFIGMANAGER.GetNeedContributionPoint();
				pUser->Send(S2C_AUTO_TEAM_REG_RES, &rs, sizeof(SS2C_AUTO_TEAM_REG_RES));
				return;
			}
		}

		if (AUTO_REG_TYPE_USER == info.regInfo.btAutoRegType)
		{
			CHECK_CONDITION_RETURN_VOID(MATCH_TYPE_RATING != info.regInfo.btMatchType && MATCH_TYPE_CLUB_LEAGUE != info.regInfo.btMatchType &&
				MATCH_TYPE_FREE != info.regInfo.btMatchType && MATCH_TYPE_AIPVP != info.regInfo.btMatchType);
			CHECK_CONDITION_RETURN_VOID(REG_OK == info.regInfo.btRegMode && MAX_MATCH_TEAM_SCALE <= info.regInfo.btMatchTeamScale);
			CHECK_CONDITION_RETURN_VOID(MATCH_TYPE_AIPVP == info.regInfo.btMatchType && MATCH_TEAM_SCALE_3ON3 != info.regInfo.btMatchTeamScale);

			if(REG_OK == info.regInfo.btRegMode)
			{
				pUser->GetJoinTeamConfig()->SetPositions(info.regInfo.btMatchType, info.regInfo.btMatchTeamScale, info.regInfo.iaPosition, info.regInfo.iAvoidTeam);
				pUser->GetMatchLoginUserInfo(info, info.regInfo.btMatchType);
			}
			else
			{
				info.AvatarInfo.iGameIDIndex = pUser->GetGameIDIndex();
			}

			pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.regInfo.btMatchType, pUser->GetCurUsedAvtarLv());
		}
		else if (AUTO_REG_TYPE_TEAM == info.regInfo.btAutoRegType)
		{
			CHECK_CONDITION_RETURN_VOID(0 >= pUser->GetMatchLocation());

			info.AvatarInfo.iGameIDIndex = pUser->GetGameIDIndex();

			pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		}

		if(pMatch) 
		{
			if(REG_OK == info.regInfo.btRegMode)
				pUser->SetMatching(TRUE);
			else if(REG_CANCEL == info.regInfo.btRegMode)
				pUser->SetMatching(FALSE);

			pMatch->SendPacket(G2M_AUTO_TEAM_REG_REQ, &info, sizeof(SG2M_AUTO_TEAM_REG_REQ));
		}
	}
}

void CFSGameThread::Process_ActionRoomAniReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CActionShop* pActionShop = pServer->GetActionShop();;
	CHECK_NULL_POINTER_VOID(pActionShop);

	SG2M_ACTION_ANIM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	
	int iInputKey = 0;
	const char* szTempAniGameID = pUser->GetGameID();
	m_ReceivePacketBuffer.Read(&iInputKey);
	m_ReceivePacketBuffer.Read((BYTE*)info.szAniGameID, MAX_GAMEID_LENGTH+1);

	info.iActionCode = pUser->GetUserActionKey(iInputKey);

	int iSex = pUser->GetCurUsedAvatarSex();
	int iActionTeamIndex = pActionShop->GetActionTeamIndex(info.iActionCode);

	if(iSex == 1 && info.iActionCode > 0 && -1 == iActionTeamIndex )
		info.iActionCode += 1;	// 0 - male, 1 - female

	if( info.iActionCode <= 0 ) 
		return;
	info.iKind = pActionShop->GetActionKind( info.iActionCode );

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendActionAnimReq(info);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLONE_CHARACTER_CHANGE_ROOM_REQ)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBCBase);

	if( TRUE == pUser->GetCloneCharacter() )
	{
		int iUseStyle = pUser->GetUseStyleIndex();
		Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > iUseStyle ? Clone_Index_Clone : Clone_Index_Mine ;// �Ųٷ� �ٲ���� ��.
	
		SAvatarInfo* pAvatarInfo = pUser->ChangeCloneAvatar(eIndex);
		if( NULL != pAvatarInfo )
		{
			int iChangeStyle = pUser->GetCloneCharacterStyleIndex(eIndex);

 			pUser->GetUserItemList()->ChangeBasicItem_Sex(pAvatarInfo->iSex);
 			pUser->ChangeCloneAvatarDiffSexItem(pAvatarInfo->iSex);
 			if(ODBC_RETURN_SUCCESS == pODBCBase->AVATAR_ChangeUseStyleIndex(pUser->GetGameIDIndex(), iChangeStyle))
 			{
 				pUser->SetUseStyleIndex(iChangeStyle);
			}
			pUser->RemoveChangeDressItem();

 			if( PCROOM_KIND_PREMIUM == pUser->GetPCRoomKind())
 				pUser->LoadPCRoomItemList(TRUE);

			pUser->SendUseStyleAndNameInfo();
			pUser->SendAvatarInfoUpdateToMatch(true);

			CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
			if(pMatch) 
			{
				//pMatch->SendActionSlot(pUser);	
				SG2M_ACTION_SLOT_UPDATE info;
				SG2M_BASE infobase;

				infobase.iGameIDIndex = info.iGameIDIndex = pUser->GetGameIDIndex();
				infobase.btServerIndex = info.btServerIndex = _GetServerIndex;

				pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());
				pUser->CopyActionSlotPacket(pODBCBase, pServer->GetActionShop(), info);

				pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ACTION_SLOT, &info, sizeof(SG2M_ACTION_SLOT_UPDATE));
				pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TEAM_MEMBER_LIST_REQ, &infobase, sizeof(SG2M_BASE));		
			}
		}
	}
	else	// �ߴ������ �����ϰ� �ȿ��� �˻��մϴ�.
	{
		BOOL bResult = FALSE;

		SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
		CHECK_NULL_POINTER_VOID(pAvatarInfo);

		CAvatarCreateManager* pAvatarCreateManager = &AVATARCREATEMANAGER;
		CHECK_NULL_POINTER_VOID(pAvatarCreateManager);

		SSpecialAvatarConfig* pAvatarConfig = pAvatarCreateManager->GetSpecialAvatarConfigByIndex(pAvatarInfo->iSpecialCharacterIndex);
		CHECK_NULL_POINTER_VOID(pAvatarConfig);

		int iStyleCount = pAvatarConfig->GetMultipleCharacterCount();
		CHECK_CONDITION_RETURN_VOID( 2 > iStyleCount ); // �ߴ��� �ƴϿ� 

		int iUseStyle = 1;

		if( /*Clone_Index_Clone*/ Clone_Index_Mine == pUser->CheckCloneCharacter_Index() ) // �ߴ����ϰ�� �ε����� ������ ��� �Ѵ�. �Ͽ� �Ųٷ� ��
		{
			iUseStyle += (CLONE_CHARACTER_DEFULAT_STYLE_INDEX-1);
		}

		if(iUseStyle > 0 && iUseStyle <= MAX_STYLE_COUNT)
		{
			if(ODBC_RETURN_SUCCESS == pODBCBase->AVATAR_ChangeUseStyleIndex(pUser->GetGameIDIndex(), iUseStyle))
			{
				bResult = pUser->SetUseStyleIndex(iUseStyle);
			}
		}

		pUser->SendUseStyleAndNameInfo();
		pUser->SendAvatarInfoUpdateToMatch(true);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_UPDATE_ROOM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_UPDATE_ROOM_INFO_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info,sizeof(SC2S_PVE_UPDATE_ROOM_INFO_REQ));

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());		
	if(nullptr != pMatch && 
		(pMatch->GetMatchType() == MATCH_TYPE_PVE ||
		pMatch->GetMatchType() == MATCH_TYPE_PVE_3CENTER ||
		pMatch->GetMatchType() == MATCH_TYPE_PVE_CHANGE_GRADE ||
		pMatch->GetMatchType() == MATCH_TYPE_PVE_AFOOTBALL))
	{
		CPacketComposer Packet(G2M_PVE_UPDATE_ROOM_INFO_REQ);

		SG2M_PVE_UPDATE_ROOM_INFO_REQ rq;
		rq.iGameIDIndex = pUser->GetGameIDIndex();
		rq.btServerIndex = _GetServerIndex;
		rq.btIsAllowToObserve = info.btIsAllowToObserve;
		rq.btUpdate = info.btUpdate;
		rq.iButtonIndex = info.iButtonIndex;

		Packet.Add((PBYTE)&rq, sizeof(SG2M_PVE_UPDATE_ROOM_INFO_REQ));

		pMatch->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_CHANGE_AI_POSITION_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_CHANGE_AI_POSITION_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info,sizeof(SC2S_PVE_CHANGE_AI_POSITION_REQ));

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());		
	CHECK_NULL_POINTER_VOID(pMatch);
	if(pMatch->GetMatchType() == MATCH_TYPE_PVE || pMatch->GetMatchType() == MATCH_TYPE_AIPVP ||
		pMatch->GetMatchType() == MATCH_TYPE_PVE_3CENTER ||
		pMatch->GetMatchType() == MATCH_TYPE_PVE_CHANGE_GRADE ||
		pMatch->GetMatchType() == MATCH_TYPE_PVE_AFOOTBALL)
	{
		SG2M_PVE_CHANGE_AI_POSITION_REQ rq; 
		rq.iGameIDIndex = pUser->GetGameIDIndex();
		rq.btServerIndex = _GetServerIndex;
		rq.shNewPosition = info.shNewPosition;
		rq.shSlotNumber = info.shSlotNumber;

		CPacketComposer Packet(G2M_PVE_CHANGE_AI_POSITION_REQ);
		Packet.Add((PBYTE)&rq, sizeof(SG2M_PVE_CHANGE_AI_POSITION_REQ));
		pMatch->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_AIPVP_CHANGE_GAMEPLAYERNUM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_AIPVP_CHANGE_GAMEPLAYERNUM_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info,sizeof(SC2S_AIPVP_CHANGE_GAMEPLAYERNUM_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(nullptr != pMatch && pMatch->GetMatchType() == MATCH_TYPE_AIPVP)
	{
		SG2M_AIPVP_CHANGE_GAMEPLAYERNUM_REQ rq; 
		rq.iGameIDIndex = pUser->GetGameIDIndex();
		rq.btServerIndex = _GetServerIndex;
		rq.btPlayerNum = info.btPlayerNum;

		CPacketComposer Packet(G2M_AIPVP_CHANGE_GAMEPLAYERNUM_REQ);
		Packet.Add((PBYTE)&rq, sizeof(SG2M_AIPVP_CHANGE_GAMEPLAYERNUM_REQ));
		pMatch->Send(&Packet);
	}
}