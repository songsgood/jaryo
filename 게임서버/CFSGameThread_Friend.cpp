qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CenterSvrProxy.h"
qinclude "FriendInviteManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CFSGameThread::Process_MsgToAllFriend(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if(!pUser) return;
	// C2S_MSG_TO_ALLFRIEND	
	short 	msgLen;
	char 	msg[128]; 
	
	m_ReceivePacketBuffer.Read(&msgLen);
	if(msgLen <= 0 || 128 < msgLen) return;
	m_ReceivePacketBuffer.Read((BYTE*)msg, msgLen);		msg[msgLen] = '\0';
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	
	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	
	if(pCenter) pCenter->SendMsgToAllFriend((char*)pUser->GetGameID(), (BYTE*)msg, msgLen);	
}

void CFSGameThread::Process_MsgToFriend(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if(!pUser) return;
	
	// C2S_MSG_TO_FRIEND
	DECLARE_INIT_TCHAR_ARRAY(recvID, MAX_GAMEID_LENGTH+1);	
	short 	msgLen;
	char 	msg[128]; 
	
	m_ReceivePacketBuffer.Read((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
	recvID[MAX_GAMEID_LENGTH] = 0;	
	m_ReceivePacketBuffer.Read(&msgLen);
	if(msgLen <= 0 || 128 < msgLen || strlen(recvID) < 1) return;
	m_ReceivePacketBuffer.Read((BYTE*)msg, msgLen);		msg[msgLen] = '\0';
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	
	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	
	if(pCenter) pCenter->SendMsgToFriend((char*)pUser->GetGameID(), (BYTE*)msg, msgLen, recvID);	
}

void CFSGameThread::Process_AddFriend(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	
	if (NULL == pUser)
		return;
	
	int iGameIDIndex = pUser->GetGameIDIndex();
	
	if ( -1 == iGameIDIndex)
		return;
	
	BYTE  	OpCode;
	char 	friendID[MAX_GAMEID_LENGTH+1];	
	BYTE	res = 0;
	BYTE	position = 0;
	
	m_ReceivePacketBuffer.Read(&OpCode);
	m_ReceivePacketBuffer.Read((BYTE*)friendID, MAX_GAMEID_LENGTH+1);	
	friendID[MAX_GAMEID_LENGTH] = 0;	
	
	if(strcmp(friendID, pUser->GetGameID()) == 0) {	return;	}
	if(strlen(friendID) < 1) return;
	
	m_ReceivePacketBuffer.Read(&res);// res = 0 : cancel  res = 1 : ok
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if(NULL != pServer)
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
		if(NULL != pCenter) 
		{
			pCenter->SendAddFriend(iGameIDIndex, (char*)pUser->GetGameID(), friendID, OpCode, res, 
				pUser->GetCurUsedAvatarPosition(), pUser->GetCurUsedAvtarLv(),
				pUser->GetCurUsedAvtarFameLevel(), pUser->GetEquippedAchievementTitle());
		}
	}
}

void CFSGameThread::Process_AddInterestUser(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	
	if (NULL == pUser)
		return;
	
	int iGameIDIndex = pUser->GetGameIDIndex();
	
	if ( -1 == iGameIDIndex)
		return;
	
	DECLARE_INIT_TCHAR_ARRAY(InterestGameID, MAX_GAMEID_LENGTH+1);	
	
	m_ReceivePacketBuffer.Read((BYTE*)InterestGameID, MAX_GAMEID_LENGTH+1);	
	InterestGameID[MAX_GAMEID_LENGTH] = 0;	
	
	if (strcmp(InterestGameID, pUser->GetGameID()) == 0 || strlen(InterestGameID) < 1)	
	{	
		return;	
	}	
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if (NULL == pServer) return;

	CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	
	if (NULL != pCenterSvrProxy) 
	{
		pCenterSvrProxy->SendAddInterestUserReq(iGameIDIndex, (char*)pUser->GetGameID(), InterestGameID);		
	}
}

void CFSGameThread::Process_AddBannedUser(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	
	if (NULL == pUser)
		return;
	
	int iGameIDIndex = pUser->GetGameIDIndex();
	
	if ( -1 == iGameIDIndex)
		return;
	
	DECLARE_INIT_TCHAR_ARRAY(BannedGameID, MAX_GAMEID_LENGTH+1);	
	
	m_ReceivePacketBuffer.Read((BYTE*)BannedGameID, MAX_GAMEID_LENGTH+1);	
	BannedGameID[MAX_GAMEID_LENGTH] = 0;
	
	if (strcmp(BannedGameID, pUser->GetGameID()) == 0 || strlen(BannedGameID) < 1) 
	{	
		return;	
	}	
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if (NULL == pServer) return;
	
	CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
	if (NULL != pCenterSvrProxy) 
	{
		pCenterSvrProxy->SendAddBannedUserReq(iGameIDIndex, (char*)pUser->GetGameID(), BannedGameID);	
	}
}

void CFSGameThread::Process_DelInterestUser(CFSGameClient *pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	
	if (NULL == pUser)
		return;
	
	int iGameIDIndex = pUser->GetGameIDIndex();
	
	if ( -1 == iGameIDIndex)
		return;
	
	DECLARE_INIT_TCHAR_ARRAY(InterestGameID, MAX_GAMEID_LENGTH+1);
	
	m_ReceivePacketBuffer.Read((BYTE*)InterestGameID, MAX_GAMEID_LENGTH+1);	
	InterestGameID[MAX_GAMEID_LENGTH] = 0;
	
	if (strcmp(InterestGameID, pUser->GetGameID()) == 0 || strlen(InterestGameID) < 1) 
	{	
		return;	
	}	
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if (pServer)
	{
		CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		
		if (pCenterSvrProxy) 
			pCenterSvrProxy->SendDelInterestUserReq(iGameIDIndex, (char*)pUser->GetGameID(), InterestGameID);
	}
}

void CFSGameThread::Process_DelBannedUser(CFSGameClient *pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	
	if (NULL == pUser)
		return;
	
	int iGameIDIndex = pUser->GetGameIDIndex();
	
	if ( -1 == iGameIDIndex)
		return;
	
	DECLARE_INIT_TCHAR_ARRAY(BannedGameID, MAX_GAMEID_LENGTH+1);
	
	m_ReceivePacketBuffer.Read((BYTE*)BannedGameID, MAX_GAMEID_LENGTH+1);	
	BannedGameID[MAX_GAMEID_LENGTH] = 0;
	
	if (strcmp(BannedGameID, pUser->GetGameID()) == 0 || strlen(BannedGameID) < 1) 
	{	
		return;	
	}	
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if (pServer)
	{
		CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		
		if (pCenterSvrProxy) 
			pCenterSvrProxy->SendDelBannedUserReq(iGameIDIndex, (char*)pUser->GetGameID(), BannedGameID);	
	}
}

void CFSGameThread::Process_DelFriend(CFSGameClient *pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	
	if (NULL == pUser)
		return;
	
	int iGameIDIndex = pUser->GetGameIDIndex();
	
	if ( -1 == iGameIDIndex)
		return;
	
	DECLARE_INIT_TCHAR_ARRAY(friendID, MAX_GAMEID_LENGTH+1);
	
	m_ReceivePacketBuffer.Read((BYTE*)friendID, MAX_GAMEID_LENGTH+1);	
	friendID[MAX_GAMEID_LENGTH] = 0;
	
	if(strlen(friendID) < 1) return ;
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if(pServer)
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if(pCenter) 
			pCenter->SendDelFriend(iGameIDIndex, (char*)pUser->GetGameID(), friendID);
	}
}


DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserFriendInvite()->SendFriendInviteMyInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_INVITE_MISSION_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((BYTE*)&rq, sizeof(SC2S_FRIEND_INVITE_MISSION_INFO_REQ));

	pUser->GetUserFriendInvite()->SendFriendMissionInfo(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_UPDATE_COMMENT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_INVITE_UPDATE_COMMENT_REQ rq;
	m_ReceivePacketBuffer.Read((BYTE*)&rq, sizeof(SC2S_FRIEND_INVITE_UPDATE_COMMENT_REQ));

	pUser->GetUserFriendInvite()->UpdateFriendComment(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserFriendInvite()->SendFriendInviteRewardList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_MYMISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserFriendInvite()->SendMyMissionInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_REWARD_RECEIVING_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_INVITE_REWARD_RECEIVING_REQ rq;
	m_ReceivePacketBuffer.Read((BYTE*)&rq, sizeof(SC2S_FRIEND_INVITE_REWARD_RECEIVING_REQ));

	pUser->GetUserFriendInvite()->GiveFriendInviteReward(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_UPDATE_FRIEND_STATUS_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_INVITE_UPDATE_FRIEND_STATUS_REQ rq;
	m_ReceivePacketBuffer.Read((BYTE*)&rq, sizeof(SC2S_FRIEND_INVITE_UPDATE_FRIEND_STATUS_REQ));

	pUser->GetUserFriendInvite()->UpdateFriendStatus(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_ADD_FRIEND_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_INVITE_ADD_FRIEND_REQ rq;
	m_ReceivePacketBuffer.Read((BYTE*)&rq, sizeof(SC2S_FRIEND_INVITE_ADD_FRIEND_REQ));

	pUser->GetUserFriendInvite()->AddInviteFriend(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_FRIEND_INVITE_KING_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_FRIEND_INVITE_KING_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((BYTE*)&rq, sizeof(SC2S_EVENT_FRIEND_INVITE_KING_INFO_REQ));

	CHECK_CONDITION_RETURN_VOID(FALSE == pUser->GetUserFriendInvite()->GetEventInviteKing()->LoadFriendInviteKingInfo());

	SS2C_EVENT_FRIEND_INVITE_KING_INFO_RES rs;

	CPacketComposer Packet(S2C_EVENT_FRIEND_INVITE_KING_INFO_RES);
	rs.iCurrentPage = rq.iSelectPage;
	rs.iMaxUserCount = FRIENDINVITEKING.GetInviteKingRankUserCount();
	rs.sMyInfo.btServerINdex = _GetServerIndex;
	rs.sMyInfo.iAcquisitionExp = pUser->GetUserFriendInvite()->GetEventInviteKing()->GetTotalExp();
	rs.sMyInfo.iRank = FRIENDINVITEKING.GetMyRankNum(pUser->GetUserIDIndex());
	const SREPRESENT_INFO* pRepresent = pUser->GetRepresentInfo();
	if(pRepresent)
		strncpy_s(rs.sMyInfo.szGameID, _countof(rs.sMyInfo.szGameID), pRepresent->szGameID, MAX_GAMEID_LENGTH);
	else
		strcpy_s(rs.sMyInfo.szGameID, _countof(rs.sMyInfo.szGameID), "null");

	rs.iUserCnt = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_FRIEND_INVITE_KING_INFO_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	rs.iUserCnt = FRIENDINVITEKING.MakePacketRanklist(Packet, rq);

	memcpy(pCountLocation, &rs.iUserCnt, sizeof(int));

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_CHECK_INVITE_CODE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_INVITE_CHECK_INVITE_CODE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_FRIEND_INVITE_CHECK_INVITE_CODE_REQ));

	pUser->GetUserFriendInvite()->CheckAndSendFriendInviteCode(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_ADD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_ADD_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_ADD_REQ));

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
	if(NULL != pCenter) 
	{
		SG2S_FRIEND_ACCOUNT_ADD_REQ ss;
		ss.iGameIDIndex = pUser->GetGameIDIndex();
		strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		ss.btOptionType = rs.btOptionType;

		if(FriendAccount::OPTION_TYPE_ADD_FRIEND_REQ == rs.btOptionType)
		{
			strncpy_s(ss.szFriendGameID, _countof(ss.szFriendGameID), rs.szFriendGameID, MAX_GAMEID_LENGTH);
		}
		else
		{
			ss.iFriendIDIndex = rs.iFriendIDIndex;
			ss.bOk = rs.bOk;
		}

		CPacketComposer Packet(G2S_FRIEND_ACCOUNT_ADD_REQ);
		Packet.Add((PBYTE)&ss, sizeof(SG2S_FRIEND_ACCOUNT_ADD_REQ));
		pCenter->Send(&Packet);			
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_ADD_RECOMMEND_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_ADD_RECOMMEND_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_ADD_RECOMMEND_REQ));

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
	if(NULL != pCenter) 
	{
		SG2S_FRIEND_ACCOUNT_ADD_REQ ss;
		ss.iGameIDIndex = pUser->GetGameIDIndex();
		strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		ss.btOptionType = FriendAccount::OPTION_TYPE_ADD_FRIEND_REQ;

		CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		SREPRESENT_INFO	sInfo;
		CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->Represent_GetUserInfo(rs.iUserIDIndex, sInfo));

		strncpy_s(ss.szFriendGameID, _countof(ss.szFriendGameID), sInfo.szGameID, MAX_GAMEID_LENGTH);

		CPacketComposer Packet(G2S_FRIEND_ACCOUNT_ADD_REQ);
		Packet.Add((PBYTE)&ss, sizeof(SG2S_FRIEND_ACCOUNT_ADD_REQ));
		pCenter->Send(&Packet);			
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_DELETE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_DELETE_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_DELETE_REQ));

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
	if(NULL != pCenter)
	{
		SG2S_FRIEND_ACCOUNT_DELETE_REQ	ss;
		ss.iGameIDIndex = pUser->GetGameIDIndex();
		strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		ss.iFriendIDIndex = rs.iFriendIDIndex;

		CPacketComposer Packet(G2S_FRIEND_ACCOUNT_DELETE_REQ);
		Packet.Add((PBYTE)&ss, sizeof(SG2S_FRIEND_ACCOUNT_DELETE_REQ));
		pCenter->Send(&Packet);	
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_MEMO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_UPDATE_MEMO_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_UPDATE_MEMO_REQ));

	SS2C_FRIEND_ACCOUNT_UPDATE_MEMO_RES	ss;
	ss.iFriendIDIndex = rs.iFriendIDIndex;
	ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_MEMO_RES::_FAIL;

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	if(strlen(rs.szMemo) > 0 && 
		FALSE == CFSGameServer::GetInstance()->GetGameIDStringChecker()->IsValidGameIDCharacter(rs.szMemo, FALSE))
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_MEMO_RES::_FAIL_BAD_WORD;
	}
	else
	{
		int iRet = pODBC->FRIENDACCOUNT_UpdateFriendMemo(pUser->GetUserIDIndex(), rs.iFriendIDIndex, rs.szMemo);
		ss.eResult = static_cast<SS2C_FRIEND_ACCOUNT_UPDATE_MEMO_RES::RESULT>(iRet);

		if(ODBC_RETURN_SUCCESS == iRet)
		{
			CScopedLockFriendAccount pFriend(pUser->GetFriendAccountList(), rs.iFriendIDIndex);
			if(pFriend != nullptr)
			{
				pFriend->SetNoteName(rs.szMemo);
				ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_MEMO_RES::_SUCCESS;

				CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
				if(NULL != pCenter)
				{
					SG2S_FRIEND_ACCOUNT_UPDATE_MEMO_NOT	ss;
					ss.iGameIDIndex = pUser->GetGameIDIndex();
					strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
					ss.sInfo = rs;

					CPacketComposer Packet(G2S_FRIEND_ACCOUNT_UPDATE_MEMO_NOT);
					Packet.Add((PBYTE)&ss, sizeof(SG2S_FRIEND_ACCOUNT_UPDATE_MEMO_NOT));
					pCenter->Send(&Packet);	
				}
			}
		}
	}

	pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_MEMO_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_MEMO_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ));

	SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES));

	SUserFriendAccountInfo& sUserInfo = pUser->GetUserFriendAccountInfo();
	time_t tCurrentTime = _time64(NULL);
	if(tCurrentTime < sUserInfo.tLastTime_UpdateAccountID + (24*60*60))
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES::_FAIL_REMAIN_COOLTIME_1DAY;
		pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES));
		return;
	}

	if(FALSE == CFSGameServer::GetInstance()->GetGameIDStringChecker()->IsValidGameIDCharacter(rs.szNewAccountID))
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES::_FAIL_BAD_WORD;
		pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES));
		return;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN_VOID(NULL == pODBC);

	int iRet = pODBC->FRIENDACCOUNT_UpdateAccountID(pUser->GetUserIDIndex(), tCurrentTime, rs.szNewAccountID);
	if(ODBC_RETURN_SUCCESS != iRet)
	{
		ss.eResult = static_cast<SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES::RESULT>(iRet);
		pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES));
		return;
	}

	sUserInfo.tLastTime_UpdateAccountID = tCurrentTime;
	ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES::_SUCCESS;
	pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES));

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
	if(NULL != pCenter)
	{
		SG2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ	ss2;
		ss2.iGameIDIndex = pUser->GetGameIDIndex();
		strncpy_s(ss2.GameID, _countof(ss2.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		strncpy_s(ss2.szNewAccountID, _countof(ss2.szNewAccountID), rs.szNewAccountID, FriendAccount::MAX_ACCOUNT_ID_LENGTH);

		CPacketComposer Packet(G2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ);
		Packet.Add((PBYTE)&ss2, sizeof(SG2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ));
		pCenter->Send(&Packet);	
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ));

	SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES	ss;

	if(rs.iCnt > MAX_SEND_AVATAR_LIST_SIZE)
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES::_FAIL_OVER_MAX_CNT;
		pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES));
		return;
	}

	SUserFriendAccountInfo& sUserInfo = pUser->GetUserFriendAccountInfo();
	time_t tCurrentTime = _GetCurrentDBDate;
	if(tCurrentTime < sUserInfo.tLastTime_UpdateSecretAvatar + (24*60*60))
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES::_FAIL_REMAIN_COOLTIME_1DAY;
		pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES));
		return;
	}

	vector<int> vAvatar;
	int iGameIDIndex = 0;

	for(int i = 0; i < rs.iCnt; ++i)
	{
		m_ReceivePacketBuffer.Read(&iGameIDIndex);
		vAvatar.push_back(iGameIDIndex);
	}

	char szText[MAX_LENGTH_SECRETAVATAR_CHG_LIST_TEXT + 1] = {'\0',};
	ConvertRewardIndexStr(szText, &vAvatar, MAX_SEND_AVATAR_LIST_SIZE, MAX_LENGTH_SECRETAVATAR_CHG_LIST_TEXT);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN_VOID(NULL == pODBC);

	int iRet = pODBC->FRIENDACCOUNT_UpdateSecretAvatar(pUser->GetUserIDIndex(), tCurrentTime, szText);
	if(ODBC_RETURN_SUCCESS != iRet)
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND, DB_DATA_UPDATE, CHECK_FAIL, "FRIENDACCOUNT_UpdateSecretAvatar Fail. UserIDIndex=11866902, Ret=0", pUser->GetUserIDIndex(), iRet);
urn;
	}

	bool bOpt = false;
	bool bNeedNotice = pUser->UpdateMyAvatarSecretInfo(vAvatar, bOpt);
	sUserInfo.tLastTime_UpdateSecretAvatar = tCurrentTime;

	// 접속중인 캐릭터가 전환된 경우
	if(bNeedNotice)
	{
		sUserInfo.bIsSecret_CurrentAvatar = bOpt;

		if(true == bOpt) // 비공개캐릭터로 된 경우
		{
			if(ODBC_RETURN_SUCCESS != pODBC->FRIENDACCOUNT_UpdateLastLogoutTime(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), tCurrentTime))
			{
				WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "FRIENDACCOUNT_UpdateLastLogoutTime Fail. GameIDIndex:11866902, Time:0", pUser->GetGameIDIndex(), tCurrentTime);

	}

		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
		if(NULL != pCenter) 
		{
			SG2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ	ss;
			ss.iGameIDIndex = pUser->GetGameIDIndex();
			strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
			ss.bOpt = bOpt;
			ss.tCurrentTime = tCurrentTime;
			CPacketComposer Packet(G2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ);
			Packet.Add((PBYTE)&ss, sizeof(SG2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ));
			pCenter->Send(&Packet);	
		}
	}
	else
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES::_SUCCESS;
		pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_REQ));

	SS2C_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_RES	ss;
	ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_RES::_FAIL;

	SUserFriendAccountInfo& sUserInfo = pUser->GetUserFriendAccountInfo();

	time_t tCurrentTime = _GetCurrentDBDate;
	if(tCurrentTime < sUserInfo.tLastTime_UpdateRecommend + (24*60*60))
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_RES::_FAIL_REMAIN_COOLTIME_1DAY;
	}
	else if(static_cast<bool>(rs.eType) == sUserInfo.bIsRecommend)
	{
		ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_RES::_FAIL_SAME_OPT;
	}
	else
	{
		CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
		CHECK_CONDITION_RETURN_VOID(NULL == pODBC);

		int iRet = pODBC->FRIENDACCOUNT_UpdateRecommendOpt(pUser->GetUserIDIndex(), rs.eType, tCurrentTime);
		if(ODBC_RETURN_SUCCESS != iRet)
		{
			WRITE_LOG_NEW(LOG_TYPE_FRIEND, DB_DATA_UPDATE, CHECK_FAIL, "FRIENDACCOUNT_UpdateRecommendOpt Fail. UserIDIndex=11866902, Ret=0", pUser->GetUserIDIndex(), iRet);
		else
		{
			ss.eResult = SS2C_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_RES::_SUCCESS;
			sUserInfo.tLastTime_UpdateRecommend = tCurrentTime;
			sUserInfo.bIsRecommend = static_cast<bool>(rs.eType);
		}
	}

	pUser->Send(S2C_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if(0 == pUser->GetFriendAccountCnt())
	{
		SS2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_RES	ss;
		ss.iCnt = 0;
		pUser->Send(S2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_RES));

		return;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN_VOID(NULL == pODBC);

	vector<SRECOMMEND_FRIEND_INFO>	vRecommedFriend;
	if(ODBC_RETURN_SUCCESS == pODBC->FRIENDACCOUNT_GetRecommendFriendList(pUser->GetUserIDIndex(), vRecommedFriend))
	{
		SS2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_RES	ss;
		ss.iCnt = vRecommedFriend.size();

		CPacketComposer Packet(S2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_RES);
		Packet.Add((PBYTE)&ss, sizeof(SS2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_RES));

		for(int i = 0; i < ss.iCnt; ++i)
		{
			Packet.Add((PBYTE)&vRecommedFriend.at(i), sizeof(SRECOMMEND_FRIEND_INFO));
		}

		pUser->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_REQ));

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN_VOID(NULL == pODBC);

	vector<int>	vBothKnowFriend;
	if(ODBC_RETURN_SUCCESS == pODBC->FRIENDACCOUNT_GetBothKnowFriendList(pUser->GetUserIDIndex(), rs.iFriendIDIndex, vBothKnowFriend))
	{
		SS2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_RES	ss;
		ss.iCnt = vBothKnowFriend.size();

		CPacketComposer Packet(S2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_RES);
		Packet.Add((PBYTE)&ss, sizeof(SS2C_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_RES));

		for(int i = 0; i < ss.iCnt; ++i)
		{
			Packet.Add((PBYTE)&vBothKnowFriend.at(i), sizeof(int));
		}

		pUser->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ));

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);		
	if(NULL != pCenter)
	{
		SG2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ	ss;
		ss.iGameIDIndex = pUser->GetGameIDIndex();
		strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);

		ss.iFriendIDIndex = rs.iFriendIDIndex;
		ss.bFavor = rs.bFavor;

		CPacketComposer Packet(G2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ);
		Packet.Add((PBYTE)&ss, sizeof(SG2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ));
		pCenter->Send(&Packet);	
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SERVER_TIME_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_SERVER_TIME_INFO_RES	ss;
	ss.tCurrentTime = _GetCurrentDBDate;
	pUser->Send(S2C_SERVER_TIME_INFO_RES, &ss, sizeof(SS2C_SERVER_TIME_INFO_RES));
}