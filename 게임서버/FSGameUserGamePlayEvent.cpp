qinclude "stdafx.h"
qinclude "FSGameUserGamePlayEvent.h"
qinclude "GamePlayEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserGamePlayEvent::CFSGameUserGamePlayEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserGamePlayEvent::~CFSGameUserGamePlayEvent(void)
{
}

BOOL CFSGameUserGamePlayEvent::Load()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEPLAY_GetUserInfo(m_pUser->GetUserIDIndex(), m_UserEventInfo._iPoint, m_UserEventInfo._iClearCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEPLAYEVENT, INITIALIZE_DATA, FAIL, "EVENT_GAMEPLAY_GetUserInfo error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

BOOL CFSGameUserGamePlayEvent::MakeGamePlayEventInfo(CPacketComposer& Packet)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == GAMEPLAYEVENT.IsOpen(), FALSE);

	SS2C_GAMEPLAY_EVENT_INFO_RES rs;
	rs.iCurrentRewardPoint = m_UserEventInfo._iPoint;
	rs.iCurrentClearCnt = m_UserEventInfo._iClearCount;

	GAMEPLAYEVENT.MakeGamePlayEventInfo(Packet, rs);

	return TRUE;
}

void CFSGameUserGamePlayEvent::GetRewardReq()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == GAMEPLAYEVENT.IsOpen());

	SS2C_GAMEPLAY_EVNET_GET_REWARD_RES rs;
	if(m_UserEventInfo._iPoint < GAMEPLAYEVENT.GetMaxRewardPoint())
	{
		rs.btResult = RESULT_GAMEPLAY_EVNET_GET_REWARD_FAIL_ENOUGH_POINT;
		m_pUser->Send(S2C_GAMEPLAY_EVNET_GET_REWARD_RES, &rs, sizeof(SS2C_GAMEPLAY_EVNET_GET_REWARD_RES));
		return;
	}

	int iPresentSize = m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();
	BOOL bBonusReward = FALSE;
	SRewardInfo sBonusRewardInfo;
	if(TRUE == GAMEPLAYEVENT.GetCurrentBonusRewardInfo(m_UserEventInfo._iClearCount, sBonusRewardInfo))
	{
		iPresentSize += 1;
		bBonusReward = TRUE;
	}

	if(iPresentSize >= MAX_PRESENT_LIST)
	{
		rs.btResult = RESULT_GAMEPLAY_EVNET_GET_REWARD_FAIL_FULL_MAIL;
		m_pUser->Send(S2C_GAMEPLAY_EVNET_GET_REWARD_RES, &rs, sizeof(SS2C_GAMEPLAY_EVNET_GET_REWARD_RES));
		return;
	}

	SRewardInfo sRewardInfo;
	CHECK_CONDITION_RETURN_VOID(FALSE == GAMEPLAYEVENT.GetRandomRewardInfo(sRewardInfo));

	SRewardConfig* pReward = REWARDMANAGER.GetReward(sRewardInfo.iRewardIndex);
	CHECK_CONDITION_RETURN_VOID(NULL == pReward);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iPrePoint = m_UserEventInfo._iPoint;
	m_UserEventInfo._iPoint = 0;

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEPLAY_UpdateUserInfo(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), 
		GAMEPLAYEVENT_DB_UPDATE_TYPE_UPDATE_CLEARCOUNT, 0,
		sRewardInfo.btRewardType, sRewardInfo.iItemCode, sRewardInfo.iPropertyValue, m_UserEventInfo._iClearCount))
	{
		m_UserEventInfo._iPoint = iPrePoint;

		rs.btResult = RESULT_GAMEPLAY_EVNET_GET_REWARD_FAIL;
		m_pUser->Send(S2C_GAMEPLAY_EVNET_GET_REWARD_RES, &rs, sizeof(SS2C_GAMEPLAY_EVNET_GET_REWARD_RES));
		return;
	}

	pReward = REWARDMANAGER.GiveReward(m_pUser, sRewardInfo.iRewardIndex);
	if (NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEPLAYEVENT, SET_DATA, FAIL, "CFSGameUserGamePlayEvent::GetRewardReq error - GameIDIndex:10752790, RewardIndex:0", 
pUser->GetGameIDIndex(), sRewardInfo.iRewardIndex);
		return;
	}

	rs.btResult = RESULT_GAMEPLAY_EVNET_GET_REWARD_SUCCESS;
	rs.iRewardCnt = 1;
	if(TRUE == bBonusReward)
		rs.iRewardCnt = 2;

	CPacketComposer Packet(S2C_GAMEPLAY_EVNET_GET_REWARD_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_GAMEPLAY_EVNET_GET_REWARD_RES));

	SS2C_GAMEPLAY_EVENT_REWARD_INFO info;
	info.btEventRewardType = GAMEPLAYEVENT_REWARD_TYPE_REWARD;
	info.btRewardType = sRewardInfo.btRewardType;
	info.iItemCode = sRewardInfo.iItemCode;
	info.iPropertyKind = sRewardInfo.iPropertyKind;
	info.iPropertyType = sRewardInfo.iPropertyType;
	info.iPropertyValue = sRewardInfo.iPropertyValue;
	Packet.Add((PBYTE)&info, sizeof(SS2C_GAMEPLAY_EVENT_REWARD_INFO));

	if(TRUE == bBonusReward)
	{
		int iUpdated_ClearCount;
		CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEPLAY_UpdateUserInfo(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), 
			GAMEPLAYEVENT_DB_UPDATE_TYPE_INSERT_LOG, 0,
			sBonusRewardInfo.btRewardType, sBonusRewardInfo.iItemCode, sBonusRewardInfo.iPropertyValue, iUpdated_ClearCount))

		pReward = REWARDMANAGER.GiveReward(m_pUser, sBonusRewardInfo.iRewardIndex, sBonusRewardInfo.iPropertyValue-1);
		if (NULL == pReward)
		{
			WRITE_LOG_NEW(LOG_TYPE_GAMEPLAYEVENT, SET_DATA, FAIL, "CFSGameUserGamePlayEvent::GetRewardReq error - GameIDIndex:10752790, RewardIndex:0",
_pUser->GetGameIDIndex(), sRewardInfo.iRewardIndex);
			return;
		}

		info.btEventRewardType = GAMEPLAYEVENT_REWARD_TYPE_BONUSREWARD;
		info.btRewardType = sBonusRewardInfo.btRewardType;
		info.iItemCode = sBonusRewardInfo.iItemCode;
		info.iPropertyKind = sBonusRewardInfo.iPropertyKind;
		info.iPropertyType = sBonusRewardInfo.iPropertyType;
		info.iPropertyValue = sBonusRewardInfo.iPropertyValue;
		Packet.Add((PBYTE)&info, sizeof(SS2C_GAMEPLAY_EVENT_REWARD_INFO));
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserGamePlayEvent::UpdateUserPoint(int iPoint, int& iAddPoint)
{
	CHECK_CONDITION_RETURN_VOID(0 == iPoint);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN_VOID(NULL == pODBC);

	iAddPoint = iPoint;
	if((m_UserEventInfo._iPoint+iPoint) > GAMEPLAYEVENT.GetMaxRewardPoint())
	{
		iPoint = GAMEPLAYEVENT.GetMaxRewardPoint();
		iAddPoint = GAMEPLAYEVENT.GetMaxRewardPoint() - m_UserEventInfo._iPoint;
	}

	int iUpdated_ClearCount;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEPLAY_UpdateUserInfo(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), 
		GAMEPLAYEVENT_DB_UPDATE_TYPE_UPDATE_POINT, m_UserEventInfo._iPoint+iAddPoint,
		0, 0, 0, iUpdated_ClearCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEPLAYEVENT, DB_DATA_UPDATE, FAIL, "UpdateUserPoint::EVENT_GAMEPLAY_UpdateUserInfo error - UserIDIndex:10752790, GameIDIndex:0, Point:7106560", 
->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iPoint);
	}

	m_UserEventInfo._iPoint += iAddPoint;
}

BYTE CFSGameUserGamePlayEvent::GetGameResultNoticeStatus(BYTE btMatchType, BOOL bIsDisconnectedMatch, BOOL bRunAway)
{
	CHECK_CONDITION_RETURN(CLOSED == GAMEPLAYEVENT.IsOpen(), GAMERESULT_NOTICE_STATUS_NONE);
	CHECK_CONDITION_RETURN(btMatchType != MATCH_TYPE_RATING && btMatchType != MATCH_TYPE_RANKMATCH && btMatchType != MATCH_TYPE_CLUB_LEAGUE, GAMERESULT_NOTICE_STATUS_NONE);
	CHECK_CONDITION_RETURN(TRUE == bIsDisconnectedMatch || TRUE == bRunAway, GAMERESULT_NOTICE_STATUS_NOT_NORMAL_GAME);
	CHECK_CONDITION_RETURN(m_UserEventInfo._iPoint < GAMEPLAYEVENT.GetMaxRewardPoint(), GAMERESULT_NOTICE_STATUS_ADDPOINT);

	return GAMERESULT_NOTICE_STATUS_IMPOSSIBLE_GET_REWARD;
}

