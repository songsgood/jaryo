qinclude "stdafx.h"
qinclude "CFSGameUserRandomBox.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "RandomBoxManager.h"
qinclude "RewardManager.h"
qinclude "CBillingManager.h"
qinclude "CFSGameUserItem.h"
qinclude "UserHighFrequencyItem.h"

CFSGameUserRandomBox::CFSGameUserRandomBox(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserRandomBox::~CFSGameUserRandomBox(void)
{
}

BOOL CFSGameUserRandomBox::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMBOX.IsOpen(), TRUE);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	if (ODBC_RETURN_SUCCESS != pBaseODBC->RANDOMBOX_GetAvatarMission(m_pUser->GetGameIDIndex(), m_AvatarRandomBoxMission))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMBOX, DB_DATA_LOAD, FAIL, "RANDOMBOX_GetAvatarMission failed");
		return FALSE;
	}

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	if (iCurrentDate != m_AvatarRandomBoxMission.iMissionLastDate)
	{
		m_AvatarRandomBoxMission.btMissionPlayCount = 0;	
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

RANDOMBOX_MISSION_STATUS CFSGameUserRandomBox::GetRandomBoxMissionStatus()
{
	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	CHECK_CONDITION_RETURN(iCurrentDate != m_AvatarRandomBoxMission.iMissionLastDate, RBM_STATUS_ON_GOING);

	CHECK_CONDITION_RETURN(TRUE == m_AvatarRandomBoxMission.bReceiveMissionReward, RBM_STATUS_CLOSED);

	CHECK_CONDITION_RETURN(RANDOMBOX.GetMissionPlayCount() <= m_AvatarRandomBoxMission.btMissionPlayCount, RBM_STATUS_COMPLETED);

	return RBM_STATUS_ON_GOING;	
}

void CFSGameUserRandomBox::SendRandomBoxInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	SS2C_RANDOMBOX_LIST_RES rs;
	RANDOMBOX.GetOpenTime(rs.tStartDate, rs.tEndDate);
	rs.btStatus = GetRandomBoxMissionStatus();
	rs.btMissionPlayCount = RANDOMBOX.GetMissionPlayCount();

	if(iCurrentDate != m_AvatarRandomBoxMission.iMissionLastDate)
		rs.btCurrentPlayCount = 0;
	else
		rs.btCurrentPlayCount = m_AvatarRandomBoxMission.btMissionPlayCount;

	vector<SRandomBoxItemConfig> vRandomBoxItemConfig;
	RANDOMBOX.GetRandomBoxItemList(vRandomBoxItemConfig);
	rs.iBoxItemCount = vRandomBoxItemConfig.size();

	CPacketComposer Packet(S2C_RANDOMBOX_LIST_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RANDOMBOX_LIST_RES));
	
	int iPropertyKind = 0;
	SRANDOMBOX_INFO info;
	for(int i = 0; i < rs.iBoxItemCount && i < MAX_RANDOMBOX_COUNT; ++i)
	{
		info.iBoxItemCode = vRandomBoxItemConfig[i].iBoxItemCode;
		SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(vRandomBoxItemConfig[i].iPropertyKind);
		if(NULL != pItem)
			info.iCount = pItem->iPropertyTypeValue;
		else
			info.iCount = 0;

		Packet.Add((PBYTE)&info, sizeof(SRANDOMBOX_INFO));
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserRandomBox::SendRandomBoxRewardInfo(int iBoxItemCode)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());

	CPacketComposer Packet(S2C_RANDOMBOX_REWARD_INFO_RES);
	RANDOMBOX.MakeRandomBoxRewardList(iBoxItemCode, Packet);
	m_pUser->Send(&Packet);
}

RESULT_USE_RANDOMBOX CFSGameUserRandomBox::UseRandomBox(int iBoxItemCode, int& iRewardIndex, int& iUpdatedBoxCount)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMBOX.IsOpen(), RESULT_USE_RANDOMBOX_FAILED);
	CHECK_CONDITION_RETURN(NULL == m_pUser->GetUserHighFrequencyItem(), RESULT_USE_RANDOMBOX_FAILED);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_AVATAR)->GetSize() >= MAX_PRESENT_LIST, RESULT_USE_RANDOMBOX_PRESENT_LIST_FULL);

	int iPropertyKind = RANDOMBOX.GetRandomBoxPropertyKind(iBoxItemCode);

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem( iPropertyKind );
	CHECK_CONDITION_RETURN(NULL == pItem, RESULT_USE_RANDOMBOX_NOT_ENOUGH_ITEM);
	CHECK_CONDITION_RETURN(0 >= pItem->iPropertyTypeValue, RESULT_USE_RANDOMBOX_NOT_ENOUGH_ITEM);

	srand( (unsigned)time( NULL ));
	int iPercent = (((long)rand() << 15) | rand() )TREASURE_BOX_PERCENT;


	iRewardIndex = RANDOMBOX.GetRewardIndex(iBoxItemCode, iPercent);
	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_USE_RANDOMBOX_FAILED);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_USE_RANDOMBOX_FAILED);

	int iRetValue = RESULT_USE_RANDOMBOX_SUCCESS;
	iRetValue = pODBC->ITEM_UseHighFrequencyItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), pItem->iPropertyKind, 1, iUpdatedBoxCount );
	if ( ODBC_RETURN_SUCCESS != iRetValue )
	{
		return (RESULT_USE_RANDOMBOX)iRetValue;
	}

	if (0 >= iUpdatedBoxCount)
	{
		m_pUser->GetUserHighFrequencyItem()->DeleteUserHighFrequencyItem( pItem->iPropertyKind );
		pItem = NULL;
	}
	else
	{
		pItem->iPropertyTypeValue = iUpdatedBoxCount;
	}

	pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_USE_RANDOMBOX_FAILED);

	return RESULT_USE_RANDOMBOX_SUCCESS;
}

RESULT_RANDOMBOX_MISSION_REWARD CFSGameUserRandomBox::RecvMissionReward(SS2C_RANDOMBOX_MISSION_REWARD_RES& rs)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMBOX.IsOpen(), RESULT_RANDOMBOX_MISSION_REWARD_FAILED);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_AVATAR)->GetSize() >= MAX_PRESENT_LIST, RESULT_RANDOMBOX_MISSION_REWARD_LIST_FULL);

	RANDOMBOX_MISSION_STATUS eMissionStatus = GetRandomBoxMissionStatus();

	CHECK_CONDITION_RETURN(RBM_STATUS_CLOSED == eMissionStatus, RESULT_RANDOMBOX_MISSION_REWARD_CLOSED);
	CHECK_CONDITION_RETURN(RBM_STATUS_ON_GOING == eMissionStatus, RESULT_RANDOMBOX_MISSION_REWARD_NOT_COMPLETE);
	
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_RANDOMBOX_MISSION_REWARD_FAILED);
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->RANDOMBOX_AvatarCompleteMission(m_pUser->GetGameIDIndex()), RESULT_RANDOMBOX_MISSION_REWARD_FAILED);

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, RANDOMBOX.GetMissionRewardIndex());
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_RANDOMBOX_MISSION_REWARD_FAILED);

	rs.iBoxItemCode = pReward->GetRewardValue();
	rs.iBoxItemCount = pReward->GetItemCount();

	int iPropertyKind = RANDOMBOX.GetRandomBoxPropertyKind(rs.iBoxItemCode);
	rs.iUpdatedBoxCount = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( iPropertyKind );

	m_AvatarRandomBoxMission.bReceiveMissionReward = TRUE;

	m_pUser->SendHighFrequencyItemCount(ITEM_PROPERTY_KIND_RANDOM_BAGS);

	return RESULT_RANDOMBOX_MISSION_REWARD_SUCCESS;
}

void CFSGameUserRandomBox::UpdateMission()
{
	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());

	CHECK_CONDITION_RETURN_VOID(RBM_STATUS_ON_GOING != GetRandomBoxMissionStatus());

	CHECK_CONDITION_RETURN_VOID(m_AvatarRandomBoxMission.btMissionPlayCount >= RANDOMBOX.GetMissionPlayCount() && iCurrentDate == m_AvatarRandomBoxMission.iMissionLastDate);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);
	pODBC->RANDOMBOX_AvatarUpdateMission(m_pUser->GetGameIDIndex(), m_AvatarRandomBoxMission.btMissionPlayCount, m_AvatarRandomBoxMission.iMissionLastDate, m_AvatarRandomBoxMission.bReceiveMissionReward);
}
