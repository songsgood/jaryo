qinclude "stdafx.h"
qinclude "FSGameUserMiniGameZoneEvent.h"
qinclude "EventDateManager.h"
qinclude "CFSGameUser.h"
qinclude "MiniGameZoneEventManager.h"
qinclude "RewardManager.h"

CFSGameUserMiniGameZoneEvent::CFSGameUserMiniGameZoneEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_bIsLoginReward(FALSE)
	, m_iCurrentSeason(0)
	, m_iGameCoin(0)
	, m_iMainGameIDIndex(0)
	, m_btTodayTicketGetCnt(0)
	, m_btTodayMainGameIDUpdateCnt(0)
	, m_tRecentResetDate(-1)
	, m_iWaitRewardFlag(0)
{
	for(int i = 0; i < MAX_MINIGAMEZONE_GAME_TYPE; ++i)
	{
		m_iGameTicket[i] = 0;
		m_iGameScore[i] = 0;

		for(BYTE btSeason = 0; btSeason < MAX_MINIGAMEZONE_RANK_SEASON_TYPE; ++btSeason)
			m_iGameScore_Last[btSeason][i] = 0;
	}
}

CFSGameUserMiniGameZoneEvent::~CFSGameUserMiniGameZoneEvent(void)
{
}

BOOL CFSGameUserMiniGameZoneEvent::Load()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);
	
	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_MINIGAMEZONE_GetUserData(m_pUser->GetUserIDIndex(), m_iCurrentSeason, m_iGameCoin, m_iMainGameIDIndex, m_btTodayTicketGetCnt, m_btTodayMainGameIDUpdateCnt, m_tRecentResetDate, m_iWaitRewardFlag))
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAMEZONE_EVENT, INITIALIZE_DATA, FAIL, "EVENT_MINIGAMEZONE_GetUserData error. UserIDIndex=10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}
	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_MINIGAMEZONE_GetUserScore(m_pUser->GetUserIDIndex(), m_iGameTicket, m_iGameScore, m_iGameScore_Last))
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAMEZONE_EVENT, INITIALIZE_DATA, FAIL, "EVENT_MINIGAMEZONE_GetUserScore error. UserIDIndex=10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	m_bDataLoaded = TRUE;

	CheckResetDate();
	CheckAndGiveReward();

	return TRUE;
}

BOOL CFSGameUserMiniGameZoneEvent::CheckResetDate(time_t tCurrentTime/* = _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), FALSE);
	
	BOOL bResult = FALSE;

	if(-1 == m_tRecentResetDate && 0 == m_iCurrentSeason)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tRecentResetDate, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= MINIGAMEZONE.GetResetHour();

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tRecentResetDate <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(TRUE == bResult)
	{
		CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pBaseODBC);

		int iLoginRewardCnt = MINIGAMEZONE.GetLoginRewardCnt();
		int iCurrentSeason = TimetToYYYYMMDD(tCurrentTime - (MINIGAMEZONE.GetResetHour()*60*60)) / 100; // 매월 1일 07시에 시즌 리셋.

		if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_MINIGAMEZONE_ResetUserData(m_pUser->GetUserIDIndex(), tCurrentTime, iLoginRewardCnt, iCurrentSeason))
		{
			m_tRecentResetDate = tCurrentTime;
			m_bIsLoginReward = TRUE;
			m_btTodayTicketGetCnt = 0;
			m_btTodayMainGameIDUpdateCnt = 0;
			
			for(int i = 0; i < MAX_MINIGAMEZONE_GAME_TYPE; ++i)
			{
				m_iGameTicket[i] += iLoginRewardCnt;
				m_btTodayTicketGetCnt += iLoginRewardCnt;
			}

			if(iCurrentSeason > m_iCurrentSeason)
			{
				for(int i = 0; i < MAX_MINIGAMEZONE_GAME_TYPE; ++i)
				{					
					m_iGameScore_Last[MINIGAMEZONE_RANK_SEASON_TYPE_CURRENT][i] = 0;
					m_iGameScore_Last[MINIGAMEZONE_RANK_SEASON_TYPE_LAST][i] = m_iGameScore[i];
					m_iGameScore[i] = 0;
				}

				m_iCurrentSeason = iCurrentSeason;
			}
			else
			{
				for(int i = 0; i < MAX_MINIGAMEZONE_GAME_TYPE; ++i)
				{
					m_iGameScore_Last[MINIGAMEZONE_RANK_SEASON_TYPE_CURRENT][i] = m_iGameScore[i];
				}
			}
		}		
	}

	return bResult;
}

BOOL CFSGameUserMiniGameZoneEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), FALSE);

	CPacketComposer Packet(S2C_MINIGAMEZONE_EVENT_INFO_RES);
	MINIGAMEZONE.MakePacketEventInfo(Packet);

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserMiniGameZoneEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), FALSE);

	CheckResetDate();

	SS2C_MINIGAMEZONE_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_MINIGAMEZONE_USER_INFO_RES));

	for(int i = 0; i < MAX_MINIGAMEZONE_GAME_TYPE; ++i)
	{
		ss.iBestRecord[i] = m_iGameScore[i];
		ss.iGameTicket[i] = m_iGameTicket[i];
	}
	ss.iGameCoin = m_iGameCoin;
	ss.bIsLoginReward = m_bIsLoginReward;
	m_bIsLoginReward = FALSE;

	SMyAvatar sAvatar;
	m_pUser->GetMyAvatarGameID(m_iMainGameIDIndex, sAvatar);
	memcpy(ss.szMyGameID, sAvatar.szGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);

	ss.iRankListNum = MINIGAMEZONE.GetRankListNum();

	m_pUser->Send(S2C_MINIGAMEZONE_USER_INFO_RES, &ss, sizeof(SS2C_MINIGAMEZONE_USER_INFO_RES));

	return TRUE;
}

BOOL CFSGameUserMiniGameZoneEvent::UseCoin(SC2S_MINIGAMEZONE_USE_COIN_REQ& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), FALSE);

	SS2C_MINIGAMEZONE_USE_COIN_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_MINIGAMEZONE_USE_COIN_RES));

	SMiniGameZoneProductInfo	sInfo;
	if(FALSE == MINIGAMEZONE.GetProductInfo(rs.iProductIndex, sInfo))
	{
		ss.btResult = RESULT_MINIGAMEZONE_USE_COIN_FAIL_WRONG_PRODUCT;
	}
	else if(m_iGameCoin < sInfo.iPrice)
	{
		ss.btResult = RESULT_MINIGAMEZONE_USE_COIN_FAIL_LACK_COIN;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_MINIGAMEZONE_USE_COIN_FAIL_FULL_MAILBOX;
	}
	else
	{
		CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pBaseODBC);

		if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_MINIGAMEZONE_UseCoin(m_pUser->GetUserIDIndex(), sInfo.iPrice))
		{
			m_iGameCoin -= sInfo.iPrice;

			SRewardConfig* pConfig = REWARDMANAGER.GiveReward(m_pUser, sInfo.sReward.iRewardIndex);
			if(NULL == pConfig)
			{
				WRITE_LOG_NEW(LOG_TYPE_MINIGAMEZONE_EVENT, LA_DEFAULT, NONE, "CFSGameUserMiniGameZoneEvent::UseCoin REWARDMANAGER.GiveReward, UserIDIndex:10752790, RewardIndex:0"
, m_pUser->GetUserIDIndex(), sInfo.sReward.iRewardIndex);
			}

			ss.btResult = RESULT_MINIGAMEZONE_USE_COIN_SUCCESS;
			ss.iProductIndex = sInfo.iProductIndex;
		}
	}

	m_pUser->Send(S2C_MINIGAMEZONE_USE_COIN_RES, &ss, sizeof(SS2C_MINIGAMEZONE_USE_COIN_RES));

	return TRUE;
}

BOOL CFSGameUserMiniGameZoneEvent::UseTicket(SC2S_MINIGAMEZONE_USE_TICKET_REQ& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), FALSE);

	CheckResetDate();

	SS2C_MINIGAMEZONE_USE_TICKET_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_MINIGAMEZONE_USE_TICKET_RES));

	if(rs.btGameType >= MAX_MINIGAMEZONE_GAME_TYPE ||
		m_iGameTicket[rs.btGameType] <= 0)
	{
		ss.btResult = RESULT_MINIGAMEZONE_GAME_START_FAIL_LACK_TICKET;
	}
	else if(m_iMainGameIDIndex <= 0)
	{
		ss.btResult = RESULT_MINIGAMEZONE_GAME_START_EMPTY_MAIN_GAMEID;
	}
	else
	{
		CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pBaseODBC);

		const int iUseTicketCnt = 1;
		
		if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_MINIGAMEZONE_UpdateTicket(m_pUser->GetUserIDIndex(), rs.btGameType, -iUseTicketCnt))
		{
			m_iGameTicket[rs.btGameType] -= iUseTicketCnt;

			m_tGameStartTime = _time64(NULL);
			m_dwTicketKey = static_cast<DWORD>((rand()*rand())(UINT_MAX-1))+1;

		m_btPlayingGameType = rs.btGameType;
			ss.btResult = RESULT_MINIGAMEZONE_GAME_START_SUCCESS;
			ss.dwKey = m_dwTicketKey;
		}		
	}

	m_pUser->Send(S2C_MINIGAMEZONE_USE_TICKET_RES, &ss, sizeof(SS2C_MINIGAMEZONE_USE_TICKET_RES));

	return TRUE;
}

BOOL CFSGameUserMiniGameZoneEvent::UpdateScore(SC2S_MINIGAMEZONE_UPDATE_SCORE_REQ& rs, BOOL& bIsBestScore)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), FALSE);

	SS2C_MINIGAMEZONE_UPDATE_SCORE_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_MINIGAMEZONE_UPDATE_SCORE_RES));

	int iRewardCoinCnt = 0;

	if(rs.btGameType != m_btPlayingGameType ||
		rs.btGameType >= MAX_MINIGAMEZONE_GAME_TYPE ||
		rs.dwKey != m_dwTicketKey ||
		_time64(NULL) - m_tGameStartTime >= (30 + 3) ||
		FALSE == MINIGAMEZONE.GetRewardCoinCnt(rs.btGameType, rs.iScore, iRewardCoinCnt))
	{
		ss.btResult = RESULT_MINIGAMEZONE_RECORD_UPDATE_FAIL_WRONG_INFO;
	}
	else
	{
		CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pBaseODBC);

		bIsBestScore = (0 < rs.iScore &&m_iGameScore[rs.btGameType] < rs.iScore);

		if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_MINIGAMEZONE_UpdateScore(m_pUser->GetUserIDIndex(), rs.btGameType, rs.iScore, bIsBestScore, iRewardCoinCnt))
		{
			m_tGameStartTime = -1;
			m_dwTicketKey = 0;
			m_btPlayingGameType = MAX_MINIGAMEZONE_GAME_TYPE;
			m_iGameCoin += iRewardCoinCnt;

			ss.btResult = RESULT_MINIGAMEZONE_RECORD_UPDATE_SUCCESS;

			if(bIsBestScore)
			{
				ss.btResult = RESULT_MINIGAMEZONE_RECORD_UPDATE_SUCCESS_BEST_RECORD;
				m_iGameScore[rs.btGameType] = rs.iScore;
			}
		}	
	}

	m_pUser->Send(S2C_MINIGAMEZONE_UPDATE_SOCRE_RES, &ss, sizeof(SS2C_MINIGAMEZONE_UPDATE_SCORE_RES));

	return TRUE;
}

BOOL CFSGameUserMiniGameZoneEvent::CheckAndGiveTicket(BYTE& btResultTicketType, int& iGiveTicketCnt)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), FALSE);

	CheckResetDate();

	CHECK_CONDITION_RETURN(m_btTodayTicketGetCnt >= MINIGAMEZONE.GetTicketGetMaxCnt(), FALSE);
	CHECK_CONDITION_RETURN(rand()  == 0, FALSE);
;
	
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	btResultTicketType = rand() MAX_MINIGAMEZONE_GAME_TYPE;	

	const int cGiveTicketCnt = 1;

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_MINIGAMEZONE_UpdateTicket(m_pUser->GetUserIDIndex(), btResultTicketType, cGiveTicketCnt), FALSE);

	m_btTodayTicketGetCnt += cGiveTicketCnt;
	m_iGameTicket[btResultTicketType] += cGiveTicketCnt;
	iGiveTicketCnt = cGiveTicketCnt;

	return TRUE;
}

void CFSGameUserMiniGameZoneEvent::CheckAndGiveReward()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(0 >= m_iWaitRewardFlag);
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	for(int i = 0; i < MAX_MINIGAMEZONE_RANK_RECORD_TYPE; ++i)
	{
		if(m_iWaitRewardFlag & MINIGAMEZONE.GetRewardFlag(i))
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, MINIGAMEZONE.GetRewardInfo(i));
			if(NULL != pReward)
			{
				//
			}
		}
	}
	
	pBaseODBC->EVENT_MINIGAMEZONE_UpdateWaitReward(m_pUser->GetUserIDIndex(), 0);
}

BYTE CFSGameUserMiniGameZoneEvent::UpdateMainGameID(int iGameIDIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_WRONG_INFO);
	CHECK_CONDITION_RETURN(CLOSED == MINIGAMEZONE.IsOpen(), RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_WRONG_INFO);

	CheckResetDate();

	CHECK_CONDITION_RETURN(m_btTodayMainGameIDUpdateCnt >= MINIGAMEZONE.GetMainGameIDUpdateMaxCnt()
		, RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_CANNOT_TODAY);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);

	CHECK_CONDITION_RETURN(NULL == pBaseODBC || ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_MINIGAMEZONE_UpdateMainGameID(m_pUser->GetUserIDIndex(), iGameIDIndex)
		, RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_WRONG_INFO);

	m_iMainGameIDIndex = iGameIDIndex;
	++m_btTodayMainGameIDUpdateCnt;

	return RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_SUCCESS;
}

int	CFSGameUserMiniGameZoneEvent::GetLastScore(BYTE btSeasonType, BYTE btRecordType)
{
	CHECK_CONDITION_RETURN(btSeasonType >= MAX_MINIGAMEZONE_RANK_SEASON_TYPE, 0);

	switch(btRecordType)
	{
	case MINIGAMEZONE_RANK_RECORD_TYPE_TOTAL:
		{
			int iScore = 0;
			for(BYTE btGameType = 0; btGameType < MAX_MINIGAMEZONE_GAME_TYPE; ++btGameType)
				iScore += m_iGameScore_Last[btSeasonType][btGameType];

			return iScore;
		}

	case MINIGAMEZONE_RANK_RECORD_TYPE_FIND_ORDER_NUM:
	case MINIGAMEZONE_RANK_RECORD_TYPE_FIND_PAIR_NUM:
	case MINIGAMEZONE_RANK_RECORD_TYPE_BASKETBALL:
		{
			BYTE btGameType = btRecordType - 1;
			CHECK_CONDITION_RETURN(btGameType >= MAX_MINIGAMEZONE_GAME_TYPE, 0);

			return m_iGameScore_Last[btSeasonType][btGameType];
		}

	default: return 0;
	}
}