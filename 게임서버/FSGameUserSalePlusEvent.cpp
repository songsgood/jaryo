qinclude "stdafx.h"
qinclude "FSGameUserSalePlusEvent.h"
qinclude "SalePlusEventManager.h"
qinclude "CFSGameUser.h"

CFSGameUserSalePlusEvent::CFSGameUserSalePlusEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}


CFSGameUserSalePlusEvent::~CFSGameUserSalePlusEvent(void)
{
}

BOOL			CFSGameUserSalePlusEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == SALEPLUS.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<BYTE/*DayIndex*/> vecBuyDayIndex;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_SALEPLUS_GetUserData(m_pUser->GetUserIDIndex(), SALEPLUS.GetNowSeasonMinDayIndex(), vecBuyDayIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_SALEPLUS_GetUserData failed");
		return FALSE;	
	}

	for(int i = 0; i < vecBuyDayIndex.size(); ++i)
	{
		m_setBuyDayIndex.insert(vecBuyDayIndex[i]);
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void			CFSGameUserSalePlusEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == SALEPLUS.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	
	SS2C_SALEPLUS_EVENT_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SALEPLUS_EVENT_INFO_RES));

	int iToday_YYYYMMDD = 0;
	CHECK_CONDITION_RETURN_VOID(FALSE == SALEPLUS.GetProductInfo(ss, iToday_YYYYMMDD));
		
	BYTE btTodayDayIndex = SALEPLUS.GetDayIndex(iToday_YYYYMMDD);

	if(SECOND_DAY_BASIC == ss.btOnButtonIndex &&
		m_setBuyDayIndex.end() != m_setBuyDayIndex.find(btTodayDayIndex - 1))
	{
		// 둘째 날에 해당할때, 어제 상품을 구매했으면 둘째 날짜 할인상품으로 적용.
		ss.btOnButtonIndex = SECOND_DAY_SALEPLUS;
	}

	ss.bIsClear = (m_setBuyDayIndex.end() != m_setBuyDayIndex.find(btTodayDayIndex));

	m_pUser->Send(S2C_SALEPLUS_EVENT_INFO_RES, &ss, sizeof(SS2C_SALEPLUS_EVENT_INFO_RES));
}

BOOL			CFSGameUserSalePlusEvent::GetBuyInfo(BYTE btDayIndex)
{
	return m_setBuyDayIndex.end() != m_setBuyDayIndex.find(btDayIndex);
}

BOOL			CFSGameUserSalePlusEvent::BuySalePlusItem_AfterPay(BYTE btDayIndex)
{
	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_SALEPLUS_BuySalePlusItem(m_pUser->GetUserIDIndex(), btDayIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_SALEPLUS_BuySalePlusItem failed. UserIDIndex=10752790, DayIndex=0", m_pUser->GetUserIDIndex(), btDayIndex);
urn FALSE;	
	}

	m_setBuyDayIndex.insert(btDayIndex);

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add((int)FS_ITEM_OP_BUY_EVENT_SALEPLUS);
	PacketComposer.Add(BUY_ITEM_ERROR_SUCCESS);
	m_pUser->Send(&PacketComposer);

	return TRUE;
}
