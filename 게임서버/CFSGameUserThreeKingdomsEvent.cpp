qinclude "stdafx.h"
qinclude "CFSGameUserThreeKingdomsEvent.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "ThreeKingdomsEventManager.h"
qinclude "RewardManager.h"
qinclude "CFSItemList.h"

CFSGameUserThreeKingdomsEvent::CFSGameUserThreeKingdomsEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
	m_iProgressAreaIndex = EVENT_THREE_KINGDOMS_AREA_INDEX_NONE;
}

CFSGameUserThreeKingdomsEvent::~CFSGameUserThreeKingdomsEvent(void)
{
}

BOOL CFSGameUserThreeKingdomsEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == THREEKINGDOMSEVENT.IsOpen(), TRUE);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	SUserThreeKingdomsEventAreaCondition sUserAreaCondition;
	ZeroMemory(&sUserAreaCondition, sizeof(SUserThreeKingdomsEventAreaCondition));
	for(int i = 0; i < MAX_EVENT_THREE_KINGDOMS_AREA_COUNT; ++i)
	{
		sUserAreaCondition.iAreaIndex = i;
		m_mapUserAreaCondition[i] = sUserAreaCondition;
	}

	ZeroMemory(&m_UserMissionCondition, sizeof(SUserThreeKingdomsEventMissionCondition));
	ZeroMemory(&m_UserRewardCondition, sizeof(SUserThreeKingdomsEventRewardCondition)*MAX_EVENT_THREE_KINGDOMS_REWARD_TYPE_COUNT);

	vector<SUserThreeKingdomsEventAreaCondition> vUserAreaCondition;
	vector<SUserThreeKingdomsEventRewardCondition> vUserRewardCondition;
	if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_GetUserAreaCondition(m_pUser->GetUserIDIndex(), vUserAreaCondition) ||
		ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_GetUserMissionCondition(m_pUser->GetUserIDIndex(), m_UserMissionCondition) ||
		ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_GetUserRewardCondition(m_pUser->GetUserIDIndex(), vUserRewardCondition))
	{
		WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_LOAD, FAIL, "EVENT_THREE_KINGDOMS_GetUserData failed");
		return FALSE;
	}

	for(int i = 0; i < vUserAreaCondition.size(); ++i)
	{
		m_mapUserAreaCondition[vUserAreaCondition[i].iAreaIndex] = vUserAreaCondition[i];
	}

	for(int i = 0; i < vUserRewardCondition.size(); ++i)
	{
		m_UserRewardCondition[vUserRewardCondition[i].btRewardType] = vUserRewardCondition[i];
	}

	CheckAndUpdateAreaStatus();

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserThreeKingdomsEvent::CheckAndUpdateAreaStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == THREEKINGDOMSEVENT.IsOpen());

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	if(m_UserRewardCondition[EVENT_THREE_KINGDOMS_REWARD_TYPE_LAST].btStatus == EVENT_THREE_KINGDOMS_REWARD_STATUS_INACTIVATION)
	{
		for(int i = 0; i < m_mapUserAreaCondition.size(); ++i)
		{
			if(m_mapUserAreaCondition[i].btStatus == EVENT_THREE_KINGDOMS_STATUS_PROGRESS)
			{
				if(m_mapUserAreaCondition[i].iLastUpdateDate != iCurrentDate)
					UpdateAreaStatus(i, EVENT_THREE_KINGDOMS_STATUS_PROGRESS);
				else
					m_iProgressAreaIndex = i;

				break;
			}
		}

		if(m_iProgressAreaIndex == EVENT_THREE_KINGDOMS_AREA_INDEX_NONE)
		{
			SUserThreeKingdomsEventAreaCondition sLastUserAreaCondition;
			sLastUserAreaCondition.iAreaIndex = EVENT_THREE_KINGDOMS_AREA_INDEX_NONE;
			for(int i = MAX_EVENT_THREE_KINGDOMS_AREA_COUNT-1; i >= 0; --i)
			{
				if(m_mapUserAreaCondition[i].btStatus == EVENT_THREE_KINGDOMS_STATUS_COMPLETED)
				{
					memcpy(&sLastUserAreaCondition, &m_mapUserAreaCondition[i], sizeof(SUserThreeKingdomsEventAreaCondition));
					break;
				}
			}

			if(sLastUserAreaCondition.iAreaIndex == EVENT_THREE_KINGDOMS_AREA_INDEX_NONE &&
				m_mapUserAreaCondition[0].btStatus == EVENT_THREE_KINGDOMS_STATUS_NONE)
			{
				UpdateAreaStatus(EVENT_THREE_KINGDOMS_AREA_INDEX_1, EVENT_THREE_KINGDOMS_STATUS_PROGRESS);
			}
			else if(sLastUserAreaCondition.iAreaIndex > EVENT_THREE_KINGDOMS_AREA_INDEX_NONE &&
				sLastUserAreaCondition.iAreaIndex < MAX_EVENT_THREE_KINGDOMS_AREA_COUNT)
			{
				if(sLastUserAreaCondition.iLastUpdateDate != iCurrentDate)
				{
					sLastUserAreaCondition.iAreaIndex++;
					if(sLastUserAreaCondition.iAreaIndex > EVENT_THREE_KINGDOMS_AREA_INDEX_NONE &&
						sLastUserAreaCondition.iAreaIndex < MAX_EVENT_THREE_KINGDOMS_AREA_COUNT)
					{
						UpdateAreaStatus(sLastUserAreaCondition.iAreaIndex, EVENT_THREE_KINGDOMS_STATUS_PROGRESS);
					}
				}
			}
		}		
	}

	if(m_iProgressAreaIndex != EVENT_THREE_KINGDOMS_AREA_INDEX_NONE &&
		m_mapUserAreaCondition[m_iProgressAreaIndex].btStatus == EVENT_THREE_KINGDOMS_STATUS_PROGRESS &&
		m_UserMissionCondition.iLastUpdateDate == iCurrentDate &&
		m_UserMissionCondition.iProgressedCount >= THREEKINGDOMSEVENT.GetMaxMissionCount())		
		UpdateAreaStatus(m_iProgressAreaIndex, EVENT_THREE_KINGDOMS_STATUS_COMPLETED);
	else
		CheckAndUpdateAllAreaCompleted();
}

void CFSGameUserThreeKingdomsEvent::UpdateAreaStatus(int iAreaIndex, BYTE btStatus)
{
	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_UpdateAreaStatus(m_pUser->GetUserIDIndex(), iAreaIndex, btStatus, iCurrentDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_UPDATE, FAIL, "EVENT_THREE_KINGDOMS_UpdateAreaStatus failed");
		return;
	}
	m_mapUserAreaCondition[iAreaIndex].btStatus = btStatus;
	m_mapUserAreaCondition[iAreaIndex].iLastUpdateDate = iCurrentDate;

	if(btStatus == EVENT_THREE_KINGDOMS_STATUS_PROGRESS)
	{
		m_iProgressAreaIndex = iAreaIndex;
		InitializeDailyMission();
	}
	else if(btStatus == EVENT_THREE_KINGDOMS_STATUS_COMPLETED)
	{
		m_iProgressAreaIndex = EVENT_THREE_KINGDOMS_AREA_INDEX_NONE;
		if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_UpdateRewardStatus(m_pUser->GetUserIDIndex(), EVENT_THREE_KINGDOMS_REWARD_TYPE_DAILY, EVENT_THREE_KINGDOMS_REWARD_STATUS_ACTIVATION, (int)-1))
		{
			WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_UPDATE, FAIL, "EVENT_THREE_KINGDOMS_UpdateRewardStatus failed");
			return;
		}
		m_UserRewardCondition[EVENT_THREE_KINGDOMS_REWARD_TYPE_DAILY].btStatus = EVENT_THREE_KINGDOMS_REWARD_STATUS_ACTIVATION;

		CheckAndUpdateAllAreaCompleted();
	}
}

void CFSGameUserThreeKingdomsEvent::CheckAndUpdateAllAreaCompleted()
{
	CHECK_CONDITION_RETURN_VOID(m_UserRewardCondition[EVENT_THREE_KINGDOMS_REWARD_TYPE_LAST].btStatus != EVENT_THREE_KINGDOMS_REWARD_STATUS_INACTIVATION);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	BOOL bAllAreaCompleted = TRUE;
	for(int i = MAX_EVENT_THREE_KINGDOMS_AREA_COUNT - 1; i >= 0; --i)
	{
		if(m_mapUserAreaCondition[i].btStatus != EVENT_THREE_KINGDOMS_STATUS_COMPLETED)
		{
			bAllAreaCompleted = FALSE;
			break;
		}
	}

	if(bAllAreaCompleted == TRUE)
	{
		if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_UpdateRewardStatus(m_pUser->GetUserIDIndex(), EVENT_THREE_KINGDOMS_REWARD_TYPE_LAST, EVENT_THREE_KINGDOMS_REWARD_STATUS_ACTIVATION, (int)-1))
		{
			WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_UPDATE, FAIL, "EVENT_THREE_KINGDOMS_UpdateRewardStatus failed");
			return;
		}
		m_UserRewardCondition[EVENT_THREE_KINGDOMS_REWARD_TYPE_LAST].btStatus = EVENT_THREE_KINGDOMS_REWARD_STATUS_ACTIVATION;
	}
}

void CFSGameUserThreeKingdomsEvent::InitializeDailyMission()
{
	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_UpdateMissionCount(m_pUser->GetUserIDIndex(), 0, iCurrentDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_UPDATE, FAIL, "EVENT_THREE_KINGDOMS_UpdateMissionCount failed");
		return;
	}
	m_UserMissionCondition.iProgressedCount = 0;
	m_UserMissionCondition.iLastUpdateDate = iCurrentDate;

	if(m_UserRewardCondition[EVENT_THREE_KINGDOMS_REWARD_TYPE_DAILY].btStatus != EVENT_THREE_KINGDOMS_REWARD_STATUS_INACTIVATION)
	{
		if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_UpdateRewardStatus(m_pUser->GetUserIDIndex(), EVENT_THREE_KINGDOMS_REWARD_TYPE_DAILY, EVENT_THREE_KINGDOMS_REWARD_STATUS_INACTIVATION, (int)-1))
		{
			WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_UPDATE, FAIL, "EVENT_THREE_KINGDOMS_UpdateRewardStatus failed");
			return;
		}
		m_UserRewardCondition[EVENT_THREE_KINGDOMS_REWARD_TYPE_DAILY].btStatus = EVENT_THREE_KINGDOMS_REWARD_STATUS_INACTIVATION;
	}
}

void CFSGameUserThreeKingdomsEvent::SendThreeKingdomsEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == THREEKINGDOMSEVENT.IsOpen());

	CheckAndUpdateAreaStatus();

	SS2C_EVENT_THREE_KINGDOMS_INFO_RES rs;
	THREEKINGDOMSEVENT.GetOpenTime(rs.tStartTime, rs.tEndTime);
	rs.iMaxMissionCount = THREEKINGDOMSEVENT.GetMaxMissionCount();
	rs.iProgressedMissionCount = m_UserMissionCondition.iProgressedCount;
	for(BYTE btEventRewardType = 0; btEventRewardType < MAX_EVENT_THREE_KINGDOMS_REWARD_TYPE_COUNT; ++btEventRewardType)
	{
		rs.btRewardStatus[btEventRewardType] = m_UserRewardCondition[btEventRewardType].btStatus;
	}
	rs.iRecvedDailyRewardIndex = m_UserRewardCondition[EVENT_THREE_KINGDOMS_REWARD_TYPE_DAILY].iReceivedRewardIndex;
	rs.iMaxRewardCount = THREEKINGDOMSEVENT.GetMaxRewardCount();
	rs.iMaxAreaCount = MAX_EVENT_THREE_KINGDOMS_AREA_COUNT;

	CPacketComposer Packet(S2C_EVENT_THREE_KINGDOMS_INFO_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_THREE_KINGDOMS_INFO_RES));

	vector<SThreeKingdomsEventRewardConfig> vRewardInfo;
	THREEKINGDOMSEVENT.GetRewardInfo(vRewardInfo);

	SEVENT_THREE_KINGDOMS_REWARD_INFO rewardInfo;
	for(int i = 0; i < vRewardInfo.size(); ++i)
	{
		rewardInfo.btEventRewardType = vRewardInfo[i].btEventRewardType;
		rewardInfo.iRewardIndex = vRewardInfo[i].iRewardIndex;
		rewardInfo.btRewardType = vRewardInfo[i].btRewardType;
		rewardInfo.iItemCode = vRewardInfo[i].iItemCode;
		rewardInfo.iPropertyKind = vRewardInfo[i].iPropertyKind;
		rewardInfo.iPropertyType = vRewardInfo[i].iPropertyType;
		rewardInfo.iValue = vRewardInfo[i].iValue;
		rewardInfo.iTeamIndex = vRewardInfo[i].iTeamIndex;
		Packet.Add((PBYTE)&rewardInfo, sizeof(SEVENT_THREE_KINGDOMS_REWARD_INFO));
	}

	SAREA_INFO info;
	for(int i = 0; i < rs.iMaxAreaCount; ++i)
	{
		info.iAreaIndex = m_mapUserAreaCondition[i].iAreaIndex;
		info.btStatus = m_mapUserAreaCondition[i].btStatus;
		Packet.Add((PBYTE)&info, sizeof(SAREA_INFO));
	}

	m_pUser->Send(&Packet);
}

RESULT_EVENT_THREE_KINGDOMS_GET_REWARD CFSGameUserThreeKingdomsEvent::RecvMissionReward(CFSItemShop* pItemShop, BYTE btEventRewardType, int& iRewardIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == THREEKINGDOMSEVENT.IsOpen(), RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);
	CHECK_CONDITION_RETURN(pItemShop == NULL, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);
	CHECK_CONDITION_RETURN(btEventRewardType != EVENT_THREE_KINGDOMS_REWARD_TYPE_DAILY && btEventRewardType != EVENT_THREE_KINGDOMS_REWARD_TYPE_LAST, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);
	CHECK_CONDITION_RETURN(m_UserRewardCondition[btEventRewardType].btStatus == EVENT_THREE_KINGDOMS_REWARD_STATUS_INACTIVATION, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED_NOT_COMPLETED_MISSION);
	CHECK_CONDITION_RETURN(m_UserRewardCondition[btEventRewardType].btStatus == EVENT_THREE_KINGDOMS_REWARD_STATUS_COMPLETED, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED_ALREADY_GET_REWARD);

	SThreeKingdomsEventRewardConfig sRewardInfo;
	if(btEventRewardType == EVENT_THREE_KINGDOMS_REWARD_TYPE_LAST)
	{
		SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
		CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);

		THREEKINGDOMSEVENT.GetRewardInfo(btEventRewardType, iRewardIndex, sRewardInfo);
		CHECK_CONDITION_RETURN(iRewardIndex != sRewardInfo.iRewardIndex, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);

		int iCharacterSlotType = -1;
		if(sRewardInfo.btRewardType == REWARD_TYPE_ITEM)
		{
			SShopItemInfo shopItem;	
			pItemShop->GetItem(sRewardInfo.iItemCode, shopItem);
			iCharacterSlotType = shopItem.iCharacterSlotType;
		}

		if(iCharacterSlotType != -1)
			return RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_RAEDY_CREATE_AVATAR_SUCCESS;
	}
	else
	{
		CHECK_CONDITION_RETURN(m_UserMissionCondition.iProgressedCount < THREEKINGDOMSEVENT.GetMaxMissionCount(), RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED_NOT_COMPLETED_MISSION);

		THREEKINGDOMSEVENT.GetRandomRewardInfo(sRewardInfo);
		
		SRewardConfig* pReward = REWARDMANAGER.GetReward(sRewardInfo.iRewardIndex);
		CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);

		if(pReward->GetRewardType() == REWARD_TYPE_ITEM)
		{
			CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);
		}

		iRewardIndex = sRewardInfo.iRewardIndex;
	}

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pBaseODBC, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);

	if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_UpdateRewardStatus(m_pUser->GetUserIDIndex(), btEventRewardType, EVENT_THREE_KINGDOMS_REWARD_STATUS_COMPLETED, iRewardIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_UPDATE, FAIL, "EVENT_THREE_KINGDOMS_UpdateRewardStatus failed");
		return RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED;
	}
	m_UserRewardCondition[btEventRewardType].btStatus = EVENT_THREE_KINGDOMS_REWARD_STATUS_COMPLETED;
	m_UserRewardCondition[btEventRewardType].iReceivedRewardIndex = iRewardIndex;

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, sRewardInfo.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_FAILED);

	return RESULT_EVENT_THREE_KINGDOMS_GET_REWARD_SUCCESS;
}

void CFSGameUserThreeKingdomsEvent::UpdateMissionCount()
{
	CheckAndUpdateAreaStatus();

	CHECK_CONDITION_RETURN_VOID(m_UserMissionCondition.iProgressedCount >= THREEKINGDOMSEVENT.GetMaxMissionCount());
	CHECK_CONDITION_RETURN_VOID(m_iProgressAreaIndex == EVENT_THREE_KINGDOMS_AREA_INDEX_NONE);

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	const int iCurrentDate = SystemTime.wYear * 10000 + SystemTime.wMonth * 100 + SystemTime.wDay;

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	int iProgressedCount = m_UserMissionCondition.iProgressedCount + 1;
	if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_THREE_KINGDOMS_UpdateMissionCount(m_pUser->GetUserIDIndex(), iProgressedCount, iCurrentDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_UPDATE, FAIL, "EVENT_THREE_KINGDOMS_UpdateMissionCount failed");
		return;
	}
	m_UserMissionCondition.iProgressedCount = iProgressedCount;
	m_UserMissionCondition.iLastUpdateDate = iCurrentDate;

	if(iProgressedCount >= THREEKINGDOMSEVENT.GetMaxMissionCount())
		UpdateAreaStatus(m_iProgressAreaIndex, EVENT_THREE_KINGDOMS_STATUS_COMPLETED);
}
