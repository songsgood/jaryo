qinclude "stdafx.h"
qinclude "FSGameUserBigWheelLoginEvent.h"
qinclude "BigWheelLoginEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserBigWheelLoginEvent::CFSGameUserBigWheelLoginEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_tRecentCheckDate(-1)
	, m_btCurrentDayIndex(0)
	, m_iPlayCount(0)
	, m_iTodayPlayCount(0)
	, m_btLostStampCount(0)
	, m_btMinLostStampDayIndex(0)
{
	ZeroMemory(m_btStamp, sizeof(BYTE) * MAX_BIGWHEEL_LOGIN_DAY);
}


CFSGameUserBigWheelLoginEvent::~CFSGameUserBigWheelLoginEvent(void)
{
}

BOOL			CFSGameUserBigWheelLoginEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == BIGWHEELLOGIN.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_BIGWHEELLOGIN_GetUserData(m_pUser->GetUserIDIndex(), m_btCurrentDayIndex, m_iPlayCount, m_iTodayPlayCount, m_tRecentCheckDate, m_btStamp))
	{
		WRITE_LOG_NEW(LOG_TYPE_BIGWHEELLOGIN_EVENT, DB_DATA_LOAD, FAIL, "EVENT_BIGWHEELLOGIN_GetUserData failed");
		return FALSE;	
	}
	
	m_bDataLoaded = TRUE;

	CheckResetDate();
	SetStampInfo();

	if(IsEventAllClear())
	{
		m_bDataLoaded = FALSE;
		return TRUE; // 이벤트 모두 완료
	}


	return TRUE;
}

void			CFSGameUserBigWheelLoginEvent::SetStampInfo()
{
	m_btLostStampCount = 0;
	m_btMinLostStampDayIndex = 0;

	for(int i = 0; i < m_btCurrentDayIndex; ++i)
	{
		if(0 == m_btStamp[i])
		{
			if(m_btLostStampCount++ == 0)
			{
				m_btMinLostStampDayIndex = i;
			}
		}
	}
}

void			CFSGameUserBigWheelLoginEvent::CheckResetDate(time_t tCurrentTime/* = _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	BOOL	bResult = FALSE;

	if(-1 == m_tRecentCheckDate)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tRecentCheckDate, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= 9;

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tRecentCheckDate <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);
		
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_BIGWHEELLOGIN_ResetUserData(m_pUser->GetUserIDIndex(), tCurrentTime, m_btCurrentDayIndex))
		{
			m_tRecentCheckDate = tCurrentTime;
			SetStampInfo();
			m_iTodayPlayCount = 0;
		}
	}
}

void			CFSGameUserBigWheelLoginEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == BIGWHEELLOGIN.IsOpen());

	CPacketComposer* pPacket = BIGWHEELLOGIN.GetPacketEventInfo();
	m_pUser->Send(pPacket);
}

void			CFSGameUserBigWheelLoginEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == BIGWHEELLOGIN.IsOpen());

	CheckResetDate();

	CHECK_CONDITION_RETURN_VOID(m_btCurrentDayIndex >= MAX_BIGWHEEL_LOGIN_DAY);

	SS2C_BIGWHEEL_LOGIN_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_BIGWHEEL_LOGIN_USER_INFO_RES));

	ss.btCurrentDayIndex = m_btCurrentDayIndex;
	ss.btPlayCount = m_iPlayCount BIGWHEEL_LOGIN_BONUS_STAMP_PLAYCNT;

	ss.btBonusStamp = m_iPlayCount / BIGWHEEL_LOGIN_BONUS_STAMP_PLAYCNT;
	ss.btIsTodayPlay = (0 < m_iTodayPlayCount && m_btStamp[m_btCurrentDayIndex] == 0) ? TRUE : FALSE;

	for(int i = 0; i < MAX_BIGWHEEL_LOGIN_DAY; ++i)
		ss.bStamp[i] = (bool)m_btStamp[i];

	m_pUser->Send(S2C_BIGWHEEL_LOGIN_USER_INFO_RES, &ss, sizeof(SS2C_BIGWHEEL_LOGIN_USER_INFO_RES));
}

BYTE			CFSGameUserBigWheelLoginEvent::Stamp(BYTE btStampType)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_UNKNOWN);

	CheckResetDate();

	if(CLOSED == BIGWHEELLOGIN.IsOpen() || m_btCurrentDayIndex >= MAX_BIGWHEEL_LOGIN_DAY)
	{
		return BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_ALL_CLEAR;
	}
	
	if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		return BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_FULL_MAILBOX;
	}
	
	BYTE btDayIndex = 0;
	int iUsePlayCount = 0;

	if(BIGWHEEL_LOGIN_STAMP_TYPE_TODAY == btStampType)
	{
		if(m_btStamp[m_btCurrentDayIndex] > 0)
		{
			return BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_TODAY_ALREADY;
		}
		else if(m_iTodayPlayCount <= 0)
		{
			return BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_TODAY_GAMEPLAY;
		}

		btDayIndex = m_btCurrentDayIndex;
	}
	else if(BIGWHEEL_LOGIN_STAMP_TYPE_LAST == btStampType)
	{
		if(0 == m_btLostStampCount)
		{
			return BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_NO_LOST_LAST_STAMP;
		}
		else if(m_iPlayCount < BIGWHEEL_LOGIN_BONUS_STAMP_PLAYCNT)
		{
			return BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_LACK_BONUS_STAMP;
		}

		btDayIndex = m_btMinLostStampDayIndex;
		iUsePlayCount = BIGWHEEL_LOGIN_BONUS_STAMP_PLAYCNT;
	}

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);

	if(pODBC && ODBC_RETURN_SUCCESS == pODBC->EVENT_BIGWHEELLOGIN_Stamp(m_pUser->GetUserIDIndex(), btDayIndex, iUsePlayCount))
	{
		m_iPlayCount -= iUsePlayCount;
		m_btStamp[btDayIndex] = TRUE;

		if(BIGWHEEL_LOGIN_STAMP_TYPE_LAST == btStampType)
		{		
			for(int i = btDayIndex + 1; i < m_btCurrentDayIndex; ++i)
			{
				if(0 == m_btStamp[i])
				{
					m_btMinLostStampDayIndex = i;
					break;
				}
			}
			m_btLostStampCount--;
		}		
		int iRewardindex = BIGWHEELLOGIN.GetRewardIndex(btDayIndex);
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardindex);
		
		if(pReward != NULL)
			return IsEventAllClear() ? BIGWHEEL_LOGIN_STAMP_RESULT_SUCCESS_ALL_CLEAR : BIGWHEEL_LOGIN_STAMP_RESULT_SUCCESS;

		WRITE_LOG_NEW(LOG_TYPE_BIGWHEELLOGIN_EVENT, DB_DATA_LOAD, FAIL, "CFSGameUserBigWheelLoginEvent::Stamp GiveReward Fail, UserIDIndex:10752790, RewardIndex:0, DayIndex:7106560", m_pUser->GetUserIDIndex(), iRewardindex, btDayIndex);
return BIGWHEEL_LOGIN_STAMP_RESULT_FAIL_UNKNOWN;
}

void			CFSGameUserBigWheelLoginEvent::AddPlayCount()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == BIGWHEELLOGIN.IsOpen());

	CheckResetDate();

	BOOL bJustTodayCount = FALSE;
	if(m_iPlayCount >= m_btLostStampCount * BIGWHEEL_LOGIN_BONUS_STAMP_PLAYCNT)
		bJustTodayCount = TRUE;

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_VOID(pODBC);
	
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_BIGWHEELLOGIN_AddPlayCount(m_pUser->GetUserIDIndex(), bJustTodayCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_BIGWHEELLOGIN_EVENT, DB_DATA_LOAD, FAIL, "EVENT_BIGWHEELLOGIN_AddPlayCount failed, UserIDIndex:10752790, PlayCnt:0, LostStampCount:7106560", m_pUser->GetUserIDIndex(), m_iPlayCount, m_btLostStampCount);

{
		if(FALSE == bJustTodayCount)
			m_iPlayCount++;

		m_iTodayPlayCount++;
	}
}

BOOL			CFSGameUserBigWheelLoginEvent::IsEventAllClear()
{ 
	CHECK_CONDITION_RETURN(m_btCurrentDayIndex >= MAX_BIGWHEEL_LOGIN_DAY, TRUE);
	CHECK_CONDITION_RETURN(MAX_BIGWHEEL_LOGIN_DAY - 1 == m_btCurrentDayIndex && 0 == m_btLostStampCount && 0 < m_btStamp[m_btCurrentDayIndex], TRUE);

	return FALSE;
}