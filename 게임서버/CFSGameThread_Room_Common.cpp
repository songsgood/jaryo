qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSGameUserSkill.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "ChatSvrProxy.h"
qinclude "FSGameCommon.h"
qinclude "CChannelBuffManager.h"
qinclude "LeagueSvrProxy.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// Modify for Match Server
void CFSGameThread::Process_UseSecondBoostItem(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);
	
	int iItemCode = -1;

	if(TRUE == pUser->GetUserItemList()->UseSecondBoostItem(iItemCode))
	{
		SG2M_USE_SECOND_BOOST_ITEM_REQ info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.iItemCode = iItemCode;
		memset(info.iaStatAdd, 0, sizeof(int)*MAX_STAT_NUM);

		pUser->GetAvatarSingleItemStatus(iItemCode, info.iaStatAdd);
				
		if(pMatch) 
		{
			pMatch->SendUseSecondBoostItemReq(info);
		}
	}
	else
	{
		int iResult = -1;
		
		CPacketComposer PacketComposer(S2C_USE_SECOND_BOOST_ITEM_RES);
		PacketComposer.Add(iResult);						
		pUser->Send(&PacketComposer);
	}
}

// Modify for Match Server
void CFSGameThread::Process_SecondBoostItemExpired(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_SECOND_BOOST_ITEM_EXPIRED_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read((PBYTE)info.szGameID, MAX_GAMEID_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendSecondBoostItemExpiredReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSRGameStartHalfTime(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendGameStartHalfTime(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSUsePauseItem(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	if(0 < pUser->GetUserItemList()->GetPauseItemCount())
	{
		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		if(pMatch) 
		{
			pMatch->SendUsePauseItemReq(info);
		}	
	}
	else
	{
		CPacketComposer PacketComposer(S2C_USE_PAUSEITEM_RES);
		PacketComposer.Add(PAUSE_DONT_HAVE_ITEM_FAIL);

		pUser->Send(&PacketComposer);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSRChangeGameMode(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_GAME_MODE_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iOp);
	m_ReceivePacketBuffer.Read(&info.iGameMode);

	if(1 == info.iOp)
	{
		m_ReceivePacketBuffer.Read(&info.iParamIdx);
		m_ReceivePacketBuffer.Read(&info.iParamValue);
	}

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeGameModeReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSRRequestRoomInfoInRoom(CFSGameClient *pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ROOM_INFO_IN_ROOM_REQ, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSRRequestReady(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	pMatch->SendReadyReq(info);

	SG2M_UPDATE_CHANNEL_BUFF_STATUS BuffInfo;
	BuffInfo.btStatus = CHANNELBUFFMANAGER.CheckChannelBuffTime();
	BuffInfo.iGameIDIndex = pUser->GetGameIDIndex();
	pMatch->SendUpdateChannelBuffStatusReq(BuffInfo);
}

void CFSGameThread::Process_FSRRequestToggleIamReady(CFSGameClient* pClient)
{
	CHECK_NULL_POINTER_VOID(pClient);
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*) GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	
	pMatch->SendToggleIamReadyReq(info);
}

// Modify for Match Server
void CFSGameThread::Process_FSRRequestGameStartPause(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	pMatch->SendGameStartPauseReq(info);
}

// Modify for Match Server
void CFSGameThread::Process_FSRChangeGameCourt( CFSGameClient * pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_CHANGE_COURT_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iDir);

	pMatch->SendChangeCourtReq(info);
}

void CFSGameThread::Process_FSRChangePracticeMethod( CFSGameClient * pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_CHANGE_PRACTICE_METHOD_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iPracticeMethod);
	m_ReceivePacketBuffer.Read(&info.iPracticeMethodDetail);

	pMatch->SendChangePracticeMethodReq(info);
}

void CFSGameThread::Process_FSRChangeTeamInGame(CFSGameClient * pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_TEAM_CHANGE_INGAME_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.iPlayerNum);
	for(int i=0; i<info.iPlayerNum && i<MAX_ROOM_MEMBER_NUM; i++)
	{
		m_ReceivePacketBuffer.Read(&info.iRoomUserID[i]);
		m_ReceivePacketBuffer.Read(&info.iHomeAwayIndex[i]);
	}

	CPacketComposer PacketComposer(G2M_TEAM_CHANGE_INGAME_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TEAM_CHANGE_INGAME_REQ));

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_FSRInviteUser(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_INVITE_USER_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read((PBYTE)&info.TargetInfo, sizeof(SC2S_INVITE_USER_REQ));
	info.TargetInfo.szName[MAX_GAMEID_LENGTH] = 0;

	if(0 == strcmp(pUser->GetGameID(), info.TargetInfo.szName)) 
	{
		return;
	}

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		CScopedRefGameUser pTargetUser(info.TargetInfo.szName);
		if ( pTargetUser != NULL )
		{
			if ( U_TRAINING == pTargetUser->GetLocation() ||
				0 < pTargetUser->GetMatchLocation())
			{//��������ڽ��н���ѵ�������ģʽʱ�����ܱ�����
				return;
			}
		}

		pMatch->SendPacket(G2M_INVITE_USER_REQ, &info, sizeof(SG2M_INVITE_USER_REQ));
	}	
}

// Modify for Match Server
void CFSGameThread::Process_FSRSendLoadingProgress(CFSGameClient * pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_LOADING_PROGRESS_SND info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	m_ReceivePacketBuffer.Read(&info.iLoadProgress);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_LOADING_PROGRESS_SND, &info, sizeof(info));	
}

// Modify for Match Server
void CFSGameThread::Process_FSRCheckGamePlayInput(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_SET_PACKET_EVENT, &info, sizeof(SG2M_BASE));
}

// Modify for Match Server
void CFSGameThread::Process_FSLQuickJoinRoom_As_Observer(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = ( CFSGameServer* )pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID( pServer );

	SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info.req, sizeof(SC2S_QUICK_JOIN_GAME_AS_OBSERVER_REQ));

	pUser->GetMatchLoginUserInfo(info, MATCH_TYPE_NONE);

	CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TYPE_COUNT <= info.req.btMatchType);
	CHECK_CONDITION_RETURN_VOID(AVATAR_LV_GRADE_ALL <= info.req.btAvatarLvGrade);

	info.req.btGmUser = static_cast<BYTE>(pUser->CheckGMUser());
	info.btServerIndex = _GetServerIndex;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	WRITE_LOG_NEW(LOG_TYPE_USER, RECV_DATA, NONE, "Observer Data From Client:btObserverType = 11866902, btMatchType = 0, btAvatarLvGrade = 18227200, btMatchTeamScale = -858993460, iTeamID = -858993460", "btGMUser = -858993460", 
.btMatchType, info.req.btAvatarLvGrade, info.req.btMatchTeamScale, info.req.iTeamID , info.req.btGmUser);

	CMatchSvrProxy* pMatch = NULL;
	if(OBSERVER_JOIN_TYPE_FOLLOW == info.req.btObserverType && strlen(info.req.szName) > 0)
	{
		for(BYTE btAvatarLvGrade= AVATAR_LV_GRADE_AMATURE; btAvatarLvGrade < AVATAR_LV_GRADE_ALL; ++btAvatarLvGrade)
		{
			pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_RATING, btAvatarLvGrade);
			if (NULL != pMatch)
			{
				pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
			}
		}

		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_FREE);
		if (NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PRACTICE);
		if (NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE);
		if (NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_AIPVP);
		if(NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_3CENTER);
		if (NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_CHANGE_GRADE);
		if (NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_AFOOTBALL);
		if (NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}

		CLeagueSvrProxy* pLeague = (CLeagueSvrProxy*)GAMEPROXY.GetProxy(FS_LEAGUE_SERVER_PROXY);
		if (NULL != pLeague)
		{
			pLeague->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
	}
	else
	{
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.req.btMatchType, info.req.btAvatarLvGrade);
		if (NULL != pMatch)
		{
			pMatch->SendPacket(G2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ, &info, sizeof(SG2M_QUICK_JOIN_GAME_AS_OBSERVER_REQ));
		}
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSSetObserverRoomID(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_SET_OBSERVER_ROOMID info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iObserverRoomID);	
	m_ReceivePacketBuffer.Read(&info.iObserverRoomEnterKey);	

	pMatch->SendSetObserverRoomID(info);
}

// Modify for Match Server
void CFSGameThread::Process_FSExitObservingGame(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if(NEXUS_OBSERVING == pUser->GetClient()->GetState())
	{
		CScopedRefGameUser user(pUser->GetGameIDIndex());

		user->SendPacketQueue();
		user->GetClient()->SetState(NEXUS_LOBBY);

		CPacketComposer PacketComposer(S2C_EXIT_OBSERVING_GAME_RES);
		PacketComposer.Add(static_cast<BYTE>(OBSERVER_EXIT_END_GAME));

		pUser->Send(&PacketComposer);

		return;
	}

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	pMatch->SendExitObservingGameReq(info);
}

// Modify for Match Server
void CFSGameThread::Process_FSRCheckGameStartPreparationResponse(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_CHECK_GAME_START_PREPARATION_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	m_ReceivePacketBuffer.Read(&info.btUDPSessionType);
	m_ReceivePacketBuffer.Read(&info.dwGameStartSessionKey);
	m_ReceivePacketBuffer.Read(&info.iResult);
	m_ReceivePacketBuffer.Read(&info.iFPS);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_CHECK_GAME_START_PREPARATION_RES, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSRCheckPublicIPAddressForGameResponse(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	m_ReceivePacketBuffer.Read(&info.btUDPSessionType);
	m_ReceivePacketBuffer.Read(&info.dwGameStartSessionKey);
	m_ReceivePacketBuffer.Read(&info.byResult);

	if(-1 != info.byResult && -3 != info.byResult)
	{
		m_ReceivePacketBuffer.Read((BYTE*) info.szMyPublicIP, sizeof(char)*(MAX_IPADDRESS_LENGTH+1));
		m_ReceivePacketBuffer.Read(&info.usMyPublicPort);
		m_ReceivePacketBuffer.Read((BYTE*) info.szMyPrivateIP, sizeof(char)*(MAX_IPADDRESS_LENGTH+1));
		m_ReceivePacketBuffer.Read(&info.usMyPrivatePort);
		
		info.szMyPublicIP[MAX_IPADDRESS_LENGTH] = 0;
		info.szMyPrivateIP[MAX_IPADDRESS_LENGTH] = 0;

		m_ReceivePacketBuffer.Read(&info.dwResponseTime);
		if(0 == info.dwResponseTime)
		{
			info.dwResponseTime = 1;
		}

		m_ReceivePacketBuffer.Read(&info.nSendPacketCnt);
		m_ReceivePacketBuffer.Read(&info.dwThreadAliveTime);
	}
	
	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_RES, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSRStartUDPHolepunchingResponse(CFSGameClient * pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_START_UDP_HOLEPUNCHING_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.btUDPSessionType);
	m_ReceivePacketBuffer.Read(&info.dwGameStartSessionKey);
	m_ReceivePacketBuffer.Read(&info.iMyFPS);
	m_ReceivePacketBuffer.Read(&info.iMemberCnt);

	if ( info.iMemberCnt >= MAX_ROOM_MEMBER_NUM )
	{
		return;
	}
	for(int i=0; i<info.iMemberCnt; i++)
	{
		m_ReceivePacketBuffer.Read((BYTE*)info.szGameID[i], sizeof(char)*(MAX_GAMEID_LENGTH+1));	
		m_ReceivePacketBuffer.Read((BYTE*)info.szMatchedIP[i], sizeof(char)*(MAX_IPADDRESS_LENGTH+1));	
		m_ReceivePacketBuffer.Read(&info.usMatchedPort[i]);
		m_ReceivePacketBuffer.Read(&info.iRecvedReplyCnt[i]);
		m_ReceivePacketBuffer.Read(&info.iSumPingTime[i]);
		m_ReceivePacketBuffer.Read(&info.iSendPingCnt[i]);
		info.szGameID[i][MAX_GAMEID_LENGTH] = 0;
		info.szMatchedIP[i][MAX_IPADDRESS_LENGTH] = 0;
	}

	pMatch->SendStartUdoHolepunchingRes(info);
}

void CFSGameThread::Process_FSRStartBandWidthResponse(CFSGameClient * pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_START_BANDWIDTH_CHECK_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.btUDPSessionType);
	m_ReceivePacketBuffer.Read(&info.dwGameStartSessionKey);
	m_ReceivePacketBuffer.Read(&info.iMemberCnt);

	if ( info.iMemberCnt >= MAX_ROOM_MEMBER_NUM )
	{
		return;
	}

	for(int i=0; i<info.iMemberCnt; i++)
	{
		m_ReceivePacketBuffer.Read((BYTE*)info.szGameID[i], sizeof(char)*(MAX_GAMEID_LENGTH+1));	
		m_ReceivePacketBuffer.Read(&info.iRecvedUpbandwidthBPS[i]);
		info.szGameID[i][MAX_GAMEID_LENGTH] = 0;
	}

	pMatch->SendStartBandWidthRes(info);
}

// Modify for Match Server
void CFSGameThread::Process_FSRReadyHostResponse(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	pUser->GetUserSkill()->CheckSkillSlotExpired();
	pUser->SetAvatarFeatureInfo();


	SG2M_SKILL_COUNT_UPDATE update;
	update.iBonusSkillSlotCount  = pUser->CheckAndGetBonusSkillSlot();
	update.iGameIDIndex = pUser->GetGameIDIndex();
	update.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_SKILL_COUNT_UPDATE, &update, sizeof(update));

	SG2M_READY_HOST_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	m_ReceivePacketBuffer.Read(&info.btUDPSessionType);
	m_ReceivePacketBuffer.Read(&info.dwGameStartSessionKey);
	m_ReceivePacketBuffer.Read(&info.iResult);
	m_ReceivePacketBuffer.Read(&info.usHostServerPort);
	
	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_READY_HOST_RES, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSRConnectHostResponse(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	pUser->GetUserSkill()->CheckSkillSlotExpired();
	pUser->SetAvatarFeatureInfo();

	SG2M_SKILL_COUNT_UPDATE skillUpdate;
	skillUpdate.iBonusSkillSlotCount = pUser->CheckAndGetBonusSkillSlot();
	skillUpdate.iGameIDIndex = pUser->GetGameIDIndex();
	skillUpdate.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_SKILL_COUNT_UPDATE, &skillUpdate, sizeof(skillUpdate));

	SG2M_CONNECT_HOST_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	m_ReceivePacketBuffer.Read(&info.btUDPSessionType);
	m_ReceivePacketBuffer.Read(&info.dwGameStartSessionKey);
	m_ReceivePacketBuffer.Read(&info.iResult);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_CONNECT_HOST_RES, &info, sizeof(info));
}

void CFSGameThread::Process_FSVoiceChattingUserOptionInfoReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SC2S_VOICE_CHATTING_USER_OPTION_INFO_REQ	rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_VOICE_CHATTING_USER_OPTION_INFO_REQ));

	SG2M_VOICE_CHATTING_USER_OPTION_INFO_REQ	ss;
	ss.iGameIDIndex = pUser->GetGameIDIndex();
	ss.btServerIndex = _GetServerIndex;

	ss.eStatus = rs.eStatus;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_VOICE_CHATTING_USER_OPTION_INFO_REQ, &ss, sizeof(ss));
}

// Modify for Match Server
void CFSGameThread::Process_FSTChangeTeamMode(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_TEAM_MODE_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.byTeamMode);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeTeamModeReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::ProcessFSTChangeTeamAllowObserve(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_TEAM_ALLOW_OBSERVE_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.btIsAllowToObserve);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeTeamAllowObserveReq(info);
	}
}

// 20110201 jhwoo �Ϲ��� �뿡�� �뿡 �ִ� ��� �������� �˸�.(��������)

void CFSGameThread::ProcessFSTChangeRoomAllowObserve(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_TEAM_ALLOW_OBSERVE_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read(&info.btIsAllowToObserve);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeTeamAllowObserveReq(info);
	}
}  

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_APPRAISAL_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SC2S_USER_APPRAISAL_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_USER_APPRAISAL_REQ));

	CHECK_CONDITION_RETURN_VOID(info.iGameIDIndex == pUser->GetGameIDIndex());
	if(FALSE == pUser->GetUserAppraisal()->CheckCanAppraisal(info.iGameIDIndex))
	{
		CPacketComposer Packet(S2C_USER_APPRAISAL_RES);
		BYTE btMatchType = static_cast<BYTE>(info.btMatchType);
		Packet.Add((BYTE)RESULT_USER_APPRAISAL_FAIL_ALREADY_APPRAISAL);
		Packet.Add(info.btMatchType);
		Packet.Add((BYTE)0);	// btServerIndex 0 
		Packet.Add((int)0);		// iReqGameIDIndex 0
		pUser->Send(&Packet);
		return;
	}
	if(info.btMatchType == MATCH_TYPE_RANKMATCH)
	{
		CLeagueSvrProxy* pLeague = (CLeagueSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		CHECK_NULL_POINTER_VOID(pLeague);

		SG2L_USER_APPRAISAL_REQ rq;
		strncpy_s(rq.szGameID, sizeof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH+1);
		rq.iGameIDIndex = pUser->GetGameIDIndex();
		rq.btServerIndex = pServer->GetServerIndex();
		rq.btMatchType = info.btMatchType;
		rq.iReqGameIDIndex = info.iGameIDIndex;
		rq.btReqServerIndex = info.btServerIndex;
		rq.btKind = info.btKind;

		CPacketComposer Packet(G2L_USER_APPRAISAL_REQ);
		Packet.Add((BYTE*)&rq, sizeof(SG2L_USER_APPRAISAL_REQ));
		pLeague->Send(&Packet);	
	}
	else
	{
		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		CHECK_NULL_POINTER_VOID(pMatch);

		SG2M_USER_APPRAISAL_REQ rq;
		rq.iGameIDIndex = pUser->GetGameIDIndex();
		rq.btMatchType = info.btMatchType;
		rq.iReqGameIDIndex = info.iGameIDIndex;
		rq.btKind = info.btKind;

		CPacketComposer Packet(G2M_USER_APPRAISAL_REQ);
		Packet.Add((BYTE*)&rq, sizeof(SG2M_USER_APPRAISAL_REQ));
		pMatch->Send(&Packet);	
	}
}
