qinclude "stdafx.h"
qinclude "CFSGameUserBingo.h"
qinclude "CFSGameUser.h"
qinclude "BingoManager.h"
qinclude "RewardManager.h"
qinclude "CFSGameUserItem.h"
qinclude "CBillingManagerGame.h"
qinclude "UserHighFrequencyItem.h"
qinclude "UserMissionShopEvent.h"
qinclude "CoachCardShop.h"
qinclude "ClubConfigManager.h"
qinclude "CFSGameServer.h"
qinclude "UserMissionEvent.h"

CFSGameUserBingo::CFSGameUserBingo(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_dwPickCount(0)
{
}

CFSGameUserBingo::~CFSGameUserBingo(void)
{
}

BOOL CFSGameUserBingo::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == BINGO.IsOpen(), TRUE);

	CHECK_CONDITION_RETURN(TRUE == m_bDataLoaded, TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (FALSE == LoadLine(pODBC))
	{
		return FALSE;
	}

	if (BINGO.GetTotalLineCount() != m_mapBingoLine.size())	//빙고 라인데이터가 잘못되었으면
	{
		if (ODBC_RETURN_SUCCESS != pODBC->BINGO_Initialize(m_pUser->GetGameIDIndex(), BINGO.GetScale()))
		{
			WRITE_LOG_NEW(LOG_TYPE_BINGO, INITIALIZE_DATA, FAIL, "Load-BINGO_Initialize\tGameIDIndex:11866902", m_pUser->GetGameIDIndex());
urn FALSE;
		}
	}

	if (FALSE == LoadLine(pODBC) ||
		FALSE == LoadSlot(pODBC) ||
		FALSE == LoadBoard(pODBC))
	{
		return FALSE;
	}

	m_bDataLoaded = TRUE;
	m_dwPickCount = 0;
	return TRUE;
}

BOOL CFSGameUserBingo::LoadLine(CFSODBCBase* pODBC)
{
	m_mapBingoLine.clear();

	vector<SAvatarBingoLine> vecLine;
	if (ODBC_RETURN_SUCCESS != pODBC->BINGO_GetAvatarBingoLine(m_pUser->GetGameIDIndex(), vecLine))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, INITIALIZE_DATA, FAIL, "BINGO_GetAvatarBingoLine failed");
		return FALSE;
	}

	for (int i = 0; i < vecLine.size(); ++i)
	{
		m_mapBingoLine[vecLine[i].iLineIndex] = vecLine[i];

		if( REWARD_TYPE_COACH_CARD == vecLine[i].btRewardType )
			m_mapBingoLine[vecLine[i].iLineIndex].iItemCode = COACHCARD.GetShopItemCode(vecLine[i].iItemCode);
	}

	return TRUE;
}

BOOL CFSGameUserBingo::LoadSlot(CFSODBCBase* pODBC)
{
	m_mapBingoSlot.clear();

	vector<SAvatarBingoSlot> vecSlot;
	if (ODBC_RETURN_SUCCESS != pODBC->BINGO_GetAvatarBingoSlot(m_pUser->GetGameIDIndex(), vecSlot))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, INITIALIZE_DATA, FAIL, "BINGO_GetAvatarBingoSlot failed");
		return FALSE;
	}

	for (int i = 0; i < vecSlot.size(); ++i)
	{
		m_mapBingoSlot[vecSlot[i].iSlotIndex] = vecSlot[i];
	}

	return TRUE;
}

BOOL CFSGameUserBingo::LoadBoard(CFSODBCBase* pODBC)
{
	if( ODBC_RETURN_SUCCESS != pODBC->BINGO_Initialize_SpecialReward(m_pUser->GetGameIDIndex()))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, INITIALIZE_DATA, FAIL, "BINGO_Initialize_SpecialReward\tGameIDIndex:11866902", m_pUser->GetGameIDIndex()); 
rn FALSE;
	}

	if (ODBC_RETURN_SUCCESS != pODBC->BINGO_GetAvatarBingoBoard(m_pUser->GetGameIDIndex(), m_BingoBoard))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, INITIALIZE_DATA, FAIL, "BINGO_GetAvatarBingoBoard failed");
		return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUserBingo::CheckBingoCompleted()
{
	int iBingoCount = 0;
	AVATAR_BINGO_LINE_MAP::iterator iter = m_mapBingoLine.begin();
	AVATAR_BINGO_LINE_MAP::iterator endIter = m_mapBingoLine.end();
	for (; iter != endIter; ++iter)
	{
		if (TRUE == iter->second.bIsMade)
		{
			++iBingoCount;
		}
	}

	return iBingoCount >= BINGO.GetBingoCountCondition();
}

BOOL CFSGameUserBingo::IsThereBingoLine()
{
	AVATAR_BINGO_LINE_MAP::iterator iter = m_mapBingoLine.begin();
	AVATAR_BINGO_LINE_MAP::iterator endIter = m_mapBingoLine.end();
	for (; iter != endIter; ++iter)
	{
		if (TRUE == iter->second.bIsMade)
		{
			return TRUE;
		}
	}

	return FALSE;
}

RESULT_BINGO_INITIALIZE CFSGameUserBingo::Initialize(BYTE& btCompleteRewardType)
{
	CHECK_CONDITION_RETURN(CLOSED == BINGO.IsOpen(), RESULT_BINGO_INITIALIZE_FAILED);

	CHECK_CONDITION_RETURN(BB_STATUS_COMPLETED != GetBoardStatus(), RESULT_BINGO_INITIALIZE_NOT_COMPLETED);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_BINGO_INITIALIZE_FAILED);

	btCompleteRewardType = m_BingoBoard.iCompleteSuccessCount BINGO_PREMIUM_SUCCESS_COUNT_MAX;


	if (ODBC_RETURN_SUCCESS != pODBC->BINGO_Initialize(m_pUser->GetGameIDIndex(), BINGO.GetScale()))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, INITIALIZE_DATA, FAIL, "Initialize-BINGO_Initialize\tGameIDIndex:11866902", m_pUser->GetGameIDIndex());
rn RESULT_BINGO_INITIALIZE_FAILED;
	}

	if (FALSE == LoadLine(pODBC) ||
		FALSE == LoadSlot(pODBC) ||
		FALSE == LoadBoard(pODBC))
	{
		return RESULT_BINGO_INITIALIZE_FAILED;
	}

	int iCount = m_BingoBoard.iCompleteSuccessCount BINGO_PREMIUM_SUCCESS_COUNT_MAX;

	const int iSuccessCountInitialize = 0;
	btCompleteRewardType = iCount;

	if( m_BingoBoard.iCompleteSuccessCount > 0 && iSuccessCountInitialize == iCount )
	{
		SBingoSpecialReward sReward;
		BINGO.GetBingoPremiumReward(m_BingoBoard.iSpecialRewardIndex, sReward);

		int iMailType = 0;
		vector<int> vecPresentIndex;
		if (ODBC_RETURN_SUCCESS != pODBC->REWARD_GiveItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), sReward.iRewardIndex , iMailType, vecPresentIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_REWARD, DB_DATA_UPDATE, FAIL, "BINGO_INIT - GiveReward_Item - GameIDIndex:11866902\tRewardIndex:0", m_pUser->GetGameIDIndex(), sReward.iRewardIndex);
DBC->BINGO_InsertPremiumReward_Log(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), sReward.iRewardIndex , m_BingoBoard.iCompleteSuccessCount );		
			return RESULT_BINGO_INITIALIZE_FAILED;
		}

		for (int i = 0; i < vecPresentIndex.size(); ++i)
			m_pUser->RecvPresent(iMailType, vecPresentIndex[i]);
	}

	if( ODBC_RETURN_SUCCESS != pODBC->BINGO_Initialize_SpecialReward(m_pUser->GetGameIDIndex()))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, INITIALIZE_DATA, FAIL, "BINGO_Initialize_SpecialReward\tGameIDIndex:11866902", m_pUser->GetGameIDIndex()); 
rn RESULT_BINGO_INITIALIZE_FAILED;
	}

	if(FALSE == LoadBoard(pODBC))
	{
		return RESULT_BINGO_INITIALIZE_FAILED;
	}

	return RESULT_BINGO_INITIALIZE_SUCCESS;
}

BINGO_BOARD_STATUS CFSGameUserBingo::GetBoardStatus()
{
	if (TRUE == CheckBingoCompleted())
	{
		return BB_STATUS_COMPLETED;
	}

	if (BINGO.GetMaxShuffleNumber() <= m_BingoBoard.iShuffledCount)
		return BB_STATUS_SHUFFLE_OFF;

	return BB_STATUS_SHUFFLE_ON;
}

void CFSGameUserBingo::SendBingoSlotList()
{
	CPacketComposer Packet(S2C_BINGO_BOARD_SLOT_LIST_RES);

	SS2C_BINGO_BOARD_SLOT_LIST_RES info;
	info.btBoardStatus = (BYTE)GetBoardStatus();
	info.btSlotCount = m_mapBingoSlot.size();

	Packet.Add((PBYTE)&info, sizeof(SS2C_BINGO_BOARD_SLOT_LIST_RES));

	SBINGO_SLOT_INFO SlotInfo;
	AVATAR_BINGO_SLOT_MAP::iterator iter = m_mapBingoSlot.begin();
	AVATAR_BINGO_SLOT_MAP::iterator endIter = m_mapBingoSlot.end();
	for (; iter != endIter; ++iter)
	{
		SlotInfo.btNumber = iter->second.iNumber;
		SlotInfo.btIsMark = iter->second.bMark;

		Packet.Add((PBYTE)&SlotInfo, sizeof(SBINGO_SLOT_INFO));
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserBingo::SendBingoLineList()
{
	CPacketComposer Packet(S2C_BINGO_BOARD_LINE_LIST_RES);

	SS2C_BINGO_BOARD_LINE_LIST_RES info;
	info.btLineCount = m_mapBingoLine.size();

	Packet.Add((PBYTE)&info, sizeof(SS2C_BINGO_BOARD_LINE_LIST_RES));

	SBINGO_LINE_INFO LineInfo;
	AVATAR_BINGO_LINE_MAP::iterator iter = m_mapBingoLine.begin();
	AVATAR_BINGO_LINE_MAP::iterator endIter = m_mapBingoLine.end();
	for (; iter != endIter; ++iter)
	{
		LineInfo.btLineIndex = iter->second.iLineIndex;
		LineInfo.btIsMade = iter->second.bIsMade;
		LineInfo.btRewardType = iter->second.btRewardType;
		LineInfo.iReward = iter->second.iItemCode; // 현재 ItemCode 만 사용함.
		LineInfo.iCount = iter->second.iValue;

		if( REWARD_TYPE_COACH_CARD == LineInfo.btRewardType )
			LineInfo.btPropertyType = ITEM_PROPERTY_EXHAUST;
		else
			LineInfo.btPropertyType = (BYTE)(iter->second.iPropertyType);

		Packet.Add((PBYTE)&LineInfo, sizeof(SBINGO_LINE_INFO));
	}

	m_pUser->Send(&Packet);
}

RESULT_USE_BINGO_TATOO CFSGameUserBingo::PickNumber(BYTE& btNumber, int& iUpdatedTatooCount, int& iMarkedSlotIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == BINGO.IsOpen(), RESULT_USE_BINGO_TATOO_FAILED);

	CHECK_CONDITION_RETURN(TRUE == CheckBingoCompleted(), RESULT_USE_BINGO_TATOO_FAILED);

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem( ITEM_PROPERTY_KIND_BINGO_TATOO );
	CHECK_CONDITION_RETURN(NULL == pItem, RESULT_USE_BINGO_TATOO_NOT_ENOUGH_ITEM);
	CHECK_CONDITION_RETURN(0 >= pItem->iPropertyTypeValue, RESULT_USE_BINGO_TATOO_NOT_ENOUGH_ITEM);
	
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_USE_BINGO_TATOO_FAILED);
	
	if ( m_pUser->GetPresentList( MAIL_PRESENT_ON_AVATAR )->GetSize() >= MAX_PRESENT_LIST)
	{
		return RESULT_USE_BINGO_TATOO_PRESENT_LIST_FULL;
	}
	int iRetValue = RESULT_USE_BINGO_TATOO_SUCCESS;
	iRetValue = pODBC->ITEM_UseHighFrequencyItem( m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), pItem->iPropertyKind, 1, iUpdatedTatooCount );
	if ( ODBC_RETURN_SUCCESS != iRetValue )
	{
		return (RESULT_USE_BINGO_TATOO)iRetValue;
	}

	if (0 >= iUpdatedTatooCount)
	{
		m_pUser->GetUserHighFrequencyItem()->DeleteUserHighFrequencyItem( pItem->iPropertyKind );
		pItem = NULL;
	}
	else
	{
		pItem->iPropertyTypeValue = iUpdatedTatooCount;
	}

	m_pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BINGO_SHUFFLE);

	m_pUser->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_BINGO_NUMBER_TRY);
	m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_BINGO_TATOO);
	m_pUser->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_USE_BINGO_TATOO);
	m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_BINGO_TATOO);
	m_pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_BINGO_TATOO);
	m_pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_BINGO_TRY_COUNT);

	btNumber = BINGO.GetRandNumber();

	SAvatarBingoSlot* pSlot = GetSlotWithNumber(btNumber);
	CHECK_CONDITION_RETURN(NULL == pSlot, RESULT_USE_BINGO_TATOO_SUCCESS);
	CHECK_CONDITION_RETURN(TRUE == pSlot->bMark, RESULT_USE_BINGO_TATOO_SUCCESS_DUPLICATE_NUMBER);

	iMarkedSlotIndex = pSlot->iSlotIndex;

	if (ODBC_RETURN_SUCCESS != pODBC->BINGO_MarkNumber(m_pUser->GetGameIDIndex(), iMarkedSlotIndex, btNumber))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, DB_DATA_UPDATE, FAIL, "BINGO_MarkNumber");
		return RESULT_USE_BINGO_TATOO_FAILED;
	}

	pSlot->bMark = TRUE;

	m_pUser->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_BINGO_NUMBER_GET);

	return RESULT_USE_BINGO_TATOO_SUCCESS;
}

SAvatarBingoSlot* CFSGameUserBingo::GetSlotWithNumber(int iNumber)
{
	AVATAR_BINGO_SLOT_MAP::iterator iter = m_mapBingoSlot.begin();
	AVATAR_BINGO_SLOT_MAP::iterator endIter = m_mapBingoSlot.end();
	for (; iter != endIter; ++iter)
	{
		if (iter->second.iNumber == iNumber)
		{
			return &iter->second;
		}
	}

	return NULL;
}

SAvatarBingoLine* CFSGameUserBingo::GetLine(int iLineIndex)
{
	AVATAR_BINGO_LINE_MAP::iterator iter = m_mapBingoLine.find(iLineIndex);
	CHECK_CONDITION_RETURN(m_mapBingoLine.end() == iter, NULL);

	return &iter->second;
}

SAvatarBingoSlot* CFSGameUserBingo::GetSlot(int iSlotIndex)
{
	AVATAR_BINGO_SLOT_MAP::iterator iter = m_mapBingoSlot.find(iSlotIndex);
	CHECK_CONDITION_RETURN(m_mapBingoSlot.end() == iter, NULL);

	return &iter->second;
}

void CFSGameUserBingo::CheckLineMade(int iMarkedSlotIndex/* = -1*/)
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	BINGO_LINE_VECTOR vecLine;

	if (-1 == iMarkedSlotIndex)
	{
		for (int i = 0; i < BINGO.GetTotalLineCount(); ++i)
			vecLine.push_back(i);
	}
	else
	{
		BINGO.GetLineIndex(iMarkedSlotIndex, vecLine);
	}

	const int iBingoScale = BINGO.GetScale();
	for (int i = 0; i < vecLine.size(); ++i)
	{
		int iLineIndex = vecLine[i];
		SAvatarBingoLine* pLine = GetLine(iLineIndex);
		if (NULL == pLine) continue;

		if (TRUE == pLine->bIsMade) continue;

		int* iaSlotList = BINGO.GetSlotIndexList(iLineIndex);
		if (NULL == iaSlotList) continue;

		BOOL bMade = TRUE;
		for (int j = 0; j < iBingoScale; ++j)
		{
			int iCheckSlotIndex = iaSlotList[j];
			SAvatarBingoSlot* pCheckSlot = GetSlot(iCheckSlotIndex);
			if (NULL == pCheckSlot ||
				FALSE == pCheckSlot->bMark)  
			{
				bMade = FALSE;
				break;
			}
		}

		if (TRUE == bMade)
		{
			int iOutPresentIndex = 0;
			if( ODBC_RETURN_SUCCESS != pODBC->BINGO_LineMadeAndGiveReward(m_pUser->GetGameIDIndex(), pLine->iLineIndex, iOutPresentIndex))
			{
				if ( m_pUser->GetPresentList( MAIL_PRESENT_ON_AVATAR )->GetSize() >= MAX_PRESENT_LIST 
					|| m_pUser->GetPresentList( MAIL_PRESENT_ON_ACCOUNT )->GetSize() >= MAX_PRESENT_LIST )
				{
					SS2C_USE_BINGO_TATOO_RES rs;
					rs.btResult = RESULT_USE_BINGO_TATOO_PRESENT_LIST_FULL;

					m_pUser->Send(S2C_USE_BINGO_TATOO_RES, &rs, sizeof(SS2C_USE_BINGO_TATOO_RES));
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_BINGO, DB_DATA_UPDATE, FAIL, "BINGO_LineMadeAndGiveReward:11866902", m_pUser->GetGameIDIndex());

		}
			else
			{
				if( -1 < iOutPresentIndex )
					m_pUser->RecvPresent( pLine->btMailType, iOutPresentIndex );

				pLine->bIsMade = TRUE;

				SS2C_BINGO_LINE_MADE_NOT info;
				info.btLineIndex = pLine->iLineIndex;
				m_pUser->Send(S2C_BINGO_LINE_MADE_NOT, &info, sizeof(SS2C_BINGO_LINE_MADE_NOT));
			}
		}
	}

	if (TRUE == CheckBingoCompleted())
	{
		m_pUser->Send(S2C_BINGO_COMPLETED_NOT, NULL, 0);
	}
}

RESULT_BINGO_SHUFFLE CFSGameUserBingo::Shuffle()
{
	CHECK_CONDITION_RETURN(CLOSED == BINGO.IsOpen(), RESULT_BINGO_SHUFFLE_FAILED);
	CHECK_CONDITION_RETURN(TRUE == CheckBingoCompleted(), RESULT_BINGO_SHUFFLE_FAILED);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_BINGO_SHUFFLE_FAILED);

	int iCost = BINGO.GetShuffleCashCost(m_BingoBoard.iShuffledCount + 1);

	CHECK_CONDITION_RETURN(-1 == iCost, RESULT_BINGO_SHUFFLE_OVER_SHUFFLING_COUNT);
	CHECK_CONDITION_RETURN(iCost > m_pUser->GetCoin() + m_pUser->GetEventCoin(), RESULT_BINGO_SHUFFLE_NOT_ENOUGH_CASH);

	if ( iCost > 0)
	{
		SBillingInfo BillingInfo;
		BillingInfo.iEventCode = EVENT_BINGO_SHUFFLE_SLOT;
		m_pUser->SetBillingInfo(BillingInfo, SELL_TYPE_CASH, iCost);
		strcpy_s(BillingInfo.szItemName, "Bingo_shuffleSlot");

		if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
		{
			return RESULT_BINGO_SHUFFLE_FAILED;
		}
	}
	else
	{
		int iUpdatedShuffleCount;
		if (ODBC_RETURN_SUCCESS != pODBC->BINGO_ShuffleSlot(m_pUser->GetGameIDIndex(), iUpdatedShuffleCount))
		{
			WRITE_LOG_NEW(LOG_TYPE_BINGO, DB_DATA_UPDATE, FAIL, "BINGO_ShuffleSlot:11866902", m_pUser->GetGameIDIndex());
urn RESULT_BINGO_SHUFFLE_FAILED;
		}
		else
		{
			SS2C_BINGO_SHUFFLE_RES rs;
			rs.btResult = RESULT_BINGO_SHUFFLE_FAILED;
			if (TRUE == LoadSlot(pODBC))
			{
				rs.btResult = RESULT_BINGO_SHUFFLE_SUCCESS;

				m_BingoBoard.iShuffledCount = iUpdatedShuffleCount;
			}

			if (RESULT_BINGO_SHUFFLE_SUCCESS == rs.btResult)
			{
				SendBingoSlotList();
				SendBingoLineList();

				CheckLineMade();
			}

			m_pUser->Send(S2C_BINGO_SHUFFLE_RES, &rs, sizeof(SS2C_BINGO_SHUFFLE_RES));
		}
	}

	return RESULT_BINGO_SHUFFLE_SUCCESS;
}

BOOL CFSGameUserBingo::ShuffleSlot_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, INVALED_DATA, CHECK_FAIL, "ShuffleSlot_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	SS2C_BINGO_SHUFFLE_RES rs;
	rs.btResult = RESULT_BINGO_SHUFFLE_FAILED;

	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
			CHECK_NULL_POINTER_BOOL(pODBC);

			int iUpdatedShuffleCount;
			if (ODBC_RETURN_SUCCESS != pODBC->BINGO_ShuffleSlot(m_pUser->GetGameIDIndex(), iUpdatedShuffleCount))
			{
				WRITE_LOG_NEW(LOG_TYPE_BINGO, DB_DATA_UPDATE, FAIL, "BINGO_ShuffleSlot:11866902", m_pUser->GetGameIDIndex());
			else
			{
				if (TRUE == LoadSlot(pODBC))
				{
					rs.btResult = RESULT_BINGO_SHUFFLE_SUCCESS;

					m_BingoBoard.iShuffledCount = iUpdatedShuffleCount;

					m_pUser->SetUserBillResultAtMem(pBillingInfo->iSellType, pBillingInfo->iCurrentCash - pBillingInfo->iCashChange, 0, 0, pBillingInfo->iBonusCoin);
					m_pUser->SendUserGold();
				}
			}
		}
		break;
	default:
		break;
	}
	
	if (RESULT_BINGO_SHUFFLE_SUCCESS == rs.btResult)
	{
		SendBingoSlotList();
		SendBingoLineList();

		CheckLineMade();
	}

	//원래 뒤섞기 응답 시점은 뒤섞인 후의 정보보내기 전에 보내야 맞지만 클라이언트 요구로 마지막에 보냄
	m_pUser->Send(S2C_BINGO_SHUFFLE_RES, &rs, sizeof(SS2C_BINGO_SHUFFLE_RES));

	return (RESULT_BINGO_SHUFFLE_SUCCESS == rs.btResult);	
}

RESULT_BINGO_INITIALIZE_REWARD CFSGameUserBingo::InitializeReward()
{
	CHECK_CONDITION_RETURN(CLOSED == BINGO.IsOpen(), RESULT_BINGO_INITIALIZE_REWARD_FAIL);
	CHECK_CONDITION_RETURN(TRUE == IsThereBingoLine(), RESULT_BINGO_INITIALIZE_REWARD_ALREADY_LINE_MADE);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN( NULL == pODBC, RESULT_BINGO_INITIALIZE_REWARD_FAIL);

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem( ITEM_PROPERTY_KIND_BINGO_TATOO );
	CHECK_CONDITION_RETURN( NULL == pItem, RESULT_BINGO_INITIALIZE_REWARD_NOT_ENOUGHT_CASH);

	int iCost = BINGO.GetInitRewardCost();
	CHECK_CONDITION_RETURN( iCost > pItem->iPropertyTypeValue , RESULT_BINGO_INITIALIZE_REWARD_NOT_ENOUGHT_CASH);

	SS2C_BINGO_INITIALIZE_REWARD_RES rs;
	rs.btResult = RESULT_BINGO_INITIALIZE_REWARD_FAIL;

	int iUpdatedTatooCount = 0;
	if (ODBC_RETURN_SUCCESS != pODBC->BINGO_InitializeReward(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), BINGO.GetScale(), pItem->iPropertyKind, iCost, iUpdatedTatooCount ))
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, DB_DATA_UPDATE, FAIL, "BINGO_InitializeReward:11866902", m_pUser->GetGameIDIndex());
lse
	{
		if (TRUE == LoadLine(pODBC))
		{
			rs.btResult = RESULT_BINGO_INITIALIZE_REWARD_SUCCESS;

			if (0 >= iUpdatedTatooCount)
			{
				m_pUser->GetUserHighFrequencyItem()->DeleteUserHighFrequencyItem( pItem->iPropertyKind );
				pItem = NULL;
			}
			else
			{
				pItem->iPropertyTypeValue = iUpdatedTatooCount;
			}

			m_pUser->SendHighFrequencyItemCount(ITEM_PROPERTY_KIND_BINGO_TATOO);
		}
	}

	m_pUser->Send(S2C_BINGO_INITIALIZE_REWARD_RES, &rs, sizeof(SS2C_BINGO_INITIALIZE_REWARD_RES));

	if (RESULT_BINGO_INITIALIZE_REWARD_SUCCESS == rs.btResult)
	{
		SendBingoLineList();
	}

	return RESULT_BINGO_INITIALIZE_REWARD_SUCCESS;
}

void CFSGameUserBingo::SendBingoPremiumRewardInfo()
{
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SBingoSpecialReward sReward;
	BINGO.GetBingoPremiumReward(m_BingoBoard.iSpecialRewardIndex, sReward);

	SS2C_BINGO_PREMIUM_REWARD_RES	info;
	info.btPremiumRewardType = sReward.iRewardType;
	info.iPremiumRewardItemCode = sReward.iItemCode;

	SShopItemInfo sShopItemInfo;
	if( TRUE == pItemShop->GetItem(sReward.iItemCode, sShopItemInfo))
		info.iPropertyType = sShopItemInfo.iPropertyType;
	else
		info.iPropertyType = ITEM_PROPERTY_EXHAUST;
	
	info.iValue = sReward.iValue;
	
	m_pUser->Send(S2C_BINGO_PREMIUM_REWARD_RES, &info, sizeof(SS2C_BINGO_PREMIUM_REWARD_RES));
}

void CFSGameUserBingo::SendBingoBoardInfo()
{
	SS2C_BINGO_BOARD_RES	info;
	info.iContinuousSpeed = BINGO.GetContinuousSpeed();
	info.btCompleteSuccessCount = m_BingoBoard.iCompleteSuccessCount BINGO_PREMIUM_SUCCESS_COUNT_MAX;


	m_pUser->Send(S2C_BINGO_BOARD_RES, &info, sizeof(SS2C_BINGO_BOARD_RES));
}

void CFSGameUserBingo::SendBingoRewardList()
{
	CPacketComposer	Packet(S2C_BINGO_REWARD_LIST_RES);

	if( TRUE == BINGO.MakePacketBingoRewardItemList(Packet))
	{
		m_pUser->Send(&Packet);
	}
}

void CFSGameUserBingo::SendBingoMainRewardList()
{
	CPacketComposer	Packet(S2C_BINGO_MAIN_REWARD_LIST_RES);

	if( TRUE == BINGO.MakePacketBingoMainRewardItemList(Packet))
	{
		m_pUser->Send(&Packet);
	}
}

BOOL CFSGameUserBingo::CheckContinuousCount()
{
	CHECK_CONDITION_RETURN( (m_dwPickCount + MIN_CONTINUOUS_SPEED) > GetTickCount() , TRUE );

	m_dwPickCount = GetTickCount();

	return FALSE;
}

void CFSGameUserBingo::SendBingoTokenInfo()
{
	SS2C_BINGO_TOKEN_INFO_RES	rs;

	SBingoSpecialReward sReward;
	CHECK_CONDITION_RETURN_VOID( FALSE == BINGO.GetBingoPremiumReward(m_BingoBoard.iSpecialRewardIndex, sReward));

	const ClubTournamentTokenCountConfigVector* pVecItem = CLUBCONFIGMANAGER.GetTokenConfigVector(sReward.iItemCode);
	CHECK_NULL_POINTER_VOID(pVecItem);

	CPacketComposer	Packet(S2C_BINGO_TOKEN_INFO_RES);

	rs.iTokenItemCode = sReward.iItemCode;
	rs.btCount = (BYTE)pVecItem->size();

	Packet.Add((PBYTE)&rs, sizeof( SS2C_BINGO_TOKEN_INFO_RES));

	for( size_t iIndex = 0 ; iIndex < pVecItem->size() ; ++iIndex )
	{
		SBINGO_TOKEN_INFO info;

		info.iItemCode = (*pVecItem)[iIndex].iExchangeItemCode;
		info.iPropertyType = (*pVecItem)[iIndex].iPropertyType;
		info.iTokenCount = (*pVecItem)[iIndex].iTokenCount;
		info.iValue = (*pVecItem)[iIndex].iValue;

		Packet.Add((PBYTE)&info, sizeof( SBINGO_TOKEN_INFO ));
	}

	m_pUser->Send(&Packet);
}
