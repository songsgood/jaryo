qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserRainbowWeekEvent
{
public:
	CFSGameUserRainbowWeekEvent(CFSGameUser* pUser);
	~CFSGameUserRainbowWeekEvent(void);

	BOOL				Load();
	void				SendEventInfo();
	RESULT_EVENT_RAINBOW_GET_REWARD GetRewardReq(int iRewardIndex);
	BYTE				GetCurrentRewardStatus() { return m_btCurrentRewardStatus; }

protected:
	BOOL				CheckUpdateTime();
	void				UpdateUserEventStatus();
	int					GetCurrentEventWeek();

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	int m_iRecvRewardIndex;
	int m_iUpdateDate;
	int m_iUpdateTime;
	BYTE m_btCurrentRewardStatus;
};

