qpragma once

qinclude "CFSODBCBase.h"

class CFSGameUser;
class CFSODBCBase;

enum EVENT_TRANSFERJOYCITY_SHOP_LOG_GET_COIN_TYPE
{
	EVENT_TRANSFERJOYCITY_SHOP_LOG_GET_COIN_TYPE_LOGIN,
	EVENT_TRANSFERJOYCITY_SHOP_LOG_GET_COIN_TYPE_END_GAME,
};

class CFSGameUserTransferJoycityShopEvent
{
public:
	CFSGameUserTransferJoycityShopEvent(CFSGameUser* pUser);
	~CFSGameUserTransferJoycityShopEvent(void);

	BOOL	Load();

	void SendEventInfo();
	void SendEventShopItemInfo();
	RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD GetRewardReq(int iSlotIndex);
	RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN GetDailyCoinReq();
	BOOL CheckGameEndGiveCoin();

protected:
	void UpdateStatus();

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

 	BOOL m_bIsGetDailyCoin;
	time_t m_tGetCoinTime; 
	int m_iTodayPlayCnt;
	time_t m_tLastPlayTime;
};

