qpragma once

class CFSODBCBase;
class CFSGameUser;
class CFSGameUserCovet
{
public:
	CFSGameUserCovet(CFSGameUser* pUser);
	~CFSGameUserCovet(void);

public:

	BOOL LoadCovet();
	void SendCovetUserInfo();
	void SendCovetItemList();
	void SendCovetItemSelect(const SC2S_EVENT_COVET_ITEM_SELECT_REQ& rq);
	void SendCovetShop();
	void BuyCovetShopItem(const SC2S_EVENT_COVET_SHOP_BUY_REQ& rq);
	void UseCovet(const SC2S_EVENT_COVET_USE_REQ& rq);
	void AddCovetJinn();
	void CheckAndSaveCovetJinn(CFSODBCBase* pODBC);
	BOOL BuySummonStone_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	void SetSummonStoneCount(int iUpdateCount);

private:
	CFSGameUser* m_pUser;
	SEVENTCOVET_UserInfo m_sUserInfo;

	list<int>		m_listSelectReward[MAX_EVENT_COVET_REWARD_GRADE];
	BOOL	m_bDBSave;
	BOOL	m_bDBSave_RewardList;
	BYTE	m_btIsFirstTime;
};

