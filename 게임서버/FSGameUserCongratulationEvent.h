qpragma once

class CFSGameUser;
class CFSGameUserCongratulationEvent
{
public:
	CFSGameUserCongratulationEvent(CFSGameUser* pUser);
	~CFSGameUserCongratulationEvent(void);

	BOOL					Load();
	BOOL					LoadBalloonGiveCnt();
	BOOL					CheckResetDate(time_t tCurrentTime = _time64(NULL), BOOL bIsLogin = FALSE);
	void					SendEventInfo(BYTE btEventSubKind);
	void					SendUserInfo(BYTE btEventSubKind);
	void					GiveBalloon(BYTE btProperty);
	void					OpenBox(BYTE btMissionIndex);
	void					BuyMultipleGift(BYTE btGiftIndex);
	void					UseTotalExp(char* szGameID);
	BOOL					CheckAndUpdateMission(const BYTE btMissionConditionType, const int iGiveExp, const time_t tCurrentTime = _time64(NULL));
	BOOL					CheckGamePlayMission(BOOL bIsDisconnectedMatch, BOOL bRunAway, time_t tGameStartTime, int iBaseExp);
	BOOL					SaveStartConnectTime();
	int						GetBalloonGiveCnt();
	void					CheckAndSendBalloonMissionNoClearNot();
	BYTE					GetExpGamePlayCnt();

private:
	CFSGameUser*			m_pUser;
	BOOL					m_bDataLoaded;

	time_t					m_tLastResetTime;
	time_t					m_tStartConnectTime;
	int						m_iConnectSecond;

	BYTE					m_btBalloonStatus;
	int						m_iBalloonGiveCnt;
	int						m_iMissionValue[CongratulationEvent::MAX_CNT_MISSION_CONDITION_TYPE];
	int						m_iMissionBoxCnt[CongratulationEvent::BALLOON_MISSION_CNT];
	BYTE					m_btExpGamePlayCnt;
	BYTE					m_btExpGamePlayCnt_Send;
	int						m_iMyMultipleCoin;
	int						m_iTodayGetCoin;
	int						m_iTotalExp;
};

