qpragma once
qinclude "TUser.h"
qinclude "CFSAvatarManager.h"
qinclude "BaseMailList.h"
qinclude "FriendList.h"
qinclude "FSMatchCommon.h"
qinclude "CCompEventComponentFunction.h"
qinclude "FSClubCommon.h"
qinclude "ThreadODBCManager.h"
qinclude "JoinTeamConfigInfo.h"
qinclude "CheckSum.h"
qinclude "CFSGameUserBingo.h"
qinclude "UserRecordBoard.h"
qinclude "ProductInventory.h"
qinclude "CFSGameUserIntensivePractice.h"
qinclude "CFSGameUserLuckyBox.h"
qinclude "UserWordPuzzles.h"
qinclude "CFSGameUserRandomBox.h"
qinclude "CFSGameUserThreeKingdomsEvent.h"
qinclude "UserMissionShopEvent.h"
qinclude "FSGameUserSpecialSkin.h"
qinclude "FSGameUserHoneyWalletEvent.h"
qinclude "CFSGameUserCharacterCollection.h"
qinclude "FSGameUserHotGirlSpecialBoxEvent.h"
qinclude "FSGameUserSkyLuckyEvent.h"
qinclude "CFSGameUserCheerLeader.h"
qinclude "FSGameUserGoldenSafeEvent.h"
qinclude "FSGameUserShoppingEvent.h"
qinclude "FSGameUserLegendEvent.h"
qinclude "FSGameUserRandomArrowEvent.h"
qinclude "FSGameUserDevilTemtationEvent.h"
qinclude "FSGameUserVendingMachineEvent.h"
qinclude "FSGameUserMatchingCard.h"
qinclude "FSGameUserBasketBall.h"
qinclude "FSGameUserRandomItemBoardEvent.h"
qinclude "FSGameUserGamePlayEvent.h"
qinclude "FSGameUserRandomItemTreeEvent.h"
qinclude "FSGameUserLoginPayBackEvent.h"
qinclude "GameUserPurchaseGrade.h"
qinclude "FSGameUserRandomItemGroupEvent.h"
qinclude "FSGameUserRainbowWeekEvent.h"
qinclude "FSGameUserGameOfDiceEvent.h"
qinclude "FSGameUserRankMatch.h"
qinclude "FSGameUserPasswordEvent.h"
qinclude "FSGameUserHalloweenGamePlayEvent.h"
qinclude "FSGameUserLottoEvent.h"
qinclude "FSGameUserPresentFromDeveloperEvent.h"
qinclude "FSGameUserDiscountItemEvent.h"
qinclude "FSGameUserMiniGameZoneEvent.h"
qinclude "FSGameUserBigWheelLoginEvent.h"
qinclude "FSGameUserSalePlusEvent.h"
qinclude "FSGameUserDriveMissionEvent.h"
qinclude "FSGameUserMissionMakeItemEvent.h"
qinclude "FSGameUserMissionCashLimitEvent.h"
qinclude "FSGameUserDarkMarketEvent.h"
qinclude "FSGameUserColoringPlayEvent.h"
qinclude "FSGameUserCongratulationEvent.h"
qinclude "FSGameUserHelloNewYearEvent.h"
qinclude "FSGameUserOpenBoxEvent.h"
qinclude "FSGameUserNewbieBenefit.h"
qinclude "FSGameUserFriendInviteList.h"
qinclude "FSGameUserPrimonGoEvent.h"
qinclude "FSGameUserPrivateRoomEvent.h"
qinclude "FSGameUserHippocampusEvent.h"
qinclude "FSGameUserComebackBenefit.h"
qinclude "FSGameUserPowerupCapsule.h"
qinclude "FSGameUserUnlimitedTatooEvent.h"
qinclude "FSGameUserPVEMission.h"
qinclude "FSGameUserCovet.h"
qinclude "FSGameUserShoppingGodEvent.h"
qinclude "FSGameUserRandomItemDrawEvent.h"
qinclude "FSGameUserSelectClothesEvent.h"
qinclude "FSGameUserSubCharDevelop.h"
qinclude "FSGameUserSummerCandyEvent.h"
qinclude "FSGameUserSecretShopEvent.h"
qinclude "FSGameUserTransferJoycityPackageEvent.h"
qinclude "FSGameUserTransferJoycityShopEvent.h"
qinclude "FSGameUserTransferJoycity100DreamEvent.h"

class CFSGameUserItem;
class CFSGameUserSkill;
class CFSSkillShop;

class CAvatarCreateManager;
class CFSClubBaseODBC;
class CLobbyChatChannel;
class CCustomizeManager;

struct SRoomListUserSet
{
	int		_iMatchSvrID;
	BYTE	_btMatchTeamScale;	
	BYTE	_btListMode;
	int		_iPage;
	int		_iaShowTeamID[DEFAULT_TEAMNUM_PER_PAGE];
	int		_iSendCount;

	SRoomListUserSet()
	{
		ZeroMemory(this, sizeof(SRoomListUserSet));
	}
};

#define  CLUB_LOBBY_TEAM_LIST	0
#define  CLUB_LOBBY_USER_LIST	1
#define  CLUB_LOBBY_MEMBER_LIST	2
#define  CLUB_LOBBY_RESULT_LIST	3

// CFSGameUser접근용 ForEach
#define FOR_EACH_USER(PtrCurItem,PtrTypeList)	FOR_EACH(CFSGameUser*,PtrCurItem,PtrTypeList)

#define CHECK_SUM_INIT()	unsigned char tempChkBuf[256]; \
	unsigned char *pSendChkBuf = tempChkBuf;	m_CheckSum.Clear(); \

#define ADD_CHECK_SUM_VALUE(X) memset(pSendChkBuf, 0, sizeof(pSendChkBuf)); \
	memcpy(pSendChkBuf, &(X), sizeof(X)); m_CheckSum.Add(pSendChkBuf,sizeof(X)); \

class CFSGameUser : public CUser, public CCompEventComponentFunction
{
public:
	CFSGameUser();
	virtual ~CFSGameUser();

	BOOL			CheckGMUser()						{ return m_bGMUser; }
	void			SetGMUser(BOOL bGMUser)				{ m_bGMUser = bGMUser; }
	BOOL			CheckUseableBonusCoinUser()			{ return m_bUseableBonusCoin; }
	void			SetUseableBonusCoinUser(BOOL bUseableBonusCoin)	{ m_bUseableBonusCoin = bUseableBonusCoin; }
	BOOL			CheckCheatAble();
	void			SetCheatAble(BOOL bAble);
	int				GetLocation()						{ return m_iLocation; }
	void			SetLocation(int n);
	int				GetRefreshRoomNum()					{return m_iRefreshRoomNum; }
	void			SetRefreshRoomNum(int iRoomIdx)		{ m_iRefreshRoomNum = iRoomIdx; }
	BOOL			IsHackUser()						{ return m_bHackUser; }
	void			SetHackUser(BOOL bHackUser)			{ m_bHackUser = bHackUser; }
	int				GetCheckStatPacketNo()				{ return m_iCheckStatPacketNo; }
	void			SetCheckStatPacketNo(int iPacketNo)	{ m_iCheckStatPacketNo = iPacketNo; }
	int				GetCheckStatCheckSumPacketNo()		{ return m_iCheckStatCheckSumPacketNo; }
	void			SetCheckStatCheckSumPacketNo(int iPacketNo)	{ m_iCheckStatCheckSumPacketNo = iPacketNo;	}
	int				GetCheckStatLastSendPacketNo()		{ return m_iCheckStatLastSendPacketNo; }
	void			SetCheckStatLastSendPacketNo(int iPacketNo)	{ m_iCheckStatLastSendPacketNo = iPacketNo; }

	virtual	CUser*	GetEventUser()						{ return this;}
	virtual int		GetEventGameIDIndex()				{ return GetGameIDIndex(); }
	virtual int		GetEventUserIDIndex()				{ return GetUserIDIndex(); }

	SGameRecordLog*				GetConnectGameRecord()	{ return &m_GameRecordLog; } // 접속 후의 기록
	CFSGameUserItem*			GetUserItemList()		{ return m_UserItem; }
	CFSGameUserSkill*			GetUserSkill()			{ return m_UserSkill; }
	CBaseMailList<SPaper>*		GetPaperList()			{ return &m_paperList; }
	virtual CBaseMailList<SPresent>*	GetPresentList(int iMailType = MAIL_PRESENT_ON_AVATAR)	{ if (MAIL_PRESENT_ON_ACCOUNT == iMailType) return &m_presentListOnAccount; return &m_presentList; }
	CBaseMailList<SClubInvitationData>* GetClubInvitationList() { return &m_ClubInvitationList; }

	CFriendList*				GetFriendList()			{ return &m_FriendList; }
	CUserAction*				GetUserAction()			{ return m_pUserAction; }
	CFriendAccountList*			GetFriendAccountList()	{ return &m_FriendAccountList; }
	int							GetFriendAccountCnt()	{ return m_FriendAccountList.GetSize(); }

	int				GetItemTypeLevelUpCount()							{ return m_iItemTypeLevelUpCount; }
	void			SetItemTypeLevelUpCount(int iItemTypeLevelUpCount)	{ m_iItemTypeLevelUpCount = iItemTypeLevelUpCount; }

	BOOL			IsClubMember()						{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->bClubMember : FALSE; }
	int				GetClubSN()							{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->iClubSN : 0; }
	void			SetClubSN(int iClubSN)				{ if(GetCurUsedAvatar()) GetCurUsedAvatar()->iClubSN = iClubSN; }
	char* 			GetClubName()						{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->szClubName : ""; }
	void			SetClubName(char* szClubName);
	int				GetClubMarkCode()					{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->iClubMarkCode : 0; }
	void			SetClubMarkCode(int iClubMarkCode) { if(GetCurUsedAvatar()) GetCurUsedAvatar()->iClubMarkCode = iClubMarkCode; }
	BYTE			GetClubGrade()						{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->btClubGrade : 0; }
	void			SetClubGrade( BYTE btClubGrade )	{ if(GetCurUsedAvatar()) GetCurUsedAvatar()->btClubGrade = btClubGrade; }
	BYTE			GetClubPosition()					{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->btClubPosition : 0; }
	void			SetClubPosition(BYTE btClubPosition){ if(GetCurUsedAvatar()) GetCurUsedAvatar()->btClubPosition = btClubPosition; }
	int				GetClubContributionPoint()			{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->iClubContributionPoint : 0; }
	void			SetClubContributionPoint(int iClubContributionPoint) { if(GetCurUsedAvatar()) GetCurUsedAvatar()->iClubContributionPoint = iClubContributionPoint; }
	int				GetUnifromNum()						{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->btUnifromNum : 0; }
	void			SetUnifromNum(BYTE btUnifromNum)	{ if(GetCurUsedAvatar()) GetCurUsedAvatar()->btUnifromNum = btUnifromNum; }
	int				GetCheerLeaderCode()				{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->iCheerLeaderCode : 0; }
	void			SetCheerLeaderCode(int iCheerLeaderCode) { if(GetCurUsedAvatar()) GetCurUsedAvatar()->iCheerLeaderCode = iCheerLeaderCode; }
	BYTE			GetCheerLeaderTypeNum()				{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->btCheerLeaderTypeNum : 0; }
	void			SetCheerLeaderTypeNum(BYTE btCheerLeaderTypeNum) { if(GetCurUsedAvatar()) GetCurUsedAvatar()->btCheerLeaderTypeNum = btCheerLeaderTypeNum; }
	void			SetPennantCode(short stSlotIndex, int iPennantCode) { CHECK_CONDITION_RETURN_VOID(stSlotIndex < MIN_CLUB_PENNANT_SLOT || stSlotIndex > MAX_CLUB_PENNANT_SLOT);
																			if(GetCurUsedAvatar()) GetCurUsedAvatar()->iPennantCode[stSlotIndex] = iPennantCode; }
	time_t			GetLastDate()						{ return GetCurUsedAvatar() ? GetCurUsedAvatar()->tLastDate : 0; }

	void			InitClub();
	void			SetClub(BYTE btKind, SClubInfo sInfo, int iCheerLeaderCode);
	void			UpdateGameReultClubInfo(SCL2G_CLUB_USER_UPDATE_GAMERESULT_NOT& not);
	void			UpdateClubCoin(int iClubCoin, int iClubPlayCount, time_t tClubPlayCountUpdateDate);
	void			CheckGiveClubClothes(int* iClothesItemCode, time_t* tExpireDate);
	void			GiveClubClothes(int iItemCode);

	int 			TestLogout()						{ return InterlockedIncrement((long*)&m_TestVar0); }
	int				GetTestLogout()						{ return m_TestVar0; }

	int				GetGuageValue()						{ return m_iGuageValue; }
	void			SetGuageValue(int iGuageValue)		{ m_iGuageValue = iGuageValue; }

	BOOL			LoadAvatarList(LPCTSTR szGameID,int iEqualizeLV);

	virtual SAvatarInfo* GetCurUsedAvatar();

	BOOL			GetAvatarInfo( SAvatarInfo & AvatarInfo , int iSelectIndex);
	int				GetAvatarNum() { return m_AvatarManager.GetSize(); };
	SAvatarInfo*    GetAvatarInfoWithIdx( int iSelectIndex );
	SAvatarInfo*	GetAvatarInfoWithGameID(char *szGameID);
	int				GetCurAvatarIdx(){ return m_AvatarManager.GetCurUsedAvatarIdx();};
	int				GetEquippedAchievementTitle(){ return m_AvatarManager.GetEquippedAchievementTitlebyAvatarIdx(GetCurAvatarIdx()); };
	void			SetEquippedAchievementTitle(int iEquippedAchievementTitle){ m_AvatarManager.SetEquippedAchievementTitlebyAvatarIdx(GetCurAvatarIdx(), iEquippedAchievementTitle); };

	BOOL			CurUsedAvatarSnapShot(SAvatarInfo &AvatarInfo);
	int				GetCurUsedAvtarFameLevel();

	virtual SAvatarSeasonInfo*	GetAvatarSeasonInfo()	{ return &m_AvatarSeasonInfo;	}
	void			GetAvatarRankInfo(BYTE _btRecordType, int _iRankType, int _iCategory, int& iRank, int& iValue);

	void			SetUserLoginInfo(SFSLoginInfo& LoginInfo);
	BOOL			SetCurUsedAvatar(LPCTSTR strGameID);
	void			SetCurrentTotalRecord(); // 기록로그에 필요한 기록중 로그인시의 기록을  저장해둔다

	void			RemoveDisconncted();

	/////// 포지션 분화 
	BOOL			SetNewPosition(int iPos);
	void			CheckPositionSelect();
	
	///// Character ItemList
	BOOL			GetAvatarSingleItemStatus(int iItemCode, int *iaStat);
	void			GetSeasonInfo( int & iYear , int & iMonth );
	int				GetUserCoin(){ return GetCoin(); };
	void			SetUserCoin(int iCoin ){ SetCoin(iCoin); };
	int				GetUserSkillPoint() { return m_iSkillPoint; };
	void			SetUserSkillPoint(int iSkillPoint) { m_iSkillPoint = iSkillPoint; };

	int				IsSecondBoostItemEquipped(BYTE& bPropertyNum);
	CAvatarItemList*	GetCurAvatarItemListWithGameID(char *szGameID);
	virtual BOOL		GetAvatarItemStatus(int *iaStat, int *iaMinusStat = NULL);						// 아이템에서 얻는 스탯값
	BOOL				GetAvatarItemStatus(int *iaStat, BOOL& bWearPropertyItem, int *iaMinusStat = NULL, int* piCloneFeature = NULL);						// 아이템에서 얻는 스탯값
	virtual BOOL		GetAvatarLinkItemStatus(int *iaStat);					// 링크 아이템으로 얻는 스탯값
	virtual BOOL		GetAvatarSentenceStatus(int *iaStat);
	virtual BOOL		GetAvatarPowerupCapsuleStatus(int *iaStat);
	virtual BOOL		GetLvIntervalBuffItemStatus(int* iaStat);
	virtual BOOL		GetAvatarSpecialPieceProperty( int *iaStat );
	virtual BOOL		GetCheerLeaderStatus(int *iaStat);

	BOOL			CheckExpPointBonusItem(int &iExp, int &iSkillPoint, int &iFame);	// 아이템에서 얻는 경험치,포인트 보너스
	BOOL			CheckExpPointBonusWinItem(int &iExp, int &iSkillPoint);	// 아이템에서 얻는 승리 경험치,포인트 보너스
	BOOL			CheckPremiumItemExpPointBonus(int &iExp, int &iSkillPoint);
	void			SetAvatarFeatureInfo();
	BOOL			CheckAvatarFeature();
	BOOL			CheckExpireItem();
	BOOL			CheckCoachCardExpireItem();
	void			ExpireAvatarBindAccountItem(CFSGameODBC* pFreeStyleODBC, vector<int> vecExpiredItem);
	
	//////////////////// lobby /////////////////////////
	BOOL			EnableBroadcastFreeRoomInfo(int iMatchSvrID, int iPage, int iRoomIdx , int iOp); 
	BOOL			EnableBroadcastTeamInfo(int iMatchSvrID, BYTE btMatchTeamScale, int iPage, int iTeamIdx, int iClubSerialNum , int iOp);
	virtual BOOL	CanWhisper();
	virtual BOOL	CanInvite();
	virtual BOOL LoadEventCoin();

	//////////////////// room /////////////////////////
	void			InitStartGame();
		
	/////////////////////////// Team  //////////////////////
	BOOL			EnableBroadcastTeamInfoInTeam(int iPage, int iTeamIdx , int iOp);
	
	/////////////////////////// Paper, Present //////////////////////
	void			InitPaperList(vector<SPaper*>* v);			// 추가하고 vector내 데이타를 제거한다.	
	void			UpdatePaperList(vector<SPaper*>* v);
	void			SendPaper(SPaper* paper);
	BOOL			SendPaperList();
	void			SendPaperInfo(int index);
	BOOL			SendPresentList(int iMailType);
	void			SendPresentInfo(int iMailType, int index);
	BOOL			IsChoosePropertyItem(int iMailType, int iPresentIndex);
	BOOL			IsCharacterSlot(int iMailType, int iPresentIndex);
	void			SendChoosePropertyItemInfoPresent(int iMailType, int iPresentIndex);
	BOOL			AcceptPresent(int iMailType, int iIndexCnt, int iIndex, BYTE btPopUpIndex, int &iErrorCode);
	BOOL			GetChoosePropertyItemFromPresent(int iMailType, int iPresentIndex, int iChangeItemCode, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT], int& iErrorCode);
	void			RecvPaper(int index);
	
	void			LoadBindaccountItemList();

	BOOL			GetAndSetHeightStat( int& iNewHeight, int& iWeight, int& iOutMinHeight, int& iOutMaxHeight, int *iaChangeStat );
	void			InsertPropertyValueData( CAvatarItemList *pAvatarItemList, int iItemIdx, int *iaStat );
	BOOL			UpdateShoutCount(int iItemIdx);	// 정량제 아이템 차감
	
	virtual SAvatarStatus*	GetEqualizeStatus();
	int				GetEqualizePosition( int iPosition );
	
	int				CheckAndGetSelfProtectionRingCount();
	BOOL			CheckMacroChatItem();

	BOOL			CheckAndGetProtectHand( int& iPropertyKind, int& iNum );

	BOOL			SetNewCharacterName( char* szNewGameID, int iItemIdx, int iPropertyKind);
	BOOL			CheckGameID(char *szGameID);
	BOOL			SetAvatarGameID(LPSTR szNewGameID);
	
	int				GetSkillSlotCount(SExtraSkillSlot* pSkillSlot, int iPresentItemCode = -1);
	BOOL			CheckSkillSlotExchangeStatus(SExtraSkillSlot* pSkillSlot, int iItemCode);

	BOOL			SetAvatarFameSkillStatus(int* iStat);
	virtual BOOL			SetAvatarFameSkillAddMinusStatus(int* iAddStat, int* iMinusStat);

	void			SetEqualizeAvatarStatus(CFSGameODBC * pFSODBC,int iGameIDIndex, int iEqualizeLV);

	// 20090610 Add Tournament Trophy System
	virtual void	SetUserTrophy(int iUserTrophy, BOOL bNeedSync = FALSE);
	void			SubtractTrophy(int iSubtractCount);

	void			SendTournamentChampionCnt();

	/////////////// 경험치를 보고 렙업이 됐는지 검사한다 
	BOOL			CheckLvUp(int iGetExp, CFSGameODBC* pGameODBC);

	//////////////// 렙업됐을때 스탯올려야 할것을 찾아서 저장해둔다
	BOOL			ProcessStatUp(CFSGameODBC* pGameODBC);
	BOOL			ProcessStatUp(int *iaStat);
	void			UpdateAvatarStat(int iaAddStat[]);
	void			SendUserStat(int *iaDummyStat = NULL);

	void			ProcessLevelUp(int iLvUpGameIDIndex, int iGamePosition, int iPrevLv, int iUpdateLv, int iUpdateExp, BOOL bGameEnd = FALSE);
	void			SendLevelUpInfo(int iPrevLv, int iCurrentLv, BOOL bIsEventCheck, int iPosition = POSITION_CODE_NO_POSITION);
	void			GivePackageCrazyLevelUp(int iPrevLv, int iCurrentLv);

	void			SetTotalStatusByIndex(int iaStat[MAX_STAT_NUM]);
	void			UpdateTotalStatusByIndex( const int& iStatusIndex, const int& iStatusValue );	// 20090401 TWN service 'Quick Charge Item' task - modified member total stat in class game user for using boost item.
	void			ProcessCheckAvatarStatus(CReceivePacketBuffer* pRecvBuffer);
	void			ProcessCheckAvatarStatus1(CReceivePacketBuffer* pRecvBuffer);
	void			ProcessCheckAvatarStatus2(CReceivePacketBuffer* pRecvBuffer);
	void			ProcessCheckSumStatus(CReceivePacketBuffer* pRecvBuffer);
	void			ProcessCheckAvatarSomeAction_ChangeStatus(BYTE* pRecvBuffer, int iSize , int iHackType = HACK_TYPE_SOME_ACTION_CHANGE_STATUS);
	unsigned int	GetAvatarStateCheckSum();
	unsigned int	GetAvatarStateCheckSum1();
	unsigned int	GetAvatarStateCheckSum2();
	static bool		opSortSpecialty(SAvatarSpecialtySkill obj, SAvatarSpecialtySkill obj2);

	BOOL			CheckAvatarStatus(BYTE btCheckSeed, const int* pClientStatus,  int iHeightStatus, int iaSkill[MAX_SKILL_STORAGE_COUNT], int iaFreeStyle[MAX_SKILL_STORAGE_COUNT], const int iFaceID);
	BYTE			GenerateStatCheckSeed()	{	return m_btStatCheckSeed = rand(); }
	BOOL			CheckAvatarStat(const int* pClientStatus, const int iFaceID);

	void			SendRookieItemNotice(BOOL bBuying = FALSE);
	void			GetScaledHeight( int iOriginalHeight, int &iScaledHeight );		// 20090818 한국 키 조정
	void			SetDataForCheckHack(SAvatarHackCheckInfo& info);

	virtual BOOL	CheckBindAccountItemExpire(time_t& CurrentTime);
	void			CheckBindAccountInfo();
	void			SendBindAccountInfo();
	void			ResetRecordAndRank(int iRankType, int iSeasonIndex);
	void			ResetTotalRankAndRecord();
	void			ResetSeasonRankAndRecord(int iSeasonIndex);

	SBindAccountItemBonusData*	GetBindAccountBonusData(int iBindAccountItemPropertyKind);
	int							CheckAndGetBonusSkillSlot();

	int				GetDiscountRate(int iType, int iCategory);

	// 2010.02.23
	// 스탯보정
	// ktKim
	BOOL			StatusUp(CFSGameODBC* pGameODBC);
	// End
	BOOL			StatusUp(int* iaStat);

	void			SendMyItemBag( CFSItemShop* pItemShop, int iUseStyle = -1);
	void			SendMySkillBag(CFSSkillShop* pSkillShop);
	void			SendAllItemInfo( int iSex, int bCheckClone = FALSE );
	void			SendAllMyOwnItemInfo();
	void			SendFriendList();
	void			SendFriendAccountList();

	void			GetUserItemInfo(SUserItemInfo* pSlotItemInfo);

	SHORT			GetGameOption();

	// 휴식 아이템 차감
	int				DiscountPauseItem();
	// 친구 리스트 매치 서버에 전송
	void			SendToMatchAddFriendAll();
	// 매치 서버에 친구, 거부 유저 추가
	void			SendToMatchAddFriend(const char* szGameID, int iLevel, int iEquippedAchievementTitle, int iConcernStatus);
	// 매치 서버에서 친구, 거부 유저 삭제
	void			SendToMatchDelFriend(const char* szGameID);

	void			SendToMatchAddFriendAccountAll();
	void			SendToMatchAddFriendAccount(int iUserIDIndex, const char* szGameID, BYTE btGamePosition, BYTE btLv, BYTE btStatus, BYTE btChannelType, BYTE btLocation, int iWithPlayCnt, time_t tLastLogoutTime);
	void			SendToMatchDelFriendAccount(int iFriendIDIndex);

	// Record Update
	void			UpdateGameRecord(BOOL bUpdateGameRecord, BOOL bActiveProtectRing, BOOL bActiveProtectHandRecord,
		int iDisconnectPoint, SFSGameRecord GameRecord, int iGamePlayerNum, int iaStat[MAX_STAT_NUM], BYTE _ucRecordType, BYTE btFactionReversalScore);
	void			UpdateAvatarRecord( int _iGamePlayerNum, SFSGameRecord _GameRecord, BYTE _ucRecodeType);
	
	int					GetEquippedAchievementTitlebyAvatarIdx(int iAvatarIdx);

	void				RemoveFeature(int iFeatureIndex);
	void				CheckFeatureAndRemoveItem(SUserItemInfo* pItem);
	void				DecreaseFeature(int iFeatureIndex, int iDecreaseCount);
	
	BOOL	ProcessMaxLevelStep();
	BOOL	CheckMaxLevelStepUp(BYTE& byPreviousMaxLevelStep, BYTE& byCurrentMaxLevelStep);
	void	ProcessMaxLevelStepReward(BYTE byPreviousMaxLevelStep, BYTE byCurrentMaxLevelStep);

	void	SetAvatarMaxLevelStep(BYTE byMaxLevelStep);
	void	UpdateMaxLevelStepInDB(int iMaxLevelStep = -1);

	int		GetCurAvatarTotalSkillPoint();
	int		GetCurAvatarUsedSkillPointAllType();
	int		GetCurAvatarUsedSkillPoint(int iType);
	int		GetCurAvatarRemainSkillPoint();

	void	EquipAchievementTitle(int iEquipAchievementTitle);
	int		GetProgressAmountofAchievement(int iSelectedAchievementIndex);

	void	UpdateTournamentVictory(int iSeasonIndex, BYTE ucScaleMode);

	void	CheckEvent(int iPerformTimeIndex, void* lParam=NULL, int iEventIndex = 0);
	virtual void CheckCashItemBuyEvent(int iCashChange, BOOL bUseEventCoin, int iUseEventCoin, void* lParam=NULL);
	virtual BOOL EventStepComplateReward(int iParam[]);

	virtual BOOL EventStepComplateRewardPopup(int iParam[]);
	virtual BOOL EventUpdateEventUserStep(int iParam[]);
	virtual BOOL EventGameStartLoading(int iParam[]);
	virtual BOOL EventCountLoad( int iParam[] );
	virtual BOOL EventCountIncrease( int iParam[] );
	virtual BOOL EventGiveAndSendPresent(int iParam[]);
	virtual BOOL EventPopUp( int iParam[] );
	virtual BOOL EventCompleteNewUser( int iParam[]);
	virtual BOOL EventGiveAndSendSeventhEventCash(int iParam[]);
	virtual BOOL EventCheckUserMaxLv(int iParam[]);
	virtual BOOL EventCheckFriendPlayCountOnline(int iParam[]);
	virtual BOOL EventCheckPCRoomKind(int iParam[]);
	virtual BOOL EventLoadEventUserStep(int iParam[]);
	virtual BOOL EventInsertEventUserStep(int iParam[]);
	DECLARE_EVENT_FUNCTION(EVENT_GIVE_PCROOM_EVENT_REWARD);
	DECLARE_EVENT_FUNCTION(EVENT_GIVE_ITEMBUY_BONUS_REWARD);
	DECLARE_EVENT_FUNCTION(EVENT_JUMPING_EVENT_NOTICE);
	DECLARE_EVENT_FUNCTION(EVENT_JUMPING);
	DECLARE_EVENT_FUNCTION(EVENT_SEND_BURNING_INFO_NOTICE);
	DECLARE_EVENT_FUNCTION(EVENT_SEND_COMPOUNDINGITEM_NOTICE);
	virtual BOOL EventLoadAccumulatedPalyCount(int iParam[]);
	DECLARE_EVENT_FUNCTION(EVENT_ACCUMULATED_ATTENDANCE_CHECK_USER_DATA);

	void	SendEventComponentMsg( int iPopupType , int iPopupRequestType , int iArgCnt , int iPopupIndex, int iArgFirst, int iArgSecond, int iArgThird, int iArgFourth );
	
	bool	AddLuckItem();
	int		GetMaxLostNum()				{ return m_MaxLostNum; }
	void	SetMaxLostNum(int LostNum)	{ m_MaxLostNum = LostNum; }
	int		GetAIComeLostNum()			{ return m_iAIComeLost;	}
	void	SetAIComeLostNum(int iLostNum) { m_iAIComeLost = iLostNum; }
	BOOL	ProcessOpenNewPlayerGift(int iItemIndex );
	BOOL	ProcessOpenCrazyLevelUpEvent(int iItemIndex );
	VOID	ProcessAchievementbyGroupAndComplete( int iAchievementConditionGroupIndex, int iAchievementLogGroupIndex, vector<int>& vParameter, VOID* pConditionData = NULL);
	virtual BOOL GiveReward_Achievement( int iAchievementIndex , BOOL bAnnounce = TRUE);
	void	SendAchievement_AnnounceProxy( int iAchievementIndex);

	BOOL	CheckAccItemSelectNotice( );
	
	void     SendEventListInfo();

	void	SetRecAllFriend( BOOL received ) { m_bReceiveAllFriend = received; }
	void	SetEnterChannelToSendFriend( BOOL entered ) { m_bEnterChannelToSendFriend = entered; }

	void	SetRecAllFriendAccount( BOOL received ) { m_bReceiveAllFriendAccount = received; }
	void	SetEnterChannelToSendFriendAccount( BOOL entered ) { m_bEnterChannelToSendFriendAccount = entered; }

	virtual void	DisconnectUser();

	void	InitClubInvitationList(vector<SClubInvitationData*>* vecClubInvitation);
	void	RecvClubInvitation(int iPaperIndex, CFSClubBaseODBC* pClubBaseODBC);
	BOOL	SendClubInvitationList();
	void	SendClubInvitation(int iPaperindex);

	BOOL	CheckPaperExist( int iPaperIndex );

	virtual CAvatarItemList* GetAvatarItemList();
	virtual BOOL RecvPresent(int iMailType, int iPresentIndex);
	BOOL RecvPresentList(int iMailType, list<int> listPresentIndex);
	virtual BOOL GiveReward_ExhaustItemToInven(SRewardConfigItem* pReward, int iValue = 0);
	virtual BOOL GiveReward_ExhaustAndTimeItemToInven(SRewardConfigItem* pReward);
	virtual BOOL GiveReward_CoachCard(SRewardConfigCoachCard* pReward, int iValue = 0);
	CFSGameUserBingo* GetUserBingo() {	return &m_UserBingo; };
	CFSGameUserLuckyBox* GetUserLuckyBox() { return &m_UserLuckyBox; }
	CFSGameUserRandomBox* GetUserRandomBox()  { return &m_UserRandomBox; }
	CFSGameUserThreeKingdomsEvent* GetUserThreeKingdomsEvent() { return &m_UserThreeKingdomsEvent; }
	CFSGameUserSpecialSkin*	GetUserSpecialSkin()	{	return &m_UserSpecialSkin;	}
	CFSGameUserBasketBall* GetUserBasketBall()		{ return &m_UserBasketBall; }
	CFSGameUserHoneyWalletEvent* GetUserHoneyWalletEvent()	{ return &m_UserHoneyWalletEvent; }
	CGameUserCharacterCollection* GetUserCharacterCollection()	{ return &m_UserCharacterCollection; }
	CFSGameUserHotGirlSpecialBoxEvent* GetUserHotGirlSpecialBoxEvent()	{ return &m_UserHotGirlSpecialBoxEvent; }
	CFSGameUserSkyLuckyEvent* GetUserSkyLuckyEvent()	{ return &m_UserSkyLuckyEvent; }
	CFSGameUserCheerLeader* GetUserCheerLeader() { return &m_UserCheerLeader; }
	CFSGameUserGoldenSafeEvent* GetUserGoldenSafeEvent()	{ return &m_UserGoldenSafeEvent; }
	CFSGameUserShoppingEvent* GetUserShoppingEvent() { return &m_UserShoppingEvent; }
	CFSGameUserLegendEvent* GetUserLegendEvent() { return &m_UserLegendEvent; }
	CFSGameUserRandomArrowEvent* GetUserRandomArrowEvent() { return &m_UserRandomArrowEvent; }
	CFSGameUserDevilTemtationEvent* GetUserDevilTemtationEvent() { return &m_UserDevilTemtationEvent; }
	CFSGameUserVendingMachineEvent* GetUserVendingMachineEvent() { return &m_UserVendingMachineEvent; }
	CFSGameUserMatchingCard* GetUserMatchingCard()	{ return &m_UserMatchingCard; }
	CFSGameUserRandomItemBoardEvent* GetUserRandomItemBoardEvent() { return &m_UserRandomItemBoardEvent; }
	CFSGameUserGamePlayEvent* GetUserGamePlayEvent() { return &m_UserGamePlayEvent; }
	CFSGameUserRandomItemTreeEvent* GetUserRandomItemTreeEvent() { return &m_UserRandomItemTreeEvent; }
	CFSGameUserLoginPayBackEvent* GetUserLoginPayBackEvent() { return &m_UserLoginPayBackEvent; }
	CGameUserPurchaseGrade* GetUserPurchaseGrade() { return &m_UserPurchaseGrade; }
	STodayHotDealBuyCount* GetUserTodayHotDealBuyCount()	{ return &m_UserHotDealCount; }
	CFSGameUserRandomItemGroupEvent* GetUserRandomItemGroupEvent() { return &m_UserRandomItemGroupEvent; }
	CFSGameUserRainbowWeekEvent* GetUserRainbowWeekEvent() { return &m_UserRainbowWeekEvent; }
	CFSGameUserGameOfDiceEvent* GetUserGameOfDiceEvent() { return &m_UserGameOfDiceEvent; }
	CFSGameUserRankMatch* GetUserRankMatch()	{	return &m_UserRankMatch;	}
	CFSGameUserPasswordEvent* GetUserPasswordEvent() { return &m_UserPasswordEvent; }
	CFSGameUserHalloweenGamePlayEvent* GetUserHalloweenGamePlayEvent() { return &m_UserHalloweenGamePlayEvent; }
	CFSGameUserLottoEvent* GetUserLottoEvent() { return &m_UserLottotEvent; }
	CFSGameUserPresentFromDeveloperEvent* GetUserPresentFromDeveloperEvent() { return &m_UserPresentFromDeveloperEvent; }
	CFSGameUserDiscountItemEvent* GetUserDiscountItemEvent() { return &m_UserDiscountItemEvent; }
	CFSGameUserMiniGameZoneEvent* GetUserMiniGameZoneEvent() { return &m_UserMiniGameZoneEvent; }
	CFSGameUserBigWheelLoginEvent* GetUserBigWheelLoginEvent()	{ return &m_UserBigWheelLoginEvent; }
	CFSGameUserSalePlusEvent* GetUserSalePlusEvent()	{ return &m_UserSalePlusEvent; }
	CFSGameUserDriveMissionEvent* GetUserDriveMissionEvent()	{ return &m_UserDriveMissionEvent; }	
	CFSGameUserMissionMakeItemEvent* GetUserMissionMakeItemEvent()	{ return &m_UserMissionMakeItemEvent; }	
	CFSGameUserMissionCashLimitEvent* GetUserMissionCashLimitEvent()	{ return &m_UserMissionCashLimitEvent; }
	CFSGameUserDarkMarketEvent* GetUserDarkMarketEvent()	{ return &m_UserDarkMarketEvent; }	
	CFSGameUserColoringPlayEvent* GetUserColoringPlayEvent()	{ return &m_UserColoringPlayEvent; }
	CFSGameUserCongratulationEvent* GetUserCongratulationEvent()	{ return &m_UserCongratulationEvent; }
	CFSGameUserHelloNewYearEvent* GetUserHelloNewYearEvent()	{ return &m_UserHelloNewYearEvent; }	
	CFSGameUserOpenBoxEvent* GetUserOpenBoxEvent() { return &m_UserOpenBoxEvent; }
	CFSGameUserNewbieBenefit* GetUserNewbieBenefit() { return &m_UserNewbieBenefit; }
	CFSGameUserFriendInviteList* GetUserFriendInvite() { return &m_UserFriendInvite; }
	CFSGameUserPrimonGoEvent* GetUserPrimonGoEvent() { return &m_UserPrimonGoEvent; }
	CFSGameUserPrivateRoomEvent* GetUserPrivateRoomEvent() { return &m_UserPrivateRoomEvent; }
	CFSGameUserHippocampusEvent* GetUserHippocampusEvent() { return &m_UserHippocampusEvent; }
	CFSGameUserComebackBenefit* GetUserComebackBenefit() { return &m_UserComebackBenefit;	}
	CFSGameUserPowerupCapsule* GetUserPowerupCapsule() { return &m_UserPowerupCapsule;	}
	CFSGameUserUnlimitedTatooEvent* GetUserUnlimitedTatooEvent() { return &m_UserUnlimitedTatooEvent; }
	CFSGameUserPVEMission* GetUserPVEMission() { return &m_UserPVEMission; }
	CFSGameUserCovet* GetUserCovet() { return &m_UserCovet;	}
	CFSGameUserShoppingGodEvent* GetUserShoppingGodEvent() { return &m_UserShoppingGodEvent; }
	CFSGameUserRandomItemDrawEvent* GetUserRandomItemDrawEvent() { return &m_UserRandomItemDrawEvent; }
	CFSGameUserSelectClothesEvent* GetUserSelectClothesEvent() { return &m_UserSelectClothesEvent; }

	SUserFriendAccountInfo& GetUserFriendAccountInfo()	{ return m_UserFriendAccountInfo; }
	CFSGameUserSubCharDevelop* GetUserSubCharDevelop() { return &m_UserSubCharDevelop; }
	CFSGameUserSummerCandyEvent* GetUserSummerCandyEvent() { return &m_UserSummerCandyEvent; }
	CFSGameUserSecretShopEvent* GetUserSecretShopEvent() { return &m_UserSecretShopEvent; }
	CFSGameUserTransferJoycityPackageEvent* GetUserTransferJoycityPackageEvent() { return &m_UserTransferJoycityPackageEvent; }
	CFSGameUserTransferJoycityShopEvent* GetUserTransferJoycityShopEvent() { return &m_UserTransferJoycityShopEvent; }
	CFSGameUserTransferJoycity100DreamEvent* GetUserTransferJoycity100DreamEvent() { return &m_UserTransferJoycity100DreamEvent; }

	void SendAvatarInfo();
	void SendAvatarInfoUpdateToMatch(bool bIsRoomChangeClone = false);
	void SendAvatarNoBroadcastInfoUpdateToMatch();
	void SetAvailablePointRewardCard(BOOL bSet) {	m_bIsAvailablePointRewardCard = bSet; }
	BOOL IsAvailablePointRewardCard() {	return m_bIsAvailablePointRewardCard; }

	virtual BOOL	GiveRandomReward( int iRewardGroupIdx, int iRewardRandomIdx , VectorRewardInfo& vecRewardInfo );
	BOOL	CreateClub_AfterPay( SBillingInfo* pBillingInfo, int iPayResult);
	BOOL	DonationClub_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL	CheckCheerLeaderAbility(int iShootSuccessRate, int iBlockSuccessRate);
	void	ClubInfoUpdateToCenterSvr();
	void	Load_ClubBonusByProffer( CFSODBCBase* pODBCBase );

	void	SetBuyState(int iStateBuy)	{ m_iStateBuy = iStateBuy; }

	void	SetUserSubPageState(int iState)	{	m_iUserSubPageState = iState; }
	int		GetUserSubPageState()			{	return m_iUserSubPageState;	}

	void	SetRoundGroupIndex(BYTE btRoundGroupIndex) { m_btRoundGroupIndex = btRoundGroupIndex; }
	BYTE	GetRoundGroupIndex() { return m_btRoundGroupIndex; }

	BOOL	MakeShopItemSelectInfo(CPacketComposer& PacketComposer, int iOp, SShopItemInfo& ItemInfo, int iItemCode = 0, int iRenewItemIdx = 0, int iRecvUserLv = 0, int iSecretStoreIndex = 0);
	BOOL	GetShopItemSelectInfo(CPacketComposer& PacketComposer, int iOp, int iItemCode, int iRenewItemIdx, int iRecvUserLv, int iSecretStoreIndex);
	BOOL	GetShopItemSelectInfo(CPacketComposer& PacketComposer, int iPropertyKind);

	int		GetMatchLocation()	{	return m_iMatchLocation; }
	void	SetMatchLocation(int iMatchSvrProcessID) {	m_iMatchLocation = iMatchSvrProcessID; }

	void	GetMatchLoginUserInfo(SG2M_USER_INFO &info, BYTE btMatchType);
	void	GetBasicItemCode(int* iBasicItem);

	void	SetRoomListUserSet(int iMatchSvrID, SM2G_TEAM_LIST_USER_SET& info);
	void	SetRoomListUserSet(BYTE  btTeamScale, int iStartTeamID, int iSendCount);

	CJoinTeamConfigInfo*	GetJoinTeamConfig()	{	return &m_JoinTeamConfig; };
	BOOL	UpdateUserLvExp(bool bIsLvUp, int iLv, int iPlusExp, int *iaStat);
	void	UpdateAvatarLv(int iUpdatedLv);
	virtual void	SetOptionEnable(BYTE bOptionType, BOOL bSet);

	void	SetChatChannel(CLobbyChatChannel* pChannel)		{ m_pLobbyChatChannel = pChannel; }
	CLobbyChatChannel*	GetChatChannel()					{ return m_pLobbyChatChannel; }
	void	SetChatFactionChannel(CLobbyChatChannel* pChannel)	{ m_pLobbyChatFactionChannel = pChannel; }
	CLobbyChatChannel*	GetChatFactionChannel()					{ return m_pLobbyChatFactionChannel; }
	void	ExitChatChannel();
	void	ExitChatFactionChannel();

	void	GetUserBufferItemInfo( SBufferItem pBufferItem[BUFFER_ITEM_NUM] );
	char*	GetRc4Key(void)	{	return m_szRc4Key;	}
	char*	GetChecksum_Rc4Key(void)	{	return m_szCheckSum_Rc4Key;	}
	char*	GetAnyOperation_Rc4Key(void)	{	return m_sz_AnyOperation_Rc4Key;	}
	void	UpdateRand_Rc4Key(int iHackType);
	void	SendHighFrequencyItemCount(int nType, BOOL bAcceptPresent = FALSE );

	void	SendRecordBoardStatus();

	CUserRecordBoard* GetUserRecordBoard() { return &m_UserRecordBoard; }

	BOOL BuyRecordBoardPage_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	
	BOOL	BuyHighFrequencyItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult);

	// Product-Item
	BOOL	CheckCapital(BYTE bySellType, int iPrice, int& iErrorCode);
	void	SubtractCapital(BYTE bySellType, int iPrice);
	void	AddProductToInventory(SProductData* sProductData, BOOL bNewStatus = TRUE);
	void	AddProductToInventory(SProductData& ProductData, BOOL bNewStatus = TRUE);
	void	RemoveProductToInventory(int iTendencyType, int iProductIndex, int iProductCount = 0);
	void	ProcessAfterRandomCoachCardUse(int iTendencyType,int iUseProductIndex, int iProductCount, SProductData* pProductData, vector<SActionInfluenceConfig>& vecAddedActionInfluence);
	int		GetProductInventoryCount();
	void	SendCoachCardShopInventoryListRes(int iRequestPage, BYTE byInventoryRange,int iRequestTendencyType,int iSortPropertyType,int iSortOrder,int iRequestType);
	BOOL	IsThrereProduct(int iProductnIndex);
	void	GetOwnedProductItemInfo(int iItemIDNumber, int& iProductIndex, int& iInventoryIndex);
	void	SendCoachCardRewardNot(int iStateType , int iReceiveProductIndex, int iUseProductIndex, int iUpdatedProductCount);

	// Product Item Information Func
	CProduct* GetInventoryProduct(int iProductIndex);
	int		GetInventoryGradeLevel(int iUseProductIndex);
	int		GetInventoryTendencyType(int iUseProductIndex);
	BOOL	GetPotentialComponent(int iProductIndex, SPotentialComponent& sPotentialComponent);
	void    GetInventorySubActionInfluence(int iUseProductIndex, vector<int>& vecSubActionInfluence);
	BOOL	IsPotentialComponent(int iProductIndex);

	// Product-Item
	BOOL	LoadProductInventoryList();
	BOOL	LoadItemSlotDataList();
	BOOL	SetSlotPackageInDB(int iTendencyType, int iProductIndex);
	BOOL	EquipProduct(SSlotData& sSlotData, int& iErrorCode);
	BOOL	UnEquipProduct(int iTendencyType, int& iErrorCode);
	int		GetEquipProductIndex(int iTendencyType);
	void	SendEquipCoachCardRes(BOOL bResult, int iErrorCode, int iProductIndex);
	void	SendUnEquipCoachCardRes(BOOL bResult,int iErrorCode,int iProductIndex);
	BOOL	RegisterItemCombine(const SCombineData& AddCombineData);
	BOOL	UnregisterItemCombine(int iProductIndex);
	void	ClearItemCombineList();
	BOOL	GetCombineLevel(int iIndex, int& iGradeLevel);
	BOOL	GetCombineInventoryIndex(CombineDataVec& vecCombineData);
	BOOL	GetInventoryIndex(int iProductIndex, int& iInventoryIndex);
	BOOL	GetItemIDNumber(int iProductIndex);
	void	SendCoachCardSlotPackageListRes();		
	void MakeEquipSlotPackageListRes( CPacketComposer &PacketComposer);
	void MakeCoachCardSlotPackageListRes( CPacketComposer &PacketComposer);
	void MakeDataForCoachCardSlotPackageList(PBYTE pBuffer, int& iSize);
	BOOL	BuyCoachCardTermExtend(int iProductIndex, int iTermTime,int iSellType,int iPrice, int& iRemainTime);
	void	UpgradePotenCard(SC2S_UPGRADE_POTENTIAL_CARD_REQ& req, SS2C_UPGRADE_POTENTIAL_CARD_RES& res);
	RESULT_CHOOSE_POTENTIAL_CARD_UPGRADE DecideUpgradePotenCard(int iProductIndex, BOOL bChooseNewUpgrade);
	void	CheckAndSendPausedPotenUpgradeInfo();

	void	SubBuyCost(BYTE bySellType, int iPrice);
	BOOL	LoadActionInfluence();
	BOOL	LoadPotentialComponent();
	void	AddProductMainActionInfluence(SItemActionInfluence& sItemActionInfluence);
	void	AddProductSubActionInfluence(int iProductIndex, int iActionInfluenceIndex);
	void	SetAndSortCoachCardInventory(int iSortPropertyType, int iSortOrder);
	void	SetPotentialComponent( int iProductIndex, const SPotentialComponent& sPotentialInfo );
	int		GetPotentialExistIndexCount( POTENTIAL_COMPONENT ePotential, int iRelateIndex );
	int		GetSlotPotentialExistIndexCount( POTENTIAL_COMPONENT ePotential, int iRelateIndex );
	BOOL	IsExistInSlotFreeStylePotentialCard(int iFreeStyleNo);
	int		CompareSystemTime(SYSTEMTIME CurrentTime, SDateInfo& ExpireDate);
	void	MakeCoachCardActionInfluenceList( CPacketComposer& PacketComposer);
	void	MakeCoachCardPotentialComponentList(CPacketComposer& PacketComposer);
	void    GetEquipActionInfluence(int iProductIndex, vector<int>& vecEquipActionInfluenceIndex);
	void	GetPotentialFreeStyleUpgradeInfo(int iFreeStyleNo, int& iUpgradeIndex, int& iUpgradeStep);
	virtual void	GetPotentialFreeStyleUpgradeInfo(vector<SSkillUpgradeInfo>& vec);
	virtual void	GetSpecialSkinAdditionalEffectsInfo( vector<SSpecialSkinAdditionaleffectsInfo>& vec );
	
	BOOL			LoadStyleInfo();
	void			CheckAndSaveStyleInfo();
	void			ChangeUseStyleName(char* szStyleName);
	char*			GetUseStyleName();
	BOOL			SetUseStyleIndex(int iStyleIndex, BOOL bReLoad = TRUE);
	int				GetUseStyleIndex();
	BOOL			CheckItemUseStyle(int iStyleIndex, int iItemIdx);
	void			SendUseStyleAndNameInfo();
	SAvatarStyleInfo*	GetStyleInfo();
	BOOL			ReLoadFeatureInfo( BOOL bSendStat = TRUE );
	BOOL			ChangeUseTryItemFeature(int iStyleIndex);

	void	SendSpecialPartsInfo(map<int, BYTE> mapEquipCount, map<int, vector<int>> mapProperty, int iChangedUseStyle = -1);
	void	SendSpecialPartsList();
	BOOL	LoadSpecialPartsUserProperty();
	SSpecialPartsUserProperty* GetSpecialPartsUserProperty(int iUseStyleIndex, int iSpecialPartsIndex, int iSlotNum);
	void	UpdateSpecialPartsUserProperty(int iSpecialPartsIndex, vector<int>& vecProperty);
	void	CheckSpecialPartsUserProperty();
	BOOL	CheckSpecialPartsOptionProperty(float fShootSuccessRate, float fBlockSuccessRate);
	void	GetSpecialPartsOptionProperty(float& fShootSuccessRate, float& fBlockSuccessRate);
	void	GetSpecialPartsOptionProperty(SG2M_ADD_STAT_UPDATE& sInfo);

	void			CheckFullCrtUniform();

	void			SetExpirePremiumPCRoom(BOOL bExpirePremiumPCRoom)	{ m_bExpirePremiumPCRoom = bExpirePremiumPCRoom; }
	BOOL			IsExpirePremiumPCRoom()								{ return m_bExpirePremiumPCRoom; }
	void			RemovePCRoomBenefits();
	void			CheckLoadPCRoomItemList();
	void			LoadPCRoomItemList(BOOL bRemoveItem = FALSE);	
	void			UpdatePCRoomItemStat(int iLv);

	void			SendSkillTakeEnable(int iSkillNo = SKILL_USE_DIRECT_PASS);


	void MakePacketForCoachCardSubActionInfluence(CPacketComposer& Packet, int iProductIndex);
	void MakeDataForCoachCardSubActionInfluence(PBYTE pBuffer, int& iSize, int iProductIndex);
	void MakePacketForCoachCardInfo(CPacketComposer& Packet, int iProductIndex);
	void MakeDataForCoachCardInfo(PBYTE pBuffer, int& iSize, int iProductIndex);
	BOOL MakePacketForUserInfoCoachCard(CPacketComposer& Packet, int iCoachCardProductIndex, int iCoachCardFront);
	void MakeDataForUserInfoCoachCard(PBYTE pBuffer, int& iSize, int iCoachCardProductIndex, int iCoachCardFront);
	BOOL BuyCoachCardItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	
	void AddPointAchievements();
	void AddFriendCountAchievements();
	int GetFriendCount();
	void AddTotalRankingAchievements();
	void AddRevengeMatchingWinAchievements();
	int GetRevengeMatchingWinCnt();
	BOOL AddRevengeMatchingWinCnt(int iAddCnt = 1);
	BOOL LoadRevengeMatchingWinCnt();

	void			SetRoomRevengeMatchingType(BYTE btRoomRevengeMatchingType)	{ m_btRoomRevengeMatchingType = btRoomRevengeMatchingType; }
	BYTE			GetRoomRevengeMatchingType()	{ return m_btRoomRevengeMatchingType; }

	void SetGuaranteeCardInfo(SGuaranteeInfo sInfo);
	int	 GetGuaranteeCardValue(int iCategory);

	BOOL GiveClubTournamentVoteUserBenefitItem(int iItemCode, int iItemIdx, int iUpdatedPropertyValue);

	BOOL	LoadAvatarSecretStore(void);
	BOOL	SendSecretStoreOpen(void);
	void	SendSecretStoreInfoRes();
	void	BuySuccessSecretStoreItem(int iStoreIndex );
	void	SuccessCheckSecretStoreItem();
	BYTE	GetSecretStoreMaxLv_NPU()	{ return m_SecretStoreInfo.btMaxLv_NPU; }
	int		GetSecretStoreGrade(int iItemCode);
	int		GetSecretStoreIndex(int iItemCode);
	BOOL	IsOpenSecretStore(time_t tCurrentTime = _time64(NULL));

	CFSGameUserIntensivePractice* GetUserIntensivePractice() {	return &m_UserIntensivePractice; };
	void	GetFriendGameIDList(vector<LPTSTR>& vGameID);

	int		CheckAdvantageUserType();
	void	ProcessLogOut();

	void	SetMatchingPoolCareInfo(SMatchingPoolCareUserInfo& stMatchingPoolCareInfo);
	BOOL	CheckMatchingPoolCareUser();
	void	CheckMatchingPoolCareExpPointBonus(int& iExp, int& iPoint);
	BOOL	CheckAndRemoveMatchingPoolCareTarget();
	void	AddMatchingPoolCarePlayCount();
	int		GetMatchingPoolCarePlayCount();
	int		GetMatchingPoolCareRemainPlayCount();

	void			LoadUserFaction();
	void			CheckFactionJoinItem();
	void			CheckFactionRaceRankReward();
	void			UpdateFactionLastPlayTime(BOOL bClearSeason);
	void			CheckFactionRankReward();

	virtual void	OnRecieveReward(SRewardConfig* pReward);
	void			SetIsLastMatchDisconnected(BOOL bDisconnected) { m_bIsLastMatchDisconnected = bDisconnected; }
	BOOL			IsLastMatchDisconnected()	{ return m_bIsLastMatchDisconnected; }
	void			SetLeftSeatStatus(BOOL bStatus)	{ m_bLeftSeatStatus = bStatus; }
	BOOL			GetLeftSeatStatus()				{	return m_bLeftSeatStatus; }
	
	void			SendSuportGrowthInfo();
	void			SendWordPuzzlesEventStatus();
	BOOL			IsUseEquipItem();
	void			SendRandomBoxStatus(BYTE btUserState);
	void			SendPuzzlesStatus();
	void			SendThreeKingdomsEventStatus();
	virtual void	SendMissionShopEventStatus();
	void			SendFactionRaceStatus();

	BOOL		CheckInventory_ComboundingItemWithItemCode(int iItemCode, int iRequiredVal, int& iItemIdx);
	int			ComboundingItem_Combine(int iItemIndex, SComboundingItem_Config	sConfig, SS2C_COMPOUNDINGITEM_COMBINE_RES& rinfo);
	int			ComboundingItem_Combine_ProcessingResult(int iItemIndex, SComboundingItem_Config sConfig, int iDeleteIndex[],SS2C_COMPOUNDINGITEM_COMBINE_RES& rinfo);

	int				CustomizeItem_InsertLog( int iItemCode, int iInventoryIndex, int* iPropertyIndex, BYTE btColorType );
	BOOL			GetCustomizeItem_Received( int iInventoryIndex , S_ODBC_CustomizeLog& sLogInfo);
	void			MakeDataForUserInfoCharacterCollection(PBYTE pBuffer, int& iSize);
	void			MakeDataForUserInfoCharacterCollection(CPacketComposer &Packet);
	void			SendHotGirlTime();

	SAvatarInfo* 	ChangeCloneAvatar(Clone_Index eIndex);
	void			ChangeCloneAvatarDiffSexItem(int iSex);
	void			ChangeBasicItem_Sex(int iSex);
	void			SendUserItemList( int iInventoryKind, int iBigKind, int iSmallKind, int iPage , BOOL bCloneChange = FALSE);
	BOOL			CheckAndGetCloneIndex(Clone_Index& eIndex);
	BOOL			GetCloneCharacter(void)	const	{	return m_bCloneCharacter;	}	
	BOOL			GetChangeofCharacter(void)const	{	return m_bChangeofCharacter;	}
	void			SetChangeofCharacter(BOOL bCheck)	{	m_bChangeofCharacter = bCheck; }
	Clone_Index		CheckCloneCharacter_Index(void);

	int				GetCloneCharacterStyleIndex(Clone_Index eIndex);
	int				GetCloneCeremonyList( int iActionCode, int& iMineCeremony, int& iCloneCeremony);
	void			ChangeCloneDefaultSeremony(int iUseStyle, bool bChangeStyle = true);
	void			SetCloneCharacterDefaultSeremony(int iUseStyle);
	void			UpdateHeightAndStatus(Clone_Index eIndex);
	void			SetCloneCharacterAddStat(int iIndex);
	void			RemoveChangeDressItem(void);
	BOOL			GetTodayHotDealRes(SS2C_EVENT_TODAY_HOTDEAL_INFO_RES& rs);
	void			HotDealItemCheckAndUpdateLimitCount(int iItemCode, int iPropertyValue);
	void			HotDealItemGetLimitCount();
	void			SendExposeUserItem(void);
	BOOL			CheckItemAcceptProperty(int iMailType, int iPresentIndex, int* aiPropertyIndex);
	void			ClearUserFactionInfo();
	void			AllDistrictUseBuffItem(time_t tBuffEndTime);
	void			SendPotenCardExpiredInfoNot(void);
	int				PotenCardTermExtendforEquipCard(int iPropertyValue);

	void			AddLobbyClubUser();
	void			RemoveLobbyClubUser();
	void			SendPotenCardEventInfo();
	
	void			CheckEquipPotentialProductSetting(int iProductIndex);
	void			SendCheerLeaderEventStatus();
	BOOL			CheckAndInitPayBackEvent(void);
	void			MakeAndSendPaybackInfo(void);
	void			GiveAndSendUserWantPayback(void);
	void			EventPayBackBuyItem(int iUseCash);
	BOOL			CheckAndInitGoldenCrushEvent(void);
	BOOL			CheckAndSendGoldenCrushInfo(void);
	void			InitializeGoldenCrushAndSend(void);
	void			CrushEventGoldenCrush(BYTE btType, SS2C_EVENT_GOLDENCRUSH_CRUSH_RES& rs);
	void			SendGoldenCrushEventStatus();
	void			SendMatchingCardEventStatus();
	void			UpdateUserEventCoin();
	void			CheckAndSendEventCoinInfo(void);
	void			CheckExpiredEventCoin(void);
	void			SendPuzzleRewardInfo(void);
	void			GiveAndSendEventPuzzleReward(int iTokenIndex );
	BOOL			SendSonOfChoiceEventProductList();
	BOOL			BuySonOfChoiceEventRandomProduct(SC2S_SONOFCHOICE_BUY_RANDOM_PRODUCT_REQ& rs);
	BOOL			BuySonOfChoiceEventRandomProduct_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL			BuyPackageItemGiveInventory_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL			CheckAndInitAttendanceItemShopEvent(void);
	void			SendAttendaceItemshopStatus(void);
	BOOL			BuyAttendanceItemShop( int iItemCode , int& iErrorCode, int iOpCode );
	BOOL			BuyAttendanceRandomItem( int iItemCode, int& iErrorCode );

	void			CheckMainRewardLog();
	void			GiveLvUpEventReward(int iPreLv, BOOL bLogin = FALSE);
	void			SendJumpingLvUpEventUserInfo();
	int				GetJumpingPlayerGameIDIndex();
	BOOL			GetJumpingPlayerGameID(char* szGameID);
	BOOL			IsJumpingPlayerAvatar();

	void			SendChangeFaceNotice();

	BOOL			LuckyStar_GiveReward( int iRewardIndex );
	void			CheckAndGiveSkillAndSlot();

	void			SendEventPlusCardInfo();
	SUserPingInfo*	GetUserPingInfo();


	void					SendExchangeNameAvatarList();
	void					UpdateExchangeNameItem(char* szNewGameID);
	RESULT_EXCHANGE_NAME	ExchangeNameReq(SC2S_EXCHANGE_NAME_REQ rq);

	void					SendChangeCharStatInfo();
	RESULT_CHAGNE_CHAR_STAT ChangeCharStatReq(SC2S_CHAGNE_CHAR_STAT_REQ rq);
	void			SendItemShopEventButton();

	virtual void			SendFirstCashBackEventStatus();
	virtual BOOL			IsSkyLuckyEventAllClear();
	virtual BOOL			IsGoldenSafeEventButtonOpen();
	virtual BOOL			IsHoneyWalletEventOnGoing();
	virtual BOOL			IsBigWheelLoginEventAllClear();
	void					SendRandomCardEventStatus();
	void			SetLobbyEventButtonType(BYTE btEventButtonType, BYTE btStatus);
	BYTE			GetLobbyEventButtonStatus(BYTE btEventButtonType);
	BOOL			CheckEventButtonSend(BYTE btEventButtonType, BYTE btCurrentStatus);
	void			SetCloneStatToArray(int* ipaBaseStat, BOOL bIsClone);

	RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM CheckAddShoppingBasketItemReq(SS2C_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_REQ rq, SODBCEventUserShoppingBasketItem& Item);
	RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET BuyShoppingBasket();

	void			SendEventHalfPriceUserInfo();
	void			UseHalfPrice(SC2S_EVENT_HALFPRICE_USE_REQ& req);
	BOOL			BuyHalfPriceItem(SBillingInfo* pBillingInfo, int iPayResult);
	void			SendMovePotenCardInfo(SC2S_MOVE_POTENCARD_INFO_REQ& req);
	void			SendMovePotenCardCharacterInfo(int iSelectPage);
	void			UpdateMovePotenCard(SC2S_MOVE_POTENCARD_SELECT_REQ req);

	void CheckAndGiveGameOfDiceEventRankReward();
	virtual void GiveReward_BasketBall(BYTE btBallPartType, int iBallPartIndex, int iPropertyValue);
	virtual BOOL	CheckDeveloperEventUser();

	BOOL CheckAndGiveEventRandomItemDraw_Coin();
	void EventRandomItemDrawReq(int iProductGroupIndex);

	virtual BOOL	CheckDiscountItemEventUser();
	BOOL			CheckTransformAbility(int iTransformAbilityType, int iTransformAbilityValue);

	BOOL			IsActiveLvIntervalBuffItem(int iPropertyKind, int& iUserCheckLv);

	virtual BOOL	LoadMyAvatarList();
	virtual void	SendMyAvatarList();
	virtual BOOL	GetMyAvatar(int iGameIDIndex, SMyAvatar& sAvatar);
	virtual BOOL	GetMyAvatar(char* szGameID, SMyAvatar& sAvatar);
	virtual void	UpdateAvatarExp(int iGameIDIndex, int iLv, int iExp);
	void			SendEventRankMyAvatarList();
	void			SelectEventRankMainGameID(SC2S_EVENT_RANK_MAIN_GAMEID_SELECT_REQ& rs);
	BOOL			GetMyAvatarGameID(int iGameIDIndex, SMyAvatar& sAvatar);
	BOOL			GetMyAvatarGameID(int iGameIDIndex, char* szGameID);
	void			UpdateEventRankMyAvatarName(char* szNewGameID);
	void			UpdateEventRankMyAvatarNameExchange(int iGameIDIndex_L, int iGameIDIndex_R);
	bool			UpdateMyAvatarSecretInfo(const vector<int>& vAvatar IN, bool& bOpt OUT);

	void			SetMatching(BOOL bIsMatching) { m_bIsMatching = bIsMatching; }
	BOOL			IsMatching() { return m_bIsMatching; }

	int				GetSkillTokenCount(int iCategory);
	int				GetSkillTokenConditionLevel(int iCategory);
	inline	time_t	GetAccountCreateTime(void) const {	return m_tAccountCreateTime;	}
	inline time_t	GetAccounetLastLoginTime(void) const { return m_tAccounetLastLoginTime;	}
	int				GetMaxAvatarLevel(void);
	void			CheckMaxAvatarLv(int iLv);
	void			SendHotGirlMissionStatus();

	void			LoadEventGiveMagicBallStatus();
	void			SendEventGiveMagicBallStatus();
	RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD GetEventGiveMagicBallReward();
	BOOL			ResetEventGiveMagicBallRewardTime();

	void			LoadLastPlayTime();
	void			LoadLastCheckHeartTatooTime();
	void			UpdateLastPlayTime(BYTE btMatchType);
	void			UpdateLastCheckHeartTatooTime();
	void			CheckHeartTatto();
	void			LoadLoseLeadUpdateTime();
	void			LoseLeadOrSend(BOOL bPass = FALSE);
	void			UpdateLoseLeadNotOpenToday();
	void			UpdateLoseLeadPveModeReady();
	void			SetReadyAICome(BOOL b);
	BOOL			GetReadyAICome();
	void			SetLoginRating(const float& f);
	float			GetLoginRating();
	void			SetReadyAIComePopup(int istatus);
	int				GetReadyAIComePopup();
	void			LoseLeadGiveReward();
	void			LoadLoseLeadUserTime();
	BOOL			CheckNeedPauseAndAIComePopUp();
	void			SendPromiseUserInfo();
	BOOL			LoadPromiseEvent();
	void			GivePromiseReward(const SC2S_EVENT_PROMISE_RECEIVING_REQ& rq);
	void			CheckAndUpdatePromisePlayCount();
	void			SavePromiseUserInfo(CFSEventODBC* pODBC = nullptr);
	int				GetPromiseSpecialEXP();
	void			AddAndUpdatePromiseSpecialEXP(int iExpCount);

	void			SendPVEModeOpenStatus();
	void			CheckPromiseEventInfo();
	void			ProcessPCRoomEvent();
	void			CheckAndGiveFactionDailyGameReward();
	void			ProcessKyoungLeeCharacter(int iItemIdx);
	void			LoadAndSendLobbyButtonType();
	void			SaveLobbyButtonType(BOOL bDBSave, const SC2S_LOBBY_BUTTON_TYPE_SAVE_NOT * pinfo);
	virtual void	GiveReward_IncompletePieceKey(SRewardConfig* pReward);
	void			GetDragonTigerCloneInfo(SAvatarInfo& sAvatar, int iaCloneStat[MAX_STAT_TYPE_COUNT]);
	void			GetTheOtherCloneFeature(IN const int iSpecialAvatarIndex, OUT int& iFace, OUT int iaTransformFeature[MAX_ITEMCHANNEL_NUM], OUT int iaiaTransformFeature_Show[MAX_ITEMCHANNEL_NUM] = nullptr);
	BOOL			CheckBuyUser_KyoungLeePackageItem();

	void			SendClubLeagueOpenStatus();
	BOOL			LoadClubUserRankRewardLog();
	void			GiveClubUserRankReward();
	virtual int		GetUserClubPlayCount();
	virtual	void	SetUserClubPlayCount(int iClubPlayCount);
	virtual time_t	GetUserClubPlayCountUpdateDate();
	virtual void	SetUserClubPlayCountUpdateDate(time_t tClubPlayCountUpdateDate);
	virtual int		GetUserClubCoin();
	virtual void	SetUserClubCoin(int iClubCoin);
	void			SendClubShopUserCashInfo();
	BOOL			BuyClubMark_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL			ClubSetUp_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL			ClubChangePR_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);

	void			LoadEventNewClubRewardLog();
	void			SendEventNewClubInfo();
	void			GetEventNewClubReward();

	void			LoadTopRatingCharacter();
	void			MakeTopRatingCharacterList(SS2C_USER_RATING_TOP_CHARACTER_LIST_RES& rs);
	BOOL			GetUserTopRatingCharacter(int iGamePosition, SUserTopRatingCharacter& sCharacter);
	BYTE			GetUserTopRatingCharacterRatingGrade(int iGamePosition);

	virtual void	GiveReward_SummerCandy(SRewardConfig* pReward);
	virtual void	GiveReward_PremiumPassXP(SRewardConfig* pReward);
	void			SendSpeechBubbleInfo();
	BOOL			LoadEventSpeechBubble();
	BOOL			CheckHaveBubbleItemWithManager(const int& iItemCode);
	void			UpdateBubbleItem(const int& iItemCode , int iProperty1, int iProperty2);

protected:
	// 따라가기 기능 추가
	// 상태 변경 정보 전송
	void				SendUserStatusUpdate();
	BOOL				PotentialProductSetting( int iProductIndex , BOOL bExpired = FALSE);

	BOOL				PotentialProductReSet( int iProductIndex );
	void				GetExpiredEquipPotenCard( vector<SSlotData>& rvec );
	void				GetExpiredEquipPotenCardPriceList(vector<SSlotData>& rvec, map<int /* TernmExtend */, SPOTENCARD_PRICE_INFO>& rmap);

private:
	void			SendAttendanceItemShop( int iSex );

private:


	BOOL				m_bGMUser;
	BOOL				m_bUseableBonusCoin;
	
	//////// room
	int					m_iLocation;
	
	//rc4 Key
	char				m_szRc4Key[RC4_KEY_LENGTH+1];
	//rc4 key = checksum
	char				m_szCheckSum_Rc4Key[RC4_KEY_LENGTH+1];
	//rc4 Key = Character Action 
	char				m_sz_AnyOperation_Rc4Key[RC4_KEY_LENGTH+1];

	// check sum
	CheckSum			m_CheckSum;
	
	//////// 접속동안의 게임 기록 저장
	SGameRecordLog		m_GameRecordLog;

	CFSAvatarManager	m_AvatarManager;	
	SAvatarSeasonInfo	m_AvatarSeasonInfo;
	SAvatarAdvantageRecord	m_AvatarAdvantageRecord;

	CFSGameUserItem*	m_UserItem;
	CFSGameUserSkill*	m_UserSkill;
	
	// Mail : Paper, Present 추가	
	CBaseMailList<SPaper>	m_paperList;		
	CBaseMailList<SPresent>	m_presentList;
	CBaseMailList<SPresent>	m_presentListOnAccount;
	CBaseMailList<SClubInvitationData>			m_ClubInvitationList;

	CFriendList				m_FriendList;
	CFriendAccountList		m_FriendAccountList;
	
	bool				m_bWCGPositionSelect;	// WCG

	// 2009.12.09
	// InterlockedIncrement 사용 변수에 volatile 속성 추가
	// ktKim
	int volatile		m_TestVar0;
	// End
	
	BOOL	m_bMySex;
	BOOL	m_bAnotherSex;
	

	int		m_iRefreshRoomNum;
	int		m_iGuageValue;

	SAvatarHackCheckInfo m_AvatarStatCheckInfo;
	BOOL	m_bHackUser;
	int		m_iCheckStatPacketNo;
	int		m_iCheckStatCheckSumPacketNo;
	int		m_iCheckStatLastSendPacketNo;

	int				m_iItemTypeLevelUpCount;

	void*	m_MemPointer;

	BOOL	m_bCheatAble;

	int		m_MaxLostNum;
	BOOL m_bReceiveAllFriend;
	BOOL m_bEnterChannelToSendFriend;

	BOOL m_bReceiveAllFriendAccount;
	BOOL m_bEnterChannelToSendFriendAccount;

	BYTE m_btStatCheckSeed;

	BOOL		m_bIsAvailablePointRewardCard;

	int		m_iStateBuy;

	int		m_iMatchLocation;

	SRoomListUserSet	m_RoomListUserSet;

	CJoinTeamConfigInfo	m_JoinTeamConfig;

	CLobbyChatChannel* m_pLobbyChatChannel;
	CLobbyChatChannel* m_pLobbyChatFactionChannel;

	CFSGameUserBingo	m_UserBingo;
	CUserRecordBoard m_UserRecordBoard;
	CFSGameUserSpecialSkin	m_UserSpecialSkin;
	CFSGameUserBasketBall m_UserBasketBall;

	LOCK_RESOURCE(m_csEvent);

	CProductInventory m_ProductInventory;
	CombineDataVec m_vecCombineData;
	SPausedPotenUpgradeInfo	m_PausedPotenUpgradeInfo;

	SAvatarStyleInfo	m_AvatarStyleInfo;
	vector<SSpecialPartsUserProperty*>	m_vecSpecialPartsUserProperty;

	BOOL		m_bExpirePremiumPCRoom;
	
	SGuaranteeInfo m_sGuaranteeCard;

	int		m_iUserSubPageState;
	BYTE	m_btRoundGroupIndex;		// CLUBTOURNAMENT_ROUND_GROUP_INDEX

	SAvatarSecretStoreInfo	m_SecretStoreInfo;

	CFSGameUserIntensivePractice m_UserIntensivePractice;

	SMatchingPoolCareUserInfo m_MatchingPoolCareInfo;

	BOOL m_bIsLastMatchDisconnected;
	BOOL m_bLeftSeatStatus;

	CFSGameUserLuckyBox m_UserLuckyBox;
	CFSGameUserRandomBox m_UserRandomBox;
	CFSGameUserCheerLeader m_UserCheerLeader;

	CFSGameUserThreeKingdomsEvent m_UserThreeKingdomsEvent;
	CFSGameUserHoneyWalletEvent	m_UserHoneyWalletEvent;
	list<S_ODBC_CustomizeLog> m_listCustomReceiveditem;
	CGameUserCharacterCollection m_UserCharacterCollection;
	CFSGameUserHotGirlSpecialBoxEvent m_UserHotGirlSpecialBoxEvent;
	CFSGameUserSkyLuckyEvent m_UserSkyLuckyEvent;
	CFSGameUserGoldenSafeEvent m_UserGoldenSafeEvent;
	CFSGameUserShoppingEvent	m_UserShoppingEvent;
	CFSGameUserLegendEvent	m_UserLegendEvent;
	CFSGameUserRandomArrowEvent	m_UserRandomArrowEvent;
	CFSGameUserDevilTemtationEvent m_UserDevilTemtationEvent;
	CFSGameUserVendingMachineEvent	m_UserVendingMachineEvent;
	SCloneCharacterInfo	m_sCloneCharacterInfo[Max_Clone];
	BOOL	m_bCloneCharacter;
	BOOL	m_bChangeofCharacter;
	BOOL	m_bFirstChangePCRoomItem;
	int		m_iChangePCRoomDeffCount;
	STodayHotDealBuyCount	m_UserHotDealCount;
	bool	m_bCheckPotenCardExpiredSend;
	SUserPayBackInfo m_sPayBackInfo;
	SUserGoldenCrushInfo m_sGoldenCrushInfo;
	CFSGameUserMatchingCard	m_UserMatchingCard;

	SUSEREVENTCOININFO		m_sUserEventCoinInfo;
	SEVENT_ATTENDANCEITEMSHOP_USERINFO	m_sUserAttendanceItemShop_Info;
	CFSGameUserRandomItemBoardEvent m_UserRandomItemBoardEvent;
	CFSGameUserGamePlayEvent m_UserGamePlayEvent;
	CFSGameUserRandomItemTreeEvent m_UserRandomItemTreeEvent;
	CFSGameUserLoginPayBackEvent m_UserLoginPayBackEvent;
	CGameUserPurchaseGrade m_UserPurchaseGrade;
	CFSGameUserRandomItemGroupEvent m_UserRandomItemGroupEvent;
	CFSGameUserRainbowWeekEvent m_UserRainbowWeekEvent;
	SUserEventButtonInfo	m_sUserEventButtonInfo[MAX_EVENT_BUTTON_TYPE];
	CFSGameUserGameOfDiceEvent m_UserGameOfDiceEvent;
	SUserFriendAccountInfo m_UserFriendAccountInfo;
	CFSGameUserSummerCandyEvent m_UserSummerCandyEvent;
	CFSGameUserSecretShopEvent m_UserSecretShopEvent;

	SUserPingInfo			m_UserPingInfo;
	CFSGameUserRankMatch	m_UserRankMatch;
	CFSGameUserPasswordEvent m_UserPasswordEvent;
	CFSGameUserHalloweenGamePlayEvent m_UserHalloweenGamePlayEvent;
	SEventHalfPrice_UserInfo	m_sHalfPriceInfo;

	CFSGameUserLottoEvent m_UserLottotEvent;
	CFSGameUserPresentFromDeveloperEvent m_UserPresentFromDeveloperEvent;
	CFSGameUserDiscountItemEvent m_UserDiscountItemEvent;
	CFSGameUserMiniGameZoneEvent m_UserMiniGameZoneEvent;
	CFSGameUserBigWheelLoginEvent m_UserBigWheelLoginEvent;
	CFSGameUserSalePlusEvent m_UserSalePlusEvent;
	CFSGameUserDriveMissionEvent m_UserDriveMissionEvent;
	CFSGameUserMissionMakeItemEvent m_UserMissionMakeItemEvent;
	CFSGameUserMissionCashLimitEvent m_UserMissionCashLimitEvent;
	CFSGameUserDarkMarketEvent m_UserDarkMarketEvent;
	CFSGameUserColoringPlayEvent m_UserColoringPlayEvent;
	CFSGameUserCongratulationEvent m_UserCongratulationEvent;	
	CFSGameUserHelloNewYearEvent m_UserHelloNewYearEvent;
	CFSGameUserOpenBoxEvent m_UserOpenBoxEvent;
	CFSGameUserNewbieBenefit	m_UserNewbieBenefit;
	CFSGameUserFriendInviteList	m_UserFriendInvite;
	CFSGameUserPrimonGoEvent m_UserPrimonGoEvent;
	CFSGameUserPrivateRoomEvent m_UserPrivateRoomEvent;
	CFSGameUserHippocampusEvent m_UserHippocampusEvent;
	CFSGameUserComebackBenefit m_UserComebackBenefit;
	CFSGameUserPowerupCapsule m_UserPowerupCapsule;
	CFSGameUserUnlimitedTatooEvent m_UserUnlimitedTatooEvent;
	CFSGameUserPVEMission m_UserPVEMission;
	CFSGameUserCovet	m_UserCovet;
	CFSGameUserShoppingGodEvent	m_UserShoppingGodEvent;
	CFSGameUserRandomItemDrawEvent m_UserRandomItemDrawEvent;
	CFSGameUserSelectClothesEvent m_UserSelectClothesEvent;
	CFSGameUserSubCharDevelop m_UserSubCharDevelop;
	CFSGameUserTransferJoycityPackageEvent m_UserTransferJoycityPackageEvent;
	CFSGameUserTransferJoycityShopEvent m_UserTransferJoycityShopEvent;
	CFSGameUserTransferJoycity100DreamEvent m_UserTransferJoycity100DreamEvent;

	BOOL m_bIsMatching;
	int m_iJumpingPlayerGameIDIndex;
	int m_iMaxAvatarLevel;
	bool m_bBuyKyoungLeePackge;

	time_t m_tReciveMagicballTime;
	time_t m_tLastPlayTime;	// ELO&랭킹모드
	time_t m_tLastCheckHeartTatooTime;
	int m_iHeartTatooDailyPlayCount;	// 하트문신 체크용. 오늘 경기했으면 +1(오전7시기준)
	time_t m_tLoseLeadUpdateTime;
	bool m_bLobbyButtonSave;
	BYTE m_btLobbyButtonIndex[eMENUBUTTON_TYPE_MAX]; 

	float m_fLoginRating;
	BOOL m_bReadyAICome; // ai 유도 팝업 대기자
	int m_ReadyAIComePopupStatus;
	int m_iAIComeLost;
	BOOL m_bAIComePass;
	SEVENTPROMISE_UserInfo m_sPromiseInfo;

	int m_iRevengeMatchingWinCnt;
	BYTE m_btRoomRevengeMatchingType;

	CLUBUSER_RANK_REARD_LOG_MAP m_mapClubUserRankRewardLog;

	BOOL m_bClubUserGiveEventCoin;
	int m_iIsHave_Speechbubble[MAX_STAT_TYPE_COUNT];

};


		
