qpragma once

class CFSGameUser;
class CFSGameUserSalePlusEvent
{
public:
	CFSGameUserSalePlusEvent(CFSGameUser* pUser);
	~CFSGameUserSalePlusEvent(void);
	
	BOOL			Load();
	void			SendEventInfo();
	BOOL			GetBuyInfo(BYTE btDayIndex);
	BOOL			BuySalePlusItem_AfterPay(BYTE btDayIndex);

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;
	set<BYTE>		m_setBuyDayIndex;
};

