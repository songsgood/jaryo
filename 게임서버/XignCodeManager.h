qpragma once
qinclude "Singleton.h"
qinclude "CFSGameClient.h"

class CXignCodeManager : public Singleton<CXignCodeManager>
{
public:
	CXignCodeManager(void);
	virtual ~CXignCodeManager(void);
	
	BOOL Initialize();
	void Release();

	xbool OnSend(xpvoid uid, xpvoid meta, xpcch buf, xulong size);
	void OnDisconnect(xpvoid uid, xpvoid meta, xint code, xctstr report);

	void OnAccept(CFSGameClient* pClient);
	void OnReceive(CFSGameClient* pClient, SXIGNCODE_SECURITY_DATA_NOT info);
	void OnDisconnect(CFSGameClient* pClient);

private:
	IXigncodeServerPtr m_pXignCodeServer;
	BOOL m_bEnableXignCode;
};

#define XIGNCODEMANAGER CXignCodeManager::GetInstance()