qpragma once
qinclude "Singleton.h"

class CFSGameUser;
class CUser;

class CClubTournamentAgency : public Singleton<CClubTournamentAgency>
{
public:
	CClubTournamentAgency(void);
	virtual ~CClubTournamentAgency(void);

public:
	void Update(SCL2G_UPDATE_CLUBTOURNAMENT_AGENCY_NOT& info);

	void MakeGameRoomTimeInfo(SS2C_CLUBTOURNAMENT_GAMEROOM_TIME_INFO_NOT& info);
	void MakeClubTournamentStatus(int iCurrentStatus, SS2C_CLUBTOURNAMENT_INFO_NOT& info);
	void SendClubTournamentStatus(CUser* pUser);

public:
	BYTE GetCurrentStatus()		{ return m_btCurrentStatus; }
	int GetCurrentMonth()		{ return m_iCurrentMonth; }

private:
	void SetClubTournamentScheduleInfo(CLUBTOURNAMENT_SCHEDULEINFO& info);
	void GetClubTournamentScheduleInfo(CLUBTOURNAMENT_SCHEDULEINFO& info);

private:
	BYTE m_btCurrentStatus;			// CLUBTOURNAMENT_STATUS
	int m_iCurrentMonth;

	CLUBTOURNAMENT_SCHEDULEINFO m_Schedule[MAX_MONTH_COUNT];	// 1�� ����
};

#define CLUBTOURNAMENTAGENCY CClubTournamentAgency::GetInstance()