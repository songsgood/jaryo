qinclude "stdafx.h"
qinclude "UnlimitedTatooEventManager.h"
qinclude "FSGameUserUnlimitedTatooEvent.h"
qinclude "TUser.h"
qinclude "FSServer.h"
qinclude "RewardManager.h"
qinclude "CFSGameServer.h"

CFSGameUserUnlimitedTatooEvent::CFSGameUserUnlimitedTatooEvent(CUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserUnlimitedTatooEvent::~CFSGameUserUnlimitedTatooEvent(void)
{
}

BOOL CFSGameUserUnlimitedTatooEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == UNLIMITEDTATOO.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<int> vRewardIndex;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_UNLIMITED_USER_GetRewardLog(m_pUser->GetUserIDIndex(), vRewardIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_UNLIMITED_TATOO_EVENT, INITIALIZE_DATA, FAIL, "EVENT_UNLIMITED_USER_GetRewardLog error");
		return FALSE;
	}

	for(int i = 0; i < vRewardIndex.size(); ++i)
	{
		m_setRewardLog.insert(UNLIMITED_TATOO_REWARD_LOG_SET::value_type(vRewardIndex[i]));
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserUnlimitedTatooEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == UNLIMITEDTATOO.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CPacketComposer Packet(S2C_EVENT_UNLIMITED_TATOO_INFO_RES);
	UNLIMITEDTATOO.MakeEventInfo(Packet, m_setRewardLog);

	m_pUser->Send(&Packet);
}

RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD CFSGameUserUnlimitedTatooEvent::GetRewardReq(int iRewardIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == UNLIMITEDTATOO.IsOpen(), RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_FAIL_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_FAIL);

	UNLIMITED_TATOO_REWARD_LOG_SET::iterator iter = m_setRewardLog.find(iRewardIndex);
	CHECK_CONDITION_RETURN(iter != m_setRewardLog.end(), RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_FAIL_ALREADY_GET_REWARD);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_FAIL_MAILBOX_FULL);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_UNLIMITED_USER_InsertRewardLog(m_pUser->GetUserIDIndex(), iRewardIndex), RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_FAIL_ALREADY_GET_REWARD);

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	if(NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_UNLIMITED_TATOO_EVENT, DB_DATA_UPDATE, FAIL, "CFSGameUserUnlimitedTatooEvent::GetRewardReq - UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), iRewardIndex);
urn RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_FAIL;
	}

	m_setRewardLog.insert(UNLIMITED_TATOO_REWARD_LOG_SET::value_type(iRewardIndex));

	return RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD_SUCCESS;
}
