qinclude "stdafx.h"
qinclude <CFSCommonFunc.h>
qinclude "CFSGameServer.h"
qinclude "XignCodeManager.h"
qinclude "CContentsManager.h"

CXignCodeManager::CXignCodeManager(void)
	:m_pXignCodeServer(NULL)
	,m_bEnableXignCode(FALSE)
{

}

CXignCodeManager::~CXignCodeManager(void)
{
	Release();
}

BOOL CXignCodeManager::Initialize()
{
	FLOAT fEnable = 0;
	CHECK_CONDITION_RETURN(FALSE == CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_CHECK_XIGNCODE, 0, fEnable) ||
		0 == fEnable, FALSE);

	m_bEnableXignCode = TRUE;

	CreateXigncodeServer CXProc = LoadHelperDll(NULL);
	if(!CXProc)
	{
		WRITE_LOG_NEW(LOG_TYPE_XIGNCODE, INVALED_DATA, FAIL, "ErrorCode:00a41316", GetLastError());
turn FALSE;
	}

	if(!CXProc(&m_pXignCodeServer, SendProc, DisconnectProc))
	{
		WRITE_LOG_NEW(LOG_TYPE_XIGNCODE, INVALED_DATA, FAIL, "ErrorCode:00a41316", GetLastError());
turn FALSE;
	}

	if(!ZSWAVE_SendCommand(ZCMD_SERVER32_ON_BEGIN, m_pXignCodeServer, XIGNCODE_PACKET_SIZE))
	{
		WRITE_LOG_NEW(LOG_TYPE_XIGNCODE, INVALED_DATA, FAIL, "ErrorCode:00a41316", GetLastError());
turn FALSE;
	}

	return TRUE;
}

void CXignCodeManager::Release()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bEnableXignCode);

	ZSWAVE_SendCommand(ZCMD_SERVER32_ON_END, m_pXignCodeServer);
	ZSWAVE_SendCommand(ZCMD_SERVER32_RELEASE, m_pXignCodeServer);
}

xbool CXignCodeManager::OnSend(xpvoid uid, xpvoid meta, xpcch buf, xulong size)
{
	CHECK_CONDITION_RETURN(FALSE == m_bEnableXignCode, FALSE);

	CFSGameClient* pClient = (CFSGameClient*)uid;
	if(pClient == NULL)
	{
		ZSWAVE_SendCommand(ZCMD_SERVER32_ON_DISCONNECT, m_pXignCodeServer, uid);
		return FALSE;
	}

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if(NULL == pUser)
	{
		ZSWAVE_SendCommand(ZCMD_SERVER32_ON_DISCONNECT, m_pXignCodeServer, uid);
		return FALSE;
	}

	if(0 < pUser->GetTestLogout())
	{
		ZSWAVE_SendCommand(ZCMD_SERVER32_ON_DISCONNECT, m_pXignCodeServer, uid);
		return FALSE;
	}

	SXIGNCODE_SECURITY_DATA_NOT info;
	memcpy(info.data, buf, size);
	info.dataSize = size;

	pClient->SendPacket(S2C_XIGNCODE_SECURITY_DATA_NOT, &info, sizeof(SXIGNCODE_SECURITY_DATA_NOT));

	return TRUE;
}

void CXignCodeManager::OnDisconnect(xpvoid uid, xpvoid meta, xint code, xctstr report)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bEnableXignCode);

	ZSWAVE_SendCommand(ZCMD_SERVER32_ON_DISCONNECT, m_pXignCodeServer, uid);

	CFSGameClient* pClient = (CFSGameClient*)uid;
	if(pClient != NULL)
	{
		pClient->DisconnectClient();
	}
}

void CXignCodeManager::OnAccept(CFSGameClient* pClient)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bEnableXignCode);
	CHECK_NULL_POINTER_VOID(pClient);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	xpvoid uid = pClient;
	ZSWAVE_SendCommand(ZCMD_SERVER32_ON_ACCEPT, m_pXignCodeServer, uid, NULL);

	xulong ip_address = inet_addr(pUser->GetIPAddress());
	ZSWAVE_SendCommand(ZCMD_SERVER32_SET_USER_INFORMATION_A, m_pXignCodeServer, uid, ip_address, pUser->GetUserID());
}

void CXignCodeManager::OnReceive(CFSGameClient* pClient, SXIGNCODE_SECURITY_DATA_NOT info)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bEnableXignCode);
	CHECK_NULL_POINTER_VOID(pClient);

	xpvoid uid = pClient;
	ZSWAVE_SendCommand(ZCMD_SERVER32_ON_RECEIVE, m_pXignCodeServer, uid, info.data, XIGNCODE_PACKET_SIZE);
}

void CXignCodeManager::OnDisconnect(CFSGameClient* pClient)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bEnableXignCode);
	CHECK_NULL_POINTER_VOID(pClient);

	xpvoid uid = pClient;
	ZSWAVE_SendCommand(ZCMD_SERVER32_ON_DISCONNECT, m_pXignCodeServer, uid);
}




