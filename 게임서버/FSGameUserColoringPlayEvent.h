qpragma once

class CFSGameUser;
class CFSGameUserColoringPlayEvent
{
public:
	CFSGameUserColoringPlayEvent(CFSGameUser* pUser);
	~CFSGameUserColoringPlayEvent(void);

	BOOL					Load();
	BOOL					CheckResetDate(time_t tCurrentTime = _time64(NULL), BOOL bIsLogin = FALSE);
	void					SendEventInfo();
	void					SendUserInfo();
	BOOL					IsFullPencilSlot();
	BOOL					CheckAndUpdateMission(BYTE btMissionConditionType, time_t tCurrentTime = _time64(NULL));
	BOOL					CheckGamePlayMission(BOOL bIsWin, BOOL bIsDisconnectedMatch, BOOL bRunAway);
	BOOL					SaveStartConnectTime();
	BOOL					UsePencil(BYTE btPictureIndex, BYTE btColorIndex);
	BOOL					GetReward(BYTE btProductIndex);
	BOOL					CheckMainStatus(BYTE btPictureIndex);

private:
	CFSGameUser*			m_pUser;
	BOOL					m_bDataLoaded;

	time_t					m_tLastResetTime;
	time_t					m_tStartConnectTime;
	int						m_iConnectSecond;
	int						m_iPencilSlot;
	BYTE					m_btTodayUsePencil;
	BYTE					m_btMainStatus;
	int						m_iColorFlag[ColoringPlayEvent::PICTURE_CNT];
	BYTE					m_btStatus[ColoringPlayEvent::PICTURE_CNT];
};

