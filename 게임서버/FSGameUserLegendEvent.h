qpragma once

class CFSGameUser;
class CFSODBCBase;

typedef map<int/*iRewardIndex*/, SLegendTryRewardUserInfo> LEGEND_TRY_REWARD_USER_INFO_MAP;

class CFSGameUserLegendEvent
{
public:
	CFSGameUserLegendEvent(CFSGameUser* pUser);
	~CFSGameUserLegendEvent(void);

	BOOL				Load();
	void				SendLegendEventInfo();
	void				SendLegendUserInfo();
	void				SetLegendKeyCount(int iCount);
	BOOL				BuyLegendKey_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL				UseKey(SC2S_LEGEND_EVENT_USE_KEY_REQ& rs);
	BOOL				UseStar(SC2S_LEGEND_EVENT_USE_STAR_REQ& rs);
	eLEGEND_GET_TRY_REWARD_ITEM_RESULT GetTryRewardReq(int iRewardIndex);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;
	BOOL				m_bIsFirstLogin;	
	SLegendUserInfo		m_sUserInfo;
	LEGEND_TRY_REWARD_USER_INFO_MAP m_mapTryRewardInfo;
};

