qinclude "stdafx.h"
qinclude "FSGameUserBasketBall.h"
qinclude "CFSGameUser.h"
qinclude "CFSGameServer.h"
qinclude "ThreadODBCManager.h"
qinclude "BasketBallManager.h"
qinclude "CBillingManagerGame.h"
qinclude "RandomCardShopManager.h"
qinclude "ClubConfigManager.h"

CFSGameUserBasketBall::CFSGameUserBasketBall(CFSGameUser* pUser)
	: m_pUser(pUser) 
{
}

CFSGameUserBasketBall::~CFSGameUserBasketBall()
{

}

BOOL CFSGameUserBasketBall::Load()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	// TODO 한시적 농구공 체크 및 삭제 SP 추가.

	BOOL bUseOption = FALSE;
	int iaBallPartSlot[MAX_BASKETBALL_BALLPART] ={-1, -1, -1};
	BALLPARTINFO_VEC	vecInventory[MAX_BASKETBALL_BALLPART];
	
	if( ODBC_RETURN_SUCCESS != pODBCBase->BASKETBALL_GetUserSlot(m_pUser->GetGameIDIndex(), bUseOption, iaBallPartSlot) )
	{
		WRITE_LOG_NEW(LOG_TYPE_BASKETBALL, INITIALIZE_DATA, FAIL, "Load-BASKETBALL_GetUserSlot\tGameIDIndex:10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}
	if( ODBC_RETURN_SUCCESS != pODBCBase->BASKETBALL_GetUserInventory(m_pUser->GetGameIDIndex(), vecInventory) )
	{
		WRITE_LOG_NEW(LOG_TYPE_BASKETBALL, INITIALIZE_DATA, FAIL, "Load-BASKETBALL_GetUserInventory\tGameIDIndex:10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}
	SetInventory(bUseOption, iaBallPartSlot, vecInventory);

	SendUserUseOption();

	return TRUE;
}

void CFSGameUserBasketBall::SetInventory(BOOL bUseOption, int iaBallPartSlot[MAX_BASKETBALL_BALLPART], vector<SBasketBallInventoryInfo> vecInventory[MAX_BASKETBALL_BALLPART])
{
	CLock lock(&m_SyncUserBasketBall);

	m_bUseOption = bUseOption;

	for(int i = BASKETBALL_BALLPART_BACKGROUND; i < MAX_BASKETBALL_BALLPART; ++i)
	{
		m_iaBallPartSlot[i] = iaBallPartSlot[i];

		BALLPARTINFO_VEC::const_iterator cIterVec		= vecInventory[i].begin();
		BALLPARTINFO_VEC::const_iterator cIterVecEnd	= vecInventory[i].end();

		for(; cIterVec != cIterVecEnd; ++cIterVec)
		{
			SBasketBallInventoryInfo sInventoryInfo;
			sInventoryInfo.iBallPartIndex = cIterVec->iBallPartIndex;
			sInventoryInfo.tExpireDate = cIterVec->tExpireDate;

			m_vecInventory[i].push_back(sInventoryInfo);
			m_mapInventory[i].insert(std::make_pair(cIterVec->iBallPartIndex, cIterVec->tExpireDate));
		}
	}
}

BOOL CFSGameUserBasketBall::SendUserUseOption()
{
	SS2C_BAKSETBALL_USERINFO_NOT	ss;
	CPacketComposer Packet(S2C_BAKSETBALL_USERINFO_NOT);

	BOOL bUseOption = FALSE;
	GetBallPartSlotInfo(bUseOption, ss.iaBallPartIndex);
	ss.bUseOption = bUseOption;

	Packet.Add((PBYTE)&ss, sizeof(SS2C_BAKSETBALL_USERINFO_NOT));

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserBasketBall::SendUserInventory(BYTE btBallPartType)
{	
	CHECK_CONDITION_RETURN(MAX_BASKETBALL_BALLPART <= btBallPartType, FALSE);

	CheckAndGiveConditionBallSkin(btBallPartType);
	CheckAndRemoveExpireBallPartInventory(btBallPartType);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);
	
	SS2C_BASKETBALL_MY_LIST_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_BASKETBALL_MY_LIST_RES));

	ss.btBallPartType = btBallPartType;
	ss.iCount = 0;

	CPacketComposer Packet(S2C_BASKETBALL_MY_LIST_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_BASKETBALL_MY_LIST_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	SMY_BALLPART_INFO sMyInfo;
	time_t tCurrentDate = _time64(NULL);

	BEGIN_SCOPE_LOCK(m_SyncUserBasketBall)
	for(BYTE btStatus = BALLPART_WEAR; btStatus < MAX_BALLPART_STATUS; ++btStatus)
	{
		sMyInfo.btStatus = btStatus;

		switch(btStatus)
		{
		case BALLPART_WEAR:
			{
				if(m_iaBallPartSlot[btBallPartType] <= 0)
					continue;

				sMyInfo.iBallPartIndex = m_iaBallPartSlot[btBallPartType];

				BALLPARTINFO_MAP::const_iterator cIterMap = m_mapInventory[btBallPartType].find(sMyInfo.iBallPartIndex);
				if(cIterMap != m_mapInventory[btBallPartType].end())
				{
					sMyInfo.iRemainSec = (cIterMap->second != -1) ? cIterMap->second - tCurrentDate : -1;
				}

				Packet.Add((PBYTE)&sMyInfo, sizeof(SMY_BALLPART_INFO));

				ss.iCount++;
			}
			break;

		case BALLPART_HAVE:
			{
				BALLPARTINFO_VEC::const_iterator cIterVec		= m_vecInventory[btBallPartType].begin();
				BALLPARTINFO_VEC::const_iterator cIterVecEnd	= m_vecInventory[btBallPartType].end();
				for(; cIterVec != cIterVecEnd; ++cIterVec)
				{
					if(m_iaBallPartSlot[btBallPartType] == cIterVec->iBallPartIndex)
						continue;

					sMyInfo.iBallPartIndex = cIterVec->iBallPartIndex;
					sMyInfo.iRemainSec = (cIterVec->tExpireDate != -1) ? cIterVec->tExpireDate - tCurrentDate : -1;
					Packet.Add((PBYTE)&sMyInfo, sizeof(SMY_BALLPART_INFO));

					ss.iCount++;
				}
			}
			break;

		case BALLPART_LOCK:
			{
				BASKETBALL_CONDITIONLIST_MAP mapConditionList = pBasketBallManager->GetConditionListMap(btBallPartType);

				BASKETBALL_CONDITIONLIST_MAP::const_iterator cIterMap		= mapConditionList.begin();
				BASKETBALL_CONDITIONLIST_MAP::const_iterator cIterMapEnd	= mapConditionList.end();
				for(; cIterMap != cIterMapEnd; ++cIterMap)
				{
					BALLPARTINFO_MAP::const_iterator cIterSet = m_mapInventory[btBallPartType].find(cIterMap->first);
					if(cIterSet != m_mapInventory[btBallPartType].end())
						continue;

					sMyInfo.iBallPartIndex = cIterMap->first;
					sMyInfo.iRemainSec = -1;
					Packet.Add((PBYTE)&sMyInfo, sizeof(SMY_BALLPART_INFO));

					ss.iCount++;
				}
			}
			break;
		}		
	}
	END_SCOPE_LOCK

	memcpy(pCountLocation, &ss.iCount, sizeof(int));

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserBasketBall::SendBasketBallShopList(BYTE btBallPartType, BYTE btItemType)
{
	CHECK_CONDITION_RETURN(MAX_BASKETBALL_BALLPART <= btBallPartType, FALSE);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);

	SS2C_BASKETBALL_SHOP_LIST_RES	ss;
	ss.btBallPartType = btBallPartType;
	CPacketComposer Packet(S2C_BASKETBALL_SHOP_LIST_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_BASKETBALL_SHOP_LIST_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	BEGIN_SCOPE_LOCK(m_SyncUserBasketBall)
		
		SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
		CHECK_NULL_POINTER_BOOL(pAvatar);

		pBasketBallManager->MakePacketBasketBallShopList(btBallPartType, btItemType, ss.iCount, Packet, m_mapInventory[btBallPartType], pAvatar->iClubLeagueSeasonRank);

	END_SCOPE_LOCK


	memcpy(pCountLocation, &ss.iCount, sizeof(int));

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserBasketBall::SendBasketBallProductInfo(SC2S_BASKETBALL_PRODUCT_INFO_REQ& rs)
{
	CHECK_CONDITION_RETURN(MAX_BASKETBALL_BALLPART <= rs.btBallPartType, FALSE);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);

	SS2C_BASKETBALL_PRODUCT_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_BASKETBALL_PRODUCT_INFO_RES));
	ss.btBallPartType = rs.btBallPartType;
	ss.iBallPartIndex = rs.iBallPartIndex;
	ss.iUserGuaranteeValue = GUARANTEECARD_NOGOT;
	BOOL bIsRandom = FALSE;

	pBasketBallManager->GetBasketBallProductRewardInfo(rs.btBallPartType, rs.iBallPartIndex, ss.iRewardValue, bIsRandom);
	
	vector<SBasketBallShopPriceInfo> vecInfo;
	pBasketBallManager->GetBasketBallSkinPriceInfo(rs.btBallPartType, rs.iBallPartIndex, vecInfo);

	ss.iPriceCount = vecInfo.size();

	if(bIsRandom)
	{
		SGuaranteeInfo sGuaranteeInfo;
		int iLimit = 0;
		BYTE btCategory = rs.btBallPartType + 7;

		if(GUARANTEE_SUCCESS == RANDOMCARDSHOP.GetGuaranteeCardInfo(m_pUser->GetUserIDIndex(), sGuaranteeInfo) &&
			TRUE == RANDOMCARDSHOP.GetGuaranteeCardLimitAndPrice(btCategory-1, iLimit, ss.iGuaranteeCardPrice))
		{
			m_pUser->SetGuaranteeCardInfo(sGuaranteeInfo);
			ss.iUserGuaranteeValue = m_pUser->GetGuaranteeCardValue(btCategory);
		}
	}

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_CONDITION_RETURN(NULL == pAvatar, FALSE);

	CPacketComposer Packet(S2C_BASKETBALL_PRODUCT_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_BASKETBALL_PRODUCT_INFO_RES));

	SBASKETBALL_PRICE_INFO sPriceInfo;

	for(int i = 0; i < ss.iPriceCount; ++i)
	{
		sPriceInfo.iPropertyValue	= vecInfo[i].iPropertyValue;
		sPriceInfo.iSellType		= vecInfo[i].iSellType - 1;
		sPriceInfo.iPrice			= vecInfo[i].iPrice;
		sPriceInfo.iSalePrice		= vecInfo[i].iSalePrice;

		// 클럽 아이템일 경우는 세일가격이 있어도 무시. 클럽 리그 세일가격 적용
		if(BASKETBALL_SHOP_ITEM_TYPE_CLUB == pBasketBallManager->GetBasketBallItemType(rs.btBallPartType, rs.iBallPartIndex))
			sPriceInfo.iPrice = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, sPriceInfo.iPrice);

		Packet.Add((PBYTE)&sPriceInfo, sizeof(SBASKETBALL_PRICE_INFO));
	}

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserBasketBall::SendNewMarkInfo()
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);

	SS2C_BASKETBALL_EVENTBUTTON_RENDER_NOT not;
	not.btOpenStatus = pBasketBallManager->IsEventButtonOpen();

	if( FALSE == m_pUser->CheckEventButtonSend(EVENT_BUTTON_TYPE_BASKETBALL, not.btOpenStatus))
		return FALSE;

	CPacketComposer Packet(S2C_BASKETBALL_EVENTBUTTON_RENDER_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_BASKETBALL_EVENTBUTTON_RENDER_NOT));

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserBasketBall::ChangeBasketBallSkin(int iaBallPartIndex[MAX_BASKETBALL_BALLPART])
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	SS2C_CHANGE_BASKETBALL_SKIN_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_CHANGE_BASKETBALL_SKIN_RES));

	ss.btResult = BASKETBALL_CHANGE_SKIN_SUCCESS;

	if(iaBallPartIndex[BASKETBALL_BALLPART_BACKGROUND] <= 0)
	{
		ss.btResult = BASKETBALL_CHANGE_SKIN_BACKGROUND_CANNOT_REMOVE_FAIL;
	}
	else
	{
		CLock lock(&m_SyncUserBasketBall);

		for(int i = BASKETBALL_BALLPART_BACKGROUND; i < MAX_BASKETBALL_BALLPART; ++i)
		{
			if(iaBallPartIndex[i] > 0)
			{
				BALLPARTINFO_MAP::const_iterator cIterMap = m_mapInventory[i].find(iaBallPartIndex[i]);
				if(cIterMap == m_mapInventory[i].end())
				{
					ss.btResult = BASKETBALL_CHANGE_SKIN_NO_HAVE_FAIL;
					break;
				}
			}			
			ss.iaBallPartIndex[i] =	iaBallPartIndex[i];
		}
	}	

	if(BASKETBALL_CHANGE_SKIN_SUCCESS == ss.btResult)
	{
		if(ODBC_RETURN_SUCCESS != pODBCBase->BASKETBALL_UpdateUserSlot(m_pUser->GetGameIDIndex(), iaBallPartIndex))
		{
			ss.btResult = BASKETBALL_CHANGE_SKIN_DB_FAIL;
		}
		else
		{
			CLock lock(&m_SyncUserBasketBall);

			memcpy(m_iaBallPartSlot, ss.iaBallPartIndex, sizeof(int) * MAX_BASKETBALL_BALLPART);
			m_bUseOption = TRUE;
		}

		if(m_pUser->GetLocation() == U_TEAM)
		{
			m_pUser->SendAvatarInfoUpdateToMatch();
		}
	}

	m_pUser->Send(S2C_CHANGE_BASKETBALL_SKIN_RES, &ss, sizeof(SS2C_CHANGE_BASKETBALL_SKIN_RES));

	return TRUE;
}

BOOL CFSGameUserBasketBall::ChangeBasketBallOption(BOOL bUseOption)
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	SS2C_CHANGE_BASKETBALL_OPTION_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_CHANGE_BASKETBALL_OPTION_RES));

	ss.bResult = true;

	if(ODBC_RETURN_SUCCESS != pODBCBase->BASKETBALL_UpdateUserOption(m_pUser->GetGameIDIndex(), bUseOption))
	{
		ss.bResult = false;
	}
	else
	{
		CLock lock(&m_SyncUserBasketBall);
		
		m_bUseOption = bUseOption;
		ss.bUseOption = bUseOption;
	}

	m_pUser->SendAvatarInfoUpdateToMatch();
	m_pUser->Send(S2C_CHANGE_BASKETBALL_OPTION_RES, &ss, sizeof(SS2C_CHANGE_BASKETBALL_OPTION_RES));

	return TRUE;
}

BOOL CFSGameUserBasketBall::BuyBasketBallSkin(SC2S_BUY_BASKETBALL_REQ& rs)
{
	CHECK_CONDITION_RETURN(MAX_BASKETBALL_BALLPART <= rs.btBallPartType, FALSE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);

	SS2C_BUY_BASKETBALL_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_BUY_BASKETBALL_RES));

	CHECK_CONDITION_RETURN(BASKETBALL_BALLPART_EFFECT == rs.btBallPartType && BLACK_LIGHTNING_EFFECT_BALL_INDEX == rs.iBallPartIndex, FALSE);

	ss.btResult = BASKETBALL_BUY_RESULT_SUCCESS;
	
	BEGIN_SCOPE_LOCK(m_SyncUserBasketBall)
	if(m_mapInventory[rs.btBallPartType].end() != m_mapInventory[rs.btBallPartType].find(rs.iBallPartIndex))
	{
		ss.btResult = BASKETBALL_BUY_RESULT_ALREADY_HAVE_FAIL;

		// 같은 문양이다(빙고쿠폰 농구공)
		if(rs.iBallPartIndex == 110014 || rs.iBallPartIndex == 110020)
		{
			int iBallPartIndex = 110014;
			if(rs.iBallPartIndex == 110014)
				iBallPartIndex = 110020;

			if(m_mapInventory[rs.btBallPartType].end() != m_mapInventory[rs.btBallPartType].find(rs.iBallPartIndex))
			{
				ss.btResult = BASKETBALL_BUY_RESULT_ALREADY_HAVE_FAIL;
			}
		}
	}
	END_SCOPE_LOCK
	
	if(BASKETBALL_BUY_RESULT_SUCCESS == ss.btResult)
	{
		if(BASKETBALL_SHOP_ITEM_TYPE_CLUB == pBasketBallManager->GetBasketBallItemType(rs.btBallPartType, rs.iBallPartIndex))
		{
			if(0 >= m_pUser->GetClubSN())
			{
				ss.btResult = BASKETBALL_BUY_RESULT_NOT_CLUBUSER;
			}
			else
			{
				ss.btResult = CLUBCONFIGMANAGER.CanBuyClubBasketball(rs.iBallPartIndex, m_pUser->GetClubGrade(), m_pUser->GetClubContributionPoint());
			}
		}
	}

	int iSellType = rs.iSellType + 1;	// 서버의 판매타입으로 조정.
	int iSkinPrice = pBasketBallManager->GetBasketBallSkinPrice(rs.btBallPartType, rs.iBallPartIndex, iSellType, rs.iPropertyValue);

	// 클럽 아이템일 경우는 세일가격이 있어도 무시. 클럽 리그 세일가격 적용 
	if(BASKETBALL_SHOP_ITEM_TYPE_CLUB == pBasketBallManager->GetBasketBallItemType(rs.btBallPartType, rs.iBallPartIndex))
		iSkinPrice = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, iSkinPrice);

	if(BASKETBALL_BUY_RESULT_SUCCESS == ss.btResult)
	{
		if(iSkinPrice <= 0)
		{
			ss.btResult = BASKETBALL_BUY_RESULT_WRONG_PRICE_FAIL;
		}
		else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
		{
			ss.btResult = BASKETBALL_BUY_RESULT_MAILBOX_FULL_FAIL;
		}
		else if(SELL_TYPE_CASH == iSellType &&
			m_pUser->GetCoin() + m_pUser->GetEventCoin() < iSkinPrice)
		{
			ss.btResult = BASKETBALL_BUY_RESULT_LACK_MONEY_FAIL;
		}
		else if(SELL_TYPE_POINT == iSellType &&
			m_pUser->GetSkillPoint() < iSkinPrice)
		{
			ss.btResult = BASKETBALL_BUY_RESULT_LACK_MONEY_FAIL;
		}
		// 다른 판매타입에 대해서 추가작업 필요.
		else
		{
			SBillingInfo BillingInfo;

			BillingInfo.pUser = m_pUser;
			BillingInfo.iEventCode = EVENT_BUY_BASKETBALL;
			BillingInfo.iPropertyType = ITEM_PROPERTY_TIME;

			BillingInfo.iItemCode = rs.iBallPartIndex;
			BillingInfo.iSellType = iSellType;

			if(SELL_TYPE_CASH == iSellType)
				BillingInfo.iCashChange = iSkinPrice;
			else if(SELL_TYPE_POINT == iSellType)
				BillingInfo.iPointChange = iSkinPrice;

			BillingInfo.iSerialNum = m_pUser->GetUserIDIndex();
			memcpy(BillingInfo.szUserID, m_pUser->GetUserID(), MAX_USERID_LENGTH+1);
			memcpy( BillingInfo.szGameID, m_pUser->GetGameID(), MAX_GAMEID_LENGTH + 1 );	
			memcpy( BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1 );

			BillingInfo.iParam0 = rs.btBallPartType;
			BillingInfo.iParam1 = ss.btResult;
			BillingInfo.iParam2 = rs.iPropertyValue;
			BillingInfo.iParam3 = (int)(pBasketBallManager->IsRandomBasketBall(rs.btBallPartType, rs.iBallPartIndex));

			BillingInfo.iCurrentCash = m_pUser->GetCoin();

			char* pItemName = pBasketBallManager->GetBasketBallName(rs.iBallPartIndex);
			if( NULL == pItemName )
			{
				sprintf_s(BillingInfo.szItemName, _countof(BillingInfo.szItemName), "BasketBall_");
			}
			else
			{
				char sztemp[MAX_ITEMNAME_LENGTH+1] = {NULL,};

				sprintf_s(sztemp, _countof(BillingInfo.szItemName), "BasketBall_");
				strcat_s(sztemp, _countof(sztemp), pItemName);

				memcpy( BillingInfo.szItemName, sztemp, MAX_ITEMNAME_LENGTH + 1 );
			}

			BillingInfo.iItemPrice = iSkinPrice;

			if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
			{
				return FALSE;
			}
			if( BillingInfo.iCashChange > 0 && BillingInfo.iSellType == SELL_TYPE_CASH )
			{
				m_pUser->SetItemCode( BillingInfo.iItemCode );
				m_pUser->SetPayCash( BillingInfo.iCashChange );
				m_pUser->CheckEvent(PERFORM_TIME_ITEMBUY,pODBCBase);
				m_pUser->ResetUserData();
			}
		}
	}

	if(BASKETBALL_BUY_RESULT_SUCCESS != ss.btResult)
	{
		CPacketComposer Packet(S2C_BUY_BASKETBALL_RES);
		Packet.Add((PBYTE)&ss, sizeof(SS2C_BUY_BASKETBALL_RES));
		m_pUser->Send(&Packet);
	}

	return TRUE;
}


BOOL CFSGameUserBasketBall::BuyBasketBallSkin_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if( FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)) )
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, INVALED_DATA, CHECK_FAIL, "BuyBasketBallSkin_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);
	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);

	char* szUserID = pBillingInfo->szUserID;
	int iEventCode = pBillingInfo->iEventCode;
	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szGameID = pBillingInfo->szGameID;	
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iBallPartIndex = pBillingInfo->iItemCode;
	int iPrevMoney = 0;
	int iSellType = pBillingInfo->iSellType;
	int iSkinPrice = 0;
	if(SELL_TYPE_CASH == iSellType)
	{
		iPrevMoney = pBillingInfo->iCurrentCash;
		iSkinPrice = pBillingInfo->iCashChange;
	}
	else if(SELL_TYPE_POINT == iSellType)
	{
		iPrevMoney = pUser->GetSkillPoint();
		iSkinPrice = pBillingInfo->iPointChange;
	}
	int iPostMoney = iPrevMoney - iSkinPrice;
	SS2C_BUY_BASKETBALL_RES ss;
	ZeroMemory(&ss, sizeof(SS2C_BUY_BASKETBALL_RES));

	BYTE btBallPartType = pBillingInfo->iParam0;
	ss.btResult = pBillingInfo->iParam1;
	int iPeriod = pBillingInfo->iParam2;
	BOOL bIsRandom = (BOOL)(pBillingInfo->iParam3);

	if( iPayResult == PAY_RESULT_SUCCESS &&
		BASKETBALL_BUY_RESULT_SUCCESS == ss.btResult)
	{
		ss.iUserGuaranteeValue = GUARANTEECARD_NOGOT;
		BYTE btCategory = btBallPartType + 7;
		SGuaranteeInfo sGuaranteeInfo;
		SBasketBallShopRewardInfo sRewardInfo;
		int iGuaranteeUpdateType = -1;

		if(bIsRandom) // 뽑기 구매
		{
			if(GUARANTEE_SUCCESS == RANDOMCARDSHOP.GetGuaranteeCardInfo(pUser->GetUserIDIndex(), sGuaranteeInfo))
			{
				pUser->SetGuaranteeCardInfo(sGuaranteeInfo);
				ss.iUserGuaranteeValue = pUser->GetGuaranteeCardValue(btCategory);
			}

			if(ss.iUserGuaranteeValue == 1) // 메인보상 당첨되야함
			{
				ss.btRewardType = BASKETBALL_SHOP_REWARD_MAIN;
			}
			else if(TRUE == pBasketBallManager->GetBasketBallRandomRewardInfo(btBallPartType, iBallPartIndex, sRewardInfo))
			{
				ss.btRewardType = sRewardInfo.btShopRewardType;
				ss.iRewardValue = sRewardInfo.iPropertyValue;
			}

			if(ss.iUserGuaranteeValue != GUARANTEECARD_NOGOT)
			{
				iGuaranteeUpdateType = (BASKETBALL_SHOP_REWARD_MAIN == ss.btRewardType) ? GUARANTEE_UPDATE_TYPE_INIT : GUARANTEE_UPDATE_TYPE_BUY;
			}
		}
		else // 일반 구매
		{
			ss.btRewardType = BASKETBALL_SHOP_REWARD_MAIN;
			ss.iRewardValue = iPeriod;
		}

		int iSortNum = 0;
		time_t tCurrentDate = _time64(NULL);
		time_t tExpireDate = tCurrentDate;

		if(ODBC_RETURN_SUCCESS == pODBCBase->BASKETBALL_BuyBasketBallSkin(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), iBallPartIndex, ss.btRewardType, iSortNum
			, iSellType, iPrevMoney, iPostMoney, sRewardInfo.btPropertyType, sRewardInfo.iPropertyValue, iGuaranteeUpdateType, btCategory-1, pBillingInfo->iItemPrice, iPeriod, tCurrentDate, tExpireDate))
		{
			if(iGuaranteeUpdateType > -1)
			{
				RANDOMCARDSHOP.GetGuaranteeCardInfo(pUser->GetUserIDIndex(), sGuaranteeInfo);
				pUser->SetGuaranteeCardInfo(sGuaranteeInfo);

				ss.iUserGuaranteeValue = pUser->GetGuaranteeCardValue(btCategory);
			}	

			if(BASKETBALL_SHOP_REWARD_MAIN == ss.btRewardType)
			{
				SBasketBallInventoryInfo sInventoryInfo;
				sInventoryInfo.iBallPartIndex = iBallPartIndex;
				sInventoryInfo.tExpireDate = (iPeriod > 0) ? tExpireDate : -1;

				CLock lock(&m_SyncUserBasketBall);

				m_vecInventory[btBallPartType].insert(m_vecInventory[btBallPartType].begin() + iSortNum, sInventoryInfo);
				m_mapInventory[btBallPartType].insert(std::make_pair(iBallPartIndex, tExpireDate));
			}			
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_BASKETBALL, DB_DATA_UPDATE, FAIL, "BuyBasketBallSkin_AfterPay - BallPartIndex:10752790, UserID:(null), GameID:", 
lPartIndex, pUser->GetUserID(), pUser->GetGameID());	

			ss.btResult = BASKETBALL_BUY_RESULT_DB_FAIL;

			CPacketComposer Packet(S2C_BUY_BASKETBALL_RES);
			Packet.Add((PBYTE)&ss, sizeof(SS2C_BUY_BASKETBALL_RES));
			pUser->Send(&Packet);

			return FALSE;
		}

		CPacketComposer Packet(S2C_BUY_BASKETBALL_RES);
		Packet.Add((PBYTE)&ss, sizeof(SS2C_BUY_BASKETBALL_RES));
		pUser->Send(&Packet);

		if(BASKETBALL_SHOP_REWARD_POINT == ss.btRewardType)
			pUser->SetSkillPoint(pUser->GetSkillPoint() + sRewardInfo.iPropertyValue);

		m_pUser->SetUserBillResultAtMem(iSellType, iPostMoney, pBillingInfo->iPointChange, 0, pBillingInfo->iBonusCoin );
		m_pUser->SendUserGold();

		if(BASKETBALL_SHOP_ITEM_TYPE_CLUB == pBasketBallManager->GetBasketBallItemType(btBallPartType, iBallPartIndex))
			m_pUser->SendClubShopUserCashInfo();

		if(pBillingInfo->iPointChange > 0)
		{
			m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
			m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_POINT, pBillingInfo->iPointChange);
			m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_BUY_POINT_ITEM);
			m_pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_POINT, pBillingInfo->iPointChange);
			m_pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USE_POINT,  pBillingInfo->iPointChange);

			m_pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POINT_ITEM);
			m_pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_USEPOINT, pBillingInfo->iPointChange);
		}
	}

	return TRUE;
}

void CFSGameUserBasketBall::GetBallPartSlotInfo(BOOL& bUseOption, int iaBallPartSlot[MAX_BASKETBALL_BALLPART])
{
	CLock Lock(&m_SyncUserBasketBall);

	bUseOption = m_bUseOption;
	memcpy(iaBallPartSlot, m_iaBallPartSlot, sizeof(int) * MAX_BASKETBALL_BALLPART);
}

void CFSGameUserBasketBall::CheckAndGiveConditionBallSkin(BYTE btBallPartType)
{
	CHECK_CONDITION_RETURN_VOID(MAX_BASKETBALL_BALLPART <= btBallPartType);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);
	CHECK_NULL_POINTER_VOID(m_pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_VOID(pBasketBallManager);

	vector<int/*iBallPartIndex*/>	vecGiveBallSkin;
	BASKETBALL_CONDITIONLIST_MAP mapConditionList = pBasketBallManager->GetConditionListMap(btBallPartType);
	BASKETBALL_CONDITIONLIST_MAP::const_iterator cIterMap		= mapConditionList.begin();
	BASKETBALL_CONDITIONLIST_MAP::const_iterator cIterMapEnd	= mapConditionList.end();

	BEGIN_SCOPE_LOCK(m_SyncUserBasketBall)
	for(; cIterMap != cIterMapEnd; ++cIterMap)
	{
		if(m_mapInventory[btBallPartType].end() != m_mapInventory[btBallPartType].find(cIterMap->first))
			continue;

		int	iCurrentValue = 0;
		BOOL bResult = FALSE;

		if(BASKETBALL_CONDITION_LEAGUE == cIterMap->second.btConditionType)
		{
			BYTE btConditionVal = m_pUser->GetUserRankMatch()->GetPreRewardTier();

			if( btConditionVal == static_cast<BYTE>(cIterMap->second.iConditionValue))
			{
				bResult = TRUE;
			}
		}
		else if(BASKETBALL_CONDITION_LV == cIterMap->second.btConditionType)
		{
			SAvatarInfo* pAvatarInfo = m_pUser->GetCurUsedAvatar();
			if(NULL == pAvatarInfo) 
				continue;

			iCurrentValue = pAvatarInfo->iLv;
		}
		else if(BASKETBALL_CONDITION_GOOD_APPRAISAL == cIterMap->second.btConditionType)
		{
			iCurrentValue = m_pUser->GetUserAppraisal()->GetMaxGoodPoint();
		}

		switch(cIterMap->second.btCompareType)
		{
			case BASKETBALL_CONDITION_COMPARE_UPPERBOUND:
				bResult = (iCurrentValue >= cIterMap->second.iConditionValue);
				break;
		}

		if(TRUE == bResult)
		{
			vecGiveBallSkin.push_back(cIterMap->first);
		}
	}
	END_SCOPE_LOCK

	if(vecGiveBallSkin.size() > 0)
	{
		int	iSortNum = 0;
		time_t tExpireDate = 0;	// TODO niceshot11 임시코드
		
		for(int i = 0; i < vecGiveBallSkin.size(); ++i)
		{
			if(ODBC_RETURN_SUCCESS == pODBCBase->BASKETBALL_GiveUserInventory(m_pUser->GetGameIDIndex(), vecGiveBallSkin[i], 2/*조건지급*/, 0, tExpireDate, iSortNum))
			{
				SBasketBallInventoryInfo sInfo;
				sInfo.iBallPartIndex = vecGiveBallSkin[i];
				sInfo.tExpireDate = -1;

				CLock lock(&m_SyncUserBasketBall);

				m_vecInventory[btBallPartType].insert(m_vecInventory[btBallPartType].begin() + iSortNum, sInfo);
				m_mapInventory[btBallPartType].insert(std::make_pair(sInfo.iBallPartIndex, sInfo.tExpireDate));
			}
		}
	}
}

void CFSGameUserBasketBall::CheckAndRemoveBasketBall()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);
	CHECK_NULL_POINTER_VOID(m_pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_VOID(pBasketBallManager);

	vector<int/*iBallPartIndex*/> vBallPartIndex[MAX_BASKETBALL_BALLPART];

	BEGIN_SCOPE_LOCK(m_SyncUserBasketBall)
		for(BYTE btBallPartType = 0; btBallPartType < MAX_BASKETBALL_BALLPART; ++btBallPartType)
		{
			vector<int/*iBallPartIndex*/>	vecGiveBallSkin;
			BASKETBALL_CONDITIONLIST_MAP mapConditionList = pBasketBallManager->GetConditionListMap(btBallPartType);
			BASKETBALL_CONDITIONLIST_MAP::const_iterator cIterMap		= mapConditionList.begin();
			BASKETBALL_CONDITIONLIST_MAP::const_iterator cIterMapEnd	= mapConditionList.end();

			for(; cIterMap != cIterMapEnd; ++cIterMap)
			{
				if(m_mapInventory[btBallPartType].find(cIterMap->first) == m_mapInventory[btBallPartType].end())
					continue;

				if(BASKETBALL_CONDITION_GOOD_APPRAISAL != cIterMap->second.btConditionType)
					continue;

				if(m_pUser->GetUserAppraisal()->GetMaxGoodPoint() < cIterMap->second.iConditionValue)
					vBallPartIndex[btBallPartType].push_back(cIterMap->second.iBallPartIndex);
			}
		}
	END_SCOPE_LOCK

	BOOL bRemove = FALSE;
	for(BYTE btBallPartType = 0; btBallPartType < MAX_BASKETBALL_BALLPART; ++btBallPartType)
	{
		for(int i = 0; i < vBallPartIndex[btBallPartType].size(); ++i)
		{
			if(ODBC_RETURN_SUCCESS == pODBCBase->BASKETBALL_DeleteUserInventory(m_pUser->GetGameIDIndex(), vBallPartIndex[btBallPartType][i], 3/*뺏기*/))
			{
				CLock lock(&m_SyncUserBasketBall);

				BALLPARTINFO_VEC::const_iterator cIterVec		= m_vecInventory[btBallPartType].begin();
				BALLPARTINFO_VEC::const_iterator cIterVecEnd	= m_vecInventory[btBallPartType].end();
				for(; cIterVec != cIterVecEnd; ++cIterVec)
				{
					if(vBallPartIndex[btBallPartType][i] == cIterVec->iBallPartIndex)
					{
						m_vecInventory[btBallPartType].erase(cIterVec);
						break;
					}
				}

				BALLPARTINFO_MAP::iterator iter = m_mapInventory[btBallPartType].find(vBallPartIndex[btBallPartType][i]);
				if(iter != m_mapInventory[btBallPartType].end())
				{
					m_mapInventory[btBallPartType].erase(iter);
				}

				if(m_iaBallPartSlot[btBallPartType] == vBallPartIndex[btBallPartType][i])
					m_iaBallPartSlot[btBallPartType] = (btBallPartType == BASKETBALL_BALLPART_BACKGROUND) ? pBasketBallManager->GetBasicBallBackground() : -1;

				bRemove = TRUE;
			}
		}
	}

	if(TRUE == bRemove)
	{
		SendUserUseOption();

		if(m_pUser->GetLocation() == U_TEAM)
		{
			m_pUser->SendAvatarInfoUpdateToMatch();
		}
	}
}

BOOL CFSGameUserBasketBall::CheckAndRemoveExpireBallPartSlot()
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);

	time_t tCurrentDate = _time64(NULL);
	BOOL bResult = FALSE;

	BEGIN_SCOPE_LOCK(m_SyncUserBasketBall)
		for(BYTE btBallPartType = 0; btBallPartType < MAX_BASKETBALL_BALLPART; ++btBallPartType)
		{
			BALLPARTINFO_MAP::const_iterator cIterMap = m_mapInventory[btBallPartType].find(m_iaBallPartSlot[btBallPartType]);

			if(cIterMap == m_mapInventory[btBallPartType].end())
				continue;

			if(cIterMap->second > 0 &&
				cIterMap->second <= tCurrentDate)
			{
				m_iaBallPartSlot[btBallPartType] = (btBallPartType == BASKETBALL_BALLPART_BACKGROUND) ? pBasketBallManager->GetBasicBallBackground() : -1;
				bResult = TRUE;
			}
		}
	END_SCOPE_LOCK

	if(bResult)
	{
		SendUserUseOption();

		if(m_pUser->GetLocation() == U_TEAM)
		{
			m_pUser->SendAvatarInfoUpdateToMatch();
		}
	}

	return bResult;
}

BOOL CFSGameUserBasketBall::CheckAndRemoveExpireBallPartInventory(BYTE btBallPartType)
{
	CHECK_CONDITION_RETURN(MAX_BASKETBALL_BALLPART <= btBallPartType, FALSE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	CHECK_NULL_POINTER_BOOL(pBasketBallManager);

	vector<int/*iBallPartIndex*/>	vecDeleteBallSkin;
	time_t tCurrentDate = _time64(NULL);

	BEGIN_SCOPE_LOCK(m_SyncUserBasketBall)

	BALLPARTINFO_VEC::const_iterator cIterVec		= m_vecInventory[btBallPartType].begin();
	BALLPARTINFO_VEC::const_iterator cIterVecEnd	= m_vecInventory[btBallPartType].end();

	for(; cIterVec != cIterVecEnd; ++cIterVec)
	{
		if(cIterVec->tExpireDate > 0 &&
			cIterVec->tExpireDate <= tCurrentDate)
		{
			vecDeleteBallSkin.push_back(cIterVec->iBallPartIndex);
		}
	}

	END_SCOPE_LOCK

	if(vecDeleteBallSkin.size() > 0)
	{
		for(int i = 0; i < vecDeleteBallSkin.size(); ++i)
		{
			if(ODBC_RETURN_SUCCESS == pODBCBase->BASKETBALL_DeleteUserInventory(m_pUser->GetGameIDIndex(), vecDeleteBallSkin[i], 3/*뺏기*/))
			{
				CLock lock(&m_SyncUserBasketBall);

				BALLPARTINFO_VEC::const_iterator cIterVec		= m_vecInventory[btBallPartType].begin();
				BALLPARTINFO_VEC::const_iterator cIterVecEnd	= m_vecInventory[btBallPartType].end();
				for(; cIterVec != cIterVecEnd; ++cIterVec)
				{
					if(vecDeleteBallSkin[i] == cIterVec->iBallPartIndex)
					{
						m_vecInventory[btBallPartType].erase(cIterVec);
						break;
					}
				}

				BALLPARTINFO_MAP::iterator iter = m_mapInventory[btBallPartType].find(vecDeleteBallSkin[i]);
				if(iter != m_mapInventory[btBallPartType].end())
				{
					m_mapInventory[btBallPartType].erase(iter);
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

void CFSGameUserBasketBall::GiveReward_BasketBall(BYTE btBallPartType, int iBallPartIndex, int iPropertyValue)
{
	CHECK_CONDITION_RETURN_VOID(MAX_BASKETBALL_BALLPART <= btBallPartType);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);
	CHECK_NULL_POINTER_VOID(m_pUser);

	int	iSortNum = 0;
	time_t tExpireDate = 0;
	if(ODBC_RETURN_SUCCESS == pODBCBase->BASKETBALL_GiveUserInventory(m_pUser->GetGameIDIndex(), iBallPartIndex, 2/*조건지급*/, iPropertyValue, tExpireDate, iSortNum))
	{
		SBasketBallInventoryInfo sInfo;
		sInfo.iBallPartIndex = iBallPartIndex;
		sInfo.tExpireDate = tExpireDate;

		CLock lock(&m_SyncUserBasketBall);

		m_vecInventory[btBallPartType].insert(m_vecInventory[btBallPartType].begin() + iSortNum, sInfo);
		m_mapInventory[btBallPartType].insert(std::make_pair(sInfo.iBallPartIndex, sInfo.tExpireDate));
	}
}
