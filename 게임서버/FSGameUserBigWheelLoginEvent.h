qpragma once

class CFSGameUser;
class CFSGameUserBigWheelLoginEvent
{
public:
	CFSGameUserBigWheelLoginEvent(CFSGameUser* pUser);
	~CFSGameUserBigWheelLoginEvent(void);

	BOOL			Load();
	void			CheckResetDate(time_t tCurrentTime = _time64(NULL));
	void			SetStampInfo();
	void			SendEventInfo();
	void			SendUserInfo();
	BYTE			Stamp(BYTE btStampType);
	void			AddPlayCount();
	BOOL			IsEventAllClear();

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;

	time_t			m_tRecentCheckDate;
	BYTE			m_btCurrentDayIndex;
	int				m_iPlayCount;
	int				m_iTodayPlayCount;
	BYTE			m_btLostStampCount;
	BYTE			m_btMinLostStampDayIndex;
	BYTE			m_btStamp[MAX_BIGWHEEL_LOGIN_DAY];
};

