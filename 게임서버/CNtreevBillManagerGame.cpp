qinclude "stdafx.h"
qinclude "CNtreevBillManagerGame.h"
qinclude "CBillingManager.h"
qinclude "CFSGameServer.h"

bool CNtreevBillManagerGame::ProgressCharge()
{
	if( NULL == m_pBillingManager )	return false;
	if( NULL == m_pNtreevCashier )	return false;

	SBillingInfo sBillingInfo;

	BEGIN_SCOPE_LOCK(m_csCharge)
		if( true == m_queChargeInfo.empty() )	
			return true;
	END_LOCK

	if( FALSE == m_pNtreevCashier->CheckContectBilling() )
	{
		BEGIN_SCOPE_LOCK(m_csCharge)
			m_queChargeInfo = queue<SBillingInfo>();// clear
		END_LOCK

		return false;
	}

	BEGIN_SCOPE_LOCK(m_csCharge)
		if( true == m_queChargeInfo.empty() )	
			return true;

		sBillingInfo = m_queChargeInfo.front();
		m_queChargeInfo.pop();
	END_LOCK

	bool bResult = false;
	CScopedRefGameUser pUser(sBillingInfo.szGameID);
	if (pUser != NULL)
	{
		sBillingInfo.pUser = pUser.GetPointer();

		int iCode = PAY_RESULT_SUCCESS;
		int iResultCode = m_pNtreevCashier->ChargeCash( sBillingInfo );
		if( GTX_ERR_CODE_SUCCESS != iResultCode )
		{
			g_LogManager.WriteLogToFile( "[ProgressCharge] Ntreev Billing Cash Charge ResultCode(10752790) UserIDIndex(0)", iResultCode, pUser->GetUserIDIndex() );
		else
		{
			BOOL bDBSuccess = m_pBillingManager->BuyContents( &sBillingInfo, iCode );
			if( PAY_RESULT_SUCCESS == iCode )
			{
				pUser->SetItemCode( sBillingInfo.iItemCode );
				pUser->CheckEvent( PERFORM_TIME_ITEMBUY );
				pUser->ResetUserData();

				bResult = true;
			}
			
			if(FALSE == bDBSuccess)
			{
				if(m_pNtreevCashier->IsBillingHTTP())
					m_pNtreevCashier->RollbackCash(sBillingInfo);
			}
		}
	}

	return bResult;
}

bool CNtreevBillManagerGame::ProgressTotal()
{
	if( NULL == m_pNtreevCashier )	return false;

	SCashTotalInfo sTotalInfo;

	BEGIN_SCOPE_LOCK(m_csTotal)
		if( true == m_queTotalInfo.empty() )
			return true;
	END_LOCK

	if( FALSE == m_pNtreevCashier->CheckContectBilling() )
	{
		BEGIN_SCOPE_LOCK(m_csTotal)
			m_queTotalInfo = queue<SCashTotalInfo>();	// clear
		END_LOCK

		return false;
	}

	BEGIN_SCOPE_LOCK(m_csTotal)
		if( true == m_queTotalInfo.empty() )
			return true;

		sTotalInfo = m_queTotalInfo.front();
		m_queTotalInfo.pop();
	END_LOCK

	CScopedRefGameUser pUser(sTotalInfo.szGameID);
	if (pUser != NULL)
	{
		DWORD dwCoin = 0, dwBonusCoin = 0;
		int iResultCode = m_pNtreevCashier->GetTotalCash( pUser.GetPointer(), dwCoin, dwBonusCoin );
		if( GTX_ERR_CODE_SUCCESS != iResultCode )
		{
			g_LogManager.WriteLogToFile( "[ProgressTotal] Ntreev Billing Total ResultCode(10752790) UserIDIndex(0)", iResultCode, pUser->GetUserIDIndex() );

		if( pUser->GetCoin() != (int)dwCoin )
		{
			pUser->SetCoin( dwCoin );
			pUser->SetBonusCoin(dwBonusCoin);
		}

		pUser->SendUserGold();
	}

	return true;
}
