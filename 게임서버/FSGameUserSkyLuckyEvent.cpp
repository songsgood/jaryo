qinclude "stdafx.h"
qinclude "FSGameUserSkyLuckyEvent.h"
qinclude "CFSGameUser.h"
qinclude "SkyLuckyEventManager.h"
qinclude "RewardManager.h"

CFSGameUserSkyLuckyEvent::CFSGameUserSkyLuckyEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iBuyTicketTypeFlag(0)
	, m_btCurrentStepDay(0)
	, m_btCurrentStepStatus(0)
	, m_tRecentCheckDate(-1)
{
}

CFSGameUserSkyLuckyEvent::~CFSGameUserSkyLuckyEvent(void)
{
}

BOOL	CFSGameUserSkyLuckyEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == SKYLUCKY.IsOpen(), TRUE);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	int iRet = pBaseODBC->EVENT_SKYLUCKY_GetUserData(m_pUser->GetUserIDIndex(), m_iBuyTicketTypeFlag, m_btCanGetRewardMinStepDay, m_btCurrentStepDay, m_btCurrentStepStatus, m_tRecentCheckDate);
	if(ODBC_RETURN_SUCCESS != iRet)
	{
		if(iRet == -1)	// 이벤트 단계 모두 완료
		{
			return TRUE;
		}
		WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, DB_DATA_LOAD, FAIL, "EVENT_SKYLUCKY_GetUserData failed");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	CheckResetDate();

	return TRUE;
}

void	CFSGameUserSkyLuckyEvent::CheckResetDate(time_t tCurrentTime /*= _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(m_btCurrentStepDay >= SKYLUCKY_STEP_DAY_COUNT);

	BOOL	bResult = FALSE;

	if(-1 == m_tRecentCheckDate)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tRecentCheckDate, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= SKYLUCKY_EVENT_RESET_TIME;

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tRecentCheckDate <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		BYTE btUpdatedStepDay = m_btCurrentStepDay + 1;
		BYTE btUpdatedStepStatus = SKYLUCKY_STEP_STATUS_CHECK_OK;
		BYTE btCanGetRewardMinStepDay = 0;

		if(SKYLUCKY_STEP_STATUS_CURRENT_ALL_REWARD_OK == m_btCurrentStepStatus)
		{
			btCanGetRewardMinStepDay = m_btCurrentStepDay + 1;
		} 
		else
		{
			btCanGetRewardMinStepDay = m_btCanGetRewardMinStepDay;

			if(SKYLUCKY_STEP_STATUS_CURRENT_BASIC_REWARD_OK == m_btCurrentStepStatus)
			{
				if(TRUE == SKYLUCKY.CheckBuyTicket(m_btCurrentStepDay, m_iBuyTicketTypeFlag))
				{
					btUpdatedStepStatus = SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK_TICKET_REWARD_READY;
				}
				else
				{
					btUpdatedStepStatus = SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK;
				}
			}
			else if(-1 == m_tRecentCheckDate)
			{
				btUpdatedStepDay = 0;
			}
			else
			{
				btUpdatedStepStatus = m_btCurrentStepStatus;
			}
		}

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_SKYLUCKY_GoToNextStepDay(m_pUser->GetUserIDIndex(), tCurrentTime, btUpdatedStepDay, btUpdatedStepStatus, btCanGetRewardMinStepDay))
		{
			m_tRecentCheckDate			= tCurrentTime;
			m_btCurrentStepDay			= btUpdatedStepDay;
			m_btCurrentStepStatus		= btUpdatedStepStatus;
			m_btCanGetRewardMinStepDay	= btCanGetRewardMinStepDay;
		}
	}
}

void	CFSGameUserSkyLuckyEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SKYLUCKY.IsOpen());
	CHECK_CONDITION_RETURN_VOID(TRUE == IsEventAllClear());

	CPacketComposer	Packet(S2C_SKYLUCKY_EVENT_INFO_NOT);
	SKYLUCKY.MakePacketBoardInfo(Packet);
	m_pUser->Send(&Packet);
}

void	CFSGameUserSkyLuckyEvent::SendUserEventData()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CheckResetDate();

	SS2C_SKYLUCKY_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SKYLUCKY_USER_INFO_RES));

	ss.bIsBuyTicket[SKYLUCKY_TICKET_300]	= (m_iBuyTicketTypeFlag & SKYLUCKY_TICKET_FLAG_300);
	ss.bIsBuyTicket[SKYLUCKY_TICKET_400]	= (m_iBuyTicketTypeFlag & SKYLUCKY_TICKET_FLAG_400);
	ss.bIsBuyTicket[SKYLUCKY_TICKET_500]	= (m_iBuyTicketTypeFlag & SKYLUCKY_TICKET_FLAG_500);
	//ss.bIsBuyTicket[SKYLUCKY_TICKET_1000]	= (m_iBuyTicketTypeFlag & SKYLUCKY_TICKET_FLAG_1000);
	
	ss.btCanGetRewardMinStepDay	= m_btCanGetRewardMinStepDay;
	ss.btCurrentStepDay			= m_btCurrentStepDay;
	ss.btCurrentStepStatus		= m_btCurrentStepStatus;
	ss.iRemainTime				= -1;

	if(SKYLUCKY.IsCanBuyTicketToday(m_btCurrentStepDay))
	{
		SYSTEMTIME systemTime;
		::GetLocalTime(&systemTime);

		int iHour = 0;

		if(systemTime.wHour >= SKYLUCKY_EVENT_RESET_TIME)
		{
			iHour = 23 - systemTime.wHour + SKYLUCKY_EVENT_RESET_TIME;
		}
		else
		{
			iHour = 23 - systemTime.wHour + SKYLUCKY_EVENT_RESET_TIME - 1;

			if(iHour >= 23)
			{
				iHour -= 23;
			}
		}

		ss.iRemainTime = (iHour * 10000) + ((59 - systemTime.wMinute) * 100) + (59 - systemTime.wSecond);
	}	
	
	CPacketComposer	Packet(S2C_SKYLUCKY_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_SKYLUCKY_USER_INFO_RES));

	m_pUser->Send(&Packet);
}

BOOL	CFSGameUserSkyLuckyEvent::BuySkyLuckyTicket_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, INVALED_DATA, CHECK_FAIL, "BuySkyLuckyTicket_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}
	
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	int	iRewardValue = 0;
	SS2C_SKYLUCKY_BUY_TICKET_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_SKYLUCKY_BUY_TICKET_NOT));

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	BYTE	btTicketFlag = SKYLUCKY.GetTicketFlag(m_btCurrentStepDay);
	int		iBuyTicketTypeFlag = m_iBuyTicketTypeFlag | btTicketFlag;
	BYTE	btCanGetRewardMinStepDay = (SKYLUCKY_STEP_STATUS_CURRENT_BASIC_REWARD_OK == m_btCurrentStepStatus) ? m_btCurrentStepDay : m_btCanGetRewardMinStepDay;

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_SKYLUCKY_BuyTicket(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iBuyTicketTypeFlag, m_btCurrentStepDay, _time64(NULL), iItemCode, iPrevCash, iPostCash, pBillingInfo->iItemPrice, btCanGetRewardMinStepDay))
	{
		WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, LA_DEFAULT, NONE, "EVENT_SKYLUCKY_BuyTicket Fail, User:10752790, CurrentStepDay:0", m_pUser->GetUserIDIndex(), m_btCurrentStepDay);
btResult = RESULT_SKYLUCKY_BUY_FIRSTCLASS_FAIL;
	}
	else
	{
		m_iBuyTicketTypeFlag = iBuyTicketTypeFlag;
		m_btCanGetRewardMinStepDay = btCanGetRewardMinStepDay;
		ss.btResult = RESULT_SKYLUCKY_BUY_FIRSTCLASS_SUCCESS;

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);

	// 티켓 구매결과 알림 패킷 보냄.
	CPacketComposer	Packet(S2C_SKYLUCKY_BUY_TICKET_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_SKYLUCKY_BUY_TICKET_NOT));
	m_pUser->Send(&Packet);

	int iCostType = pBillingInfo->iSellType;
	m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin );
	m_pUser->SendUserGold();

	return (ss.btResult == RESULT_SKYLUCKY_BUY_FIRSTCLASS_SUCCESS);
}

void	CFSGameUserSkyLuckyEvent::GiveSkyLuckyReward()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	time_t	tCurrentTime = _time64(NULL);

	CheckResetDate(tCurrentTime);

	int		iRewardIndex = 0;	
	BYTE	btCurrentStepStatus			= SKYLUCKY_STEP_STATUS_NONE;
	BYTE	btCanGetRewardMinStepDay	= m_btCanGetRewardMinStepDay;

	SS2C_SKYLUCKY_GET_REWARD_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SKYLUCKY_GET_REWARD_RES));

	ss.btResult = RESULT_SKYLUCKY_GET_REWARD_SUCCESS;
	
	if(SKYLUCKY_STEP_STATUS_CURRENT_ALL_REWARD_OK == m_btCurrentStepStatus)
	{
		ss.btResult = RESULT_SKYLUCKY_GET_REWARD_ALREADY_GET_FAIL;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_SKYLUCKY_GET_REWARD_FULL_MAILBOX_FAIL;
	}
	else if(CLOSED == SKYLUCKY.IsOpen())
	{
		ss.btResult = RESULT_SKYLUCKY_GET_REWARD_EVENT_CLOSED_FAIL;
	}
	else
	{
		if((SKYLUCKY_STEP_STATUS_CHECK_OK == m_btCurrentStepStatus || SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK == m_btCurrentStepStatus) &&
			m_btCanGetRewardMinStepDay <= m_btCurrentStepDay)
		{
			if(m_btCanGetRewardMinStepDay < m_btCurrentStepDay)
			{
				ss.btRewardStepDay	= m_btCanGetRewardMinStepDay;

				if(TRUE == SKYLUCKY.CheckBuyTicket(m_btCanGetRewardMinStepDay, m_iBuyTicketTypeFlag))
				{
					btCurrentStepStatus			= SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK_TICKET_REWARD_READY;
					ss.btRewardStepStatus		= SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK_TICKET_REWARD_READY;
					btCanGetRewardMinStepDay	= m_btCanGetRewardMinStepDay;
				}
				else
				{
					btCurrentStepStatus			= SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK;
					ss.btRewardStepStatus		= SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK;
					btCanGetRewardMinStepDay	= m_btCanGetRewardMinStepDay + 1;
				}
			}
			else if(m_btCanGetRewardMinStepDay == m_btCurrentStepDay)
			{
				ss.btRewardStepDay			= m_btCurrentStepDay;
				ss.btRewardStepStatus		= SKYLUCKY_STEP_STATUS_CURRENT_BASIC_REWARD_OK;
				btCurrentStepStatus			= SKYLUCKY_STEP_STATUS_CURRENT_BASIC_REWARD_OK;
				
				if(TRUE == SKYLUCKY.CheckBuyTicket(m_btCanGetRewardMinStepDay, m_iBuyTicketTypeFlag))
				{
					btCanGetRewardMinStepDay = m_btCanGetRewardMinStepDay;
				}
				else
				{
					btCanGetRewardMinStepDay = m_btCanGetRewardMinStepDay + 1;
				}
			}

			iRewardIndex = SKYLUCKY.GetBasicRewardIndex(m_btCanGetRewardMinStepDay);
		}
		else if(SKYLUCKY_STEP_STATUS_BASIC_REWARD_OK_TICKET_REWARD_READY == m_btCurrentStepStatus &&
			m_btCanGetRewardMinStepDay <= m_btCurrentStepDay)
		{
			if(m_btCanGetRewardMinStepDay < m_btCurrentStepDay)
			{
				ss.btRewardStepDay		= m_btCanGetRewardMinStepDay;
				ss.btRewardStepStatus	= SKYLUCKY_STEP_STATUS_CHECK_OK;
				btCurrentStepStatus		= SKYLUCKY_STEP_STATUS_CHECK_OK;
			}
			else if(m_btCanGetRewardMinStepDay == m_btCurrentStepDay)
			{
				ss.btRewardStepDay		= m_btCurrentStepDay;
				ss.btRewardStepStatus	= SKYLUCKY_STEP_STATUS_CURRENT_BASIC_REWARD_OK;
				btCurrentStepStatus		= SKYLUCKY_STEP_STATUS_CURRENT_BASIC_REWARD_OK;
			}
			btCanGetRewardMinStepDay	= m_btCanGetRewardMinStepDay + 1;

			iRewardIndex = SKYLUCKY.GetTicketRewardIndex(m_btCanGetRewardMinStepDay);
		}
		else if(SKYLUCKY_STEP_STATUS_CURRENT_BASIC_REWARD_OK == m_btCurrentStepStatus &&
				m_btCanGetRewardMinStepDay == m_btCurrentStepDay)
		{
			if(TRUE == SKYLUCKY.CheckBuyTicket(m_btCurrentStepDay, m_iBuyTicketTypeFlag))
			{
				ss.btRewardStepDay		= m_btCurrentStepDay;
				ss.btRewardStepStatus	= SKYLUCKY_STEP_STATUS_CURRENT_ALL_REWARD_OK;				
				btCurrentStepStatus		= SKYLUCKY_STEP_STATUS_CURRENT_ALL_REWARD_OK;
				btCanGetRewardMinStepDay	= m_btCanGetRewardMinStepDay + 1;

				iRewardIndex = SKYLUCKY.GetTicketRewardIndex(m_btCanGetRewardMinStepDay);
			}
			else
			{
				ss.btResult = RESULT_SKYLUCKY_GET_REWARD_HAVETO_BUY_TICKET_FAIL;
			}
		}
		else
		{
			ss.btResult = RESULT_SKYLUCKY_GET_REWARD_CONDITION_FAIL;
		}
		
		if(RESULT_SKYLUCKY_GET_REWARD_SUCCESS == ss.btResult && iRewardIndex <= 0)
		{
			ss.btResult = RESULT_SKYLUCKY_GET_REWARD_EVENT_CLOSED_FAIL;
		}
	}	

	if(RESULT_SKYLUCKY_GET_REWARD_SUCCESS == ss.btResult)
	{
		CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pBaseODBC);

		if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_SKYLUCKY_GiveReward(m_pUser->GetUserIDIndex(), iRewardIndex, tCurrentTime, 
			btCanGetRewardMinStepDay, btCurrentStepStatus, ss.btRewardStepDay, ss.btRewardStepStatus))
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);

			if(NULL == pReward)
			{
				ss.btResult = RESULT_SKYLUCKY_GET_REWARD_GIVE_FAIL;

				WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, LA_DEFAULT, NONE, "EVENT_SKYLUCKY_GiveReward Fail, UserIDIndex:10752790, RewardIndex:0, RewardStepDay:17051648, RewardStepStatus:-858993460"
serIDIndex(), iRewardIndex, ss.btRewardStepDay, ss.btRewardStepStatus);
			}	
			
			m_btCanGetRewardMinStepDay	= btCanGetRewardMinStepDay;
			m_btCurrentStepStatus		= btCurrentStepStatus;					
		}		
	}

	CPacketComposer	Packet(S2C_SKYLUCKY_GET_REWARD_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_SKYLUCKY_GET_REWARD_RES));
	m_pUser->Send(&Packet);
}