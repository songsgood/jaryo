// CFSGameThread.cpp: implementation of the CFSGameThread class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameDBCfg.h"
qinclude "CFSFileLog.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameClient.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "CFSSvrList.h"
qinclude "ChatSvrProxy.h"
qinclude "CFSSkillShop.h"
qinclude "HackingManager.h"
qinclude "UserPatternManager.h"
qinclude "ClubConfigManager.h"
qinclude "GameGuideItemConfig.h"
qinclude "CIntensivePracticeManager.h"
qinclude "FactionManager.h"
qinclude "WordPuzzlesManager.h"
qinclude "LobbyChatUserManager.h"
qinclude "AttendanceItemShop.h"
qinclude "XignCodeManager.h"
qinclude "CContentsManager.h"
qinclude "LeagueSvrProxy.h"
qinclude "ODBCSvrProxy.h"
qinclude "PVEManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CFSGameDBCfg*	g_pFSDBCfg; 

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CFSGameThread::CFSGameThread(CIOCP * pIOCP) : CSocketIOCPThread(pIOCP)
{

}

CFSGameThread::~CFSGameThread()
{

}

unsigned CFSGameThread::Run()
{
	CoInitialize(NULL);
	unsigned ret = CIOCPThread::Run();
	CoUninitialize();
	return ret;
}

CIOCPThread* CFSGameThread::AllocSingleThread()
{
	return CFSGameThread::Allocate(m_pIOCP);
}

BOOL CFSGameThread::OnStartup()
{
	WRITE_LOG_NEW(LOG_TYPE_SYSTEM, THREAD_START, NONE, "CFSGameThread");
	
	return TRUE;
}

void CFSGameThread::Process_MonitorAliveEcho(CIOCPSocketClient* pIOCPSocketClient)
{
	CPacketComposer PacketComposer(MON_ALIVE_ECHO);
	
	pIOCPSocketClient->Send(&PacketComposer);
}

void CFSGameThread::ProcessPacket( CIOCPSocketClient * pIOCPSocketClient )
{
//#ifdef TEST_LOG
	int iStartTickCount = GetTickCount();
	float fEnable = 0.0f;
	CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_CLIENT_PACKET_TICKCOUNT_LOG, (int)1, fEnable);

	BOOL bWriteLog = (BOOL)fEnable;
	if(TRUE == bWriteLog)
	{
		WRITE_LOG_HOUR(LOG_TYPE_SYSTEM, PROCESS_PACKET_START, NONE, "ThreadId:11866902, PacketNum:0", ::GetCurrentThreadId(), m_ReceivePacketBuffer.GetCommand());
/#endif

	ProcessPacket_Log( pIOCPSocketClient );

//#ifdef TEST_LOG
	if(TRUE == bWriteLog)
	{
		WRITE_LOG_HOUR(LOG_TYPE_SYSTEM, PROCESS_PACKET_END, NONE, "ThreadId:11866902, PacketNum:0, ProcessTime:18227200", ::GetCurrentThreadId(), m_ReceivePacketBuffer.GetCommand(), GetTickCount() - iStartTickCount);
f
}

////////////////////////////////////////////// Chatting //////////////////////////////////////////////////////////
void CFSGameThread::SendNotice( CFSGameClient* pClient, int iCode, int nError )
{
	int iType = 0 , iParam0 = 0, iParam1 = 0, iParam2 = 0, iParam3 = 0;
	
	int iLoopIndex = 0;
	
	CPacketComposer Packet(S2C_NOTICE); 
	Packet.Add(&iCode);

	SFSConfigInfo sConfig_Penalty;
	memset(&sConfig_Penalty, 0, sizeof(SFSConfigInfo));

	CFSGameUser *pUser = (CFSGameUser *)pClient->GetUser(); 
	CHECK_NULL_POINTER_VOID( pUser );

	SAvatarInfo *pAvatarInfo = pUser->GetCurUsedAvatar(); 
	CHECK_NULL_POINTER_VOID( pAvatarInfo );

	// 20090610 중국 강종 페널티 강화로 재접시 공지의 감소 수치 변경.
	if ( CFSGameServer::GetInstance()->IsAbusePenaltyReinforcementEnable() == TRUE && 
		pAvatarInfo->DisconnectPoint >= CFSGameServer::GetInstance()->GetAbusePenaltyDisconnectPointCondition() )
	{
		if( pAvatarInfo->iLv >= GAME_LVUP_BOUND2 )
		{
			g_pFSDBCfg->GetCfg( CFGCODE_DISCONNECT, ABUSE_PENALTY_MAJOR, sConfig_Penalty );
		}
		else
		{
			g_pFSDBCfg->GetCfg( CFGCODE_DISCONNECT, ABUSE_PENALTY_ROOKIE, sConfig_Penalty );
		}
	}
	else
	{
		g_pFSDBCfg->GetCfg( CFGCODE_DISCONNECT, DISCONNECT_PENALTY_DEFAULT, sConfig_Penalty );
	}
	
	Packet.Add(&iType);
	if( nError == 0 )
	{
		Packet.Add(&sConfig_Penalty.iParam[EXP_PENALTY_OVER_TIME]);
		Packet.Add(&sConfig_Penalty.iParam[SKILLPOINT_PENALTY_OVER_TIME]);
	}
	else
	{
		Packet.Add(&sConfig_Penalty.iParam[EXP_PENALTY_IN_TIME]);
		Packet.Add(&sConfig_Penalty.iParam[SKILLPOINT_PENALTY_IN_TIME]);
	}
	
	pClient->Send(&Packet);
}


////////////////////////////////////////////// Log out //////////////////////////////////////////

void CFSGameThread::OnPeerDisconnected(CIOCPSocketClient* pIOCPSocketClient)
{
	LogOut(pIOCPSocketClient);
	CSocketIOCPThread::OnPeerDisconnected(pIOCPSocketClient);
}

void CFSGameThread::Process_FSChooseAvatarInfo( CFSGameClient * pClient )
{
	CFSGameUser* pGameUser = (CFSGameUser*) pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pGameUser);

	pGameUser->SendAvatarInfo();

	if (pClient->GetState() == FS_CLUB ||
		pClient->GetState() == FS_ITEM || 
		pClient->GetState() == FS_SKILL)
	{
		return;
	}

	pGameUser->SendUserStat();		// edited by rimbaud
	pGameUser->SendUserGold();
	pGameUser->SendCurAvatarTrophy();
	SendCurAvatarSkillPoint(pClient, pGameUser->GetCurAvatarRemainSkillPoint(), pGameUser->GetCurAvatarUsedSkillPointAllType());

	pGameUser->SendAvatarInfoUpdateToMatch();
}

void CFSGameThread::SendCurAvatarSkillPoint(CFSGameClient* pFSClient, int iTotalSkillPoint, int iUsedSkillPoint)
{	
	if( pFSClient == NULL  )
	{
		return;
	}
	CPacketComposer PacketComposer(S2C_SKILLPOINT_RES);
	PacketComposer.Add(iTotalSkillPoint);	
	PacketComposer.Add(iUsedSkillPoint);

	pFSClient->Send(&PacketComposer);	
}

void CFSGameThread::Process_FSCheckAlive( CFSGameClient * pFSClient)
{
	CFSGameUser * pGameUser = (CFSGameUser*) pFSClient->GetUser();
	
	if( pGameUser == NULL ) return;
	
	bool bNimiral = false;
	CPacketComposer PacketComposer(S2C_CHECK_ALIVE_RES);
	PacketComposer.Add((PBYTE)&bNimiral, sizeof(bool));
	
	pGameUser->Send(&PacketComposer);
}

// Modify for Match Server
void CFSGameThread::Process_FSSetOption(CFSGameClient * pFSClient)
{
	CFSGameUser * pGameUser = (CFSGameUser*) pFSClient->GetUser();
	if( pGameUser == NULL )	return;	
	
	int iType , iOp , iResult;
	m_ReceivePacketBuffer.Read(&iType);
	m_ReceivePacketBuffer.Read(&iOp);

	if(iOp > 1)
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, RECV_DATA, CHECK_FAIL, "Process_FSSetOption - IOp:11866902", iOp);
rn;
	}
	
	iResult = -1;

	switch (iType)
	{
	case OPTION_TYPE_INVITE:
	case OPTION_TYPE_WHISPER:
	case OPTION_TYPE_MAIL:
	case OPTION_TYPE_NAVIGATION:
	case OPTION_TYPE_SEE_ABILITY:
		{
			if(pGameUser->GetOptionEnable(iType) == iOp) return;

			pGameUser->SetOptionEnable(iType, iOp);
			iResult = 0;
		
			pGameUser->SaveGameOption();
		} break;
	case OPTION_TYPE_MAIL_AND_WHISPER:
		{
			if(	pGameUser->GetOptionEnable(OPTION_TYPE_WHISPER) == iOp &&
				pGameUser->GetOptionEnable(OPTION_TYPE_MAIL) == iOp) 
				return;
		
			pGameUser->SetOptionEnable(OPTION_TYPE_WHISPER, iOp);
			pGameUser->SetOptionEnable(OPTION_TYPE_MAIL, iOp);
			iResult = 0;
		
			pGameUser->SaveGameOption();
		} break;
	default:
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, RECV_DATA, CHECK_FAIL, "Process_FSSetOption - Type:11866902", iType);
rn;
	}

	CPacketComposer PacketComposer(S2C_SET_OPTION_RES);
	PacketComposer.Add(iType);
	PacketComposer.Add(iOp);
	PacketComposer.Add(iResult);

	pFSClient->Send(&PacketComposer);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pGameUser->GetMatchLocation());
	if(pMatch) 
	{
		SG2M_SET_GAME_OPTION info;
		info.iGameIDIndex = pGameUser->GetGameIDIndex();
		info.shGameOpt = pGameUser->GetGameOption();
		pMatch->SendSetGameOption(info);
	}
}

void CFSGameThread::Process_FSAnnounceCheckAccount(CFSGameClient * pFSClient)
{
    if(MAX_OPERATORID_LENGTH + MAX_OPERATORPASSWORD_LENGTH + 6 != m_ReceivePacketBuffer.GetDataSize())
    {
        return ;
    }

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );
	
	CFSGameUser *pUser = (CFSGameUser*) pFSClient->GetUser();
	if( pUser == NULL ) return;
	
    CPacketComposer PacketComposer(21001);
    BYTE ucResult = 0;
	
    if(NEXUS_INITIAL == pFSClient->GetState())
    {
		DECLARE_INIT_TCHAR_ARRAY(szOperatorID, MAX_OPERATORID_LENGTH+1);
		DECLARE_INIT_TCHAR_ARRAY(szOperatorPassword, MAX_OPERATORPASSWORD_LENGTH+1);
		DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);
		
        BYTE ucIPAddress[4];
		
        m_ReceivePacketBuffer.Read((PBYTE) szOperatorID, MAX_OPERATORID_LENGTH + 1); szOperatorID[MAX_OPERATORID_LENGTH] = '\0';
        m_ReceivePacketBuffer.Read((PBYTE) szOperatorPassword, MAX_OPERATORPASSWORD_LENGTH + 1); szOperatorPassword[MAX_OPERATORPASSWORD_LENGTH] = '\0';
        m_ReceivePacketBuffer.Read(ucIPAddress, 4);
		
		strcpy_s(szGameID, _countof(szGameID), szOperatorID);
		
        if(ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->spFSCheckOpUser(szGameID, szOperatorPassword))
		{
			ucResult = 1;
            pUser->SetGameID(szGameID);
            pFSClient->SetState(NEXUS_LOGINSUCCESS);
			
		}
    }
	
	pUser->SetLocation(U_OPERATOR);
	
    PacketComposer.Add(ucResult);
    pFSClient->Send(&PacketComposer);
	
	return;
}

void CFSGameThread::Process_FSAnnounce(CFSGameClient * pClient)
{
	CHECK_NULL_POINTER_VOID(pClient);
	CFSGameUser *pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer::GetInstance()->BroadCastNotifyMsg(2, (LPCTSTR) m_ReceivePacketBuffer.GetRemainedData(), m_ReceivePacketBuffer.GetRemainedDataSize());
}

void CFSGameThread::Process_FSCashInfo( CFSGameClient *pFSClient)
{
	CFSGameUser *pUser  = (CFSGameUser *) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	int iResult = BILLING_GAME.GetCashBalance(pUser);
	if( CASH_BALANCE_SUCCESS == iResult)
	{
		pUser->SendUserGold();
	}
}

void CFSGameThread::Process_UserShout(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if( NULL == pUser) return;
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if( pServer == NULL ) return;
	
	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
	if( pCenter == NULL ) return;

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	if( pGameODBC == NULL ) return;
	
	DECLARE_INIT_TCHAR_ARRAY(szText, MAX_CHATBUFF_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szToGameID, MAX_GAMEID_LENGTH+1);
	
	int iShoutItemMode = 0;
	
	m_ReceivePacketBuffer.Read((PBYTE)szToGameID, MAX_GAMEID_LENGTH+1);	
	m_ReceivePacketBuffer.Read((PBYTE)szText, MAX_CHATBUFF_LENGTH+1); 
	m_ReceivePacketBuffer.Read(&iShoutItemMode);

	int iShoutItemcode = -1;
	if ( 0 == iShoutItemMode  ) //鬼윙걷
	{
		iShoutItemcode = 3910001;
	}
	else if ( 1 == iShoutItemMode ) //vip윙걷
	{
		iShoutItemcode = 3910501;
	}

	if ( -1 != iShoutItemcode )
	{
		CFSItemShop* pItemShop = pServer->GetItemShop();
		if ( pItemShop )
		{
			SShopItemInfo iteminfo;
			if( TRUE == pItemShop->GetItem( iShoutItemcode, iteminfo ) )
			{
				if ( false == iteminfo.IsLvCondition(pUser->GetCurUsedAvtarLv()) )
				{
					int iErrorCode2 = -3;
					CPacketComposer PacketComposer(S2C_SHOUT);
					PacketComposer.Add(iErrorCode2);
					PacketComposer.Add(iShoutItemMode);
					pUser->Send(&PacketComposer);
					return ;
				}
			}
		}
	}
	
	szText[MAX_CHATBUFF_LENGTH] = 0;	
	
	DWORD dwResultTime = CFSGameServer::GetInstance()->CheckShoutTime(iShoutItemMode);
	
	if( dwResultTime != 0 )
	{
		int iErrorCode2 = -10;
		CPacketComposer PacketComposer(S2C_SHOUT);
		PacketComposer.Add(iErrorCode2);
		PacketComposer.Add(iShoutItemMode);
		PacketComposer.Add((int)dwResultTime);
		pUser->Send(&PacketComposer);
		
		return;
	}
	
	if( pUser->GetUserItemList()->ReadyShoutItem(iShoutItemMode) )
	{
		CAvatarItemList *pAvatarItemList = pUser->GetUserItemList()->GetCurAvatarItemList();
		if( NULL == pAvatarItemList ) return ;

		int iShoutItemCode = pAvatarItemList->GetShoutItemCodeByMode(iShoutItemMode);
		int iItemCnt = 0;
		if ( 0 != iShoutItemCode )
		{
			pGameODBC->spGetUserItemCount( pUser->GetGameIDIndex(), iShoutItemCode, iItemCnt );
		}

		if( iItemCnt > 0 )
		{
			pCenter->SendShout((char*)pUser->GetGameID(), szText,iShoutItemMode);
		}
		else
		{
			int iErrorCode2 = -2;
			CPacketComposer PacketComposer(S2C_SHOUT);
			PacketComposer.Add(iErrorCode2);
			PacketComposer.Add(iShoutItemMode);
			pUser->Send(&PacketComposer);
		}
	}
	else
	{
		int iErrorCode2 = -2;
		CPacketComposer PacketComposer(S2C_SHOUT);
		PacketComposer.Add(iErrorCode2);
		PacketComposer.Add(iShoutItemMode);
		pUser->Send(&PacketComposer);
	}
}

void CFSGameThread::Process_FSShoutCountReq(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if( NULL == pUser) return;
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if( pServer == NULL ) return;
	
	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
	if( pCenter == NULL ) return;
	
	int iShoutItemMode = 0;
	int iShoutCount = 0;
	
	m_ReceivePacketBuffer.Read(&iShoutItemMode);
	
	iShoutCount = pUser->GetUserItemList()->GetShoutItemCount(iShoutItemMode);
	
	CPacketComposer PacketComposer(S2C_SHOUT_COUNT_RES);
	PacketComposer.Add(iShoutCount);
	PacketComposer.Add(iShoutItemMode);
	
	pUser->Send(&PacketComposer);
}

void CFSGameThread::SendEquipItemInfo( CFSGameClient * pClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( pUser == NULL )
	{
		return;
	}

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );
	
	int iCategory = 0 , iPage = 0, iOp = 0;
	int iStatus = 0;
	
	if( iPage < 0 ) return;
		
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	vector< SUserItemInfo* > vUserItemInfo;
	
	pUser->GetUserItemList()->GetEquipItemList( vUserItemInfo );
	
	if( vUserItemInfo.empty() ) 
	{
		return;
	}
	
	pUser->CheckExpireItem();
	
	time_t CurrentTime = _time64(NULL);
	
	int iBasicItemCount = 0;
	for (int iIndex=0; iIndex < vUserItemInfo.size(); iIndex++)
	{
		if (vUserItemInfo[iIndex]->iSellType == 0)
		{
			
			if (vUserItemInfo[iIndex]->iChannel == ITEMCHANNEL_HAIR ||			
				vUserItemInfo[iIndex]->iChannel == ITEMCHANNEL_UP || 			
				vUserItemInfo[iIndex]->iChannel == ITEMCHANNEL_DOWN || 			
				vUserItemInfo[iIndex]->iChannel == ITEMCHANNEL_SHOES)
			{
				iBasicItemCount++;
			}
		}
	}
	
	CPacketComposer PacketComposer(S2C_MY_INFO_ITEM_RES);
	PacketComposer.Add(vUserItemInfo.size() - iBasicItemCount);
	
	for(int i=0 ; i< vUserItemInfo.size() ;i++)
	{
		if (vUserItemInfo[i]->iSellType == 0)
		{
			if (vUserItemInfo[i]->iChannel == ITEMCHANNEL_HAIR ||			
				vUserItemInfo[i]->iChannel == ITEMCHANNEL_UP || 			
				vUserItemInfo[i]->iChannel == ITEMCHANNEL_DOWN || 			
				vUserItemInfo[i]->iChannel == ITEMCHANNEL_SHOES)
			{
				continue;
			}
		}
		
		PacketComposer.Add(vUserItemInfo[i]->iItemIdx);
		PacketComposer.Add(vUserItemInfo[i]->iItemCode);
		
		int iSellPrice = 0;
		
		SShopItemInfo ShopItemInfo;
		pItemShop->GetItem(vUserItemInfo[i]->iItemCode,ShopItemInfo);
		
		if( ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST )	// 개수제 아이템 
		{
			iSellPrice = ( ( vUserItemInfo[i]->iSellPrice ) * vUserItemInfo[i]->iPropertyTypeValue * 5 ) / 100 ;
		}
		else if( ShopItemInfo.iPropertyType == ITEM_PROPERTY_TIME ) // 기간제 아이템
		{
			if (vUserItemInfo[i]->iPropertyTypeValue == -1)
			{
				iSellPrice = (vUserItemInfo[i]->iSellPrice * 5) / 100;
			}
			else
			{
				int iRemainHours = 0;
				double dDiffSeconds = difftime(CurrentTime, vUserItemInfo[i]->ExpireDate);
				if( dDiffSeconds > 0 )
				{
					iSellPrice = 0;
				}
				else
				{
					iRemainHours = 	(int)(dDiffSeconds / 3600)*(-1);
					if( vUserItemInfo[i]->iPropertyTypeValue != 0 )
					{
						iSellPrice = ((vUserItemInfo[i]->iSellPrice * 5) / 100) * iRemainHours / vUserItemInfo[i]->iPropertyTypeValue;
					}
					else
					{
						iSellPrice = 0;
					}
				}
			}			
		}
		else
		{
			iSellPrice = (vUserItemInfo[i]->iSellPrice * 5) / 100;
		}
		
		PacketComposer.Add(iSellPrice);
		
		iStatus = vUserItemInfo[i]->iStatus;
		//1: 미착용 2: 다른애가착용 3:내가착용
		//0: 미착용 1: 착용
		if( 1 == iStatus )
			iStatus = 0;
		else if( 2 == iStatus )
			iStatus = 1;
		
		PacketComposer.Add(iStatus);
		int iPropertyType = vUserItemInfo[i]->iPropertyType;
		PacketComposer.Add(vUserItemInfo[i]->iPropertyType);
		
		if( vUserItemInfo[i]->iPropertyType == 1 )
		{
			int iDay = -1;
			int iHour = 0;
			int iMin = 0;
			
			if(vUserItemInfo[i]->iPropertyTypeValue != -1)
			{
				int iDiffSecond = difftime(vUserItemInfo[i]->ExpireDate, CurrentTime);	
				
				iDay = iDiffSecond / 86400;
				iHour = (iDiffSecond ) / 3600;
3600;
				iMin = (iDiffSecond ) / 60;
 60;
			}
			
			PacketComposer.Add(iDay);
			PacketComposer.Add(iHour);
			PacketComposer.Add(iMin);
		}
		if( vUserItemInfo[i]->iPropertyType == -2 )
		{
			int iDay = -1;
			int iHour = 0;
			int iMin = 0;
			
			if(vUserItemInfo[i]->iPropertyTypeValue != -1)
			{
				int iDiffSecond = difftime(vUserItemInfo[i]->ExpireDate, CurrentTime);	

				iDay = iDiffSecond / 86400;
				iHour = (iDiffSecond ) / 3600;
3600;
				iMin = (iDiffSecond ) / 60;
 60;
			}

			PacketComposer.Add(iDay);
			PacketComposer.Add(iHour);
			PacketComposer.Add(iMin);
		}
		else if( vUserItemInfo[i]->iPropertyType == 2)
		{
			PacketComposer.Add(vUserItemInfo[i]->iPropertyTypeValue);
		}
		
		int iPropertyKind = vUserItemInfo[i]->iPropertyKind;
		
		if(iPropertyKind >= MIN_WIN_TATTOO_KIND_NUM && iPropertyKind <= MAX_WIN_TATTOO_KIND_NUM)
		{
			iPropertyKind = 0;
		}
		
		int iUserItemPropertyNum = vUserItemInfo[i]->iPropertyNum;
		
		if( iPropertyKind > 0 )
		{
			iPropertyKind = iUserItemPropertyNum;
		}
		
		// 속성 주는 곳 
		vector< SUserItemProperty > vUserItemProperty;
		
		PacketComposer.Add(iPropertyKind);
		PacketComposer.Add(&iUserItemPropertyNum);
		
		if( iUserItemPropertyNum > 0 )
		{
			pUser->GetUserItemList()->GetCurAvatarItemList()->GetItemProperty(vUserItemInfo[i]->iItemIdx, vUserItemProperty);
		}
		
		for (int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size() ; iPropertyCount++ )
		{
			int iProperty	= vUserItemProperty[iPropertyCount].iProperty;
			int iValue		= vUserItemProperty[iPropertyCount].iValue;
			
			PacketComposer.Add(&iProperty);
			PacketComposer.Add(&iValue);
		}
	}
	
	PacketComposer.Add((int)0);	//ToDo - delete League embelm
	PacketComposer.Add(pUser->GetEquippedAchievementTitlebyAvatarIdx(pUser->GetCurAvatarIdx()));

	pClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSSetupChatOption(CFSGameClient *pFSClient)
{
	CFSGameUser  * pUser = (CFSGameUser *) pFSClient->GetUser();
	if( NULL == pUser ) return;

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );
	
	int iResult = -1;
	
	int aLinkIcon[MAX_EMOTICON_NUM];
	memset(aLinkIcon, 0, sizeof (int) * MAX_EMOTICON_NUM);
	
	char szChatMacro1[MAX_CHAT_MACRO_LENGTH+1];
	char szChatMacro2[MAX_CHAT_MACRO_LENGTH+1];
	char szChatMacro3[MAX_CHAT_MACRO_LENGTH+1];	
	char szChatMacro4[MAX_CHAT_MACRO_LENGTH+1];	
	
	memset(szChatMacro1, 0, sizeof (char) * MAX_CHAT_MACRO_LENGTH + 1);
	memset(szChatMacro2, 0, sizeof (char) * MAX_CHAT_MACRO_LENGTH + 1);
	memset(szChatMacro3, 0, sizeof (char) * MAX_CHAT_MACRO_LENGTH + 1);
	memset(szChatMacro4, 0, sizeof (char) * MAX_CHAT_MACRO_LENGTH + 1);
	
	for (int i = 0; i< MAX_EMOTICON_NUM; i++)
	{		 
		m_ReceivePacketBuffer.Read(&aLinkIcon[i]); 
	}
	
	m_ReceivePacketBuffer.Read((PBYTE)szChatMacro1, sizeof(char) * (MAX_CHAT_MACRO_LENGTH + 1) ); 
	m_ReceivePacketBuffer.Read((PBYTE)szChatMacro2, sizeof(char) * (MAX_CHAT_MACRO_LENGTH + 1) ); 
	m_ReceivePacketBuffer.Read((PBYTE)szChatMacro3, sizeof(char) * (MAX_CHAT_MACRO_LENGTH + 1) ); 	
	m_ReceivePacketBuffer.Read((PBYTE)szChatMacro4, sizeof(char) * (MAX_CHAT_MACRO_LENGTH + 1) ); 	
	
	szChatMacro1[MAX_CHAT_MACRO_LENGTH] = 0;		
	szChatMacro2[MAX_CHAT_MACRO_LENGTH] = 0;	
	szChatMacro3[MAX_CHAT_MACRO_LENGTH] = 0;
	szChatMacro4[MAX_CHAT_MACRO_LENGTH] = 0;
	
	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->spSetChatMacroOption(pUser->GetUserIDIndex(), aLinkIcon, szChatMacro1, szChatMacro2, szChatMacro3, szChatMacro4, iResult) )
	{		
		CPacketComposer PacketComposer(S2C_SETUP_CHAT_RES);
		PacketComposer.Add(iResult);
		pUser->Send(&PacketComposer);
	}
	else
	{
		iResult = -2;
		CPacketComposer PacketComposer(S2C_SETUP_CHAT_RES);
		PacketComposer.Add(iResult);
		pUser->Send(&PacketComposer);
	} 
}

void CFSGameThread::Process_FSShoutMessage(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	if( NULL == pServer ) return;
	
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	CFSGameUserItem* pUserItem = pUser->GetUserItemList();
	if( NULL == pUserItem )	return;
	
	CAvatarItemList* pItemList = pUserItem->GetCurAvatarItemList();
	if( NULL == pItemList )	return;
	
	CPacketComposer Packet(S2C_SHOUT_MESSAGE_RES);
	
	SUserItemInfo* pItem = pItemList->GetItemWithPropertyKind(SHOUT_ITEM4_KIND_NUM);
	if( NULL == pItem ) 
	{
		Packet.Add((int)-2);
		pFSClient->Send(&Packet);
		return;
	}
	
	int iItemCode = pItem->iItemCode;
	if( iItemCode <= 0 )
	{
		Packet.Add((int)-3);
		pFSClient->Send(&Packet);
		return;
	}

	int iMessageLen;
	char szMessage[MAX_SHOUT_MESSAGE_LEN+1];
	memset(szMessage, 0, sizeof(char)*(MAX_SHOUT_MESSAGE_LEN+1));
	
	m_ReceivePacketBuffer.Read(&iMessageLen);	
	
	if(iMessageLen <= 0 || MAX_SHOUT_MESSAGE_LEN < iMessageLen) 
	{
		Packet.Add((int)-17);
		pFSClient->Send(&Packet);
		return;
	}
	
	m_ReceivePacketBuffer.Read((BYTE*)szMessage, iMessageLen);	
	szMessage[iMessageLen] = '\0';
	
	CCenterSvrProxy* pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	if(NULL == pCenterProxy) 
	{
		Packet.Add((int)-3);
		pFSClient->Send(&Packet);
	}
	else
	{ 
		pCenterProxy->SendShoutMessageReq(pUser->GetGameID(), pUser->GetGameIDIndex(), pItem->iItemIdx, iItemCode, szMessage);
	}
}

void CFSGameThread::Process_FSRealTimeMessage_Req(CFSGameClient* pFSClient)
{
	// 운영자 인지아닌지 판별 해야쥐 
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser )	return;

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );
	
	int iResult = -1;
	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->GM_CheckGMUser(pUser->GetUserIDIndex(), iResult ) )
	{
		return;
	}
	
	if( iResult != 0 )
	{
		return;
	}
	
	DECLARE_INIT_TCHAR_ARRAY(szGMMsg,	MAX_GM_MSG_LENGTH + 1);
	
	int iMsgType = 0; 
	
	m_ReceivePacketBuffer.Read(&iMsgType);
	m_ReceivePacketBuffer.Read((PBYTE) szGMMsg, sizeof(char) * (MAX_GM_MSG_LENGTH+1) );
	
	if( iMsgType == 0 || iMsgType == 1 )
	{
		CFSGameServer::GetInstance()->BroadCastSendGMMsg(iMsgType, szGMMsg);
	}
}

void CFSGameThread::ProcessPacket_Log( CIOCPSocketClient * pIOCPSocketClient )
{
	CFSGameClient * pFSClient = (CFSGameClient *)	pIOCPSocketClient;
	
	if( NULL == pFSClient )
	{
		return;
	}
	
#ifdef _DEBUG
	TRACE ( "CFSGameThread::Packet Num:11866902 TICK:0 \r\n", m_ReceivePacketBuffer.GetCommand(), GetTickCount() ) ; 
f

	int iStartTick = GetTickCount();
	
	pFSClient->SetPacketEvent();
	
	CFSGameServer * pServer = CFSGameServer::GetInstance();
	if( NULL == pServer )
	{
		return;
	}
	
	if( !pServer->GetEnable() && pFSClient->GetState() < FS_PROXY) return;
	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

// 20090513 GM여부 판별	
	if( pUser != NULL )
	{
		if( pUser->CheckGMUser() == 1 )
		{
			if (ProcessGMPacket( pFSClient ))
				return;
		}
	}

	switch (m_ReceivePacketBuffer.GetCommand())
	{
	case 10 :	// Start Encryption
		{
			pFSClient->SendKey();
			pFSClient->SetEncrypt();
		}
		return;
	case 12 :	// Start Decryption
		{
			pFSClient->SetDecrypt();
		}
		return;
	case MON_ALIVE_ECHO_REQ:			pFSClient->SetMonAliveClient(TRUE);		//not return; excute Process_MonitorAliveEcho
	case MON_ALIVE_ECHO:				Process_MonitorAliveEcho(pFSClient);		return;
	}
	
	switch( pFSClient->GetState() )
	{
	case NEXUS_INITIAL:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{			
			case C2S_REQ_USERCERT:			Process_FSLogin(pFSClient); break; 
			case 21000			:			Process_FSAnnounceCheckAccount(pFSClient); break;
				
				//////////////////////
			}
		}
		return;
	case NEXUS_LOGINSUCCESS:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_REQ_USERCERT:		break;
			case 21002:			Process_FSAnnounce(pFSClient); break;
			}
		}
		return;
	case FS_SINGLE:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_TUTORIAL_COMPLET_REQ:		Process_FSTutorialComplete(pFSClient);		break;
			case C2S_MINIGAME_INFO_REQ:			Process_FSMiniGameInfo(pFSClient);			break;
			case C2S_MINIGAME_START_REQ:		Process_FSMiniGameStart(pFSClient);			break;
			case C2S_MINIGAME_COMPLET_REQ:		Process_FSMiniGameComplete(pFSClient);		break;
			case C2S_EPISODE_RESULT_REQ:		Process_FSEpisodeResultReq(pFSClient);		break;
			case C2S_AI_GAME_INFO_REQ:			Process_FSAIGameInfo(pFSClient);			break;
			case C2S_SINGLEGAME_END_REQ:		Process_FSSingleGameEnd(pFSClient);			break;
			case C2S_AI_GAME_RESULT_REQ:		Process_FSAIGameResult(pFSClient);			break;
			}
		}
		break;
	case NEXUS_LOBBY:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_REQ_ROOMLIST:				Process_FSLRequestRoomList(pFSClient);			break;
			case C2S_REQ_CREATE_ROOM:			Process_FSLCreateRoom(pFSClient);				break;
			case C2S_REQ_JOIN_ROOM:				Process_FSLJoinRoom(pFSClient);					break;
			case C2S_QUICK_ROOM_INFO_REQ:		Process_FSLRequestRoomORTeamInfo(pFSClient);	break;
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);			break;
			case C2S_ROOM_MEMBER_REQ:			Process_FSLRoomMemberList(pFSClient);			break;
			case C2S_WAIT_TEAM_LIST_REQ:		Process_FSLTeamList(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:			Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:		Process_FSLEnterTeam(pFSClient);				break;
			case C2S_CLUB_TEAM_LIST_REQ:		Process_FSLClubTeamList(pFSClient);				break;
			case C2S_POSITION_SELECT_RES:		Process_FSLSelectPosition(pFSClient);			break;
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);					break;
			case C2S_CHECK_ALIVE_REQ:			Process_FSCheckAlive(pFSClient);				break;
			case C2S_CASH_INFO_REQ:				Process_FSCashInfo(pFSClient);					break;
			case C2S_SHOPITEM_LIST_REQ:			Process_FSIShopItemList(pFSClient);				break;			
			case C2S_QUICK_JOIN_GAME_AS_OBSERVER_REQ: Process_FSLQuickJoinRoom_As_Observer(pFSClient);	break;			
			case C2S_REMAIN_PLAY_TIME_IN_ROOM_REQ:	Process_RemainPlayTime_In_Room(pFSClient);			break;
			case C2S_FOLLOW_FRIEND_REQ:				Process_FSFollowFriend(pFSClient);					break;
			case C2S_CHEAT_REQ:						Process_Cheat(pFSClient);							break;
			case C2S_AUTO_TEAM_REG_REQ:				ProcessFSTAutoTeamRegister(pFSClient);			break;
			case C2S_SET_OBSERVER_ALLOW_REQ:		Process_SetObserverAllowReq( pFSClient );		break;
			case C2S_PREVIEW_GET_FREESTYLE_REQ:		Process_UserGetFreeStylePreviewReq(pFSClient);	break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			case C2S_ITEM_SELECT_REQ:				Process_FSIItemSelect(pFSClient);				break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_REPEAT_BAN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_BOARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_BINGO_TATOO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_SHUFFLE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_SHUFFLE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_INITIALIZE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_INITIALIZE_REWARD_COST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_INITIALIZE_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_REWARD_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_MAIN_REWARD_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BINGO_TOKEN_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_MATCH_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_MATCH_DETAIL_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_CREATE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RESET_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_ADD_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_ADD_CASH_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_GET_DAY_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_FINISH_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TEST_RECORDBOARD_ADD_MATCH_DATA_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RANKING_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RANKING_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SECRETSTORE_ITEMLIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_INTENSIVEPRACTICE_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_START_INTENSIVEPRACTICE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_INTENSIVEPRACTICE_RANK_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_JOIN_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_RACE_BOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_JOIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_SHOP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LUCKYBOX_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LUCKYBOX_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_LUCKYTICKET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PUZZLES_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PUZZLES_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PUZZLES_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_THREE_KINGDOMS_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_THREE_KINGDOMS_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSIONSHOP_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSIONSHOP_PREVIEW_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSIONSHOP_BUY_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSIONSHOP_MISSION_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSIONSHOP_MISSION_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HONEYWALLET_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HONEYWALLET_OPEN_WALLET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_OPEN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_DISTRICT_SELECT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_SUPPORT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_BUFFITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USE_BUFFITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_DAILY_RANK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_SEASON_RANK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_RANK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USER_RANKING_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USER_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USER_MISSION_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TODAY_HOTDEAL_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SKYLUCKY_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SKYLUCKY_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GOLDENSAFE_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GOLDENSAFE_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
			case C2S_CHECK_CHANGE_DERSS_ITEM_REQ:		Process_CheckDressItemReq(pFSClient);	break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHEERLEADER_EVENT_CHEERLEADER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHEERLEADER_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BUY_EVENT_CHEERLEADER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BUY_EVENT_CHEERLEADER_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_INITIALIZE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_CRUSH_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENTCASH_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PUZZLES_WANT_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_ATTENDANCE_ITEMSHOP_BOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LVUPEVENT_GET_CHAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_EVENT_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_SELL_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_USE_COIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LEGEND_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LEGEND_EVENT_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LEGEND_EVENT_USE_KEY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LEGEND_EVENT_USE_STAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LEGEND_EVENT_GET_TRY_REWARD_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_RANK_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_RANK_MY_AVATAR_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_RANK_MAIN_GAMEID_SELECT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MINIGAMEZONE_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MINIGAMEZONE_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MINIGAMEZONE_USE_TICKET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MINIGAMEZONE_USE_COIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MINIGAMEZONE_UPDATE_SCORE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOVEPAYBACK_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOVEPAYBACK_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOVEPAYBACK_USE_LOVE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BIGWHEEL_LOGIN_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BIGWHEEL_LOGIN_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BIGWHEEL_LOGIN_STAMP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SALEPLUS_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MISSION_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MONEY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FIRSTCASH_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMARROW_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMARROW_POT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMARROW_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMARROW_USE_ARROW_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMARROW_USE_POT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_VENDINGMACHINE_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_VENDINGMACHINE_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_VENDINGMACHINE_INPUT_MONEY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_VENDINGMACHINE_USE_MONEY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DEVILTEMTATION_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DEVILTEMTATION_BONUS_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DEVILTEMTATION_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DEVILTEMTATION_USE_TICKET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DEVILTEMTATION_USE_SOULSTONE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LVUPEVENT_USER_INFO_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEPLAY_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEPLAY_EVNET_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_OPEN_EVENT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LUCKYSTAR_SESSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_CHANGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_GET_REWARD_REQ);
			case C2S_PACKAGEITEM_COMPONENT_LIST_REQ:	Process_PackageItemComponentListReq(pFSClient);	break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEM_TREE_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEM_TREE_EVENT_USE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_AVATAR_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_LOGINPAYBACK_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_LOGINPAYBACK_REQUEST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_CHANGE_PIECE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_USE_PIECE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SONOFCHOICE_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SONOFCHOICE_BUY_RANDOM_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSION_BINGO_BOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSION_BINGO_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSION_BINGO_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSION_BINGO_BOARD_RESET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GUARANTEE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GUARANTEE_BUY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_REWARD_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_USE_COIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SALE_RANDOMITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SALE_RANDOMITEM_OPEN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_RAINBOW_WEEK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_RAINBOW_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_CHARACTER_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_DICE_ROLL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_BUY_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_RANK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_RANK_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_WELCOME_USER_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_WELCOME_USER_EVENT_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_CHEAT_DICE_ROLL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAMEOFDICE_EVENT_CHEAT_TICKET_INIT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GOTO_LOBBY_IN_CHANNEL_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SET_PASSWORD_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SET_PASSWORD_EVENT_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SET_PASSWORD_EVENT_TRY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SET_PASSWORD_EVENT_INITIALIZE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HALFPRICE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HALFPRICE_ITEMLIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HALFPRICE_USE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_LOTTO_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_RANDOM_NUMBER_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_PRESENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_REQ);
			case C2S_CHECK_GAME_START_PREPARATION_RES: Process_FSRCheckGameStartPreparationResponse(pFSClient); break;	// 음성채팅 방장호스트 생성 시, 클라가 너무 빠를때.
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_RANDOMITEM_DRAW_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_RANDOMITEM_DRAW_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_DISCOUNT_ITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_DISCOUNT_ITEM_BUY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_OPEN_BOX_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_OPEN_BOX_UPDATE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_NEWBIE_BENEFIT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_NEWBIE_BENEFIT_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_NEWBIE_BENEFIT_RECEIVING_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_UPDATE_COMMENT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_UPDATE_FRIEND_STATUS_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_REWARD_RECEIVING_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_MYMISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_ADD_FRIEND_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_FRIEND_INVITE_KING_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_INVITE_CHECK_INVITE_CODE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSION_MAKEITEM_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSION_MAKEITEM_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSION_MAKEITEM_MAKE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSIONCASHLIMIT_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MISSIONCASHLIMIT_GIVE_CASH_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COLORINGPLAY_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COLORINGPLAY_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COLORPLAY_USE_PENCIL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COLORPLAY_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECOMMEND_ABILITY_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_MAIN_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HELLONEWYEAR_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HELLONEWYEAR_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_UPDATE_REWARD_STATUS_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_POTION_MAKING_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_POTION_MAKING_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_POTION_MAKING_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIMONGO_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIMONGO_OPEN_PRIMONBALL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIMONGO_GET_FREE_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIVATEROOM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIVATEROOM_EXCHANGEITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIVATEROOM_GET_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_STEELBAG_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_STEELBAG_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_STEELBAG_STAMP_COUPON_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COMEBACK_BENEFIT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COMEBACK_BENEFIT_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COMEBACK_BENEFIT_RECEIVING_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_USE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_APPLY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GIVE_MAGICBALL_STATUS_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_GIVE_MAGICBALL_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_UNLIMITED_TATOO_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_UNLIMITED_TATOO_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_COVET_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_COVET_ITEM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_COVET_ITEM_SELECT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_COVET_SHOP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_COVET_SHOP_BUY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_COVET_USE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PROMISE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PROMISE_RECEIVING_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_USE_MAGIC_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_GOD_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_GOD_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_GOD_USE_CHIP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SPECIAL_PIECE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SPECIAL_PIECE_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PACKAGEITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PACKAGEITEM_PACK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_NEW_CLUB_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_NEW_CLUB_INFO_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_WHITEDAY_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_WHITEDAY_GET_REWARD_REQ);

			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_EXP_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_EXP_ACCEPT_REQ);

			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_GACHA_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_RANK_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_RANK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_AVATAR_FEATURE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_DAILY_LOGIN_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_DAILY_LOGIN_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SPEECH_BUBBLE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_PACKAGE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_SLOT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ);
			}
		}
		break;
	case FS_TEAM:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_READY_TEAM_LIST_REQ:		Process_FSTReadyTeamList(pFSClient);			break;
			case C2S_TEAM_MEMBER_REQ:			Process_FSTRequestTeamMemberList(pFSClient);	break;
			case C2S_TEAM_INFO_IN_TEAM_REQ:		Process_FSTRequsetTeamInfo(pFSClient);			break;
			case C2S_EXIT_WAIT_TEAM_REQ:		Process_FSTExitWaitTeam(pFSClient);				break;
			case C2S_REQUEST_TEAM_MATCH_REQ:	Process_FSTRequestTeamMatch(pFSClient);			break;
			case C2S_INVITE_USER_REQ:				Process_FSRInviteUser(pFSClient);				break;
			case C2S_CHANGE_ROOM_PASS_REQ:		Process_FSTChanageTeamPassword(pFSClient);		break;
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);			break;
			case C2S_MAKE_USER_EXIT:			Process_FSTForceOutUser(pFSClient);				break;
			case C2S_CASH_INFO_REQ:				Process_FSCashInfo(pFSClient);					break;
			case C2S_CHANGE_POSITION_REQ:		Process_FSTChangeUserPosition(pFSClient);		break;		
			case C2S_MYITEM_BAG_REQ:			Process_FSTGetMyItemBag(pFSClient);				break;
			case C2S_BAG_ITEM_APPLY_REQ:		Process_FSApplyBagItem(pFSClient);				break;
			case C2S_MYSKILL_BAG_REQ:			Process_FSTGetMySkillBag(pFSClient);			break;
			case C2S_BAG_SKILL_APPLY_REQ:		Process_FSApplyBagSkill(pFSClient);				break;				
			case C2S_CHIEF_ASSIGN_CHECK_REQ:	Process_FSTCheckChiefAssign(pFSClient);			break;
			case C2S_CHIEF_ASSIGN_MOVE_REQ:		Process_FSTMoveChief(pFSClient);				break;
			case C2S_CHANGE_TEAM_NAME_REQ:		Process_FSTChangeTeamName(pFSClient);			break;
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);					break;
			case C2S_QUICK_JOIN_GAME_AS_OBSERVER_REQ:	Process_FSLQuickJoinRoom_As_Observer(pFSClient);break;			
			case C2S_CHANGE_TEAM_MODE_REQ:				Process_FSTChangeTeamMode(pFSClient);			break;
			case C2S_CHANGE_TEAM_ALLOW_OBSERVE_REQ:		ProcessFSTChangeTeamAllowObserve(pFSClient);	break;
			case C2S_REG_TEAM_REQ:				ProcessFSTChallengeRegister(pFSClient);			break;
			case C2S_AVOID_TEAM_REQ:			ProcessFSTChangeAvoidTeam(pFSClient);			break;
			case C2S_AUTO_REG_REQ:				ProcessFSTAutoRegister(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:			ProcessFSTAutoTeamRegister(pFSClient);			break;
			case C2S_USER_ACTION_INVENTORY_LIST_REQ: Process_UserActionInventoryListReq(pFSClient);	break;
			case C2S_USER_KEYACTION_SLOT_REQ:	Process_UserKeyActionSlotReq(pFSClient);		break;
			case C2S_USER_SEREMONY_SLOT_REQ:	Process_UserSeremonySlotReq(pFSClient);			break;
			case C2S_USER_ACTION_CHANGE_REQ:	Process_UserActionChangeReq(pFSClient);			break;
			case C2S_ROOM_ACTION_ANI_REQ:		Process_ActionRoomAniReq(pFSClient);			break;
			case C2S_USER_ACTION_SLOT_APPLYSET_REQ: Process_ActionRoomApplySetReq(pFSClient);	break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_MATCH_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_MATCH_DETAIL_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_CREATE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RESET_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_ADD_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_ADD_CASH_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_GET_DAY_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_FINISH_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TEST_RECORDBOARD_ADD_MATCH_DATA_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RANKING_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RANKING_USER_INFO_REQ);

			case C2S_COACH_CARD_POTENTIAL_ABILITY_REQ:		Process_CoachCardPotentialAbility(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_BALANCE_REQ:		Process_CoachCardPotentialRageBalance(pFSClient);		break;
			case C2S_COACH_CARD_POTENTIAL_ACTION_REQ:		Process_CoachCardPotentialAction(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_FREESTYLE_REQ:	Process_CoachCardPotentialFreeStyle(pFSClient);			break;

			case C2S_ITEM_SELECT_REQ:				Process_FSIItemSelect(pFSClient);				break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLONE_CHARACTER_CHANGE_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_JOIN_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_RACE_BOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_JOIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_DISTRICT_SELECT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_SUPPORT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_BUFFITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USE_BUFFITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_DAILY_RANK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USER_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USER_MISSION_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHANGE_BASKETBALL_SKIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_CHANGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_ROOM_READY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MISSION_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MONEY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_REQ);

			case C2S_CHECK_GAME_START_PREPARATION_RES: Process_FSRCheckGameStartPreparationResponse(pFSClient); break;
			case C2S_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_RES: Process_FSRCheckPublicIPAddressForGameResponse(pFSClient); break;
			case C2S_START_UDP_HOLEPUNCHING_RES: Process_FSRStartUDPHolepunchingResponse(pFSClient); break;
			case C2S_START_BANDWIDTH_CHECK_RES: Process_FSRStartBandWidthResponse(pFSClient); break;
			case C2S_READY_HOST_RES: Process_FSRReadyHostResponse(pFSClient); break;
			case C2S_CONNECT_HOST_RES: Process_FSRConnectHostResponse(pFSClient); break;

			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_AIPVP_CHANGE_GAMEPLAYERNUM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_CHANGE_AI_POSITION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_SENTENCE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_SENTENCE_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_IN_ROOM_WEAR_REQ);
			}		
		}
		break;
	case FS_RANKING:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_TOTAL_RANKING_REQ:			Process_FSTotalRankingList(pFSClient);			break;
			case C2S_USER_RANKING_REQ:			Process_FSUserRanking(pFSClient);				break;
			case C2S_EXIT_RANKING_REQ:			Process_FSExitRankingPage(pFSClient);			break;							
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);					break;
			case C2S_CHECK_ALIVE_REQ:			Process_FSCheckAlive(pFSClient);				break;				
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);			break;
			case C2S_CASH_INFO_REQ:				Process_FSCashInfo(pFSClient);					break;
			case C2S_REQ_JOIN_ROOM:				Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:			Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:		Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:			ProcessFSTAutoTeamRegister(pFSClient);			break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			case C2S_RANK_SEARCH_REQ:			Process_RankSearchReq(pFSClient);				break;		
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);		
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_APPRAISAL_RANK_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_APPRAISAL_USER_RANK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_RANKMATCH_LYAER_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_RANK_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_RANK_SEASON_RECORD_REQ);

			}
		}	
		break;
	case FS_RANK_MATCH:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_RANKMATCH_LYAER_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_RANK_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_RANK_SEASON_RECORD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_ROOM_READY_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);					break;
			case C2S_CHECK_ALIVE_REQ:			Process_FSCheckAlive(pFSClient);				break;				
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);			break;
			case C2S_CASH_INFO_REQ:				Process_FSCashInfo(pFSClient);					break;
			case C2S_REQ_JOIN_ROOM:				Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:			Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:		Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:			ProcessFSTAutoTeamRegister(pFSClient);			break;
			}
		}
		break;
	case FS_ITEM:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_EXIT_ITEMPAGE_REQ:			Process_FSIExitItemPage(pFSClient); 		break;
			case C2S_USERITEM_LIST_REQ:			Process_FSIUserItemList(pFSClient);			break;
			case C2S_SHOPITEM_LIST_REQ:			Process_FSIShopItemList(pFSClient);			break;
			case C2S_ITEM_SELECT_REQ:			Process_FSIItemSelect(pFSClient);			break;
			case C2S_RESET_FEATURE_REQ:			Process_FSIResetFeature(pFSClient);			break;
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);				break;
			case C2S_CHECK_ALIVE_REQ:			Process_FSCheckAlive(pFSClient);			break;
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);		break;
			case C2S_CASH_INFO_REQ:				Process_FSCashInfo(pFSClient);				break;
			case C2S_OPEN_T_BOX:				Process_FSIOpenTreasureBox(pFSClient);		break;
			case C2S_SEND_GIFT_GAME_ID_REQ:		Process_FSSendGiftGameID(pFSClient);		break;
			case C2S_MY_OWN_ITEM_LIST_REQ:		ProcessMyOwnItemList(pFSClient);			break;
			case C2S_CHANGE_HEIGHT_REQ:			Process_FSIChangeHeight(pFSClient);			break;
			case C2S_HEIGHT_SETUP_REQ:			Process_FSISetupHeight(pFSClient);			break;
			case C2S_CHANGE_NAME_REQ:			Process_FSIChangeName(pFSClient);			break;
			case C2S_CHECK_NAME_REQ:			Process_FSICheckName(pFSClient);			break;
			case C2S_USER_ITEM_LOCK_REQ:		Process_FSIUserItemLock(pFSClient);			break;
			case C2S_CHANGE_ITEM_COLOR_REQ:		Process_ChangeItemColorReq(pFSClient);		break;
			case C2S_CHANNEL_BUFF_ITEM_CHECK_USE_REQ: Process_ChannelBuffItemCheckUseReq(pFSClient);break;
			case C2S_CHANNEL_BUFF_ITEM_USE_REQ:	Process_ChannelBuffItemUseReq(pFSClient);	break;
			case C2S_CHECK_CHANGE_DERSS_ITEM_REQ:		Process_CheckDressItemReq(pFSClient);	break;
			case C2S_REGISTER_CHANGE_DERSS_ITEM_REQ:	Process_RegisterDressItemReq(pFSClient);break;
			case C2S_PACKAGEITEM_COMPONENT_LIST_REQ:	Process_PackageItemComponentListReq(pFSClient);	break;
		//	case C2S_USE_SPECIALPROPERTY_ITEM_REQ:		Process_UseSpecialPropertyItemReq(pFSClient);break;
		//	case C2S_USE_CHANGE_ITEM_REQ:				Process_ChangeItemReq(pFSClient);break;
				
			case C2S_REQ_JOIN_ROOM:				Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:			Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:		Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:			ProcessFSTAutoTeamRegister(pFSClient);			break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_VOICE_SETTING_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GUARANTEE_BUY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GUARANTEE_INFO_REQ);

			case C2S_CHANGE_USE_STYLE_IN_ITEMSHOP_REQ:	Process_ChangeUseStyleInItemShopReq(pFSClient);			break;
			
			case C2S_USE_ITEM_REQ:						Process_UseItemReq(pFSClient);							break;
			case C2S_COACH_CARD_INFO_REQ:				Process_CoachCardInfo(pFSClient);	
			case C2S_COACH_CARD_SUB_ACTIONINFLUENCE_REQ:	Process_CoachCardSubActionInfluence(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_ABILITY_REQ:		Process_CoachCardPotentialAbility(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_BALANCE_REQ:		Process_CoachCardPotentialRageBalance(pFSClient);		break;
			case C2S_COACH_CARD_POTENTIAL_ACTION_REQ:		Process_CoachCardPotentialAction(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_FREESTYLE_REQ:	Process_CoachCardPotentialFreeStyle(pFSClient);			break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TOKEN_EXCHANGE_ITEM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TOKEN_EXCHANGE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COMPOUNDINGITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_COMPOUNDINGITEM_COMBINE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CUSTOMIZE_ITEM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CUSTOMIZE_ITEM_MAKE_FINISH_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CUSTOMIZE_ITEM_CHECK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLONE_CHARACTER_CHANGE_INVENTORY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_FACE_CHANGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXPBOX_ITEM_USE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXPBOX_ITEM_USE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXCHANGE_NAME_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXCHANGE_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PURCHASE_GRADE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PURCHASE_GRADE_GIVE_EVENT_COIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAGNE_CHAR_STAT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAGNE_CHAR_STAT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MOVE_POTENCARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MOVE_POTENCARD_CHARACTER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MOVE_POTENCARD_SELECT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SECRETSTORE_ITEMLIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PACKAGEITEM_PACK_INFO_REQ);
			}
			break;
		}
	case FS_SKILL:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_EXIT_SKILLPAGE_REQ:		Process_FSSExitSkillPage(pFSClient);	break;
			case C2S_SKILL_LIST_REQ:			Process_FSSSkillList(pFSClient);		break;
			case C2S_BUY_SKILL_REQ:				Process_FSSBuySkill(pFSClient);			break;
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);			break;
			case C2S_CHECK_ALIVE_REQ:			Process_FSCheckAlive(pFSClient);		break;				
			case C2S_USER_SKILL_INVENTORY_REQ:  Process_FSSSkillInventoryList(pFSClient);	break;
			case C2S_USER_SKILL_SLOT_REQ:		Process_FSSSkillSlotList(pFSClient);	break;
			case C2S_INSERT_TO_SLOT_REQ:		Process_FSSInsertToSlot(pFSClient);		break;
			case C2S_INSERT_TO_INVENTORY_REQ:	Process_FSSInsertToInventory(pFSClient);break;				
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);	break;
			case C2S_CASH_INFO_REQ:				Process_FSCashInfo(pFSClient);			break;
			case C2S_USER_ACTION_INVENTORY_LIST_REQ: Process_UserActionInventoryListReq(pFSClient);	break;
			case C2S_USER_KEYACTION_SLOT_REQ:	Process_UserKeyActionSlotReq(pFSClient);	break;
			case C2S_USER_SEREMONY_SLOT_REQ:	Process_UserSeremonySlotReq(pFSClient);		break;
			case C2S_USER_ACTION_CHANGE_REQ:	Process_UserActionChangeReq(pFSClient);		break;
			case C2S_USER_ACTION_SLOT_RESET_REQ:Process_UserActionSlotResetReq(pFSClient);	break;
			case C2S_PREVIEW_APPLY_FREESTYLE_REQ:Process_UserApplyFreeStylePreviewReq( pFSClient );	break;
			case C2S_REQ_JOIN_ROOM:				Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:			Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:		Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:			ProcessFSTAutoTeamRegister(pFSClient);			break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPECIALSKIN_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPECIALSKIN_BUY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPECIALSKIN_INVENTORYLIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPECIALSKIN_EQUIP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPECIALSKIN_UNEQUIP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SKILL_OVERLAPCOMMAND_CHANGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ);
			}
			break;
		}
	case NEXUS_WAITROOM:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_REQ_EXIT_ROOM:			Process_FSRExitRoom(pFSClient);					break;
			case C2S_TEAM_MEMBER_REQ:		Process_FSRRequestTeamMemberList(pFSClient);		break;
			case C2S_TEAM_CHANGE_REQ:		Process_FSRRequestTeamChange(pFSClient);			break;
			case C2S_READY_REQ:				Process_FSRRequestReady(pFSClient);				break;
			case C2S_READY_TOGGLE_REQ:		Process_FSRRequestToggleIamReady(pFSClient);		break;
			case C2S_GAMESTART_PAUSE_REQ:   Process_FSRRequestGameStartPause(pFSClient);		break;
			case C2S_CHANGE_GAME_MODE_REQ:  Process_FSRChangeGameMode(pFSClient);			break;
			case C2S_ROOM_INFO_IN_ROOM_REQ:	Process_FSRRequestRoomInfoInRoom(pFSClient);		break;
			case C2S_LOADING_PROGRESS_SND:	Process_FSRSendLoadingProgress(pFSClient);	break;
			case C2S_CHANGE_COURT_REQ:		Process_FSRChangeGameCourt(pFSClient);	break;
			case C2S_CHANGE_PRACTICEMETHOD_REQ:		Process_FSRChangePracticeMethod(pFSClient);	break;
			case C2S_MAKE_USER_EXIT:		Process_FSRForceOutUser(pFSClient);	 break;
			case C2S_USER_INFO_REQ:			Process_FSLRequestUserInfo(pFSClient);		break;
			case C2S_INVITE_USER_REQ:			Process_FSRInviteUser(pFSClient); break;
			case C2S_INITIALGAME_RES:		Process_FSGInitialGame(pFSClient);				break;
			case C2S_CANCEL_TEAM_MATCH_REQ:	Process_FSTRCancelTeamMatch(pFSClient);		break;
			case C2S_REG_TEAM_REQ:				ProcessFSTChallengeRegister(pFSClient);		break;
			case C2S_TEAM_ESTIMATE_REQ:			ProcessFSRTeamEstimate(pFSClient);		break;
			case C2S_EXIT_WAIT_TEAM_REQ:	Process_FSTExitWaitTeam(pFSClient);	break;
			case C2S_CHANGE_ROOM_PASS_REQ:	Process_FSRChangeRoomPassword(pFSClient);	break;
			case C2S_TEAM_INFO_IN_ROOM:		Process_FSTRSendTeamInfo(pFSClient);	break;
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);	break;		
			case C2S_CHECK_ALIVE_REQ:			Process_FSCheckAlive(pFSClient);	break;
			case C2S_CASH_INFO_REQ:				Process_FSCashInfo(pFSClient);	break;
			case C2S_MYITEM_BAG_REQ:			Process_FSRGetMyItemBag(pFSClient);			break;
			case C2S_BAG_ITEM_APPLY_REQ:		Process_FSApplyBagItem(pFSClient);			break;
			case C2S_MYSKILL_BAG_REQ:			Process_FSRGetMySkillBag(pFSClient);		break;
			case C2S_BAG_SKILL_APPLY_REQ:		Process_FSApplyBagSkill(pFSClient);		break;				
			case C2S_CHIEF_ASSIGN_CHECK_REQ:	Process_FSRCheckChiefAssign(pFSClient);		break;
			case C2S_CHIEF_ASSIGN_MOVE_REQ:		Process_FSRMoveChief(pFSClient);			break;
			case C2S_CHANGE_GAMEROOM_NAME_REQ:	Process_FSChangeGameRoomName(pFSClient);	break;
			case C2S_NOTIFY_OBSERVER_ROOM	: Process_FSSetObserverRoomID(pFSClient);		break;
			case C2S_EXIT_OBSERVING_GAME_REQ : Process_FSExitObservingGame(pFSClient);		break;
			case C2S_QUICK_JOIN_GAME_AS_OBSERVER_REQ: Process_FSLQuickJoinRoom_As_Observer(pFSClient); break;

			case C2S_CHECK_GAME_START_PREPARATION_RES: Process_FSRCheckGameStartPreparationResponse(pFSClient); break;
			case C2S_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_RES: Process_FSRCheckPublicIPAddressForGameResponse(pFSClient); break;
			case C2S_START_UDP_HOLEPUNCHING_RES: Process_FSRStartUDPHolepunchingResponse(pFSClient); break;
			case C2S_START_BANDWIDTH_CHECK_RES: Process_FSRStartBandWidthResponse(pFSClient); break;
			case C2S_READY_HOST_RES: Process_FSRReadyHostResponse(pFSClient); break;
			case C2S_CONNECT_HOST_RES: Process_FSRConnectHostResponse(pFSClient); break;

			case C2S_CHANGE_TEAM_ALLOW_OBSERVE_REQ: ProcessFSTChangeRoomAllowObserve(pFSClient); break; //20101126 jhwoo

			case C2S_USER_ACTION_INVENTORY_LIST_REQ: Process_UserActionInventoryListReq(pFSClient);	break;
			case C2S_USER_KEYACTION_SLOT_REQ:	Process_UserKeyActionSlotReq(pFSClient);	break;
			case C2S_USER_SEREMONY_SLOT_REQ:	Process_UserSeremonySlotReq(pFSClient);		break;
			case C2S_USER_ACTION_CHANGE_REQ:	Process_UserActionChangeReq(pFSClient);		break;
			case C2S_ROOM_ACTION_ANI_REQ:		Process_ActionRoomAniReq(pFSClient);		break;
			case C2S_USER_ACTION_SLOT_APPLYSET_REQ: Process_ActionRoomApplySetReq(pFSClient); break;
			case C2S_CHANGE_TEAM_NAME_REQ:		Process_FSTChangeTeamName(pFSClient);			break;

			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_MATCH_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_MATCH_DETAIL_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_CREATE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RESET_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_ADD_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_ADD_CASH_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_GET_DAY_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_FINISH_PAGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TEST_RECORDBOARD_ADD_MATCH_DATA_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RANKING_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECORDBOARD_RANKING_USER_INFO_REQ);

			case C2S_COACH_CARD_POTENTIAL_ABILITY_REQ:		Process_CoachCardPotentialAbility(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_BALANCE_REQ:		Process_CoachCardPotentialRageBalance(pFSClient);		break;
			case C2S_COACH_CARD_POTENTIAL_ACTION_REQ:		Process_CoachCardPotentialAction(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_FREESTYLE_REQ:	Process_CoachCardPotentialFreeStyle(pFSClient);			break;
			case C2S_ITEM_SELECT_REQ:						Process_FSIItemSelect(pFSClient);						break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_GAME_START_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLONE_CHARACTER_CHANGE_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_JOIN_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_RACE_BOARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_JOIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_DISTRICT_SELECT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_SUPPORT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_BUFFITEM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USE_BUFFITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_DAILY_RANK_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USER_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FACTION_USER_MISSION_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_CHANGE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_ROOM_READY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_READY_TIME_CHECK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MISSION_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_PRODUCT_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_USER_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MONEY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_UPDATE_ROOM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_CHANGE_AI_POSITION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_SENTENCE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_SENTENCE_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_IN_ROOM_WEAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_REQ);
			}
		}
		break;
	case NEXUS_GAMEROOM:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_SCORE_ADD_REQ:					Process_FSGRequestScoreChange(pFSClient);		break;
			case C2S_PLAY_READY_OK_REQ:				Process_FSGPlayReadyOk(pFSClient);				break;
			case C2S_GAME_START_OK_REQ:				Process_FSGPlayStartOK(pFSClient);				break;
			case C2S_QUARTER_END_ACK:				Process_FSGQuarterEndAck(pFSClient);			break;
			case C2S_SHOOT_TRY:						Process_FSGShootTry(pFSClient);					break;
			case C2S_REBOUND:						Process_FSGRebound(pFSClient);					break;
			case C2S_ASIST:							Process_FSGAsist(pFSClient);					break;
			case C2S_STEAL:							Process_FSGSteal(pFSClient);					break;
			case C2S_BLOCK:							Process_FSGBlock(pFSClient);					break;
			case C2S_JUMPBALL:						Process_FSGJumpBall(pFSClient);					break;
			case C2S_SHOOT_SUCCESS_OR_FAILURE:		Process_FSGShootSuccessorFailure(pFSClient);	break;
			case C2S_GAME_RESULT_REQ:				Process_FSGSendGameResult(pFSClient);			break;
			case C2S_GAME_TERMINATED_RES:			Process_FSGTerminateGame(pFSClient);			break;
			case C2S_PEER_DISCONNECTED_REQ:			Process_FSGDisconnectedGame(pFSClient);			break;	
			case C2S_EXTRA_QUARTER_START:			Process_FSGChangeGameStatus(pFSClient);			break;
			case C2S_GAME_TIME_RES:					Process_FSGSetGameTime(pFSClient);				break;
			case C2S_CHECK_GAME_PLAY_REQ:			Process_FSRCheckGamePlayInput(pFSClient);		break;
			case C2S_SET_OPTION_REQ:				Process_FSSetOption(pFSClient);					break;
			case C2S_CHECK_ALIVE_REQ:				Process_FSCheckAlive(pFSClient);				break;
			case C2S_REQ_EXIT_ROOM:					Process_FSRExitRoom(pFSClient);					break;
			case C2S_USE_SECOND_BOOST_ITEM_REQ:		Process_UseSecondBoostItem(pFSClient);			break;				
			case C2S_SECOND_BOOSTITEM_EXPIRED_REQ:	Process_SecondBoostItemExpired(pFSClient);		break;									
			case C2S_GAME_START_HALFTIME_REQ:		Process_FSRGameStartHalfTime(pFSClient);		break;		
			case C2S_USE_PAUSEITEM_REQ:				Process_FSUsePauseItem(pFSClient);				break;
			case C2S_GAME_PAUSE_REQ:				Process_FSGamePause(pFSClient);					break;
			case C2S_CHECK_AVATAR_STATUS_RES :		
			case C2S_CHECK_AVATAR_STATUS1_RES :		
			case C2S_CHECK_AVATAR_STATUS2_RES :		
													Process_FSCheckAvatarStatus(pFSClient);			break;
			case C2S_CHECK_STATUS_CHECKSUM_RES:
			case C2S_CHECK_STATUS_CHECKSUM1_RES:
			case C2S_CHECK_STATUS_CHECKSUM2_RES:
													Process_FSCheckSumStatus(pFSClient);			break;
			case C2S_DATA_HACK_DETECTED :			Process_FSCheckClientHack(pFSClient);			break;
			case C2S_NOTIFY_OBSERVER_ROOM	:		Process_FSSetObserverRoomID(pFSClient);			break;
			case C2S_EXIT_OBSERVING_GAME_REQ :		Process_FSExitObservingGame(pFSClient);			break;
			case C2S_INTENTIONAL_FOUL_REQ:			Process_FSPunishment(pFSClient);				break;
			case C2S_GIVEUP_VOTE_REQ:				Process_FSGiveupVote(pFSClient);				break;
			case C2S_CHANGE_PRACTICEMETHOD_REQ:		Process_FSRChangePracticeMethod(pFSClient);		break;
			case C2S_TEAM_CHANGE_INGAME_REQ:		Process_FSRChangeTeamInGame(pFSClient);			break;
			case C2S_PLAYER_USE_HACK_NOTIFY_RES:	Process_FSGUseHackNotifyRes(pFSClient);			break;
			case C2S_EXPOSE_REQ:					Process_FSRExposeReq(pFSClient);				break;
			case C2S_OPEN_POINT_REWARD_CARD_REQ:	Process_OpenPointRewardCardReq(pFSClient);		break;
			
			case C2S_COACH_CARD_POTENTIAL_ABILITY_REQ:		Process_CoachCardPotentialAbility(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_BALANCE_REQ:		Process_CoachCardPotentialRageBalance(pFSClient);		break;
			case C2S_COACH_CARD_POTENTIAL_ACTION_REQ:		Process_CoachCardPotentialAction(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_FREESTYLE_REQ:	Process_CoachCardPotentialFreeStyle(pFSClient);			break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_GAME_START_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LEFT_SEAT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTOR_STAT_MODULATION_CHECK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOSEBALL_CATCH);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_APPRAISAL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_GAME_EXIT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_GAMEEND_DETAIL_RECORD_NOT);

			case C2S_CHECK_GAME_START_PREPARATION_RES: Process_FSRCheckGameStartPreparationResponse(pFSClient); break;
			case C2S_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_RES: Process_FSRCheckPublicIPAddressForGameResponse(pFSClient); break;
			case C2S_READY_HOST_RES: Process_FSRReadyHostResponse(pFSClient); break;
			case C2S_CONNECT_HOST_RES: Process_FSRConnectHostResponse(pFSClient); break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TRANSFORM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TRANSFORM_DISABLE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TRANSFORM_STATUS_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_OPEN_BOX_UPDATE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_INGAME_SCORE_ADD_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_FALL_DOWN_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_DROP_BALL_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_CHEAP_OUT_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT);
			}
		}
		break;
	case NEXUS_OBSERVING:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_EXIT_OBSERVING_GAME_REQ:	Process_FSExitObservingGame(pFSClient);			break;
			}
		}
		break;
	case FS_CLUB:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_USE_ITEM_REQ:						Process_UseItemReq(pFSClient);								break;
			case C2S_COACH_CARD_INFO_REQ:				Process_CoachCardInfo(pFSClient);							break;
			case C2S_COACH_CARD_SUB_ACTIONINFLUENCE_REQ:Process_CoachCardSubActionInfluence(pFSClient);				break;
			case C2S_USER_INFO_REQ:						Process_FSLRequestUserInfo(pFSClient);						break;	
			case C2S_EXIT_CLUB_REQ:						Process_FSExitClubReq(pFSClient);							break;
			case C2S_CLUB_INFO_REQ:						Process_FSClubInfoReq(pFSClient);							break;	
			case C2S_CLUB_MEMBER_LIST_REQ:				Process_FSClubMemberListReq(pFSClient);						break;
			case C2S_CLUB_LIST_REQ:						Process_FSClubListReq(pFSClient);							break;	
			case C2S_CLUB_MARK_LIST_REQ:				Process_FSClubMarkListReq(pFSClient);						break;	
			case C2S_CLUB_MARK_CHANGE_REQ:				Process_FSClubMarkChangeReq(pFSClient);						break;
			case C2S_CLUB_GRADE_UP_INFO_REQ:			Process_FSClubGradeUpInfoReq(pFSClient);					break;
			case C2S_CLUB_GRADE_UP_REQ:					Process_FSClubGradeUpReq(pFSClient);						break;
			case C2S_CLUB_NOTICE_CHANGE_REQ:			Process_FSClubNoticeChangeReq(pFSClient);					break;
			case C2S_CLUB_JOIN_REQUEST_REQ:				Process_FSClubJoinRequestReq(pFSClient);					break;
			case C2S_CLUB_JOIN_REQUEST_LIST_REQ:		Process_FSClubJoinRequestListReq(pFSClient);				break;
			case C2S_CLUB_JOIN_REQUEST_INFO_REQ:		Process_FSClubJoinRequestInfoReq( pFSClient);				break;
			case C2S_CLUB_JOIN_REQUEST_ANSWER_REQ:		Process_FSClubRequestAnswerReq(pFSClient);					break;
			case C2S_CLUB_REMOVE_MEMBER_REQ:			Process_FSClubRemoveMemberReq(pFSClient);					break;
			case C2S_CLUB_WITHDRAW_REQ:					Process_FSClubWithDrawReq(pFSClient);						break;
			case C2S_CLUB_ADMIN_MEMBER_LIST_REQ:		Process_FSClubAdminMemberListReq(pFSClient);				break;
			case C2S_CLUB_ADD_ADMIN_MEMBER_REQ:			Process_FSClubAddAdminMemberReq(pFSClient);					break;
			case C2S_CLUB_REMOVE_ADMIN_MEMBER_REQ:		Process_FSClubRemoveAdminMemberReq(pFSClient);				break;
			case C2S_CLUB_ADD_KEY_PLAYER_REQ:			Process_FSClubAddKeyPlayerReq(pFSClient);					break;
			case C2S_CLUB_REMOVE_KEY_PLAYER_REQ:		Process_FSClubRemoveKeyPlayerReq(pFSClient);				break;
			case C2S_CLUB_EXTEND_KEY_PLAYER_POPUP_REQ:	Process_FSClubExtendKeyPlayerPopUpReq(pFSClient);			break;
			case C2S_CLUB_EXTEND_KEY_PLAYER_REQ:		Process_FSClubExtendKeyPlayerReq(pFSClient);				break;
			case C2S_CLUB_LINE_BOARD_LIST_REQ:			Process_FSClubLineBoardListReq(pFSClient);					break;
			case C2S_CLUB_ADD_LINE_BOARD_REQ:			Process_FSClubAddLineBoardReq(pFSClient);					break;
			case C2S_CLUB_MEMBER_PR_CHANGE_REQ:			Process_FSClubMemberPRChangeReq(pFSClient);					break;
			case C2S_CLUB_SETUP_POPUP_REQ:				Process_FSClubSetUpPopUpReq(pFSClient);						break;
			case C2S_CLUB_CHECK_CLUBNAME_REQ:			Process_FSClubCheckClubNameReq(pFSClient);					break;
			case C2S_CLUB_SETUP_REQ:					Process_FSClubSetUpReq(pFSClient);							break;
			case C2S_CLUB_TRANSFER_REQ:					Process_FSClubTransferReq(pFSClient);						break;
			case C2S_CLUB_DONATION_POPUP_REQ:			Process_FSClubDonationPopUpReq(pFSClient);					break;
			case C2S_CLUB_DONATION_REQ:					Process_FSClubDonationReq(pFSClient);						break;
			case C2S_CLUB_CREATE_POPUP_REQ:				Process_FSClubCreatePopUpReq(pFSClient);					break;
			case C2S_CLUB_CREATE_REQ:					Process_FSClubCreateReq(pFSClient);							break;
			case C2S_CLUB_MEMBER_RECORD_LIST_REQ:		Process_FSClubMemberRecordListReq(pFSClient);				break;
			case C2S_CLUB_RECENT_MATCH_RECORD_LIST_REQ:	Process_FSClubRecentMatchRecordListReq(pFSClient);			break;
			case C2S_CLUB_PR_LIST_REQ:					Process_FSClubPRListReq(pFSClient);							break;
			case C2S_CLUB_PR_CHANGE_REQ:				Process_FSClubPRChangeReq(pFSClient);						break;
			case C2S_MY_CLUB_RANK_REQ:					Process_FSMyClubRankReq(pFSClient);							break;
			case C2S_CLUB_RANK_LIST_REQ:				Process_FSClubRankListReq(pFSClient);						break;
			case C2S_CLUB_CHEERLEADER_INFO_REQ:			Process_FSClubCheerLeaderInfoReq(pFSClient);				break;
			case C2S_CLUB_BUY_CHEERLEADER_POPUP_REQ:	Process_FSClubBuyCheerLeaderPopUpReq(pFSClient);			break;
			case C2S_CLUB_BUY_CHEERLEADER_REQ:			Process_FSClubBuyCheerLeaderReq(pFSClient);					break;
			case C2S_CLUB_CHEERLEADER_CHANGE_REQ:		Process_FSClubCheerLeaderChangeReq(pFSClient);				break;
			case C2S_CLUB_AREA_LIST_REQ:				Process_FSClubAreaListReq(pFSClient);						break;
//			case C2S_CLUB_BUY_CL_POPUP_REQ:				Process_FSClubBuyCLPopupReq(pFSClient);						break;
			case C2S_CLUB_STATUS_CHANGE_REQ:			Process_FSClubStatusChangeReq(pFSClient);					break;
			case C2S_MY_CLUB_PR_REQ:					Process_FSMyClubPRReq(pFSClient);							break;
//			case C2S_MY_CLUB_CL_REQ:					Process_FSMyClubCLReq(pFSClient);							break;
			case C2S_DELETE_BOARD_MSG_REQ:				Process_FSDeleteBoardMsgReq(pFSClient);						break;
			case C2S_ITEM_SELECT_REQ:					Process_FSIItemSelect(pFSClient);							break;
			case C2S_REQ_JOIN_ROOM:						Process_FSLJoinRoom(pFSClient);								break;
			case C2S_CREATE_TEAM_REQ:					Process_FSLCreateTeam(pFSClient);							break;
			case C2S_ENTER_WAIT_TEAM_REQ:				Process_FSLEnterTeam(pFSClient);							break;
			case C2S_AUTO_TEAM_REG_REQ:					ProcessFSTAutoTeamRegister(pFSClient);						break;
			case C2S_SET_OPTION_REQ:					Process_FSSetOption(pFSClient);								break;
			case C2S_RESET_FEATURE_REQ:					Process_FSIResetFeature(pFSClient);							break;
			case C2S_QUICK_JOIN_GAME_AS_OBSERVER_REQ:	Process_FSLQuickJoinRoom_As_Observer(pFSClient);			break;		
			case C2S_CHECK_CHANGE_DERSS_ITEM_REQ:		Process_CheckDressItemReq(pFSClient);						break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_MISSION_INFO_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_MISSION_GET_BENEFIT_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_PENNANT_SLOT_INFO_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_PENNANT_INFO_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_USE_PENNANT_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_BUY_PENNANT_SLOT_POPUP_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_BUY_PENNANT_SLOT_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_BUY_CLOTHES_POPUP_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_BUY_CLOTHES_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_BUY_CLUBMARK_POPUP_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_BUY_CLUBMARK_REQ)
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CLUBTOURNAMENT_PAGE_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_CLUBTOURNAMENT_PAGE_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_MATCH_TBALE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_KEY_PLAYER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_ENTER_GAMEROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_VOTE_ROOM_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CLUBTOURNAMENT_VOTE_PAGE_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_CLUBTOURNAMENT_VOTE_PAGE_NOT);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_VOTE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_MATCH_RESULT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_BENEFIT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_GET_BENEFIT_ITEM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBTOURNAMENT_GAME_START_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_LEAGUE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_RANKING_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_RANKING_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_RANKING_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_TOURNAMENT_RANKING_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_TOURNAMENT_RANKING_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_SHOP_BUY_CONDITION_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_SHOP_USER_CASH_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_SHOP_BUY_ITEM_POPUP_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUBMOTION_SHOP_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_BUY_CLUBMOTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_COLLECT_LETTER_EVENT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_NOTICE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
			case C2S_ENTER_RANKING_REQ:						Process_FSEnterRankingPage(pFSClient);					break;
			}
		}
		break;
	case FS_COACH_CARD:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_USE_ITEM_REQ:							Process_UseItemReq(pFSClient);							break;
			case C2S_REGISTER_COACH_CARD_COMBINE_LIST_REQ:	Process_Register_Coach_Card_Combine(pFSClient);			break;
			case C2S_UNREGISTER_COACH_CARD_COMBINE_LIST_REQ:Process_UnregisterCoachCardCombine(pFSClient);			break;
			case C2S_NEED_PRICE_FOR_ITEM_COMBINE_REQ :		Process_NeedPriceForItemCombineReq(pFSClient);			break;
			case C2S_ENTER_COACH_CARD_COMBINATION_TAB_REQ : Process_EnterCoachCardCombinationTabRes(pFSClient);		break;
			case C2S_COMBINE_ITEM_REQ :						Process_CombineItemReq(pFSClient);						break;
			case C2S_THROW_AWAY_ITEM_REQ:					Process_ThrowAwayItemReq(pFSClient);					break;
			case C2S_COACH_CARD_SUB_ACTIONINFLUENCE_REQ:	Process_CoachCardSubActionInfluence(pFSClient);			break;
			case C2S_COACH_CARD_INFO_REQ:					Process_CoachCardInfo(pFSClient);						break;
			case C2S_COACH_CARD_POTENTIAL_ABILITY_REQ:		Process_CoachCardPotentialAbility(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_BALANCE_REQ:		Process_CoachCardPotentialRageBalance(pFSClient);		break;
			case C2S_COACH_CARD_POTENTIAL_ACTION_REQ:		Process_CoachCardPotentialAction(pFSClient);			break;
			case C2S_COACH_CARD_POTENTIAL_FREESTYLE_REQ:	Process_CoachCardPotentialFreeStyle(pFSClient);			break;	

			case C2S_USER_INFO_REQ:						Process_FSLRequestUserInfo(pFSClient);						break;	
			case C2S_REQ_JOIN_ROOM:						Process_FSLJoinRoom(pFSClient);								break;
			case C2S_CREATE_TEAM_REQ:					Process_FSLCreateTeam(pFSClient);							break;
			case C2S_ENTER_WAIT_TEAM_REQ:				Process_FSLEnterTeam(pFSClient);							break;
			case C2S_AUTO_TEAM_REG_REQ:					ProcessFSTAutoTeamRegister(pFSClient);						break;
			case C2S_SET_OPTION_REQ:					Process_FSSetOption(pFSClient);								break;
			
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);

			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_UPGRADE_POTENTIAL_CARD_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_UPGRADE_POTENTIAL_CARD_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
			}
		}
		break;
	case FS_INTENSIVEPRACTICE:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_END_INTENSIVEPRACTICE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHANGE_ROOMMODE_INTENSIVEPRACTICE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ADD_POINT_INTENSIVEPRACTICE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_INTENSIVEPRACTICE_ROOMINFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTOR_STAT_MODULATION_CHECK_REQ);
			}
		}
		break;
	case FS_CHARACTER_COLLECTION:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_REQ_JOIN_ROOM:				Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:			Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:		Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:			ProcessFSTAutoTeamRegister(pFSClient);			break;
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);			break;	
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);					break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_CHAR_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_SENTENCE_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_SENTENCE_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
			default:	break;
			}
		}
		break;
	case FS_BASKETBALL:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_REQ_JOIN_ROOM:				Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:			Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:		Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:			ProcessFSTAutoTeamRegister(pFSClient);			break;
			case C2S_USER_INFO_REQ:				Process_FSLRequestUserInfo(pFSClient);			break;	
			case C2S_SET_OPTION_REQ:			Process_FSSetOption(pFSClient);					break;
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GUARANTEE_BUY_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_GUARANTEE_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_BASKETBALL_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BASKETBALL_MY_LIST_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHANGE_BASKETBALL_SKIN_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ);
			CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHANGE_BASKETBALL_OPTION_REQ);
			default:	break;
			}
		}
		break;
	case FS_PVE_RANK:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{	
			case C2S_SET_OPTION_REQ:				Process_FSSetOption(pFSClient);					break;
			case C2S_CHECK_ALIVE_REQ:				Process_FSCheckAlive(pFSClient);				break;				
			case C2S_USER_INFO_REQ:					Process_FSLRequestUserInfo(pFSClient);			break;
			case C2S_CASH_INFO_REQ:					Process_FSCashInfo(pFSClient);					break;
			case C2S_REQ_JOIN_ROOM:					Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:				Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:			Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:				ProcessFSTAutoTeamRegister(pFSClient);			break;
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_PVE_RANK_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_RANK_LIST_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_RANK_USER_INFO_REQ);
			default:	break;
			}		
		}
		break;
	case FS_SPECIAL_PIECE:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{	
			case C2S_SET_OPTION_REQ:				Process_FSSetOption(pFSClient);					break;
			case C2S_CHECK_ALIVE_REQ:				Process_FSCheckAlive(pFSClient);				break;				
			case C2S_USER_INFO_REQ:					Process_FSLRequestUserInfo(pFSClient);			break;
			case C2S_CASH_INFO_REQ:					Process_FSCashInfo(pFSClient);					break;
			case C2S_REQ_JOIN_ROOM:					Process_FSLJoinRoom(pFSClient);					break;
			case C2S_CREATE_TEAM_REQ:				Process_FSLCreateTeam(pFSClient);				break;
			case C2S_ENTER_WAIT_TEAM_REQ:			Process_FSLEnterTeam(pFSClient);				break;
			case C2S_AUTO_TEAM_REG_REQ:				ProcessFSTAutoTeamRegister(pFSClient);			break;
			case C2S_ITEM_SELECT_REQ:				Process_FSIItemSelect(pFSClient);				break;
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_SPECIAL_PIECE_PAGE_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_BOX_PAGE_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_BOX_KEY_AND_TINYPIECE_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_MAKE_BOX_KEY_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_OPEN_BOX_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_PIECE_PAGE_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(CS2_SP_SKILL_UP_PAGE_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_SA_CREATE_ITEM_PAGE_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_SA_ITEM_PAGE_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_SPLIT_PIECE_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(CS2_SP_SKILL_UP_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(CS2_SP_MAKE_SA_CREATE_ITEM_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(CS2_SP_MAKE_SA_ITEM_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_SKILL_LV_PAGE_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SP_SELECT_SKILL_LV_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ);
				CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ);
			default:	break;
			}		
		}
		break;
	case FS_CLUB_SERVER_PROXY:
		{
			CClubSvrProxy* csProxy = (CClubSvrProxy*)pFSClient;
			csProxy->ProcessPacket(&m_ReceivePacketBuffer);
		}
		break;
		
	case FS_CENTER_SERVER_PROXY:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case LG2S_MULTI_LOGIN_USER_LOGOUT_REQ:		Process_FSMultiLoginUserLogOut(pFSClient);		break;
			}
			
			CCenterSvrProxy* csProxy = (CCenterSvrProxy*)pFSClient;
			csProxy->ProcessPacket(&m_ReceivePacketBuffer);
		}
		break;
	case FS_MATCH_SERVER_PROXY:
		{
			CMatchSvrProxy* csProxy = (CMatchSvrProxy*)pFSClient;
			csProxy->ProcessPacket(&m_ReceivePacketBuffer);
		}
		break;
	case FS_CHAT_SERVER_PROXY:
		{
			CChatSvrProxy* csProxy = (CChatSvrProxy*)pFSClient;
			csProxy->ProcessPacket(&m_ReceivePacketBuffer);
		}
		break;
	case FS_LEAGUE_SERVER_PROXY:
		{
			CLeagueSvrProxy* csProxy = (CLeagueSvrProxy*)pFSClient;
			csProxy->ProcessPacket(&m_ReceivePacketBuffer);
		}
		break;
	case FS_ODBC_SERVER_PROXY:
		{
			CODBCSvrProxy* csProxy = (CODBCSvrProxy*)pFSClient;
			csProxy->ProcessPacket(&m_ReceivePacketBuffer);
		}
		break;
	}

	if (FS_PROXY > pFSClient->GetState())
	{
		switch(m_ReceivePacketBuffer.GetCommand())
		{
		case C2S_CHAT:							Process_FSChat(pFSClient);							break;
		case C2S_SETUP_CHAT_REQ:				Process_FSSetupChatOption(pFSClient);				break;
		case C2S_SHOUT:							Process_UserShout(pFSClient);						break;	
		case C2S_SHOUT_COUNT_REQ:				Process_FSShoutCountReq(pFSClient);					break;		
		case C2S_MOVE_SERVER_REQ:				Process_FSUpdateUserSession(pFSClient);				break;
		case C2S_LOGOUT_REQ:					Process_FSLogOut(pFSClient);						break;
		case C2S_PAPER_INFO_REQ:				Process_PaperInfoReq(pFSClient);					break;
		case C2S_SEND_PAPER:					Process_SendPaper(pFSClient);						break;
		case C2S_DEL_PAPER_REQ:					Process_DelPaperOrPresent(pFSClient);				break;
		case C2S_PRESENT_LIST:					Process_PresentListReq(pFSClient);					break;
		case C2S_PRESENT_INFO_REQ:				Process_PresentInfoReq(pFSClient);					break;
		case C2S_ACCEPT_PRESENT:				Process_AcceptPresent(pFSClient);					break;	
		case C2S_GET_CHOOSE_PROPERTY_ITEM_REQ:	Process_GetChoosePropertyItemReq(pFSClient);		break;
		case C2S_MSG_TO_ALLFRIEND:				Process_MsgToAllFriend(pFSClient);					break;
		case C2S_MSG_TO_FRIEND:					Process_MsgToFriend(pFSClient);						break;
		case C2S_ADD_FRIEND_REQ:				Process_AddFriend(pFSClient);						break;
		case C2S_DEL_FRIEND_REQ:				Process_DelFriend(pFSClient);						break;
		case C2S_ADD_INTERESTUSER_REQ:			Process_AddInterestUser(pFSClient);					break;
		case C2S_DEL_INTERESTUSER_REQ:			Process_DelInterestUser(pFSClient);					break;		
		case C2S_ADD_BANNEDUSER_REQ:			Process_AddBannedUser(pFSClient);					break;
		case C2S_DEL_BANNEDUSER_REQ:			Process_DelBannedUser(pFSClient);					break;		
		case C2S_ENTER_ITEMPAGE_REQ:			Process_FSIEnterItemPage(pFSClient); 				break;
		case C2S_ENTER_SKILLPAGE_REQ:			Process_FSSEnterSkillPage(pFSClient);				break;		
		case C2S_REALTIME_MESSAGE_REQ:			Process_FSRealTimeMessage_Req(pFSClient); 			break;
		case C2S_SHOUT_MESSAGE_REQ:				Process_FSShoutMessage(pFSClient);					break;
		case C2S_LOGOUT_NOT:					Process_FSLogOutTypeCheck(pFSClient);				break;
		case C2S_P2P_LATENCY_CHECK:				Process_FSUserLatencyLog(pFSClient);				break;
		case C2S_EXPOSE_USERITEM_REQ:			Process_FSExposeUserItem(pFSClient);				break;
		case C2S_CHECK_ABLE_REPORT_SWEARWORD_REQ:		Process_ChatReportCheck(pFSClient);			break;
		case C2S_REPORT_SWEARWORD_REQ:					Process_ChatReport(pFSClient);				break;
		case C2S_ACHIEVEMENT_LIST_REQ:					Process_AchievementConfigListReq(pFSClient);			break;
		case C2S_COMPLETED_ACHIEVEMENT_LIST_REQ:		Process_CompletedAchievementListReq(pFSClient);			break;
		case C2S_EQUIP_ACHIEVEMENT_TITLE_REQ:			Process_EquipAchievementTitleReq(pFSClient);			break;
		case C2S_SINGLE_ACHIEVEMENT_INFO_REQ:			Process_SingleAchievementLogInfomationReq(pFSClient);	break;
		case C2S_INVITE_USERLIST:						Process_InviteUserList(pFSClient);						break;
		//case C2S_TOURNAMENT_LIST:						Process_TournamentList(pFSClient);						break;
		//case C2S_TOURNAMENT_WAIT_LIST:					Process_TournamentWaitList(pFSClient);					break;
		//case C2S_TOURNAMENT_NOTIFY_REQ:					Process_TournamentNotifyReq(pFSClient);					break;
		//case C2S_TOURNAMENT_PROGRAM_INFO_REQ:			Process_TournamentProgramInfoReq(pFSClient);			break;
		//case C2S_TOURNAMENT_CREATE_REQ:					Process_TournamentCreateReq(pFSClient);					break;
		//case C2S_TOURNAMENT_ENTER_REQ:					Process_TournamentEnterReq(pFSClient);					break;
		//case C2S_TOURNAMENT_EXIT_REQ:					Process_TournamentExitReq(pFSClient);					break;
		//case C2S_TOURNAMENT_CREATE_PREMADE_TEAM_REQ:	Process_TournamentCreatePreMadeTeamReq(pFSClient);		break;
		//case C2S_TOURNAMENT_INVITE_USER_REQ:			Process_TournamentInviteUserReq(pFSClient);				break;
		//case C2S_TOURNAMENT_INVITE_SUGGEST_RES:			Process_TournamentInviteSuggestRes(pFSClient);			break;
		//case C2S_TOURNAMENT_EXIT_PREMADE_TEAM_REQ:		Process_TournamentExitPreMadeTeamReq(pFSClient);		break;
		//case C2S_TOURNAMENT_BUTTON_INFO_REQ:			Process_TournamentButtonInfoReq(pFSClient);				break;
		//case C2S_TOURNAMENT_MOVE_SEASON_REQ:			Process_TournamentMoveSeasonReq(pFSClient);				break;
		//case C2S_TOURNAMENT_REGISTER_PREMADE_TEAM_REQ:	Process_TournamentRegisterPreMadeTeamReq(pFSClient);	break;
		//case C2S_TOURNAMENT_CHANGE_NAME_REQ:			Process_TournamentChangeNameReq(pFSClient);				break;
		//case C2S_TOURNAMENT_CHANGE_PASSWORD_REQ:		Process_TournamentChangePasswordReq(pFSClient);			break;
		//case C2S_TOURNAMENT_CHANGE_TEAM_REQ:			Process_TournamentChangeTeamReq(pFSClient);				break;
		//case C2S_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_REQ:	Process_TournamentForceOutPreMadeTeamReq(pFSClient);	break;
		//case C2S_TOURNAMENT_START_REQ:					Process_TournamentStartReq(pFSClient);					break;
		//case C2S_TOURNAMENT_MYTEAM_INFO_REQ:			Process_TournamentMyTeamInfoReq(pFSClient);				break;
		//case C2S_TOURNAMENT_UNRESISTER_TEAM:			Process_TournamentUnresisterTeam(pFSClient);			break;
		//case C2S_TOURNAMENT_PUBLIC_OPEN_INFO_REQ:		Process_TournamentPublicOpenInfoReq(pFSClient);			break;
		//case C2S_TOURNAMENT_CREATABLE_REQ:				Process_TournamentCreatableReq(pFSClient);				break;
		case C2S_ENTER_LOBBY_REQ:						ProcessEnterLobbyReq(pFSClient);						break;
		case C2S_STEP_COMPLATE_REWARD_REQ:				Process_StepComplateReward(pFSClient);					break;
		case C2S_DETAIL_GAME_RECORD:					Process_DetailGameRecord(pFSClient);					break;
		case C2S_CHEAT_RATING_POINT_REQ:				Process_RATING_POINT_REQ(pFSClient);					break;
		case C2S_ACCITEM_SELECT_REQ:					Process_AccItemSelect_Req(pFSClient);					break;
		case C2S_USER_GAMEPLAYSEARCH_RES:				Process_UserGamePlayResearchRes( pFSClient );			break; 	
		case C2S_ACTION_SHOP_LIST_REQ:					Process_ActionShopListReq(pFSClient);					break;
		case C2S_USER_ACTION_SLOT_REQ:					Process_UserActionSlotReq(pFSClient);					break;
		case C2S_CHANNEL_BUFF_RANKING_REQ:				Process_ChannelBuffRankingReq(pFSClient);				break;
		case C2S_VISUALLOBBY_SIGN_REQ:					Process_SignReq(pFSClient);								break;

		case C2S_ENTER_PRIVATE_CHAT_ROOM_REQ:			Process_EnterPrivateChatReq(pFSClient);					break;
		case C2S_EXIT_PRIVATE_CHAT_ROOM_REQ:			Process_ExitPrivateChatReq(pFSClient);					break;
		case C2S_PRIVATE_CHAT_USER_PAGE_STATUS_NOT:		Process_PrivateChatUserPageStatusNot(pFSClient);		break;
		case C2S_PRIVATE_CHAT_USER_LIST_REQ:			Process_PrivateChatUserListReq(pFSClient);				break;

		case C2S_ENTER_CLUB_REQ:						Process_FSEnterClubReq(pFSClient);						break;
		case C2S_CLUB_INVITE_REQ:						Process_FSClubInviteReq(pFSClient);						break;
		case C2S_CLUB_INVITE_DATA_REQ:					Process_FSClubInviteDataReq(pFSClient);					break;
		case C2S_CLUB_JOIN_REQ:							Process_FSClubJoinReq(pFSClient);						break;
		case C2S_CLUB_CHEERLEADERSHOP_LIST_REQ:			Process_FSClubCheerLeaderShopListReq(pFSClient);		break;
		case C2S_CLUB_CHEERLEADER_LIST_REQ:				Process_FSClubCheerLeaderListReq(pFSClient);			break;
		case C2S_CLUB_MY_CHEERLEADER_CHANGE_REQ:		Process_FSClubMyCheerLeaderChangeReq(pFSClient);		break;
		case C2S_EVENTLIST_INFO_REQ:					Process_EventListInfoReq(pFSClient);					break;	
		case C2S_UPREPORT:								Process_ReportUserPattern(pFSClient);					break;	
		case C2S_SINGLEGAME_START_REQ:					Process_FSSingleGameStart(pFSClient);					break;
		case C2S_ENTER_RANKING_REQ:						Process_FSEnterRankingPage(pFSClient);					break;
		case C2S_EPISODE_INFO_REQ:						Process_FSEpisodeInfoReq(pFSClient);					break;
		case C2S_FASTRECHARGE_SVRDATA_REQ:				Process_FSFastRechargeSvrDataReq(pFSClient);			break;
		case C2S_FS_AVATAR_SELECT_REQ:					Process_FSChooseAvatarInfo(pFSClient);					break;	
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_AUTO_ENTER_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ITEM_BUYING_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_CLOTHES_SHOP_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_CLUBMARK_SHOP_LIST_REQ);

		case C2S_ENTER_COACH_CARD_PAGE_REQ:				Process_EnterCoachCardPageReq(pFSClient);				break;
		case C2S_COACH_CARD_SHOP_INVENTORY_LIST_REQ:	Process_CoachCardShopInventoryListReq(pFSClient);		break;
		case C2S_COACH_CARD_SLOT_PACKAGE_LIST_REQ:		Process_CoachCardSlotPackageListReq(pFSClient);			break;
		case C2S_EQUIP_COACH_CARD_REQ:					Process_EquipCoachCardReq(pFSClient);					break;
		case C2S_UNEQUIP_COACH_CARD_REQ:				Process_UnEquipCoachCardReq(pFSClient);					break;
		case C2S_COACH_CARD_TERM_EXTEND_PRICELIST_REQ:	Process_CoachCardTermExtendPriceListReq(pFSClient);		break;
		case C2S_COACH_CARD_TERM_EXTEND_REQ:			Process_CoachCardTermExtend(pFSClient);					break;
		case C2S_VOICE_CHATTING_USER_OPTION_INFO_REQ:	Process_FSVoiceChattingUserOptionInfoReq(pFSClient);	break;
	
		case C2S_CHANGE_USE_STYLE_REQ:				Process_ChangeUseStyleReq(pFSClient);			break;
		case C2S_CHANGE_STYLE_NAME_REQ:				Process_ChangeStyleNameReq(pFSClient);			break;
		case C2S_CHANGE_USE_STYLE_IN_BAG_REQ:		Process_ChangeUseStyleInBagReq(pFSClient);		break;
		case C2S_SPECIAL_PARTS_INFO_REQ:			Process_SpecialPartsInfoReq(pFSClient);			break;
		case C2S_SPECIAL_PARTS_STAT_APPLY_REQ:		Process_SpecialPartsStatApplyReq(pFSClient);	break;
		case C2S_ITEM_PREMIUM_INFO_REQ:				Process_FSIPremiumStat(pFSClient);				break;
		case C2S_SHOPITEM_SELECT_REQ:				Process_FSIShopItemSelect(pFSClient);			break;
		case C2S_ACTION_DETAIL_INFO_REQ:			Process_ActionDetailInfoReq(pFSClient);			break;
		case C2S_ACTION_BUY_REQ:					Process_ActionBuyReq(pFSClient);				break;
		case C2S_DISCOUNTITEMINFO_REQ:				Process_FSDiscountItemInfoReq(pFSClient);		break;
		case C2S_CLUB_LOBBY_MEMBER_LIST_REQ:	Process_FSClubLobbyMemberListReq(pFSClient);	break;
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_TEST_CLUBTOURNAMENT_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EXIT_CHAT_ROOM_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_USER_ACTION_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_WORDPUZZLES_EVENT_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_WORDPUZZLES_EVENT_GET_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMBOX_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMBOX_REWARD_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USE_RANDOMBOX_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANDOMBOX_MISSION_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SPECIALSKIN_MY_EQUIP_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_POTENCARD_TERM_EXTEND_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PAYBACK_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PAYBACK_WANTPAYBACK_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MATCHINGCARD_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MATCHINGCARD_READY_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MATCHINGCARD_REWARDINFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MATCHINGCARD_FLIPCARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MATCHINGCARD_TIMECHECK_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_ATTENDANCE_ITEMSHOP_UPDATEITEM_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_XIGNCODE_SECURITY_DATA_NOT);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MISSION_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MISSION_GET_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_MISSION_CHECK_GET_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EPORTSCARD_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EPORTSCARD_BUY_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DELETE_WAIT_FRIEND_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FIRST_CASHBACK_EVENT_ENABLE_USER_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_AVOID_TEAM_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_AVOID_TEAM_SAVE_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_RANKMATCH_MATCHING_COMPLETED_RES);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_RANKMATCH_LYAER_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PCROOM_BENEFIT_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CASHITEM_REWARD_EVENT_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CASHITEM_REWARD_EVENT_OPEN_BOX_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_CREATE_ROOM_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_TEST_ADD_FOOD0_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_REWARD_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_EAT_FOOD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_RANK_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_RANK_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DARKMARKET_PRODUCT_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DARKMARKET_USER_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_DARKMARKET_BUY_PRODUCT_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLOSE_BUTTON_CLICK_NOTICE);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_ADD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_ADD_RECOMMEND_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_DELETE_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_MEMO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SERVER_TIME_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_MY_AVATAR_LIST_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOSEGAME_NOT_OPEN_TODAY_NOT);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOSEGAME_MOVE_PVEMODE_PLAY_NOT);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_PVE_RANK_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_MISSION_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_MISSION_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_MISSION_GET_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_DAILY_MISSION_GET_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_SHOPPING_GOD_CHECK_LOGIN_TIME_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_ENTER_SPECIAL_PIECE_PAGE_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_HOTGIRLGIFT_CLEAR_STEP_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CAMERA_ANGLE_SAVE_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_LOBBY_BUTTON_TYPE_SAVE_NOT);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_MOTION_SLOT_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BASKETBALL_SHOP_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BASKETBALL_PRODUCT_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_BUY_BASKETBALL_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_CLUB_NAME_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_USER_RATING_TOP_CHARACTER_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GRADE_REWARD_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_MISSION_INFO_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SECRET_SHOP_LIST_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(C2S_EVENT_SECRET_SHOP_BUY_ITEM_REQ);
		}
	}
	else
	{
		switch(m_ReceivePacketBuffer.GetCommand())
		{
		CASE_GAMETHREAD_PACKET_PROC_FUNC(S2S_CHECK_FIRST_CONNECT_REQ);
		CASE_GAMETHREAD_PACKET_PROC_FUNC(S2S_CHECK_FIRST_CONNECT_RES);
		}
	}

	int iEndTick = GetTickCount();

	if(m_ReceivePacketBuffer.GetCommand() < CRASH_CHECK_NUM)
	{
		int iCrashCheckIndex = m_ReceivePacketBuffer.GetCommand();
		CFSGameServer::GetInstance()->m_CrashCheckPacketNum[iCrashCheckIndex]++;
		CFSGameServer::GetInstance()->m_CrashCheckTickAvg[iCrashCheckIndex] += iEndTick - iStartTick;
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSCheckClientHack( CFSGameClient* pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_DATA_HACK_DETECTED info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iHackType);

	pMatch->SendPacket(G2M_DATA_HACK_DETECTED, &info, sizeof(SG2M_DATA_HACK_DETECTED));
}

void CFSGameThread::Process_FSCheckAvatarStatus(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	switch( m_ReceivePacketBuffer.GetCommand() )
	{
	case C2S_CHECK_AVATAR_STATUS_RES:	pUser->ProcessCheckAvatarStatus(&m_ReceivePacketBuffer); break;
	case C2S_CHECK_AVATAR_STATUS1_RES:	pUser->ProcessCheckAvatarStatus1(&m_ReceivePacketBuffer); break;
	case C2S_CHECK_AVATAR_STATUS2_RES:	pUser->ProcessCheckAvatarStatus2(&m_ReceivePacketBuffer); break;
	default: break;
	}
}

void CFSGameThread::Process_FSCheckSumStatus( CFSGameClient* pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->ProcessCheckSumStatus(&m_ReceivePacketBuffer);
}

// Modify for Match Server
void CFSGameThread::Process_FSPunishment( CFSGameClient* pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_INTENTIONAL_FOUL_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iOPcode);
	m_ReceivePacketBuffer.ReadNTString(info.szFoulUserID, MAX_GAMEID_LENGTH + 1);

	if(1 != pUser->CheckGMUser())
	{
		return;
	}

	//TD - GM Process_FSPunishment
	CMatchSvrProxy* pMatch = NULL;//(CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	if(pMatch) 
	{
		pMatch->SendIntentionalFoulReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSGiveupVote(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_GIVEUP_VOTE_REQ, &info, sizeof(info));
}

void CFSGameThread::Process_ChatReportCheck(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC(ODBC_LOG);
	CHECK_NULL_POINTER_VOID( pLogODBC);

	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);
	m_ReceivePacketBuffer.Read((BYTE*)szGameID, MAX_GAMEID_LENGTH + 1);

	int iReturnValue = 0;

	NEXUS_CLIENT_STATE eCurUserState = pFSClient->GetState();

	iReturnValue = pLogODBC->CHECK_ChatReport(pUser->GetGameIDIndex(), szGameID, eCurUserState);	
	if (0 > iReturnValue) 
	{
		iReturnValue = 2;
	}
	
	CPacketComposer PacketComposer(S2C_CHECK_ABLE_REPORT_SWEARWORD_RES);
	PacketComposer.Add(iReturnValue);
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_ChatReport(CFSGameClient* pFSClient)
{
	enum { MAX_BYTE = 3000};

	const int iEncodeMaxLength = MAX_BYTE;

	int iKeyMax = 0, iEncodeLength = 0;

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
	CHECK_NULL_POINTER_VOID( pLogODBC );

	DECLARE_INIT_TCHAR_ARRAY(szTargetGameID, MAX_GAMEID_LENGTH + 1);
	DECLARE_INIT_TCHAR_ARRAY(szEncodedMsg, iEncodeMaxLength + 1);
	DECLARE_INIT_TCHAR_ARRAY(szKey, 32);

	const char* pUserGameID = pUser->GetGameID();
	memcpy(szKey,pUserGameID, MAX_GAMEID_LENGTH + 1);
	szKey[MAX_GAMEID_LENGTH]    = '\0';
	iKeyMax = strlen(szKey);

	m_ReceivePacketBuffer.Read((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);
	m_ReceivePacketBuffer.Read((BYTE*)&iEncodeLength, sizeof(iEncodeLength));

	enum{ RESULT_NULLTargetID = 6, RESULT_SameIDAsTargetID= 7};
	int iReturnValue = 2; //System Error


	if ( 0 == strcmp(pUserGameID,szTargetGameID) )
	{
		iReturnValue = RESULT_SameIDAsTargetID;
	} 
	else if( 0 == strcmp("",szTargetGameID) )
	{
		iReturnValue = RESULT_NULLTargetID;
	}
	else 
	{
		if (iEncodeLength < 0 )
		{
			WRITE_LOG_NEW(LOG_TYPE_CHAT, INVALED_DATA, CHECK_FAIL, "Process_ChatReport - DataLength:11866902", iEncodeLength);
urn;
		}

		if ( iEncodeLength > iEncodeMaxLength ) 
		{
			iEncodeLength  = iEncodeMaxLength;
		}

		m_ReceivePacketBuffer.Read((BYTE*)szEncodedMsg, iEncodeLength + 1);

		for(int i = 0, iKeyIndex = 0; i < iEncodeLength; i++, iKeyIndex++)
		{
			if(iKeyIndex == iKeyMax)
				iKeyIndex = 0;

			szEncodedMsg[i] ^= szKey[iKeyIndex];
		}

		NEXUS_CLIENT_STATE eCurUserState = pFSClient->GetState();

		iReturnValue = pLogODBC->REGISTER_ChatReport( pUser->GetGameIDIndex(), szTargetGameID, szEncodedMsg, eCurUserState);

		if (0 > iReturnValue) 
		{
			iReturnValue = 2;
		}

	}

	CPacketComposer PacketComposer(S2C_REPORT_SWEARWORD_RES);
	PacketComposer.Add(iReturnValue);
	pUser->Send(&PacketComposer);
	pUser->SetReportedGameID(szTargetGameID);
}

void CFSGameThread::Process_FSTutorialComplete( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();

	if( NULL == pUser ) return;

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );

	int iBonusPoint = 0;
	int iBonusType = 0;

	if (ODBC_RETURN_SUCCESS != pGameODBC->spSetUserTutorialComplete( pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), iBonusPoint, iBonusType))
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAME, DB_DATA_UPDATE, FAIL, "spSetUserTutorialComplete - UserID:��4", pUser->GetUserID());
}

	if( iBonusType == 1 )/*포인트를 보상으로 줄 때*/
	{
		pUser->SetSkillPoint( pUser->GetSkillPoint() + iBonusPoint );
		pUser->AddPointAchievements(); 
	}
	else if( iBonusType == 2 )/*vip를 보상으로 줄 때*/
	{
		pUser->CheckAndSendBindAccountInfo();
		CPacketComposer PacketComposer(S2C_TUTORIAL_COMPLET_RES);
		PacketComposer.Add(1);
		pFSClient->Send(&PacketComposer);
	}
	else if( iBonusType == 3 )/*케릭터 슬롯을 보상으로 줄 때*/
	{
		CPacketComposer PacketComposer(S2C_TUTORIAL_COMPLET_RES);
		PacketComposer.Add(1);
		pFSClient->Send(&PacketComposer);
	}
}

void CFSGameThread::Process_FSMiniGameInfo( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	SAvatarInfo *pAvatarInfo = pUser->GetCurUsedAvatar(); 
	CHECK_NULL_POINTER_VOID( pAvatarInfo );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	if( ODBC_RETURN_SUCCESS != pGameODBC->spGetUserMiniGameInfo( pAvatarInfo ) )
		return;

	BYTE byGameCnt;
	if( FALSE == CFSGameServer::GetInstance()->SetUserSingleContent( pAvatarInfo, byGameCnt) )
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAME, SET_DATA, FAIL, "SetUserSingleContent - GameID:��4", pAvatarInfo->szGameID);
	return;
	}

	CPacketComposer PacketComposer(S2C_MINIGAME_INFO_RES);
	PacketComposer.Add((PBYTE)&byGameCnt, sizeof(BYTE));

	for( int i = 0; i < byGameCnt ; ++i)
	{
		PacketComposer.Add((PBYTE)&i, sizeof(BYTE));
		PacketComposer.Add((PBYTE)&pAvatarInfo->UserSingleContents[i].byTrainingKind, sizeof(BYTE));
		PacketComposer.Add((PBYTE)&pAvatarInfo->UserSingleContents[i].byTotalStageNum, sizeof(BYTE));
		// 20070910 Add User MiniGame
		PacketComposer.Add((PBYTE)&pAvatarInfo->UserSingleContents[i].iStageCount, sizeof(int));
		// End
		// 20080908 신규 유저 캐릭터슬롯 선물
		PacketComposer.Add((PBYTE)&pAvatarInfo->UserSingleContents[i].byArrow, sizeof(BYTE));
		// End
	}

	pFSClient->Send(&PacketComposer);

}

void CFSGameThread::Process_FSMiniGameStart( CFSGameClient* pFSClient )
{

	BYTE byTrainingKind = 250;
	BYTE byStartStage = 250;

	m_ReceivePacketBuffer.Read((PBYTE)&byTrainingKind,sizeof(BYTE));
	m_ReceivePacketBuffer.Read((PBYTE)&byStartStage,sizeof(BYTE));

	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	SAvatarInfo *pAvatarInfo = pUser->GetCurUsedAvatar(); 
	CHECK_NULL_POINTER_VOID( pAvatarInfo );

	// 20070911 Add MiniGame Info
	int iExp = 0;
	int iPoint = 0;
	int iUserStage = 0;
	for( int i = 0; i < MAX_MINIGAME_NUM ; i++ )
	{
		if( pAvatarInfo->UserSingleContents[i].byTrainingKind == byTrainingKind )
		{
			iUserStage = pAvatarInfo->UserSingleContents[i].iStageCount;
			break;
		}
	}

	BYTE byReStartStage = byStartStage;
	if( byReStartStage >= 0 )
	{
		byReStartStage--;
	}

	BOOL bResult = CFSGameServer::GetInstance()->GetSingleContentBonus( pAvatarInfo, iUserStage, byTrainingKind, byReStartStage, iExp, iPoint);

	CPacketComposer PacketComposer(S2C_MINIGAME_START_RES);
	PacketComposer.Add((PBYTE)&byTrainingKind, sizeof(BYTE));
	PacketComposer.Add((PBYTE)&byStartStage, sizeof(BYTE));
	PacketComposer.Add((PBYTE)&iExp, sizeof(int));
	PacketComposer.Add((PBYTE)&iPoint, sizeof(int));
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSMiniGameComplete( CFSGameClient* pFSClient )
{
	BYTE byTrainingKind;
	int	 iCompleteStage;

	int iPlusPoint = 0;
	int iPlusExp = 0;
	int iSuccess = 0;

	m_ReceivePacketBuffer.Read((PBYTE)&byTrainingKind,sizeof(BYTE));
	m_ReceivePacketBuffer.Read((PBYTE)&iCompleteStage,sizeof(int));

	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	SAvatarInfo *pAvatarInfo = pUser->GetCurUsedAvatar(); 
	CHECK_NULL_POINTER_VOID( pAvatarInfo );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );

	BYTE byTrainingNum;
	if( FALSE == pUser->GetUserTrainNum( pAvatarInfo, byTrainingKind, byTrainingNum ) )	return;

	int iTempCompleteStage = pUser->CalculationMiniGameStage( pAvatarInfo->UserSingleContents[byTrainingNum].iStageCount, iCompleteStage - 1 );

	if( ODBC_RETURN_SUCCESS != pGameODBC->spSetUserMiniGameInfo( pUser->GetUserIDIndex(), pAvatarInfo, 
		byTrainingNum, ( iCompleteStage-1 ), iTempCompleteStage, iPlusPoint, iPlusExp, iSuccess) )
	{
		return;
	}

	int iExp1 = 0;
	if ( pGameODBC && pAvatarInfo )
	{
		int bIncrGameTimesInput = FALSE, bIncrGameTimesOutput = FALSE;
		if( ODBC_RETURN_SUCCESS == pGameODBC->spCheckUserGameDailyLimitInfo( pAvatarInfo->iGameIDIndex, iPlusExp, iExp1, bIncrGameTimesInput, bIncrGameTimesOutput ) )
		{
			if ( iExp1 != iPlusExp )
			{
				WRITE_LOG_NEW(LOG_TYPE_MINIGAME, GET_DATA, NONE, "Login UserGame Check Limit Exp - GameIDIndex:11866902, iPlusExp:0, iExp1:18227200", 
rInfo->iGameIDIndex, iPlusExp, iExp1);

				if( iPlusExp > iExp1 ) iPlusExp = iExp1;
			}
		}
	}

	//찮섬꼇朗쒔駱
	if ( GAME_LVUP_MAX == pAvatarInfo->iLv )
	{
		iPlusExp = 0;
	}
	//20070629
	int iPreLv = pAvatarInfo->iLv;
	int iCurrentExp = pAvatarInfo->iExp;
	int iMinExp = CFSGameServer::GetInstance()->GetLvUpExp(pAvatarInfo->iLv);    
	int iMaxExp = CFSGameServer::GetInstance()->GetLvUpExp(pAvatarInfo->iLv+1); 
	int iNextLv = pAvatarInfo->iLv;

	bool bIsLvUp = false;
	if( (pAvatarInfo->iLv + 1 < GAME_LVUP_MAX + 1) && ((iCurrentExp+iPlusExp) >= iMaxExp) )	bIsLvUp = true;

	if( iSuccess == 0 )	// 성공
	{
		pAvatarInfo->UserSingleContents[byTrainingNum].iStageCount = iTempCompleteStage;

		CPacketComposer PacketComposer(S2C_MINIGAME_COMPLET_RES);
		PacketComposer.Add((PBYTE)&byTrainingKind, sizeof(BYTE));
		PacketComposer.Add((BYTE)iSuccess); //클라에서 쓰지 않는데..

		//20070629 
		PacketComposer.Add(iPlusExp);
		PacketComposer.Add(iCurrentExp);
		PacketComposer.Add(iMinExp);
		PacketComposer.Add(iMaxExp);

		PacketComposer.Add(iPlusPoint);

		PacketComposer.Add((BYTE)bIsLvUp); 

		int iaStat[MAX_STAT_NUM];
		memset(iaStat , 0 ,  sizeof(int) * MAX_STAT_NUM );

		if( bIsLvUp )
		{
			if ( FALSE == pUser->ProcessStatUp(iaStat) ) return;

			iNextLv = pAvatarInfo->iLv + 1;
			int iNextMaxExp = CFSGameServer::GetInstance()->GetLvUpExp(pAvatarInfo->iLv+2);
			int iStatUpNum=0;
			for(int i=0;i<MAX_STAT_NUM;i++)
			{
				if( 0 != iaStat[i] )
					iStatUpNum++;
			}

			PacketComposer.Add(iNextMaxExp); 
			PacketComposer.Add((short)iNextLv); 
			PacketComposer.Add((short)iStatUpNum); 

			for(int i=0;i<MAX_STAT_NUM;i++)
			{
				if( 0 != iaStat[i] )
				{
					PacketComposer.Add((short)i); 
					PacketComposer.Add((short)iaStat[i]); 
				}
			}
			
			pUser->ProcessLevelUp(pUser->GetGameIDIndex(), pUser->GetCurUsedAvatarPosition(), iPreLv, iNextLv, pUser->GetCurUsedAvtarExp());
		}
		//DB,메모리 업데이트
		if( FALSE == pUser->UpdateUserLvExp( bIsLvUp, iNextLv, iPlusExp, iaStat ) )
			return;

		pFSClient->Send(&PacketComposer);
		if( iPlusPoint > 0 )
		{
			pUser->AddSkillPoint(iPlusPoint);
			// 20080723 이벤트 시스템 
			int iSkillPoint = 0;
			if( NULL == pGameODBC || ODBC_RETURN_SUCCESS == (pGameODBC)->spUserGetUserPointInfo( pUser->GetUserIDIndex(), iSkillPoint ))
			{
				pUser->SetSkillPoint( iSkillPoint );
				pUser->AddPointAchievements(); 
			}

			// End
		}		
	}
}

// 20070823 Add Story Mode
void CFSGameThread::Process_FSEpisodeInfoReq( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );


	// 20090619 Add Episode Group
	short iEpisodeGroupIndex = -1;

	m_ReceivePacketBuffer.Read(&iEpisodeGroupIndex);
	// End
	pUser->SendEpisodeInfo(iEpisodeGroupIndex);
}
void CFSGameThread::Process_FSFastRechargeSvrDataReq( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );
	DWORD  CurrentTime = (unsigned long)time(NULL);
	
	CPacketComposer PacketComposer(S2C_FASTRECHARGE_SVRDATA_RES);
	PacketComposer.Add(&CurrentTime);
	pFSClient->Send(&PacketComposer);
}
void CFSGameThread::Process_FSEpisodeResultReq( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	SAvatarInfo *pAvatarInfo = pUser->GetCurUsedAvatar(); 
	CHECK_NULL_POINTER_VOID( pAvatarInfo );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );

	int iEpisodeGroupIndex	= 0;
	int iEpisodeNum		= -1;
	int	iStageNum		= -1;
	bool bStageComplete	= false;

	m_ReceivePacketBuffer.Read(&iEpisodeGroupIndex);
	m_ReceivePacketBuffer.Read(&iEpisodeNum);
	m_ReceivePacketBuffer.Read(&iStageNum);
	m_ReceivePacketBuffer.Read((PBYTE)&bStageComplete,sizeof(bool));


	bool bResult = false;

	CPacketComposer PacketComposer(S2C_EPISODE_RESULT_RES);

	if( bStageComplete == TRUE ) // 성공
	{
		int iStage = iStageNum + 1;
		// 20070911 Add AI Game 
		int iPoint = 0;

		if(ODBC_RETURN_SUCCESS != (pGameODBC)->EP_SetUserEpisode(pUser->GetUserIDIndex(), iEpisodeGroupIndex, 
			iEpisodeNum, iStage, iPoint) )
		{
			PacketComposer.Add(&iEpisodeGroupIndex);
			PacketComposer.Add(&iEpisodeNum);
			PacketComposer.Add(&iStageNum);
			PacketComposer.Add((PBYTE)&bResult, sizeof(bool));

			pFSClient->Send(&PacketComposer);
			return;
		}

		bResult = true ;

		pUser->SetEpisodeStage(iEpisodeGroupIndex,iEpisodeNum, iStage);

		PacketComposer.Add(&iEpisodeGroupIndex);
		PacketComposer.Add(&iEpisodeNum);
		PacketComposer.Add(&iStageNum);
		PacketComposer.Add((PBYTE)&bResult, sizeof(bool));
		PacketComposer.Add(&iPoint);

		if( iPoint > 0 )
		{
			if( ODBC_RETURN_SUCCESS != pGameODBC->spFSUpdateUserSkillPoint( pUser->GetUserIDIndex(), iPoint ) )
			{
				WRITE_LOG_NEW(LOG_TYPE_MINIGAME, DB_DATA_UPDATE, FAIL, "spFSUpdateUserSkillPoint - GameID:��4", pAvatarInfo->szGameID);
			return;
			}

			pUser->AddSkillPoint(iPoint);
		}
		// End		
		pFSClient->Send(&PacketComposer);

		return;
	}
	else
	{
		PacketComposer.Add(&iEpisodeGroupIndex);
		PacketComposer.Add(&iEpisodeNum);
		PacketComposer.Add(&iStageNum);
		PacketComposer.Add((PBYTE)&bResult, sizeof(bool));

		pFSClient->Send(&PacketComposer);
	}
}

//20070712
void CFSGameThread::Process_FSAIGameInfo( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	SAvatarInfo *pAvatarInfo = pUser->GetCurUsedAvatar(); 
	CHECK_NULL_POINTER_VOID( pAvatarInfo );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	if( ODBC_RETURN_SUCCESS != pGameODBC->spGetAvatarSkillInfo( pAvatarInfo ) )
		return; 

	CPacketComposer PacketComposer(S2C_AI_GAME_INFO_RES);
	for (int i = 0; i < MAX_SKILL_STORAGE_COUNT; ++i)		//스킬번호 확장 클라와 함께 작업시 SKILL_STORAGE_3 -> MAX_SKILL_STORAGE_COUNT 변경
		PacketComposer.Add((int)(pAvatarInfo->Skill.iaSkill[i] ^ pAvatarInfo->Skill.iaSkillStock[i]));
	for (int i = 0; i < MAX_SKILL_STORAGE_COUNT; ++i)
		PacketComposer.Add((int)(pAvatarInfo->Skill.iaFreestyle[i] ^ pAvatarInfo->Skill.iaFreestyleStock[i]));
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSAIGameResult( CFSGameClient* pFSClient )
{
	//20070701 AI 기록 남기기
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );
	BYTE byMode, byDifficulty, byResult;

	m_ReceivePacketBuffer.Read((PBYTE)&byMode,sizeof(BYTE));
	m_ReceivePacketBuffer.Read((PBYTE)&byDifficulty,sizeof(BYTE));
	m_ReceivePacketBuffer.Read((PBYTE)&byResult,sizeof(BYTE));

	if( FALSE == pUser->SetUserAIGameLog(byMode, byDifficulty, byResult) )
		WRITE_LOG_NEW(LOG_TYPE_AI, DB_DATA_UPDATE, FAIL, "SetUserAIGameLog - UserID:��4", pUser->GetUserID());


void CFSGameThread::Process_FSSingleGameEnd( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	if (TRUE == pFSClient->SetState(NEXUS_LOBBY))
	{
		//써監쏵쌓祁족샀양헙친駕
		pUser->SetOptionEnable(OPTION_TYPE_WHISPER, TRUE );
	}

	CPacketComposer PacketComposer( S2C_SINGLEGAME_END_RES);
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSSingleGameStart( CFSGameClient* pFSClient )
{
	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	char chGameType	= 0; // enum { TUTORIAL = 0, BASIC_TRAINING = 2, MATCH_TRAINING = 3, EPISODE = 4};
	m_ReceivePacketBuffer.Read(&chGameType);

	CHECK_CONDITION_RETURN_VOID(0 < pUser->GetMatchLocation());

	if (FALSE == pFSClient->SetState(FS_SINGLE))
	{
		return;
	}

	pUser->SetOptionEnable(OPTION_TYPE_WHISPER, FALSE );
	//쏵흙쏵쌓祁족、양헙친駕
	pUser->SetLocation( U_TRAINING );

	CPacketComposer PacketComposer( S2C_SINGLEGAME_START_RES );
	PacketComposer.Add(chGameType);
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_AchievementConfigListReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	vector<SAchievementConfig> vecConfig;
	if (FALSE == ACHIEVEMENTMANAGER.GetAchievementConfigList(vecConfig, pUser->GetCurUsedAvatarPosition()))
	{
		WRITE_LOG_NEW(LOG_TYPE_ACHIEVEMENT, GET_DATA, FAIL, "GetAchievementConfigList - GameID:��4", pUser->GetGameID());
	return;
	}

	enum{ PACKET_START, PACKET_DATA, PACKET_END };
	const int c_Max_List_count = 100;
	int iLoopCount = vecConfig.size() / c_Max_List_count;
	int iLastCount = vecConfig.size() _Max_List_count;


	CPacketComposer PacketComposerStart(S2C_ACHIEVEMENT_LIST_RES);
	PacketComposerStart.Add((int)PACKET_START);
	pFSClient->Send( &PacketComposerStart );

	for ( int i = 0; i < iLoopCount + 1; ++i )
	{
		int iLoop_j = i < iLoopCount ? c_Max_List_count : iLastCount;
		CPacketComposer PacketComposerData(S2C_ACHIEVEMENT_LIST_RES);
		PacketComposerData.Add((int)PACKET_DATA);
		PacketComposerData.Add((int)iLoop_j);

		for ( int j = 0; j < iLoop_j; ++j )
		{
			PacketComposerData.Add( vecConfig[j+c_Max_List_count*i].iAchievementIndex );
			PacketComposerData.Add( vecConfig[j+c_Max_List_count*i].iAchievementGroup );
			PacketComposerData.Add( vecConfig[j+c_Max_List_count*i].iScore );
			PacketComposerData.Add( vecConfig[j+c_Max_List_count*i].bDoHaveTitle );
			PacketComposerData.Add( vecConfig[j+c_Max_List_count*i].iOrder );
		}

		pFSClient->Send( &PacketComposerData );
	}

	CPacketComposer PacketComposerEnd(S2C_ACHIEVEMENT_LIST_RES);
	PacketComposerEnd.Add((int)PACKET_END);
	pFSClient->Send( &PacketComposerEnd );
}

void CFSGameThread::Process_CompletedAchievementListReq(CFSGameClient* pFSClient)
{
	const int ACHIEVEMENT_COUNT_ZERO = 0;

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser * pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	BOOL bFound = TRUE;
	char szTargetGameID[MAX_GAMEID_LENGTH + 1] = {0,};

	m_ReceivePacketBuffer.Read((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);

	CPacketComposer PacketComposer(S2C_COMPLETED_ACHIEVEMENT_LIST_RES);

	if (strncmp(pUser->GetGameID(), szTargetGameID, MAX_GAMEID_LENGTH) != 0)
	{
		CScopedRefGameUser pTargetUser(szTargetGameID);
		if (pTargetUser == NULL)
		{
			vector<SAchievementLog> vAchievementLog;
			if (ODBC_RETURN_SUCCESS == pGameODBC->ACHIEVEMENT_GetAvatarCompletedList(szTargetGameID, vAchievementLog))
			{
				PacketComposer.Add((int)bFound);
				PacketComposer.Add((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);
				PacketComposer.Add((int)vAchievementLog.size());
				vector<SAchievementLog>::iterator iter = vAchievementLog.begin();
				vector<SAchievementLog>::iterator endIter = vAchievementLog.end();
				for (; iter != endIter; ++iter)
				{
					PacketComposer.Add((*iter).iAchievementIndex);
					PacketComposer.Add((*iter).wCompletedYear);
					PacketComposer.Add((*iter).wCompletedMonth);
					PacketComposer.Add((*iter).wCompletedDay);
					PacketComposer.Add((*iter).wCompletedHour);
					PacketComposer.Add((*iter).wCompletedMinute);
				}
			}
			else
			{
				bFound = FALSE;
				PacketComposer.Add((int)bFound);
				PacketComposer.Add((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);
				PacketComposer.Add(ACHIEVEMENT_COUNT_ZERO);
			}
		}
		else
		{
			PacketComposer.Add((int)bFound);
			PacketComposer.Add((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);
			if (FALSE == pTargetUser->MakeAvatarAchievementDatatoPacket(PacketComposer))
			{
				WRITE_LOG_NEW(LOG_TYPE_ACHIEVEMENT, GET_DATA, FAIL, "MakeAvatarAchievementDatatoPacket - CurGameID:��4, TarGameID:(null)", pUser->GetGameID(), szTargetGameID);
acketComposer.Add(ACHIEVEMENT_COUNT_ZERO);
			}
		}
	}
	else
	{
		PacketComposer.Add((int)bFound);
		PacketComposer.Add((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);
		if (FALSE == pUser->MakeAvatarAchievementDatatoPacket(PacketComposer))
		{
			WRITE_LOG_NEW(LOG_TYPE_ACHIEVEMENT, GET_DATA, FAIL, "MakeAvatarAchievementDatatoPacket - CurGameID:��4, TarGameID:(null)", pUser->GetGameID(), szTargetGameID);
cketComposer.Add(ACHIEVEMENT_COUNT_ZERO);
		}
	}

	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_EquipAchievementTitleReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if (FACTION_RACE_ON_GOING == FACTION.GetRaceStatus() && 0 < pUser->GetUserFaction()->_iFactionIndex)
	{
		const int ERROR_CODE_FACTION_ON_GOING = -3;
		CPacketComposer PacketComposer(S2C_EQUIP_ACHIEVEMENT_TITLE_RES);
		PacketComposer.Add((int)FALSE);
		PacketComposer.Add(ERROR_CODE_FACTION_ON_GOING);
		PacketComposer.Add((int)0);
		pUser->Send(&PacketComposer);
		return;
	}

	int iEquipAchievementTitle = -1;
	m_ReceivePacketBuffer.Read(&iEquipAchievementTitle);

	pUser->EquipAchievementTitle(iEquipAchievementTitle);	
}

void CFSGameThread::Process_SingleAchievementLogInfomationReq(CFSGameClient* pFSClient)
{
	const int ACHIEVEMENT_COUNT_ZERO = 0;

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	int iSelectedAchievementIndex = -1;
	BOOL bFound = TRUE;
	char szTargetGameID[MAX_GAMEID_LENGTH + 1] = {0,};
	int iGamePosition = POSITION_CODE_CENTER;

	m_ReceivePacketBuffer.Read((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);
	m_ReceivePacketBuffer.Read(&iSelectedAchievementIndex);

	CPacketComposer PacketComposer(S2C_SINGLE_ACHIEVEMENT_INFO_RES);
	PacketComposer.Add((int)bFound);
	PacketComposer.Add((BYTE*)szTargetGameID, MAX_GAMEID_LENGTH + 1);
	PacketComposer.Add(iSelectedAchievementIndex);

	if (strncmp(pUser->GetGameID(), szTargetGameID, MAX_GAMEID_LENGTH) != 0)
	{
		CScopedRefGameUser pTargetUser(szTargetGameID);
		if (pTargetUser == NULL)
		{
			PacketComposer.Add(ACHIEVEMENT_COUNT_ZERO);
		}
		else
		{
			PacketComposer.Add(pTargetUser->GetProgressAmountofAchievement(iSelectedAchievementIndex));
			iGamePosition = pTargetUser->GetCurUsedAvatarPosition();
		}
	}
	else
	{
		PacketComposer.Add(pUser->GetProgressAmountofAchievement(iSelectedAchievementIndex));
		iGamePosition = pUser->GetCurUsedAvatarPosition();
	}

	if (iSelectedAchievementIndex == ACHIEVEMENT_CODE_SHOP_SKILL_ALL)
	{
		CFSSkillShop* pSkillShop = pServer->GetSkillShop();
		CHECK_NULL_POINTER_VOID(pSkillShop);

		PacketComposer.Add(pSkillShop->GetPositionTotalSkillCount(iGamePosition));
	}
	else if (iSelectedAchievementIndex == ACHIEVEMENT_CODE_SHOP_FREESTYLE_ALL)
	{
		CFSSkillShop* pSkillShop = pServer->GetSkillShop();
		CHECK_NULL_POINTER_VOID(pSkillShop);

		PacketComposer.Add(pSkillShop->GetPositionTotalFreeStyleCount(iGamePosition));
	}
	else
	{
		ACHIEVEMENTMANAGER.MakeSingleAchievementDatatoPacket(PacketComposer, iSelectedAchievementIndex);
	}

	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_InviteUserList(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_INVITE_USERLIST info;

	m_ReceivePacketBuffer.Read(&info.iCode);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_INVITE_USERLIST);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_INVITE_USERLIST));

	//TD - 초대 유저리스트(토너먼트용)
	CMatchSvrProxy* pMatch = NULL; //(CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentList(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_LIST info;

	m_ReceivePacketBuffer.Read(&info.iTournamentMode);
	m_ReceivePacketBuffer.Read(&info.iGameMode);
	m_ReceivePacketBuffer.Read(&info.iPageNum);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_LIST);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_LIST));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentWaitList(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_WAIT_LIST info;

	m_ReceivePacketBuffer.Read(&info.iTournamentMode);
	m_ReceivePacketBuffer.Read(&info.iGameMode);
	m_ReceivePacketBuffer.Read(&info.iStartIdx);
	m_ReceivePacketBuffer.Read(&info.iDerection);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_WAIT_LIST);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_WAIT_LIST));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentNotifyReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_NOTIFY_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentProgramInfoReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_PROGRAM_INFO_REQ info;

	m_ReceivePacketBuffer.Read(&info.iScaleTapCode);
	m_ReceivePacketBuffer.Read(&info.iGroup);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_PROGRAM_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_PROGRAM_INFO_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentCreateReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_CREATE_REQ info;

	m_ReceivePacketBuffer.Read(&info.iGameMode);
	m_ReceivePacketBuffer.Read(&info.iScaleMode);
	m_ReceivePacketBuffer.Read((BYTE*)info.szTournamentName, MAX_TOURNAMENT_NAME_LENGTH+1);
	m_ReceivePacketBuffer.Read((BYTE*)info.szPass, MAX_TOURNAMENT_PASS_LENGTH+1);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_CREATE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_CREATE_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentEnterReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_ENTER_REQ info;

	m_ReceivePacketBuffer.Read(&info.iTournamentMode);
	m_ReceivePacketBuffer.Read(&info.iGameMode);
	m_ReceivePacketBuffer.Read(&info.iNumber);
	m_ReceivePacketBuffer.Read((BYTE*)info.szPass, MAX_TOURNAMENT_PASS_LENGTH+1);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_ENTER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_ENTER_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentExitReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_EXIT_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentCreatePreMadeTeamReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_CREATE_PREMADE_TEAM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentInviteUserReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_INVITE_USER_REQ info;
	m_ReceivePacketBuffer.Read(&info.iSlotNumber);
	m_ReceivePacketBuffer.Read((PBYTE)info.szName, MAX_GAMEID_LENGTH+1);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_INVITE_USER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_INVITE_USER_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentInviteSuggestRes(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_INVITE_SUGGEST_RES info;
	m_ReceivePacketBuffer.Read(&info.iResult);
	m_ReceivePacketBuffer.Read(&info.iSlotNumber);
	m_ReceivePacketBuffer.Read((PBYTE)info.szName, MAX_GAMEID_LENGTH+1);
	
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_INVITE_SUGGEST_RES);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_INVITE_SUGGEST_RES));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentExitPreMadeTeamReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_EXIT_PREMADE_TEAM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentButtonInfoReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_BUTTON_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);	
}

void CFSGameThread::Process_TournamentMoveSeasonReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_MOVE_SEASON_REQ info;
	m_ReceivePacketBuffer.Read(&info.iDir); 

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_MOVE_SEASON_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_MOVE_SEASON_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentRegisterPreMadeTeamReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_REGISTER_PREMADE_TEAM_REQ info;

	m_ReceivePacketBuffer.Read((PBYTE)info.szTeamName, MAX_TEAM_NAME_LENGTH+1); 
	for(int i=0; i<MAX_TOURNAMENT_TEAM_MEMBER_NUM-1; i++)
	{
		m_ReceivePacketBuffer.Read(&info.iaPositionSlot[i]); 
	}

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_REGISTER_PREMADE_TEAM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_REGISTER_PREMADE_TEAM_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentChangeNameReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_CHANGE_NAME_REQ info;

	m_ReceivePacketBuffer.Read((PBYTE)info.szName, MAX_TOURNAMENT_NAME_LENGTH+1); 

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_CHANGE_NAME_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_CHANGE_NAME_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentChangePasswordReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_CHANGE_PASSWORD_REQ info;

	m_ReceivePacketBuffer.Read((PBYTE)info.szPass, MAX_TOURNAMENT_PASS_LENGTH+1); 

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_CHANGE_PASSWORD_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_CHANGE_PASSWORD_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentChangeTeamReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_CHANGE_TEAM_REQ info;

	m_ReceivePacketBuffer.Read(&info.iNodeIdx1); 
	m_ReceivePacketBuffer.Read(&info.iNodeIdx2); 

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_CHANGE_TEAM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_CHANGE_TEAM_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentForceOutPreMadeTeamReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_REQ info;

	m_ReceivePacketBuffer.Read((PBYTE)info.szName, MAX_GAMEID_LENGTH+1); 

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_REQ));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentStartReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_START_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentMyTeamInfoReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_MYTEAM_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentUnresisterTeam(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_UNRESISTER_TEAM);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentPublicOpenInfoReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_PUBLIC_OPEN_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_TournamentCreatableReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer PacketComposer(G2M_TOURNAMENT_CREATABLE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_TOURNAMENT);
	CHECK_NULL_POINTER_VOID(pMatch);

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_StepComplateReward(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC);

	int m_iEventIndex;
	m_ReceivePacketBuffer.Read(&m_iEventIndex);
	pUser->CheckEvent(PERFORM_TIME_EVENT_REWARD, pGameODBC,m_iEventIndex);
}

void CFSGameThread::ProcessEnterLobbyReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	BOOL bResult = TRUE;
	int  iErrorCode = RESULT_COMMON_SUCCESS;

	if(FALSE == pFSClient->SetState(NEXUS_LOBBY))
	{
		bResult =FALSE;
		iErrorCode = ERROR_CODE_SYSTEM;
	}

	pFSClient->SendEnterLobbyRes(bResult,iErrorCode);
	pUser->GetUserRankMatch()->LoadUserNotice();
}

void CFSGameThread::Process_DetailGameRecord(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_DETAIL_GAME_RECORD info;
	ZeroMemory(&info, sizeof(SG2M_DETAIL_GAME_RECORD));
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iCount);
	CHECK_CONDITION_RETURN_VOID(DETAIL_GAME_RECORD_MAX_NUM < info.iCount);

	for(int i=0; i<info.iCount; i++)
	{
		m_ReceivePacketBuffer.Read(&info.byType1[i]);
		m_ReceivePacketBuffer.Read(&info.byType2[i]);
		m_ReceivePacketBuffer.Read(&info.byType3[i]);
		m_ReceivePacketBuffer.Read(&info.byValue[i]);
	}
	
	CPacketComposer PacketComposer(G2M_DETAIL_GAME_RECORD);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_DETAIL_GAME_RECORD));

	pMatch->Send(&PacketComposer);
}

void CFSGameThread::Process_RATING_POINT_REQ(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	if(TRUE == pUser->CheckCheatAble())
	{
		SG2M_CHEAT_RATING_POINT_REQ info;
		ZeroMemory(&info, sizeof(SG2M_CHEAT_RATING_POINT_REQ));
		info.iGameIDIndex = pUser->GetGameIDIndex();

		m_ReceivePacketBuffer.Read((PBYTE)info.szTargetGameID, MAX_GAMEID_LENGTH+1);

		CPacketComposer PacketComposer(G2M_CHEAT_RATING_POINT_REQ);
		PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CHEAT_RATING_POINT_REQ));

		pMatch->Send(&PacketComposer);
	}
}

void CFSGameThread::Process_UserGamePlayResearchRes( CFSGameClient* pClient )
{
	return ; //밑균
}

void CFSGameThread::Process_SignReq( CFSGameClient* pFSClient )
{
	/*CHECK_NULL_POINTER_VOID( pFSClient );
	CFSGameUser* pUser = (CFSGameUser *) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	int iResult = -1, iSignedCount = 0;
	pGameODBC->spUpdateSignLog( pUser->GetUserIDIndex(), iResult, iSignedCount );

	CPacketComposer packet( S2C_VISUALLOBBY_SIGN_RES );
	packet.Add( iResult );
	if ( 1 == iResult )
	{
		VectorRewardInfo vecInfo;

		int iRewardRandomIdx = rand() ;
000;
		if( pUser->GiveRandomReward( ERandomRewardGroupIDX_SIGN, iRewardRandomIdx , vecInfo ) )
		{
			packet.Add( int( vecInfo.size() ) );
			for ( int i = 0; i < vecInfo.size(); ++ i )
			{
				packet.Add( vecInfo[i].nRewardType );
				packet.Add( vecInfo[i].nRewardValue );
			}
		}
		packet.Add( iSignedCount );
	}
	pFSClient->Send( &packet );

	pUser->SendUserGold();
	pUser->SendCurAvatarTrophy();*/
}

void CFSGameThread::Process_SetObserverAllowReq( CFSGameClient* pFSClient )
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	int iIsAllowObserver = 1;
	BOOL bIsAllowObserver = TRUE;
	SG2M_IS_ALLOW_OBSERVERED_UPDATE info;

	m_ReceivePacketBuffer.Read( &iIsAllowObserver );
	bIsAllowObserver = ( iIsAllowObserver == 1 ? TRUE:FALSE );
	pUser->SetIsAllowObserver( bIsAllowObserver );

	info.bIsAllow = bIsAllowObserver;
	CPacketComposer packetComposer( G2M_IS_ALLOW_OBSERVERED_UPDATE );
	packetComposer.Add( (BYTE*)&info, sizeof(SG2M_IS_ALLOW_OBSERVERED_UPDATE) );
	pMatch->Send( &packetComposer );
}

void CFSGameThread::Process_ReportUserPattern(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser *) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	int iCount = 0, iPatternType = 0, iPattern = 0;
	m_ReceivePacketBuffer.Read(&iPatternType);
	m_ReceivePacketBuffer.Read(&iCount);

	vector<int> vPattern;
	for(int i = 0; i < iCount; i++)
	{
		m_ReceivePacketBuffer.Read(&iPattern);
		vPattern.push_back(iPattern);
	}

	USERPATTERNMANAGER.InsertUserPattern( iPatternType, vPattern);
}

void CFSGameThread::GiveGameGuideItem( CFSGameUser * pUser )
{
	int iItemCode = GAMEGUIDEITEMCODE;
	int iFirstUserResult = 0;
	int iCombackUserType = 0;
	int iNormalUserResult = 0;
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	CFSGameUserItem* pGameUserItem = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pGameUserItem);

	SUserItemInfo sUserItemInfo;
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID( pItemShop );

	SShopItemInfo	shopInfo;
	if( FALSE == pItemShop->GetItem(iItemCode, shopInfo))
		return;
	
	sUserItemInfo.iItemCode = iItemCode;
	sUserItemInfo.iChannel = shopInfo.iChannel;
	sUserItemInfo.iSexCondition = shopInfo.iSexCondition;		
	sUserItemInfo.iPropertyType = 1;
	sUserItemInfo.iPropertyKind = -1;
	sUserItemInfo.iBigKind = shopInfo.iBigKind;
	sUserItemInfo.iSmallKind = shopInfo.iSmallKind;

	sUserItemInfo.iStatus = 1;
	sUserItemInfo.iSellType = SELL_TYPE_CASH;
	sUserItemInfo.iSpecialPartsIndex = -1;
	sUserItemInfo.iCategory = shopInfo.iCategory;
	sUserItemInfo.iSellPrice = 0;

	CHECK_CONDITION_RETURN_VOID( pGameUserItem->IsInMyInventory(iItemCode) );

	pBaseODBC->GameGuideItem_Check_FirstCharacter(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), GAMEGUIDEITEMConfig.GetFirstCharacterCreationPeriod(), 
			GAMEGUIDEITEMConfig.GetFirstAccountCreationPeriod(),iFirstUserResult);
	pBaseODBC->GameGuideItem_Check_CombackUser(pUser->GetUserIDIndex(),iCombackUserType);
	pBaseODBC->GameGuideItem_Check_WinningRatePeriod(iNormalUserResult);

	
	if (iFirstUserResult == 1) // 캐릭터 만든지 1주일이 안된 유저라면,
	{		
		if (ODBC_RETURN_SUCCESS == pBaseODBC->ITEM_GiveInventory(pUser->GetGameIDIndex(),iItemCode,sUserItemInfo,GAMEGUIDEITEMConfig.GetFirstUserPeriod(),-1,-1,-1))
		{
			sUserItemInfo.iPropertyTypeValue = GAMEGUIDEITEMConfig.GetFirstUserPeriod();
			pGameUserItem->InsertNewItem(sUserItemInfo);
			pUser->SendEventComponentMsg(0,0,0,40,0,0,0,0);
		}

		//지급
	}
	else if (iCombackUserType == 1)
	{		
		if (ODBC_RETURN_SUCCESS == pBaseODBC->ITEM_GiveInventory(pUser->GetGameIDIndex(),iItemCode,sUserItemInfo,GAMEGUIDEITEMConfig.GetCombackUserPeriod(),-1,-1,-1))
		{
			sUserItemInfo.iPropertyTypeValue = GAMEGUIDEITEMConfig.GetCombackUserPeriod();
			pGameUserItem->InsertNewItem(sUserItemInfo);
			pUser->SendEventComponentMsg(0,0,0,40,0,0,0,0);
		}
	}
	else if (iNormalUserResult)// 기존 유저 이벤트 기간 확인
	{
		SAvatarInfo UserAvatarInfo;
		pUser->CurUsedAvatarSnapShot(UserAvatarInfo);			
		int iWin = UserAvatarInfo.TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_WIN];
		int iLose = UserAvatarInfo.TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_LOSE];
		int iTotalMatch = iWin + iLose;
		float fWinPercent = ((float)iWin / (float)(iTotalMatch) ) * 100.0f;

		if (fWinPercent <= GAMEGUIDEITEMConfig.GetWinningRate() && iTotalMatch >= GAMEGUIDEITEMConfig.GetiMinimumPlayCount() ) // 승률 확인 및 판수 확인
		{
			if (ODBC_RETURN_SUCCESS == pBaseODBC->ITEM_GiveInventory(pUser->GetGameIDIndex(),iItemCode,sUserItemInfo,GAMEGUIDEITEMConfig.GetWinningRatePeriod(),-1,-1,-1))
			{
				sUserItemInfo.iPropertyTypeValue = GAMEGUIDEITEMConfig.GetWinningRatePeriod();
				pGameUserItem->InsertNewItem(sUserItemInfo);
				pUser->SendEventComponentMsg(0,0,0,40,0,0,0,0);
			}
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_USER_ACTION_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_USER_ACTION_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_EVENT_USER_ACTION_REQ));

	pUser->SetEventUserActionType((EVENT_USER_ACTION_TYPE)req.iEventUserActionType);

	pUser->CheckEvent(PERFORM_TIME_USER_ACTION, &req.iParam, req.iEventIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_END_INTENSIVEPRACTICE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if(TRUE == pFSClient->SetState(NEXUS_LOBBY))
	{
		pUser->SetOptionEnable(OPTION_TYPE_WHISPER, TRUE);
		pUser->GetUserIntensivePractice()->EndGame();
	}

	CPacketComposer Packet(S2C_END_INTENSIVEPRACTICE_RES);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHANGE_ROOMMODE_INTENSIVEPRACTICE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_CHANGE_ROOMMODE_INTENSIVEPRACTICE_RES rs;
	m_ReceivePacketBuffer.Read(&rs.iRoomMode);
	m_ReceivePacketBuffer.Read(&rs.iLevel);

	CHECK_CONDITION_RETURN_VOID(rs.iRoomMode < INTENSIVEPRACTICE_ROOM_MODE_NONE || rs.iRoomMode > INTENSIVEPRACTICE_ROOM_MODE_RANKING);
	pUser->GetUserIntensivePractice()->SetRoomMode(rs.iRoomMode, rs.iAddGameTime);
	pUser->GetUserIntensivePractice()->SetLevel(rs.iLevel);

	if(rs.iRoomMode == INTENSIVEPRACTICE_ROOM_MODE_RANKING)
	{
		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_INTENSIVEPRACTICE, WORDPUZZLES_REWARDTYPE_INTENSIVEPRACTICE_SUCCESS_RANKINGMODE_STAGE, rs.iLevel);
		
		int iMissionConditionType = -1;
		int iHotGirlMissionType = -1;

		switch(rs.iLevel)
		{
		case INTENSIVEPRACTICE_LEVEL_BRONZE:	
			{
				iHotGirlMissionType = EVENT_HOTGIRL_MISSION_TYPE_CLEAR_INTENSIVE_PRACTICE_RANKMODE_0;
				break;
			}			
		case INTENSIVEPRACTICE_LEVEL_GOLD:
			{
				iHotGirlMissionType = EVENT_HOTGIRL_MISSION_TYPE_CLEAR_INTENSIVE_PRACTICE_RANKMODE_2;
				break;
			}
		case INTENSIVEPRACTICE_LEVEL_PLATINUM:	
			{	
				iHotGirlMissionType = EVENT_HOTGIRL_MISSION_TYPE_CLEAR_INTENSIVE_PRACTICE_RANKMODE_3;
				iMissionConditionType = CONDITION_TYPE_INTENSIVE_PRACTICE_3_CLEAR;
				break;
			}
		}

		if(iMissionConditionType > 0)
		{
			pUser->GetUserMissionShopEvent()->CheckMissionValue(iMissionConditionType);
		}
		if(iHotGirlMissionType > 0)
		{
			pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(iHotGirlMissionType);
		}
	}

	CPacketComposer Packet(S2C_CHANGE_ROOMMODE_INTENSIVEPRACTICE_RES);
	rs.btResult = RESULT_CHANGE_ROOMMODE_INTENSIVEPRACTICE_SUCCESS;
	INTENSIVEPRACTICEMANAGER.MakeNPCInfoList(Packet, rs, pUser->GetUserIntensivePractice()->GetPracticeType());
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ADD_POINT_INTENSIVEPRACTICE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_ADD_POINT_INTENSIVEPRACTICE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_ADD_POINT_INTENSIVEPRACTICE_REQ));

	if(rq.btResult == RESULT_ADD_POINT_INTENSIVEPRACTICE_SUCCESS)
	{
		SS2C_ADD_POINT_INTENSIVEPRACTICE_RES rs;
		pUser->GetUserIntensivePractice()->AddScore(rq.iaScoreType, rs.iAddGameTime);
		rs.iUpdatedScore = pUser->GetUserIntensivePractice()->GetScore(); 
		rs.iUpdatedBestPoint = pUser->GetUserIntensivePractice()->GetBestPoint(); 
		rs.iUpdatedExp = pUser->GetUserIntensivePractice()->GetUpdateExp();
		rs.iUpdatedGamePoint = pUser->GetUserIntensivePractice()->GetUpdatePoint();
		pUser->Send(S2C_ADD_POINT_INTENSIVEPRACTICE_RES, &rs, sizeof(SS2C_ADD_POINT_INTENSIVEPRACTICE_RES));
	}	
	else
	{
		if(pUser->GetUserIntensivePractice()->GetRoomMode() != INTENSIVEPRACTICE_ROOM_MODE_RANKING)
			pUser->GetUserIntensivePractice()->InitializeGame();
	}
	pUser->GetUserIntensivePractice()->AddTryCount();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_INTENSIVEPRACTICE_ROOMINFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_INTENSIVEPRACTICE_ROOMINFO_RES rs;
	rs.iBestPoint = pUser->GetUserIntensivePractice()->GetBestPoint(); 
	rs.iMaxExp = INTENSIVEPRACTICEMANAGER.GetOneDayLimitedExp(pUser->GetUserIntensivePractice()->GetPracticeType());
	rs.iExp = pUser->GetUserIntensivePractice()->GetUpdateExp();
	rs.iMaxGamePoint = INTENSIVEPRACTICEMANAGER.GetOneDayLimitedPoint(pUser->GetUserIntensivePractice()->GetPracticeType());
	rs.iGamePoint = pUser->GetUserIntensivePractice()->GetUpdatePoint();
	pUser->Send(S2C_INTENSIVEPRACTICE_ROOMINFO_RES, &rs, sizeof(SS2C_INTENSIVEPRACTICE_ROOMINFO_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_JOIN_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	FACTION.SendFactionJoinInfo(pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_RACE_BOARD_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SUserFactionInfo* pUserFaction = pUser->GetUserFaction();
	CHECK_NULL_POINTER_VOID(pUserFaction);

	////////////////////////////////////////////////////////////////
	/* TODO : Faction Renew
	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
	if(pCenter != NULL) 
	{		
		CPacketComposer Packet(G2S_FACTION_DELETE_BUFFITEM_REQ);
		pCenter->Send(&Packet);
	}

	if(pUserFaction->_iFactionIndex > 0 &&
		TRUE == FACTION.IsAllDistrictUseBuffStatus(pUserFaction->_iFactionIndex))
	{		
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
		if(pCenter != NULL) 
		{
			SG2S_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ info;
			strncpy_s(info.GameID, _countof(info.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
			info.iGameIDIndex = pUser->GetGameIDIndex();
			info.iFactionIndex = pUser->GetUserFaction()->_iFactionIndex;

			int iBuffItemCount = 1;
			info.iBuffTime = FACTION.GetBuffTime(iBuffItemCount);

			CPacketComposer Packet(G2S_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ);
			Packet.Add((PBYTE)&info, sizeof(SG2S_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ));
			pCenter->Send(&Packet);
		}
	}	
	*//////////////////////////////////////////////////////////////////

	FACTION.SendFactionRaceBoardInfo(pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_JOIN_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_ON_GOING != FACTION.GetRaceStatus() &&
		FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME != FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FACTION_JOIN_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_FACTION_JOIN_REQ));

	SS2C_FACTION_JOIN_RES res;
	res.btResult = FACTION.Join(pUser, req.iFactionIndex);
	res.iFactionIndex = req.iFactionIndex;

	pUser->Send(S2C_FACTION_JOIN_RES, &res, sizeof(SS2C_FACTION_JOIN_RES));

	if (RESULT_FACTION_JOIN_SUCCESS == res.btResult)
	{
		pUser->CheckFactionJoinItem();
		pUser->SendAvatarInfoUpdateToMatch();
		LOBBYCHAT.AddFactionUser(pUser);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_DISTRICT_SELECT_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_ON_GOING != FACTION.GetRaceStatus() &&
		FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME != FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FACTION_DISTRICT_SELECT_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_FACTION_DISTRICT_SELECT_REQ));

	SS2C_FACTION_DISTRICT_SELECT_RES res;
	res.btResult = FACTION.SelectDistrict(pUser, req.iDistrictIndex);
	res.iDistrictIndex = req.iDistrictIndex;
	pUser->Send(S2C_FACTION_DISTRICT_SELECT_RES, &res, sizeof(SS2C_FACTION_DISTRICT_SELECT_RES));

	if (RESULT_FACTION_DISTRICT_SELECT_SUCCESS == res.btResult || RESULT_FACTION_DISTRICT_SELECT_SUCCESS_REVERSAL == res.btResult)
	{
		pUser->SendAvatarInfoUpdateToMatch();
		FACTION.SendFactionRaceBoardInfo(pUser);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_SUPPORT_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_ON_GOING != FACTION.GetRaceStatus() &&
		FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME != FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if(TRUE == FACTION.SupportDistrict(pUser))
	{
		pUser->SendAvatarInfoUpdateToMatch();
		FACTION.SendFactionRaceBoardInfo(pUser);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_BUFFITEM_INFO_REQ)
{
	// TODO : Faction Renew
	return;

	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_ON_GOING != FACTION.GetRaceStatus() &&
		FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME != FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_FACTION_BUFFITEM_INFO_RES res;
	FACTION.MakeBuffItemInfo(res);
	res.iUserCash = pUser->GetCoin() + pUser->GetEventCoin();
	pUser->Send(S2C_FACTION_BUFFITEM_INFO_RES, &res, sizeof(SS2C_FACTION_BUFFITEM_INFO_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USE_BUFFITEM_REQ)
{
	// TODO : Faction Renew
	return;

	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_ON_GOING != FACTION.GetRaceStatus() &&
		FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME != FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FACTION_USE_BUFFITEM_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_FACTION_USE_BUFFITEM_REQ));

	SS2C_FACTION_USE_BUFFITEM_RES res;
	SBillingInfo BillingInfo;
	res.btResult = FACTION.UseBuffItem(pUser, req.iDistrictIndex, BillingInfo);
	res.iDistrictIndex = req.iDistrictIndex;
	if(RESULT_FACTION_USE_BUFFITEM_SUCCESS == res.btResult)
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
		if(pCenter == NULL) 
		{
			res.btResult = RESULT_FACTION_USE_BUFFITEM_FAIL;
			pUser->Send(S2C_FACTION_USE_BUFFITEM_RES, &res, sizeof(SS2C_FACTION_USE_BUFFITEM_RES));
			return;
		}
	
		SG2S_FACTION_USE_BUFFITEM_REQ req;
		strncpy_s(req.GameID, _countof(req.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		req.iGameIDIndex = pUser->GetGameIDIndex();
		memcpy(&req.BillingInfo, &BillingInfo, sizeof(SBillingInfo));

		CPacketComposer Packet(G2S_FACTION_USE_BUFFITEM_REQ);
		Packet.Add((PBYTE)&req, sizeof(SG2S_FACTION_USE_BUFFITEM_REQ));
		pCenter->Send(&Packet);
	}
	else
	{
		pUser->Send(S2C_FACTION_USE_BUFFITEM_RES, &res, sizeof(SS2C_FACTION_USE_BUFFITEM_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_DAILY_RANK_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	FACTION.SendDailyRankInfo(pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_SEASON_RANK_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FACTION_SEASON_RANK_INFO_REQ	rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_FACTION_SEASON_RANK_INFO_REQ));

	FACTION.SendSeasonRankInfo(pUser, rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_RANK_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FACTION_RANK_INFO_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_FACTION_RANK_INFO_REQ));

	FACTION.SendRankInfo(pUser, req);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USER_RANKING_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FACTION_USER_RANKING_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_FACTION_USER_RANKING_REQ));

	FACTION.SendUserRank(pUser, req);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USER_MISSION_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	FACTION.SendFactionUserMissionInfo(pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USER_MISSION_REWARD_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_FACTION_USER_MISSION_REWARD_REQ	rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_FACTION_USER_MISSION_REWARD_REQ));

	FACTION.GiveMissionRewardFactionPoint(pUser, rs.btMissionConditionType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FACTION_SHOP_REQ)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	FACTION.SendFactionShopInfo(pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_WORDPUZZLES_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == WORDPUZZLES.IsOpen());

	pUser->GetUserWordPuzzle()->SendWordPuzzleInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_WORDPUZZLES_EVENT_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == WORDPUZZLES.IsOpen());

	SC2S_WORDPUZZLES_EVENT_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_WORDPUZZLES_EVENT_GET_REWARD_REQ));

	SS2C_WORDPUZZLES_EVENT_GET_REWARD_RES rs;
	rs.iWordIndex = rq.iWordIndex;
	rs.btResult = pUser->GetUserWordPuzzle()->GetRewardReq(rq.iWordIndex);
	pUser->Send(S2C_WORDPUZZLES_EVENT_GET_REWARD_RES, &rs, sizeof(SS2C_WORDPUZZLES_EVENT_GET_REWARD_RES));
	
	if(rs.btResult == RESULT_WORDPUZZLES_EVENT_GET_REWARD_SUCCESS)
		pUser->GetUserWordPuzzle()->SendWordPuzzleInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLGIFT_CLEAR_STEP_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_HOTGIRLGIFT_CLEAR_STEP_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GetUserHotGirlGiftEvent()->SendClearStepResult(rq.iCurrentStep, rq.btProductIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PAYBACK_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->MakeAndSendPaybackInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PAYBACK_WANTPAYBACK_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GiveAndSendUserWantPayback();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMatchingCard()->SendMatchingCardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_READY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMatchingCard()->SendMatchingCardReady();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_REWARDINFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMatchingCard()->SendMatchingCardRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_FLIPCARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_MATCHINGCARD_FLIPCARD_REQ	info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_EVENT_MATCHINGCARD_FLIPCARD_REQ));

	pUser->GetUserMatchingCard()->FlipCard(info.btIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_TIMECHECK_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMatchingCard()->CheckAndSendTimeCheck();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_ATTENDANCE_ITEMSHOP_UPDATEITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == ATTENDANCEITEMSHOP.IsOpen());

	SS2C_EVENT_ATTENDANCE_ITEMSHOP_UPDATEITEM_RES	rs;
	time_t	tCurrentDBDate = _GetCurrentDBDate;

	rs.iRefreshKind = ATTENDANCEITEMSHOP.GetAttendanceYesterdayItemBigKind(tCurrentDBDate);	

	if ( 0 > ATTENDANCEITEMSHOP.GetTodayItemIndex(tCurrentDBDate))
		rs.btCount = 0;
	else
		rs.btCount = 1;

	CHECK_CONDITION_RETURN_VOID( ITEM_BIG_KIND_DEFAULT == rs.iRefreshKind );

	CPacketComposer	Packet(S2C_EVENT_ATTENDANCE_ITEMSHOP_UPDATEITEM_RES);

	Packet.Add((PBYTE)&rs, sizeof( SS2C_EVENT_ATTENDANCE_ITEMSHOP_UPDATEITEM_RES));
	
	if( 0 < rs.btCount )
		CHECK_CONDITION_RETURN_VOID( FALSE == ATTENDANCEITEMSHOP.CheckAndMakePacketAttenDanceTodayItem(tCurrentDBDate,Packet));

	pUser->Send(&Packet);

	pUser->SendAttendaceItemshopStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_XIGNCODE_SECURITY_DATA_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SXIGNCODE_SECURITY_DATA_NOT info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SXIGNCODE_SECURITY_DATA_NOT));

	XIGNCODEMANAGER.OnReceive(pFSClient, info);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DELETE_WAIT_FRIEND_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_DELETE_WAIT_FRIEND_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_DELETE_WAIT_FRIEND_REQ));

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
	if(pCenter == NULL) 
	{
		SS2C_DELETE_WAIT_FRIEND_RES rs;
		rs.btResult = RESULT_DELETE_WAIT_FRIEND_FAIL;
		pUser->Send(S2C_DELETE_WAIT_FRIEND_RES, &rs, sizeof(SS2C_DELETE_WAIT_FRIEND_RES));
		return;
	}

	SG2S_DELETE_WAIT_FRIEND_REQ req;
	strncpy_s(req.GameID, _countof(req.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
	req.iGameIDIndex = pUser->GetGameIDIndex();
	strncpy_s(req.szFriendID, _countof(req.szFriendID), rq.szFriendID, MAX_GAMEID_LENGTH);	

	CPacketComposer Packet(G2S_DELETE_WAIT_FRIEND_REQ);
	Packet.Add((PBYTE)&req, sizeof(SG2S_DELETE_WAIT_FRIEND_REQ));
	pCenter->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FIRST_CASHBACK_EVENT_ENABLE_USER_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_FIRST_CASHBACK_EVENT_ENABLE_USER_RES rs;
	rs.bEventUser = FALSE;
	if(TRUE == pUser->CheckFirstCashBackEventUser())
		rs.bEventUser = TRUE;

	pUser->Send(S2C_FIRST_CASHBACK_EVENT_ENABLE_USER_RES, &rs, sizeof(SS2C_FIRST_CASHBACK_EVENT_ENABLE_USER_RES));
}
DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_CREATE_ROOM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_CONDITION_RETURN_VOID(nullptr != pMatch);

	if(NULL == pUser->GetCurUsedAvatar())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, CHECK_FAIL, "Process_PVECreateRoom - ��4", pUser->GetGameID());
	return;
	}
	
	if (pUser->IsShutdownPlayImpossibleTime())
	{
		int iResult = (int)ERRORCODE_SHUTDOWN_USER_RESTRICTION;
		CPacketComposer PacketComposer(S2C_CREATE_ROOM_RESULT);
		PacketComposer.Add(iResult);
		pUser->Send(&PacketComposer);
		return;
	}

	SC2S_PVE_CREATE_ROOM_REQ	rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_CREATE_ROOM_REQ));

	SG2M_JOIN_PVE_ROOM_REQ	info;

	if(MATCH_TYPE_PVE == rq.btMatchType)
	{
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE);
		CHECK_NULL_POINTER_VOID(pMatch);
	}
	else if(MATCH_TYPE_PVE_3CENTER == rq.btMatchType)
	{
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_3CENTER);
		CHECK_NULL_POINTER_VOID(pMatch);
	}
	else if(MATCH_TYPE_PVE_CHANGE_GRADE == rq.btMatchType)
	{
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_CHANGE_GRADE);
		CHECK_NULL_POINTER_VOID(pMatch);
	}
	else if(MATCH_TYPE_PVE_AFOOTBALL == rq.btMatchType)
	{
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_AFOOTBALL);
		CHECK_NULL_POINTER_VOID(pMatch);
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_PVE, INVALED_DATA, CHECK_FAIL, "Process_PVECreateRoom - GameID:��4, MatchType:0", pUser->GetGameID(), rq.btMatchType);
		return;
	}

	if((MATCH_TYPE_PVE_3CENTER == rq.btMatchType ||
		MATCH_TYPE_PVE_CHANGE_GRADE == rq.btMatchType ||
		MATCH_TYPE_PVE_AFOOTBALL == rq.btMatchType) &&
		CLOSED == PVEMANAGER.IsOpen(rq.btMatchType))
	{
		SS2C_PVE_CREATE_ROOM_RES rs;
		rs.btResult = RESULT_PVE_CREATE_ROOM_FAIL_NOT_OPEN_TIME;
		pUser->Send(S2C_PVE_CREATE_ROOM_RES, &rs, sizeof(SS2C_PVE_CREATE_ROOM_RES));
		return;
	}

	if(INDEX_JOIN_PVE_MODE_JOIN == rq.iMode)
	{
		pUser->GetJoinTeamConfig()->SetPVEPosition(rq.iLOD, rq.shSlotPosition[0],rq.shSlotPosition[1], rq.iPlayerNum);
		rq.iPlayerNum = pUser->GetJoinTeamConfig()->GetPVEInfo(rq);
	}

	if(INDEX_JOIN_PVE_MODE_AUTOJOIN == rq.iMode)
	{
		rq.iMode = INDEX_JOIN_PVE_MODE_JOIN;
		rq.iPlayerNum = pUser->GetJoinTeamConfig()->GetPVEInfo(rq);
	}

	pUser->GetMatchLoginUserInfo(info, pMatch->GetMatchType());

	info.iMode = rq.iMode;
	info.uRoomID = rq.uiRoomID;
	info.iPlayerNum = rq.iPlayerNum;
	info.iAIPositionIndex = 0;
	info.iLOD = rq.iLOD;
	strncpy_s(info.szName, _countof(info.szName), rq.szName, MAX_TEAM_NAME_LENGTH);
	strncpy_s(info.szPass, _countof(info.szPass), rq.szPass, MAX_TEAM_PASS_LENGTH);
	info.btIsAllowToObserve = rq.btIsAllowToObserve;
	for( int i = 0 ; i < MAX_USER_SLOT_CONFIG_COUNT ; ++i )
	{
		if( -1 < rq.shSlotPosition[i] )
			info.shSlotPosition[i] = rq.shSlotPosition[i];
		else
			info.shSlotPosition[i] = -1;
	}

	pMatch->SendPacket(G2M_JOIN_PVE_ROOM_REQ, &info, sizeof(SG2M_JOIN_PVE_ROOM_REQ));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOSEGAME_NOT_OPEN_TODAY_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->UpdateLoseLeadNotOpenToday();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOSEGAME_MOVE_PVEMODE_PLAY_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->UpdateLoseLeadPveModeReady();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CAMERA_ANGLE_SAVE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CAMERA_ANGLE_SAVE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_CAMERA_ANGLE_SAVE_REQ));

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC(ODBC_LOG);
	CHECK_NULL_POINTER_VOID(pLogODBC);

	pLogODBC->LOG_SAVE_USER_CAMERA_Angle(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), rq.iAngleType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_BUTTON_TYPE_SAVE_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_LOBBY_BUTTON_TYPE_SAVE_NOT rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_LOBBY_BUTTON_TYPE_SAVE_NOT));

	pUser->SaveLobbyButtonType(FALSE, &rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(S2S_CHECK_FIRST_CONNECT_RES)
{
	const NEXUS_CLIENT_STATE eProxy = pFSClient->GetState();
	if( eProxy > FS_PROXY && eProxy < FS_PROXY_END )
	{
		CSvrProxy* csProxy = (CSvrProxy*)pFSClient;
		csProxy->SetFirstPacketRecv(TRUE);
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, PROXY_CONNET, SUCCESS, "FIRST PACKET PID:11866902, STATE:0", csProxy->GetProcessID(), eProxy);


DEFINE_GAMETHREAD_PROC_FUNC(S2S_CHECK_FIRST_CONNECT_REQ)
{
	CPacketComposer	PacketComposer(S2S_CHECK_FIRST_CONNECT_RES);
	pFSClient->Send(&PacketComposer);
}