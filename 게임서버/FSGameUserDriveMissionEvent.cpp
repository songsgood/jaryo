qinclude "stdafx.h"
qinclude "FSGameUserDriveMissionEvent.h"
qinclude "DriveMissionEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserDriveMissionEvent::CFSGameUserDriveMissionEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_btMissionStatus(USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_NONE)
	, m_btCurrentChoiceMissionIndex(0)
	, m_iCurrentValue(0)
	, m_iJoyCityMissionCurrentValue(0)
	, m_iMyMoney(0)
	, m_iTodayGetMoney(0)
	, m_tRecentCheckDate(-1)
{
}

CFSGameUserDriveMissionEvent::~CFSGameUserDriveMissionEvent(void)
{
}

BOOL			CFSGameUserDriveMissionEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == DRIVEMISSIONEVENT.IsOpen(), TRUE);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_DRIVEMISSION_GetUserData(m_pUser->GetUserIDIndex(), m_btMissionStatus, m_btCurrentChoiceMissionIndex, m_iCurrentValue, m_iJoyCityMissionCurrentValue, m_iMyMoney, m_iTodayGetMoney, m_tRecentCheckDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_DRIVEMISSION_EVENT, DB_DATA_LOAD, FAIL, "EVENT_DRIVEMISSION_GetUserData failed");
		return FALSE;	
	}

	m_bDataLoaded = TRUE;

	CheckResetDate();

	return TRUE;
}

void			CFSGameUserDriveMissionEvent::CheckResetDate(time_t tCurrentTime/* = _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	BOOL	bResult = FALSE;

	if(-1 == m_tRecentCheckDate)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tRecentCheckDate, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= EVENT_RESET_HOUR;

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tRecentCheckDate <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		int iJoyCityMissionValue = MIN(DRIVE_JOYCITY_MISSION_CLEAR_VALUE, m_iJoyCityMissionCurrentValue + 1);
		
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_DRIVEMISSION_ResetUserData(m_pUser->GetUserIDIndex(), tCurrentTime, iJoyCityMissionValue))
		{
			m_btMissionStatus = USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_NONE;
			m_iCurrentValue = 0;
			m_iTodayGetMoney = 0;
			m_tRecentCheckDate = tCurrentTime;
			m_iJoyCityMissionCurrentValue = iJoyCityMissionValue;
		}
	}
}

void			CFSGameUserDriveMissionEvent::SendEventMissionInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DRIVEMISSIONEVENT.IsOpen());

	CPacketComposer* pPacket = DRIVEMISSIONEVENT.GetPacketEventMissionInfo();
	m_pUser->Send(pPacket);
}

void			CFSGameUserDriveMissionEvent::SendEventProductInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DRIVEMISSIONEVENT.IsOpen());

	CPacketComposer* pPacket = DRIVEMISSIONEVENT.GetPacketEventProductInfo();
	m_pUser->Send(pPacket);
}

void			CFSGameUserDriveMissionEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DRIVEMISSIONEVENT.IsOpen());
	
	CheckResetDate();
	
	// ����Ƚ�� �ʱ�ȭ
	if(m_pUser->GetLocation() == U_LOBBY)
		CheckUpdateMission(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_PLAY_CONTINUE_WIN_CNT, 0);

	SS2C_USER_CHOICE_MISSION_2ND_USER_INFO_RES	ss;
	ss.btCurrentChoiceMissionIndex = m_btCurrentChoiceMissionIndex;
	ss.btMissionStatus = m_btMissionStatus;
	ss.iCurrentValue = m_iCurrentValue;
	ss.iJoyCityMissionCurrentValue = m_iJoyCityMissionCurrentValue;
	ss.iMyMoney = m_iMyMoney;
	ss.iTodayGetMoney = m_iTodayGetMoney;
	
	m_pUser->Send(S2C_USER_CHOICE_MISSION_2ND_USER_INFO_RES, &ss, sizeof(SS2C_USER_CHOICE_MISSION_2ND_USER_INFO_RES));
}

void			CFSGameUserDriveMissionEvent::SendButtonStatus()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DRIVEMISSIONEVENT.IsOpen());

	CheckResetDate();

	SS2C_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_RES	ss;
	ss.btStatus = m_btMissionStatus;
	ss.bGetTodayLimitMoney = (m_iTodayGetMoney >= DRIVE_1DAY_MAX_LIMIT_MONEY);

	m_pUser->Send(S2C_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_RES, &ss, sizeof(SS2C_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_RES));
}

void			CFSGameUserDriveMissionEvent::ChoiceMission(BYTE btMissionIndex)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CheckResetDate();

	SS2C_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_RES	ss;
	BYTE btMissionType = 0;

	if(CLOSED == DRIVEMISSIONEVENT.IsOpen())
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ_FAIL_EVENT_CLOSED;
	}
	else if(USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_NONE != m_btMissionStatus &&
		btMissionIndex == m_btCurrentChoiceMissionIndex)
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ_FAIL_ALREADY_CHOICE;
	}
	else if(FALSE == DRIVEMISSIONEVENT.GetMissionType(btMissionIndex, btMissionType))
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ_FAIL_WRONG_MISSIONINDEX;
	}
	else if(DRIVE_MISSION_TYPE_SPECIAL == btMissionType &&	
		m_iJoyCityMissionCurrentValue < DRIVE_JOYCITY_MISSION_CLEAR_VALUE)
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ_FAIL_HAVETO_COMPLETE_CONDITION_MISSION;
	}
	else if(DRIVE_MISSION_TYPE_NORMAL == btMissionType &&
		m_iTodayGetMoney >= DRIVE_1DAY_MAX_LIMIT_MONEY)
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ_FAIL_TODAY_GET_LIMIT_MONEY;
	}
	else
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_DRIVEMISSION_ChoiceMission(m_pUser->GetUserIDIndex(), btMissionIndex))
		{
			m_btCurrentChoiceMissionIndex = btMissionIndex;
			m_btMissionStatus = USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_GOING;
			m_iCurrentValue = 0;
			ss.btResult = USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ_SUCCESS;
		}
	}

	m_pUser->Send(S2C_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_RES, &ss, sizeof(SS2C_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_RES));
}

void			CFSGameUserDriveMissionEvent::GiveMoney()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CheckResetDate();

	SS2C_USER_CHOICE_MISSION_2ND_MONEY_RES	ss;

	if(CLOSED == DRIVEMISSIONEVENT.IsOpen())
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_MONEY_REQ_FAIL_EVENT_CLOSED;
	}
	else if(USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_COMPLETE != m_btMissionStatus)
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_MONEY_REQ_FAIL_MISSION_NOT_CLEAR;
	}
	else
	{
		BYTE btMissionType = 0;
		int iJoyCityMissionValue = 0;
		int iAddMoney = DRIVEMISSIONEVENT.GetMissionRewardMoney(m_btCurrentChoiceMissionIndex);
		int iAddTodayGetMoney = 0;
		CHECK_CONDITION_RETURN_VOID(FALSE == DRIVEMISSIONEVENT.GetMissionType(m_btCurrentChoiceMissionIndex, btMissionType));
		
		if(DRIVE_MISSION_TYPE_NORMAL == btMissionType)
		{
			iJoyCityMissionValue = m_iJoyCityMissionCurrentValue;
			iAddMoney = MIN(iAddMoney, DRIVE_1DAY_MAX_LIMIT_MONEY - m_iTodayGetMoney);
			iAddTodayGetMoney = iAddMoney;
		}

		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_DRIVEMISSION_GiveMoney(m_pUser->GetUserIDIndex(), iJoyCityMissionValue, iAddMoney, iAddTodayGetMoney))
		{
			m_btMissionStatus = USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_NONE;
			m_iCurrentValue = 0;
			m_iJoyCityMissionCurrentValue = iJoyCityMissionValue;
			m_iMyMoney += iAddMoney;
			m_iTodayGetMoney += iAddTodayGetMoney;

			ss.btResult = USER_CHOICE_MISSION_2ND_MONEY_REQ_SUCCESS;
			if(DRIVE_MISSION_TYPE_NORMAL == btMissionType && m_iTodayGetMoney >= DRIVE_1DAY_MAX_LIMIT_MONEY)
			{
				ss.btResult = USER_CHOICE_MISSION_2ND_MONEY_REQ_SUCCESS_GET_TODAY_LIMIT_CUT;
			}
		}
	}

	m_pUser->Send(S2C_USER_CHOICE_MISSION_2ND_MONEY_RES, &ss, sizeof(SS2C_USER_CHOICE_MISSION_2ND_MONEY_RES));
}

void			CFSGameUserDriveMissionEvent::BuyProduct(BYTE btProductIndex)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_RES	ss;
	SDriveProductInfo	sProduct;

	if(CLOSED == DRIVEMISSIONEVENT.IsOpen())
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ_FAIL_EVENT_CLOSED;
	}
	else if(FALSE == DRIVEMISSIONEVENT.GetProductInfo(btProductIndex, sProduct))
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ_FAIL_WRONG_PRODUCT;
	}
	else if(m_iMyMoney < sProduct.iPrice)
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ_FAIL_LACK_MONEY;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ_FAIL_FULL_MAILBOX;
	}
	else
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_DRIVEMISSION_BuyProduct(m_pUser->GetUserIDIndex(), btProductIndex, sProduct.iPrice))
		{
			m_iMyMoney -= sProduct.iPrice;
			ss.btResult = USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ_SUCCESS;
			
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, sProduct.sReward.iRewardIndex);
			if(NULL == pReward)
			{
				ss.btResult = USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ_FAIL_WRONG_PRODUCT;
				WRITE_LOG_NEW(LOG_TYPE_DRIVEMISSION_EVENT, DB_DATA_LOAD, FAIL, "EVENT_DRIVEMISSION_BuyProduct GiveReward Fail, UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), sProduct.sReward.iRewardIndex);

	}
	}

	m_pUser->Send(S2C_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_RES, &ss, sizeof(SS2C_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_RES));
}

BOOL			CFSGameUserDriveMissionEvent::CheckUpdateMission(BYTE btMissionConditType, int iAddValue/* = 1*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == DRIVEMISSIONEVENT.IsOpen(), FALSE);

	CheckResetDate();

	CHECK_CONDITION_RETURN(USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_GOING != m_btMissionStatus, FALSE);

	SDriveMissionInfo	sMission;
	CHECK_CONDITION_RETURN(FALSE == DRIVEMISSIONEVENT.GetMissionInfo(m_btCurrentChoiceMissionIndex, sMission), FALSE);
	CHECK_CONDITION_RETURN(sMission.btMissionConditionType != btMissionConditType, FALSE;)

	int iMissionValue = m_iCurrentValue;

	if(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_PLAY_CONTINUE_WIN_CNT == btMissionConditType)
	{
		iMissionValue = iAddValue;
	}
	else if(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_GET_BETTER_THAN_PLAY_GRADE_A == btMissionConditType)
	{
		SHORT shEstPoint = iAddValue;
		CHECK_CONDITION_RETURN(
			GAME_ESTIMATE_AAA != shEstPoint && 
			GAME_ESTIMATE_AA != shEstPoint && 
			GAME_ESTIMATE_A != shEstPoint, FALSE);

		iMissionValue ++;
	}
	else
	{
		iMissionValue += iAddValue;
	}

	BYTE btMissionStatus = USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_GOING;

	if(iMissionValue >= sMission.iClearValue)
	{
		iMissionValue = sMission.iClearValue;
		btMissionStatus = USER_CHOICE_MISSION_2ND_USER_MISSION_STATUS_COMPLETE;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_DRIVEMISSION_UpdateMission(m_pUser->GetUserIDIndex(), iMissionValue, btMissionStatus), FALSE);

	m_iCurrentValue = iMissionValue;
	m_btMissionStatus = btMissionStatus;

	return TRUE;
}
