qinclude "stdafx.h"
qinclude "FSGameUserRandomItemDrawEvent.h"

qinclude "CFSGameUser.h"
qinclude "RandomItemDrawEventManager.h"
qinclude "ThreadODBCManager.h"

CFSGameUserRandomItemDrawEvent::CFSGameUserRandomItemDrawEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_btGiveRandomItemDrawFirstConnectCoin(RANDOM_ITEM_DRAW_FIRST_CONNECT_NONE)
	, m_btGiveRandomItemDrawFirstGamePlayCoin(RANDOM_ITEM_DRAW_FIRST_GAME_NONE)
{
}
CFSGameUserRandomItemDrawEvent::~CFSGameUserRandomItemDrawEvent(void)
{
}

BOOL CFSGameUserRandomItemDrawEvent::Load(void)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMDRAWEVENT.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_DRAW_GetUserData(m_pUser->GetUserIDIndex(), m_btGiveRandomItemDrawFirstConnectCoin, m_btGiveRandomItemDrawFirstGamePlayCoin))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_RANDOMITEM_DRAW_GetUserData failed. UserIDIndex:10752790", m_pUser->GetUserIDIndex());
turn FALSE;
	}

	return TRUE;
}
BOOL CFSGameUserRandomItemDrawEvent::CheckAndUpdateGamePlay(void)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMDRAWEVENT.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(RANDOM_ITEM_DRAW_FIRST_GAME_NONE != m_btGiveRandomItemDrawFirstGamePlayCoin, FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iTempUpdatedCoin = 0;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_DRAW_UpdateUserData(RANDOM_ITEM_DRAW_FIRST_COIN_PLAY, m_pUser->GetUserIDIndex(), RANDOM_ITEM_DRAW_FIRST_GAME_PLAY_COMPLETE, iTempUpdatedCoin))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "CheckAndUpdateGamePlay EVENT_RANDOMITEM_DRAW_UpdateUserData failed. UserIDIndex:10752790", m_pUser->GetUserIDIndex());
turn FALSE;
	}

	m_btGiveRandomItemDrawFirstGamePlayCoin = RANDOM_ITEM_DRAW_FIRST_GAME_PLAY_COMPLETE;

	return TRUE;
}
BOOL CFSGameUserRandomItemDrawEvent::CheckRandomDrawItemGamePlay(int& iUpdatedCoin)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMDRAWEVENT.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(RANDOM_ITEM_DRAW_FIRST_GAME_PLAY_COMPLETE != m_btGiveRandomItemDrawFirstGamePlayCoin, FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_DRAW_UpdateUserData(RANDOM_ITEM_DRAW_FIRST_COIN_PLAY, m_pUser->GetUserIDIndex(), RANDOM_ITEM_DRAW_FIRST_GAME_PLAY_GET_REWARD, iUpdatedCoin))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "CheckRandomDrawItemGamePlay EVENT_RANDOMITEM_DRAW_UpdateUserData failed. UserIDIndex:10752790", m_pUser->GetUserIDIndex());
turn FALSE;
	}

	m_btGiveRandomItemDrawFirstGamePlayCoin = RANDOM_ITEM_DRAW_FIRST_GAME_PLAY_GET_REWARD;

	return TRUE;
}
BOOL CFSGameUserRandomItemDrawEvent::CheckRandomDrawItemConnect(int& iUpdatedCoin)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMDRAWEVENT.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(RANDOM_ITEM_DRAW_FIRST_CONNECT_NONE != m_btGiveRandomItemDrawFirstConnectCoin, FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_DRAW_UpdateUserData(RANDOM_ITEM_DRAW_FIRST_COIN_CONNECT, m_pUser->GetUserIDIndex(), RANDOM_ITEM_DRAW_FIRST_CONNECT_GET_REWARD, iUpdatedCoin))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "CheckRandomDrawItemConnect EVENT_RANDOMITEM_DRAW_UpdateUserData failed. UserIDIndex:10752790", m_pUser->GetUserIDIndex());
turn FALSE;
	}

	m_btGiveRandomItemDrawFirstConnectCoin = RANDOM_ITEM_DRAW_FIRST_CONNECT_GET_REWARD;

	return TRUE;
}
