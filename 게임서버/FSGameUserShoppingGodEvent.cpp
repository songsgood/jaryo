qinclude "stdafx.h"
qinclude "FSGameUserShoppingGodEvent.h"
qinclude "ShoppingGodEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserShoppingGodEvent::CFSGameUserShoppingGodEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
	ZeroMemory(&m_sUserInfo, sizeof(SShoppingGodUserInfo));
}


CFSGameUserShoppingGodEvent::~CFSGameUserShoppingGodEvent(void)
{
}

BOOL CFSGameUserShoppingGodEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == SHOPPINGGOD.IsEventOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	time_t tCurrentTime = _time64(NULL);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_SHOPPING_GOD_GetUserData(m_pUser->GetUserIDIndex(), tCurrentTime, m_sUserInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGGOD_EVENT, INITIALIZE_DATA, FAIL, "EVENT_SHOPPING_GOD_GetUserData error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	CheckResetTime(tCurrentTime);

	return TRUE;
}

void CFSGameUserShoppingGodEvent::SendEventStatus()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPINGGOD.IsEventOpen());

	SS2C_SHOPPING_GOD_STATUS_NOT	ss;
	ss.btStatus = SHOPPINGGOD.IsEventOpen();
	m_pUser->Send(S2C_SHOPPING_GOD_STATUS_NOT, &ss, sizeof(SS2C_SHOPPING_GOD_STATUS_NOT));
}

void CFSGameUserShoppingGodEvent::SendShoppingGodEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPINGGOD.IsEventOpen());

	CPacketComposer Packet(S2C_SHOPPING_GOD_EVENT_INFO_RES);
	SHOPPINGGOD.MakePacketShoppingGodEventInfo(Packet);
	m_pUser->Send(&Packet);
}

void CFSGameUserShoppingGodEvent::SendShoppingGodUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPINGGOD.IsEventOpen());

	CheckAndUpdateUserMission(SHOPPING_GOD_MISSION_TYPE_LOGIN_TIME);

	SS2C_SHOPPING_GOD_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SHOPPING_GOD_USER_INFO_RES));

	ss.iCurrentChip = m_sUserInfo.iCurrentChip;
	ss.iTodayGetChip = m_sUserInfo.iTodayGetChip;

	m_pUser->Send(S2C_SHOPPING_GOD_USER_INFO_RES, &ss, sizeof(SS2C_SHOPPING_GOD_USER_INFO_RES));
}

void CFSGameUserShoppingGodEvent::SendLoginTimeInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPINGGOD.IsEventOpen());

	SS2C_SHOPPING_GOD_CHECK_LOGIN_TIME_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SHOPPING_GOD_CHECK_LOGIN_TIME_RES));

	time_t tCurrentTime = _time64(NULL);
	ss.bIsGetChip = CheckAndUpdateUserMission(SHOPPING_GOD_MISSION_TYPE_LOGIN_TIME, tCurrentTime);

	if(m_sUserInfo.iTodayGetChip >= SHOPPINGGOD.GetOneDayMaxChip())
	{
		ss.iNextRemainSecTime = -1;
	}
	else
	{
		int iLoginTimeMissionSec = SHOPPINGGOD.GetConditionValue(SHOPPING_GOD_MISSION_TYPE_LOGIN_TIME) * 60;

		ss.iNextRemainSecTime = iLoginTimeMissionSec - (tCurrentTime - m_sUserInfo.tStartCheckTime);
	}	

	m_pUser->Send(S2C_SHOPPING_GOD_CHECK_LOGIN_TIME_RES, &ss, sizeof(SS2C_SHOPPING_GOD_CHECK_LOGIN_TIME_RES));
}

BOOL CFSGameUserShoppingGodEvent::CheckResetTime(time_t tCurrentTime /*= _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == SHOPPINGGOD.IsEventOpen(), FALSE);

	BOOL	bResult = FALSE;

	if(-1 == m_sUserInfo.tRecentResetTime)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_sUserInfo.tRecentResetTime, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= SHOPPINGGOD.GetResetTime();

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_sUserInfo.tRecentResetTime <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_SHOPPING_GOD_ResetUserMission(m_pUser->GetUserIDIndex(), tCurrentTime))
		{
			m_sUserInfo.iTodayGetChip = 0;
			m_sUserInfo.tStartCheckTime = tCurrentTime;
			m_sUserInfo.tRecentResetTime = tCurrentTime;
		}
	}

	return bResult;
}

BOOL CFSGameUserShoppingGodEvent::CheckAndUpdateUserMission(BYTE btMissionType, time_t tCurrentTime/* = _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == SHOPPINGGOD.IsEventOpen(), FALSE);

	BOOL bCheck = FALSE;

	CheckResetTime(tCurrentTime);

	CHECK_CONDITION_RETURN(m_sUserInfo.iTodayGetChip >= SHOPPINGGOD.GetOneDayMaxChip(), FALSE);

	if(SHOPPING_GOD_MISSION_TYPE_LOGIN_TIME == btMissionType)
	{
		// 분 단위가 바뀌었는지 체크.
		bCheck = (((tCurrentTime - m_sUserInfo.tStartCheckTime) / 60) > 0);
	}
	else if(SHOPPING_GOD_MISSION_TYPE_ELO_PLAY == btMissionType)
	{
		bCheck = TRUE;
	}

	if(TRUE == bCheck)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		int iAddChip = 0;

		int iResult = pODBC->EVENT_SHOPPING_GOD_GiveUserChip(m_pUser->GetUserIDIndex(), btMissionType, tCurrentTime, iAddChip, m_sUserInfo.tStartCheckTime);
		if(ODBC_RETURN_SUCCESS == iResult)
		{
			m_sUserInfo.iCurrentChip += iAddChip;
			m_sUserInfo.iTodayGetChip += iAddChip;

			if(iAddChip > 0)
				return TRUE;
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_SHOPPINGGOD_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_SHOPPING_GOD_GiveUserChip Fail - UserIDIndex:10752790, btMissionType:0, iResult:7106560", m_pUser->GetUserIDIndex(), btMissionType, iResult);

return FALSE;
}

BOOL CFSGameUserShoppingGodEvent::UseChip(SC2S_SHOPPING_GOD_USE_CHIP_REQ& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	SS2C_SHOPPING_GOD_USE_CHIP_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SHOPPING_GOD_USE_CHIP_RES));

	int iChoiceSumPrice = 0;

	if(CLOSED == SHOPPINGGOD.IsEventOpen())
	{
		ss.btResult = SHOPPING_GOD_USE_CHIP_CLOSED_EVENT;
	}
	else if(FALSE == SHOPPINGGOD.GetChoiceSumPrice(rs.iChoiceProductIndex, iChoiceSumPrice))
	{
		ss.btResult = SHOPPING_GOD_USE_CHIP_CHOICE_COUNT_FAIL;
	}
	else if(m_sUserInfo.iCurrentChip < iChoiceSumPrice)
	{
		ss.btResult = SHOPPING_GOD_USE_CHIP_LACK_CHIP_FAIL;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
	{
		ss.btResult = SHOPPING_GOD_USE_CHIP_FULL_MAILBOX_FAIL;
	}
	else
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		ss.btResult = SHOPPING_GOD_USE_CHIP_GIVE_REWARD_FAIL;

		int iRandom = rand() % SHOPPING_GOD_PRODUCT_CHOICE_COUNT;
		ss.iResultProductIndex = rs.iChoiceProductIndex[iRandom];

		int iReturn = pODBC->EVENT_SHOPPING_GOD_UseChip(m_pUser->GetUserIDIndex(), rs.iChoiceProductIndex, ss.iResultProductIndex, iChoiceSumPrice);
		if(ODBC_RETURN_SUCCESS == iReturn)
		{
			m_sUserInfo.iCurrentChip -= iChoiceSumPrice;

			int iRewardIndex = SHOPPINGGOD.GetRewardIndex(ss.iResultProductIndex);

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(NULL != pReward)
			{
				ss.btResult = SHOPPING_GOD_USE_CHIP_SUCCESS;
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_SHOPPINGGOD_EVENT, LA_DEFAULT, NONE, "REWARDMANAGER.GiveReward, UserIDIndex:%d, RewardIndex:%d"
					, m_pUser->GetUserIDIndex(), iRewardIndex);
			}
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_SHOPPINGGOD_EVENT, LA_DEFAULT, NONE, "EVENT_SHOPPING_GOD_UseChip Fail, UserIDIndex:%d, iReturn:%d"
				, m_pUser->GetUserIDIndex(), iReturn);
		}
	}

	m_pUser->Send(S2C_SHOPPING_GOD_USE_CHIP_RES, &ss, sizeof(SS2C_SHOPPING_GOD_USE_CHIP_RES));

	return TRUE;
}
