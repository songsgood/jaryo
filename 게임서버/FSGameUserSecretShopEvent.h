qpragma once

qinclude "FSEventODBC.h"

class CFSGameUser;
class CFSODBCBase;

typedef map<int/*_iSlotIndex*/,SUserSecretShopItem> EVENT_USER_SECRET_SHOP_ITEM_MAP;

class CFSGameUserSecretShopEvent
{
public:
	CFSGameUserSecretShopEvent(CFSGameUser* pUser);
	~CFSGameUserSecretShopEvent(void);

	BOOL	Load();
	BOOL	CheckReset(BOOL bPlayGame = FALSE);
	
	void	MakeShopList();
	void	UpdateSlotStatus();
	void	SendShopList();
	RESULT_EVENT_SECRET_SHOP_BUY_ITEM BuyItemReq(int iSlotIndex);
	void	MakeEventOpen(CPacketComposer& Packet, int& iEventCount);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	EVENT_USER_SECRET_SHOP_ITEM_MAP m_mapUserItem;
	time_t				m_tResetTime;
	BOOL				m_bPlayGame;
};

