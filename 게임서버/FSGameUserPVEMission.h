qpragma once

qinclude "PVECommon.h"

class CFSGameUser;
class CFSODBCBase;

enum PVE_SPECIAL_AVATAR_PIECE_UPDATETYPE
{
	PVE_SPECIAL_AVATAR_PIECE_UPDATETYPE_GET,
	PVE_SPECIAL_AVATAR_PIECE_UPDATETYPE_USE,
};

enum PVE_SPECIAL_AVATAR_PIECE_LOGTYPE
{
	PVE_SPECIAL_AVATAR_PIECE_LOGTYPE_GET_PIECE,
	PVE_SPECIAL_AVATAR_PIECE_LOGTYPE_EXCHANGE_PIECE,
	PVE_SPECIAL_AVATAR_PIECE_LOGTYPE_USE_PIECE,
};

#define PVE_RANDOM_SPECIAL_AVATAR_PIECE_ITEMCODE	52114751

class CFSGameUserPVEMission
{
public:
	CFSGameUserPVEMission(CFSGameUser* pUser);
	~CFSGameUserPVEMission(void);

	BOOL Load();
	void SetMission(vector<SPVEUserMission>& vPVEMission);
	void SetTotalPoint();
	void SetPiece(vector<SPVEUserPiece>& vUserPiece);
	
	BYTE GetPVEType(BYTE btMatchType);

	void SendMissionInfo();
	void SendMissionList(BYTE btPVEType);
	RESULT_PVE_MISSION_GET_REWARD GetRewardReq(int iMissionIndex, SREWARD_INFO& sReward);
	RESULT_PVE_DAILY_MISSION_GET_REWARD GetDailyRewardReq(SREWARD_INFO& sReward);
	BYTE GiveRandomSpecialAvatarPieceType(int iPieceCount);
	void CheckAndUpdateMission(BYTE btMatchType, BYTE btPVEEOD, BYTE btMissionType, int iValue1, int iValue2, int iValue3);
	void CheckAndUpdateMission(BYTE btMatchType, BYTE btPVEEOD, BYTE btMissionType, int iAddValue = 1);
	BOOL ResetDailyMisson();
	void CheckAndUpdateDailyMission(BYTE btMatchType, BYTE btPVEEOD, BYTE btMissionType, int iAddValue = 1);
	void SendAvatarPieceInfo();
	RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE ExchangePieceReq(SC2S_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_REQ& rq);
	RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD GetRewardPiece(BYTE btPieceType, SREWARD_INFO& sReward);
	void SendRankList(BYTE btPVEType);
	void MakeLastMissionList(BYTE btPVEType, SS2C_PVE_RANK_USER_INFO_RES& rs);
	void UpdateRankGameID(int iGameIDIndex, char* szGameID);

	int GetRankGameIDIndex() { return m_iRankGameIDIndex; }
	char* GetRankGameID() { return m_szRankGameID; }

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	PVE_USER_MISSION_MAP m_mapUserMission;
	PVE_USER_MISSION_LIST m_listMission;

	SPVEUserDailyMission m_UserDailyMission;
	SPVEUserPiece m_UserPiece[MAX_PVE_PIECE_TYPE_COUNT];

	int m_iTotalPoint;
	int m_iPoint[MAX_PVE_TYPE_COUNT];
	int m_iaCompletedMissionCount[MAX_PVE_TYPE_COUNT];

	int m_iRankGameIDIndex;
	char m_szRankGameID[MAX_GAMEID_LENGTH+1];
};

