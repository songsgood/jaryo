qpragma once

class CFSGameUser;
class CFSODBCBase;

enum GAMERESULT_NOTICE_STATUS
{
	GAMERESULT_NOTICE_STATUS_NONE,
	GAMERESULT_NOTICE_STATUS_ADDPOINT,
	GAMERESULT_NOTICE_STATUS_IMPOSSIBLE_GET_REWARD,
	GAMERESULT_NOTICE_STATUS_NOT_NORMAL_GAME,
};

struct CGamePlayUserInfo
{
	int _iPoint;
	int _iClearCount;

	CGamePlayUserInfo()
	{
		ZeroMemory(this, sizeof(CGamePlayUserInfo));
	}
};

class CFSGameUserGamePlayEvent
{
public:
	CFSGameUserGamePlayEvent(CFSGameUser* pUser);
	~CFSGameUserGamePlayEvent(void);

	BOOL				Load();

	BOOL				MakeGamePlayEventInfo(CPacketComposer& Packet);
	void				GetRewardReq();
	void				UpdateUserPoint(int iPoint, int& iAddPoint);
	BYTE				GetGameResultNoticeStatus(BYTE btMatchType, BOOL bIsDisconnectedMatch, BOOL bRunAway);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	CGamePlayUserInfo	m_UserEventInfo;
};

