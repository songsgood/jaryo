qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CenterSvrProxy.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CFSGameThread::Process_PaperInfoReq(CFSGameClient *pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	// C2S_PAPER_INFO_REQ	
	int	index = 0;
	m_ReceivePacketBuffer.Read(&index);		// 쪽지 번호
	if ( index < 0 )
		return;

	pUser->SendPaperInfo(index);
}


void CFSGameThread::Process_SendPaper(CFSGameClient *pFSClient)	
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	if(!pUser) return;
	
	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if(pAvatarInfo == NULL)
	{
		return;
	}
	
	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if(NULL == pServer)
	{
		return;
	}

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );
	
	// C2S_SEND_PAPER				
	DECLARE_INIT_TCHAR_ARRAY(recvID, MAX_GAMEID_LENGTH+1);	// 받는사람 ID	
	char 	title[MAX_PAPER_TITLE_LENGTH+1];
	short 	textLen;
	char 	text[257]; 
	BYTE	BannedUser;
	m_ReceivePacketBuffer.Read(&BannedUser);
	
	m_ReceivePacketBuffer.Read((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
	recvID[MAX_GAMEID_LENGTH] = '\0';	FSStrTrim(recvID);	
	m_ReceivePacketBuffer.Read((BYTE*)title, MAX_PAPER_TITLE_LENGTH+1);	
	title[MAX_PAPER_TITLE_LENGTH] = '\0';
	m_ReceivePacketBuffer.Read(&textLen);	
	if(textLen <= 0 || 256 < textLen) return;
	m_ReceivePacketBuffer.Read((BYTE*)text, textLen);					text[textLen] = '\0';
	
	if(strlen(recvID) < 1 || strlen(title) < 1) return;
	
	if(!strcmp(recvID, pUser->GetGameID())) return ;	//아이디 같으면 취소
	
	BOOL bGetOption = FALSE;
	int opt = 0;
	
	int iGameIDIndex = -1;
	int iResult = -1;
	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->spGetGameIDIndex( recvID , iGameIDIndex, iResult))
	{	
		CPacketComposer PacketComposer(S2C_SEND_PAPER);
		PacketComposer.Add((BYTE)1);
		PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
		pFSClient->Send(&PacketComposer);
		return;
	}
	
	if( iResult == -1 )
	{
		CPacketComposer PacketComposer(S2C_SEND_PAPER);
		PacketComposer.Add((BYTE)1);
		PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
		pFSClient->Send(&PacketComposer);
		return;
	}
	
	if(ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->GetGameOption(iGameIDIndex, opt))
		bGetOption = TRUE;
	
	int iConcernStatus = -1;
	BEGIN_LOCK
		CScopedLockFriend fr(pUser->GetFriendList(), recvID);
		if (fr != NULL) iConcernStatus = fr->GetConcernStatus();
	END_LOCK;

	if((BannedUser != 1) && (iConcernStatus != FRIEND_ST_FRIEND_USER) )	// 추방당한 유저라면 추방 당한 쪽지를 무조건 받는다 
	{
		
		if(bGetOption == TRUE)
		{
			if(0 == (opt & 0x00000004)) // (0x0001 << OPTION_TYPE_MAIL+1)	//^^^
			{
				CPacketComposer PacketComposer(S2C_SEND_PAPER);
				PacketComposer.Add((BYTE)3);
				PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
				pFSClient->Send(&PacketComposer);
				return;
			}
		}
		else
		{
			CPacketComposer PacketComposer(S2C_SEND_PAPER);
			PacketComposer.Add((BYTE)1);
			PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
			pFSClient->Send(&PacketComposer);
		}
	}

	//렷봤堂笭係묘콘된섬털뙤
	if( iConcernStatus != FRIEND_ST_FRIEND_USER && pUser->GetCurUsedAvtarLv() < 5 )
	{
		CPacketComposer PacketComposer(S2C_SEND_PAPER);
		PacketComposer.Add((BYTE)4);
		PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
		pFSClient->Send(&PacketComposer);
		return;
	}

	int index = 100;		
	
	int iFlag = 0;
	if(0 == (opt & 0x00000004))	 
		iFlag = 1;
	
	int ret = ((CFSGameODBC *)pNexusODBC)->spAddPaper(iGameIDIndex, index, pAvatarInfo->iGameIDIndex, title, text, iFlag);
	BYTE errCode = ret;
	
	switch(ret)
	{
	case 0:		//	성공   	    0
		{	
			CScopedRefGameUser recver(recvID);
			if(recver == NULL) 
			{
				
				CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
				if(pCenter) pCenter->SendUserNotify(recvID, index, MAIL_NOTE);				
			}
			else
			{
				recver->RecvPaper(index);
			}		
		}
		break;
	case 3: 
		{
			if( BannedUser != 1)
			{			
				CPacketComposer PacketComposer(S2C_SEND_PAPER);
				PacketComposer.Add((BYTE)3);
				PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
				pFSClient->Send(&PacketComposer);
				return;
			}
		}
		break;
	case 1:		//	없는 유저     1
	case 2:		//	쪽지함 꽉참   2
		{
			CPacketComposer PacketComposer(S2C_SEND_PAPER);
			PacketComposer.Add((BYTE)2);
			PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
			pFSClient->Send(&PacketComposer);
			return;
		}
	default:	//	DB에러. 굳이 유저에게 알릴 필요가 있을까요...
		break;	
	}
	
	
	if(BannedUser != 1)	
	{
		CPacketComposer PacketComposer(S2C_SEND_PAPER);
		PacketComposer.Add(errCode);
		PacketComposer.Add((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
		pFSClient->Send(&PacketComposer);
	}
}

void CFSGameThread::Process_DelPaperOrPresent(CFSGameClient *pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	if(!pUser) return;

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC( ODBC_CLUB );
	CHECK_NULL_POINTER_VOID( pClubBaseODBC );
	
	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if(pAvatarInfo == NULL)
	{
		return;
	}
	
	BYTE	btMailType;
	BYTE	indexCnt;
	int 	index;
	
	m_ReceivePacketBuffer.Read(&btMailType);
	m_ReceivePacketBuffer.Read(&indexCnt);
	
	CPacketComposer PacketComposer(S2C_DEL_PAPER_RES);
	PacketComposer.Add(btMailType);
	PacketComposer.Add(indexCnt);
	
	for(int i = 0 ; i < indexCnt ; i++)
	{
		m_ReceivePacketBuffer.Read(&index);
		
		if(MAIL_NOTE == btMailType)	// paper
		{
			int ret = ((CFSGameODBC *)pNexusODBC)->spDelPaper(pAvatarInfo->iGameIDIndex, index);
			
			if(ret == ODBC_RETURN_SUCCESS)
			{
				SPaper* paper = pUser->GetPaperList()->Remove(index);
				if(paper) delete paper;
			}
		}
		else if (MAIL_PRESENT_ON_AVATAR == btMailType || MAIL_PRESENT_ON_ACCOUNT == btMailType)	
		{			
			int ret = ((CFSGameODBC *)pNexusODBC)->spDelPresent(pUser->GetUserIDIndex(), pAvatarInfo->iGameIDIndex, btMailType, index);
			if(ret == ODBC_RETURN_SUCCESS)
			{
				SPresent* present = pUser->GetPresentList(btMailType)->Remove(index);
				if(present) delete present;//??? error 시 처리
			}
		}
		else if (MAIL_CLUB_INVITE == btMailType)
		{
			int iResult = CLUB_REMOVE_INVITE_PAPER_RESULT_FAIL;
			if ( ODBC_RETURN_SUCCESS == pClubBaseODBC->SP_Club_DeleteInvitation( pAvatarInfo->iGameIDIndex, index, iResult ) )
			{
				if ( CLUB_REMOVE_INVITE_PAPER_RESULT_SUCCESS <= iResult )
				{
					SClubInvitationData* pClubInvitationDate = pUser->GetClubInvitationList()->Remove( index );
					if ( NULL != pClubInvitationDate )
					{
						delete pClubInvitationDate;
					}
				}
			}
		}
		
		PacketComposer.Add(index);
	}
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_PresentListReq(CFSGameClient *pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iMailType;
	m_ReceivePacketBuffer.Read(&iMailType);

	pUser->SendPresentList(iMailType);
}

void CFSGameThread::Process_PresentInfoReq(CFSGameClient *pFSClient)	
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	// C2S_PRESENT_INFO_REQ	
	BYTE btMailType;
	int	index = 0;
	m_ReceivePacketBuffer.Read(&btMailType);
	m_ReceivePacketBuffer.Read(&index);		// 쪽지 번호
	if ( index < 0 )
		return;
	
	pUser->SendPresentInfo(btMailType, index);
}

void CFSGameThread::Process_AcceptPresent(CFSGameClient *pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	BYTE btMailType;
	int iIndexCnt;
	int iIndex[MAX_MAIL_SLOT_COUNT_PER_PAGE];
	memset(iIndex, -1, sizeof(int)*MAX_MAIL_SLOT_COUNT_PER_PAGE);

	BYTE btPopUpIndex = 0;
	m_ReceivePacketBuffer.Read(&btMailType);
	m_ReceivePacketBuffer.Read(&iIndexCnt);
	for(int i = 0; i < iIndexCnt; ++i)
	{
		m_ReceivePacketBuffer.Read(&iIndex[i]);	
		if(iIndex[i] < 0)
			return;
	}	
	m_ReceivePacketBuffer.Read(&btPopUpIndex);	// 초기값 0, 스킬슬롯 받을 때 추가팝업이라면 1

	CPacketComposer PacketComposer(S2C_ACCEPT_PRESENT_RESULT);
	int iErrorCode = 0;
	int iSuccessCnt = 0;

	if(iIndexCnt <= 1)
	{
		if( TRUE == pUser->IsChoosePropertyItem(btMailType, iIndex[0]))
		{
			pUser->SendChoosePropertyItemInfoPresent(btMailType, iIndex[0]);
		}
		else
		{
			pUser->AcceptPresent(btMailType, iIndexCnt, iIndex[0], btPopUpIndex, iErrorCode);
			iSuccessCnt++;

			CHECK_CONDITION_RETURN_VOID(ACCEPT_PRESENT_CHARACTER_SLOT == iErrorCode);

			PacketComposer.Add(&iErrorCode);
			PacketComposer.Add(btMailType);
			PacketComposer.Add(iSuccessCnt);
			PacketComposer.Add(iIndex[0]);

			pUser->Send(&PacketComposer);
		}
	}
	else
	{
		bool bCheerLeader = false;
		for(int i = 0; i < iIndexCnt; ++i)
		{
			if( TRUE == pUser->IsChoosePropertyItem(btMailType, iIndex[i]))
			{
				iIndex[i] = -1;
				continue;
			}

			if( TRUE == pUser->IsCharacterSlot(btMailType, iIndex[i]))
			{
				iIndex[i] = -1;
				continue;
			}

			if(TRUE == pUser->AcceptPresent(btMailType, iIndexCnt, iIndex[i], btPopUpIndex, iErrorCode))
			{
				iSuccessCnt++;

				if(iErrorCode == ACCEPT_PRESENT_CHEERLEADER)
				{
					bCheerLeader = true;
				}
			}
			else
			{
				iIndex[i] = -1;
			}
		}

		if(bCheerLeader)			iErrorCode = ACCEPT_PRESENT_CHEERLEADER;
		else if(0 < iSuccessCnt)	iErrorCode = 0;

		PacketComposer.Add(&iErrorCode);
		PacketComposer.Add(btMailType);
		PacketComposer.Add(iSuccessCnt);
		for(int i = 0; i < MAX_MAIL_SLOT_COUNT_PER_PAGE; ++i)
		{
			if(iIndex[i] != -1)
			{
				PacketComposer.Add(iIndex[i]);
			}
		}
		pUser->Send(&PacketComposer);
	}	
}

void CFSGameThread::Process_GetChoosePropertyItemReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	BYTE btMailType;
	int iPresentIndex = -1, iItemCode = -1;
	int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT];
	for( int i = 0 ; i < MAX_ITEM_PROPERTYINDEX_COUNT ; ++i )
	{
		aiPropertyIndex[i] = -1;
	}

	m_ReceivePacketBuffer.Read(&btMailType);
	m_ReceivePacketBuffer.Read(&iPresentIndex);
	m_ReceivePacketBuffer.Read(&iItemCode);

	BOOL bIsDuplicate = FALSE;
	set<int> setPropertyIndex;

	for (int j = 0 ; j < MAX_ITEM_PROPERTYINDEX_COUNT ; j++)
	{
		m_ReceivePacketBuffer.Read(&aiPropertyIndex[j]);

		if(aiPropertyIndex[j] > 0) 
		{
			if(setPropertyIndex.end() != setPropertyIndex.find(aiPropertyIndex[j]))
				bIsDuplicate = TRUE; // 능력치 중복됨
			else
				setPropertyIndex.insert(aiPropertyIndex[j]);
		}
	}

	int iErrorCode = GET_PROPERTY_SELECT_ITEM_NOT_EXIST_ITEM;
	if(bIsDuplicate)
	{
		iErrorCode = GET_PROPERTY_SELECT_ITEM_DUPLICATE_PROPERTY;
	}
	else if(TRUE == pUser->GetChoosePropertyItemFromPresent(btMailType, iPresentIndex, iItemCode, aiPropertyIndex, iErrorCode))
	{		
		Process_FSExposeUserItem(pFSClient);
	}

	CPacketComposer PacketComposer(S2C_ACCEPT_PRESENT_RESULT);
	if(GET_PROPERTY_SELECT_ITEM_SUCESS_USE_LVUPITEM == iErrorCode)
		PacketComposer.Initialize(S2C_GET_CHOOSE_PROPERTY_ITEM_RES);

	PacketComposer.Add(&iErrorCode);
	PacketComposer.Add(btMailType);

	int iSuccessCnt = 1;
	PacketComposer.Add(iSuccessCnt);
	PacketComposer.Add(iPresentIndex);

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_FSSendGiftGameID(CFSGameClient* pFSClient)
{
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC )

	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);	
	DECLARE_INIT_TCHAR_ARRAY(szTargetGameID, MAX_GAMEID_LENGTH+1);
	
	m_ReceivePacketBuffer.Read((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);
	szGameID[MAX_GAMEID_LENGTH] = 0;
	
	strncpy_s(szTargetGameID, _countof(szTargetGameID), szGameID, MAX_GAMEID_LENGTH);
	szTargetGameID[MAX_GAMEID_LENGTH] = 0;
	
	_strupr_s(szTargetGameID, _countof(szTargetGameID));
	
	int iLv = 1;
	int iSex = 0;
	int iPosition = -1;
	int iResult = -1;
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if( NULL == pUser ) 
	{
		return;
	}

	if( pUser->GetUserType() LIENT_PLUS_TYPE_NEW != CLIENT_ACCOUNT_TYPE_NORMAL )
)
	{
		iResult = -4;
		CPacketComposer PacketError(S2C_SEND_GIFT_GAME_ID_RES);
		PacketError.Add(&iResult);
		PacketError.Add((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);	
		
		pFSClient->Send(&PacketError);
		return;
	}
	
	DECLARE_INIT_TCHAR_ARRAY(szSelfGameID, MAX_GAMEID_LENGTH+1);	
	
	strncpy_s(szSelfGameID, _countof(szSelfGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
	szSelfGameID[MAX_GAMEID_LENGTH] = 0;	
	_strupr_s(szSelfGameID, _countof(szSelfGameID));
	
	if ( strcmp(szTargetGameID, szSelfGameID ) == 0 )
	{
		iResult = -5;
		CPacketComposer PacketError(S2C_SEND_GIFT_GAME_ID_RES);
		PacketError.Add(&iResult);
		PacketError.Add((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);	
		
		pFSClient->Send(&PacketError);
		return;
	}
	
	int iSpecialCharacterIndex = -1;
	int iGameIDIndex = -1;
	int iGameIDResult = -1;
	int iCharacterType = -1;

	iResult = 0;

	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->spGetGameIDIndex( szGameID , iGameIDIndex, iGameIDResult))
	{
		iResult = -1;
		CPacketComposer PacketError(S2C_SEND_GIFT_GAME_ID_RES);
		PacketError.Add(&iResult);
		PacketError.Add((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);	
		
		pFSClient->Send(&PacketError);
		return;
	}
	
	if( iGameIDResult == -1 )
	{
		iResult = -1;
		CPacketComposer PacketError(S2C_SEND_GIFT_GAME_ID_RES);
		PacketError.Add(&iResult);
		PacketError.Add((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);	
		
		pFSClient->Send(&PacketError);
		return;
	}
	
	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->spGetAvatarSexLv( iGameIDIndex , iLv, iSex, iPosition, iSpecialCharacterIndex, iCharacterType) )
	{
		iResult = -1;
		CPacketComposer PacketError(S2C_SEND_GIFT_GAME_ID_RES);
		PacketError.Add(&iResult);
		PacketError.Add((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);	
		
		pFSClient->Send(&PacketError);
		return;
	}
	
	CPacketComposer PacketComposer(S2C_SEND_GIFT_GAME_ID_RES);
	PacketComposer.Add(&iResult);
	PacketComposer.Add((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);	
	PacketComposer.Add(&iLv);
	
	pFSClient->Send(&PacketComposer);
}
