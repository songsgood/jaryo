qinclude "stdafx.h"
qinclude "GMSvrProxy.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameServer.h"

CGMSvrProxy::CGMSvrProxy(void)
{
	SetState(FS_GM_SERVER_PROXY);
}


CGMSvrProxy::~CGMSvrProxy(void)
{
}

void CGMSvrProxy::SendFirstPacket()
{
	CPacketComposer PacketComposer(G2S_CLIENT_INFO);
	SG2S_CLIENT_INFO info;

	info.ProcessID = CFSGameServer::GetInstance()->GetProcessID();	
	info.iServerType = SERVER_TYPE_NORMAL;
	strncpy_s(info.ProcessName, _countof(info.ProcessName), CFSGameServer::GetInstance()->GetProcessInfo()->szServerName, MAX_PROCESS_NAME_LENGTH);

	PacketComposer.Add((BYTE*)&info, sizeof(SG2S_CLIENT_INFO));

	Send(&PacketComposer);
}

void CGMSvrProxy::ProcessPacket( CReceivePacketBuffer* recvPacket )
{
// 	switch(recvPacket->GetCommand())
// 	{
// 
// 	}
}
