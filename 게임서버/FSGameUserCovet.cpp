qinclude "stdafx.h"
qinclude "FSGameUserCovet.h"
qinclude "ThreadODBCManager.h"
qinclude "CFSGameUser.h"
qinclude "CovetEventManager.h"
qinclude "EventDateManager.h"
qinclude "CenterSvrProxy.h"
qinclude "GameProxyManager.h"

CFSGameUserCovet::CFSGameUserCovet(CFSGameUser* pUser)
	:m_pUser(pUser)
	,m_bDBSave(FALSE)
	,m_bDBSave_RewardList(FALSE)
	,m_btIsFirstTime(FALSE)
{
}


CFSGameUserCovet::~CFSGameUserCovet(void)
{
}

BOOL CFSGameUserCovet::LoadCovet()
{
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET), TRUE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	m_sUserInfo.iUserIDIndex =  m_pUser->GetUserIDIndex();
	m_btIsFirstTime = FALSE;
	if(ODBC_RETURN_SUCCESS != pODBCBase->EVENT_COVET_GetUserInfo(m_sUserInfo, m_btIsFirstTime))
	{
		return FALSE;
	}

	for(int i = 0 ; i < MAX_EVENT_COVET_REWARD_GRADE ; ++ i)
		m_listSelectReward[i].clear();
	
	COVET.ConvertKeyToList(m_listSelectReward, m_sUserInfo.iRewardKey);

	return TRUE;
}

void CFSGameUserCovet::SendCovetUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET));

	SS2C_EVENT_COVET_INFO_RES info;

	info.iCurrentGauge = m_sUserInfo.iGauge;
	info.iMaxGauge = COVET.GetCovetConfigValue(EVENTCOVET_CONFIG_MaxGauge);
	info.iFullSetItemCode = COVET.GetCovetConfigValue(EVENTCOVET_CONFIG_MainItemCode);
	info.iSpecialStageCount = m_sUserInfo.iSpecialStageCount;
	info.btSpecialStage = COVET.CheckSpecialStage(m_sUserInfo.iSpecialStageCount);
	info.iSummonStone = m_sUserInfo.iSummonStone;
	info.iJinnCount = m_sUserInfo.iJinnCount;
	info.btStep = (BYTE)m_sUserInfo.iStep;
	if(0 == m_sUserInfo.iStep) m_sUserInfo.iStep = 1;

	info.btIsFirstTime = m_btIsFirstTime;

	const int iMaxJinnCount = COVET.GetCovetConfigValue(EVENTCOVET_CONFIG_MaxJinnCount);
	if(info.iJinnCount > iMaxJinnCount)
		info.iJinnCount = iMaxJinnCount;
	
	if(TRUE == info.btSpecialStage)
	{
		CHECK_CONDITION_RETURN_VOID(FALSE == COVET.GetSpecialStageRewardInfo(info.sRewardInfo));
	}
	else
	{
		CHECK_CONDITION_RETURN_VOID(FALSE == COVET.GetSelectRewardInfo(m_listSelectReward, info.sRewardInfo));
	}

	CPacketComposer Packet(S2C_EVENT_COVET_INFO_RES);
	Packet.Add((PBYTE)&info,sizeof(SS2C_EVENT_COVET_INFO_RES));

	m_pUser->Send(&Packet);

	if(m_btIsFirstTime) m_btIsFirstTime = FALSE;
}

void CFSGameUserCovet::SendCovetItemList()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET));

	CPacketComposer Packet(S2C_EVENT_COVET_ITEM_LIST_RES);
	
	SS2C_EVENT_COVET_ITEM_LIST_RES info;
	info.iItemCount = COVET.GetCovetItemSize();
	
	Packet.Add((PBYTE)&info,sizeof(SS2C_EVENT_COVET_ITEM_LIST_RES));
	COVET.MakePacketCovetItemList(Packet);

	m_pUser->Send(&Packet);
}

void CFSGameUserCovet::SendCovetItemSelect( const SC2S_EVENT_COVET_ITEM_SELECT_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET));

	SS2C_EVENT_COVET_ITEM_SELECT_RES info;
	BYTE btSpecialStage = COVET.CheckSpecialStage(m_sUserInfo.iSpecialStageCount);

	if( TRUE == btSpecialStage)
	{
		info.btResult = COVET.CheckSpecialStageRewardList(rq);
	}
	else
	{
		info.btResult = COVET.GetSelectRewardList(rq ,m_listSelectReward);
	}
	
	CPacketComposer Packet(S2C_EVENT_COVET_ITEM_SELECT_RES);
	Packet.Add((PBYTE)&info,sizeof(SS2C_EVENT_COVET_ITEM_SELECT_RES));

	if(FALSE == btSpecialStage && RESULT_EVENT_COVET_ITEM_SELECT_SUCCESS == info.btResult)
	{
		m_bDBSave_RewardList = TRUE;
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserCovet::SendCovetShop()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET));

	SS2C_EVENT_COVET_SHOP_RES info;
	info.iEnergy = m_sUserInfo.iEnergy;
	info.iItemCount = COVET.GetCovetShopSize();
	
	CPacketComposer Packet(S2C_EVENT_COVET_SHOP_RES);
	Packet.Add((PBYTE)&info,sizeof(SS2C_EVENT_COVET_SHOP_RES));
	COVET.MakePacketCovetShop(Packet);

	m_pUser->Send(&Packet);
}

void CFSGameUserCovet::BuyCovetShopItem( const SC2S_EVENT_COVET_SHOP_BUY_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET));
	CHECK_CONDITION_RETURN_VOID(FALSE == COVET.IsValidCovetShopIndex(rq.iIndex));

	int iPrice = COVET.GetShopPrice(rq.iIndex);
	int iMailClearance = MAX_PRESENT_LIST - m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();

	SS2C_EVENT_COVET_SHOP_BUY_RES rs;
	rs.iEnergy = m_sUserInfo.iEnergy;
	rs.btResult = RESULT_EVENT_COVET_SHOP_BUY_FAIL;
	if(iPrice > m_sUserInfo.iEnergy)
		rs.btResult = RESULT_EVENT_COVET_SHOP_BUY_SHORTAGE_FAIL;
	else if(iMailClearance < 1)
		rs.btResult = RESULT_EVENT_COVET_SHOP_BUY_FULL_MAILBOX_FAIL;
	else
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);
		SEVENTCOVET_EnergyShopBuy buy;

		buy.iConfigPriceIndex = COVET.GetShopIndex(rq.iIndex);
		buy.iGameIDIndex = m_pUser->GetGameIDIndex();
		buy.iPresentIndex = -1;
		buy.iResultEnergy = -1;
		buy.iRewardIndex = COVET.GetShopRewardIndex(buy.iConfigPriceIndex);
		buy.iUserIDIndex = m_pUser->GetUserIDIndex();

		if(ODBC_RETURN_SUCCESS != pODBCBase->EVENT_COVET_EnergyShopBuy(buy))
		{
			WRITE_LOG_NEW(LOG_TYPE_COVET_EVENT, CALL_SP, FAIL
				, "EVENT_COVET_EnergyShopBuy / GameIDIndex(10752790), RewardIndex(0)"
 buy.iGameIDIndex, buy.iRewardIndex);
		}
		else
		{
			m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, buy.iPresentIndex);

			rs.iEnergy = m_sUserInfo.iEnergy = buy.iResultEnergy;
			rs.btResult = RESULT_EVENT_COVET_SHOP_BUY_SUCCESS;
		}
	}

	CPacketComposer Packet(S2C_EVENT_COVET_SHOP_BUY_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_COVET_SHOP_BUY_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserCovet::UseCovet( const SC2S_EVENT_COVET_USE_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET));
	CHECK_CONDITION_RETURN_VOID(rq.btUseType != EVENT_COVET_USE_TYPE_1 && rq.btUseType != EVENT_COVET_USE_TYPE_10);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	SS2C_EVENT_COVET_USE_RES rs;
	list<EVENT_COVET_REWARD_INFO> listRewardInfo;

	rs.btResult = RESULT_EVENT_COVET_USE_FAIL;
	rs.btSpecialStage = COVET.CheckSpecialStage(m_sUserInfo.iSpecialStageCount);
	rs.iCurrentGauge = m_sUserInfo.iGauge;
	rs.iJinnCount = m_sUserInfo.iJinnCount;
	const int iMaxJinnCount = COVET.GetCovetConfigValue(EVENTCOVET_CONFIG_MaxJinnCount);
	if(rs.iJinnCount > iMaxJinnCount )
		rs.iJinnCount = iMaxJinnCount;

	rs.iRewardCount = 0;
	rs.iSpecialStageCount = m_sUserInfo.iSpecialStageCount;
	rs.iSummonStone = m_sUserInfo.iSummonStone;
	rs.btStep = (BYTE)m_sUserInfo.iStep;

	int iPrice = COVET.GetStonePrice(rq.btUseType);
	int iGiveMailCount = rq.btUseType == EVENT_COVET_USE_TYPE_1 ? 1 : TRUE == rs.btSpecialStage ? 1 : 10;
	int iMailClearance = MAX_PRESENT_LIST - m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();

	if(FALSE == rs.btSpecialStage && iPrice > m_sUserInfo.iSummonStone)
		rs.btResult = RESULT_EVENT_COVET_USE_SHORTAGE_FAIL;
	else if(iMailClearance < iGiveMailCount)
		rs.btResult = RESULT_EVENT_COVET_USE_FULL_MAILBOX_FAIL;
	else
	{
		SEVENTCOVET_Use use;
		use.iEnergy = -1;
		use.iGameIDIndex = m_pUser->GetGameIDIndex();
		use.iGauge = -1;
		use.iIsEffect = static_cast<int>(rq.btEffect);
		use.iPrevStone = m_sUserInfo.iSummonStone;
		use.iPostStone = use.iPrevStone - iPrice;
		use.iResultCount = -1;
		use.iSpecialStageCount = -1;
		use.iStoneCount = m_sUserInfo.iSummonStone;
		use.iUserIDIndex = m_pUser->GetUserIDIndex();
		use.iUseType = static_cast<int>(rq.btUseType);
		use.btInitGauge = 0;
		use.btSpecialStage = COVET.CheckSpecialStage(m_sUserInfo.iSpecialStageCount);
		use.iSummonOneCount = m_sUserInfo.iSummonOneCount;

		if(TRUE == use.btSpecialStage)
			use.iStep = m_sUserInfo.iStep;
		else
			use.iStep = COVET.CheckAddStep(use.iUseType,use.iSummonOneCount ,m_sUserInfo.iStep, m_sUserInfo.iGauge);
		
		use.i_OutSummonOneCount = 0;

		BOOL bGuaranteeGold = (BOOL)COVET.CheckMaxStep(use.iStep);
		int iGroup = use.iUseType == EVENT_COVET_USE_TYPE_1 ? EVENT_COVET_GROUP_1 : EVENT_COVET_GROUP_10;

		ZeroMemory(use.szRewardData,MAX_COVET_REWARD_LENGTH+1);
		BOOL bShout = FALSE;

		int iGetReward = FALSE;
		if(TRUE == use.btSpecialStage)
		{
			iGetReward = COVET.GetCovetSpecialStageReward(use.szRewardData, bShout, listRewardInfo);
		}
		else
		{
			iGetReward = COVET.GetCovetRandomReward(use.iUseType, use.iStep, use.szRewardData
				, m_listSelectReward,  listRewardInfo, bShout,iGroup,bGuaranteeGold);
		}

		if( TRUE == iGetReward )
		{
			int iResult = 0;
			list<int> listPresentIndex;

			if(bShout || bGuaranteeGold)
			{
				use.iStep = 1;
				use.btInitGauge = 1;
			}

			if(ODBC_RETURN_SUCCESS != (iResult = pODBCBase->EVENT_COVET_Use(use, listPresentIndex)))
			{
				WRITE_LOG_NEW(LOG_TYPE_COVET_EVENT, CALL_SP, FAIL
					, "EVENT_COVET_EnergyShopUse / GameIDIndex(10752790) Result(0)"
, use.iGameIDIndex, iResult);
			}
			else
			{
				m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);

				if(FALSE == use.btSpecialStage)
				{
					m_sUserInfo.iJinnCount -= iPrice;
				}

				m_sUserInfo.iEnergy = use.iEnergy;
				m_sUserInfo.iStep = use.iStep;
				m_sUserInfo.iSummonOneCount = use.i_OutSummonOneCount;

				rs.btResult = RESULT_EVENT_COVET_USE_SUCCESS;
				rs.iSummonStone = m_sUserInfo.iSummonStone = use.iResultCount;
				rs.iSpecialStageCount = m_sUserInfo.iSpecialStageCount = use.iSpecialStageCount;
				rs.iCurrentGauge = m_sUserInfo.iGauge = use.iGauge;
				rs.btSpecialStage = COVET.CheckSpecialStage(m_sUserInfo.iSpecialStageCount);
				rs.iJinnCount = m_sUserInfo.iJinnCount;
				const int iMaxJinnCount = COVET.GetCovetConfigValue(EVENTCOVET_CONFIG_MaxJinnCount);
				if(rs.iJinnCount > iMaxJinnCount)
					rs.iJinnCount = iMaxJinnCount;

				rs.iRewardCount = listRewardInfo.size();
				rs.btStep = (BYTE)m_sUserInfo.iStep;

				if(TRUE == bShout)
				{
					CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
					if(nullptr != pCenterSvrProxy)
					{
						SG2S_EVENT_SHOUT_REQ  info;
						info.iEventKind = EVENT_KIND_COVET;
						info.btContentsType = 0;
						strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), _countof(info.GameID)-1);
						pCenterSvrProxy->SendPacket(G2S_EVENT_SHOUT_REQ, &info, sizeof(SG2S_EVENT_SHOUT_REQ));
					}
				}
			}
		}
	}

	CPacketComposer Packet(S2C_EVENT_COVET_USE_RES);
	if(rs.iCurrentGauge == COVET.GetCovetConfigValue(EVENTCOVET_CONFIG_MaxGauge))
	{
		rs.btStep += 1; // client 보여주기용
	}
		
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_COVET_USE_RES));

	if(0 < rs.iRewardCount)
	{
		for(auto iter = listRewardInfo.begin() ; iter != listRewardInfo.end() ; ++iter )
			Packet.Add((PBYTE)&(*iter), sizeof(EVENT_COVET_REWARD_INFO));
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserCovet::AddCovetJinn()
{
	m_sUserInfo.iJinnCount += COVET.GetCovetConfigValue(EVENTCOVET_CONFIG_AddGamePlayReward);
	m_bDBSave = TRUE;
}

void CFSGameUserCovet::CheckAndSaveCovetJinn(CFSODBCBase* pODBC)
{
	if(TRUE == m_bDBSave || TRUE == m_bDBSave_RewardList)
	{
		if(nullptr == pODBC)
		{
			pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		}
		CHECK_NULL_POINTER_VOID(pODBC);

		SEVENTCOVET_UpdateJinnCnt jinn;
		jinn.iJinnCount = TRUE == m_bDBSave ? m_sUserInfo.iJinnCount : -1;
		jinn.iUserIDIndex = m_pUser->GetUserIDIndex();

		if(FALSE == m_bDBSave_RewardList)
			jinn.iRewardKey[0] = -1;
		else
		{
			int iIndex = 0;
			for(int i = EVENT_COVET_REWARD_GRADE_PURPLE ; i < MAX_EVENT_COVET_REWARD_GRADE ; ++i )
			{
				for(auto iter = m_listSelectReward[i].begin() ; 
					iter != m_listSelectReward[i].end() ; ++iter )
				{
					jinn.iRewardKey[iIndex++] = (*iter);
					
					if( MAX_COVET_SUMMON_COUNT == iIndex)
						break;
				}
			}
		}

		pODBC->EVENT_COVET_UpdateJinnCnt(jinn);
	}
}

BOOL CFSGameUserCovet::BuySummonStone_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET), FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, INVALED_DATA, CHECK_FAIL, "BuyHalfPriceItem - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	BOOL bResult = TRUE;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	SEVENTCOVET_Buy buy;
	buy.iGameIDIndex = m_pUser->GetGameIDIndex();
	buy.iUserIDIndex = m_pUser->GetUserIDIndex();
	buy.iBuyCount = pBillingInfo->iParam1;
	buy.iItemCode = pBillingInfo->iItemCode;
	buy.iPrevCash = pBillingInfo->iCurrentCash;
	buy.iPostCash = buy.iPrevCash - pBillingInfo->iCashChange;
	buy.iPrice	= pBillingInfo->iItemPrice;
	buy.iResultCount = 0;
	

	if(ODBC_RETURN_SUCCESS != pODBCBase->EVENT_COVET_Buy(buy))
	{
		WRITE_LOG_NEW(LOG_TYPE_COVET_EVENT, LA_DEFAULT, NONE, "EVENT_COVET_Buy Fail, User:10752790, GameID:(null)"
er->GetUserIDIndex(), m_pUser->GetGameID());
		bResult = FALSE;
	}
	else
	{
		m_sUserInfo.iSummonStone = buy.iResultCount;
		
		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(buy.iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);

	m_pUser->SetUserBillResultAtMem(pBillingInfo->iSellType, pBillingInfo->iCurrentCash-pBillingInfo->iCashChange, 0, 0,0);
	m_pUser->SendUserGold();

	return bResult;
}

void CFSGameUserCovet::SetSummonStoneCount( int iUpdateCount )
{
	m_sUserInfo.iSummonStone = iUpdateCount;
}
