qpragma once

enum RANDOM_ITEM_DRAW_FIRST_COIN_TYPE
{
	RANDOM_ITEM_DRAW_FIRST_COIN_CONNECT = 0,
	RANDOM_ITEM_DRAW_FIRST_COIN_PLAY	= 1,
	MAX_RANDOM_ITEM_DRAW_FIRST_COIN_TYPE,
};

enum RANDOM_ITEM_DRAW_FIRST_CONNECT_COIN_TYPE
{
	RANDOM_ITEM_DRAW_FIRST_CONNECT_NONE = 0,
	RANDOM_ITEM_DRAW_FIRST_CONNECT_GET_REWARD,
};

enum RANDOM_ITEM_DRAW_FIRST_GAMEPLAY_COIN_TYPE
{
	RANDOM_ITEM_DRAW_FIRST_GAME_NONE = 0,
	RANDOM_ITEM_DRAW_FIRST_GAME_PLAY_COMPLETE,
	RANDOM_ITEM_DRAW_FIRST_GAME_PLAY_GET_REWARD,
};

class CFSGameUser;
class CFSGameUserRandomItemDrawEvent
{
private:
	CFSGameUser* m_pUser;
	BYTE m_btGiveRandomItemDrawFirstConnectCoin;
	BYTE m_btGiveRandomItemDrawFirstGamePlayCoin;

public:
	BOOL Load(void);
	BOOL CheckAndUpdateGamePlay(void);
	BOOL CheckRandomDrawItemGamePlay(int& iUpdatedCoin);
	BOOL CheckRandomDrawItemConnect(int& iUpdatedCoin);

public:
	CFSGameUserRandomItemDrawEvent(CFSGameUser* pUser);
	~CFSGameUserRandomItemDrawEvent(void);
};
