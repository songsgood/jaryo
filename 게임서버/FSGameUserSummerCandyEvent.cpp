qinclude "stdafx.h"
qinclude "FSGameUserSummerCandyEvent.h"
qinclude "SummerCandyEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "UserHighFrequencyItem.h"

CFSGameUserSummerCandyEvent::CFSGameUserSummerCandyEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserSummerCandyEvent::~CFSGameUserSummerCandyEvent(void)
{
}

BOOL CFSGameUserSummerCandyEvent::Load()
{
	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_SUMMER_CANDY_GetUserInfo(m_pUser->GetUserIDIndex(), m_UserSummerCandyEvent._IBuyCandyCount, m_UserSummerCandyEvent._iDailyGamePlayCount, m_UserSummerCandyEvent._tLastGamePlayDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_SUMMERCANDY_EVENT, INITIALIZE_DATA, FAIL, "EVENT_SUMMER_CANDY_GetUserInfo error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserSummerCandyEvent::ResetPlayCount()
{
	time_t tCurrentTime = _time64(NULL);
	CHECK_CONDITION_RETURN_VOID(TRUE == TodayCheck(EVENT_RESET_HOUR, m_UserSummerCandyEvent._tLastGamePlayDate, tCurrentTime));

	m_UserSummerCandyEvent._iDailyGamePlayCount = 0;
	m_UserSummerCandyEvent._tLastGamePlayDate = tCurrentTime;
}

void CFSGameUserSummerCandyEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == SUMMERCANDYEVENT.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	ResetPlayCount();

	SS2C_EVENT_SUMMER_CANDY_USER_INFO_RES rs;
	rs.iHaveCandyCount = 0;

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY);
	if(NULL != pItem)
		rs.iHaveCandyCount = pItem->iPropertyTypeValue;

	rs.btCanBuyCandyUserCount = SUMMERCANDYEVENT.GetMaxBuyCandyCount() - m_UserSummerCandyEvent._IBuyCandyCount;
	if(rs.btCanBuyCandyUserCount < 0)
		rs.btCanBuyCandyUserCount = 0;

	rs.btPlayCount = m_UserSummerCandyEvent._iDailyGamePlayCount;

	m_pUser->Send(S2C_EVENT_SUMMER_CANDY_USER_INFO_RES, &rs, sizeof(SS2C_EVENT_SUMMER_CANDY_USER_INFO_RES));
}

void CFSGameUserSummerCandyEvent::SendUserCandyInfo()
{
	SS2C_EVENT_SUMMER_CANDY_UPDATE_CANDY_NOT not;
	not.iHaveCandyCount = 0;

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY);
	if(NULL != pItem)
		not.iHaveCandyCount = pItem->iPropertyTypeValue;

	not.btCanBuyCandyUserCount = SUMMERCANDYEVENT.GetMaxBuyCandyCount() - m_UserSummerCandyEvent._IBuyCandyCount;
	if(not.btCanBuyCandyUserCount < 0)
		not.btCanBuyCandyUserCount = 0;

	m_pUser->Send(S2C_EVENT_SUMMER_CANDY_UPDATE_CANDY_NOT, &not, sizeof(SS2C_EVENT_SUMMER_CANDY_UPDATE_CANDY_NOT));
}

void CFSGameUserSummerCandyEvent::BuyCandy(int iPropertyValue)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == SUMMERCANDYEVENT.IsOpen());

	m_UserSummerCandyEvent._IBuyCandyCount += iPropertyValue;

	SendUserCandyInfo();
}

BOOL CFSGameUserSummerCandyEvent::CheckGiveRewardCandy()
{
	CHECK_CONDITION_RETURN(CLOSED == SUMMERCANDYEVENT.IsOpen(), FALSE);

	ResetPlayCount();

	CHECK_CONDITION_RETURN(SUMMERCANDYEVENT.GetDailyMaxGamePlayCount() != (m_UserSummerCandyEvent._iDailyGamePlayCount+1), FALSE);

	return TRUE;
}

void CFSGameUserSummerCandyEvent::GiveRewardCandy(int iGiveCandyCount/* = 1 */)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iUpdatedCount;
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->ITEM_AddHighFrequencyItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY, iGiveCandyCount, iUpdatedCount));

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY);
	if(NULL != pItem)
	{
		pItem->iPropertyTypeValue = iUpdatedCount;
	}
	else
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdatedCount;
		m_pUser->GetUserHighFrequencyItem()->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	IncreaseDailyGamePlayCount();
}

void CFSGameUserSummerCandyEvent::IncreaseDailyGamePlayCount()
{
	ResetPlayCount();

	CHECK_CONDITION_RETURN_VOID(SUMMERCANDYEVENT.GetDailyMaxGamePlayCount() <= m_UserSummerCandyEvent._iDailyGamePlayCount);

	m_UserSummerCandyEvent._iDailyGamePlayCount += 1;
	m_UserSummerCandyEvent._tLastGamePlayDate = _time64(NULL);
}

RESULT_EVENT_SUMMER_CANDY_GET_REWARD CFSGameUserSummerCandyEvent::GetRewardReq(int iRewardIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == SUMMERCANDYEVENT.IsOpen(), RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL_CLOSED_EVENT);

	int iNeedCandyCount = SUMMERCANDYEVENT.CheckRewardNeedCandyCount(iRewardIndex);
	CHECK_CONDITION_RETURN(0 >= iNeedCandyCount, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL);

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY);
	CHECK_CONDITION_RETURN(NULL == pItem, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL_LEAK_CANDY);
	CHECK_CONDITION_RETURN(iNeedCandyCount > pItem->iPropertyTypeValue, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL_LEAK_CANDY);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL_FULL_MAILBOX);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL);

	int iUpdatedCandyCount;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->ITEM_UseHighFrequencyItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY, iNeedCandyCount, iUpdatedCandyCount), RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL);

	pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY);
	if(NULL != pItem)
		pItem->iPropertyTypeValue = iUpdatedCandyCount;

	pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL);

	CFSEventODBC* pEventODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pEventODBC, RESULT_EVENT_SUMMER_CANDY_GET_REWARD_FAIL);

	pEventODBC->EVENT_SUMMER_CANDY_InsertRewardLog(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iUpdatedCandyCount+iNeedCandyCount, iUpdatedCandyCount, iRewardIndex);

	return RESULT_EVENT_SUMMER_CANDY_GET_REWARD_SUCCESS;
}

void CFSGameUserSummerCandyEvent::SaveUserInfo(CFSEventODBC* pODBC)
{
	if(NULL == pODBC)
	{
		pODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	}
	CHECK_NULL_POINTER_VOID(pODBC);

	CHECK_CONDITION_RETURN_VOID(CLOSED == SUMMERCANDYEVENT.IsOpen());

	pODBC->EVENT_SUMMER_CANDY_UpdateUserInfo(m_pUser->GetUserIDIndex(), m_UserSummerCandyEvent._IBuyCandyCount, m_UserSummerCandyEvent._iDailyGamePlayCount, m_UserSummerCandyEvent._tLastGamePlayDate);
}