qinclude "stdafx.h"
qinclude "CFSGameUserLuckyBox.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "LuckyBoxManager.h"
qinclude "RewardManager.h"

CFSGameUserLuckyBox::CFSGameUserLuckyBox(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserLuckyBox::~CFSGameUserLuckyBox(void)
{
}

BOOL CFSGameUserLuckyBox::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == LUCKYBOX.IsOpen(), TRUE);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	if (ODBC_RETURN_SUCCESS != pBaseODBC->LUCKYBOX_GetAvatarMission(m_pUser->GetUserIDIndex(), m_AvatarLuckyBoxMission))
	{
		WRITE_LOG_NEW(LOG_TYPE_LUCKYBOX, DB_DATA_LOAD, FAIL, "LUCKYBOX_GetAvatarMission failed");
		return FALSE;
	}

	m_bDataLoaded = TRUE;
	CheckResetDate();

	return TRUE;
}

BOOL CFSGameUserLuckyBox::CheckResetDate(time_t tCurrentTime/* = _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == LUCKYBOX.IsOpen(), FALSE);

	BOOL	bResult = FALSE;

	if(-1 == m_AvatarLuckyBoxMission.tMissionLastDate)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_AvatarLuckyBoxMission.tMissionLastDate, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= LUCKYBOX.GetInitHour();

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_AvatarLuckyBoxMission.tMissionLastDate <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->LUCKYBOX_AvatarUpdateMission(m_pUser->GetUserIDIndex(), eLUCKYBOX_UPDATE_TYPE_DAILY_RESET, 0, 0, tCurrentTime))
		{
			m_AvatarLuckyBoxMission.tMissionLastDate = tCurrentTime;
			m_AvatarLuckyBoxMission.iMissionPlayCount = 0;

			return TRUE;
		}
	}

	return FALSE;
}

void CFSGameUserLuckyBox::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == LUCKYBOX.IsOpen());

	CPacketComposer* pPacket = LUCKYBOX.GetPacketEventInfo();
	m_pUser->Send(pPacket);
}

void CFSGameUserLuckyBox::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == LUCKYBOX.IsOpen());

	CheckResetDate();

	SS2C_LUCKYBOX_USER_INFO_RES ss;
	ss.btCurrentPlayCount = m_AvatarLuckyBoxMission.iMissionPlayCount;
	ss.btLuckyTicketCount = m_AvatarLuckyBoxMission.iLuckyTicketCount;

	CPacketComposer Packet(S2C_LUCKYBOX_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_LUCKYBOX_USER_INFO_RES));
	m_pUser->Send(&Packet);
}

RESULT_USE_LUCKYTICKET CFSGameUserLuckyBox::UseLuckyTicket(BYTE& btProductIndex, int& iUpdatedLuckyTicketCount)
{
	CHECK_CONDITION_RETURN(CLOSED == LUCKYBOX.IsOpen(), RESULT_USE_LUCKYTICKET_EVENT_CLOSED);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST, RESULT_USE_LUCKYTICKET_PRESENT_LIST_FULL);

	if(0 >= m_AvatarLuckyBoxMission.iLuckyTicketCount)
	{
		CHECK_CONDITION_RETURN(FALSE == CheckResetDate() && m_AvatarLuckyBoxMission.iMissionPlayCount >= LUCKYBOX.GetMaxMissionPlayCount()
			, RESULT_USE_LUCKYTICKET_COMPLETED_TODAY_USECOUNT);

		return RESULT_USE_LUCKYTICKET_NOT_ENOUGH_ITEM;
	}

	int iRewardIndex = 0;
	CHECK_CONDITION_RETURN(FALSE == LUCKYBOX.GetRandomProduct(btProductIndex, iRewardIndex), RESULT_USE_LUCKYTICKET_FAILED);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_USE_LUCKYTICKET_FAILED);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_USE_LUCKYTICKET_FAILED);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->LUCKYBOX_UseLuckyBox(m_pUser->GetUserIDIndex(), btProductIndex), RESULT_USE_LUCKYTICKET_FAILED);

	pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_USE_LUCKYTICKET_FAILED);

	iUpdatedLuckyTicketCount = -- m_AvatarLuckyBoxMission.iLuckyTicketCount;

	return RESULT_USE_LUCKYTICKET_SUCCESS;
}

void CFSGameUserLuckyBox::UpdateMission(int iMatchType, BOOL bIsDisconnectedMatch, BOOL bRunAway, BYTE& btGiveLuckyTicketCnt)
{
	btGiveLuckyTicketCnt = 0;
	CHECK_CONDITION_RETURN_VOID(CLOSED == LUCKYBOX.IsOpen());
	CHECK_CONDITION_RETURN_VOID(MATCH_TYPE_RATING != iMatchType && MATCH_TYPE_RANKMATCH != iMatchType);
	CHECK_CONDITION_RETURN_VOID(TRUE == bIsDisconnectedMatch || TRUE == bRunAway);
	CHECK_CONDITION_RETURN_VOID(m_AvatarLuckyBoxMission.iMissionPlayCount >= LUCKYBOX.GetMaxMissionPlayCount());

	BYTE btUpdatedMissionPlayCnt = m_AvatarLuckyBoxMission.iMissionPlayCount + 1;
	BYTE btGiveLuckyTicket = LUCKYBOX.GetGiveTicketCnt(btUpdatedMissionPlayCnt);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->LUCKYBOX_AvatarUpdateMission(m_pUser->GetUserIDIndex(), eLUCKYBOX_UPDATE_TYPE_PLAY_MISSION, btUpdatedMissionPlayCnt, btGiveLuckyTicket, 0));

	m_AvatarLuckyBoxMission.iMissionPlayCount = btUpdatedMissionPlayCnt;
	m_AvatarLuckyBoxMission.iLuckyTicketCount += btGiveLuckyTicket;

	btGiveLuckyTicketCnt = btGiveLuckyTicket;
}