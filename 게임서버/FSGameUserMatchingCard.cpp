qinclude "stdafx.h"
qinclude "FSGameUserMatchingCard.h"
qinclude "ThreadODBCManager.h"
qinclude "CFSGameUser.h"
qinclude "CenterSvrProxy.h"
qinclude "GameProxyManager.h"

CFSGameUserMatchingCard::CFSGameUserMatchingCard( CFSGameUser* pUser )
:m_pUser(pUser)
,m_dwStartTick(0)
,m_iGameLogIndex(0)
,m_btFirstCardIndex(0)
,m_btSecondCardIndex(0)
,m_iTodayCumulativeCoin(0)
,m_bInitDBdata(FALSE)
,m_tOpdate(-1)
{

}


CFSGameUserMatchingCard::~CFSGameUserMatchingCard(void)
{
}

BOOL CFSGameUserMatchingCard::Load( void )
{
	CHECK_CONDITION_RETURN(CLOSED == MATCHINGCARD.IsOpen(), TRUE);
	CHECK_NULL_POINTER_BOOL( m_pUser );

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL( pODBCBase );

	ZeroMemory(&m_sUserMatchingCardInfo, sizeof( SUSERMATCHINGCARD ));

	if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_MATCHINGCARD_GetUser( m_pUser->GetUserIDIndex()
		, m_sUserMatchingCardInfo._iPlayCount, m_sUserMatchingCardInfo._iGameCoin , m_iTodayCumulativeCoin, m_tOpdate) )
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, DB_DATA_LOAD, FAIL, "EVENT_MATCHINGCARD_GetUser");
		return FALSE;
	}

	if( PCROOM_KIND_PREMIUM == m_pUser->GetPCRoomKind() )
		CheckAndGivePlayCountWithPcRoom(); // 피씨방은 하나 더 줘야한다고 함
	// CheckAndGivePlayCount(Opdate); JOB 에서 6시 초기화시에 기본으로 1개 넣어줌. 접속안해도 상관X

	m_bInitDBdata = TRUE;

	return TRUE;
}

void CFSGameUserMatchingCard::SendMatchingCardInfo( void )
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == MATCHINGCARD.IsOpen() );
	const SMATCHINGCARDCONFIG sConfig = MATCHINGCARD.GetMatchingCardConfig();

	if( FALSE == m_bInitDBdata ) //왠지 클라에서 자꾸요청한다. 그래서 막아야함.
	{
		m_sUserMatchingCardInfo._iLife = sConfig._iMaxPlayLife;
		Load();
	}
	// 한국만
	int iPcroomBonusGameCoin = (PCROOM_KIND_PREMIUM == m_pUser->GetPCRoomKind() ? sConfig._iPcroomBonusPlayCoin : 0 );

	CPacketComposer	Packet(S2C_EVENT_MATCHINGCARD_INFO_RES);
	
	SS2C_EVENT_MATCHINGCARD_INFO_RES rs;

	rs.btResult = RESULT_EVENT_MATCHINGCARD_SUCCESS;
	rs.tOpenDate = sConfig._tOpenDate;
	rs.tEndDate = sConfig._tEndDate;

	rs.btCoin = (BYTE)m_sUserMatchingCardInfo._iLife; // 아.. 남은생명임

	BYTE btPlayCount = 0;
	if( m_sUserMatchingCardInfo._iPlayCount >= sConfig._iMaxTodayPlayCount )
		btPlayCount = sConfig._iOneStepPlayCount;
	else 
		btPlayCount = m_sUserMatchingCardInfo._iPlayCount ��4Config._iOneStepPlayCount;

	rs.btPlayCount[EVENT_MATCHINGCARD_STATUS_LEFT] = btPlayCount;
	rs.btPlayCount[EVENT_MATCHINGCARD_STATUS_RIGHT] = (BYTE)sConfig._iOneStepPlayCount;
	rs.btCoinAmountObtain[EVENT_MATCHINGCARD_STATUS_LEFT] = (BYTE)m_iTodayCumulativeCoin;
	rs.btCoinAmountObtain[EVENT_MATCHINGCARD_STATUS_RIGHT] = (BYTE)sConfig._iMaxTodayCoin + iPcroomBonusGameCoin;
	rs.btCurrentGameCoin = (BYTE)m_sUserMatchingCardInfo._iGameCoin;
	rs.iLimitTime_Sec = sConfig._iMaxPlaySecond;

	Packet.Add((PBYTE)&rs, sizeof( SS2C_EVENT_MATCHINGCARD_INFO_RES ));

	m_pUser->Send(&Packet);
}

void CFSGameUserMatchingCard::SendMatchingCardReady( void )
{
	SS2C_EVENT_MATCHINGCARD_READY_RES rs;
	CHECK_CONDITION_RETURN_VOID( FALSE == InitializeMatchingCard()) ;
	if( CLOSED == MATCHINGCARD.IsOpen() )
	{
		rs.btResult = RESULT_EVENT_MATCHINGCARD_NO_TIME;
	}
	else
	{
		rs.btResult = RESULT_EVENT_MATCHINGCARD_SUCCESS;
	}

	if( m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST - 3 )
		rs.btResult = RESULT_EVENT_MATCHINGCARD_MAILBOX_FULL;
	
	if( RESULT_EVENT_MATCHINGCARD_SUCCESS == rs.btResult )
	{
		ReLoadUserData(); 

		if( 0 < m_sUserMatchingCardInfo._iGameCoin )
		{
			CFSODBCBase* pODBCBase = static_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID( pODBCBase );

			ODBCRETURN iResult = pODBCBase->EVENT_MATCHINGCARD_Ready( m_pUser->GetUserIDIndex(), m_iGameLogIndex, m_sUserMatchingCardInfo._iGameCoin);
			if( ODBC_RETURN_SUCCESS != iResult )
			{
				WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, DB_DATA_LOAD, FAIL, "EVENT_MATCHINGCARD_Ready Fail! - UserIDIndex:10752790" , m_pUser->GetUserIDIndex());
turn;
			}
		}
		else
		{
			rs.btResult = RESULT_EVENT_MATCHINGCARD_NO_GAMECOIN;
		}
	}

	m_pUser->Send(S2C_EVENT_MATCHINGCARD_READY_RES, &rs, sizeof(SS2C_EVENT_MATCHINGCARD_READY_RES));

	if( RESULT_EVENT_MATCHINGCARD_SUCCESS == rs.btResult )
	{
		m_btFirstCardIndex = m_btSecondCardIndex = 0;

		m_sUserMatchingCardInfo._iLife = MATCHINGCARD.GetMatchingCardConfig()._iMaxPlayLife;
		m_dwStartTick = GetTickCount();
		SendCumulativeCashToCenterSvr();
	}
}

int CFSGameUserMatchingCard::GetGameLogIndex( void )
{
	return m_iGameLogIndex;
}

void CFSGameUserMatchingCard::SendMatchingCardRewardInfo( void )
{
	CPacketComposer	Packet(S2C_EVENT_MATCHINGCARD_REWARDINFO_RES);

	MATCHINGCARD.MakePacketRewardItemList(Packet);

	m_pUser->Send(&Packet);
}

void CFSGameUserMatchingCard::FlipCard( BYTE btIndex )
{
	CHECK_CONDITION_RETURN_VOID( MIN_MATCHINGCARD_INDEX > btIndex || btIndex >= MAX_MATCHINGCARD_INDEX );

	SS2C_EVENT_MATCHINGCARD_FLIPCARD_RES rs;
	ZeroMemory(&rs, sizeof(SS2C_EVENT_MATCHINGCARD_FLIPCARD_RES));

	int iOpenCardCount = GetOpenCardCount();

	CHECK_CONDITION_RETURN_VOID( 1 == (iOpenCardCount ) && m_btFirstCardIndex == btIndex );
;

	const SMATCHINGCARDCONFIG sConfig = MATCHINGCARD.GetMatchingCardConfig();
	rs.btResult = CheckFlipCard(sConfig);

	if( RESULT_EVENT_MATCHINGCARD_FLIPCARD_ONECARD_SUCCESS != rs.btResult )
	{
		MakePacketDataMatchingCardInfo(rs.sMatchingCard);
		SendFlipCard(rs);
		return; // 먼가 실패해쩡
	}

	MAP_USERREWARDITEM::iterator iter = m_mapUserRewardItem.find(m_sUserMatchingCard[btIndex].btIndex);
	CHECK_CONDITION_RETURN_VOID(iter == m_mapUserRewardItem.end());

	m_sUserMatchingCard[btIndex].btStatus = (BYTE)OPEN;
	CopyMatchingCardPacketItemInfo(m_sUserMatchingCard[btIndex].sItemInfo, (iter->second));
	
	if( 0 == (iOpenCardCount ) ) // 첫번째 장
�
	{
		rs.btFirstIndex = m_btFirstCardIndex = btIndex;
		rs.btSecondIndex = 0;
		
		rs.btResult = RESULT_EVENT_MATCHINGCARD_FLIPCARD_ONECARD_SUCCESS;
	}
	else
	{
		rs.btResult = (TRUE == CollisionFirstFlipCard( btIndex )) ? RESULT_EVENT_MATCHINGCARD_FLIPCARD_MATCH_SUCCESS/*당첨*/: RESULT_EVENT_MATCHINGCARD_FLIPCARD_MATCH_FAIL;
		
		if( RESULT_EVENT_MATCHINGCARD_FLIPCARD_MATCH_SUCCESS == rs.btResult )
		{
			if ( FALSE == GiveRewardMatchingCardItem(btIndex))
			{
				rs.btResult = RESULT_EVENT_MATCHINGCARD_FLIPCARD_MATCH_FAIL;
				//SendFlipCard(rs); // 지급실패
				//return; 
			}
			else
			{
				if( TRUE == CheckAllClear())
				{
					CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
					if( NULL != pCenter )
					{
						SS2S_DEFAULT req;
						strncpy_s(req.GameID, _countof(req.GameID), m_pUser->GetGameID(), _countof(req.GameID)-1);
						req.iGameIDIndex = m_pUser->GetGameIDIndex();
						pCenter->SendPacket(G2S_MATCHINGCARD_GIVE_EVENTCASH_REQ, &req, sizeof(SS2S_DEFAULT));
					}

					rs.btResult = RESULT_EVENT_MATCHINGCARD_FLIPCARD_GRATE;
				}
			}
		}

		if( RESULT_EVENT_MATCHINGCARD_FLIPCARD_MATCH_FAIL == rs.btResult )
		{
			if ( TRUE == CheckPlayCountAndDecreaseLife() ) // 다 끝남
			{
				rs.btResult = RESULT_EVENT_MATCHINGCARD_FLIPCARD_NO_LIFE; 
			}

			m_sUserMatchingCard[m_btFirstCardIndex].btStatus = (BYTE)CLOSED; // 실패 하였을 시 열려진것 둘다 닫습니다.
			m_sUserMatchingCard[btIndex].btStatus = (BYTE)CLOSED;
		}
		
		rs.btFirstIndex = m_btFirstCardIndex;
		rs.btSecondIndex = btIndex;

		m_btSecondCardIndex = m_btFirstCardIndex = 0; // 두번째 카드일때 프로세스 다끝나면 초기화
	}

	MakePacketDataMatchingCardInfo(rs.sMatchingCard);
	SendFlipCard(rs);

	if( RESULT_EVENT_MATCHINGCARD_FLIPCARD_NO_LIFE == rs.btResult )
		RemoveGame(); // 끝낸다.

	if( RESULT_EVENT_MATCHINGCARD_FLIPCARD_GRATE == rs.btResult || 
		RESULT_EVENT_MATCHINGCARD_FLIPCARD_NO_LIFE == rs.btResult)
		m_bInitDBdata = FALSE;
}

BOOL CFSGameUserMatchingCard::InitializeMatchingCard( void )
{
	// reset
	LIST_MATCHINGCARD_ITEM	listMatchingCardItem;
	MATCHINGCARD.GetMatchingCardItemList(listMatchingCardItem);

	CHECK_CONDITION_RETURN( (MAX_MATCHINGCARD_INDEX / 2) != listMatchingCardItem.size(), FALSE )
	
	m_mapUserRewardItem.clear();
	LIST_MATCHINGCARD_ITEM::iterator	 iter = listMatchingCardItem.begin();
	for( int iIndex = 0 
		; iIndex < MAX_MATCHINGCARD_INDEX && iter != listMatchingCardItem.end()
		; ++iIndex )
	{
		m_sUserMatchingCard[iIndex].btIndex = (BYTE)(iIndex);
		m_sUserMatchingCard[iIndex].btStatus = (BYTE)CLOSED;
		ZeroMemory(&m_sUserMatchingCard[iIndex].sItemInfo, sizeof(SMATCHINGCARD_REWARD_ITEM_INFO));

		SMATCHINGCARD_REWARD_ITEM_INFO sInfo;
		CopyMatchingCardPacketItemInfo(sInfo, (*iter));

		m_mapUserRewardItem.insert(MAP_USERREWARDITEM::value_type(m_sUserMatchingCard[iIndex].btIndex, sInfo));

		if( (iIndex ) == 1 )
)
			++iter;
	}

	BYTE btTemp, btDest, btSour;
	btTemp = btDest = btSour = 0;
	// bubble
	for( size_t iIndex = 0; iIndex < (MAX_MATCHINGCARD_INDEX * 3) ; ++iIndex )
	{
		btSour = rand() / (RAND_MAX / MAX_MATCHINGCARD_INDEX);
		btDest = rand() / (RAND_MAX / MAX_MATCHINGCARD_INDEX);

		btTemp = m_sUserMatchingCard[btSour].btIndex;
		m_sUserMatchingCard[btSour].btIndex = m_sUserMatchingCard[btDest].btIndex;
		m_sUserMatchingCard[btDest].btIndex = btTemp;
	}

	return TRUE;
}

void CFSGameUserMatchingCard::CopyMatchingCardPacketItemInfo( SMATCHINGCARD_REWARD_ITEM_INFO& sLeft, SMATCHINGCARD_REWARD_ITEM_INFO sRight )
{
	sLeft.btRewardType = sRight.btRewardType;
	sLeft.iItemCode = sRight.iItemCode;
	sLeft.iPropertyType = sRight.iPropertyType;
	sLeft.iPropertyTypeValue = sRight.iPropertyTypeValue;
}

void CFSGameUserMatchingCard::CopyMatchingCardPacketItemInfo( SMATCHINGCARD_REWARD_ITEM_INFO& sLeft, SMATCHINGCARDITEM sRight )
{
	sLeft.btRewardType = sRight._iRewardType;
	sLeft.iItemCode = sRight._iItemCode;
	sLeft.iPropertyType = sRight._iPropertyType;
	sLeft.iPropertyTypeValue = sRight._iPropertyValue;
}

int CFSGameUserMatchingCard::GetOpenCardCount( void )
{
	int iOpenCardCount = 0;
	for( size_t iIndex = 0 ; iIndex < MAX_MATCHINGCARD_INDEX ; ++iIndex )
	{
		if( OPEN == m_sUserMatchingCard[iIndex].btStatus)
		{
			++iOpenCardCount;
		}
	}

	return iOpenCardCount;
}

RESULT_EVENT_MATCHINGCARD_FLIPCARD CFSGameUserMatchingCard::CheckFlipCard( SMATCHINGCARDCONFIG sConfig )
{
	if( (m_dwStartTick + (sConfig._iMaxPlaySecond * 1000)) < GetTickCount())
		return RESULT_EVENT_MATCHINGCARD_FLIPCARD_OVER_TIME;
	else if( CLOSED == MATCHINGCARD.IsOpen() )
		return RESULT_EVENT_MATCHINGCARD_FLIPCARD_NO_TIME;
	else if( 0 >= m_sUserMatchingCardInfo._iLife )
		return RESULT_EVENT_MATCHINGCARD_FLIPCARD_NO_LIFE;
	else if( 0 > m_sUserMatchingCardInfo._iGameCoin )
		return RESULT_EVENT_MATCHINGCARD_FLIPCARD_NO_COIN;

	return RESULT_EVENT_MATCHINGCARD_FLIPCARD_ONECARD_SUCCESS;
}

void CFSGameUserMatchingCard::MakePacketDataMatchingCardInfo( SMATCHINGCARD_INFO* pCardInfo )
{
	memcpy(pCardInfo, m_sUserMatchingCard, sizeof(SMATCHINGCARD_INFO)*MAX_MATCHINGCARD_INDEX);
}

void CFSGameUserMatchingCard::SendFlipCard( SS2C_EVENT_MATCHINGCARD_FLIPCARD_RES& rs )
{
	CPacketComposer Packet(S2C_EVENT_MATCHINGCARD_FLIPCARD_RES);	
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_MATCHINGCARD_FLIPCARD_RES));
	m_pUser->Send(&Packet);
}

BOOL CFSGameUserMatchingCard::CollisionFirstFlipCard( BYTE btIndex )
{
	CHECK_CONDITION_RETURN( MIN_MATCHINGCARD_INDEX > m_btFirstCardIndex || m_btFirstCardIndex >= MAX_MATCHINGCARD_INDEX , FALSE);
	CHECK_CONDITION_RETURN( MIN_MATCHINGCARD_INDEX > btIndex || btIndex >= MAX_MATCHINGCARD_INDEX , FALSE);

	if( m_sUserMatchingCard[m_btFirstCardIndex].sItemInfo.iItemCode == m_sUserMatchingCard[btIndex].sItemInfo.iItemCode &&
		m_sUserMatchingCard[m_btFirstCardIndex].sItemInfo.iPropertyTypeValue == m_sUserMatchingCard[btIndex].sItemInfo.iPropertyTypeValue )
	{
		return TRUE;
	}

	return FALSE;
}

BOOL CFSGameUserMatchingCard::GiveRewardMatchingCardItem( BYTE btIndex )
{
	CHECK_CONDITION_RETURN( MIN_MATCHINGCARD_INDEX > btIndex || btIndex >= MAX_MATCHINGCARD_INDEX , FALSE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	S_ODBC_UserMatch	sODBCInfo;
	sODBCInfo.iUserIDIndex = m_pUser->GetUserIDIndex();
	sODBCInfo.iGameIDIndex = m_pUser->GetGameIDIndex();
	sODBCInfo.iPlayCount = m_sUserMatchingCardInfo._iPlayCount;
	sODBCInfo.iGameCoin = m_sUserMatchingCardInfo._iGameCoin;
	sODBCInfo.iRewardType = (int)m_sUserMatchingCard[btIndex].sItemInfo.btRewardType ;
	sODBCInfo.iItemCode = m_sUserMatchingCard[btIndex].sItemInfo.iItemCode;
	sODBCInfo.iPropertyValue = m_sUserMatchingCard[btIndex].sItemInfo.iPropertyTypeValue;
	sODBCInfo.iGameIdx = m_iGameLogIndex;
	sODBCInfo.iOutGameCoin = 0;
	sODBCInfo.iOutPlayCount = 0;
	sODBCInfo.iOutPresentIndex = -1; 
	sODBCInfo.iGiveEventCash = 0; //동기화 된 것 말고, 실시간으로 모여있는것 전부를 줘야 한다고 함.. //TRUE == CheckAllClear() ? MATCHINGCARD.GetCumulativeCash() : 0;
	sODBCInfo.iOutEventCash = 0;

	sODBCInfo.iGameCoin = m_sUserMatchingCardInfo._iGameCoin;
	sODBCInfo.iGameIDIndex = m_pUser->GetGameIDIndex();
	ODBCRETURN iResult = pODBCBase->EVENT_MATCHINGCARD_Match(sODBCInfo);
	if(ODBC_RETURN_SUCCESS != iResult)
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, CALL_SP, FAIL
			, "EVENT_MATCHINGCARD_Match fail - UserIDIndex:10752790, GameIDIndex:0, ItemCode:7106560, GameIdx:-858993460, PlayCount:-858993460, GiveEventcash:-858993460, ErrorCode:-858993460" , 
IDIndex,  sODBCInfo.iItemCode, sODBCInfo.iGameIdx, sODBCInfo.iPlayCount, sODBCInfo.iGiveEventCash, iResult);

		return FALSE;
	}

	m_sUserMatchingCardInfo._iGameCoin = sODBCInfo.iOutGameCoin;
	m_sUserMatchingCardInfo._iPlayCount = sODBCInfo.iOutPlayCount;

// 	if( 0 < sODBCInfo.iOutEventCash )
// 	{
// 		m_pUser->SetEventCoin(/*m_pUser->GetEventCoin() +*/ sODBCInfo.iOutEventCash, TRUE);
// 		m_pUser->SendUserGold();
// 	}
	if( REWARD_TYPE_POINT == sODBCInfo.iRewardType )
	{
		m_pUser->SetSkillPoint( m_pUser->GetSkillPoint()  + sODBCInfo.iPropertyValue );
		m_pUser->SendUserGold();
	}

	if( -1 < sODBCInfo.iOutPresentIndex)
	{
		m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, sODBCInfo.iOutPresentIndex );
	}
	
	return TRUE;
}

BOOL CFSGameUserMatchingCard::CheckAllClear( void )
{
	for( size_t iIndex = 0 ; iIndex < MAX_MATCHINGCARD_INDEX ; ++iIndex )
	{
		if( CLOSED == m_sUserMatchingCard[iIndex].btStatus )
			return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUserMatchingCard::CheckPlayCountAndDecreaseLife( void ) // 실패하였을 경우
{
// 	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
// 	CHECK_NULL_POINTER_BOOL (pODBCBase);

	--m_sUserMatchingCardInfo._iLife;

	if( 0 >= m_sUserMatchingCardInfo._iLife )
		return TRUE;

	return FALSE;
}

void CFSGameUserMatchingCard::RemoveGame( void )
{
	m_mapUserRewardItem.clear();
	
	ZeroMemory(&m_sUserMatchingCard, sizeof(SMATCHINGCARD_INFO)*MAX_MATCHINGCARD_INDEX);
}

void CFSGameUserMatchingCard::CheckAndAddPlayCount( void )
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == MATCHINGCARD.IsOpen() );

	const SMATCHINGCARDCONFIG sConfig = MATCHINGCARD.GetMatchingCardConfig();

	if( m_sUserMatchingCardInfo._iPlayCount >= sConfig._iMaxTodayPlayCount )
	{
		m_sUserMatchingCardInfo._iPlayCount = sConfig._iMaxTodayPlayCount;
		return; // 오늘의 누적판수가 끝났으면 리턴
	}
	
	++m_sUserMatchingCardInfo._iPlayCount;

	int iAddPlayCount = 1; // 플레이카운트는 한판마다..
	int iAddGameCoin = 0;

	if( 0 != m_sUserMatchingCardInfo._iPlayCount &&
		0 == m_sUserMatchingCardInfo._iPlayCount ��4Config._iOneStepPlayCount ) // 코인을 넣어줘야할때
	{
		iAddGameCoin = 1;
	}

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID( pODBCBase );

	if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_MATCHINGCARD_UpdatePlayCount(
		m_pUser->GetUserIDIndex(), iAddPlayCount, iAddGameCoin, m_sUserMatchingCardInfo._iPlayCount, m_sUserMatchingCardInfo._iGameCoin, m_iTodayCumulativeCoin))
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, CALL_SP, FAIL, " EVENT_MATCHINGCARD_UpdatePlayCount - FAIL , UserIDIndex:10752790, PlayCount:0, GameCoin:7106560, CumulativeCoin:-858993460"
serIDIndex(), m_sUserMatchingCardInfo._iPlayCount, m_sUserMatchingCardInfo._iGameCoin, m_iTodayCumulativeCoin);

		return;
	}
}

void CFSGameUserMatchingCard::CheckAndSendTimeCheck( void )
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == MATCHINGCARD.IsOpen() );

	SS2C_EVENT_MATCHINGCARD_TIMECHECK_RES	rs;
	const SMATCHINGCARDCONFIG sConfig = MATCHINGCARD.GetMatchingCardConfig();

	int iRemaintime = sConfig._iMaxPlaySecond - ((GetTickCount() - m_dwStartTick) / 1000);

	if( iRemaintime <= 0 )
	{
		rs.btResult = RESULT_EVENT_MATCHINGCARD_TIMECHECK_TIMEOVER;
		rs.btRemainSecond = 0;

		RemoveGame();
	}
	else
	{
		rs.btResult = RESULT_EVENT_MATCHINGCARD_TIMECHECK_REMAIN_TIME;
		rs.btRemainSecond = abs(iRemaintime);
	}

	m_pUser->Send(S2C_EVENT_MATCHINGCARD_TIMECHECK_RES, &rs, sizeof(SS2C_EVENT_MATCHINGCARD_TIMECHECK_RES));
}

void CFSGameUserMatchingCard::CheckAndGivePlayCount(time_t tOpDate)
{
	const SMATCHINGCARDCONFIG sConfig = MATCHINGCARD.GetMatchingCardConfig();
	const int cnResetHour = 6;

	TIMESTAMP_STRUCT SCurrentDate, STempCurrentDate, SYesterDate;
	time_t tCurrentDate = _time64(NULL);
	time_t tYesterDate = tCurrentDate - (60*60*24);
	time_t tLimitDate;
	TimetToTimeStruct(tCurrentDate, SCurrentDate);
	TimetToTimeStruct(tCurrentDate, STempCurrentDate);
	TimetToTimeStruct(tYesterDate, SYesterDate);

	STempCurrentDate.hour = cnResetHour;
	STempCurrentDate.minute = 0;
	tLimitDate = TimeStructToTimet(STempCurrentDate);
	
	bool bGiveCheck = false;
	if( SCurrentDate.hour >= cnResetHour && tLimitDate > m_tOpdate ) // 6시 지났는뎅 오늘 6시 이전에 받았으면 
	{
		bGiveCheck = true;
	}

	SYesterDate.hour = cnResetHour;
	SYesterDate.minute = 0;
	tLimitDate = TimeStructToTimet(SYesterDate);
	if( SCurrentDate.hour < cnResetHour && tLimitDate > m_tOpdate ) // 6시 이전인데 전날 6시 이전에 받았으면
	{
		bGiveCheck = true;
	}

	if( true == bGiveCheck )
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID( pODBCBase );

		if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_MATCHINGCARD_GiveLoginReward(m_pUser->GetUserIDIndex(), 1, m_sUserMatchingCardInfo._iGameCoin, m_iTodayCumulativeCoin))
		{
			WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, CALL_SP, FAIL, "EVENT_MATCHINGCARD_GiveLoginReward - FAIL, UserIDIndex:10752790 ", m_pUser->GetUserIDIndex());
urn;
		}
	}
}

void CFSGameUserMatchingCard::ReLoadUserData( void )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID( pODBCBase );

	time_t Opdate;
	if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_MATCHINGCARD_GetUser( m_pUser->GetUserIDIndex()
		, m_sUserMatchingCardInfo._iPlayCount, m_sUserMatchingCardInfo._iGameCoin , m_iTodayCumulativeCoin, Opdate) )
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, DB_DATA_LOAD, FAIL, "EVENT_MATCHINGCARD_GetUser - ReLoad Fail UserIDIndex:10752790 ", m_pUser->GetUserIDIndex());
rn;
	}
}

void CFSGameUserMatchingCard::SendCumulativeCashToCenterSvr( void )
{
	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
	if( NULL != pCenter )
	{
		const SMATCHINGCARDCONFIG sConfig = MATCHINGCARD.GetMatchingCardConfig();

		SG2S_MATCHINGCARD_CASH_NOT not;
		not.iIncreaseCash = sConfig._iIncreaseEventCash; 

		pCenter->SendPacket(G2S_MATCHINGCARD_CASH_NOT, &not, sizeof(SG2S_MATCHINGCARD_CASH_NOT));
	}
}

void CFSGameUserMatchingCard::CheckAndGivePlayCountWithPcRoom()
{
	CHECK_CONDITION_RETURN_VOID( PCROOM_KIND_PREMIUM != m_pUser->GetPCRoomKind());
	CHECK_CONDITION_RETURN_VOID(CLOSED == MATCHINGCARD.IsOpen());

	const SMATCHINGCARDCONFIG sConfig = MATCHINGCARD.GetMatchingCardConfig();
	const int cnResetHour = 6;

	TIMESTAMP_STRUCT SCurrentDate, STempCurrentDate, SYesterDate;
	time_t tCurrentDate = _time64(NULL);
	time_t tYesterDate = tCurrentDate - (60*60*24);
	time_t tLimitDate;
	TimetToTimeStruct(tCurrentDate, SCurrentDate);
	TimetToTimeStruct(tCurrentDate, STempCurrentDate);
	TimetToTimeStruct(tYesterDate, SYesterDate);

	STempCurrentDate.hour = cnResetHour;
	STempCurrentDate.minute = 1;
	tLimitDate = TimeStructToTimet(STempCurrentDate);

	bool bGiveCheck = false;
	if( SCurrentDate.hour >= cnResetHour && tLimitDate > m_tOpdate ) // 6시 지났는뎅 오늘 6시 이전에 받았으면 
	{
		bGiveCheck = true;
	}

	SYesterDate.hour = cnResetHour;
	SYesterDate.minute = 1;
	tLimitDate = TimeStructToTimet(SYesterDate);
	if( SCurrentDate.hour < cnResetHour && tLimitDate > m_tOpdate ) // 6시 이전인데 전날 6시 이전에 받았으면
	{
		bGiveCheck = true;
	}

	if( true == bGiveCheck )
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID( pODBCBase );

		if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_MATCHINGCARD_GiveLoginReward(m_pUser->GetUserIDIndex(), 1
			, m_sUserMatchingCardInfo._iGameCoin, m_iTodayCumulativeCoin))
		{
			WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, CALL_SP, FAIL, "EVENT_MATCHINGCARD_GiveLoginReward - FAIL, UserIDIndex:10752790 ", m_pUser->GetUserIDIndex());
urn;
		}
	}
}

