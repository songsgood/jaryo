// CBillingManagerGame.cpp: implementation of the CBillingManagerGame class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CBillingManagerGame.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSGameODBC.h"
qinclude "CUserAction.h"
qinclude "LogSvrProxy.h"
qinclude "FactionManager.h"
qinclude "CenterSvrProxy.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBillingManagerGame::CBillingManagerGame()
{

}

CBillingManagerGame::~CBillingManagerGame()
{
}

int CBillingManagerGame::GetCashBalance(CUser* pUser)
{
	int iResult = CASH_BALANCE_SUCCESS;
	if(NULL != pUser)
	{
		if( TRUE == m_bNtreevEnable && NULL != m_pNtreevBillManager )
		{
			SCashTotalInfo sTotalInfo;
			memcpy( sTotalInfo.szGameID, pUser->GetGameID(), sizeof(char) * (MAX_GAMEID_LENGTH + 1) );

			m_pNtreevBillManager->PushTotalInfo(sTotalInfo);
		}
		else if( NULL != m_pLocalCashier )
		{
			int iCoin = 0;
			if( FALSE == m_pLocalCashier->GetCashBalance( pUser, iCoin) )
				iResult = CASH_BALANCE_FAIL_LOCAL;

			pUser->SetCoin(iCoin);
			pUser->SetBonusCoin(0);
		}
	}
	else
	{
		iResult = CASH_BALANCE_FAIL_NULL_POINTER;
	}

	return iResult;
}

BOOL CBillingManagerGame::RequestPurchaseItem(SBillingInfo &BillingInfo)
{
	CHECK_NULL_POINTER_BOOL( BillingInfo.pUser );

	BillingInfo.iBonusCoin = BillingInfo.pUser->GetBonusCoin();

	if(  BillingInfo.iSellType == SELL_TYPE_TROPHY
		|| BillingInfo.iSellType == SELL_TYPE_POINT
		|| BillingInfo.iSellType == SELL_TYPE_CLUBCOIN
		||  BillingInfo.iEventIndex > -1 
		|| ( BillingInfo.iEventCode == EVENT_BUY_SKILL && BillingInfo.iSellType == SELL_TYPE_SKILL_TICKET )
		|| ( BillingInfo.iEventCode == EVENT_BUY_FREESTYLE && BillingInfo.iSellType == SELL_TYPE_SKILL_TICKET) 
		|| ( BillingInfo.iEventCode == EVENT_BUY_RANDOMCARD_ITEM && BillingInfo.bUseRandomCardTicket == TRUE) )
	{
		if( FALSE == BuyContents( &BillingInfo, PAY_RESULT_SUCCESS ) )
		{
			return FALSE;
		}
	}
	else
	{
		int iCurEventCoin = BillingInfo.pUser->GetEventCoin();

		if(KOREA_PUBLISHER_NAVER == BillingInfo.pUser->GetPublisherIDIndex() &&
			BillingInfo.iCashChange > 0 &&
			BillingInfo.iCashChange > iCurEventCoin)
		{
			CPacketComposer Packet(S2C_ITEM_SELECT_RES);
			Packet.Add(FS_ITEM_OP_BUY);
			Packet.Add((int)BUY_ITEM_ERROR_NOT_USE_CASH_NAVER_USER);
			Packet.Add(BillingInfo.iItemCode);
			Packet.Add((int)BUY_ITEM_END_OPERATION);		
			BillingInfo.pUser->Send(&Packet);

			return FALSE;
		}

		if (BillingInfo.bIsPurchasableInEventCoin == TRUE &&
			iCurEventCoin > 0 &&
			FALSE == BillingInfo.bIsSendItem &&
			BillingInfo.iEventCode != EVENT_DONATION_CLUB)
		{
			CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
			CHECK_NULL_POINTER_BOOL(pGameODBC)

			int iChangedEventCoin = 0;
			if( !BillingInfo.pUser->UpDateEventCoin(pGameODBC, BillingInfo.iItemCode,BillingInfo.iCashChange,iChangedEventCoin,EVENTCOIN_PAY))
			{
				return FALSE;
			}
			BillingInfo.bUseEventCoin = TRUE;
			
			if( iChangedEventCoin >= 0 )
			{
				BillingInfo.iCashChange = 0;
				BillingInfo.iUseEventCoin = iCurEventCoin - iChangedEventCoin;
				BillingInfo.pUser->SetEventCoin(iChangedEventCoin);
			}
			else
			{
				BillingInfo.iCashChange =  -iChangedEventCoin;
				BillingInfo.iUseEventCoin = iCurEventCoin;
				BillingInfo.pUser->SetEventCoin(0);
			}
		}

		if( TRUE == m_bNtreevEnable && NULL != m_pNtreevBillManager )
		{
			if(BillingInfo.iCashChange == 0)
			{
				if( FALSE == BuyContents(&BillingInfo, PAY_RESULT_SUCCESS))
					return FALSE;
			}
			else
			{
				m_pNtreevBillManager->PushChargeInfo(BillingInfo);
			}
		}
		else if( NULL != m_pLocalCashier )
		{
			if( FALSE == BuyContents(&BillingInfo, m_pLocalCashier->UseCashAndBuyProduct(BillingInfo)) )
				return FALSE;
		}
	}

	return TRUE;
}

// Channeling
BOOL CBillingManagerGame::BuyContents(SBillingInfo *pBillingInfo, int iCode)
{
	CHECK_NULL_POINTER_BOOL(pBillingInfo);

	BOOL bResult = FALSE;
	CFSGameUser* pUser = (CFSGameUser*) pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL(pUser);

	CFSGameODBC* pGameODBC = (CFSGameODBC*) ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC);

	if(pUser != NULL)
	{
		switch( pBillingInfo->iEventCode )
		{
		case EVENT_BUY_CHARACTERSLOT:
			bResult = pUser->GetUserItemList()->BuyCharacterSlot_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_ITEM:
			bResult = pUser->GetUserItemList()->BuyItem_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_SKILL:
		case EVENT_BUY_FREESTYLE:
			bResult = pUser->GetUserSkill()->BuySkillProcess_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_SKILLSLOT:
			bResult = pUser->GetUserItemList()->BuySkillSlot_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_FACEOFF:
			bResult = pUser->GetUserItemList()->BuyFaceOff_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_PREMIUM_ITEM:
			bResult = pUser->GetUserItemList()->BuyPremiumItem_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_LV_UP_ITEM:
			bResult = pUser->GetUserItemList()->BuyLvUpItem_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_BIND_ACCOUNT_ITEM:
			bResult = pUser->GetUserItemList()->BuyBinAccountItem_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_RESET_RECORD_ITEM:
			bResult = pUser->GetUserItemList()->BuyResetRecordItem_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_ACTION:
			bResult = pUser->GetUserAction()->BuyAction_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_CHANGE_DRESS:
			bResult = pUser->GetUserItemList()->BuyChangeDressItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_SPECIALTY_SKILL:
			bResult = pUser->GetUserSkill()->BuySpecialtySkill_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_CREATE_CLUB:
			bResult = pUser->CreateClub_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_DONATION_CLUB:
			bResult = pUser->DonationClub_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_PACKAGE_ITEM:
			bResult = pUser->GetUserItemList()->BuyPackageItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BINGO_SHUFFLE_SLOT:
			bResult = pUser->GetUserBingo()->ShuffleSlot_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_RECORDBOARD_PAGE:
			bResult = pUser->BuyRecordBoardPage_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_CHANGE_BODYSHAPE:
			bResult = pUser->GetUserItemList()->BuyChangeBodyShape_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_HIGH_FREQUENCY_ITEM:
			bResult = pUser->BuyHighFrequencyItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_COACH_CARD:
			bResult = pUser->BuyCoachCardItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_RANDOMCARD_ITEM:
			bResult = pUser->GetUserItemList()->BuyRandomCard_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_GUARANTEECARD_ITEM:
			bResult = pUser->GetUserItemList()->BuyGuaranteeCard_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_SECRETSTORE_ITEM:
			bResult = pUser->GetUserItemList()->BuySecretStore_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_SPECIALSKIN_ITEM:
			bResult = pUser->GetUserSpecialSkin()->BuySpecialSkinCard_AfterPay( pBillingInfo, iCode );
			break;
		case EVENT_BUY_HONEYWALLET_ITEM:
			bResult = pUser->GetUserHoneyWalletEvent()->BuyHoneyWallet_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_HOTGIRLSPECIALBOX_ITEM:
			bResult = pUser->GetUserHotGirlSpecialBoxEvent()->BuySpecialBox_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_FACTION_BUFF_ITEM:
			bResult = FACTION.UseBuffItem_AfterPay(pBillingInfo, iCode);		
			break;
		case EVENT_BUY_FIRSTCLASS_TICKET_ITEM:
			bResult = pUser->GetUserSkyLuckyEvent()->BuySkyLuckyTicket_AfterPay(pBillingInfo, iCode);		
			break;
		case EVENT_BUY_EVENT_CHEERLEADER:
			bResult = pUser->GetUserCheerLeader()->BuyEventCheerLeader_AfterPay(pBillingInfo, iCode);		
			break;
		case EVENT_BUY_BASKETBALL:
			bResult = pUser->GetUserBasketBall()->BuyBasketBallSkin_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_LEGEND_KEY_ITEM:
			bResult = pUser->GetUserLegendEvent()->BuyLegendKey_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_SONOFCHOICE_PRODUCT:
			bResult = pUser->BuySonOfChoiceEventRandomProduct_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_RANDOM_ARROW_ITEM:
			bResult = pUser->GetUserRandomArrowEvent()->BuyRandomArrow_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_EVENT_SHOPPINGBASKET_ITEM:
			bResult = pUser->GetUserShoppingFestivalEvent()->BuyShoppingBasketItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_EVENT_LIMITED_SALEITEM:
			bResult = pUser->GetUserShoppingFestivalEvent()->BuyLimitedSaleItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_EVENT_HALFPRICE_NEEDLE:
			bResult = pUser->BuyHalfPriceItem(pBillingInfo, iCode);
			break;
		case EVENT_BUY_EVENT_DISCOUNT_ITEM:
			bResult = pUser->GetUserDiscountItemEvent()->BuyDiscountItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_CHEERLEADER:
			bResult = pUser->GetUserItemList()->BuyCheerLeader_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_DEVILTEMTATION:
			bResult = pUser->GetUserDevilTemtationEvent()->BuyDevilTemtationTicket_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_MISSIONMAKEITEM:
			bResult = pUser->GetUserMissionMakeItemEvent()->BuyCashItem_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_DARKMARKET_PACKAGE_PRODUCT:
			bResult = pUser->GetUserDarkMarketEvent()->BuyPackageProduct_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_COVET_SUMMON_STONE:
			bResult = pUser->GetUserCovet()->BuySummonStone_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_PACKAGE_ITEM_GIVE_INVENTORY:
			bResult = pUser->BuyPackageItemGiveInventory_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_SPECIAL_PIECE:
			bResult = pUser->GetUserSpecialPiece()->BuySpecialPiece_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_CLUBMARK:
			bResult = pUser->BuyClubMark_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_CLUB_SETUP:
			bResult = pUser->ClubSetUp_AfterPay(pBillingInfo, iCode);
			break;
		case EVENT_BUY_CHANGE_CLUB_PR:
			bResult = pUser->ClubChangePR_AfterPay(pBillingInfo, iCode);
			break;
		default:
			WRITE_LOG_NEW(LOG_TYPE_BILLING, INVALED_DATA, CHECK_FAIL, "BuyContents - EventCode");
			bResult = false;
			break;
		};

		if( pBillingInfo->bUseEventCoin )
		{
			int nCurEventCoin  = 0;
			if (  TRUE == bResult )
			{ //真正扣除代金券
				pUser->UpDateEventCoin(pGameODBC, pBillingInfo->iItemCode, 0,nCurEventCoin,EVENTCOIN_PAY_SUCCESS);
			}
			else
			{ //回滚
				pUser->UpDateEventCoin(pGameODBC, pBillingInfo->iItemCode, 0,nCurEventCoin,EVENTCOIN_PAY_FAIL);
				pUser->SetEventCoin(nCurEventCoin);
			}
		}
		if (TRUE == bResult && 0 < pBillingInfo->iCashChange)
		{
			int iAccumulativeCash = 0;
			pGameODBC->AVATAR_UpdateAccumulativeCash(pUser->GetGameIDIndex(), pBillingInfo->iCashChange, iAccumulativeCash);

			CLogSvrProxy* pLogProxy = (CLogSvrProxy*) GAMEPROXY.GetProxy(FS_LOG_SERVER_PROXY);
			if (NULL != pLogProxy) 
			{
				SG2L_CASH_USED_NOT info;
				info.iUsedCash = pBillingInfo->iCashChange;
				pLogProxy->SendPacket(G2L_CASH_USED_NOT, &info, sizeof(SG2L_CASH_USED_NOT));
			}

			if(pBillingInfo->iEventCode == EVENT_BUY_FACTION_BUFF_ITEM)
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
				if(pCenter != NULL) 
				{
					CPacketComposer Packet(G2S_FACTION_USE_BUFFITEM_NOT);
					SG2S_FACTION_USE_BUFFITEM_NOT not;
					not.iType = 0;
					Packet.Add((BYTE*)&not, sizeof(SG2S_FACTION_USE_BUFFITEM_NOT));
					pCenter->Send(&Packet);
				}
			}

			pUser->EventPayBackBuyItem(pBillingInfo->iCashChange);
			pUser->GetUserLoginPayBackEvent()->CheckUseCash(pBillingInfo->iCashChange);
			pUser->GetUserPurchaseGrade()->UseCash(pBillingInfo->iCashChange);
			pUser->GetUserReceiptEvent()->CheckUseCash(pBillingInfo);
			pUser->GetUserLovePaybackEvent()->CheckUseCash(pBillingInfo->iCashChange);
			pUser->GetUserFirstCashEvent()->CheckUseCash(pBillingInfo->iCashChange);
			pUser->GetUserDarkMarketEvent()->CheckUseCash(pBillingInfo->iCashChange, (EVENT_BUY_DARKMARKET_PACKAGE_PRODUCT == pBillingInfo->iEventCode) ? pBillingInfo->iParam2 : 0);

			WORDPUZZLES_REWARDTIME eRewardTime = (pBillingInfo->iEventCode == EVENT_CREATE_CLUB ? WORDPUZZLES_REWARDTIME_CLUBCREATE : WORDPUZZLES_REWARDTIME_NONE);
			pUser->GetUserWordPuzzle()->UpdateLetter(eRewardTime, WORDPUZZLES_REWARDTYPE_BUY_ITEM);			
			

			if(EVENT_BUY_EVENT_SHOPPINGBASKET_ITEM == pBillingInfo->iEventCode)
			{
				int iResult = BILLING_GAME.GetCashBalance(pUser);
				if( CASH_BALANCE_SUCCESS == iResult)
				{
					pUser->SendUserGold();
				}
			}
		}

		if( TRUE == bResult)
		{
			if(FS_ITEM_OP_BUY_TODAYHOTDEAL == pBillingInfo->iOperationCode)
			{
				int iPropertyValue = (pBillingInfo->iTerm != 0) ? pBillingInfo->iTerm : pBillingInfo->iParam1;

				pUser->HotDealItemCheckAndUpdateLimitCount(pBillingInfo->iItemCode, iPropertyValue);
			}
			else if(FS_ITEM_OP_BUY_EVENT_SALEPLUS == pBillingInfo->iOperationCode)
			{
				pUser->GetUserSalePlusEvent()->BuySalePlusItem_AfterPay(static_cast<BYTE>(pBillingInfo->iValue0));
			}	
		}

		if(TRUE == bResult && TRUE == pBillingInfo->bSaleRandomItemEvent)
		{
			pUser->GetUserSaleRandomItemEvent()->BuyItem(pBillingInfo->iItemCode, pBillingInfo->iPropertyValue, pBillingInfo->iItemPrice);
		}

		if(TRUE == bResult &&
			(pBillingInfo->iSellType == SELL_TYPE_CASH ||
			pBillingInfo->iSellType == SELL_TYPE_CASH_AND_POINT))
		{
			pUser->GiveEventCash_FirstCashBackEventUser(pBillingInfo->iItemPrice, pBillingInfo->iEventCode);
			pUser->GetUserCashItemRewardEvent()->GiveBox(pBillingInfo->iItemPrice, pBillingInfo->iEventCode);
		}
	}

	return bResult;
}
