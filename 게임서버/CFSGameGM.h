#ifndef _CFSGAMEGM_H_
#define _CFSGAMEGM_H_

qinclude "ThreadODBCManager.h"

#define GM_G2S_ROOMLIST_REQ			200
// None Arguments

#define GM_S2G_ROOMLIST_RES			201
//	INT		nCode	: 0 to clean room
//	{
//	UINT	uRoomNo	
//	STRING	szRoomName
//	}

#define GM_G2S_ROOMINFO_REQ			202
//	UINT	uRoomNo

#define GM_S2G_ROOMINFO_RES			203
//	UINT	uRoomNo
//	INT		nResult	: 0 success, other fail
//	{
//	STRING	szUserName
//	}

#define GM_G2S_CHAT_REQ				204
//	UINT	uRoomNo
//	STRING	szMessage
//	INT		nType	: 3 private, 0 normal
//	if (nType = 3)
//	{
//	STRING	szUserID
//	}

#define GM_S2G_CHAT_RES				205
//	UINT	uRoomNo
//	INT		nType	: 3 private, 0 normal
//	STRING	szUserID
//	STRING	szMessage

#define GM_G2S_LOGIN_REQ			206
//	STRING	GMLoginID
//	STRING	GMPassword

#define GM_S2G_LOGIN_RES			207
//	INT		nResult	: -1 fail, other success

#define GM_G2S_KICKOUT_REQ			208
//	UINT	uRoomNo
//	INT		nOpCode	: 0 kickout, 1 disconnect
//	STRING	szUserID

#define GM_S2G_KICKOUT_RES			209
//	UINT	uRoomNo

#define GM_G2S_FINDUSER_REQ			210
//	STRING	szUserID

#define GM_S2G_FINDUSER_RES			211
//	STRING	szUserID
//	INT		nResult	: -1 Not Found, 0 In Room, 1 In Lobby
//	UINT	uRoomNo
//	STRING	szRoomName

#define GM_G2S_CLOSEROOM_REQ		212
//	UINT	uRoomNo

#define GM_S2G_CLOSEROOM_RES		213
//	UINT	uRoomNo

#define GM_G2S_FORBID_USER_REQ		214
#define GM_S2G_FORBID_USER_RES		215

#define GM_G2S_CHANGE_HOST_REQ		216
#define GM_S2G_CHANGE_HOST_RES		217

#define GM_G2S_FORBID_MULITUSER_REQ	218

#endif//_CFSGAMEGM_H_