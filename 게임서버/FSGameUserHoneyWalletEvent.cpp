qinclude "stdafx.h"
qinclude "FSGameUserHoneyWalletEvent.h"
qinclude "CFSGameUser.h"
qinclude "HoneyWalletEventManager.h"
qinclude "CenterSvrProxy.h"
qinclude "GameProxyManager.h"
qinclude "RewardManager.h"

CFSGameUserHoneyWalletEvent::CFSGameUserHoneyWalletEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iUserRemainWalletDay(0)
	, m_bIsBuyHoneyWallet(FALSE)
	, m_bIsCanOpenWalletToday(FALSE)
{
}
CFSGameUserHoneyWalletEvent::~CFSGameUserHoneyWalletEvent(void)
{
}

BOOL CFSGameUserHoneyWalletEvent::Load(void)
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);
	
	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_HONEYWALLET_GetUserWalletData(m_pUser->GetUserIDIndex(), m_tWalletOpenRecentDate, m_tWalletExpireDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, DB_DATA_LOAD, FAIL, "EVENT_HONEYWALLET_GetUserWalletData failed");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	CheckCanOpenWalletToday();

	return TRUE;
}
BOOL CFSGameUserHoneyWalletEvent::IsEventOnGoing(void)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == HONEYWALLET.IsWalletOpen(), FALSE);

	if(OPEN == HONEYWALLET.IsSellOpen())
	{
		return (FALSE == m_bIsBuyHoneyWallet || m_iUserRemainWalletDay > 0);
	}
	else if(m_bIsBuyHoneyWallet && m_iUserRemainWalletDay > 0)
	{
		return TRUE;
	}

	return FALSE;	
}
void CFSGameUserHoneyWalletEvent::SendUserWalletData(void)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CheckCanOpenWalletToday();

	SS2C_HONEYWALLET_USER_INFO_RES	rs;
	ZeroMemory(&rs, sizeof(SS2C_HONEYWALLET_USER_INFO_RES));

	if(m_bIsBuyHoneyWallet)
	{
		rs.bIsOpenUserEvent	= (m_iUserRemainWalletDay > 0);

		if(m_iUserRemainWalletDay == 1 && m_bIsCanOpenWalletToday == FALSE)
		{
			rs.bIsOpenUserEvent = FALSE;	// 기간 하루남았을때 지갑 사용했으면 이벤트 종료로 간주함
		}
	}
	else if(OPEN == HONEYWALLET.IsSellOpen())
	{
		rs.bIsOpenUserEvent = TRUE;
	}
	
	rs.bIsBuyHoneyWallet		= m_bIsBuyHoneyWallet;
	rs.bIsCanOpenWalletToday	= m_bIsCanOpenWalletToday;
	rs.iUserRemainWalletDay		= m_iUserRemainWalletDay;
	
	CPacketComposer	Packet(S2C_HONEYWALLET_USER_INFO_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_HONEYWALLET_USER_INFO_RES));

	m_pUser->Send(&Packet);
}
void CFSGameUserHoneyWalletEvent::CheckCanOpenWalletToday(void)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	
	m_bIsBuyHoneyWallet = (m_tWalletExpireDate != -1);
	
	time_t tCurrentDate = _time64(NULL);

	if(m_bIsBuyHoneyWallet)
	{
		if(tCurrentDate > m_tWalletExpireDate)
		{
			m_iUserRemainWalletDay = 0;
		}
		else
		{
			CTime		ctCurrentDate(tCurrentDate);	
			CTime		ctExpireDate(m_tWalletExpireDate);
			CTimeSpan	TimeSpan = ctExpireDate - ctCurrentDate;
			m_iUserRemainWalletDay = TimeSpan.GetDays() + 1;

			if(m_iUserRemainWalletDay < 0) 
			{
				m_iUserRemainWalletDay = 0;
			}
		}		
	}
	
	if(FALSE == m_bIsBuyHoneyWallet || m_iUserRemainWalletDay <= 0)
	{
		m_bIsCanOpenWalletToday = FALSE;
	}
	else if(m_tWalletOpenRecentDate == -1)
	{
		m_bIsCanOpenWalletToday = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT OpenRecentDate;
		TIMESTAMP_STRUCT DayOfDate;		// 지갑오픈 최근날짜의 지정 리셋시간으로 설정.
		TimetToTimeStruct(m_tWalletOpenRecentDate, OpenRecentDate);
		ZeroMemory(&DayOfDate, sizeof(TIMESTAMP_STRUCT));
		DayOfDate.year	= OpenRecentDate.year;
		DayOfDate.month	= OpenRecentDate.month;
		DayOfDate.day	= OpenRecentDate.day;
		DayOfDate.hour	= HONEYWALLET.GetOpenLimitInitHour();
		
		for(time_t tTime = TimeStructToTimet(DayOfDate); tTime < tCurrentDate; tTime += (24*60*60))
		{
			if(m_tWalletOpenRecentDate <= tTime && tTime <= tCurrentDate)
			{
				m_bIsCanOpenWalletToday = TRUE;
				return;
			}
		}
		m_bIsCanOpenWalletToday = FALSE;
	}
}
BOOL CFSGameUserHoneyWalletEvent::BuyHoneyWallet_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(CLOSED == HONEYWALLET.IsSellOpen(), FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, INVALED_DATA, CHECK_FAIL, "BuyHoneyWallet_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);
	
	SS2C_HONEYWALLET_BUY_NOT	rs;
	ZeroMemory(&rs, sizeof(SS2C_HONEYWALLET_BUY_NOT));

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_HONEYWALLET_BuyWallet(m_pUser->GetUserIDIndex(), HONEYWALLET.GetWalletDayCount(), m_tWalletExpireDate, m_pUser->GetGameIDIndex(), iItemCode, iPrevCash, iPostCash, pBillingInfo->iItemPrice))
	{
		WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, LA_DEFAULT, NONE, "EVENT_HONEYWALLET_BuyWallet Fail, User:10752790, GameID:(null)", m_pUser->GetUserIDIndex(), m_pUser->GetGameID());
ult = RESULT_BUY_WALLET_FAIL;
	}
	else if(NULL == REWARDMANAGER.GiveReward(m_pUser, HONEYWALLET.GetBasicProductRewardIndex()))
	{
		WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, LA_DEFAULT, NONE, "EVENT_HONEYWALLET_BuyWallet GiveReward Fail, User:10752790, GameID:(null)", m_pUser->GetUserIDIndex(), m_pUser->GetGameID());
ult = RESULT_BUY_WALLET_FAIL;
	}
	else
	{
		CheckCanOpenWalletToday();
		rs.btResult = RESULT_BUY_WALLET_SUCCESS;
	}

	CPacketComposer	Packet(S2C_HONEYWALLET_BUY_NOT);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_HONEYWALLET_BUY_NOT));

	m_pUser->Send(&Packet);

	int iCostType = pBillingInfo->iSellType;
	m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin );
	m_pUser->SendUserGold();

	return (rs.btResult == RESULT_BUY_SUCCESS);
}
BOOL CFSGameUserHoneyWalletEvent::OpenHoneyWallet(void)
{
	CheckCanOpenWalletToday();

	SS2C_HONEYWALLET_OPEN_WALLET_RES rs;
	ZeroMemory(&rs, sizeof(SS2C_HONEYWALLET_OPEN_WALLET_RES));

	if(FALSE == m_bIsBuyHoneyWallet)
	{
		rs.btResult = RESULT_OPEN_HAVETO_BUY_WALLET_FAIL;
	}
	else if(m_iUserRemainWalletDay <= 0)
	{
		rs.btResult = RESULT_OPEN_WALLET_EXPIRE_FAIL;
	}
	else if(FALSE == m_bIsCanOpenWalletToday)
	{
		rs.btResult = RESULT_OPEN_ALREADY_TODAY_FAIL;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		rs.btResult = RESULT_OPEN_FULL_MAILBOX_FAIL;
	}
	else
	{
		int	iRewardIndex = 0;

		if(GiveWalletReward(iRewardIndex, rs.btResult))
		{
			SHoneyWalletRewardInfo sInfo;
			ZeroMemory(&sInfo, sizeof(SHoneyWalletRewardInfo));
			HONEYWALLET.GetHoneyWalletRewardInfo(iRewardIndex, sInfo);

			rs.iRewardType		= sInfo.sReward.btRewardType;
			rs.iItemCode		= sInfo.sReward.iItemCode;
			rs.iPropertyType	= sInfo.sReward.iPropertyType;
			rs.iValue			= sInfo.sReward.iPropertyValue;
		}
	}	

	CPacketComposer	Packet(S2C_HONEYWALLET_OPEN_WALLET_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_HONEYWALLET_OPEN_WALLET_RES));

	m_pUser->Send(&Packet);

	return TRUE;
}
BOOL CFSGameUserHoneyWalletEvent::GiveWalletReward(int& iRewardIndex, BYTE& btResult)
{
	BOOL bIsMainReward = FALSE;
	iRewardIndex = HONEYWALLET.GetWalletRewardRandom(bIsMainReward);

	SRewardConfig*	pReward = REWARDMANAGER.GetReward(iRewardIndex);

	if(NULL == pReward)
	{
		btResult = RESULT_OPEN_REWARD_WRONG_FAIL;
	}
	else
	{
		time_t	tOpenNowDate = _time64(NULL);
		BOOL	bIsReplay	= FALSE;
		if(UpdateUserHoneyWalletDate(iRewardIndex, tOpenNowDate, bIsMainReward, bIsReplay))
		{
			pReward = REWARDMANAGER.GiveReward(m_pUser, pReward);
			if (NULL == pReward)
			{
				btResult = RESULT_OPEN_REWARD_WRONG_FAIL;
			}
			else
			{
				btResult = RESULT_OPEN_SUCCESS;

				m_tWalletOpenRecentDate = tOpenNowDate;
				CheckCanOpenWalletToday();

				if(m_iUserRemainWalletDay == 1 && m_bIsCanOpenWalletToday == FALSE)
				{
					btResult = RESULT_OPEN_SUCCESS_EVENT_CLOSE;
				}

				if(bIsMainReward)	// 메인보상의 제한카운트가 차감됐을때
				{
					SendMainRewardShout();
				}

				return TRUE;
			}
		}
		else
		{
			if(bIsReplay)
			{
				return GiveWalletReward(iRewardIndex, btResult);
			}

			WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, LA_DEFAULT, NONE, "UpdateUserHoneyWalletDate Fail, User:10752790, GameID:(null)", m_pUser->GetUserIDIndex(), m_pUser->GetGameID());

return FALSE;
}
BOOL CFSGameUserHoneyWalletEvent::UpdateUserHoneyWalletDate(int iRewardIndex, time_t tOpenNowDate, BOOL bIsMainReward, BOOL& bIsReplay)
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	ODBCRETURN iRet = pBaseODBC->EVENT_HONEYWALLET_OpenWallet(m_pUser->GetUserIDIndex(), iRewardIndex, tOpenNowDate, bIsMainReward);

	if(ODBC_RETURN_SUCCESS != iRet)
	{
		if(iRet == -4)	// 이벤트 보상 가능 횟수가 0 이면 다시 뽑기
		{
			bIsReplay = TRUE;
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, DB_DATA_LOAD, FAIL, "EVENT_HONEYWALLET_OpenWallet failed. Return:10752790", iRet);
	return FALSE;
	}

	return TRUE;
}
void CFSGameUserHoneyWalletEvent::SendMainRewardShout()
{
	CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	if( NULL != pCenter )
	{
		SS2S_DEFAULT  info;
		strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), _countof(info.GameID)-1);
		pCenter->SendPacket(G2S_HONEYWALLET_SHOUT_REQ, &info, sizeof(SS2S_DEFAULT));
	}
}
