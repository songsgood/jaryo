qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserCheerLeader
{
public:
	CFSGameUserCheerLeader(CFSGameUser* pUser);
	~CFSGameUserCheerLeader(void);

	BOOL Load();
	void AddCheerLeader(SAvatarCheerLeader AvatarCheerLeader);
	
	BOOL CheckCheerLeader(int iCheerLeaderIndex, BYTE& btTypeNum);
	BOOL CheckCheerLeader(int iCheerLeaderIndex);
	BOOL GetCheerLeader(int iCheerLeaderIndex, SAvatarCheerLeader& sCheerLeader);
	int GetCheerLeaderEffectRemainCnt(int iCheerLeaderIndex, BYTE btTypeNum);
	int GetDefaultCheerLeaderIndex();
	BUY_EVENT_CHEERLEADER_RESULT BuyEventCheerLeader(int iCheerLeaderIndex, BYTE btTypeNum, int iPeriod);
	BOOL BuyEventCheerLeader_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);

	time_t GetRemainTime(int iCheerLeaderIndex);
	void SetClubCheerLeaderIndex(int iCheerLeaderIndex) { m_iClubCheerLeaderIndex = iCheerLeaderIndex; }
	void SetClubCheerLeaderTypeNum(BYTE btTypeNum) { m_btClubCheerLeaderTypeNum = btTypeNum; }

	int GetCheerLeaderCount() { return m_mapAvatarCheerLeader.size(); }
	void MakeCheerLeaderList(CPacketComposer& Packet, BYTE btCheerLeaderCnt);

	void CheckExpiredCheerLeader();

	int GetClubCheerLeaderIndex() { return m_iClubCheerLeaderIndex; }

private:
	CFSGameUser* m_pUser;
	BOOL		m_bDataLoaded;

	AVATAR_CHEERLEADER_MAP m_mapAvatarCheerLeader;
	int m_iClubCheerLeaderIndex;
	BYTE m_btClubCheerLeaderTypeNum;
};

