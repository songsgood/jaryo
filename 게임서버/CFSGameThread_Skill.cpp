qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSTrainingShop.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSSkillShop.h"
qinclude "CActionShop.h"
qinclude "CFSGameClient.h"
qinclude "MatchSvrProxy.h"
qinclude "CFSGameUserItem.h"
qinclude "SpecialSkinManager.h"
qinclude "HotGirlMissionEventManager.h"
qinclude "CUserAction.h"
qinclude "ShoppingFestivalEventManager.h"
qinclude "ClubConfigManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////// Process Skill ///////////////////////////////////////

void CFSGameThread::Process_FSSSkillList(CFSGameClient* pFSClient)
{
	CFSGameServer *pServer = (CFSGameServer *) pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser); 
	
	int iCategory = 0, iStep = -1, iPage = 0;
	int iSubCategory = 0;
	int iCostPage = 0;
	
	m_ReceivePacketBuffer.Read(&iCategory);

	CPacketComposer PacketComposer(S2C_SKILL_LIST_RES);
	PacketComposer.Add(iCategory);

	int iTokenCount = pUser->GetSkillTokenCount(iCategory);
	PacketComposer.Add(iTokenCount);

	int iConditionLevel = pUser->GetSkillTokenConditionLevel(iCategory);
	PacketComposer.Add(iConditionLevel);

	SAvatarInfo AvatarInfo;
	if( FALSE == pUser->GetUserSkill()->GetAvatarInfoInSkillPage(AvatarInfo)) return;
	
	if(SKILL_TYPE_TRAINING == iCategory)
	{
		m_ReceivePacketBuffer.Read(&iStep);

		CFSTrainingShop * pTrainingShop = pServer->GetTrainingShop();
		CHECK_NULL_POINTER_VOID(pTrainingShop);
		
		pTrainingShop->MakeTrainingPacket(iStep, PacketComposer, AvatarInfo);
	}
	else if(SKILL_TYPE_SKILL <= iCategory && SKILL_TYPE_FAME >= iCategory)
	{
		CFSSkillShop* pSkillShop = pServer->GetSkillShop();
		CHECK_NULL_POINTER_VOID(pSkillShop);
		int iSkillTicketCount = pUser->GetUserItemList()->GetSkillTicketCount();
		int iUserCheckLv = 0;
		if(FALSE == pUser->IsActiveLvIntervalBuffItem(ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF_SKILL, iUserCheckLv))
		{
			iUserCheckLv = 0;
		}
		
		pSkillShop->MakeSkillPacket(PacketComposer, AvatarInfo, iCategory, iSkillTicketCount, iUserCheckLv);
	}
	else if (SKILL_TYPE_SPECIALTY == iCategory)
	{
		CFSSkillShop* pSkillShop = pServer->GetSkillShop();
		CHECK_NULL_POINTER_VOID(pSkillShop);
		
		pSkillShop->MakeSpecialtySkillPacket(PacketComposer, AvatarInfo, pUser->GetUserSkill(), iCategory);
	}
	else if ( SKILL_TYPE_SPECIALSKIN == iCategory )
	{
		int iGamePosition = pUser->GetCurUsedAvatarPosition();
		list<int> listRetentionOfSkinCode;
		pUser->GetUserSpecialSkin()->GetUserRetentionOfSkilCode(listRetentionOfSkinCode);
		SPECIALSKINManager.MakePacketForSkinCardList(PacketComposer, iGamePosition, listRetentionOfSkinCode);
	}
	else
	{
		PacketComposer.Add((int)0);	//Count
	}
	
	pUser->Send(&PacketComposer);
}

// 20090815 스킬 구입 리펙토링
void CFSGameThread::Process_FSSBuySkill(CFSGameClient* pFSClient)
{
	CFSGameServer *pServer = (CFSGameServer*) pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID( pServer );
	CFSGameUser *pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );

	int iCategory = 0;
	int iKind = 0;													// Kind 1:Training, 2:Skill, 3:FreeStyle
	int iSellType = 0;												// SellType 0:Cash, 1:Point, 2:Trophy
	int iSkillIndex = 0;											// 스킬 고유 코드		
	int iSpecialtyNo = -1;
	int iErrorCode = BUY_SKILL_ERROR_GENERAL;						// 에러코드 0:성공, 11:보너스포함 성공, -값:에러코드
	BOOL bReturnValue = FALSE;
	int iConditionSkillIndex = 0;									// 선행 되어야 할 스킬 고유 코드
	int iBonusPoint = 0;
	int iNextSkillNo = 0;
	int iUseCouponPropertyKind = 0;
	BYTE btUseToken = FALSE;

	m_ReceivePacketBuffer.Read(&iCategory);
	m_ReceivePacketBuffer.Read(&iKind);
	m_ReceivePacketBuffer.Read(&iSkillIndex);
	m_ReceivePacketBuffer.Read(&iSpecialtyNo);
	m_ReceivePacketBuffer.Read(&iSellType);
	m_ReceivePacketBuffer.Read(&iUseCouponPropertyKind);
	m_ReceivePacketBuffer.Read(&btUseToken);

	CPacketComposer PacketComposer(S2C_BUY_SKILL_RES);

	///////////////////////////////// WCG ///////////////////////////////
	if( CFSGameServer::IsWCGServer() )
	{
		PacketComposer.Add(BUY_SKILL_ERROR_WCG_SERVER);
		pFSClient->Send(&PacketComposer);
		return;
	}
	/////////////////////////////////////////////////////////////////////
		
	DWORD dwStart = GetTickCount();
		
	if(SKILL_TYPE_TRAINING == iCategory && iSellType == BUY_SKILL_SELL_TYPE_POINT)			// Training은 왠만하면 다른 패킷으로 하는게 나을 듯
	{
		CFSTrainingShop* pTrainingShop = pServer->GetTrainingShop();
		CHECK_NULL_POINTER_VOID(pTrainingShop);

		if(TRUE == pUser->GetUserSkill()->BuyTraining((CFSGameODBC*)pNexusODBC, iSkillIndex, pTrainingShop, iNextSkillNo, iErrorCode, btUseToken))
		{
			iErrorCode = BUY_SKILL_ERROR_SUCCESS;
		}
	}
	else if(SKILL_TYPE_SKILL == iCategory || SKILL_TYPE_FREESTYLE == iCategory || SKILL_TYPE_FAME == iCategory ) 
	{
		CFSSkillShop* pShop = pServer->GetSkillShop();
		CHECK_NULL_POINTER_VOID(pShop);

		if(TRUE == pUser->GetUserSkill()->CheckSkillBuyCondition(pShop, iCategory, iSkillIndex, iKind, iConditionSkillIndex))
		{
			if(TRUE == pUser->GetUserSkill()->BuySkillORFreeStyle((CFSGameODBC*)pNexusODBC, iKind, iSkillIndex, iSellType, iUseCouponPropertyKind, pShop, iErrorCode, iCategory, btUseToken))
			{
				if(SKILL_TYPE_SKILL == iCategory || SKILL_TYPE_FREESTYLE == iCategory)
				{	
					pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_SKILL);
				}

				return;
			}
		}
		else
		{
			iErrorCode = BUY_SKILL_ERROR_NEED_CONDITION_SKILL;
		}
	}
	else if (SKILL_TYPE_SPECIALTY == iCategory)
	{
		CFSSkillShop* pShop = pServer->GetSkillShop();
		CHECK_NULL_POINTER_VOID(pShop);

		if(TRUE == pUser->GetUserSkill()->CheckSkillBuyCondition(pShop, iCategory, iSkillIndex, iKind, iConditionSkillIndex))
		{
			if(TRUE == pUser->GetUserSkill()->BuySpecialtySkill((CFSGameODBC*)pNexusODBC, iKind, iSkillIndex, iSpecialtyNo, iSellType, pShop, iErrorCode))
			{
				return;
			}
		}
		else
		{
			iErrorCode = BUY_SKILL_ERROR_NEED_CONDITION_SKILL;
		}
	}

	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iCategory);
	PacketComposer.Add(iKind);
	PacketComposer.Add(iSkillIndex);
	PacketComposer.Add(iSpecialtyNo);
	if (SKILL_TYPE_TRAINING == iCategory && (BUY_SKILL_ERROR_SUCCESS == iErrorCode || BUY_SKILL_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode))
	{
		PacketComposer.Add(iNextSkillNo);
	}
	if( iErrorCode == BUY_SKILL_ERROR_NEED_CONDITION_SKILL )
	{
		PacketComposer.Add(iConditionSkillIndex);
	}
	
	int iTokenCount = pUser->GetSkillTokenCount(iCategory);
	PacketComposer.Add(iTokenCount);

	pFSClient->Send(&PacketComposer);

	if (iErrorCode == BUY_SKILL_ERROR_SUCCESS)
	{
		pUser->GetUserSkill()->ProcessAchievementBuyTraining();
	}
	
	if( BUY_SKILL_ERROR_SUCCESS == iErrorCode )
	{
		pUser->SendUserStat();
		int iTicketcount = pUser->GetUserItemList()->GetSkillTicketCount();
		pUser->SendUserGold(iTicketcount);
		SendCurAvatarSkillPoint(pFSClient, pUser->GetCurAvatarRemainSkillPoint(), pUser->GetCurAvatarUsedSkillPointAllType());
	}
	
	DWORD dwEnd = GetTickCount();

	dwSkillProcess = dwStart - dwEnd;
}

void CFSGameThread::Process_FSSEnterSkillPage(CFSGameClient* pFSClient)
{
	int iSuccess = 0;
	
	CFSGameUser  * pUser = (CFSGameUser *) pFSClient->GetUser();
	if( NULL == pUser ) return;

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());

	NEXUS_CLIENT_STATE eOldState = pFSClient->GetState();
	if (FALSE == pFSClient->SetState(FS_SKILL))
	{
		iSuccess = -1;
		CPacketComposer PacketComposer(S2C_ENTER_SKILLPAGE_RES);
		PacketComposer.Add(iSuccess);
		pFSClient->Send(&PacketComposer);
		return;
	}

	CPacketComposer PacketComposer(S2C_ENTER_SKILLPAGE_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);
			
	//pUser->SendAvatarInfo();
	pUser->SendUserStat();
	int iTicketcount = pUser->GetUserItemList()->GetSkillTicketCount();
	pUser->SendUserGold(iTicketcount);
			
	pUser->GetUserSkill()->CheckSkillSlotExpired();
	pUser->SendFirstCashBackEventStatus();
	pUser->GetUserCashItemRewardEvent()->SendEventStatus();
}


void CFSGameThread::Process_FSSExitSkillPage(CFSGameClient* pFSClient)
{
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iSuccess = -1;
	
	if( pFSClient->SetState(NEXUS_LOBBY) )
	{
		iSuccess = 0;
	}
	
	CPacketComposer PacketComposer(S2C_EXIT_SKILLPAGE_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);
	
	pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());
}



void CFSGameThread::Process_FSSSkillInventoryList(CFSGameClient* pFSClient)
{
	CFSGameServer *pServer = (CFSGameServer *) pFSClient->GetServer();
	if( NULL == pServer )
	{
		return;
	}
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if( NULL == pUser ) 
	{
		return;
	}
	
	CFSSkillShop *pShop = pServer->GetSkillShop();
	if( NULL == pShop ) return;
	
	int iType;
	
	m_ReceivePacketBuffer.Read(&iType);
	
	pUser->GetUserSkill()->SendSkillListInventory(iType, pShop);
}

void CFSGameThread::Process_FSSSkillSlotList(CFSGameClient* pFSClient)
{
	CFSGameServer *pServer = (CFSGameServer *) pFSClient->GetServer();
	if( NULL == pServer )
	{
		return;
	}
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if( NULL == pUser ) 
	{
		return;
	}
	
	CFSSkillShop *pShop = pServer->GetSkillShop();
	if( NULL == pShop ) return;
	
	int iType;
	
	m_ReceivePacketBuffer.Read(&iType);
	
	pUser->GetUserSkill()->SendSkillListSlot(iType, pShop);
}


void CFSGameThread::Process_FSSInsertToSlot(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSSkillShop *pShop = CFSGameServer::GetInstance()->GetSkillShop();
	CHECK_NULL_POINTER_VOID(pShop);
	
	int iKind, iSkillNo, iConditionKind, iConditionSkillNo;
	iConditionSkillNo = iSkillNo = iKind = iConditionKind = 0;

	m_ReceivePacketBuffer.Read(&iKind);
	m_ReceivePacketBuffer.Read(&iSkillNo);
	
	CPacketComposer PacketComposer(S2C_INSERT_TO_SLOT_RES);

	int iSuccessCode = -1;
	if(FALSE == pUser->GetUserSkill()->CheckSkillSlotCondition(pShop, iKind, iSkillNo, iConditionKind, iConditionSkillNo))
	{
		iSuccessCode = INSERT_SKILL_SLOT_CONDITION_ERROR; // 선행스킬
	}
	else if( FALSE == pUser->GetUserSkill()->CheckOverLapCommandCondition(pShop, iKind, iSkillNo))
	{
		iSuccessCode = INSERT_SKILL_SLOT_OVERLAP_COMMAND_ERROR; // 커맨드 겹침
	}

	if( -1 != iSuccessCode )
	{
		PacketComposer.Add(iSuccessCode);
		PacketComposer.Add(iKind);
		PacketComposer.Add(iSkillNo);
		PacketComposer.Add(iConditionKind);
		PacketComposer.Add(iConditionSkillNo);

		pFSClient->Send(&PacketComposer);
		return;
	}
	
	if( iKind == SKILL_TYPE_SKILL )
	{
		if( pUser->GetUserSkill()->InsertToSlot( iSkillNo , pShop , iSuccessCode ) )
		{
			pUser->GetUserSkill()->SendSkillListInventory(iKind, pShop);
			pUser->GetUserSkill()->SendSkillListSlot(iKind, pShop);
			
		}
		else
		{
			PacketComposer.Add(iSuccessCode);
			PacketComposer.Add(iKind);
			PacketComposer.Add(iSkillNo);
			
		}
	}
	else if( iKind == SKILL_TYPE_FREESTYLE )
	{
		if( pUser->GetUserSkill()->InsertToSlotFreestyle( iSkillNo , pShop , iSuccessCode ) )
		{
			pUser->GetUserSkill()->SendSkillListInventory(iKind, pShop);
			pUser->GetUserSkill()->SendSkillListSlot(iKind, pShop);
			
		}
		else
		{
			PacketComposer.Add(iSuccessCode);
			PacketComposer.Add(iKind);
			PacketComposer.Add(iSkillNo);
		}
	}
	
	pFSClient->Send(&PacketComposer);
	
	SendCurAvatarSkillPoint(pFSClient, pUser->GetCurAvatarRemainSkillPoint(), pUser->GetCurAvatarUsedSkillPointAllType());
}


void CFSGameThread::Process_FSSInsertToInventory(CFSGameClient* pFSClient)
{
	CFSGameServer *pServer = (CFSGameServer *) pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSSkillShop *pShop = pServer->GetSkillShop();
	CHECK_NULL_POINTER_VOID(pShop);
	
	int iKind, iSkillNo, iOp;
	int iSuccessCode = -1;
	
	m_ReceivePacketBuffer.Read(&iKind);
	m_ReceivePacketBuffer.Read(&iOp);
	
	CPacketComposer PacketComposer(S2C_INSERT_TO_INVENTORY_RES);
	
	if( iKind == SKILL_TYPE_SKILL )
	{
		if(iOp == INSERT_INVENTORY_ONE )
		{
			m_ReceivePacketBuffer.Read(&iSkillNo);
			
			if( pUser->GetUserSkill()->InsertToInventory( iSkillNo , pShop , iSuccessCode ))
			{
				pUser->GetUserSkill()->SendSkillListInventory(iKind, pShop);
				pUser->GetUserSkill()->SendSkillListSlot(iKind, pShop);
			}
			else
			{
				PacketComposer.Add(iSuccessCode);
				PacketComposer.Add(iKind);
				PacketComposer.Add(iOp);
				PacketComposer.Add(iSkillNo);
				
			}
			
		}
		else if(iOp == INSERT_INVENTORY_ALL )
		{
			
			if( pUser->GetUserSkill()->InsertToInventoryAllSkill( iSuccessCode) )
			{
				iSuccessCode = 0;
				PacketComposer.Add(iSuccessCode);
				PacketComposer.Add(iKind);
				PacketComposer.Add(iOp);
				
			}
			else
			{
				PacketComposer.Add(iSuccessCode);
				PacketComposer.Add(iKind);
				PacketComposer.Add(iOp);
				
			}
			
		}
	}
	else if( iKind == SKILL_TYPE_FREESTYLE )
	{
		if(iOp == INSERT_INVENTORY_ONE )
		{
			m_ReceivePacketBuffer.Read(&iSkillNo);
			
			if( pUser->GetUserSkill()->InsertToInventoryFreestyle( iSkillNo , pShop , iSuccessCode ))
			{
				pUser->GetUserSkill()->SendSkillListInventory(iKind, pShop);
				pUser->GetUserSkill()->SendSkillListSlot(iKind, pShop);
			}
			else
			{
				PacketComposer.Add(iSuccessCode);
				PacketComposer.Add(iKind);
				PacketComposer.Add(iOp);
				PacketComposer.Add(iSkillNo);
				
			}
			
		}
		else if(iOp == INSERT_INVENTORY_ALL )
		{
			
			if( pUser->GetUserSkill()->InsertToInventoryAllFreestyle( iSuccessCode) )
			{
				iSuccessCode = 0;
				PacketComposer.Add(iSuccessCode);
				PacketComposer.Add(iKind);
				PacketComposer.Add(iOp);
				
			}
			else
			{
				PacketComposer.Add(iSuccessCode);
				PacketComposer.Add(iKind);
				PacketComposer.Add(iOp);
				
			}
			
		}
	}
	pFSClient->Send(&PacketComposer);
	
	SendCurAvatarSkillPoint(pFSClient, pUser->GetCurAvatarRemainSkillPoint(), pUser->GetCurAvatarUsedSkillPointAllType());
}
////////////////////////////////////////// Process Skil End /////////////////////////////////////////////

void CFSGameThread::Process_ActionShopListReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CActionShop* pActionShop = pServer->GetActionShop();
	CHECK_NULL_POINTER_VOID(pActionShop);

	CPacketComposer PacketComposer(S2C_ACTION_SHOP_LIST_RES);
	pActionShop->CopyActiontoPacket(PacketComposer, pUser->GetUserAction());

	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_ActionDetailInfoReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CActionShop* pActionShop = pServer->GetActionShop();
	CHECK_NULL_POINTER_VOID(pActionShop);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	int iActionCode = 0;
	m_ReceivePacketBuffer.Read(&iActionCode);

	CPacketComposer PacketComposer(S2C_ACTION_DETAIL_INFO_RES);
	pActionShop->CopyActionPricetoPacket(iActionCode, pAvatar->iClubLeagueSeasonRank, PacketComposer);

	pUser->Send(&PacketComposer);

	int iTicketcount = pUser->GetUserItemList()->GetSkillTicketCount();
	pUser->SendUserGold(iTicketcount);
	pUser->SendCurAvatarTrophy();
}

void CFSGameThread::Process_ActionBuyReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CActionShop* pActionShop = pServer->GetActionShop();
	CHECK_NULL_POINTER_VOID(pActionShop);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatarInfo = pUser->GetAvatarInfoWithGameID(pUser->GetGameID());
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	CUserAction* pUserAction = pUser->GetUserAction();
	CHECK_NULL_POINTER_VOID(pUserAction);

	int iActionCode = 0;
	short sUseTerm = 0;
	int iUseCouponPropertyKind = 0;
	m_ReceivePacketBuffer.Read(&iActionCode);
	m_ReceivePacketBuffer.Read(&sUseTerm);
	m_ReceivePacketBuffer.Read(&iUseCouponPropertyKind);

	CPacketComposer PacketComposer(S2C_ACTION_BUY_RES);
	
	pUser->SetUseCashStatus(TRUE);

	BOOL bClubMotion = FALSE;
	int iClubLeagueRankSaleCash = 0;
	if(TRUE == CLUBCONFIGMANAGER.IsClubMotion(iActionCode))
	{
		BYTE btSellType = CLUBCONFIGMANAGER.GetClubMotionSellType(iActionCode);
		if(SELL_TYPE_CASH != btSellType)
		{
			PacketComposer.Add((short)BUY_ACTION_NOT_EXIST_ACTION);
			pUser->Send(&PacketComposer);
			return;
		}

		int iResult = CLUBCONFIGMANAGER.CanBuyClubMotion(iActionCode, pUser->GetClubGrade(), pUser->GetClubContributionPoint());
		if(BUY_ACTION_SUCCESS != iResult)
		{		
			PacketComposer.Add((short)iResult);
			pUser->Send(&PacketComposer);
			return;
		}

		int iCashPrice = pActionShop->GetActionPrice(iActionCode, sUseTerm);
		int iSalePrice = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatarInfo->iClubLeagueSeasonRank, iCashPrice);
		if(iCashPrice - iSalePrice > 0)
			iClubLeagueRankSaleCash = iCashPrice - iSalePrice;

		bClubMotion = TRUE;
	}

	int iSaleCouponCash = 0;
	if(iUseCouponPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
		iUseCouponPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
	{
		if(CLOSED == SHOPPINGFESTIVAL.IsOpen())
		{
			PacketComposer.Add((short)BUY_ACTION_CLOSED_EVENT_SALE_COUPON);
			pUser->Send(&PacketComposer);
			return;
		}

		if(SELL_TYPE_CASH != pActionShop->GetActionSellType(iActionCode))
		{
			PacketComposer.Add((short)BUY_ACTION_NOT_USE_EVENT_SALE_COUPON);
			pUser->Send(&PacketComposer);
			return;
		}

		int iCashPrice = pActionShop->GetActionPrice(iActionCode, sUseTerm);
		if(FALSE == pUser->GetUserShoppingFestivalEvent()->CheckUseableSaleCoupon(iUseCouponPropertyKind, iCashPrice))
		{
			PacketComposer.Add((short)BUY_ACTION_NOT_USE_EVENT_SALE_COUPON);
			pUser->Send(&PacketComposer);
			return;
		}

		if(-1 == (iSaleCouponCash = SHOPPINGFESTIVAL.GetSaleCouponCash(iUseCouponPropertyKind)))
		{
			PacketComposer.Add((short)BUY_ACTION_NOT_USE_EVENT_SALE_COUPON);
			pUser->Send(&PacketComposer);
			return;
		}
	}

	SBillingInfo BillingInfo;
	if(FALSE == pActionShop->CheckBuyAction( PacketComposer, 
		iActionCode, sUseTerm, pUser->GetCurUsedAvatarSex(), pUser->GetCurUsedAvtarLv(), 
		pUser->GetCoin() + pUser->GetEventCoin(), pUser->GetSkillPoint(), pUser->GetUserTrophy(), 
		pAvatarInfo->iAvatarTeamIndex, pAvatarInfo->iSpecialCharacterIndex,
		BillingInfo.iSellType, BillingInfo.iCashChange, BillingInfo.iPointChange, BillingInfo.iTrophyChange, iSaleCouponCash, iClubLeagueRankSaleCash))
	{
		pUser->Send(&PacketComposer);
		return;
	}

	if( FALSE == pUserAction->CheckOwnUnLimitAction(iActionCode, sUseTerm) )
	{
		PacketComposer.Add((short)BUY_ACTION_UNLIMITED_RETENTION);
		pUser->Send(&PacketComposer);
		return;
	}

	BillingInfo.iEventCode = EVENT_BUY_ACTION;	
	BillingInfo.pUser = pUser;
	memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
	BillingInfo.iSerialNum = pUser->GetUserIDIndex();
	memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH + 1);	
	memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1);
	memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;

	BillingInfo.iItemCode = iActionCode;
	char* szActionName = pActionShop->GetActionName(iActionCode);
	memcpy(BillingInfo.szItemName, szActionName, MAX_ACTION_NAME+1);

	BillingInfo.iCurrentCash = pUser->GetCoin();

	BillingInfo.iParam0 = pUser->GetGameIDIndex();
	BillingInfo.iParam1 = pActionShop->GetActionCategory(iActionCode);
	BillingInfo.iParam2 = pActionShop->GetActionKind(iActionCode);
	BillingInfo.iParam3 = sUseTerm;

	if(BillingInfo.iSellType == SELL_TYPE_CASH)
	{
		BillingInfo.iCashChange -= (iSaleCouponCash+iClubLeagueRankSaleCash);
		BillingInfo.iItemPrice = BillingInfo.iCashChange;
	}
	else if(BillingInfo.iSellType == SELL_TYPE_POINT)
	{
		BillingInfo.iItemPrice = BillingInfo.iPointChange;
	}
	else if(BillingInfo.iSellType == SELL_TYPE_TROPHY)
	{
		BillingInfo.iItemPrice = BillingInfo.iTrophyChange;
	}

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		short sErrorCode = BUY_ACTION_NOT_EXIST_ACTION;
		PacketComposer.Add(sErrorCode);
		pUser->Send(&PacketComposer);
		pUser->SetUseCashStatus(FALSE);
		return;
	}

	if( BillingInfo.iCashChange > 0 && BillingInfo.iSellType == SELL_TYPE_CASH )
	{
		CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
		CHECK_NULL_POINTER_VOID( pGameODBC );

		pUser->SetItemCode( BillingInfo.iItemCode );
		pUser->SetPayCash( BillingInfo.iCashChange );
		pUser->CheckEvent(PERFORM_TIME_ITEMBUY,pGameODBC);
		pUser->ResetUserData();
	}
	pUser->SetUseCashStatus(FALSE);

	if(BillingInfo.iPointChange > 0)
	{
		pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
		pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_BUY_POINT_ITEM);
		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POINT_ITEM);
		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_USEPOINT, BillingInfo.iPointChange);

	}

	if(iUseCouponPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
		iUseCouponPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
	{
		pUser->GetUserShoppingFestivalEvent()->UseSaleCoupon(BillingInfo.iEventCode, iUseCouponPropertyKind, BillingInfo.iItemCode);
		pUser->GetUserShoppingFestivalEvent()->SendEventOpenStatus();
	}

	if(TRUE == bClubMotion)
		pUser->SendClubShopUserCashInfo();
}

void CFSGameThread::Process_UserActionInventoryListReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_USER_ACTION_INVENTORY_LIST_RES);
	pUser->CopyActionInventorytoPacket(pServer->GetActionShop(), PacketComposer);

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_UserKeyActionSlotReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());

	CPacketComposer PacketComposer(S2C_USER_KEYACTION_SLOT_RES);
	pUser->CopyKeyActionSlotPacket(PacketComposer);

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_UserSeremonySlotReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());

	CPacketComposer PacketComposer(S2C_USER_SEREMONY_SLOT_RES);
	pUser->CopySeremonySlotPacket(PacketComposer);

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_UserActionChangeReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	BYTE btCategory = 0, btStatus = 0, btSlotNum = 0;
	int iActionCode = 0;

	m_ReceivePacketBuffer.Read(&btCategory);
	m_ReceivePacketBuffer.Read(&btStatus);
	m_ReceivePacketBuffer.Read(&iActionCode);
	m_ReceivePacketBuffer.Read(&btSlotNum);

	CPacketComposer PacketComposer(S2C_USER_ACTION_CHANGE_RES);

	int iMineCeremony, iCloneCeremony; 
	iMineCeremony = iCloneCeremony = -1;
	int iPrevActionCode = iActionCode;
	if( TRUE == pUser->GetCloneCharacter() &&
		ACTION_SLOT_SEREMONY == btCategory ) // 클론캐릭터면 세레모니 코드다를 수 있슴.
	{
		pUser->GetCloneCeremonyList(iActionCode , iMineCeremony, iCloneCeremony);

		if( -1 < iMineCeremony && -1 < iCloneCeremony )
		{
			iActionCode = iMineCeremony;
		}
	}

	short sError = 1;

	pUser->ChangeAction(pServer->GetActionShop(), pUser->GetGameIDIndex(), btCategory, btStatus, iActionCode, btSlotNum, PacketComposer,sError);

	if(sError == CHANGE_ACTION_SUCCESS)
	{
		if( -1 < iMineCeremony && -1 < iCloneCeremony )
		{
			pUser->ChangeCloneDefaultSeremony(pUser->GetUseStyleIndex());
			iActionCode = iPrevActionCode; 
		}

		PacketComposer.Add(btCategory);
		PacketComposer.Add(btStatus);
		PacketComposer.Add(iActionCode);
		PacketComposer.Add(btSlotNum);
	}

	pUser->Send(&PacketComposer);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
		CHECK_NULL_POINTER_VOID(pODBCBase);

		SG2M_ACTION_SLOT_UPDATE info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.btServerIndex = _GetServerIndex;

		pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());
		pUser->CopyActionSlotPacket(pODBCBase, pServer->GetActionShop(), info);

		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ACTION_SLOT, &info, sizeof(SG2M_ACTION_SLOT_UPDATE));
	}
}

void CFSGameThread::Process_UserActionSlotReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ACTION_SLOT_REQ, &info, sizeof(info));
}

void CFSGameThread::Process_UserActionSlotResetReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_USER_ACTION_SLOT_RESET_RES);
	pUser->ResetActionSlot(pUser->GetGameIDIndex(), PacketComposer);

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_ActionRoomApplySetReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iActionCode[3] = {0};
	int iCeremonyCode[] = {-1, -1, -1, -1, -1};
	int iMotionCode[MAX_USER_MOTION_SLOT] = {0};

	m_ReceivePacketBuffer.Read((PBYTE)iActionCode, sizeof(iActionCode));
	m_ReceivePacketBuffer.Read((PBYTE)iCeremonyCode, sizeof(iCeremonyCode));
	m_ReceivePacketBuffer.Read((PBYTE)iMotionCode, sizeof(iMotionCode));

	CPacketComposer PacketComposer(S2C_USER_ACTION_SLOT_APPLYSET_RES);
	pUser->ApplyActionSlot(pUser->GetGameIDIndex(), PacketComposer
		,iActionCode, iCeremonyCode, iMotionCode);

	pUser->Send(&PacketComposer);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		//pMatch->SendActionSlot(pUser);	
		CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
		CHECK_NULL_POINTER_VOID(pODBCBase);

		SG2M_ACTION_SLOT_UPDATE info;
		SG2M_BASE infobase;

		infobase.iGameIDIndex = info.iGameIDIndex = pUser->GetGameIDIndex();
		infobase.btServerIndex = info.btServerIndex = _GetServerIndex;

		pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());
		pUser->CopyActionSlotPacket(pODBCBase, pServer->GetActionShop(), info);

		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ACTION_SLOT, &info, sizeof(SG2M_ACTION_SLOT_UPDATE));
		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TEAM_MEMBER_LIST_REQ, &infobase, sizeof(SG2M_BASE));		
	}
}

void CFSGameThread::Process_UserApplyFreeStylePreviewReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iSize = 0;
	m_ReceivePacketBuffer.Read(&iSize);

	int iPreviewCheck[FREESTYLE_PREVIEW_MAX_CHECK] = {-1, -1, -1, -1};

	for(int i = 0 ; i < iSize; ++i)
	{
		m_ReceivePacketBuffer.Read(&iPreviewCheck[i]);
	}

	CPacketComposer PacketComposer(S2C_PREVIEW_APPLY_FREESTYLE_RES);

	pUser->GetUserSkill()->ApplyPreviewSet(pUser->GetGameIDIndex()
		, PacketComposer,iPreviewCheck);

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_UserGetFreeStylePreviewReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_PREVIEW_GET_FREESTYLE_RES);

	int iPreviewCheck[FREESTYLE_PREVIEW_MAX_CHECK] = { -1, -1, -1, -1};

	pUser->GetUserSkill()->GetUseFreeStylePreview(pUser->GetGameIDIndex()
		, PacketComposer, iPreviewCheck);

	pUser->Send(&PacketComposer);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SKILL_OVERLAPCOMMAND_CHANGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSSkillShop* pSkillShop = CFSGameServer::GetInstance()->GetSkillShop();
	CHECK_NULL_POINTER_VOID(pSkillShop);

	SC2S_SKILL_OVERLAPCOMMAND_CHANGE_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_SKILL_OVERLAPCOMMAND_CHANGE_REQ));

	CPacketComposer	Packet(S2C_SKILL_OVERLAPCOMMAND_CHANGE_RES);
	SS2C_SKILL_OVERLAPCOMMAND_CHANGE_RES	rs;
	if( info.btKind == info.btConditionKind )
	{
		rs.btResult = (FALSE == (pUser->GetUserSkill()->ChangeSkillSlot( pSkillShop,  info.iSkillNo , info.btKind, info.iConditionSkillNo )) 
				? RESULT_SKILL_OVERLAPCOMMAND_CHANGE_FAIL : RESULT_SKILL_OVERLAPCOMMAND_CHANGE_SUCCESS) ; 
		
		Packet.Add((PBYTE)&rs, sizeof( SS2C_SKILL_OVERLAPCOMMAND_CHANGE_RES));
		pUser->Send(&Packet);

		pUser->GetUserSkill()->SendSkillListInventory( info.btKind, pSkillShop); // 실패했어도 refresh
		pUser->GetUserSkill()->SendSkillListSlot( info.btKind, pSkillShop);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_MOTION_SLOT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CUserAction* pUserAction = pUser->GetUserAction();
	CHECK_NULL_POINTER_VOID(pUserAction);

	CPacketComposer Packet(S2C_USER_MOTION_SLOT_RES);
	pUserAction->MakeMotionSlot(Packet);
	pUser->Send(&Packet);
}
