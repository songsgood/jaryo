qpragma once
qinclude "Lock.h"
qinclude "Singleton.h"

class CFSGameUser;
class CFSGameClient;

typedef map<int/*iUserIDIndex*/, CFSGameUser*> USERMAP;

class CFSLobby : public Singleton<CFSLobby>
{

public:
	CFSLobby();
	virtual ~CFSLobby();

private:

	BOOL			AddUser	(CFSGameUser* pUser, NEXUS_CLIENT_STATE eState); 
	BOOL			RemoveUser	(CFSGameUser* pUser, NEXUS_CLIENT_STATE eState); 

public:
	BOOL			UpdateState(CFSGameClient* pClient, NEXUS_CLIENT_STATE eState);

	void			BroadCast(CPacketComposer* pComposer, NEXUS_CLIENT_STATE eState);
	void			BroadCastNotTournamentUser(CPacketComposer* pComposer);
	void			BroadCastToClubTournament(CPacketComposer* pComposer);
	void			BroadCastToClubTournamentVoteRoom(CPacketComposer* pComposer, BYTE btRoundGroupIndex);

	void			BroadCastTeamInfo(CPacketComposer* pComposer, int iMatchSvrID, BYTE btMatchTeamScale, int iPage, int iTeamIdx, int iClubSN , int iOp, BOOL bSend=TRUE);
	void			BroadCastFreeRoomInfo(CPacketComposer* pComposer, int iMatchSvrID, int iPage, int iRoomIdx, int iOp, BOOL bSend=TRUE);

	void			BroadCastWordPuzzlesEventStatus(OPEN_STATUS eStatus);
	void			BroadCastPuzzlesStatus(OPEN_STATUS eStatus);
	void			BroadCastThreeKingdomsEventStatus(OPEN_STATUS eStatus);
	void			BroadCastMissionShopEventStatus();

	void			CheckIntensivePracticeGameTime();
	void			SetItemShopEventButton(BOOL b);
	BOOL			GetItemShopeventButton(void);

	void			BroadCastHotGirlMissionEventStatus(OPEN_STATUS eStatus);
	void			BroadCastCheerLeaderEventStatus(OPEN_STATUS eStatus);
	void			BroadCastLvUpEventStatus(OPEN_STATUS eStatus);
	void			BroadCastRandomItemBoardEventStatus(OPEN_STATUS eStatus);
	void			BroadCastEventStatus();
	void			BroadCastUserRandomItemTreeEvent(OPEN_STATUS eStatus);
	void			BroadCastUserExpBoxItemEvent(OPEN_STATUS eStatus);
	void			BroadCastUserRandomItemGroupEvent(OPEN_STATUS eStatus);
	void			BroadCastRandomCardEventStatus(OPEN_STATUS eStatus);
	void			BroadCastShoppingFestivalEventStatus(BYTE btCurrentEventType, BYTE btOpenStatus);
	void			BroadCastShoppingFestivalEventInfo(BYTE btEventType);

private:
	USERMAP			m_mapUser[END_STATE_RELATED_NOTICE];
	LOCK_RESOURCE(m_csUserMap[END_STATE_RELATED_NOTICE]);

	BOOL			m_bItemShopEventButton;
};

#define FSLOBBY CFSLobby::GetInstance()