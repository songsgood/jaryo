qinclude "stdafx.h"
qinclude "ODBCSvrProxy.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameServer.h"
qinclude <Tbuffer>
qinclude "GameOfDiceEventManager.h"
qinclude "EventDateManager.h"
qinclude "RewardManager.h"
qinclude "CenterSvrProxy.h"
qinclude "MiniGameZoneEventManager.h"
qinclude "FriendInviteManager.h"
qinclude "HippocampusEventManager.h"

#define DEFINE_ODBCPROXY_PROC_FUNC(x) void CODBCSvrProxy::Proc_##x(CReceivePacketBuffer* rp)


CODBCSvrProxy::CODBCSvrProxy(void)
{
	SetState(FS_ODBC_SERVER_PROXY);

}

CODBCSvrProxy::~CODBCSvrProxy(void)
{
}

void CODBCSvrProxy::SendFirstPacket()
{
	CPacketComposer PacketComposer(S2O_CLIENT_INFO);
	SS2O_CLIENT_INFO info;

	CFSGameServer* pServer = (CFSGameServer*)GetServer();

	info.iProcessID = pServer->GetProcessID();	
	info.iServerType = pServer->GetServerType();

	PacketComposer.Add((BYTE*)&info, sizeof(SS2O_CLIENT_INFO));

	Send(&PacketComposer);
}

void CODBCSvrProxy::ProcessPacket( CReceivePacketBuffer* recvPacket )
{
#ifndef CASE_ODBC_PROXY_PACKET_PROC
#define CASE_ODBC_PROXY_PACKET_PROC(x)	case x:		Proc_##x(recvPacket);	break;
#endif

	switch(recvPacket->GetCommand())
	{
		CASE_ODBC_PROXY_PACKET_PROC(O2S_RANKMATCH_RANK_LIST_RES);
		CASE_ODBC_PROXY_PACKET_PROC(O2S_RANKMATCH_RANK_SEASON_RECORD_RES);
		CASE_ODBC_PROXY_PACKET_PROC(O2S_RANKMATCH_USER_INFO_RES);
		CASE_ODBC_PROXY_PACKET_PROC(O2S_EVENT_GAMEOFDICE_RANK_LIST_RES);
		CASE_ODBC_PROXY_PACKET_PROC(O2S_EVENT_MINIGAMEZONE_RANK_LIST_RES);
		CASE_ODBC_PROXY_PACKET_PROC(O2S_EVENT_MINIGAMEZONE_LAST_TOP_RANKER_INFO_NOT);
		CASE_ODBC_PROXY_PACKET_PROC(O2S_EVENT_FRIENDINVITE_RANK_LIST_NOT);
		CASE_ODBC_PROXY_PACKET_PROC(O2S_EVENT_HIPPOCAMPUS_RANK_LIST_RES);
	}
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_RANKMATCH_RANK_LIST_RES)
{
	int iGameIDIndex = 0;
	
	rp->Read(&iGameIDIndex);

	CScopedRefGameUser	user(iGameIDIndex);
	if( user != NULL )
	{
		CPacketComposer Packet(S2C_RANKMATCH_RANK_LIST_RES);

		Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
		
		user->Send(&Packet);
	}	
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_RANKMATCH_RANK_SEASON_RECORD_RES)
{
	int iGameIDIndex = 0;

	rp->Read(&iGameIDIndex);

	CScopedRefGameUser	user(iGameIDIndex);
	if( user != NULL )
	{
		CPacketComposer Packet(S2C_RANKMATCH_RANK_SEASON_RECORD_RES);

		Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());

		user->Send(&Packet);
	}	
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_RANKMATCH_USER_INFO_RES)
{
	SO2S_RANKMATCH_USER_INFO_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SO2S_RANKMATCH_USER_INFO_RES));

	CScopedRefGameUser	user(rs.iGameIDIndex);
	if( user != NULL )
	{
		user->GetUserRankMatch()->UpdateRankMatchInfo(rs.base);
	}
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_EVENT_GAMEOFDICE_RANK_LIST_RES)
{
	SO2S_EVENT_GAMEOFDICE_RANK_LIST_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SO2S_EVENT_GAMEOFDICE_RANK_LIST_RES));

	GAMEOFDICEEVENT.SetLoadRank(TRUE);

	if(FALSE == rs.bAddList)
		GAMEOFDICEEVENT.ClearRankList(rs.btType);

	SEventGameOfDiceRank Rank;
	for(int i = 0; i < rs.iRankCnt; ++i)
	{
		rp->Read((PBYTE)&Rank, sizeof(SEventGameOfDiceRank));
		GAMEOFDICEEVENT.AddRankList(rs.btType, Rank);
	}

	CHECK_CONDITION_RETURN_VOID(FALSE == rs.bLastList);
	CHECK_CONDITION_RETURN_VOID(FALSE == rs.bGiveReward);

	time_t tEndTime;
	EVENTDATEMANAGER.GetClosedEventEndDate(EVENT_KIND_GAMEOFDICE, tEndTime);
	CHECK_CONDITION_RETURN_VOID(GetYYYYMMDD() != TimetToYYYYMMDD(tEndTime));

	vector<SEventGameOfDiceRank> vRank;
	GAMEOFDICEEVENT.GetRankList(rs.btType, _GetServerIndex, vRank);

	for(int i = 0; i < vRank.size(); ++i)
	{
		vector<SEventGameOfDiceRankWaitReward> vReward;
		GAMEOFDICEEVENT.GetRankRewardIndexList(rs.btType, vRank[i]._iRank, vReward);

		if(vReward.empty())
			continue;

		CScopedRefGameUser	user(vRank[i]._iGameIDIndex);
		if(user != NULL)
		{
			for(int j = 0; j < vReward.size(); ++j)
			{
				CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
				if(pODBC)
				{
					int iGameIDIndex = vRank[i]._iGameIDIndex;
					if(REWARD_TYPE_ITEM == vReward[j]._btRewardType ||
						REWARD_TYPE_ACHIEVEMENT == vReward[j]._btRewardType)
						iGameIDIndex = -1;

					if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(vRank[i]._iUserIDIndex, iGameIDIndex, vReward[j]._iRewardIndex))
						continue;

					SRewardConfig* pReward = REWARDMANAGER.GiveReward(user.GetPointer(), vReward[j]._iRewardIndex);
					if (NULL != pReward)
					{
						CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
						if(pODBC)
							pODBC->EVENT_GAMEOFDICE_RANK_InsertRewardLog(rs.btType, vRank[i]._iRank, vRank[i]._iUserIDIndex, iGameIDIndex, vRank[i]._szGameID, vReward[j]._iRewardIndex);
					}
					else
					{
						// 추후 지급
						CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
						if(pODBC)
						{
							if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(vRank[i]._iUserIDIndex, iGameIDIndex, vReward[j]._iRewardIndex))
								continue;

							pODBC->EVENT_GAMEOFDICE_InsertRankWaitGiveRewardList(vRank[i]._iUserIDIndex, iGameIDIndex, vReward[j]._iRewardIndex, rs.btType, vRank[i]._iRank);
						}
					}
				}
				else
				{
					// 추후 지급
					CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
					if(pODBC)
					{
						int iGameIDIndex = vRank[i]._iGameIDIndex;
						if(REWARD_TYPE_ITEM == vReward[j]._btRewardType ||
							REWARD_TYPE_ACHIEVEMENT == vReward[j]._btRewardType)
							iGameIDIndex = -1;

						if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(vRank[i]._iUserIDIndex, iGameIDIndex, vReward[j]._iRewardIndex))
							continue;

						pODBC->EVENT_GAMEOFDICE_InsertRankWaitGiveRewardList(vRank[i]._iUserIDIndex, iGameIDIndex, vReward[j]._iRewardIndex, rs.btType, vRank[i]._iRank);
					}
				}			
			}			
		}
		else
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
			if(pCenter)
			{
				SG2S_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ info;
				info.btType = rs.btType;
				info.iRank = vRank[i]._iRank;
				info.iUserIDIndex = vRank[i]._iUserIDIndex;
				strncpy_s(info.GameID, _countof(info.GameID), vRank[i]._szGameID, MAX_GAMEID_LENGTH);
				info.iGameIDIndex = vRank[i]._iGameIDIndex;
				info.iRewardCnt = vReward.size();

				CPacketComposer Packet(G2S_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ);
				Packet.Add((PBYTE)&info, sizeof(SG2S_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ));

				SEventGameOfDiceRankWaitReward reward;
				for(int j = 0; j < vReward.size(); ++j)
				{
					reward._btRewardType = vReward[j]._btRewardType;
					reward._iRewardIndex = vReward[j]._iRewardIndex;
					Packet.Add((PBYTE)&reward, sizeof(SEventGameOfDiceRankWaitReward));
				}
				pCenter->Send(&Packet);
			}
			else
			{
				// 추후 지급
				CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
				if(pODBC)
				{
					for(int j = 0; j < vReward.size(); ++j)
					{
						int iGameIDIndex = vRank[i]._iGameIDIndex;
						if(REWARD_TYPE_ITEM == vReward[j]._btRewardType ||
							REWARD_TYPE_ACHIEVEMENT == vReward[j]._btRewardType)
							iGameIDIndex = -1;

						if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(vRank[i]._iUserIDIndex, iGameIDIndex, vReward[j]._iRewardIndex))
							continue;

						pODBC->EVENT_GAMEOFDICE_InsertRankWaitGiveRewardList(vRank[i]._iUserIDIndex, iGameIDIndex, vReward[j]._iRewardIndex, rs.btType, vRank[i]._iRank);
					}
				}
			}
		}
	}
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_EVENT_MINIGAMEZONE_RANK_LIST_RES)
{
	SO2S_EVENT_MINIGAMEZONE_RANK_LIST_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SO2S_EVENT_MINIGAMEZONE_RANK_LIST_RES));

	if(TRUE == rs.btFirstList)
	{
		MINIGAMEZONE.ClearRankList(rs.btRecordType, rs.btSeasonType);
	}

	SEVENT_MINIGAMEZONE_RANK	sRank;
	for(int i = 0; i < rs.iRankCnt; ++i)
	{
		rp->Read((PBYTE)&sRank, sizeof(SEVENT_MINIGAMEZONE_RANK));
		MINIGAMEZONE.AddRankList(rs.btRecordType, rs.btSeasonType, sRank);
	}

	MINIGAMEZONE.CompleteRankLoad(rs.iRankListNum);

	CHECK_CONDITION_RETURN_VOID(FALSE == rs.btLastList);
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_EVENT_MINIGAMEZONE_LAST_TOP_RANKER_INFO_NOT)
{
	SO2S_EVENT_MINIGAMEZONE_LAST_TOP_RANKER_INFO_NOT	rs;
	rp->Read((PBYTE)&rs, sizeof(SO2S_EVENT_MINIGAMEZONE_LAST_TOP_RANKER_INFO_NOT));

	SEVENT_MINIGAMEZONE_RANK	sRank;
	vector<SEVENT_MINIGAMEZONE_RANK> vRank;
	for(int i = 0; i < rs.iRankerCnt; ++i)
	{
		rp->Read((PBYTE)&sRank, sizeof(SEVENT_MINIGAMEZONE_RANK));
		vRank.push_back(sRank);
	}

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	if(NULL == pODBC)
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAMEZONE_EVENT, LA_DEFAULT, NONE, "ODBC NullPtr O2S_EVENT_MINIGAMEZONE_LAST_TOP_RANKER_INFO_NOT");
		return;
	}

	// Last Season Top 1 Ranker Give Achievement

	int iRewardIndex = MINIGAMEZONE.GetRewardInfo(rs.btRecordType);

	CHECK_CONDITION_RETURN_VOID(iRewardIndex <= 0);

	vector<SEVENT_MINIGAMEZONE_RANK>::iterator iter = vRank.begin();
	vector<SEVENT_MINIGAMEZONE_RANK>::iterator iterEnd = vRank.end();
	for(; iter != iterEnd; ++iter)
	{
		if(_GetServerIndex != iter->btServerIndex)
			continue;

		pODBC->EVENT_MINIGAMEZONE_InsertGiveRewardLog((*iter), rs.btRecordType, iRewardIndex);

		if(ODBC_RETURN_SUCCESS != pODBC->EVENT_MINIGAMEZONE_UpdateWaitReward(iter->iUserIDIndex, MINIGAMEZONE.GetRewardFlag(rs.btRecordType)))
		{
			WRITE_LOG_NEW(LOG_TYPE_MINIGAMEZONE_EVENT, LA_DEFAULT, NONE, "EVENT_MINIGAMEZONE_InsertWaitRewardList Fail, UserIDIndex:10752790, RecordType:0"
 iter->iUserIDIndex, rs.btRecordType);
		}
	}
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_EVENT_FRIENDINVITE_RANK_LIST_NOT)
{
	SO2S_EVENT_FRIENDINVITE_RANK_LIST_NOT rs;
	rp->Read((PBYTE)&rs,sizeof(SO2S_EVENT_FRIENDINVITE_RANK_LIST_NOT));

	if(FALSE == rs.btAddList)
	{
		FRIENDINVITEKING.ClearInviteKingRank();
	}

	SFRIENDINVITE_KING_INFO sInfo;
	for(int i = 0; i < rs.iRankCnt; ++i)
	{
		rp->Read((PBYTE)&sInfo, sizeof(SFRIENDINVITE_KING_INFO));

		FRIENDINVITEKING.AddInviteKingRank(sInfo);
	}

	if( TRUE == rs.btLastList )
	{
		FRIENDINVITEKING.UpdateInviteKingRank(_GetServerIndex);
	}
}

DEFINE_ODBCPROXY_PROC_FUNC(O2S_EVENT_HIPPOCAMPUS_RANK_LIST_RES)
{
	SO2S_EVENT_HIPPOCAMPUS_RANK_LIST_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SO2S_EVENT_HIPPOCAMPUS_RANK_LIST_RES));

	HIPPOCAMPUS.SetLoadRank(TRUE);

	if(FALSE == rs.bAddList)
		HIPPOCAMPUS.ClearRankList(rs.btRankType);

	SEventHippocampusRank Rank;
	for(int i = 0; i < rs.iRankCnt; ++i)
	{
		rp->Read((PBYTE)&Rank, sizeof(SEventHippocampusRank));
		HIPPOCAMPUS.AddRankList(rs.btRankType, Rank);
	}

	CHECK_CONDITION_RETURN_VOID(FALSE == rs.bLastList);
	CHECK_CONDITION_RETURN_VOID(FALSE == rs.bGiveReward);

	time_t tEndTime;
	EVENTDATEMANAGER.GetClosedEventEndDate(EVENT_KIND_HIPPOCAMPUS, tEndTime);
	CHECK_CONDITION_RETURN_VOID(GetYYYYMMDD() != TimetToYYYYMMDD(tEndTime));

	vector<SEventHippocampusRank> vRank;
	HIPPOCAMPUS.GetRankList(rs.btRankType, _GetServerIndex, vRank);

	for(int i = 0; i < vRank.size(); ++i)
	{
		// 칭호
		int iRewardIndex = 0;
		HIPPOCAMPUS.GetRankRewardIndex(rs.btRankType, vRank[i]._iRank, iRewardIndex);

		if(0 == iRewardIndex)
			continue;

		CFSEventODBC* pODBC = (CFSEventODBC*) ODBCManager.GetODBC(ODBC_EVENT);
		if(NULL == pODBC)
		{
			WRITE_LOG_NEW(LOG_TYPE_HIPPOCAMPUS_EVENT, DB_CONNECT, FAIL, "O2S_EVENT_HIPPOCAMPUS_RANK_LIST_RES 1 - UserIDIndex:10752790, RewardIndex:0", vRank[i]._iUserIDIndex, iRewardIndex);
ntinue;
		}

		if(ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_RANK_CheckGiveReward(vRank[i]._iUserIDIndex, iRewardIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_HIPPOCAMPUS_EVENT, DB_CONNECT, FAIL, "O2S_EVENT_HIPPOCAMPUS_RANK_LIST_RES 2 - UserIDIndex:10752790, RewardIndex:0", vRank[i]._iUserIDIndex, iRewardIndex);
ntinue;
		}

		CScopedRefGameUser	user(vRank[i]._iGameIDIndex);
		if(user != NULL)
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(user.GetPointer(), iRewardIndex);
			if (NULL != pReward)
			{
				if(pODBC)
					pODBC->EVENT_HIPPOCAMPUS_RANK_InsertRewardLog(rs.btRankType, vRank[i]._iRank, vRank[i]._iUserIDIndex, iRewardIndex);
			}	
			else
			{
				if(pODBC)
					pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(vRank[i]._iUserIDIndex, iRewardIndex);
			}
		}
		else
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
			if(pCenter)
			{
				SG2S_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ info;
				info.btRankType = rs.btRankType;
				info.iRank = vRank[i]._iRank;
				info.iUserIDIndex = vRank[i]._iUserIDIndex;
				strncpy_s(info.GameID, _countof(info.GameID), vRank[i]._szGameID, MAX_GAMEID_LENGTH);
				info.iGameIDIndex = vRank[i]._iGameIDIndex;
				info.iRewardIndex = iRewardIndex;

				CPacketComposer Packet(G2S_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ);
				Packet.Add((PBYTE)&info, sizeof(SG2S_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ));
				pCenter->Send(&Packet);
			}
			else
			{
				if(pODBC)
					pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(vRank[i]._iUserIDIndex, iRewardIndex);
			}
		}
	}
}