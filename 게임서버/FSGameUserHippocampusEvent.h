qpragma once

qinclude "FSODBCCommon.h"

class CFSGameUser;
class CFSODBCBase;

struct SEventHippocampusUserInfo
{
	BYTE _btGrade;	// EVENT_HIPPOCAMPUS_GRADE
	int _iLv;
	int _iExp;
	int _iKeepCount;
	int _iDieCount;
	BYTE _btEnableBonusExpType;	// EVENT_HIPPOCAMPUS_BONUS_EXP_TYPE
	int _iBonusExpEnableCount;

	SEventHippocampusUserInfo()
	{
		ZeroMemory(this, sizeof(SEventHippocampusUserInfo));
	}

	void SetUserInfo(SODBCEventHippocampusUserInfo& ODBCUserInfo)
	{
		_btGrade = ODBCUserInfo._btGrade;
		_iLv = ODBCUserInfo._iLv;
		_iExp = ODBCUserInfo._iExp;
		_iKeepCount = ODBCUserInfo._iKeepCount;
		_iDieCount = ODBCUserInfo._iDieCount;
		_btEnableBonusExpType = ODBCUserInfo._btEnableBonusExpType;
		_iBonusExpEnableCount = ODBCUserInfo._iBonusExpEnableCount;
	}

	void GetUserInfo(SODBCEventHippocampusUserInfo& ODBCUserInfo)
	{
		ODBCUserInfo._btGrade = _btGrade;
		ODBCUserInfo._iLv = _iLv;
		ODBCUserInfo._iExp = _iExp;
		ODBCUserInfo._iKeepCount = _iKeepCount;
		ODBCUserInfo._iDieCount = _iDieCount;
		ODBCUserInfo._btEnableBonusExpType = _btEnableBonusExpType;
		ODBCUserInfo._iBonusExpEnableCount = _iBonusExpEnableCount;
	}
};

struct SEventHippocampusUserFood
{
	BYTE _btFoodType;	// EVENT_HIPPOCAMPUS_FOOD_TYPE
	int _iCount;

	SEventHippocampusUserFood()
	{
		ZeroMemory(this, sizeof(SEventHippocampusUserFood));
	}

	void SetUserFood(BYTE btFoodType, int iCount)
	{
		_btFoodType = btFoodType;
		_iCount = iCount;
	}
};
typedef map<BYTE/*_btFoodType*/, SEventHippocampusUserFood> HIPPOCAMPUS_USER_FOOD_MAP;

struct SEventHippocampusUserLog
{
	BYTE _btGrade;	// EVENT_HIPPOCAMPUS_GRADE
	int _iLv;
	int _iExp;
	int _iDieCount;

	SEventHippocampusUserLog()
	{
		ZeroMemory(this, sizeof(SEventHippocampusUserLog));
	}

	void SetUserLog(SODBCEventHippocampusUserLog& ODBCUserLog)
	{
		_btGrade = ODBCUserLog._btGrade;
		_iLv = ODBCUserLog._iLv;
		_iExp = ODBCUserLog._iExp;
		_iDieCount = ODBCUserLog._iDieCount;
	}

	void GetUserLog(SODBCEventHippocampusUserLog& ODBCUserLog)
	{
		ODBCUserLog._btGrade = _btGrade;
		ODBCUserLog._iLv = _iLv;
		ODBCUserLog._iExp = _iExp;
		ODBCUserLog._iDieCount = _iDieCount;
	}

	void MakeUserLog(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT& not)
	{
		not.btGrade = _btGrade;
		not.iLv = _iLv;
		not.iExp = _iExp;
		not.iDieCount = _iDieCount;
	}
};

class CFSGameUserHippocampusEvent
{
public:
	CFSGameUserHippocampusEvent(CFSGameUser* pUser);
	~CFSGameUserHippocampusEvent(void);

	BOOL Load();
	void SetUserFood(vector<SODBCEventHippocampusUserFood>& vFood);
	void SetUserLog(BYTE btGrade, int iLv, int iExp, int iDieCount);

	void SendEventInfo();
	void SendRewardInfo();
	RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD EatFoodReq(BYTE btFoodType, SS2C_EVENT_HIPPOCAMPUS_EAT_FOOD_RES& rs);
	RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP GetBonusExpReq(BYTE btBonusExpType, SS2C_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_RES& rs);
	void GiveWaitGiveReward();
	void SendRankInfo(BYTE btRankType, BYTE btServerIndex);
	void MakeIntegrateDB_UpdateUserLog(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT& not);
	void MakeGiveFood(CPacketComposer& Packet, int& iCount, BYTE btMatchType, BOOL bIsDisconnectedMatch, BOOL bRunAway);
	void TestAddFood();

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	SEventHippocampusUserInfo m_UserInfo;
	HIPPOCAMPUS_USER_FOOD_MAP m_mapUserFood;
	SEventHippocampusUserLog m_UserLog;
};

