// CFSGameUserSkill.h: interface for the CFSGameUserSkill class.
//
// 유저 스킬 관리
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CFSGAMEUSERSKILL_H__D18E23FA_DB0F_472B_B346_91D6F5882AD8__INCLUDED_)
#define AFX_CFSGAMEUSERSKILL_H__D18E23FA_DB0F_472B_B346_91D6F5882AD8__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "Lock.h"
qinclude "AvatarSpecialtySkill.h"
qinclude "ThreadODBCManager.h"

class CFSGameUser;
class CFSSkillShop;
class CFSTrainingShop;

struct SBillingInfo;

class CFSGameUserSkill : public CAvatarSpecialtySkill
{
public:
	CFSGameUserSkill();
	virtual ~CFSGameUserSkill();

	BOOL			GetAvatarInfoInSkillPage(SAvatarInfo & AvatarInfo);

	// 20090805 채널링 리펙토링 추가 함수
	void			SetUserBillResultAtMem( CFSGameUser* pUser, int iSellType, int iPostMoney, int iSkillPriceCash, int iSkillPricePoint,
											 int iSkillPriceTrophy, int iBonusCash );
	BOOL			CheckPayAbiility( int &iSellType, SSkillBase* pSkillBase, int& iSkillPriceCash,
									int& iSkillPricePoint, int& iSkillPriceTrophy, int iSaleCouponCash, int &iErrorCode );
	BOOL			GetDBColumnAndShiftBit( SAvatarInfo *pAvatarInfo, int iSkillIndex, int &iStorageType, int &iSkillShiftBit, int iKind );
	BOOL			BuySkillProcess_BeforePay( SAvatarInfo *pAvatarInfo, SSkillInfo &SkillInfo, int &iErrorCode );
	BOOL			BuySkillORFreeStyle(CFSGameODBC* pFSODBC, int iSkillKind, int iSkillIndex, int iSellType, int iUseCouponPropertyKind, CFSSkillShop* pSkillShop, int &iErrorCode, int iCategory, BYTE btUseToken);
	BOOL			BuySpecialtySkill(CFSGameODBC* pFSODBC, int iSkillKind, int iSkillIndex, int iSpecialtyNo, int iSellType, CFSSkillShop* pSkillShop, int &iErrorCode);
	BOOL			BuySkillProcess_AfterPay( SBillingInfo *pBillingInfo, int iPayResult );
	BOOL			BuySpecialtySkill_AfterPay( SBillingInfo *pBillingInfo, int iPayResult );

	void			InsertSkill(char *szGameID, int iSkillIndex, int iSkillShiftBit, BOOL bFameSkill);	
	void			InsertFreestyle(char *szGameID ,  int iFreestyleNo , int iFreestyle, BOOL bFameSkill);

	BOOL			InsertToSlot(int iSkillNo, CFSSkillShop *pSkillShop , int & iErrorCode , int iSlot = -1);
	BOOL			InsertToInventory( int iSkillNo , CFSSkillShop *pSkillShop, int & iErrorCode, int iSlot = -1);

	BOOL			InsertToInventoryAllSkill(int & iErrorCode, int iMode = 0 , int iStartSlotNumber = 0  );
	BOOL			InsertToSlotFreestyle(int iFreestyleNo, CFSSkillShop *pSkillShop , int & iErrorCode , int iSlot = -1);
	BOOL			InsertToInventoryFreestyle(int iFreestyleNo , CFSSkillShop *pSkillShop, int & iErrorCode, int iSlot = -1);
	BOOL			InsertToInventoryAllFreestyle(int & iErrorCode, int iMode = 0 , int iStartSlotNumber = 0  );
	int				FindEmptySlot(int iSlot , int *iaUserSkill, int iMaxSlotNum);
	int				FindUsedSlot(int iSkillNo , int *iaUserSkill, int iMaxSlotNum);

	void			SetGameUser(CFSGameUser * pUser) { m_pUser = pUser; };
	virtual			CUser* GetUser()		{	return (CUser*)m_pUser; };
	// 2010.01.12
	// Modify for Migration 2005
	// ktKim
	void			ConvertItemSlotToString(char *szUseItem, size_t sizeOfBuffer, int *iaUseSkill, int *iaUseSkillType);
	// End

	BOOL			SendSkillListInventory( int iType, CFSSkillShop *pSkillShop );
	BOOL			SendSkillListSlot( int iType, CFSSkillShop *pSkillShop );

	BOOL			CheckSkillSlotCondition(CFSSkillShop *pSkillShop, int iType, int iSkillNo, int& iConditionType, int& iConditionSkillNo);
	BOOL			CheckSkillBuyCondition(CFSSkillShop* pSkillShop, int iCategory, int iSkillNo, int iType, int &iConditionSkillNo);
	BOOL			CheckOverLapCommandCondition(CFSSkillShop* pSkillShop, int iKind, int iSkillNo);
	BOOL			ChangeSkillSlot(CFSSkillShop* pSkillShop, int iSkillNo, int iKind, int iSwapSkillNo);


	bool			CheckSkillSlotExpired();
	short			GetRemainDate(SDateInfo &DateInfo);

	bool			CheckSlotOverSkill(CFSGameODBC *pFSODBC = NULL);
	BOOL			ApplyBagSkill( CFSSkillShop* pSkillShop, int iSkillKind, short nSkillCnt, int* iaUseSkill, short nFreeStyleCnt = 0, int* iaUseFreeStyle = NULL);
	BOOL			GetSlotSkillList( CFSSkillShop *pSkillShop, int iSkillType, CPacketComposer& PacketComposer );
	BOOL			GetInventorySkillList( CFSSkillShop *pSkillShop, int iSkillType, CPacketComposer& PacketComposer );
	BOOL			BuyTraining(CFSGameODBC *pFSODBC, int iTrainingNo , CFSTrainingShop *pTrainigShop, int& iNextSkillNo, int & iErrorCode, BYTE btUseToken);

	/////////////////// Preview	///////////////////////////
	void			ApplyPreviewSet(int iGameIDIndex, CPacketComposer& PacketComposer, int iPreview[]);
	void			GetUseFreeStylePreview(int iGameIDIndex, CPacketComposer& PacketComposer, int iPreview[]);

	/////////////////// WCG ///////////////////////////////
	void			LoadSkillForWCG();
	///////////////////////////////////////////////////////
	int				GetUsedSkillPointAllType();
	int				GetUsedSkillPoint(int iType);
	int				GetFameSkillStatus(int* iStat);
	int				GetFameSkillAddMinusStatus(int* iAddStat, int* iMinusStat);

	void			GetTrainingCountsforAchievement(SAvatarInfo* pAvatarInfo, vector<int>& vTraingCounts);

	void			ProcessAchievementBuySkill( CFSGameODBC *pFSODBC, CFSSkillShop *pSkillShop, char *szGameID);
	void			ProcessAchievementBuyFreeStyle( CFSGameODBC *pFSODBC, CFSSkillShop *pSkillShop, char *szGameID);
	void			ProcessAchievementBuyTraining();

	void			InsertFreestyleByPotentialComponent(CFSSkillShop* pSkillShop, char *szUserID, int iUserIDIndex, int iGameIDIndex, char* szGameID, int iStorageType,
														int iSkillNo, int iSkillShiftBit, int iSkillPricePoint, int iPrevMoney, int iPostMoney, int iSkillPriceTrophy, int iCondition);
	void			DeleteFreestyleByPotentialComponent(CFSSkillShop* pSkillShop, int iSkillNo);
	BOOL			DeletePotentialFreestyle(int iFreestyleNo , CFSSkillShop *pSkillShop, int iSlot = -1);

//
public:
	/////////////////// WCG ///////////////////////////////
	bool			m_bSetSkillForWCG;
	///////////////////////////////////////////////////////

private:
	CFSGameUser*	m_pUser;
	
	LOCK_RESOURCE(m_SyncObject); 
};

#endif // !defined(AFX_CFSGAMEUSERSKILL_H__D18E23FA_DB0F_472B_B346_91D6F5882AD8__INCLUDED_)
