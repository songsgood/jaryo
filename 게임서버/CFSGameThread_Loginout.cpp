qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CFSGameUserItem.h"
qinclude "CenterSvrProxy.h"
//qinclude "ODBCSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "ChatSvrProxy.h"
qinclude "CFSSvrList.h"
qinclude "CChannelBuffManager.h"
qinclude "LobbyChatUserManager.h"
qinclude "ClubTournamentAgency.h"
qinclude "MatchingPoolCareManager.h"
qinclude "LobbyClubUserManager.h"
qinclude "XignCodeManager.h"
qinclude "UserMissionEvent.h"
qinclude "ShutdownUserManager.h"
qinclude "ComebackBenefitManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CFSGameThread::Process_FSLogin( CFSGameClient * pClient )
{
	// 20090402 Write Login Request Count Log
	CFSGameServer *pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	if(TRUE == pServer->IsUseLoginRequestLog())
	{
		pServer->IncreaseLoginRequestCount();
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, NONE, "Request Count:11866902", pServer->GetLoginRequestCount());
	
	DECLARE_INIT_TCHAR_ARRAY(szUserID, MAX_USERID_LENGTH+1);
	int iPublisherIDIndex = -1;
	DECLARE_INIT_TCHAR_ARRAY(szPublisherUserNo,MAX_PUBLISHER_USERNO_LENGTH+1);
	int iHaofangConnect = 0;
	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szPassword,	MAX_PASSWORD_LENGTH + 1);
	DECLARE_INIT_TCHAR_ARRAY(szUKey, MAX_USERKEY_LENGTH +1);
	DECLARE_INIT_TCHAR_ARRAY(szSessionKey, MAX_SESSIONKEY_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szGameKey, MAX_GAMEKEY_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szLocalIPAddress, MAX_IPADDRESS_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szLoginTime, MAX_LOGINTIME_LENGTH+1);
	
	m_ReceivePacketBuffer.Read((PBYTE) szUserID, sizeof(char) * (MAX_USERID_LENGTH + 1));
	m_ReceivePacketBuffer.Read(&iPublisherIDIndex);
	m_ReceivePacketBuffer.Read((PBYTE) szPublisherUserNo,sizeof(char) * (MAX_PUBLISHER_USERNO_LENGTH+1));
	m_ReceivePacketBuffer.Read(&iHaofangConnect);
	m_ReceivePacketBuffer.Read((PBYTE) szGameID, sizeof(char) * (MAX_GAMEID_LENGTH  + 1));
	m_ReceivePacketBuffer.Read((PBYTE) szPassword, sizeof(char) * (MAX_PASSWORD_LENGTH  + 1));
	m_ReceivePacketBuffer.Read((PBYTE) szUKey, sizeof(char) * (MAX_USERKEY_LENGTH + 1));
	m_ReceivePacketBuffer.Read((PBYTE) szSessionKey, sizeof(char) * (MAX_SESSIONKEY_LENGTH + 1));
	m_ReceivePacketBuffer.Read((PBYTE) szGameKey, sizeof(char) * (MAX_GAMEKEY_LENGTH + 1));
	m_ReceivePacketBuffer.Read((BYTE *) szLocalIPAddress, sizeof(char) * (MAX_IPADDRESS_LENGTH + 1));

	int  iPCStation = 0;	
	m_ReceivePacketBuffer.Read(&iPCStation); //ToDo - Delete PCStation

	m_ReceivePacketBuffer.Read((PBYTE) szLoginTime, sizeof(char) * (MAX_LOGINTIME_LENGTH + 1));
	
	szGameID[MAX_GAMEID_LENGTH] = 0;
	szSessionKey[MAX_SESSIONKEY_LENGTH] = 0;
	szGameKey[MAX_GAMEKEY_LENGTH] = 0 ;
	szLocalIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	szLoginTime[MAX_LOGINTIME_LENGTH] = 0;
	szUKey[MAX_USERKEY_LENGTH] = 0;
	
	int	iUserIDIndex = -1;

	WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, NONE, "Address:00B51316 , GameID((null))", pClient, szGameID);
wStart = GetTickCount();
	DWORD dwCheck0 = 0, dwCheck1 = 0, dwCheck2 = 0;
	if(0 != strncmp(szGameKey, GAMEKEY, MAX_GAMEKEY_LENGTH)) 
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "0 != strncmp(szGameKey, GAMEKEY, MAX_GAMEKEY_LENGTH)");
		SendLoginResult(pClient, LOGIN_FAIL_VER_ERROR);
		return;
	}

	if(1 > strlen(szUserID) || 1 > strlen(szGameID))
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "1 > strlen(szUserID) || 1 > strlen(szGameID)");
		SendLoginResult(pClient, LOGIN_FAIL_NOTEXISTIDORPASS);
		return;
	}
	
	if(NULL != pServer)
	{
		if(pServer->GetCurrentUserCount() >= pServer->GetMaxUserNum())
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "pServer->GetCurrentUserCount() >= pServer->GetMaxUserNum()");
			SendLoginResult(pClient, LOGIN_FAIL_FAILCHANNEL);
			return;
		}
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "0 != strncmp(szGameKey, GAMEKEY, MAX_GAMEKEY_LENGTH)");
		return;
	}
	
	CFSGameUser *pUser = (CFSGameUser *)pClient->GetUser();
	if(NULL == pUser)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "CFSGameUser *pUser == NULL");
		return;
	} 

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	
	if(NULL == pNexusODBC)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "NULL == pNexusODBC");
		return;
	}

	CFSLoginGlobalODBC* pLoginGlobalODBC = (CFSLoginGlobalODBC*)ODBCManager.GetODBC( ODBC_LOGINGLOBAL );
	if( NULL == pLoginGlobalODBC )
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "NULL == pLoginGlobalODBC");
		return;
	}

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
	CHECK_NULL_POINTER_VOID( pLogODBC );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC( ODBC_CLUB );
	CHECK_NULL_POINTER_VOID( pClubBaseODBC );

	// todo pcroom old code
	CFSPCRoomODBC* pPCRoomODBC = nullptr;
	if(FALSE == CFSGameServer::GetInstance()->GetNewPCRoom())
	{
		pPCRoomODBC = (CFSPCRoomODBC*)ODBCManager.GetODBC( ODBC_PCROOM );
	}
	//CHECK_NULL_POINTER_VOID( pPCRoomODBC );

	int iGameIDIndex = -1;
	int iGameIDIndexResult = -1;
	if(ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->spGetGameIDIndex( szGameID , iGameIDIndex, iGameIDIndexResult))
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "spGetGameIDIndex()");
		return;
	}

	if(iGameIDIndexResult == -1)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "iGameIDIndexResult == -1");
		return;
	}

	pUser->SetGameIDIndex(iGameIDIndex);
	pUser->SetPublisherIDIndex(iPublisherIDIndex);
	pUser->SetPublisherUserNo(szPublisherUserNo);
	
	int iErrorCode		= 0;
	
	pUser->SetIsLocalPubliser(FALSE);

	//short sAdult = 1;
	//char szIDName[MAX_USERIDNUMBER_LENGTH+1] = {NULL};
	//if( ODBC_RETURN_SUCCESS != pLoginGlobalODBC->spLoginEx( szUserID, iHaofangConnect, szPassword, &iUserIDIndex, &iErrorCode, &sAdult, szIDName) )
	//{
	//	WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "m_pLoginGlobalODBC->spLoginEx");
	//	return;
	//}
	//통합 - 작업대기 로그인
	if ((KOREA_PUBLISHER_JOYCITY == iPublisherIDIndex && TRUE == BILLING_GAME.NTreev_IsBillingEnable()) ||
		KOREA_PUBLISHER_NAVER == iPublisherIDIndex)
	{
		int iInputPublisherUserNo = atoi(szPublisherUserNo);
		int iReturnValue = 0;

		if(	ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->Publisher_GetMappingUserIDIndex( iInputPublisherUserNo, iUserIDIndex, iReturnValue ))
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, DB_DATA_LOAD, FAIL, "Publisher_GetMappingUserIDIndex. PublisherUserIDIndex(11866902) UserIDIndex(0) Return(18227200)", 
iInputPublisherUserNo, iUserIDIndex, iReturnValue );
			SendLoginResult( pClient, LOGIN_FAIL_KORBILLING );
			return;
		}
	}
	else
	{
		if(ODBC_RETURN_SUCCESS != pLoginGlobalODBC->spGetUserIDIndex(szUserID, iPublisherIDIndex, iUserIDIndex, &iErrorCode))
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, DB_DATA_LOAD, FAIL, "spGetUserIDIndex. UserID(��4), PublisherIDIndex(0)", szUserID, iPublisherIDIndex);
			SendLoginResult(pClient,LOGIN_FAIL_FAILCHANNEL);
			return;
		}
	}

	pUser->SetAbleSaleItem(TRUE);
	
	pUser->SetGameID(szGameID);
	pUser->SetUserID(szUserID);
	pUser->SetSessionKey(szSessionKey);
	
	// 20081002 계정과 케릭터 검사
	int iUserCertRet = -1;
	if(ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->USER_UserCert(iUserIDIndex, iGameIDIndex, iUserCertRet))
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "USER_UserCert - UserIDIndex(11866902), GameIDIndex(0)", iUserIDIndex, iGameIDIndex);
dLoginResult(pClient, LOGIN_FAIL_LOADAVATAR);
		return;
	}
	else if(iUserCertRet != 0)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Hack - UserIDIndex(11866902)", iUserIDIndex);
LoginResult(pClient, LOGIN_FAIL_EXISTSAMEID);
		return;
	}

	dwCheck0 = GetTickCount();

	BEGIN_LOCK
		// Check Multi Login User
		CScopedRefGameUser pPrevUser(szGameID);
		if (pPrevUser != NULL)
		{
			// Disconnect Multi Login User
			if(pPrevUser != pUser)
			{
				CFSGameClient *pGetClient = (CFSGameClient*)pPrevUser->GetClient();
				if(NULL != pGetClient)
				{
					if(TRUE == CFSGameServer::GetInstance()->IsCheckLog())
					{
						WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Duplicated User - GameID(��4), UserID((null))", szGameID, szUserID);
}
					pGetClient->DisconnectClient();
				}

				SendLoginResult(pClient, LOGIN_FAIL_EXISTSAMEID);

				WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Exist Same User - UserID(��4)", szGameID);
		}
			else
			{
				SendLoginResult(pClient, LOGIN_FAIL_ALREADYCONNECT); 

				WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "User Already In Server - UserID(��4)", szUserID); 	
			return;
			}
		}
	END_LOCK;

	DECLARE_INIT_TCHAR_ARRAY(szDBSessionKey, MAX_SESSIONKEY_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szDBGameID, MAX_GAMEID_LENGTH+1);
	USER_STATE eUserState = USER_STATE_CONNECT;
	int iProcessID = 0;
	int iChannelID = INVALID_IDINDEX;
	int iBirthDay = 0;
	int iSafeConstantFirstChkSum = 0;

	ODBCRETURN Result = pLogODBC->USER_GetSession(iUserIDIndex, szDBSessionKey, szDBGameID, eUserState, iProcessID, iChannelID, iBirthDay, iSafeConstantFirstChkSum, iErrorCode);
	if(Result == ODBC_RETURN_SUCCESS)
	{
		pUser->SetSafeConstantFirstChkSum(iSafeConstantFirstChkSum);
		pUser->SetUserIDIndex(iUserIDIndex);
		pUser->SetBirthDay(iBirthDay);
		pUser->SetShutdownTime();

		// SessionKey 인증
		if(strcmp(pUser->GetSessionKey(), szDBSessionKey) != 0)
		{
			SendLoginResult(pClient, LOGIN_FAIL_ADDUSER);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Not Same UserSessionKey - UserID(��4), SessionKey((null))", szUserID, szSessionKey); 
turn;
		}

		if(eUserState != USER_STATE_MOVING)
		{
			SendLoginResult(pClient, LOGIN_FAIL_NOTEXIST_MOVINGLOCATION);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "UserState - UserID(��4), UserState(0)", szUserID, eUserState); 
			return;
		}
		// Add User
		pServer->LockUserManager();
		if(!pServer->AddUser((CUser *)pUser)) 
		{
			pServer->UnlockUserManager();

			SendLoginResult(pClient, LOGIN_FAIL_ALREADYCONNECT);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Add UserMap - UserID(��4)", szUserID); 
		return; 
		}
		pServer->UnlockUserManager();

		// Succeed Login
		pClient->SetState(NEXUS_LOGINSUCCESS);

		DECLARE_INIT_TCHAR_ARRAY(szIPAddress, MAX_IPADDRESS_LENGTH+1);
		pClient->GetIPFrom(szIPAddress, _countof(szIPAddress));
	
		SFSLoginInfo LoginInfo;
		if(ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->spFSGetLoginUserInfo(iUserIDIndex, LoginInfo))  
		{
			SendLoginResult(pClient, LOGIN_FAIL_LOADAVATAR);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "spFSGetLoginUserInfo - UserID(��4)", szUserID); 
		return;
		}
		pUser->SetUserLoginInfo(LoginInfo);

		int iResult = BILLING_GAME.GetCashBalance(pUser);
		if( CASH_BALANCE_FAIL_LOCAL == iResult)
		{				
			SendLoginResult(pClient, LOGIN_FAIL_LOADAVATAR);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "GetCashBalance - UserID(��4)", szUserID); 
		return;
		}
		if(pServer->IsTournamentEnable())
		{
			int iTroPhyCount = 0;
			int iTournamentChampionCnt = 0;
			if(ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->Trophy_GetTrophyAndChampionCnt(iUserIDIndex, iGameIDIndex, iTournamentChampionCnt, iTroPhyCount))
			{
				SendLoginResult(pClient, LOGIN_FAIL_LOADAVATAR);
				WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Trophy_GetTrophyAndChampionCnt - UserID(��4)", szUserID);

			return;
			}
			pUser->SetUserTrophy(iTroPhyCount);
			pUser->SetTournamentChampionCnt(iTournamentChampionCnt);
			pUser->SendTournamentChampionCnt();
		}
		
		// 20090513 GM여부 판별
		int iGMRet = -1;
		if(ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->GM_CheckGMUser(iUserIDIndex, iGMRet))
		{
			if(iGMRet == 0)
			{
				pUser->SetGMUser(TRUE);
			}
		}

		int iCheatAble = 0;
		if(ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->USER_CheckCheatAble(iUserIDIndex, iCheatAble))
		{
			if(1 == iCheatAble)
			{
				pUser->SetCheatAble(TRUE);
			}
		}	

		// todo pcroom old code
		if(FALSE == CFSGameServer::GetInstance()->GetNewPCRoom() && 
			pPCRoomODBC != NULL)
		{
			int iGameCode = PCROOMDB_FS_GAMECODE;
			int	iPSN = 0;
			int iProductCode = 0;
			int iPCRoomCheckRet = -1;
			if(ODBC_RETURN_SUCCESS == pPCRoomODBC->CHECK_PCBANG(iGameCode, szIPAddress, iPSN, iProductCode, iPCRoomCheckRet))
			{
				pUser->SetPCRoomNum(iPSN);
				pUser->SetPCRoomProductCode(iProductCode);
				pUser->SetPCRoomCheckData(iPCRoomCheckRet);
			}
		}	
		/////////////////////////////////////

		if(ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->USER_CheckUseableBonusCoinUser(iUserIDIndex))
		{
			pUser->SetUseableBonusCoinUser(TRUE);
		}

		char szAccountID[FriendAccount::MAX_ACCOUNT_ID_LENGTH + 1] = {'\0',};
		if(ODBC_RETURN_SUCCESS == pGameODBC->FRIENDACCOUNT_GetUserData(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), szAccountID, pUser->GetUserFriendAccountInfo()))
		{
			pUser->SetAccountID(szAccountID);
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "FRIENDACCOUNT_GetUserData Fail, UserIDIndex=11866902", pUser->GetUserIDIndex());

	if(FALSE == pUser->LoadAvatarList(szGameID, pServer->GetEqualizeLv()))
		{
			SendLoginResult(pClient, LOGIN_FAIL_LOADAVATAR);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "LoadAvatarList - UserID(��4)", szUserID); 
		return;
		}
		
		pUser->CheckBindAccountInfo();	//Must be call after LoadAvatarList 
		pUser->LoadBindaccountItem();

		int iRemainUserExamCourt = 0;
		float fRatingPoint = RATING_DEFAULT;
		if(ODBC_RETURN_SUCCESS != pGameODBC->MATCHMAKING_GetRatingInfo(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), RATING_CODE_NORMAL_3ON3, fRatingPoint, iRemainUserExamCourt))
		{
			fRatingPoint = RATING_DEFAULT;
		}
		pUser->SetRatingPoint(fRatingPoint);
		pUser->SetLoginRating(fRatingPoint);

		int iEquippedAchievementTitle = -1;
		if (FALSE == pUser->LoadAvatarAchievementData( pUser->GetGameIDIndex(), iEquippedAchievementTitle))
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, NONE, "LoadAvatarAchievementData FAILED - UserID=��4", szUserID);
	}
		else
		{
			if (iEquippedAchievementTitle != -1)
			{
				pUser->SetEquippedAchievementTitle(iEquippedAchievementTitle);
			}
		}

		pUser->LoadUserFaction();
		pUser->GetUserFriendInvite()->SendCenterSvrMyFriendListMissionReq();

		// 20101021 로그인이벤트로 받은 포인트 반영 - 주의! 위치를 옴길때 제대로 동작하지 않을 수 있습니다.
		int iSkillPoint = 0;
		if(ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->spUserGetUserPointInfo(iUserIDIndex, iSkillPoint))  
		{
			pUser->SetSkillPoint(iSkillPoint);
			pUser->AddPointAchievements(); 
		}

		dwCheck1 = GetTickCount();

		pUser->SetIPAddress(szIPAddress);

		int iUserType = 0;
		if(ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->USER_GetUserType(iUserIDIndex, iUserType))
		{
			CPacketComposer PacketComposer(S2C_ACCOUNT_TYPE_RES);

			pUser->SetUserType(iUserType);

			PacketComposer.Add(iUserType);

			int	iClientFunction[CLIENT_FUNCTION_MAX_NUM] = {0,};

			if(iUserType LIENT_PLUS_TYPE_NEW ==  CLIENT_ACCOUNT_TYPE_NORMAL)
)
			{
				iClientFunction[CLIENT_CHARACTER_SLOT_BUY] = 1;
				iClientFunction[CLIENT_CHARACTER_CREATE] = 1;
				iClientFunction[CLIENT_CHARACTER_DELETE] = 1;
				iClientFunction[CLIENT_ITEM_BUY] = 1;
				iClientFunction[CLIENT_ITEM_PRESENT] = 1;
			}
			else if(iUserType LIENT_PLUS_TYPE_NEW == CLIENT_ACCOUNT_TYPE_EXPERIENCE)
)
			{
				iClientFunction[CLIENT_CHARACTER_SLOT_BUY] = 0;
				iClientFunction[CLIENT_CHARACTER_CREATE] = 0;
				iClientFunction[CLIENT_CHARACTER_DELETE] = 0;
				iClientFunction[CLIENT_ITEM_BUY] = 1;
				iClientFunction[CLIENT_ITEM_PRESENT] = 1;
			}
			else
			{
				iClientFunction[CLIENT_CHARACTER_SLOT_BUY] = 0;
				iClientFunction[CLIENT_CHARACTER_CREATE] = 0;
				iClientFunction[CLIENT_CHARACTER_DELETE] = 0;
				iClientFunction[CLIENT_ITEM_BUY] = 0;
				iClientFunction[CLIENT_ITEM_PRESENT] = 0;
			}

			for(int i = 0; i < CLIENT_FUNCTION_MAX_NUM; i++)
			{
				PacketComposer.Add(iClientFunction[i]);
			}

			pClient->Send(&PacketComposer);

		}

		//채널. 로비에 강제로 넣는다.
		if(TRUE == pClient->SetState(NEXUS_LOBBY))
		{
			pUser->SendBindAccountInfo();
			SendLoginResult(pClient, 0);

			//강제종료 체크하자
			SAvatarInfo AvatarInfo;
			if(pUser->CurUsedAvatarSnapShot(AvatarInfo))
			{
				int nDisconnectCause = AvatarInfo.iDisconnected;
				if( NO_INTENTIAL_EXIT != nDisconnectCause )
				{
					if( INTENTIAL_EXIT_HARD == nDisconnectCause)
					{
						SendNotice(pClient, 0, 0);
					}
					else if( INTENTIAL_EXIT_SOFT == nDisconnectCause)
					{
						SendNotice(pClient, 0, 1);
					}
					pUser->GetUserRankMatch()->CheckAndSendNotice();

					pUser->RemoveDisconncted();
				}
			}
			pUser->CheckPositionSelect();
		}
		else
		{
			SendLoginResult(pClient, LOGIN_FAIL_FAILCHANNEL);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL,  "User Enter Lobby - GameID(��4)", szGameID); 
		return;
		}

		/// Add Last Use GameID
		((CFSGameODBC *)pNexusODBC)->spGameSvrEnter(iUserIDIndex , iGameIDIndex);

		DWORD dwEnd = GetTickCount();

		dwLoginProcess0 = dwEnd - dwStart;
		dwLoginProcess1 = dwCheck0 - dwStart;
		dwLoginProcess2 = dwCheck1 - dwCheck0;
		
		// load paper
		vector<SPaper*> v;
		((CFSGameODBC *)pNexusODBC)->spGetPaperAll(iGameIDIndex, &v);
		pUser->InitPaperList(&v);
		if(FALSE == pUser->SendPaperList())
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, NONE, "Send Paper List Fail - GameID(��4)", pUser->GetGameID());		
	}

		// load paper
		for (int i = MAIL_PRESENT_ON_AVATAR; i <= MAIL_PRESENT_ON_ACCOUNT; ++i)
			((CFSGameODBC *)pNexusODBC)->spGetPresentAll(iUserIDIndex, iGameIDIndex, i, pUser->GetPresentList(i));
		pUser->SendPresentList(MAIL_PRESENT_ON_AVATAR);
		pUser->SendPresentList(MAIL_PRESENT_ON_ACCOUNT);

		pUser->GetUserComebackBenefit()->LoginAttendanceGiveBenefit();

		LOBBYCLUBUSER.AddUser(pUser);
		SHUTDOWNUSER.AddUser(pUser);

		iResult = 0;
		vector<SClubInvitationData*> vecSClubInvitation;
		if ( ODBC_RETURN_SUCCESS == pClubBaseODBC->SP_Club_GetInvitationList(iGameIDIndex, &vecSClubInvitation, iResult))
		{
			pUser->InitClubInvitationList(&vecSClubInvitation);
		}
		pUser->SendClubInvitationList();

		CClubSvrProxy* pClubSvr = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
		if( pClubSvr  )
		{
			if( pUser->GetClubSN() > 0 )
			{
				SAvatarInfo* pInfo = pUser->GetCurUsedAvatar();
				BYTE btPosition = 0, btLv = 0, btSex = 0;
				int iFameLevel = 0;
				if(pInfo)
				{
					btPosition	= pInfo->Status.iGamePosition;
					btLv		= pInfo->iLv;
					btSex		= pInfo->iSex;
					iFameLevel	= pInfo->iFameLevel;
				}

				SG2CL_CLUB_MEMBER_LOGIN	memberInfo;
				memberInfo.iClubSN = pUser->GetClubSN();
				strncpy_s(memberInfo.szGameID, _countof(memberInfo.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
				memberInfo.btPosition = btPosition;
				memberInfo.btLv = btLv;
				memberInfo.btSex = btSex;
				memberInfo.iFameLevel = iFameLevel;

				pClubSvr->SendClubMemberLogin(memberInfo);

				SG2CL_CLUB_INFO_REQ info;
				info.iClubSN = pUser->GetClubSN();
				strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
				info.btKind	= CLUB_INFO_REQUEST_KIND_LOGIN;
				info.iCheerLeaderIndex = pUser->GetCheerLeaderCode();
				pClubSvr->SendClubInfoReq(info);

			}
				
		}
		if( CFSGameServer::GetInstance()->IsNoWithdrawChannel() )
		{
			BYTE byBeginnerChannel = 1;
			CPacketComposer PacketComposer(S2C_BEGINER_CHANNE_RES);
			PacketComposer.Add(byBeginnerChannel);
			pClient->Send(&PacketComposer);
		}
		else
		{
			BYTE byBeginnerChannel = 0;
			CPacketComposer PacketComposer(S2C_BEGINER_CHANNE_RES);
			PacketComposer.Add(byBeginnerChannel);
			pClient->Send(&PacketComposer);
		}

		// todo pcroom old code
		if(FALSE == CFSGameServer::GetInstance()->GetNewPCRoom())
		{
			pUser->SendPCRoomNotice();
		}

		/////////////  접속시의 기록을 저장한다 /////////////
		pUser->SetCurrentTotalRecord();
		////////////////////////////////////////////////////

		pUser->LoadEventCoin();

		pUser->SendUseStyleAndNameInfo();

		if(FALSE == pUser->LoadSpecialPartsUserProperty())
		{
			SendLoginResult(pClient, LOGIN_FAIL_LOADAVATAR);

			g_LogManager.WriteLogToFile("[LOGIN_USER] LoadSpecialPartsUserProperty Error! : UserID(��4)", szUserID);
		return;
		}

		CPacketComposer PacketComposer(S2C_CHANNEL_BUFF_INFO_NOTIFY);
		CHANNELBUFFMANAGER.MakeChannelBuffTimeInfo(PacketComposer);
		pServer->BroadCast(PacketComposer);

		SendOptionsToClient(pClient);

		// USER_STATE_MOVING
		if(ODBC_RETURN_SUCCESS != pLogODBC->USER_UpdateSessionReLogin(pUser->GetSessionKey(), iUserIDIndex, szGameID, iGameIDIndex, USER_STATE_CONNECT,
																		pServer->GetProcessID(),INVALID_IDINDEX, pServer->GetIP(), pServer->GetPort(), iErrorCode))
		{
			SendLoginResult(pClient, LOGIN_FAIL_ALREADYCONNECT);

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Update State - UserID(��4), UserState(0)", szUserID, eUserState); 
			// 2010.03.31
			// Server의 Client State가 NEXUS_INITIAL인 상태에서 Logout 시에는 UserMap에서 User를 제거 하지 않기 때문에 여기서 제거해 줘야 한다
			// ktKim
			pServer->LockUserManager();
			{
				pServer->RemoveUser(pUser);
			}
			pServer->UnlockUserManager();
			// End
			
			return; 
		}

		if(TRUE == pUser->ProcessMaxLevelStep())
		{
			pUser->UpdateMaxLevelStepInDB();
		}

		//홍벌젬힙검퓻宅뒈혐검퓻
		int iAchievementIndex = 0;
		if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_NATIONLLY_TOURNAMEN_1) )
		{
			iAchievementIndex = ACHIEVEMENT_NATIONLLY_TOURNAMEN_1;
		}
		else if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_NATIONLLY_TOURNAMEN_2) )
		{
			iAchievementIndex = ACHIEVEMENT_NATIONLLY_TOURNAMEN_2;
		}
		else if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_NATIONLLY_TOURNAMEN_3) )
		{
			iAchievementIndex = ACHIEVEMENT_NATIONLLY_TOURNAMEN_3;
		}
		else if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_NATIONLLY_TOURNAMEN_8) )
		{
			iAchievementIndex = ACHIEVEMENT_NATIONLLY_TOURNAMEN_8;
		}
		else if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_CITY_TOURNAMEN_1) )
		{
			iAchievementIndex = ACHIEVEMENT_CITY_TOURNAMEN_1;
		}
		else if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_CITY_TOURNAMEN_2) )
		{
			iAchievementIndex = ACHIEVEMENT_CITY_TOURNAMEN_2;
		}
		else if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_CITY_TOURNAMEN_3) )
		{
			iAchievementIndex = ACHIEVEMENT_CITY_TOURNAMEN_3;
		}
		else if( pUser->PreCheckAchievementAlreadyComplete(ACHIEVEMENT_CITY_TOURNAMEN_8) )
		{
			iAchievementIndex = ACHIEVEMENT_CITY_TOURNAMEN_8;
		}
		
		CAvatarItemList *pAvatarItemList = pUser->GetCurAvatarItemListWithGameID(pUser->GetGameID());
		if (NULL != pAvatarItemList)
		{
			vector<int> vNewlyReachedAchievementIndex;
			vector<int> vAnnounceAchievementIndex;
			vector<int> vInventoryItemCounts;

			pAvatarItemList->GetInventoryItemCountforAchievement(vInventoryItemCounts);

			if (TRUE == pUser->ProcessAchievementbyGroup(ACHIEVEMENT_CONDITION_GROUP_INVENTORY, ACHIEVEMENT_LOG_GROUP_SHOP, vInventoryItemCounts, vNewlyReachedAchievementIndex))
			{
				pUser->ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_SHOP, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex);
			}
		}

		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if (pCenter == NULL) 
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "CCenterSvrProxy* pCenter == NULL");
		}
		else
		{	
			SG2S_USER_DB_INFO_REQ info;
			info.eState = CENTER_USER_GAMESVR_LOGIN;
			strncpy_s(info.GameID, _countof(info.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
			strncpy_s(info.szAccountID, _countof(info.szAccountID), pUser->GetAccountID(), FriendAccount::MAX_ACCOUNT_ID_LENGTH);	
			info.iGameIDIndex	= pUser->GetGameIDIndex();
			info.position		= pUser->GetCurUsedAvatarPosition();
			info.iUserIDIndex	= iUserIDIndex;
			strncpy_s(info.UserID,_countof(info.UserID), szUserID, MAX_USERID_LENGTH);
			info.iLv			= pUser->GetCurUsedAvtarLv();
			info.iFameLevel		= pUser->GetCurUsedAvtarFameLevel();
			info.btSex			= pUser->GetCurUsedAvatarSex();
			info.iEquippedAchievementTitle	= pUser->GetEquippedAchievementTitle();
			info.bCanInvite		= pUser->GetOptionEnable(OPTION_TYPE_INVITE);
			info.bIsSecretAvatar	= pUser->GetUserFriendAccountInfo().bIsSecret_CurrentAvatar;
			info.tLastLogoutTime	= pUser->GetUserFriendAccountInfo().tLastTime_Logout;
			strncpy_s(info.szIPAddress,_countof(info.szIPAddress), pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH);
			
			pCenter->SendPacket(G2S_USER_DB_INFO_REQ, &info, sizeof(SG2S_USER_DB_INFO_REQ));
		}
	}
	else
	{
		SendLoginResult(pClient, LOGIN_FAIL_FAILCHANNEL);

		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Not Exist Session - UserID(��4), ErrorCode(0)", szUserID, iErrorCode);
		return;
	}

	GiveGameGuideItem(pUser);

	//pUser->CheckFirstTimeTutorial(m_pGameODBC);

	// 20070823 Add Story Mode
	CFSGameServer::GetInstance()->SetUserBaseEpisodeInfo(pUser);
	pUser->SetEpisodeInfo();

	WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, SUCCESS, "Address:00B51316, UserAddress:00000000", pClient, pUser);
eckEvent(PERFORM_TIME_LOGIN_ARENA, pGameODBC);
	pUser->CheckEvent(PERFORM_TIME_LOGIN_NORMALLOBBY, pGameODBC);

	pUser->SendEventListInfo();

	if( pUser->CheckAccItemSelectNotice() )
	{
		CPacketComposer PacketComposer(S2C_ACCITEM_NOSELECT_NOTICE);
		pUser->Send(&PacketComposer);
	}

	pUser->SetEnterChannelToSendFriend( TRUE );
	pUser->SetEnterChannelToSendFriendAccount( TRUE );

	pUser->SendFriendList();
	pUser->SendFriendAccountList();

	//Load JoinTeamConfig
	SS2C_JOIN_TEAM_CONFIG_INFO JoinTeamInfo;
	pUser->GetJoinTeamConfig()->Load(pUser->GetGameIDIndex(), pGameODBC, JoinTeamInfo);
	pUser->Send(S2C_JOIN_TEAM_CONFIG_INFO, &JoinTeamInfo, sizeof(SS2C_JOIN_TEAM_CONFIG_INFO));

	//notice ChatServer
	CChatSvrProxy* pChatProxy = (CChatSvrProxy*) GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	BYTE btChatSvrStatus = pChatProxy ? SERVER_ON : SERVER_OFF;
	CPacketComposer Packet(S2C_CHAT_SERVER_STATUS_NOT);
	Packet.Add(btChatSvrStatus);
	pUser->Send(&Packet);

	//notice MatchServer
	GAMEPROXY.SendMatchServerStatus(pUser);

	pUser->SendRecordBoardStatus();

	//pUser->CheckFullCrtUniform();

	pUser->SendSkillTakeEnable();

	pUser->AddTotalRankingAchievements();

	CLUBTOURNAMENTAGENCY.SendClubTournamentStatus(pUser);

	//pUser->SendRookieItemNotice();

	pUser->GetUserBasketBall()->CheckAndRemoveBasketBall();
	pUser->GetUserSkyLuckyEvent()->SendEventInfo();
	pUser->GetUserGoldenSafeEvent()->SendEventInfo();
	pUser->GiveLvUpEventReward(pUser->GetCurUsedAvtarLv(), TRUE);
	
	pUser->GetUserPuzzle()->GivePuzzleFromRewardType(PUZZLE_REWARD_TYPE_DAILY_LOGIN);

	// todo pcroom old code
	if(FALSE == CFSGameServer::GetInstance()->GetNewPCRoom())
	{
		if(PCROOM_KIND_PREMIUM == pUser->GetPCRoomKind())
			pUser->GetUserPuzzle()->GivePuzzleFromRewardType(PUZZLE_REWARD_TYPE_DAILY_LOGIN_PCROOM);
	}

	if(TRUE == CFSGameServer::GetInstance()->CheckMatchingPoolCareTime())
	{
		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_RATING, pUser->GetCurUsedAvtarLv());
		if(pMatch) 
		{
			SG2M_CEHCK_OPENED_TEAM_COUNT_REQ info;
			info.iGameIDIndex = pUser->GetGameIDIndex();
			info.btMatchTeamScale = MATCH_TEAM_SCALE_3ON3;
			pMatch->SendPacket(G2M_CEHCK_OPENED_TEAM_COUNT_REQ, &info, sizeof(SG2M_CEHCK_OPENED_TEAM_COUNT_REQ));
		}
	}

	vector<int> vLevel;
	vLevel.push_back(pUser->GetCurUsedAvtarLv());
	pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_LEVEL, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vLevel);

	pUser->GetUserFreeStyleMatchEvent()->UpdateLoginPoint();
	pUser->GetUserHippocampusEvent()->GiveWaitGiveReward();
	pUser->LoadAndSendLobbyButtonType();

	XIGNCODEMANAGER.OnAccept(pClient);

	/*	
		TODO : Event 재사용시 확인 
		pUser->GetUserMissionShopEvent()->SendMissionCoinInfo();
		pUser->GetUserHotGirlMissionEvent()->CheckAndGiveAchievement(ACHIEVEMENT_CONDITION_GROUP_FS11YEARS_EVENT_LOGIN);
	*/
}

void CFSGameThread::SendLoginResult(CFSGameClient * pClient, int iErrorCode)
{	
	if(NULL == pClient)	
		return; 
	
	CPacketComposer PacketComposer(S2C_USERCERT_RESULT);
	PacketComposer.Add(iErrorCode); 
	
	if(iErrorCode == 0)
	{
		PacketComposer.Add((int)0);
		
		CFSGameServer *pServer = CFSGameServer::GetInstance();
		if( NULL == pServer )
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "SendLoginResult::pServer");
			return ;
		}
		
		int iGameMode = 0, iChannelType = 1;
		PacketComposer.Add(pServer->GetMatchMode());
		PacketComposer.Add(iChannelType); // TODO DELETE		
		PacketComposer.Add(iGameMode); // TODO DELETE		

		CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
		if ( NULL == pUser )
		{
			PacketComposer.Add( int(0) );
		}
		else
		{
			PacketComposer.Add( pUser->CheckGMUser() );
		}
	}
	else
	{
		if(TRUE == CFSGameServer::GetInstance()->IsCheckLog())
		{
			CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();

			if(NULL != pUser)
			{
				WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Send UserCert : GameID(��4), UserID((null)), ErrorCode(18227200)", pUser->GetGameID(), pUser->GetUserID(), iErrorCode);
se
			{
				WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGIN, FAIL, "Send UserCert Unknown User : ErrorCode(11866902)", iErrorCode);
		}
	}
	
	pClient->Send(&PacketComposer);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//      LogOut 부분 
//
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Modify for Match Server
void CFSGameThread::LogOut(CIOCPSocketClient* pIOCPSocketClient)
{
	if (FS_PROXY < ((CBaseClient*)pIOCPSocketClient)->GetState())
	{
		GAMEPROXY.Disconnect((CSvrProxy*)pIOCPSocketClient);
		return;
	}
	CFSGameClient* pClient = (CFSGameClient*)pIOCPSocketClient;
	CFSGameServer* pServer = CFSGameServer::GetInstance();

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();

	WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, NONE, "[Log Out] Client(00B51316) User(00000000)", pClient, pUser);
= pUser)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "User is null Address(00B51316)", pClient);
rn;
	}

	if(1 < pUser->TestLogout())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, NONE, "Logout Count(11866902), Client State(0), Address(01162000), User Address(CCCCCCCC)", pUser->GetTestLogout() , pClient->GetState() , pClient , pUser );
	if(NEXUS_INITIAL == pClient->GetState())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "State(11866902), Address(00000000), User Address(01162000)", pClient->GetState(), pClient, pUser);
CClubSvrProxy* pClubSvr = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubSvr && pUser->GetUserIDIndex() > 0 && pUser->IsClubMember() == TRUE) 
	{
		SG2CL_DEFAULT info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		pClubSvr->SendClubMemberLogout(info);
	} 
	
	CMatchBaseSvrProxy* pMatchSvrProxy = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatchSvrProxy) 
	{
		pMatchSvrProxy->SendUserLogout(pUser);
	}

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	if (pChat)
	{
		SS2T_BASE info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		pChat->SendPacket(S2T_EXIT_PRIVATE_CHAT_ROOM_REQ, &info, sizeof(SS2T_BASE));
		pChat->SendPacket(S2T_EXIT_LOBBY_CHAT_ROOM_REQ, &info, sizeof(SS2T_BASE));
	}

	int iErrorCode = 0;
	
	if(U_OPERATOR == pUser->GetLocation()) 
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "User is U_OPERATOR Address(00B51316), User Address(00000000)", pClient, pUser);
}
	
	DECLARE_INIT_TCHAR_ARRAY(szSessionKey, MAX_SESSIONKEY_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);
	USER_STATE eUserState = USER_STATE_CONNECT;
	int iProcessID = 0;
	int iChannelID = INVALID_IDINDEX;
	int iBirthDay;
	int iSafeConstantFirstChkSum;

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
	if( NULL != pLogODBC )
	{
		ODBCRETURN Result = pLogODBC->USER_GetSession(pUser->GetUserIDIndex(), szSessionKey, szGameID, eUserState, iProcessID, iChannelID, iBirthDay, iSafeConstantFirstChkSum, iErrorCode);
		if(Result != ODBC_RETURN_NODATA && eUserState != USER_STATE_MOVING && iProcessID == pServer->GetProcessID())
		{
			time_t tCurrentDBTime = -1;
			Result = pLogODBC->USER_DeleteSession(1, pServer->GetProcessID(), pUser->GetUserIDIndex(), pUser->GetSessionKey(), tCurrentDBTime, iErrorCode);
		}
	}

	time_t tCurrentTime = _GetCurrentDBDate;

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	if(pCenter) 
	{
		SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ ss;
		ss.iGameIDIndex = pUser->GetGameIDIndex();
		strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		ss.btUpdateType = MATCHINGPOOLCARE_UPDATE_TARGETUSER_TYPE_REMOVE;
		ss.btLvGrade = pUser->GetCurUsedAvtarLvGrade();
		pCenter->SendPacket(G2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ, &ss, sizeof(SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ));

		SG2S_USER_LOGOUT info;
		info.eState = USER_STATE_MOVING == eUserState ? CENTER_USER_LOGOUT_ON_MOVE : CENTER_USER_LOGOUT;
		strncpy_s(info.GameID, MAX_GAMEID_LENGTH+1, pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iUserIDIndex = pUser->GetUserIDIndex();
		info.tCurrentTime = tCurrentTime;
		pCenter->SendPacket(G2S_USER_LOGOUT, &info, sizeof(SG2S_USER_LOGOUT));
	}

	pUser->SetRecAllFriend( FALSE );
	pUser->SetEnterChannelToSendFriend( FALSE );

	pUser->SetRecAllFriendAccount( FALSE );
	pUser->SetEnterChannelToSendFriendAccount( FALSE );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	if( NULL != pGameODBC )
	{
		if(false == pUser->GetUserFriendAccountInfo().bIsSecret_CurrentAvatar)
		{
			if(ODBC_RETURN_SUCCESS != pGameODBC->FRIENDACCOUNT_UpdateLastLogoutTime(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), tCurrentTime))
			{
				WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "FRIENDACCOUNT_UpdateLastLogoutTime Fail. GameIDIndex:11866902, Time:0", pUser->GetGameIDIndex(), tCurrentTime);

	}	

		pGameODBC->AVATAR_UpdateLastDate(pUser->GetGameIDIndex());
		//ToDo - 이벤트 관련은 comp. event를 사용하자
		pGameODBC->EVENT_IncreaseAccumulatedPalyCountDB(2, 0, pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), 0 );

		pUser->GetJoinTeamConfig()->Save(pGameODBC);
		pUser->GetUserFriendInvite()->UpdateAllMissionUpdate(dynamic_cast<CFSODBCBase*>(pGameODBC));
		pUser->GetUserFriendInvite()->SendCenterSvrLastConnectTimeNoticeForFriend(FALSE);
		pUser->GetUserComebackBenefit()->SaveComebackBenefit(FALSE, dynamic_cast<CFSODBCBase*>(pGameODBC));
	}

	pUser->SaveLobbyButtonType(TRUE, nullptr);


	SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
	if(NULL == pAvatarInfo)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "AvatarInfo is null Address(00B51316), User Address(00000000)", pClient, pUser);
ver->LockUserManager();
		{
			pClient->SetState(NEXUS_INITIAL); 

			pServer->RemoveUser(pUser);
		}
		pServer->UnlockUserManager();

		return;
	}
	
	if(TRUE == pServer->IsSetUserLog())
	{
		SGameRecordLog GameRecordLog;
		memcpy( &GameRecordLog , pUser->GetConnectGameRecord() , sizeof(SGameRecordLog));
		if(GameRecordLog.bUse == true)
		{
			CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC( ODBC_RANK );
			if( NULL != pRankODBC )
			{
				pRankODBC->spInsertRecordLogNew(pUser->GetUserIDIndex(), pAvatarInfo->iGameIDIndex, pAvatarInfo->iLv, pAvatarInfo->iExp, pUser->GetSkillPoint(), 
					&pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[0], // 로그를 남길 목적은 모르겠으나 3:3 total로 생각됨
					GameRecordLog, pUser->GetIPAddress() );
			}
		}
	}

	CBaseClient* pBaseClient  =(CBaseClient*)pIOCPSocketClient; 
	if(PCK_GAME != pBaseClient->GetKind())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "Client kind is not Game Address(00B51316), User Address(00000000)", pClient, pUser);
	}

	if(TRUE == pServer->IsCheckLog())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, SUCCESS, "GameID(��4), State(0)", pUser->GetGameID(), pBaseClient->GetState());
	}

	XIGNCODEMANAGER.OnDisconnect(pClient);

	pServer->LockUserManager();
	{
		pClient->SetState(NEXUS_INITIAL); 

		pServer->RemoveUser(pUser);
	}
	pServer->UnlockUserManager();

	pUser->ProcessLogOut();

	pUser->CheckAndSaveStyleInfo();

	pUser->GetUserRandomItemBoardEvent()->SaveRandomItemBoard();

	pUser->GetUserShoppingEvent()->SaveUserShoppingInfo();
	pUser->GetUserMissionMakeItemEvent()->SaveStartConnectTime();
	pUser->GetUserColoringPlayEvent()->SaveStartConnectTime();
	pUser->GetUserCongratulationEvent()->SaveStartConnectTime();
	pUser->GetUserHelloNewYearEvent()->CheckRemainBuffSec(_time64(NULL), true);
	pUser->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_LOGIN_TIME);
	pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_LOGIN_TIME_MIN);
	pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE,WORDPUZZLES_REWARDTYPE_120MIN_CONNECT_TIME);

	pUser->GetUserAppraisal()->SavePenaltyTime();
	pUser->GetUserAppraisal()->SavePraisePoint(dynamic_cast<CFSODBCBase*>(pGameODBC));
	pUser->GetUserMissionEvent()->SaveConnectingTime();
	pUser->GetUserCovet()->CheckAndSaveCovetJinn(dynamic_cast<CFSODBCBase*>(pGameODBC));
	pUser->SavePromiseUserInfo();

	// 연승횟수 초기화
	pUser->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_WINCONTINUE_COUNT, 0);
	pUser->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_CONNECTTIME);
	pUser->GetUserPCRoomEvent()->SaveUserInfo();
	pUser->GetUserRankMatch()->SaveLimitMatchingInfo();
	pUser->GetUserPresentFromDeveloperEvent()->UpdateUserEventStatus(TRUE);
	pUser->GetUserDiscountItemEvent()->UpdateItemPrice(TRUE);
	pUser->GetUserPotionMakingEvent()->SaveUserUpdateTime();
	pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_LOGIN_TIME);
	pUser->UpdateRealCash();
	pUser->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_LOGIN_TIME);
	pUser->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_LOGIN_TIME);
	pUser->GetUserShoppingGodEvent()->CheckAndUpdateUserMission(SHOPPING_GOD_MISSION_TYPE_LOGIN_TIME);
	pUser->GetUserDriveMissionEvent()->CheckUpdateMission(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_PLAY_CONTINUE_WIN_CNT, 0);
	pUser->GetUserSummerCandyEvent()->SaveUserInfo();
	pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_LOGIN_TIME);
	if(TRUE == pUser->GetUserPremiumPassEvent()->CheckResetDailyMission())
		pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_LOGIN_COUNT);
}

void CFSGameThread::Process_FSUpdateUserSession(CFSGameClient * pClient)
{
	CFSGameUser * pUser = (CFSGameUser *)pClient->GetUser();
	if(NULL == pUser)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_MOVE_SERVER, FAIL, "Not Exist GameUser");
		return;
	}
	
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(NULL == pServer) 
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_MOVE_SERVER, FAIL, "Not Exist GameServer");
		return;
	}

	CPacketComposer PacketComposer(S2C_MOVE_SERVER_RES);

	int iResult = pUser->UpdateSession(pServer->GetProcessID(), pServer->GetIP(), pServer->GetPort(), pUser->GetGameID(), pUser->GetGameIDIndex(), INVALID_INDEX);
	PacketComposer.Add(iResult);
	pClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSLogOut(CFSGameClient* pClient)
{
	CFSGameUser * pUser = (CFSGameUser *)pClient->GetUser();
	if(NULL == pUser)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "Not Exist User");
		return;
	}
	
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(NULL == pServer) 
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "Not Exist Server");
		return;
	}

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
	CHECK_NULL_POINTER_VOID( pLogODBC );

	DECLARE_INIT_TCHAR_ARRAY(szSessionKey, MAX_SESSIONKEY_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);
	USER_STATE eUserState = USER_STATE_CONNECT;
	int iErrorCode = -1;
	int iProcessID = 0;
	int iChannelID = INVALID_IDINDEX;
	int iBirthDay;
	int iSafeConstantFirstChkSum;
	int iResult = 0;

	CPacketComposer PacketComposer(S2C_LOGOUT_RES);

	ODBCRETURN Result = pLogODBC->USER_GetSession(pUser->GetUserIDIndex(), szSessionKey, szGameID, eUserState, iProcessID, iChannelID, iBirthDay, iSafeConstantFirstChkSum, iErrorCode);
	if(Result == ODBC_RETURN_NODATA || Result == ODBC_RETURN_FAIL || eUserState != USER_STATE_MOVING)
	{
		if(ODBC_RETURN_SUCCESS != pLogODBC->USER_UpdateSession(pUser->GetSessionKey(), pUser->GetUserIDIndex(), pUser->GetGameID(), pUser->GetGameIDIndex(),
												USER_STATE_CONNECT, pServer->GetProcessID(),INVALID_IDINDEX , pServer->GetIP(), pServer->GetPort(), iProcessID, iErrorCode))
		{
			WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "Update Session - SessionKey(11866902), UserID((null))", szSessionKey, pUser->GetUserID());
acketComposer.Add(iResult);
		pClient->Send(&PacketComposer);

		WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, FAIL, "User Session - UserID(��4), UserIDIndex(0), SessionKey(), UserState(-858993460), ErrorCode(-858993460)", pUser->GetUserID(), pUser->GetUserIDIndex(), szSessionKey, eUserState, iErrorCode);


pUser->SetEnterChannelToSendFriend( FALSE );
	pUser->SetRecAllFriend( FALSE );

	PacketComposer.Add(iResult);
	pClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSMultiLoginUserLogOut(CFSGameClient* pFSClient)
{	
	DECLARE_INIT_TCHAR_ARRAY(szSessionKey, MAX_SESSIONKEY_LENGTH+1);	
	int iUserIDIndex = 0;
	DECLARE_INIT_TCHAR_ARRAY(szUserID, MAX_USERID_LENGTH+1);	
	int iPublisherIDIndex = 1;		
	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);	
	int iProcessID = 0;

	m_ReceivePacketBuffer.Read((BYTE*)szSessionKey, MAX_SESSIONKEY_LENGTH+1);
	m_ReceivePacketBuffer.Read(&iUserIDIndex);
	m_ReceivePacketBuffer.Read((BYTE*)szUserID, MAX_USERID_LENGTH+1);
	m_ReceivePacketBuffer.Read(&iPublisherIDIndex);
	m_ReceivePacketBuffer.Read((BYTE*)szGameID, MAX_GAMEID_LENGTH+1);
	m_ReceivePacketBuffer.Read(&iProcessID);
	
	CFSGameServer  *pServer	= CFSGameServer::GetInstance();
	if(!pServer) 
		return;

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
	CHECK_NULL_POINTER_VOID( pLogODBC );

	pServer->LockUserManager();
	
	CFSGameUser *pPrevUser	= NULL;
	if(lstrcmp(szGameID, "") != 0)
		pPrevUser = (CFSGameUser*)pServer->IsThereSameID(szGameID);
	else
		pPrevUser = (CFSGameUser*)pServer->IsSameUserGameServer(szUserID,iPublisherIDIndex);
	
	BOOL bResult = FALSE;
	if(pPrevUser)
	{
		SendLoginResult(pFSClient, LOGIN_FAIL_EXISTSAMEID);
		
		CFSGameClient *pGetClient = (CFSGameClient*)pPrevUser->GetClient();
		if(pGetClient != NULL)
		{
			int iErrorCode = -1;
			time_t tCurrentDBTime = -1;
			if(ODBC_RETURN_SUCCESS == pLogODBC->USER_DeleteSession(1, pServer->GetProcessID(), pPrevUser->GetUserIDIndex(), szSessionKey, tCurrentDBTime, iErrorCode))
			{
				bResult = TRUE;
			}

			WRITE_LOG_NEW(LOG_TYPE_USER, USER_MULTILOGIN, NONE, "Process_FSMultiLoginUserLogOut - UserID(��4), PublisherIDIndex(0)", szUserID, iPublisherIDIndex);
			
			pGetClient->DisconnectClient();
		}
	}
	pServer->UnlockUserManager(); 

	// Multi Login User Disconnect Respond
	CPacketComposer packet(S2LG_MULTI_LOGIN_USER_LOGOUT_RES);
	packet.Add((BYTE*)szSessionKey, MAX_SESSIONKEY_LENGTH+1);
	packet.Add((INT)iUserIDIndex);
	packet.Add((INT)iProcessID);
	packet.Add((BOOL)bResult);

	CCenterSvrProxy* pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	if(pCenterProxy)
	{
		pCenterProxy->Send(&packet);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSUserLatencyLog(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_P2P_LATENCY_CHECK info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	m_ReceivePacketBuffer.Read((PBYTE)&info.iTotalLatency, sizeof(SG2M_P2P_LATENCY_CHECK)-sizeof(SG2M_BASE));

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_P2P_LATENCY_CHECK, &info, sizeof(SG2M_P2P_LATENCY_CHECK));
}

// Modify for Match Server
void CFSGameThread::Process_FSLogOutTypeCheck(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_LOGOUT_TYPE_CHECK info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iLogOutType);

	pMatch->SendLogoutTypeCheck(info);
}

void CFSGameThread::SendOptionsToClient(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(NULL == pServer) return;

	CPacketComposer PacketComposer(S2C_NOTIFY_GAMECHANNEL_OPTIONS);

	/////////////////////////////// 여기서 부터 순서대로 옵션에 대한 패킷값 추가 및 설명 추가 ////////////////////////////////////

	///////////////////////////////////// 패킷 값 추가 끝 지금까지 총 (0)개 -업데이트 바람 ///////////////////////////////////////

	pFSClient->Send(&PacketComposer);
}