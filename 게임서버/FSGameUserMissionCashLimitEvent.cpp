qinclude "stdafx.h"
qinclude "FSGameUserMissionCashLimitEvent.h"
qinclude "CFSGameUser.h"
qinclude "MissionCashLimitEventManager.h"
qinclude "RewardManager.h"

CFSGameUserMissionCashLimitEvent::CFSGameUserMissionCashLimitEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
	for(int i = 0; i < MissionCashLimitEvent::MAX_MISSION_CONDITION_TYPE; ++i)
		m_iCurrentValue[i] = 0;
}

CFSGameUserMissionCashLimitEvent::~CFSGameUserMissionCashLimitEvent(void)
{
}

BOOL			CFSGameUserMissionCashLimitEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == MISSIONCASHLIMIT.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_MISSIONCASHLIMIT_GetUserData(m_pUser->GetUserIDIndex(), m_iCurrentValue))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_MISSIONCASHLIMIT_GetUserData failed");
		return FALSE;	
	}
	vector<BYTE> vecTemp;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_MISSIONCASHLIMIT_GetUserCash(m_pUser->GetUserIDIndex(), vecTemp))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_MISSIONCASHLIMIT_GetUserCash failed");
		return FALSE;	
	}

	for(int i = 0; i < vecTemp.size(); ++i)
	{
		m_setClearMission.insert(vecTemp[i]);
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void			CFSGameUserMissionCashLimitEvent::SendUserInfo(int iServerIndex)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONCASHLIMIT.IsOpen());

	SS2C_MISSIONCASHLIMIT_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_MISSIONCASHLIMIT_USER_INFO_RES));

	ss.iServerIndex = iServerIndex;
	for(int i = 0; i < MissionCashLimitEvent::MAX_MISSION_CONDITION_TYPE; ++i)
		ss.iCurrentValue[i] = m_iCurrentValue[i];

	vector<SMISSIONCASHLIMIT_CASH_INFO>	vecMissionInfoList;
	MISSIONCASHLIMIT.GetMissionInfoList(m_iCurrentValue, vecMissionInfoList);
	ss.iMissionCnt = vecMissionInfoList.size();
	
	CPacketComposer	Packet(S2C_MISSIONCASHLIMIT_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_MISSIONCASHLIMIT_USER_INFO_RES));

		
	for(int i = 0; i < ss.iMissionCnt; ++i)
	{
		if(true == vecMissionInfoList[i].bOnButton &&
			m_setClearMission.end() != m_setClearMission.find(vecMissionInfoList[i].btMissionIndex))
		{
			vecMissionInfoList[i].bOnButton = false;
		}

		Packet.Add((PBYTE)&vecMissionInfoList[i], sizeof(SMISSIONCASHLIMIT_CASH_INFO));
	}

	m_pUser->Send(&Packet);
}

BYTE			CFSGameUserMissionCashLimitEvent::GiveCashReq(BYTE btMissionIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_GIVE_CASH_FAIL);
	CHECK_CONDITION_RETURN(CLOSED == MISSIONCASHLIMIT.IsOpen(), RESULT_GIVE_CASH_FAIL);

	if(m_setClearMission.end() != m_setClearMission.find(btMissionIndex))
	{
		return RESULT_GIVE_CASH_FAIL_ALREADY_GIVE;
	}
	
	if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		return RESULT_GIVE_CASH_FAIL_FULL_MAILBOX;
	}

	if(0 >= MISSIONCASHLIMIT.GetRemainCash(btMissionIndex))
	{
		return RESULT_GIVE_CASH_FAIL_NO_REMAIN_CASH;
	}

	return RESULT_GIVE_CASH_SUCCESS;
}

void			CFSGameUserMissionCashLimitEvent::GiveCashRes(BYTE btResult, BYTE btMissionIndex, int iRewardIndex)
{
	if(MissionCashLimitEvent::RESULT_GIVE_CASH_SUCCESS == btResult)
	{
		m_setClearMission.insert(btMissionIndex);

		SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
		if(pReward == NULL)
		{
			WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "GiveCashRes REWARDMANAGER.GiveReward failed. UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), iRewardIndex);
	}

	SS2C_MISSIONCASHLIMIT_GIVE_CASH_RES	ss;
	ss.btResult = btResult;
	m_pUser->Send(S2C_MISSIONCASHLIMIT_GIVE_CASH_RES, &ss, sizeof(SS2C_MISSIONCASHLIMIT_GIVE_CASH_RES));
}

void			CFSGameUserMissionCashLimitEvent::CheckAndUpdateMission(bool bWin, BOOL bIsDisconnectedMatch, BOOL bRunAway)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(TRUE == bIsDisconnectedMatch || TRUE == bRunAway);
	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONCASHLIMIT.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == MISSIONCASHLIMIT.IsEventTime());

	int	iAddValue[MissionCashLimitEvent::MAX_MISSION_CONDITION_TYPE] = {0,};
	iAddValue[MISSION_CONDITION_TYPE_GAME_PLAY_CNT] = 1;
	iAddValue[MISSION_CONDITION_TYPE_GAME_WIN_CNT] = static_cast<BOOL>(bWin);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_VOID(pODBC);

	if(ODBC_RETURN_SUCCESS == pODBC->EVENT_MISSIONCASHLIMIT_UpdateMission(m_pUser->GetUserIDIndex(), iAddValue))
	{
		for(int i = 0; i < MissionCashLimitEvent::MAX_MISSION_CONDITION_TYPE; ++i)
			m_iCurrentValue[i] += iAddValue[i];
	}
}
