qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserRandomBox
{
public:
	CFSGameUserRandomBox(CFSGameUser* pUser);
	~CFSGameUserRandomBox(void);

	BOOL Load();

	RANDOMBOX_MISSION_STATUS GetRandomBoxMissionStatus();

	void SendRandomBoxInfo();
	void SendRandomBoxRewardInfo(int iBoxItemCode);
	RESULT_USE_RANDOMBOX UseRandomBox(int iBoxItemCode, int& iRewardIndex, int& iUpdatedBoxCount);
	RESULT_RANDOMBOX_MISSION_REWARD RecvMissionReward(SS2C_RANDOMBOX_MISSION_REWARD_RES& rs);
	void UpdateMission();

private:

private:
	CFSGameUser* m_pUser;
	BOOL		m_bDataLoaded;

	SAvatarRandomBoxMission m_AvatarRandomBoxMission;
};

