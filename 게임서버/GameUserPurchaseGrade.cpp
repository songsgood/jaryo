qinclude "stdafx.h"
qinclude "GameUserPurchaseGrade.h"
qinclude "PurchaseGradeManager.h"
qinclude "RewardManager.h"
qinclude "CFSGameUser.h"
qinclude "CFSGameUserItem.h"

CGameUserPurchaseGrade::CGameUserPurchaseGrade(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_sCurrentGradeInfo(0 ,PURCHASE_GRADE_TYPE_NEW)
	, m_iSumUseCash_Current(0)
	, m_iSumUseCash_Next(0)
	, m_iRecentCardOpenDate(0)
	, m_bIsGetRewardEventCash(FALSE)
{
	ZeroMemory(m_iaGradeCount, sizeof(int) * MAX_PURCHASE_GRADE_TYPE);
}


CGameUserPurchaseGrade::~CGameUserPurchaseGrade(void)
{
}

BOOL CGameUserPurchaseGrade::Load()
{
	if(FALSE == LoadUserInfo())
	{
		return FALSE;
	}		

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS != pODBC->PURCHASE_GRADE_GetUserGradeCount(m_pUser->GetUserIDIndex(), m_iaGradeCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_PURCHASE_GRADE, INITIALIZE_DATA, FAIL, "PURCHASE_GRADE_GetUserGradeCount error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;
	
	return TRUE;
}

BOOL CGameUserPurchaseGrade::LoadUserInfo(int iCurrentMonth/* = GetYYYYMMDD() / 100*/)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);
	
	map<int/*yyyymm*/,int/*iUseCash*/> mapUseCash;

	if (ODBC_RETURN_SUCCESS != pODBC->PURCHASE_GRADE_GetUserUseCash(m_pUser->GetUserIDIndex(), mapUseCash))
	{
		WRITE_LOG_NEW(LOG_TYPE_PURCHASE_GRADE, INITIALIZE_DATA, FAIL, "PURCHASE_GRADE_GetUserUseCash error");
		return FALSE;
	}
	
	int iTempMonth = iCurrentMonth;
	int iBefore_1Month = ((iTempMonth) == 1) ? ((iTempMonth/100)-1)*100 + 12 : iTempMonth - 1;
1;

	iTempMonth = iBefore_1Month;
	int iBefore_2Month = ((iTempMonth) == 1) ? ((iTempMonth/100)-1)*100 + 12 : iTempMonth - 1;
1;

	iTempMonth = iBefore_2Month;
	int iBefore_3Month = ((iTempMonth) == 1) ? ((iTempMonth/100)-1)*100 + 12 : iTempMonth - 1;
1;
	
	map<int/*yyyymm*/,int/*iUseCash*/>::iterator itMap = mapUseCash.begin();
	map<int/*yyyymm*/,int/*iUseCash*/>::iterator itMapEnd = mapUseCash.end();
	m_iSumUseCash_Current = 0;
	m_iSumUseCash_Next = 0;

	for(; itMap != itMapEnd; ++itMap)
	{
		if(iBefore_3Month == itMap->first)
		{
			m_iSumUseCash_Current += itMap->second;
		}
		else if(iBefore_2Month == itMap->first)
		{
			m_iSumUseCash_Current += itMap->second;
			m_iSumUseCash_Next += itMap->second;
		}
		else if(iBefore_1Month == itMap->first)
		{
			m_iSumUseCash_Current += itMap->second;
			m_iSumUseCash_Next += itMap->second;
		}
		else if(iCurrentMonth == itMap->first)
		{
			m_iSumUseCash_Next += itMap->second;
		}
	}

	m_sCurrentGradeInfo.btCurrentGrade = PURCHASEGRADE.GetCurrentGrade(m_iSumUseCash_Current);

	int iRet = pODBC->PURCHASE_GRADE_GetUserCard(m_pUser->GetUserIDIndex(), iCurrentMonth, m_sCurrentGradeInfo.btCurrentGrade, m_iRecentCardOpenDate, m_bIsGetRewardEventCash);
	if(iRet == 1 || iRet == 2)
	{
		if(FS_ITEM == m_pUser->GetClient()->GetState())
		{
			SendCurrentGradeButton();
		}
		else if(FS_COACH_CARD == m_pUser->GetClient()->GetState())
		{
			SendUserGradeCardCombineRateUp();
		}

		if(iRet == 1)
		{
			if(ODBC_RETURN_SUCCESS == pODBC->PURCHASE_GRADE_GetUserGradeCount(m_pUser->GetUserIDIndex(), m_iaGradeCount))
			{
				CheckAndGiveAchievement();
			}	
		}
	}
	else if(ODBC_RETURN_SUCCESS != iRet)
	{
		WRITE_LOG_NEW(LOG_TYPE_PURCHASE_GRADE, INITIALIZE_DATA, FAIL, "PURCHASE_GRADE_GetUserCard error");
		return FALSE;
	}

	m_sCurrentGradeInfo.iCurrentMonth = iCurrentMonth;

	return TRUE;
}

BOOL CGameUserPurchaseGrade::CheckResetMonth(int iCurrentMonth/* = GetYYYYMMDD() / 100*/)
{
	CHECK_CONDITION_RETURN(m_sCurrentGradeInfo.iCurrentMonth == iCurrentMonth, FALSE);
	CHECK_CONDITION_RETURN(FALSE == LoadUserInfo(iCurrentMonth), FALSE);
	
	return TRUE;
}

void CGameUserPurchaseGrade::SendCurrentGradeButton()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_PURCHASE_GRADE_CURRENT_GRADE_NOT ss;
	ss.btCurrentGrade = m_sCurrentGradeInfo.btCurrentGrade;
	CPacketComposer Packet(S2C_PURCHASE_GRADE_CURRENT_GRADE_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_PURCHASE_GRADE_CURRENT_GRADE_NOT));

	m_pUser->Send(&Packet);
}

void CGameUserPurchaseGrade::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CheckResetMonth();

	SS2C_PURCHASE_GRADE_INFO_RES ss;
	
	ss.btMonth = m_sCurrentGradeInfo.iCurrentMonth ;
00;
	ss.btCurrentMonthGrade = m_sCurrentGradeInfo.btCurrentGrade;
	ss.btNextMonthPredictableGrade = PURCHASEGRADE.GetCurrentGrade(m_iSumUseCash_Next);
	int iNextGradeConditionCash = PURCHASEGRADE.GetConditionUseCash(ss.btNextMonthPredictableGrade + 1);
	ss.iNextGradeNeedCash = (iNextGradeConditionCash > 0) ? iNextGradeConditionCash - m_iSumUseCash_Next : 0;
	ss.IsGetRewardEventCash = m_bIsGetRewardEventCash;

	CPacketComposer Packet(S2C_PURCHASE_GRADE_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_PURCHASE_GRADE_INFO_RES));

	m_pUser->Send(&Packet);
}

void CGameUserPurchaseGrade::SendUserGradeCardCombineRateUp()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_PURCHASE_GRADE_CARD_COMBINE_RATE_UP_NOT ss;
	ss.iRateUp = PURCHASEGRADE.GetCardCombineRateUpValue(m_sCurrentGradeInfo.btCurrentGrade);
	CHECK_CONDITION_RETURN_VOID(ss.iRateUp <= 0);

	CPacketComposer Packet(S2C_PURCHASE_GRADE_CARD_COMBINE_RATE_UP_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_PURCHASE_GRADE_CARD_COMBINE_RATE_UP_NOT));

	m_pUser->Send(&Packet);
}

BOOL CGameUserPurchaseGrade::GiveRewardEventCash()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	int iYYYYMM = GetYYYYMMDD() / 100;
	BOOL bReset = CheckResetMonth(iYYYYMM);

	SS2C_PURCHASE_GRADE_GIVE_EVENT_COIN_RES	ss;

	if(TRUE == m_bIsGetRewardEventCash)
	{
		ss.btResult = PURCHASE_GRADE_GIVE_EVENT_COIN_FAIL;
	}
	else
	{
		int iRewardIndex = PURCHASEGRADE.GetEventCashRewardIndex(m_sCurrentGradeInfo.btCurrentGrade);

		if(iRewardIndex > 0 && FALSE == bReset)
			LoadUserInfo(iYYYYMM);
		
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if (ODBC_RETURN_SUCCESS == pODBC->PURCHASE_GRADE_GiveRewardEventCash(m_pUser->GetUserIDIndex(), m_sCurrentGradeInfo.iCurrentMonth, m_sCurrentGradeInfo.btCurrentGrade))
		{
			m_bIsGetRewardEventCash = TRUE;

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);

			if(NULL == pReward)
			{
				WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, LA_DEFAULT, NONE, "CGameUserPurchaseGrade::GiveRewardEventCash() Fail, UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), iRewardIndex);

			else
			{
				ss.btResult = PURCHASE_GRADE_GIVE_EVENT_COIN_SUCCESS;
			}
		}
	}

	CPacketComposer Packet(S2C_PURCHASE_GRADE_GIVE_EVENT_COIN_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_PURCHASE_GRADE_GIVE_EVENT_COIN_RES));

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CGameUserPurchaseGrade::GiveCardReward(int iItemCode, int iPropertyKind)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	// 현재등급이 카드가 있으면 체크.
	CHECK_CONDITION_RETURN(PURCHASEGRADE.GetCardItemCode(m_sCurrentGradeInfo.btCurrentGrade) <= 0, FALSE);

	int iResult = -1;
	int iCurrentDate = GetYYYYMMDD();

	if(iCurrentDate == m_iRecentCardOpenDate)
	{
		iResult = PURCHASE_GRADE_GIVE_CARD_REWARD_FAIL_ALREADY_USE;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 
		PURCHASEGRADE.GetMailRewardCnt(m_sCurrentGradeInfo.btCurrentGrade) > MAX_PRESENT_LIST)
	{
		iResult = PURCHASE_GRADE_GIVE_CARD_REWARD_FAIL_FULL_MAILBOX;
	}
	else
	{
		// CHN - 중국은 청약철회 없음.
		//CheckResetMonth(iCurrentDate / 100);	

		// KOR - 한국은 청약철회 때문에 항상 다시 로드.
		CHECK_CONDITION_RETURN(FALSE == LoadUserInfo(iCurrentDate / 100), FALSE); 

		int iCardItemCode = PURCHASEGRADE.GetCardItemCode(m_sCurrentGradeInfo.btCurrentGrade);
		if(iItemCode == iCardItemCode)
		{
			CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
			CHECK_NULL_POINTER_BOOL(pODBC);

			if(ODBC_RETURN_SUCCESS == pODBC->PURCHASE_GRADE_OpenUserCard(m_pUser->GetUserIDIndex(), m_sCurrentGradeInfo.btCurrentGrade, iCurrentDate))
			{
				iResult = PURCHASE_GRADE_GIVE_CARD_REWARD_SUCCESS;
				m_iRecentCardOpenDate = iCurrentDate;

				vector<int/*iRewardIndex*/> vec;
				PURCHASEGRADE.GetCardRewardIndex(m_sCurrentGradeInfo.btCurrentGrade, vec);

				for(int i = 0; i < vec.size(); ++i)
				{
					SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, vec[i]);
					if(NULL == pReward)
					{
						WRITE_LOG_NEW(LOG_TYPE_PURCHASE_GRADE, INITIALIZE_DATA, FAIL, "PURCHASE_GRADE_OpenUserCard. GiveReward Fail. UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), vec[i]);
}
				}
			}		
		}
		else
		{
			// 등급 변했으면 인벤토리의 등급카드 데이터 업데이트
			CFSGameUserItem* pUserItemList = m_pUser->GetUserItemList();
			CHECK_NULL_POINTER_BOOL(pUserItemList);

			CAvatarItemList* pItemList = pUserItemList->GetCurAvatarItemList();
			CHECK_NULL_POINTER_BOOL(pItemList);

			SUserItemInfo* pUserItemInfo = pItemList->GetInventoryItemWithPropertyKind(iPropertyKind, iItemCode);
			CHECK_NULL_POINTER_BOOL(pUserItemInfo);

			if(iCardItemCode <= 0)
			{
				pItemList->RemoveItem(pUserItemInfo);
			}
			else if(USER_ITEM_ARRAY_NUM_UNDRESS_PURCHASE_GRADE_CARD == pUserItemInfo->iArrayNum)
			{
				pUserItemInfo->iItemCode = PURCHASEGRADE.GetCardItemCode(m_sCurrentGradeInfo.btCurrentGrade);
				pUserItemInfo->iPropertyKind = PURCHASEGRADE.GetCardPropertyKind(m_sCurrentGradeInfo.btCurrentGrade);
			}

			iResult = PURCHASE_GRADE_GIVE_CARD_REWARD_FAIL_CHANGE_GRADE;
		}
	}
	
	CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
	PacketComposer.Add(OPEN_T_BOX_GRADECARD_RES_CODE);	
	PacketComposer.Add(iResult);	
	m_pUser->Send(&PacketComposer);	

	return TRUE;
}

BOOL CGameUserPurchaseGrade::UseCash(int iUseCash)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iYYYYMM = GetYYYYMMDD() / 100;

	CheckResetMonth(iYYYYMM);

	if (ODBC_RETURN_SUCCESS == pODBC->PURCHASE_GRADE_InsertUseCash(m_pUser->GetUserIDIndex(), iYYYYMM, iUseCash))
	{
		m_iSumUseCash_Next += iUseCash;
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_PURCHASE_GRADE, INITIALIZE_DATA, FAIL, "PURCHASE_GRADE_InsertUseCash error");
		return FALSE;
	}

	return TRUE;
}

float CGameUserPurchaseGrade::GetCardCombineRateUpValue()
{
	CheckResetMonth();

	return static_cast<float>(PURCHASEGRADE.GetCardCombineRateUpValue(m_sCurrentGradeInfo.btCurrentGrade)) / 100;
}

int	CGameUserPurchaseGrade::GetCurrentGradeCount(BYTE btGrade)
{
	CHECK_CONDITION_RETURN(btGrade >= MAX_PURCHASE_GRADE_TYPE, 0);
	return m_iaGradeCount[btGrade];
}

BOOL CGameUserPurchaseGrade::CheckAndGiveAchievement()
{
	int iAchievementConditionGroup = PURCHASEGRADE.GetAchievementConditionGroup(m_sCurrentGradeInfo.btCurrentGrade);
	CHECK_CONDITION_RETURN(iAchievementConditionGroup < 0, FALSE);

	vector<int> vParameters;
	vParameters.push_back(m_iaGradeCount[m_sCurrentGradeInfo.btCurrentGrade]);
	m_pUser->ProcessAchievementbyGroupAndComplete(iAchievementConditionGroup, ACHIEVEMENT_LOG_GROUP_SHOP, vParameters);

	return TRUE;
}
