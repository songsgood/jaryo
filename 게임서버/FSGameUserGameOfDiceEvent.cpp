qinclude "stdafx.h"
qinclude "FSGameUserGameOfDiceEvent.h"
qinclude "GameOfDiceEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "EventDateManager.h"
qinclude "ODBCSvrProxy.h"
qinclude "GameProxyManager.h"
qinclude "CFSGameServer.h"

CFSGameUserGameOfDiceEvent::CFSGameUserGameOfDiceEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserGameOfDiceEvent::~CFSGameUserGameOfDiceEvent(void)
{
}

BOOL CFSGameUserGameOfDiceEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen() && CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_GAMEOFDICE_RANK), TRUE);

	m_pUser->LoadRepresentInfo();

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	vector<SODBCGameOfDiceUserItemShopBuyLog> vUserItemShopBuyLog;
	vector<SODBCGameOfDiceRankSelectAvatar> vAvatar;
	SODBCGameOfDiceUserLog ODBCUserLog;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_GetUserData(m_pUser->GetUserIDIndex(), ODBCUserInfo) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_GetUserItemShopPriceLog(m_pUser->GetUserIDIndex(), vUserItemShopBuyLog) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_GetRankAvatarList(m_pUser->GetUserIDIndex(), vAvatar) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_GetUserLog(m_pUser->GetUserIDIndex(), ODBCUserLog))
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEOFDICE, INITIALIZE_DATA, FAIL, "EVENT_GAMEOFDICE_GetUserData error");
		return FALSE;
	}

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_GAMEOFDICE_TICKET);
	if(NULL != pItem)
		ODBCUserInfo._iDiceTicket = pItem->iPropertyTypeValue;

	if(ODBCUserInfo._iRankGameIDIndex <= 0)
	{
		const SREPRESENT_INFO* pInfo = m_pUser->GetRepresentInfo();
		if(pInfo)
		{
			ODBCUserInfo._iRankGameIDIndex = pInfo->iGameIDIndex;
			strncpy_s(ODBCUserInfo._szRankeGameID,_countof(ODBCUserInfo._szRankeGameID), pInfo->szGameID, MAX_GAMEID_LENGTH);
		}
	}

	m_UserGameOfDice.SetUserInfo(ODBCUserInfo);

	CheckAndUpdateRankGameID();

	for(int i = 0; i < vUserItemShopBuyLog.size(); ++i)
		m_mapUserItemLog[vUserItemShopBuyLog[i]._iItemIndex] = vUserItemShopBuyLog[i]._iBuyCount;

	for(int i = 0; i < vAvatar.size(); ++i)
	{
		if(vAvatar[i]._iGameIDIndex == m_pUser->GetGameIDIndex())
			vAvatar[i]._tLastDate = _time64(NULL);

		SGameOfDiceRankSelectAvatar Avatar;
		Avatar.SetAvatar(vAvatar[i]);

		m_mapRankSelectAvatar[Avatar._iGameIDIndex] = Avatar;
		m_listRankSelectAvatar.push_back(&m_mapRankSelectAvatar[Avatar._iGameIDIndex]);
	}
	m_listRankSelectAvatar.sort(SGameOfDiceRankSelectAvatar::SortByLv);

	m_UserLog.SetLog(ODBCUserLog);

	m_bDataLoaded = TRUE;

	CheckAndGiveReward();

	return TRUE;
}

void CFSGameUserGameOfDiceEvent::CheckAndGiveReward()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	m_UserGameOfDice.GetUserInfo(ODBCUserInfo);

	int iYYYYMMDD = TimetToYYYYMMDD(_time64(NULL) - (EVENT_RESET_HOUR * 60 * 60));
	int iRewardDiceTicketCount = GAMEOFDICEEVENT.GetRewardDiceTicketCount();
	int iRewardEventCoin = 0;
	if(m_UserGameOfDice._iDailyRewardUpdateDate == 0)	// 최초지급
		iRewardEventCoin = GAMEOFDICEEVENT.GetRewardEventCoin();
	else	
		CHECK_CONDITION_RETURN_VOID(m_UserGameOfDice._iDailyRewardUpdateDate >= iYYYYMMDD);

	ODBCUserInfo._iDiceTicket += iRewardDiceTicketCount;
	ODBCUserInfo._iEventCoin += iRewardEventCoin;
	ODBCUserInfo._iDailyRewardUpdateDate = iYYYYMMDD;
	m_UserLog._iTotalGetDiceTicket += iRewardDiceTicketCount;
	m_UserLog._iTotalGetEventCoin += iRewardEventCoin;

	SODBCGameOfDiceUserLog Log;
	m_UserLog.GetLog(Log);
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserData(EVENT_GAMEOFDICE_DB_LOG_TYPE_GIVE_DAILY_REWARD, m_pUser->GetUserIDIndex(), ODBCUserInfo, Log));

	CUserHighFrequencyItem* pHighFrequencyItem = m_pUser->GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_GAMEOFDICE_TICKET;
		sUserHighFrequencyItem.iPropertyTypeValue = ODBCUserInfo._iDiceTicket;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	m_UserGameOfDice._iDiceTicket = ODBCUserInfo._iDiceTicket;
	m_UserGameOfDice._iEventCoin = ODBCUserInfo._iEventCoin;
	m_UserGameOfDice._iDailyRewardUpdateDate = ODBCUserInfo._iDailyRewardUpdateDate;
	m_UserLog.SetLog(Log);

	SS2C_GAMEOFDICE_EVENT_REWARD_NOT info;
	info.iDiceTicket = iRewardDiceTicketCount;
	info.iEventCoin = iRewardEventCoin;
	m_pUser->Send(S2C_GAMEOFDICE_EVENT_REWARD_NOT, &info, sizeof(SS2C_GAMEOFDICE_EVENT_REWARD_NOT));
}

void CFSGameUserGameOfDiceEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == GAMEOFDICEEVENT.IsOpen());

	CheckAndGiveReward();

	CPacketComposer Packet(S2C_GAMEOFDICE_EVENT_INFO_RES);

	SS2C_GAMEOFDICE_EVENT_INFO_RES rs;
	MakeEventInfo(Packet, rs);

	m_pUser->Send(&Packet);
}

void CFSGameUserGameOfDiceEvent::SendRankAvatarList()
{
	SS2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_RES rs;
	rs.btListIndex = 0;
	rs.bLastList = FALSE;
	if(m_listRankSelectAvatar.size() <= MAX_GAMEOFDICE_EVENT_RANK_AVATAR_LIST_SIZE)
		rs.bLastList = TRUE;

	rs.iTotalAvatarCnt = m_listRankSelectAvatar.size();
	rs.iAvatarCnt = 0;

	CPacketComposer Packet(S2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	int iCount = 0;
	GAMEOFDICE_RANK_AVATAR_LIST::iterator iter = m_listRankSelectAvatar.begin();
	for( ; iter != m_listRankSelectAvatar.end(); ++iter)
	{
		SGAMEOFDICE_EVENT_RANK_AVATAR info;
		info.iGameIDIndex = (*iter)->_iGameIDIndex;
		strncpy_s(info.szGameID, _countof(info.szGameID), (*iter)->_szGameID, MAX_GAMEID_LENGTH);
		info.iGamePosition = (*iter)->_iGamePosition;
		info.btSex = (*iter)->_btSex;
		info.iLv = (*iter)->_iLv;
		Packet.Add((PBYTE)&info, sizeof(SGAMEOFDICE_EVENT_RANK_AVATAR));

		++rs.iAvatarCnt;

		if(++iCount >= MAX_GAMEOFDICE_EVENT_RANK_AVATAR_LIST_SIZE)
			break;
	}

	memcpy(pCountLocation, &rs.iAvatarCnt, sizeof(int));
	m_pUser->Send(&Packet);

	CHECK_CONDITION_RETURN_VOID(iCount == m_listRankSelectAvatar.size());

	Packet.Initialize(S2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_RES);
	rs.btListIndex = 1;
	rs.bLastList = TRUE;
	rs.iTotalAvatarCnt = m_listRankSelectAvatar.size();
	rs.iAvatarCnt = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_RES));
	pCountLocation = Packet.GetTail() - sizeof(int);

	iter++;
	iCount = 0;
	for( ; iter != m_listRankSelectAvatar.end(); ++iter)
	{
		SGAMEOFDICE_EVENT_RANK_AVATAR info;
		info.iGameIDIndex = (*iter)->_iGameIDIndex;
		strncpy_s(info.szGameID, _countof(info.szGameID), (*iter)->_szGameID, MAX_GAMEID_LENGTH);
		info.iGamePosition = (*iter)->_iGamePosition;
		info.btSex = (*iter)->_btSex;
		info.iLv = (*iter)->_iLv;
		Packet.Add((PBYTE)&info, sizeof(SGAMEOFDICE_EVENT_RANK_AVATAR));

		++rs.iAvatarCnt;

		if(++iCount >= MAX_GAMEOFDICE_EVENT_RANK_AVATAR_LIST_SIZE)
			break;
	}

	memcpy(pCountLocation, &rs.iAvatarCnt, sizeof(int));
	m_pUser->Send(&Packet);
}

void CFSGameUserGameOfDiceEvent::MakeEventInfo(CPacketComposer& Packet, SS2C_GAMEOFDICE_EVENT_INFO_RES& rs)
{
	rs.iDiceTicket = m_UserGameOfDice._iDiceTicket;
	rs.iEventCoin = m_UserGameOfDice._iEventCoin;
	rs.iBankEventCoin = m_UserGameOfDice._iBankEventCoin;
	rs.iRankGameIDIndex = m_UserGameOfDice._iRankGameIDIndex;
	rs.iCharacterIndex = m_UserGameOfDice._iCharacterIndex;
	rs.iCurrentTileNumber = m_UserGameOfDice._iCurrentTileNumber;
	rs.iPrisonEscapeCost = GAMEOFDICEEVENT.GetPrisonEscapeCost();
	rs.iRemainPrisonTurn = m_UserGameOfDice._iRemainPrisonTurn;
	rs.bUseDiceTicket = m_UserGameOfDice._bUseDiceTicket;
	rs.iRemainGoldCalfTurn = m_UserGameOfDice._iRemainGoldCalfTurn;
	rs.iRemainFreeTurn = m_UserGameOfDice._iRemainFreeTurn;
	rs.iRemainMiniGameTurn = m_UserGameOfDice._iRemainMiniGameTurn;

	GAMEOFDICEEVENT.MakeEventInfo(m_mapUserItemLog, Packet, rs);
}

RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR CFSGameUserGameOfDiceEvent::SelectRankAvatarReq(int iGameIDIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen(), RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_FAIL);
	CHECK_CONDITION_RETURN(m_UserGameOfDice._iRankGameIDIndex != -1, RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_FAIL);

	GAMEOFDICE_RANK_AVATAR_MAP::iterator iter = m_mapRankSelectAvatar.find(iGameIDIndex);
	CHECK_CONDITION_RETURN(iter == m_mapRankSelectAvatar.end(), RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_FAIL);

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	m_UserGameOfDice.GetUserInfo(ODBCUserInfo);

	ODBCUserInfo._iRankGameIDIndex = iter->second._iGameIDIndex;
	memcpy(ODBCUserInfo._szRankeGameID, iter->second._szGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);

	SODBCGameOfDiceUserLog Log;

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserData(EVENT_GAMEOFDICE_DB_LOG_TYPE_NONE, m_pUser->GetUserIDIndex(), ODBCUserInfo, Log), RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_FAIL);

	m_UserGameOfDice._iRankGameIDIndex = iter->second._iGameIDIndex;
	memcpy(m_UserGameOfDice._szRankeGameID, iter->second._szGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);

	return RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_SUCCESS;
}

RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER CFSGameUserGameOfDiceEvent::SelectCharacterReq(int iCharacterIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen(), RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER_FAIL);
	CHECK_CONDITION_RETURN(m_UserGameOfDice._iCharacterIndex != EVENT_GAMEOFDICE_CHARACTER_INDEX_NONE, RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER_FAIL_ALREADY_SELECTED);
	CHECK_CONDITION_RETURN(iCharacterIndex <= EVENT_GAMEOFDICE_CHARACTER_INDEX_NONE || iCharacterIndex > EVENT_GAMEOFDICE_CHARACTER_INDEX_2, RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER_FAIL);

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	m_UserGameOfDice.GetUserInfo(ODBCUserInfo);

	ODBCUserInfo._iCharacterIndex = iCharacterIndex;

	SODBCGameOfDiceUserLog Log;

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserData(EVENT_GAMEOFDICE_DB_LOG_TYPE_NONE, m_pUser->GetUserIDIndex(), ODBCUserInfo, Log), RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER_FAIL);

	m_UserGameOfDice._iCharacterIndex = iCharacterIndex;

	return RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER_SUCCESS;
}

RESULT_GAMEOFDICE_EVENT_DICE_ROLL CFSGameUserGameOfDiceEvent::DiceRollReq(BYTE btDiceRollType, SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES& rs)
{
	rs.iCurrentTileNumber = m_UserGameOfDice._iCurrentTileNumber;

	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen(), RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);
	CHECK_CONDITION_RETURN(m_UserGameOfDice._iCharacterIndex == EVENT_GAMEOFDICE_CHARACTER_INDEX_NONE, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);

	BYTE btCurrentTileKind = GAMEOFDICEEVENT.GetTileKind(m_UserGameOfDice._iCurrentTileNumber);
	// 행운로드,고난로드,감옥 제외 이용권 차감
	CHECK_CONDITION_RETURN((btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_MINIGAME ||
		(btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_MINIGAME && m_UserGameOfDice._iRemainMiniGameTurn == 0)) &&
		btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_LUCKY_LOAD &&
		((((GAMEOFDICE_EVENT_DICE_ROLL_TYPE_NONE == btDiceRollType && m_UserGameOfDice._iRemainPrisonTurn == 0) || 
		GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_SELECT_POPUP == btDiceRollType) && 
		btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_PRISON) || btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_PRISON) &&
		btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_HARDSHIP_LOAD &&
		m_UserGameOfDice._iDiceTicket <= 0 &&
		m_UserGameOfDice._iRemainFreeTurn <= 0
		, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL_NOT_EXIST_TICKET);  

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	m_UserGameOfDice.GetUserInfo(ODBCUserInfo);

	SODBCGameOfDiceUserLog Log;
	m_UserLog.GetLog(Log);

	int iGoingTurnCount = 0;

	if(GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_USE_EVENTCOIN == btDiceRollType &&
		EVENT_GAMEOFDICE_TILE_KIND_PRISON == btCurrentTileKind &&
		m_UserGameOfDice._iRemainPrisonTurn > 0)
	{
		// 감옥 다음턴 탈출 사용
		CHECK_CONDITION_RETURN(m_UserGameOfDice._iEventCoin < GAMEOFDICEEVENT.GetPrisonEscapeCost(), RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL_NOT_EXIST_EVENTCOIN);
		ODBCUserInfo._iEventCoin -= GAMEOFDICEEVENT.GetPrisonEscapeCost();
		ODBCUserInfo._iRemainPrisonTurn = 0;
		ODBCUserInfo._bUseDiceTicket = FALSE;
		Log._iTotalGetEventCoin -= GAMEOFDICEEVENT.GetPrisonEscapeCost();

		rs.btResult = RESULT_GAMEOFDICE_EVENT_DICE_ROLL_SUCCESS_PRISION_USE_EVENTCOIN;
		memset(rs.iDiceNumber, -1, sizeof(int)*MAX_EVENT_GAMEOFDICE_DICE_COUNT);
		rs.iNextTileNumber = m_UserGameOfDice._iCurrentTileNumber;
		rs.iEventCoin = GAMEOFDICEEVENT.GetPrisonEscapeCost();
		rs.iBankEventCoin = 0;
	}
	else
	{
		if(GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_SELECT_POPUP == btDiceRollType &&
			EVENT_GAMEOFDICE_TILE_KIND_PRISON == btCurrentTileKind)
		{
			// 감옥 탈출방법 선택
			CHECK_CONDITION_RETURN(m_UserGameOfDice._iRemainPrisonTurn == 0, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);
			ODBCUserInfo._bUseDiceTicket = TRUE;

			rs.btResult = RESULT_GAMEOFDICE_EVENT_DICE_ROLL_SUCCESS;
			memset(rs.iDiceNumber, -1, sizeof(int)*MAX_EVENT_GAMEOFDICE_DICE_COUNT);
			rs.iNextTileNumber = m_UserGameOfDice._iCurrentTileNumber;
			rs.iEventCoin = 0;
			rs.iBankEventCoin = 0;
		}
		else
		{
			rs.btResult = RESULT_GAMEOFDICE_EVENT_DICE_ROLL_SUCCESS;
			rs.iNextTileNumber = GAMEOFDICEEVENT.GetRandomNextTileNumber(ODBCUserInfo, rs, Log);
			CHECK_CONDITION_RETURN(rs.iNextTileNumber == -1, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);
			
			ODBCUserInfo._bUseDiceTicket = FALSE;
			if(rs.iDiceNumber[0] != rs.iDiceNumber[1] &&
				ODBCUserInfo._iRemainFreeTurn > 0)
				ODBCUserInfo._iRemainFreeTurn--;
		}
	
		if((btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_MINIGAME ||
			(btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_MINIGAME && m_UserGameOfDice._iRemainMiniGameTurn == 0)) &&
			btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_LUCKY_LOAD &&
			((((GAMEOFDICE_EVENT_DICE_ROLL_TYPE_NONE == btDiceRollType && m_UserGameOfDice._iRemainPrisonTurn == 0) || 
			GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_SELECT_POPUP == btDiceRollType) && 
			btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_PRISON) || btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_PRISON) &&
			btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_HARDSHIP_LOAD &&
			m_UserGameOfDice._iRemainFreeTurn <= 0)
		{
			ODBCUserInfo._iDiceTicket--;
		}
	}

	ODBCUserInfo._iCurrentTileNumber = rs.iNextTileNumber;

	// 더블
	if(GAMEOFDICEEVENT.GetTileKind(rs.iNextTileNumber) != EVENT_GAMEOFDICE_TILE_KIND_PRISON &&
		rs.iDiceNumber[0] != -1 &&
		rs.iDiceNumber[0] == rs.iDiceNumber[1])
		ODBCUserInfo._iRemainFreeTurn = 1;

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserData(EVENT_GAMEOFDICE_DB_LOG_TYPE_DICE_ROLL, m_pUser->GetUserIDIndex(), ODBCUserInfo, Log), RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);

	CUserHighFrequencyItem* pHighFrequencyItem = m_pUser->GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_GAMEOFDICE_TICKET;
		sUserHighFrequencyItem.iPropertyTypeValue = ODBCUserInfo._iDiceTicket;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	m_UserGameOfDice.SetUserInfo(ODBCUserInfo);
	m_UserLog.SetLog(Log);

	rs.iUpdatedDiceTicket = ODBCUserInfo._iDiceTicket;
	
	return (RESULT_GAMEOFDICE_EVENT_DICE_ROLL)rs.btResult;
}

RESULT_GAMEOFDICE_EVENT_BUY_ITEM CFSGameUserGameOfDiceEvent::BuyItemReq(int iItemIndex, CPacketComposer& Packet, SS2C_GAMEOFDICE_EVENT_BUY_ITEM_RES& rs)
{
	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen(), RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL);

	SEventGameOfDiceItemShopConfig Config;
	CHECK_CONDITION_RETURN(FALSE == GAMEOFDICEEVENT.GetItemShopConfig(iItemIndex, Config), RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL);

	int iBuyCount = 0;
	GAMEOFDICE_USER_ITEM_LOG_MAP::iterator iter = m_mapUserItemLog.find(iItemIndex);
	if(iter != m_mapUserItemLog.end())
		iBuyCount = iter->second;

	int iPrice = Config._iPrice + iBuyCount * GAMEOFDICEEVENT.GetItemIncreasePrice();
	CHECK_CONDITION_RETURN(m_UserGameOfDice._iEventCoin < iPrice, RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL_NOT_EXIST_COIN);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL_FULL_MAILBOX);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(Config._sReward.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL);

	int iUpdateBuyCount = 0, iUpdateEventCoin = 0;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserItemShopPriceLog(m_pUser->GetUserIDIndex(), iItemIndex, iPrice, Config._sReward.iRewardIndex, iUpdateBuyCount, iUpdateEventCoin), RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL);

	iter = m_mapUserItemLog.find(iItemIndex);
	if(iter != m_mapUserItemLog.end())
		iter->second = iUpdateBuyCount;
	else
		m_mapUserItemLog[iItemIndex] = iUpdateBuyCount;

	m_UserGameOfDice._iEventCoin = iUpdateEventCoin;

	pReward = REWARDMANAGER.GiveReward(m_pUser, Config._sReward.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_GAMEOFDICE_EVENT_BUY_ITEM_FAIL);
	
	rs.btResult = RESULT_GAMEOFDICE_EVENT_BUY_ITEM_SUCCESS;
	rs.iEventCoin = iUpdateEventCoin;
	GAMEOFDICEEVENT.MakeShopItemInfo(m_mapUserItemLog, Packet, rs);

	return RESULT_GAMEOFDICE_EVENT_BUY_ITEM_SUCCESS;
}

int CFSGameUserGameOfDiceEvent::GiveGameResultReward()
{
	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen(), 0);

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	m_UserGameOfDice.GetUserInfo(ODBCUserInfo);

	int iGiveDiceTicket = 1;
	ODBCUserInfo._iDiceTicket += iGiveDiceTicket;
	m_UserLog._iTotalGetDiceTicket += iGiveDiceTicket;

	SODBCGameOfDiceUserLog Log;
	m_UserLog.GetLog(Log);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, 0);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserData(EVENT_GAMEOFDICE_DB_LOG_TYPE_GIVE_GAMERESULT_REWARD, m_pUser->GetUserIDIndex(), ODBCUserInfo, Log), 0);

	CUserHighFrequencyItem* pHighFrequencyItem = m_pUser->GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_GAMEOFDICE_TICKET;
		sUserHighFrequencyItem.iPropertyTypeValue = ODBCUserInfo._iDiceTicket;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	m_UserGameOfDice._iDiceTicket = ODBCUserInfo._iDiceTicket;
	m_UserLog.SetLog(Log);

	return iGiveDiceTicket;
}

void CFSGameUserGameOfDiceEvent::MakeIntegrateDB_UpdateUserLog(SS2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT& not)
{
	if(0 >= m_UserGameOfDice._iRankGameIDIndex )
	{
		const SREPRESENT_INFO* pInfo = m_pUser->GetRepresentInfo();
		if(pInfo)
		{
			m_UserGameOfDice._iRankGameIDIndex = pInfo->iGameIDIndex;
			strncpy_s(m_UserGameOfDice._szRankeGameID,_countof(m_UserGameOfDice._szRankeGameID), pInfo->szGameID, MAX_GAMEID_LENGTH);
		}
	}

	not.iUserIDIndex = m_pUser->GetUserIDIndex();
	not.iGameIDIndex = m_UserGameOfDice._iRankGameIDIndex;
	memcpy(not.szGameID, m_UserGameOfDice._szRankeGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);

	m_UserLog.MakeLog(not);
}

RESULT_GAMEOFDICE_EVENT_DICE_ROLL CFSGameUserGameOfDiceEvent::TestDiceRollReq(int iDiceNumber, BYTE& btDiceRollType, SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES& rs)
{
	btDiceRollType = GAMEOFDICE_EVENT_DICE_ROLL_TYPE_NONE;
	rs.iCurrentTileNumber = m_UserGameOfDice._iCurrentTileNumber;

	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen(), RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);
	CHECK_CONDITION_RETURN(iDiceNumber < 1 || iDiceNumber > 12, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);

	BYTE btCurrentTileKind = GAMEOFDICEEVENT.GetTileKind(m_UserGameOfDice._iCurrentTileNumber);
	// 행운로드,고난로드,감옥 제외 이용권 차감
	CHECK_CONDITION_RETURN((btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_MINIGAME ||
		(btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_MINIGAME && m_UserGameOfDice._iRemainMiniGameTurn == 0)) &&
		btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_LUCKY_LOAD &&
		((((GAMEOFDICE_EVENT_DICE_ROLL_TYPE_NONE == btDiceRollType && m_UserGameOfDice._iRemainPrisonTurn == 0) || 
		GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_SELECT_POPUP == btDiceRollType) && 
		btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_PRISON) || btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_PRISON) &&
		btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_HARDSHIP_LOAD &&
		m_UserGameOfDice._iDiceTicket <= 0 &&
		m_UserGameOfDice._iRemainFreeTurn <= 0
		, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL_NOT_EXIST_TICKET);  

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	m_UserGameOfDice.GetUserInfo(ODBCUserInfo);

	SODBCGameOfDiceUserLog Log;
	m_UserLog.GetLog(Log);

	if(GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_USE_EVENTCOIN == btDiceRollType &&
		EVENT_GAMEOFDICE_TILE_KIND_PRISON == btCurrentTileKind &&
		m_UserGameOfDice._iRemainPrisonTurn > 0)
	{
		btDiceRollType = GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_USE_EVENTCOIN;
		return RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL;		
	}
	else
	{
		if(GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_SELECT_POPUP == btDiceRollType &&
			EVENT_GAMEOFDICE_TILE_KIND_PRISON == btCurrentTileKind)
		{
			btDiceRollType = GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_SELECT_POPUP;
			return RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL;		
		}
		else
		{
			rs.btResult = RESULT_GAMEOFDICE_EVENT_DICE_ROLL_SUCCESS;
			if(btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_PRISON)
				btDiceRollType = GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_CHALLENGE_DOUBLE;

			rs.iNextTileNumber = GAMEOFDICEEVENT.GetTestRandomNextTileNumber(iDiceNumber, ODBCUserInfo, rs, Log);
			CHECK_CONDITION_RETURN(rs.iNextTileNumber == -1, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);

			ODBCUserInfo._bUseDiceTicket = FALSE;
			if(rs.iDiceNumber[0] != rs.iDiceNumber[1] &&
				ODBCUserInfo._iRemainFreeTurn > 0)
				ODBCUserInfo._iRemainFreeTurn--;
		}

		if((btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_MINIGAME ||
			(btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_MINIGAME && m_UserGameOfDice._iRemainMiniGameTurn == 0)) &&
			btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_LUCKY_LOAD &&
			((((GAMEOFDICE_EVENT_DICE_ROLL_TYPE_NONE == btDiceRollType && m_UserGameOfDice._iRemainPrisonTurn == 0) || 
			GAMEOFDICE_EVENT_DICE_ROLL_TYPE_PRISION_SELECT_POPUP == btDiceRollType) && 
			btCurrentTileKind == EVENT_GAMEOFDICE_TILE_KIND_PRISON) || btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_PRISON) &&
			btCurrentTileKind != EVENT_GAMEOFDICE_TILE_KIND_HARDSHIP_LOAD &&
			m_UserGameOfDice._iRemainFreeTurn <= 0)
			ODBCUserInfo._iDiceTicket--;
	}

	ODBCUserInfo._iCurrentTileNumber = rs.iNextTileNumber;

	// 더블
	if(GAMEOFDICEEVENT.GetTileKind(rs.iNextTileNumber) != EVENT_GAMEOFDICE_TILE_KIND_PRISON &&
		rs.iDiceNumber[0] != -1 &&
		rs.iDiceNumber[0] == rs.iDiceNumber[1])
		ODBCUserInfo._iRemainFreeTurn = 1;

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserData(EVENT_GAMEOFDICE_DB_LOG_TYPE_DICE_ROLL, m_pUser->GetUserIDIndex(), ODBCUserInfo, Log), RESULT_GAMEOFDICE_EVENT_DICE_ROLL_FAIL);

	m_UserGameOfDice.SetUserInfo(ODBCUserInfo);
	m_UserLog.SetLog(Log);

	rs.iUpdatedDiceTicket = ODBCUserInfo._iDiceTicket;

	return (RESULT_GAMEOFDICE_EVENT_DICE_ROLL)rs.btResult;
}

void CFSGameUserGameOfDiceEvent::TestInitDiceTicket()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == GAMEOFDICEEVENT.IsOpen());

	SODBCGameOfDiceUserInfo ODBCUserInfo;
	m_UserGameOfDice.GetUserInfo(ODBCUserInfo);

	ODBCUserInfo._iDiceTicket = 1;

	SODBCGameOfDiceUserLog Log;
	m_UserLog.GetLog(Log);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_UpdateUserData(EVENT_GAMEOFDICE_DB_LOG_TYPE_NONE, m_pUser->GetUserIDIndex(), ODBCUserInfo, Log));

	m_UserGameOfDice._iDiceTicket = ODBCUserInfo._iDiceTicket;
}

int CFSGameUserGameOfDiceEvent::GetValue(BYTE btRankType)
{
	CHECK_CONDITION_RETURN(CLOSED == GAMEOFDICEEVENT.IsOpen() && CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_GAMEOFDICE_RANK), 0);
	CHECK_CONDITION_RETURN(btRankType >= MAX_EVENT_GAMEOFDICE_RANK_TYPE_COUNT, 0);

	switch(btRankType)
	{
	case EVENT_GAMEOFDICE_RANK_TYPE_RICH:			return m_UserLog._iTotalGetEventCoin;
	case EVENT_GAMEOFDICE_RANK_TYPE_PRISONER:		return m_UserLog._iGoingPrisonCount;
	case EVENT_GAMEOFDICE_RANK_TYPE_GAMBLER:		return m_UserLog._iMiniGameWinCount;
	case EVENT_GAMEOFDICE_RANK_TYPE_GOOD_TAX_PAYER:	return m_UserLog._iMaxBankEventCoin;
	case EVENT_GAMEOFDICE_RANK_TYPE_LUCKY_PLAYER:	return m_UserLog._iGoldCalfCount;
	default: return 0;
	}

	return 0;
}

void CFSGameUserGameOfDiceEvent::CheckAndUpdateRankGameID(CFSODBCBase* pODBC /*= nullptr*/)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == GAMEOFDICEEVENT.IsOpen());

	if(nullptr == pODBC)
		pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));

	CHECK_NULL_POINTER_VOID(pODBC);

	const SREPRESENT_INFO* pInfo = m_pUser->GetRepresentInfo();
	CHECK_NULL_POINTER_VOID(pInfo);

	if(m_UserGameOfDice._iRankGameIDIndex != pInfo->iGameIDIndex ||
		0 != ::strcmp(m_UserGameOfDice._szRankeGameID, pInfo->szGameID))
	{	// 대표 캐릭이 변경되었다.
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_GAMEOFDICE_UpdateUserRankID(m_pUser->GetUserIDIndex(), pInfo->iGameIDIndex, (char*)pInfo->szGameID))
		{
			CODBCSvrProxy* pODBCSvr =dynamic_cast<CODBCSvrProxy*>(GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY));
			if(nullptr != pODBCSvr)
			{
				CPacketComposer Packet(S2O_EVENT_GAMEOFDICE_USER_ID_UPDATE_NOT);
				SS2O_EVENT_GAMEOFDICE_USER_ID_UPDATE_NOT not;
				not.iUserIDIndex = m_pUser->GetUserIDIndex();
				not.iGameIDIndex = pInfo->iGameIDIndex;
				not.btServerIndex = _GetServerIndex;
				strncpy_s(not.szGameID,_countof(not.szGameID), pInfo->szGameID, MAX_GAMEID_LENGTH);

				Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_GAMEOFDICE_USER_ID_UPDATE_NOT));
				pODBCSvr->Send(&Packet);
			}

			m_UserGameOfDice._iRankGameIDIndex = pInfo->iGameIDIndex;
			strncpy_s(m_UserGameOfDice._szRankeGameID,_countof(m_UserGameOfDice._szRankeGameID), pInfo->szGameID, MAX_GAMEID_LENGTH);
		}
	}
}