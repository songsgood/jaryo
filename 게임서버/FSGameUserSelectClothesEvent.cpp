qinclude "stdafx.h"
qinclude "FSGameUserSelectClothesEvent.h"

qinclude "SelectClothesEventManager.h"
qinclude "RewardManager.h"
qinclude "EventDateManager.h"

qinclude "CFSGameUser.h"
qinclude "CFSGameUserItem.h"
qinclude "UserHighFrequencyItem.h"

CFSGameUserSelectClothesEvent::CFSGameUserSelectClothesEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}
CFSGameUserSelectClothesEvent::~CFSGameUserSelectClothesEvent(void)
{
}

BOOL CFSGameUserSelectClothesEvent::Load(void)
{
	CheckGiveRankReward();
	
	CHECK_CONDITION_RETURN(CLOSED == SELECTCLOTHESEVENT.IsOpen() && CLOSED == SELECTCLOTHESEVENT.IsRankOpen(), TRUE);

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_CONDITION_RETURN(NULL == pEventODBC, FALSE);

	if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_SELECT_CLOTHES_GetUserData(m_pUser->GetUserIDIndex(), m_tUserEventInfo.iUseKey, m_tUserEventInfo.btGiveCountRewardStep))
	{
		WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, SET_DATA, FAIL, "CFSGameUserSelectClothesEvent Load EVENT_SELECT_CLOTHES_GetUserData error!");

		return FALSE;
	}

	m_bDataLoaded = TRUE;

	CheckGiveCountReward();

	return TRUE;
}

void CFSGameUserSelectClothesEvent::SendUserInfo(void)
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_USER_INFO_RES);

	SS2C_EVENT_SELECT_CLOTHES_USER_INFO_RES rs;
	rs.iUseKey = m_tUserEventInfo.iUseKey;
	rs.iHavingKey = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount(ITEM_PROPERTY_KIND_EVENT_SELECT_CLOTHES);
	SELECTCLOTHESEVENT.FindUserRank(m_pUser->GetUserIDIndex(), rs.iRank);

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_USER_INFO_RES));

	m_pUser->Send(&Packet);
}

void CFSGameUserSelectClothesEvent::SendAvatarFeatureInfo(void)
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	SFeatureInfo FeatureInfo;
	m_pUser->GetUserItemList()->GetItemFeatureEventPreview(FeatureInfo);

	CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_AVATAR_FEATURE_RES);

	int iItemNum = 0;
	for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
	{
		if(-1 != FeatureInfo.iaFeature[i]) ++iItemNum;
	}
	Packet.Add(iItemNum);

	for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
	{	
		if(-1 != FeatureInfo.iaFeature[i]) 
		{
			SUserItemInfo* pItem = m_pUser->GetUserItemList()->GetCurAvatarItemList()->GetItemWithItemCodeInAll(FeatureInfo.iaFeature[i]);
			int iChannel = (NULL != pItem) ? pItem->iChannel : 0;

			Packet.Add(FeatureInfo.iaFeature[i]);
			Packet.Add(iChannel);
		}
	}

	int iChangeDressItemChannelNum = 0;
	Packet.Add(iChangeDressItemChannelNum);

	PBYTE pChangeDressItemChannelNum = Packet.GetTail() - sizeof(int);
	for(int i = 0; i < MAX_CHANGE_DRESS_NUM; ++i)
	{
		if(-1 != FeatureInfo.iaChangeDressFeature[i])
		{
			Packet.Add(FeatureInfo.iaChangeDressFeature[i]);

			++iChangeDressItemChannelNum;
		}
	}

	memcpy(pChangeDressItemChannelNum, &iChangeDressItemChannelNum, sizeof(int));

	m_pUser->Send(&Packet);
}

void CFSGameUserSelectClothesEvent::SendRankRewardInfo(void)
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_RANK_REWARD_RES);

	SS2C_EVENT_SELECT_CLOTHES_RANK_REWARD_RES rs;
	rs.btSendOption = EVENT_SELECT_CLOTHES_GACHA_REWARD_PACKET_START;
	rs.btDetailType = 0;
	rs.iRewardCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_RANK_REWARD_RES));
	m_pUser->Send(&Packet);

	for(BYTE bt = 0; bt < MAX_EVENT_SELECT_CLOTHES_RANK_REWARD_TYPE; ++bt)
	{
		Packet.Initialize(S2C_EVENT_SELECT_CLOTHES_RANK_REWARD_RES);

		rs.btSendOption = EVENT_SELECT_CLOTHES_GACHA_REWARD_PACKET_ING;
		rs.btDetailType = bt;
		rs.iRewardCount = 0;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_RANK_REWARD_RES));

		PBYTE pCount = Packet.GetTail() - sizeof(int);
		int iCount = 0;

		vector<SEVENT_SELECT_CLOTHES_REWARD_INFO> vecReward;
		SELECTCLOTHESEVENT.GetRankRewardInfo(bt, vecReward);

		for(int i = 0; i < vecReward.size(); ++i)
		{
			Packet.Add((PBYTE)&vecReward[i], sizeof(SEVENT_SELECT_CLOTHES_REWARD_INFO));

			++iCount;
		}
		memcpy(pCount, &iCount, sizeof(int));

		m_pUser->Send(&Packet);
	}

	Packet.Initialize(S2C_EVENT_SELECT_CLOTHES_RANK_REWARD_RES);

	rs.btSendOption = EVENT_SELECT_CLOTHES_GACHA_REWARD_PACKET_END;
	rs.btDetailType = 0;
	rs.iRewardCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_RANK_REWARD_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserSelectClothesEvent::SendGachaRewardInfo(void)
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_REWARD_RES);

	SS2C_EVENT_SELECT_CLOTHES_GACHA_REWARD_RES rs;
	rs.btSendOption = EVENT_SELECT_CLOTHES_GACHA_REWARD_PACKET_START;
	rs.btDetailType = 0;
	rs.iRewardCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_REWARD_RES));
	m_pUser->Send(&Packet);

	for(BYTE bt = 0; bt < MAX_EVENT_SELECT_CLOTHES_GACHA_REWARD_TYPE; ++bt)
	{
		Packet.Initialize(S2C_EVENT_SELECT_CLOTHES_GACHA_REWARD_RES);

		rs.btSendOption = EVENT_SELECT_CLOTHES_GACHA_REWARD_PACKET_ING;
		rs.btDetailType = bt;
		rs.iRewardCount = 0;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_REWARD_RES));

		PBYTE pCount = Packet.GetTail() - sizeof(int);
		int iCount = 0;

		vector<SEVENT_SELECT_CLOTHES_REWARD_INFO> vecReward;
		SELECTCLOTHESEVENT.GetGachaRewardInfo(bt, vecReward);

		for(int i = 0; i < vecReward.size(); ++i)
		{
			Packet.Add((PBYTE)&vecReward[i], sizeof(SEVENT_SELECT_CLOTHES_REWARD_INFO));

			++iCount;
		}

		memcpy(pCount, &iCount, sizeof(int));

		m_pUser->Send(&Packet);
	}

	Packet.Initialize(S2C_EVENT_SELECT_CLOTHES_GACHA_REWARD_RES);

	rs.btSendOption = EVENT_SELECT_CLOTHES_GACHA_REWARD_PACKET_END;
	rs.btDetailType = 0;
	rs.iRewardCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_REWARD_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserSelectClothesEvent::SendOpenBoxReq(BYTE btOpenType, BYTE btSelectFirstIndex, BYTE btSelectSecondIndex)
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	if(CLOSED == SELECTCLOTHESEVENT.IsOpen())
	{
		CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES);

		SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES rs;
		rs.btResult = RESULT_EVENT_SELECT_CLOTHES_OPEN_BOX_EXPIRE_EVENT;
		rs.iHavingKey = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount(ITEM_PROPERTY_KIND_EVENT_SELECT_CLOTHES);
		rs.iUseKey = m_tUserEventInfo.iUseKey;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES));

		m_pUser->Send(&Packet);
		return;
	}

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	if(NULL == pEventODBC)
	{
		CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES);

		SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES rs;
		rs.btResult = RESULT_EVENT_SELECT_CLOTHES_OPEN_BOX_FAIL;
		rs.iHavingKey = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount(ITEM_PROPERTY_KIND_EVENT_SELECT_CLOTHES);
		rs.iUseKey = m_tUserEventInfo.iUseKey;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES));

		m_pUser->Send(&Packet);
		return;
	}

	int iMaxRewardCnt = SELECTCLOTHESEVENT.GetGachaCount(btOpenType);
	int iUseCoin = SELECTCLOTHESEVENT.GetUseKeyCount(btOpenType);
	int iCoin = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount(ITEM_PROPERTY_KIND_EVENT_SELECT_CLOTHES);
	if(iUseCoin > iCoin)
	{
		CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES);

		SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES rs;
		rs.btResult = RESULT_EVENT_SELECT_CLOTHES_OPEN_BOX_LEAK_KEY;
		rs.iHavingKey = iCoin;
		rs.iUseKey = m_tUserEventInfo.iUseKey;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES));

		m_pUser->Send(&Packet);
		return;
	}

	if(MAX_PRESENT_LIST < m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + iMaxRewardCnt)
	{
		CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES);

		SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES rs;
		rs.btResult = RESULT_EVENT_SELECT_CLOTHES_OPEN_BOX_FULL_MAILBOX;
		rs.iHavingKey = iCoin;
		rs.iUseKey = m_tUserEventInfo.iUseKey;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES));

		m_pUser->Send(&Packet);
		return;
	}

	vector<int/*RewardIndex*/> vecReward;
	if(FALSE == SELECTCLOTHESEVENT.OpenBoxReq(btOpenType, btSelectFirstIndex, btSelectSecondIndex, vecReward))
	{
		WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, SET_DATA, FAIL, "SendOpenBoxReq SELECTCLOTHESEVENT.OpenBoxReq error - UserIDIndex:10752790, GameIDIndex:0, OpenType:7106560, SelectFirstIndex::-858993460, btSelectSecondIndex:-858993460", 
(), m_pUser->GetGameIDIndex(), btOpenType, btSelectFirstIndex, btSelectSecondIndex);

		CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES);

		SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES rs;
		rs.btResult = RESULT_EVENT_SELECT_CLOTHES_OPEN_BOX_UNKNOWN_INDEX;
		rs.iHavingKey = iCoin;
		rs.iUseKey = m_tUserEventInfo.iUseKey;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES));

		m_pUser->Send(&Packet);
		return;
	}

	int iUpdatedCoin = 0;
	if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_SELECT_CLOTHES_UseCoin(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), btOpenType, btSelectFirstIndex, btSelectSecondIndex, vecReward, iUseCoin, iUpdatedCoin))
	{
		WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, SET_DATA, FAIL, "SendOpenBoxReq EVENT_SELECT_CLOTHES_UseCoin - UserIDIndex:10752790, GameIDIndex:0, OpenType:7106560, SelectFirstIndex::-858993460, btSelectSecondIndex:-858993460", 
(), m_pUser->GetGameIDIndex(), btOpenType, btSelectFirstIndex, btSelectSecondIndex);

		CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES);

		SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES rs;
		rs.btResult = RESULT_EVENT_SELECT_CLOTHES_OPEN_BOX_FAIL;
		rs.iHavingKey = iCoin;
		rs.iUseKey = m_tUserEventInfo.iUseKey;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES));

		m_pUser->Send(&Packet);

		return;
	}

	m_tUserEventInfo.iUseKey += iUseCoin;

	CUserHighFrequencyItem* pHighFrequencyItem = m_pUser->GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_EVENT_SELECT_CLOTHES;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdatedCoin;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES);

	SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES rs;
	rs.btResult = RESULT_EVENT_SELECT_CLOTHES_OPEN_BOX_SUCCESS;
	rs.iHavingKey = iUpdatedCoin;
	rs.iUseKey = m_tUserEventInfo.iUseKey;
	rs.iRewardCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_RES));

	PBYTE pCount = Packet.GetTail() - sizeof(int);
	int iCount = 0;

	for(int i = 0; i < vecReward.size(); ++i)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, vecReward[i]);
		if(NULL == pReward)
		{
			WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, SET_DATA, FAIL, "CFSGameUserSelectClothesEvent SendOpenBoxReq GiveReward error - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
r->GetUserIDIndex(), m_pUser->GetGameIDIndex(), vecReward[i]);

			continue;
		}

		++iCount;

		Packet.Add((PBYTE)&vecReward[i], sizeof(int));
	}

	CheckGiveCountReward();

	memcpy(pCount, &iCount, sizeof(int));

	m_pUser->Send(&Packet);
}

void CFSGameUserSelectClothesEvent::SendRankList(void)
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	vector<SEVENT_SELECT_CLOTHES_RANK_INFO> vecRank;
	SELECTCLOTHESEVENT.GetRankList(vecRank);

	CPacketComposer Packet(S2C_EVENT_SELECT_CLOTHES_RANK_RES);

	SS2C_EVENT_SELECT_CLOTHES_RANK_RES rs;
	for(int i = 0 ; i < vecRank.size(); ++i)
	{
		if(MAX_EVENT_SELECT_CLOTHES_RANK_DISPLAY_COUNT <= i)
		{
			break;
		}

		rs.tRankInfo[i].btRank = vecRank[i].btRank;
		rs.tRankInfo[i].iUseKey = vecRank[i].iUseKey;
		strcpy_s(rs.tRankInfo[i].szGameID, _countof(rs.tRankInfo[i].szGameID), vecRank[i].szGameID);
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SELECT_CLOTHES_RANK_RES));
	m_pUser->Send(&Packet);
}

BOOL CFSGameUserSelectClothesEvent::CheckGiveCountReward(void)
{
	CHECK_CONDITION_RETURN(CLOSED == SELECTCLOTHESEVENT.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(m_tUserEventInfo.iUseKey <= 0, FALSE);

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_CONDITION_RETURN(NULL == pEventODBC, FALSE);
	
	BYTE btUserRewardStep = m_tUserEventInfo.btGiveCountRewardStep;
	BYTE btCountRewardStep = SELECTCLOTHESEVENT.GetCurrentCountRewardStep(m_tUserEventInfo.iUseKey);
	if(btUserRewardStep < btCountRewardStep)
	{
		CHECK_CONDITION_RETURN(MAX_PRESENT_LIST < m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + (btCountRewardStep - btUserRewardStep), FALSE);

		for(BYTE bt = btUserRewardStep; bt < btCountRewardStep; ++bt)
		{
			int iRewardIndex = 0;
			SELECTCLOTHESEVENT.GetRankRewardInfo(EVENT_SELECT_CLOTHES_RANK_REWARD_COUNT, bt, iRewardIndex);
			if(0 == iRewardIndex)
			{
				continue;
			}

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			CHECK_CONDITION_RETURN(NULL == pReward, FALSE);

			if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_SELECT_CLOTHES_UpdateUserData(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), bt))
			{
				WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, SET_DATA, FAIL, 
					"CFSGameUserSelectClothesEvent CheckGiveCountReward EVENT_SELECT_CLOTHES_UpdateUserData error - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
er->GetUserIDIndex(), m_pUser->GetGameIDIndex(), bt);

				return FALSE;
			}

			m_tUserEventInfo.btGiveCountRewardStep = (bt + 1);
		}
		
		return TRUE;
	}

	return FALSE;
}

BOOL CFSGameUserSelectClothesEvent::CheckGiveRankReward(void)
{
	time_t tEndTime = 0;
	EVENTDATEMANAGER.GetEventEndDate(EVENT_KIND_SELECT_CLOTHES, tEndTime);
	CHECK_CONDITION_RETURN(tEndTime > _time64(NULL), FALSE);

	int iRank = 0;
	SELECTCLOTHESEVENT.FindUserRank(m_pUser->GetUserIDIndex(), iRank);
	CHECK_CONDITION_RETURN(0 == iRank || MAX_EVENT_SELECT_CLOTHES_RANK_DISPLAY_COUNT < iRank, FALSE);

	CHECK_CONDITION_RETURN(FALSE == SELECTCLOTHESEVENT.CheckAlreadyGetRankReward(iRank), FALSE);

	int iRankRewardStep = SELECTCLOTHESEVENT.GetRankRewardStep(iRank);
	CHECK_CONDITION_RETURN(-1 == iRankRewardStep, FALSE);

	int iRewardIndex = 0;
	SELECTCLOTHESEVENT.GetRankRewardInfo(EVENT_SELECT_CLOTHES_RANK_REWARD_RANK, static_cast<BYTE>(iRankRewardStep), iRewardIndex);
	CHECK_CONDITION_RETURN(0 == iRewardIndex, FALSE);
	CHECK_CONDITION_RETURN(MAX_PRESENT_LIST < m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1, FALSE);

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_CONDITION_RETURN(NULL == pEventODBC, FALSE);
	if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_SELECT_CLOTHES_CheckGetRankReward(m_pUser->GetUserIDIndex(), iRank))
	{
		WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, SET_DATA, FAIL, "CFSGameUserSelectClothesEvent CheckGiveRankReward EVENT_SELECT_CLOTHES_CheckGetRankReward error - UserIDIndex:10752790, Rank:0", m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRank);
return FALSE;
	}

	SELECTCLOTHESEVENT.AddRankRewardLog(iRank);

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, FALSE);

	return TRUE;
}
