qinclude "stdafx.h"
qinclude "FSGameUserLoginPayBackEvent.h"
qinclude "CFSGameUser.h"
qinclude "EventDateManager.h"
qinclude "CFSGameServer.h"

CFSGameUserLoginPayBackEvent::CFSGameUserLoginPayBackEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}


CFSGameUserLoginPayBackEvent::~CFSGameUserLoginPayBackEvent(void)
{
}

BOOL CFSGameUserLoginPayBackEvent::Load()
{
	CHECK_CONDITION_RETURN(
		CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_LOGINPAYBACK_BUTTON) &&
		CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_LOGINPAYBACK_GIVECASH), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	time_t tCurrentTime = _GetCurrentDBDate;

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_LOGINPAYBACK_GetUserData(m_pUser->GetUserIDIndex(), TimetToYYYYMMDD(tCurrentTime), m_mapUserCashInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_LOGINPAYBACK_EVENT, INITIALIZE_DATA, FAIL, "EVENT_LOGINPAYBACK_GetUserData error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	CheckAndPayBackCash(tCurrentTime);

	return TRUE;
}

BOOL CFSGameUserLoginPayBackEvent::CheckAndPayBackCash(time_t tCurrentTime /*= _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_LOGINPAYBACK_GIVECASH), FALSE);
		
	int iCurrentDate = TimetToYYYYMMDD(tCurrentTime);
	if(CheckPayBackTarget(iCurrentDate))
	{
		CPacketComposer Packet(S2C_EVENT_LOGINPAYBACK_TARGET_NOT);
		m_pUser->Send(&Packet);
	}

	TIMESTAMP_STRUCT CurrentTime;
	TimetToTimeStruct(tCurrentTime, CurrentTime);

	CHECK_CONDITION_RETURN(CurrentTime.hour < LOGINPAYBACK_RESET_HOUR, FALSE);

	int iLastDate = TimetToYYYYMMDD(YYYYMMDDHHMMSSToTimeT(iCurrentDate, 0) - 60*60*24);
	
	USER_LOGINPAYBACK_INFO_MAP::iterator ItMap = m_mapUserCashInfo.find(iLastDate);
	CHECK_CONDITION_RETURN(ItMap == m_mapUserCashInfo.end(), FALSE);
	
	if(FALSE == ItMap->second.btIsRequest)
	{
		m_mapUserCashInfo.erase(ItMap);
		return FALSE;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_LOGINPAYBACK_CheckAndPayBackCash(m_pUser->GetUserIDIndex()
		, iLastDate, GetPayBackCash(ItMap->second.iUseCash)))
	{
		WRITE_LOG_NEW(LOG_TYPE_LOGINPAYBACK_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_LOGINPAYBACK_CheckAndPayBackCash error. UserIDIndex:10752790, CurrentDate:0", m_pUser->GetUserIDIndex(), iCurrentDate);
urn FALSE;
	}
	
	m_mapUserCashInfo.erase(ItMap);
	return TRUE;
}

void CFSGameUserLoginPayBackEvent::SendLoginPayBackUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_LOGINPAYBACK_BUTTON));

	time_t tCurrentTime = _GetCurrentDBDate;

	CheckAndPayBackCash(tCurrentTime);

	SS2C_EVENT_LOGINPAYBACK_USER_INFO_RES ss;
	ZeroMemory(&ss, sizeof(SS2C_EVENT_LOGINPAYBACK_USER_INFO_RES));

	int iCurrentDate = TimetToYYYYMMDD(tCurrentTime);

	USER_LOGINPAYBACK_INFO_MAP::iterator ItMap = m_mapUserCashInfo.find(iCurrentDate);
	if(ItMap != m_mapUserCashInfo.end())
	{
		ss.iPayBackCash = GetPayBackCash(ItMap->second.iUseCash);
		ss.btIsRequest = ItMap->second.btIsRequest;
	}

	CPacketComposer Packet(S2C_EVENT_LOGINPAYBACK_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_EVENT_LOGINPAYBACK_USER_INFO_RES));

	m_pUser->Send(&Packet);
}

void CFSGameUserLoginPayBackEvent::RequestPayBack()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_LOGINPAYBACK_BUTTON));

	time_t tCurrentTime = _GetCurrentDBDate;

	CheckAndPayBackCash(tCurrentTime);

	SS2C_EVENT_LOGINPAYBACK_REQUEST_RES ss;
	ZeroMemory(&ss, sizeof(SS2C_EVENT_LOGINPAYBACK_REQUEST_RES));

	int iCurrentDate = TimetToYYYYMMDD(tCurrentTime);

	USER_LOGINPAYBACK_INFO_MAP::iterator ItMap = m_mapUserCashInfo.find(iCurrentDate);
	
	if(ItMap == m_mapUserCashInfo.end() ||
		ItMap->second.iUseCash <= 0)
	{
		ss.btResult = LOGINPAYBACK_REQUEST_NO_USECASH_FAIL;
	}
	else if(TRUE == ItMap->second.btIsRequest)
	{
		ss.btResult = LOGINPAYBACK_REQUEST_ALREADY_FAIL;
	}
	else
	{
		ss.btResult = LOGINPAYBACK_REQUEST_DB_UPDATE_FAIL;

		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_LOGINPAYBACK_RequestPayBack(m_pUser->GetUserIDIndex(), iCurrentDate))
		{
			ItMap->second.btIsRequest = TRUE;
			ss.btResult = LOGINPAYBACK_REQUEST_SUCCESS;
		}
	}

	CPacketComposer Packet(S2C_EVENT_LOGINPAYBACK_REQUEST_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_EVENT_LOGINPAYBACK_REQUEST_RES));

	m_pUser->Send(&Packet);
}

void CFSGameUserLoginPayBackEvent::CheckUseCash(int iUseCash)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_LOGINPAYBACK_BUTTON));
	CHECK_CONDITION_RETURN_VOID(iUseCash <= 0);

	time_t tCurrentTime = _GetCurrentDBDate;

	CheckAndPayBackCash(tCurrentTime);

	int iCurrentDate = TimetToYYYYMMDD(tCurrentTime);

	USER_LOGINPAYBACK_INFO_MAP::iterator ItMap = m_mapUserCashInfo.find(iCurrentDate);
	CHECK_CONDITION_RETURN_VOID(ItMap != m_mapUserCashInfo.end() && TRUE == ItMap->second.btIsRequest);
	
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_LOGINPAYBACK_UseCash(m_pUser->GetUserIDIndex(), iCurrentDate, iUseCash))
	{
		WRITE_LOG_NEW(LOG_TYPE_LOGINPAYBACK_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_LOGINPAYBACK_UseCash error. UserIDIndex:10752790, UseCash:0", m_pUser->GetUserIDIndex(), iUseCash);
urn;
	}

	if(ItMap != m_mapUserCashInfo.end())
	{
		ItMap->second.iUseCash += iUseCash;
	}
	else
	{
		SUserLoginPayBackInfo sInfo;
		sInfo.iUseCash = iUseCash;
		sInfo.btIsRequest = FALSE;

		m_mapUserCashInfo.insert(USER_LOGINPAYBACK_INFO_MAP::value_type(iCurrentDate, sInfo));
	}

	CPacketComposer Packet(S2C_EVENT_LOGINPAYBACK_TARGET_NOT);
	m_pUser->Send(&Packet);
}

BOOL CFSGameUserLoginPayBackEvent::CheckPayBackTarget(int iCurrentDate/* = GetYYYYMMDD()*/)
{
	USER_LOGINPAYBACK_INFO_MAP::iterator ItMap = m_mapUserCashInfo.find(iCurrentDate);
	CHECK_CONDITION_RETURN(ItMap == m_mapUserCashInfo.end(), FALSE);
	CHECK_CONDITION_RETURN(GetPayBackCash(ItMap->second.iUseCash) <= 0, FALSE);
	CHECK_CONDITION_RETURN(ItMap->second.btIsRequest == TRUE, FALSE);
	
	return TRUE;
}

int	CFSGameUserLoginPayBackEvent::GetPayBackCash(int iUseCash)
{
	int iPayBackCash = iUseCash * LOGINPAYBACK_PERCENT / 100;

	return (iPayBackCash > LOGINPAYBACK_MAX_CASH) ? LOGINPAYBACK_MAX_CASH : iPayBackCash;
}