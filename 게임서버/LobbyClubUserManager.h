qpragma once
qinclude "Singleton.h"
qinclude "Lock.h"

class CFSGameUser;

typedef map<int/*iGameIDIndex*/, CFSGameUser*> GAMEUSERMAP;

class CLobbyClubUserManager : public Singleton<CLobbyClubUserManager>
{
public:
	CLobbyClubUserManager(void);
	virtual ~CLobbyClubUserManager(void);

	void AddUser(CFSGameUser* pUser);
	void RemoveUser(int iGameIDIndex);

	void Broadcast(int iClubSN, CPacketComposer& Packet, BOOL bNotInPlaying = FALSE);

	void SetClubCheerLeaderCode(int iClubSN, int iExpiredCheerLeaderCode, int iChangedCheerLeaderCode);
	void SetClubCheerLeaderCode(int iClubSN, int iChangedCheerLeaderCode, BYTE btTypeNum);
	void SetClubPennantCode(int iClubSN, short stSlotIndex, int iPennantCode);
	void SetClubName(int iClubSN, char* szClubName);

	void GiveClubClothes(int iClubSN, vector<int>& vGameIDIndex);

	void UpdateClubContributionPointRankSeasonIndex(int iSeasonIndex);
	void UpdateClubLeagueRankSeasonIndex(int iSeasonIndex);
	void UpdateUserClubContributionPointRank(int iSeasonIndex, vector<SCLUB_UPDATE_MEMBER_RANKING>& vRank);
	void UpdateUserClubLeagueRank(int iSeasonIndex, vector<SCLUB_UPDATE_MEMBER_RANKING>& vRank);

private:
	LOCK_RESOURCE(m_csUserMap);
	GAMEUSERMAP	m_mapUser;
};

#define LOBBYCLUBUSER CLobbyClubUserManager::GetInstance() 
