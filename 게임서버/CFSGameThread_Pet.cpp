qinclude "stdafx.h"
qinclude "TTFSGameSvr.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameODBC.h"
qinclude "CFSGameClient.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "CFSGameUserPet.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSFileLog.h"
qinclude "FSMiscellaneous.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


extern CLogManager		g_LogManager;
extern CFSGameDBCfg	*	g_pFSDBCfg; 
extern CFSFileLog		g_FSFileLog;
extern CFSCrashLog		g_FSCrashLog;
extern  CFSHackShieldLog	g_FSHackShieldLog;


void CFSGameThread::Process_FSPSendUserPetList(CFSGameClient* pFSClient)
{
	CHECK_NULL_POINTER_VOID(pFSClient);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserPet* pUserPet = pUser->GetUserPetList();
	CHECK_NULL_POINTER_VOID(pUserPet);

	int iUserPetCnt = pUserPet->GetUserPetCount();

	SUserPetInfo aPetList[MAX_PET_KIND];
	pUserPet->GetAvatarPetList(aPetList);

	CPacketComposer PacketComposer(S2C_USERPET_LIST_RES);
	PacketComposer.Add(iUserPetCnt);
	for(int i = 0; i < iUserPetCnt; i++)
	{
		int iClntIdx = (aPetList[i].iIndex + 1) * -1;
		PacketComposer.Add(iClntIdx);
		PacketComposer.Add(aPetList[i].iCode);

		/* 일단 인벤토리에 아이템 처럼 보여지게 하기 위해 다음의 코드를 정의한다. 추후 아래 코드는 필요 없을 수 있음 */
		int iInventoryButtonType = 4; /* 4는 먹이기 버튼이 활성화 되는 타입으로 정의한다. */

		PacketComposer.Add(iInventoryButtonType);

		/* 추후 먹이 충전 시스템이 들어갈 경우 아래 코드를 이용한다 */
		//PacketComposer.Add(aPetList[i].iFeedingStatus);
		//PacketComposer.Add(aPetList[i].iCapacity);
	}
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSPFeedUserPet(CFSGameClient* pFSClient)
{
	CHECK_NULL_POINTER_VOID(pFSClient);
	CFSGameUser* pUser = (CFSGameUser* )pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iPetIdx = -1;
	/* 우선 먹이는 이번 스펙에서 한번에 하나식 줄 수 있다. */
	int iPortion = 1;
	m_ReceivePacketBuffer.Read(&iPetIdx);

	int iSvrIdx = (iPetIdx * -1) - 1;

	if(iSvrIdx < 0 || iSvrIdx > 2)
	{
		return;
	}

	CFSGameUserPet* pUserPet = pUser->GetUserPetList();
	CHECK_NULL_POINTER_VOID(pUserPet);

	CFSGameUserItem* pUserItem = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItem);

	int iSuccess = -1;

	if( pUserPet->FeedUserPet((CFSGameODBC*)m_pNexusODBC, iSvrIdx, iPortion, pUserItem) )
	{
		iSuccess = 0;
	}

	CPacketComposer PacketComposer(S2C_PET_FEEDING_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);

	/* 지금은 1먹이만 먹이면 바로 펫보물상자가 Open되기 때문에 다음 처리를 한다. 클라이언트에서 C2S_PET_OPEN_REQ패킷을 전송하지 않음.
	추후에 유저가 원할 때 펫을 열게 하고 싶다면, 즉 '먹이기'와 '열기'를 분리하고 싶다면 아래 패킷은 별도로 처리함 */ 
	if( iSuccess == 0 )
	{
		Process_FSPOpenUserPet(pFSClient, iSvrIdx);
		// 상자를 열었으니, 펫의 충전상태를 초기화 지금은 필요 없다. 
		//pUserPet->SetUserPetFeedingStatus(iSvrIdx, 0);
	}
}

void CFSGameThread::Process_FSPOpenUserPet(CFSGameClient* pFSClient, int iPetIdx)
{
	CHECK_NULL_POINTER_VOID(pFSClient);

	CFSGameUser* pUser = (CFSGameUser* )pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer* )pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUserItem* pUserItem = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItem);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CAvatarItemList* pAvatarItemList = pUser->GetCurAvatarItemListWithGameID(pUser->GetGameID());
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	CFSGameUserPet* pUserPet = pUser->GetUserPetList();
	CHECK_NULL_POINTER_VOID(pUserPet);

	srand((unsigned)time(NULL));
	int iPercent = (((long)rand() << 15) | rand()) TREASURE_BOX_PERCENT;

	
	int iItemIndex = -1;
	int iItemNum = -1;
	int iResult = -1;
	
	int iCategory = 0 , iPage = 0;
	
	int iPropertyKind = -1;
	int iPoint = 0;
	int iNewItemCode = -1;
	int iIndex = -1;
	int iPropertyType = 0;
	int iMyPropertyKind = -1;
	int iPropertyTypeValue = -100, iProperty0 = 0, iValue0 = 0, iProperty1 = 0, iValue1 = 0;
	int iYear = 0, iMonth = 0, iDay = 0, iHour = 0, iMin = 0;
	int iSellPrice = 0;
	int iPropertyIndex1 = -1;
	int iPropertyIndex2 = -1;
	int iPropertyIndex3 = -1;

	SUserPetInfo aPetList[MAX_PET_KIND];
	pUserPet->GetAvatarPetList(aPetList);

	iPropertyKind = aPetList[iPetIdx].iCode;

	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC *)m_pNexusODBC)->spOpenPet( pUser->GetGameIDIndex(), iPropertyKind, iPercent, iPoint, iNewItemCode, iItemIndex, iIndex, 
		iPropertyType, iPropertyTypeValue, iMyPropertyKind, iPropertyIndex1, iPropertyIndex2, iPropertyIndex3,
		iYear, iMonth, iDay, iHour, iMin, iSellPrice, iResult ) )
	{
		g_LogManager.WriteLogToFile(" Magic Box Failed - ��4 ",pUser->GetGameID());
	return;
	}
	
	if( iResult == 0 && iPoint == -1 )
	{
		iResult = 0;
	}
	else if( ( iResult == 0 && iPoint > -1 ) || ( iResult == 0 && iPoint > -1 ) )
	{
		iResult = 1;
	}
	
	if( iResult >= 0 )
	{
		int iData = -1;
		
		if( iResult == 1 || iResult == 2 )
		{
			pUser->SetUserSkillPoint( pUser->GetUserSkillPoint() + iPoint );
			iData= iPoint;
			
			CPacketComposer PacketComposer(S2C_MONEY_CHANGE_RES);
			
			PacketComposer.Add(-1);
			PacketComposer.Add(pUser->GetUserCoin());
			PacketComposer.Add(pUser->GetUserSkillPoint());
			
			pUser->Send(&PacketComposer);
			
		}
		else
		{
			iData = iNewItemCode;
		}
		
		
		CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
		PacketComposer.Add(iResult);
		PacketComposer.Add(iData);
		
		pUser->Send(&PacketComposer);
		
	}
	
	
	if( iResult == 0 && iNewItemCode != 200002)
	{
		SUserItemInfo UserItemInfo;
		SShopItemInfo ShopItemInfo;
		
		int iErrorCode = 0;
		
		if( FALSE == pItemShop->GetItem(iNewItemCode , ShopItemInfo) )  
		{
			iErrorCode = -5;
		}
		else
		{
			UserItemInfo.iItemIdx = iItemIndex;
			UserItemInfo.iChannel = ShopItemInfo.iChannel;
			UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;
			
			// 20080829 선물 상자 버그 수정
			if( ShopItemInfo.iKind == ITEM_KIND_PCROOM )
			{
				UserItemInfo.iKind = ShopItemInfo.iKind + 1;
			}
			else
			{
				UserItemInfo.iKind = ShopItemInfo.iKind;
			}
			
			UserItemInfo.iItemCode = iNewItemCode;
			UserItemInfo.iSellType = 2; // 1: cash   2: point		
			
			
			UserItemInfo.iSellPrice	= iSellPrice;
			
			UserItemInfo.iStatus = 1;
			
			UserItemInfo.iPropertyType = iPropertyType;
			UserItemInfo.iPropertyTypeValue = iPropertyTypeValue;		// 20060917 TestCode
			
			UserItemInfo.iPropertyKind =  iMyPropertyKind;
			
			// 속성
			CItemPropertyList* pItemProperty1 = NULL;
			CItemPropertyList* pItemProperty2 = NULL;
			CItemPropertyList* pItemProperty3 = NULL;
			
			if( iPropertyIndex1 != -1 )
			{
				pItemProperty1 = pItemShop->GetItemPropertyList( iPropertyIndex1 );
				if( pItemProperty1 == NULL )
				{
					iErrorCode = -5;
					return;
				}
			}
			
			if( iPropertyIndex2 != -1 )
			{
				pItemProperty2 = pItemShop->GetItemPropertyList( iPropertyIndex2 );
				if( pItemProperty2 == NULL )
				{
					iErrorCode = -5;
					return;
				}
			}
			
			if( iPropertyIndex3 != -1 )
			{
				pItemProperty3 = pItemShop->GetItemPropertyList( iPropertyIndex3 );
				if( pItemProperty3 == NULL )
				{
					iErrorCode = -5;
					return;
				}
			}
			
			if( iPropertyIndex1 != -1 )
			{
				vector< SItemProperty > vItemProperty1;
				pItemProperty1->GetItemProperty( iPropertyIndex1, vItemProperty1 );
				pUserItem->InsertUserItemProperty( pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, pAvatarItemList, vItemProperty1);
				UserItemInfo.iPropertyNum	+= vItemProperty1.size();
				
			}
			
			if( iPropertyIndex2 != -1 )
			{
				vector< SItemProperty > vItemProperty2;
				pItemProperty2->GetItemProperty( iPropertyIndex2, vItemProperty2 );
				pUserItem->InsertUserItemProperty( pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, pAvatarItemList, vItemProperty2);
				UserItemInfo.iPropertyNum	+= vItemProperty2.size();
				
			}
			
			if( iPropertyIndex3 != -1 )
			{
				vector< SItemProperty > vItemProperty3;
				pItemProperty3->GetItemProperty( iPropertyIndex3, vItemProperty3 );
				pUserItem->InsertUserItemProperty( pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, pAvatarItemList, vItemProperty3);
				UserItemInfo.iPropertyNum	+= vItemProperty3.size();
			}
			
			UserItemInfo.ExpireDate.iYear = iYear;
			UserItemInfo.ExpireDate.iMonth = iMonth;
			UserItemInfo.ExpireDate.iDay = iDay;
			UserItemInfo.ExpireDate.iHour = iHour;
			UserItemInfo.ExpireDate.iMin = iMin;
			UserItemInfo.iPropertyTypeValue = iPropertyTypeValue;
			
			UserItemInfo.iPropertyKind =  iMyPropertyKind;
			
			int res = 0;
			//				
			iErrorCode = 0;
			
			
			
			SUserItemInfo* pItem  = pAvatarItemList->GetItemWithItemIdx(UserItemInfo.iItemIdx);
			
			if( NULL != pItem && pItem->iStatus == 0 )
			{
				
				pItem->iItemCode = UserItemInfo.iItemCode;
				pItem->iKind = UserItemInfo.iKind;
				
				pItem->iSellType = UserItemInfo.iSellType;
				pItem->iChannel = ShopItemInfo.iChannel;
				pItem->iSexCondition = UserItemInfo.iSexCondition;
				pItem->iSellPrice = UserItemInfo.iSellPrice;
				pItem->iStatus = 1;
				
				pItem->iPropertyType = UserItemInfo.iPropertyType;
				pItem->iPropertyTypeValue = UserItemInfo.iPropertyTypeValue;		
				pItem->iPropertyKind = UserItemInfo.iPropertyKind;	//// 20060525 1182 Item Update Bug Fix
				pItem->iPropertyNum = UserItemInfo.iPropertyNum;
				memcpy(&pItem->ExpireDate, &UserItemInfo.ExpireDate, sizeof(SDateInfo));	//20050930 time based item
			}
			else
			{
				pAvatarItemList->AddItem(UserItemInfo);
			}
		}
	}
	else if( iResult == 0 && iNewItemCode == 200002 )
	{
		if( iIndex != -1 )
		{
			pUser->RecvPresent(iIndex, (CFSGameODBC *)m_pNexusODBC );
		}
	}
	else if( iResult < 0 )
	{
		CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
		PacketComposer.Add(iResult);
		
		pUser->Send(&PacketComposer);
	}

	SendAvatarInfo(pFSClient, 1);
	
	// 우선 이벤 스펙에서 이 패킷은 사용하지 않음
	//CPacketComposer PacketComposer(S2C_PET_OPEN_RES);
	//pFSClient->Send(&PacketComposer);
}
