#pragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserShoppingGodEvent
{
public:
	CFSGameUserShoppingGodEvent(CFSGameUser* pUser);
	~CFSGameUserShoppingGodEvent(void);

	BOOL				Load();
	void				SendEventStatus();
	void				SendShoppingGodEventInfo();
	void				SendShoppingGodUserInfo();
	void				SendLoginTimeInfo();
	BOOL				CheckResetTime(time_t tCurrentTime = _time64(NULL));
	BOOL				CheckAndUpdateUserMission(BYTE btMissionType, time_t tCurrentTime = _time64(NULL));
	BOOL				UseChip(SC2S_SHOPPING_GOD_USE_CHIP_REQ& rs);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;
	SShoppingGodUserInfo	m_sUserInfo;
};