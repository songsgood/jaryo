﻿// CenterSvrProxy.cpp: implementation of the CCenterSvrProxy class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CenterSvrProxy.h"
qinclude "CFSGameServer.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameUser.h"
qinclude "FriendList.h"
qinclude "CChannelBuffManager.h"
qinclude "ClubSvrProxy.h"
qinclude "CFSGameClient.h"
qinclude "MatchSvrProxy.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSSvrList.h"
qinclude "CIntensivePracticeManager.h"
qinclude "MatchingPoolCareManager.h"
qinclude "FactionManager.h"
qinclude "CFSLobby.h"
qinclude "CustomizeManager.h"
qinclude "RewardManager.h"
qinclude "LobbyChatUserManager.h"
qinclude "EventDateManager.h"
qinclude "ShoppingFestivalEventManager.h"
qinclude "MissionCashLimitEventManager.h"
qinclude "ShutdownCommon.h"
qinclude "PVEManager.h"
qinclude "ClubConfigManager.h"
qinclude "PremiumPassEventManager.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#ifndef DEFINE_CENTERPROXY_PROC_FUNC(x)
#define DEFINE_CENTERPROXY_PROC_FUNC(x)	void CCenterSvrProxy::Proc_##x(CReceivePacketBuffer* rp)
#endif

CCenterSvrProxy::CCenterSvrProxy()
{
	SetState(FS_CENTER_SERVER_PROXY);
}

CCenterSvrProxy::~CCenterSvrProxy()
{
}

void CCenterSvrProxy::SendFirstPacket()
{
	CPacketComposer PacketComposer(G2S_CLIENT_INFO);
	SG2S_CLIENT_INFO info;
	
	CFSGameServer* pServer = (CFSGameServer *)GetServer();
	
	info.ProcessID = pServer->GetProcessID();	
	info.iServerType = SERVER_TYPE_NORMAL;

	strncpy_s(info.ProcessName, _countof(info.ProcessName), pServer->GetProcessInfo()->szServerName, MAX_PROCESS_NAME_LENGTH);
	
	PacketComposer.Add((BYTE*)&info, sizeof(SG2S_CLIENT_INFO));
	
	Send(&PacketComposer);
}

void CCenterSvrProxy::ProcessPacket(CReceivePacketBuffer* recvPacket)
{
#define CASE_CENTERPROXY_PACKET_PROC_FUNC(x)	case x:	Proc_##x(recvPacket);	break;

	switch(recvPacket->GetCommand())
	{
	case S2G_FRIEND_LIST:						Process_FriendList(recvPacket);					break;
	case S2G_MSG_AT_FRIEND:						Process_MsgAtFriend(recvPacket);				break;
	case S2G_MSG_TO_FRIEND_FAIL:				Process_MsgToFriendFail(recvPacket);			break;
	case S2G_ADD_FRIEND:						Process_AddFriendReq(recvPacket);				break;	
	case S2G_DEL_FRIEND:						Process_DelFriend(recvPacket);					break;
	case S2G_ADD_BANNED_USER:					Process_AddBannedUserReq(recvPacket);			break;	
	case S2G_DEL_BANNED_USER:					Process_DelBannedUser(recvPacket);				break;
	case S2G_ADD_INTEREST_USER:					Process_AddInterestUserReq(recvPacket);			break;	
	case S2G_DEL_INTEREST_USER:					Process_DelInterestUser(recvPacket);			break;	
	case S2G_USER_STATUS_UPDATE:				Process_UserStatusUpdate(recvPacket);			break;
	case S2G_USER_NOTIFY:						Process_UserNotify(recvPacket);					break;
	case S2G_USER_SMALLINFO:					Process_UserSInfo(recvPacket);					break;
	case S2G_USER_SHOUT:						Process_UserShout(recvPacket);					break;
	case S2G_SHOUT_MESSAGE_TRN:					Process_ShoutMessage( recvPacket );				break;
	case S2G_SYSTEM_SHOUT_MESSAGE_TRN:			Process_SystemShoutMessage( recvPacket);		break;
	case S2G_SHOUT_MESSAGE_RES:					Process_ShoutMessageResult( recvPacket );		break;
	case S2G_CHANGE_FRIEND_GAMEID:				Process_FriendIDChangeNotify(recvPacket);		break;
	case S2G_CHANGE_GAMEID_RES:					Process_ChangeUserName(recvPacket);				break;
	case S2G_EVENT_SHOUT_MESSAGE_RES:			Process_EventShoutMsg( recvPacket );			break;
	case S2G_MANAGER_USER_KICK_REQ :			Process_GameManagerKickOut( recvPacket );		break;
	case S2G_MANAGER_ANNOUNCE:					Process_FSAnnounce(recvPacket);					break;
	case S2G_WHISPER_RESULT:					Process_WhisperResult(recvPacket);				break;
	case S2G_USE_CHANNEL_BUFF_ITEM_RES:			Process_UseChannelBuffItem(recvPacket);			break;
	case S2G_LIMITED_EDITION_ITEM_COUNT_RES:	Process_LimitedEditionItemCount(recvPacket);	break;
	case S2G_CLUB_JOIN_RES:						Process_ClubJoinRes(recvPacket);				break;
	case S2G_CLUB_WITHDRAW_RES:					Process_ClubWithDrawRes(recvPacket);			break;
	case S2G_CLUB_UPDATE_ADMIN_MEMBER_RES:		Process_ClubUpdateAdminMemberRes(recvPacket);	break;
	case S2G_CLUB_UPDATE_KEY_PLAYER_RES:		Process_ClubUpdateKeyPlayerRes(recvPacket);		break;
	case S2G_CLUB_TRANSFER_RES:					Process_ClubTransferRes(recvPacket);			break;
	case S2G_FOLLOW_FRIEND_RES:					Process_FollowFriendRes(recvPacket);			break;
	case S2G_FOLLOW_FRIEND_RESPONSE_RES:		Process_FollowFriendResponseRes(recvPacket);	break;
	case S2G_INVITE_USER_RES:					Process_InviteUserRes(recvPacket);				break;
	case S2G_INVITE_USER_RESPONSE_RES:			Process_InviteUserResponseRes(recvPacket);		break;
	case S2G_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT:	Process_AnnounceCompleteAchievementNot(recvPacket);	break;
	case S2G_UPDATE_SERVER_STATE_NOT:			Process_UpdateServerState(recvPacket);			break;

	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_UPDATE_INTENSIVEPRACTICE_RANK_REQ)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2A_FACTION_RACE_STATUS_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FACTION_GIVE_REWARD_REQ);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2A_FACTION_USERCOUNT_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2A_FACTION_DISTRICT_SCORE_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FACTION_NEWSBOARD_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FACTION_USE_BUFFITEM_REQ)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_HONEYWALLET_SHOUT_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_CUSTOMIZE_MAKE_FINISH_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_HOTGIRLTIME_SHOUT_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_HOTGIRLTIME_EVENT_OPEN_SHOUT_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_MATCHINGCARD_CASH_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_MATCHINGCARD_GIVE_EVENTCASH_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_LUCKYSTAR_OPEN_AND_GIVE_REWARD_REQ)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_LUCKYSTAR_SESSION_INFO_RES)

	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_RANDOMITEM_TREE_EVENT_GET_REWARD_SHOUT_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_DELETE_WAIT_FRIEND_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EXCHANGE_GAMEID_NOTICE)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_USER_KICK_WITHWEB_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_RANDOMARROW_EVENT_S_REWARD_SHOUT_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_OPENSTATUS_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_UPDATE_LIMITED_SALEITEM_DAY_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_UPDATE_NOT)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_HALFPRICE_SHOUT_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_SHOUT_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_STATUS_FRIEND_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_REFUSAL_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_FRIEND_MISSION_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_FRIENDLIST_STATUS_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_GIVE_BENEFIT_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_KING_EVENT_UPDATE_EXP_RES)
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_ADD_FRIEND_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_REMOVE_FRIENDLIST_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_LASTCONNECT_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_MISSION_SUCCESS_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_INVITE_FRIEND_MISSION_LIST_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_MISSIONCASHLIMIT_GIVECASH_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_MISSIONCASHLIMIT_DECREASE_CASH_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_ACCOUNT_LIST_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_ACCOUNT_ADD_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_ACCOUNT_DELETE_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_PVE_SCHEDULER_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_PVE_DAILY_MISSION_SCHEDULER_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_ENTER_PCROOM_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EXPIRE_PREMIUM_PCROOM_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_PREMIUMPASS_MISSION_SCHEDULER_NOT);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES);
	CASE_CENTERPROXY_PACKET_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES);
	}
}

void CCenterSvrProxy::Process_FriendList(CReceivePacketBuffer* rp)
{
	
	SS2G_FRIEND_LIST	rs;
	
	rp->Read((BYTE*)&rs, sizeof(SS2G_FRIEND_LIST));
	
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	if ( 1 == rs.code )
	{
		SFRIEND_INFO info;
		for(int i = 0 ; i < rs.cnt ; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SFRIEND_INFO));
			user->GetFriendList()->AddNewFriend(info.GameID, info.Concernstatus, info.status, info.location, 
				info.iProcessID, info.position, info.iWithPlayCnt, info.iLevel, info.iFameLevel,
				info.iEquippedAchievementTitle );
		}
	}
	else if ( 2 == rs.code )
	{
		user->SetRecAllFriend( TRUE );
		user->SendFriendList();
		user->AddFriendCountAchievements();
	}
}

void CCenterSvrProxy::Process_MsgAtFriend(CReceivePacketBuffer* rp)
{
	SS2G_MSG_AT_FRIEND	rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_MSG_AT_FRIEND));
	
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	BYTE bisGM = false;
	
	char msg[MAX_CHATBUFF_LENGTH+1];
	if(rs.msgLen <= 0 || rs.msgLen >= MAX_CHATBUFF_LENGTH)
	{
		return;
	}
	
	rp->Read((BYTE*)&msg, rs.msgLen);
	//	msg[rs.msgLen-1] = 0;
	
	CPacketComposer PacketComposer(S2C_CHAT); 

	//if(strcmp(rs.recvID, rs.GameID) == 0)   // SendID => RecvID 로 메세지 보낸 경우... 이 경우는 GameID가 RecvID와 같다. RecvID로 메세지 전달한다.
	{
		CScopedRefGameUser user(rs.GameID);
		if( user != NULL )
		{
			if ( user->GetClient() )
			{
		
				int iType = 3;	//ӼӸ
				PacketComposer.Add(iType);
				if(rs.isGM)
				{
					bisGM = 1;
				}
				PacketComposer.Add(bisGM);
				PacketComposer.Add((PBYTE)rs.sendID, MAX_GAMEID_LENGTH+1);	
				PacketComposer.Add((int)rs.msgLen);
				PacketComposer.Add((PBYTE)msg, rs.msgLen) ;

				SG2S_MSG_TO_RESULT msgToResult;

				memset(&msgToResult, 0, sizeof (SG2S_MSG_TO_RESULT));
				strncpy_s(msgToResult.GameID, _countof(msgToResult.GameID), rs.sendID, MAX_GAMEID_LENGTH);	
				strncpy_s(msgToResult.recvID , _countof(msgToResult.recvID), rs.recvID, MAX_GAMEID_LENGTH);	
				msgToResult.nOperaion = RESULT_WHISPER_OPERATION;
				msgToResult.msgLen = rs.msgLen;
				int iFrConceernStatus = -1;
				CScopedLockFriend fr1(user->GetFriendList(), rs.sendID);
				if( fr1 != NULL )
				{
					iFrConceernStatus = fr1->GetConcernStatus();
				}

				if ( user->GetClient()->GetState() == FS_SINGLE )
				{
					msgToResult.nResult = WHISPER_USER_REFUSE_SIGNPLAY;
				}
				else if( iFrConceernStatus == FRIEND_ST_BANNED_USER )
				{
					msgToResult.nResult = WHISPER_USER_BANNED;
				}
				else if( iFrConceernStatus == FRIEND_ST_FRIEND_USER || user->CanWhisper() )
				{
					user->Send(&PacketComposer);

					msgToResult.nResult = WHISPER_SUCCESS;
				}
				else  // 보낼 수 없다고 센터 서버한테 다시 보낸다.
				{
					msgToResult.nResult = WHISPER_USER_REFUSE;
				}

				CPacketComposer PacketComposer(G2S_MSG_TO_RESULT);
				PacketComposer.Add((BYTE*)&msgToResult, sizeof(SG2S_MSG_TO_RESULT));
				PacketComposer.Add((PBYTE)msg, msgToResult.msgLen) ;
				Send(&PacketComposer);
			}
		}
	}

}

void CCenterSvrProxy::Process_MsgToFriendFail(CReceivePacketBuffer* rp)
{
	SS2G_MSG_TO_FRIEND_FAIL rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_MSG_TO_FRIEND_FAIL));

	// rs.GameID : 귓속말 보낸 넘 아이디
	// rs.recvID : 귓속말 받는 넘 아이디
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	char msg[MAX_CHATBUFF_LENGTH+1];
	if(rs.msgLen <= 0 || rs.msgLen >= MAX_CHATBUFF_LENGTH)
	{
		WRITE_LOG_NEW(LOG_TYPE_CHAT, RECV_DATA, CHECK_FAIL, "Process_MsgToFriendFail - MsgLen:11866902", rs.msgLen);
rn;
	}

	rp->Read((BYTE*)&msg, rs.msgLen);

	CPacketComposer PacketComposer(S2C_CHAT); 
	
	enum { WHISPER_RESULT_FAILED = -1 };

	int iResult = WHISPER_RESULT_FAILED;
	int iType = CHAT_TYPE_WHISPER_RETURN;
	PacketComposer.Add(iType);
	PacketComposer.Add((BYTE)0);
	PacketComposer.Add(iResult);
	PacketComposer.Add((PBYTE)rs.recvID, MAX_GAMEID_LENGTH+1);
	PacketComposer.Add((int)rs.msgLen);
	PacketComposer.Add((PBYTE)msg, rs.msgLen);

	user->Send(&PacketComposer);
}

void CCenterSvrProxy::Process_AddFriendReq(CReceivePacketBuffer* rp)
{
	SS2G_ADD_FRIEND rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_ADD_FRIEND));
	
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	if(rs.OpCode == 0)
	{
		if(user->IsPlaying())
		{
			CPacketComposer* sp = new CPacketComposer(S2C_ADD_FRIEND_RES);
			
			sp->Add((BYTE)rs.OpCode);
			sp->Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
			user->PushPacketQueue(sp);
		}
		else
		{
			CPacketComposer PacketComposer(S2C_ADD_FRIEND_RES);	
			PacketComposer.Add((BYTE)rs.OpCode);
			PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
			user->Send(&PacketComposer);
		}
	}
	else
	{
		switch(rs.res)
		{
		case FR_RES_NONE:
			{
				BEGIN_LOCK
					CScopedLockFriend fr(user->GetFriendList(), rs.friendID);
					if (fr == NULL)
					{			
						user->GetFriendList()->AddNewFriend(rs.friendID, FRIEND_ST_WAIT_USER, USER_ST_OFFLINE, U_NONE,
							0, rs.position, 0, rs.iLevel, rs.iFameLevel, rs.iEquippedAchievementTitle );
					}
					else
					{	
						fr->SetConcernStatus(FRIEND_ST_WAIT_USER);
					}
				END_LOCK;
			}
			break;
			
		case FR_RES_REJECT:	
			{			
				user->GetFriendList()->RemoveDestroyFriend(rs.friendID);				
			}
			break;
			
		case FR_RES_OK:
			{
				BEGIN_LOCK
					CScopedLockFriend fr(user->GetFriendList(), rs.friendID);
					if(fr != NULL) 
					{
						fr->SetConcernStatus(FRIEND_ST_FRIEND_USER);
						fr->SetStatus(USER_ST_SAMECHANNEL);
					}
					
				END_LOCK;

				user->SendToMatchAddFriend(rs.friendID, rs.iLevel, rs.iEquippedAchievementTitle, FRIEND_ST_FRIEND_USER);

				break;
			}
		}
		
		if(user->IsPlaying())
		{
			CPacketComposer* sp = new CPacketComposer(S2C_ADD_FRIEND_RES);	

			sp->Add((BYTE)rs.OpCode);
			sp->Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
			sp->Add((BYTE)rs.res);
			sp->Add((BYTE)rs.position);
			sp->Add((BYTE)rs.iLevel);
			sp->Add(rs.iFameLevel);
			sp->Add(rs.iEquippedAchievementTitle);
			sp->Add( (int)-1 ); //통합 삭제대기 VIP
			sp->Add( (int)-1 );	//통합 삭제대기 결혼
			sp->Add( (int)-1 ); //통합 삭제대기 POWERUP
			user->PushPacketQueue(sp);
		}
		else
		{
			CPacketComposer PacketComposer(S2C_ADD_FRIEND_RES);	
			PacketComposer.Add((BYTE)rs.OpCode);
			PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
			PacketComposer.Add((BYTE)rs.res);
			PacketComposer.Add((BYTE)rs.position);
			PacketComposer.Add((BYTE)rs.iLevel);
			PacketComposer.Add(rs.iFameLevel);
			PacketComposer.Add(rs.iEquippedAchievementTitle);
			PacketComposer.Add( (int)-1 );	//통합 삭제대기 VIP
			PacketComposer.Add( (int)-1 );	//통합 삭제대기 결혼
			PacketComposer.Add((int)-1 );	//통합 삭제대기 POWERUP
			user->Send(&PacketComposer);
		}
		
	}

	user->AddFriendCountAchievements();
}

void CCenterSvrProxy::Process_DelFriend(CReceivePacketBuffer* rp)
{
	SS2G_DEL_FRIEND	 rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_DEL_FRIEND));
	
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	user->GetFriendList()->RemoveDestroyFriend(rs.friendID);

	user->SendToMatchDelFriend(rs.friendID);
	
	if(user->IsPlaying())
	{
		CPacketComposer* sp = new CPacketComposer(S2C_DEL_FRIEND_RES);	
		
		sp->Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		user->PushPacketQueue(sp);
	}
	else
	{
		CPacketComposer PacketComposer(S2C_DEL_FRIEND_RES);	
		PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		user->Send(&PacketComposer);
	}
}

void CCenterSvrProxy::Process_AddInterestUserReq(CReceivePacketBuffer* rp)
{
	SS2G_ADD_FRIEND rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_ADD_FRIEND));
	
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	switch(rs.res)
	{
	case FR_RES_OK:
		BEGIN_LOCK
			CScopedLockFriend pFriend(user->GetFriendList(), rs.friendID);
			if (pFriend == NULL)	
			{								
				user->GetFriendList()->AddNewFriend(rs.friendID, FRIEND_ST_INTEREST_USER, USER_ST_OFFLINE, U_NONE, 0, 
					rs.position, 0, rs.iLevel, rs.iFameLevel, rs.iEquippedAchievementTitle );
			}			
			else
			{			
				pFriend->SetConcernStatus(FRIEND_ST_INTEREST_USER);
				pFriend->SetStatus(USER_ST_OFFLINE);				
			}
		END_LOCK;

		break;		
	}
	
	
	if(user->IsPlaying())
	{
		CPacketComposer* sp = new CPacketComposer(S2C_ADD_INTERESTUSER_RES);	
		if(sp == NULL)
		{
			return;
		}
		
		sp->Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		sp->Add((BYTE)rs.res);				
		sp->Add((BYTE)rs.position);
		sp->Add((BYTE)rs.iLevel);
		sp->Add(rs.iEquippedAchievementTitle);
		sp->Add( (int)-1 ); //통합 삭제대기 vip
		sp->Add( (int)-1 ); //통합 삭제대기 결혼
		sp->Add( (int)-1 ); //통합 삭제대기 powerup
		user->PushPacketQueue(sp);
	}
	else
	{
		CPacketComposer PacketComposer(S2C_ADD_INTERESTUSER_RES);			
		PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add((BYTE)rs.res);
		PacketComposer.Add((BYTE)rs.position);
		PacketComposer.Add((BYTE)rs.iLevel);
		PacketComposer.Add(rs.iEquippedAchievementTitle);
		PacketComposer.Add( (int)-1  ); //통합 삭제대기 vip
		PacketComposer.Add( (int)-1 );	//통합 삭제대기 결혼
		PacketComposer.Add(  (int)-1  ); //통합 삭제대기 powerup
		user->Send(&PacketComposer);
	}	

	user->AddFriendCountAchievements();
}

void CCenterSvrProxy::Process_AddBannedUserReq(CReceivePacketBuffer* rp)
{
	SS2G_ADD_FRIEND rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_ADD_FRIEND));
	
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	switch(rs.res)
	{
	case FR_RES_OK:
		BEGIN_LOCK
			CScopedLockFriend pFriend(user->GetFriendList(), rs.friendID);
			if (pFriend == NULL)
			{				
				user->GetFriendList()->AddNewFriend(rs.friendID, FRIEND_ST_BANNED_USER, USER_ST_OFFLINE, U_NONE, 0, 
					rs.position, 0, rs.iLevel, rs.iFameLevel, rs.iEquippedAchievementTitle );
			}			
			else
			{			
				pFriend->SetConcernStatus(FRIEND_ST_BANNED_USER);
				pFriend->SetStatus(USER_ST_OFFLINE);
			}
		END_LOCK;

		user->SendToMatchAddFriend(rs.friendID, rs.iLevel, rs.iEquippedAchievementTitle, FRIEND_ST_BANNED_USER);
		break;		
	}
	


	CPacketComposer PacketComposer(S2C_ADD_BANNEDUSER_RES);			
	PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
	PacketComposer.Add((BYTE)rs.res);
	PacketComposer.Add((BYTE)rs.position);
	PacketComposer.Add( (BYTE) rs.iLevel );
	PacketComposer.Add( (int)-1 );	//통합 삭제대기 vip
	PacketComposer.Add( (int)-1 );	//통합 삭제대기 결혼
	PacketComposer.Add( (int)-1 );	//통합 삭제대기 powerup
	user->Send(&PacketComposer);


	user->AddFriendCountAchievements();
}

void CCenterSvrProxy::Process_DelInterestUser(CReceivePacketBuffer* rp)
{
	SS2G_DEL_FRIEND	 rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_DEL_FRIEND));
	
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	user->GetFriendList()->RemoveDestroyFriend(rs.friendID);
	
	if(user->IsPlaying())
	{
		CPacketComposer* sp = new CPacketComposer(S2C_DEL_INTERESTUSER_RES);	

		sp->Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		user->PushPacketQueue(sp);
	}
	else
	{
		CPacketComposer PacketComposer(S2C_DEL_INTERESTUSER_RES);	
		PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		user->Send(&PacketComposer);
	}
}

void CCenterSvrProxy::Process_DelBannedUser(CReceivePacketBuffer* rp)
{
	SS2G_DEL_FRIEND	 rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_DEL_FRIEND));

	bool bPopUp = true;
	rp->Read((BYTE*)&bPopUp, sizeof(bool));		
	
	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	user->GetFriendList()->RemoveDestroyFriend(rs.friendID);

	user->SendToMatchDelFriend(rs.friendID);
	
	if(user->IsPlaying())
	{
		CPacketComposer* sp = new CPacketComposer(S2C_DEL_BANNEDUSER_RES);	

		sp->Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		sp->Add((BYTE*)&bPopUp, sizeof(bool));	
		user->PushPacketQueue(sp);
	}
	else
	{
		CPacketComposer PacketComposer(S2C_DEL_BANNEDUSER_RES);	
		PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add((BYTE*)&bPopUp, sizeof(bool));
		user->Send(&PacketComposer);
	}
}

// 2010.02.02
// 따라가기 기능 추가
// ktKim
void CCenterSvrProxy::Process_UserStatusUpdate(CReceivePacketBuffer* rp)
{
	SS2G_USER_STATUS_UPDATE rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_USER_STATUS_UPDATE));
	
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);
	
	BEGIN_LOCK
		CScopedLockFriend fr(user->GetFriendList(), rs.friendID);
		if(fr == NULL)	
		{
			CFriend* pFriend = user->GetFriendList()->AddNewFriend(rs.friendID, rs.ConcernStatus, rs.status, rs.location, rs.iProcessID, 				
				rs.position, rs.iWithPlayCnt, rs.iLevel, rs.iFameLevel, rs.iEquippedAchievementTitle );
		
			if(!pFriend) 
			{
				return;
			}
		}
		else
		{
			fr->SetStatus(rs.status);
			fr->SetConcernStatus(rs.ConcernStatus);
		
			if(rs.position == 0xff) rs.position = fr->GetPosition();
			else fr->SetPosition(rs.position);
			fr->SetProcessID(rs.iProcessID);
			fr->SetWithPlayCnt(rs.iWithPlayCnt);
			fr->SetLocation(rs.location);
			fr->SetFameLevel(rs.iFameLevel);
			fr->SetEquippedAchievementTitle(rs.iEquippedAchievementTitle);
			if( rs.ConcernStatus == FRIEND_ST_FRIEND_USER )
			{
				user->SendToMatchAddFriend(rs.friendID, rs.iLevel, rs.iEquippedAchievementTitle, FRIEND_ST_FRIEND_USER);
			}
			if ( rs.ConcernStatus == FRIEND_ST_DELETE_USER )
			{
				user->SendToMatchDelFriend( rs.friendID );
			}
		}	
	END_LOCK;
	
	switch(rs.ConcernStatus)
	{	
	case FRIEND_ST_REQUEST_USER:
	case FRIEND_ST_BANNED_USER:
		return;
	case FRIEND_ST_WAIT_USER:
		rs.status = USER_ST_OFFLINE;
		break;
	case FRIEND_ST_DELETE_USER:
		{
			user->GetFriendList()->RemoveDestroyFriend(rs.friendID);
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_USER_STATUS_UPDATE);	
	PacketComposer.Add((BYTE*)rs.friendID, MAX_GAMEID_LENGTH+1);	
	PacketComposer.Add((BYTE)rs.ConcernStatus);
	PacketComposer.Add((BYTE)rs.status);
	PacketComposer.Add((BYTE)rs.location);
	PacketComposer.Add((BYTE)rs.position);
	PacketComposer.Add((BYTE)rs.iLevel);
	PacketComposer.Add(rs.iFameLevel);
	PacketComposer.Add(rs.iEquippedAchievementTitle);
	PacketComposer.Add((int)-1);	//통합 삭제대기 vip
	PacketComposer.Add((int)-1 );	//통합 삭제대기 결혼
	PacketComposer.Add((int)-1 );	//통합 삭제대기 powerup
	user->Send(&PacketComposer);

	user->AddFriendCountAchievements();
}

void CCenterSvrProxy::Process_UserNotify(CReceivePacketBuffer* rp)
{
	SS2G_USER_NOTIFY	rs;
	
	rp->Read((BYTE*)&rs, sizeof(SS2G_USER_NOTIFY));
	
	CScopedRefGameUser user(rs.GameID);

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_VOID( pClubBaseODBC );

	if(user != NULL)
	{
		if (MAIL_NOTE == rs.btMailType)
		{
			user->RecvPaper(rs.index);
		}
		else if (MAIL_CLUB_INVITE == rs.btMailType)
		{
			user->RecvClubInvitation( rs.index, pClubBaseODBC );
		}
		else	// present	
		{
			user->RecvPresent(rs.btMailType, rs.index);
		}
	}
}

void CCenterSvrProxy::Process_UserSInfo(CReceivePacketBuffer* rp)
{

	SS2G_USER_SMALLINFO		rs;
	rp->Read((BYTE*)&rs, sizeof(SS2G_USER_SMALLINFO));
	
	if(rs.OpCode == USER_INFO_OPCODE_RES && rs.status == USER_ST_OFFLINE)
	{
		CPacketComposer PacketComposer(S2C_USER_INFO_RES);
		PacketComposer.Add((BYTE)0);
		PacketComposer.Add((short)rs.type);

		if(USER_INFO_EMOTION == rs.type)
		{
			PacketComposer.Add((int)0);
			PacketComposer.Add((BYTE*)rs.GameID, MAX_GAMEID_LENGTH+1);
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
		}
		else
		{
			PacketComposer.Add((BYTE*)rs.GameID, MAX_GAMEID_LENGTH+1);	
		}
		
		CScopedRefGameUser user(rs.recvID);
		if(user != NULL) 
		{
			user->Send(&PacketComposer);
		}
		return;
	}
	
	if(rs.OpCode == USER_INFO_OPCODE_REQ)	
	{		
		CFSGameServer* pServer = (CFSGameServer*)GetServer();

		CScopedRefGameUser pUser(rs.GameID);
		CHECK_NULL_POINTER_VOID(pUser);

		SG2S_USER_SMALLINFO	ss;
		ss.iUserIDIndex = rs.iUserIDIndex;
		strncpy_s(ss.GameID, _countof(ss.GameID), rs.GameID, MAX_GAMEID_LENGTH);	
		strncpy_s(ss.recvID, _countof(ss.recvID), rs.recvID, MAX_GAMEID_LENGTH);	
		ss.OpCode = USER_INFO_OPCODE_RES;	
		
		if (U_GAME == pUser->GetLocation()) ss.status = 2;	
		else ss.status = 1;
		ss.type = rs.type;		
		
		CPacketComposer PacketComposer(G2S_USER_SMALLINFO);	
		PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_USER_SMALLINFO));
		
		SAvatarInfo UserAvatarInfo;
		pUser->CurUsedAvatarSnapShot(UserAvatarInfo);			
		
		switch(rs.type)
		{
		case USER_INFO_BASE:
			{
				SUserSInfo0		info;
				info.btAvatarLvGrade = GET_AVATAR_LV_GRADE(UserAvatarInfo.iLv);
				info.bIsAllowAccessAbilityInfo = FALSE;
				if (TRUE == pServer->GetUserInfoProtVerID())
				{
					info.bIsAllowAccessAbilityInfo = pUser->GetOptionEnable(OPTION_TYPE_SEE_ABILITY);
				}

				pUser->MakeDataForUserInfoBase(info.Buffer, info.iDataSize, pServer);

				PacketComposer.Add((BYTE*)&info, sizeof(SUserSInfo0));	
			}	
			break;
		case USER_INFO_RANK:
			{
				if( rs.recordType >= MAX_RECORD_TYPE )
				{
					WRITE_LOG_NEW(LOG_TYPE_USER, RECV_DATA, CHECK_FAIL, "Process_UserSInfo - RecordType:11866902", rs.recordType);
eturn;
				}

				SUserSInfo1		info;	

				pUser->MakeDataForUserInfoRank(info.Buffer, info.iDataSize, rs.recordType, rs.season );

				PacketComposer.Add((BYTE*)&info, sizeof(SUserSInfo1));		
			}
			break;
		case USER_INFO_STAT:
			{
				SUserSInfoForStat info;
				pUser->MakeDataForUserInfoStat(info, pServer, rs.iLinkItemAddStat );

				PacketComposer.Add((BYTE*)&info, sizeof(SUserSInfoForStat));
			}
			break;
		case USER_INFO_COACHCARD:
			{
				SUserSInfo1 info;
				pUser->MakeDataForCoachCardSlotPackageList(info.Buffer, info.iDataSize );

				if (MAX_USER_INFO_RANK_DATA <= info.iDataSize)
				{
					WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeCoachCardSlotPackageListRes - DataSize overflow:11866902, GameIDIndex:0", info.iDataSize, pUser->GetGameIDIndex());
return;
				}

				PacketComposer.Add((BYTE*)&info, sizeof(SUserSInfo1));
			}
			break;
		case USER_INFO_COACHCARD_DETAIL:
			{
				SUserSInfo1 info;
				pUser->MakeDataForUserInfoCoachCard(info.Buffer, info.iDataSize, rs.iCoachCardProductIndex, rs.iCoachCardFront );

				if (MAX_USER_INFO_RANK_DATA <= info.iDataSize)
				{
					WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeCoachCardSlotPackageListRes - DataSize overflow:11866902, GameIDIndex:0", info.iDataSize, pUser->GetGameIDIndex());
return;
				}

				PacketComposer.Add((BYTE*)&info, sizeof(SUserSInfo1));
			}
			break;
		case USER_INFO_CHARACTER_COLLECTION:
			{
				SUserSInfoForCharacterCollection info;
				pUser->MakeDataForUserInfoCharacterCollection(info.Buffer, info.iDataSize);

				if (MAX_USER_INFO_RANK_DATA <= info.iDataSize)
				{
					WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeDataForUserInfoCharacterCollection - DataSize overflow:11866902, GameIDIndex:0", info.iDataSize, pUser->GetGameIDIndex());
return;
				}

				PacketComposer.Add((BYTE*)&info, sizeof(SUserSInfoForCharacterCollection));
			}
			break;
		}	
		
		Send(&PacketComposer);
	}
	else
	{
		CFSGameServer* pServer = (CFSGameServer*)GetServer();
		CScopedRefGameUser recver(rs.recvID);
		CHECK_NULL_POINTER_VOID(recver);
		
		CPacketComposer PacketComposer(S2C_USER_INFO_RES);
		PacketComposer.Add((BYTE)1);
		PacketComposer.Add((short)rs.type);

		switch(rs.type)
		{
		case USER_INFO_BASE:
			{
				SUserSInfo0		info;
				rp->Read((BYTE*)&info, sizeof(SUserSInfo0));
								
				short sIsFriend = 0;
				int iFriendIDIndex = 0;
				BYTE btFriendAccountStatus = FriendAccount::STATUS_NONE;
				BYTE btFriendAccountChannelType = rs.status;

				BEGIN_LOCK

					CScopedRefGameUser sender(rs.iGameIDIndex);
					if(sender != nullptr)
					{
						iFriendIDIndex = sender->GetUserIDIndex();
					}

				END_LOCK

				BEGIN_LOCK

					CScopedLockFriend fr(recver->GetFriendList(), rs.GameID);
					if(fr != NULL && fr->GetStatus() != USER_ST_OFFLINE)
					{
						switch(fr->GetConcernStatus())
						{
						case FRIEND_ST_FRIEND_USER: 
							sIsFriend = FRIEND_ST_FRIEND_USER;  
							break;
						}	
					}

					CScopedLockFriendAccount frAccount(recver->GetFriendAccountList(), iFriendIDIndex);
					if(frAccount != nullptr && frAccount->GetChannelType() != FriendAccount::CHANNEL_TYPE_OFFLINE)
					{
						btFriendAccountStatus = frAccount->GetStatus();
					}

				END_LOCK;
				
				PacketComposer.Add(rs.iUserIDIndex);
				PacketComposer.Add((BYTE*)rs.GameID, MAX_GAMEID_LENGTH+1);	
				PacketComposer.Add( (BYTE)0 );	//IsMyInfo?
				PacketComposer.Add(sIsFriend);
				PacketComposer.Add(btFriendAccountStatus);
				PacketComposer.Add(info.bIsAllowAccessAbilityInfo);
				PacketComposer.Add(info.btAvatarLvGrade);
				if( info.bIsAllowAccessAbilityInfo || sIsFriend == FRIEND_ST_FRIEND_USER)
				{
					PacketComposer.Add((short)rs.status);
				}
				if( info.bIsAllowAccessAbilityInfo || btFriendAccountStatus >= FriendAccount::STATUS_FRIEND)
				{
					PacketComposer.Add(btFriendAccountChannelType);
				}
								
				PacketComposer.Add(info.Buffer, info.iDataSize);

				recver->Send(&PacketComposer);
			}	
			break;
		case USER_INFO_RANK:
			{
				SUserSInfo1		info;	
				rp->Read((BYTE*)&info, sizeof(SUserSInfo1));
				if(info.iDataSize >  MAX_USER_INFO_RANK_DATA )
				{
					info.iDataSize = MAX_USER_INFO_RANK_DATA;
				}
				PacketComposer.Add(info.Buffer, info.iDataSize);

				recver->Send(&PacketComposer);
			}
			break;
		case USER_INFO_STAT:
			{
				SUserSInfoForStat		info;	
				rp->Read((BYTE*)&info, sizeof(SUserSInfoForStat));

				PacketComposer.Add((PBYTE)&info, sizeof(SUserSInfoForStat));

				recver->Send(&PacketComposer);
			}
			break;
		case USER_INFO_COACHCARD:
		case USER_INFO_COACHCARD_DETAIL:
			{
				SUserSInfo1		info;	
				rp->Read((BYTE*)&info, sizeof(SUserSInfo1));

				PacketComposer.Add(info.Buffer, info.iDataSize);

				recver->Send(&PacketComposer);
			}
			break;
		case USER_INFO_CHARACTER_COLLECTION:
			{
				SUserSInfoForCharacterCollection info;
				rp->Read((BYTE*)&info, sizeof(SUserSInfoForCharacterCollection));

				PacketComposer.Add(info.Buffer, info.iDataSize);

				recver->Send(&PacketComposer);
			}
			break;
		default:
			{
				WRITE_LOG_NEW(LOG_TYPE_USER, RECV_DATA, CHECK_FAIL, "Process_UserSInfo - Type:11866902", rs.type);
recver->Send(&PacketComposer);
			};
			break;
		}
	}
}

BOOL CCenterSvrProxy::SendMsgToAllFriend(char* gameID, BYTE* msg, int msglen)
{	
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if(!pServer)
		return false;

	CScopedRefGameUser pUser(gameID);

	BYTE bisGM = false;

	if(pUser != NULL)
	{
		if(pUser->CheckGMUser())
			bisGM = true;
	}


	SG2S_MSG_TO_ALLFRIEND	ss;
	ss.msgLen = msglen;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	ss.isGM = bisGM;
	
	CPacketComposer PacketComposer(G2S_MSG_TO_ALLFRIEND);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_MSG_TO_ALLFRIEND));
	PacketComposer.Add(msg, msglen);
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendMsgToFriend(char* gameID, BYTE* msg, int msglen, char* recver)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if(!pServer)
		return false;

	CScopedRefGameUser pUser(gameID);

	BYTE bisGM = false;

	if(pUser != NULL)
	{
		if(pUser->CheckGMUser())
			bisGM = true;
	}

	SG2S_MSG_TO_FRIEND	ss;
	ss.msgLen = msglen;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.recvID, _countof(ss.recvID), recver, MAX_GAMEID_LENGTH);
	ss.isGM = bisGM;
	
	CPacketComposer PacketComposer(G2S_MSG_TO_FRIEND);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_MSG_TO_FRIEND));
	PacketComposer.Add(msg, msglen);
	
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendUserStatusUpdate(char* gameID, int status, int iLocation)
{
	SG2S_USER_STATUS_UPDATE	ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	ss.status = status;
	ss.location = iLocation;
	
	CPacketComposer PacketComposer(G2S_USER_STATUS_UPDATE);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_USER_STATUS_UPDATE));
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendAddFriend(int iGameIDIndex, char* gameID, char* friendID, int OpCode, 
	int res, int position, int iLevel, int iFameLevel, int iEquippedAchievementTitle)
{
	SG2S_ADD_FRIEND		ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.friendID, _countof(ss.friendID), friendID, MAX_GAMEID_LENGTH);	
	ss.OpCode = OpCode;
	ss.res =  res;
	ss.position = position;
	ss.iGameIDIndex = iGameIDIndex;
	ss.iLevel = iLevel;
	ss.iFameLevel = iFameLevel;
	ss.iEquippedAchievementTitle = iEquippedAchievementTitle;
	
	CPacketComposer PacketComposer(G2S_ADD_FRIEND);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_ADD_FRIEND));
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendDelFriend(int iGameIDIndex, char* gameID, char* friendID)
{
	SG2S_DEL_FRIEND		ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.friendID, _countof(ss.friendID), friendID, MAX_GAMEID_LENGTH);	
	
	ss.iGameIDIndex = iGameIDIndex;
	
	
	CPacketComposer PacketComposer(G2S_DEL_FRIEND);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_DEL_FRIEND));
	Send(&PacketComposer);	
	return TRUE;
}

BOOL CCenterSvrProxy::SendAddInterestUserReq(int iGameIDIndex, char* pGameID, char* pInterestID)
{
	SG2S_ADD_FRIEND		ss;
	
	memset(&ss, 0, sizeof (SG2S_ADD_FRIEND));
	
	ss.iGameIDIndex = iGameIDIndex;
	strncpy_s(ss.GameID, _countof(ss.GameID), pGameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.friendID, _countof(ss.friendID), pInterestID, MAX_GAMEID_LENGTH);
	ss.iFameLevel = 0;
	
	CPacketComposer PacketComposer(G2S_ADD_INTEREST_USER);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_ADD_FRIEND));
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendAddBannedUserReq(int iGameIDIndex, char* pGameID, char* pBannedID )
{
	SG2S_ADD_FRIEND		ss;
	memset(&ss, 0, sizeof (SG2S_ADD_FRIEND));
	
	ss.iGameIDIndex = iGameIDIndex;
	strncpy_s(ss.GameID, _countof(ss.GameID), pGameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.friendID, _countof(ss.friendID), pBannedID, MAX_GAMEID_LENGTH);	
	ss.iFameLevel = 0;
	
	CPacketComposer PacketComposer(G2S_ADD_BANNED_USER);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_ADD_FRIEND));
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendDelInterestUserReq(int iGameIDIndex, char* gameID, char* friendID)
{
	SG2S_DEL_FRIEND		ss;
	memset(&ss, 0, sizeof (SG2S_DEL_FRIEND));
	
	ss.iGameIDIndex = iGameIDIndex;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.friendID, _countof(ss.friendID), friendID, MAX_GAMEID_LENGTH);	
	
	CPacketComposer PacketComposer(G2S_DEL_INTEREST_USER);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_DEL_FRIEND));
	Send(&PacketComposer);	
	return TRUE;
}

BOOL CCenterSvrProxy::SendDelBannedUserReq(int iGameIDIndex, char* gameID, char* friendID)
{
	SG2S_DEL_FRIEND		ss;
	memset(&ss, 0, sizeof (SG2S_DEL_FRIEND));
	
	ss.iGameIDIndex = iGameIDIndex;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.friendID, _countof(ss.friendID), friendID, MAX_GAMEID_LENGTH);	
	
	CPacketComposer PacketComposer(G2S_DEL_BANNED_USER);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_DEL_FRIEND));
	Send(&PacketComposer);	
	return TRUE;
}


BOOL CCenterSvrProxy::SendUserNotify(char* recver, int index, int iMailType)
{
	SG2S_USER_NOTIFY  ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), recver, MAX_GAMEID_LENGTH);	
	ss.index = index;
	ss.btMailType = iMailType;
	
	CPacketComposer PacketComposer(G2S_USER_NOTIFY);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_USER_NOTIFY)); 
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendUserSInfo(char* gameID, char* recvID, int type, int iRecordType, int iSeason, int iLinkItemAddStat[MAX_STAT_NUM], int iCoachCardProductIndex/* = 0*/, int iCoachCardFront/* = 0*/)
{	
	SG2S_USER_SMALLINFO ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.recvID, _countof(ss.recvID), recvID, MAX_GAMEID_LENGTH);	
	ss.OpCode = USER_INFO_OPCODE_REQ;
	ss.status = USER_ST_OFFLINE;
	ss.type = type;
	ss.recordType = iRecordType;
	ss.season = iSeason;
	memcpy(ss.iLinkItemAddStat, iLinkItemAddStat, sizeof(int)*MAX_STAT_NUM);
	ss.iCoachCardProductIndex = iCoachCardProductIndex;
	ss.iCoachCardFront = iCoachCardFront;

	CPacketComposer PacketComposer(G2S_USER_SMALLINFO);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_USER_SMALLINFO));
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendWhisper(char* gameID, char* recvID, char* szText)
{
	int iChatSize = strlen(szText);
	if(!gameID || !recvID || iChatSize < 1 || iChatSize > MAX_CHATBUFF_LENGTH) return FALSE; 

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if(!pServer)
		return false;

	CScopedRefGameUser pUser(gameID);

	BYTE bisGM = false;

	if(pUser != NULL)
	{
		if(pUser->CheckGMUser())
			bisGM = true;
	}
	
	SG2S_MSG_TO_FRIEND ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	strncpy_s(ss.recvID, _countof(ss.recvID), recvID, MAX_GAMEID_LENGTH);	
	ss.msgLen = iChatSize+1;
	szText[iChatSize] = 0;
	ss.isGM = bisGM;
	
	CPacketComposer Packet(G2S_MSG_TO_FRIEND); 
	Packet.Add((BYTE*)&ss, sizeof(SG2S_MSG_TO_FRIEND));
	Packet.Add((BYTE*)szText, ss.msgLen);
	Send(&Packet);
	return TRUE;
}

BOOL CCenterSvrProxy::SendShout( char* gameID, char* szText,int iShoutItemMode )
{
	int iChatSize = strlen(szText);
	if(!gameID || iChatSize < 1 || iChatSize > MAX_CHATBUFF_LENGTH) return FALSE; 
	
	SS2G_MSG_SHOUT  ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), gameID, MAX_GAMEID_LENGTH);	
	ss.msgLen = iChatSize+1;
	szText[iChatSize] = 0;
	ss.ShoutItemMode = iShoutItemMode;
	
	CPacketComposer PacketComposer(G2S_USER_SHOUT);
	PacketComposer.Add((BYTE*)&ss, sizeof(SS2G_MSG_SHOUT)); 
	PacketComposer.Add((BYTE*)szText, ss.msgLen);
	
	Send(&PacketComposer);
	return TRUE;
}

BOOL CCenterSvrProxy::SendShout( char* gameID, char* szText, size_t iSize, int iShoutItemMode )
{
	CHECK_NULL_POINTER_BOOL(gameID);
	CHECK_NULL_POINTER_BOOL(szText);

	if( iSize < 1 || iSize > MAX_CHATBUFF_LENGTH) return FALSE; 
	
	SS2G_MSG_SHOUT  ss;
	strncpy_s(ss.GameID, sizeof(ss.GameID), gameID, MAX_GAMEID_LENGTH+1);	
	ss.msgLen = iSize+1;
	szText[iSize] = 0;
	ss.ShoutItemMode = iShoutItemMode;
	
	CPacketComposer PacketComposer(G2S_USER_SHOUT);
	PacketComposer.Add((BYTE*)&ss, sizeof(SS2G_MSG_SHOUT)); 
	PacketComposer.Add((BYTE*)szText, ss.msgLen);
	
	Send(&PacketComposer);
	return TRUE;
}

void CCenterSvrProxy::Process_UserShout(CReceivePacketBuffer* rp)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if( pServer == NULL ) return;
	
	int iConfirm = -1;
	SS2G_MSG_SHOUT	rs;
	char	msg[MAX_CHATBUFF_LENGTH+1];
	
	int iShoutItemMode =0;
	
	rp->Read((BYTE*)&iShoutItemMode, sizeof(int));
	rp->Read((BYTE*)&rs.GameID, MAX_GAMEID_LENGTH+1);	
	rp->Read((BYTE*)&iConfirm, sizeof(int));
	rp->Read((BYTE*)&msg, MAX_CHATBUFF_LENGTH+1);
	
	pServer->BroadCastShoutMsg(rs.GameID, iConfirm, iShoutItemMode,msg );	
	
}


void CCenterSvrProxy::SendShoutMessageReq(char* szGameID, int iGameIDIndex, int iItemIdx, int iItemCode, char* szMessage)
{
	SG2S_SHOUT_MESSAGE	sShoutMsg;
	strncpy_s(sShoutMsg.GameID, _countof(sShoutMsg.GameID), szGameID, MAX_GAMEID_LENGTH);	
	sShoutMsg.iGameIDIndex = iGameIDIndex;
	sShoutMsg.iItemIdx = iItemIdx;
	sShoutMsg.iItemCode = iItemCode;
	strncpy_s(sShoutMsg.szMessage, _countof(sShoutMsg.szMessage), szMessage, MAX_SHOUT_MESSAGE_LEN);
	
	CPacketComposer PacketComposer(G2S_SHOUT_MESSAGE_REQ);
	PacketComposer.Add((BYTE*)&sShoutMsg, sizeof(SG2S_SHOUT_MESSAGE));
	Send(&PacketComposer);	
}

void CCenterSvrProxy::SendSystemShoutMessageReqToThisServer(int iMessageColor,int iShowTime,int iSystemTextIndex,CPacketComposer* params)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if( NULL == pServer ) return;

	CPacketComposer PacketComposer(S2C_SYSTEM_SHOUT_MESSAGE_NOT);
	PacketComposer.Add(iMessageColor);
	PacketComposer.Add(iShowTime);
	PacketComposer.Add(iSystemTextIndex);
	if(params)
	{
		params->Finalize();
		PacketComposer.Add((BYTE*)params->GetBodyBuffer(),params->GetBodySize());
	}

	pServer->BroadCast(PacketComposer);
}

void CCenterSvrProxy::Process_ShoutMessageResult(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if( NULL == pServer ) return;
	
	SS2G_SHOUT_MESSAGE_RESULT	 rs;
	
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_SHOUT_MESSAGE_RESULT));
	
	CScopedRefGameUser pUser(rs.GameID);
	CHECK_NULL_POINTER_VOID(pUser);
	
	pUser->UpdateShoutCount(rs.iItemIdx);
	
	if(rs.iResult == 0)
	{
		pServer->UpdateShoutTime();

		pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_SHOUT_ITEM);
		pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_SHOUT_ITEM);
	}

	CPacketComposer PacketComposer(S2C_SHOUT_MESSAGE_RES);
	PacketComposer.Add(rs.iResult);
	pUser->Send(&PacketComposer);
}

void CCenterSvrProxy::Process_ShoutMessage(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if( NULL == pServer ) return;
	
	SS2G_SHOUT_MESSAGE	 rs;
	
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_SHOUT_MESSAGE));
	
	int iMessageLen = strlen(rs.szMessage);
	
	CPacketComposer PacketComposer(S2C_SHOUT_MESSAGE_NOT);
	PacketComposer.Add((BYTE*)rs.GameID, MAX_GAMEID_LENGTH+1);	
	PacketComposer.Add(rs.iItemCode);
	PacketComposer.Add((BYTE)iMessageLen);
	PacketComposer.Add((BYTE*)rs.szMessage, iMessageLen+1);
	pServer->BroadCast(PacketComposer);	
}

void CCenterSvrProxy::Process_SystemShoutMessage(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if( NULL == pServer ) return;
	
	SS2G_SYSTEM_SHOUT_MESSAGE rs;
	recvPacket->Read((BYTE*)&rs,sizeof(SS2G_SYSTEM_SHOUT_MESSAGE));
	if(0 <= rs.iDataBlockLen && rs.iDataBlockLen <= MAX_SYSTEM_SHOUT_DATABLOCK_LEN)
	{
		CPacketComposer PacketComposer(S2C_SYSTEM_SHOUT_MESSAGE_NOT);
		PacketComposer.Add(rs.iMessageColor);
		PacketComposer.Add(rs.iShowTime);
		PacketComposer.Add(rs.iSystemTextIndex);
		PacketComposer.Add((BYTE*)rs.aDataBlock,rs.iDataBlockLen);
		pServer->BroadCast(PacketComposer);
	}

}

void CCenterSvrProxy::SendRequestGameIDChange(int iUserIDIndex, int iGameIDIndex , char* szGameID , char* szGameIDNew, int iFactionIndex, BOOL bLogin)
{
	SG2S_CHANGE_GAMEID_INFO	 rs;
	rs.iUserIDIndex = iUserIDIndex;
	rs.iGameIDIndex = iGameIDIndex;
	
	strncpy_s(rs.GameID, _countof(rs.GameID), szGameID, MAX_GAMEID_LENGTH);
	strncpy_s(rs.szGameIDOld, _countof(rs.szGameIDOld), szGameID, MAX_GAMEID_LENGTH);
	strncpy_s(rs.szGameIDNew, _countof(rs.szGameIDNew), szGameIDNew, MAX_GAMEID_LENGTH);
	
	rs.iFactionIndex = iFactionIndex;
	rs.bLogin = bLogin;

	CPacketComposer PacketComposer(G2S_CHANGE_GAMEID_REQ);
	PacketComposer.Add((BYTE*)&rs, sizeof(SG2S_CHANGE_GAMEID_INFO));
	Send(&PacketComposer);	
}


void CCenterSvrProxy::Process_FriendIDChangeNotify(CReceivePacketBuffer* recvPacket)
{
	SG2S_CHANGE_GAMEID_INFO	 rs;
	
	recvPacket->Read((BYTE*)&rs, sizeof(SG2S_CHANGE_GAMEID_INFO));
	
	CScopedRefGameUser user(rs.GameID);
	if(user != NULL)
	{
		if(TRUE == user->GetFriendList()->ChangeFriendGameID(rs.szGameIDOld, rs.szGameIDNew))
		{
			CScopedLockFriend pFriend(user->GetFriendList(), rs.szGameIDNew);
			if( pFriend != NULL )
			{
				int Status = pFriend->GetStatus();
				int iProcessID = pFriend->GetProcessID();
				int Position = pFriend->GetPosition();
				int iWithPlayCnt = pFriend->GetWithPlayCnt();
				int ConcernStatus = pFriend->GetConcernStatus();
				int Location = pFriend->GetLocation();
				int iLevel = pFriend->GetLevel();
				int iFameLevel = pFriend->GetFameLevel();
				int iEquippedAchievementTitle = pFriend->GetEquippedAchievementTitle();

				if(user->IsPlaying())
				{
					// 2009.02.01
					// 친구가 아이디 변경 시 이전 아이디가 거부 목록에 추가되는 버그 수정
					// ktKim
					CPacketComposer *PacketComposer = new CPacketComposer(S2C_USER_STATUS_UPDATE);	
					PacketComposer->Add((BYTE*)rs.szGameIDOld, MAX_GAMEID_LENGTH+1);	
					PacketComposer->Add((BYTE)FRIEND_ST_DELETE_USER);
					PacketComposer->Add((BYTE)Status);
					PacketComposer->Add((BYTE)Location);
					PacketComposer->Add((BYTE)Position);
					PacketComposer->Add((BYTE)iLevel);
					PacketComposer->Add(iFameLevel);
					PacketComposer->Add(iEquippedAchievementTitle);
					PacketComposer->Add( (int)-1 );//통합 삭제대기 vip
					PacketComposer->Add( (int)-1 ); //통합 삭제대기 결혼
					PacketComposer->Add( (int)-1 );//통합 삭제대기 powerup
					user->PushPacketQueue(PacketComposer);
					// End
				
					CPacketComposer *PacketComposer2 = new CPacketComposer(S2C_USER_STATUS_UPDATE);	
					PacketComposer2->Add((BYTE*)rs.szGameIDNew, MAX_GAMEID_LENGTH+1);	
					PacketComposer2->Add((BYTE)ConcernStatus);
					PacketComposer2->Add((BYTE)Status);
					PacketComposer2->Add((BYTE)Location);
					PacketComposer2->Add((BYTE)Position);
					PacketComposer2->Add((BYTE)iLevel);
					PacketComposer2->Add(iFameLevel);
					PacketComposer2->Add(iEquippedAchievementTitle);
					PacketComposer2->Add( (int)-1 );//통합 삭제대기 vip
					PacketComposer2->Add( (int)-1 ); //통합 삭제대기 결혼
					PacketComposer2->Add( (int)-1 );//통합 삭제대기 powerup
					user->PushPacketQueue(PacketComposer2);
				}
				else
				{
					// 2009.02.01
					// 친구가 아이디 변경 시 이전 아이디가 거부 목록에 추가되는 버그 수정
					// ktKim
					CPacketComposer PacketComposer(S2C_USER_STATUS_UPDATE);	
					PacketComposer.Add((BYTE*)rs.szGameIDOld, MAX_GAMEID_LENGTH+1);		
					PacketComposer.Add((BYTE)FRIEND_ST_DELETE_USER);
					PacketComposer.Add((BYTE)Status);
					PacketComposer.Add((BYTE)Location);
					PacketComposer.Add((BYTE)Position);
					PacketComposer.Add((BYTE)iLevel);
					PacketComposer.Add(iFameLevel);
					PacketComposer.Add(iEquippedAchievementTitle);
					PacketComposer.Add( (int)-1 );//통합 삭제대기 vip
					PacketComposer.Add( (int)-1 ); //통합 삭제대기 결혼
					PacketComposer.Add( (int)-1 );//통합 삭제대기 powerup
					user->Send(&PacketComposer);
					// End
				
					CPacketComposer PacketComposer2(S2C_USER_STATUS_UPDATE);	
					PacketComposer2.Add((BYTE*)rs.szGameIDNew, MAX_GAMEID_LENGTH+1);	
					PacketComposer2.Add((BYTE)ConcernStatus);
					PacketComposer2.Add((BYTE)Status);
					PacketComposer2.Add((BYTE)Location);
					PacketComposer2.Add((BYTE)Position);
					PacketComposer2.Add((BYTE)iLevel);
					PacketComposer2.Add(iFameLevel);
					PacketComposer2.Add(iEquippedAchievementTitle);
					PacketComposer2.Add( (int)-1 );//통합 삭제대기 vip
					PacketComposer2.Add( (int)-1 ); //통합 삭제대기 결혼
					PacketComposer2.Add( (int)-1 );//통합 삭제대기 powerup
					user->Send(&PacketComposer2);
				
				}
			}
		}
	}
}

void CCenterSvrProxy::Process_ChangeUserName(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2S_CHANGE_GAMEID_INFO	 rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SG2S_CHANGE_GAMEID_INFO));
	CHANNELBUFFMANAGER.ChangeUserName(rs.iGameIDIndex,rs.szGameIDOld,rs.szGameIDNew);
	FACTION.ChangeGameName(rs.iUserIDIndex, rs.szGameIDOld, rs.szGameIDNew, rs.iFactionIndex);
	INTENSIVEPRACTICEMANAGER.ChangeUserName(rs.iGameIDIndex, rs.szGameIDNew);

	CPacketComposer PacketComposer(S2C_CHANNEL_BUFF_INFO_NOTIFY);
	CHANNELBUFFMANAGER.MakeChannelBuffTimeInfo(PacketComposer);
	CFSGameServer::GetInstance()->BroadCast(PacketComposer);
}

BOOL CCenterSvrProxy::SendEventShoutMsgReq( int iGameIDIndex, char* UserID, int iItemCode )
{
	SG2S_EVENT_SHOUT_MSG		sEventShout;
	memset(&sEventShout, 0, sizeof (SG2S_EVENT_SHOUT_MSG));
	
	sEventShout.iGameIDIndex = iGameIDIndex;
	strncpy_s(sEventShout.szUserID, _countof(sEventShout.szUserID), UserID, MAX_USERID_LENGTH);
	sEventShout.iItemCode = iItemCode;
	
	CPacketComposer PacketComposer(G2S_EVENT_SHOUT_MESSAGE_REQ);
	PacketComposer.Add((BYTE*)&sEventShout, sizeof(SG2S_EVENT_SHOUT_MSG));
	Send(&PacketComposer);	
	return TRUE;
}

void CCenterSvrProxy::Process_EventShoutMsg(CReceivePacketBuffer* recvPacket)
{
	
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if( NULL == pServer ) return;
	
	SG2S_EVENT_SHOUT_MSG		sEventShout;
	memset(&sEventShout, 0, sizeof (SG2S_EVENT_SHOUT_MSG));
	
	recvPacket->Read((PBYTE)&sEventShout, sizeof(SG2S_EVENT_SHOUT_MSG)); 
	
	int iEventType = BONUS_STAT_EVENT;
	int iEventMsgType = FLOW_MSG;
	int iBonusType = EVENT_BONUS_ITEM_SHOUT_TYPE;
	int iEventParam = 4;
	
	char szTempUserID[MAX_USERID_LENGTH + 1 ];
	memset(szTempUserID, 0, MAX_USERID_LENGTH +1 );
	memcpy(szTempUserID, sEventShout.szUserID, MAX_USERID_LENGTH);
	
	CPacketComposer PacketComposer( S2C_EVENT_MSG_RES );
	PacketComposer.Add( iEventType );
	PacketComposer.Add( iEventMsgType );
	PacketComposer.Add( iBonusType );
	PacketComposer.Add((BYTE*)szTempUserID, MAX_USERID_LENGTH+1);	
	PacketComposer.Add( sEventShout.iItemCode );
	PacketComposer.Add( iEventParam );
	pServer->BroadCast(PacketComposer);
	
}

// 20090520 CHN expand GM kick out function.
void CCenterSvrProxy::SendRequestKickOut( char* szGameID )
{		
	CPacketComposer PacketComposer( G2S_MANAGER_USER_KICK_REQ );
	PacketComposer.Add( (BYTE*)szGameID, MAX_GAMEID_LENGTH+1 );
	Send( &PacketComposer );	
	return;
}

void CCenterSvrProxy::Process_GameManagerKickOut( CReceivePacketBuffer* recvPacket )
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID( pServer )
		
	DECLARE_INIT_TCHAR_ARRAY( szGameID, MAX_GAMEID_LENGTH+1 );	
	
	recvPacket->ReadNTString( szGameID, MAX_GAMEID_LENGTH + 1 );

	CScopedRefGameUser pUser(szGameID);
	CHECK_NULL_POINTER_VOID( pUser )
	
	CFSGameClient* pUserClient = (CFSGameClient*)pUser->GetClient();

	if ( pUserClient )
	{
		int iResult = -10;
		CPacketComposer packet( S2C_MANAGER_USER_KICK_RES );
		packet.Add(iResult);
		pUserClient->Send(&packet);
		pUserClient->DisconnectClient();
	}
}
// End

void CCenterSvrProxy::Process_FSAnnounce( CReceivePacketBuffer* recvPacket )
{
	int iOutputType = 0;
	DECLARE_INIT_TCHAR_ARRAY( szNoticeMessage, MAX_NOTICE_MESSAGE_SIZE+1 );
	int iMessageLength;

	recvPacket->Read(&iOutputType);
	recvPacket->ReadNTString( szNoticeMessage, MAX_NOTICE_MESSAGE_SIZE + 1 );
	
	szNoticeMessage[MAX_NOTICE_MESSAGE_SIZE] = '\0';
	iMessageLength = strlen(szNoticeMessage);

	CFSGameServer::GetInstance()->BroadCastNotifyMsg(iOutputType , szNoticeMessage, iMessageLength);

}

void CCenterSvrProxy::Process_WhisperResult( CReceivePacketBuffer* recvPacket )
{
	CHECK_NULL_POINTER_VOID(recvPacket);

	SG2S_WHISPER_RESULT	 rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SG2S_WHISPER_RESULT));

	BYTE msg[512];//[MAX_CHAT_LENGTH+1];
	recvPacket->Read((BYTE*)&msg, rs.msgLen);

	CScopedRefGameUser pSender(rs.sendID);
	CHECK_NULL_POINTER_VOID(pSender);

	BYTE bisGM = false;

	if(pSender->CheckGMUser())
		bisGM = true;

	int iType = CHAT_TYPE_WHISPER_RETURN;

	int iResult = rs.nResult;

	CPacketComposer PacketComposer(S2C_CHAT); 
	PacketComposer.Add(iType);
	PacketComposer.Add(bisGM);
	PacketComposer.Add(iResult);
	PacketComposer.Add((PBYTE)rs.recvID, MAX_GAMEID_LENGTH+1);
	PacketComposer.Add((int)rs.msgLen);
	PacketComposer.Add((PBYTE)msg, rs.msgLen);

	pSender->Send(&PacketComposer);
}

void CCenterSvrProxy::Process_UseChannelBuffItem(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID( pServer )

	SS2G_USE_CHANNEL_BUFF_ITEM_NOTIFY rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_USE_CHANNEL_BUFF_ITEM_NOTIFY));

	if(rs.iNum != -1 && rs.iProcessID != pServer->GetProcessID())
	{
		CHANNELBUFFMANAGER.AddChannelBuffTime(rs.iProcessID, rs.iNum, rs.iGameIDIndex, rs.szGameID);

		CPacketComposer PacketComposer(S2C_CHANNEL_BUFF_INFO_NOTIFY);
		CHANNELBUFFMANAGER.MakeChannelBuffTimeInfo(PacketComposer);
		CFSGameServer::GetInstance()->BroadCast(PacketComposer);
	}
	// pChannelBuffManager->UpdateChannelBuffRanking(rs.iGameIDIndex, rs.szGameID, rs.iTotalUseCount);

	CPacketComposer PacketComposer( S2C_USE_CHANNEL_BUFF_ITEM_NOTIFY );
	PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);
	PacketComposer.Add( rs.iReqNumer );
	PacketComposer.Add(rs.stChannelBuffTime);
	PacketComposer.Add(rs.iPoint);
	PacketComposer.Add(rs.iExp);
	CFSGameServer::GetInstance()->BroadCast(PacketComposer);
}

void CCenterSvrProxy::Process_LimitedEditionItemCount(CReceivePacketBuffer* recvPacket)
{
	CLimitedEditionItemManager* pLimitedEditionItemManager = CFSGameServer::GetInstance()->GetLimitedEditionItemManager();
	CHECK_NULL_POINTER_VOID(pLimitedEditionItemManager);

	SS2G_LIMITED_EDITION_ITEM_COUNT rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_LIMITED_EDITION_ITEM_COUNT));

	pLimitedEditionItemManager->UpdateLimitedEditionItem(rs.iItemCode, rs.iCurrentCount);

	CPacketComposer PacketComposer(S2C_LIMITED_EDITION_ITEM_COUNT_INFO);
	PacketComposer.Add(rs.iItemCode);
	PacketComposer.Add(rs.iLimitedEditionCount);
	CFSGameServer::GetInstance()->BroadCast(PacketComposer);
}

void CCenterSvrProxy::SendClubJoinReq(int iClubSN, BYTE btKind, char* szGameID)
{
	SG2S_CLUB_JOIN_REQ ss;
	ss.iClubSN = iClubSN;
	ss.btKind = btKind;
	strncpy_s(ss.szCharName, _countof(ss.szCharName), szGameID, MAX_GAMEID_LENGTH);	

	CPacketComposer PacketComposer(G2S_CLUB_JOIN_REQ);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_CLUB_JOIN_REQ));
	Send(&PacketComposer);	
}

void CCenterSvrProxy::Process_ClubJoinRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_CLUB_JOIN_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_CLUB_JOIN_RES));

	CScopedRefGameUser pUser(rs.szCharName);
	if(pUser != NULL)
	{
		CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
		if(pClubProxy != NULL)
		{ 
			pUser->SetClubSN( rs.iClubSN );
			pUser->AddLobbyClubUser();

			SG2CL_CLUB_INFO_REQ info;
			info.iClubSN = rs.iClubSN;
			strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
			info.btKind	= CLUB_INFO_REQUEST_KIND_LOGIN;
			info.iCheerLeaderIndex = pUser->GetCheerLeaderCode();
			pClubProxy->SendClubInfoReq(info);

			if( rs.btKind == CLUB_JOIN_KIND_APPLICATION_FORM || rs.btKind == CLUB_JOIN_KIND_INVITE )
			{
				SAvatarInfo* pInfo = pUser->GetCurUsedAvatar();
				BYTE btPosition = 0, btLv = 0, btSex = 0;
				if(pInfo)
				{
					btPosition	= pInfo->Status.iGamePosition;
					btLv		= pInfo->iLv;
					btSex		= pInfo->iSex;
				}

				SG2CL_CLUB_MEMBER_LOGIN	loginInfo;
				loginInfo.iClubSN = rs.iClubSN;
				strncpy_s(loginInfo.szGameID, _countof(loginInfo.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
				loginInfo.btPosition = btPosition;
				loginInfo.btLv = btLv;
				loginInfo.btSex = btSex;
				pClubProxy->SendClubMemberLogin(loginInfo);

				pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_JOIN_CLUB);
			}
		}
	}
}

void CCenterSvrProxy::SendClubWithDrawReq(int iClubSN, char* szGameID)
{
	SG2S_CLUB_WITHDRAW_REQ ss;
	ss.iClubSN = iClubSN;
	strncpy_s(ss.GameID, _countof(ss.GameID), szGameID, MAX_GAMEID_LENGTH);	

	CPacketComposer PacketComposer(G2S_CLUB_WITHDRAW_REQ);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_CLUB_WITHDRAW_REQ));
	Send(&PacketComposer);	
}

void CCenterSvrProxy::Process_ClubWithDrawRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_CLUB_WITHDRAW_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_CLUB_WITHDRAW_RES));

	CScopedRefGameUser pUser(rs.GameID);
	if(pUser != NULL)
	{
		pUser->InitClub();
		pUser->RemoveLobbyClubUser();

		CPacketComposer	PacketComposer(S2C_CLUB_WITHDRAW_RES);
		PacketComposer.Add((BYTE)CLUB_WITHDRAW_RESULT_SUCCESS);
		pUser->Send(&PacketComposer);
	}
}

void CCenterSvrProxy::SendClubUpdateAdminMemberReq(char* szGameID, BYTE btClubPosition)
{
	SG2S_CLUB_UPDATE_ADMIN_MEMBER_REQ ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), szGameID, MAX_GAMEID_LENGTH);	
	ss.btClubPosition	= btClubPosition;

	CPacketComposer PacketComposer(G2S_CLUB_UPDATE_ADMIN_MEMBER_REQ);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_CLUB_UPDATE_ADMIN_MEMBER_REQ));
	Send(&PacketComposer);	
}

void CCenterSvrProxy::Process_ClubUpdateAdminMemberRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_CLUB_UPDATE_ADMIN_MEMBER_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_CLUB_UPDATE_ADMIN_MEMBER_RES));

	CScopedRefGameUser pUser(rs.GameID);
	if(pUser != NULL)
	{
		pUser->SetClubPosition(rs.btClubPosition);
	}
}

void CCenterSvrProxy::SendClubUpdateKeyPlayerReq(char* szGameID, BYTE btUniformNum)
{
	SG2S_CLUB_UPDATE_KEY_PLAYER_REQ ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), szGameID, MAX_GAMEID_LENGTH);	
	ss.btUniformNum	= btUniformNum;

	CPacketComposer PacketComposer(G2S_CLUB_UPDATE_KEY_PLAYER_REQ);
	PacketComposer.Add((BYTE*)&ss, sizeof(SG2S_CLUB_UPDATE_KEY_PLAYER_REQ));
	Send(&PacketComposer);	
}

void CCenterSvrProxy::Process_ClubUpdateKeyPlayerRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_CLUB_UPDATE_KEY_PLAYER_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_CLUB_UPDATE_KEY_PLAYER_RES));

	CScopedRefGameUser pUser(rs.GameID);
	if(pUser != NULL)
	{
		pUser->SetUnifromNum(rs.btUniformNum);
	}
}

void CCenterSvrProxy::SendClubTransferReq(char* szGameID)
{
	SS2S_DEFAULT ss;
	strncpy_s(ss.GameID, _countof(ss.GameID), szGameID, MAX_GAMEID_LENGTH);	

	CPacketComposer PacketComposer(G2S_CLUB_TRANSFER_REQ);
	PacketComposer.Add((BYTE*)&ss, sizeof(SS2S_DEFAULT));
	Send(&PacketComposer);	
}

void CCenterSvrProxy::Process_ClubTransferRes(CReceivePacketBuffer* recvPacket)
{
	SS2S_DEFAULT rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2S_DEFAULT));

	CScopedRefGameUser pUser(rs.GameID);
	if(pUser != NULL)
	{
		pUser->SetClubPosition(CLUB_POSITION_MASTER);

		CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
		if(pClubProxy != NULL)
		{ 
			SG2CL_CLUB_INFO_REQ info;
			info.iClubSN = pUser->GetClubSN();
			strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
			info.btKind	= CLUB_INFO_REQUEST_KIND_LOGIN;
			info.iCheerLeaderIndex = pUser->GetCheerLeaderCode();
			pClubProxy->SendClubInfoReq(info);
		}
	}
}

void CCenterSvrProxy::Process_FollowFriendRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_FOLLOW_FRIEND_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_FOLLOW_FRIEND_RES));

	CScopedRefGameUser pFriend(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pFriend);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pFriend->GetMatchLocation());
	if(pMatch) 
	{
		SG2M_FOLLOW_FRIEND_REQ info;
		info.iGameIDIndex = rs.iReqGameIDIndex;
		info.iAvatarLvGrade = rs.iAvatarLvGrade;
		info.iLobbySvrID = rs.iLobbySvrID;
		info.iFriendGameIDIndex = rs.iGameIDIndex;
		info.iClubIndex = rs.iClubIndex;
		info.iClubContributionPoint = rs.iClubContributionPoint;
		pMatch->SendPacket(G2M_FOLLOW_FRIEND_REQ, &info, sizeof(SG2M_FOLLOW_FRIEND_REQ));
	}
	else
	{
		SG2S_FOLLOW_FRIEND_RESPONSE_REQ ss;
		ss.iGameIDIndex = rs.iReqGameIDIndex;
		ss.btResult = RESULT_FOLLOW_FRIEND_NOT_IN_ROOM;
		ss.iLobbySvrID = rs.iLobbySvrID;
		SendPacket(G2S_FOLLOW_FRIEND_RESPONSE_REQ, &ss, sizeof(SG2S_FOLLOW_FRIEND_RESPONSE_REQ));
	}
}

void CCenterSvrProxy::Process_FollowFriendResponseRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_FOLLOW_FRIEND_RESPONSE_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_FOLLOW_FRIEND_RESPONSE_RES));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_FOLLOW_FRIEND_RES info;
	info.btResult = rs.btResult;
	pUser->Send(S2C_FOLLOW_FRIEND_RES, &info, sizeof(SS2C_FOLLOW_FRIEND_RES));
}

void CCenterSvrProxy::Process_InviteUserRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_INVITE_USER_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_INVITE_USER_RES));

	CScopedRefGameUser pUser(rs.inviteInfo.szName);
	CHECK_NULL_POINTER_VOID(pUser);

	int iResult = RESULT_INVITE_USER_FAILED;

	int iFriendConcernStatus = FRIEND_ST_DELETE_USER;
	BEGIN_LOCK
		CScopedLockFriend fr(pUser->GetFriendList(), rs.inviteInfo.szReqGameName);
		if (fr != NULL)
		{
			iFriendConcernStatus = fr->GetConcernStatus();
		}
	END_LOCK;

	if (FRIEND_ST_BANNED_USER == iFriendConcernStatus)
	{
		iResult = RESULT_INVITE_USER_REJECTED;
	}
	else if (MATCH_TYPE_RATING == rs.inviteInfo.btMatchType &&
		AVATAR_LV_GRADE_ALL != rs.inviteInfo.btAvatarLvGrade &&
		rs.inviteInfo.btAvatarLvGrade != pUser->GetCurUsedAvtarLvGrade())
	{
		iResult = RESULT_INVITE_USER_NOT_EQUAL_LVGRADE;
	}
	else if(MATCH_TYPE_CLUB_LEAGUE == rs.inviteInfo.btMatchType &&
		rs.inviteInfo.iClubIndex != pUser->GetClubSN())
	{
		iResult = RESULT_INVITE_USER_NOT_SAME_CLUB;
	}
	else if(MATCH_TYPE_CLUB_LEAGUE == rs.inviteInfo.btMatchType &&
		rs.inviteInfo.iClubIndex == pUser->GetClubSN() &&
		pUser->GetClubContributionPoint() < CLUBCONFIGMANAGER.GetNeedContributionPoint())
	{
		iResult = RESULT_INVITE_USER_ENOUGH_CLUB_CONTRIBUTIONPOINT;
	}
	else if(TRUE == pUser->CanInvite())
	{
		iResult = RESULT_INVITE_USER_SUCCESS;

		CPacketComposer Packet(S2C_INVITE_USER);
		Packet.Add(rs.inviteInfo.btMatchType);

		if(MATCH_TYPE_PRACTICE == rs.inviteInfo.btMatchType)
		{
			Packet.Add((PBYTE)rs.GameID, sizeof(char)*(MAX_GAMEID_LENGTH+1));
			Packet.Add(rs.inviteInfo.iTeamNum);
		}
		else
		{
			Packet.Add((PBYTE)rs.inviteInfo.szTeamName, sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
			Packet.Add(rs.inviteInfo.btMatchTeamScale);
			Packet.Add(rs.inviteInfo.iTeamNum);
			Packet.Add(rs.inviteInfo.shTotalNum);

			for(int i=0; i<rs.inviteInfo.shTotalNum && i<MAX_TEAM_MEMBER_NUM; i++)
			{
				Packet.Add(rs.inviteInfo.shaPos[i]);
				Packet.Add(rs.inviteInfo.shaLv[i]);
			}
			Packet.Add(rs.inviteInfo.iRoomNum);
		}
		pUser->Send(&Packet);
	}

	SG2S_INVITE_USER_RESPONSE_REQ ss;
	ss.iGameIDIndex = rs.iGameIDIndex;
	strncpy_s(ss.GameID, _countof(ss.GameID), rs.GameID, MAX_GAMEID_LENGTH);
	ss.iResult = iResult;
	SendPacket(G2S_INVITE_USER_RESPONSE_REQ, &ss, sizeof(SG2S_INVITE_USER_RESPONSE_REQ));
}

void CCenterSvrProxy::Process_InviteUserResponseRes(CReceivePacketBuffer* recvPacket)
{
	SS2G_INVITE_USER_RESPONSE_RES rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_INVITE_USER_RESPONSE_RES));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_INVITE_USER_RES info;
	info.btResult = rs.iResult;
	pUser->Send(S2C_INVITE_USER_RES, &info, sizeof(SS2C_INVITE_USER_RES)); 
}

void CCenterSvrProxy::Process_AnnounceCompleteAchievementNot(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2S_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SG2S_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT));

	CPacketComposer PacketComposer(S2C_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT);
	PacketComposer.Add((PBYTE)rs.GameID, MAX_GAMEID_LENGTH + 1);
	PacketComposer.Add(rs.iAchievementCount);

	ANNOUNCE_COMPLETE_ACHIEVEMENT_INDEX index;
	for (int iLoopIndex = 0; iLoopIndex < rs.iAchievementCount; iLoopIndex++)
	{
		recvPacket->Read((BYTE*)&index, sizeof(ANNOUNCE_COMPLETE_ACHIEVEMENT_INDEX));
		PacketComposer.Add( (BYTE*)&index,  sizeof(ANNOUNCE_COMPLETE_ACHIEVEMENT_INDEX));
	}

	pServer->BroadCast(PacketComposer);
}

void CCenterSvrProxy::Process_UpdateServerState(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSSvrList* pSvrList = pServer->GetServerList();
	CHECK_NULL_POINTER_VOID(pSvrList);

	SS2G_UPDATE_SERVER_STATE_NOT rs;
	recvPacket->Read((BYTE*)&rs, sizeof(SS2G_UPDATE_SERVER_STATE_NOT));

	pSvrList->UpdateServerState(rs);
}

// todo pcroom old code
void CCenterSvrProxy::Process_ExpirePremiumPCRoomNot(CReceivePacketBuffer* recvPacket)
{
	CHECK_CONDITION_RETURN_VOID(TRUE == CFSGameServer::GetInstance()->GetNewPCRoom());

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	SS2S_DEFAULT sDefault;
	recvPacket->Read((PBYTE)&sDefault, sizeof(SS2S_DEFAULT));

	CScopedRefGameUser pUser(sDefault.GameID);
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SetExpirePremiumPCRoom(TRUE);
	CPacketComposer PacketComposer(S2C_EXPIRE_PREMIUM_PCROOM_NOT);
	pUser->CheckAndSend(&PacketComposer, pUser->IsPlaying());
}
//////////////////////////////////

DEFINE_CENTERPROXY_PROC_FUNC(S2G_UPDATE_INTENSIVEPRACTICE_RANK_REQ)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2S_UPDATE_INTENSIVEPRACTICE_RANK_REQ info;
	rp->Read((PBYTE)&info, sizeof(SG2S_UPDATE_INTENSIVEPRACTICE_RANK_REQ)); 

	INTENSIVEPRACTICEMANAGER.UpdateRank(info.iPracticeType, info.iGameIDIndex, info.GameID, info.iGamePosition, info.iBestPoint, info.tUpdateDate);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2S_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ info;
	rp->Read((PBYTE)&info, sizeof(SG2S_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ)); 

	CScopedRefGameUser pUser(info.GameID);
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserIntensivePractice()->UpdateRank(info.iPracticeType, info.iUpdateGameIDIndex, info.szUpdateGameID, info.iGamePosition, info.iBestPoint, info.tUpdateDate);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ info;
	rp->Read((PBYTE)&info, sizeof(SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ)); 

	if(info.btUpdateType == MATCHINGPOOLCARE_UPDATE_TARGETUSER_TYPE_ADD)
		MATCHINGPOOLCAREMANAGER.AddTargetUser(info.iGameIDIndex, info.btLvGrade);
	else if(info.btUpdateType == MATCHINGPOOLCARE_UPDATE_TARGETUSER_TYPE_REMOVE)
		MATCHINGPOOLCAREMANAGER.RemoveTargetUser(info.iGameIDIndex, info.btLvGrade);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2A_FACTION_RACE_STATUS_NOT)
{
	SS2A_FACTION_RACE_STATUS_NOT info;
	rp->Read((PBYTE)&info, sizeof(SS2A_FACTION_RACE_STATUS_NOT)); 

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iOldSeasonIndex = FACTION.GetCurrentSeasonIndex();
	FACTION_RACE_STATUS eOldRaceStatus = FACTION.GetRaceStatus();
	FACTION.UpdateFactionRaceStatus(SERVER_TYPE_NORMAL, info);

	if(((eOldRaceStatus == FACTION_RACE_ON_GOING || eOldRaceStatus == FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME) && info.eRaceStatus == FACTION_RACE_CLOSED) ||
		(eOldRaceStatus == FACTION_RACE_CLOSED && (info.eRaceStatus == FACTION_RACE_ON_GOING || info.eRaceStatus == FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME)))
	{
		pServer->BroadCast_FactionStatus(info.eRaceStatus);
	}
	
	if(eOldRaceStatus != FACTION_RACE_ON_BATTLE_END &&
		info.eRaceStatus == FACTION_RACE_ON_BATTLE_END)
	{
		LOBBYCHAT.RemoveFactionUser();
	}

	if(iOldSeasonIndex > 0 &&
		(iOldSeasonIndex != 
		FACTION.GetCurrentSeasonIndex()))
	{
		pServer->ClearUserFactionInfo();
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FACTION_GIVE_REWARD_REQ)
{
	SS2G_FACTION_GIVE_REWARD_REQ req;
	rp->Read((PBYTE)&req, sizeof(SS2G_FACTION_GIVE_REWARD_REQ)); 

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	pServer->GiveFactionRaceRankReward(req, rp);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2A_FACTION_USERCOUNT_NOT)
{
	SS2A_FACTION_USERCOUNT_NOT info;
	rp->Read((PBYTE)&info, sizeof(SS2A_FACTION_USERCOUNT_NOT)); 

	FACTION.UpdateFactionUserCount(info, rp);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2A_FACTION_DISTRICT_SCORE_NOT)
{
	SS2A_FACTION_DISTRICT_SCORE_NOT info;
	rp->Read((PBYTE)&info, sizeof(SS2A_FACTION_DISTRICT_SCORE_NOT)); 

	FACTION.UpdateDistrictScore(info, rp);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FACTION_USE_BUFFITEM_REQ)
{
	SG2S_FACTION_USE_BUFFITEM_REQ info;
	rp->Read((PBYTE)&info, sizeof(SG2S_FACTION_USE_BUFFITEM_REQ)); 

	CScopedRefGameUser pUser(info.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pUser);

	if(FALSE == BILLING_GAME.RequestPurchaseItem(info.BillingInfo))
	{
		SS2C_FACTION_USE_BUFFITEM_RES res;
		res.btResult = RESULT_FACTION_USE_BUFFITEM_FAIL;
		pUser->Send(S2C_FACTION_USE_BUFFITEM_RES, &res, sizeof(SS2C_FACTION_USE_BUFFITEM_RES));
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ)
{
	SG2S_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ info;
	rp->Read((PBYTE)&info, sizeof(SG2S_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ)); 

	CScopedRefGameUser pUser(info.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->AllDistrictUseBuffItem(info.tBuffEndTime);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FACTION_NEWSBOARD_NOT)
{
	SS2G_FACTION_NEWSBOARD_NOT info;
	rp->Read((PBYTE)&info, sizeof(SS2G_FACTION_NEWSBOARD_NOT)); 

	SS2C_FACTION_NEWSBOARD_NOT not;
	not.iLength = info.iNewsLength;

	CPacketComposer Packet(S2C_FACTION_NEWSBOARD_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_FACTION_NEWSBOARD_NOT));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	CFSGameServer::GetInstance()->BroadCast(Packet, TRUE);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_HONEYWALLET_SHOUT_RES)
{
	SS2S_DEFAULT	info;
	rp->Read((PBYTE)&info, sizeof(SS2S_DEFAULT));

	SS2C_HONEYWALLET_SHOUT_NOT	not;
	strncpy_s(not.szGameID, _countof(not.szGameID), info.GameID, _countof(not.szGameID)-1);

	CPacketComposer Packet(S2C_HONEYWALLET_SHOUT_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_HONEYWALLET_SHOUT_NOT));
	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_CUSTOMIZE_MAKE_FINISH_RES)
{
	int iItemCode = 0;
	BYTE btColorType = 0;

	rp->Read(&iItemCode);
	rp->Read(&btColorType);

	iItemCode = iItemCode / 10 * 10 + 1;
	CUSTOMIZEManager.UpdateCustomizeItemColorUsed(iItemCode, btColorType);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_HOTGIRLTIME_SHOUT_NOT)
{
	SA2S_HOTGIRLTIME_SHOUT_NOT	info;
	rp->Read((PBYTE)&info, sizeof(SA2S_HOTGIRLTIME_SHOUT_NOT));

	SRewardConfig* pReward = REWARDMANAGER.GetReward(info.iRewardIndex);
	if( NULL != pReward && NULL != pReward->szRewardMessage )
	{
		char szAnnounce[256] = {NULL,};
		sprintf_s(szAnnounce, _countof(szAnnounce)-1, pReward->szRewardMessage, info.GameID);
		szAnnounce[256] = '\0';
		int iMessageLength = strlen(szAnnounce);

		CPacketComposer Packet(S2C_ANNOUNCE);
		Packet.Add(static_cast<int>(ANNOUNCE_OUTPUT_IN_TOPANDCHATTAB));
		Packet.Add(iMessageLength);
		Packet.Add((PBYTE)szAnnounce, iMessageLength);

		CFSGameServer::GetInstance()->BroadCast(Packet);
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_HOTGIRLTIME_EVENT_OPEN_SHOUT_NOT)
{
	CPacketComposer Packet(S2C_HOTGIRLTIME_EVENT_OPEN_SHOUT_NOT);
	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_MATCHINGCARD_CASH_NOT)
{
	SS2G_MATCHINGCARD_CASH_NOT info;
	rp->Read((PBYTE)&info, sizeof(SS2G_MATCHINGCARD_CASH_NOT));

	char  szGameID[MAX_GAMEID_LENGTH+1] = {NULL};

	MATCHINGCARD.SetCumulativeCash(info.iCumulativeCash);
	
	CPacketComposer Packet( S2C_EVENT_MATCHINGCARD_CASHPRIZE_NOT );

	SS2C_EVENT_MATCHINGCARD_CASHPRIZE_NOT	not;

	not.btType = info.btType;
	not.iCashPrize = info.iCumulativeCash;

	Packet.Add((PBYTE)&not, sizeof(SS2C_EVENT_MATCHINGCARD_CASHPRIZE_NOT));

	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_MATCHINGCARD_GIVE_EVENTCASH_RES) // 당첨되었다
{
	SS2G_MATCHINGCARD_GIVE_EVENTCASH_RES info;
	rp->Read((PBYTE)&info, sizeof(SS2G_MATCHINGCARD_GIVE_EVENTCASH_RES));

	CScopedRefGameUser user(info.iGameIDIndex);
	if( user != NULL )
	{
		CFSODBCBase* pODBCBase	= dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		if( NULL != pODBCBase )
		{
			if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_MATCHINGCARD_GiveRewardEventCash(user->GetUserIDIndex(), user->GetGameIDIndex(), info.iGiveCashValue))
			{
				WRITE_LOG_NEW( LOG_TYPE_MATCHINGCARD, CALL_SP, FAIL
					, "EVENT_MATCHINGCARD_GiveRewardEventCash Fail UserIDIndex:11866902,  GameIDIndex:0,  GiveCash:18227200 "
->GetUserIDIndex(), user->GetGameIDIndex(), info.iGiveCashValue );
			}
			else
			{
				user->SetEventCoin(user->GetEventCoin() + info.iGiveCashValue, TRUE);
				user->SendUserGold();
			}
		}
	}

	MATCHINGCARD.SetCumulativeCash(0); // 당첨되었으니 0으로 밀어야징 

	CPacketComposer Packet( S2C_EVENT_MATCHINGCARD_CASHPRIZE_NOT ); 

	SS2C_EVENT_MATCHINGCARD_CASHPRIZE_NOT	not;

	not.btType = (BYTE)EVENT_MATCHINGCARD_CASHPRIZE_TYPE_WIN;
	not.iCashPrize = info.iGiveCashValue;

	Packet.Add((PBYTE)&not, sizeof(SS2C_EVENT_MATCHINGCARD_CASHPRIZE_NOT));
	Packet.Add((PBYTE)info.GameID, MAX_GAMEID_LENGTH+1);
	
	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_LUCKYSTAR_OPEN_AND_GIVE_REWARD_REQ)
{
	SS2G_LUCKYSTAR_OPEN_AND_GIVE_REWARD_REQ	info;
	rp->Read((PBYTE)&info, sizeof(SS2G_LUCKYSTAR_OPEN_AND_GIVE_REWARD_REQ));

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	G2SS_LUCKYSTAR_OPEN_AND_GIVE_REWARD_RES res;
	res.iResult = LUCKYSTAR_RETURN_VALUE_SUCCESS;
	res.iSessionIdx = info.iSessionIdx;
	CPacketComposer PacketComposer( G2S_LUCKYSTAR_OPEN_AND_GIVE_REWARD_RES );

	vector<char*> vGameID;
	if ( FALSE == pServer->GetRandomGameUser( MAX_LUCKYSTAR_OWNER_COUNT , vGameID ) )
	{
		res.iResult = LUCKYSTAR_RETURN_VALUE_NOT_ENOUGH_USER;
		PacketComposer.Add((PBYTE)&res, sizeof(G2SS_LUCKYSTAR_OPEN_AND_GIVE_REWARD_RES));
		Send(&PacketComposer);
		return;
	}

	SRewardConfig* pReward = NULL;
	for ( int i = 0; i < MAX_LUCKYSTAR_REWARD_COUNT; ++i )
	{
		CScopedRefGameUser user( vGameID[i] );
		if ( user != NULL )
		{
			user->LuckyStar_GiveReward( info.iRewardIndex[i] );
		}
		strncpy_s(res.szGameID[i], _countof(res.szGameID[i]), vGameID[i], _countof(res.szGameID[i])-1);
	}
	PacketComposer.Add((PBYTE)&res, sizeof(G2SS_LUCKYSTAR_OPEN_AND_GIVE_REWARD_RES));
	Send( &PacketComposer );
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_LUCKYSTAR_SESSION_INFO_RES)
{
	SS2G_LUCKYSTAR_SESSION_INFO_RES	info;
	rp->Read((PBYTE)&info, sizeof(SS2G_LUCKYSTAR_SESSION_INFO_RES));

	SS2C_LUCKYSTAR_SESSION_INFO_RES resInfo;
	resInfo.iMode = info.iMode;
	memcpy(&resInfo.sOwnerData, &info.sOwnerData, sizeof(SLuckyStarSessionOwnerData));
	EVENTDATEMANAGER.GetEventDate(EVENT_KIND_LUCKYSTART, resInfo.tStartDate, resInfo.tEndDate);

	for ( int i = 0 ; i < MAX_LUCKYSTAR_REWARD_COUNT; ++i )
	{
		if ( resInfo.sOwnerData.iRewardIndex[i] > -1 )
		{
			SRewardConfigItem* pRewardItem = (SRewardConfigItem*)REWARDMANAGER.GetReward(resInfo.sOwnerData.iRewardIndex[i]);
			if ( pRewardItem )
			{
				resInfo.sOwnerData.iRewardIndex[i] = pRewardItem->iItemCode;
			}
		}

	}

	CScopedRefGameUser user( info.iGameIDIndex );
	if ( user != NULL )
	{
		CPacketComposer packet( S2C_LUCKYSTAR_SESSION_INFO_RES );
		packet.Add( (PBYTE)&resInfo, sizeof( SS2C_LUCKYSTAR_SESSION_INFO_RES ) );
		user->Send( &packet );
	}

}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_RANDOMITEM_TREE_EVENT_GET_REWARD_SHOUT_NOT)
{
	SG2S_RANDOMITEMTREE_EVENT_GET_REWARD_SHOUT_NOT	info;
	rp->Read((PBYTE)&info, sizeof(SG2S_RANDOMITEMTREE_EVENT_GET_REWARD_SHOUT_NOT));

	SRewardConfig* pReward = REWARDMANAGER.GetReward(info.iRewardIndex);
	if(NULL != pReward && NULL != pReward->szRewardMessage)
	{
		char szAnnounce[256] = {NULL,};
		sprintf_s(szAnnounce, _countof(szAnnounce)-1, pReward->szRewardMessage, info.GameID);
		szAnnounce[256] = '\0';
		int iMessageLength = strlen(szAnnounce);

		CPacketComposer Packet(S2C_ANNOUNCE);
		Packet.Add(static_cast<int>(ANNOUNCE_OUTPUT_IN_CHAT_TAB));
		Packet.Add(iMessageLength);
		Packet.Add((PBYTE)szAnnounce, iMessageLength);

		CFSGameServer::GetInstance()->BroadCast(Packet);
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_DELETE_WAIT_FRIEND_RES)
{
	SS2G_DELETE_WAIT_FRIEND_RES	info;
	rp->Read((PBYTE)&info, sizeof(SS2G_DELETE_WAIT_FRIEND_RES));

	CScopedRefGameUser pUser(info.iGameIDIndex);
	if(pUser != NULL)
	{
		if(info.btResult == RESULT_DELETE_WAIT_FRIEND_SUCCESS)
		{
			pUser->GetFriendList()->RemoveDestroyFriend(info.szFriendID);	
		}

		pUser->SendToMatchDelFriend(info.szFriendID);

		SS2C_DELETE_WAIT_FRIEND_RES rs;
		rs.btResult = info.btResult;
		strncpy_s(rs.szFriendID, _countof(rs.szFriendID), info.szFriendID, MAX_GAMEID_LENGTH);	

		CPacketComposer Packet(S2C_DELETE_WAIT_FRIEND_RES);
		Packet.Add((PBYTE)&rs, sizeof(SS2C_DELETE_WAIT_FRIEND_RES));
		pUser->Send(&Packet);
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EXCHANGE_GAMEID_NOTICE)
{
	SG2S_EXCHANGE_GAMEID_NOTICE	info;
	rp->Read((PBYTE)&info, sizeof(SG2S_EXCHANGE_GAMEID_NOTICE));

	CScopedRefGameUser pUser(info.GameID);
	CHECK_NULL_POINTER_VOID( pUser );

	pUser->RecvPaper(info.iPaperIndex);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_USER_KICK_WITHWEB_NOT)
{
	SS2S_DEFAULT info;
	rp->Read((PBYTE)&info, sizeof(SS2S_DEFAULT));

	CScopedRefGameUser pUser(info.GameID);
	CHECK_NULL_POINTER_VOID( pUser );

	CFSGameClient* pUserClient = (CFSGameClient*)pUser->GetClient();
	if ( nullptr != pUserClient )
	{
		pUserClient->DisconnectClient();
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_RANDOMARROW_EVENT_S_REWARD_SHOUT_RES)
{
	SS2S_DEFAULT	info;
	rp->Read((PBYTE)&info, sizeof(SS2S_DEFAULT));

	SS2C_RANDOMARROW_S_REWARD_SHOUT_NOT	not;
	strncpy_s(not.szGameID, _countof(not.szGameID), info.GameID, _countof(not.szGameID)-1);

	CPacketComposer Packet(S2C_RANDOMARROW_S_REWARD_SHOUT_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_RANDOMARROW_S_REWARD_SHOUT_NOT));
	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_OPENSTATUS_NOT)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer)

		SS2G_EVENT_SHOPPINGFASTIVAL_OPENSTATUS_NOT	not;
	rp->Read((PBYTE)&not, sizeof(SS2G_EVENT_SHOPPINGFASTIVAL_OPENSTATUS_NOT));

	SHOPPINGFESTIVAL.SetCurrentEventType(not.btCurrentEventType);
	SS2G_EVENT_SHOPPINGFASTIVAL_OPENSTATUS info;
	for(int i = 0; i < not.iEventCnt; ++i)
	{
		rp->Read((PBYTE)&info, sizeof(SS2G_EVENT_SHOPPINGFASTIVAL_OPENSTATUS));
		SHOPPINGFESTIVAL.UpdateOpenStatus(info);
	}

	BYTE btOpenStatus = SHOPPINGFESTIVAL.GetOpenStatus(not.btCurrentEventType);
	if(EVENT_SHOPPINGFESTIVAL_OPEN_STATUS_OPEN == btOpenStatus)
		btOpenStatus = OPEN;
	else 
		btOpenStatus = CLOSED;

	CHECK_CONDITION_RETURN_VOID(EVENT_SHOPPINGFESTIVAL_TYPE_SALE_COUPON == not.btCurrentEventType && CLOSED == btOpenStatus);
	CHECK_CONDITION_RETURN_VOID(EVENT_SHOPPINGFESTIVAL_TYPE_LIMITED_SALE_ITEM == not.btCurrentEventType && OPEN == btOpenStatus);

	if(EVENT_SHOPPINGFESTIVAL_TYPE_LIMITED_SALE_ITEM == not.btCurrentEventType)
		not.btCurrentEventType = EVENT_SHOPPINGFESTIVAL_TYPE_LIMITED_SALE_ITEM;

	pServer->BroadCastShoppingFestivalEventStatus(not.btCurrentEventType, btOpenStatus);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_UPDATE_LIMITED_SALEITEM_DAY_NOT)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer)

		SS2G_EVENT_SHOPPINGFASTIVAL_UPDATE_LIMITED_SALEITEM_DAY_NOT not;
	rp->Read((PBYTE)&not, sizeof(SS2G_EVENT_SHOPPINGFASTIVAL_UPDATE_LIMITED_SALEITEM_DAY_NOT));

	SHOPPINGFESTIVAL.UpdateLimitedSaleItemDay(not);

	int iCurrentEventType = SHOPPINGFESTIVAL.GetCurrentEventType();
	CHECK_CONDITION_RETURN_VOID(EVENT_SHOPPINGFESTIVAL_TYPE_LIMITED_SALE_ITEM != iCurrentEventType);
	pServer->BroadCastShoppingFestivalEventInfo(iCurrentEventType);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_RES)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer)

		SS2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_RES));

	CScopedRefGameUser pUser(rs.GameID);
	CHECK_NULL_POINTER_VOID(pUser);

	SEventShoppingFestivalLimitedSaleItem sItem;
	if(FALSE == SHOPPINGFESTIVAL.GetLimitedSaleItem(rs.iIndex, sItem))
	{
		SG2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_FAIL_NOT not;
		not.iIndex = rs.iIndex;

		CPacketComposer Packet(G2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_FAIL_NOT);
		Packet.Add((PBYTE)&not, sizeof(SG2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_FAIL_NOT));
		Send(&Packet);

		pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
		return;
	}

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SBillingInfo BillingInfo;
	BillingInfo.iEventCode = EVENT_BUY_EVENT_LIMITED_SALEITEM;
	BillingInfo.pUser = pUser.GetPointer();
	memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
	BillingInfo.iSerialNum = pUser->GetUserIDIndex();
	memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH+1);	
	memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH+1);
	memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	char* szItemName = pItemShop->GetItemTextCheckAndReturnEventCodeDescription(BillingInfo.iItemCode, BillingInfo.iEventCode);
	if(NULL == szItemName)
		printf(BillingInfo.szItemName, "Buy_ItemShop");
	else
		memcpy(BillingInfo.szItemName, szItemName, MAX_ITEMNAME_LENGTH + 1);	

	BillingInfo.iSellType = SELL_TYPE_CASH;
	BillingInfo.iCurrentCash = pUser->GetCoin();
	BillingInfo.iItemCode = sItem._sReward.iItemCode;
	BillingInfo.iCashChange = sItem._iSalePrice;
	BillingInfo.iPointChange = 0;
	BillingInfo.iTrophyChange = 0;
	BillingInfo.iParam5 = rs.iIndex;
	BillingInfo.iParam6 = sItem._sReward.iRewardIndex;

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
		rs.btResult = RESULT_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_FAIL;
		pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES));

		SG2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_FAIL_NOT not;
		not.iIndex = rs.iIndex;

		CPacketComposer Packet(G2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_FAIL_NOT);
		Packet.Add((PBYTE)&not, sizeof(SG2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_FAIL_NOT));
		Send(&Packet);

		pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
		return;
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_UPDATE_NOT)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer)

		SS2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_UPDATE_NOT not;
	rp->Read((PBYTE)&not, sizeof(SS2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_UPDATE_NOT));

	if(TRUE == SHOPPINGFESTIVAL.UpdateRemainSellCount(not.iIndex, not.iUpdatedRemainSellCount))
	{
		pServer->BroadCastShoppingFestivalEventInfo(EVENT_SHOPPINGFESTIVAL_TYPE_LIMITED_SALE_ITEM);
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_HALFPRICE_SHOUT_RES)
{
	SS2S_DEFAULT	info;
	rp->Read((PBYTE)&info, sizeof(SS2S_DEFAULT));

	SS2C_EVENT_HALFPRICE_SHOUT_NOT	not;
	strncpy_s(not.szGameID, _countof(not.szGameID), info.GameID, _countof(not.szGameID)-1);

	CPacketComposer Packet(S2C_EVENT_HALFPRICE_SHOUT_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_EVENT_HALFPRICE_SHOUT_NOT));
	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2S_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ rq;
	rp->Read((PBYTE)&rq, sizeof(SG2S_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ));

	SEventGameOfDiceRankWaitReward reward;
	vector<SEventGameOfDiceRankWaitReward> vReward;
	for(int i = 0; i < rq.iRewardCnt; ++i)
	{
		rp->Read((PBYTE)&reward, sizeof(SEventGameOfDiceRankWaitReward));
		vReward.push_back(reward);
	}

	CScopedRefGameUser pUser(rq.GameID);
	if(pUser != NULL)
	{
		for(int i = 0; i < vReward.size(); ++i)
		{
			CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
			if(pODBC)
			{
				int iGameIDIndex = rq.iGameIDIndex;
				if(REWARD_TYPE_ITEM == vReward[i]._btRewardType ||
					REWARD_TYPE_ACHIEVEMENT == vReward[i]._btRewardType)
					iGameIDIndex = -1;

				if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(rq.iUserIDIndex, iGameIDIndex, vReward[i]._iRewardIndex))
					continue;

				SRewardConfig* pReward = REWARDMANAGER.GiveReward(pUser.GetPointer(), vReward[i]._iRewardIndex);
				if (NULL != pReward)
				{
					CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
					if(pODBC)
						pODBC->EVENT_GAMEOFDICE_RANK_InsertRewardLog(rq.btType, rq.iRank, rq.iUserIDIndex, iGameIDIndex, rq.GameID, vReward[i]._iRewardIndex);
				}
				else
				{
					// 추후 지급
					CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
					if(pODBC)
					{
						if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(rq.iUserIDIndex, iGameIDIndex, vReward[i]._iRewardIndex))
							continue;

						pODBC->EVENT_GAMEOFDICE_InsertRankWaitGiveRewardList(rq.iUserIDIndex, iGameIDIndex, vReward[i]._iRewardIndex, rq.btType, rq.iRank);
					}
				}
			}
			else
			{
				// 추후 지급
				CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
				if(pODBC)
				{
					int iGameIDIndex = rq.iGameIDIndex;
					if(REWARD_TYPE_ITEM == vReward[i]._btRewardType ||
						REWARD_TYPE_ACHIEVEMENT == vReward[i]._btRewardType)
						iGameIDIndex = -1;

					if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(rq.iUserIDIndex, iGameIDIndex, vReward[i]._iRewardIndex))
						continue;

					pODBC->EVENT_GAMEOFDICE_InsertRankWaitGiveRewardList(rq.iUserIDIndex, iGameIDIndex, vReward[i]._iRewardIndex, rq.btType, rq.iRank);
				}
			}
		}
	}
	else
	{
		// 추후 지급
		CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
		if(pODBC)
		{
			for(int i = 0; i < vReward.size(); ++i)
			{
				int iGameIDIndex = rq.iGameIDIndex;
				if(REWARD_TYPE_ITEM == vReward[i]._btRewardType ||
					REWARD_TYPE_ACHIEVEMENT == vReward[i]._btRewardType)
					iGameIDIndex = -1;

				if(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_CheckGiveReward(rq.iUserIDIndex, iGameIDIndex, vReward[i]._iRewardIndex))
					continue;

				pODBC->EVENT_GAMEOFDICE_InsertRankWaitGiveRewardList(rq.iUserIDIndex, iGameIDIndex, vReward[i]._iRewardIndex, rq.btType, rq.iRank);
			}
		}
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOUT_RES)
{
	SS2G_EVENT_SHOUT_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_EVENT_SHOUT_RES));

	SS2C_EVENT_SHOUT_NOT	ss;
	ss.iEventKind = rs.iEventKind;
	ss.btContentsType = rs.btContentsType;
	strncpy_s(ss.szGameID, _countof(ss.szGameID), rs.GameID, _countof(ss.szGameID)-1);
	memcpy(&ss.sReward, &rs.sReward, sizeof(SREWARD_INFO));

	CPacketComposer Packet(S2C_EVENT_SHOUT_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_EVENT_SHOUT_NOT));
	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_STATUS_FRIEND_RES)
{
	SS2G_FRIEND_INVITE_UPDATE_STATUS_FRIEND_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_UPDATE_STATUS_FRIEND_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	// 초대를 요청한 유저의 상호작용
		user->GetUserFriendInvite()->UpdateInviteStatus(rs.iUserIDIndex, rs.iFriendUserIDIndex, rs.iStatus, rs.szGameID);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_REFUSAL_RES)
{
	SS2G_FRIEND_INVITE_REFUSAL_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_REFUSAL_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->RefusalUser(rs.iRefusalUserIDIndex, rs.iUserIDIndex);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_FRIEND_MISSION_RES)
{
	SS2G_FRIEND_INVITE_UPDATE_FRIEND_MISSION_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_UPDATE_FRIEND_MISSION_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateFriendMission(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_FRIENDLIST_STATUS_RES)
{
	SS2G_FRIEND_INVITE_UPDATE_FRIENDLIST_STATUS_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_UPDATE_FRIENDLIST_STATUS_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateFriendListStatus(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_GIVE_BENEFIT_RES)
{
	SS2G_FRIEND_INVITE_GIVE_BENEFIT_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_GIVE_BENEFIT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateGaveBenefitReward(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_KING_EVENT_UPDATE_EXP_RES)
{
	SS2G_FRIEND_KING_EVENT_UPDATE_EXP_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_KING_EVENT_UPDATE_EXP_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateFriendInviteKingExp(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_ADD_FRIEND_RES)
{
	SS2G_FRIEND_INVITE_ADD_FRIEND_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_ADD_FRIEND_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateFriendInviteAddFriend(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_REMOVE_FRIENDLIST_RES)
{
	SS2G_FRIEND_INVITE_REMOVE_FRIENDLIST_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_REMOVE_FRIENDLIST_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateFriendInvteRemoveFriendList(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_LASTCONNECT_RES)
{
	SS2G_FRIEND_INVITE_LASTCONNECT_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_LASTCONNECT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateLastConnectTimeFriend(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_MISSION_SUCCESS_RES)
{
	SS2G_FRIEND_INVITE_MISSION_SUCCESS_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_MISSION_SUCCESS_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->UpdateMissionSuccessFriend(rs);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_FRIEND_MISSION_LIST_RES)
{
	SS2G_FRIEND_INVITE_FRIEND_MISSION_LIST_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_INVITE_FRIEND_MISSION_LIST_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserFriendInvite()->SendCenterSvrFriendMission();
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ)
{
	SG2S_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ rs;
	rp->Read((PBYTE)&rs, sizeof(SG2S_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ));

	CFSEventODBC* pODBC = (CFSEventODBC*) ODBCManager.GetODBC(ODBC_EVENT);
	if(NULL == pODBC)
	{
		WRITE_LOG_NEW(LOG_TYPE_HIPPOCAMPUS_EVENT, DB_CONNECT, FAIL, "S2G_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ 1 - UserIDIndex:11866902, RewardIndex:0", rs.iUserIDIndex, rs.iRewardIndex);
urn;
	}

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_RANK_CheckGiveReward(rs.iUserIDIndex, rs.iRewardIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_HIPPOCAMPUS_EVENT, DB_CONNECT, FAIL, "S2G_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ 2 - UserIDIndex:11866902, RewardIndex:0", rs.iUserIDIndex, rs.iRewardIndex);
urn;
	}

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(user.GetPointer(), rs.iRewardIndex);
		if (NULL != pReward)
		{
			if(pODBC)
				pODBC->EVENT_HIPPOCAMPUS_RANK_InsertRewardLog(rs.btRankType, rs.iRank, rs.iUserIDIndex, rs.iRewardIndex);
		}	
		else
		{
			if(pODBC)
				pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(rs.iUserIDIndex, rs.iRewardIndex);
		}
	}
	else
	{
		if(pODBC)
			pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(rs.iUserIDIndex, rs.iRewardIndex);
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_MISSIONCASHLIMIT_GIVECASH_RES)
{
	SS2G_MISSIONCASHLIMIT_GIVECASH_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_MISSIONCASHLIMIT_GIVECASH_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->GetUserMissionCashLimitEvent()->GiveCashRes(rs.btResult, rs.btMissionIndex, rs.iRewardIndex);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_MISSIONCASHLIMIT_DECREASE_CASH_NOT)
{
	SS2G_MISSIONCASHLIMIT_DECREASE_CASH_NOT rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_MISSIONCASHLIMIT_DECREASE_CASH_NOT));

	MISSIONCASHLIMIT.DecreaseCash(rs.btMissionIndex);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_LIST_NOT)
{
	SS2G_FRIEND_ACCOUNT_LIST_NOT	rs;

	rp->Read((BYTE*)&rs, sizeof(SS2G_FRIEND_ACCOUNT_LIST_NOT));

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	SFRIEND_ACCOUNT_INFO_S2G sInfo;

	for(BYTE i = 0; i < rs.btFriendCnt; ++i)
	{
		rp->Read((BYTE*)&sInfo, sizeof(SFRIEND_ACCOUNT_INFO_S2G));
		user->GetFriendAccountList()->AddNewFriend(sInfo.info.iUserIDIndex, sInfo.info.szGameID, sInfo.info.szAccountID, sInfo.info.szFriendMemo, sInfo.bIsSecret, sInfo.info.btGamePosition, sInfo.info.btLv, sInfo.iProcessID, sInfo.info.btStatus, sInfo.info.btChannelType, sInfo.info.btLocation, sInfo.info.iPlayWithCnt, sInfo.info.tLastLoginTime);
	}

	if(rs.bIsLast)
	{
		user->SetRecAllFriendAccount( TRUE );
		user->SendFriendAccountList();
	}	
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_ADD_RES)
{
	SS2G_FRIEND_ACCOUNT_ADD_RES	rs;

	rp->Read((BYTE*)&rs, sizeof(SS2G_FRIEND_ACCOUNT_ADD_RES));

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	switch(rs.btOptionType)
	{
	case FriendAccount::OPTION_TYPE_ADD_FRIEND_REQ:
		{
			if(FriendAccount::RESULT_ADD_FRIEND_SUCCESS == rs.btResult)
			{
				if(FriendAccount::STATUS_REQ_SEND == rs.sInfo.btStatus)
				{
					CScopedLockFriendAccount pFriend(user->GetFriendAccountList(), rs.sInfo.iUserIDIndex);
					if(pFriend == nullptr)
					{
						if(nullptr == user->GetFriendAccountList()->AddNewFriend(
							rs.sInfo.iUserIDIndex
							, rs.sInfo.szGameID
							, rs.sInfo.szAccountID
							, rs.sInfo.szFriendMemo
							, rs.bIsSecret
							, rs.sInfo.btGamePosition
							, rs.sInfo.btLv
							, rs.iProcessID
							, rs.sInfo.btStatus
							, rs.sInfo.btChannelType
							, rs.sInfo.btLocation
							, rs.sInfo.iPlayWithCnt
							, rs.sInfo.tLastLoginTime))
						{
							return;
						}
					}
					else
					{
						pFriend->SetStatus(rs.sInfo.btStatus);
					}
				}
			}
		}
		break;

	case FriendAccount::OPTION_TYPE_ADD_FRIEND_RES:
		{
			switch(rs.btResult)
			{
			case FriendAccount::RESULT_ADD_FRIEND_SUCCESS:
				{
					BEGIN_LOCK

						CScopedLockFriendAccount fr(user->GetFriendAccountList(), rs.sInfo.iUserIDIndex);
						if(fr != nullptr)
						{
							fr->SetStatus(FriendAccount::STATUS_FRIEND);
							fr->SetChannelType(FriendAccount::CHANNEL_TYPE_ONLINE_SAME_SVR);

							user->SendToMatchAddFriendAccount(
									fr->GetUserIDIndex()
								,	fr->GetGameID()
								,	fr->GetGamePosition()
								,	fr->GetLv()
								,	fr->GetStatus()
								,	fr->GetChannelType()
								,	fr->GetLocation()
								,	fr->GetWithPlayCnt()
								,	fr->GetLastLogoutTime());
						}

					END_LOCK
				}
				break;

			case FriendAccount::RESULT_ADD_FRIEND_SUCCESS_REJECT:
				{
					user->GetFriendAccountList()->RemoveDestroyFriend(rs.sInfo.iUserIDIndex);
				}
				break;

			default:
				{

				}
				break;
			}
		}
		break;
	}

	SS2C_FRIEND_ACCOUNT_ADD_RES	ss;
	ss.btOptionType = rs.btOptionType;
	ss.btResult = rs.btResult;
	ss.tCurrentTime = _GetCurrentDBDate;
	ss.sInfo = rs.sInfo;

	if(user->IsPlaying())
	{
		CPacketComposer* pPacket = new CPacketComposer(S2C_FRIEND_ACCOUNT_ADD_RES);
		pPacket->Add((PBYTE)&ss, sizeof(SS2C_FRIEND_ACCOUNT_ADD_RES));
		user->PushPacketQueue(pPacket);
	}
	else
	{
		user->Send(S2C_FRIEND_ACCOUNT_ADD_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_ADD_RES));
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_DELETE_RES)
{
	SS2G_FRIEND_ACCOUNT_DELETE_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_ACCOUNT_DELETE_RES));

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	user->GetFriendAccountList()->RemoveDestroyFriend(rs.iFriendIDIndex);

	user->SendToMatchDelFriendAccount(rs.iFriendIDIndex);

	SS2C_FRIEND_ACCOUNT_DELETE_RES	ss;
	ss.eResult = rs.eResult;
	ss.iFriendIDIndex = rs.iFriendIDIndex;

	if(user->IsPlaying())
	{
		CPacketComposer* pPacket = new CPacketComposer(S2C_FRIEND_ACCOUNT_DELETE_RES);
		pPacket->Add((PBYTE)&ss, sizeof(SS2C_FRIEND_ACCOUNT_DELETE_RES));
		user->PushPacketQueue(pPacket);
	}
	else
	{
		user->Send(S2C_FRIEND_ACCOUNT_DELETE_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_DELETE_RES));
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES)
{
	SS2G_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES));

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES	ss;
	ss.eResult = rs.eResult;
	user->Send(S2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES));
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES)
{
	SS2G_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES));

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES	ss;

	ss.eResult = rs.eResult;
	user->Send(S2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES));
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT)
{
	SS2G_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT	rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT));

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	SS2C_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT	ss;
	ss.tCurrentTime = _GetCurrentDBDate;

	bool bMatchNotice = false;

	BEGIN_LOCK

		CScopedLockFriendAccount pFriend(user->GetFriendAccountList(), rs.info.iUserIDIndex);
		if(pFriend == nullptr)
		{
			if(nullptr == user->GetFriendAccountList()->AddNewFriend(
					rs.info.iUserIDIndex
					, rs.info.szGameID
					, rs.info.szAccountID
					, rs.info.szFriendMemo
					, rs.bIsSecret
					, rs.info.btGamePosition
					, rs.info.btLv
					, rs.iProcessID
					, rs.info.btStatus
					, rs.info.btChannelType
					, rs.info.btLocation
					, rs.info.iPlayWithCnt
					, rs.info.tLastLoginTime))
			{
				return;
			}

			ss.sInfo = rs.info;
		}
		else
		{
			if(rs.info.btChannelType != pFriend->GetChannelType())
			{
				bMatchNotice = true;
			}			

			pFriend->SetAccountID(rs.info.szAccountID);
			pFriend->SetGameID(rs.info.szGameID);
			pFriend->SetSecretAvatar(rs.bIsSecret);
			pFriend->SetGamePosition(rs.info.btGamePosition);
			pFriend->SetLv(rs.info.btLv);
			pFriend->SetNoteName(rs.info.szFriendMemo);
			pFriend->SetProcessID(rs.iProcessID);
			pFriend->SetStatus(rs.info.btStatus);
			pFriend->SetChannelType(rs.info.btChannelType);
			pFriend->SetLocation(rs.info.btLocation);
			rs.info.iPlayWithCnt = max(pFriend->GetWithPlayCnt(),rs.info.iPlayWithCnt);
			pFriend->SetWithPlayCnt(rs.info.iPlayWithCnt);
			pFriend->SetLastLogoutTime(rs.info.tLastLoginTime);

			pFriend->GetData(ss.sInfo);
		}
		
	END_LOCK

	if(bMatchNotice)
	{
		if(FriendAccount::CHANNEL_TYPE_OFFLINE == rs.info.btChannelType)
		{
			user->SendToMatchDelFriendAccount(rs.info.iUserIDIndex);
		}
		else
		{
			user->SendToMatchAddFriendAccount(
					rs.info.iUserIDIndex
				,	rs.info.szGameID
				,	rs.info.btGamePosition
				,	rs.info.btLv
				,	rs.info.btStatus
				,	rs.info.btChannelType
				,	rs.info.btLocation
				,	rs.info.iPlayWithCnt
				,	rs.info.tLastLoginTime);
		}
	}	
	
	if(FriendAccount::CHANNEL_TYPE_OFFLINE == rs.info.btChannelType)
	{
		if(ss.tCurrentTime < rs.info.tLastLoginTime)
		{
			// 최근 로그아웃 시간은 최소 "0초 전" 으로 보이게함.
			ss.sInfo.tLastLoginTime = ss.tCurrentTime;
		}
	}
	
	user->Send(S2C_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT));
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES)
{
	SS2G_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES	rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES));

	CScopedRefGameUser user(rs.GameID);
	CHECK_NULL_POINTER_VOID(user);

	CScopedLockFriendAccount pFriend(user->GetFriendAccountList(), rs.iFriendIDIndex);
	if(pFriend != nullptr)
	{
		BYTE btUpdateStatus = (true == rs.bFavor) ? FriendAccount::STATUS_FRIEND_FAVORITE : FriendAccount::STATUS_FRIEND;
		pFriend->SetStatus(btUpdateStatus);
	}

	SS2C_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES	ss;
	ss.iFriendIDIndex = rs.iFriendIDIndex;
	ss.bFavor = rs.bFavor;
	ss.eResult = rs.eResult;
	user->Send(S2C_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES));
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_PVE_SCHEDULER_NOT)
{
	SS2G_PVE_SCHEDULER_NOT not;
	rp->Read((PBYTE)&not, sizeof(SS2G_PVE_SCHEDULER_NOT));

	SS2G_PVE_SCHEDULER_INFO info;
	for(int i = 0; i < not.iMatchCnt; ++i)
	{
		rp->Read((PBYTE)&info, sizeof(SS2G_PVE_SCHEDULER_INFO));
		PVEMANAGER.UpdateOpenStatus(info);
	}
	PVEMANAGER.SendOpenStatus();
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_PVE_DAILY_MISSION_SCHEDULER_NOT)
{
	SS2G_PVE_DAILY_MISSION_SCHEDULER_NOT not;
	rp->Read((PBYTE)&not, sizeof(SS2G_PVE_DAILY_MISSION_SCHEDULER_NOT));

	PVEMANAGER.UpdateDailyMissionOpenStatus(not.bIsOpen);
}


DEFINE_CENTERPROXY_PROC_FUNC(S2G_ENTER_PCROOM_NOT)
{
	SS2G_ENTER_PCROOM_NOT info;
	rp->Read((PBYTE)&info, sizeof(SS2G_ENTER_PCROOM_NOT));

	CScopedRefGameUser pUser(info.GameID);
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SetPCRoomKind(info.iPCRoomKind);
	pUser->CheckLoadPCRoomItemList();
	pUser->SendExposeUserItem();
	pUser->SendUserStat();
	pUser->SendAvatarInfoUpdateToMatch();	
	pUser->ProcessPCRoomEvent();
	pUser->SendPCRoomNotice();
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EXPIRE_PREMIUM_PCROOM_NOT)
{
	if(FALSE == CFSGameServer::GetInstance()->GetNewPCRoom())
	{
		Process_ExpirePremiumPCRoomNot(rp);
		return;
	}

	SS2G_EXPIRE_PREMIUM_PCROOM_NOT info;
	rp->Read((PBYTE)&info, sizeof(SS2G_EXPIRE_PREMIUM_PCROOM_NOT));

	CScopedRefGameUser pUser(info.GameID);
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SetPCRoomKind(PCROOM_KIND_NOT);
	pUser->SetExpirePremiumPCRoom(TRUE);
	pUser->SendExposeUserItem();
	pUser->SendUserStat();
	pUser->SendAvatarInfoUpdateToMatch();	

	CPacketComposer Packet(S2C_EXPIRE_PREMIUM_PCROOM_NOT);
	pUser->CheckAndSend(&Packet, pUser->IsPlaying());
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ)
{
	SG2S_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ rq;
	rp->Read((PBYTE)&rq, sizeof(SG2S_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

	CScopedRefGameUser user(rq.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	vector<int> vNewlyReachedAchievementIndex;
	vector<int> vAnnounceAchievementIndex;

	vNewlyReachedAchievementIndex.push_back(rq.iAchievementIndex); 

	if (TRUE == user->ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex))
	{
		SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
		info.iGameIDIndex = user->GetGameIDIndex();

		strncpy_s(info.szGameID, _countof(info.szGameID), user->GetGameID(), _countof(info.szGameID)-1);
		info.iAchievementCount = vAnnounceAchievementIndex.size();

		for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
		{
			info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
		}

		CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
		PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(user->GetMatchLocation());
		if (pMatch != NULL)
		{
			pMatch->Send(&PacketComposer);
		}
	}
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_PREMIUMPASS_MISSION_SCHEDULER_NOT)
{
	SS2G_PREMIUMPASS_MISSION_SCHEDULER_NOT not;
	rp->Read((PBYTE)&not, sizeof(SS2G_PREMIUMPASS_MISSION_SCHEDULER_NOT));

	PREMIUMPASSEVENT.SetCurrentWeekType(not.btCurrentWeekType);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES)
{
	SS2G_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	CPacketComposer Packet(S2C_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES);
	Packet.Add((BYTE*)&rs.rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES));

	EVENT_TRANSFERJOYCITY_100DREAM_RANK_INFO info;
	for(int i = 0; i < rs.rs.iCount; ++i)
	{
		rp->Read((PBYTE)&info, sizeof(EVENT_TRANSFERJOYCITY_100DREAM_RANK_INFO));
		Packet.Add((BYTE*)&info, sizeof(EVENT_TRANSFERJOYCITY_100DREAM_RANK_INFO));
	}

	user->Send(&Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES)
{
	SS2G_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	if(RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_SUCCESS == rs.rs.btResult)
	{
		user->GetUserTransferJoycity100DreamEvent()->UpdateTryWinRes(rs);
	}

	SS2C_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES info;
	info.btResult = rs.rs.btResult;
	info.iGiveCash = rs.rs.iGiveCash;
	info.iRankNumber = rs.rs.iRankNumber;

	CPacketComposer Packet(S2C_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES);
	Packet.Add((BYTE*)&info, sizeof(SS2C_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES));
	user->Send(&Packet);
}

DEFINE_CENTERPROXY_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES)
{
	SS2G_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SS2G_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	SS2C_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES info;
	info.btResult = rs.rs.btResult;

	CPacketComposer Packet(S2C_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES);
	Packet.Add((BYTE*)&info, sizeof(SS2C_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES));
	user->Send(&Packet);
}