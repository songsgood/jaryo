// CFSGameThread.h: interface for the CFSGameThread class.
//
// 패킷 처리 부분
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CFSTHREAD_H__010F05F3_55B1_416B_AB9D_AFA57A56C5F6__INCLUDED_)
#define AFX_CFSTHREAD_H__010F05F3_55B1_416B_AB9D_AFA57A56C5F6__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "CSocketIOCPThread.h"
qinclude "ThreadODBCManager.h"

class CFSGameClient;
class CFSRankManager;
class CContentsManager;
class CFSGameUser;


#define DECLARE_GAMETHREAD_PROC_FUNC(x) void Proc_##x(CFSGameClient*);
#define DEFINE_GAMETHREAD_PROC_FUNC(x)	void CFSGameThread::Proc_##x(CFSGameClient* pFSClient)
#define CASE_GAMETHREAD_PACKET_PROC_FUNC(x)	case x:	Proc_##x(pFSClient);	break;

class CFSGameThread : public CSocketIOCPThread  
{

public://구동을 위한 필수 - public
	CFSGameThread(CIOCP * pIOCP);
	virtual ~CFSGameThread();

	virtual unsigned Run();
	virtual	CIOCPThread* AllocSingleThread();
	static	CFSGameThread *	Allocate(CIOCP* pIOCP){ return (new CFSGameThread(pIOCP));}

	void LogOut(CIOCPSocketClient* pIOCPSocketClient);

private:

protected://구동을 위한 필수 - protected
	virtual void OnPeerDisconnected(CIOCPSocketClient* pIOCPSocketClient);

////////////////////////////////Packet Process
	void ProcessPacket(CIOCPSocketClient* pIOCPSocketClient);
//

	virtual BOOL OnStartup();

protected:

	///////////// test variable ////////////////
	DWORD	dwLoginProcess0;
	DWORD	dwLoginProcess1;
	DWORD	dwLoginProcess2;
	DWORD	dwLogoutProcess;
	DWORD	dwItemProcess0;
	DWORD	dwItemProcess1;
	DWORD	dwItemProcess2;
	DWORD	dwSkillProcess;
	DWORD	dwGameResultProcess;
	DWORD	dwGameResultUpdate;
	DWORD	dwChatProcess;
	////////////////////////////////////////////

	void Process_FSLogOutTypeCheck(CFSGameClient* pFSClient);
	void Process_FSUserLatencyLog(CFSGameClient* pFSClient);	

	BOOL ProcessGMPacket( CFSGameClient * pFSClient );
	void Process_GM_G2S_LOGIN_REQ( CFSGameClient * pFSClient );
	void Process_GM_G2S_ROOMLIST_REQ( CFSGameClient * pFSClient );
	void Process_GM_G2S_ROOMINFO_REQ( CFSGameClient * pFSClient );
	void Process_GM_G2S_CHAT_REQ( CFSGameClient * pFSClient );
	void Process_GM_G2S_KICKOUT_REQ( CFSGameClient * pFSClient );
	void Process_GM_G2S_FINDUSER_REQ( CFSGameClient * pFSClient );
	void Process_GM_G2S_CLOSEROOM_REQ( CFSGameClient * pFSClient );
	void Process_GM_G2S_FORBID_USER_REQ(CFSGameClient* pFSClient);
	void Process_GM_G2S_CHANGE_HOST_REQ(CFSGameClient* pFSClient);
	void Process_GM_G2S_FORBID_MULITUSER_REQ(CFSGameClient* pFSClient);	
	// 20090401 GM Kick기능
	void Process_GM_C2S_KICKOUT_REQ( CFSGameClient * pFSClient );
	// GM 전체 유저 Kick 기능
	void Process_GM_ALLUSER_KICKOUT_REQ(CFSGameClient * pFSClient);
	// GM 전체 유저 쪽지 기능
	void Process_GM_ALLUSER_SENDPAPER_REQ(CFSGameClient * pFSClient);
	// End

	void Process_RATING_POINT_REQ(CFSGameClient* pFSClient);

	void Process_MonitorAliveEcho(CIOCPSocketClient* pIOCPSocketClient);
	void Process_FSCheckAlive( CFSGameClient * pFSClient);

	//Process_Login
	void Process_FSLogin( CFSGameClient * pClient );

	void Process_FSChat( CFSGameClient * pClient );

	void Process_EventListInfoReq( CFSGameClient * pClient );
	
	
	//Process_Lobby ////////////////////////////////////////////
	void Process_FSLRequestRoomList( CFSGameClient * pClient );

	void Process_FSLCreateRoom(CFSGameClient * pClient);
	void Process_FSLJoinRoom(CFSGameClient * pClient);
	void Process_FSUpdateUserSession(CFSGameClient * pClient);
	void Process_FSLogOut(CFSGameClient * pClient);
	void Process_FSLRequestUserInfo(CFSGameClient * pFSClient);	
	void Process_FSLRoomMemberList(CFSGameClient * pFSClient);
	void Process_FSLSelectPosition(CFSGameClient * pFSClient);
	void Process_FSLTeamList(CFSGameClient* pFSClient);	
	void Process_FSLClubTeamList(CFSGameClient* pFSClient);
	void Process_FSLCreateTeam(CFSGameClient* pFSClient);
	void Process_FSLEnterTeam(CFSGameClient* pFSClient);
	void Process_FSLQuickJoinRoom_As_Observer(CFSGameClient* pFSClient);
	void Process_FSLRequestRoomORTeamInfo(CFSGameClient* pFSClient);
	void Process_UseSecondBoostItem(CFSGameClient *pFSClient);
	void Process_SecondBoostItemExpired(CFSGameClient *pFSClient);
	void Process_RemainPlayTime_In_Room(CFSGameClient* pFSClient);

	// 2010.02.02
	// 따라가기 기능 추가
	// ktKim
	void Process_FSFollowFriend(CFSGameClient * pClient);
	// End
	
	//치트키 기능 추가
	void Process_Cheat(CFSGameClient * pClient); 

	///////////////////////  Team Wait Room ///////////////////////////
	void Process_FSTExitWaitTeam(CFSGameClient* pFSClient);
	void Process_FSTRequestTeamMemberList(CFSGameClient* pClient);
	void Process_FSTReadyTeamList(CFSGameClient* pFSClient);
	void Process_FSTRequsetTeamInfo(CFSGameClient* pFSClient);
	void Process_FSTRequestTeamMatch(CFSGameClient* pFSClient);	
	void Process_FSTChanageTeamPassword(CFSGameClient *pFSClient);
	void Process_FSTForceOutUser(CFSGameClient* pClient);

	//////////////////////  Team Ready Room ///////////////////////////
	void Process_FSTRCancelTeamMatch(CFSGameClient* pFSClient);
	void Process_FSTRSendTeamInfo(CFSGameClient* pFSClient);

	//Process_Room /////////////////////////////////////////////////////
	void Process_FSRExitRoom(CFSGameClient* pClient);
	void Process_FSRRequestTeamMemberList(CFSGameClient* pClient);
	void Process_FSRRequestTeamChange(CFSGameClient* pClient);
	void Process_FSRInviteUser(CFSGameClient* pClient);

	void Process_FSRSendLoadingProgress(CFSGameClient * pFSClient);

	////////////////// Change GameMode ////
	void Process_FSRChangeGameMode(CFSGameClient* pClient);
	void Process_FSRRequestRoomInfoInRoom(CFSGameClient *pClient);
	void Process_FSChooseAvatarInfo( CFSGameClient * pClient );
	void Process_FSRChangeGameCourt( CFSGameClient * pFSClient);
	void Process_FSRForceOutUser(CFSGameClient* pClient);
	void Process_FSRChangeRoomPassword(CFSGameClient* pFSClient);

	void Process_FSRChangePracticeMethod(CFSGameClient * pFSClient);
	void Process_FSRChangeTeamInGame(CFSGameClient * pFSClient);

	/////////////////Wait -> Game  /////////////////////////////////////////
	void Process_FSRRequestReady(CFSGameClient* pClient);
	void Process_FSRRequestToggleIamReady(CFSGameClient* pClient);
	void Process_FSRRequestGameStartPause(CFSGameClient* pClient);
	void Process_FSGInitialGame(CFSGameClient* pClient);
	void Process_FSGPlayReadyOk(CFSGameClient* pClient);
	void Process_FSGPlayStartOK(CFSGameClient* pClient);

	///////////////// New Wait->Game //////////////////////////
	void Process_FSRCheckGameStartPreparationResponse(CFSGameClient* pClient);
	void Process_FSRCheckPublicIPAddressForGameResponse(CFSGameClient *pClient);
	void Process_FSRStartUDPHolepunchingResponse(CFSGameClient * pClient);
	void Process_FSRStartBandWidthResponse(CFSGameClient * pClient);
	void Process_FSRReadyHostResponse(CFSGameClient* pClient);
	void Process_FSRConnectHostResponse(CFSGameClient* pClient);

	///////////////// Voice Chatting //////////////////////////
	void Process_FSVoiceChattingUserOptionInfoReq(CFSGameClient* pClient);

	/////////////////In Game /////////////////////////////////////////////////
	void Process_FSGRequestScoreChange(CFSGameClient* pClient);
	void Process_FSGQuarterEndAck(CFSGameClient* pClient);
	void Process_FSGShootTry(CFSGameClient* pClient);
	void Process_FSGRebound(CFSGameClient* pClient);
	void Process_FSGAsist(CFSGameClient* pClient);
	void Process_FSGSteal(CFSGameClient* pClient);
	void Process_FSGBlock(CFSGameClient* pClient);
	void Process_FSGJumpBall(CFSGameClient* pClient);
	void Process_FSGSendGameResult(CFSGameClient* pClient);
	void Process_FSGChangeGameStatus(CFSGameClient* pClient);
	void Process_FSGTerminateGame(CFSGameClient* pClient);
	void Process_FSGDisconnectedGame(CFSGameClient* pClient);
	void Process_FSGSetGameTime(CFSGameClient* pClient);
	void Process_FSRCheckGamePlayInput(CFSGameClient *pClient);
	void Process_FSGShootSuccessorFailure(CFSGameClient* pClient);

	//// Ranking Page //////////////
	void Process_FSEnterRankingPage(CFSGameClient* pFSClient);
	void Process_FSTotalRankingList(CFSGameClient* pFSClient);
	void Process_FSUserRanking(CFSGameClient* pFSClient);
	void Process_FSExitRankingPage(CFSGameClient* pFSClient);
	void Process_RankSearchReq(CFSGameClient* pFSClient);

	/////// Item Page /////
	void Process_FSIEnterItemPage(CFSGameClient* pFSClient);
	void Process_FSIExitItemPage(CFSGameClient* pFSClient);
	void Process_FSIUserItemList(CFSGameClient* pFSClient);
	void Process_FSIShopItemList(CFSGameClient* pFSClient);
	void Process_FSIItemSelect(CFSGameClient* pFSClient);
	void Process_FSIResetFeature(CFSGameClient* pFSClient);
	void Process_FSIAvatarSelectInItem(CFSGameClient* pFSClient);
	void Process_FSIShopItemSelect(CFSGameClient* pFSClient);
	void Process_FSExposeUserItem(CFSGameClient * pClient);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EPORTSCARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EPORTSCARD_BUY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GUARANTEE_BUY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GUARANTEE_INFO_REQ);
	/////// Skill Page ///////////
	void Process_FSSSkillList(CFSGameClient* pFSClient);
	void Process_FSSBuySkill(CFSGameClient* pFSClient);
	void Process_FSSEnterSkillPage(CFSGameClient* pFSClient);
	void Process_FSSExitSkillPage(CFSGameClient* pFSClient);
	void Process_FSSSkillInventoryList(CFSGameClient* pFSClient);
	void Process_FSSSkillSlotList(CFSGameClient* pFSClient);
	void Process_FSSInsertToSlot(CFSGameClient* pFSClient);
	void Process_FSSInsertToInventory(CFSGameClient* pFSClient);
	
	void Process_FSEnterClubReq(CFSGameClient* pFSClient);
	void Process_FSExitClubReq(CFSGameClient* pFSClient);
	void Process_FSClubInfoReq(CFSGameClient* pFSClient);
	void Process_FSClubLobbyMemberListReq(CFSGameClient* pFSClient);
	void Process_FSClubMemberListReq(CFSGameClient* pClient);
	void Process_FSClubListReq(CFSGameClient* pFSClient);	
	void Process_FSClubMarkListReq(CFSGameClient* pFSClient);
	void Process_FSClubMarkChangeReq(CFSGameClient* pFSClient);
	void Process_FSClubGradeUpInfoReq(CFSGameClient* pFSClient);
	void Process_FSClubGradeUpReq(CFSGameClient* pFSClient);
	void Process_FSClubNoticeChangeReq(CFSGameClient* pFSClient);
	void Process_FSClubInviteReq(CFSGameClient* pFSClient);
	void Process_FSClubInviteDataReq(CFSGameClient *pClient);
	void Process_FSClubJoinReq(CFSGameClient* pFSClient);
	void Process_FSClubJoinRequestReq(CFSGameClient* pFSClient);
	void Process_FSClubJoinRequestListReq(CFSGameClient* pFSClient);
	void Process_FSClubJoinRequestInfoReq(CFSGameClient* pFSClient);
	void Process_FSClubRequestAnswerReq(CFSGameClient* pFSClient);
	void Process_FSClubRemoveMemberReq(CFSGameClient* pFSClient);
	void Process_FSClubWithDrawReq(CFSGameClient* pFSClient);
	void Process_FSClubAdminMemberListReq(CFSGameClient* pFSClient);
	void Process_FSClubAddAdminMemberReq(CFSGameClient* pFSClient);
	void Process_FSClubRemoveAdminMemberReq(CFSGameClient* pFSClient);
	void Process_FSClubAddKeyPlayerReq(CFSGameClient* pFSClient);
	void Process_FSClubRemoveKeyPlayerReq(CFSGameClient* pFSClient);
	void Process_FSClubExtendKeyPlayerPopUpReq(CFSGameClient* pFSClient);
	void Process_FSClubExtendKeyPlayerReq(CFSGameClient* pFSClient);
	void Process_FSClubLineBoardListReq(CFSGameClient* pFSClient);
	void Process_FSClubAddLineBoardReq(CFSGameClient* pFSClient);
	void Process_FSClubMemberPRChangeReq(CFSGameClient* pFSClient);
	void Process_FSClubSetUpPopUpReq(CFSGameClient* pFSClien);
	void Process_FSClubCheckClubNameReq(CFSGameClient* pFSClient);
	void Process_FSClubSetUpReq(CFSGameClient* pFSClient);
	void Process_FSClubTransferReq(CFSGameClient* pFSClient);
	void Process_FSClubDonationPopUpReq(CFSGameClient* pFSClient);
	void Process_FSClubDonationReq(CFSGameClient* pFSClient);
	void Process_FSClubCreatePopUpReq(CFSGameClient* pFSClient);
	void Process_FSClubCreateReq(CFSGameClient* pFSClient);
	void Process_FSClubMemberRecordListReq(CFSGameClient* pFSClient);
	void Process_FSClubRecentMatchRecordListReq(CFSGameClient* pFSClient);
	void Process_FSClubPRListReq(CFSGameClient* pFSClient);
	void Process_FSClubPRChangeReq(CFSGameClient* pFSClient);
	void Process_FSMyClubRankReq(CFSGameClient* pFSClient);
	void Process_FSClubRankListReq(CFSGameClient* pFSClient);
	void Process_FSClubCheerLeaderInfoReq(CFSGameClient* pFSClient);
	void Process_FSClubCheerLeaderShopListReq(CFSGameClient* pFSClient);
	void Process_FSClubBuyCheerLeaderPopUpReq(CFSGameClient* pFSClient);
	void Process_FSClubBuyCheerLeaderReq(CFSGameClient* pFSClient);
	void Process_FSClubCheerLeaderListReq(CFSGameClient* pFSClient);
	void Process_FSClubCheerLeaderChangeReq(CFSGameClient* pFSClient);
	void Process_FSClubMyCheerLeaderChangeReq(CFSGameClient* pFSClient);
	void Process_FSClubBuyCLPopupReq(CFSGameClient* pFSClient);

	void Process_FSMultiLoginUserLogOut(CFSGameClient* pFSClient);
	void Process_UserShout(CFSGameClient* pFSClient);
	void Process_FSShoutCountReq(CFSGameClient* pFSClient);
	void Process_FSSendGiftGameID(CFSGameClient* pFSClient);
	void Process_FSTChangeUserPosition(CFSGameClient* pFSClient);

	//////////////////////////////////Packet Send
	void SendCurAvatarTrophy(CFSGameClient* pFSClient, int iTrophy );
	void SendCurAvatarSkillPoint(CFSGameClient* pFSClient, int iTotalSkillPoint, int iUsedSkillPoint);
	void SendLoginResult(CFSGameClient * pClient, int iErrorCode );
	void SendChangeLobbyResult(CFSGameClient * pClient, int iErrorCode); 
	void SendOptionsToClient(CFSGameClient* pFSClient);
	void SendEquipItemInfo( CFSGameClient * pClient );
	void SendNotice( CFSGameClient * pClient , int iCode , int nError);

	////////////////////////////////// Others
	void Process_FSSetOption(CFSGameClient * pFSClient);
	void Process_FSAnnounce( CFSGameClient* pFSClient );
	void Process_FSAnnounceCheckAccount(CFSGameClient * pFSClient);
	void Process_FSCashInfo( CFSGameClient *pFSClient);

	// 쪽지 선물 친구 ///////////////////////////////////////////
	void Process_PaperInfoReq(CFSGameClient *pFSClient);		
	void Process_PresentListReq(CFSGameClient *pFSClient);
	void Process_PresentInfoReq(CFSGameClient *pFSClient);			
	void Process_AcceptPresent(CFSGameClient *pFSClient);	
	void Process_GetChoosePropertyItemReq(CFSGameClient *pFSClient);
	void Process_SendPaper(CFSGameClient *pFSClient);		
	void Process_DelPaperOrPresent(CFSGameClient *pFSClient);		
	void Process_MsgToAllFriend(CFSGameClient *pFSClient);			
	void Process_MsgToFriend(CFSGameClient *pFSClient);					
	void Process_AddFriend(CFSGameClient *pFSClient);					
	void Process_DelFriend(CFSGameClient *pFSClient);	
	void Process_FSIOpenTreasureBox(CFSGameClient *pFSClient);
	void Process_FSRCheckChiefAssign(CFSGameClient* pFSClient);
	void Process_FSRMoveChief(CFSGameClient* pFSClient);
	void Process_FSTCheckChiefAssign(CFSGameClient* pFSClient);
	void Process_FSTMoveChief(CFSGameClient* pFSClient);
	void Process_AddInterestUser(CFSGameClient *pFSClient);
	void Process_DelInterestUser(CFSGameClient *pFSClient);
	void Process_AddBannedUser(CFSGameClient *pFSClient);
	void Process_DelBannedUser(CFSGameClient *pFSClient);
	void Process_FSSetupChatOption(CFSGameClient *pFSClient);
	void Process_FSRGameStartHalfTime(CFSGameClient* pFSClient);

	void Process_FSTGetMyItemBag(CFSGameClient* pFSClient);
	void Process_FSRGetMyItemBag(CFSGameClient* pFSClient);
	void Process_FSApplyBagItem(CFSGameClient* pFSClient);
	void Process_FSRGetMySkillBag(CFSGameClient* pFSClient);
	void Process_FSTGetMySkillBag(CFSGameClient* pFSClient);
	void Process_FSApplyBagSkill(CFSGameClient* pFSClient);
	void ProcessMyOwnItemList(CFSGameClient* pFSClient);
	void Process_FSIChangeHeight(CFSGameClient* pFSClient);
	void Process_FSISetupHeight(CFSGameClient* pFSClient);
	void Process_FSShoutMessage(CFSGameClient* pFSClient);
	void Process_FSIChangeName(CFSGameClient* pFSClient);
	void Process_FSICheckName(CFSGameClient* pFSClient);
	void Process_FSTChangeTeamName(CFSGameClient *pFSClient);
	void Process_FSRealTimeMessage_Req(CFSGameClient* pFSClient);
	void Process_FSUsePauseItem(CFSGameClient *pFSClient);
	void Process_FSGamePause(CFSGameClient *pFSClient);
	void ProcessPacket_Log( CIOCPSocketClient * pIOCPSocketClient );
	void Process_FSCheckAvatarStatus(CFSGameClient* pFSClient);
	void Process_FSCheckSumStatus(CFSGameClient* pFSClient);
	void Process_FSCheckClientHack(CFSGameClient* pFSClient);
	void Process_FSIUserItemLock(CFSGameClient* pFSClient);
	void Process_FSClubAreaListReq(CFSGameClient* pFSClient);
	void Process_FSChangeGameRoomName(CFSGameClient* pFSClient);
	void Process_FSClubStatusChangeReq(CFSGameClient* pFSClient);
	void Process_FSMyClubPRReq(CFSGameClient* pFSClient);
	void Process_FSMyClubCLReq(CFSGameClient* pFSClient);
	void Process_FSDeleteBoardMsgReq(CFSGameClient* pFSClient);

	void Process_FSSetObserverRoomID(CFSGameClient* pFSClient);
	void Process_FSExitObservingGame(CFSGameClient* pFSClient);	
	
	void Process_FSTChangeTeamMode(CFSGameClient* pFSClient);

	void ProcessFSTChangeTeamAllowObserve(CFSGameClient* pFSClient);
	void ProcessFSTChangeRoomAllowObserve(CFSGameClient* pFSClient);

	void Process_FSPunishment(CFSGameClient* pFSClient);

	// 2010.01.22
	// 도망가기 기능 추가
	// ktKim
	void Process_FSGiveupVote(CFSGameClient* pFSClient);
	// End
	// 2010.02.12
	// 매치메이킹
	// ktKim
	void ProcessFSTChallengeRegister(CFSGameClient* pFSClient);
	void ProcessFSTChangeAvoidTeam(CFSGameClient* pFSClient);
	void ProcessFSTAutoRegister(CFSGameClient* pFSClient);
	void ProcessFSTAutoTeamRegister(CFSGameClient* pFSClient);
	void ProcessFSRTeamEstimate(CFSGameClient* pClient);
	// End

	// 홀펀칭 진단 패킷
	void Process_FSDiagnosis(CFSGameClient*pFSClient);

	// Game Start 진단 패킷
	void Process_FSDiagnosis2(CFSGameClient*pFSClient);

	// 긴급 진단패킷
	void Process_FSUrgent(CFSGameClient*pFSClient);

	void Process_ChatReport(CFSGameClient* pClient);
	void Process_ChatReportCheck(CFSGameClient* pClient);

	// 싱글플레이 기능 로그인 서버에서 게임서버로 이전
	void Process_FSTutorialComplete( CFSGameClient* pFSClient );
	void Process_FSMiniGameInfo( CFSGameClient* pFSClient );
	void Process_FSMiniGameStart( CFSGameClient* pFSClient );
	void Process_FSMiniGameComplete( CFSGameClient* pFSClient );
	void Process_FSEpisodeInfoReq( CFSGameClient* pFSClient );
	void Process_FSEpisodeResultReq( CFSGameClient* pFSClient );
	void Process_FSAIGameInfo( CFSGameClient* pFSClient );
	void Process_FSAIGameResult( CFSGameClient* pFSClient );
	void Process_FSSingleGameEnd( CFSGameClient* pFSClient );
	void Process_FSSingleGameStart( CFSGameClient* pFSClient );
	void Process_FSFastRechargeSvrDataReq( CFSGameClient* pFSClient );
	

	void	Process_AchievementConfigListReq(CFSGameClient* pFSClient);
	void	Process_CompletedAchievementListReq(CFSGameClient* pFSClient);
	void	Process_EquipAchievementTitleReq(CFSGameClient* pFSClient);
	void	Process_SingleAchievementLogInfomationReq(CFSGameClient* pFSClient);

	void	Process_InviteUserList(CFSGameClient* pFSClient);

	void	Process_TournamentList(CFSGameClient* pFSClient);		
	void	Process_TournamentWaitList(CFSGameClient* pFSClient);
	void	Process_TournamentNotifyReq(CFSGameClient* pFSClient);
	void	Process_TournamentProgramInfoReq(CFSGameClient* pFSClient);
	void	Process_TournamentCreateReq(CFSGameClient* pFSClient);
	void	Process_TournamentEnterReq(CFSGameClient* pFSClient);
	void	Process_TournamentExitReq(CFSGameClient* pFSClient);
	void	Process_TournamentCreatePreMadeTeamReq(CFSGameClient* pFSClient);
	void	Process_TournamentInviteUserReq(CFSGameClient* pFSClient);
	void	Process_TournamentInviteSuggestRes(CFSGameClient* pFSClient);
	void	Process_TournamentExitPreMadeTeamReq(CFSGameClient* pFSClient);
	void	Process_TournamentButtonInfoReq(CFSGameClient* pFSClient);
	void	Process_TournamentMoveSeasonReq(CFSGameClient* pFSClient);
	void	Process_TournamentRegisterPreMadeTeamReq(CFSGameClient* pFSClient);
	void	Process_TournamentChangeNameReq(CFSGameClient* pFSClient);
	void	Process_TournamentChangePasswordReq(CFSGameClient* pFSClient);
	void	Process_TournamentChangeTeamReq(CFSGameClient* pFSClient);
	void	Process_TournamentForceOutPreMadeTeamReq(CFSGameClient* pFSClient);
	void	Process_TournamentStartReq(CFSGameClient* pFSClient);
	void	Process_TournamentMyTeamInfoReq(CFSGameClient* pFSClient);
	void	Process_TournamentUnresisterTeam(CFSGameClient* pFSClient);
	void	Process_TournamentPublicOpenInfoReq(CFSGameClient* pFSClient);
	void	Process_TournamentCreatableReq(CFSGameClient* pFSClient);

	void	ProcessEnterLobbyReq(CFSGameClient* pFSClient);
	
	void	Process_StepComplateReward(CFSGameClient* pFSClient);

	void	Process_DetailGameRecord(CFSGameClient* pFSClient);

	void Process_FSGUseHackNotifyRes(CFSGameClient* pFSClient);
	void Process_FSRExposeReq(CFSGameClient* pFSClient);
	void Process_FSDiscountItemInfoReq(CFSGameClient* pFSClient);
	void Process_AccItemSelect_Req(CFSGameClient* pFSClient);
	void Process_UserGamePlayResearchRes( CFSGameClient* pClient );
	
	void Process_OpenPointRewardCardReq(CFSGameClient* pFSClient);

	void Process_ActionShopListReq(CFSGameClient* pFSClient);
	void Process_ActionDetailInfoReq(CFSGameClient* pFSClient);
	void Process_ActionBuyReq(CFSGameClient* pFSClient);
	void Process_UserKeyActionSlotReq(CFSGameClient* pFSClient);
	void Process_UserSeremonySlotReq(CFSGameClient* pFSClient);
	void Process_UserActionInventoryListReq(CFSGameClient* pFSClient);
	void Process_UserActionChangeReq(CFSGameClient* pFSClient);
	void Process_UserActionSlotReq(CFSGameClient* pFSClient);
	void Process_UserActionSlotResetReq(CFSGameClient* pFSClient);
	void Process_ActionRoomAniReq(CFSGameClient* pFSClient);
	void Process_ActionRoomApplySetReq(CFSGameClient* pFSClient);
	void Process_UserApplyFreeStylePreviewReq(CFSGameClient* pFSClient);
	void Process_UserGetFreeStylePreviewReq(CFSGameClient* pFSClient);
	void Process_ChangeItemColorReq(CFSGameClient* pFSClient);
	void Process_ChannelBuffItemCheckUseReq(CFSGameClient* pFSClient);
	void Process_ChannelBuffItemUseReq(CFSGameClient* pFSClient);
	void Process_ChannelBuffRankingReq(CFSGameClient* pFSClient);
	void Process_SignReq( CFSGameClient* pFSClient );
	void Process_SetObserverAllowReq( CFSGameClient* pFSClient );
	void Process_CheckDressItemReq(CFSGameClient* pFSClient);
	void Process_EnterPrivateChatReq(CFSGameClient* pFSClient);
	void Process_ExitPrivateChatReq(CFSGameClient* pFSClient);
	void Process_PrivateChatUserPageStatusNot(CFSGameClient* pFSClient);
	void Process_PrivateChatUserListReq(CFSGameClient* pFSClient);
	void Process_RegisterDressItemReq(CFSGameClient* pFSClient);
	void Process_UseSpecialPropertyItemReq(CFSGameClient* pFSClient);
	void Process_ReportUserPattern(CFSGameClient* pFSClient);
	void Process_PackageItemComponentListReq(CFSGameClient* pFSClient);
	void Process_ChangeItemReq(CFSGameClient* pFSClient);
	
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_AUTO_ENTER_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CHAT_ROOM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHAT_REPEAT_BAN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_TOKEN_EXCHANGE_ITEM_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_TOKEN_EXCHANGE_ITEM_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ITEM_BUYING_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_BOARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USE_BINGO_TATOO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_SHUFFLE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_SHUFFLE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_INITIALIZE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_INITIALIZE_REWARD_COST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_INITIALIZE_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_REWARD_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_MAIN_REWARD_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BINGO_TOKEN_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_MATCH_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_MATCH_DETAIL_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_CREATE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_RESET_PAGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_ADD_PAGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_ADD_CASH_PAGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_GET_DAY_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_FINISH_PAGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_TEST_RECORDBOARD_ADD_MATCH_DATA_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_RANKING_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_RANKING_USER_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_MISSION_GET_BENEFIT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_PENNANT_SLOT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_PENNANT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_USE_PENNANT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_PENNANT_SLOT_POPUP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_PENNANT_SLOT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_CLOTHES_SHOP_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLOTHES_POPUP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLOTHES_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_CLUBMARK_SHOP_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLUBMARK_POPUP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLUBMARK_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_VOICE_SETTING_INFO_REQ);

	// Coach card
	void	Process_EnterCoachCardPageReq(CFSGameClient* pFSClient);
	void	Process_CoachCardShopItemListReq(CFSGameClient* pFSClient);
	void	Process_BuyItemReq(CFSGameClient* pFSClient);
	void	Process_CoachCardShopInventoryListReq(CFSGameClient* pFSClient);
	void	Process_UseItemReq(CFSGameClient* pFSClient);
	void	Process_EquipCoachCardReq(CFSGameClient* pFSClient);
	void	Process_UnEquipCoachCardReq(CFSGameClient* pFSClient);
	void	Process_Register_Coach_Card_Combine(CFSGameClient* pFSClient);
	void	Process_UnregisterCoachCardCombine(CFSGameClient* pFSClient);
	void	Process_NeedPriceForItemCombineReq(CFSGameClient* pFSClient);
	void	Process_EnterCoachCardCombinationTabRes(CFSGameClient* pFSClient);
	void	Process_CombineItemReq(CFSGameClient* pFSClient);
	void	Process_ThrowAwayItemReq(CFSGameClient* pFSClient);
	void	Process_CoachCardSlotPackageListReq(CFSGameClient* pFSClient);
	void	Process_CoachCardSubActionInfluence(CFSGameClient* pFSClient);
	void	Process_CoachCardInfo(CFSGameClient* pFSClient);
	void	Process_CoachCardPotentialAbility(CFSGameClient* pFSClient);
	void	Process_CoachCardPotentialRageBalance(CFSGameClient* pFSClient);
	void	Process_CoachCardPotentialAction(CFSGameClient* pFSClient);
	void	Process_CoachCardPotentialFreeStyle(CFSGameClient* pFSClient);
	void Process_CoachCardSort( CFSGameClient* pFSClient);
	void Process_CoachCardTermExtendPriceListReq( CFSGameClient * pFSClient );
	void Process_CoachCardTermExtend( CFSGameClient * pFSClient );
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_UPGRADE_POTENTIAL_CARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_UPGRADE_POTENTIAL_CARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_REQ);

	void Process_ChangeUseStyleReq(CFSGameClient* pFSClient);
	void Process_ChangeStyleNameReq(CFSGameClient* pFSClient);
	void Process_ChangeUseStyleInItemShopReq(CFSGameClient* pFSClient);
	void Process_ChangeUseStyleInBagReq(CFSGameClient* pFSClient);
	void Process_SpecialPartsInfoReq(CFSGameClient* pFSClient);
	void Process_SpecialPartsStatApplyReq(CFSGameClient* pFSClient);

	void Process_FSIPremiumStat(CFSGameClient* pFSClient);

	void GiveGameGuideItem( CFSGameUser * pUser );

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_CLUBTOURNAMENT_PAGE_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CLUBTOURNAMENT_PAGE_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_MATCH_TBALE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_KEY_PLAYER_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_ENTER_GAMEROOM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_VOTE_ROOM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_CLUBTOURNAMENT_VOTE_PAGE_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CLUBTOURNAMENT_VOTE_PAGE_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_VOTE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_MATCH_RESULT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_BENEFIT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_GET_BENEFIT_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_GAME_START_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_TEST_CLUBTOURNAMENT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SECRETSTORE_ITEMLIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_USER_ACTION_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_INTENSIVEPRACTICE_ROOM_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_START_INTENSIVEPRACTICE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_END_INTENSIVEPRACTICE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHANGE_ROOMMODE_INTENSIVEPRACTICE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ADD_POINT_INTENSIVEPRACTICE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_INTENSIVEPRACTICE_RANK_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_INTENSIVEPRACTICE_ROOMINFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LEFT_SEAT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_JOIN_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_RACE_BOARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_JOIN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_DISTRICT_SELECT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_SUPPORT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_BUFFITEM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USE_BUFFITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_DAILY_RANK_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_SEASON_RANK_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_RANK_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USER_RANKING_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USER_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_USER_MISSION_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FACTION_SHOP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LUCKYBOX_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LUCKYBOX_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USE_LUCKYTICKET_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_WORDPUZZLES_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_WORDPUZZLES_EVENT_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COMPOUNDINGITEM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COMPOUNDINGITEM_COMBINE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMBOX_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMBOX_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USE_RANDOMBOX_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMBOX_MISSION_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_THREE_KINGDOMS_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_THREE_KINGDOMS_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_PREVIEW_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_BUY_PRODUCT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_MISSION_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_MISSION_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_BUY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_INVENTORYLIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_EQUIP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_UNEQUIP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_MY_EQUIP_LIST_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HONEYWALLET_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HONEYWALLET_OPEN_WALLET_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_MAKE_FINISH_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_CHECK_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CHARACTER_COLLECTION_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_CHAR_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_SENTENCE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_SENTENCE_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_IN_ROOM_WEAR_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLGIFT_CLEAR_STEP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_OPEN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLONE_CHARACTER_CHANGE_INVENTORY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLONE_CHARACTER_CHANGE_ROOM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TODAY_HOTDEAL_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_POTENCARD_TERM_EXTEND_LIST_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SKYLUCKY_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SKYLUCKY_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHEERLEADER_EVENT_CHEERLEADER_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHEERLEADER_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BUY_EVENT_CHEERLEADER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BUY_EVENT_CHEERLEADER_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PAYBACK_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PAYBACK_WANTPAYBACK_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_INITIALIZE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_CRUSH_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GOLDENSAFE_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GOLDENSAFE_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_BASKETBALL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BASKETBALL_MY_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BASKETBALL_SHOP_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BASKETBALL_PRODUCT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHANGE_BASKETBALL_SKIN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHANGE_BASKETBALL_OPTION_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BUY_BASKETBALL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_EVENT_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_SELL_PRODUCT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_USE_COIN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_USE_KEY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_USE_STAR_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_GET_TRY_REWARD_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANK_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANK_MY_AVATAR_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANK_MAIN_GAMEID_SELECT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_USE_TICKET_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_USE_COIN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_UPDATE_SCORE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOVEPAYBACK_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOVEPAYBACK_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOVEPAYBACK_USE_LOVE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BIGWHEEL_LOGIN_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BIGWHEEL_LOGIN_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_BIGWHEEL_LOGIN_STAMP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SALEPLUS_EVENT_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MISSION_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MONEY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FIRSTCASH_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_POT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_USE_ARROW_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_USE_POT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_INPUT_MONEY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_USE_MONEY_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_BONUS_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_USE_TICKET_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_USE_SOULSTONE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LVUPEVENT_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_READY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_REWARDINFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_FLIPCARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MATCHINGCARD_TIMECHECK_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENTCASH_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_WANT_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTOR_STAT_MODULATION_CHECK_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SKILL_OVERLAPCOMMAND_CHANGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_ATTENDANCE_ITEMSHOP_BOARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_ATTENDANCE_ITEMSHOP_UPDATEITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_XIGNCODE_SECURITY_DATA_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LVUPEVENT_GET_CHAR_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOSEBALL_CATCH);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_FACE_CHANGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEPLAY_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEPLAY_EVNET_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_OPEN_EVENT_LIST_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MISSION_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MISSION_CHECK_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_APPRAISAL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_APPRAISAL_RANK_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_APPRAISAL_USER_RANK_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LUCKYSTAR_SESSION_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_CHANGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_TREE_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_TREE_EVENT_USE_ITEM_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_AVATAR_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOGINPAYBACK_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOGINPAYBACK_REQUEST_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_CHANGE_PIECE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_USE_PIECE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SONOFCHOICE_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SONOFCHOICE_BUY_RANDOM_PRODUCT_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_BOARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_BOARD_RESET_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PURCHASE_GRADE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PURCHASE_GRADE_GIVE_EVENT_COIN_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_REWARD_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_USE_COIN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXPBOX_ITEM_USE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXPBOX_ITEM_USE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SALE_RANDOMITEM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SALE_RANDOMITEM_OPEN_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXCHANGE_NAME_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXCHANGE_NAME_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHAGNE_CHAR_STAT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CHAGNE_CHAR_STAT_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DELETE_WAIT_FRIEND_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FIRST_CASHBACK_EVENT_ENABLE_USER_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RAINBOW_WEEK_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RAINBOW_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_CHARACTER_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_DICE_ROLL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_BUY_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_CHEAT_DICE_ROLL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_CHEAT_TICKET_INIT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_RANK_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_RANK_LIST_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_WELCOME_USER_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_WELCOME_USER_EVENT_GET_REWARD_REQ);


	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_ROOM_READY_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_ROOM_READY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_GAME_EXIT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_AVOID_TEAM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_AVOID_TEAM_SAVE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_MATCHING_COMPLETED_RES);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_READY_TIME_CHECK_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_RANKMATCH_LYAER_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_RANKMATCH_LYAER_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_RANK_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_RANK_SEASON_RECORD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_GAMEEND_DETAIL_RECORD_NOT);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PCROOM_BENEFIT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CASHITEM_REWARD_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CASHITEM_REWARD_EVENT_OPEN_BOX_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GOTO_LOBBY_IN_CHANNEL_NOT);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_TRY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_INITIALIZE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALFPRICE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALFPRICE_ITEMLIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALFPRICE_USE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MOVE_POTENCARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MOVE_POTENCARD_CHARACTER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MOVE_POTENCARD_SELECT_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_RANDOM_NUMBER_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_PRESENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANDOMITEM_DRAW_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANDOMITEM_DRAW_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DISCOUNT_ITEM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DISCOUNT_ITEM_BUY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_TRANSFORM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_TRANSFORM_DISABLE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_TRANSFORM_STATUS_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_OPEN_BOX_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_OPEN_BOX_UPDATE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_NEWBIE_BENEFIT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_NEWBIE_BENEFIT_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_NEWBIE_BENEFIT_RECEIVING_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_UPDATE_COMMENT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_UPDATE_FRIEND_STATUS_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_REWARD_RECEIVING_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_MYMISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_ADD_FRIEND_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_FRIEND_INVITE_KING_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_INVITE_CHECK_INVITE_CODE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSION_MAKEITEM_PRODUCT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSION_MAKEITEM_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSION_MAKEITEM_MAKE_ITEM_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSIONCASHLIMIT_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MISSIONCASHLIMIT_GIVE_CASH_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DARKMARKET_PRODUCT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DARKMARKET_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_DARKMARKET_BUY_PRODUCT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLOSE_BUTTON_CLICK_NOTICE);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_MY_AVATAR_LIST_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MAIN_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_REQ);
	
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HELLONEWYEAR_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_HELLONEWYEAR_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_ADD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_ADD_RECOMMEND_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_DELETE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_MEMO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_RECOMMEND_OPT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_MY_RECOMMEND_FRIEND_DETAIL_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SERVER_TIME_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COLORINGPLAY_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COLORINGPLAY_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COLORPLAY_USE_PENCIL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COLORPLAY_GET_REWARD_REQ);	

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_RECOMMEND_ABILITY_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_UPDATE_REWARD_STATUS_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POTION_MAKING_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POTION_MAKING_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POTION_MAKING_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_OPEN_PRIMONBALL_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_GET_FREE_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_EXCHANGEITEM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_GET_PRODUCT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_CREATE_ROOM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_UPDATE_ROOM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_CHANGE_AI_POSITION_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_INGAME_SCORE_ADD_NOT);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_EAT_FOOD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_RANK_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_RANK_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_TEST_ADD_FOOD0_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_STEELBAG_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_STEELBAG_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_STEELBAG_STAMP_COUPON_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COMEBACK_BENEFIT_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COMEBACK_BENEFIT_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_COMEBACK_BENEFIT_RECEIVING_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_USE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_APPLY_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GIVE_MAGICBALL_STATUS_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GIVE_MAGICBALL_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_UNLIMITED_TATOO_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_UNLIMITED_TATOO_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_AIPVP_CHANGE_GAMEPLAYERNUM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOSEGAME_NOT_OPEN_TODAY_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOSEGAME_MOVE_PVEMODE_PLAY_NOT);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_MISSION_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_MISSION_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_DAILY_MISSION_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_PVE_RANK_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_PVE_RANK_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_RANK_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_RANK_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_FALL_DOWN_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_DROP_BALL_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_PVE_CHEAP_OUT_NOT);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_ITEM_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_ITEM_SELECT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_SHOP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_SHOP_BUY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_USE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PROMISE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PROMISE_RECEIVING_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PACKAGEITEM_PACK_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_USE_MAGIC_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_CHECK_LOGIN_TIME_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_USE_CHIP_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_ENTER_SPECIAL_PIECE_PAGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EXIT_SPECIAL_PIECE_PAGE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_BOX_PAGE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_BOX_KEY_AND_TINYPIECE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_MAKE_BOX_KEY_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_OPEN_BOX_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_PIECE_PAGE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(CS2_SP_SKILL_UP_PAGE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_SA_CREATE_ITEM_PAGE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_SA_ITEM_PAGE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_SPLIT_PIECE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(CS2_SP_SKILL_UP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(CS2_SP_MAKE_SA_CREATE_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(CS2_SP_MAKE_SA_ITEM_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_SKILL_LV_PAGE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SP_SELECT_SKILL_LV_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SPECIAL_PIECE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SPECIAL_PIECE_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PACKAGEITEM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CAMERA_ANGLE_SAVE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_BUTTON_TYPE_SAVE_NOT);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_MOTION_SLOT_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_LEAGUE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_RANKING_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_RANKING_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_RANKING_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_TOURNAMENT_RANKING_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_TOURNAMENT_RANKING_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_SHOP_BUY_CONDITION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_SHOP_USER_CASH_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_SHOP_BUY_ITEM_POPUP_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUBMOTION_SHOP_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLUBMOTION_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_NEW_CLUB_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_NEW_CLUB_INFO_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_COLLECT_LETTER_EVENT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_NAME_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_CLUB_NOTICE_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_WHITEDAY_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_WHITEDAY_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_USER_RATING_TOP_CHARACTER_LIST_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_EXP_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_EXP_ACCEPT_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_GACHA_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_RANK_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_RANK_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_AVATAR_FEATURE_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_USER_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DAILY_LOGIN_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DAILY_LOGIN_GET_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GRADE_REWARD_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_MISSION_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SECRET_SHOP_LIST_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SECRET_SHOP_BUY_ITEM_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SPEECH_BUBBLE_INFO_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(S2S_CHECK_FIRST_CONNECT_RES);
	DECLARE_GAMETHREAD_PROC_FUNC(S2S_CHECK_FIRST_CONNECT_REQ);

	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_PACKAGE_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_SLOT_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_REQ);
	DECLARE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ);
};

#endif // !defined(AFX_CFSTHREAD_H__010F05F3_55B1_416B_AB9D_AFA57A56C5F6__INCLUDED_)