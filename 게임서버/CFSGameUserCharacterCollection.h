qpragma once
qinclude "FSCommon.h"
qinclude "FSMatchCommon.h"

class CFSGameUser;
class CFSODBCBase;

typedef map<int/*iSpecialAvatarIndex*/, SCharacterCollectionUserBook*> CHARACTER_COLLECTION_USER_BOOK_MAP;
typedef list<SCharacterCollectionUserBook*> CHARACTER_COLLECTION_USER_BOOK_LIST;

typedef map<int/*iSentenceIndex*/, SCharacterCollectionUserSentence*> CHARACTER_COLLECTION_USER_SENTENCE_MAP;
typedef list<SCharacterCollectionUserSentence*> CHARACTER_COLLECTION_USER_SENTENCE_LIST;

class CGameUserCharacterCollection
{
public:
	CGameUserCharacterCollection(CFSGameUser* pUser);
	~CGameUserCharacterCollection(void);

	BOOL Load();
	void SendCharList(int iPageNum);
	void SendWearSentenceList();
	void SendSentenceList(int iPageNum);
	void SendHaveSentenceList();
	RESULT_CHARACTER_COLLECTION_WEAR SentenceWearReq(SC2S_CHARACTER_COLLECTION_WEAR_REQ rq, int& iReleaseSentenceIndex);
	RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR SentenceWearReq(SC2S_CHARACTER_COLLECTION_IN_ROOM_WEAR_REQ rq);
	void CheckLvUp(int iLv);
	void MakeDataForUserInfoCharacterCollection(PBYTE pBuffer, int& iSize);
	void GetSentenceStatus(int *iaStat);
	void GetSentenceStatus(float &fSentenceShootSuccessRate, float &fSentenceBlockSuccessRate);
	void GetSentenceStatus(SG2M_ADD_STAT_UPDATE& info);
	void GetSentenceStatus(SG2M_AVATAR_INFO_UPDATE& info);
	float GetSentenceRewardValue(int iRewardType);
	BOOL CheckSentenceStatus(float fSentenceShootSuccessRate, float fSentenceBlockSuccessRate);

protected:
	void RemoveAll();
	void AddUserAvatar(vector<SCharacterCollectionUserBook> vUserAvatar);
	void AddUserSentenceSlot(vector<SCharacterCollectionUserSentenceSlot> vUserSentenceSlot);
	void MakeBook();
	void MakeSentenceList();
	void GetWearRewardList(vector<SCharacterCollectionSentenceRewardConfig>& vReward);
	void CheckWearSlot();
	BYTE GetAllSentenceConditionStatus(int iSentenceIndex);
	BYTE GetSentenceConditionStatus(int iSpecialAvatarIndex, int iConditionLv);
	BOOL CHARACTER_COLLECTION_UpdateUserSentenceSlot(int iSlotIndex1, int iSentenceIndex1, int iSlotIndex2 = -1, int iSentenceIndex2 = -1);
	void GetSentenceCharConditionInfo(int iSpecialAvatarIndex, CHARACTER_COLLECTION_SENTENCE_CONDITION_INFO& sConditionInfo);

private:
	CUser*		m_pUser;
	BOOL		m_bDataLoaded;

	CHARACTER_COLLECTION_USER_BOOK_MAP			m_mapUserBook;
	CHARACTER_COLLECTION_USER_BOOK_LIST			m_listUserBook;

	CHARACTER_COLLECTION_USER_SENTENCE_MAP		m_mapUserSentence;
	CHARACTER_COLLECTION_USER_SENTENCE_LIST		m_listUserSentence;

	int	m_iaSentenceSlot[MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT];
};

