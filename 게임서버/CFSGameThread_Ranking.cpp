qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSRankManager.h"
qinclude "CFSGameClient.h"
qinclude "ClubConfigManager.h"
qinclude "CUserAppraisalManager.h"
qinclude "PVEManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CFSGameThread::Process_FSEnterRankingPage(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	if(FALSE == pFSClient->SetState(FS_RANKING))
	{
		return;
	}

	BYTE btAvatarLvGrade = pUser->GetCurUsedAvtarLvGrade();
	int iYear = 0, iMonth = 0;
	pUser->GetSeasonInfo(iYear, iMonth);

	CPacketComposer PacketComposer(S2C_ENTER_RANKING_RES);
	PacketComposer.Add(btAvatarLvGrade);
	PacketComposer.Add(iYear);
	PacketComposer.Add(iMonth);
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSTotalRankingList(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iRecordType = 0, iRankType = 0, iCategory = 0, iPage = 0;
	BYTE btAvatarLvGrade = 0, btSeasonType = 0;

	m_ReceivePacketBuffer.Read(&btAvatarLvGrade);
	m_ReceivePacketBuffer.Read(&btSeasonType);
	m_ReceivePacketBuffer.Read(&iRecordType);
	m_ReceivePacketBuffer.Read(&iRankType);
	m_ReceivePacketBuffer.Read(&iCategory);
	m_ReceivePacketBuffer.Read(&iPage);

	int iSeasonIndex = GetCurrentSeasonIndex();
	if(SEASON_TYPE_LAST == btSeasonType)
		DecreaseSeasonIndex(iSeasonIndex);

	if(0 <= iPage && (BYTE)iRecordType < MAX_RECORD_TYPE)
	{
		int iStart = 0, iEnd = 0;

		if(TRUE == RANK.GetRankPageIdx(btAvatarLvGrade, btSeasonType, (BYTE)iRecordType, iRankType, iCategory, iPage, iStart, iEnd))
		{
			int iListNum = iEnd - iStart + 1;

			if(0 < iListNum)
			{
				int iRankListCount = RANK.GetTopRankCount(btAvatarLvGrade, btSeasonType, (BYTE)iRecordType, iRankType, iCategory);

				CPacketComposer PacketComposer(S2C_TOTAL_RANKING_RES);
				PacketComposer.Add(iRecordType);
				PacketComposer.Add(iRankType);
				PacketComposer.Add(iCategory);
				PacketComposer.Add(iPage);
				PacketComposer.Add(iRankListCount);
				PacketComposer.Add(iListNum);

				SRankInfo UserRank;
				memset(&UserRank, 0, sizeof(SRankInfo));

				for(int i=iStart; i<=iEnd; i++)
				{
					RANK.GetRankPageRow(btAvatarLvGrade, btSeasonType, (BYTE)iRecordType, iRankType, iCategory, i, UserRank);
					PacketComposer.Add(UserRank.iRank);
					PacketComposer.Add((BYTE*)UserRank.szGameID, sizeof(char)*(MAX_GAMEID_LENGTH+1));	

					if(SEASON_RANK_USER_RATING_GRADE == iCategory)
						PacketComposer.Add(UserRank.iUserIDIndex);
					else
						PacketComposer.Add(UserRank.iGameIDIndex);

					PacketComposer.Add(UserRank.iRecordValue);
				}

				if(0 == iPage) // 첫페이지이면 내 랭킹을 보낸다
				{
					int iRank = -1, iGameIDIndex = 0, iValue = 0;

					iGameIDIndex = pUser->GetGameIDIndex();
					char szGameID[MAX_GAMEID_LENGTH+1];
					strncpy_s(szGameID, _countof(szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);

					SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
					CHECK_NULL_POINTER_VOID(pAvatarInfo);
					SAvatarSeasonInfo* pAvatarSeasonInfo = pUser->GetAvatarSeasonInfo();

					if( iRankType == RANK_TYPE_SEASON )
					{
						if( iCategory < 0 || iCategory >= MAX_SEASON_RANK_NUM )
						{
							WRITE_LOG_NEW(LOG_TYPE_RANK, INVALED_DATA, CHECK_FAIL, "Process_FSTotalRankingList - Category");
							return;
						}

						AvatarSeasonInfoMap::iterator pos = pAvatarSeasonInfo->mapSeasonInfo[iRecordType].find(iSeasonIndex);
						if(pos != pAvatarSeasonInfo->mapSeasonInfo[iRecordType].end())
						{
							SAvatarSeasonInfoEntry* pEntry = &pos->second;
							iRank	= pEntry->Rank[iCategory].iRank;
							iValue	= pEntry->Rank[iCategory].iRecordValue;

							if(iRank > 0)
							{
								iGameIDIndex = pEntry->Rank[iCategory].iGameIDIndex;
								strncpy_s(szGameID, _countof(szGameID), pEntry->Rank[iCategory].szGameID, MAX_GAMEID_LENGTH);
							}	
						}

						if(SEASON_RANK_USER_RATING_GRADE == iCategory)
						{
							iGameIDIndex = pUser->GetUserIDIndex();
							pUser->GetRepresentGameID(szGameID);
						}
					}
					else if( iRankType == RANK_TYPE_TOTAL )
					{
						if( iCategory < 0 || iCategory >= MAX_TOTAL_RANK_NUM )
						{
							WRITE_LOG_NEW(LOG_TYPE_RANK, INVALED_DATA, CHECK_FAIL, "Process_FSTotalRankingList - Category");
							return;
						}

						iRank	= pAvatarInfo->TotalInfo[iRecordType].Rank[iCategory].iRank;
						iValue	= pAvatarInfo->TotalInfo[iRecordType].Rank[iCategory].iRecordValue;
					}

					PacketComposer.Add(iRank);
					PacketComposer.Add((BYTE*)szGameID, sizeof(char)*(MAX_GAMEID_LENGTH+1));

					if(SEASON_RANK_USER_RATING_GRADE == iCategory)
						PacketComposer.Add(pUser->GetUserIDIndex());
					else
						PacketComposer.Add(iGameIDIndex);

					PacketComposer.Add(iValue);
				}

				pFSClient->Send(&PacketComposer);		
			}
		}
		else
		{
			CPacketComposer PacketComposer(S2C_TOTAL_RANKING_RES);
			PacketComposer.Add(iRecordType);
			PacketComposer.Add(iRankType);
			PacketComposer.Add(iCategory);
			PacketComposer.Add(iPage);
			PacketComposer.Add(0);
			PacketComposer.Add(0);

			if(0 == iPage) // 첫페이지이면 내 랭킹을 보낸다
			{
				int iRank = -1, iValue = 0;

				CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
				pUser->GetAvatarRankInfo(iRecordType, iRankType, iCategory, iRank, iValue);

				PacketComposer.Add(iRank);
				PacketComposer.Add((BYTE*)pUser->GetGameID(), sizeof(char)*(MAX_GAMEID_LENGTH+1));

				if(SEASON_RANK_USER_RATING_GRADE == iCategory)
					PacketComposer.Add(pUser->GetUserIDIndex());
				else
					PacketComposer.Add(pUser->GetGameIDIndex());

				PacketComposer.Add(iValue);
			}

			pFSClient->Send(&PacketComposer);	
		}
	}	
}

void CFSGameThread::Process_FSUserRanking(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSAvatarFeature* pAvatarFeature = RANK.GetAvatarFeature();
	CHECK_NULL_POINTER_VOID(pAvatarFeature);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	CFSODBCBase * pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_VOID( pODBC );

	int iRecordType = 0, iGameIDIndex = 0, iCategory = 0;
	BYTE btAvatarLvGrade = 0, btSeasonType = 0;

	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);
	m_ReceivePacketBuffer.Read( &btAvatarLvGrade );
	m_ReceivePacketBuffer.Read( &btSeasonType );
	m_ReceivePacketBuffer.Read(&iRecordType);
	m_ReceivePacketBuffer.Read(&iGameIDIndex);	
	m_ReceivePacketBuffer.Read(&iCategory);

	if(SEASON_RANK_USER_RATING_GRADE == iCategory)
	{
		int iUserIDIndex = iGameIDIndex;
		iGameIDIndex = 0;
		iGameIDIndex = RANK.GetTopRatingUserGameIDIndex(btSeasonType, iUserIDIndex);

		if(iUserIDIndex == pUser->GetUserIDIndex() &&
			0 >= iGameIDIndex)
		{
			iGameIDIndex = pUser->GetRepresentGameIDIndex();
		}
	}

	SAvatarFeatureInRank AvatarFeatureInRank;
	if(TRUE == pAvatarFeature->GetFeature(iGameIDIndex, AvatarFeatureInRank))
	{
		int iSuccess = 0;

		CPacketComposer PacketComposer(S2C_USER_RANKING_RES);
		PacketComposer.Add(iSuccess);

		if(iGameIDIndex == pUser->GetGameIDIndex())
			PacketComposer.Add((BYTE*)pUser->GetGameID(), sizeof(char)*(MAX_GAMEID_LENGTH+1));
		else
			PacketComposer.Add((BYTE*)AvatarFeatureInRank.szGameID, sizeof(char)*(MAX_GAMEID_LENGTH+1));

		PacketComposer.Add(AvatarFeatureInRank.iCharacterType);
		PacketComposer.Add(AvatarFeatureInRank.iFace);
		PacketComposer.Add(AvatarFeatureInRank.iPosition);
		PacketComposer.Add(AvatarFeatureInRank.iAchievementIndex);
		PacketComposer.Add(AvatarFeatureInRank.iFameLevel);
		PacketComposer.Add((int)-1); //통합 삭제대기 vip
		PacketComposer.Add((int)-1); //통합 삭제대기 결혼

		PBYTE pFeatureCountLocation = PacketComposer.GetTail();
		int iCount = 0;
		PacketComposer.Add(iCount);

		BYTE btIsWearClubUniform = 0;
		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != AvatarFeatureInRank.iaFeatureItemIndex[i]) 
			{
				++iCount;
				PacketComposer.Add(AvatarFeatureInRank.iaFeatureItemIndex[i]);

				if(TRUE == CLUBCONFIGMANAGER.IsClubUniform(AvatarFeatureInRank.iaFeatureItemIndex[i]))
					btIsWearClubUniform = 1;
			}
		}
		memcpy(pFeatureCountLocation, &iCount, sizeof(int));

		PacketComposer.Add(AvatarFeatureInRank.iClubMarkCode);
		PacketComposer.Add(AvatarFeatureInRank.btClubPosition);
		PacketComposer.Add(btIsWearClubUniform);
		PacketComposer.Add(AvatarFeatureInRank.btUnifromNum);

		int RankInfoSeason[MAX_SEASON_RANK_NUM];
		int RankInfoTotal[MAX_TOTAL_RANK_NUM];

		memset(RankInfoSeason, -1, sizeof(int)*MAX_SEASON_RANK_NUM);
		memset(RankInfoTotal, -1, sizeof(int)*MAX_TOTAL_RANK_NUM );

		RANK.GetAvatarTopRanking(btAvatarLvGrade, btSeasonType, (BYTE)iRecordType, RANK_TYPE_SEASON, AvatarFeatureInRank.iGameIDIndex, RankInfoSeason);
		RANK.GetAvatarTopRanking(btAvatarLvGrade, btSeasonType, (BYTE)iRecordType, RANK_TYPE_TOTAL, AvatarFeatureInRank.iGameIDIndex, RankInfoTotal);

		BYTE btSeasonCategorySize = 0;
		BYTE btTotalCategorySize = 0;
		PBYTE pSeasonCategoryList = NULL;
		PBYTE pTotalCategoryList = NULL;
		switch (iRecordType)
		{
		case RECORD_TYPE_HALFCOURT:
		case RECORD_TYPE_FULLCOURT:
			pSeasonCategoryList = SEASON_RANK_CATEGORY_NORMAL;
			pTotalCategoryList = TOTAL_RANK_CATEGORY_NORMAL;
			btSeasonCategorySize = sizeof(SEASON_RANK_CATEGORY_NORMAL);
			btTotalCategorySize = sizeof(TOTAL_RANK_CATEGORY_NORMAL);
			break;
		case RECORD_TYPE_TOURNAMENT:
			pSeasonCategoryList = SEASON_RANK_CATEGORY_TOURNAMENT;
			pTotalCategoryList = TOTAL_RANK_CATEGORY_TOURNAMENT;
			btSeasonCategorySize = sizeof(SEASON_RANK_CATEGORY_TOURNAMENT);
			btTotalCategorySize = sizeof(TOTAL_RANK_CATEGORY_TOURNAMENT);
			break;
		}

		PacketComposer.Add(btSeasonCategorySize);
		for(BYTE i=0;i<btSeasonCategorySize ; i++)
		{
			PacketComposer.Add(pSeasonCategoryList[i]);
			PacketComposer.Add(RankInfoSeason[pSeasonCategoryList[i]]);
		}

		PacketComposer.Add(btTotalCategorySize);
		for(BYTE i=0;i<btTotalCategorySize ; i++)
		{
			PacketComposer.Add(pTotalCategoryList[i]);
			PacketComposer.Add(RankInfoTotal[pTotalCategoryList[i]]);
		}

		PacketComposer.Add(AvatarFeatureInRank.iSkillLvCode);

		pFSClient->Send(&PacketComposer);		
	}
}

void CFSGameThread::Process_FSExitRankingPage(CFSGameClient* pFSClient)
{
	int iSuccess = 0;

	if(FALSE == pFSClient->SetState(NEXUS_LOBBY))
	{
		iSuccess = -1;
	}

	CPacketComposer PacketComposer(S2C_EXIT_RANKING_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_RankSearchReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btAvatarLvGrade, btSeasonType;
	int iRecordType = 0, iRankType = 0, iCategory = 0, iPage = 0;
	DECLARE_INIT_TCHAR_ARRAY(szSearchGameID, MAX_GAMEID_LENGTH+1);
	int RankInfo[MAX_SEASON_RANK_NUM] = {0};

	m_ReceivePacketBuffer.Read(&btAvatarLvGrade);
	m_ReceivePacketBuffer.Read(&btSeasonType);
	m_ReceivePacketBuffer.Read((BYTE*)szSearchGameID, MAX_GAMEID_LENGTH+1);	
	szSearchGameID[MAX_GAMEID_LENGTH] = 0;

	m_ReceivePacketBuffer.Read(&iRecordType);
	m_ReceivePacketBuffer.Read(&iRankType);
	m_ReceivePacketBuffer.Read(&iCategory);
	m_ReceivePacketBuffer.Read(&iPage);

	CHECK_CONDITION_RETURN_VOID(0 > iPage);
	CHECK_CONDITION_RETURN_VOID(MAX_RECORD_TYPE <= (BYTE)iRecordType);

	CPacketComposer PacketComposer(S2C_TOTAL_RANKING_RES);
	PacketComposer.Add(iRecordType);
	PacketComposer.Add(iRankType);
	PacketComposer.Add(iCategory);
	PacketComposer.Add(iPage);

	if (FALSE == RANK.MakePacketRankPageRow(PacketComposer, btAvatarLvGrade, btSeasonType, (BYTE)iRecordType, iRankType, iCategory, iPage, szSearchGameID))
		return;

	if(0 == iPage) // 첫페이지이면 내 랭킹을 보낸다
	{
		int iRank = -1, iValue = 0;

		CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
		pUser->GetAvatarRankInfo(iRecordType, iRankType, iCategory, iRank, iValue);

		PacketComposer.Add(iRank);
		PacketComposer.Add((BYTE*)pUser->GetGameID(), sizeof(char)*(MAX_GAMEID_LENGTH+1));

		if(SEASON_RANK_USER_RATING_GRADE == iCategory)
			PacketComposer.Add(pUser->GetUserIDIndex());
		else
			PacketComposer.Add(pUser->GetGameIDIndex());

		PacketComposer.Add(iValue);
	}

	pFSClient->Send(&PacketComposer);		
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_APPRAISAL_RANK_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_APPRAISAL_RANK_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_USER_APPRAISAL_RANK_LIST_REQ)); 

	USERAPPRAISAL.SendRankList(rq, pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_APPRAISAL_USER_RANK_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_APPRAISAL_USER_RANK_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_USER_APPRAISAL_USER_RANK_INFO_REQ)); 

	USERAPPRAISAL.SendUserRankInfo(rq, pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_PVE_RANK_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_ENTER_PVE_RANK_RES rs;
	rs.btResult = RESULT_ENTER_PVE_RANK_SUCCESS;
	if(FALSE == pFSClient->SetState(FS_PVE_RANK))
		rs.btResult = RESULT_ENTER_PVE_RANK_FAIL;

	pUser->Send(S2C_ENTER_PVE_RANK_RES, &rs, sizeof(SS2C_ENTER_PVE_RANK_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_PVE_RANK_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EXIT_PVE_RANK_RES rs;
	rs.btResult = RESULT_EXIT_PVE_RANK_SUCCESS;
	if(FALSE == pFSClient->SetState(NEXUS_LOBBY))
		rs.btResult = RESULT_EXIT_PVE_RANK_FAIL;

	pUser->Send(S2C_EXIT_PVE_RANK_RES, &rs, sizeof(SS2C_EXIT_PVE_RANK_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_RANK_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_RANK_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_RANK_LIST_REQ)); 

	pUser->GetUserPVEMission()->SendRankList(rq.btPVEType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_RANK_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_RANK_USER_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_RANK_USER_INFO_REQ));

	SS2C_PVE_RANK_USER_INFO_RES rs;
	rs.iCharacterType = -1;
	ZeroMemory(rs.sLastMission, sizeof(SPVE_LAST_COMPLETED_MISSION)*MAX_LAST_COMPLETED_MISSION_COUNT);

	if(pUser->GetUserIDIndex() == rq.iUserIDIndex)
		pUser->GetUserPVEMission()->MakeLastMissionList(rq.btPVEType, rs);

	PVEMANAGER.SendUserRank((CUser*)pUser, rq.btPVEType, rq.iUserIDIndex, pUser->GetUserPVEMission()->GetRankGameID(), rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_RATING_TOP_CHARACTER_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_RATING_TOP_CHARACTER_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_USER_RATING_TOP_CHARACTER_LIST_REQ));

	SS2C_USER_RATING_TOP_CHARACTER_LIST_RES rs;
	ZeroMemory(&rs, sizeof(SS2C_USER_RATING_TOP_CHARACTER_LIST_RES));

	if(rq.iUserIDIndex == pUser->GetUserIDIndex() &&
		0 == rq.btSeasonType)
	{
		pUser->MakeTopRatingCharacterList(rs);
	}
	else
	{
		CHECK_CONDITION_RETURN_VOID(FALSE == RANK.CheckTopRatingUser(rq.btSeasonType, rq.iUserIDIndex));

		RANK.MakeTopRatingCharacterList(rq.btSeasonType, rq.iUserIDIndex, rs);
	}

	pUser->Send(S2C_USER_RATING_TOP_CHARACTER_LIST_RES, &rs, sizeof(SS2C_USER_RATING_TOP_CHARACTER_LIST_RES));
}
