qinclude "stdafx.h"
qinclude "FSGameUserColoringPlayEvent.h"
qinclude "CFSGameUser.h"
qinclude "ColoringPlayEventManager.h"
qinclude "RewardManager.h"

CFSGameUserColoringPlayEvent::CFSGameUserColoringPlayEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_tLastResetTime(-1)
	, m_tStartConnectTime(-1)
	, m_iConnectSecond(0)
	, m_iPencilSlot(0)
	, m_btTodayUsePencil(0)
	, m_btMainStatus(ColoringPlayEvent::COLOR_PLAY_STATUS_GOING)
{
	ZeroMemory(m_iColorFlag, sizeof(int) * ColoringPlayEvent::PICTURE_CNT);
	ZeroMemory(m_btStatus, sizeof(BYTE) * ColoringPlayEvent::PICTURE_CNT);
}

CFSGameUserColoringPlayEvent::~CFSGameUserColoringPlayEvent(void)
{
}

BOOL			CFSGameUserColoringPlayEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == COLORINGPLAY.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_COLORINGPLAY_GetUserData(m_pUser->GetUserIDIndex(), m_tLastResetTime, m_iConnectSecond, m_iPencilSlot, m_btTodayUsePencil, m_btMainStatus, m_iColorFlag, m_btStatus))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_COLORINGPLAY_GetUserData failed");
		return FALSE;	
	}

	m_bDataLoaded = TRUE;
	m_tStartConnectTime = _time64(NULL);

	CheckResetDate(m_tStartConnectTime, TRUE);

	return TRUE;
}

BOOL			CFSGameUserColoringPlayEvent::CheckResetDate(time_t tCurrentTime/* = _time64(NULL)*/, BOOL bIsLogin/* = FALSE*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	BOOL	bResult = FALSE;

	if(-1 == m_tLastResetTime)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tLastResetTime, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= COLORINGPLAY_EVENT_RESET_TIME;

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tLastResetTime <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_COLORINGPLAY_ResetUserData(m_pUser->GetUserIDIndex(), tCurrentTime))
		{			
			m_iConnectSecond = 0;
			m_tLastResetTime = tCurrentTime;
			m_tStartConnectTime = tCurrentTime; 

			m_iPencilSlot = 0;
			m_btTodayUsePencil = 0;

			if(FALSE == bIsLogin)	// 접속하고 있다가 리셋이 된 경우, 접속 시작시각을  그 날 07시부터로 설정.
			{
				TIMESTAMP_STRUCT	RecentResetHourDate;
				TimetToTimeStruct(m_tLastResetTime, RecentResetHourDate);
				RecentResetHourDate.hour = COLORINGPLAY_EVENT_RESET_TIME;
				RecentResetHourDate.minute = 0;
				RecentResetHourDate.second = 0;

				m_tStartConnectTime = TimeStructToTimet(RecentResetHourDate);
			}
		}
	}

	return bResult;
}

void			CFSGameUserColoringPlayEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == COLORINGPLAY.IsOpen());

	CPacketComposer* pPacket = COLORINGPLAY.GetPacketEventInfo();
	m_pUser->Send(pPacket);
}

void			CFSGameUserColoringPlayEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == COLORINGPLAY.IsOpen());

	int iRewardCnt = 0;
	int iClearValue = 0;

	CHECK_CONDITION_RETURN_VOID(FALSE == COLORINGPLAY.GetMissionInfo(ColoringPlayEvent::MISSION_CONDITION_TYPE_CONNECT_TIME_MIN, iClearValue, iRewardCnt));

	time_t tCurrentTime = _time64(NULL);
	CheckAndUpdateMission(ColoringPlayEvent::MISSION_CONDITION_TYPE_CONNECT_TIME_MIN, tCurrentTime);
	
	SS2C_COLORINGPLAY_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_COLORINGPLAY_USER_INFO_RES));

	for(int i = 0; i < ColoringPlayEvent::PICTURE_CNT; ++i)
	{
		ss.btStatus[i] = m_btStatus[i];

		for(int j = 0; 0 < m_iColorFlag[i] && j < ColoringPlayEvent::COLOR_CNT; ++j)
		{
			ss.btUserColor[i][j] = (m_iColorFlag[i] & static_cast<int>(pow(2.f, j))) > 0 ? COLOR_USE : COLOR_USE_NOT;
		}
	}
	ss.btStatus[ColoringPlayEvent::REWARD_CNT - 1] = m_btMainStatus;

	for(int i = m_btTodayUsePencil, iPencilSlot = m_iPencilSlot; i < ColoringPlayEvent::PENCIL_CNT, iPencilSlot >= 0; ++i, iPencilSlot -= ColoringPlayEvent::PENCIL_SLOT_CNT)
	{
		ss.btPencil[i] = (iPencilSlot / ColoringPlayEvent::PENCIL_SLOT_CNT > 0) ? ColoringPlayEvent::PENCIL_SLOT_CNT : iPencilSlot oloringPlayEvent::PENCIL_SLOT_CNT;
;
	}

	ss.btTodayUsePencil = m_btTodayUsePencil;

	if(IsFullPencilSlot())
	{
		ss.iRemainSec = -1;
	}
	else
	{
		ss.iRemainSec = (iClearValue * 60) - static_cast<int>(tCurrentTime - m_tStartConnectTime + m_iConnectSecond);

		if(ss.iRemainSec < 0)
			ss.iRemainSec = 0;
	}

	m_pUser->Send(S2C_COLORINGPLAY_USER_INFO_RES, &ss, sizeof(SS2C_COLORINGPLAY_USER_INFO_RES));
}

BOOL			CFSGameUserColoringPlayEvent::IsFullPencilSlot()
{
	return m_iPencilSlot >= (ColoringPlayEvent::PENCIL_CNT - m_btTodayUsePencil) * ColoringPlayEvent::PENCIL_SLOT_CNT;
}

BOOL			CFSGameUserColoringPlayEvent::CheckAndUpdateMission(BYTE btMissionConditionType, time_t tCurrentTime/* = _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == COLORINGPLAY.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(FALSE == CheckResetDate(tCurrentTime) && TRUE == IsFullPencilSlot(), FALSE);

	int iRewardCnt = 0;
	int iClearValue = 0;
	int iConnectSecond = 0;
	int iClearCnt = 0;
	time_t tStartConnectTime = m_tStartConnectTime;

	CHECK_CONDITION_RETURN(FALSE == COLORINGPLAY.GetMissionInfo(btMissionConditionType, iClearValue, iRewardCnt), FALSE);
	CHECK_CONDITION_RETURN(0 == iRewardCnt || 0 == iClearValue, FALSE);

	switch(btMissionConditionType)
	{
	case ColoringPlayEvent::MISSION_CONDITION_TYPE_PLAY_CNT:
		{
			iClearCnt = 1;
			iConnectSecond = m_iConnectSecond;
		}
		break;

	case ColoringPlayEvent::MISSION_CONDITION_TYPE_PLAY_WIN:
		{
			iClearCnt = 2;
			iConnectSecond = m_iConnectSecond;
		}
		break;

	case ColoringPlayEvent::MISSION_CONDITION_TYPE_CONNECT_TIME_MIN:
		{
			iClearCnt = ((tCurrentTime - m_tStartConnectTime + m_iConnectSecond) / 60) / iClearValue;

			CHECK_CONDITION_RETURN(iClearCnt <= 0, FALSE);

			iConnectSecond = (tCurrentTime - m_tStartConnectTime + m_iConnectSecond) - (iClearCnt * 60 * iClearValue);
			tStartConnectTime = tCurrentTime;
		}
		break;

	default:
		return FALSE;
	}

	int iAddPencilCnt = iRewardCnt * iClearCnt;
	if(m_iPencilSlot + iAddPencilCnt >= (ColoringPlayEvent::PENCIL_CNT - m_btTodayUsePencil) * ColoringPlayEvent::PENCIL_SLOT_CNT)
	{
		iAddPencilCnt = ((ColoringPlayEvent::PENCIL_CNT - m_btTodayUsePencil) * ColoringPlayEvent::PENCIL_SLOT_CNT) - m_iPencilSlot;
	}

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_COLORINGPLAY_GivePencil(m_pUser->GetUserIDIndex(), iAddPencilCnt), FALSE);

	m_iPencilSlot = m_iPencilSlot + iAddPencilCnt;
	m_iConnectSecond = iConnectSecond;
	m_tStartConnectTime = tStartConnectTime;

	return TRUE;
}

BOOL			CFSGameUserColoringPlayEvent::CheckGamePlayMission(BOOL bIsWin, BOOL bIsDisconnectedMatch, BOOL bRunAway)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(TRUE == bIsDisconnectedMatch || TRUE == bRunAway, FALSE);

	BYTE btMissionConditionType = bIsWin ? ColoringPlayEvent::MISSION_CONDITION_TYPE_PLAY_WIN : ColoringPlayEvent::MISSION_CONDITION_TYPE_PLAY_CNT;

	return CheckAndUpdateMission(btMissionConditionType);
}

BOOL			CFSGameUserColoringPlayEvent::SaveStartConnectTime()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == COLORINGPLAY.IsOpen(), FALSE);

	time_t tCurrentTime = _time64(NULL);
	CHECK_CONDITION_RETURN(TRUE == CheckAndUpdateMission(ColoringPlayEvent::MISSION_CONDITION_TYPE_CONNECT_TIME_MIN, tCurrentTime), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_COLORINGPLAY_SaveStartConnectTime(m_pUser->GetUserIDIndex(), tCurrentTime - m_tStartConnectTime + m_iConnectSecond), FALSE);

	return TRUE;
}

BOOL			CFSGameUserColoringPlayEvent::UsePencil(BYTE btPictureIndex, BYTE btColorIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	CheckResetDate();

	SS2C_COLORPLAY_USE_PENCIL_RES	ss;
	ss.btPictureIndex = btPictureIndex;
	ss.btColorIndex = btColorIndex;

	int iAddColorFlag = pow(2.f, btColorIndex);

	if(CLOSED == COLORINGPLAY.IsOpen())
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL_EVENT_CLOSED;
	}
	else if(btPictureIndex >= ColoringPlayEvent::PICTURE_CNT || btColorIndex >= ColoringPlayEvent::COLOR_CNT)
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL_WRONG_INFO;
	}
	else if(m_iPencilSlot < ColoringPlayEvent::PENCIL_SLOT_CNT)
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL_LACK_PENCIL_CNT;
	}
	else if(m_btTodayUsePencil >= ColoringPlayEvent::PENCIL_CNT)
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL_OVER_USE_TODAY;
	}
	else if((m_iColorFlag[btPictureIndex] & iAddColorFlag) > 0)
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL_ALREADY_CLEAR;
	}
	else if(COLOR_PLAY_STATUS_GOING != m_btStatus[btPictureIndex])
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL_ALREADY_CLEAR;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL_FULL_MAILBOX;
	}
	else
	{
		ss.btResult = RESULT_USE_PENCIL_FAIL;		

		BOOL bUseColor = COLORINGPLAY.CheckAnswerColor(btPictureIndex, iAddColorFlag);
		BYTE btStatus = (TRUE == bUseColor && COLORINGPLAY.CheckClearPicture(btPictureIndex, m_iColorFlag[btPictureIndex] | iAddColorFlag)) ? COLOR_PLAY_STATUS_CLEAR : COLOR_PLAY_STATUS_GOING;
		BYTE btMainStatus = (COLOR_PLAY_STATUS_CLEAR == btStatus && TRUE == CheckMainStatus(btPictureIndex)) ? COLOR_PLAY_STATUS_CLEAR : COLOR_PLAY_STATUS_GOING;

		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_COLORINGPLAY_UsePencil(m_pUser->GetUserIDIndex(), ss.btPictureIndex, ss.btColorIndex, iAddColorFlag, btStatus, btMainStatus))
		{	
			if(TRUE == bUseColor)
			{
				ss.btResult = RESULT_USE_PENCIL_SUCCESS;
			}
			else
			{
				SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, COLORINGPLAY.GetFailRewardIndex());
				if(NULL != pReward)
				{
					ss.btResult = RESULT_USE_PENCIL_SUCCESS_WRONG_COLOR;					
				}
			}
			
			m_iPencilSlot -= ColoringPlayEvent::PENCIL_SLOT_CNT;
			m_btTodayUsePencil ++;
			m_iColorFlag[btPictureIndex] |= iAddColorFlag;
			m_btStatus[btPictureIndex] = btStatus;
			m_btMainStatus = btMainStatus;
		}
	}

	m_pUser->Send(S2C_COLORPLAY_USE_PENCIL_RES, &ss, sizeof(SS2C_COLORPLAY_USE_PENCIL_RES));

	return TRUE;
}

BOOL			CFSGameUserColoringPlayEvent::GetReward(BYTE btProductIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	SS2C_COLORPLAY_GET_REWARD_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_COLORPLAY_GET_REWARD_RES));

	if(CLOSED == COLORINGPLAY.IsOpen())
	{
		ss.btResult = RESULT_REWARD_REQ_FAIL_EVENT_CLOSED;
	}
	else if(btProductIndex >= ColoringPlayEvent::REWARD_CNT)
	{
		ss.btResult = RESULT_REWARD_REQ_FAIL_CONDITION;
	}
	else if(btProductIndex == ColoringPlayEvent::REWARD_CNT - 1 && COLOR_PLAY_STATUS_CLEAR != m_btMainStatus)
	{
		ss.btResult = RESULT_REWARD_REQ_FAIL_CONDITION;
	}
	else if(btProductIndex < ColoringPlayEvent::PICTURE_CNT && COLOR_PLAY_STATUS_CLEAR != m_btStatus[btProductIndex])
	{
		ss.btResult = RESULT_REWARD_REQ_FAIL_CONDITION;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_REWARD_REQ_FAIL_FULL_MAILBOX;
	}
	else
	{
		ss.btResult = RESULT_REWARD_REQ_FAIL;		

		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		BOOL bIsMainReward = (btProductIndex == ColoringPlayEvent::REWARD_CNT - 1);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_COLORINGPLAY_GiveReward(m_pUser->GetUserIDIndex(), btProductIndex, bIsMainReward))
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, COLORINGPLAY.GetRewardIndex(btProductIndex));
			if(NULL != pReward)
			{
				ss.btResult = RESULT_REWARD_REQ_SUCCESS;

				if(bIsMainReward)
					m_btMainStatus = COLOR_PLAY_STATUS_GET_REWARD;
				else
					m_btStatus[btProductIndex] = COLOR_PLAY_STATUS_GET_REWARD;
			}			
		}
	}

	m_pUser->Send(S2C_COLORPLAY_GET_REWARD_RES, &ss, sizeof(SS2C_COLORPLAY_GET_REWARD_RES));

	return TRUE;
}

BOOL			CFSGameUserColoringPlayEvent::CheckMainStatus(BYTE btPictureIndex)
{
	for(int i = 0; i < ColoringPlayEvent::PICTURE_CNT; ++i)
	{
		if(btPictureIndex == i)
			continue;

		if(COLOR_PLAY_STATUS_GOING == m_btStatus[i])
			return FALSE;
	}

	return TRUE;
}
