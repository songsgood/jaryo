qinclude "stdafx.h"
qinclude "CFSGameThread.h"

qinclude "CFSGameServer.h"

qinclude "MatchSvrProxy.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
