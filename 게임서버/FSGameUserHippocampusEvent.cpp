qinclude "stdafx.h"
qinclude "FSGameUserHippocampusEvent.h"
qinclude "HippocampusEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "EventDateManager.h"

CFSGameUserHippocampusEvent::CFSGameUserHippocampusEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
}

CFSGameUserHippocampusEvent::~CFSGameUserHippocampusEvent(void)
{
}

BOOL CFSGameUserHippocampusEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == HIPPOCAMPUS.IsOpen() && CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_HIPPOCAMPUS_RANK), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	SODBCEventHippocampusUserInfo sUserInfo;
	vector<SODBCEventHippocampusUserFood> vFood;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_GetUserData(m_pUser->GetUserIDIndex(), sUserInfo) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_GetUserFoodData(m_pUser->GetUserIDIndex(), vFood) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_GetUserLog(m_pUser->GetUserIDIndex(), m_UserLog._btGrade, m_UserLog._iLv, m_UserLog._iExp, m_UserLog._iDieCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_HIPPOCAMPUS_EVENT, INITIALIZE_DATA, FAIL, "EVENT_HIPPOCAMPUS_GetUserData error");
		return FALSE;
	}

	m_pUser->LoadRepresentInfo();

	m_UserInfo.SetUserInfo(sUserInfo);
	SetUserFood(vFood);

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserHippocampusEvent::SetUserFood(vector<SODBCEventHippocampusUserFood>& vFood)
{
	for(int i = 0; i < vFood.size(); ++i)
	{
		SEventHippocampusUserFood Food;
		Food.SetUserFood(vFood[i]._btFoodType, vFood[i]._iCount);
		m_mapUserFood[Food._btFoodType] = Food;
	}
}

void CFSGameUserHippocampusEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == HIPPOCAMPUS.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_EVENT_HIPPOCAMPUS_INFO_RES rs;
	rs.btGrade = m_UserInfo._btGrade;
	rs.iLv = m_UserInfo._iLv;
	rs.iExp = m_UserInfo._iExp;
	rs.iKeepCount = m_UserInfo._iKeepCount;

	int iFoodCount[MAX_EVENT_HIPPOCAMPUS_FOOD_TYPE_COUNT];
	ZeroMemory(iFoodCount, sizeof(int)*MAX_EVENT_HIPPOCAMPUS_FOOD_TYPE_COUNT);

	for(BYTE btType = 0; btType < MAX_EVENT_HIPPOCAMPUS_FOOD_TYPE_COUNT; ++btType)
	{
		HIPPOCAMPUS_USER_FOOD_MAP::iterator iter = m_mapUserFood.find(btType);
		if(iter != m_mapUserFood.end())
			iFoodCount[btType] = iter->second._iCount;
	}

	CPacketComposer Packet(S2C_EVENT_HIPPOCAMPUS_INFO_RES);
	HIPPOCAMPUS.MakeEventInfo(Packet, rs, m_UserInfo._btEnableBonusExpType, iFoodCount);

	m_pUser->Send(&Packet);
}

void CFSGameUserHippocampusEvent::SendRewardInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == HIPPOCAMPUS.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CPacketComposer Packet(S2C_EVENT_HIPPOCAMPUS_REWARD_INFO_RES);
	HIPPOCAMPUS.MakeRewardInfo(Packet);

	m_pUser->Send(&Packet);
}

RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD CFSGameUserHippocampusEvent::EatFoodReq(BYTE btFoodType, SS2C_EVENT_HIPPOCAMPUS_EAT_FOOD_RES& rs)
{
	rs.btResult = RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FAIL;
	rs.btFoodType = btFoodType;

	CHECK_CONDITION_RETURN(CLOSED == HIPPOCAMPUS.IsOpen(), RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FAIL);
	
	CHECK_CONDITION_RETURN(MAX_EVENT_HIPPOCAMPUS_FOOD_TYPE_COUNT <= btFoodType, RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FAIL);
	
	HIPPOCAMPUS_USER_FOOD_MAP::iterator iter = m_mapUserFood.find(btFoodType);
	CHECK_CONDITION_RETURN(iter == m_mapUserFood.end(), RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_NOT_EXIST_FOOD);
	CHECK_CONDITION_RETURN(0 >= iter->second._iCount, RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_NOT_EXIST_FOOD);

	SODBCEventHippocampusUserInfo UserInfo;
	m_UserInfo.GetUserInfo(UserInfo);

	SODBCEventHippocampusUserLog Log;
	m_UserLog.GetUserLog(Log);

	rs.iFoodExp = 0;
	rs.btGrade = m_UserInfo._btGrade;
	rs.iLv = m_UserInfo._iLv;
	rs.iExp = m_UserInfo._iExp;
	ZeroMemory(&rs.sReward, sizeof(SREWARD_INFO));

	BOOL bGradeUp = FALSE;
	BOOL bGivePresent = TRUE;
	int iRewardIndex = 0;
	if(TRUE == HIPPOCAMPUS.GradeUp(btFoodType, m_UserInfo._iDieCount, UserInfo, rs, iRewardIndex))
	{
		bGradeUp = TRUE;
		if(iRewardIndex > 0)
		{
			if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
				bGivePresent = FALSE;
		}
	}
	else
	{
		if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
			bGivePresent = FALSE;

		UserInfo._btGrade = EVENT_HIPPOCAMPUS_GRADE_1;
		UserInfo._iLv = 1;
		UserInfo._iExp = 0;

		UserInfo._iDieCount += 1;
		if(EVENT_HIPPOCAMPUS_GRADE_5 <= m_UserInfo._btGrade)
			UserInfo._iDieCount = 0;

		UserInfo._iKeepCount += 1;

		Log._iDieCount += 1;
		if(m_UserInfo._btGrade > Log._btGrade ||
			(m_UserInfo._btGrade == Log._btGrade && m_UserInfo._iLv > Log._iLv) ||
			(m_UserInfo._btGrade == Log._btGrade && m_UserInfo._iLv == Log._iLv && m_UserInfo._iExp > Log._iExp))
		{
			Log._btGrade = m_UserInfo._btGrade;
			Log._iLv = m_UserInfo._iLv;
			Log._iExp = m_UserInfo._iExp;
		}
	}

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FAIL);

	int iUpdateFoodCount = iter->second._iCount - 1;

	if(MAX_EVENT_HIPPOCAMPUS_BONUS_EXP_TYPE_COUNT > UserInfo._btEnableBonusExpType)
	{
		UserInfo._iBonusExpEnableCount += 1;
		if(UserInfo._iBonusExpEnableCount >= 5)
		{
			UserInfo._btEnableBonusExpType = EVENT_HIPPOCAMPUS_BONUS_EXP_TYPE_NONE;
			UserInfo._iBonusExpEnableCount = 0;
		}
	}
	else
	{
		SODBCEventHippocampusBonusExpConfig Config;
		if(TRUE == HIPPOCAMPUS.GetRandomBonusExp(Config))
		{
			if(MAX_EVENT_HIPPOCAMPUS_BONUS_EXP_TYPE_COUNT > Config._btType)
			{
				UserInfo._btEnableBonusExpType = Config._btType;
				UserInfo._iBonusExpEnableCount = 0;
			}
		}
	}

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_UpdateUserData(m_pUser->GetUserIDIndex(), UserInfo, TRUE, btFoodType, iUpdateFoodCount, Log, iRewardIndex), RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FAIL);

	if(iRewardIndex > 0)
	{
		if(TRUE == bGivePresent)
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(NULL == pReward)
			{
				pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(m_pUser->GetUserIDIndex(), iRewardIndex);
				return RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FAIL;
			}
		}
		else
		{
			pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(m_pUser->GetUserIDIndex(), iRewardIndex);

			if(TRUE == bGradeUp)
				rs.btResult = RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FULL_MAIL_BOX_GRADE_UP_SUCCESS;
			else
				rs.btResult = RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FULL_MAIL_BOX_GRADE_UP_FAIL;
		}
	}

	m_UserInfo.SetUserInfo(UserInfo);
	iter->second._iCount = iUpdateFoodCount;

	m_UserLog.SetUserLog(Log);

	return (RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD)rs.btResult;
}

RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP CFSGameUserHippocampusEvent::GetBonusExpReq(BYTE btBonusExpType, SS2C_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_RES& rs)
{
	rs.btResult = RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FAIL;

	CHECK_CONDITION_RETURN(CLOSED == HIPPOCAMPUS.IsOpen(), RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FAIL);

	CHECK_CONDITION_RETURN(MAX_EVENT_HIPPOCAMPUS_BONUS_EXP_TYPE_COUNT <= btBonusExpType, RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FAIL);
	CHECK_CONDITION_RETURN(MAX_EVENT_HIPPOCAMPUS_BONUS_EXP_TYPE_COUNT <= m_UserInfo._btEnableBonusExpType, RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_DISABLE);
	CHECK_CONDITION_RETURN(btBonusExpType != m_UserInfo._btEnableBonusExpType, RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_DISABLE);

	SODBCEventHippocampusUserInfo UserInfo;
	m_UserInfo.GetUserInfo(UserInfo);

	SODBCEventHippocampusUserLog Log;
	m_UserLog.GetUserLog(Log);

	rs.btBonusExpType = btBonusExpType;
	rs.iBonusExpTypeExp = 0;
	rs.btGrade = m_UserInfo._btGrade;
	rs.iLv = m_UserInfo._iLv;
	rs.iExp = m_UserInfo._iExp;
	ZeroMemory(&rs.sReward, sizeof(SREWARD_INFO));

	UserInfo._btEnableBonusExpType = EVENT_HIPPOCAMPUS_BONUS_EXP_TYPE_NONE;
	UserInfo._iBonusExpEnableCount = 0;

	BOOL bGradeUp = FALSE;
	BOOL bGivePresent = TRUE;
	int iRewardIndex = 0;
	if(TRUE == HIPPOCAMPUS.GradeUp(btBonusExpType, m_UserInfo._iDieCount, UserInfo, rs, iRewardIndex))
	{
		bGradeUp = TRUE;
		if(iRewardIndex > 0)
		{
			if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
				bGivePresent = FALSE;
		}
	}
	else
	{
		if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
			bGivePresent = FALSE;

		UserInfo._btGrade = EVENT_HIPPOCAMPUS_GRADE_1;
		UserInfo._iLv = 1;
		UserInfo._iExp = 0;

		UserInfo._iDieCount += 1;
		if(EVENT_HIPPOCAMPUS_GRADE_5 <= m_UserInfo._btGrade)
			UserInfo._iDieCount = 0;

		UserInfo._iKeepCount += 1;

		Log._iDieCount += 1;
		if(m_UserInfo._btGrade > Log._btGrade ||
			(m_UserInfo._btGrade == Log._btGrade && m_UserInfo._iLv > Log._iLv) ||
			(m_UserInfo._btGrade == Log._btGrade && m_UserInfo._iLv == Log._iLv && m_UserInfo._iExp > Log._iExp))
		{
			Log._btGrade = m_UserInfo._btGrade;
			Log._iLv = m_UserInfo._iLv;
			Log._iExp = m_UserInfo._iExp;
		}
	}

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_GRADE_UP_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_UpdateUserData(m_pUser->GetUserIDIndex(), UserInfo, FALSE, MAX_EVENT_HIPPOCAMPUS_FOOD_TYPE_COUNT, 0, Log, iRewardIndex), RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FAIL);

	if(iRewardIndex > 0)
	{
		if(TRUE == bGivePresent)
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(NULL == pReward)
			{
				pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(m_pUser->GetUserIDIndex(), iRewardIndex);
				return RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FAIL;
			}
		}
		else
		{
			pODBC->EVENT_HIPPOCAMPUS_UpdateUserWaitGiveReward(m_pUser->GetUserIDIndex(), iRewardIndex);

			if(TRUE == bGradeUp)
				rs.btResult = RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FULL_MAIL_BOX_GRADE_UP_SUCCESS;
			else if(rs.btResult == RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_GRADE_UP_FAIL)
				rs.btResult = RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FULL_MAIL_BOX_GRADE_UP_FAIL;
			else if(rs.btResult == RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_EXP_UP_FAIL)
				rs.btResult = RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FULL_MAIl_BOX_EXP_UP_FAIL;
		}
	}

	m_UserInfo.SetUserInfo(UserInfo);
	m_UserLog.SetUserLog(Log);

	return (RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP)rs.btResult;
}

void CFSGameUserHippocampusEvent::GiveWaitGiveReward()
{
	CHECK_CONDITION_RETURN_VOID(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_VOID(pODBC);

	vector<SODBCEventHippocampusUserWaitGiveReward> vReward;
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_GetUserWaitGiveReward(m_pUser->GetUserIDIndex(), vReward));

	for(int iNum = 0; iNum < vReward.size(); ++iNum)
	{
		for(int iCount = 0; iCount < vReward[iNum]._iCount; ++iCount)
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, vReward[iNum]._iRewardIndex);
			if(NULL != pReward)
			{
				if(ODBC_RETURN_SUCCESS == pODBC->EVENT_HIPPOCAMPUS_DeleteUserWaitGiveReward(m_pUser->GetUserIDIndex(), vReward[iNum]._iRewardIndex))
				{
					CHECK_CONDITION_RETURN_VOID(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST);
				}
			}			
		}
	}
}

void CFSGameUserHippocampusEvent::SendRankInfo(BYTE btRankType, BYTE btServerIndex)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == HIPPOCAMPUS.IsOpen() && CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_HIPPOCAMPUS_RANK));
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_EVENT_HIPPOCAMPUS_RANK_INFO_RES rs;
	rs.btRankType = btRankType;
	rs.iMyRank = 0;
	rs.btMyServerIndex = btServerIndex;
	if(0 == rs.btMyServerIndex)
		rs.btMyServerIndex = 1;
	else if(1 == rs.btMyServerIndex)
		rs.btMyServerIndex = 2;
	else if(2 == rs.btMyServerIndex)
		rs.btMyServerIndex = 4;
	else if(3 == rs.btMyServerIndex)
		rs.btMyServerIndex = 5;

	ZeroMemory(rs.szMyGameID, _countof(rs.szMyGameID));
	const SREPRESENT_INFO* pRePresent = m_pUser->GetRepresentInfo();
	if(pRePresent != NULL)
		strncpy_s(rs.szMyGameID, _countof(rs.szMyGameID), pRePresent->szGameID, MAX_GAMEID_LENGTH+1);

	rs.iValue1 = 0;
	rs.iValue2 = 0;
	if(EVENT_HIPPOCAMPUS_RANK_TYPE_1 == btRankType)
	{
		rs.iValue1 = m_UserLog._btGrade;
		rs.iValue2 = m_UserLog._iLv;
	}
	else
	{
		rs.iValue1 = m_UserLog._iDieCount;
	}

	rs.iTotalRankCount = 0;
	rs.iRewardCount = 0;

	CPacketComposer Packet(S2C_EVENT_HIPPOCAMPUS_RANK_INFO_RES);
	HIPPOCAMPUS.MakeRankInfo(btRankType, btServerIndex, m_pUser->GetUserIDIndex(), Packet, rs);

	m_pUser->Send(&Packet);
}

void CFSGameUserHippocampusEvent::MakeIntegrateDB_UpdateUserLog(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT& not)
{
	not.iUserIDIndex = m_pUser->GetUserIDIndex();
	not.iGameIDIndex = -1;
	ZeroMemory(not.szGameID, _countof(not.szGameID));
	const SREPRESENT_INFO* pRePresent = m_pUser->GetRepresentInfo();
	if(pRePresent != NULL)
	{
		not.iGameIDIndex = pRePresent->iGameIDIndex;
		strncpy_s(not.szGameID, _countof(not.szGameID), pRePresent->szGameID, MAX_GAMEID_LENGTH+1);
	}

	if(-1 == not.iGameIDIndex)
	{
		not.iGameIDIndex = m_pUser->GetGameIDIndex();
		strncpy_s(not.szGameID, _countof(not.szGameID), m_pUser->GetGameID(), MAX_GAMEID_LENGTH+1);
	}

	m_UserLog.MakeUserLog(not);
}

void CFSGameUserHippocampusEvent::MakeGiveFood( CPacketComposer& Packet, int& iCount, BYTE btMatchType, BOOL bIsDisconnectedMatch, BOOL bRunAway )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == HIPPOCAMPUS.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(btMatchType != MATCH_TYPE_RATING && btMatchType != MATCH_TYPE_RANKMATCH && btMatchType != MATCH_TYPE_CLUB_LEAGUE);

	BYTE btNoticeStatus;	// EVENT_HIPPOCAMPUS_REWARD_NOTICE_STATUS
	BYTE btFoodType;		// EVENT_HIPPOCAMPUS_FOOD_TYPE
	int iRewardCount;

	btNoticeStatus = EVENT_HIPPOCAMPUS_REWARD_NOTICE_STATUS_ADD_FOOD;

	if(TRUE == bIsDisconnectedMatch || TRUE == bRunAway)
		btNoticeStatus = EVENT_HIPPOCAMPUS_REWARD_NOTICE_STATUS_NOT_NORMAL_GAME;

	int iFoodCount = 0;
	HIPPOCAMPUS_USER_FOOD_MAP::iterator iter = m_mapUserFood.begin();
	for( ; iter != m_mapUserFood.end(); ++iter)
	{
		iFoodCount += iter->second._iCount;
	}

	if(EVENT_HIPPOCAMPUS_REWARD_NOTICE_STATUS_NOT_NORMAL_GAME != btNoticeStatus &&
		iFoodCount >= HIPPOCAMPUS.GetMaxFoodCount())
		btNoticeStatus = EVENT_HIPPOCAMPUS_REWARD_NOTICE_STATUS_MAX_FOOD_COUNT;

	btFoodType = 0;
	iRewardCount = 0;

	SEVENT_GAME_RESULT_COMPLETED_INFO info;
	info.iEventKind = EVENT_KIND_HIPPOCAMPUS;
	info.iStatus = btNoticeStatus;
	if(EVENT_HIPPOCAMPUS_REWARD_NOTICE_STATUS_ADD_FOOD == btNoticeStatus)
	{
		SODBCEventHippocampusFoodConfig Config;
		CHECK_CONDITION_RETURN_VOID(FALSE == HIPPOCAMPUS.GetRandomFood(Config));
		CHECK_CONDITION_RETURN_VOID(MAX_EVENT_HIPPOCAMPUS_FOOD_TYPE_COUNT <= Config._btFoodType);

		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);

		int iRewardCount = Config._iRewardCount;
		if((iFoodCount+iRewardCount) > HIPPOCAMPUS.GetMaxFoodCount())
			iRewardCount -= (iFoodCount+iRewardCount) - HIPPOCAMPUS.GetMaxFoodCount();

		int iUpdatedCount = 0;
		CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_UpdateUserFood(m_pUser->GetUserIDIndex(), Config._btFoodType, iRewardCount, iUpdatedCount));

		iter = m_mapUserFood.find(Config._btFoodType);
		if(iter != m_mapUserFood.end())
		{
			iter->second._iCount = iUpdatedCount;
		}
		else
		{
			SEventHippocampusUserFood Food;
			Food.SetUserFood(Config._btFoodType, iUpdatedCount);
			m_mapUserFood[Config._btFoodType] = Food;
		}

		btFoodType = Config._btFoodType;
		iRewardCount = iRewardCount;
	}
	
	info.iValue = btFoodType;
	++iCount; // packet count
	Packet.Add((PBYTE)&info, sizeof(SEVENT_GAME_RESULT_COMPLETED_INFO));
}

void CFSGameUserHippocampusEvent::TestAddFood()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == HIPPOCAMPUS.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	int iFoodCount = 0;
	HIPPOCAMPUS_USER_FOOD_MAP::iterator iter = m_mapUserFood.begin();
	for( ; iter != m_mapUserFood.end(); ++iter)
	{
		iFoodCount += iter->second._iCount;
	}

	int iAddFoodCount = HIPPOCAMPUS.GetMaxFoodCount() - iFoodCount;
	CHECK_CONDITION_RETURN_VOID(0 == iAddFoodCount);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iUpdatedCount = 0;
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_HIPPOCAMPUS_UpdateUserFood(m_pUser->GetUserIDIndex(), EVENT_HIPPOCAMPUS_FOOD_TYPE_GOLD, iAddFoodCount, iUpdatedCount));

	iter = m_mapUserFood.find(EVENT_HIPPOCAMPUS_FOOD_TYPE_GOLD);
	if(iter != m_mapUserFood.end())
	{
		iter->second._iCount = iUpdatedCount;
	}
	else
	{
		SEventHippocampusUserFood Food;
		Food.SetUserFood(EVENT_HIPPOCAMPUS_FOOD_TYPE_GOLD, iUpdatedCount);
		m_mapUserFood[EVENT_HIPPOCAMPUS_FOOD_TYPE_GOLD] = Food;
	}
}
