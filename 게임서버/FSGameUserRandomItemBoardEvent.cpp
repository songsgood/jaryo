qinclude "stdafx.h"
qinclude "RandomItemBoardEventManager.h"
qinclude "FSGameUserRandomItemBoardEvent.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "UserHighFrequencyItem.h"
qinclude "RewardManager.h"

CFSGameUserRandomItemBoardEvent::CFSGameUserRandomItemBoardEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_bRandomItemBoardUpdated(FALSE)
	, m_bRandomItemBoardLoaded(FALSE)
{
}

CFSGameUserRandomItemBoardEvent::~CFSGameUserRandomItemBoardEvent(void)
{
}

BOOL CFSGameUserRandomItemBoardEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMBOARDEVENT.IsOpen(), TRUE);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iODBCReturn;
	if(ODBC_RETURN_SUCCESS != (iODBCReturn = pODBC->EVENT_RANDOMITEM_BOARD_GetUseData(m_pUser->GetUserIDIndex(), m_RandomItemBoardInfo)))
	{
		CHECK_CONDITION_RETURN(ODBC_RETURN_NODATA == iODBCReturn, TRUE);
		
		return FALSE;
	}

	m_bRandomItemBoardLoaded = TRUE;

	return TRUE;
}

void CFSGameUserRandomItemBoardEvent::SendRandomItemBoardEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMBOARDEVENT.IsOpen());

	SS2C_RANDOMITEMBOARD_EVENT_INFO_RES rs;
	rs.iBoardTicketCount = 0;

	SUserHighFrequencyItem* pBoardTicket = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_BOARD_TICKET);
	if(pBoardTicket != NULL)
		rs.iBoardTicketCount = pBoardTicket->iPropertyTypeValue;

	if(m_bRandomItemBoardLoaded == TRUE || m_bRandomItemBoardUpdated == TRUE)
	{
		RANDOMITEMBOARDEVENT.MakeRandomItemBoardInfo(rs, m_RandomItemBoardInfo);
	}
	else
	{
		RANDOMITEMBOARDEVENT.MakeRandomItemBoardInfo(rs);
	}

	m_pUser->Send(S2C_RANDOMITEMBOARD_EVENT_INFO_RES, &rs, sizeof(SS2C_RANDOMITEMBOARD_EVENT_INFO_RES));
}

void CFSGameUserRandomItemBoardEvent::SaveRandomItemBoard(SC2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ rq)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMBOARDEVENT.IsOpen());

	int iItemBarCnt[MAX_RANDOMITEMBOARD_ITEMBAR_COUNT];
	ZeroMemory(iItemBarCnt, sizeof(int)*MAX_RANDOMITEMBOARD_ITEMBAR_COUNT);
	int iRandomItemBoardInfo[MAX_EVENT_RANDOMBOARD_COUNT];	

	for(int iBoardNumber = 0; iBoardNumber < MAX_EVENT_RANDOMBOARD_COUNT; ++iBoardNumber)
	{
		if(rq.sBoardInfo[iBoardNumber].iItemBarIndex >= EVENT_RANDOMITEMBOARD_ITEMBAR_INDEX_1 && rq.sBoardInfo[iBoardNumber].iItemBarIndex < MAX_RANDOMITEMBOARD_ITEMBAR_COUNT)
		{
			iRandomItemBoardInfo[iBoardNumber] = rq.sBoardInfo[iBoardNumber].iItemBarIndex;
			iItemBarCnt[iRandomItemBoardInfo[iBoardNumber]]++;
		}
		else
		{
			iRandomItemBoardInfo[iBoardNumber] = -1;
		}
	}

	if(TRUE == RANDOMITEMBOARDEVENT.CheckItemBarCount(iItemBarCnt))
	{
		memcpy(m_RandomItemBoardInfo, iRandomItemBoardInfo, sizeof(int)*MAX_EVENT_RANDOMBOARD_COUNT);
		m_bRandomItemBoardUpdated = TRUE;
	}
}

void CFSGameUserRandomItemBoardEvent::SaveRandomItemBoard()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMBOARDEVENT.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bRandomItemBoardUpdated);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBC);

	pODBC->EVENT_RANDOMITEM_BOARD_UpdateUseData(m_pUser->GetUserIDIndex(), m_RandomItemBoardInfo);
}

void CFSGameUserRandomItemBoardEvent::UseBoardTicket(SC2S_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_REQ rq)
{
	if(CLOSED == RANDOMITEMBOARDEVENT.IsOpen())
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
		return;
	}

	SUserHighFrequencyItem* pBoardTicket = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_BOARD_TICKET);
	if((pBoardTicket == NULL) ||
		pBoardTicket != NULL && pBoardTicket->iPropertyTypeValue <= 0)
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_ENOUGH_TICKET);
		return;
	}

	if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET__FULL_MAIL);
		return;
	}

	// 사용한 바의 보상정보 유효성 검사 
	int iItemBarCnt[MAX_RANDOMITEMBOARD_ITEMBAR_COUNT];
	ZeroMemory(iItemBarCnt, sizeof(int)*MAX_RANDOMITEMBOARD_ITEMBAR_COUNT);
	map<int/*iBoardNumber*/,SRandomItemBarRewardConfig> mapItemBarReward[MAX_RANDOMITEMBOARD_ITEMBAR_COUNT];
	vector<int/*iBoardNumber*/> vNotItemBarBoardNumber;
	SRandomItemBarRewardConfig RewardConfig;

	for(int iBoardNumber = 0; iBoardNumber < MAX_EVENT_RANDOMBOARD_COUNT; ++iBoardNumber)
	{
		if(rq.sBoardInfo[iBoardNumber].iRewardNumber < 0)
		{
			vNotItemBarBoardNumber.push_back(iBoardNumber);
			continue;
		}

		if(FALSE == RANDOMITEMBOARDEVENT.GetItemBarRewardConfig(rq.sBoardInfo[iBoardNumber].iItemBarIndex, rq.sBoardInfo[iBoardNumber].iRewardNumber, RewardConfig))
		{
			SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
			return;
		}
		mapItemBarReward[RewardConfig._iItemBarIndex].insert(map<int/*iBoardNumber*/,SRandomItemBarRewardConfig>::value_type(RewardConfig._iItemBarIndex, RewardConfig));
		iItemBarCnt[RewardConfig._iItemBarIndex]++;
	}

	if(FALSE == RANDOMITEMBOARDEVENT.CheckItemBarCount(iItemBarCnt))
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
		return;
	}

	for(int iItemBarIndex = EVENT_RANDOMITEMBOARD_ITEMBAR_INDEX_1; iItemBarIndex < MAX_RANDOMITEMBOARD_ITEMBAR_COUNT; ++iItemBarIndex)
	{
		int iRewardNumber = 0;
		map<int/*iBoardNumber*/,SRandomItemBarRewardConfig>::iterator iterRq = mapItemBarReward[iItemBarIndex].begin();
		for( ; iterRq != mapItemBarReward[iItemBarIndex].end(); ++iterRq)
		{
			if(FALSE == RANDOMITEMBOARDEVENT.GetItemBarRewardConfig(iterRq->second._iItemBarIndex, iterRq->second._iRewardNumber, RewardConfig))
			{
				SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
				return;
			}

			// Config 보상 정보와 순서대로 일치하는지 검사		 
			if(iterRq->second._iItemBarIndex != RewardConfig._iItemBarIndex ||
				iterRq->second._iRewardNumber != RewardConfig._iRewardNumber)
			{
				SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
				return;
			}	
		}	
	}

	if(FALSE == RANDOMITEMBOARDEVENT.GetRandomRewardConfig(RewardConfig))
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
		return;
	}

	SS2C_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_RES rs;
	rs.btResult = RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_SUCCESS;
	rs.btRewardType = RewardConfig._sReward.btRewardType;
	rs.iItemCode = RewardConfig._sReward.iItemCode;
	rs.iPropertyKind = RewardConfig._sReward.iPropertyKind;
	rs.iPropertyType = RewardConfig._sReward.iPropertyType;
	rs.iPropertyValue = RewardConfig._sReward.iPropertyValue;

	int iRewardIndex = RewardConfig._sReward.iRewardIndex;
	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	if(pReward == NULL)
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
		return;
	}

	rs.iBoardNumber = 0;
	for(int iBoardNumber = 0; iBoardNumber < MAX_EVENT_RANDOMBOARD_COUNT; ++iBoardNumber)
	{
		if(rq.sBoardInfo[iBoardNumber].iItemBarIndex >= 0 && rq.sBoardInfo[iBoardNumber].iItemBarIndex < MAX_RANDOMITEMBOARD_ITEMBAR_COUNT &&
			rq.sBoardInfo[iBoardNumber].iItemBarIndex == RewardConfig._iItemBarIndex &&
			rq.sBoardInfo[iBoardNumber].iRewardNumber == RewardConfig._iRewardNumber)
		{
			// 아이템 바에 있는 아이템 당첨
			rs.iBoardNumber = iBoardNumber+1;
			rs.btRewardKind = RANDOMBOARD_REWARD_KIND_ITEMBAR;
			break;
		}
	}

	if(rs.iBoardNumber == 0)
	{
		// 보드 번호 아무거나 알려준다.
		int iRateKey = rand() vNotItemBarBoardNumber.size();

		if(iRateKey < 0 || iRateKey >= vNotItemBarBoardNumber.size())
			iRateKey = 0;

		rs.iBoardNumber = vNotItemBarBoardNumber[iRateKey]+1;
		rs.btRewardKind = RANDOMBOARD_REWARD_KIND_NOT_ITEMBAR;
	}

	if(rs.btResult != RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_SUCCESS)
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
		return;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iUpdated_BoardTicketCnt;
	if (ODBC_RETURN_SUCCESS == pODBC->EVENT_RANDOMITEM_BOARD_UseTicket(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex, TRUE, iUpdated_BoardTicketCnt))
	{
		rs.iUpdatedBoardTicketCount = iUpdated_BoardTicketCnt;

		SUserHighFrequencyItem* pBoardTicket = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_BOARD_TICKET);
		if(pBoardTicket != NULL)
			pBoardTicket->iPropertyTypeValue = iUpdated_BoardTicketCnt;

		SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
		if(pReward == NULL)
		{
			// 보상 지급 실패시 뽑기권 반환
			if (ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_BOARD_UseTicket(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex, FALSE, iUpdated_BoardTicketCnt))
			{
				WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMBOARD_EVENT, DB_DATA_UPDATE, FAIL, "GiveReward - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560",
er->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
			}

			if(pBoardTicket != NULL)
				pBoardTicket->iPropertyTypeValue = iUpdated_BoardTicketCnt;
		}

		m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_RANDOMITEM_TICKET);
	}
	else
	{
		SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_FAIL);
		return;
	}

	m_pUser->Send(S2C_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_RES, &rs, sizeof(SS2C_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_RES));

	SC2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ save;
	memcpy(&save.sBoardInfo, &rq.sBoardInfo, sizeof(int)*MAX_EVENT_RANDOMBOARD_COUNT);
	m_pUser->GetUserRandomItemBoardEvent()->SaveRandomItemBoard(save);

	//내맘대로 뽑기 뽑아보기 일일미션
	m_pUser->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_RANDOMITEMBOARD);
}

void CFSGameUserRandomItemBoardEvent::SendUseBoardTicketFail(RESULT_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET eResult)
{
	SS2C_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_RES rs;
	rs.btResult = eResult;
	m_pUser->Send(S2C_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_RES, &rs, sizeof(SS2C_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_RES));
}
