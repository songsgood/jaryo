qpragma once
qinclude "MiniGameZoneRank.h"

class CFSGameUser;

class CFSGameUserMiniGameZoneEvent
{
public:
	CFSGameUserMiniGameZoneEvent(CFSGameUser* pUser);
	virtual ~CFSGameUserMiniGameZoneEvent(void);

	BOOL			Load();
	BOOL			CheckResetDate(time_t tCurrentTime = _time64(NULL));
	BOOL			SendEventInfo();
	BOOL			SendUserInfo();
	BOOL			UseTicket(SC2S_MINIGAMEZONE_USE_TICKET_REQ& rs);
	BOOL			UseCoin(SC2S_MINIGAMEZONE_USE_COIN_REQ& rs);
	BOOL			UpdateScore(SC2S_MINIGAMEZONE_UPDATE_SCORE_REQ& rs, BOOL& bIsBestScore);
	BOOL			CheckAndGiveTicket(BYTE& btResultTicketType, int& iGiveTicketCnt);
	void			CheckAndGiveReward();
	BYTE			UpdateMainGameID(int iGameIDIndex);
	int				GetMainGameIDIndex()			{ return m_iMainGameIDIndex; }
	int				GetCurrentSeason()				{ return m_iCurrentSeason; }
	int				GetBestScore(BYTE btGameType)	{ return (btGameType >= MAX_MINIGAMEZONE_GAME_TYPE) ? 0 : m_iGameScore[btGameType]; }
	int				GetLastScore(BYTE btSeasonType, BYTE btRecordType);

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;
	BOOL			m_bIsLoginReward;
	int				m_iCurrentSeason;
	int				m_iGameTicket[MAX_MINIGAMEZONE_GAME_TYPE];
	int				m_iGameScore[MAX_MINIGAMEZONE_GAME_TYPE];
	int				m_iGameScore_Last[MAX_MINIGAMEZONE_RANK_SEASON_TYPE][MAX_MINIGAMEZONE_GAME_TYPE];
	int				m_iWaitRewardFlag;
	int				m_iGameCoin;
	int				m_iMainGameIDIndex;
	char			m_szRankGameID[MAX_GAMEID_LENGTH+1];
	BYTE			m_btTodayTicketGetCnt;
	BYTE			m_btTodayMainGameIDUpdateCnt;
	time_t			m_tRecentResetDate;

	time_t			m_tGameStartTime;
	BYTE			m_btPlayingGameType;
	DWORD			m_dwTicketKey;
};

