qpragma once

class CFSGameUser;
class CFSODBCBase;
class CFSItemShop;

typedef map<int/*iAreaIndex*/, SUserThreeKingdomsEventAreaCondition> USER_THREEKINGDOMSEVENT_AREA_CONDITION_MAP;

class CFSGameUserThreeKingdomsEvent
{
public:
	CFSGameUserThreeKingdomsEvent(CFSGameUser* pUser);
	~CFSGameUserThreeKingdomsEvent(void);

	BOOL Load();

	void SendThreeKingdomsEventInfo();
	RESULT_EVENT_THREE_KINGDOMS_GET_REWARD RecvMissionReward(CFSItemShop* pItemShop, BYTE btEventRewardType, int& iRewardIndex);
	void UpdateMissionCount();

private:
	void CheckAndUpdateAreaStatus();
	void UpdateAreaStatus(int iAreaIndex, BYTE btStatus);
	void CheckAndUpdateAllAreaCompleted();
	void InitializeDailyMission();

private:
	CFSGameUser* m_pUser;
	BOOL		m_bDataLoaded;

	USER_THREEKINGDOMSEVENT_AREA_CONDITION_MAP	m_mapUserAreaCondition;
	SUserThreeKingdomsEventMissionCondition		m_UserMissionCondition;
	SUserThreeKingdomsEventRewardCondition		m_UserRewardCondition[MAX_EVENT_THREE_KINGDOMS_REWARD_TYPE_COUNT];
	int m_iProgressAreaIndex;
};

