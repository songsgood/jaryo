#pragma once

class CUser;
class CFSODBCBase;

#define MAX_TRY_COUNT	10

struct SEventUserPassword
{
	int _iPassword[PASSWORD_NUMBER_COUNT];
	int _iRemainTryCount;

	SEventUserPassword()
	{
		ZeroMemory(this, sizeof(SEventUserPassword));
	}

	void SetUserPassword(SODBCEventUserPassword UserPassword)
	{
		for(int i = 0; i < PASSWORD_NUMBER_COUNT; ++i)		
			_iPassword[i] = UserPassword._iPassword[i];

		_iRemainTryCount = UserPassword._iRemainTryCount;
	}
};

struct SEventUserPlayData
{
	int _iTryCount;
	int _iPassword[PASSWORD_NUMBER_COUNT];
	int _iStrike;
	int _iBall;

	SEventUserPlayData()
	{
		ZeroMemory(this, sizeof(SEventUserPlayData));
	}

	void SetUserPlayData(SODBCEventUserPlayData UserPlayData)
	{
		_iTryCount = UserPlayData._iTryCount;

		for(int i = 0; i < PASSWORD_NUMBER_COUNT; ++i)		
			_iPassword[i] = UserPlayData._iPassword[i];

		_iStrike = UserPlayData._iStrike;
		_iBall = UserPlayData._iBall;
	}
};
typedef map<int/*_iTryCount*/,SEventUserPlayData> USER_PLAY_DATA_MAP;

class CFSGameUserPasswordEvent
{
public:
	CFSGameUserPasswordEvent(CUser* pUser);
	~CFSGameUserPasswordEvent(void);

	BOOL Load();

	void SendEventInfo();
	void SendEventRewardInfo();
	RESULT_SET_PASSWORD_EVENT_TRY TryReq(SC2S_SET_PASSWORD_EVENT_TRY_REQ rq, SS2C_SET_PASSWORD_EVENT_TRY_RES& rs);
	int AddTryCount();
	void InitializePassword();
	
protected:
	void UpdatePassword(BOOL bUserReq = FALSE);

private:
	CUser* m_pUser;
	BOOL m_bDataLoaded;
	BOOL m_bInitializing;

	SEventUserPassword m_UserPassword;
	USER_PLAY_DATA_MAP m_mapUserPlayData;
};

