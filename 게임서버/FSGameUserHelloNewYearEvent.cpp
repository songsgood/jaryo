qinclude "stdafx.h"
qinclude "CFSGameUser.h"
qinclude "FSGameUserHelloNewYearEvent.h"
qinclude "ThreadODBCManager.h"
qinclude "RewardManager.h"
qinclude "HelloNewYearEventManager.h"

CFSGameUserHelloNewYearEvent::CFSGameUserHelloNewYearEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iRamainBuffSec(-1)
	, m_btButtonStatus(HelloNewYearEvent::BUTTON_STATUS_OFF)
	, m_tStartLoginTime(-1)
{
}


CFSGameUserHelloNewYearEvent::~CFSGameUserHelloNewYearEvent(void)
{
}

BOOL					CFSGameUserHelloNewYearEvent::Load()
{
	CHECK_CONDITION_RETURN(FALSE == HELLONEWYEAR.IsPossibleOpenEvent(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_HELLONEWYEAR_GetUserData(m_pUser->GetUserIDIndex(), m_iRamainBuffSec, m_btButtonStatus))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_HELLONEWYEAR_GetUserData failed. UserIDIndex:10752790", m_pUser->GetUserIDIndex());
rn FALSE;	
	}

	if(m_iRamainBuffSec > 0)
	{
		if(CLOSED == HELLONEWYEAR.IsOpen()) 
		{
			// 이벤트가 아직 시작 안했을 경우, 접속시간을 이벤트 시작시간으로 설정. (버프시간 카운팅은 아직 안함)
			HELLONEWYEAR.GetStartDate(m_tStartLoginTime);
		}
		else
		{
			m_tStartLoginTime = _time64(NULL);
		}
	}

	m_bDataLoaded = TRUE;
	return TRUE;
}

void					CFSGameUserHelloNewYearEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == HELLONEWYEAR.IsOpen());

	CheckRemainBuffSec(_time64(NULL));

	SS2C_HELLONEWYEAR_USER_INFO_RES	ss;
	ss.iRemainSec = m_iRamainBuffSec;
	ss.btButtonStatus = m_btButtonStatus;

	m_pUser->Send(S2C_HELLONEWYEAR_USER_INFO_RES, &ss, sizeof(SS2C_HELLONEWYEAR_USER_INFO_RES));
}

BOOL					CFSGameUserHelloNewYearEvent::CheckRemainBuffSec(time_t tCheckTime, bool bNeedUpdate/* = FALSE*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == HELLONEWYEAR.IsOpen(), FALSE);

	if(0 >= m_iRamainBuffSec)
	{
		return FALSE;
	}

	if(tCheckTime <= m_tStartLoginTime)	
	{
		// 이벤트가 시작할 때 부터 카운팅.
		return FALSE;
	}

	m_iRamainBuffSec -= (tCheckTime - m_tStartLoginTime);
	m_tStartLoginTime = tCheckTime;	

	if(m_iRamainBuffSec <= 0)
	{
		bNeedUpdate = TRUE;
		m_iRamainBuffSec = 0;
		m_tStartLoginTime = -1;
	}
	
	if(bNeedUpdate)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if(ODBC_RETURN_SUCCESS != pODBC->EVENT_HELLONEWYEAR_UpdateBuffSec(m_pUser->GetUserIDIndex(), m_iRamainBuffSec, m_btButtonStatus))
		{
			WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "EVENT_HELLONEWYEAR_UpdateBuffSec Fail. UserIDIndex:10752790, RemainSec:0", m_pUser->GetUserIDIndex(), m_iRamainBuffSec);
	}

	return TRUE;
}

void					CFSGameUserHelloNewYearEvent::GetRewardReq()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	
	SS2C_HELLONEWYEAR_GET_REWARD_RES	ss;
	
	if(CLOSED == HELLONEWYEAR.IsOpen())
	{
		ss.btResult = HelloNewYearEvent::RESULT_GET_REWARD_FAIL_EVENT_CLOSED;
	}
	else if(HelloNewYearEvent::BUTTON_STATUS_OFF == m_btButtonStatus)
	{
		ss.btResult = HelloNewYearEvent::RESULT_GET_REWARD_FAIL_ALREADY_GET;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = HelloNewYearEvent::RESULT_GET_REWARD_FAIL_FULL_MAILBOX;
	}
	else
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);

		ss.btResult = HelloNewYearEvent::RESULT_GET_REWARD_FAIL;

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_HELLONEWYEAR_UpdateBuffSec(m_pUser->GetUserIDIndex(), m_iRamainBuffSec, HelloNewYearEvent::BUTTON_STATUS_OFF))
		{
			m_btButtonStatus = HelloNewYearEvent::BUTTON_STATUS_OFF;

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, HELLONEWYEAR.GetLoginRewardIndex());
			if(pReward)
			{
				ss.btResult = HelloNewYearEvent::RESULT_GET_REWARD_SUCCESS;
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "CFSGameUserHelloNewYearEvent::GetRewardReq GiveReward Fail. UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), HELLONEWYEAR.GetLoginRewardIndex());

	}
	}

	m_pUser->Send(S2C_HELLONEWYEAR_GET_REWARD_RES, &ss, sizeof(SS2C_HELLONEWYEAR_GET_REWARD_RES));
}

int CFSGameUserHelloNewYearEvent::GetUserBuffRate(bool bCheckTime/* = false*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, 0);
	CHECK_CONDITION_RETURN(CLOSED == HELLONEWYEAR.IsOpen(), 0);

	if(bCheckTime)
	{
		CheckRemainBuffSec(_time64(NULL));
	}

	return (0 < m_iRamainBuffSec) ? HELLONEWYEAR.GetExpBuffRate() : 0;
}
