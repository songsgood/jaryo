qinclude "stdafx.h"
qinclude "FSGameUserTransferJoycity100DreamEvent.h"
qinclude "TransferJoycity100DreamEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "CFSGameServer.h"
qinclude "UserHighFrequencyItem.h"
qinclude "CenterSvrProxy.h"

CFSGameUserTransferJoycity100DreamEvent::CFSGameUserTransferJoycity100DreamEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iGetDailyTicketCount(0)
	, m_tGetTicketTime(-1)
{
}

CFSGameUserTransferJoycity100DreamEvent::~CFSGameUserTransferJoycity100DreamEvent(void)
{
}

BOOL CFSGameUserTransferJoycity100DreamEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == TRANSFERJOYCITY100DREAM.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<STransferJoycity100DreamEventUserRecord> vUserRecord;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_TRANSFERJOYCITY_100DREAM_GetUserData(m_pUser->GetUserIDIndex(), m_iGetDailyTicketCount, m_tGetTicketTime) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_TRANSFERJOYCITY_100DREAM_GetUserRecord(m_pUser->GetUserIDIndex(), vUserRecord))
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITY100DREAM, INITIALIZE_DATA, FAIL, "EVENT_TRANSFERJOYCITY_100DREAM_GetUserData error");
		return FALSE;
	}

	for(int i = 0; i < vUserRecord.size(); ++i)
	{
		m_mapUserRecord[vUserRecord[i]._iIndex] = vUserRecord[i];
	}

	CheckAndUpdateRankGameID();

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserTransferJoycity100DreamEvent::SendEventInfo(BYTE btType)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == TRANSFERJOYCITY100DREAM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(btType >= MAX_EVENT_TRANSFERJOYCITY_100DREAM_REQ_TYPE);

	ResetTicket();

	SS2C_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES rs;
	rs.iRemainTicketCount = 0;
	rs.btType = btType;
	rs.iCount = 0;

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_100DREAM_TICKET);
	if(NULL != pItem)
		rs.iRemainTicketCount = pItem->iPropertyTypeValue;

	CPacketComposer Packet(S2C_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES);
	if(EVENT_TRANSFERJOYCITY_100DREAM_REQ_TYPE_MY_RECORD == btType)
	{
		rs.iCount = m_mapUserRecord.size();
		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES));

		EVENT_TRANSFERJOYCITY_100DREAM_MY_RECORD myRecord;
		TRANSFERJOYCITY_100DREAM_USER_RECORD_MAP::reverse_iterator iter = m_mapUserRecord.rbegin();
		for( ; iter != m_mapUserRecord.rend(); ++iter)
		{
			myRecord.iGiveCash = iter->second._iGiveCash;
			myRecord.tGiveDate = iter->second._tGiveDate;
			Packet.Add((PBYTE)&myRecord, sizeof(EVENT_TRANSFERJOYCITY_100DREAM_MY_RECORD));
		}
		m_pUser->Send(&Packet);
	}
	else
	{
		CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
		if(NULL != pCenter)
		{
			SG2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ rq;
			rq.iGameIDIndex = m_pUser->GetGameIDIndex();
			strncpy_s(rq.GameID, _countof(rq.GameID), m_pUser->GetGameID(), _countof(rq.GameID)-1);
			rq.iUserIDIndex = m_pUser->GetUserIDIndex();
			rq.rs.iRemainTicketCount = rs.iRemainTicketCount;
			rq.rs.btType = rs.btType;
			rq.rs.iCount = rs.iCount;
			pCenter->SendPacket(G2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ, &rq, sizeof(SG2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ));
		}
	}
}

void CFSGameUserTransferJoycity100DreamEvent::MakeGiveTicket(CPacketComposer& Packet, int& iCount, BYTE btMatchType, BYTE btMatchScaleType, BOOL bIsDisconnectedMatch, BOOL bRunAway)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == TRANSFERJOYCITY100DREAM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(btMatchType != MATCH_TYPE_RATING && btMatchType != MATCH_TYPE_RANKMATCH && btMatchType != MATCH_TYPE_CLUB_LEAGUE);
	CHECK_CONDITION_RETURN_VOID(btMatchScaleType != MATCH_TEAM_SCALE_3ON3 && btMatchScaleType != MATCH_TEAM_SCALE_3ON3CLUB);

	ResetTicket();

	CHECK_CONDITION_RETURN_VOID(m_iGetDailyTicketCount >= TRANSFERJOYCITY100DREAM.GetDailyGiveTicketCnt());

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iAddTicket = 1;
	int iUpdated_Ticket = 0;
	int iUpdated_GetDailyTicketCount = 0;
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_TRANSFERJOYCITY_100DREAM_UpdateTicket(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iAddTicket, iUpdated_Ticket, iUpdated_GetDailyTicketCount));

	CUserHighFrequencyItem* pHighFrequencyItem = m_pUser->GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_100DREAM_TICKET;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdated_Ticket;
		if(iUpdated_Ticket > 0)
			pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
		else
			pHighFrequencyItem->DeleteUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_100DREAM_TICKET);
	}

	m_iGetDailyTicketCount = iUpdated_GetDailyTicketCount;
	m_tGetTicketTime = _GetCurrentDBDate;

	SEVENT_GAME_RESULT_COMPLETED_INFO info;
	info.iEventKind = EVENT_KIND_TRANSFERJOYCITY_100DREAM;
	info.iValue = iAddTicket;
	info.iStatus = 0;
	++iCount; // packet count
	Packet.Add((PBYTE)&info, sizeof(SEVENT_GAME_RESULT_COMPLETED_INFO));
}

void CFSGameUserTransferJoycity100DreamEvent::ResetTicket()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == TRANSFERJOYCITY100DREAM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(TRUE == TodayCheck(EVENT_RESET_HOUR, m_tGetTicketTime, _GetCurrentDBDate));

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_TRANSFERJOYCITY_100DREAM_ResetTicket(m_pUser->GetUserIDIndex()));

	m_pUser->GetUserHighFrequencyItem()->UpdateUserHighFrequencyItemCount(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_100DREAM_TICKET, 0);

	m_iGetDailyTicketCount = 0;
	m_tGetTicketTime = _GetCurrentDBDate;
}

RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN CFSGameUserTransferJoycity100DreamEvent::TryWinReq(int& iGiveCash, int& iRankNumber)
{
	CHECK_CONDITION_RETURN(CLOSED == TRANSFERJOYCITY100DREAM.IsOpen(), RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_FAIL);

	ResetTicket();

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_100DREAM_TICKET);
	CHECK_CONDITION_RETURN(NULL == pItem, RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_NOT_ENOUGH_TICKET);
	CHECK_CONDITION_RETURN(0 >= pItem->iPropertyTypeValue, RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_NOT_ENOUGH_TICKET);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_FULL_MAILBOX);

	CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	if(NULL != pCenter)
	{
		SG2S_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_REQ rq;
		rq.iGameIDIndex = m_pUser->GetGameIDIndex();
		strncpy_s(rq.GameID, _countof(rq.GameID), m_pUser->GetGameID(), _countof(rq.GameID)-1);
		rq.iUserIDIndex = m_pUser->GetUserIDIndex();

		const SREPRESENT_INFO* pRePresent = m_pUser->GetRepresentInfo();
		if(pRePresent != NULL)
		{
			rq.iRepresentGameIDIndex = pRePresent->iGameIDIndex;
			strncpy_s(rq.szRepresentGameID, _countof(rq.szRepresentGameID), pRePresent->szGameID, MAX_GAMEID_LENGTH+1);
		}
		else
		{
			rq.iRepresentGameIDIndex = m_pUser->GetGameIDIndex();
			strncpy_s(rq.szRepresentGameID, _countof(rq.szRepresentGameID), m_pUser->GetGameID(), MAX_GAMEID_LENGTH+1);
		}

		pCenter->SendPacket(G2S_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_REQ, &rq, sizeof(SG2S_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_REQ));

		return RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_SUCCESS;
	}
	
	return RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_FAIL;
}

void CFSGameUserTransferJoycity100DreamEvent::UpdateTryWinRes(SS2G_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES& rs)
{
	REWARDMANAGER.GiveReward(m_pUser, rs.iRewardIndex);

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_100DREAM_TICKET);
	CHECK_NULL_POINTER_VOID(pItem);

	if (0 >= rs.iUpdated_Ticket)
	{
		m_pUser->GetUserHighFrequencyItem()->DeleteUserHighFrequencyItem(pItem->iPropertyKind);
		pItem = NULL;
	}
	else
	{
		pItem->iPropertyTypeValue = rs.iUpdated_Ticket;
	}

	STransferJoycity100DreamEventUserRecord userRecord;
	userRecord._iIndex = rs.iIndex;
	userRecord._iGiveCash = rs.rs.iGiveCash;
	userRecord._tGiveDate = rs.tGiveDate;
	m_mapUserRecord[userRecord._iIndex] = userRecord;
}

RESULT_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS CFSGameUserTransferJoycity100DreamEvent::WriteWordsReq(SC2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ& rq)
{
	CHECK_CONDITION_RETURN(CLOSED == TRANSFERJOYCITY100DREAM.IsOpen(), RESULT_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_FAIL);

	CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	if(NULL != pCenter)
	{
		SG2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ info;
		info.iGameIDIndex = m_pUser->GetGameIDIndex();
		strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), _countof(info.GameID)-1);
		info.iUserIDIndex = m_pUser->GetUserIDIndex();
		info.rq.iRankNumber = rq.iRankNumber;
		strncpy_s(info.rq.szWords, _countof(info.rq.szWords), rq.szWords, _countof(rq.szWords)-1);
		pCenter->SendPacket(G2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ, &info, sizeof(SG2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ));

		return RESULT_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_SUCCESS;
	}

	return RESULT_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_FAIL;
}

void CFSGameUserTransferJoycity100DreamEvent::CheckAndUpdateRankGameID()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == TRANSFERJOYCITY100DREAM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	const SREPRESENT_INFO* pInfo = m_pUser->GetRepresentInfo();
	CHECK_NULL_POINTER_VOID(pInfo);

	CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	if(NULL != pCenter)
	{
		SG2S_EVENT_TRANSFERJOYCITY_100DREAM_CHANGE_RANK_GAMEID_REQ info;
		info.iUserIDIndex = m_pUser->GetUserIDIndex();
		info.iRepresentGameIDIndex = pInfo->iGameIDIndex;
		strncpy_s(info.szRepresentGameID, _countof(info.szRepresentGameID), pInfo->szGameID, MAX_GAMEID_LENGTH+1);
		pCenter->SendPacket(G2S_EVENT_TRANSFERJOYCITY_100DREAM_CHANGE_RANK_GAMEID_REQ, &info, sizeof(SG2S_EVENT_TRANSFERJOYCITY_100DREAM_CHANGE_RANK_GAMEID_REQ));
	}
}