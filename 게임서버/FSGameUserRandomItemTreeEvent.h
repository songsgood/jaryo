qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserRandomItemTreeEvent
{
public:
	CFSGameUserRandomItemTreeEvent(CFSGameUser* pUser);
	~CFSGameUserRandomItemTreeEvent(void);

	BOOL Load();

	void SendEventOpenStatus();
	void SendEventInfo();
	RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM UseItemReq();

private:
	CFSGameUser* m_pUser;
	BOOL m_bDataLoaded;

	int m_iUseItemCount;
};

