qinclude "stdafx.h"
qinclude "FSGameUserVendingMachineEvent.h"
qinclude "VendingMachineEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserVendingMachineEvent::CFSGameUserVendingMachineEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iGiveMoney(0)
	, m_iInputMoney(0)
	, m_iMyMoney(0)
	, m_tRecentResetTime(-1)
{
}


CFSGameUserVendingMachineEvent::~CFSGameUserVendingMachineEvent(void)
{
}

BOOL	CFSGameUserVendingMachineEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == VENDINGMACHINE.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_VENDINGMACHINE_GetUserData(m_pUser->GetUserIDIndex(), m_iInputMoney, m_iMyMoney, m_tRecentResetTime))
	{
		WRITE_LOG_NEW(LOG_TYPE_VENDINGMACHINEEVENT, INITIALIZE_DATA, FAIL, "EVENT_VENDINGMACHINE_GetUserData error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;
	CheckAndGiveMoney();

	return TRUE;
}

BOOL	CFSGameUserVendingMachineEvent::CheckAndGiveMoney(time_t tCurrentTime /*= _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == VENDINGMACHINE.IsOpen(), FALSE);

	BOOL	bResult = FALSE;

	if(-1 == m_tRecentResetTime)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tRecentResetTime, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= EVENT_RESET_HOUR;

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tRecentResetTime <= tTime && tTime <= tCurrentTime)
			{
				bResult = TRUE;
				break;
			}
		}
	}	

	if(bResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);
		int iAddMoney = 0;

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_VENDINGMACHINE_CheckAndGiveMoney(m_pUser->GetUserIDIndex(), tCurrentTime, iAddMoney))
		{
			m_iGiveMoney = iAddMoney;
			m_iMyMoney += iAddMoney;
			m_tRecentResetTime = tCurrentTime;
		}
	}

	return bResult;
}

void	CFSGameUserVendingMachineEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == VENDINGMACHINE.IsOpen());

	CPacketComposer Packet(S2C_VENDINGMACHINE_PRODUCT_LIST_RES);
	VENDINGMACHINE.MakePacketProductInfo(Packet);
	m_pUser->Send(&Packet);
}

void	CFSGameUserVendingMachineEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == VENDINGMACHINE.IsOpen());

	CheckAndGiveMoney();

	SS2C_VENDINGMACHINE_USER_INFO_RES	ss;
	ss.iGiveMoney = m_iGiveMoney;
	ss.iInputMoney = m_iInputMoney;
	ss.iMyMoney = m_iMyMoney;
	m_iGiveMoney = 0;

	CPacketComposer Packet(S2C_VENDINGMACHINE_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_VENDINGMACHINE_USER_INFO_RES));
	m_pUser->Send(&Packet);
}

BOOL	CFSGameUserVendingMachineEvent::InputMoney()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == VENDINGMACHINE.IsOpen(), FALSE);

	CheckAndGiveMoney();

	SS2C_VENDINGMACHINE_INPUT_MONEY_RES	ss;

	if(m_iMyMoney <= 0)
	{
		ss.btResult = VENDINGMACHINE_INPUT_MONEY_RESULT_FAIL_LACK_MONEY;
	}
	else
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_VENDINGMACHINE_InputMoney(m_pUser->GetUserIDIndex()))
		{
			ss.btResult = VENDINGMACHINE_INPUT_MONEY_RESULT_SUCCESS;
			++m_iInputMoney;
			--m_iMyMoney;
			ss.iGiveMoney = m_iGiveMoney;
			ss.iInputMoney = m_iInputMoney;
			ss.iMyMoney = m_iMyMoney;
			m_iGiveMoney = 0;
		}
	}

	CPacketComposer Packet(S2C_VENDINGMACHINE_INPUT_MONEY_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_VENDINGMACHINE_INPUT_MONEY_RES));
	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL	CFSGameUserVendingMachineEvent::UseMoney(BYTE btProductIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == VENDINGMACHINE.IsOpen(), FALSE);

	CheckAndGiveMoney();

	SS2C_VENDINGMACHINE_USE_MONEY_RES	ss;
	int iPrice = 0;

	if(FALSE == VENDINGMACHINE.GetProductPrice(btProductIndex, iPrice))
	{
		ss.btResult = VENDINGMACHINE_USE_MONEY_RESULT_FAIL_UNKNOWN;
	}
	else if(m_iInputMoney < iPrice)
	{
		ss.btResult = VENDINGMACHINE_USE_MONEY_RESULT_FAIL_LACK_MONEY;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = VENDINGMACHINE_USE_MONEY_RESULT_FAIL_FULL_MAILBOX;
	}
	else
	{
		ss.btResult = VENDINGMACHINE_USE_MONEY_RESULT_FAIL_UNKNOWN;
		
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_VENDINGMACHINE_UseMoney(m_pUser->GetUserIDIndex(), btProductIndex))
		{
			ss.btResult = VENDINGMACHINE_USE_MONEY_RESULT_SUCCESS;
			m_iInputMoney -= iPrice;

			int iRewardIndex = VENDINGMACHINE.GetProductRewardIndex(btProductIndex);

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(NULL == pReward)
			{
				WRITE_LOG_NEW(LOG_TYPE_VENDINGMACHINEEVENT, LA_DEFAULT, NONE, "REWARDMANAGER.GiveReward UsePot, UserIDIndex:10752790, RewardIndex:0"
, m_pUser->GetUserIDIndex(), iRewardIndex);
			}
		}
	}

	CPacketComposer Packet(S2C_VENDINGMACHINE_USE_MONEY_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_VENDINGMACHINE_USE_MONEY_RES));
	m_pUser->Send(&Packet);

	return TRUE;
}