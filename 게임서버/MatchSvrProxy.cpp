qinclude "stdafx.h"
qinclude "MatchSvrProxy.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameServer.h"
qinclude "CFSLobby.h"
qinclude "ClubSvrProxy.h"
qinclude "CenterSvrProxy.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSSvrList.h"
qinclude "FSGameCommon.h"
qinclude "CFSGameGM.h"
qinclude "HackingManager.h"
qinclude "CFSGameClient.h"
qinclude "Rc4.h"
qinclude "RewardManager.h"
qinclude "ClubConfigManager.h"
qinclude "CoachCardShop.h"
qinclude "ActionInfluenceList.h"
qinclude "RageBalance.h"
qinclude "SportBigEventManager.h"
qinclude "ClubTournamentAgency.h"
qinclude "MatchingPoolCareManager.h"
qinclude "FactionManager.h"
qinclude "RewardCardManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "UserMissionEvent.h"
qinclude "HotGirlTimeManager.h"
qinclude "UserChoiceMissionEvent.h"
qinclude "EventDateManager.h"
qinclude "RankMatchManager.h"
qinclude "ODBCSvrProxy.h"
qinclude "PromiseManager.h"
qinclude "WhitedayEventManager.h"
qinclude "SummerCandyEventManager.h"

CMatchSvrProxy::CMatchSvrProxy(void)
	: m_bIsAutoTeam(FALSE)
{
	SetState(FS_MATCH_SERVER_PROXY);
}

CMatchSvrProxy::~CMatchSvrProxy(void)
{
}

void CMatchSvrProxy::ProcessPacket(CReceivePacketBuffer* recvPacket)
{
	switch(recvPacket->GetCommand())
	{
	case M2G_TEAM_CREATE:									Process_TeamCreate(recvPacket);										break;
	case M2G_MOTION_STATE_DATA:								Process_MotionStateData(recvPacket);								break;
	case M2G_BROADCAST_WAIT_TEAM_LIST:						Process_BroadcastWaitTeamList(recvPacket);							break;
	case M2G_TEAM_MEMBER_LIST_RES:							Process_TeamMemberListRes(recvPacket);								break;
	case M2G_TEAM_MEMBER_RES:								Process_TeamMemberRes(recvPacket);									break;
	case M2G_TEAM_INFO_IN_TEAM_RES:							Process_TeamInfoInTeamRes(recvPacket);								break;
	case M2G_AVOID_TEAM_RES:								Process_AvoidTeamRes(recvPacket);									break;
	case M2G_CHANGE_TEAM_ALLOW_OBSERVE_RES:					Process_ChangeTeamAllowObserveRes(recvPacket);						break;
	case M2G_CHANGE_TEAM_NAME_RES:							Process_ChangeTeamNameRes(recvPacket);								break;
	case M2G_CHANGE_TEAM_PASSWORD_RES:						Process_ChangeTeamPasswordRes(recvPacket);							break;
	case M2G_CHANGE_TEAM_MODE_RES:							Process_ChangeTeamModeRes(recvPacket);								break;
	case M2G_CHAT_RES:										Process_ChatRes(recvPacket);										break;
	case M2G_BROADCAST_CHAT_RES:							Process_BroadcastChatRes(recvPacket);								break;
	case M2G_ENTER_TEAM_RES:								Process_TeamEnter(recvPacket);										break;
	case M2G_TEAM_EXIT_RES:									Process_TeamExit(recvPacket);										break;
	case M2G_GOTO_TEAM_MODE_REQ:							Process_GotoTeamModeReq(recvPacket);								break;
	case M2G_WAIT_TEAM_LIST_RES:							Process_WaitTeamListRes(recvPacket);								break;
	case M2G_TEAM_LIST_USER_SET:							Process_TeamListUserSet(recvPacket);								break;
	case M2G_CHIEF_ASSIGN_CHECK_RES:						Process_ChiefAssignCheckRes(recvPacket);							break;
	case M2G_FORCEOUT_USER_RES:								Process_ForceOutUserRes(recvPacket);								break;
	case M2G_REG_TEAM_RES:									Process_RegTeamRes(recvPacket);										break;
	case M2G_AUTO_REG_RES:									Process_AutoRegRes(recvPacket);										break;
	case M2G_AUTO_TEAM_REG_RES:								Process_AutoTeamRegRes(recvPacket);									break;
	case M2G_AUTO_TEAM_MAKE_COMPLETE:						Process_AutoTeamMakeComplete(recvPacket);							break;
	case M2G_READY_TEAM_LIST_RES:							Process_ReadyTeamListRes(recvPacket);								break;
	case M2G_BROADCAST_READY_TEAM_NOT:						Process_ReadyTeamNOT(recvPacket);								break;
	case M2G_REQUEST_TEAM_MATCH_RES:						Process_RequestTeamMatchRes(recvPacket);							break;
	case M2G_ENTER_READY_TEAM_RES:							Process_EnterReadyTeamRes(recvPacket);								break;
	case M2G_CHANGE_COURT_RES:								Process_ChangeCourtRes(recvPacket);									break;
	case M2G_CHANGE_PRACTICE_METHOD_RES:					Process_ChangePracticeMethodRes(recvPacket);						break;
	case M2G_TEAM_CHANGE_RES:								Process_TeamChangeRes(recvPacket);									break;
	case M2G_TEAM_CHANGE_INGAME_RES:						Process_TeamChangeInGameRes(recvPacket);							break;
	case M2G_TEAM_INFO_RES:									Process_TeamInfoRes(recvPacket);									break;
	case M2G_ROOM_INFO_IN_ROOM_RES:							Process_RoomInfoInRoomRes(recvPacket);								break;
	case M2G_INVITE_USER_RES:								Process_InviteUserRes(recvPacket);									break;
	case M2G_FOLLOW_FRIEND_RES:								Process_FollowFriendRes(recvPacket);								break;
	case M2G_QUICK_ROOM_INFO_RES:							Process_QuickRoomInfoRes(recvPacket);								break;
	case M2G_CHANGE_POSITION_RES:							Process_ChangePositionRes(recvPacket);								break;
	case M2G_EVENT_VIEW_TYPE_RES:							Process_EventViewTypeRes(recvPacket);								break;
	case M2G_BROADCAST_ROOM_INFO:							Process_BroadcastRoomInfo(recvPacket);								break;
	case M2G_CHECK_GAME_START_PREPARATION_REQ:				Process_CheckGameStartPreparationReq(recvPacket);					break;
	case M2G_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_REQ:			Process_CheckPublicIPAddressForGameReq(recvPacket);					break;
	case M2G_START_UDP_HOLEPUNCHING_REQ:					Process_StartUdpHolepunchingReq(recvPacket);						break;
	case M2G_START_BANDWIDTH_CHECK_REQ:						Process_StartBandWidthCheckReq(recvPacket);							break;
	case M2G_NOTICE_WHO_IS_HOST:							Process_NoticeWhoIsHost(recvPacket);								break;
	case M2G_READY_HOST_REQ:								Process_ReadyHostReq(recvPacket);									break;
	case M2G_SET_BROADCASTER:								Process_SetBroadCaster(recvPacket);									break;
	case M2G_GAMESTART_RES:									Process_GameStartRes(recvPacket);									break;	
	case M2G_SET_USER_STATE:								Process_SetUserState(recvPacket);									break;	
	case M2G_SET_USER_LOCATION:								Process_SetUserLocation(recvPacket);								break;	
	case M2G_DISPLAY_COUNTER:								Process_DisplayCounter(recvPacket);									break;	
	case M2G_CONNECT_HOST_REQ:								Process_ConnectHostReq(recvPacket);									break;	
	case M2G_CONNECT_HOST_JOIN_REQ:							Process_ConnectHostJoinReq(recvPacket);								break;
	case M2G_VOICE_CHATTING_USER_STATUS_INFO_NOT:			Process_VoiceChattingUserStatusInfoNot(recvPacket);					break;
	case M2G_VOICE_CHATTING_USER_OPTION_INFO_RES:			Process_VoiceChattingUserOptionInfoRes(recvPacket);					break;
	case M2G_VOICE_CHATTING_CLOSE_NOT:						Process_VoiceChattingCloseNot(recvPacket);							break;
	case M2G_INITIALGAME_REQ:
	case M2G_RE_INITIALGAME_REQ:
															Process_InitialGameReq(recvPacket);									break;	
	case M2G_GAME_INITIALIZED_DATA_NOT:						Process_GameInitializedDataNOT(recvPacket);							break;	
	case M2G_PLAYCOUNT_EVENT_INFO_RES:						Process_PlayCountEventInfoRes(recvPacket);							break;	
	case M2G_MACRO_ITEM_POSSESSION:							Process_MacroItemProssession(recvPacket);							break;
	case M2G_INIT_START_GAME:								Process_InitStartGame(recvPacket);									break;
	case M2G_LOADGAME_REQ:									Process_LoadGameReq(recvPacket);									break;
	case M2G_LOADING_PROGRESS_RES:							Process_LoadingProgressRes(recvPacket);								break;
	case M2G_STARTGAME_AS_OBSERVER_REQ:						Process_StartGameAsObserverReq(recvPacket);							break;
	case M2G_STAT_CHECK_PACKET:								Process_StatCheckPacket(recvPacket);								break;
	case M2G_STAT_CHECKSUM_PACKET:							Process_StatCheckSumPacket(recvPacket);								break;
	case M2G_GIVEUP_ENABLE_RES:								Process_GiveupEnableRes(recvPacket);								break;
	case M2G_THUMBNAIL_RECORD:								Process_ThumbnailRecord(recvPacket);								break;
	case M2G_DISCONNECT_CLIENT:								Process_DisconnectClient(recvPacket);								break;
	case M2G_GAME_RESULT_RES:								Process_GameResultRes(recvPacket);									break;
	case M2G_GAME_END:										Process_GameEnd(recvPacket);										break;
	case M2G_CREATE_ROOM_RESULT:							Process_CreateRoomResult(recvPacket);								break;
	case M2G_ROOM_INFO:										Process_RoomInfo(recvPacket);										break;
	case M2G_CHANGE_GAMEROOM_NAME_RES:						Process_ChangeGameRoomNameRes(recvPacket);							break;
	case M2G_TEAM_ESTIMATE_RES:								Process_TeamEstimateRes(recvPacket);								break;
	case M2G_JOIN_ROOM_RESULT:								Process_JoinRoomResult(recvPacket);									break;
	case M2G_CANCEL_TEAM_MATCH_RES:							Process_CancelTeamMatchRes(recvPacket);								break;
	case M2G_EXIT_ROOM_RESULT:								Process_ExitRoomResult(recvPacket);									break;
	case M2G_GAME_START_PAUSE_RES:							Process_GameStartPauseRes(recvPacket);								break;
	case M2G_GAME_TERMINATED_REQ:							Process_GameTerminatedReq(recvPacket);								break;
	case M2G_UPDATE_VARIATION_TROPHY:						Process_UpdateVariationTrophy(recvPacket);							break;
	case M2G_UPDATE_TROPHYANDCHAMPION_CNT:					Process_UpdateTrophyAndChampionCnt(recvPacket);						break;
	case M2G_QUICK_JOIN_GAME_AS_OBSERVER_RES:				Process_QuickJoinGameAsObserverRes(recvPacket);						break;
	case M2G_REMAIN_PLAY_TIME_IN_ROOM_RES:					Process_RemainPlayTimeInRoomRes(recvPacket);						break;
	case M2G_REMAIN_PLAY_SCORE_IN_ROOM_RES:					Process_RemainPlayScoreInRoomRes(recvPacket);						break;
	case M2G_EXIT_OBSERVING_GAME_RES:						Process_ExitObservingGameRes(recvPacket);							break;
	case M2G_EXIT_OBSERVING_GAME_TO_TEAM_RES:				Process_ExitObservingGameToTeamRes(recvPacket);						break;
	case M2G_SEND_PACKET_QUEUE:								Process_SendPacketQueue(recvPacket);								break;
	case M2G_ABUSE_USER_GAME:								Process_AbuseUserGame(recvPacket);									break;
	case M2G_GAME_START_HALFTIME_RES:						Process_GameStartHalfTimeRes(recvPacket);							break;
	case M2G_GAME_START_HALFTIME_NOW_RES:					Process_GameStartHalfTimeNowRes(recvPacket);						break;
	case M2G_GIVEUP_VOTE_RES:								Process_GiveUpVoteRes(recvPacket);									break;
	case M2G_INTENTIONAL_FOUL_RES:							Process_IntentionalFoulRes(recvPacket);								break;
	case M2G_USE_PAUSE_ITEM_RES:							Process_UsePauseItemRes(recvPacket);								break;
	case M2G_DISCOUNT_PAUSE_ITEM_REQ:						Process_DiscountPauseItemReq(recvPacket);							break;
	case M2G_GAME_PAUSE_RES:								Process_GamePauseRes(recvPacket);									break;
	case M2G_USE_SECOND_BOOST_ITEM_RES:						Process_UseSecondBoostItemRes(recvPacket);							break;
	case M2G_SECOND_BOOST_ITEM_EXPIRED_RES:					Process_SecondBoostItemExpiredRes(recvPacket);						break;
	case M2G_ROOM_MEMBER_RES:								Process_RoomMemberRes(recvPacket);									break;
	case M2G_GM_CHANGE_HOST_RES:							Process_GMChangeHost(recvPacket);									break;
	case M2G_REMOVE_FEATURE:								Process_RemoveFeature(recvPacket);									break;
	case M2G_DECREASE_FEATURE:								Process_DecreaseFeature(recvPacket);								break;
	case M2G_BONUS_POINT_LEVELUP:							Process_BonusPointLevelUp(recvPacket);								break;
	case M2G_UPDATE_MAX_LEVEL_STEP_AND_TAKE_ITEM_REWARD:	Process_UpdateMaxLevelStepAndTakeItemReward(recvPacket);			break;
	case M2G_CHOOSE_JUMPBALL_INFO_RES:						Process_JumpBallPlayer(recvPacket);									break;
	case M2G_MATCH_FAIL_NOT:								Process_MatchFailNot(recvPacket);									break;
	case M2G_ENTRANCE_SCENE:								Process_EntranceScene(recvPacket);									break;
	case M2G_EXIT_SCENE:									Process_ExitScene(recvPacket);										break;
	case M2G_ACHIEVEMENT_COMPLETE_INGAME_NOT:				Process_CompleteAchievementInGame(recvPacket);						break;
	case M2G_ANNOUNCE_ACHIEVEMENT_COMPLETE_RES:				Process_AnnounceAchievementComplete(recvPacket);					break;
	case M2G_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_RES:			Process_EquipAchievementTitleBroadcastRes(recvPacket);				break;
	case M2G_CHANGE_ROOM_DECIDE_OUTCOME_DETAIL_RES:			Process_ChangeDecideOutcomeDetailRes(recvPacket);					break;
	case M2G_RECORD_STEP_INFO_RES:							Process_RecordStepInfoRes(recvPacket);								break;
	case M2G_RECV_PRESENT_NOT:							Process_RecvPresentNot(recvPacket);								break;
	case M2G_EVENT_COMPONENT_MSG:							Process_SendEventComponentMsg(recvPacket);							break;

	case M2G_INVITE_USERLIST_START:							Process_InviteUserListStart(recvPacket);							break;
	case M2G_INVITE_USERLIST_INFO:							Process_InviteUserListInfo(recvPacket);								break;
	case M2G_INVITE_USERLIST_END:							Process_InviteUserListEnd(recvPacket);								break;
	case M2G_TOURNAMENT_LIST_INFO:							Process_TournamentListInfo(recvPacket);								break;
	case M2G_TOURNAMENT_WAIT_LIST_INFO:						Process_TournamentWaitListInfo(recvPacket);							break;
	case M2G_TOURNAMENT_NOTIFY_RES:							Process_TournamentNotifyRes(recvPacket);							break;
	case M2G_TOURNAMENT_PROGRAM_INFO_RES:					Process_TournamentProgramInfoRes(recvPacket);						break;
	case M2G_TOURNAMENT_ENTER_RES:							Process_TournamentEnterRes(recvPacket);								break;
	case M2G_TOURNAMENT_EXIT_RES:							Process_TournamentExitRes(recvPacket);								break;
	case M2G_TOURNAMENT_CREATE_PREMADE_TEAM_RES:			Process_TournamentCreatePreMadeTeamRes(recvPacket);					break;
	case M2G_TOURNAMENT_INVITE_USER_RES:					Process_TournamentInviteUserRes(recvPacket);						break;
	case M2G_TOURNAMENT_INVITE_SUGGEST_REQ:					Process_TournamentInviteSuggestReq(recvPacket);						break;
	case M2G_TOURNAMENT_ENTER_PREMADE_TEAM:					Process_TournamentEnterPreMadeTeam(recvPacket);						break;
	case M2G_TOURNAMENT_EXIT_PREMADE_TEAM_RES:				Process_TournamentExitPreMadeTeamRes(recvPacket);					break;
	case M2G_TOURNAMENT_REGISTER_PREMADE_TEAM_RES:			Process_TournamentRegisterPreMadeTeamRes(recvPacket);				break;
	case M2G_TOURNAMENT_BUTTON_INFO_RES:					Process_TournamentButtonInfoRes(recvPacket);						break;
	case M2G_TOURNAMENT_MOVE_SEASON_RES:					Process_TournamentMoveSeasonRes(recvPacket);						break;
	case M2G_TOURNAMENT_CHIEF:								Process_TournamentChief(recvPacket);								break;
	case M2G_TOURNAMENT_CHANGE_NAME_RES:					Process_TournamentChangeNameRes(recvPacket);						break;
	case M2G_TOURNAMENT_CHANGE_PASSWORD_RES:				Process_TournamentChangePasswordRes(recvPacket);					break;
	case M2G_TOURNAMENT_READY_COUNT:						Process_TournamentReadyCount(recvPacket);							break;
	case M2G_TOURNAMENT_CHANGE_TEAM_RES:					Process_TournamentChangeTeamRes(recvPacket);						break;
	case M2G_TOURNAMENT_ENTER_MAKING_TEAM:					Process_TournamentEnterMakingTeam(recvPacket);						break;
	case M2G_TOURNAMENT_EXIT_MAKING_TEAM:					Process_TournamentExitMakingTeam(recvPacket);						break;
	case M2G_TOURNAMENT_CHANGE_STATUS:						Process_TournamentChangeStatus(recvPacket);							break;
	case M2G_TOURNAMENT_ENTER_GAMEROOM:						Process_TournamentEnterGameRoom(recvPacket);						break;
	case M2G_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_RES:			Process_TournamentForceOutPreMadeTeamRes(recvPacket);				break;
	case M2G_TOURNAMENT_GAME_RESULT:						Process_TournamentGameResult(recvPacket);							break;
	case M2G_TOURNAMENT_GAME_END:							Process_TournamentGameEnd(recvPacket);								break;
	case M2G_TOURNAMENT_START_FAIL:							Process_TournamentStartFail(recvPacket);							break;
	case M2G_TOURNAMENT_MYTEAM_INFO_RES:					Process_TournamentMyTeamInfoRes(recvPacket);						break;
	case M2G_TOURNAMENT_PUBLIC_OPEN_INFO_RES:				Process_TournamentPublicOpenInfoRes(recvPacket);					break;
	case M2G_TOURNAMENT_VICTORY_TEAM_INFO:					Process_TournamentVictoryTeamInfo(recvPacket);						break;
	case M2G_TOURNAMENT_CREATABLE_RES:						Process_TournamentCreatableRes(recvPacket);							break;
	case M2G_TOURNAMENT_MOVE_NEXT_SEASON_NOTIFY:			Process_TournamentMoveNextSeasonNotify(recvPacket);					break;
	case M2G_TOURNAMENT_START_CANCEL_LACK_TEAM:				Process_TournamentStartCancelLackTeam(recvPacket);					break;
	case M2G_TOURNAMENT_SEASON_END:							Process_TournamentSeasonEnd(recvPacket);							break;
	case M2G_TOURNAMENT_VICTORY:							Process_TournamentVictory(recvPacket);								break;
	case M2G_EVENT_COMPONENT_REWARD_POPUP:					Process_EventcomponentRewardPopup(recvPacket);						break;
	case M2G_ADD_POINT_AND_CASH:							Process_AddPointAndPoint(recvPacket);								break;
	case M2G_CHEAT_RATING_POINT_RES:						Process_CheatRatingPointRes(recvPacket);							break;
	case M2G_FSG_USEHACK_NOTIRYREQ:							Process_UseHackNotifyReq( recvPacket );								break;
	case M2G_MATCHING_CANCEL_PENALTY_NOT:					Process_MatchingCancelPenalty( recvPacket );						break;
	case M2G_GAMEED_GIVEBUFFITEM:                           Process_GameEndGiveBuffItem(recvPacket);	                        break;
	case M2G_SEND_PLAYTIME_REMAIN:							Process_SendPlayTimeRemain(recvPacket);								break;
	case M2G_ACHIEVE_REWARD_NOT:							Process_AchieveRewardNot(recvPacket);								break;
	case M2G_ACHIEVE_COMPETITION_REWARD_NOT:				Process_AchieveCometitionRewardNot(recvPacket);						break;
	case M2G_USER_RATING_UPDATE_NOT:						Process_UserRatingUpdateNot(recvPacket);							break;
	case M2G_OPEN_POINT_REWARD_CARD_NOT:					Process_OpenPointRewardCardNot(recvPacket);							break;
	case M2G_ACTION_SLOT_RES:								Process_ActionSlotRes(recvPacket);									break;
	case M2G_ACTION_ROOM_ANIM_RES:							Process_ActionAnimRes(recvPacket);									break;
	case M2G_EVENT_GIVE_EVENTCASH:							Process_EventGiveEventCash( recvPacket );							break;
	case M2G_GM_FORBID_MULITUSER_REQ:						Process_GM_FORBID_MULITUSER_REQ( recvPacket );						break;
	case M2G_UPDATE_FRIEND_PLAYCOUNT:						Process_UpdateFriendPlayCount( recvPacket );						break;
	case M2G_EVENT_SEND_PAPER:								Process_EventSendPaper( recvPacket );								break;
	case M2G_USER_PLAYGAME_ACHIEVEMENT:						Process_UserPlayGameAchievement(recvPacket);						break;
	case M2G_MATCH_LOCATION_NOT:							Process_MatchLocationNOT( recvPacket );								break;
	case M2G_USER_INFO_RES:									Process_UserInfoReq(recvPacket);									break;
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CLIENT_INFO);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_GIVE_REWARD_EXHAUST_ITEM_TO_INVEN_REQ);
	case M2G_POTENTIAL_ABILITY_LIST:						Process_Potential_Ability(recvPacket);								break;
	case M2G_POTENTIAL_BALANCE_LIST:						Process_Potential_Balance(recvPacket);								break;
	case M2G_EQUIP_PRODUCT_LIST_IN_ROOM_NOT:				Process_EquipProductListInRoomNot(recvPacket);						break;
	case M2G_RAGE_AVATAR_INFLUENCE_LIST:					Process_RageAvatarInfluenceList(recvPacket);						break;
	case M2G_NET_STATE_NOT:									Process_NetState(recvPacket);										break;
	case M2G_GAME_GUIDE_UI:									Process_Game_Guide_UI(recvPacket);									break;
	case M2G_EVENT_SPORTBIGEVENT:							Process_EVENT_SportBigEvent(recvPacket);							break;
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CLUBTOURNAMENT_ENTER_GAMEROOM_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CLUBTOURNAMENT_JOIN_PLAYER_EXIT_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CLUBTOURNAMENT_START_FAIL_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_LEFT_SEAT_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_LEFT_SEAT_NOTICE);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_MATCH_ADVANTAGE_INFLUENCE_LIST);
	
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_BURNING_STEP_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CEHCK_OPENED_TEAM_COUNT_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_GIVE_REWARD_ACHIEVEMENT_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_UPDATE_HOTGIRL_MISSION_COUNT_REQ);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_UPDATE_MISSIONBINGO_MISSION_COUNT_REQ);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CHARACTOR_STAT_MODULATION_CHECK_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_USER_APPRAISAL_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_USER_APPRAISAL_UPDATE_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_TRANSFORM_STATUS_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_PVE_UPDATE_ROOM_INFO_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_PVE_AI_BALANCE_STAT_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_AIPVP_CHANGE_GAMEPLAYERNUM_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_PVE_CHANGE_AI_TEAM_MEMBER_RES);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(G2M_FACTION_UPDATE_POINT_SCORE_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_HELLONEWYEAR_CHECK_EXP_BUFF_REQ);	
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_FRIEND_ACCOUNT_ADD_WITHPLAYCNT_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_CLUB_LEAGUE_OPEN_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_GAME_RESULT_BEST_PLAYER_UPDATE_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_GAME_APPRAISAL_MISSION_NOT);
	CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(M2G_WEBDB_USER_GAMERECORD_NOT);
	}
}

void CMatchSvrProxy::Process_TeamCreate(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_CREATE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_CREATE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if (RESULT_CREATE_TEAM_SUCCESS == rs.info.btResult)
		{
			user->GetClient()->SetState(FS_TEAM);

			user->SendToMatchAddFriendAll();
			user->SendToMatchAddFriendAccountAll();
		}

		user->Send(S2C_CREATE_TEAM_RES, &rs.info, sizeof(SS2C_CREATE_TEAM_RES));
	}
	else
	{
		SendUserLogout(user.GetPointer());
	}
}

void CMatchSvrProxy::Process_MotionStateData(CReceivePacketBuffer* rp)
{
	SM2G_MOTION_STATE_DATA rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_MOTION_STATE_DATA));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_MOTION_STATE_DATA);
		PacketComposer.Add(rs.iState);
		if(MOTION_STATE_ENTER_TEAM == rs.iState)
		{
			PacketComposer.Add((BYTE*)rs.GameID, MAX_GAMEID_LENGTH + 1);
		}
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_BroadcastWaitTeamList(CReceivePacketBuffer* rp)
{
	SM2G_BROADCAST_WAIT_TEAM_LIST rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BROADCAST_WAIT_TEAM_LIST));

	CPacketComposer PacketComposer(S2C_WAIT_TEAM_LIST_RES);
	PacketComposer.Add(&rs.iOPCode);
	PacketComposer.Add(rs.Buffer, rs.iSize);

	FSLOBBY.BroadCastTeamInfo(&PacketComposer, this->GetProcessID(), rs.btMatchTeamScale, rs.iPage , rs.iTeamNum , rs.iClubSN , rs.iOPCode);
}

void CMatchSvrProxy::Process_TeamMemberListRes(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_MEMBER_LIST_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_MEMBER_LIST_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TEAM_MEMBER_RES);
		PacketComposer.Add(rs.iOPCode);
		PacketComposer.Add(rs.iHomeAwayIdx);
		if (OP0_CLEAN == rs.iOPCode)
		{
			PacketComposer.Add(rs.btMatchType);
			PacketComposer.Add(rs.btMatchTeamScale);
		}
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TeamMemberRes(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_MEMBER_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_MEMBER_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TEAM_MEMBER_RES);
		PacketComposer.Add(rs.iOPCode);

		if(OP3_REMOVE == rs.iOPCode)
		{
			int iTeamIdx = 0;
			PacketComposer.Add(iTeamIdx);
			PacketComposer.Add((BYTE*)rs.szUpdateGameID, MAX_GAMEID_LENGTH+1);
		}
		else
		{
			BYTE Buffer[USER_INFO_SIZE];
			PBYTE pByte = Buffer;
			int	Size =0;

			memcpy(pByte+Size, &rs.iHomeAwayIdx, sizeof(int)); 
			Size += sizeof(int);
			memcpy(pByte+Size, rs.szUpdateGameID, MAX_GAMEID_LENGTH+1); 
			Size += (MAX_GAMEID_LENGTH+1);
			memcpy(pByte+Size, &rs.btStatus, sizeof(BYTE)); 
			Size += sizeof(BYTE);		
			memcpy(pByte+Size, &rs.iIsChief, sizeof(int)); 
			Size += sizeof(int);
			memcpy(pByte+Size, &rs.iIamReady, sizeof(int)); 
			Size += sizeof(int);
			memcpy(pByte+Size, &rs.shIsAIAvatar, sizeof(SHORT)); 
			Size += sizeof(SHORT);
				
			if(OP2_UPDATE != rs.iOPCode)
			{
				memcpy(pByte+Size, &rs.bySex, sizeof(BYTE)); 
				Size += sizeof(BYTE);
				memcpy(pByte+Size, &rs.byPCBang, sizeof(BYTE)); 
				Size += sizeof(BYTE);
				memcpy(pByte+Size, &rs.iLv, sizeof(int)); 
				Size += sizeof(int);
				memcpy(pByte+Size, &rs.iFameLevel, sizeof(int)); 
				Size += sizeof(int);
				memcpy(pByte+Size, &rs.iGamePosition, sizeof(int)); 
				Size += sizeof(int);
				memcpy(pByte+Size, &rs.iCharacterType, sizeof(int)); 
				Size += sizeof(int);
				memcpy(pByte+Size, &rs.iFace, sizeof(int)); 
				Size += sizeof(int);
				memcpy(pByte+Size, &rs.iHeight, sizeof(int)); 
				Size += sizeof(int);

				memcpy(pByte+Size, &rs.iFeatureNum, sizeof(int)); 
				Size += sizeof(int);
				
				BYTE btIsWearClubUniform = 0;
				for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
				{
					if(-1 != rs.FeatureInfo[i])
					{
						memcpy(pByte+Size, &rs.FeatureInfo[i], sizeof(int));
						Size += sizeof(int);

						if(TRUE == CLUBCONFIGMANAGER.IsClubUniform(rs.FeatureInfo[i]))
							btIsWearClubUniform = 1;
					}
				}
				memcpy(pByte+Size, &rs.iClubSN, sizeof(int)); 
				Size += sizeof(int);

				memcpy(pByte+Size, &rs.iClubEmblem, sizeof(int)); 
				Size += sizeof(int);

				memcpy(pByte+Size, rs.szClubName, MAX_CLUB_NAME_LENGTH+1);	
				Size += MAX_CLUB_NAME_LENGTH+1;	

				memcpy(pByte+Size, &rs.btClubPosition, sizeof(BYTE)); 
				Size += sizeof(BYTE);		

				memcpy(pByte+Size, &btIsWearClubUniform, sizeof(BYTE)); 
				Size += sizeof(BYTE);	

				memcpy(pByte+Size, &rs.btUnifromNum, sizeof(BYTE)); 
				Size += sizeof(BYTE);			

				for(int i=0 ; i<MAX_CLUB_PENNANT_SLOT_COUNT; ++i)
				{
					memcpy(pByte+Size, &rs.iPennantCode[i], sizeof(int)); 
					Size += sizeof(int);
				}

				memcpy(pByte+Size, &rs.iClubCheerLearderCode, sizeof(int)); 
				Size += sizeof(int);

				memcpy(pByte+Size, &rs.iaBallPartSlot, sizeof(int)*MAX_BASKETBALL_BALLPART); 
				Size += sizeof(int)*MAX_BASKETBALL_BALLPART;

				memcpy(pByte+Size, &rs.bBallUseOption, sizeof(BOOL)); 
				Size += sizeof(BOOL);

				//통합 삭제대기 - 스페셜팀
				int iSpecialTeamIndex = 0;
				memcpy(pByte+Size, &iSpecialTeamIndex, sizeof(int)); 
				Size += sizeof(int);

				//통합 삭제대기 vip powerup
				int ivip = -1;
				memcpy(pByte+Size, &ivip, sizeof(int)); 
				Size += sizeof(int);
				int iMarry = -1;
				memcpy(pByte+Size, &iMarry, sizeof(int)); 
				Size += sizeof(int);
				int ipowerup = -1;
				memcpy(pByte+Size, &ipowerup, sizeof(int)); 
				Size += sizeof(int);
				//
			}

			PacketComposer.Add(Buffer, Size);
			PacketComposer.Add(rs.iPositionSlot);
			PacketComposer.Add((int)0); //ToDo - delete league emblem
			PacketComposer.Add(rs.iEquippedAchievementTitle);
			PacketComposer.Add(rs.iSkillLvCode);
			PacketComposer.Add((PBYTE)&rs.bIsRoomChangeClone, sizeof(bool));

			if( OP1_CREATE == rs.iOPCode )
			{
				for ( int iPropertyKind = 0; iPropertyKind < BUFFER_ITEM_NUM; iPropertyKind++ )
				{
					SBufferItem pBufferItem = rs.sBufferItem[iPropertyKind];
					if ( pBufferItem.iPropertyKind != -1 )
					{
						PacketComposer.Add(pBufferItem.iPropertyKind);
						PacketComposer.Add(pBufferItem.iPropertyNum);
						for ( int iPropertyValue = 0; iPropertyValue < MAX_BUFFER_ITEM_PROPERTY; iPropertyValue++ )
						{
							int iProperty = pBufferItem.iProperty[iPropertyValue];
							int iValue = pBufferItem.iValue[iPropertyValue];

							if ( iProperty > 0 )
							{
								PacketComposer.Add(&iProperty);
								PacketComposer.Add(&iValue);
							}
						}
					}
					else
					{
						PacketComposer.Add( -1 );
					}
				}
			}

			// TODO DELETE CCHALLENGEMODE
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
			PacketComposer.Add((BOOL)0);			
			PacketComposer.Add((BYTE)0);
			PacketComposer.Add((BOOL)0);
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
			// CCHALLENGEMODE

			BYTE btCloneType = (BYTE)(user->CheckCloneCharacter_Index() + 1);
			PacketComposer.Add(btCloneType);

			PacketComposer.Add(rs.btServerIndex);
			PacketComposer.Add(rs.btTier);
			PacketComposer.Add(rs.btLeaguePosition);
			PacketComposer.Add((PBYTE)rs.szFriendInviteGameID, MAX_GAMEID_LENGTH+1);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TeamInfoInTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_INFO_IN_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_INFO_IN_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TEAM_INFO_IN_TEAM_RES);
		PacketComposer.Add(rs.Buffer, rs.iSize);
		PacketComposer.Add(rs.iWin);
		PacketComposer.Add(rs.iLose);
		PacketComposer.Add(rs.btFactionOption);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_AvoidTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_AVOID_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_AVOID_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_AVOID_TEAM_RES);
		PacketComposer.Add(rs.shAvoidTeam);
		PacketComposer.Add(rs.shResult);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangeTeamAllowObserveRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_TEAM_ALLOW_OBSERVE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_TEAM_ALLOW_OBSERVE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_TEAM_ALLOW_OBSERVE_RES);
		PacketComposer.Add(rs.btIsAllowToObserve);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangeTeamNameRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_TEAM_NAME_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_TEAM_NAME_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_TEAM_NAME_RES);
		PacketComposer.Add(rs.iWin);
		PacketComposer.Add(rs.iLose);
		PacketComposer.Add((BYTE*)rs.szName , MAX_TEAM_NAME_LENGTH+1);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangeTeamPasswordRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_TEAM_PASSWORD_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_TEAM_PASSWORD_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_ROOM_PASS_RES);
		PacketComposer.Add((BYTE*)rs.szPassword , MAX_TEAM_PASS_LENGTH+1);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangeTeamModeRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_TEAM_MODE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_TEAM_MODE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_TEAM_MODE_RES);
		PacketComposer.Add(rs.byTeamMode);
		PacketComposer.Add((BYTE)0); //ToDo delete HalfCourt
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChatRes(CReceivePacketBuffer* rp)
{
	SM2G_CHAT_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHAT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer Packet(S2C_CHAT); 

		if(CHAT_TYPE_MACRO == rs.iType)
		{
			Packet.Add(&rs.iType);
			Packet.Add(&rs.bIsGM);
			Packet.Add((PBYTE)rs.GameID, MAX_GAMEID_LENGTH+1);
			Packet.Add(&rs.iMacroNum);
			user->Send(&Packet);
		}
		else if ( CHAT_TYPE_TEAM_VOICE == rs.iType )
		{
			Packet.Add( &rs.iType );
			Packet.Add( (PBYTE)rs.GameID, MAX_GAMEID_LENGTH+1 );
			Packet.Add( &rs.iVoiceIndex );
			user->Send(&Packet);
		}
		else
		{
			Packet.Add(&rs.iType);
			Packet.Add(&rs.bIsGM);
			Packet.Add((PBYTE)rs.GameID, MAX_GAMEID_LENGTH+1);
			Packet.Add(&rs.iChatSize);
			Packet.Add((PBYTE)rs.szText, rs.iChatSize+1) ;
			user->Send(&Packet);
		}
	}
}

void CMatchSvrProxy::Process_BroadcastChatRes(CReceivePacketBuffer* rp)
{
	SM2G_BROADCAST_CHAT_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BROADCAST_CHAT_RES));

	CPacketComposer PacketComposer(S2C_CHAT);

	PacketComposer.Add(&rs.iType);
	PacketComposer.Add(&rs.bIsGM);
	PacketComposer.Add((PBYTE)rs.GameID, MAX_GAMEID_LENGTH + 1);
	PacketComposer.Add(&rs.iChatSize);
	PacketComposer.Add((PBYTE)rs.szText, rs.iChatSize + 1);

	if(CHAT_TYPE_TOURNAMENT == rs.iType)
		FSLOBBY.BroadCastNotTournamentUser(&PacketComposer);
}

void CMatchSvrProxy::Process_TeamEnter(CReceivePacketBuffer* rp)
{
	SM2G_ENTER_TEAM_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_ENTER_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if (RESULT_ENTER_WAIT_TEAM_SUCCESS == rs.info.btResult)
		{
			user->GetClient()->SetState(FS_TEAM);

			user->SendToMatchAddFriendAll();
			user->SendToMatchAddFriendAccountAll();
		}

		user->Send(S2C_ENTER_WAIT_TEAM_RES, &rs.info, sizeof(SS2C_ENTER_WAIT_TEAM_RES));
	}
	else
	{
		SendUserLogout(user.GetPointer());
	}
}

void CMatchSvrProxy::Process_TeamExit(CReceivePacketBuffer* rp)
{
	SM2G_EXIT_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_EXIT_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(0 == rs.iErrorCode)
		{
			user->GetClient()->SetState(NEXUS_LOBBY);
		}

		CPacketComposer PacketComposer(S2C_EXIT_WAIT_TEAM_RES);
		PacketComposer.Add(rs.iErrorCode);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_GotoTeamModeReq(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GOTO_TEAM_MODE_REQ);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_WaitTeamListRes(CReceivePacketBuffer* rp)
{
	SM2G_WAIT_TEAM_LIST_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_WAIT_TEAM_LIST_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_WAIT_TEAM_LIST_RES);
		PacketComposer.Add(rs.iOPCode);
		if(OP0_CLEAN == rs.iOPCode)
		{
			PacketComposer.Add(&rs.btMatchType);
			PacketComposer.Add(&rs.btMatchTeamScale);
			PacketComposer.Add(&rs.btAvatarLvGrade);
			PacketComposer.Add(&rs.btListMode);
			PacketComposer.Add(&rs.iSentPage);
			PacketComposer.Add(&rs.iTotalPage);
		}
		else if(OP1_CREATE == rs.iOPCode)
		{
			PacketComposer.Add(rs.Buffer, rs.iSize);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TeamListUserSet(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_LIST_USER_SET rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_LIST_USER_SET));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SetRoomListUserSet(this->GetProcessID(), rs);
	}
}

void CMatchSvrProxy::Process_ChiefAssignCheckRes(CReceivePacketBuffer* rp)
{
	SM2G_CHIEF_ASSIGN_CHECK_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHIEF_ASSIGN_CHECK_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHIEF_ASSIGN_CHECK_RES);
		PacketComposer.Add((BYTE*)rs.szName, MAX_GAMEID_LENGTH+1);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ForceOutUserRes(CReceivePacketBuffer* rp)
{
	SM2G_FORCEOUT_USER_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_FORCEOUT_USER_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_MAKE_USER_EXIT);
		PacketComposer.Add((BYTE*)rs.szName, MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add((BYTE*)rs.szBannedGameID, MAX_GAMEID_LENGTH+1);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_RegTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_REG_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_REG_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_REG_TEAM_RES);
		PacketComposer.Add(rs.iMode);	
		PacketComposer.Add(rs.iResult);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_AutoRegRes(CReceivePacketBuffer* rp)
{
	SM2G_AUTO_REG_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_AUTO_REG_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_AUTO_REG_RES);
		PacketComposer.Add(rs.shMode);	
		PacketComposer.Add(rs.shResult);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_AutoTeamRegRes(CReceivePacketBuffer* rp)
{
	SM2G_AUTO_TEAM_REG_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_AUTO_TEAM_REG_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if (RESULT_AUTO_TEAM_REG_SUCCESS == rs.info.btResult)
		{
			user->SendToMatchAddFriendAll();
			user->SendToMatchAddFriendAccountAll();
		}
		else
		{
			user->SetMatching(FALSE);
		}

		user->Send(S2C_AUTO_TEAM_REG_RES, &rs.info, sizeof(SS2C_AUTO_TEAM_REG_RES));
	}
	else if(REG_OK == rs.info.btRegMode)
	{
		SendUserLogout(user.GetPointer());
	}
}

void CMatchSvrProxy::Process_AutoTeamMakeComplete(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_AUTO_TEAM_MAKE_COMPLETE);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ReadyTeamListRes(CReceivePacketBuffer* rp)
{
	SM2G_READY_TEAM_LIST_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_READY_TEAM_LIST_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_READY_TEAM_LIST_RES);
		PacketComposer.Add(rs.iOP);
		if(OP0_CLEAN == rs.iOP)
		{
			PacketComposer.Add(rs.iMode);
			PacketComposer.Add(rs.iSendPage);
		}
		else if(OP3_REMOVE == rs.iOP)
		{
			PacketComposer.Add(rs.iTeamIdx);
		}
		else if(OP1_CREATE == rs.iOP)
		{
			PacketComposer.Add(rs.Buffer, rs.iSize);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ReadyTeamNOT(CReceivePacketBuffer* rp)
{
	SM2G_READY_TEAM_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_READY_TEAM_LIST_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if (TRUE == user->EnableBroadcastTeamInfoInTeam(rs.iPage, rs.iTeamIdx, rs.iOP))
		{
			CPacketComposer PacketComposer(S2C_READY_TEAM_LIST_RES);
			PacketComposer.Add(rs.iOP);
			if(OP3_REMOVE == rs.iOP)
			{
				PacketComposer.Add(rs.iTeamIdx);
			}
			else if(OP1_CREATE == rs.iOP ||
				OP2_UPDATE == rs.iOP)
			{
				PacketComposer.Add(rs.Buffer, rs.iSize);
			}

			user->Send(&PacketComposer);
		}
	}
}

void CMatchSvrProxy::Process_RequestTeamMatchRes(CReceivePacketBuffer* rp)
{
	SM2G_REQUEST_TEAM_MATCH_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_REQUEST_TEAM_MATCH_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(0 == rs.iMode || 1 == rs.iMode || 3 == rs.iMode)
		{
			int iSuccess = 0;
			CPacketComposer PacketComposer(S2C_REQUEST_TEAM_MATCH_RES); 
			PacketComposer.Add(rs.iMode);
			if(0 == rs.iMode)
			{
				PacketComposer.Add(rs.iTeamNum);
			}
			else
			{
				PacketComposer.Add(rs.iSuccess);
				iSuccess = rs.iSuccess;
			}

			if(0 == iSuccess)
			{
				PacketComposer.Add((BYTE*)rs.szTeamName, sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
				PacketComposer.Add(rs.iEstimateCode);
			}
			else if(2 == rs.iMode)
			{
				PacketComposer.Add((BYTE*)rs.szTeamName, sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
			}

			user->Send(&PacketComposer);
		}
	}
}

void CMatchSvrProxy::Process_EnterReadyTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_ENTER_READY_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ENTER_READY_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(0 == rs.iErrorCode)
		{
			CFSGameClient* pClient = (CFSGameClient*)user->GetClient();
			if(NULL != pClient)
			{
				pClient->SetState(NEXUS_WAITROOM);
			}
		}

		CPacketComposer PacketComposer(S2C_ENTER_READY_TEAM_RES); 
		PacketComposer.Add(rs.iErrorCode);
		PacketComposer.Add(rs.bNoticeSound);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangeCourtRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_COURT_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_COURT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_COURT_RES); 
		PacketComposer.Add(rs.iCourtIdx);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangePracticeMethodRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_PRACTICE_METHOD_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_PRACTICE_METHOD_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_PRACTICEMETHOD_RES);
		PacketComposer.Add(rs.iResult);
		PacketComposer.Add(rs.iPracticeMethod);
		PacketComposer.Add(rs.iPracticeMethodDetail);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TeamChangeRes(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_CHANGE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_CHANGE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TEAM_CHANGE_RES);
		PacketComposer.Add(rs.iResult);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TeamChangeInGameRes(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_CHANGE_INGAME_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_CHANGE_INGAME_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TEAM_CHANGE_INGAME_RES);
		PacketComposer.Add(rs.iResult);
		PacketComposer.Add(rs.iPlayerNum);
		for(int i=0; i<rs.iPlayerNum; i++)
		{
			PacketComposer.Add(rs.iRoomUserID[i]);
			PacketComposer.Add(rs.iHomeAwayIndex[i]);
		}
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TeamInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TEAM_INFO_IN_ROOM); 
		for(int i=0; i<MAX_TEAM_NUM; i++)
		{
			PacketComposer.Add((BYTE*)rs.szTeamName[i], sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
			PacketComposer.Add(rs.btFactionIndex[i]);
			PacketComposer.Add(rs.iWinContinue[i]);
			PacketComposer.Add(rs.iWin[i]);
			PacketComposer.Add(rs.iLose[i]);
		}
		PacketComposer.Add(rs.byDoubleGame);
		PacketComposer.Add(rs.byEventGameType);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_RoomInfoInRoomRes(CReceivePacketBuffer* rp)
{
	SM2G_ROOM_INFO_IN_ROOM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ROOM_INFO_IN_ROOM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_ROOM_INFO_IN_ROOM);
		PacketComposer.Add(rs.Buffer, rs.iSize);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_InviteUserRes(CReceivePacketBuffer* rp)
{
	SM2G_INVITE_USER_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_INVITE_USER_RES));
	rs.inviteInfo.btAvatarLvGrade = GetAvatarLvGrade();

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		int iFriendConcernStatus = FRIEND_ST_FRIEND_USER;
		int iFriendLvGrade = AVATAR_LV_GRADE_ALL;
		BEGIN_LOCK
			CScopedLockFriend fr(user->GetFriendList(), rs.inviteInfo.szName);
			if (fr != NULL)
			{
				iFriendConcernStatus = fr->GetConcernStatus();
				iFriendLvGrade = GET_AVATAR_LV_GRADE(fr->GetLevel());
			}
		END_LOCK;

		SS2C_INVITE_USER_RES info;
		info.btResult = RESULT_INVITE_USER_FAILED;

		if (MATCH_TYPE_RATING == GetMatchType() &&
			AVATAR_LV_GRADE_ALL != rs.inviteInfo.btAvatarLvGrade &&
			iFriendLvGrade != rs.inviteInfo.btAvatarLvGrade)
		{
			info.btResult = RESULT_INVITE_USER_NOT_EQUAL_LVGRADE;
			user->Send(S2C_INVITE_USER_RES, &info, sizeof(SS2C_INVITE_USER_RES)); 
			return;
		}

		CScopedRefGameUser pTargetUser(rs.inviteInfo.szName);
		if(pTargetUser != NULL)
		{	
			BEGIN_LOCK
				CScopedLockFriend fr(pTargetUser->GetFriendList(), user->GetGameID());
				if (fr != NULL && FRIEND_ST_BANNED_USER == fr->GetConcernStatus())
				{
					info.btResult = RESULT_INVITE_USER_REJECTED;
					user->Send(S2C_INVITE_USER_RES, &info, sizeof(SS2C_INVITE_USER_RES)); 
					return;
				}
			END_LOCK;

			if(MATCH_TYPE_CLUB_LEAGUE == rs.inviteInfo.btMatchType &&
				rs.inviteInfo.iClubIndex != pTargetUser->GetClubSN())
			{
				info.btResult = RESULT_INVITE_USER_NOT_SAME_CLUB;
				user->Send(S2C_INVITE_USER_RES, &info, sizeof(SS2C_INVITE_USER_RES)); 
				return;
			}
			else if(MATCH_TYPE_CLUB_LEAGUE == rs.inviteInfo.btMatchType &&
				rs.inviteInfo.iClubIndex == pTargetUser->GetClubSN() &&
				pTargetUser->GetClubContributionPoint() < CLUBCONFIGMANAGER.GetNeedContributionPoint())
			{
				info.btResult = RESULT_INVITE_USER_ENOUGH_CLUB_CONTRIBUTIONPOINT;
				user->Send(S2C_INVITE_USER_RES, &info, sizeof(SS2C_INVITE_USER_RES)); 
				return;
			}

			if(FRIEND_ST_FRIEND_USER == iFriendConcernStatus && TRUE == pTargetUser->CanInvite())
			{
				info.btResult = RESULT_INVITE_USER_SUCCESS;

				CPacketComposer Packet(S2C_INVITE_USER);
				Packet.Add(rs.inviteInfo.btMatchType);

				if(MATCH_TYPE_PRACTICE == rs.inviteInfo.btMatchType )
				{
					Packet.Add((PBYTE)rs.inviteInfo.szReqGameName, sizeof(char)*(MAX_GAMEID_LENGTH+1));
					Packet.Add(rs.inviteInfo.iTeamNum);
				}
				else
				{
					Packet.Add((PBYTE)rs.inviteInfo.szTeamName, sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
					Packet.Add(rs.inviteInfo.btMatchTeamScale);
					Packet.Add(rs.inviteInfo.iTeamNum);
					Packet.Add(rs.inviteInfo.shTotalNum);

					for(int i=0; i<rs.inviteInfo.shTotalNum && i<MAX_TEAM_MEMBER_NUM; i++)
					{
						Packet.Add(rs.inviteInfo.shaPos[i]);
						Packet.Add(rs.inviteInfo.shaLv[i]);
					}
					Packet.Add(rs.inviteInfo.iRoomNum);
				}
				pTargetUser->Send(&Packet);
			}

			user->Send(S2C_INVITE_USER_RES, &info, sizeof(SS2C_INVITE_USER_RES)); 
		}
		else
		{
			// 다른 로비
			if(FRIEND_ST_FRIEND_USER == iFriendConcernStatus)
			{
				CCenterSvrProxy* pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
				if(pCenterProxy != NULL) 
				{
					SG2S_INVITE_USER_REQ ss;
					ss.iGameIDIndex = rs.iGameIDIndex;
					strncpy_s(ss.GameID, _countof(ss.GameID), user->GetGameID(), MAX_GAMEID_LENGTH);
					memcpy(&ss.inviteInfo, &rs.inviteInfo, sizeof(INVITE_USER_NOT_INFO));
					pCenterProxy->SendPacket(G2S_INVITE_USER_REQ, &ss, sizeof(SG2S_INVITE_USER_REQ));
				}
			}
			else
			{
				user->Send(S2C_INVITE_USER_RES, &info, sizeof(SS2C_INVITE_USER_RES)); 

			}
		}
	}
}

void CMatchSvrProxy::Process_FollowFriendRes(CReceivePacketBuffer* rp)
{
	SM2G_FOLLOW_FRIEND_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_FOLLOW_FRIEND_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->Send(S2C_FOLLOW_FRIEND_RES, &rs.info, sizeof(SS2C_FOLLOW_FRIEND_RES));
	}
}

void CMatchSvrProxy::Process_QuickRoomInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_QUICK_ROOM_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_QUICK_ROOM_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->Send(S2C_QUICK_ROOM_INFO_RES, &rs.info, sizeof(SS2C_QUICK_ROOM_INFO_RES));
	}
}

void CMatchSvrProxy::Process_ChangePositionRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_POSITION_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_POSITION_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_POSITION_RES);
		PacketComposer.Add(rs.shSlotNumber);
		PacketComposer.Add(rs.shNewPosition);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_EventViewTypeRes(CReceivePacketBuffer* rp)
{
	/* 하나의 패킷으로 FSDAY 및 FSTIME 까지 함께 사용하게 수정 2011.07.11 by gizama */
	SM2G_EVENT_IMAGE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_EVENT_IMAGE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		int iEventViewType = NORMAL_EVENT_VIEW;
		int iEventViewPosition = rs.iEventPosition;
		int iEventViewIdx = rs.iEventImage;

		switch (iEventViewIdx)
		{
		case LVUP_EVENT_VIEW_IDX:
		case QUICK_LVUP_EVENT_VIEW_IDX:
			iEventViewType = LVUP_EVENT_VIEW;
			break;
		case FSTIME100_EVENT_VIEW_IDX:
		case FSTIME300_EVENT_VIEW_IDX:
		case FSTIME200_EVENT_VIEW_IDX:
			iEventViewType = FSTIME_EVENT_VIEW;
			break;
		case CLUB_DAY_EVENT_VIEW_IDX:
			iEventViewType = CLUB_DAY_EVENT_VIEW;
			break;
		}

		if(iEventViewPosition == -1)
		{
			iEventViewType = -1;
		}

		CPacketComposer PacketComposer(S2C_EVENT_VIEW_TYPE_RES);
		PacketComposer.Add(iEventViewType);
		PacketComposer.Add(iEventViewPosition);
		PacketComposer.Add(iEventViewIdx);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_BroadcastRoomInfo(CReceivePacketBuffer* rp)
{
	SM2G_BROADCAST_ROOM_INFO rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BROADCAST_ROOM_INFO));

	CPacketComposer PacketComposer(S2C_ROOM_INFO);

	PacketComposer.Add(&rs.iOPCode);
	if(OP3_REMOVE == rs.iOPCode)
	{
		PacketComposer.Add(&rs.iRoomID);
	}
	else
	{
		PacketComposer.Add(rs.Buffer, rs.iSize);
	}

	FSLOBBY.BroadCastFreeRoomInfo(&PacketComposer, this->GetProcessID(), rs.iPage, rs.iRoomID, rs.iOPCode);
}

void CMatchSvrProxy::Process_CheckGameStartPreparationReq(CReceivePacketBuffer* rp)
{
	SM2G_CHECK_GAME_START_PREPARATION_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHECK_GAME_START_PREPARATION_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHECK_GAME_START_PREPARATION_REQ);

		PacketComposer.Add(&rs.btUDPSessionType);
		PacketComposer.Add(&rs.dwGameStartSessionKey);
		PacketComposer.Add(&rs.iGameCount);
		PacketComposer.Add(&rs.iP2PNetLibType);
		user->Send(&PacketComposer);

		user->SendAvatarNoBroadcastInfoUpdateToMatch();
	}
}

void CMatchSvrProxy::Process_CheckPublicIPAddressForGameReq(CReceivePacketBuffer* rp)
{
	SM2G_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_REQ);
		PacketComposer.Add(rs.btUDPSessionType);
		PacketComposer.Add(rs.dwGameStartSessionKey);
		PacketComposer.Add((PBYTE)rs.szIPCheckSvrIP, MAX_IPADDRESS_LENGTH+1);
		PacketComposer.Add(rs.usIPCheckSvrPort);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_StartUdpHolepunchingReq(CReceivePacketBuffer* rp)
{
	SM2G_START_UDP_HOLEPUNCHING_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_START_UDP_HOLEPUNCHING_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_START_UDP_HOLEPUNCHING_REQ);
		PacketComposer.Add(rs.btUDPSessionType);
		PacketComposer.Add(rs.dwGameStartSessionKey);
		PacketComposer.Add(rs.iRealUserCount);
		for(int i=0; i<rs.iRealUserCount; i++)
		{
			PacketComposer.Add((PBYTE)rs.szGameID[i], MAX_GAMEID_LENGTH+1);	
			PacketComposer.Add((PBYTE)rs.szMyPublicIP[i], MAX_IPADDRESS_LENGTH+1);
			PacketComposer.Add(rs.usMyPublicPort[i]);
			PacketComposer.Add((PBYTE)rs.szMyPrivateIP[i], MAX_IPADDRESS_LENGTH+1);
			PacketComposer.Add(rs.usMyPrivatePort[i]);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_StartBandWidthCheckReq(CReceivePacketBuffer* rp)
{
	SM2G_START_BANDWIDTH_CHECK_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_START_BANDWIDTH_CHECK_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_START_BANDWIDTH_CHECK_REQ);
		PacketComposer.Add(rs.btUDPSessionType);
		PacketComposer.Add(rs.dwGameStartSessionKey);
		PacketComposer.Add(rs.iCheckBandwidthSize);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_NoticeWhoIsHost(CReceivePacketBuffer* rp)
{
	SM2G_NOTICE_WHO_IS_HOST rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_NOTICE_WHO_IS_HOST));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_NOTICE_WHO_IS_HOST);
		PacketComposer.Add(rs.btUDPSessionType);
		PacketComposer.Add(rs.dwGameStartSessionKey);
		PacketComposer.Add(rs.iUseHostServer);
		PacketComposer.Add((PBYTE)rs.szHostUserName, MAX_GAMEID_LENGTH+1);
		for(int i = 0; i<MAX_ROOM_MEMBER_NUM; i++)
		{
			PacketComposer.Add((PBYTE)rs.szGameID[i],MAX_GAMEID_LENGTH+1);
			PacketComposer.Add(rs.iLatency[i]);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ReadyHostReq(CReceivePacketBuffer* rp)
{
	SM2G_READY_HOST_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_READY_HOST_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_READY_HOST_REQ);
		PacketComposer.Add(rs.btUDPSessionType);
		PacketComposer.Add(rs.dwGameStartSessionKey);
		PacketComposer.Add(rs.iUseHostServer);
		PacketComposer.Add(rs.usMyPort);
		PacketComposer.Add((PBYTE)rs.szHostServerIP, MAX_IPADDRESS_LENGTH+1);
		PacketComposer.Add(rs.usHostServerPort);
		PacketComposer.Add(rs.iGameKey);
		PacketComposer.Add(rs.iNoOvertime);
		PacketComposer.Add(rs.iGoldenBallRule);
		PacketComposer.Add(rs.iViolationRule);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_SetBroadCaster(CReceivePacketBuffer* rp)
{
	SM2G_SET_BROADCASTER rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_SET_BROADCASTER));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_SET_BROADCASTER);
		PacketComposer.Add((PBYTE)rs.szObserverSvrAddr, MAX_IPADDRESS_LENGTH+1);
		PacketComposer.Add(rs.iObserverSvrPort);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_GameStartRes(CReceivePacketBuffer* rp)
{
	SM2G_GAMESTART_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GAMESTART_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_STARTGAME_RES);

		PacketComposer.Add(rs.iError);
		if(-10 == rs.iError)
		{
			PacketComposer.Add((BYTE*)rs.szFailedUser, sizeof(rs.szFailedUser));
		}
		else if( 0 == rs.iError ) // success
		{
			user->UpdateRand_Rc4Key(HACK_TYPE_SOME_ACTION_CHANGE_STATUS);
			PacketComposer.Add((PBYTE)user->GetAnyOperation_Rc4Key(), RC4_KEY_LENGTH+1);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_SetUserState(CReceivePacketBuffer* rp)
{
	SM2G_SET_USER_STATE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_SET_USER_STATE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->GetClient()->SetState(rs.eState);
	}
}

void CMatchSvrProxy::Process_SetUserLocation(CReceivePacketBuffer* rp)
{
	SM2G_SET_USER_LOCATION rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_SET_USER_LOCATION));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SetLocation(rs.eState);
	}
}

void CMatchSvrProxy::Process_DisplayCounter(CReceivePacketBuffer* rp)
{
	SM2G_DISPLAY_COUNTER rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_DISPLAY_COUNTER));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_DISPLAY_COUNTER);
		PacketComposer.Add(&rs.iCounterType);
		PacketComposer.Add(&rs.iCounter);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ConnectHostReq(CReceivePacketBuffer* rp)
{
	SM2G_CONNECT_HOST_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CONNECT_HOST_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CONNECT_HOST_REQ);
		PacketComposer.Add(rs.btUDPSessionType);
		PacketComposer.Add(rs.dwGameStartSessionKey);
		PacketComposer.Add((PBYTE)rs.szToMatchedIP, MAX_IPADDRESS_LENGTH+1);
		PacketComposer.Add(rs.usToMatchedPort);
		PacketComposer.Add(rs.bIsUserHost);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ConnectHostJoinReq(CReceivePacketBuffer* rp)
{
	SM2G_CONNECT_HOST_JOIN_REQ rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_CONNECT_HOST_JOIN_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_CONNECT_HOST_JOIN_REQ	ss;
		ss.btUDPSessionType = rs.btUDPSessionType;
		ss.dwSessionKey = rs.dwSessionKey;
		strncpy_s(ss.szHostPublicIP, _countof(ss.szHostPublicIP), rs.szHostPublicIP, _countof(ss.szHostPublicIP)-1);
		strncpy_s(ss.szHostPrivateIP, _countof(ss.szHostPrivateIP), rs.szHostPrivateIP, _countof(ss.szHostPrivateIP)-1);
		ss.usHostPublicPort = rs.usHostPublicPort;
		ss.usHostPrivatePort = rs.usHostPrivatePort;

		CPacketComposer PacketComposer(S2C_CONNECT_HOST_JOIN_REQ);
		PacketComposer.Add((PBYTE)&ss, sizeof(SS2C_CONNECT_HOST_JOIN_REQ));

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_VoiceChattingUserStatusInfoNot(CReceivePacketBuffer* rp)
{
	SM2G_VOICE_CHATTING_USER_STATUS_INFO_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_VOICE_CHATTING_USER_STATUS_INFO_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_VOICE_CHATTING_USER_STATUS_INFO_NOT	ss;

		ss.eStatus = rs.eStatus;
		strncpy_s(ss.szGameID, _countof(ss.szGameID), rs.szGameID, _countof(ss.szGameID)-1);

		CPacketComposer PacketComposer(S2C_VOICE_CHATTING_USER_STATUS_INFO_NOT);
		PacketComposer.Add((PBYTE)&ss, sizeof(SS2C_VOICE_CHATTING_USER_STATUS_INFO_NOT));

		user->Send(&PacketComposer);
	}
}
	
void CMatchSvrProxy::Process_VoiceChattingUserOptionInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_VOICE_CHATTING_USER_OPTION_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_VOICE_CHATTING_USER_OPTION_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_VOICE_CHATTING_USER_OPTION_INFO_RES	ss;

		ss.eStatus = rs.eStatus;
		ss.iOptionOnMemberCnt = rs.iOptionOnMemberCnt;

		for(int i = 0; i<MAX_ROOM_MEMBER_NUM; i++)
		{
			strncpy_s(ss.szGameID[i], _countof(ss.szGameID[i]), rs.szGameID[i], _countof(ss.szGameID[i])-1);
		}

		CPacketComposer PacketComposer(S2C_VOICE_CHATTING_USER_OPTION_INFO_RES);
		PacketComposer.Add((PBYTE)&ss, sizeof(SS2C_VOICE_CHATTING_USER_OPTION_INFO_RES));

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_VoiceChattingCloseNot(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_VOICE_CHATTING_CLOSE_NOT);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_InitialGameReq(CReceivePacketBuffer* rp)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iGameIDIndex;
	rp->Read(&iGameIDIndex);

	CScopedRefGameUser user(iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	user->SetPlaying(TRUE);

	CPacketComposer PacketComposer(S2C_INITIALGAME_REQ);
	if (M2G_INITIALGAME_REQ == rp->GetCommand())
	{
		PacketComposer.Add(user->GenerateStatCheckSeed());
	}
	else 
	{
		PacketComposer.Add((BYTE)0);
	}
	PacketComposer.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());

	user->Send(&PacketComposer);
}
void CMatchSvrProxy::Process_GameInitializedDataNOT(CReceivePacketBuffer* rp)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SM2G_GAME_INITIALIZED_DATA_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_GAME_INITIALIZED_DATA_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	SAvatarInfo* pAvatar = user->GetCurUsedAvatar();
	if( !pAvatar )
	{
		return;
	}

	int iScaledHeight = 0;
	user->GetScaledHeight(pAvatar->Status.iHeight, iScaledHeight);
	rs.CheckData.iHeight = iScaledHeight;

	user->SetDataForCheckHack(rs.CheckData);
}
void CMatchSvrProxy::Process_ActionSlotRes(CReceivePacketBuffer* rp)
{
	SM2G_ACTION_SLOT_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ACTION_SLOT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_USER_ACTION_SLOT_RES);
		for(int i=0; i<rs.iPlayerNum; i++)
		{
			// 키액션
			for(int j=0; j<MAX_USER_KEYACTION_SLOT; j++)
			{
				PacketComposer.Add(rs.sUserKeyActionSlot[i][j].iActionCode);
				PacketComposer.Add(rs.sUserKeyActionSlot[i][j].iTeamIdx);
				PacketComposer.Add(rs.sUserKeyActionSlot[i][j].iSpecialAvatarIndex);
			}

			// 세리머니
			short sActionCount = 0;
			for(int j=0; j<MAX_USER_ACTION_SEREMONY_COUNT; j++)
			{
				if(rs.sUserActionSeremonySlot[i][j].iActionCode > 0)
				{
					sActionCount++;
				}
			}

			PacketComposer.Add(sActionCount);
			for(short j=0; j<sActionCount; j++)
			{
				if(rs.sUserActionSeremonySlot[i][j].iActionCode > 0)
				{
					PacketComposer.Add(rs.sUserActionSeremonySlot[i][j].btKind);
					PacketComposer.Add(rs.sUserActionSeremonySlot[i][j].iActionCode);
					PacketComposer.Add(rs.sUserActionSeremonySlot[i][j].iTeamIdx);
					PacketComposer.Add(rs.sUserActionSeremonySlot[i][j].iSpecialAvatarIndex);
				}
			}

			// 모션 
// 			short sMotionCount = 0;
// 			for(int j=0; j<MAX_USER_MOTION_SLOT; j++)
// 			{
// 				if(rs.sUserMotionSlot[i][j].iActionCode > 0)
// 				{
// 					sMotionCount++;
// 				}
// 			}
// 
// 			PacketComposer.Add(sMotionCount);
// 			for(short j=0; j<sMotionCount; j++)
// 			{
// 				if(rs.sUserMotionSlot[i][j].iActionCode > 0)
// 				{
// 					PacketComposer.Add(rs.sUserMotionSlot[i][j].iActionCode);
// 					PacketComposer.Add(rs.sUserMotionSlot[i][j].btSceneType);
// 				}
// 			}
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_PlayCountEventInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_PLAYCOUNT_EVENT_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_PLAYCOUNT_EVENT_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_PLAYCOUNT_EVENT_INFO_RES);

		PacketComposer.Add((int)TRUE);
		PacketComposer.Add(rs.iCurrentPlayCount);
		PacketComposer.Add(rs.iNextStepPlayCount);
		PacketComposer.Add(rs.iCurrentStep);
		PacketComposer.Add(rs.iTotalStep);
		PacketComposer.Add(rs.iServeralTimes);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_MacroItemProssession(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_MACRO_ITEM_POSSESSION);
		if(TRUE == user->CheckMacroChatItem())
		{
			PacketComposer.Add((int)1);																
		}
		else
		{
			PacketComposer.Add((int)0);																
		}
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_InitStartGame(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->InitStartGame();
	}
}

void CMatchSvrProxy::Process_LoadGameReq(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_LOADGAME_REQ);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_LoadingProgressRes(CReceivePacketBuffer* rp)
{
	SM2G_LOADING_PROGRESS_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_LOADING_PROGRESS_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_LOADING_PROGRESS_RES);
		PacketComposer.Add((BYTE*)rs.GameID, MAX_GAMEID_LENGTH+1);
		PacketComposer.Add(rs.iLoadProgress);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_StartGameAsObserverReq(CReceivePacketBuffer* rp)
{
	SM2G_STARTGAME_AS_OBSERVER_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_STARTGAME_AS_OBSERVER_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CFSGameClient* pClient = (CFSGameClient*)user->GetClient();
		if(NULL != pClient)
		{
			pClient->SetState(NEXUS_GAMEROOM);
		}

		CPacketComposer PacketComposer(S2C_STARTGAME_AS_OBSERVER_REQ);
		PacketComposer.Add(rs.iResult);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_StatCheckPacket(CReceivePacketBuffer* rp)
{
	SM2G_STAT_CHECK_PACKET rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_STAT_CHECK_PACKET));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(FALSE == user->IsHackUser())
		{
			if(0 != user->GetCheckStatPacketNo())
			{
				user->SetHackUser(TRUE);

				HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, user.GetPointer(), "StatCheck: No response");
			}
			else
			{
				int iCheckPacketNo;

				if (ROOM_TYPE_PRACTICE == rs.iRoomType)
				{
					iCheckPacketNo = S2C_CHECK_AVATAR_STATUS_REQ;
				}
				else
				{
					int iaPacket[] = {S2C_CHECK_AVATAR_STATUS_REQ, S2C_CHECK_AVATAR_STATUS1_REQ, S2C_CHECK_AVATAR_STATUS2_REQ};
					int iPacketCount = sizeof(iaPacket)/sizeof(int);
					int iPacketIndex = rand()10752790PacketCount;
CheckPacketNo = iaPacket[iPacketIndex];
				}

				user->SetCheckStatPacketNo(iCheckPacketNo);
				user->SetCheckStatLastSendPacketNo(iCheckPacketNo);

				CPacketComposer PacketComposer(iCheckPacketNo);
				
				user->UpdateRand_Rc4Key(HACK_TYPE_CHANGE_STATUS);
				PacketComposer.Add((PBYTE)user->GetRc4Key(), RC4_KEY_LENGTH+1);
				
				user->Send(&PacketComposer);
			}
		}
	}
}

void CMatchSvrProxy::Process_StatCheckSumPacket( CReceivePacketBuffer* rp )
{
	SM2G_STAT_CHECK_PACKET rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_STAT_CHECK_PACKET));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if( FALSE == user->IsHackUser() )
		{
			if(0 != user->GetCheckStatCheckSumPacketNo())
			{
				user->SetHackUser(TRUE);

				HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_CHECKSUM, user.GetPointer(), "Stat CheckSum : no response");
			}
			else
			{
				int iCheckPacketNo;
				int iCheckStatLastSendPacket = user->GetCheckStatLastSendPacketNo();

				if (ROOM_TYPE_PRACTICE == rs.iRoomType)
				{
					iCheckPacketNo = S2C_CHECK_STATUS_CHECKSUM_REQ;
				}
				else
				{
					switch( iCheckStatLastSendPacket )
					{
					case S2C_CHECK_AVATAR_STATUS_REQ:	iCheckPacketNo = S2C_CHECK_STATUS_CHECKSUM_REQ;		break;
					case S2C_CHECK_AVATAR_STATUS1_REQ:	iCheckPacketNo = S2C_CHECK_STATUS_CHECKSUM1_REQ;	break;
					case S2C_CHECK_AVATAR_STATUS2_REQ:	iCheckPacketNo = S2C_CHECK_STATUS_CHECKSUM2_REQ;	break;
					default :	return;
					}
				}

				user->SetCheckStatCheckSumPacketNo(iCheckPacketNo);

				CPacketComposer PacketComposer(iCheckPacketNo);

				user->UpdateRand_Rc4Key(HACK_TYPE_CHANGE_CHECKSUM);
				PacketComposer.Add((PBYTE)user->GetChecksum_Rc4Key(), RC4_KEY_LENGTH+1);
				user->Send(&PacketComposer);
			}
		}
	}
}

void CMatchSvrProxy::Process_GiveupEnableRes(CReceivePacketBuffer* rp)
{
	SM2G_GIVEUP_ENABLE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GIVEUP_ENABLE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GIVEUP_ENABLE_RES);
		PacketComposer.Add(rs.shResult);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ThumbnailRecord(CReceivePacketBuffer* rp)
{
	SM2G_THUMBNAIL_RECORD rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_THUMBNAIL_RECORD));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_THUMBNAIL_RECORD);
		PacketComposer.Add((BYTE*)rs.szGameID, MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add(rs.iLv);
		PacketComposer.Add(rs.iGamePosition);
		PacketComposer.Add(rs.iCategory);
		PacketComposer.Add(rs.iRecord);
		PacketComposer.Add((int)0); //ToDo - delete league emblem
		PacketComposer.Add(rs.iEquippedAchievementTitle);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_DisconnectClient(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->GetClient()->DisconnectClient();
	}
}

void CMatchSvrProxy::Process_GameResultRes(CReceivePacketBuffer* rp)
{
	SM2G_GAME_RESULT_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GAME_RESULT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SetAvailablePointRewardCard(TRUE);

		BOOL bCompletedPremiumEventMission = FALSE;

		CPacketComposer PacketComposer(S2C_GAME_RESULT_RES);
		PacketComposer.Add(rs.shMode);
		PacketComposer.Add(rs.iMatchType);

		int iGamePlayerNum = rs.shMemberNum[TEAM_INDEX_HOME]+rs.shMemberNum[TEAM_INDEX_AWAY];

		if(0 == rs.shMode)
		{
			PacketComposer.Add(rs.shHomeScore);
			PacketComposer.Add(rs.shAwayScore);
			int iIdx = 0;
			for(int i=0; i< MAX_TEAM_NUM; i++)
			{
				PacketComposer.Add((PBYTE)rs.szName[i], sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
				PacketComposer.Add(rs.shMemberNum[i]);
				for(int j=0; j<rs.shMemberNum[i]; j++)
				{
					PacketComposer.Add((PBYTE)rs.szGameID[iIdx], MAX_GAMEID_LENGTH+1);
					PacketComposer.Add(rs.shPoint[iIdx]);
					PacketComposer.Add(rs.shRebound[iIdx]);
					PacketComposer.Add(rs.shAsist[iIdx]);
					PacketComposer.Add(rs.shSteal[iIdx]);
					PacketComposer.Add(rs.shBlock[iIdx]);
					PacketComposer.Add(rs.shEstPoint[iIdx]);
					PacketComposer.Add(rs.shLooseBall[iIdx]);

					iIdx++;
				}
			}
		}
		else if(1 == rs.shMode)
		{
			int iPreLv = user->GetCurUsedAvtarLv();
			user->UpdateGameRecord( rs.bUpdateGameRecord, rs.bActiveProtectRing, rs.bActiveProtectHandRecord,
				rs.iDisconnectPoint, rs.GameRecord, iGamePlayerNum, rs.iaStat, rs.ucRecordType, rs.btReversalFactionScore);
			user->SetIsLastMatchDisconnected(rs.bIsDisconnectedMatch);
			user->SetLeftSeatStatus(rs.btLeftSeatStatus);

			PacketComposer.Add(rs.iMentorPopup);
			PacketComposer.Add(rs.shHomeScore);
			PacketComposer.Add(rs.shAwayScore);
			PacketComposer.Add(rs.btReventMatchingType);

			user->SetRoomRevengeMatchingType(rs.btReventMatchingType);

			int iIdx = 0;
			int iRewardMultiple[MAX_ROOM_MEMBER_NUM];
			int iDoubleRewardEventMultiple[MAX_ROOM_MEMBER_NUM];
			ZeroMemory(iRewardMultiple, sizeof(int)*MAX_ROOM_MEMBER_NUM);
			ZeroMemory(iDoubleRewardEventMultiple, sizeof(int)*MAX_ROOM_MEMBER_NUM);
			OPEN_STATUS bIsDoubleRewardEvent = EVENTDATEMANAGER.IsOpen(EVENT_KIND_GAMERESULT_DOUBLE_REWARD);

			for(int i=0; i< MAX_TEAM_NUM; i++)
			{
				for(int j=0; j<rs.shMemberNum[i]; j++)
				{
					int iMainRewardIndex = 0;
					for(int k = 0 ; k < MAX_GAMERESULT_REWARD_COUNT && k < rs.btRewardCount[j]; ++k )
					{
						if(GAME_RESULT_REWARD_TYPE_MAIN_REWARD == rs.btGameResultRewardType[j][k])
						{
							iMainRewardIndex = k;
							break;
						}
					}

					// 농구공 더블보상
					if((rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_PVE || rs.iMatchType == MATCH_TYPE_AIPVP ||
						rs.iMatchType == MATCH_TYPE_PVE_3CENTER || rs.iMatchType == MATCH_TYPE_PVE_CHANGE_GRADE || rs.iMatchType == MATCH_TYPE_PVE_AFOOTBALL || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE) &&
						BASKETBALL_EFFECT_TYPE_GAME_RESULT_REWARD_DOUBLE == rs.btaBallPartEffectType[iIdx] &&
						(REWARD_TYPE_POINT == rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].btRewardType || REWARD_TYPE_EXHAUST_ITEM_TO_INVEN == rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].btRewardType
						|| REWARD_TYPE_COACH_CARD == rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].btRewardType))
					{
						iRewardMultiple[iIdx] = (iRewardMultiple[iIdx] <= 0) ? 1 : iRewardMultiple[iIdx];
						iRewardMultiple[iIdx] *= 2;

						rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].iPropertyValue *= 2;
					}

					if((rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || /*rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE ||*/
						rs.iMatchType == MATCH_TYPE_PVE_3CENTER || rs.iMatchType == MATCH_TYPE_PVE_CHANGE_GRADE || rs.iMatchType == MATCH_TYPE_PVE_AFOOTBALL) &&
						OPEN == bIsDoubleRewardEvent &&
						(REWARD_TYPE_POINT == rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].btRewardType || REWARD_TYPE_EXHAUST_ITEM_TO_INVEN == rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].btRewardType
						|| REWARD_TYPE_COACH_CARD == rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].btRewardType))
					{
						iRewardMultiple[iIdx] = (iRewardMultiple[iIdx] <= 0) ? 1 : iRewardMultiple[iIdx];
						iRewardMultiple[iIdx] *= 2;

						iDoubleRewardEventMultiple[iIdx] = (iDoubleRewardEventMultiple[iIdx] <= 0) ? 1 : iDoubleRewardEventMultiple[iIdx];
						iDoubleRewardEventMultiple[iIdx] *= 2;

						rs.sGameResultRewardInfo[iIdx][iMainRewardIndex].iPropertyValue *= 2;
					}
					iIdx++;
				}
			}

			BYTE btMaterialType = 0; 
			int iAddMaterialCount = 0;
			int iaMaterialCount[MAX_SHOPPING_MISSION_MATERIAL_TYPE];
			ZeroMemory(iaMaterialCount, sizeof(int)*MAX_SHOPPING_MISSION_MATERIAL_TYPE);

			int iaGetMaterial[MissionMakeItemEvent::MAX_MATERIAL_TYPE];
			ZeroMemory(iaGetMaterial, sizeof(int)*MissionMakeItemEvent::MAX_MATERIAL_TYPE);

			iIdx = 0;
			int iMyIdx = 0;
			for(int i=0; i< MAX_TEAM_NUM; i++)
			{
				PacketComposer.Add((PBYTE)rs.szClubName[iIdx], MAX_CLUB_NAME_LENGTH+1);
				PacketComposer.Add(rs.iClubMarkCode[iIdx]);
				PacketComposer.Add((PBYTE)rs.szName[i], sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
				PacketComposer.Add(rs.shMemberNum[i]);
				
				for(int j=0; j<rs.shMemberNum[i]; j++)
				{
					if (0 == strcmp(user->GetGameID(), rs.szGameID[iIdx]))
					{
						iMyIdx = iIdx;
						for(int k = 0 ; k < MAX_GAMERESULT_REWARD_COUNT && k < rs.btRewardCount[iMyIdx]; ++k)
						{
							if(GAME_RESULT_REWARD_TYPE_CLUB_EVENT == rs.btGameResultRewardType[iMyIdx][k])
								continue;

							if(GAME_RESULT_REWARD_TYPE_NONE < rs.btGameResultRewardType[iMyIdx][k])
							{
								// give reward 
								int iAddValue = GAME_RESULT_REWARD_TYPE_MAIN_REWARD == rs.btGameResultRewardType[iMyIdx][k] ? iRewardMultiple[iIdx] : 0;

								if(nullptr == REWARDMANAGER.GiveReward(user.GetPointer(), rs.sGameResultRewardInfo[iMyIdx][k].iRewardIndex, iAddValue))
								{
									rs.btGameResultRewardType[iMyIdx][k] = 0;
									rs.sGameResultRewardInfo[iMyIdx][k].iRewardIndex = 0;
									rs.sGameResultRewardInfo[iMyIdx][k].btRewardType = 0;
									rs.sGameResultRewardInfo[iMyIdx][k].iPropertyValue = 0;
								}
								else if(rs.sGameResultRewardInfo[iMyIdx][k].iRewardIndex == HOTGIRLTIMEManager.GetHotGirlTimeMainRewardIndex())
								{
									CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
									if(pODBCBase)
									{
										pODBCBase->EVENT_HOTGIRLTIME_InsertUserRewardLog(user->GetUserIDIndex(), user->GetGameIDIndex(), rs.sGameResultRewardInfo[iMyIdx][k].iRewardIndex, rs.iMatchType, rs.btMatchScaleType);
									}
								}
							}
						}
																		
						user->GetUserBasketBall()->CheckAndRemoveExpireBallPartSlot();

						if(rs.iMatchType == MATCH_TYPE_RATING && rs.shGameRecordWin == 0)
							user->GetUserIntensivePractice()->UpdateReadyGiveReward(rs.iaLoseContinue[i], rs.shHomeScore-rs.shAwayScore);

						if(rs.iMatchType == MATCH_TYPE_RATING && rs.shMemberNum[i] == NUM_PLAYER_3ON3 &&
							TRUE == user->CheckMatchingPoolCareUser())
						{
							user->AddMatchingPoolCarePlayCount();
							if(TRUE == user->CheckAndRemoveMatchingPoolCareTarget())
							{
								CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
								if(pCenter != NULL) 
								{
									SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ ss;
									ss.iGameIDIndex = user->GetGameIDIndex();
									strncpy_s(ss.GameID, _countof(ss.GameID), user->GetGameID(), MAX_GAMEID_LENGTH);
									ss.btUpdateType = MATCHINGPOOLCARE_UPDATE_TARGETUSER_TYPE_REMOVE;
									ss.btLvGrade = user->GetCurUsedAvtarLvGrade();
									pCenter->SendPacket(G2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ, &ss, sizeof(SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ));
								}
							}

							SS2C_MATCHINGPOOLCARE_REMAIN_PLAYCOUNT_NOTICE info;
							info.iRemainPlayCount = user->GetMatchingPoolCareRemainPlayCount();
							user->Send(S2C_MATCHINGPOOLCARE_REMAIN_PLAYCOUNT_NOTICE, &info, sizeof(SS2C_MATCHINGPOOLCARE_REMAIN_PLAYCOUNT_NOTICE));
						}

						// 리벤지 매칭
						if(rs.iaRevengeMatchingRewardIndex[iIdx] > 0)
						{
							SRewardConfig* pReward = REWARDMANAGER.GiveReward(user.GetPointer(), rs.iaRevengeMatchingRewardIndex[iIdx]);

							if(pReward && rs.btReventMatchingType == RevengeMatching::REVENGE_MATCHING_TYPE_REVENGE_REWARD_INFO)
							{
								user->CheckNeedPauseAndAIComePopUp();
							}
						}

						if( rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE)
						{
							if( TRUE == rs.btFriendInviteBonus || TRUE == rs.btFriendInviteWithGame)
							{
								user->GetUserFriendInvite()->UpdateFriendInviteMission(FRIEND_INVITE_MISSION_INDEX_WITH_FRIEND_GAMEPLAY);
							}

							user->GetUserFriendInvite()->UpdateFriendInviteKingExp(rs.GameRecord.iaGameRecord[AVATAR_GAME_RECORD_EXP]);
							user->GetUserFriendInvite()->UpdateFriendInviteMission(FRIEND_INVITE_MISSION_INDEX_GAMEPLAY_RACE);
							user->GetUserFriendInvite()->CheckAndGiveFriendBenefit(FRIEND_INVITE_BENEFIT_INDEX_POINT);

							if( FALSE == rs.bRunAway && FALSE == rs.bIsDisconnectedMatch)
							{
								if( rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3  || rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3CLUB)
								{
									WHITEDAY.CheckAndIncreaseGamePlayCount(user.GetPointer());

									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_PLAY_COUNT, 1, TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_PLAY_TOTAL_GET_POINT, rs.shPoint[iIdx], TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_ONE_PLAY_TOTAL_GET_POINT, rs.shPoint[iIdx], TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_BLOCK_COUNT, rs.shBlock[iIdx], TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_ASSIST_COUNT, rs.shAsist[iIdx], TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_2POINT_SHOOT_COUNT, rs.sh2Point[iIdx]/2, TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_3POINT_SHOOT_COUNT, rs.sh3Point[iIdx]/3, TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_BLOCK_OR_LOSEBALL_TOTAL_COUNT, rs.shBlock[iIdx]+rs.shLooseBall[iIdx], TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_REBOUND_OR_POINT_TOTAL_COUNT, rs.shRebound[iIdx]+(rs.sh2Point[iIdx]/2)+(rs.sh3Point[iIdx]/3), TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_REBOUND_OR_ASSIST_TOTAL_COUNT, rs.shRebound[iIdx]+rs.shAsist[iIdx], TRUE, bCompletedPremiumEventMission);
									bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_PLAY_RESULT_A_SOME_GRADE_COMPLETED_COUNT, rs.shEstPoint[iIdx], TRUE, bCompletedPremiumEventMission);

									if(POSITION_CODE_SMALL_FOWARD == rs.iGamePosition[iIdx] ||
										POSITION_CODE_SHOOT_GUARD == rs.iGamePosition[iIdx] ||
										POSITION_CODE_SWING_MAN == rs.iGamePosition[iIdx])
										bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_PLAY_SF_OR_SG_OR_SW_POSITION_PLAYCOUNT, 1, TRUE, bCompletedPremiumEventMission);

									if(strcmp(rs.szPOGGameID, user->GetGameID()) == 0)
									{
										bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_POG_COUNT, 1, TRUE, bCompletedPremiumEventMission);
									}				
								}
								user->GetUserComebackBenefit()->IncreaseGamePlay();

								user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_RK_ELO_COUNT);
								user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_LOOSEBALL_COUNT, rs.shLooseBall[iIdx]);
								user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_POINT_REBOUND_COUNT, rs.shPoint[iIdx] + rs.shRebound[iIdx]);
								user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_BLOCK_STEEL_COUNT, rs.shBlock[iIdx] + rs.shSteal[iIdx]);

								if(TRUE == user->GetUserShoppingEvent()->CheckAndUpdateUserMission(SHOPPING_MISSION_TYPE_ELO_PLAY, btMaterialType, iAddMaterialCount))
									iaMaterialCount[btMaterialType] = iAddMaterialCount;

								if(TRUE == user->GetUserMissionMakeItemEvent()->CheckAndUpdateMission(MISSION_CONDITION_TYPE_PLAY_CNT, btMaterialType, iAddMaterialCount))
									iaGetMaterial[btMaterialType] = iAddMaterialCount;

								user->GetUserDriveMissionEvent()->CheckUpdateMission(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_GET_BETTER_THAN_PLAY_GRADE_A, rs.shEstPoint[iIdx]);
								user->GetUserDriveMissionEvent()->CheckUpdateMission(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_PLAY_CNT);

								user->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_RATING_MODE_PLAY);
								user->GetUserHotGirlMissionEvent()->CheckAndUpdateBonusMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_RATING_MODE_PLAY);

								user->GetUserRandomItemDrawEvent()->CheckAndUpdateGamePlay();

								user->GetUserGamePlayLoginEvent()->UpdateGameResultRewardStatus();

								if(1 == rs.shGameRecordWin)
								{
									user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_ELO_MODE_GAMEPLAY_WIN_COUNT);

									if(TRUE == user->GetUserShoppingEvent()->CheckAndUpdateUserMission(SHOPPING_MISSION_TYPE_ELO_WIN, btMaterialType, iAddMaterialCount))
										iaMaterialCount[btMaterialType] = iAddMaterialCount;

									if(TRUE == user->GetUserMissionMakeItemEvent()->CheckAndUpdateMission(MISSION_CONDITION_TYPE_PLAY_WIN, btMaterialType, iAddMaterialCount))
										iaGetMaterial[btMaterialType] = iAddMaterialCount;

									user->GetUserDriveMissionEvent()->CheckUpdateMission(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_PLAY_WIN_CNT);

									user->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_RATING_MODE_WIN);
									user->GetUserHotGirlMissionEvent()->CheckAndUpdateBonusMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_RATING_MODE_WIN);

									if( rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3  || rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3CLUB)
									{
										bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_WIN_PLAY_COUNT, 1, TRUE, bCompletedPremiumEventMission);
										bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_PLAY_GAP_A_SOME_LESS_THEN_POINT_WIN_COUNT, abs(rs.shHomeScore-rs.shAwayScore), TRUE, bCompletedPremiumEventMission);
										bCompletedPremiumEventMission = user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_PLAY_GAP_A_SOME_MORE_THEN_POINT_WIN_COUNT, abs(rs.shHomeScore-rs.shAwayScore), TRUE, bCompletedPremiumEventMission);							
									}
								}
							}

							user->GetUserCovet()->AddCovetJinn();
							user->CheckAndUpdatePromisePlayCount();
						}

						if(rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE)
						{
							user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_12CONTINUE_COUNT);
							user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_24CONTINUE_COUNT);
							
							if(1 == rs.shGameRecordWin && TRUE != user->IsUseEquipItem())
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_NOT_POTENCARD_WINCONTINUE);

							const int iCheckCondition = 3;
							if(rs.iaWinContinue[i] == iCheckCondition)
								user->GetUserPuzzle()->GivePuzzle();

							user->GetUserPuzzle()->GivePuzzleFromRewardType(PUZZLE_REWARD_TYPE_RATING_MODE_5PLAYCOUNT);

							user->GetUserThreeKingdomsEvent()->UpdateMissionCount();

							user->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_ELO_PLAY);
							
							user->GetUserHotGirlSpecialBoxEvent()->GivePotion(REWARD_TYPE_NORMAL_POTION);

							FACTION.CheckUserMissionValue(user.GetPointer(), FACTION_MISSION_CONDITION_TYPE_ELO_PLAY_3);
							FACTION.CheckUserMissionValue(user.GetPointer(), FACTION_MISSION_CONDITION_TYPE_ELO_PLAY_10);

							user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_COUNT);
							user->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_ELO_RANK_MODE_PLAY);
							user->GetUserWelcomeUserEvent()->CheckAndUpdateMission(EVENT_WELCOME_USER_MISSION_TYPE_GAMEPLAY_COUNT);
							user->GetUserMissionCashLimitEvent()->CheckAndUpdateMission(static_cast<bool>(rs.shGameRecordWin), rs.bIsDisconnectedMatch, rs.bRunAway);
							user->GetUserColoringPlayEvent()->CheckGamePlayMission(static_cast<BOOL>(rs.shGameRecordWin), rs.bIsDisconnectedMatch, rs.bRunAway);
							user->GetUserCongratulationEvent()->CheckGamePlayMission(rs.bIsDisconnectedMatch, rs.bRunAway, rs.tGameStartTime, static_cast<int>(rs.saExp[GAME_RESULT_REWARD_BONUS_TYPE_BASE]));
							user->GetUserPrivateRoomEvent()->CheckMission(EVENT_PRIVATEROOM_MISSION_TYPE_PLAY_COUNT);

							if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
							{
								user->GetUserBigWheelLoginEvent()->AddPlayCount();
								user->GetUserShoppingGodEvent()->CheckAndUpdateUserMission(SHOPPING_GOD_MISSION_TYPE_ELO_PLAY);
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_1);
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_2);
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_1);
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_2);
								user->GetUserPotionMakingEvent()->CheckMission(SEVENT_POTION_MAKING_MISSION_TYPE_PLAY_COUNT);
							
								if(rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3 || rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3CLUB)
									user->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_COUNT);
							}

							if(GAME_ESTIMATE_AAA == rs.shEstPoint[iIdx] ||
								GAME_ESTIMATE_AA == rs.shEstPoint[iIdx] ||
								GAME_ESTIMATE_A == rs.shEstPoint[iIdx])
							{
								user->GetUserPrivateRoomEvent()->CheckMission(EVENT_PRIVATEROOM_MISSION_TYPE_A_GRADE_COUNT);

								if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
								{
									user->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_A_GRADE_COUNT);
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_A_GRADE_COUNT);
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_A_GRADE_COUNT);
									if(rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3 || rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3CLUB)
									{
										user->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_A_EFF);
									}
								}
							}

							if(1 == rs.shGameRecordWin)
							{
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_3WINCONTINUE, rs.iaWinContinue[i]);
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_5WINCONTINUE, rs.iaWinContinue[i]);
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_2WINCONTINUE, rs.iaWinContinue[i]);

								if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
								{
									user->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_WIN_CONTINUE_COUNT, rs.iaWinContinue[i]);
									user->GetUserDriveMissionEvent()->CheckUpdateMission(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_PLAY_CONTINUE_WIN_CNT, rs.iaWinContinue[i]);
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_WIN_COUNT);
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_WIN_COUNT);
								}

								user->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_ELO_WIN);
								FACTION.CheckUserMissionValue(user.GetPointer(), FACTION_MISSION_CONDITION_TYPE_ELO_WIN);
								user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_WINCOUNT);
								user->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_ELO_RANK_MODE_WIN);

								if(rs.shMemberNum[i] == NUM_PLAYER_3ON3 )
								{
									if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
									{
										if(rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3 || rs.btMatchScaleType == MATCH_TEAM_SCALE_3ON3CLUB)
										{
											user->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_WINCOUNT);
											user->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_WINCONTINUE_COUNT, rs.iaWinContinue[i]);
										}
										
										user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_ELO_3VS3_MODE_GAMEPLAY_WIN_COUNT);	
									}
								}
								
								user->GetUserWelcomeUserEvent()->CheckAndUpdateMission(EVENT_WELCOME_USER_MISSION_TYPE_GAMEPLAY_WIN);
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_5WIN, 1);
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_2WIN, 1);
								user->GetUserPrivateRoomEvent()->CheckMission(EVENT_PRIVATEROOM_MISSION_TYPE_WIN_PLAY_COUNT);
							}
							else
							{
								// 연승횟수 초기화
								user->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_WINCONTINUE_COUNT, 0);
								user->GetUserDriveMissionEvent()->CheckUpdateMission(USER_CHOICE_MISSION_2ND_MISSION_CONDITION_TYPE_PLAY_CONTINUE_WIN_CNT, 0);
							}

							user->GetUserMatchingCard()->CheckAndAddPlayCount();
							user->GetUserWelcomeUserEvent()->GiveGameResultReward(rs.shGameRecordWin);
							user->GetUserPCRoomEvent()->GiveGameResultReward();
						}
						else if(rs.iMatchType == MATCH_TYPE_FREE)
						{
						}
						else if(rs.iMatchType == MATCH_TYPE_RANKMATCH)
						{
							user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_RK_COUNT);
							user->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_ELO_RANK_MODE_PLAY);
							user->GetUserHotGirlSpecialBoxEvent()->GivePotion(REWARD_TYPE_NORMAL_POTION);
							user->GetUserPCRoomEvent()->GiveGameResultReward();
							
							user->GetUserBigWheelLoginEvent()->AddPlayCount();
							user->GetUserMissionCashLimitEvent()->CheckAndUpdateMission(static_cast<bool>(rs.shGameRecordWin), rs.bIsDisconnectedMatch, rs.bRunAway);
							user->GetUserColoringPlayEvent()->CheckGamePlayMission(static_cast<BOOL>(rs.shGameRecordWin), rs.bIsDisconnectedMatch, rs.bRunAway);

							user->GetUserWelcomeUserEvent()->CheckAndUpdateMission(EVENT_WELCOME_USER_MISSION_TYPE_GAMEPLAY_COUNT);
							user->GetUserPrivateRoomEvent()->CheckMission(EVENT_PRIVATEROOM_MISSION_TYPE_PLAY_COUNT);
							
							if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
							{
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_1);
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_2);
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_1);
								user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_COUNT_2);
								user->GetUserPotionMakingEvent()->CheckMission(SEVENT_POTION_MAKING_MISSION_TYPE_PLAY_COUNT);
							}							

							if(GAME_ESTIMATE_AAA == rs.shEstPoint[iIdx] ||
								GAME_ESTIMATE_AA == rs.shEstPoint[iIdx] ||
								GAME_ESTIMATE_A == rs.shEstPoint[iIdx])
							{
								user->GetUserPrivateRoomEvent()->CheckMission(EVENT_PRIVATEROOM_MISSION_TYPE_A_GRADE_COUNT);

								if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
								{
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_A_GRADE_COUNT);
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_A_GRADE_COUNT);
								}
							}

							if(1 == rs.shGameRecordWin)
							{
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_5WIN, 1);
								user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_RATING_MODE_2WIN, 1);

								user->GetUserWelcomeUserEvent()->CheckAndUpdateMission(EVENT_WELCOME_USER_MISSION_TYPE_GAMEPLAY_WIN);
								user->GetUserPrivateRoomEvent()->CheckMission(EVENT_PRIVATEROOM_MISSION_TYPE_WIN_PLAY_COUNT);
								
								if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
								{
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_1, EVENT_STEELBAG_MISSION_TYPE_PLAY_WIN_COUNT);
									user->GetUserSteelBagMissionEvent()->CheckMission(EVENT_STEELBAG_MISSION_STEP_2, EVENT_STEELBAG_MISSION_TYPE_PLAY_WIN_COUNT);
								}								
							}

							user->GetUserWelcomeUserEvent()->GiveGameResultReward(rs.shGameRecordWin);
						}
						else if(rs.iMatchType == MATCH_TYPE_PVE)
						{	
							user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_AI_MODE_GAMEPLAY_COUNT);
						}
						else if(MATCH_TYPE_PVE_3CENTER == rs.iMatchType ||
							MATCH_TYPE_PVE_CHANGE_GRADE == rs.iMatchType ||
							MATCH_TYPE_PVE_AFOOTBALL == rs.iMatchType)
						{
							if(1 == rs.shGameRecordWin)
							{
								user->GetUserPVEMission()->CheckAndUpdateDailyMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT);

								if(POSITION_CENTER == user->GetCurUsedAvatarPosition())
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_CENTER);
								else if(POSITION_POWER_FORWARD == user->GetCurUsedAvatarPosition())
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_POWERFOWARD);
								else if(POSITION_SMALL_FORWARD == user->GetCurUsedAvatarPosition())
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_SMALLFOWARD);
								else if(POSITION_POINT_GUARD== user->GetCurUsedAvatarPosition())
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_POINTGUARD);
								else if(POSITION_SHOOTING_GUARD == user->GetCurUsedAvatarPosition())
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_SHOOTGUARD);
								else if(POSITION_SWING_MAN == user->GetCurUsedAvatarPosition())
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_SWINGMAN);

								if(rs.shSteal[iIdx] > 0)
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_STELL_COUNT, rs.shSteal[iIdx]);

								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_WIN_COUNT);

								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_TOTAL_POINT_LESS_THAN_A_SOME_POINT, rs.shHomeScore+rs.shAwayScore);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_TOTAL_POINT_A_SOME_POINT, rs.shHomeScore+rs.shAwayScore);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_GAP_A_SOME_POINT, abs(rs.shHomeScore-rs.shAwayScore));

								BOOL bALLSamePositionTeam = TRUE;
								int iTeamMemberNum = 0, iMemberNum = 0, iDiffTeamReboundCount = 0, iDiffTeamBlockCount = 0;
								int iBlockCount[NUM_PLAYER_3ON3], i3PointCount[NUM_PLAYER_3ON3], i2PointCount[NUM_PLAYER_3ON3], iDunkCount[NUM_PLAYER_3ON3], iStealCount[NUM_PLAYER_3ON3], iReboundCount[NUM_PLAYER_3ON3];
								ZeroMemory(iBlockCount, sizeof(int)*NUM_PLAYER_3ON3);
								ZeroMemory(i3PointCount, sizeof(int)*NUM_PLAYER_3ON3);
								ZeroMemory(i2PointCount, sizeof(int)*NUM_PLAYER_3ON3);
								ZeroMemory(iDunkCount, sizeof(int)*NUM_PLAYER_3ON3);
								ZeroMemory(iStealCount, sizeof(int)*NUM_PLAYER_3ON3);
								ZeroMemory(iReboundCount, sizeof(int)*NUM_PLAYER_3ON3);

								for(int iNum = 0; iNum < MAX_ROOM_MEMBER_NUM; ++iNum)
								{
									if(rs.iTeamIndex[iNum] == rs.iTeamIndex[iIdx])
									{
										if(rs.iGamePosition[iNum] != rs.iGamePosition[iIdx])
										{
											bALLSamePositionTeam = FALSE;
										}

										if(0 < rs.iaGameIDIndex[iNum]) // kor 
											iTeamMemberNum++;

										if(iMemberNum < NUM_PLAYER_3ON3)
										{
											iBlockCount[iMemberNum] = rs.shBlock[iNum];
											i3PointCount[iMemberNum] = rs.sh3Point[iNum];
											i2PointCount[iMemberNum] = rs.sh2Point[iNum];
											iDunkCount[iMemberNum] = rs.shDunk[iNum];
											iStealCount[iMemberNum] = rs.shSteal[iNum];
											iReboundCount[iMemberNum] = rs.shRebound[iNum];

											iMemberNum++;
										}
									}
									else
									{
										if(rs.shRebound[iNum] > 0)
											iDiffTeamReboundCount += rs.shRebound[iNum];

										if(rs.shBlock[iNum] > 0)
											iDiffTeamBlockCount += rs.shBlock[iNum];
									}
								}
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_AWAY_LESS_THAN_A_SOME_REBOUND_COUNT, iDiffTeamReboundCount);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_AWAY_LESS_THAN_A_SOME_BLOCK_COUNT, iDiffTeamBlockCount);

								if(TRUE == bALLSamePositionTeam)
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_SAME_POSITON_TEAM);

								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_1_USER, iTeamMemberNum);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_COUNT_3_USER, iTeamMemberNum);

								SHORT sScore = rs.shHomeScore;
								if(TEAM_INDEX_AWAY == rs.iTeamIndex[iIdx])
									sScore = rs.shAwayScore;

								if(TRUE == rs.bNotLoseAPoint)
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_GAME_START_A_SOME_TIME_NOT_LOSE_A_POINT, 0);

								if(TRUE == rs.bExtraGame)
									user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_OVERTIME);

								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_CONTINUE_DUNK_SHOOT_COUNT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_DUNK]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_CONTINUE_GOAL_SHOOT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_GOAL_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_CONTINUE_RAYUP, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_RAYUP]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_CONTINUE_MIDDLE_SHOOT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_MIDDLE_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_CONTINUE_3POINT_SHOOT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_3POINT_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_SUCCESS_CONTINUE_SHOOT_TOTAL_POINT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_NOT_FALL_DOWN, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_NOT_FALL_DOWN]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_NOT_DROP_BALL, rs.iaMissionGameRecord[MISSION_GAME_RECORD_CONTINUE_NOT_DROP_BALL]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_HALF_LINE_SHOOT_COUNT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_HALF_LINE_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_NOT_SUFFER_STEAL, rs.iaMissionGameRecord[MISSION_GAME_RECORD_SUFFER_STEAL]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_SUCCESS_REBOUND_COUNT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_REBOUND]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_SUCCESS_CLOSEUP_SHOOT_COUNT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_CLOSEUP_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_FALL_DOWN_COUNT, rs.iaMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_FALL_DOWN]);

								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_NOT_WEAR_PROPERTY_ITEM, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_NOT_WEAR_PROPERTY_ITEM]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_DUNK_SHOOT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_ONLY_USE_DUNK_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_GOAL_SHOOT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_ONLY_USE_GOAL_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_RAYUP, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_ONLY_USE_RAYUP]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_SUCCESS_BLOCK_CATCH_COUNT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_BLOCK_CATCH_COUNT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_SUCCESS_ALLEYOOP_DUNK_COUNT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_ALLEYOOP_DUNK_COUNT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_SUCCESS_CONTINUE_CHEAPOUT_COUNT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_CONTINUE_CHEAPOUT_COUNT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEP1_WIN_PLAY_SHOOT_TOTAL_POINT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_CHANGE_GRADE_MODE_STEP1_SHOOT_POINT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEP1_WIN_PLAY_LESS_A_SOME_TOTAL_POINT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_CHANGE_GRADE_MODE_STEP1_SHOOT_POINT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEM2_WIN_PLAY_KISSTHERING_BLOCK_COUNT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_KISSTHERING_BLOCK]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEM2_WIN_PLAY_AN_IRON_WILL_BLOCK_COUNT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_AN_IRON_WILL_BLOCK]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEM2_WIN_PLAY_BEHIND_3POINT_BLOCK_COUNT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_BEHIND_3POINT_BLOCK]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEM2_WIN_PLAY_LESS_A_SOME_POINT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_SUCCESS_CHANGE_GRADE_MODE_STEP2_SHOOT_POINT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_MIDDLE_SHOOT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_ONLY_USE_MIDDLE_SHOOT]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_3POINT_SHOOT, rs.iaTeamMissionGameRecord[MISSION_GAME_RECORD_ONLY_USE_3POINT]);

								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_BLOCK_COUNT, iBlockCount[0], iBlockCount[1], iBlockCount[2]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_3POINT_SHOOT_COUNT, i3PointCount[0], i3PointCount[1], i3PointCount[2]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_2POINT_SHOOT_COUNT, i2PointCount[0], i2PointCount[1], i2PointCount[2]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_DUNK_SHOOT_COUNT, iDunkCount[0], iDunkCount[1], iDunkCount[2]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_STELL_COUNT, iStealCount[0], iStealCount[1], iStealCount[2]);
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_WIN_PLAY_REBOUND_COUNT, iReboundCount[0], iReboundCount[1], iReboundCount[2]);
							}

							BOOL bGetAllLooseBall = TRUE;
							for(int iNum = 0; iNum < MAX_ROOM_MEMBER_NUM; ++iNum)
							{
								if(iNum != iIdx &&
									rs.shLooseBall[iNum] > 0)
								{
									bGetAllLooseBall = FALSE;
									break;
								}
							}
							if(TRUE == bGetAllLooseBall)
								user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_ALL_GET_LOOSEBALL);

							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_POINT, rs.shPoint[iIdx]);
							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_3POINT, rs.sh3Point[iIdx]);
							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_ASSIST, rs.shAsist[iIdx]);
							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_2POINT, rs.sh2Point[iIdx]);
							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_REBOUND, rs.shRebound[iIdx]);
							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_BLOCK, rs.shBlock[iIdx]);
							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_TOTAL_STELL, rs.shSteal[iIdx]);

							user->GetUserPVEMission()->CheckAndUpdateMission(GetMatchType(), rs.btPVEEOD, PVE_MISSION_TYPE_MISSION_COMPLETED_COUNT);
						}
					}

					PacketComposer.Add(rs.btServerIndex[iIdx]);
					PacketComposer.Add(rs.iaGameIDIndex[iIdx]);
					PacketComposer.Add((PBYTE)rs.szGameID[iIdx], MAX_GAMEID_LENGTH+1);
					PacketComposer.Add(rs.sh2Point[iIdx]);
					PacketComposer.Add(rs.sh3Point[iIdx]);
					PacketComposer.Add(rs.shOffenceRebound[iIdx]);
					PacketComposer.Add(rs.shDefenceRebound[iIdx]);
					PacketComposer.Add(rs.shAsist[iIdx]);
					PacketComposer.Add(rs.shSteal[iIdx]);
					PacketComposer.Add(rs.shBlock[iIdx]);
					PacketComposer.Add(rs.shEstPoint[iIdx]);
					PacketComposer.Add(rs.shLooseBall[iIdx]);
					PacketComposer.Add(rs.iaEventImageIndex[iIdx]);

					PacketComposer.Add(rs.iaBallPartBackground[iIdx]);
					PacketComposer.Add(rs.iaBallPartPattern[iIdx]);
					PacketComposer.Add(rs.iaBallPartEffect[iIdx]);

					if(rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH)
						PacketComposer.Add(iDoubleRewardEventMultiple[iIdx]);
					else
						PacketComposer.Add((int)1);

					iIdx++;
				}
			}

			if(iMyIdx >= MAX_ROOM_MEMBER_NUM || iMyIdx < 0)
			{	
				WRITE_LOG_NEW(LOG_TYPE_MATCH, GAME_END, FAIL, "iMyIdx <> MAX ROOM MEMBER CNT - iMyIdx:10752790", iMyIdx);
turn; // err
			}

			PacketComposer.Add(rs.btRewardCount[iMyIdx]);
			SREWARD_INFO info;
			for(int k = 0 ; k < rs.btRewardCount[iMyIdx] && k < MAX_GAMERESULT_REWARD_COUNT; ++k)
			{
				PacketComposer.Add(rs.btGameResultRewardType[iMyIdx][k]);

				info.btRewardType = rs.sGameResultRewardInfo[iMyIdx][k].btRewardType;
				info.iItemCode = rs.sGameResultRewardInfo[iMyIdx][k].iItemCode;
				info.iPropertyType = rs.sGameResultRewardInfo[iMyIdx][k].iPropertyType;
				info.iPropertyValue = rs.sGameResultRewardInfo[iMyIdx][k].iPropertyValue;

				PacketComposer.Add((PBYTE)&info, sizeof(SREWARD_INFO));
			}

			PacketComposer.Add(rs.shGameRecordWin); 
			PacketComposer.Add(rs.shTournamentRound);
			
			PacketComposer.Add((PBYTE)rs.saPoint, sizeof(SHORT)*MAX_GAME_RESULT_REWARD_BONUS_TYPE);
			PacketComposer.Add((PBYTE)rs.saExp, sizeof(SHORT)*MAX_GAME_RESULT_REWARD_BONUS_TYPE);
			PacketComposer.Add((PBYTE)rs.saFame, sizeof(SHORT)*MAX_GAME_RESULT_REWARD_BONUS_TYPE);	

			PacketComposer.Add(rs.iPoint);
			PacketComposer.Add(rs.iExp);

			PacketComposer.Add(rs.iCurrentExp);
			PacketComposer.Add(rs.iMinExp);
			PacketComposer.Add(rs.iMaxExp);		
			PacketComposer.Add(rs.byIsLvUp);

			if(TRUE == (BOOL)rs.byIsLvUp)
			{
				PacketComposer.Add(rs.iNextMaxExp); 
				PacketComposer.Add(rs.iLv);
				PacketComposer.Add(rs.iChannelMaxLevel);
				PacketComposer.Add(rs.iStatUpNum);

				for(int i = 0; i < MAX_STAT_NUM; i++)
				{
					if(0 != rs.iaStat[i])
					{
						PacketComposer.Add((short)i);
						PacketComposer.Add((short)rs.iaStat[i]);
					}
				}

				user->ProcessLevelUp(user->GetGameIDIndex(), user->GetCurUsedAvatarPosition(), iPreLv, rs.iLv, user->GetCurUsedAvtarExp(), TRUE);

				if( RANKMATCH.GetConfigValue(LEAGUE_CONFIG_PageRankPositionLimitLevel) == rs.iLv )
					user->GetUserRankMatch()->Load(); // 레벨업시 한번만 로드.
			}

			//! Sub Char Develop
			int iSubCharDevelopExp = 0;
			if((MATCH_TYPE_RATING == rs.iMatchType || MATCH_TYPE_RANKMATCH == rs.iMatchType || MATCH_TYPE_CLUB_LEAGUE == rs.iMatchType) && 
				FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
			{
				iSubCharDevelopExp = user->GetUserSubCharDevelop()->AddExp(rs.iLv, user->GetCurUsedAvatarPosition(),
					static_cast<int>(rs.saExp[GAME_RESULT_REWARD_BONUS_TYPE_BASE]), rs.shEstPoint[iMyIdx]);
			}
			PacketComposer.Add(iSubCharDevelopExp);

			PacketComposer.Add(rs.iFamePoint);
			PacketComposer.Add(rs.iCurrentFamePoint);
			PacketComposer.Add(rs.iMinFamePoint);
			PacketComposer.Add(rs.iMaxFamePoint);	
			PacketComposer.Add(rs.byIsFameLevelUp); 	

			if(TRUE == (BOOL)rs.byIsFameLevelUp)
			{
				PacketComposer.Add(rs.iNextMaxFamePoint); 
				PacketComposer.Add(rs.iFameLevel);

				user->SendCurAvatarSkillPoint(user->GetCurAvatarRemainSkillPoint(), user->GetCurAvatarUsedSkillPointAllType(), user->IsPlaying());
			}

			PacketComposer.Add((PBYTE)rs.szPOGGameID, MAX_GAMEID_LENGTH+1);
			PacketComposer.Add(rs.iProtectRing);

			PacketComposer.Add(rs.btClubContributionPoint); 
			PacketComposer.Add(rs.shClubContributionPointTeamBonusRate); 
			PacketComposer.Add(rs.shClubContributionPointEventBonusRate);
			PacketComposer.Add((BYTE)rs.GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FACTION_SCORE]);
			PacketComposer.Add((SHORT)rs.GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FACTION_POINT]);
			PacketComposer.Add((BYTE)rs.btReversalFactionScore);	// Reversal benefit Score
			
			int iCurrentFactionScore = (user->GetUserFaction()->_iTotalFactionScore);
			int iCurrentFactionPoint = (user->GetUserFaction()->_iFactionPoint);

			PacketComposer.Add(iCurrentFactionScore);	// Current Faction Score
			PacketComposer.Add(iCurrentFactionPoint);	// Current Faction Point

			if( 0 < rs.btReversalFactionScore || 0 < user->GetUserFaction()->_iReversalRate)
			{
				FACTION.UpdateFactionUserReversalDB((CUser*)user.GetPointer(), FALSE);
			}
			if(TRUE == user->GetUserFaction()->_bCheckFristGiveReward)
			{
				user->CheckAndGiveFactionDailyGameReward();
			}

			PacketComposer.Add(rs.iRemainEventGameCount);

			int iPointCardCost = REWARDCARDMANAGER.GetPointCardCost();
			if ((rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE) &&
				EVENTDATEMANAGER.IsOpen(EVENT_KIND_POINTCARD) == OPEN)
				iPointCardCost = 0;
			PacketComposer.Add(iPointCardCost);

			PacketComposer.Add(rs.iSupportGrowthExpBonus);

			// GAEMPLAY EVENT
			BYTE btNoticeStatus = user->GetUserGamePlayEvent()->GetGameResultNoticeStatus(rs.iMatchType, rs.bIsDisconnectedMatch, rs.bRunAway);
			PacketComposer.Add(btNoticeStatus);

			int iAddPoint = 0;
			if(btNoticeStatus != GAMERESULT_NOTICE_STATUS_NONE)
				user->GetUserGamePlayEvent()->UpdateUserPoint(rs.iGamePlayEventRewardPoint, iAddPoint);
			PacketComposer.Add((int)iAddPoint);
			////////////////

			// EXPBOXITEM EVENT
			user->GetUserExpBoxItemEvent()->MakeGameEnd_CurrentBoxStatus(rs.iMatchType, PacketComposer);

			// SPECIAL AVATAR PIECE EVENT
			if(rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE)
			{
				BOOL bIsDouble = FALSE;
				if(OPEN == bIsDoubleRewardEvent)
					bIsDouble = TRUE;

				user->GetUserSpecialAvatarPieceEvent()->MakePacketAndGivePiece(PacketComposer, TRUE, bIsDouble);
			}
			else
			{
				SS2C_SPEICAL_AVATAR_PIECE_GIVE_PIECE_NOT ss;
				ss.btPieceType = MAX_PIECE_TYPE;
				ss.iGiveCount = 0;
				PacketComposer.Add((PBYTE)&ss, sizeof(SS2C_SPEICAL_AVATAR_PIECE_GIVE_PIECE_NOT));
			}
			///////////////
			int iDiceTicket = 0;
			if (MATCH_TYPE_RATING == GetMatchType() || MATCH_TYPE_RANKMATCH == GetMatchType() || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE)
				iDiceTicket = user->GetUserGameOfDiceEvent()->GiveGameResultReward();
			
			PacketComposer.Add((int)iDiceTicket);	// 획득 주사위이용권

			int iAddTryCount = 0;
			if((rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE) && 
				FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
				iAddTryCount = user->GetUserPasswordEvent()->AddTryCount();
			PacketComposer.Add(iAddTryCount);

			iAddTryCount = 0;
			if((rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE) &&
				TRUE == user->GetUserHalloweenGamePlayEvent()->AddTryCount())
					iAddTryCount = 1;
			PacketComposer.Add(iAddTryCount);

			BYTE btBoxType = SEVENT_OPEN_BOX_TYPE_NONE;
			int iKeyCount = 0;
			if(rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE)
			{
				if( FALSE == rs.bRunAway && FALSE == rs.bIsDisconnectedMatch)
				{
					if(TRUE == user->GetUserOpenBoxEvent()->GiveKey())
					{
						btBoxType = user->GetUserOpenBoxEvent()->GetBoxType();
						iKeyCount = user->GetUserOpenBoxEvent()->GetKeyCount();
					}
				}
			}
			PacketComposer.Add(btBoxType);
			PacketComposer.Add(iKeyCount);

			BYTE btResultTicketType = MAX_MINIGAMEZONE_GAME_TYPE;
			int iGiveTicketCnt = 0;
			if(rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE)
				user->GetUserMiniGameZoneEvent()->CheckAndGiveTicket(btResultTicketType, iGiveTicketCnt);
			PacketComposer.Add(btResultTicketType);
			PacketComposer.Add(iGiveTicketCnt);

			for(int i = 0; i < MAX_SHOPPING_MISSION_MATERIAL_TYPE; ++i)
				PacketComposer.Add(iaMaterialCount[i]);

			//! LuckyBox Event
			BYTE btGiveLuckyTicketCnt = 0;
			user->GetUserLuckyBox()->UpdateMission(rs.iMatchType, rs.bIsDisconnectedMatch, rs.bRunAway, btGiveLuckyTicketCnt);
			PacketComposer.Add(btGiveLuckyTicketCnt);
			//!

			//! MissionMakeItem Event
			for(int i = 0; i < MissionMakeItemEvent::MAX_MATERIAL_TYPE; ++i)
				PacketComposer.Add(iaGetMaterial[i]);
			//!

			//Event Promise
			//
			int iCount = 0;
			if(0 < rs.iPromiseSpecialStageExpCount && 
				(rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_RANKMATCH || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE) &&
				FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
			{
				user->AddAndUpdatePromiseSpecialEXP(rs.iPromiseSpecialStageExpCount);
				iCount =  rs.iPromiseSpecialStageExpCount * PROMISE.GetPromiseSpecialEXPAddVal();
			}
			PacketComposer.Add(iCount);
			
			//! Congratulation Event
			PacketComposer.Add(user->GetUserCongratulationEvent()->GetExpGamePlayCnt());
			//!

			//! Hello 2018 Event
			int iHelloBuffRate = 0;
			if((rs.iMatchType == MATCH_TYPE_RATING || rs.iMatchType == MATCH_TYPE_CLUB_LEAGUE) && 
				FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
				iHelloBuffRate = user->GetUserHelloNewYearEvent()->GetUserBuffRate();
			PacketComposer.Add(iHelloBuffRate);
			//!
			PacketComposer.Add((PBYTE)rs.iBestPlayerID, sizeof(int)*MAX_GAMERESULT_BESTPLAYER_COUNT);

			PacketComposer.Add(rs.iGetClubCoin);
			PacketComposer.Add(rs.iGetClubLeaguePoint);
			PacketComposer.Add(rs.iGetUserClubLeaguePoint);

			////////////////
			// League Mode
			if( -1 != rs.iRankModeResultType )
			{
				user->GetUserRankMatch()->UpdateResult(rs.iRankModeResultType, rs.ipromotioninfo
					,rs.imyleague ,rs.imyrating, rs.iLtotalpoint, rs.iLaddpoint, rs.fLeagueRatingPoint, rs.iLeaguePopupIndex
					,rs.shGameRecordWin);

				PacketComposer.Add(&rs.iRankModeResultType);

				if(rs.iRankModeResultType == eRMRT_Promotion) // 10time  onlyText 
				{ 
					PacketComposer.Add(rs.ipromotioninfo); 
				} 
				if(rs.iRankModeResultType == eRMRT_RatingUp || // 3time 
					rs.iRankModeResultType == eRMRT_LeagueUp ) // 5time 

				{ 
					PacketComposer.Add(rs.ipromotioninfo); 
					PacketComposer.Add(rs.imyleague); 
					PacketComposer.Add(rs.imyrating); 
				} 
				else if(rs.iRankModeResultType == eRMRT_GradeChange )// 1승급, 0배치고사완료, -1강등, -2승급실패)
				{
					PacketComposer.Add(rs.ipromotioninfo); 
					PacketComposer.Add(rs.imyleague); 
					PacketComposer.Add(rs.imyrating); 
					PacketComposer.Add(rs.iLeaguePopupIndex); 
					PacketComposer.Add(rs.iLtotalpoint); 
				}
				else 
				{ 
					PacketComposer.Add(rs.imyleague); 
					PacketComposer.Add(rs.imyrating); 

					if( 999 < rs.iLtotalpoint )
						rs.iLtotalpoint = 999;

					PacketComposer.Add(rs.iLtotalpoint); 
					PacketComposer.Add(rs.iLaddpoint); 
				} 

				if( rs.iRankModeResultType != eRMRT_Promotion )
				{
					user->GetUserRankMatch()->SendUpdateBasic(&rs);
				}
			}
			else
			{
				PacketComposer.Add(&rs.iRankModeResultType);
			}

			if( -1 != rs.cRankMatchTeamIndex )
			{
				user->GetUserRankMatch()->UpdatePositionSet(rs.cRankMatchTeamIndex, rs.shGameRecordWin);
			}
			////////////////////
			if((MATCH_TYPE_RATING == rs.iMatchType || MATCH_TYPE_CLUB_LEAGUE == rs.iMatchType)
				&& iGamePlayerNum >= NUM_PLAYER_2ON2 * 2 )
			{
				int nLost = user->GetAIComeLostNum();
				if(0 == rs.shGameRecordWin)
				{
					BOOL bPass = FALSE;

					if( nLost == 2 )
					{
						user->SetAIComeLostNum(0);
						bPass = TRUE;
					}
					else
					{
						nLost += 1;
						user->SetAIComeLostNum(nLost);
					}

					user->SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_READY);
					user->LoseLeadOrSend(bPass);
				}
				else
				{
					user->SetAIComeLostNum(0);
				}
			}
			else
			{
				user->SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_NONE);
			}

			if(MATCH_TYPE_PVE == rs.iMatchType)
			{
				user->LoseLeadGiveReward();
			}
			else
			{
				user->SetReadyAICome(FALSE);
			}

			if( CFSGameServer::GetInstance()->GetUseLuckItem() 
				&& MATCH_TYPE_PVE != rs.iMatchType
				&& MATCH_TYPE_PVE_3CENTER != rs.iMatchType
				&& MATCH_TYPE_PVE_CHANGE_GRADE != rs.iMatchType
				&& MATCH_TYPE_PVE_AFOOTBALL != rs.iMatchType
				&& iGamePlayerNum >= NUM_PLAYER_2ON2 * 2 
				&& user->GetCurUsedAvtarLvGrade() == AVATAR_LV_GRADE_AMATURE )
			{
				if( 0 == rs.shGameRecordWin )
				{
					int nLost = user->GetMaxLostNum();
					if( nLost == 2 )
					{
						user->SetMaxLostNum(0);
						user->AddLuckItem();
					}
					else
					{
						nLost +=1;
						user->SetMaxLostNum(nLost);
					}
				}
				else
				{
					user->SetMaxLostNum(0);
				}
			}
		}

		user->Send(&PacketComposer);

		int iItemCount = 0, iIdx = 0;
		for(int i=0; i< MAX_TEAM_NUM; i++)
		{
			for(int j=0; j<rs.shMemberNum[i]; j++)
			{
				if(0 == strcmp(user->GetGameID(), rs.szGameID[iIdx]))
				{
					if(TRUE == user->GetUserRecordBoard()->CheckAndUpdateMatchResult(rs))
						user->SendRecordBoardStatus();
				}
				iIdx++;
			}
				
		}

		if (MATCH_TYPE_RATING == GetMatchType() || MATCH_TYPE_CLUB_LEAGUE == GetMatchType())
		{
			user->GetUserRandomBox()->UpdateMission();
			user->GetUserExpBoxItemEvent()->ProcessGameEnd_UpdateBoxStatus();
		}
		else if(MATCH_TYPE_RANKMATCH == GetMatchType())
		{
			user->GetUserExpBoxItemEvent()->ProcessGameEnd_UpdateBoxStatus();
		}

		user->AddTotalRankingAchievements();

		if(FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
		{
			user->UpdateLastPlayTime(rs.iMatchType);
			user->UpdateClubCoin(rs.iGetClubCoin, rs.iClubGamePlayCount, rs.tClubGamePlayCountUpdateDate);
		}
		
		if(rs.btReventMatchingType == RevengeMatching::REVENGE_MATCHING_TYPE_REVENGE_REWARD_INFO && rs.shGameRecordWin == 1)
		{
			// 최종보상을 받는 시점에 이겼으면 최종 리벤지매칭 승리.
			user->AddRevengeMatchingWinAchievements();
		}

		if((MATCH_TYPE_RATING == GetMatchType() ||
			MATCH_TYPE_RANKMATCH == GetMatchType() || 
			MATCH_TYPE_CLUB_LEAGUE == GetMatchType()) &&
			FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
		{
			CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC(ODBC_LOG);
			CHECK_NULL_POINTER_VOID(pLogODBC);

			pLogODBC->WB_EVENT_UpdateUserGamePlayCount(user->GetUserIDIndex());
		}

		// 경기결과창 이벤트 완료 알림(모든 이벤트 공용)
		SS2C_EVENT_GAME_RESULT_COMPLETED_NOT not;
		not.iEventCount = 0;

		CPacketComposer Packet(S2C_EVENT_GAME_RESULT_COMPLETED_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_EVENT_GAME_RESULT_COMPLETED_NOT));
		PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

		user->GetUserPremiumPassEvent()->MakeEventCompleted(Packet, not.iEventCount, bCompletedPremiumEventMission);
		user->GetUserHippocampusEvent()->MakeGiveFood(Packet, not.iEventCount, rs.iMatchType, rs.bIsDisconnectedMatch, rs.bRunAway);
		
		if ((MATCH_TYPE_RATING == GetMatchType() || MATCH_TYPE_RANKMATCH == GetMatchType() || MATCH_TYPE_CLUB_LEAGUE == GetMatchType()) &&
			FALSE == rs.bIsDisconnectedMatch && FALSE == rs.bRunAway)
			user->GetUserSecretShopEvent()->MakeEventOpen(Packet, not.iEventCount);

		user->GetUserTransferJoycity100DreamEvent()->MakeGiveTicket(Packet, not.iEventCount, rs.iMatchType, rs.btMatchScaleType, rs.bIsDisconnectedMatch, rs.bRunAway);

		memcpy(pCountLocation, &not.iEventCount, sizeof(int));
		user->Send(&Packet);
	}
}

void CMatchSvrProxy::Process_GameEnd(CReceivePacketBuffer* rp)
{
	SM2G_GAME_END rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GAME_END));

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	
	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SetAvailablePointRewardCard(FALSE);
		user->SetPlaying(FALSE);

		CPacketComposer PacketComposer(S2C_END_GAME);
		//BYTE btMatchType = (BYTE)GetMatchType();
		//PacketComposer.Add(btMatchType);

		user->Send(&PacketComposer);

		if ( rs.nCanResearch )
		{
			CPacketComposer PacketComposerHostResearch( S2C_USER_GAMEPLAYRESEARCH_REQ );
			user->Send( &PacketComposerHostResearch );
		}
		SendActionSlot(user.GetPointer());
		user->CheckExpireItem();

		if(user->IsExpirePremiumPCRoom())
		{
			user->RemovePCRoomBenefits();
		}

		user->SendAvatarInfoUpdateToMatch();

		if(GetMatchType() == MATCH_TYPE_CLUBTOURNAMENT)
		{
			SendUserLogout(user.GetPointer());
		}
	}
}

void CMatchSvrProxy::Process_CreateRoomResult(CReceivePacketBuffer* rp)
{
	SM2G_CREATE_ROOM_RESULT rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CREATE_ROOM_RESULT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(0 == rs.iErrorCode || 11 == rs.iErrorCode)
		{
			user->GetClient()->SetState(NEXUS_WAITROOM);

			user->SendToMatchAddFriendAll();
			user->SendToMatchAddFriendAccountAll();
		}

		CPacketComposer PacketComposer(S2C_CREATE_ROOM_RESULT);
		PacketComposer.Add(&rs.iErrorCode);
		
		user->Send(&PacketComposer);
	}
	else
	{
		SendUserLogout(user.GetPointer());
	}
}

void CMatchSvrProxy::Process_RoomInfo(CReceivePacketBuffer* rp)
{
	SM2G_ROOM_INFO rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ROOM_INFO));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_ROOM_INFO);

		PacketComposer.Add(&rs.iOPCode);
		if(OP0_CLEAN == rs.iOPCode)
		{
			PacketComposer.Add(rs.btListMode);
			PacketComposer.Add(rs.iPage);
			PacketComposer.Add(rs.iTotalPage);
		}
		else if(OP1_CREATE == rs.iOPCode)
		{
			PacketComposer.Add(rs.Buffer, rs.iSize);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangeGameRoomNameRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_GAMEROOM_NAME_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_GAMEROOM_NAME_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_GAMEROOM_NAME_RES);

		PacketComposer.Add((BYTE*)rs.szRoomName, sizeof(char)*(MAX_ROOM_NAME_LENGTH+1));

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TeamEstimateRes(CReceivePacketBuffer* rp)
{
	SM2G_TEAM_ESTIMATE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TEAM_ESTIMATE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TEAM_ESTIMATE_RES);

		PacketComposer.Add(rs.iEstimateCode);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_JoinRoomResult(CReceivePacketBuffer* rp)
{
	SM2G_JOIN_ROOM_RESULT rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_JOIN_ROOM_RESULT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(0 == rs.iErrorCode || 11== rs.iErrorCode)
		{
			user->GetClient()->SetState(NEXUS_WAITROOM);

			user->SendToMatchAddFriendAll();
			user->SendToMatchAddFriendAccountAll();
		}

		CPacketComposer PacketComposer(S2C_JOIN_ROOM_RESULT);
		PacketComposer.Add(&rs.iMode);
		PacketComposer.Add(&rs.iErrorCode);

		user->Send(&PacketComposer);
	}
	else
	{
		SendUserLogout(user.GetPointer());
	}
}

void CMatchSvrProxy::Process_CancelTeamMatchRes(CReceivePacketBuffer* rp)
{
	SM2G_CANCEL_TEAM_MATCH_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_CANCEL_TEAM_MATCH_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CFSGameClient* pClient = (CFSGameClient*)user->GetClient();
		if(NULL != pClient && RESULT_CANCEL_TEAM_MATCH_SUCCESS == rs.iErrorCode)
		{
			pClient->SetState(FS_TEAM);
		}

		CPacketComposer PacketComposer(S2C_CANCEL_TEAM_MATCH_RES);
		PacketComposer.Add(rs.iErrorCode);
		PacketComposer.Add((PBYTE)rs.szTeamName, sizeof(char)*(MAX_TEAM_NAME_LENGTH+1));
		PacketComposer.Add(rs.iHomeAwayIndex);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ExitRoomResult(CReceivePacketBuffer* rp)
{
	SM2G_EXIT_ROOM_RESULT rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_EXIT_ROOM_RESULT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->CheckExpireItem();

		if(0 == rs.iResult || EXIT_GAME_WITH_NO_GAME_DELAY == rs.iResult)
		{
			user->SetPlaying(FALSE);

			user->GetClient()->SetState(NEXUS_LOBBY);
		}
		else if( 60 == rs.iResult ) // leaguemode
		{
			user->SetPlaying(FALSE);
			rs.iResult = 0; // success
		}

		CPacketComposer PacketComposer(S2C_EXIT_ROOM_RESULT);
		PacketComposer.Add(rs.iResult);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_GameStartPauseRes(CReceivePacketBuffer* rp)
{
	SM2G_GAME_START_PAUSE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GAME_START_PAUSE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GAMESTART_PAUSE_RES);
		PacketComposer.Add(rs.iErrorCode);
		PacketComposer.Add(rs.iTeamIdx);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_GameTerminatedReq(CReceivePacketBuffer* rp)
{
	SM2G_GAME_TERMINATED_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GAME_TERMINATED_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GAME_TERMINATED_REQ);

		PacketComposer.Add(rs.iErrorCode);
		PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_UpdateVariationTrophy(CReceivePacketBuffer* rp)
{
	SM2G_UPDATE_VARIATION_TROPHY rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_UPDATE_VARIATION_TROPHY));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SetUserTrophy(user->GetUserTrophy() + rs.iTrophy);

		user->SendCurAvatarTrophy();
	}
}

void CMatchSvrProxy::Process_UpdateTrophyAndChampionCnt(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		int iTroPhyCount = 0;
		int iTournamentChampionCnt = 0;

		if(ODBC_RETURN_SUCCESS == pGameODBC->Trophy_GetTrophyAndChampionCnt(user->GetUserIDIndex(), user->GetGameIDIndex(), iTournamentChampionCnt, iTroPhyCount))
		{
			user->SetUserTrophy(iTroPhyCount);
			user->SetTournamentChampionCnt(iTournamentChampionCnt);

			user->SendUserGold();
			user->SendCurAvatarTrophy();
			user->SendTournamentChampionCnt();
		}
	}
}

void CMatchSvrProxy::Process_QuickJoinGameAsObserverRes(CReceivePacketBuffer* rp)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC);

	SM2G_QUICK_JOIN_GAME_AS_OBSERVER_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_QUICK_JOIN_GAME_AS_OBSERVER_RES));

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	
	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if (RESULT_QUICK_JOIN_GAME_AS_OBSERVER_SUCCESS == rs.res.iResult)
		{
			user->GetClient()->SetState(NEXUS_WAITROOM);
			user->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_JOIN_OBSERVER_MODE);
			user->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_JOIN_OBSERVER_MODE);
		}
		WRITE_LOG_NEW(LOG_TYPE_USER, RECV_DATA, NONE, "Observer Data From Client:iResult = 10752790, szObserverSvrIP = (null), shObserverSvrPort = 17051648, iObserverRoomID = -858993460, iCourtIdx = -858993460, iObserverEnterKey = -858993460 ",
P, rs.res.shObserverSvrPort, rs.res.iObserverRoomID, rs.res.iCourtIdx, rs.res.iObserverEnterKey );

		user->Send(S2C_QUICK_JOIN_GAME_AS_OBSERVER_RES, &rs.res, sizeof(SS2C_QUICK_JOIN_GAME_AS_OBSERVER_RES));
	}
}

void CMatchSvrProxy::Process_RemainPlayTimeInRoomRes(CReceivePacketBuffer* rp)
{
	SM2G_REMAIN_PLAY_TIME_IN_ROOM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_REMAIN_PLAY_TIME_IN_ROOM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_REMAIN_PLAY_TIME_IN_ROOM_RES);
		PacketComposer.Add(rs.iRemainPlayTime);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_RemainPlayScoreInRoomRes(CReceivePacketBuffer* rp)
{
	SM2G_REMAIN_PLAY_SCORE_IN_ROOM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_REMAIN_PLAY_SCORE_IN_ROOM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_REMAIN_PLAY_SCORE_IN_ROOM_RES);
		PacketComposer.Add(rs.iTargetScore);
		PacketComposer.Add(rs.iHomeScore);
		PacketComposer.Add(rs.iAwayScore);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ExitObservingGameRes(CReceivePacketBuffer* rp)
{
	SM2G_OBSERVER_EXIT_GAME_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_OBSERVER_EXIT_GAME_REQ));

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if ( NULL == pServer )
	{
		return;
	}

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SetPlaying( false );

		if(OBSERVER_EXIT_BUT_CONNECT_CONTINUE == rs.btExitOption)
		{
			user->GetClient()->SetState(NEXUS_OBSERVING);
		}
		else if(OBSERVER_EXIT_END_GAME == rs.btExitOption)
		{
			user->SendPacketQueue();
			user->GetClient()->SetState(NEXUS_LOBBY);
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_MATCH, GAME_END, FAIL, "Process_ExitObservingGameRes WrongOption(10752790)", rs.btExitOption);
	
		CPacketComposer PacketComposer(S2C_EXIT_OBSERVING_GAME_RES);
		PacketComposer.Add(rs.btExitOption);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ExitObservingGameToTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_EXIT_OBSERVING_GAME_TO_TEAM_RES);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_SendPacketQueue(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SendPacketQueue();
	}
}

void CMatchSvrProxy::Process_AbuseUserGame(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(NULL != user->GetClient())
		{
			((CFSGameClient*)user->GetClient())->DisconnectClient();
		}
	}
}

void CMatchSvrProxy::Process_GameStartHalfTimeRes(CReceivePacketBuffer* rp)
{
	SM2G_GAME_START_HALFTIME_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GAME_START_HALFTIME_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GAME_START_HALFTIME_RES);
		PacketComposer.Add(rs.byStatus);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_GameStartHalfTimeNowRes(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GAME_START_HALFTIME_NOW_RES);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_GiveUpVoteRes(CReceivePacketBuffer* rp)
{
	SM2G_GIVEUP_VOTE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GIVEUP_VOTE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GIVEUP_VOTE_RES);
		PacketComposer.Add(rs.shResult);
		PacketComposer.Add(rs.iVoteCnt);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_IntentionalFoulRes(CReceivePacketBuffer* rp)
{
	SM2G_INTENTIONAL_FOUL_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_INTENTIONAL_FOUL_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_INTENTIONAL_FOUL_RES);
		PacketComposer.Add(rs.iResult);
		if(WARNING_SUCESS == rs.iResult || PUNISHMENT_SUCESS == rs.iResult)
		{
			PacketComposer.Add((PBYTE)rs.szFoulUserID, MAX_GAMEID_LENGTH+1);
		}
		else if(PUNISHMENT_ACTION == rs.iResult || WARNING_ACTION == rs.iResult)
		{
			PacketComposer.Add(rs.iTeamExplanation);
		}
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_UsePauseItemRes(CReceivePacketBuffer* rp)
{
	SM2G_USE_PAUSE_ITEM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_USE_PAUSE_ITEM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_USE_PAUSEITEM_RES);
		PacketComposer.Add(rs.iResult);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_DiscountPauseItemReq(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	SG2M_DISCOUNT_PAUSE_ITEM_RES info;
	info.iGameIDIndex = rs.iGameIDIndex;
	info.iPauseItemCount = -1;

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		info.iPauseItemCount = user->DiscountPauseItem();
	}

	CPacketComposer PacketComposer(G2M_DISCOUNT_PAUSE_ITEM_RES);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_DISCOUNT_PAUSE_ITEM_RES));

	Send(&PacketComposer);
}

void CMatchSvrProxy::Process_GamePauseRes(CReceivePacketBuffer* rp)
{
	SM2G_GAME_PAUSE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GAME_PAUSE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GAME_PAUSE_RES);
		PacketComposer.Add(rs.iResult);
		if(rs.iResult == PAUSE_START_SUCCESS)
		{
			PacketComposer.Add(rs.PauseGameResult, rs.iSize);
			PacketComposer.Add((BYTE*)rs.szPauseRequestUserGameID, MAX_GAMEID_LENGTH+1);
			PacketComposer.Add(rs.iPauseItemCount);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_UseSecondBoostItemRes(CReceivePacketBuffer* rp)
{
	SM2G_USE_SECOND_BOOST_ITEM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_USE_SECOND_BOOST_ITEM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_USE_SECOND_BOOST_ITEM_RES);
		PacketComposer.Add(rs.iResult);
		if(0 == rs.iResult)
		{
			if(0 == strcmp(rs.szGameID, user->GetGameID()))
			{
				for(int i=0; i<MAX_STAT_NUM; i++)
				{
					user->UpdateTotalStatusByIndex(i, rs.iaStatAdd[i]);
				}
			}

			PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);
			for(int i=0; i<MAX_STAT_NUM; i++)
			{
				PacketComposer.Add(rs.iaStatAdd[i]);
			}
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_SecondBoostItemExpiredRes(CReceivePacketBuffer* rp)
{
	SM2G_SECOND_BOOST_ITEM_EXPIRED_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_SECOND_BOOST_ITEM_EXPIRED_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_SECOND_BOOSTITEM_EXPIRED_RES);
		PacketComposer.Add(rs.iResult);
		if(0 == rs.iResult)
		{
			if(0 == strcmp(rs.szGameID, user->GetGameID()))
			{
				int iaStatAdd[MAX_STAT_NUM];

				user->GetAvatarSingleItemStatus(rs.iItemCode, iaStatAdd);

				for(int i=0; i<MAX_STAT_NUM; i++)
				{	
					user->UpdateTotalStatusByIndex(i, -iaStatAdd[i]);				
				}
			}

			PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);
		}
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_RoomMemberRes(CReceivePacketBuffer* rp)
{
	SM2G_ROOM_MEMBER_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ROOM_MEMBER_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_ROOM_MEMBER_RES);
		PacketComposer.Add((PBYTE)&rs.res, sizeof(SS2C_ROOM_MEMBER_RES));
		PacketComposer.Add((PBYTE)rp->GetRemainedData(), rp->GetRemainedDataSize());
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_GMChangeHost(CReceivePacketBuffer* rp)
{
	SM2G_GM_CHANGE_HOST_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_GM_CHANGE_HOST_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(GM_S2G_CHANGE_HOST_RES);
		PacketComposer.Add(rs.iError);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_RemoveFeature(CReceivePacketBuffer* rp)
{
	SM2G_REMOVE_FEATURE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_REMOVE_FEATURE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->RemoveFeature(rs.iIdx);
	}
}

void CMatchSvrProxy::Process_DecreaseFeature(CReceivePacketBuffer* rp)
{
	SM2G_DECREASE_FEATURE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_DECREASE_FEATURE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->DecreaseFeature(rs.iIdx, rs.iDecreaseCount);
	}
}

void CMatchSvrProxy::Process_BonusPointLevelUp(CReceivePacketBuffer* rp)
{
	SM2G_BONUS_POINT_LEVELUP rs;
	
	rp->Read((BYTE*)&rs, sizeof(SM2G_BONUS_POINT_LEVELUP));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->SetBonusPointTerms(rs.iRequiredLevel, rs.iBonusPoint);
	}
}

void CMatchSvrProxy::Process_UpdateMaxLevelStepAndTakeItemReward(CReceivePacketBuffer* rp)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();

	int iTargetGameIDIndex;
	rp->Read(&iTargetGameIDIndex);

	CScopedRefGameUser user(iTargetGameIDIndex);
	if(user != NULL)
	{
		BYTE bAchieveCount = 0;
		BYTE byMaxLevelStep = 0;
		int iItemCode = 0;
		BYTE bRewardState = FALSE;

		CPacketComposer PacketComposer(S2C_TAKE_ITEM_REWARD_MAX_LEVEL_CONTENTS_NOT);
		
		rp->Read(&bAchieveCount);
		PacketComposer.Add(bAchieveCount);
		for(int i = 0; i < bAchieveCount; i++)
		{
			rp->Read(&byMaxLevelStep);
			rp->Read(&iItemCode);
			rp->Read(&bRewardState);
			PacketComposer.Add(byMaxLevelStep);
			PacketComposer.Add(iItemCode);
			PacketComposer.Add(bRewardState);
		}

		user->SetAvatarMaxLevelStep(byMaxLevelStep);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_JumpBallPlayer(CReceivePacketBuffer* rp)
{
	SM2G_JUMPBALL_INFO rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_JUMPBALL_INFO));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_JUMPBALL_PLAYER);
		PacketComposer.Add(rs.iUserRoomGameID[0]);
		PacketComposer.Add(rs.iUserRoomGameID[1]);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_MatchFailNot(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_MATCHING_FAIL);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_EntranceScene(CReceivePacketBuffer* rp)
{
	SM2G_ENTRANCE_SCENE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ENTRANCE_SCENE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		if(rs.iEntranceScene[TEAM_INDEX_HOME] == ENTRANCE_SCENE_TEAM_CLUB_EFFECT &&
			rs.iEntranceScene[TEAM_INDEX_AWAY] == ENTRANCE_SCENE_TEAM_CLUB_EFFECT)
		{
			int iDiffTeamIndex = rs.iTeamIndex == TEAM_INDEX_HOME ? TEAM_INDEX_AWAY : TEAM_INDEX_HOME;
			rs.iEntranceScene[iDiffTeamIndex] = ENTRANCE_SCENE_ALLIN_MEMBER;
		}

		CPacketComposer PacketComposer(S2C_ENTRANCE_SCENE);
		for(int i=0; i<MAX_TEAM_NUM; i++)
		{
			PacketComposer.Add(rs.iEntranceScene[i]);
		}

		PacketComposer.Add(rs.iMemberNum);
		for(int i = 0; i < rs.iMemberNum; ++i)
		{
			PacketComposer.Add(rs.sUserMotion[i].iUserRoomGameID);
			PacketComposer.Add(rs.sUserMotion[i].iMotionCode);
		}
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ExitScene(CReceivePacketBuffer* rp)
{
	SM2G_EXIT_SCENE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_EXIT_SCENE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_EXIT_SCENE);
		for(int i=0; i<MAX_TEAM_NUM; i++)
		{
			PacketComposer.Add(rs.iExitScene[i]);
		}
		PacketComposer.Add(rs.btWinTeamIndex);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ChangeDecideOutcomeDetailRes(CReceivePacketBuffer* rp)
{
	SM2G_CHANGE_ROOM_DECIDE_OUTCOME_DETAIL_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_CHANGE_ROOM_DECIDE_OUTCOME_DETAIL_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHANGE_ROOM_DECIDE_OUTCOME_DETAIL_RES);
		PacketComposer.Add(rs.byRoomDecideOutcomeDetail);
		PacketComposer.Add(rs.iDetailValue);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::SendFirstPacket()
{
	CPacketComposer PacketComposer(G2M_CLIENT_INFO);
	SG2M_CLIENT_INFO info;

	CFSGameServer* pServer = (CFSGameServer*)GetServer();

	info.ProcessID = pServer->GetProcessID();	
	info.iServerType = pServer->GetServerType();
	strncpy_s(info.ProcessName, _countof(info.ProcessName), pServer->GetProcessInfo()->szServerName, MAX_PROCESS_NAME_LENGTH);

	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CLIENT_INFO));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendTeamInfoInTeamReq(SG2M_BASE Info)
{
	CPacketComposer PacketComposer(G2M_TEAM_INFO_IN_TEAM_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendAvoidTeamReq(SG2M_AVOID_TEAM_REQ Info)
{
	CPacketComposer PacketComposer(G2M_AVOID_TEAM_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_AVOID_TEAM_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangeTeamAllowObserveReq(SG2M_CHANGE_TEAM_ALLOW_OBSERVE_REQ Info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_TEAM_ALLOW_OBSERVE_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_CHANGE_TEAM_ALLOW_OBSERVE_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangeTeamNameReq(SG2M_CHANGE_TEAM_NAME_REQ Info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_TEAM_NAME_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_CHANGE_TEAM_NAME_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangeTeamPasswordReq(SG2M_CHANGE_TEAM_PASSWORD_REQ Info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_TEAM_PASSWORD_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_CHANGE_TEAM_PASSWORD_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangeTeamModeReq(SG2M_CHANGE_TEAM_MODE_REQ Info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_TEAM_MODE_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_CHANGE_TEAM_MODE_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendTeamExitReq(SG2M_BASE Info)
{
	CPacketComposer PacketComposer(G2M_TEAM_EXIT_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendWaitTeamListReq(SG2M_WAIT_TEAM_LIST_REQ Info)
{
	CPacketComposer PacketComposer(G2M_WAIT_TEAM_LIST_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_WAIT_TEAM_LIST_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChiefAssignCheckReq(SG2M_CHIEF_ASSIGN_CHECK_REQ Info)
{
	CPacketComposer PacketComposer(G2M_CHIEF_ASSIGN_CHECK_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_CHIEF_ASSIGN_CHECK_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChiefAssignMoveReq(SG2M_CHIEF_ASSIGN_MOVE_REQ Info)
{
	CPacketComposer PacketComposer(G2M_CHIEF_ASSIGN_MOVE_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_CHIEF_ASSIGN_MOVE_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendForceOutUserReq(SG2M_FORCEOUT_USER_REQ Info)
{
	CPacketComposer PacketComposer(G2M_FORCEOUT_USER_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_FORCEOUT_USER_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendRegTeamReq(SG2M_REG_TEAM_REQ Info)
{
	CPacketComposer PacketComposer(G2M_REG_TEAM_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_REG_TEAM_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendAutoRegReq(SG2M_AUTO_REG_REQ Info)
{
	CPacketComposer PacketComposer(G2M_AUTO_REG_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_AUTO_REG_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendReadyTeamListReq(SG2M_READY_TEAM_LIST_REQ Info)
{
	CPacketComposer PacketComposer(G2M_READY_TEAM_LIST_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_READY_TEAM_LIST_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendRequestTeamMatchReq(SG2M_REQUEST_TEAM_MATCH_REQ Info)
{
	CPacketComposer PacketComposer(G2M_REQUEST_TEAM_MATCH_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_REQUEST_TEAM_MATCH_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangePositionReq(SG2M_CHANGE_POSITION_REQ Info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_POSITION_REQ);
	PacketComposer.Add((BYTE*)&Info, sizeof(SG2M_CHANGE_POSITION_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendReadyReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_READY_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendToggleIamReadyReq(const SG2M_BASE& info)
{
	CPacketComposer PacketComposer(G2M_TOGGLE_IAM_READY_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));
	Send(&PacketComposer);
}

void CMatchSvrProxy::SendStartUdoHolepunchingRes(SG2M_START_UDP_HOLEPUNCHING_RES info)
{
	CPacketComposer PacketComposer(G2M_START_UDP_HOLEPUNCHING_RES);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_START_UDP_HOLEPUNCHING_RES));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendStartBandWidthRes(SG2M_START_BANDWIDTH_CHECK_RES info)
{
	CPacketComposer PacketComposer(G2M_START_BANDWIDTH_CHECK_RES);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_START_BANDWIDTH_CHECK_RES));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangeCourtReq(SG2M_CHANGE_COURT_REQ info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_COURT_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CHANGE_COURT_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangePracticeMethodReq(SG2M_CHANGE_PRACTICE_METHOD_REQ info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_PRACTICE_METHOD_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CHANGE_PRACTICE_METHOD_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendRoomListReq(SG2M_ROOM_LIST_REQ info)
{
	CPacketComposer PacketComposer(G2M_ROOM_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ROOM_LIST_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangeGameModeReq(SG2M_CHANGE_GAME_MODE_REQ info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_GAME_MODE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CHANGE_GAME_MODE_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendChangeGameRoomNameReq(SG2M_CHANGE_GAMEROOM_NAME_REQ info)
{
	CPacketComposer PacketComposer(G2M_CHANGE_GAMEROOM_NAME_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CHANGE_GAMEROOM_NAME_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendTeamChangeReq(SG2M_TEAM_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2M_TEAM_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_TEAM_CHANGE_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendTeamEstimateReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_TEAM_ESTIMATE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendJoinRoomReq(SG2M_JOIN_ROOM_REQ info)
{
	CPacketComposer PacketComposer(G2M_JOIN_ROOM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_JOIN_ROOM_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendCancelTeamMatchReq(SG2M_CANCEL_TEAM_MATCH_REQ info)
{
	CPacketComposer PacketComposer(G2M_CANCEL_TEAM_MATCH_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CANCEL_TEAM_MATCH_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendExitRoomReq(SG2M_EXIT_ROOM_REQ info)
{
	CPacketComposer PacketComposer(G2M_EXIT_ROOM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_EXIT_ROOM_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendTournamentExitReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_TOURNAMENT_EXIT_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGameStartPauseReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_GAME_START_PAUSE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendSetObserverRoomID(SG2M_SET_OBSERVER_ROOMID info)
{
	CPacketComposer PacketComposer(G2M_SET_OBSERVER_ROOMID);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_SET_OBSERVER_ROOMID));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendExitObservingGameReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_EXIT_OBSERVING_GAME_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendClubTeamListReq(SG2M_CLUB_TEAM_LIST_REQ info)
{
	CPacketComposer PacketComposer(G2M_CLUB_TEAM_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CLUB_TEAM_LIST_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendSetGameOption(SG2M_SET_GAME_OPTION info)
{
	CPacketComposer PacketComposer(G2M_SET_GAME_OPTION);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_SET_GAME_OPTION));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGameStartHalfTime(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_GAME_START_HALFTIME);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendAddFriend(SG2M_ADD_FRIEND info)
{
	CPacketComposer PacketComposer(G2M_ADD_FRIEND);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ADD_FRIEND));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendDelFriend(SG2M_DEL_FRIEND info)
{
	CPacketComposer PacketComposer(G2M_DEL_FRIEND);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_DEL_FRIEND));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendIntentionalFoulReq(SG2M_INTENTIONAL_FOUL_REQ info)
{
	CPacketComposer PacketComposer(G2M_INTENTIONAL_FOUL_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_INTENTIONAL_FOUL_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendUsePauseItemReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_USE_PAUSE_ITEM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGamePauseReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_GAME_PAUSE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendUseSecondBoostItemReq(SG2M_USE_SECOND_BOOST_ITEM_REQ info)
{
	CPacketComposer PacketComposer(G2M_USE_SECOND_BOOST_ITEM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_USE_SECOND_BOOST_ITEM_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendSecondBoostItemExpiredReq(SG2M_SECOND_BOOST_ITEM_EXPIRED_REQ info)
{
	CPacketComposer PacketComposer(G2M_SECOND_BOOST_ITEM_EXPIRED_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_SECOND_BOOST_ITEM_EXPIRED_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendLogoutTypeCheck(SG2M_LOGOUT_TYPE_CHECK info)
{
	CPacketComposer PacketComposer(G2M_LOGOUT_TYPE_CHECK);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_LOGOUT_TYPE_CHECK));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGMRoomListReq(SG2M_BASE info)
{
	CPacketComposer PacketComposer(G2M_GM_ROOMLIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_BASE));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGMRoomInfoReq(SG2M_GM_ROOMINFO_REQ info)
{
	CPacketComposer PacketComposer(G2M_GM_ROOMINFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_GM_ROOMINFO_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGMChatReq(SG2M_GM_CHAT_REQ info)
{
	CPacketComposer PacketComposer(G2M_GM_CHAT_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_GM_CHAT_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGMKickOutReq(SG2M_GM_KICKOUT_REQ info)
{
	CPacketComposer PacketComposer(G2M_GM_KICKOUT_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_GM_KICKOUT_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGMFindUserReq(SG2M_GM_FINDUSER_REQ info)
{
	CPacketComposer PacketComposer(G2M_GM_FINDUSER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_GM_FINDUSER_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::SendGMCloseRoomReq(SG2M_GM_CLOSEROOM_REQ info)
{
	CPacketComposer PacketComposer(G2M_GM_CLOSEROOM_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_GM_CLOSEROOM_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::Process_CompleteAchievementInGame(CReceivePacketBuffer* rp)
{
	SM2G_ACHIEVEMENT_COMPLETE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ACHIEVEMENT_COMPLETE));

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	pServer->Process_CompleteAchievement(rs);
}

void CMatchSvrProxy::Process_AnnounceAchievementComplete(CReceivePacketBuffer* rp)
{
	SM2G_ANNOUNCE_ACHIEVEMENT_COMPLETE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_ANNOUNCE_ACHIEVEMENT_COMPLETE_RES));

	CPacketComposer PacketComposer(S2C_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT);
	PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH + 1);
	PacketComposer.Add(rs.iAchievementCount);

	for (int iLoopIndex = 0; iLoopIndex < rs.iAchievementCount && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
	{
		PacketComposer.Add(rs.aiAchievementIndex[iLoopIndex]);
	}

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	if (pUser != NULL)
	{
		pUser->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::SendEquippedAchievementTitleBroadcastReq(SG2M_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_REQ info)
{
	CPacketComposer PacketComposer(G2M_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::Process_EquipAchievementTitleBroadcastRes(CReceivePacketBuffer* rp)
{
	SM2G_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_RES));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	if (pUser != NULL)
	{
		CPacketComposer PacketComposer(S2C_EQUIP_ACHIEVEMENT_TITLE_BROADCAST);
		PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH + 1);
		PacketComposer.Add(rs.iAchievementTitleIndex);

		pUser->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_SendEventComponentMsg(CReceivePacketBuffer* rp)
{
	SM2G_EVENT_COMPONENT_MSG rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_EVENT_COMPONENT_MSG));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_EVENT_COMPONENT_MSG_RES);
		PacketComposer.Add(rs.iPopupType );
		PacketComposer.Add(rs.iRequestType );
		PacketComposer.Add(rs.iArgCnt);
		PacketComposer.Add(rs.iPopupIndex);
		PacketComposer.Add(rs.iArgFirst);
		PacketComposer.Add(rs.iArgSecond);
		PacketComposer.Add(rs.iArgThird);
		PacketComposer.Add(rs.iArgFourth);
		PacketComposer.Add(rs.iArgFourth);
		PacketComposer.Add(rs.iEventIndex);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_InviteUserListStart(CReceivePacketBuffer* rp)
{
	SM2G_INVITE_USERLIST_START rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_INVITE_USERLIST_START));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_INVITE_USERLIST_START);
		PacketComposer.Add(rs.iCode);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_InviteUserListInfo(CReceivePacketBuffer* rp)
{
	SM2G_INVITE_USERLIST_INFO rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_INVITE_USERLIST_INFO));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_INVITE_USERLIST_INFO);
		PacketComposer.Add(rs.iCount);
		for(int i=0; i<rs.iCount; i++)
		{
			PacketComposer.Add(rs.iGamePosition[i]);
			PacketComposer.Add(rs.iEquippedAchievementTitle[i]);
			PacketComposer.Add((int)-1); //통합 삭제대기 vip
			PacketComposer.Add((int)-1); //통합 삭제대기 결혼
			PacketComposer.Add((PBYTE)rs.szName[i], MAX_GAMEID_LENGTH+1);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_InviteUserListEnd(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_INVITE_USERLIST_END);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentListInfo(CReceivePacketBuffer* rp)	
{
	SM2G_TOURNAMENT_LIST_INFO rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_LIST_INFO));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_LIST_INFO);
		PacketComposer.Add(rs.iTournamentMode);
		PacketComposer.Add(rs.iGameMode);
		PacketComposer.Add(rs.iTotalPageCount);
		PacketComposer.Add(rs.iCurrentPageNum);
		PacketComposer.Add(rs.iCount);
		for(int i=0; i<rs.iCount; i++)
		{
			PacketComposer.Add(rs.iNumber[i]);
			PacketComposer.Add(rs.iScaleMode[i]);
			PacketComposer.Add(rs.iSeason[i]);
			PacketComposer.Add(rs.iUserCount[i]);
			PacketComposer.Add(rs.iStatus[i]);
			PacketComposer.Add(rs.iIsPass[i]);
			PacketComposer.Add((PBYTE)rs.szName[i], MAX_TOURNAMENT_NAME_LENGTH+1);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentWaitListInfo(CReceivePacketBuffer* rp)	
{
	SM2G_TOURNAMENT_WAIT_LIST_INFO rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_WAIT_LIST_INFO));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_WAIT_LIST_INFO);
		PacketComposer.Add(rs.iTournamentMode);
		PacketComposer.Add(rs.iGameMode);
		PacketComposer.Add(rs.iStartIdx);
		PacketComposer.Add(rs.iCount);
		for(int i=0; i<rs.iCount; i++)
		{
			PacketComposer.Add(rs.iNumber[i]);
			PacketComposer.Add(rs.iScaleMode[i]);
			PacketComposer.Add(rs.iSeason[i]);
			PacketComposer.Add(rs.iUserCount[i]);
			PacketComposer.Add(rs.iStatus[i]);
			PacketComposer.Add(rs.iIsPass[i]);
			PacketComposer.Add((PBYTE)rs.szName[i], MAX_TOURNAMENT_NAME_LENGTH+1);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentNotifyRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_NOTIFY_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_NOTIFY_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_NOTIFY_RES);
		PacketComposer.Add((PBYTE)rs.szNotifyText, MAX_TOURNAMENT_NOTIFY_TEXT_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentProgramInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_PROGRAM_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_PROGRAM_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_PROGRAM_INFO_RES);
		PacketComposer.Add(rs.iScaleTapCode);
		PacketComposer.Add(rs.iGroup);
		PacketComposer.Add(rs.iSeason);
		PacketComposer.Add(rs.iNodeCount);
		PacketComposer.Add(rs.iMakingTeamCount);
		for(int i=0; i<rs.iNodeCount; i++)
		{
			PacketComposer.Add(rs.iNodeIdx[i]);
			PacketComposer.Add(rs.iStatus[i]);
			PacketComposer.Add((int)-1); //ToDo - delete GroupBattle groupindex
			PacketComposer.Add((PBYTE)rs.szTeamName[i], MAX_TEAM_NAME_LENGTH+1);
			PacketComposer.Add(rs.iAbleObserve[i]);
			PacketComposer.Add(rs.iRemainTime[i]);
			PacketComposer.Add(rs.iMemberCount[i]);
			for(int j=0; j<rs.iMemberCount[i]; j++)
			{
				PacketComposer.Add(rs.iMemberPosition[i][j]);
				PacketComposer.Add((PBYTE)rs.szMemberGameID[i][j], MAX_GAMEID_LENGTH+1);
			}
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentEnterRes(CReceivePacketBuffer* rp)	
{
	SM2G_TOURNAMENT_ENTER_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_ENTER_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->SetTournamentUser(TRUE);

		CPacketComposer PacketComposer(S2C_TOURNAMENT_ENTER_RES);
		PacketComposer.Add(rs.iResult);
		PacketComposer.Add(rs.iNumber);
		PacketComposer.Add(rs.iTournamentMode);
		PacketComposer.Add(rs.iGameMode);
		PacketComposer.Add(rs.iScaleMode);
		PacketComposer.Add(rs.iSeason);
		PacketComposer.Add(rs.iStatus);
		PacketComposer.Add((PBYTE)rs.szName, MAX_TOURNAMENT_NAME_LENGTH+1);
		PacketComposer.Add((PBYTE)rs.szPass, MAX_TOURNAMENT_PASS_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentExitRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_EXIT_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_EXIT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->SetTournamentUser(FALSE);

		CPacketComposer PacketComposer(S2C_TOURNAMENT_EXIT_RES);
		PacketComposer.Add(rs.iResult);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentCreatePreMadeTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_CREATE_PREMADE_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_CREATE_PREMADE_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_CREATE_PREMADE_TEAM_RES);
		PacketComposer.Add(rs.iResult);
		PacketComposer.Add((int)0); //ToDo - delete GroupBattle

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentInviteUserRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_INVITE_USER_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_INVITE_USER_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_INVITE_USER_RES);
		PacketComposer.Add(rs.iResult);
		PacketComposer.Add(rs.iSlotNumber);
		PacketComposer.Add((PBYTE)rs.szName, MAX_GAMEID_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentInviteSuggestReq(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_INVITE_SUGGEST_REQ rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_INVITE_SUGGEST_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_INVITE_SUGGEST_REQ);
		PacketComposer.Add(rs.iSlotNumber);
		PacketComposer.Add((PBYTE)rs.szName, MAX_GAMEID_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentEnterPreMadeTeam(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_ENTER_PREMADE_TEAM rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_ENTER_PREMADE_TEAM));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_ENTER_PREMADE_TEAM);
		PacketComposer.Add(rs.iResult);
		PacketComposer.Add(rs.iSlotNumber);
		PacketComposer.Add(rs.iGamePosition);
		PacketComposer.Add(rs.iEquippedAchievementTitle);
		PacketComposer.Add((PBYTE)rs.szName, MAX_GAMEID_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentExitPreMadeTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_EXIT_PREMADE_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_EXIT_PREMADE_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_EXIT_PREMADE_TEAM_RES);
		PacketComposer.Add(rs.iResult);
		PacketComposer.Add((PBYTE)rs.szName, MAX_GAMEID_LENGTH+1);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentRegisterPreMadeTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_REGISTER_PREMADE_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_REGISTER_PREMADE_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_REGISTER_PREMADE_TEAM_RES);
		PacketComposer.Add(rs.iResult);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentButtonInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_BUTTON_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_BUTTON_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_BUTTON_INFO_RES);
		PacketComposer.Add(rs.iStatus);
		PacketComposer.Add(rs.iResisterPreMadeTeam);
		PacketComposer.Add(rs.iCurDepth);
		PacketComposer.Add((PBYTE)rs.szName, MAX_TOURNAMENT_NAME_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentMoveSeasonRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_MOVE_SEASON_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_MOVE_SEASON_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_MOVE_SEASON_RES);
		PacketComposer.Add(rs.iResult);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentChief(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_CHIEF);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentChangeNameRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_CHANGE_NAME_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_CHANGE_NAME_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_CHANGE_NAME_RES);
		PacketComposer.Add((PBYTE)rs.szName, MAX_TOURNAMENT_NAME_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentChangePasswordRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_CHANGE_PASSWORD_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_CHANGE_PASSWORD_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_CHANGE_PASSWORD_RES);
		PacketComposer.Add((PBYTE)rs.szPass, MAX_TOURNAMENT_PASS_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentReadyCount(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_READY_COUNT rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_READY_COUNT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_READY_COUNT);
		PacketComposer.Add(rs.iCount);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentChangeTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_CHANGE_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_CHANGE_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_CHANGE_TEAM_RES);
		PacketComposer.Add(rs.iNodeIdx1);
		PacketComposer.Add(rs.iNodeIdx2);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentEnterMakingTeam(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_ENTER_MAKING_TEAM rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_ENTER_MAKING_TEAM));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_ENTER_MAKING_TEAM);
		PacketComposer.Add(rs.iTeamIdx);
		PacketComposer.Add(rs.iNodeIdx);
		PacketComposer.Add((PBYTE)rs.szTeamName, MAX_TOURNAMENT_NAME_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentExitMakingTeam(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_EXIT_MAKING_TEAM rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_EXIT_MAKING_TEAM));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_EXIT_MAKING_TEAM);
		PacketComposer.Add((PBYTE)rs.szName, MAX_GAMEID_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentChangeStatus(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_CHANGE_STATUS rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_CHANGE_STATUS));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_CHANGE_STATUS);
		PacketComposer.Add(rs.iStatus);
		PacketComposer.Add(rs.iCurDepth);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentEnterGameRoom(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_ENTER_GAMEROOM rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_ENTER_GAMEROOM));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CFSGameClient* pClient = (CFSGameClient*)user->GetClient();
		if(NULL != pClient)
		{
			user->GetClient()->SetState(NEXUS_WAITROOM);
		}

		CPacketComposer PacketComposer(S2C_TOURNAMENT_ENTER_GAMEROOM);
		PacketComposer.Add(rs.iCurDepth);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentForceOutPreMadeTeamRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_FORCE_OUT_PREMADE_TEAM_RES);
		PacketComposer.Add((PBYTE)rs.szName, MAX_GAMEID_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentGameResult(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_GAME_RESULT rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_GAME_RESULT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_GAME_RESULT);
		PacketComposer.Add(rs.iWin);
		PacketComposer.Add(rs.iCurDepth);

		user->Send(&PacketComposer);

		user->AddTotalRankingAchievements();
	}
}

void CMatchSvrProxy::Process_TournamentGameEnd(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->SetPlaying(FALSE);

		CPacketComposer PacketComposer(S2C_TOURNAMENT_GAME_END);

		user->Send(&PacketComposer);

		if(user->IsExpirePremiumPCRoom())
		{
			user->RemovePCRoomBenefits();
		}
	}
}

void CMatchSvrProxy::Process_TournamentStartFail(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_START_FAIL);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentMyTeamInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_MYTEAM_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_MYTEAM_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_MYTEAM_INFO_RES);
		PacketComposer.Add((PBYTE)rs.szTeamName, MAX_TEAM_NAME_LENGTH+1);
		PacketComposer.Add(rs.iMemberCount);
		for(int i=0; i<rs.iMemberCount; i++)
		{
			PacketComposer.Add(rs.iSlotNumber[i]);
			PacketComposer.Add(rs.iMemberPosition[i]);
			PacketComposer.Add((PBYTE)rs.szMemberGameID[i], MAX_GAMEID_LENGTH+1);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentPublicOpenInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_PUBLIC_OPEN_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_PUBLIC_OPEN_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_PUBLIC_OPEN_INFO_RES);
		PacketComposer.Add(rs.iOpen);
		
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentVictoryTeamInfo(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_VICTORY_TEAM_INFO rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_VICTORY_TEAM_INFO));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_VICTORY_TEAM_INFO);
		PacketComposer.Add((PBYTE)rs.szTeamName, MAX_TEAM_NAME_LENGTH+1);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentCreatableRes(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_CREATABLE_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_CREATABLE_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_CREATABLE_RES);
		for(int i=0; i<TOURNAMENT_GAME_MODE_MAX_NUM; i++)
		{
			PacketComposer.Add(rs.iCreatable[i]);
		}

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentMoveNextSeasonNotify(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_MOVE_NEXT_SEASON_NOTIFY);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentStartCancelLackTeam(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_START_CANCEL_LACK_TEAM);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentSeasonEnd(CReceivePacketBuffer* rp)
{
	SM2G_BASE rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_TOURNAMENT_SEASON_END);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_TournamentVictory(CReceivePacketBuffer* rp)
{
	SM2G_TOURNAMENT_VICTORY rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_TOURNAMENT_VICTORY));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->UpdateTournamentVictory(rs.iSeasonIndex, rs.ucScaleMode);
	}
}

void CMatchSvrProxy::Process_RecordStepInfoRes(CReceivePacketBuffer* rp)
{
	SM2G_RECORD_STEP_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_RECORD_STEP_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_RECORD_STEP_INFO_RES);

		PacketComposer.Add(rs.iRecordNum);
		PacketComposer.Add(rs.iCurrent2PointRecord);
		PacketComposer.Add(rs.iCurrent3PointRecord);
		PacketComposer.Add(rs.iCurrentReboundRecord);
		PacketComposer.Add(rs.iCurrentStealRecord);
		PacketComposer.Add(rs.iCurrentAssistRecord);
		PacketComposer.Add(rs.iCurrentBlockRecord);
		PacketComposer.Add(rs.iCurrentWinRecord);
		PacketComposer.Add(rs.iRequired2PointRecord);
		PacketComposer.Add(rs.iRequired3PointRecord);
		PacketComposer.Add(rs.iRequiredReboundRecord);
		PacketComposer.Add(rs.iRequiredStealRecord);
		PacketComposer.Add(rs.iRequiredAssistRecord);
		PacketComposer.Add(rs.iRequiredBlockRecord);
		PacketComposer.Add(rs.iRequiredWinRecord);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_RecvPresentNot(CReceivePacketBuffer* rp)
{
	SM2G_RECV_PRESENT_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_RECV_PRESENT_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->RecvPresent(rs.iMailType, rs.iPresentIndex);
	}
}

void CMatchSvrProxy::Process_EventMessage(CReceivePacketBuffer* rp)
{
	SM2G_EVENT_MESSAGE rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_EVENT_MESSAGE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->SendEventMsg(rs.iEventType, rs.iMessageType, rs.iBonusType, rs.iGiveCash, rs.iGivePoint, rs.bPlaying);
	}
}

void CMatchSvrProxy::Process_RecordEventSendMessage(CReceivePacketBuffer* rp)
{
	SM2G_RECORD_EVENT_SEND_MESSAGE rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_RECORD_EVENT_SEND_MESSAGE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		SPresent* pPresent = user->GetPresentList()->Find(rs.iPresentIndex);
		if (NULL != pPresent)
		{
			int iPresentItemCode = pPresent->iItemCode;

			user->SendEventMsgThai(rs.iEventType, rs.iMessageType, rs.iBonusType, rs.iGamePosition, iPresentItemCode, rs.iCurrentSeveralTimes, rs.iSeveralTimes, 
				rs.iRequire2Point, rs.iRequire3Point, rs.iRequireRebound, rs.iRequireSteal, rs.iRequireAssist, rs.iRequireBlock, rs.iRequireWin, rs.bPlaying);
		}
	}
}

void CMatchSvrProxy::Process_EventcomponentRewardPopup( CReceivePacketBuffer* recvPacket )
{
	SM2G_EVENT_COMPONENT_REWARD_POPUP rs;

	recvPacket->Read((BYTE*)&rs, sizeof(SM2G_EVENT_COMPONENT_REWARD_POPUP));

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_REWARD_REQUEST_COMPONENT_RES);
	PacketComposer.Add(rs.iPopupmode);

	if (rs.iPopupmode == 2)
	{
		PacketComposer.Add(rs.iEventIndex);
		PacketComposer.Add((int)-1); //ToDo - delete groupbattleseasonindex
		PacketComposer.Add(rs.iEventStep);
		PacketComposer.Add(rs.iPoint);
		PacketComposer.Add(rs.iCash);
		PacketComposer.Add(rs.iItemCode);
		pUser->Send(&PacketComposer);
	}
	else if (rs.iPopupmode == 9 || rs.iPopupmode == 10)
	{
		PacketComposer.Add(rs.iEventIndex);
		PacketComposer.Add(rs.iPoint);
		pUser->Send(&PacketComposer);
	}
};

void CMatchSvrProxy::Process_AddPointAndPoint(CReceivePacketBuffer* rp)
{
	SM2G_ADD_POINT_AND_CASH rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_ADD_POINT_AND_CASH));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		if (0 != rs.iAddPoint || 0 != rs.iAddCash)
		{
			int iCurrentPoint = user->GetSkillPoint() + rs.iAddPoint;
			user->SetSkillPoint(iCurrentPoint);
			int iCurrentCoin = user->GetEventCoin() + rs.iAddCash;
			user->SetEventCoin(iCurrentCoin);
			user->SendUserGold();

			user->AddPointAchievements(); 
		}

		if (0 != rs.iAddTrophy)
		{
			int iCurrentTrophy = user->GetUserTrophy() + rs.iAddTrophy;
			user->SetUserTrophy(iCurrentTrophy);

			user->SendCurAvatarTrophy();
		}
	}
}

void CMatchSvrProxy::Process_CheatRatingPointRes(CReceivePacketBuffer* rp)
{
	SM2G_CHEAT_RATING_POINT_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_CHEAT_RATING_POINT_RES));

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_CHEAT_RATING_POINT_RES);
		PacketComposer.Add((PBYTE)rs.szTargetGameID, MAX_GAMEID_LENGTH+1);

		int iRatingCode = GET_RATING_CODE(this->GetMatchType(), MATCH_TEAM_SCALE_3ON3);
		if(TRUE == rs.bSuccess || ODBC_RETURN_SUCCESS == pGameODBC->MATCHMAKING_GetRatingPoint(rs.szTargetGameID, iRatingCode, rs.fRatingPoint))
		{
			PacketComposer.Add(rs.fRatingPoint);

			user->Send(&PacketComposer);
		}
	}
}

void CMatchSvrProxy::Process_UseHackNotifyReq( CReceivePacketBuffer* rp )
{
	CHECK_NULL_POINTER_VOID(rp);

	SM2G_FSG_USEHACK_NOTIRYREQ rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_FSG_USEHACK_NOTIRYREQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer( S2C_PLAYER_USE_HACK_NOTIFY_REQ );
		PacketComposer.Add( rs.iErrorCode );
		PacketComposer.Add( ( PBYTE )rs.szGameID, sizeof( char )*( MAX_GAMEID_LENGTH + 1 ) );	
		user->Send( &PacketComposer );
	}
}

void CMatchSvrProxy::Process_MatchingCancelPenalty(CReceivePacketBuffer* rp)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SM2G_MATCHING_CANCEL_PENALTY_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_MATCHING_CANCEL_PENALTY_NOT));
	
	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_MATCHING_CANCEL_PENALTY_NOT);
		PacketComposer.Add(rs.btPenaltyType);
		PacketComposer.Add(rs.iPenaltyCount);
		PacketComposer.Add(rs.iPenaltyEnablingCount);
		PacketComposer.Add(rs.iPenaltyValue);
		user->Send(&PacketComposer);
	}
}


void CMatchSvrProxy::Process_GameEndGiveBuffItem(CReceivePacketBuffer* rp)
{
	SM2G_GAMEEND_GIVEBUFFITEM rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_GAMEEND_GIVEBUFFITEM));

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{

		int iBuffItemCode = -1;
		int nItemPropertySize = 0;

		CAvatarItemList *pAvatarItemList = user->GetCurAvatarItemListWithGameID(user->GetGameID());
		if( NULL == pAvatarItemList )
		{
			return;
		}

		CPacketComposer* PacketComposer = new CPacketComposer(S2C_GAMEEND_GIVEBUFFITEM);

		PacketComposer->Add((int)rs.iBuffIndex1);
		if( rs.iBuffIndex1 > -1 )
		{

			pGameODBC->FSGetAvatarItem(pAvatarItemList, user->GetGameIDIndex(), rs.iBuffIndex1);
			pGameODBC->FSGetAvatarItemProperty(pAvatarItemList, user->GetGameIDIndex(), rs.iBuffIndex1);

			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(rs.iBuffIndex1);
			if( pItem != NULL )
			{
				iBuffItemCode = pItem->iItemCode;
				PacketComposer->Add((int)iBuffItemCode);
				PacketComposer->Add((int)pItem->iSellPrice);
				if( pItem->iPropertyNum > 0 )
				{
					vector< SUserItemProperty > vUserItemProperty;
					pAvatarItemList->GetItemProperty(pItem->iItemIdx, vUserItemProperty);
					nItemPropertySize = vUserItemProperty.size();
					PacketComposer->Add((int)nItemPropertySize);
					for( int iPropertyCount = 0; iPropertyCount < nItemPropertySize; iPropertyCount++ )
					{
						PacketComposer->Add((int)vUserItemProperty[iPropertyCount].iProperty);
						PacketComposer->Add((int)vUserItemProperty[iPropertyCount].iValue);
					}
				}
				else
				{
					PacketComposer->Add((int)nItemPropertySize);
				}
			}
			else
			{
				PacketComposer->Add((int)iBuffItemCode);
				PacketComposer->Add((int)nItemPropertySize);
			}
		}
		PacketComposer->Add((int)rs.iBuffIndex2);
		if( rs.iBuffIndex2 > -1 )
		{
			pGameODBC->FSGetAvatarItem(pAvatarItemList, user->GetGameIDIndex(), rs.iBuffIndex2);
			pGameODBC->FSGetAvatarItemProperty(pAvatarItemList, user->GetGameIDIndex(), rs.iBuffIndex2);

			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(rs.iBuffIndex2);
			if( pItem != NULL )
			{
				iBuffItemCode = pItem->iItemCode;
				PacketComposer->Add((int)iBuffItemCode);
				PacketComposer->Add((int)pItem->iSellPrice);
				if( pItem->iPropertyNum > 0 )
				{
					vector< SUserItemProperty > vUserItemProperty;
					pAvatarItemList->GetItemProperty(pItem->iItemIdx, vUserItemProperty);
					nItemPropertySize = vUserItemProperty.size();
					PacketComposer->Add((int)nItemPropertySize);
					for( int iPropertyCount = 0; iPropertyCount < nItemPropertySize; iPropertyCount++ )
					{
						PacketComposer->Add((int)vUserItemProperty[iPropertyCount].iProperty);
						PacketComposer->Add((int)vUserItemProperty[iPropertyCount].iValue);
					}
				}
				else
				{
					PacketComposer->Add((int)nItemPropertySize);
				}
			}
			else
			{
				PacketComposer->Add((int)iBuffItemCode);
				PacketComposer->Add((int)nItemPropertySize);
			}
		}
		PacketComposer->Add((int)rs.iBuffIndex3);
		if( rs.iBuffIndex3 > -1)
		{
			pGameODBC->FSGetAvatarItem(pAvatarItemList, user->GetGameIDIndex(), rs.iBuffIndex3);
			pGameODBC->FSGetAvatarItemProperty(pAvatarItemList, user->GetGameIDIndex(), rs.iBuffIndex3);
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(rs.iBuffIndex3);
			if( pItem != NULL )
			{
				iBuffItemCode = pItem->iItemCode;
				PacketComposer->Add((int)iBuffItemCode);
				PacketComposer->Add((int)pItem->iSellPrice);
				if( pItem->iPropertyNum > 0 )
				{
					vector< SUserItemProperty > vUserItemProperty;
					pAvatarItemList->GetItemProperty(pItem->iItemIdx, vUserItemProperty);
					nItemPropertySize = vUserItemProperty.size();
					PacketComposer->Add((int)nItemPropertySize);
					for( int iPropertyCount = 0; iPropertyCount < nItemPropertySize; iPropertyCount++ )
					{
						PacketComposer->Add((int)vUserItemProperty[iPropertyCount].iProperty);
						PacketComposer->Add((int)vUserItemProperty[iPropertyCount].iValue);
					}
				}
				else
				{
					PacketComposer->Add((int)nItemPropertySize);
				}
			}
			else
			{
				PacketComposer->Add((int)iBuffItemCode);
				PacketComposer->Add((int)nItemPropertySize);
			}
		}
		if(user->IsPlaying()) 
		{
			user->PushPacketQueue(PacketComposer);
		}
		else
		{		
			user->Send(PacketComposer);
			delete PacketComposer;
		}
	}
}

void CMatchSvrProxy::Process_SendPlayTimeRemain(CReceivePacketBuffer* rp)
{
	//SM2G_SEND_PLAYTIME_REMAIN rs;
	//rp->Read((BYTE*)&rs, sizeof(SM2G_SEND_PLAYTIME_REMAIN));

	//CScopedRefGameUser user(rs.iGameIDIndex);
	//if (user != NULL)
	//{
	//	CPacketComposer PacketComposer( S2C_EVENT_PLAYTIME_REMAIN );
	//	PacketComposer.Add( rs.iItemCode );
	//	PacketComposer.Add( rs.iTimeRemain );
	//	PacketComposer.Add( rs.iNum );
	//	user->Send(&PacketComposer);
	//}
}

void CMatchSvrProxy::Process_UserRatingUpdateNot(CReceivePacketBuffer* rp)
{
	SM2G_USER_RATING_UPDATE_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_USER_RATING_UPDATE_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->SetRatingPoint(rs.fRatingPoint);
		user->LoseLeadOrSend();
	}
}
void CMatchSvrProxy::Process_AchieveRewardNot(CReceivePacketBuffer* rp)
{
	SM2G_ACHIEVE_REWARD_NOT sM2G_ACHIEVE_REWARD_NOT;
	rp->Read((BYTE*)&sM2G_ACHIEVE_REWARD_NOT, sizeof(SM2G_ACHIEVE_REWARD_NOT));

	CScopedRefGameUser user(sM2G_ACHIEVE_REWARD_NOT.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_ACHIEVE_REWARD_NOT);
		PacketComposer.Add(sM2G_ACHIEVE_REWARD_NOT.iRewardIndex);
		PacketComposer.Add((BYTE*)sM2G_ACHIEVE_REWARD_NOT.szTakeGameID, MAX_GAMEID_LENGTH + 1);
		PacketComposer.Add(sM2G_ACHIEVE_REWARD_NOT.iReceivePoint);

		user->Send(&PacketComposer);	
	}
}

void CMatchSvrProxy::Process_AchieveCometitionRewardNot(CReceivePacketBuffer* rp)
{
	SM2G_ACHIEVE_COMPETITION_REWARD_NOT sM2G_ACHIEVE_COMPETITION_REWARD_NOT;
	rp->Read((BYTE*)&sM2G_ACHIEVE_COMPETITION_REWARD_NOT, sizeof(SM2G_ACHIEVE_COMPETITION_REWARD_NOT));
	
	CScopedRefGameUser user(sM2G_ACHIEVE_COMPETITION_REWARD_NOT.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_ACHIEVE_COMPETITION_REWARD_NOT);
		PacketComposer.Add(sM2G_ACHIEVE_COMPETITION_REWARD_NOT.iRewardIndex);
		PacketComposer.Add((BYTE*)sM2G_ACHIEVE_COMPETITION_REWARD_NOT.szFromGameID, MAX_GAMEID_LENGTH + 1);
		PacketComposer.Add((BYTE*)sM2G_ACHIEVE_COMPETITION_REWARD_NOT.szTakeGameID, MAX_GAMEID_LENGTH + 1);
		PacketComposer.Add(sM2G_ACHIEVE_COMPETITION_REWARD_NOT.iReceivePoint);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_UserPlayGameAchievement(CReceivePacketBuffer* rp)
{
	SM2G_USER_PLAYGAME_ACHIEVEMENT sM2G_USER_PLAYGAME_ACHIEVEMENT;
	rp->Read((BYTE*)&sM2G_USER_PLAYGAME_ACHIEVEMENT, sizeof(SM2G_USER_PLAYGAME_ACHIEVEMENT));

	CScopedRefGameUser user(sM2G_USER_PLAYGAME_ACHIEVEMENT.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_USER_PLAYGAME_ACHIEVEMENT);
		PacketComposer.Add(sM2G_USER_PLAYGAME_ACHIEVEMENT.iDataIndex);
		PacketComposer.Add(sM2G_USER_PLAYGAME_ACHIEVEMENT.iDataValue);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_OpenPointRewardCardNot(CReceivePacketBuffer* rp)
{
	SM2G_OPEN_POINT_REWARD_CARD_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_OPEN_POINT_REWARD_CARD_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer PacketComposer(S2C_OPEN_POINT_REWARD_CARD_NOT);
		PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH);
		PacketComposer.Add(rs.btRewardType);
		PacketComposer.Add(rs.iRewardValue);
		PacketComposer.Add(rs.iItemCount);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_ActionAnimRes( CReceivePacketBuffer* rp )
{
	SM2G_ACTION_ANIM_RES	info;
	rp->Read((PBYTE)&info, sizeof(SM2G_ACTION_ANIM_RES));
	
	CScopedRefGameUser user(info.iGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer	PacketComposer(S2C_ROOM_ACTION_ANI_RES);
		PacketComposer.Add(info.iActionCode);
		PacketComposer.Add(info.iKind);
		PacketComposer.Add((PBYTE)info.szAniGameID, MAX_GAMEID_LENGTH + 1);
		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::SendActionAnimReq(SG2M_ACTION_ANIM_REQ Info)
{
	CPacketComposer PacketComposer( G2M_ACTION_ROOM_ANIM_REQ );
	PacketComposer.Add((PBYTE)&Info, sizeof(SG2M_ACTION_ANIM_REQ));
	Send(&PacketComposer);
}

void CMatchSvrProxy::SendUpdateChannelBuffStatusReq(SG2M_UPDATE_CHANNEL_BUFF_STATUS info)
{
	CPacketComposer PacketComposer(G2M_UPDATE_CHANNEL_BUFF_STATUS);
	PacketComposer.Add((PBYTE)&info, sizeof(SG2M_UPDATE_CHANNEL_BUFF_STATUS));
	Send(&PacketComposer);
}

void CMatchSvrProxy::Process_EventGiveEventCash(CReceivePacketBuffer* rp)
{
	SM2G_EVENT_GIVE_EVENTCASH rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_EVENT_GIVE_EVENTCASH));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		int iCurrentEventCash = user->GetEventCoin() + rs.iGiveEventCash;
		user->SetEventCoin( iCurrentEventCash );
		user->SendUserGold(user->GetCoin(),user->GetEventCoin(), user->GetSkillPoint(), rs.bPlaying);
	}
}

void CMatchSvrProxy::Process_GM_FORBID_MULITUSER_REQ( CReceivePacketBuffer* rp )
{
	SM2G_GM_FORBID_MULITUSER_REQ rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_GM_FORBID_MULITUSER_REQ));

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pCenter);

	CFSLoginGlobalODBC* pLoginGlobalODBC = (CFSLoginGlobalODBC*)ODBCManager.GetODBC(ODBC_LOGINGLOBAL);
	CHECK_NULL_POINTER_VOID( pLoginGlobalODBC );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		int iForbidUserIDIndex = -1, iResult = -1;

		if(ODBC_RETURN_SUCCESS == pGameODBC->spGetUserIDIndexFromGameIDOrIndex(rs.szFrobiddenGameID, 0, iForbidUserIDIndex))
		{
			if(ODBC_RETURN_SUCCESS == pLoginGlobalODBC->GMForbidUser(user->GetUserID(), iForbidUserIDIndex, rs.iForbiddenTime, rs.szKickReason, iResult))
			{
				pCenter->SendRequestKickOut((char*)rs.szFrobiddenGameID);
			}
		}
		else
		{
			iResult = -2;
		}
 		CPacketComposer packet(GM_S2G_FORBID_USER_RES);
 		packet.Add((BYTE*)rs.szFrobiddenGameID, MAX_GAMEID_LENGTH+1);
 		packet.Add(iResult);
 		user->Send(&packet);
	}
}

void CMatchSvrProxy::Process_UpdateFriendPlayCount( CReceivePacketBuffer* rp )
{
	SM2G_FRIEND_PLAY_COUNT rs;
	rp->Read( (BYTE*)&rs,sizeof( SM2G_FRIEND_PLAY_COUNT ) );

	CScopedRefGameUser pGameUser(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pGameUser)

	CPacketComposer packetComposer( S2C_FRIEND_PLAYCOUNT_ADD );
	packetComposer.Add( (BYTE*)rs.szFriendGameId , MAX_GAMEID_LENGTH + 1 );
	pGameUser->Send( &packetComposer );
}

void CMatchSvrProxy::Process_EventSendPaper(CReceivePacketBuffer* rp)
{
	SM2G_EVENT_SEND_PAPER rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_EVENT_SEND_PAPER));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if (user != NULL)
	{
		user->RecvPaper( rs.iPaperIndex);
	}
}

void CMatchSvrProxy::SendCheerLeaderChangeReq(SG2M_CHEERLEADER_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2M_CHEERLEADER_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_CHEERLEADER_CHANGE_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::Process_MatchLocationNOT(CReceivePacketBuffer* rp)
{
	SM2G_MATCH_LOCATION_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_MATCH_LOCATION_NOT));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	if (TRUE == rs.bIsLogOn)
	{
		if (0 < User->GetMatchLocation() && 
			GetProcessID() != User->GetMatchLocation())
		{
			//이전 매치서버 정보가 있으면 로그아웃시킴
			CMatchBaseSvrProxy* pOldProxy = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(User->GetMatchLocation());
			if (NULL != pOldProxy) 
				pOldProxy->SendUserLogout(User.GetPointer());
		}
		User->SetMatchLocation(GetProcessID());
		User->SendAvatarInfoUpdateToMatch();
	}
	else
	{
		if (GetProcessID() == User->GetMatchLocation())
			User->SetMatchLocation(-1);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLIENT_INFO)
{
	SM2G_CLIENT_INFO info;
	rp->Read((BYTE*)&info, sizeof(SM2G_CLIENT_INFO));

	SetAutoTeam(info.bIsAutoRegTeam);
	CFSGameServer::GetInstance()->BroadCastMatchServerConnect(this);
}

void CMatchSvrProxy::SendUserInfoReq(SG2M_USER_INFO_REQ info)
{
	CPacketComposer PacketComposer(G2M_USER_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_USER_INFO_REQ));

	Send(&PacketComposer);
}

void CMatchSvrProxy::Process_UserInfoReq(CReceivePacketBuffer* rp)
{
	SM2G_USER_INFO_RES rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_USER_INFO_RES));

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	if ( NULL == pServer )
	{
		return ;
	}

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	if ( 0 == strcmp( User->GetGameID(), rs.szPeerGameID ) )
	{
		SAvatarInfo AvatarInfo;
		if(FALSE == User->CurUsedAvatarSnapShot(AvatarInfo)) 
		{
			return;
		}

		CPacketComposer PacketComposer(S2C_USER_INFO_RES);
		PacketComposer.Add((BYTE)1);
		PacketComposer.Add(rs.shType);

		User->MakePacketForUserInfoStat(PacketComposer, pServer, rs.iLinkItemAddStat);

		User->Send(&PacketComposer);
	}
	else
	{
		CPacketComposer PacketComposer(S2C_USER_INFO_RES);

		CScopedRefGameUser pPeerUser(rs.szPeerGameID);
		if( pPeerUser != NULL )
		{
			SAvatarInfo AvatarInfo;
			if(FALSE == pPeerUser->CurUsedAvatarSnapShot(AvatarInfo)) 
			{
				return;
			}
			PacketComposer.Add((BYTE)1);
			PacketComposer.Add(rs.shType);

			pPeerUser->MakePacketForUserInfoStat(PacketComposer, pServer, rs.iLinkItemAddStat);
			User->Send(&PacketComposer);
		}
		else
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);

			if(pCenter) pCenter->SendUserSInfo(rs.szPeerGameID, User->GetGameID(), USER_INFO_STAT, 0, 0, rs.iLinkItemAddStat );
			return;
		}
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_GIVE_REWARD_EXHAUST_ITEM_TO_INVEN_REQ)
{
	SM2G_GIVE_REWARD_EXHAUST_ITEM_TO_INVEN_REQ info;
	rp->Read((BYTE*)&info, sizeof(SM2G_GIVE_REWARD_EXHAUST_ITEM_TO_INVEN_REQ));

	SRewardConfig* pReward = REWARDMANAGER.GetReward(info.iRewardIndex);
	CHECK_NULL_POINTER_VOID(pReward);

	CHECK_CONDITION_RETURN_VOID(REWARD_TYPE_EXHAUST_ITEM_TO_INVEN == pReward->iRewardType);

	CScopedRefGameUser User(info.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->GiveReward_ExhaustItemToInven((SRewardConfigItem*)pReward);
}

void CMatchSvrProxy::Process_EquipProductListInRoomNot(CReceivePacketBuffer* recvPacket)
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();

	CActionInfluenceList* pActionInfluenceList = COACHCARD.GetActionInfluenceList();
	CHECK_NULL_POINTER_VOID(pActionInfluenceList);

	int iTargetGameIDIndex;
	int iPlayerCount = 0;
	recvPacket->Read(&iTargetGameIDIndex);
	recvPacket->Read(&iPlayerCount);

	CPacketComposer PacketComposer(S2C_ACTION_INFLUENCE_LIST_IN_ROOM_NOT);
	PacketComposer.Add(iPlayerCount);

	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);
	int iEquipActionInfluenceCountx = 0;
	int iProductIndex = INVALID_IDINDEX;
	SActionInfluenceConfig sActionInfluenceConfig;
	int iActionInfluenceCount = 0;

	for(int iPlayerIndex = 0; iPlayerIndex < iPlayerCount; iPlayerIndex++)
	{
		recvPacket->Read((PBYTE)szGameID, sizeof(char) * ( MAX_GAMEID_LENGTH + 1) );
		PacketComposer.Add((PBYTE)szGameID, sizeof(char) * ( MAX_GAMEID_LENGTH + 1) );

		recvPacket->Read(&iEquipActionInfluenceCountx);
		PacketComposer.Add(iEquipActionInfluenceCountx);

		int iActionInfluenceIndex = INVALID_IDINDEX;

		for(int i =0 ; i < iEquipActionInfluenceCountx; i++)
		{
			recvPacket->Read(&iActionInfluenceIndex);

			if( FALSE == pActionInfluenceList->FindAndGetActionInfluenceConfig(iActionInfluenceIndex, sActionInfluenceConfig))
			{
				g_LogManager.WriteLogToFile("Don't Find ActionInfluence Config In Process_EquipProductListInRoomNot");
				return;
			}

			PacketComposer.Add(sActionInfluenceConfig.iActionType);
			PacketComposer.Add(sActionInfluenceConfig.iInfluenceType);
			PacketComposer.Add(sActionInfluenceConfig.fValue);
		}
	}

	CScopedRefGameUser pUser(iTargetGameIDIndex);
	if(pUser != NULL)
	{
		pUser->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_Potential_Ability(CReceivePacketBuffer* recvPacket)
{
	SM2G_COACHCARD_POTENTIAL_ABILITY rs;
	recvPacket->Read( (BYTE*)&rs, sizeof(SM2G_COACHCARD_POTENTIAL_ABILITY) );

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	CScopedRefGameUser pUser(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_POTENTIAL_ABILITY_LIST_IN_ROOM_NOT);

	PacketComposer.Add( TENDENCY_MAX_COUNT );
	for( int i = 0; i < TENDENCY_MAX_COUNT; ++i )
	{
		if( POTENTIAL_COMPONENT_ABILITY == rs.sPotentialConfig[i].eComponentIndex )
		{
			// The potential-card ability information output in the property-list
			int iPropertyIndex = rs.sPotentialConfig[i].iParam;
			CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList( iPropertyIndex );
			CHECK_NULL_POINTER_VOID(pItemProperty);

			vector<SItemProperty> vecItemProperty;
			pItemProperty->GetItemProperty( vecItemProperty );
			int iSize = vecItemProperty.size();

			// Packet Add
			PacketComposer.Add( iSize );
			for( int i = 0; i < iSize; ++i )
			{
				PacketComposer.Add( vecItemProperty[i].iProperty );
				PacketComposer.Add( vecItemProperty[i].iValue );
			}
		}
		else
		{
			PacketComposer.Add( 0 );
			PacketComposer.Add( 0 );
			PacketComposer.Add( 0 );
		}

	}

	pUser->Send(&PacketComposer);
}

void CMatchSvrProxy::Process_Potential_Balance(CReceivePacketBuffer* recvPacket)
{
	SM2G_COACHCARD_POTENTIAL_BALANCE rs;
	recvPacket->Read( (BYTE*)&rs, sizeof(SM2G_COACHCARD_POTENTIAL_BALANCE) );

	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CScopedRefGameUser pUser(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_POTENTIAL_BALANCE_LIST_IN_ROOM_NOT);
	// set array point gap
	int iRageTendency = 0;	
	std::set<int> setPointGap;
	setPointGap.clear();
	set<int>::iterator itr;
	for( int i = 0; i < TENDENCY_MAX_COUNT; ++i )
	{
		if( POTENTIAL_COMPONENT_BALANCE == rs.sPotentialConfig[i].eComponentIndex )
			setPointGap.insert(rs.sPotentialConfig[i].iParam);
	}
	// set rage component count
	int iRageComponentCount;
	if(setPointGap.size() == 0)
		iRageComponentCount = 0;
	else
		iRageComponentCount = 1;
		
	int iMinPointGap = 11;
	int iPointGap = 0;
	for(itr = setPointGap.begin(); itr != setPointGap.end(); itr++)
	{
		iPointGap = *itr;
		if(iMinPointGap >= iPointGap)
		{
			iMinPointGap = iPointGap;
		}
	}

	PacketComposer.Add( iRageComponentCount );

	if(iRageComponentCount != 0)
	{
		PacketComposer.Add( iMinPointGap );
		vector<SRageBalance> vecRageBalance;
		RAGE.GetRageBalance( vecRageBalance, iMinPointGap );
		int iSize = vecRageBalance.size();
		PacketComposer.Add( iSize );
		for( int i = 0; i < iSize; ++i )
		{
			PacketComposer.Add( vecRageBalance[i].iAbilityType );
			PacketComposer.Add( vecRageBalance[i].fValue );
		}
	}

	pUser->Send(&PacketComposer);
}


void CMatchSvrProxy::Process_RageAvatarInfluenceList(CReceivePacketBuffer* rp)
{
	int iTargetGameIDIndex;
	rp->Read(&iTargetGameIDIndex);

	CScopedRefGameUser user(iTargetGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer Packet(S2C_RAGE_AVATAR_INFLUENCE_LIST_NOT);
		Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
		user->Send(&Packet);
	}
}

void CMatchSvrProxy::Process_NetState( CReceivePacketBuffer* rp )
{
	SG2M_NET_STATE rs;
	rp->Read((BYTE*)&rs, sizeof(SG2M_NET_STATE));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if(user != NULL)
	{
		user->SetNetState(rs.eNetState);
	}
}

void CMatchSvrProxy::Process_Game_Guide_UI( CReceivePacketBuffer* rp )
{
	SM2G_SET_GAME_GUIDE_UI rs;

	rp->Read((BYTE*)&rs, sizeof(SM2G_SET_GAME_GUIDE_UI));

	CScopedRefGameUser user(rs.iGameIDIndex);


	if(user != NULL)
	{
		CPacketComposer PacketComposer(S2C_GAME_GUIDE_UI);		
		PacketComposer.Add(rs.bIsGuideUI);

		user->Send(&PacketComposer);
	}
}

void CMatchSvrProxy::Process_EVENT_SportBigEvent( CReceivePacketBuffer* rp )
{
  	if(FALSE == SBIGEVENTManager.CollisionTimeCheck())
  	{
  		return;
  	}
 
 	int iTargetGameIDIndex;
 	rp->Read(&iTargetGameIDIndex);
 
 	CScopedRefGameUser user(iTargetGameIDIndex);
 
   	if( user != NULL )
  	{
  		BYTE btCountVal = 0;
  
  		CFSODBCBase*	pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
  		CHECK_NULL_POINTER_VOID(pODBCBase);
  
  		time_t CurrentDate = _time64(NULL);

  		SYSTEMTIME sCurrentTime;
  		::GetLocalTime(&sCurrentTime);
  
  		enum WEEKDAY{SUN,MON,TUE,WED,THU,FRI,SAT,MAX_WEEKDAY_COUNT,};
  
  		BYTE btCount = 0;
  		if( sCurrentTime.wDayOfWeek == SAT || sCurrentTime.wDayOfWeek == SUN )
  			btCount = 2;
  		else
  			btCount = 1;
  
  		if( ODBC_RETURN_SUCCESS == pODBCBase->EVENT_InsertSportBigEventUser(user->GetUserIDIndex(), btCount))
  		{
  			CPacketComposer Packet(S2C_EVENT_COMPONENT_MSG_RES);
  			Packet.Add(0);
  			Packet.Add(0);
  			Packet.Add(0);
  			Packet.Add(43);
  			Packet.Add(-1);
  			Packet.Add(-1);
  			Packet.Add(-1);
  			Packet.Add(-1);
  			Packet.Add(-1);
  
  			user->Send(&Packet);
   		}
  	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_ENTER_GAMEROOM_RES)
{
	SM2G_CLUBTOURNAMENT_ENTER_GAMEROOM_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_CLUBTOURNAMENT_ENTER_GAMEROOM_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_ENTER_GAMEROOM_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUBTOURNAMENT_ENTER_GAMEROOM_RES));
	User->Send(&Packet);

	CHECK_CONDITION_RETURN_VOID(rs.info.btResult != RESULT_CLUBTOURNAMENT_ENTER_GAMEROOM_SUCCESS);

	CFSGameClient* pClient = (CFSGameClient*)User->GetClient();
	if(NULL != pClient)
	{
		pClient->SetState(NEXUS_WAITROOM);
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL rq;
	rq.iGameIDIndex = User->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_TIME_INFO_REQ, &rq, sizeof(SG2CL)); 
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_NOT)
{
	SM2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_NOT info;
	rp->Read((BYTE*)&info, sizeof(SM2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_NOT));
	FSLOBBY.BroadCastToClubTournamentVoteRoom(&Packet, info.info.btRoundGroupIndex);
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_JOIN_PLAYER_EXIT_NOT)
{
	SM2G_CLUBTOURNAMENT_JOIN_PLAYER_EXIT_NOT info;
	rp->Read((BYTE*)&info, sizeof(SM2G_CLUBTOURNAMENT_JOIN_PLAYER_EXIT_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_JOIN_PLAYER_EXIT_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_JOIN_PLAYER_EXIT_NOT));
	FSLOBBY.BroadCastToClubTournamentVoteRoom(&Packet, info.info.btRoundGroupIndex);
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_START_FAIL_NOT)
{
	SM2G_CLUBTOURNAMENT_START_FAIL_NOT info;
	rp->Read((BYTE*)&info, sizeof(SM2G_CLUBTOURNAMENT_START_FAIL_NOT));

	CScopedRefGameUser User(info.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_START_FAIL_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_START_FAIL_NOT));
	User->Send(&Packet);

	SendUserLogout(User.GetPointer());
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_LEFT_SEAT_RES)
{
	SM2G_LEFT_SEAT_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_LEFT_SEAT_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_LEFT_SEAT_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_LEFT_SEAT_RES));
	User->Send(&Packet);
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_LEFT_SEAT_NOTICE)
{
	SM2G_LEFT_SEAT_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_LEFT_SEAT_NOTICE));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_LEFT_SEAT_NOTICE);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_LEFT_SEAT_NOTICE));
	User->Send(&Packet);
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_MATCH_ADVANTAGE_INFLUENCE_LIST)
{
	int iTargetGameIDIndex;
	rp->Read(&iTargetGameIDIndex);

	CScopedRefGameUser user(iTargetGameIDIndex);
	if (user != NULL)
	{
		CPacketComposer Packet(S2C_MATCH_ADVANTAGE_INFLUENCE_LIST);
		Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_BURNING_STEP_NOT)
{
	SM2G_BURNING_STEP_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_BURNING_STEP_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if( user != NULL )
	{
		CPacketComposer Packet(S2C_BURNING_PLAYCOUNT_STEP_NOTICE);

		SS2C_BURNING_PLAYCOUNT_STEP_NOTICE	info;
		
		info.btBurningComplete = rs.btBurningComplete;
		info.iBurningCurrStep = rs.iBurningCurrStep;
		info.iBurningPlayCount = rs.iBurningPlayCount;
		info.btRewardType = rs.btRewardType;

		Packet.Add((PBYTE)&info, sizeof(SS2C_BURNING_PLAYCOUNT_STEP_NOTICE));
		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CEHCK_OPENED_TEAM_COUNT_RES)
{
	SM2G_CEHCK_OPENED_TEAM_COUNT_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SM2G_CEHCK_OPENED_TEAM_COUNT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if( user != NULL )
	{
		SS2C_MATCHINGPOOLCARE_REWARD_NOTICE	info;
		if(TRUE == MATCHINGPOOLCAREMANAGER.CheckTarget(user->GetGameIDIndex(), user->GetCurUsedAvtarLvGrade(), rs.iOpenedTeamCount, info))
		{
			if(TRUE == MATCHINGPOOLCAREMANAGER.UpdateGiveLog(user->GetGameIDIndex(), user->GetCurUsedAvtarLvGrade()))
			{	
				SMatchingPoolCareUserInfo stMatchingPoolCareInfo;
				stMatchingPoolCareInfo.iMaxPlayCount = info.iMaxPlayCount;
				stMatchingPoolCareInfo.iExp = info.iExp;
				stMatchingPoolCareInfo.iPoint = info.iPoint;
				user->SetMatchingPoolCareInfo(stMatchingPoolCareInfo);

				CPacketComposer Packet(S2C_MATCHINGPOOLCARE_REWARD_NOTICE);
				Packet.Add((PBYTE)&info, sizeof(SS2C_MATCHINGPOOLCARE_REWARD_NOTICE));
				user->Send(&Packet);

				CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
				if(pCenter != NULL) 
				{
					SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ ss;
					ss.iGameIDIndex = user->GetGameIDIndex();
					strncpy_s(ss.GameID, _countof(ss.GameID), user->GetGameID(), MAX_GAMEID_LENGTH);
					ss.btUpdateType = MATCHINGPOOLCARE_UPDATE_TARGETUSER_TYPE_ADD;
					ss.btLvGrade = user->GetCurUsedAvtarLvGrade();
					pCenter->SendPacket(G2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ, &ss, sizeof(SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ));
				}
			}
		}
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_GIVE_REWARD_ACHIEVEMENT_NOT)
{
	SM2G_GIVE_REWARD_ACHIEVEMENT_NOT	rs;
	rp->Read((PBYTE)&rs, sizeof(SM2G_GIVE_REWARD_ACHIEVEMENT_NOT));

	CPacketComposer Packet(S2C_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT);	
	const int cnSizeOne = 1;
	
	Packet.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH + 1);
	Packet.Add(cnSizeOne);
	Packet.Add(rs.iAchievementIndex);

	CScopedRefGameUser user(rs.iGameIDIndex);
	if( user != NULL )
	{
		user->GiveReward_Achievement(rs.iAchievementIndex, FALSE);
	}

	if( rs.iScore >= ANNONCE_CONDITION_SCORE )
	{
		CFSGameServer::GetInstance()->BroadCast(Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_UPDATE_HOTGIRL_MISSION_COUNT_REQ)
{
	SM2G_UPDATE_HOTGIRL_MISSION_COUNT_REQ rq;
	rp->Read((PBYTE)&rq, sizeof(SM2G_UPDATE_HOTGIRL_MISSION_COUNT_REQ));

	CScopedRefGameUser	user(rq.iGameIDIndex);
	if( user != NULL )
	{
		user->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(rq.iMissionType, rq.iCount);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_UPDATE_MISSIONBINGO_MISSION_COUNT_REQ)
{
	SM2G_UPDATE_MISSIONBINGO_MISSION_COUNT_REQ rq;
	rp->Read((PBYTE)&rq, sizeof(SM2G_UPDATE_MISSIONBINGO_MISSION_COUNT_REQ));

	CScopedRefGameUser	user(rq.iGameIDIndex);
	if( user != NULL )
	{
		user->GetUserMissionBingoEvent()->CheckAndUpdateMission(rq.iMissionType, rq.iCount);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CHARACTOR_STAT_MODULATION_CHECK_RES)
{
	SM2G_BASE	rs;
	rp->Read((PBYTE)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser	user(rs.iGameIDIndex);
	if( user != NULL )
	{
		const int cnMAXBuffer = 320;
		int iDataSize = 0, iHackType = 0;
		BYTE aData[cnMAXBuffer] = {NULL,};

		rp->Read(&iHackType);
		rp->Read(&iDataSize);
		rp->Read((PBYTE)&aData , iDataSize);

		user->ProcessCheckAvatarSomeAction_ChangeStatus(aData, iDataSize, iHackType);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_USER_APPRAISAL_RES)
{
	//SM2G_USER_APPRAISAL_RES rs;
	//rp->Read((PBYTE)&rs, sizeof(SM2G_USER_APPRAISAL_RES));

	//CScopedRefGameUser user(rs.iGameIDIndex);
	//if(user != NULL)
	//{
	//	CPacketComposer Packet(S2C_USER_APPRAISAL_RES);
	//	Packet.Add(rs.btResult);
	//	Packet.Add(rs.btMatchType);
	//	Packet.Add((BYTE)0);	// btServerIndex 0 
	//	Packet.Add(rs.iReqGameIDIndex);
	//	user->Send(&Packet);
	//}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_USER_APPRAISAL_UPDATE_NOT)
{
	SM2G_BASE not;
	rp->Read((PBYTE)&not, sizeof(SM2G_BASE));

	CScopedRefGameUser user(not.iGameIDIndex);
	if(user != NULL)
	{
		int iPoint[MAX_USER_APPRAISAL_KIND_COUNT];
		ZeroMemory(iPoint, sizeof(int)*MAX_USER_APPRAISAL_KIND_COUNT);
		user->GetUserAppraisal()->GetPoint(iPoint);
		user->GetUserAppraisal()->UpdatePoint();

		if(TRUE == user->GetUserAppraisal()->CheckReciveReward(iPoint))
		{
			CPacketComposer Packet(S2C_USER_APPRAISAL_REWARD_RECIVE_NOT);
			user->Send(&Packet);
		}
	}
}

void CMatchSvrProxy::SendActionSlot( CFSGameUser* pUser )
{
	CFSGameServer* pServer = (CFSGameServer*)GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_VOID(pODBCBase);

	CPacketComposer PacketComposer(G2M_ACTION_SLOT);
	SG2M_ACTION_SLOT_UPDATE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());
	pUser->CopyActionSlotPacket(pODBCBase, pServer->GetActionShop(), info);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ACTION_SLOT_UPDATE));

	Send(&PacketComposer);
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_TRANSFORM_STATUS_NOT)
{
	SM2G_TRANSFORM_STATUS_NOT not;
	rp->Read((PBYTE)&not, sizeof(SM2G_TRANSFORM_STATUS_NOT));

	CScopedRefGameUser user(not.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer Packet(S2C_TRANSFORM_STATUS_NOT);
		Packet.Add((BYTE*)&not.info, sizeof(SS2C_TRANSFORM_STATUS_NOT));
		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_UPDATE_ROOM_INFO_RES)
{
	SM2G_PVE_UPDATE_ROOM_INFO_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SM2G_PVE_UPDATE_ROOM_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_PVE_UPDATE_ROOM_INFO_RES info;

		info.btPlayerNum = rs.btPlayerNum;
		info.btIsAllowToObserve = rs.btIsAllowToObserve;
		info.iLOD = rs.iLOD;
		for( int i = 0 ; i < MAX_ROOM_AI_POSITION ; ++i ) 
			info.shAIPosition[i] = rs.shAIPosition[i];

		CPacketComposer Packet(S2C_PVE_UPDATE_ROOM_INFO_RES);
		Packet.Add((PBYTE)&info,sizeof(SS2C_PVE_UPDATE_ROOM_INFO_RES));
		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_AI_BALANCE_STAT_NOT)
{
	int iGameIDIndex = 0;
	SM2G_PVE_AI_BALANCE_STAT_NOT rs;
	rp->Read(&iGameIDIndex);
	rp->Read((PBYTE)&rs, sizeof(SM2G_PVE_AI_BALANCE_STAT_NOT));

	CScopedRefGameUser user(iGameIDIndex);
	if(user != NULL)
	{
		SS2C_PVE_AI_BALANCE_STAT_NOT not;
		not.iAICount = rs.iAICount;
		if( 5 < not.iAICount ) return;

		CPacketComposer Packet(S2C_PVE_AI_BALANCE_STAT_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_PVE_AI_BALANCE_STAT_NOT));

		for( int i = 0 ; i < rs.iAICount && i < 5 ; ++i )
		{
			SPVE_AI_BALANCE_INFO info;

			rp->Read((PBYTE)info.szAIGameID, MAX_GAMEID_LENGTH+1);
			rp->Read((PBYTE)info.fValue, sizeof(float)*MAX_AI_BALANCE_STAT_COUNT);

			Packet.Add((PBYTE)info.szAIGameID,MAX_GAMEID_LENGTH+1);
			Packet.Add((PBYTE)info.fValue, sizeof(float)*MAX_AI_BALANCE_STAT_COUNT);
		}

		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_AIPVP_CHANGE_GAMEPLAYERNUM_RES)
{
	int iGameIDIndex = 0;
	SM2G_AIPVP_CHANGE_GAMEPLAYERNUM_RES rs;

	rp->Read(&iGameIDIndex);
	rp->Read((PBYTE)&rs, sizeof(SM2G_AIPVP_CHANGE_GAMEPLAYERNUM_RES));

	CScopedRefGameUser user(iGameIDIndex);
	if(user != NULL)
	{
		SS2C_AIPVP_CHANGE_GAMEPLAYERNUM_RES info;
		info.btPlayerNum = rs.btPlayerNum;
		info.btResult = rs.btResult;

		CPacketComposer Packet(S2C_AIPVP_CHANGE_GAMEPLAYERNUM_RES);
		Packet.Add((PBYTE)&info, sizeof(SS2C_AIPVP_CHANGE_GAMEPLAYERNUM_RES));
		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT)
{
	SM2G_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT rs;
	rp->Read((PBYTE)&rs, sizeof(SM2G_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT info;
		info = rs.info;

		CPacketComposer Packet(S2C_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT);
		Packet.Add((PBYTE)&info,sizeof(SS2C_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT));
		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_CHANGE_AI_TEAM_MEMBER_RES)
{
	SM2G_PVE_CHANGE_AI_TEAM_MEMBER_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SM2G_PVE_CHANGE_AI_TEAM_MEMBER_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_PVE_CHANGE_AI_TEAM_MEMBER_RES info;
		memcpy(info.sChangeAIAvatarInfo, rs.info, sizeof(SPVE_CHANGE_AI_INFO)*MAX_PVE_AI_COUNT);
		memcpy(info.sChangeAIAvatarBalanceInfo, rs.balanceinfo, sizeof(SPVE_AI_BALANCE_INFO)*MAX_PVE_AI_COUNT);

		CPacketComposer Packet(S2C_PVE_CHANGE_AI_TEAM_MEMBER_RES);
		Packet.Add((PBYTE)&info,sizeof(SS2C_PVE_CHANGE_AI_TEAM_MEMBER_RES));
		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(G2M_FACTION_UPDATE_POINT_SCORE_NOT)
{
	SG2M_FACTION_UPDATE_POINT_SCORE_NOT not;
	rp->Read((PBYTE)&not,sizeof(SG2M_FACTION_UPDATE_POINT_SCORE_NOT));

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	if (0 < not.iFactionPoint && 
		ODBC_RETURN_SUCCESS != pODBCBase->FACTION_UpdateUserFactionPoint(not.iUserIDIndex, not.iFactionPoint))
	{
		WRITE_LOG_NEW(LOG_TYPE_FACTION, CALL_SP, FAIL
			, "FACTION_UpdateUserFactionPoint, Useridindex:10752790, FactionPoint:0"
not.iUserIDIndex, not.iFactionPoint);	
	}

	if (0 < not.iFactionScore && 
		ODBC_RETURN_SUCCESS != pODBCBase->FACTION_UpdateUserFactionScore(
		not.iUserIDIndex, not.iCurrentDistrictIndex, not.iFactionIndex, not.iFactionScore))
	{
		WRITE_LOG_NEW(LOG_TYPE_FACTION, CALL_SP, FAIL
			, "FACTION_UpdateUserFactionScore, Useridindex:10752790, FactionScore:0, DistrictIndex:17051648, FactionIndex:-858993460"
ex, not.iFactionScore, not.iCurrentDistrictIndex, not.iFactionIndex);	
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_HELLONEWYEAR_CHECK_EXP_BUFF_REQ)
{
	SM2G_HELLONEWYEAR_CHECK_EXP_BUFF_REQ	rs;
	rp->Read((PBYTE)&rs,sizeof(SM2G_HELLONEWYEAR_CHECK_EXP_BUFF_REQ));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		SG2M_HELLONEWYEAR_CHECK_EXP_BUFF_RES	ss;
		ss.iGameIDIndex = rs.iGameIDIndex;
		ss.btServerIndex = _GetServerIndex;
		ss.iExpBuffRate = user->GetUserHelloNewYearEvent()->GetUserBuffRate(true);

		CPacketComposer Packet(G2M_HELLONEWYEAR_CHECK_EXP_BUFF_RES);
		Packet.Add((PBYTE)&ss, sizeof(SG2M_HELLONEWYEAR_CHECK_EXP_BUFF_RES));
		Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_FRIEND_ACCOUNT_ADD_WITHPLAYCNT_NOT)
{
	SM2G_FRIEND_ACCOUNT_ADD_WITHPLAYCNT_NOT	rs;
	rp->Read((PBYTE)&rs,sizeof(SM2G_FRIEND_ACCOUNT_ADD_WITHPLAYCNT_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(user);

	CScopedLockFriendAccount pFriend(user->GetFriendAccountList(), rs.iFriendIDIndex);
	if(pFriend != nullptr)
	{
		pFriend->SetWithPlayCnt(pFriend->GetWithPlayCnt() + 1);

		SS2C_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT	ss;
		ss.tCurrentTime = _GetCurrentDBDate;
		pFriend->GetData(ss.sInfo);
		user->Send(S2C_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT, &ss, sizeof(SS2C_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT));
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT)
{
	SM2G_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT info;
	rp->Read((PBYTE)&info,sizeof(SM2G_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT));

	CScopedRefGameUser user(info.iGameIDIndex);
	if(user != NULL)
	{
		CPacketComposer Packet(S2C_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT);

		SS2C_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT not;
		not.dwValue = info.dwValue;
		not.btMode = info.btMode;

		if(info.btMode == RESULT_BESTPLAYER_MODE_ADD_REWARD)
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(user.GetPointer(), GAMERESULT_BESTPLAYER_REWARD_INDEX);
			//not.btMode = nullptr != pReward ? RESULT_BESTPLAYER_MODE_ADD_REWARD : not.btMode;

			Packet.Add((PBYTE)&not,sizeof(SS2C_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT));
			SREWARD_INFO reward;
			ZeroMemory(&reward, sizeof(SREWARD_INFO));

			if(nullptr != pReward)
			{
				SRewardConfigItem* pRewardItem = dynamic_cast<SRewardConfigItem*>(pReward);
				if(nullptr != pRewardItem)
				{
					reward.btRewardType		= pRewardItem->GetRewardType();
					reward.iItemCode		= pRewardItem->GetRewardValue();
					reward.iPropertyType	= pRewardItem->GetPropertyType();
					reward.iPropertyValue	= pRewardItem->GetPropertyValue();
				}
				else
				{
					reward.btRewardType = pReward->GetRewardType();
					reward.iItemCode = pReward->GetRewardValue();
					reward.iPropertyType = 0;
					reward.iPropertyValue = pReward->GetRewardValue();
				}
			}

			Packet.Add((PBYTE)&reward, sizeof(SREWARD_INFO));
		}
		else if(info.btMode == RESULT_BESTPLAYER_MODE_BESTUSER_UPDATE)
		{
			user->GetUserAppraisal()->UpdateLastGameBest(TRUE);
			return;
		}
		else
		{
			Packet.Add((PBYTE)&not,sizeof(SS2C_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT));

			if(info.btMode == RESULT_BESTPLAYER_MODE_BESTOFBEST)
			{
				//user->GetUserAppraisal()->UpdateLastGameBest(TRUE);
				Packet.Add(info.dwBestPlayerRoomID);
			}
			else if(info.btMode == RESULT_BESTPLAYER_MODE_MYVOTE)
			{
				user->GetUserAppraisal()->IncreasePraisePoint();
			}
		}

		user->Send(&Packet);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUB_LEAGUE_OPEN_NOT)
{
	SM2G_CLUB_LEAGUE_OPEN_NOT rs;
	rp->Read((PBYTE)&rs,sizeof(SM2G_CLUB_LEAGUE_OPEN_NOT));

	CLUBCONFIGMANAGER.SetClubLeagueOpenState((OPEN_STATUS)rs.btOpen);

	SS2C_CLUB_LEAGUE_OPEN_NOT not;
	not.btOpenStatus = rs.btOpen;

	CPacketComposer Packet(S2C_CLUB_LEAGUE_OPEN_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_CLUB_LEAGUE_OPEN_NOT));
	CFSGameServer::GetInstance()->BroadCast(Packet);
}


DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_GAME_RESULT_BEST_PLAYER_UPDATE_NOT)
{
	SM2G_GAME_RESULT_BEST_PLAYER_UPDATE_NOT info;
	rp->Read((PBYTE)&info,sizeof(SM2G_GAME_RESULT_BEST_PLAYER_UPDATE_NOT));

	CScopedRefGameUser user(info.iGameIDIndex);
	if(user != NULL)
	{
		user->GetUserAppraisal()->UpdateLastGameBest(info.bIsLastGameBest);
	}
	else
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);

		pODBCBase->USERAPPRAISAL_USER_UpdateAppraisalBestPlayer(info.iGameIDIndex, info.bIsLastGameBest);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_GAME_APPRAISAL_MISSION_NOT)
{
	SM2G_BASE rs;
	rp->Read((PBYTE)&rs, sizeof(SM2G_BASE));

	CScopedRefGameUser user(rs.iGameIDIndex);
	if(user != NULL)
	{
		user->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_APPRAISAL);
		user->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_GAMEPLAY_APPRAISAL);
		user->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_GAMERESULT, WORDPUZZLES_REWARDTYPE_APPRAISAL);
		user->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_GAMEPLAY_APPRAISAL);
		user->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USER_APPRAISAL_COUNT);
	}
}

DEFINE_GAMEMATCHPROXY_PROC_FUNC(M2G_WEBDB_USER_GAMERECORD_NOT)
{
	SM2G_WEBDB_USER_GAMERECORD_NOT rs;
	rp->Read((PBYTE)&rs, sizeof(SM2G_WEBDB_USER_GAMERECORD_NOT));

	CODBCSvrProxy* pODBCProxy = dynamic_cast<CODBCSvrProxy*>(GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pODBCProxy);
	
	CPacketComposer	Packet(S2O_WEBDB_USER_GAMERECORD_NOT);
	SS2O_WEBDB_USER_GAMERECORD_NOT not;
	not.iUserIDIndex	= rs.iUserIDIndex;
	not.iGameIDIndex	= rs.iGameIDIndex;
	not.iPosition		= rs.iPosition;
	not.iPlayIdx		= rs.iPlayIdx;
	not.iPoint2			= rs.iPoint2;
	not.iPoint3			= rs.iPoint3;
	not.iAssistCnt		= rs.iAssistCnt;
	not.iReboundCnt		= rs.iReboundCnt;
	not.iStealCnt		= rs.iStealCnt;
	not.iWinPoint		= rs.iWinPoint;
	not.iIsWin			= rs.iIsWin;
	not.iPlayTime		= rs.iPlayTime;
	not.tPlayEndDate	= rs.tPlayEndDate;
	strncpy_s(not.szGameID,_countof(not.szGameID), rs.szGameID, MAX_GAMEID_LENGTH);
	Packet.Add((PBYTE)&not, sizeof(SS2O_WEBDB_USER_GAMERECORD_NOT));

	pODBCProxy->Send(&Packet);
}
