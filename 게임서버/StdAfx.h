qpragma once

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

qinclude <afx.h>
qinclude <afxwin.h>         // MFC core and standard components
qinclude <afxext.h>         // MFC extensions
qinclude <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
qinclude <afxdb.h>

#ifndef _AFX_NO_AFXCMN_SUPPORT
qinclude <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// 2009.09.17
// Warning 4786 무시 (코드 위치 이동)
// ktKim
// 2010.01.11
// Modify for Migration 2005
// ktKim
qpragma warning(disable : 4786 4114)
// End
// End

qinclude <vector>
qinclude <QUEUE>
qinclude <iostream>
qinclude <cassert>

qinclude "STLWarning.h"
qinclude "NexusCommon.h"
qinclude "FSCommon.h"
qinclude "CFSCommonFunc.h"
qinclude "LogCommon.h"
qinclude "fscentercommon.h"
qinclude "fsclubcommon.h"
qinclude "ChatCommon.h"
qinclude "zwave_sdk_helper.h"

typedef unsigned __int64 uint64;
typedef __int64 int64;
typedef uint64 uint_64;
typedef int64 int_64;




