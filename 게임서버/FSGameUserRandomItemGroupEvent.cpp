qinclude "stdafx.h"
qinclude "RandomItemGroupEventManger.h"
qinclude "FSGameUserRandomItemGroupEvent.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "UserHighFrequencyItem.h"
qinclude "RewardManager.h"
qinclude "CenterSvrProxy.h"

CFSGameUserRandomItemGroupEvent::CFSGameUserRandomItemGroupEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
	ZeroMemory(m_iaDrawCount, sizeof(int)*MAX_EVENT_RANDOMITEM_GROUP_TYPE_COUNT);
	ZeroMemory(m_iaCardCount, sizeof(int)*MAX_EVENT_RANDOMITEM_CARD_TYPE_COUNT);
}

CFSGameUserRandomItemGroupEvent::~CFSGameUserRandomItemGroupEvent(void)
{

}

BOOL CFSGameUserRandomItemGroupEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMGROUPEVENT.IsOpen(), TRUE);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_GROUP_GetUserData(m_pUser->GetUserIDIndex(), m_iaDrawCount) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_GROUP_GetUserCard(m_pUser->GetUserIDIndex(), m_iaCardCount))
	{
		return FALSE;
	}

	return TRUE;
}

void CFSGameUserRandomItemGroupEvent::SendEventOpenStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMGROUPEVENT.IsOpen());

	SS2C_RANDOMITEM_GROUP_EVENT_NOT not;
	not.btOpenStatus = RANDOMITEMGROUPEVENT.IsOpen();

	m_pUser->Send(S2C_RANDOMITEM_GROUP_EVENT_NOT, &not, sizeof(SS2C_RANDOMITEM_GROUP_EVENT_NOT));
}

void CFSGameUserRandomItemGroupEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMGROUPEVENT.IsOpen());

	SS2C_RANDOMITEM_GROUP_EVENT_LIST_RES rs;
	rs.iNormalCoinItemCode = RANDOMITEM_GROUP_NORMAL_COIN_ITEMCODE;
	rs.iNormalCoinCount = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_NORMAL_COIN);
	if(NULL != pItem)
	{
		rs.iNormalCoinCount = pItem->iPropertyTypeValue;
	}

	rs.iPremiumCoinItemCode = RANDOMITEM_GROUP_PREMIUM_COIN_ITEMCODE;
	rs.iPremiumCoinCount = 0;
	pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_PREMIUM_COIN);
	if(NULL != pItem)
	{
		rs.iPremiumCoinCount = pItem->iPropertyTypeValue;
	}

	RANDOMITEMGROUPEVENT.MakeShowRewardList(rs);

	rs.iGroupRepeatCount = MAX_EVENT_RANDOMITEM_CARD_TYPE_COUNT;

	CPacketComposer Packet(S2C_RANDOMITEM_GROUP_EVENT_LIST_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RANDOMITEM_GROUP_EVENT_LIST_RES));

	SRANDOMITEM_GROUP_REPEAT_INFO info;
	for(BYTE btGroupType = EVENT_RANDOMITEM_GROUP_TYPE_NORMAL_RED; btGroupType < MAX_EVENT_RANDOMITEM_GROUP_TYPE_COUNT; ++btGroupType)
	{
		info.btGroupType = btGroupType;
		info.iMaxRepeatCount = RANDOMITEMGROUPEVENT.GetMaxRepeatCount();
		info.iCurrentRepeatCount = m_iaDrawCount[btGroupType] - ((int)abs(m_iaDrawCount[btGroupType]/info.iMaxRepeatCount)*info.iMaxRepeatCount);
		Packet.Add((PBYTE)&info, sizeof(SRANDOMITEM_GROUP_REPEAT_INFO));
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserRandomItemGroupEvent::SendRewardList(BYTE btGroupType)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMGROUPEVENT.IsOpen());

	CPacketComposer Packet(S2C_RANDOMITEM_GROUP_EVENT_REWARD_LIST_RES);
	CHECK_CONDITION_RETURN_VOID(FALSE == RANDOMITEMGROUPEVENT.MakeRewardList(btGroupType, Packet));

	m_pUser->Send(&Packet);
};

void CFSGameUserRandomItemGroupEvent::SendExchangeProductList()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMGROUPEVENT.IsOpen());

	SS2C_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_LIST_RES rs;
	for(int i = 0; i < MAX_EVENT_RANDOMITEM_CARD_TYPE_COUNT; ++i)
	{
		rs.iCurrentCardCount[i] = m_iaCardCount[i];
	}

	CPacketComposer Packet(S2C_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_LIST_RES);
	RANDOMITEMGROUPEVENT.MakeExchangeProductList(rs, Packet);

	m_pUser->Send(&Packet);
}

RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN CFSGameUserRandomItemGroupEvent::UseCoinReq(BYTE btGroupType, SS2C_RANDOMITEM_GROUP_EVENT_USE_COIN_RES& rs)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMGROUPEVENT.IsOpen(), RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_FAIL);
	CHECK_CONDITION_RETURN(btGroupType >= MAX_EVENT_RANDOMITEM_GROUP_TYPE_COUNT, RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_FAIL);

	SUserHighFrequencyItem* pItem = NULL;
	if(btGroupType == EVENT_RANDOMITEM_GROUP_TYPE_NORMAL)
	{
		pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_NORMAL_COIN);
		CHECK_CONDITION_RETURN(NULL == pItem || pItem->iPropertyTypeValue <= 0, RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_ENOUGH_COIN);
	}
	else
	{
		pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_PREMIUM_COIN);
		CHECK_CONDITION_RETURN(NULL == pItem || pItem->iPropertyTypeValue <= 0, RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_ENOUGH_COIN);
	}

	int iRandomRewardIndex;
	CHECK_CONDITION_RETURN(0 == (iRandomRewardIndex = RANDOMITEMGROUPEVENT.GetRandomRewardIndex(btGroupType)), RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_FAIL);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRandomRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_FAIL);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_FULL_MAIL);

	BYTE btCardType = 0; 
	int iCardCount = 0;
	BOOL bBonusReward = FALSE; 
	bBonusReward = RANDOMITEMGROUPEVENT.GetBonusRewardItem(btGroupType, m_iaDrawCount[btGroupType], btCardType, iCardCount);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_FAIL);

	int iUpdatedCoinCount, iUpdatedDrawCount, iUpdatedCardCount;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_Group_UseCoin(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), btGroupType, pItem->iPropertyKind, iRandomRewardIndex, btCardType, iCardCount, iUpdatedCoinCount, iUpdatedDrawCount,iUpdatedCardCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMGROUP_EVENT, DB_DATA_LOAD, FAIL, "EVENT_RANDOMITEM_Group_UseCoin failed - UserIDIndex:10752790", m_pUser->GetUserIDIndex());
rn RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_FAIL;
	}

	pItem->iPropertyTypeValue = iUpdatedCoinCount;
	m_iaDrawCount[btGroupType] = iUpdatedDrawCount;

	if(TRUE == bBonusReward) 
		m_iaCardCount[btCardType] = iUpdatedCardCount;

	pReward = REWARDMANAGER.GiveReward(m_pUser, iRandomRewardIndex);
	if(NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMGROUP_EVENT, DB_DATA_LOAD, FAIL, "GiveReward failed - UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), iRandomRewardIndex);

RANDOMITEMGROUPEVENT.MakeRewardInfo(iRandomRewardIndex, rs.sReward);

	return RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_SUCCESS;
}

RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT CFSGameUserRandomItemGroupEvent::ExchangeProductReq(int iProductIndex, SS2C_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_RES& rs)
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMGROUPEVENT.IsOpen(), RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_FAIL);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_FAIL);

	BYTE btCardType;
	int iCardCount; 
	SRewardInfo sReward;
	CHECK_CONDITION_RETURN(FALSE == RANDOMITEMGROUPEVENT.GetExchangeProductInfo(iProductIndex, btCardType, iCardCount, sReward), RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_FAIL);
	CHECK_CONDITION_RETURN(btCardType >= MAX_EVENT_RANDOMITEM_CARD_TYPE_COUNT, RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_FAIL);
	CHECK_CONDITION_RETURN(m_iaCardCount[btCardType] < iCardCount, RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_ENOUGH_CARD);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(sReward.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_FAIL);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_FULL_MAIL);

	int iUpdatedCardCount;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_Group_UseCard(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), btCardType, iCardCount, sReward.iRewardIndex, iUpdatedCardCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMGROUP_EVENT, DB_DATA_LOAD, FAIL, "EVENT_RANDOMITEM_Group_UseCard failed - UserIDIndex:10752790", m_pUser->GetUserIDIndex());
rn RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_FAIL;
	}

	m_iaCardCount[btCardType] = iUpdatedCardCount;

	pReward = REWARDMANAGER.GiveReward(m_pUser, sReward.iRewardIndex);
	if(NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMGROUP_EVENT, DB_DATA_LOAD, FAIL, "GiveReward failed - UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), sReward.iRewardIndex);

memcpy(rs.iCurrentCardCount, m_iaCardCount, sizeof(int)*MAX_EVENT_RANDOMITEM_CARD_TYPE_COUNT);

	return RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_SUCCESS;
}