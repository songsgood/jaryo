qpragma once
qinclude "SvrProxy.h"

class CLogSvrProxy : public CSvrProxy
{
public:
	CLogSvrProxy(void);
	virtual ~CLogSvrProxy(void);

	virtual void	SendFirstPacket();
	virtual void	ProcessPacket(CReceivePacketBuffer* recvPacket);
};

