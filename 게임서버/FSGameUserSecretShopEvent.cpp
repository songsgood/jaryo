qinclude "stdafx.h"
qinclude "FSGameUserSecretShopEvent.h"
qinclude "SecretShopEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "CFSGameServer.h"
qinclude "UserHighFrequencyItem.h"
qinclude "CenterSvrProxy.h"

CFSGameUserSecretShopEvent::CFSGameUserSecretShopEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_tResetTime(-1)
	, m_bPlayGame(FALSE)
{
}

CFSGameUserSecretShopEvent::~CFSGameUserSecretShopEvent(void)
{
}

BOOL CFSGameUserSecretShopEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == SECRETSHOP.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<SUserSecretShopItem> vUserShopItem;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_SECRET_SHOP_GetUserItemList(m_pUser->GetUserIDIndex(), m_tResetTime, m_bPlayGame, vUserShopItem))
	{
		WRITE_LOG_NEW(LOG_TYPE_SECRET_SHOP, INITIALIZE_DATA, FAIL, "EVENT_SECRET_SHOP_GetUserItemList error");
		return FALSE;
	}

	for(int i = 0; i < vUserShopItem.size(); ++i)
	{
		CHECK_CONDITION_RETURN(MAX_EVENT_SECRET_SHOP_ITEM_SLOT <= vUserShopItem[i]._iSlotIndex, FALSE);

		SUserSecretShopItem sItem;
		m_mapUserItem[vUserShopItem[i]._iSlotIndex] = vUserShopItem[i];
	}

	CheckReset();
	UpdateSlotStatus();

	m_bDataLoaded = TRUE;

	return TRUE;
}

BOOL CFSGameUserSecretShopEvent::CheckReset(BOOL bPlayGame)
{
	CHECK_CONDITION_RETURN(-1 != m_tResetTime &&
		TRUE == TodayCheck(EVENT_RESET_HOUR, m_tResetTime, _GetCurrentDBDate), FALSE);

	time_t tCurrentDate = _GetCurrentDBDate;
	if(FALSE == bPlayGame)
	{
		m_bPlayGame = bPlayGame;
		m_mapUserItem.clear();

		return TRUE;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, FALSE);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_SECRET_SHOP_ResetUserItemList(m_pUser->GetUserIDIndex(), tCurrentDate, bPlayGame), FALSE);	

	m_tResetTime = tCurrentDate;
	m_bPlayGame = bPlayGame;

	m_mapUserItem.clear();

	return TRUE;
}

void CFSGameUserSecretShopEvent::MakeShopList()
{
	CheckReset();

	CHECK_CONDITION_RETURN_VOID(FALSE == m_bPlayGame);
	CHECK_CONDITION_RETURN_VOID(!m_mapUserItem.empty());

	vector<SUserSecretShopItem> vShopItem;
	SECRETSHOP.MakeShopList(vShopItem);

	CHECK_CONDITION_RETURN_VOID(vShopItem.size() != MAX_EVENT_SECRET_SHOP_ITEM_SLOT);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	vector<int> vRewardIndex;
	for(int i = 0; i < vShopItem.size(); ++i)
	{
		vRewardIndex.push_back(vShopItem[i]._iRewardIndex);
	}

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_SECRET_SHOP_MakeUserItemList(m_pUser->GetUserIDIndex(), vRewardIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_SECRET_SHOP, INITIALIZE_DATA, FAIL, "EVENT_SECRET_SHOP_MakeUserItemList error");
		return;
	}

	m_mapUserItem.clear();
	for(int i = 0; i < vShopItem.size(); ++i)
	{
		m_mapUserItem[vShopItem[i]._iSlotIndex] = vShopItem[i];
	}

	UpdateSlotStatus();
}

void CFSGameUserSecretShopEvent::UpdateSlotStatus()
{
	int iUserCashSlotCount = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SECRET_SHOP_SLOT);
	if(NULL != pItem)
		iUserCashSlotCount = pItem->iPropertyTypeValue;

	EVENT_USER_SECRET_SHOP_ITEM_MAP::iterator iter = m_mapUserItem.begin();
	for( ; iter != m_mapUserItem.end(); ++iter)
	{
		BYTE btSlotType = SECRETSHOP.GetSlotType(iter->second._iSlotIndex);
		if(EVENT_SECRET_SHOP_ITEM_SLOT_TYPE_CASH == btSlotType &&
			iter->second._btStatus != EVENT_SECRET_SHOP_ITEM_STATUS_BUY_COMPLETED)
		{
			if(0 == iUserCashSlotCount)
			{
				iter->second._btStatus = EVENT_SECRET_SHOP_ITEM_STATUS_LOCK;
			}
			else if(1 == iUserCashSlotCount)
			{
				if(EVENT_SECRET_SHOP_ITEM_SLOT_4 == iter->second._iSlotIndex)
					iter->second._btStatus = EVENT_SECRET_SHOP_ITEM_STATUS_NONE;
				else if(EVENT_SECRET_SHOP_ITEM_SLOT_5 == iter->second._iSlotIndex)
					iter->second._btStatus = EVENT_SECRET_SHOP_ITEM_STATUS_LOCK;
			}
			else if(2 == iUserCashSlotCount)
			{
				iter->second._btStatus = EVENT_SECRET_SHOP_ITEM_STATUS_NONE;
			}
		}
	}
}

void CFSGameUserSecretShopEvent::SendShopList()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == SECRETSHOP.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	MakeShopList();

	SS2C_EVENT_SECRET_SHOP_LIST_RES rs;
	rs.btShopStatus = EVENT_SECRET_SHOP_STATUS_CLOSE;
	if(TRUE == m_bPlayGame)
		rs.btShopStatus = EVENT_SECRET_SHOP_STATUS_OPEN;

	rs.iSlotPrice = SECRETSHOP.GetSlotPrice();
	
	rs.iUserCashSlotCount = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SECRET_SHOP_SLOT);
	if(NULL != pItem)
		rs.iUserCashSlotCount = pItem->iPropertyTypeValue;

	rs.iItemCount = m_mapUserItem.size();

	CPacketComposer Packet(S2C_EVENT_SECRET_SHOP_LIST_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_SECRET_SHOP_LIST_RES));

	int iSlotType = -1;
	EVENT_USER_SECRET_SHOP_ITEM_MAP::iterator iter = m_mapUserItem.begin();
	for( ; iter != m_mapUserItem.end(); ++iter)
	{
		SEVENT_SECRET_SHOP_ITEM info;
		info.btSlotType = SECRETSHOP.GetSlotType(iter->second._iSlotIndex);
		info.iSlotIndex = iter->second._iSlotIndex;
		SECRETSHOP.GetRewardInfo(iter->second._iRewardIndex, info.sReward, info.iPrice, iSlotType);
		info.btStatus = iter->second._btStatus;
		Packet.Add((PBYTE)&info, sizeof(SEVENT_SECRET_SHOP_ITEM));
	}
	
	m_pUser->Send(&Packet);
}

RESULT_EVENT_SECRET_SHOP_BUY_ITEM CFSGameUserSecretShopEvent::BuyItemReq(int iSlotIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == SECRETSHOP.IsOpen(), RESULT_EVENT_SECRET_SHOP_BUY_ITEM_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_SECRET_SHOP_BUY_ITEM_FAIL);
	CHECK_CONDITION_RETURN(FALSE == m_bPlayGame, RESULT_EVENT_SECRET_SHOP_BUY_ITEM_FAIL);

	CHECK_CONDITION_RETURN(TRUE == CheckReset(), RESULT_EVENT_SECRET_SHOP_BUY_ITEM_RESET_ITEM);

	EVENT_USER_SECRET_SHOP_ITEM_MAP::iterator iter = m_mapUserItem.find(iSlotIndex);
	CHECK_CONDITION_RETURN(EVENT_SECRET_SHOP_ITEM_STATUS_BUY_COMPLETED == iter->second._btStatus, RESULT_EVENT_SECRET_SHOP_BUY_ITEM_ALREADY_BUY_ITEM);
	CHECK_CONDITION_RETURN(EVENT_SECRET_SHOP_ITEM_STATUS_LOCK == iter->second._btStatus, RESULT_EVENT_SECRET_SHOP_BUY_ITEM_FAIL);
	
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_SECRET_SHOP_BUY_ITEM_FULL_MAILBOX);

	SREWARD_INFO sReward;
	int iPrice = 0;
	int iShoutType = -1;
	SECRETSHOP.GetRewardInfo(iter->second._iRewardIndex, sReward, iPrice, iShoutType);
	CHECK_CONDITION_RETURN(0 >= iPrice, RESULT_EVENT_SECRET_SHOP_BUY_ITEM_FAIL);
	CHECK_CONDITION_RETURN(iPrice > m_pUser->GetSkillPoint(), RESULT_EVENT_SECRET_SHOP_BUY_ITEM_ENOUGH_POINT);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_SECRET_SHOP_BUY_ITEM_FAIL);

	int iPresentIdx;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_SECRET_SHOP_BuyItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), m_pUser->GetCurUsedAvtarLv(), m_pUser->GetCurUsedAvatarPosition(), iSlotIndex, iter->second._iRewardIndex, iPrice, iPresentIdx), RESULT_EVENT_SECRET_SHOP_BUY_ITEM_FAIL);

	m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iPresentIdx);

	iter->second._btStatus = EVENT_SECRET_SHOP_ITEM_STATUS_BUY_COMPLETED;

	m_pUser->SetSkillPoint(m_pUser->GetSkillPoint()-iPrice);
	m_pUser->SendUserGold();

	if(iShoutType > -1)
	{
		CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
		if( NULL != pCenter )
		{
			SG2S_EVENT_SHOUT_REQ  info;
			info.iEventKind = EVENT_KIND_SECRET_SHOP;
			info.btContentsType = iShoutType;	// SECRETSHOP_BUYITEM_SHOUT_CONTENTS_TYPE
			strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), _countof(info.GameID)-1);
			memcpy(&info.sReward, &sReward, sizeof(SREWARD_INFO));
			pCenter->SendPacket(G2S_EVENT_SHOUT_REQ, &info, sizeof(SG2S_EVENT_SHOUT_REQ));
		}
	}

	return RESULT_EVENT_SECRET_SHOP_BUY_ITEM_SUCCESS;
}

void CFSGameUserSecretShopEvent::MakeEventOpen(CPacketComposer& Packet, int& iEventCount)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == SECRETSHOP.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CHECK_CONDITION_RETURN_VOID(FALSE == CheckReset(TRUE));
	
	iEventCount++;

	SEVENT_GAME_RESULT_COMPLETED_INFO info;
	info.iEventKind = EVENT_KIND_SECRET_SHOP;
	info.iValue = 0;
	Packet.Add((PBYTE)&info, sizeof(SEVENT_GAME_RESULT_COMPLETED_INFO));
}
