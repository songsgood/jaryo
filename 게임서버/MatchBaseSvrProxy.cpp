qinclude "stdafx.h"
qinclude "MatchBaseSvrProxy.h"

qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"

CMatchBaseSvrProxy::CMatchBaseSvrProxy(void)
{
}


CMatchBaseSvrProxy::~CMatchBaseSvrProxy(void)
{
}

void CMatchBaseSvrProxy::ProcessPacket(CReceivePacketBuffer* /*recvPacket*/)
{
	
}

void CMatchBaseSvrProxy::SendUserLogout(CFSGameUser* pUser)
{
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(G2M_USER_LOGOUT);
	SG2M_USER_LOGOUT info;

	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = CFSGameServer::GetInstance()->GetServerIndex();
	info.iRecordBoardStep = pUser->GetUserRecordBoard()->GetStepForDisconnectRecord();

	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_USER_LOGOUT));

	Send(&PacketComposer);
}
