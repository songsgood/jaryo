qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserSkyLuckyEvent
{
public:
	CFSGameUserSkyLuckyEvent(CFSGameUser* pUser);
	~CFSGameUserSkyLuckyEvent(void);

	BOOL			Load();
	void			CheckResetDate(time_t tCurrentTime = _time64(NULL));
	void			SendEventInfo();
	void			SendUserEventData();
	BOOL			BuySkyLuckyTicket_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	void			GiveSkyLuckyReward();
	BYTE			GetCurrentStepDay()		{ return m_btCurrentStepDay; }
	BYTE			GetBuyTicketTypeFlag()	{ return m_iBuyTicketTypeFlag; }
	BOOL			IsEventAllClear()		{ return (SKYLUCKY_STEP_DAY_COUNT <= m_btCurrentStepDay && SKYLUCKY_STEP_DAY_COUNT <= m_btCanGetRewardMinStepDay); }

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;
	int				m_iBuyTicketTypeFlag;
	BYTE			m_btCurrentStepDay;
	BYTE			m_btCanGetRewardMinStepDay;
	BYTE			m_btCurrentStepStatus;
	time_t			m_tRecentCheckDate;
};

