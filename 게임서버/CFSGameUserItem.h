// CFSGameUserItem.h: interface for the CFSGameUserItem class.
//
// 유저 아이템 관리
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CFSGAMEUSERITEM_H__089955F7_E912_4745_A0D2_4EA0CE8D79ED__INCLUDED_)
#define AFX_CFSGAMEUSERITEM_H__089955F7_E912_4745_A0D2_4EA0CE8D79ED__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "FSItemCommon.h"
qinclude "ThreadODBCManager.h"

class CFSGameUser;

class CFSItemShop;
class CAvatarItemList;
class CFSGameUserItem;

struct SBillingInfo;

enum ITEM_PAGE_MODE
{
	ITEM_PAGE_MODE_NONE,
	ITEM_PAGE_MODE_SHOP,
	ITEM_PAGE_MODE_MYITEM,
	ITEM_PAGE_MODE_CLUBSHOP,
};

class CFSGameUserItem  
{
public:
	CFSGameUserItem();
	virtual ~CFSGameUserItem();

	//////////////////////////// Item ///////////////////////////////////////////////
	//아이템 리스트를 로딩
	void	GetItemListPage(int iInventorytKind, int iPage, int iBigKind, int iSmallKind, SUserItemInfo *aItemList, int &iItemNum);
	BOOL	UseItem(CFSItemShop *pItemShop , int iItemIdx, int *iaRemoveList, int & iErrorCode, int iPropertyIdx1, int iPropertyIdx2  );
	BOOL	UseTryItem(int iItemCode , CFSItemShop *pItemShop, int & iErrorCode );
	BOOL	UndressItem(int iItemCode , int iChannel, CFSItemShop *pItemShop, int & iErrorCode);
	BOOL	UseTryUniform(int iItemCode , CFSItemShop *pItemShop, int & iErrorCode);

	BOOL	RemoveItemWithItemIdx( int iItemIdx );
	BOOL	DeleteExhaustedItem(SUserItemInfo* pItem , int iSlot);
	BOOL	ExpireItem(CFSGameODBC *pODBC, int iItemIdx, int iUpdateItemStatus = ITEM_STATUS_EXPIRED);	//20050930 time based item
	BOOL	DeleteExpiredItem(CFSODBCBase *pODBC, int iItemIdx);
	BOOL	DeleteItem(CFSODBCBase *pODBC, SUserItemInfo* pItem);

	BOOL	UseSecondBoostItem(int& iItemCode);
	BOOL	RemoveItemWithStatus( SAvatarInfo* pAvatar , int iSlot , SUserItemInfo * pItem , SFeatureInfo &FeaureInfo, int iChannel);
	BOOL	RemoveItemWith( int iSlot , SShopItemInfo & ItemInfo  ,SFeatureInfo &FeatureInfo);
	BOOL	RemoveChangeDressItemWith(int iSlot, int iChannel, SFeatureInfo &FeatureInfo);
	BOOL	RemoveItem(SAvatarInfo *pAvatar, int iChannel ,SFeatureInfo &FeatureInfo, CFSItemShop *pItemShop, BOOL &bRemove );
	BOOL	RemoveChangeDressItem(SAvatarInfo *pAvatar, int iChannel, SFeatureInfo &FeatureInfo, CFSItemShop *pItemShop, BOOL &bRemove);

	// 20090807 채널링 수정 추가 함수
	BOOL	CheckPayAbility( SShopItemInfo& ShopItemInfo, int iOperationCode, int iLoopCount, int iBuyPriceCash, int iTotalCash,
									   int iBuyPricePoint, int iTotalPoint, int iBuyPriceTrophy, int iTotalTrophy, int iBuyPriceClubCoin, int *res, int &iErrorCode );
	BOOL	BuyLvUpItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	BuyPremiumItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	ArrangeItemInfoToAddBuyItem( CAvatarItemList *pAvatarItemList, CFSGameUser* pUser, CFSItemShop* pItemShop, SShopItemInfo &ShopItemInfo,
												  SUserItemInfo &UserItemInfo, int iCntSupplyItem, int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT],
												  SUserItemInfo itemList[MAX_ITEMS_IN_PACKAGE], vector<SUserItemInfo>& vUserTempItemInfo );
	void	AddItemToUserItemList( CAvatarItemList *pAvatarItemList, int iCntSupplyItem, SUserItemInfo &UserItemInfo, vector<SUserItemInfo>& vUserTempItemInfo);
	BOOL	BuyItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	BuySecretStore_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	BuyCharacterSlot_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	BuyFaceOff_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	BuyChangeBodyShape_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	void	SetUserBillResultAtMem( CFSGameUser* pUser, CFSGameODBC* pGameODBC, int iSellType, int iPostMoney, int iItemPricePoint, int iItemPriceTrophy, int iBonusCash);
	BOOL	BuySkillSlot_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	void	GetAdditionalSkillSlot( SAvatarInfo *pAvatar, SExtraSkillSlot &SkillSlot, int iItemIndex, int iPropertyValue, int iSlotIdx );
	BOOL	SetPropertyPriceAndType( CFSItemShop *pItemShop, SShopItemInfo &ShopItemInfo, int iPropertyValue, int iPropertyAssignType, int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT], int iRecvUserLevel, 
											int &iBuyPriceCash, int &iBuyPricePoint, int &iItemPropertyPriceCash, int &iItemPropertyPricePoint, int &iErrorCode );
	BOOL SetItemPriceBySellType(CFSItemShop *pItemShop, SShopItemInfo &ShopItemInfo,BOOL bIsLocalPublisher, int iPropertyValue, int &iBuyPriceCash, int &iBuyPricePoint, int &iBuyPriceTrophy, int& iBuyPriceClubCoin, int &iErrorCode );
	BOOL SetOneUnitItemPrice(CFSGameODBC* pGameODBC, CFSItemShop *pItemShop,BOOL bIsLocalPublisher , SShopItemInfo &ShopItemInfo, int iPropertyValue,int iItemPriceCash,int iItemPricePoint, int &iBuyOnePriceCash, int &iBuyOnePricePoint, int &iErrorCode );
	BOOL	BuyItemProcess_BeforePay( CFSGameODBC *pFSODBC, SAvatarInfo* pAvatar, CAvatarItemList *pAvatarItemList, CFSItemShop *pItemShop, 
											   int iItemCode, BOOL bIsSendItem, int iRecvUserGameIDIndex, SShopItemInfo &ShopItemInfo, 
											   std::string &strItemName, int &iErrorCode, int* res, int &iContrastVar_Lv, int iPropertyValue);
	BOOL	CheckItemBuyConditionWithPropertyKind( CFSGameODBC *pFSODBC, SShopItemInfo &ShopItemInfo, SAvatarInfo* pAvatar, CAvatarItemList *pAvatarItemList, BOOL bIsSendItem, int iContrastVar_SpecialCharacterIndex, int iContrastVar_IsHeight, int* res, int &iErrorCode, int iPropertyValue );
	BOOL BuyItem( BOOL IsLocalPublisher, CFSItemShop *pItemShop, int iItemCode, int iLoopCount,
		int iPropertyValue, int iPropertyAssignType, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT], int iUseCouponPropertyKind, int& iTotalCash, int& iTotalPoint, int& iTotalTrophy,
		int & iErrorCode, int iOperationCode = -1, BOOL bIsUseDressItemToken = FALSE, int iRenewItemIdx = -1, char* recvID = NULL, char* title = NULL, char* text = NULL, int *res = NULL, int* index = NULL);
	int		BuyFactionShopItem(int iItemCode);
	BOOL	SellItem( CFSItemShop *pItemShop, int iItemIdx, int &iRemove, int & iErrorCode );
	BOOL	BuyItemProcess_CheckHacking(CFSItemShop* pItemShop, CFSLogODBC* pLogODBC, CFSLoginGlobalODBC* pLoginODBC, SShopItemInfo ShopItemInfo,int iRecvUserLevel, int iPropertyValue, int iPropertyAssignType, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT]);	// 20100119 해킹 체크 함수 추가
	BOOL	BuyItemProcess_CheckPropertyIndexNumber(CFSItemShop* pItemShop, int iPropertyKind,int iRecvUserLevel, int iPropertyAssignType, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] , int& iPropertyNumber );
	BOOL	BuyItemProcess_CheckPropertyIndex(CFSItemShop* pItemShop,int iPropertyKind,int iRecvUserLevel,int iPropertyAssignType,int iPropertyNumber, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT]);
	BOOL	BuyItemProcess_CheckItemPropertyValue(CFSItemShop *pItemShop, SShopItemInfo& ShopItemInfo, int iPropertyValue);
	BOOL	WishUniform( CFSClubBaseODBC *pODBC, int iItemCode, int &iSuccess );
	BOOL	BuyUniform( CFSGameODBC *pODBC, CFSItemShop *pItemShop, int iItemCode, int iParam0 , int iParam1 , int iParam2 , int iParam3 , int & iErrorCode , int & iEventPoint);
	void	CheckBasicItemNeed(SFeatureInfo &FeaureInfo, BOOL bChangeStatus = TRUE);
	void	ChangeBasicItem_Sex(int iSex);
	void	RemoveBasicItem(SFeatureInfo &FeatureInfo);
	void	CheckOriginalItemNeed(SFeatureInfo &FeatureInfo);
	void    CheckOriginalChangeDressItemNeed(SFeatureInfo &FeatureInfo);
	BOOL	RemoveItemWithRemoveList(CFSODBCBase *pODBC, SAvatarInfo *pAvatar, int iChannel, int iPropertyKind, SFeatureInfo &FeatureInfo, int *iaRemoveList, BOOL &bRemove);
	BOOL	BuyChangeDressItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	BuyPackageItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );

	BOOL	BackUpUseFeatureInfo();
	void	BackUpUseTryFeatureInfo();
	void	RestoreFeatureInfo();
	void	RestoreUseFeatureInfo();
	void	FeatureInfoInitilaize();		//스타일데이터를 기반으로 의상정보를 초기화 시킴
	BOOL	ConfirmItemChange(CFSGameODBC *pFSODBC);

	BOOL	GetAvatarInfoInItemPage(SAvatarInfo & AvatarInfo);
	
	void	GetTempItemFeature(SFeatureInfo& FeatureInfo);
	void	GetTempChangeDressItemFeature(SFeatureInfo& FeatureInfo);
	void	GetItemFeatureEventPreview(SFeatureInfo& FeatureInfo);
	char*	GetItemPageAvatarID();

	int		GetSkillTicketCount();
	BOOL	RemoveSkillTicket(int iTicketInventoryIndex);

	void	SetGameUser(CFSGameUser * pUser) { m_pUser = pUser; };
	CFSGameUser* GetGameUser() { return m_pUser; };
	void	SetMode(int iMode ) { m_iMode = iMode; };
	int		GetMode(){ return m_iMode; };

	BOOL	SendItemPresent(CFSGameODBC *pODBC, CFSItemShop *pItemShop, int iItemCode, char* SendUserID , char* szMsg);
	BOOL	IsInMyInventory( int iItemCode );
	BOOL	IsInMyInventoryAll( int iItemCode );
	BOOL	IsInMyInventoryWithPropertyKind( int iPropertyKind );

	BOOL	InsertNewItem(SUserItemInfo &UserItemInfo);

	void	SetCurrentUseItemMode(int iMode){  m_iUseItemMode = iMode; };

	//////////////////// CFSAvatarManager 에 있던것이 옮겨옴 
	BOOL				LoadAvatarItemList( CFSGameODBC * pGameODBC , SAvatarInfo* pAvatar );
	CAvatarItemList*	GetCurAvatarItemList();

	void GetTempFace(int& iFace);
	void SetTempFace(int iFace);
	void GetTempCharacterType(int& iCharacterType);
	void SetTempCharacterType(int iCharacterType);
	int GetSellPrice( int iBuyPriceCash, int iBuyPricePoint );

	int		CheckShoutItem( CFSGameODBC *pODBC , int iShoutItemMode);
	BOOL	ReadyShoutItem(int iShoutItemMode);
	int		GetShoutItemCount(int iShoutItemMode);	

	void	GetEquipItemList( vector< SUserItemInfo* >& vUserItemInfo );
	BOOL	InsertUserItemProperty( int iGameIDIndex, int iItemIdx, CAvatarItemList *pAvatarItemList, vector< SItemProperty > vItemProperty );
	BOOL	ApplyBagItem(SAvatarInfo* pAvatar, short *naItemIdx, BYTE byItemNum, int iApplyAccItemIdx[MAX_USER_APPLY_ACCITEM_PROPERTY], int *naPCRoomItemPropertyIdx1, int *naPCRoomItemPropertyIdx2, map<int/*iSpecialPartsIndex*/, vector<int/*iProperty*/>> mapProperty);
	int		GetMyInventoryItemCount();

	BOOL	UpdateFuncItemStatus(CFSGameODBC *pFSODBC, int iItemIdx, int Status, int* iaRemoveItemIdx = NULL);
	bool	InsertPremiumItem( CAvatarItemList *pAvatarItemList, SShopItemInfo &ShopItemInfo , SUserItemInfo &UserItemInfo, int iPropertyIndex1, int iPropertyIndex2, int iPropertyIndex3);

	int		GetPauseItemCount();
	int		DiscountPauseItem();
	int		CheckAndGetPropertyType(int iPropertyType, int iPropertyValue);
	BOOL	ChangeItemLockStatus( int iItemIdx, int iItemCode, int& iRet );
// 20090520 CHN Challenge letter function task - check have and use.
	BOOL	CheckChallengeLetterItem( );
	void	ExhaustChallengeLetter( CFSGameODBC *pODBC );
	BOOL	CheckHaveBindAccountItem(int iPropertyKind, int& iErrorCode);
	BOOL	BuyBinAccountItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	BOOL	BuyResetRecordItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult );
	int		GetRankTypeByPropertyKind(int iPropertyKind);
	BOOL	CheckSeasonResetItem_CloseTime( int iStartTime, int iEndTime );

	// 2011.02.28 jhwoo
	BOOL	CheckSpecialTeamIndexCondition(const int iAvatarsSpecialTeamIndex, const int iSpecialTeamIndexCondition, const int iUnableSpecialTeamIndexCondition);
	BOOL	CheckSpecialAvatarIndexCondition(const int iAvatarsSpecialAvatarIndex, const int iSpecialAvatarIndexCondition);
	void	ProcessAchievementBuyItem(CFSGameODBC *pFSODBC, CFSGameUser* pUser, CAvatarItemList *pAvatarItemList);
	int		GetItemCountWithKind(int iItemBigKind, int iItemSmallKind);

	BOOL	ArrangeItemInfoToAddItem( CFSItemShop* pItemShop, int iCntSupplyItem, SUserItemInfoEx itemList[MAX_ITEMS_IN_PACKAGE] );
	BYTE	GetUserPropertyAccItemStatus(int iItemIdx, int iBigKind, int iStatus, int iPropertyKind);
	BYTE	GetDifferentUserPropertyAccItemStatus();
	BOOL	UpdateBuffItemStatus(CFSGameODBC *pFSODBC, int iItemIdx, int Status, int* iaRemoveItemIdx = NULL);
	void	CheckAndSendChangeDressItem(CPacketComposer& PacketComposer);
	BOOL	ResigterChangeDressItem(CFSItemShop* pItemShop, BYTE btIdxCount, BYTE* btChangeDressItemType, int* iaChangeItemCodeList, CPacketComposer& PacketComposer);
	void	GetVoiceUserSettingInfo( vector<SVoiceUserSettingInfo*>& vVoiceUserSettingInfo, int iItemIndex = -1, int iVoiceActionCode = VOICE_ACTION_CODE_DEFAULT );
	void	SetVoiceInfo( int iItemIndex, int iVoiceCode, int iVoiceIndex, int iVoiceActionCode );
	BOOL	DeleteVoiceSettingInfo( int iItemIndex );

	void	RemoveClubItem();

	void	ChangeUseTryItem(int iStyleIndex);

	bool	CheckGetBasicFullCrtUniform();
	void	InsertBasicFullCrtUniform();
	void	RemovePCRoomItem(int iSex = ITEM_SEXCONDITION_UNISEX);
	BOOL	CheckUseItem(int iItemCode);
	void	UpdatePCRoomItemStat(int iLv);
	bool	CheckUserMemoryItem();
	void	SetOriginalItem();
	bool	UseOrigianlItem(int iType, SUserItemInfo * pItem);

	BOOL BuyRandomCard_AfterPay( SBillingInfo* pBillingInfo, int iPayResult);
	BOOL BuyGuaranteeCard_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL ChangeCloneCharacterItem(int iSex);

	int GetRandomCardTicketCount(ITEM_SMALL_KIND eSmallKind, int iItemCode);
	int GetRandomCardTicketItemIndex(ITEM_SMALL_KIND eSmallKind, int iItemCode);

	BOOL	BuyCheerLeader_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);

protected:

	int		FindSpaceSlot(int *iaTempFeature);
	int		FindChangeDressSpaceSlot(int *iaFeature);
	int		FindItemSlot(int *iaFeature,int iItemCode);
	int		FindChangeDressItemSlot(int iaFeature[], int iItemCode);

private:
	CFSGameUser*	m_pUser;

	int		m_iMode;	//ITEM_PAGE_MODE

	SFeatureInfo m_UseItem;		//착용하게 되는 아이템
	SFeatureInfo m_UseTryItem;	//샵에서 미리보기 착용시 사용
	SFeatureInfo m_OriginalItem;	//메모리아이템(PC방 제공 아이템)을 사용하기 전의 원래 모습

	int		m_iUseItemMode;   //// 착용하기 할때 옷가방슬롯모드

	CAvatarItemList*	m_paItemList;	//// 아이템리스트
	vector<SVoiceUserSettingInfo*> m_VoiceUserSettingInfo;
};

#endif // !defined(AFX_CFSGAMEUSERITEM_H__089955F7_E912_4745_A0D2_4EA0CE8D79ED__INCLUDED_)
