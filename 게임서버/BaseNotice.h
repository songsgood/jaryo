// BaseMailList.h: interface for the CBaseMailList class.
// 
// [CBaseNotice]
// 최근 정보가 front에 유지되는 list
// index값을 primary 키로 인식
// 기본적으로 SBaseNotice 구조체를 원소로 사용
//
//////////////////////////////////////////////////////////////////////

#if !defined(_FS_BASE_NOTICE_H_)
#define _FS_BASE_NOTICE_H_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

// 클럽 정보의 한 페이지당 출력 수
const int	MAX_CLUBINFO_LIST_CNT	= 15;

struct SBaseNotice
{
	// primary 키 값
	int		iIndex;	
	SDate	date;

	SBaseNotice() { iIndex = 0; }
};

struct SNotice : public SBaseNotice
{
	char	Text[MAX_CLUB_NOTICE_LENGTH+1];
};

struct SLineNotice : public SBaseNotice
{
	char	GameID[MAX_GAMEID_LENGTH+1];	// Channeling
	char	Text[MAX_CLUB_LINENOTICE_LENGTH+1];
};

struct SPRNotice : public SBaseNotice
{
	char	clubName[MAX_CLUB_NAME_LENGTH+1];
	char	Text[MAX_CLUB_PRNOTICE_LENGTH+1];
	int		emblemNum;
};

template <class BASE_TYPE>
class CBaseNotice  
{
typedef std::list<BASE_TYPE*>	NOTICE_LIST;

public:
	CBaseNotice(int maxNotice = 0)	{  m_maxNoticeCnt = maxNotice; };	

	// 리스트에 BASE_TYPE을 front에 추가
	// 같은 index의 BASE_TYPE을 제거하고 return 하거나
	// 리스트가 꽉 찼을 경우 맨 뒤의 BASE_TYPE을 제거하고 return
	BASE_TYPE*		Add(BASE_TYPE* base);
	// index 키 값의 BASE_TYPE을 리스트에서 제거하고 return
	BASE_TYPE*		Remove(int index);
	// 최근 값을 제거하고 return
	BASE_TYPE*		RemoveFirst();
	// 리스트의 현제 size return
	int				GetSize() { return m_listNotice.size(); }
	// 최근 값을 return
	BASE_TYPE*		FindFirst(typename NOTICE_LIST::iterator &i);
	// i의 다음 값을 return
	BASE_TYPE*		FindNext(typename NOTICE_LIST::iterator &i);
	// i의 전 값을 return
	BASE_TYPE*		FindPrev(typename NOTICE_LIST::iterator &i);
		
private:
	// BASE_TYPE의 리스트
	NOTICE_LIST			m_listNotice;
	// 리스트 크기(0은 무제한)
	int					m_maxNoticeCnt;
};

template <class BASE_TYPE>
BASE_TYPE* CBaseNotice<BASE_TYPE>::Add(BASE_TYPE* base)
{
	if(!base) return NULL;
	
	BASE_TYPE* temp = Remove(base->iIndex);
	m_listNotice.push_front(base);

	if(NULL == temp && m_maxNoticeCnt > 0 && 
		m_listNotice.size() > m_maxNoticeCnt) 	
	{
		temp = m_listNotice.back();
		m_listNotice.pop_back();
	}

	return temp;
}

template <class BASE_TYPE>
BASE_TYPE* CBaseNotice<BASE_TYPE>::Remove(int index)
{
	//if(index > GetSize()-1) return NULL;

	NOTICE_LIST::iterator i;

	BASE_TYPE* base;
	for(i = m_listNotice.begin(); i != m_listNotice.end() ; i++)
	{
		base = (*i);
		if(base->iIndex == index)
		{
			m_listNotice.erase(i);

			return base;
		}
	}

	return NULL;
}

template <class BASE_TYPE>
BASE_TYPE* CBaseNotice<BASE_TYPE>::RemoveFirst()
{
	if(0 == GetSize()) return NULL;

	NOTICE_LIST::iterator i = m_listNotice.begin();
	if(i == m_listNotice.end()) return NULL;

	BASE_TYPE* base = *i;
	m_listNotice.erase(i);

	return base;
}

template <class BASE_TYPE>
BASE_TYPE* CBaseNotice<BASE_TYPE>::FindFirst(typename NOTICE_LIST::iterator &i)
{
	if(0 == GetSize()) return NULL;

	i = m_listNotice.begin();
	if(i == m_listNotice.end()) return NULL;

	return (*i);
}

template <class BASE_TYPE>
BASE_TYPE* CBaseNotice<BASE_TYPE>::FindNext(typename NOTICE_LIST::iterator &i)
{
	if(0 == GetSize()) return NULL;

	if(i == m_listNotice.end() || (++i) == m_listNotice.end()) return NULL;
	
	return (*i);	
}

template <class BASE_TYPE>
BASE_TYPE* CBaseNotice<BASE_TYPE>::FindPrev(typename NOTICE_LIST::iterator &i)
{
	if(0 == GetSize()) return NULL;

	if(i == m_listNotice.begin() || (i--) == m_listNotice.begin()) return NULL;
	
	return (*i);	
}
// End

#endif // !defined(_FS_BASE_NOTICE_H_)


