// CFSGameUser.cpp: implementation of the CFSGameUser class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CFSGameUser.h"
qinclude "CFSSkillShop.h"
qinclude "CFSGameDBCfg.h"
qinclude "CFSGameServer.h"
qinclude "CFSRankManager.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSGameODBC.h"
qinclude "CFSGameClient.h"
qinclude "ClubSvrProxy.h"
qinclude "CenterSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "FriendList.h"
qinclude "FSGameCommon.h"
qinclude "CAvatarCreateManager.h"
qinclude "RewardCardManager.h"
qinclude "ClubConfigManager.h"
qinclude "HackingManager.h"
qinclude "CFSClubBaseODBC.h"
qinclude "LobbyChatUserManager.h"
qinclude "CReceivePacketBuffer.h"
qinclude "Rc4.h"
qinclude "RewardManager.h"
qinclude "BingoManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "CoachCardShop.h"
qinclude "ActionInfluenceList.h"
qinclude "CUserAction.h"
qinclude "CActionShop.h"
qinclude "Item.h"
qinclude "SecretStoreManager.h"
qinclude "MatchAdvantageManager.h"
qinclude "FactionManager.h"
qinclude "GameProxyManager.h"
qinclude "LuckyBoxManager.h"
qinclude "WordPuzzlesManager.h"
qinclude "Product.h"
qinclude "SkillUpgrade.h"
qinclude "RandomBoxManager.h"
qinclude "PuzzlesManager.h"
qinclude "ThreeKingdomsEventManager.h"
qinclude "MissionShopManager.h"
qinclude "HoneyWalletEventManager.h"
qinclude "CustomizeManager.h"
qinclude "HotGirlTimeManager.h"
qinclude "HotGirlMissionEventManager.h"
qinclude "RandomCardShopManager.h"
qinclude "TodayHotDeal.h"
qinclude "LobbyClubUserManager.h"
qinclude "CheerLeaderEventManager.h"
qinclude "PayBackManager.h"
qinclude "GoldenCrushManager.h"
qinclude "MatchingCardManager.h"
qinclude "AttendanceItemShop.h"
qinclude "LvUpEventManager.h"
qinclude "SonOfChoiceEventManager.h"
qinclude "RandomItemBoardEventManager.h"
qinclude "EventDateManager.h"
qinclude "RandomItemTreeEventManger.h"
qinclude "UserExpBoxItemEvent.h"
qinclude "RandomItemGroupEventManger.h"
qinclude "SaleRandomItemEventManager.h"
qinclude "ChatSvrProxy.h"
qinclude "LeagueSvrProxy.h"
qinclude "ShoppingFestivalEventManager.h"
qinclude "EventHalfPrice.h"
qinclude "ODBCSvrProxy.h"
qinclude "GameOfDiceEventManager.h"
qinclude "RandomItemDrawEventManager.h"
qinclude "CrazyLevelUpEventManager.h"
qinclude "SalePlusEventManager.h"
qinclude "DarkMarketEventManager.h"
qinclude "MiniGameZoneEventManager.h"
qinclude "HippocampusEventManager.h"
qinclude "PVEManager.h"
qinclude "PromiseManager.h"
qinclude "SpeechBubbleManager.h"
qinclude "TransferJoycity100DreamEventManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CFSGameDBCfg*	g_pFSDBCfg;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFSGameUser::CFSGameUser() 
: m_paperList(MAX_PAPER_LIST)
, m_presentList(MAX_PRESENT_LIST,TRUE)
, m_presentListOnAccount(MAX_PRESENT_LIST,TRUE)
, m_bIsAvailablePointRewardCard(FALSE)
, m_ClubInvitationList( MAX_CLUB_INVITION_COUNT )
, m_bLobbyButtonSave(false)
, m_iStateBuy(-1)
, m_iMatchLocation(-1)
, m_pLobbyChatChannel(NULL)
, m_pLobbyChatFactionChannel(NULL)
, m_UserBingo(this)
, m_UserRecordBoard(this)
, m_UserIntensivePractice(this)
, m_UserLuckyBox(this)
, m_UserRandomBox(this)
, m_UserThreeKingdomsEvent(this)
, m_UserSpecialSkin(this)
, m_UserBasketBall(this)
, m_UserHoneyWalletEvent(this)
, m_UserCharacterCollection(this)
, m_UserHotGirlSpecialBoxEvent(this)
, m_UserSkyLuckyEvent(this)
, m_UserCheerLeader(this)
, m_UserGoldenSafeEvent(this)
, m_UserShoppingEvent(this)
, m_UserLegendEvent(this)
, m_UserRandomArrowEvent(this)
, m_UserDevilTemtationEvent(this)
, m_UserVendingMachineEvent(this)
, m_UserMatchingCard(this)
, m_UserRandomItemBoardEvent(this)
, m_UserGamePlayEvent(this)
, m_UserRandomItemTreeEvent(this)
, m_UserLoginPayBackEvent(this)
, m_UserPurchaseGrade(this)
, m_UserRandomItemGroupEvent(this)
, m_UserRainbowWeekEvent(this)
, m_UserGameOfDiceEvent(this)
, m_UserRankMatch(this)
, m_UserPasswordEvent(this)
, m_UserHalloweenGamePlayEvent(this)
, m_UserLottotEvent(this)
, m_UserPresentFromDeveloperEvent(this)
, m_UserDiscountItemEvent(this)
, m_UserMiniGameZoneEvent(this)
, m_UserBigWheelLoginEvent(this)
, m_UserSalePlusEvent(this)
, m_UserDriveMissionEvent(this)
, m_UserMissionMakeItemEvent(this)
, m_UserMissionCashLimitEvent(this)
, m_UserDarkMarketEvent(this)
, m_UserColoringPlayEvent(this)
, m_UserCongratulationEvent(this)
, m_UserHelloNewYearEvent(this)
, m_UserOpenBoxEvent(this)
, m_UserNewbieBenefit(this)
, m_UserFriendInvite(this)
, m_UserPrimonGoEvent(this)
, m_UserPrivateRoomEvent(this)
, m_UserHippocampusEvent(this)
, m_UserComebackBenefit(this)
, m_UserPowerupCapsule(this)
, m_UserUnlimitedTatooEvent(this)
, m_UserPVEMission(this)
, m_UserCovet(this)
, m_UserShoppingGodEvent(this)
, m_UserRandomItemDrawEvent(this)
, m_UserSubCharDevelop(this)
, m_UserSelectClothesEvent(this)
, m_UserSummerCandyEvent(this)
, m_UserSecretShopEvent(this)
, m_UserTransferJoycityPackageEvent(this)
, m_UserTransferJoycityShopEvent(this)
, m_UserTransferJoycity100DreamEvent(this)
{
	m_bWCGPositionSelect		= FALSE;
	m_bMySex					= FALSE;
	m_bAnotherSex				= FALSE;
	m_bHackUser					= FALSE;
	m_iCheckStatPacketNo		= 0;
	m_iCheckStatCheckSumPacketNo = 0;
	m_iCheckStatLastSendPacketNo = 0;
	m_bGMUser					= FALSE;
	m_TestVar0					= 0;
	m_iGameIDIndex				= -1;
	m_iUserType					= 0;
	m_iGuageValue				= 0;
	m_iItemTypeLevelUpCount		= 0;
	m_bUseableBonusCoin			= FALSE;
	m_iMaxAvatarLevel			= 0;

	memset( m_szUserID , 0 , sizeof(char) * (MAX_USERID_LENGTH+1) );

	SetLocation( U_NONE );

	m_UserItem = new CFSGameUserItem();
	m_UserItem->SetGameUser(this);
	
	m_UserSkill = new CFSGameUserSkill();
	m_UserSkill->SetGameUser(this);
	
	SetCheatAble(FALSE);

	m_MemPointer = 0;

	m_MaxLostNum = 0;
	m_iAIComeLost = 0;
	m_bAIComePass = FALSE;
	m_bReceiveAllFriend = FALSE;
	m_bEnterChannelToSendFriend = FALSE;

	m_btStatCheckSeed = 0;

	m_AvatarStyleInfo.Initialize();

	m_bExpirePremiumPCRoom = FALSE;
	m_sGuaranteeCard.Initialize();

	m_iUserSubPageState = USER_SUB_PAGE_STATE_NONE;
	m_btRoundGroupIndex = 0;
	m_SecretStoreInfo.Initialize();

	m_bIsLastMatchDisconnected = FALSE;
	m_bLeftSeatStatus = FALSE;
	m_listCustomReceiveditem.clear();
	m_bCloneCharacter = FALSE;
	m_bChangeofCharacter = FALSE;
	m_iChangePCRoomDeffCount = 0;
	m_bIsMatching = FALSE;
	m_iJumpingPlayerGameIDIndex = -1;
	m_tReciveMagicballTime = -1;
	m_tLastPlayTime = -1;
	m_tLastCheckHeartTatooTime = -1;
	m_iHeartTatooDailyPlayCount = 0;
	m_fLoginRating = 0.f;
	m_bReadyAICome = FALSE;
	m_ReadyAIComePopupStatus = READY_AICOME_POPUP_STATUS_SEND;
	m_iRevengeMatchingWinCnt = -1;
	m_btRoomRevengeMatchingType = RevengeMatching::REVENGE_MATCHING_TYPE_NONE;
	memset(m_btLobbyButtonIndex, 255, sizeof(BYTE)*eMENUBUTTON_TYPE_MAX);
	m_bBuyKyoungLeePackge = false;
	m_bClubUserGiveEventCoin = FALSE;

	memset(&m_szCheckSum_Rc4Key, 0x00, sizeof(m_szCheckSum_Rc4Key));
	memset(&m_sz_AnyOperation_Rc4Key, 0x00, sizeof(m_sz_AnyOperation_Rc4Key));
	memset(&m_szRc4Key, 0x00, sizeof(m_szRc4Key));
	ZeroMemory(m_iIsHave_Speechbubble, sizeof(int)*MAX_STAT_TYPE_COUNT);
}

CFSGameUser::~CFSGameUser()
{
	WRITE_LOG_NEW(LOG_TYPE_USER, USER_LOGOUT, NONE, "~CFSGameUser() Address(00B51316) User Address(00000000)", m_pBaseClient, this);
!= m_UserItem )
	{
		delete m_UserItem;
		m_UserItem = NULL;
		TRACE("DELETE\n");
	}
	
	if( NULL != m_UserSkill )
	{
		delete m_UserSkill;
		m_UserSkill = NULL;
		TRACE("DELETE\n");
	}

	vector<SSpecialPartsUserProperty*>::iterator itr; 
	vector<SSpecialPartsUserProperty*>::iterator itrEnd = m_vecSpecialPartsUserProperty.end();
	for(itr = m_vecSpecialPartsUserProperty.begin(); itr != itrEnd; ++itr)
	{
		SAFE_DELETE(*itr);
	}
	m_vecSpecialPartsUserProperty.clear();
}

BOOL CFSGameUser::CheckCheatAble()
{
	return m_bCheatAble;
}

void CFSGameUser::SetCheatAble(BOOL bAble)
{
	m_bCheatAble = bAble;
}

BOOL CFSGameUser::EnableBroadcastFreeRoomInfo(int iMatchSvrID, int iPage, int iRoomIdx , int iOp)
{
	if( GetLocation() != U_LOBBY ) return FALSE;
	CHECK_CONDITION_RETURN(m_RoomListUserSet._iMatchSvrID != iMatchSvrID, FALSE);

	if (ROOM_LIST_WAITING == m_RoomListUserSet._btListMode) 
	{
		int iGetStartIdx = m_RoomListUserSet._iaShowTeamID[0];
		
		for (int i = 0; i < DEFAULT_TEAMNUM_PER_PAGE; ++i)
		{
			if( m_RoomListUserSet._iaShowTeamID[i] == iRoomIdx )
			{
				return TRUE;
			}
		}

		if( OP1_CREATE == iOp && iGetStartIdx < iRoomIdx && DEFAULT_TEAMNUM_PER_PAGE > m_RoomListUserSet._iSendCount)
		{
			++m_RoomListUserSet._iSendCount;	
			return TRUE;
		}
	}
	else if(ROOM_LIST_WHOLE == m_RoomListUserSet._btListMode)
	{
		if( m_RoomListUserSet._iPage == iPage )
		{
			return TRUE;
		}
	}
	
	return FALSE;
}


BOOL CFSGameUser::EnableBroadcastTeamInfo(int iMatchSvrID, BYTE btMatchTeamScale, int iPage, int iTeamIdx , int iClubSerialNum , int iOp)
{
	CHECK_CONDITION_RETURN(GetLocation() != U_LOBBY, FALSE);
	CHECK_CONDITION_RETURN(m_RoomListUserSet._iMatchSvrID != iMatchSvrID, FALSE);
	CHECK_CONDITION_RETURN(m_RoomListUserSet._btMatchTeamScale != btMatchTeamScale, FALSE);

	const int iTeamNumForPage = GET_TEAMNUM_PER_PAGE(btMatchTeamScale);
	
	if (ROOM_LIST_WAITING == m_RoomListUserSet._btListMode) 
	{
		int iGetStartIdx = m_RoomListUserSet._iaShowTeamID[0];
		
		for (int i = 0; i < iTeamNumForPage; ++i)
		{
			if( m_RoomListUserSet._iaShowTeamID[i] == iTeamIdx )
			{
				return TRUE;
			}
		}

		if( OP1_CREATE == iOp && iGetStartIdx < iTeamIdx && iTeamNumForPage > m_RoomListUserSet._iSendCount)
		{
			++m_RoomListUserSet._iSendCount;	
			return TRUE;
		}
	}
	else if(ROOM_LIST_WHOLE == m_RoomListUserSet._btListMode)
	{
		if( m_RoomListUserSet._iPage == iPage )
		{
			return TRUE;
		}
	}
	
	return FALSE;
}

BOOL CFSGameUser::EnableBroadcastTeamInfoInTeam(int iPage, int iTeamIdx , int iOp)
{
	if (NULL !=GetClient() && GetClient()->GetState() != FS_TEAM) 
		return FALSE;

	if (ROOM_LIST_CHALLENGING_TEAM == m_RoomListUserSet._btListMode)
	{
		int iGetStartIdx = m_RoomListUserSet._iaShowTeamID[0];
		int iGetEndIdx = m_RoomListUserSet._iaShowTeamID[DEFAULT_TEAMNUM_PER_PAGE-1];

		if( iGetStartIdx <= iTeamIdx && iTeamIdx <= iGetEndIdx )
 		{
 			return TRUE;
 		}
		
		if( iOp == OP1_CREATE && iTeamIdx > iGetStartIdx && CHALLENGE_TEAM_PER_PAGE > m_RoomListUserSet._iSendCount)
		{
			++m_RoomListUserSet._iSendCount;
			return TRUE;
		}
	}
	
	return FALSE;
}

void CFSGameUser::SetUserLoginInfo(SFSLoginInfo& LoginInfo)
{
	m_iUserIDIndex = LoginInfo.iUserIDIndex;
	m_iSkillPoint = LoginInfo.iSkillPoint;
	m_iSeason = LoginInfo.iSeason;
	m_iAdvantageUserType = LoginInfo.iAdvantageUserType;

	m_tAccountCreateTime = TimeStructToTimet(LoginInfo.CreatedTime); 
	m_tAccounetLastLoginTime = TimeStructToTimet(LoginInfo.LastLoginTime);
}

void CFSGameUser::GetSeasonInfo( int & iYear , int & iMonth )
{
	iYear = m_iSeason/100;
	iMonth = m_iSeason;
0;
}

BOOL CFSGameUser::LoadAvatarList(LPCTSTR szGameID, int iEqualizeLV)
{
	CFSGameODBC* pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pFSODBC );

	CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC( ODBC_RANK );
	CHECK_NULL_POINTER_BOOL( pRankODBC );

	if( FALSE == m_AvatarManager.LoadAvatarInfoWithGameID(pFSODBC, szGameID ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_AVATAR, DB_DATA_LOAD, FAIL, "LoadAvatarInfoWithGameID - GameID:�4", szGameID);
	return FALSE;
	}

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);

	if (FALSE == CFSGameServer::GetInstance()->IsEnableAvatarFame())
	{
		pAvatarInfo->iFameLevel = 0;
	}

	SetEqualizeAvatarStatus(pFSODBC,GetGameIDIndex(), iEqualizeLV);
	if( FALSE == m_AvatarManager.LoadRankInfo(pRankODBC, GetCurUsedAvtarLvGrade(), &m_AvatarSeasonInfo) )
	{
		WRITE_LOG_NEW(LOG_TYPE_RANK, DB_DATA_LOAD, FAIL, "LoadRankInfo - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}

	if( FALSE == m_AvatarManager.LoadRecordInfo(pRankODBC, GetCurUsedAvtarLvGrade(), &m_AvatarSeasonInfo) )
	{
		WRITE_LOG_NEW(LOG_TYPE_RECORD, DB_DATA_LOAD, FAIL, "LoadRecordInfo - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}

	if( FALSE == m_UserPurchaseGrade.Load() )	// �κ��丮 �������� ���� �ε��ؾ���. (���ī��)
	{
		WRITE_LOG_NEW(LOG_TYPE_PURCHASE_GRADE, DB_DATA_LOAD, FAIL, "m_UserPurchaseGrade - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserItem->LoadAvatarItemList(pFSODBC , pAvatarInfo ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "LoadAvatarItemList - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}

	//GetUserItemList()->BackUpUseFeatureInfo();	//LoadAvatarInfoWithGameID, LoadAvatarItemList �ڿ� ȣ��

	if( FALSE == LoadProductInventoryList())
	{
		WRITE_LOG_NEW(LOG_TYPE_COACHCARD, DB_DATA_LOAD, FAIL, "LoadProductInventoryList - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}

	if( FALSE == LoadItemSlotDataList())
	{
		WRITE_LOG_NEW(LOG_TYPE_COACHCARD, DB_DATA_LOAD, FAIL, "LoadItemSlotDataList - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}

	vector<SDELETED_SKILL_INFO> vDeleteSkill;
	if( FALSE == m_AvatarManager.LoadAvatarSkill(pFSODBC,m_iUserIDIndex, CFSGameServer::GetInstance()->GetSkillShop(), &vDeleteSkill) ) 
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, DB_DATA_LOAD, FAIL, "LoadAvatarSkill - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}

	// ���뽺ų ��ȿ���˻� ���н� ��罺ų�� �������� ��Ŵ
	if(FALSE == pAvatarInfo->Skill.Convert())
	{
		int iSuccessCode = -1;
		GetUserSkill()->InsertToInventoryAllSkill(iSuccessCode);
		GetUserSkill()->InsertToInventoryAllFreestyle( iSuccessCode);
	}

	if (FALSE == LoadStyleInfo())	//LoadAvatarItemList �ڿ� ȣ��Ǿ���
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "LoadStyleInfo - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}
	
	if(pAvatarInfo->iSpecialCharacterIndex == 700 ||
		pAvatarInfo->iSpecialCharacterIndex == 701 ||
		pAvatarInfo->iSpecialCharacterIndex == 702 ||
		pAvatarInfo->iSpecialCharacterIndex == 703)
	{
		if (ODBC_RETURN_SUCCESS != pFSODBC->TRANSFORM_GetUserTransfromAvatarInfo(pAvatarInfo->iGameIDIndex, pAvatarInfo))
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "TRANSFORM_GetUserTransfromAvatarInfo - GameIDIndex:11866902", pAvatarInfo->iGameIDIndex);
urn FALSE;
		}
	}

	LoadBindaccountItemList();

	if (ODBC_RETURN_SUCCESS != pFSODBC->SKILL_GetAvatarSpecialtySkill(CFSGameServer::GetInstance()->GetSkillShop(), GetGameIDIndex(), pAvatarInfo->Status.iGamePosition, m_UserSkill))
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, DB_DATA_LOAD, FAIL, "SKILL_GetAvatarSpecialtySkill");
		return FALSE;
	}

	if(FALSE == LoadAction(GetGameIDIndex(), (CFSODBCBase*)pFSODBC))
	{
		WRITE_LOG_NEW(LOG_TYPE_ACTION, DB_DATA_LOAD, FAIL, "LoadAction - GameID:11866902", pAvatarInfo->iGameIDIndex);
rn FALSE;
	}

	LoadUserHighFrequencyItem((CFSODBCBase*)pFSODBC);

	if (FALSE == m_UserLuckyBox.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_LUCKYBOX, DB_DATA_LOAD, FAIL, "LoadLuckyBox - GameID:�4", szGameID);
	return FALSE;
	}

	if (FALSE == m_UserWordPuzzle.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_WORDPUZZLES, DB_DATA_LOAD, FAIL, "LoadWordPuzzle - GameID:�4", szGameID);
	return FALSE;
	}

	if (FALSE == m_UserRandomBox.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMBOX, DB_DATA_LOAD, FAIL, "LoadRandomBox - GameID:�4", szGameID);
	return FALSE;
	}

	if (FALSE == m_UserPuzzle.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_PUZZLES, DB_DATA_LOAD, FAIL, "LoadPuzzle - GameID:�4", szGameID);
	return FALSE;
	}

	if (FALSE == m_UserThreeKingdomsEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_LOAD, FAIL, "LoadThreeKingdomsEvent - GameID:�4", szGameID);
	return FALSE;
	}

	if (FALSE == m_UserMissionShopEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_MISSIONSHOP, DB_DATA_LOAD, FAIL, "LoadMissionShopEvent - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserSpecialSkin.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, DB_DATA_LOAD, FAIL, "LoadSpecialSkin - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserBasketBall.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_BASKETBALL, DB_DATA_LOAD, FAIL, "LoadBasketBall - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserHoneyWalletEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, DB_DATA_LOAD, FAIL, "LoadHoneyWalletEvent - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserCharacterCollection.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_CHARACTERCOLLECTION, DB_DATA_LOAD, FAIL, "LoadCharacterCollection - GameID:�4", szGameID);
	return FALSE;
	}
	
	if( FALSE == m_UserHotGirlGiftEvent.Load(SERVER_TYPE_NORMAL))
	{
		WRITE_LOG_NEW(LOG_TYPE_HOTGIRLGIFT, DB_DATA_LOAD, FAIL, "LoadHotGirlGiftEvent - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserHotGirlSpecialBoxEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_HOTGIRLSPECIALBOX, DB_DATA_LOAD, FAIL, "LoadHotGirlSpecialBoxEvent - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserHotGirlMissionEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENTHOTGIRLMISSION, DB_DATA_LOAD, FAIL, "LoadHotGirlMissionEvent - GameID:�4", szGameID);
	return FALSE;
	}
	m_UserHotGirlMissionEvent.ClearMissionStartCheckTIme();

	if( FALSE == m_UserSkyLuckyEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, DB_DATA_LOAD, FAIL, "LoadSkyLuckyEvent - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserCheerLeader.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, DB_DATA_LOAD, FAIL, "LoadCheerLeader - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == CheckAndInitPayBackEvent() )
	{
		WRITE_LOG_NEW(LOG_TYPE_PAYBACK, DB_DATA_LOAD, FAIL, "CheckAndInitPayBackEvent - UserIDIndex : 11866902 " , m_iUserIDIndex );
rn FALSE;
	}

	if( FALSE == CheckAndInitGoldenCrushEvent() )
	{
		WRITE_LOG_NEW(LOG_TYPE_GOLDENCRUSH, DB_DATA_LOAD, FAIL, "CheckAndInitGoldenCrushEvent - UserIDIndex:11866902, GameIDIndex:0 " , m_iUserIDIndex, GetGameIDIndex() );
urn FALSE;
	}

	if( FALSE == m_UserMatchingCard.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, DB_DATA_LOAD, FAIL, "MatchingCardEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}
		
	if( FALSE == m_UserGoldenSafeEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_GOLDENSAFE, DB_DATA_LOAD, FAIL, "LoadGoldenSafeEvent - GameID:�4", szGameID);
	return FALSE;
	}

	if( FALSE == m_UserShoppingEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGEVENT, DB_DATA_LOAD, FAIL, "LoadUserShoppingEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserLegendEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, DB_DATA_LOAD, FAIL, "LoadUserLegendEvent - UserIDIndex:11866902", m_iUserIDIndex);
rn FALSE;
	}

	if( FALSE == m_UserRandomArrowEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMARROW_EVENT, DB_DATA_LOAD, FAIL, "LoadUserRandomArrowEvent - UserIDIndex:11866902", m_iUserIDIndex);
rn FALSE;
	}

	if( FALSE == m_UserDevilTemtationEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "LoadUserDevilTemtationEvent - UserIDIndex:11866902", m_iUserIDIndex);
rn FALSE;
	}

	if( FALSE == m_UserVendingMachineEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_VENDINGMACHINEEVENT, DB_DATA_LOAD, FAIL, "LoadUserVendingMachineEvent - UserIDIndex:11866902", m_iUserIDIndex);
rn FALSE;
	}

	if( FALSE == CheckAndInitAttendanceItemShopEvent() )
	{
		WRITE_LOG_NEW(LOG_TYPE_ATTENDANCEITEMSHOP, DB_DATA_LOAD, FAIL, "CheckAndInitAttendanceItemShopEvent()");
		return FALSE;
	}

	if( FALSE == m_UserRandomItemBoardEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMBOARD_EVENT, DB_DATA_LOAD, FAIL, "RandomItemBoardEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserGamePlayEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEPLAYEVENT, DB_DATA_LOAD, FAIL, "GamePlayEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserAppraisal.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_USER_APPRAISAL, DB_DATA_LOAD, FAIL, "UserAppraisal - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserMissionEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_MISSIONEVENT, DB_DATA_LOAD, FAIL, "UserMissionEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserMissionBingoEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_MISSIONBINGOEVENT, DB_DATA_LOAD, FAIL, "UserMissionBingoEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserChoiceMissionEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_USERCHOICE_MISSIONEVENT, DB_DATA_LOAD, FAIL, "UserChoiceMissionEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}
	
	if( FALSE == m_UserRandomItemTreeEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMTREE_EVENT, DB_DATA_LOAD, FAIL, "UserRandomItemTreeEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserLoginPayBackEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_LOGINPAYBACK_EVENT, DB_DATA_LOAD, FAIL, "UserLoginPayBackEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserSpecialAvatarPieceEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIAL_AVATAR_PIECE_EVENT, DB_DATA_LOAD, FAIL, "UserSpecialAvatarPieceEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserReceiptEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_RECEIPT_EVENT, DB_DATA_LOAD, FAIL, "UserReceiptEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserLovePaybackEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_LOVEPAYBACK_EVENT, DB_DATA_LOAD, FAIL, "UserLovePaybackEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserFirstCashEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_FIRSTCASH_EVENT, DB_DATA_LOAD, FAIL, "UserFirstCashEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserExpBoxItemEvent.Load(SERVER_TYPE_NORMAL))
	{
		WRITE_LOG_NEW(LOG_TYPE_EXPBOXITEM_EVENT, DB_DATA_LOAD, FAIL, "UserExpBoxItemEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserRandomItemGroupEvent.Load() )
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMGROUP_EVENT, DB_DATA_LOAD, FAIL, "UserRandomItemGroupEventt - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserSaleRandomItemEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SALE_RANDOMITEM_EVENT, DB_DATA_LOAD, FAIL, "m_UserSaleRandomItemEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == LoadFirstCashBackLog())
	{
		WRITE_LOG_NEW(LOG_TYPE_FIRST_CASHBACK_EVENT, DB_DATA_LOAD, FAIL, "LoadFirstCashBackLog - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserRainbowWeekEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_RAINBOW_WEEK_EVENT, DB_DATA_LOAD, FAIL, "LoadRainbowWeekEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserGameOfDiceEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEOFDICE, DB_DATA_LOAD, FAIL, "m_UserGameOfDiceEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserWelcomeUserEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_WELCOME_USER_EVENT, DB_DATA_LOAD, FAIL, "m_UserWelcomeUserEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserRankMatch.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "m_UserRankMatch - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	// todo pcroom old code
	if(FALSE == CFSGameServer::GetInstance()->GetNewPCRoom())
	{
		if( FALSE == m_UserPCRoomEvent.Load())
		{
			WRITE_LOG_NEW(LOG_TYPE_PCROOM_EVENT, DB_DATA_LOAD, FAIL, "m_UserPCRoomEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
turn FALSE;
		}
	}

	if( FALSE == m_UserCashItemRewardEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_CASHITEM_REWARD_EVENT, DB_DATA_LOAD, FAIL, "m_UserCashItemRewardEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserPasswordEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SET_PASSWORD_EVENT, DB_DATA_LOAD, FAIL, "m_UserPasswordEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserHalloweenGamePlayEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_HALLOWEENGAMEPLAYEVENT, DB_DATA_LOAD, FAIL, "m_UserHalloweenGamePlayEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserShoppingFestivalEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGFESTIVAL, DB_DATA_LOAD, FAIL, "m_UserShoppingFestivalEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserLottotEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_LOTTOEVENT, DB_DATA_LOAD, FAIL, "m_UserLottotEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserPresentFromDeveloperEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_DEVELOPEREVENT, DB_DATA_LOAD, FAIL, "m_UserPresentFromDeveloperEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserDiscountItemEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_DISCOUNTITEM_EVENT, DB_DATA_LOAD, FAIL, "m_UserDiscountItemEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserMiniGameZoneEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAMEZONE_EVENT, DB_DATA_LOAD, FAIL, "m_UserMiniGameZoneEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserBigWheelLoginEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_BIGWHEELLOGIN_EVENT, DB_DATA_LOAD, FAIL, "m_UserBigWheelLoginEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserSalePlusEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserSalePlusEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}	

	if( FALSE == m_UserDriveMissionEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_DRIVEMISSION_EVENT, DB_DATA_LOAD, FAIL, "m_UserDriveMissionEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserMissionMakeItemEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserMissionMakeItemEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserMissionCashLimitEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserMissionCashLimitEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserDarkMarketEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserDarkMarketEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserColoringPlayEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserColoringPlayEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserCongratulationEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserCongratulationEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserHelloNewYearEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserHelloNewYearEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserOpenBoxEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_OPENBOX_EVENT, DB_DATA_LOAD, FAIL, "m_UserOpenBoxEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserNewbieBenefit.LoadNewbieBenefit() )
	{
		WRITE_LOG_NEW(LOG_TYPE_NEWBIEBENEFIT, DB_DATA_LOAD, FAIL, "m_UserNewbieBenefit - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserFriendInvite.LoadFriendInviteMyInfo(TRUE))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "LoadFriendInviteMyInfo - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserGamePlayLoginEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEPLAY_LOGIN_EVENT, DB_DATA_LOAD, FAIL, "m_UserGamePlayLoginEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if ( FALSE == m_UserPotionMakingEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_POTION_MAKING_EVENT, DB_DATA_LOAD, FAIL, "m_UserPotionMakingEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if ( FALSE == m_UserPrimonGoEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_PRIMONGO_EVENT, DB_DATA_LOAD, FAIL, "m_UserPrimonGoEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if ( FALSE == m_UserPrivateRoomEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_PRIVATEROOM_EVENT, DB_DATA_LOAD, FAIL, "m_UserPrivateRoomEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if ( FALSE == m_UserHippocampusEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_HIPPOCAMPUS_EVENT, DB_DATA_LOAD, FAIL, "m_UserHippocampusEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if ( FALSE == m_UserSteelBagMissionEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_STEELBAG_MISSION_EVENT, DB_DATA_LOAD, FAIL, "m_UserSteelBagMissionEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserComebackBenefit.LoadComebackBenefit())
	{
		WRITE_LOG_NEW(LOG_TYPE_COMEBACKBENEFIT, DB_DATA_LOAD, FAIL, "m_UserComebackBenefit - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserPowerupCapsule.LoadPowerupCapsule())
	{
		WRITE_LOG_NEW(LOG_TYPE_POWERUP_CAPSULE, DB_DATA_LOAD, FAIL, "m_UserPowerupCapsule - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserUnlimitedTatooEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_UNLIMITED_TATOO_EVENT, DB_DATA_LOAD, FAIL, "m_UserUnlimitedTatooEvent - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if( FALSE == m_UserPVEMission.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_PVE, DB_DATA_LOAD, FAIL, "m_UserPVEMission - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserCovet.LoadCovet())
	{
		WRITE_LOG_NEW(LOG_TYPE_COVET_EVENT, DB_DATA_LOAD, FAIL, "m_UserCovet.LoadCovet() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}
	
	if(FALSE == LoadPromiseEvent())
	{
		WRITE_LOG_NEW(LOG_TYPE_PROMISE, DB_DATA_LOAD, FAIL, "LoadPromiseEvent() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserMagicMissionEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_MAGIC_MISSION, DB_DATA_LOAD, FAIL, "m_UserMagicMissionEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserShoppingGodEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGGOD_EVENT, DB_DATA_LOAD, FAIL, "m_UserShoppingGodEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserRandomItemDrawEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "m_UserRandomItemDrawEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserSelectClothesEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, DB_DATA_LOAD, FAIL, "m_UserSelectClothesEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserSubCharDevelop.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SUBCHAR_DEVELOP, DB_DATA_LOAD, FAIL, "m_UserSubCharDevelop.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserSpecialPiece.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALPIECE, DB_DATA_LOAD, FAIL, "m_UserSpecialPiece.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserSummerCandyEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SUMMERCANDY_EVENT, DB_DATA_LOAD, FAIL, "m_UserSummerCandyEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserPremiumPassEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_PREMIUM_PASS_EVENT, DB_DATA_LOAD, FAIL, "m_UserPremiumPassEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserSecretShopEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_SECRET_SHOP, DB_DATA_LOAD, FAIL, "m_UserSecretShopEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == LoadEventSpeechBubble())
	{
		WRITE_LOG_NEW(LOG_TYPE_SPEECH_BUBBLE, DB_DATA_LOAD, FAIL, "LoadEventSpeechBubble() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserTransferJoycityPackageEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITYPACKAGE, DB_DATA_LOAD, FAIL, "m_UserTransferJoycityPackageEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserTransferJoycityShopEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITYSHOP, DB_DATA_LOAD, FAIL, "m_UserTransferJoycityShopEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	if(FALSE == m_UserTransferJoycity100DreamEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITY100DREAM, DB_DATA_LOAD, FAIL, "m_UserTransferJoycity100DreamEvent.Load() - UserIDIndex:11866902, GameIDIndex:0", m_iUserIDIndex, GetGameIDIndex());
urn FALSE;
	}

	LoadLastPlayTime();
	LoadLastCheckHeartTatooTime();
	LoadLoseLeadUserTime();

	CPacketComposer PacketComposer(S2C_SEASON_MONTH_INFO_RES);
	PacketComposer.Add((short)MAX_NORMAL_SEASON_INDEX_NUM);
	PacketComposer.Add((short)MAX_TOURNAMENT_SEASON_INDEX_NUM);
	PacketComposer.Add((short)MAX_SEASON_INDEX_NUM);

	int i, iSeasonIndex = GetCurrentSeasonIndex();
	for(i=0 ; i< MAX_SEASON_INDEX_NUM; i++)
	{
		PacketComposer.Add(iSeasonIndex);
		if(FALSE == DecreaseSeasonIndex(iSeasonIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_SYSTEM, EXEC_FUNCTION, FAIL, "DecreaseSeasonIndex");
			iSeasonIndex = GetCurrentSeasonIndex();
		}
	}
	
	Send(&PacketComposer);
	
	SetGameOption(pAvatarInfo->iGameOpt);

	GetUserBingo()->Load();

	m_UserRecordBoard.Initialize(GetGameIDIndex(),  pRankODBC);

	GetUserIntensivePractice()->Load();

	pFSODBC->MATCH_AVATAR_GetAdvantageRecord(pAvatarInfo->iGameIDIndex, m_AvatarAdvantageRecord);

	if( TRUE == GetUserItemList()->IsInMyInventoryWithPropertyKind(ITEM_PROPERTY_KIND_CUSTOMIZE_ITEM))
	{
		pFSODBC->CUSTOMIZE_GetUserLogAll(GetUserIDIndex(), GetGameIDIndex(), m_listCustomReceiveditem);
	}

	int iTargetSpecialIndex = 0;
	m_bCloneCharacter = FALSE;
	if( TRUE == AVATARCREATEMANAGER.CheckCloneAvailableCharacter(pAvatarInfo->iSpecialCharacterIndex, iTargetSpecialIndex) )
	{
		if( ODBC_RETURN_SUCCESS == pFSODBC->CLONE_CHARACTER_GetUserCloneCharacterInfo( m_iGameIDIndex, iTargetSpecialIndex, m_sCloneCharacterInfo[Clone_Index_Clone]) ) 
		{
			m_sCloneCharacterInfo[Clone_Index_Clone].SetTrainingStat( pAvatarInfo->Skill.iaTraining ); // 1
			AVATARCREATEMANAGER.UpdateCloneCharacterStat( &m_sCloneCharacterInfo[Clone_Index_Clone], &pAvatarInfo->Status ); 

			m_sCloneCharacterInfo[Clone_Index_Mine].Status.Reset();
			ZeroMemory(m_sCloneCharacterInfo[Clone_Index_Mine].iaAddStat, sizeof(int)*MAX_STAT_TYPE_COUNT);

			m_sCloneCharacterInfo[Clone_Index_Mine].SetStyleIndex( // mine ���� ó�� �ѹ��� ��Ÿ���� �����Ѵ�.
				m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Mine],m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Clone]);

			m_sCloneCharacterInfo[Clone_Index_Mine].SetCloneDate(pAvatarInfo->iSpecialCharacterIndex, pAvatarInfo->iSex, pAvatarInfo->iCharacterType, 
				pAvatarInfo->iFace, pAvatarInfo->Status);

			m_sCloneCharacterInfo[Clone_Index_Clone].Status.iGamePosition = pAvatarInfo->Status.iGamePosition;
			m_sCloneCharacterInfo[Clone_Index_Clone] = m_sCloneCharacterInfo[Clone_Index_Clone];
			
			m_bCloneCharacter = TRUE; // ���� Ŭ��ĳ���͸� �������ִ�.

			int iUseStyle = GetUseStyleIndex();
			SetUseStyleIndex(iUseStyle);

			CAvatarCreateManager* pManager = &AVATARCREATEMANAGER;
			if (NULL != pManager)
			{
				Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > iUseStyle ? Clone_Index_Mine : Clone_Index_Clone;

				SAvatarInfo* pAvatarInfo = ChangeCloneAvatar(eIndex);
				CHECK_CONDITION_RETURN(NULL == pAvatarInfo, FALSE);

				pManager->UpdateMultipleCharacterInfo(GetCurUsedAvatar(), m_AvatarStyleInfo.iUseStyle, iUseStyle);
			}

			SetCloneCharacterDefaultSeremony(iUseStyle);
			if( CLONE_CHARACTER_DEFULAT_STYLE_INDEX <= iUseStyle ) // ������ Ŭ��ĳ���ʹ�.
			{
				pAvatarInfo->SetAvatarCloneData(m_sCloneCharacterInfo[Clone_Index_Clone]);
				GetUserItemList()->ChangeBasicItem_Sex(pAvatarInfo->iSex);
				ChangeCloneAvatarDiffSexItem(m_sCloneCharacterInfo[Clone_Index_Clone].iSex);
				ReLoadFeatureInfo(FALSE);
				GetUserItemList()->BackUpUseFeatureInfo();
			}
			else
			{
				ChangeCloneAvatarDiffSexItem(m_sCloneCharacterInfo[Clone_Index_Mine].iSex);
			}
		}
	}
	
	// todo pcroom old code
	if(FALSE == CFSGameServer::GetInstance()->GetNewPCRoom())
	{
		if( PCROOM_KIND_PREMIUM == GetPCRoomKind())
		{
			SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
			if( pAvatarInfo )
			{
				int SkillSlotCount = pAvatarInfo->GetTotalSkillSlotCount( 0 );
				if( SkillSlotCount < MAX_SKILL_SLOT ) 
				{
					pAvatarInfo->Skill.iPCBangBonusSlot = PREMIUM_PCROOM_SKILL_SLOT_BONUS_KOREA;
				}
			}
		
			LoadPCRoomItemList();
		}
	}
	///////////////////////////////
	m_bFirstChangePCRoomItem = FALSE;
	m_bCheckPotenCardExpiredSend = false;

	HotDealItemGetLimitCount();	
	m_UserPingInfo.init();

	CheckAndGiveGameOfDiceEventRankReward();

	if(OPEN == GAMEOFDICEEVENT.IsOpen())
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			SS2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT not;
			not.btServerIndex = _GetServerIndex;
			GetUserGameOfDiceEvent()->MakeIntegrateDB_UpdateUserLog(not);

			CPacketComposer	Packet(S2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT);
			Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT));

			pODBCSvr->Send(&Packet);
		}
	}

	if(OPEN == HIPPOCAMPUS.IsOpen())
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT not;
			not.btServerIndex = _GetServerIndex;
			GetUserHippocampusEvent()->MakeIntegrateDB_UpdateUserLog(not);

			CPacketComposer	Packet(S2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT);
			Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT));

			pODBCSvr->Send(&Packet);
		}
	}

	LoadEventNewClubRewardLog();
	if(FALSE == LoadClubUserRankRewardLog())
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, EXEC_FUNCTION, FAIL, "LoadClubUserRankRewardLog");
		return FALSE;
	}

	if(vDeleteSkill.size() > 0)
	{
		SS2C_SKILL_FREESTYLE_DELETE_NOT	ss;
		ss.iDeletedCnt = vDeleteSkill.size();
		CPacketComposer Packet(S2C_SKILL_FREESTYLE_DELETE_NOT);
		Packet.Add((PBYTE)&ss, sizeof(SS2C_SKILL_FREESTYLE_DELETE_NOT));

		for(int i = 0; i < ss.iDeletedCnt; ++i)
		{
			Packet.Add((PBYTE)&vDeleteSkill[i], sizeof(SDELETED_SKILL_INFO));
		}
		Send(&Packet);
	}

	LoadEventGiveMagicBallStatus();

	return TRUE;
}

BOOL CFSGameUser::SetCurUsedAvatar(LPCTSTR strGameID)
{
	int iIdxCurUsedAvatar = m_AvatarManager.FindAvatarIdx(strGameID);
	if( -1 != iIdxCurUsedAvatar )
	{
		m_AvatarManager.SetCurUsedAvatar(iIdxCurUsedAvatar);
		return TRUE;
	}
	return FALSE;
}

SAvatarInfo * CFSGameUser::GetCurUsedAvatar()
{
	SAvatarInfo * pAvatarInfo = NULL;

	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	
	return pAvatarInfo;
}

int CFSGameUser::GetCurUsedAvtarFameLevel()
{
	int iFameLevel = 0;
	SAvatarInfo * pAvatarInfo = NULL;

	pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
	if( NULL != pAvatarInfo )
	{
		iFameLevel = pAvatarInfo->iFameLevel;
	}

	return iFameLevel;
}

BOOL CFSGameUser::CurUsedAvatarSnapShot(SAvatarInfo & AvatarInfo)
{
	memset(&AvatarInfo, 0, sizeof(SAvatarInfo));
	
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if(pAvatarInfo)
	{
		memcpy(&AvatarInfo, pAvatarInfo, sizeof(SAvatarInfo));
		return TRUE;
	}
	if ( TRUE == CFSGameServer::GetInstance()->IsEqualizeChannel() )
	{
		if (NULL != m_AvatarManager.GetEqualizeStatus())
		{
			memcpy(&AvatarInfo.Status, m_AvatarManager.GetEqualizeStatus(), sizeof(SAvatarStatus));
		}
	}
	return FALSE;
}

void CFSGameUser::InitStartGame()
{
	m_iCheckStatPacketNo = 0;
	m_bHackUser = FALSE;
}

BOOL CFSGameUser::GetAvatarInfo( SAvatarInfo & AvatarInfo , int iSelectIndex)
{
	if( 0 == GetAvatarNum() )		return FALSE;
	
	BOOL bRet = FALSE;
	
	
	memset(&AvatarInfo, 0, sizeof(SAvatarInfo));
	
	if( 0 == iSelectIndex )
	{
		SAvatarInfo *pAvatar = m_AvatarManager.GetAvatarWithIdx( m_AvatarManager.GetCurUsedAvatarIdx() );
		if( NULL == pAvatar ) return FALSE;
		
		m_AvatarManager.SetCurUsedAvatar(m_AvatarManager.GetCurUsedAvatarIdx());		
		
		memcpy( &AvatarInfo, pAvatar , sizeof(SAvatarInfo));
		
		bRet = TRUE;
	}
	
	return bRet;
}

SAvatarInfo*  CFSGameUser::GetAvatarInfoWithIdx( int iSelectIndex )
{
	SAvatarInfo *pAvatar = m_AvatarManager.GetAvatarWithIdx( iSelectIndex );
	
	return pAvatar;
}


SAvatarInfo* CFSGameUser::GetAvatarInfoWithGameID(char *szGameID)
{
	SAvatarInfo *pAvatar = NULL;
	
	int iIdxAvatar = m_AvatarManager.FindAvatarIdx(szGameID);
	if( -1 != iIdxAvatar )
	{
		pAvatar =  m_AvatarManager.GetAvatarWithIdx(iIdxAvatar);
	}
	
	
	return pAvatar;
}

BOOL CFSGameUser::SetNewPosition(int iPos)
{
	CFSGameODBC* pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL(pFSODBC);
	
	int iStartIdx = 0, iElementNum = 0;
	
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);
	
	if(TRUE == CFSGameServer::IsWCGServer())
	{
		pAvatarInfo->Status.iGamePosition = iPos;
		
		m_UserSkill->LoadSkillForWCG();
	}
	else
	{
		if( pAvatarInfo->iLv < GAME_POSITION_UP ||  pAvatarInfo->Status.iGamePosition > 2 || pAvatarInfo->Status.iGamePosition == 0 ) 
			return FALSE;
		
		if( iPos != pAvatarInfo->Status.iGamePosition * 2 +1  && iPos != (pAvatarInfo->Status.iGamePosition * 2 ) + 2 ) return FALSE;
		
		int iaStatAdd[MAX_STAT_TYPE_COUNT] = {0};

		iaStatAdd[STAT_TYPE_RUN]			= pAvatarInfo->Status.iStatRun - pAvatarInfo->Status.iStatRunBase;
		iaStatAdd[STAT_TYPE_JUMP]			= pAvatarInfo->Status.iStatJump - pAvatarInfo->Status.iStatJumpBase;
		iaStatAdd[STAT_TYPE_STR]				= pAvatarInfo->Status.iStatStr - pAvatarInfo->Status.iStatStrBase;
		iaStatAdd[STAT_TYPE_PASS]			= pAvatarInfo->Status.iStatPass - pAvatarInfo->Status.iStatPassBase;
		iaStatAdd[STAT_TYPE_DRIBBLE]		= pAvatarInfo->Status.iStatDribble - pAvatarInfo->Status.iStatDribbleBase;
		iaStatAdd[STAT_TYPE_REBOUND]	=pAvatarInfo->Status.iStatRebound - pAvatarInfo->Status.iStatReboundBase;
		iaStatAdd[STAT_TYPE_BLOCK]		= pAvatarInfo->Status.iStatBlock - pAvatarInfo->Status.iStatBlockBase;
		iaStatAdd[STAT_TYPE_STEAL]			= pAvatarInfo->Status.iStatSteal - pAvatarInfo->Status.iStatStealBase;
		iaStatAdd[STAT_TYPE_2POINT]		= pAvatarInfo->Status.iStat2Point - pAvatarInfo->Status.iStat2PointBase;
		iaStatAdd[STAT_TYPE_3POINT]		= pAvatarInfo->Status.iStat3Point - pAvatarInfo->Status.iStat3PointBase;
		iaStatAdd[STAT_TYPE_DRIVE]			= pAvatarInfo->Status.iStatDrive - pAvatarInfo->Status.iStatDriveBase;
		iaStatAdd[STAT_TYPE_CLOSE]			= pAvatarInfo->Status.iStatClose - pAvatarInfo->Status.iStatCloseBase;
		
		int iPropensityItemUserInventoryIndex = -1;
		if (-1 != pAvatarInfo->iSpecialCharacterIndex)
		{
			SSpecialAvatarConfig* pAvatarConfig = AVATARCREATEMANAGER.GetSpecialAvatarConfigByIndex(pAvatarInfo->iSpecialCharacterIndex);
			if (NULL != pAvatarConfig)
			{
				switch(pAvatarInfo->Status.iGamePosition)
				{
				case POSITION_CODE_FOWARD:
					{
						CHECK_CONDITION_RETURN(
							POSITION_CODE_POWER_FOWARD == iPos && 
							FALSE == pAvatarConfig->_naChangePosition[CHANGE_AVATAR_POSITION_POWER_FORWARD], FALSE);

						CHECK_CONDITION_RETURN(
							POSITION_CODE_SMALL_FOWARD == iPos && 
							FALSE == pAvatarConfig->_naChangePosition[CHANGE_AVATAR_POSITION_SMALL_FORWARD], FALSE);
					}
					break;
				case POSITION_CODE_GUARD:
					{
						CHECK_CONDITION_RETURN(
							POSITION_CODE_POINT_GUARD == iPos && 
							FALSE == pAvatarConfig->_naChangePosition[CHANGE_AVATAR_POSITION_POINT_GUARD], FALSE);

						CHECK_CONDITION_RETURN(
							POSITION_CODE_SHOOT_GUARD == iPos && 
							FALSE == pAvatarConfig->_naChangePosition[CHANGE_AVATAR_POSITION_SHOOTING_GUARD], FALSE);
					}
					break;
				}

				SpecialAvatarStatInfo* pNewAddStatInfo = AVATARCREATEMANAGER.GetSpecialAvatarAddStat(pAvatarInfo->iSpecialCharacterIndex, pAvatarInfo->Status.iHeight, iPos);
				if (NULL != pNewAddStatInfo)
				{
					memcpy(iaStatAdd, pNewAddStatInfo->iaAddStat, sizeof(int)*MAX_STAT_TYPE_COUNT);
				}
				
				if (NULL != pAvatarConfig->_pAvatarPropensityItem)
				{
					CAvatarItemList* pAvatarItemList = GetCurAvatarItemListWithGameID(GetGameID());
					if (NULL != pAvatarItemList)
					{
						SUserItemInfo* pPropensityItem = pAvatarItemList->GetInventoryItemWithPropertyKind(PROPERTY_KIND_PROPENSITY_ITEM);
						if (NULL != pPropensityItem)
						{
							iPropensityItemUserInventoryIndex = pPropensityItem->iItemIdx;

							pAvatarItemList->RemoveItemProperty(iPropensityItemUserInventoryIndex);	//���� �Ӽ� �� �����

							ITEMPROPERTYCONFIGMAP_FORPOSITION::iterator iter = pAvatarConfig->_pAvatarPropensityItem->mapItemPropertyConfig.find(iPos);
							if (pAvatarConfig->_pAvatarPropensityItem->mapItemPropertyConfig.end() != iter)
							{
								//���� �־�����
								SUserItemProperty ItemProperty;
								ItemProperty.iGameIDIndex = GetGameIDIndex();
								ItemProperty.iItemIdx = iPropensityItemUserInventoryIndex;
								pPropensityItem->iPropertyNum = iter->second.size();

								ITEMPROPERTYCONFIGLIST::iterator iterProperty = iter->second.begin();
								ITEMPROPERTYCONFIGLIST::iterator endIterProperty = iter->second.end();
								for (; iterProperty != endIterProperty; ++iterProperty)
								{
									ItemProperty.iProperty = (*iterProperty).iProperty;
									ItemProperty.iValue = (*iterProperty).iPropertyValue;

									pAvatarItemList->AddUserItemProperty(ItemProperty);
								}
							}
						}
					}
				}
			}
		}

		SAvatarStatus Status;

		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateGamePosition(pAvatarInfo->iGameIDIndex, iPos, iPropensityItemUserInventoryIndex, Status))
		{
			return FALSE;
		}
		
		pAvatarInfo->Status.iGamePosition = iPos;
		
		pAvatarInfo->Status.iStatRunBase = Status.iStatRunBase;
		pAvatarInfo->Status.iStatJumpBase = Status.iStatJumpBase;
		pAvatarInfo->Status.iStatStrBase = Status.iStatStrBase;
		pAvatarInfo->Status.iStatPassBase = Status.iStatPassBase;
		pAvatarInfo->Status.iStatDribbleBase = Status.iStatDribbleBase;
		pAvatarInfo->Status.iStatReboundBase = Status.iStatReboundBase;
		pAvatarInfo->Status.iStatBlockBase = Status.iStatBlockBase;
		pAvatarInfo->Status.iStatStealBase = Status.iStatStealBase;
		pAvatarInfo->Status.iStat2PointBase = Status.iStat2PointBase;
		pAvatarInfo->Status.iStat3PointBase = Status.iStat3PointBase;
		pAvatarInfo->Status.iStatDriveBase = Status.iStatDriveBase;
		pAvatarInfo->Status.iStatCloseBase = Status.iStatCloseBase;
		
		pAvatarInfo->Status.iStatRun = Status.iStatRunBase				+ iaStatAdd[STAT_TYPE_RUN];	
		pAvatarInfo->Status.iStatJump = Status.iStatJumpBase			+ iaStatAdd[STAT_TYPE_JUMP];
		pAvatarInfo->Status.iStatStr = Status.iStatStrBase					+ iaStatAdd[STAT_TYPE_STR];
		pAvatarInfo->Status.iStatPass = Status.iStatPassBase				+ iaStatAdd[STAT_TYPE_PASS];
		pAvatarInfo->Status.iStatDribble = Status.iStatDribbleBase		+ iaStatAdd[STAT_TYPE_DRIBBLE];
		pAvatarInfo->Status.iStatRebound = Status.iStatReboundBase + iaStatAdd[STAT_TYPE_REBOUND];
		pAvatarInfo->Status.iStatBlock = Status.iStatBlockBase			+ iaStatAdd[STAT_TYPE_BLOCK];
		pAvatarInfo->Status.iStatSteal = Status.iStatStealBase			+ iaStatAdd[STAT_TYPE_STEAL];
		pAvatarInfo->Status.iStat2Point = Status.iStat2PointBase		+ iaStatAdd[STAT_TYPE_2POINT];
		pAvatarInfo->Status.iStat3Point = Status.iStat3PointBase		+ iaStatAdd[STAT_TYPE_3POINT];
		pAvatarInfo->Status.iStatDrive = Status.iStatDriveBase			+ iaStatAdd[STAT_TYPE_DRIVE];
		pAvatarInfo->Status.iStatClose = Status.iStatCloseBase			+ iaStatAdd[STAT_TYPE_CLOSE];

		if ( TRUE == GetCloneCharacter() )
		{
			for(int i = Clone_Index_Mine; i < Max_Clone; ++i)
			{
				for(int j = 0; j < MAX_STAT_TYPE_COUNT; ++j)
				{
					m_sCloneCharacterInfo[i].Status.iGamePosition = iPos;
				}
			}
		}
	}
	
	return TRUE;
}

void CFSGameUser::CheckPositionSelect()
{
	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pFSODBC );

	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	
	if( NULL != pAvatarInfo )
	{
		
		//////////////////// WCG /////////////////////////////////////////////////////////
		if( CFSGameServer::IsWCGServer() )
		{
			if( m_bWCGPositionSelect == false )
			{
				if( pAvatarInfo->Status.iGamePosition > 0 )  /// ���Ͱ� �ƴϸ� 
				{
					CPacketComposer PacketComposer(S2C_POSITION_SELECT_REQ);
					PacketComposer.Add(2);
					
					if( pAvatarInfo->Status.iGamePosition < 3 ) /// ��ȭ�����̸� 
					{
						PacketComposer.Add(pAvatarInfo->Status.iGamePosition*2+1);
						PacketComposer.Add((pAvatarInfo->Status.iGamePosition*2)+2);
					}
					else
					{
						PacketComposer.Add( ((pAvatarInfo->Status.iGamePosition-1)/2)*2 +1 );
						PacketComposer.Add( ((pAvatarInfo->Status.iGamePosition-1)/2)*2 +2);
					}
					
					Send(&PacketComposer);
					
					
					m_UserSkill->CheckSkillSlotExpired();
					m_UserSkill->m_bSetSkillForWCG = true;
					
					
				}
				else		//// �����̸� ����� ����
				{
					m_UserSkill->CheckSkillSlotExpired();
					
					m_UserSkill->LoadSkillForWCG();
					m_UserSkill->m_bSetSkillForWCG = true;
					
					
				}
				
				m_bWCGPositionSelect = true;
			}
			
		}
		else
		{
			if( pAvatarInfo->iLv >= GAME_POSITION_UP && pAvatarInfo->Status.iGamePosition != 0 && pAvatarInfo->Status.iGamePosition <= 2) 
			{
				//Send Position Select
				CPacketComposer PacketComposer(S2C_POSITION_SELECT_REQ);
				PacketComposer.Add(POSITION_TYPE_SEND);
				
				PacketComposer.Add(pAvatarInfo->Status.iGamePosition*2+1);
				PacketComposer.Add((pAvatarInfo->Status.iGamePosition*2)+2);
				
				Send(&PacketComposer);
			}
		}
		
	}
}

CAvatarItemList* CFSGameUser::GetCurAvatarItemListWithGameID(char *szGameID)
{
	CAvatarItemList *pAvatarItem = NULL;
	
	pAvatarItem = m_UserItem->GetCurAvatarItemList();
	
	return pAvatarItem;
}

BOOL CFSGameUser::CanWhisper()
{
	BOOL bReturn = FALSE;
	CFSGameClient* pClient = (CFSGameClient*) GetClient();
	
	if( pClient && GetOptionEnable(OPTION_TYPE_WHISPER) )
	{
		bReturn = TRUE; 
	}
	
	return bReturn;
}


BOOL CFSGameUser::CanInvite()
{
	BOOL bReturn = FALSE;
	CFSGameClient* pClient = (CFSGameClient*) GetClient();
	
	if( pClient && NEXUS_LOBBY == pClient->GetState() && GetOptionEnable(OPTION_TYPE_INVITE) )
	{
		bReturn = TRUE; 
	}
	
	return bReturn;
}

void CFSGameUser::RemoveDisconncted()
{
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if(pAvatarInfo)
	{
		pAvatarInfo->iDisconnected = NO_INTENTIAL_EXIT;
	}
	
}

BOOL CFSGameUser::GetAvatarItemStatus(int *iaStat, int *iaMinusStat/* = NULL*/)
{
	BOOL bWearPropertyItem = FALSE;
	return GetAvatarItemStatus(iaStat, bWearPropertyItem, iaMinusStat);
}

BOOL CFSGameUser::GetAvatarItemStatus(int *iaStat, BOOL& bWearPropertyItem, int *iaMinusStat/* = NULL*/, int* piCloneFeature/* = NULL*/)
{
	SAvatarInfo * pAvatarInfo = NULL;
	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( pAvatarInfo == NULL || NULL == pAvatarItemList ) return FALSE;
	
	CFSGameServer* pServer = CFSGameServer::GetInstance();									
	if( NULL == pServer ) return FALSE;
	
	memset( iaStat , 0 , sizeof(int) * MAX_STAT_NUM );

	bool bCheckClone = (nullptr != piCloneFeature);

	for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
	{
		const int iItemCode = (bCheckClone) ? piCloneFeature[i] : pAvatarInfo->AvatarFeature.FeatureInfo[i];

		if( -1 != iItemCode )
		{
			SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCodeNotChangeDressItem(iItemCode, bCheckClone);
			if( pSlotItem != NULL )
			{
				if( pSlotItem->iChannel == ITEMCHANNEL_BOOST2)
				{
					continue;					
				}		
				
				if( (pSlotItem->iBigKind == ITEM_BIG_KIND_ACC 
					|| (MIN_LINK_ITEM_PROPERTY_KIND_NUM <= pSlotItem->iPropertyKind && pSlotItem->iPropertyKind <= MAX_LINK_ITEM_PROPERTY_KIND_NUM) 
					|| pSlotItem->iPropertyKind == ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC) && 
						FALSE == pAvatarItemList->CheckUserApplyAccItemProperty(pSlotItem->iItemIdx) )
				{	// �Ǽ��縮���� �ɷ�ġ�� ���� ���� LinkItem �������� ó��.
					continue;
				}
				else if( (pSlotItem->iPropertyKind < MIN_LINK_ITEM_PROPERTY_KIND_NUM  || pSlotItem->iPropertyKind > MAX_LINK_ITEM_PROPERTY_KIND_NUM) || pSlotItem->iPropertyKind == ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC)
				{
					int iUserItemPropertyNum = pSlotItem->iPropertyNum;
					if( iUserItemPropertyNum > 0 )
					{
						vector< SUserItemProperty > vUserItemProperty;
							
						pAvatarItemList->GetItemProperty(pSlotItem->iItemIdx, vUserItemProperty);
							
						for( int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++ )
						{
							if( 0 <= vUserItemProperty[iPropertyCount].iProperty  && vUserItemProperty[iPropertyCount].iProperty < MAX_STAT_NUM )
							{
								if (0 > vUserItemProperty[iPropertyCount].iValue &&
									NULL != iaMinusStat)
								{
									iaMinusStat[ vUserItemProperty[iPropertyCount].iProperty ] += vUserItemProperty[iPropertyCount].iValue;
								}
								else
								{
									iaStat[ vUserItemProperty[iPropertyCount].iProperty ] += vUserItemProperty[iPropertyCount].iValue;
								}
								bWearPropertyItem = TRUE;
							}
							else if( vUserItemProperty[iPropertyCount].iProperty == 30 )
							{
								for(int i=0; i < MAX_STAT_NUM ; i++)
								{
									iaStat[i] += vUserItemProperty[iPropertyCount].iValue;
								}
								bWearPropertyItem = TRUE;
							}
								
						}
					}
				}
			}
		}
	}
	
	int iBindAccountItemPropertyKind = GetBindAccountItemPropertyKind();
	if(iBindAccountItemPropertyKind > DONT_HAVE_VPI_ITEM)
	{
		CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
		CHECK_NULL_POINTER_BOOL(pItemShop);

		CShopItemList* pShopItemList = pItemShop->GetShopItemList();
		CHECK_NULL_POINTER_BOOL(pShopItemList);
		
		SBindAccountItemBonusData* pBindAccountItemBonusData= pShopItemList->GetBindAccountBonusData(iBindAccountItemPropertyKind);
		CHECK_NULL_POINTER_BOOL(pBindAccountItemBonusData);

		for(int i =0; i< MAX_STAT_NUM; i++)
		{
			iaStat[i] += pBindAccountItemBonusData->iaBindAccountStat[i];
		}	
	}

	for( int iPremiumKind = PREMIUM_ITEM_KIND_START+1; iPremiumKind < PREMIUM_ITEM_KIND_END; iPremiumKind++ )
	{
		if(iPremiumKind == PREMIUM_ITEM_KIND_FLAME_MAN && iBindAccountItemPropertyKind > DONT_HAVE_VPI_ITEM )
		{
			continue;
		}

		SUserItemInfo * pSlotItem = pAvatarItemList->GetPremiumItemWithPropertyKind(iPremiumKind);
		if( pSlotItem != NULL )
		{
			int iUserItemPropertyNum = pSlotItem->iPropertyNum;
			if( iUserItemPropertyNum > 0 )
			{
				vector< SUserItemProperty > vUserItemProperty;
				
				pAvatarItemList->GetItemProperty(pSlotItem->iItemIdx, vUserItemProperty);
				
				for( int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++ )
				{
					if( 0 <= vUserItemProperty[iPropertyCount].iProperty  && vUserItemProperty[iPropertyCount].iProperty < MAX_STAT_NUM )
					{
						iaStat[ vUserItemProperty[iPropertyCount].iProperty ] += vUserItemProperty[iPropertyCount].iValue;
					}
					else if( vUserItemProperty[iPropertyCount].iProperty == 30 )
					{
						for(int i=0; i < MAX_STAT_NUM ; i++)
						{
							iaStat[i] += vUserItemProperty[iPropertyCount].iValue;
						}
					}
				}
			}
		}
	}
	SUserItemInfo * pBuffStatusItem = pAvatarItemList->GetItemWithPropertyKind(BUFF_ITEM_KIND_STATUS);
	if( pBuffStatusItem != NULL )
	{
		int iUserItemPropertyNum = pBuffStatusItem->iPropertyNum;
		if( iUserItemPropertyNum > 0 )
		{
			vector< SUserItemProperty > vUserItemProperty;
			pAvatarItemList->GetItemProperty(pBuffStatusItem->iItemIdx, vUserItemProperty);

			for( int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++ )
			{
				if( 0 <= vUserItemProperty[iPropertyCount].iProperty  && vUserItemProperty[iPropertyCount].iProperty < MAX_STAT_NUM )
				{
					iaStat[ vUserItemProperty[iPropertyCount].iProperty ] += vUserItemProperty[iPropertyCount].iValue;
				}
			}
		}
	}
	if ( CFSGameServer::GetInstance()->GetUseLuckItem() )
	{
		SUserItemInfo * pLuckItem =  pAvatarItemList->GetPremiumItemWithPropertyKind(LICK_ITEM_KIND_NUM);
		if( pLuckItem	)
		{
			int iUserItemPropertyNum = pLuckItem->iPropertyNum;
			if( iUserItemPropertyNum > 0 )
			{
				vector< SUserItemProperty > vUserItemProperty;
				pAvatarItemList->GetItemProperty(pLuckItem->iItemIdx, vUserItemProperty);
				for( int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++ )
				{
					if( 0 <= vUserItemProperty[iPropertyCount].iProperty  && vUserItemProperty[iPropertyCount].iProperty < MAX_STAT_NUM )
					{						
						iaStat[ vUserItemProperty[iPropertyCount].iProperty ] += vUserItemProperty[iPropertyCount].iValue;
					}
				}
			}
		}
	}

	int iUseStyle = GetUseStyleIndex();

	if(nullptr != piCloneFeature)
	{
		Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > iUseStyle ? Clone_Index_Clone : Clone_Index_Mine ;// �Ųٷ� �ٲ���� ��.
		iUseStyle = GetCloneCharacterStyleIndex(eIndex) - 1;
	}

	map<int, SSpecialPartsItemEquipInfo*> mapSpecialPartsItemEquipInfo;

	pAvatarItemList->GetSpecialPartsItemEquipList(mapSpecialPartsItemEquipInfo);

	map<int, SSpecialPartsItemEquipInfo*>::iterator itrMap;
	map<int, SSpecialPartsItemEquipInfo*>::iterator itrMapEnd = mapSpecialPartsItemEquipInfo.end();

	for(itrMap = mapSpecialPartsItemEquipInfo.begin(); itrMap != itrMapEnd; ++itrMap)
	{
		SSpecialPartsItemEquipInfo* pSpecialPartsItemEquipInfo = itrMap->second;

		if(pSpecialPartsItemEquipInfo)
		{
			int iSpecialPartsIndex = pSpecialPartsItemEquipInfo->iSpecialPartsIndex;
			BYTE btEquipCount = 0;

			for(int i = 0; i < MAX_SPECIAL_PARTS_ITEM_COUNT; ++i)
			{
				if(pSpecialPartsItemEquipInfo->iItemStatus[i] == ITEM_STATUS_WEAR)
				{
					++btEquipCount;
				}
			}

			SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
			vector<SSpecialPartsPropertyList> vecPropertyList;
			SPECIALPARTS.GetSpecialPartsPropertyList(sKey, vecPropertyList);
			int iStatNum = vecPropertyList.size();

			for(int i = 0; i < iStatNum; ++i)
			{
				if(vecPropertyList[i].btListType != SPECIALPARTS_PROPERTY_LIST_NORMAL_CHOICE &&
					vecPropertyList[i].btListType != SPECIALPARTS_PROPERTY_LIST_ALLSTAT_CHOICE)
				{
					continue;
				}

				SSpecialPartsUserProperty* pSpecialPartsUserProperty = GetSpecialPartsUserProperty(iUseStyle, iSpecialPartsIndex, vecPropertyList[i].btPropertyNum);

				if(pSpecialPartsUserProperty)
				{
					int iProperty = pSpecialPartsUserProperty->iProperty;
					int iValue = pSpecialPartsUserProperty->iValue;

					if(PROPERTY_RUN <= iProperty && iProperty <= PROPERTY_CLOSE)
					{
						iaStat[iProperty] += iValue;

						bWearPropertyItem = TRUE;
					}
					else if(PROPERTY_ALL_STAT == iProperty)
					{
						for(int i = 0; i < MAX_STAT_NUM; ++i)
							iaStat[i] += iValue;

						bWearPropertyItem = TRUE;
					}
				}
			}

			delete pSpecialPartsItemEquipInfo;
		}
	}

	vector<int> vecEquipProduct;
	m_SlotPackageList.GetEquipItemAll( vecEquipProduct );
	std::set<int> setProperty;
	set<int>::iterator itr;
	int iEquipProductSize = vecEquipProduct.size();

	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );

	for( int i = 0; i < iEquipProductSize; ++i )
	{
		SPotentialComponent sPotentialInfo;
		GetPotentialComponent( vecEquipProduct[i], sPotentialInfo );

		if( POTENTIAL_COMPONENT_ABILITY == sPotentialInfo.eComponentIndex )
		{
			SPotentialComponentConfig sPotentialConfigInfo;
			COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );			
			setProperty.insert(sPotentialConfigInfo.iParam);
		}
	} 
	
	for(itr = setProperty.begin(); itr != setProperty.end(); itr++)
	{
		int iProperty = *itr;
		CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList( iProperty );
		CHECK_NULL_POINTER_BOOL( pItemPropertyList );

		SItemProperty* pItemProperty = pItemPropertyList->GetItemProperty( 0 );
		CHECK_NULL_POINTER_BOOL( pItemProperty );

		iaStat[ pItemProperty->iProperty ] += pItemProperty->iValue;
	}

	if (PCROOM_KIND_PREMIUM == GetPCRoomKind())
	{
		for(int i =0; i < MAX_STAT_TYPE_COUNT; i++)
		{
			iaStat[i] += PREMIUM_PCROOM_STAT_BONUS;
		}
	}

	return TRUE;
}

BOOL CFSGameUser::CheckExpPointBonusItem(int &iExp, int &iSkillPoint, int &iFame)
{
	iExp = 0; iSkillPoint = 0; iFame = 0;

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	
	if(NULL != pAvatarInfo && NULL != pAvatarItemList)
	{
		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != pAvatarInfo->AvatarFeature.FeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]);
				
				if(NULL != pSlotItem)
				{
					if (ITEM_PROPERTY_KIND_EXP_POINT == pSlotItem->iPropertyKind ||
						(MIN_HEART_TATTO_KIND_NUM <= pSlotItem->iPropertyKind && MAX_HEART_TATTO_KIND_NUM >= pSlotItem->iPropertyKind))
					{
						int iUserItemPropertyNum = pSlotItem->iPropertyNum;
						if(0 < iUserItemPropertyNum)
						{
							vector< SUserItemProperty > vUserItemProperty;
							
							pAvatarItemList->GetItemProperty(pSlotItem->iItemIdx, vUserItemProperty);
							
							for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
							{
								if(20 == vUserItemProperty[iPropertyCount].iProperty) // ����ġ 
								{
									iExp = vUserItemProperty[iPropertyCount].iValue;

									if(GetPCRoomKind() == PCROOM_KIND_PREMIUM &&
										(pSlotItem->iItemCode == 612201 || pSlotItem->iItemCode == 612901 || pSlotItem->iItemCode == 613001))
										iExp += 10;
								}
								
								if(21 == vUserItemProperty[iPropertyCount].iProperty) // ����Ʈ 
								{
									iSkillPoint = vUserItemProperty[iPropertyCount].iValue;

									if(GetPCRoomKind() == PCROOM_KIND_PREMIUM &&
										(pSlotItem->iItemCode == 612201 || pSlotItem->iItemCode == 612901 || pSlotItem->iItemCode == 613001))
										iSkillPoint += 10;
								}
								
								if(22 == vUserItemProperty[iPropertyCount].iProperty) // ���� 
								{
									iFame = vUserItemProperty[iPropertyCount].iValue;
								}
							}
						}
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CFSGameUser::CheckExpPointBonusWinItem(int &iExp, int &iSkillPoint)
{
	iExp = 0; iSkillPoint = 0;

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	
	if(NULL != pAvatarInfo && NULL != pAvatarItemList)
	{
		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != pAvatarInfo->AvatarFeature.FeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]);
				if(NULL != pSlotItem)
				{
					if(MIN_WIN_TATTOO_KIND_NUM <= pSlotItem->iPropertyKind && MAX_WIN_TATTOO_KIND_NUM >= pSlotItem->iPropertyKind)
					{
						int iUserItemPropertyNum = pSlotItem->iPropertyNum;
						if(0 < iUserItemPropertyNum)
						{
							vector< SUserItemProperty > vUserItemProperty;

							pAvatarItemList->GetItemProperty(pSlotItem->iItemIdx, vUserItemProperty);

							for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
							{
								if(20 == vUserItemProperty[iPropertyCount].iProperty) // ����ġ 
								{
									iExp = vUserItemProperty[iPropertyCount].iValue;
								}

								if(21 == vUserItemProperty[iPropertyCount].iProperty) // ����Ʈ 
								{
									iSkillPoint = vUserItemProperty[iPropertyCount].iValue;
								}

							}
						}
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CFSGameUser::CheckPremiumItemExpPointBonus(int &iExp, int &iSkillPoint)
{
	iExp = 0; iSkillPoint = 0;

	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) 
		return -1;

	int iBindAccountItemPropertyKind = GetBindAccountItemPropertyKind();
	if(DONT_HAVE_VPI_ITEM < iBindAccountItemPropertyKind)
	{
		SBindAccountItemBonusData* pBindAccountItemBonusData = GetBindAccountBonusData(iBindAccountItemPropertyKind);
		CHECK_NULL_POINTER_BOOL(pBindAccountItemBonusData);

		iExp = pBindAccountItemBonusData->iBonusExp;
		iSkillPoint = pBindAccountItemBonusData->iBonusPoint;
	}
	else
	{
		for(int iPremiumKind = PREMIUM_ITEM_KIND_START+1; iPremiumKind < PREMIUM_ITEM_KIND_END; iPremiumKind++)
		{
			SUserItemInfo* pSlotItem = pAvatarItemList->GetPremiumItemWithPropertyKind(iPremiumKind);
			if(NULL != pSlotItem)
			{
				int iUserItemPropertyNum = pSlotItem->iPropertyNum;
				if(0 < iUserItemPropertyNum)
				{
					vector< SUserItemProperty > vUserItemProperty;

					pAvatarItemList->GetItemProperty(pSlotItem->iItemIdx, vUserItemProperty);

					for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
					{
						if(20 == vUserItemProperty[iPropertyCount].iProperty) // ����ġ 
						{
							iExp += vUserItemProperty[iPropertyCount].iValue;
						}

						if(21 == vUserItemProperty[iPropertyCount].iProperty) // ����Ʈ 
						{
							iSkillPoint += vUserItemProperty[iPropertyCount].iValue;
						}
					}
				}
			}
		}
	}

	SUserItemInfo* pBuffExpItem = pAvatarItemList->GetItemWithPropertyKind(BUFF_ITEM_KIND_EXP);
	if(NULL != pBuffExpItem && pBuffExpItem->iStatus != ITEM_STATUS_EXPIRED && pBuffExpItem->iStatus != ITEM_STATUS_PRESENTBOX )
	{
		int iUserItemPropertyNum = pBuffExpItem->iPropertyNum;
		if(0 < iUserItemPropertyNum)
		{
			vector< SUserItemProperty > vUserItemProperty;

			pAvatarItemList->GetItemProperty(pBuffExpItem->iItemIdx, vUserItemProperty);

			for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
			{
				if(20 == vUserItemProperty[iPropertyCount].iProperty) // ����ġ 
				{
					iExp += vUserItemProperty[iPropertyCount].iValue;
				}

				if(21 == vUserItemProperty[iPropertyCount].iProperty) // ����Ʈ 
				{
					iSkillPoint += vUserItemProperty[iPropertyCount].iValue;
				}
			}
		}
	}

	SUserItemInfo* pBuffPointItem = pAvatarItemList->GetItemWithPropertyKind(BUFF_ITEM_KIND_POIN);
	if(NULL != pBuffPointItem && pBuffPointItem->iStatus != ITEM_STATUS_EXPIRED && pBuffPointItem->iStatus != ITEM_STATUS_PRESENTBOX )
	{
		int iUserItemPropertyNum = pBuffPointItem->iPropertyNum;
		if(0 < iUserItemPropertyNum)
		{
			vector< SUserItemProperty > vUserItemProperty;

			pAvatarItemList->GetItemProperty(pBuffPointItem->iItemIdx, vUserItemProperty);

			for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
			{
				if(20 == vUserItemProperty[iPropertyCount].iProperty) // ����ġ 
				{
					iExp += vUserItemProperty[iPropertyCount].iValue;
				}

				if(21 == vUserItemProperty[iPropertyCount].iProperty) // ����Ʈ 
				{
					iSkillPoint += vUserItemProperty[iPropertyCount].iValue;
				}
			}
		}
	}

	int iUserCheckLv = 0;
	if(IsActiveLvIntervalBuffItem(ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF_EXP, iUserCheckLv))
	{
		iExp += 100;
	}

	return TRUE;
}

void CFSGameUser::SetAvatarFeatureInfo()
{
	int iUseStyleIndex = GetUseStyleIndex() - 1;
	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();

	if(NULL == pAvatarInfo || NULL == pAvatarItemList || iUseStyleIndex < 0 || iUseStyleIndex > MAX_STYLE_COUNT)
	{
		return;
	}

	for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
	{
		if(-1 != m_AvatarStyleInfo.iItemIdx[iUseStyleIndex][i])
		{
			SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemIdx(m_AvatarStyleInfo.iItemIdx[iUseStyleIndex][i]);

			if(NULL != pSlotItem)
			{
				pAvatarInfo->AvatarFeature.FeatureItemIndex[i] = pSlotItem->iItemIdx;
				pAvatarInfo->AvatarFeature.FeatureProtertyType[i] = pSlotItem->iPropertyType;
				pAvatarInfo->AvatarFeature.FeatureProtertyKind[i] = pSlotItem->iPropertyKind;
				pAvatarInfo->AvatarFeature.FeatureProtertyChannel[i] = pSlotItem->iChannel;
				pAvatarInfo->AvatarFeature.FeatureProtertyValue[i] = pSlotItem->iPropertyTypeValue;
			}
		}
	}
	for(int i=0; i<MAX_CHANGE_DRESS_NUM; i++)
	{
		if(-1 != pAvatarInfo->AvatarFeature.ChangeDressFeatureInfo[i])
		{
			SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.ChangeDressFeatureInfo[i]);

			if(NULL != pSlotItem)
			{
				pAvatarInfo->AvatarFeature.ChangeDressFeatureItemIndex[i] = pSlotItem->iItemIdx;
				pAvatarInfo->AvatarFeature.ChangeDressFeatureProtertyChannel [i] = pSlotItem->iChannel;
			}
		}
	}
}

void CFSGameUser::ExpireAvatarBindAccountItem(CFSGameODBC* pFreeStyleODBC, vector<int> vecExpiredItem)
{
	CHECK_NULL_POINTER_VOID(pFreeStyleODBC);

	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	for (int iLoopIndex = 0; iLoopIndex < vecExpiredItem.size(); iLoopIndex++)
	{
		SUserItemInfo* pUserItemInfo = pAvatarItemList->GetItemWithItemIdx(vecExpiredItem[iLoopIndex]);
		if (NULL != pUserItemInfo)
		{
			m_UserItem->ExpireItem(pFreeStyleODBC, pUserItemInfo->iItemIdx, ITEM_STATUS_RESELL);
		}
	}
}

BOOL CFSGameUser::CheckExpireItem()
{
	CFSGameODBC *pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pGameODBC );


	SAvatarInfo * pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);
	
	CHECK_NULL_POINTER_BOOL(m_UserItem);

	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);
	
	time_t CurrentTime = _time64(NULL);
	CheckBindAccountItemExpire(CurrentTime);

	vector<int> vecExpiredItemList;
	if (TRUE == CheckBindAccountInventoryItemExpire(CurrentTime, vecExpiredItemList))
	{
		ExpireAvatarBindAccountItem(pGameODBC, vecExpiredItemList);
	}

	vector<int> VINT; 
	pAvatarItemList->Lock();

	int iVectorReserve = pAvatarItemList->GetSize();
	if(5 < iVectorReserve)
		VINT.reserve(iVectorReserve);

	CTypeList< SUserItemInfo* >::POSITION pos = pAvatarItemList->GetHeadPosition();
	CTypeList< SUserItemInfo* >::POSITION pEnd = pAvatarItemList->GetEndPosition();
	while( pos != pEnd )
	{
		SUserItemInfo* pItem = (*pos);
		if( pItem != NULL )
		{
			VINT.push_back(pItem->iItemIdx);
		}
		pos = pAvatarItemList->GetNextPosition(pos);
	}
	int nSzie = VINT.size();
	int iChangedSpecialPartsItemCnt = 0;

	for( int n = 0; n < nSzie; n++ )
	{
		SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(VINT[n]);
		if ( pItem  )
		{
			if ( ( pItem->iBigKind == ITEM_BIG_KIND_ACC || pItem->iSmallKind == ITEM_SMALL_KIND_HAIR) && 
				pItem->iPropertyKind >= ITEM_PROPERTY_KIND_DATEVALUE_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_DATEVALUE_MAX)
			{
				if( CurrentTime > pItem->PropertyExpireDate )
				{
					pGameODBC->SP_DeleteSpecialPropertyItem(GetGameIDIndex(), pItem->iItemIdx);
					pItem->iPropertyKind = -1;
					pItem->iPropertyNum = 0;

					if(TRUE == pAvatarItemList->CheckUserApplyAccItemProperty(pItem->iItemIdx))  
					{ 
						if(ODBC_RETURN_SUCCESS == pGameODBC->ITEM_DeleteUserApplyAccItemProperty(GetGameIDIndex(), pItem->iItemIdx))                           
						{ 
							pAvatarItemList->RemoveUserApplyAccItemProperty(pItem->iItemIdx); 
						} 
					}

					pAvatarItemList->RemoveItemProperty(pItem->iItemIdx);
				}
			}

			if ( pItem->iPropertyKind == ITEM_PROPERTY_KIND_VOICE_SETTING_ITEM && CurrentTime > pItem->ExpireDate )
			{
				if ( m_UserItem->DeleteVoiceSettingInfo( pItem->iItemIdx ) )
				{
					pGameODBC->VOICE_DeleteUserVoiceInfo( GetGameIDIndex(), pItem->iItemIdx );
				}
			}
	
			if( pItem->iPropertyType != ITEM_PROPERTY_TIME || 
				pItem->iStatus == ITEM_STATUS_RESELL ||
				 pItem->iPropertyTypeValue == -1 )
			{
				continue;
			}

			if (ITEM_STATUS_EXPIRED == pItem->iStatus)
			{
				if( CurrentTime > pItem->ExpireDate + EXPIRED_ITEM_DELETE_WAITING_TIME )
				{
					m_UserItem->DeleteExpiredItem(pGameODBC, pItem->iItemIdx);
				}
			}
			else
			{
				if( CurrentTime > pItem->ExpireDate )
				{
					m_UserItem->ExpireItem(pGameODBC, pItem->iItemIdx);

					if(pItem->iSpecialPartsIndex > 0)
						iChangedSpecialPartsItemCnt++;
				}		
			}
		}
	}
	pAvatarItemList->Unlock();

	//CoachCard
	CheckCoachCardExpireItem();
	
	if(iChangedSpecialPartsItemCnt > 0)
		CheckSpecialPartsUserProperty();

	return TRUE;
}

BOOL CFSGameUser::CheckCoachCardExpireItem()
{
	vector<int> vecExpireItem;
	int iRet = TRUE;
	SSlotData sSlotData;
	if (TRUE == m_ProductInventory.CheckExpireItem(vecExpireItem))
	{
		for (int i = 0 ; i < vecExpireItem.size(); i++)
		{
			if (TRUE == m_SlotPackageList.GetSlotData(vecExpireItem[i],sSlotData))
			{
				if( TRUE == PotentialProductReSet(sSlotData.iProductIndex) )
				{
					iRet = TRUE;
				}
				else
				{
					iRet = FALSE;
				}

// 				if (TRUE == UnEquipProduct(sSlotData.iTendencyType,iErrorCode))
// 				{
// 					iRet = TRUE;
// 				}
// 				else
// 				{
// 					iRet = FALSE;
// 				}
			}
		}
		
	}
	return iRet;
}

void CFSGameUser::SetCurrentTotalRecord()
{
	SAvatarInfo *pAvatarInfo = GetCurUsedAvatar();
	
	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	
	sprintf_s(m_GameRecordLog.szLoginTime, _countof(m_GameRecordLog.szLoginTime), "11866902-00-18227200 3435973836:3435973836:3435973836", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay, SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond);	
 =  true;
	
	m_GameRecordLog.sLoginLv = (short) pAvatarInfo->iLv;
	m_GameRecordLog.iLoginExp = pAvatarInfo->iExp;
	m_GameRecordLog.iLoginSkillPoint = m_iSkillPoint;
	m_GameRecordLog.iLoginWin = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_WIN];
	m_GameRecordLog.iLoginLose = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_LOSE];
	m_GameRecordLog.iLoginPoint = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_POINT2] + pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_POINT3];
	m_GameRecordLog.iLoginPoint3 = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_POINT3];
	m_GameRecordLog.iLoginAsist = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_ASSIST];
	m_GameRecordLog.iLoginRebound = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_REBOUND];
	m_GameRecordLog.iLoginBlock = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_BLOCK];
	m_GameRecordLog.iLoginSteal = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_STEAL];
	m_GameRecordLog.iLoginWinPoint = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_WINPOINT];
	m_GameRecordLog.iLoginWinPoint1on1 = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_WINPOINT1ON1];
	m_GameRecordLog.iLoginPlaytime = pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[TOTAL_RECORD_PLAYTIME];
}

void CFSGameUser::InitPaperList(vector<SPaper*>* v)
{
	m_paperList.Reset();
	vector<SPaper*>::iterator i;
	SPaper* paper = NULL;
	
	for(i = v->begin() ; i != v->end() ; i++)
	{
		paper = *i;		
		if(paper) m_paperList.Add(paper);
	}
	v->erase(v->begin(), v->end());
}

void CFSGameUser::UpdatePaperList(vector<SPaper*>* v)
{
	vector<SPaper*>::iterator i;
	SPaper* paper = NULL;
	int iPaperListTail =  m_paperList.GetSize();
	for(i = v->begin() ; i != v->end() ; i++)
	{
		paper = *i;		
		
		if(paper && m_paperList.Find(paper->GetIndex()) == NULL)
		{
			m_paperList.Add(paper);
			SendPaper(paper);
		}
	}
	v->erase(v->begin(), v->end());
}

void CFSGameUser::SendPaper(SPaper* paper)
{
	CPacketComposer PacketComposer(S2C_RECV_PAPER);
	PacketComposer.Add(paper->GetIndex());
	PacketComposer.Add((BYTE*)paper->szSendID, MAX_GAMEID_LENGTH+1);
	PacketComposer.Add((BYTE*)paper->szTitle, MAX_PAPER_TITLE_LENGTH+1);
	PacketComposer.Add((DWORD)paper->GetDate());	
	
	Send(&PacketComposer);
}

BOOL CFSGameUser::SendPaperList()
{
	BYTE cnt = m_paperList.GetSize();
	if(cnt < 0) return TRUE;
	
	SPaper* paper = NULL;
	
	CPacketComposer PacketComposer(S2C_PAPER_LIST);
	PacketComposer.Add(cnt);
	
	paper = m_paperList.FindFirst();
	for(int i = 0 ; i < cnt ; i++)
	{				
		ASSERT(paper);
		if(NULL == paper) return FALSE;
		
		PacketComposer.Add(paper->GetIndex());	//	BYTE 	index;	
		PacketComposer.Add((BYTE)paper->bIsCheck);		//	BYTE 	isCheck; 
		PacketComposer.Add((BYTE*)paper->szSendID, MAX_GAMEID_LENGTH+1);	//	char 	sendID[15];	
		PacketComposer.Add((BYTE*)paper->szTitle, MAX_PAPER_TITLE_LENGTH+1);//	char	title[21]; 
		PacketComposer.Add((DWORD)paper->GetDate());	
		PacketComposer.Add((BYTE)paper->bFSOfficial);		//	BYTE 	bFSOfficial; 	
		
		paper = m_paperList.FindNext();
	}
	
	Send(&PacketComposer);
	return TRUE;
}

void CFSGameUser::SendPaperInfo(int index)
{
	CFSGameODBC* pDb = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pDb );

	SAvatarInfo *pAvatar = GetCurUsedAvatar();
	if( pAvatar == NULL )
	{
		return;
	}
	
	SPaper* paper = m_paperList.Find(index);
	if(!paper) return;
	
	short	textLen = strlen(paper->pText);    // ���� ����
	if(paper->bIsCheck != 1)
	{
		paper->bIsCheck = 1;		// ���� ���� ��û�� �� Ȯ���̴�
		pDb->spUpdatePaper(pAvatar->iGameIDIndex, index, paper->bIsCheck);	// ���ϰ� üũ ����
	}
	
	CPacketComposer PacketComposer(S2C_PAPER_INFO_RES);	
	PacketComposer.Add(paper->GetIndex());
	PacketComposer.Add(textLen);
	PacketComposer.Add((BYTE*)paper->pText, textLen);//MAX_PAPER_TEXT_LENGTH+1);			
	
	Send(&PacketComposer);
}

BOOL CFSGameUser::SendPresentList(int iMailType)
{
	CHECK_CONDITION_RETURN(iMailType != MAIL_PRESENT_ON_AVATAR && 
		iMailType != MAIL_PRESENT_ON_ACCOUNT, FALSE);

	if(MAIL_PRESENT_ON_ACCOUNT == iMailType)
		GetUserFreeStyleMatchEvent()->RecvPresent();

	CBaseMailList<SPresent>* pPresentList = NULL;
	BYTE btIsValid = 0;
	DWORD dwRemainTime = 0;
	time_t tCurrentTime = _time64(NULL);

	pPresentList = GetPresentList(iMailType);
		
	int cnt = pPresentList->GetSize();
	if(cnt < 0) return TRUE;
	SPresent* present = NULL;
	CPacketComposer PacketComposer(S2C_PRESENT_LIST);
	PacketComposer.Add(OPERATOR_CODE_INITIAL);
	PacketComposer.Add((BYTE)iMailType);

	Send(&PacketComposer, FALSE);
	
	present = pPresentList->FindFirst();
	
	int iPageNumber = cnt / MAX_SEND_COUNT_IN_PACKET;
	for(int i =0; i <= iPageNumber ; i++)
	{
		PacketComposer.Initialize(S2C_PRESENT_LIST);
		PacketComposer.Add(OPERATOR_CODE_DATA_SENDING);
		
		BYTE btSendCount = 0;
		if(i < iPageNumber)
		{
			btSendCount = MAX_SEND_COUNT_IN_PACKET;
		}
		else
		{
			btSendCount = cnt MAX_SEND_COUNT_IN_PACKET;

		}
		
		PacketComposer.Add(btSendCount);
		
		for(int j = 0 ; j < btSendCount ; j++)
		{				
			ASSERT(present);
			if(NULL == present) return FALSE;
				
			btIsValid = PRESENT_INFO_ISVALID_STATUS_NOT_VALID;
			dwRemainTime = 0;
			if ( present->tExpireDate > present->tSendTime + 5*60 )
			{
				btIsValid = PRESENT_INFO_ISVALID_STATUS_IS_VALID;
				dwRemainTime = present->tExpireDate - tCurrentTime;
				if ( present->tExpireDate <= tCurrentTime )
				{
					btIsValid = PRESENT_INFO_ISVALID_STATUS_OUT_VALID;
					dwRemainTime = 0;
				}
			}
			
			PacketComposer.Add(present->GetIndex());		//	BYTE 	index;	
			PacketComposer.Add((BYTE)present->bIsCheck);		//	BYTE 	isCheck; 
			PacketComposer.Add((BYTE*)present->szSendID, MAX_GAMEID_LENGTH+1);		//	char 	sendID[15];
			PacketComposer.Add((BYTE*)present->szTitle, MAX_PRESENT_TITLE_LENGTH+1);	//	char	title[21]; 
			PacketComposer.Add((DWORD)present->GetDate());	
			PacketComposer.Add((BYTE)btIsValid);
			PacketComposer.Add( dwRemainTime );
			
			present = pPresentList->FindNext();
		}
		Send(&PacketComposer, FALSE);
	}
	
	PacketComposer.Initialize(S2C_PRESENT_LIST);
	PacketComposer.Add(OPERATOR_CODE_END);
	
	Send(&PacketComposer);
	
	return TRUE;
}

void CFSGameUser::SendPresentInfo(int iMailType, int index)
{
	SPresent* present = GetPresentList(iMailType)->Find(index);
	if(!present) return;
	
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if( NULL == pServer ) return;
	CFSItemShop* pItemShop = pServer->GetItemShop();
	if( NULL == pItemShop ) return;

	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return ;
	
	SShopItemInfo ItemInfo;
	if(present->iItemCode != POINT_ITEMCODE && 
		present->iItemCode != TROPHY_ITEMCODE && 
		present->iItemCode != EVENTCASH_ITEMCODE && 
		FALSE == pItemShop->GetItem(present->iItemCode, ItemInfo)) return;
	short	textLen = strlen(present->pText)+1;
	
	CPacketComposer PacketComposer(S2C_PRESENT_INFO_RES);	
	PacketComposer.Add((BYTE)iMailType);
	PacketComposer.Add(present->GetIndex());
	PacketComposer.Add(textLen);		
	PacketComposer.Add((BYTE*)present->pText, textLen);			
	
	PacketComposer.Add((int)present->iItemCode);
	PacketComposer.Add((int)ItemInfo.iLvCondition);
	
	PacketComposer.Add((int)ItemInfo.iFirstRankCondition);
	PacketComposer.Add((int)ItemInfo.iLastRankCondition);
	
	ITEM_PROPERTY_VECTOR vecProperty;
	pItemShop->GetItemPropertyList(present->iaPropertyIndex, vecProperty);
	int iUserItemPropertyNum = vecProperty.size();
	int iPropertyKind = ItemInfo.iPropertyKind;
	int iPropertyType = ItemInfo.iPropertyType;
	int iPropertyTypeValue = present->iPropertyValue;

	if ( ItemInfo.iPropertyKind >= MIN_BONUS_EXP_PROPERTY_NUM && ItemInfo.iPropertyKind <= MAX_BONUS_EXP_PROPERTY_NUM )
	{
		iPropertyKind = 0;

		if( ItemInfo.iPropertyKind >= MIN_HEART_TATTO_KIND_NUM && ItemInfo.iPropertyKind <= MAX_HEART_TATTO_KIND_NUM )
			iPropertyKind = ItemInfo.iPropertyKind;
	}
	else if (	ITEM_PROPERTY_KIND_HYBRID_STAT == ItemInfo.iPropertyKind ||
				(ItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && ItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX) ||
				(ItemInfo.iPropertyKind >= BUFF_ITEM_KIND_EXP && ItemInfo.iPropertyKind <= BUFF_ITEM_KIND_STATUS))
	{
		iPropertyKind = ItemInfo.iPropertyKind;
	}
	else if (ItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_SET_1 ||
		ItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_SET_FOOTWEAR_1 ||
		ItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_SET_FOOTWEAR_2 ||
		ItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_SET_2)
	{
		iPropertyKind = ITEM_PROPERTY_KIND_HYBRID_STAT;
	}
	else
	{
		iPropertyKind = iUserItemPropertyNum;
	}

	if(ItemInfo.iPropertyKind >= BUFF_ITEM_KIND_EXP && ItemInfo.iPropertyKind <= BUFF_ITEM_KIND_STATUS)
	{
		iPropertyType = ITEM_PROPERTY_EXHAUST;
		iPropertyTypeValue = present->iPresentParam;
	}
	else if(ItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_DATEVALUE_MIN && ItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_DATEVALUE_MAX)
	{
		iPropertyTypeValue = -1;
	}

	if (POINT_ITEMCODE == present->iItemCode || 
		TROPHY_ITEMCODE == present->iItemCode ||
		EVENTCASH_ITEMCODE == present->iItemCode)
	{
		iPropertyType = ITEM_PROPERTY_NORMAL;
		iPropertyTypeValue = present->iPresentParam;
	}

	PacketComposer.Add(iPropertyKind);
	PacketComposer.Add(iUserItemPropertyNum);
	PacketComposer.Add(iPropertyType);
	PacketComposer.Add(iPropertyTypeValue);

	for (int iPropertyCount = 0; iPropertyCount < vecProperty.size() ; iPropertyCount++ )
	{
		int iProperty	= vecProperty[iPropertyCount].iProperty;
		int iValue		= vecProperty[iPropertyCount].iValue;
		
		PacketComposer.Add(&iProperty);
		PacketComposer.Add(&iValue);
	}
	
	BYTE btIsValid = 0;
	time_t tCurrentTime = _time64(NULL);
	DWORD dwRemainTime = PRESENT_INFO_ISVALID_STATUS_NOT_VALID;
	if ( present->tExpireDate > present->tSendTime + 5*60 )
	{
		if ( present->tExpireDate <= tCurrentTime )
		{
			btIsValid = PRESENT_INFO_ISVALID_STATUS_OUT_VALID;
		}
		else
		{
			btIsValid = PRESENT_INFO_ISVALID_STATUS_IS_VALID;
			dwRemainTime = present->tExpireDate - tCurrentTime;
		}
	}

	PacketComposer.Add((BYTE)btIsValid);
	PacketComposer.Add( dwRemainTime );
	Send(&PacketComposer);
}

BOOL CFSGameUser::IsChoosePropertyItem(int iMailType, int index)
{
	SPresent* present = GetPresentList(iMailType)->Find(index);
	CHECK_NULL_POINTER_BOOL(present);

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo shopItem;		
	if(FALSE == pItemShop->GetItem(present->iItemCode, shopItem)) 
		return FALSE;

	if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_LV_UP ||
		shopItem.iPropertyKind == ITEM_PROPERTY_KIND_MOVE_POTENCARD ||
		shopItem.iPropertyKind == CHANGE_NAME_SPECIAL_KIND_NUM ||
		shopItem.iPropertyKind == ITEM_PROPERTY_KIND_EXP_BOX)
	{
		return TRUE;
	}

	if(SShopItemInfo::IsChoosePropertyPremiumItem(shopItem.iPropertyKind) == FALSE &&
		SShopItemInfo::IsTwelveAccItem(shopItem.iPropertyKind) == FALSE &&
		FALSE == present->bIsSelectableProperty)
	{
		return FALSE;
	}

	if(CHARACTER_SLOT_TYPE_NORMAL <= shopItem.iCharacterSlotType ||
		shopItem.iPropertyKind == SKILL_SLOT_KIND_NUM ||
		(shopItem.iPropertyKind > BIND_ACCOUNT_ITEM_KIND_START && shopItem.iPropertyKind < BIND_ACCOUNT_ITEM_KIND_END))
	{
		return FALSE;
	}

	int iPropertyKind = shopItem.iPropertyKind;
	if(present->iPropertyKind > -1)
	{
		iPropertyKind = present->iPropertyKind;
	}

	CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(iPropertyKind);
	if(pItemPropertyBoxList != NULL && 
		(iPropertyKind < MIN_EXP_UP_ITEM_KIND_NUM || iPropertyKind > MAX_EXP_UP_ITEM_KIND_NUM) &&
		(iPropertyKind < MIN_LEVEL_UP_ITEM_KIND_NUM || iPropertyKind > MAX_LEVEL_UP_ITEM_KIND_NUM))
	{
		int iPropertyIndex = pItemPropertyBoxList->GetPropertyIndexByLv(GetCurUsedAvtarLv());
		CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList(iPropertyIndex);
		if(pItemPropertyList != NULL)
		{
			return TRUE;
		}
	}

	if(SShopItemInfo::IsTwelveAccItem(shopItem.iPropertyKind) == TRUE)
		return TRUE; 

	if(shopItem.iItemCode1 > 0 || shopItem.iItemCode2 > 0 || shopItem.iItemCode3 > 0 || shopItem.iItemCode4 > 0)
	{
		return TRUE; 
	}

	return FALSE;
}

void CFSGameUser::SendChoosePropertyItemInfoPresent(int iMailType, int iPresentIndex)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SPresent* present = GetPresentList(iMailType)->Find(iPresentIndex);
	CHECK_NULL_POINTER_VOID(present);

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	SShopItemInfo shopItem;		
	if(FALSE == pItemShop->GetItem(present->iItemCode, shopItem)) 
		return;

	int iPropertyTypeValue = present->iPropertyValue;

	CPacketComposer PacketComposer(S2C_ACCEPT_PRESENT_RESULT);
	int iErrorCode = ACCEPT_PRESENT_POPUP_CHOOSE_ITEM_PROPERTY;

	if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_LV_UP)
	{
		iErrorCode = ACCEPT_PRESENT_POPUP_USE_LVUPITEM;
		if(TRUE == IsJumpingPlayerAvatar())
		{
			iErrorCode = ACCEPT_PRESENT_NOT_USE_EVENTCHAR;
		}

		if(FALSE == LVUPEVENT.CheckUseRewardItem(shopItem.iItemCode0, GetCurUsedAvtarLv()))
		{
			iErrorCode = ACCEPT_PRESENT_NOT_USE_LV_CONDITION;
		}
	}
	else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_EXP_BOX)
	{
		if(pAvatarInfo->iLv == GAME_LVUP_MAX)
		{
			iErrorCode = ACCEPT_PRESENT_NOT_USE_EXPBOX_LV_CONDITION;
		}
	}
	else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_MOVE_POTENCARD )
	{
		iErrorCode = ACCEPT_PRESENT_POPUP_MOVE_POTENCARD;
	}
	else if(shopItem.iPropertyKind == CHANGE_NAME_SPECIAL_KIND_NUM )
	{
		iErrorCode = ACCEPT_PRESENT_POPUP_MOVE_SPECIALNAME_CHANGE;
	}

	time_t tCurrentTime = _time64(NULL);
	if ( present->tExpireDate > present->tSendTime + 5*60 && present->tExpireDate <= tCurrentTime)
	{
		iErrorCode = ACCEPT_PRESENT_OUT_VALID;
	}

	if(pAvatarInfo->iLv < shopItem.iLvCondition)
		iErrorCode = ACCEPT_PRESENT_LV_CONDITION;

	if(shopItem.iSexCondition != ITEM_SEXCONDITION_UNISEX && shopItem.iSexCondition != pAvatarInfo->iSex)
		iErrorCode = ACCEPT_PRESENT_SEX_CONDITION;

	if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_LV_UP ||
		shopItem.iPropertyKind == ITEM_PROPERTY_KIND_MOVE_POTENCARD ||
		shopItem.iPropertyKind == CHANGE_NAME_SPECIAL_KIND_NUM)
	{
		PacketComposer.Add(iErrorCode);
		PacketComposer.Add((BYTE)iMailType);
		PacketComposer.Add(iPresentIndex);
		PacketComposer.Add(shopItem.iItemCode0);
		Send(&PacketComposer);
		return;
	}
	else if(ACCEPT_PRESENT_POPUP_CHOOSE_ITEM_PROPERTY != iErrorCode)
	{
		const int iTempCnt = 0;
		PacketComposer.Add(iErrorCode);
		PacketComposer.Add((BYTE)iMailType);
		PacketComposer.Add(iTempCnt);
		Send(&PacketComposer);
		return;
	}

	PBYTE pLocationMultiStatType = PacketComposer.GetTail();
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add((BYTE)iMailType);
	PacketComposer.Add(iPresentIndex);
	PacketComposer.Add(shopItem.iItemCode0);
	PacketComposer.Add(shopItem.iItemCode1);
	PacketComposer.Add(shopItem.iItemCode2);
	PacketComposer.Add(shopItem.iItemCode3);
	PacketComposer.Add(shopItem.iItemCode4);
	PacketComposer.Add(shopItem.iPropertyType);
	PacketComposer.Add(iPropertyTypeValue);

	if(ITEM_PROPERTY_KIND_EXP_BOX == shopItem.iPropertyKind)
	{
		int iSendPropertyKind	= 1;
		BYTE btListPropertyAssignSize = 1;
		BYTE btPropertyAssignType = 0;
		int iMaxBoxIdx = 1;
		int iAssignChannel = -1;
		int iMaxCategoryCount = 1;
		short sItemPropertyBoxSize = 1;
		short sPriceType = 0;
		int iMaxPropertyNum = 1;
		int iSendPropertyIndex	= -1;
		short sSendCategory		= 0;
		short sSendProperty		= 50;
		int iSendValue			= present->iPropertyValue;
		int iSendOrder			= 0;

		PacketComposer.Add(iSendPropertyKind);
		PacketComposer.Add(btListPropertyAssignSize);
		PacketComposer.Add(btPropertyAssignType);
		PacketComposer.Add(iMaxBoxIdx);
		PacketComposer.Add(&iAssignChannel);
		PacketComposer.Add(&iMaxCategoryCount);
		PacketComposer.Add(sItemPropertyBoxSize);
		PacketComposer.Add(sPriceType);
		PacketComposer.Add(iMaxPropertyNum);
		PacketComposer.Add(iSendPropertyIndex);
		PacketComposer.Add(sSendCategory);
		PacketComposer.Add(sSendProperty);
		PacketComposer.Add(iSendValue);
		PacketComposer.Add(iSendOrder);
		Send(&PacketComposer);
		return;
	}
	
	int iPropertyKind		= shopItem.iPropertyKind;
	if(present->iPropertyKind > -1)
	{
		iPropertyKind = present->iPropertyKind;
	}
	int iSendPropertyKind	= -1;
	int iLv = shopItem.iLvCondition;

	CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(iPropertyKind);

	if(pItemPropertyBoxList != NULL && 
		(iPropertyKind < MIN_EXP_UP_ITEM_KIND_NUM || iPropertyKind > MAX_EXP_UP_ITEM_KIND_NUM) &&
		(iPropertyKind < MIN_LEVEL_UP_ITEM_KIND_NUM || iPropertyKind > MAX_LEVEL_UP_ITEM_KIND_NUM) &&
		(iPropertyKind < ITEM_PROPERTY_KIND_SALE_COUPON_MIN || iPropertyKind > ITEM_PROPERTY_KIND_SALE_COUPON_MAX))
	{
		CItemPropertyPriceList *pPropertyPriceList = pItemShop->GetItemPropertyPriceList(iPropertyKind);
		CHECK_NULL_POINTER_VOID(pPropertyPriceList);

		if((iPropertyKind >= MIN_BONUS_EXP_PROPERTY_NUM && iPropertyKind <= MAX_BONUS_EXP_PROPERTY_NUM) ||
			(iPropertyKind >= MIN_WIN_TATTOO_KIND_NUM && iPropertyKind <= MAX_WIN_TATTOO_KIND_NUM))	
		{
			iSendPropertyKind = 0;

			if( iPropertyKind >= MIN_HEART_TATTO_KIND_NUM && iPropertyKind <= MAX_HEART_TATTO_KIND_NUM )
				iSendPropertyKind = shopItem.iPropertyKind;
		}
		else
			iSendPropertyKind = 1; 
		
		PacketComposer.Add(iSendPropertyKind);

		list<int>* pListPropertyAssignType = pPropertyPriceList->GetPropertyAssignTypeList();
		CHECK_NULL_POINTER_VOID(pListPropertyAssignType);

		PacketComposer.Add((BYTE)pListPropertyAssignType->size());

		if (1 < pListPropertyAssignType->size())	//1�̻��̸� �ɷ�ġ 2 ���� (����/��Ʈ ����) �ӵ�
		{
			iErrorCode = ACCEPT_PRESENT_POPUP_MULTISTAT;
			memcpy(pLocationMultiStatType, &iErrorCode, sizeof(iErrorCode));
		}

		int iUserLv = GetCurUsedAvtarLv();

		list<int>::iterator		iter = pListPropertyAssignType->begin();
		list<int>::iterator		endtier = pListPropertyAssignType->end();
		for( ; iter != endtier; ++iter )
		{
			int iPropertyAssignType = *iter;
			PacketComposer.Add((BYTE)iPropertyAssignType);
			int iMaxBoxIdx = pItemPropertyBoxList->GetMaxBoxIdxCount( iUserLv, iPropertyAssignType);

			if(iPropertyKind == PREMIUM_ITEM_KIND_SUPER_ROOKIE)
			{
				SUserItemInfo* pCheckItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iPropertyKind);
				if(pCheckItem)
				{
					iMaxBoxIdx = 0;
				}
			}

			PacketComposer.Add(iMaxBoxIdx);

			for(int iBoxIdx = 0; iBoxIdx < iMaxBoxIdx ; iBoxIdx++) 
			{
				int iMaxCategoryCount = pItemPropertyBoxList->GetMaxCategoryCount(iBoxIdx, iUserLv, iPropertyAssignType );
				vector< SItemPropertyBox > vItemPropertyBox;
				short sSmallMaxNum = (short)pItemPropertyBoxList->GetItemPropertyBoxIdxCount(iBoxIdx, iUserLv, iPropertyAssignType); 
				pItemPropertyBoxList->GetItemPropertyBox( iBoxIdx, iUserLv, iPropertyAssignType, vItemPropertyBox );
				sort( vItemPropertyBox.begin(), vItemPropertyBox.end(), PropertyBoxSort() );

				int iAssignChannel = 0;
				if (!vItemPropertyBox.empty())	iAssignChannel = vItemPropertyBox[0].iAssignChannel;
				PacketComposer.Add(&iAssignChannel);
				PacketComposer.Add(&iMaxCategoryCount);
				short sVectorSize = (short)vItemPropertyBox.size();
				PacketComposer.Add(sVectorSize);

				for(int i = 0; i < vItemPropertyBox.size(); i++)
				{
					int iPropertyIndex = vItemPropertyBox[i].iPropertyIndex;
					CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList(iPropertyIndex);
					CHECK_NULL_POINTER_VOID(pItemProperty);
					int iMaxPropertyNum = pItemProperty->GetPropertyMaxCount();
					SItemPropertyBox* pItemPropertyBox = pItemPropertyBoxList->GetItemPropertyBox(vItemPropertyBox[i].iCategory, iBoxIdx, iUserLv, iPropertyAssignType);
					CHECK_NULL_POINTER_VOID(pItemPropertyBox);

					short sPriceType			= (short)( pItemPropertyBox->iPriceType - 1);
					PacketComposer.Add((PBYTE)&sPriceType, sizeof(short));

					PacketComposer.Add(iMaxPropertyNum);

					for(int iPropertyCount = 0; iPropertyCount < iMaxPropertyNum; iPropertyCount++)
					{
						vector< SItemProperty > vItemProperty;
						pItemProperty->GetItemProperty( vItemProperty );
						sort( vItemProperty.begin(), vItemProperty.end(), PropertySort() );

						int iSendPropertyIndex	= vItemProperty[iPropertyCount].iPropertyIndex;
						short sSendCategory		= (short)vItemPropertyBox[i].iCategory;
						short sSendProperty		= (short)vItemProperty[iPropertyCount].iProperty;
						int iSendValue			= vItemProperty[iPropertyCount].iValue;
						int iSendOrder			= vItemProperty[iPropertyCount].iOrder;

						PacketComposer.Add(iSendPropertyIndex);
						PacketComposer.Add(sSendCategory);
						PacketComposer.Add(sSendProperty);
						PacketComposer.Add(iSendValue);
						PacketComposer.Add(iSendOrder);
					}
				}
			}
		}
	}
	else
	{
		PacketComposer.Add( iSendPropertyKind );
	}

	Send(&PacketComposer);
}

BOOL CFSGameUser::AcceptPresent(int iMailType, int iIndexCnt, int iIndex, BYTE btPopUpIndex, int &iErrorCode)
{
	CFSGameODBC *pDb = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL(pDb);

	iErrorCode = ACCEPT_PRESENT_FAIL;
	
	SPresent* present = GetPresentList(iMailType)->Find(iIndex);
	CHECK_NULL_POINTER_BOOL(present);
	
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);
	
	CHECK_CONDITION_RETURN(present->bIsCheck == 1, FALSE);
		
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);
		
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	SShopItemInfo shopItem;		
	if (present->iItemCode != POINT_ITEMCODE && 
		present->iItemCode != TROPHY_ITEMCODE &&
		present->iItemCode != EVENTCASH_ITEMCODE)
	{
		CHECK_CONDITION_RETURN(FALSE == pItemShop->GetItem(present->iItemCode, shopItem), FALSE);

		if (false == shopItem.IsLvCondition(pAvatarInfo->iLv))
		{
			iErrorCode = ACCEPT_PRESENT_LV_CONDITION;
			return FALSE;
		}
			
		if(shopItem.iSexCondition != ITEM_SEXCONDITION_UNISEX &&
			shopItem.iSexCondition != pAvatarInfo->iSex)
		{
			iErrorCode = ACCEPT_PRESENT_SEX_CONDITION;
			return FALSE;
		}
	}		

	if(shopItem.iPropertyKind == WONDER_GIRLS_CHARACTER_SLOT_KIND_NUM ||
		shopItem.iPropertyKind == WONDER_GIRLS_CHARACTER_SLOT_KIND_NUM_EXPERT ||
		shopItem.iPropertyKind == WONDER_GIRLS_CHARACTER_SLOT_KIND_NUM_SET)
	{
		iErrorCode = ACCEPT_PRESENT_WONDER_GIRLS_CHARACTER_SLOT;
		return FALSE;
	}

	time_t tCurrentTime = _time64(NULL);
	if ( present->tExpireDate > present->tSendTime + 5*60 && present->tExpireDate <= tCurrentTime)
	{
		iErrorCode = ACCEPT_PRESENT_OUT_VALID;
		return FALSE;
	}

	if( ITEM_PROPERTY_KIND_CLONE_CHARACTER_ITEM == shopItem.iPropertyKind )
	{
		if(iIndexCnt <= 1)
		{
			int iTargetSpecialIndex = 0;
			if( TRUE == AVATARCREATEMANAGER.CheckCloneAvailableCharacter(pAvatarInfo->iSpecialCharacterIndex, iTargetSpecialIndex) )
			{
				if( FALSE == GetCloneCharacter() )
				{
					int iGameIDIndex = MAIL_PRESENT_ON_ACCOUNT == iMailType ? -1 : GetGameIDIndex();
					int iItemFace = 9;	

					CPacketComposer PacketComposer(S2C_CHOOSE_SPECIAL_CHARACTER_FROM_PRESENT);	
					PacketComposer.Add(iItemFace);
					PacketComposer.Add(shopItem.iPropertyKind);	// ITEM_PROPERTY_KIND_CLONE_CHARACTER_ITEM

					short sGroupIndex = AVATARCREATEMANAGER.GetGroupIndexByPropertyKind(shopItem.iPropertyKind);
					int iCreateLv = AVATARCREATEMANAGER.GetSlotCreateLevel(sGroupIndex, shopItem.iPropertyKind);
					PacketComposer.Add(sGroupIndex);
					PacketComposer.Add(iCreateLv);

					PacketComposer.Add(present->index);
					PacketComposer.Add(iGameIDIndex);
					Send(&PacketComposer);
				}
				else
				{
					iErrorCode = ACCEPT_PRESENT_CLONE_CHAR_ITEM_HAVE_ALREADY;
					return FALSE;
				}
			}
			else
			{
				iErrorCode = ACCEPT_PRESENT_CLONE_CHAR_ITEM_NOT_APPLICABLE_CHAR;
				return FALSE;
			}

			iErrorCode = ACCEPT_PRESENT_CHARACTER_SLOT;
			return TRUE;
		}
			
		return FALSE;
	}
	else if (  CHARACTER_SLOT_TYPE_NORMAL <= shopItem.iCharacterSlotType )
	{
		if(iIndexCnt <= 1)
		{
			int iResult = 0;
			//���� ������ ���� GameIDIndex�� -1�� �ؾ� ��������
			int iGameIDIndex = MAIL_PRESENT_ON_ACCOUNT == iMailType ? -1 : GetGameIDIndex();

			if ( CHARACTER_SLOT_TYPE_SPECIAL == shopItem.iCharacterSlotType ||
				CHARACTER_SLOT_TYPE_CHANGE_AVATAR == shopItem.iCharacterSlotType)
			{					
				int iItemFace = 9;	

				int iPropertyKind = shopItem.iPropertyKind;

				CPacketComposer PacketComposer(S2C_CHOOSE_SPECIAL_CHARACTER_FROM_PRESENT);									
				PacketComposer.Add(iItemFace);
				PacketComposer.Add(iPropertyKind);

				short sGroupIndex = AVATARCREATEMANAGER.GetGroupIndexByPropertyKind(shopItem.iPropertyKind);
				int iCreateLv = AVATARCREATEMANAGER.GetSlotCreateLevel(sGroupIndex, shopItem.iPropertyKind);
				PacketComposer.Add(sGroupIndex);
				PacketComposer.Add(iCreateLv);

				PacketComposer.Add(present->GetIndex());
				PacketComposer.Add(iGameIDIndex);
				Send(&PacketComposer);															
			}														
			else
			{																						
				if( shopItem.iPropertyKind == EXTRA_CHARACTER_SLOT_KIND_NUM || shopItem.iPropertyKind == EXTRA_CHARACTER_SLOT_POINT_KIND_NUM)
					iResult = 1;
				else
					iResult = 0;
				
				CPacketComposer PacketComposer(S2C_GIFT_GENERAL_SLOTITEM_RES);					
				PacketComposer.Add(iResult);
				PacketComposer.Add(present->iItemCode);
				PacketComposer.Add((int)(present->GetIndex()));
				PacketComposer.Add(iGameIDIndex);
				Send(&PacketComposer);															
			}	

			iErrorCode = ACCEPT_PRESENT_CHARACTER_SLOT;
			return TRUE;
		}

		return FALSE;
	}
	else if( shopItem.iPropertyKind == SKILL_SLOT_KIND_NUM )
	{
		SAvatarInfo* pAvatar = GetCurUsedAvatar();
		CHECK_NULL_POINTER_BOOL(pAvatar);
						
		if(0 == btPopUpIndex &&
			-1 == present->iPropertyValue)
		{			
			if(TRUE == CheckSkillSlotExchangeStatus(pAvatar->Skill.SkillSlot, present->iItemCode))
			{
				iErrorCode = ACCEPT_PRESENT_FULL_SKILLSLOT_NOTICE_POPUP;
				return FALSE;
			}
		}

		BOOL bExchange = FALSE;
		int iMaxSkillSlot = MAX_SKILL_SLOT_OTHERS;
		if( GetSkillSlotCount(pAvatar->Skill.SkillSlot, present->iItemCode) > iMaxSkillSlot )
		{
			if(-1 != present->iPropertyValue)
			{
				iErrorCode = ACCEPT_PRESENT_OVER_SKILL_SLOT_COUNT;
				return FALSE;
			}
			else
			{
				if(FALSE == CheckSkillSlotExchangeStatus(pAvatar->Skill.SkillSlot, present->iItemCode))
				{
					iErrorCode = ACCEPT_PRESENT_OVER_SKILL_SLOT_COUNT;
					return FALSE;
				}

				bExchange = TRUE;
			}		
		}
			
		if( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptSkillSlot( m_iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, GetCurUsedAvatar()->Skill, bExchange ))
		{
			return FALSE;
		}		
	}
	else if(shopItem.iPropertyKind > BIND_ACCOUNT_ITEM_KIND_START && shopItem.iPropertyKind < BIND_ACCOUNT_ITEM_KIND_END)
	{
		// �ٸ� ������ VIP �������̶�� ���� 
		if(TRUE == CheckHaveBindAccountItem(shopItem.iPropertyKind))
		{
			iErrorCode = ACCEPT_PRESENT_NOT_HAVE_VIP_KIND;
			return FALSE;
		}

		SAcceptBindOutputItem AcceptBindOutputItem;
		if(ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptBindAccountItem(GetCurUsedAvatar()->iUserIDIndex,GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, AcceptBindOutputItem))
		{
			return FALSE;
		}

		SBindAccountData BindAccountData;
		BindAccountData.iBindAccountItemPropertyKind = shopItem.iPropertyKind;
		BindAccountData.ExpireDate = AcceptBindOutputItem.ExpireDateInfo;
		SetBindAccountData(BindAccountData);
		SendBindAccountInfo();

		for(int i = 0; i < MAX_EVENT_PRESENT_SIZE; i++)
		{
			if(AcceptBindOutputItem.iItemIdx[i] > 0)
			{
				RecvPresent(MAIL_PRESENT_ON_AVATAR, AcceptBindOutputItem.iItemIdx[i]);
			}
		}

		if(AcceptBindOutputItem.iExtraBonusPoint > 0)
		{
			SetSkillPoint(GetSkillPoint() + AcceptBindOutputItem.iExtraBonusPoint);
			int iTicketcount = GetUserItemList()->GetSkillTicketCount();
			SendUserGold(iTicketcount);
			AddPointAchievements(); 
		}
	}
	else if(present->iItemCode == POINT_ITEMCODE)
	{
		int iAmount = 0;
		if(ODBC_RETURN_SUCCESS != pDb->PRESENT_AcceptPoint(GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iAmount))
		{
			return FALSE;
		}

		AddSkillPoint(iAmount);
		int iTicketcount = GetUserItemList()->GetSkillTicketCount();
		SendUserGold(iTicketcount);
	}
	else if(present->iItemCode == TROPHY_ITEMCODE)
	{
		int iAmount = 0;
		if(ODBC_RETURN_SUCCESS != pDb->PRESENT_AcceptTrophy(GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iAmount))
		{
			return FALSE;
		}

		SetUserTrophy(GetUserTrophy() + iAmount);
		SendCurAvatarTrophy();
	}
	else if(present->iItemCode == EVENTCASH_ITEMCODE)
	{
		int iAmount = 0;
		if(ODBC_RETURN_SUCCESS != pDb->PRESENT_AcceptEventCoin(GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iAmount))
		{
			return FALSE;
		}

		SetEventCoin(GetEventCoin() + iAmount, TRUE);
		SendUserGold();
	}
	else if ( shopItem.iPropertyKind >= ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MIN && shopItem.iPropertyKind <= ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MAX )
	{
		int iUpdatedPropertyValue = 0;
		if ( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptHighFrequencyItem( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iUpdatedPropertyValue) )
		{
			return FALSE;
		}

		CUserHighFrequencyItem* pHighFrequencyItem = GetUserHighFrequencyItem();
		if ( pHighFrequencyItem != NULL )
		{
			SUserHighFrequencyItem sUserHighFrequencyItem;
			sUserHighFrequencyItem.iPropertyKind = shopItem.iPropertyKind;
			sUserHighFrequencyItem.iPropertyTypeValue = iUpdatedPropertyValue;
			pHighFrequencyItem->AddUserHighFrequencyItem( sUserHighFrequencyItem );
			SendHighFrequencyItemCount(shopItem.iPropertyKind, TRUE);
		}

		if(ITEM_PROPERTY_KIND_GOLD_POTION == shopItem.iPropertyKind)
		{
			GetUserPotionMakingEvent()->UpdatePotionMakeCount(SEVENT_POTION_TYPE_GOLD, iUpdatedPropertyValue);
		}
	}
	else if((shopItem.iBigKind == ITEM_BIG_KIND_COACHCARD || (shopItem.iSmallKind >= ITEM_SMALL_KIND_COACHCARD_OFFENCE && shopItem.iSmallKind <= ITEM_SMALL_KIND_COACHCARD_ALL))
		|| (ITEM_BIG_KIND_CLUB_PRIMIUM == shopItem.iBigKind && ITEM_SMALL_KIND_CLUB_COACHCARD == shopItem.iSmallKind))
	{
		CItem* pCoachCardItem = COACHCARD.FindItemByShopItemCode(present->iItemCode);
		CHECK_NULL_POINTER_BOOL(pCoachCardItem);
			
		int iItemIDNumber = pCoachCardItem->GetItemIDNumber();
		SDateInfo sExpireDate;
		int iInputProductIndex = INVALID_IDINDEX;
		int iInputInventoryIndex = INVALID_IDINDEX;
		int iAddProductCount = present->iPropertyValue;
		int iNewProductIndex = INVALID_IDINDEX;
		int iNewInventoryIndex = INVALID_IDINDEX;
		int iNewProductCount = 0;
		GetOwnedProductItemInfo(iItemIDNumber, iInputProductIndex, iInputInventoryIndex);
		int iItemType = COACHCARD.GetItemType(iItemIDNumber);
		int iTendencyType = TENDENCY_TYPE_NONE;
		vector<int> vecGradeLevel;
		int iGradeLevel = RANDOMCARD_GRADE_LEVEL_ALL;
		COACHCARD.GetGradeLevel(iItemIDNumber, vecGradeLevel);
		int iSpecialCardIndex = COACHCARD.GetSpecialCardIndex(iItemIDNumber);
		int iRarityLevel = RARITY_LEVEL_NONE;
		SDateInfo	sProductionDate;

		if (1 < vecGradeLevel.size() || 0 == vecGradeLevel.size())	
			iGradeLevel = RANDOMCARD_GRADE_LEVEL_ALL;
		else
			iGradeLevel = vecGradeLevel[0];

		int iResult ;
		iResult = pDb->ITEM_AcceptCoachCard(GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iItemIDNumber
			,sExpireDate, sProductionDate,  iTendencyType, iGradeLevel,  iItemType, iInputProductIndex, iInputInventoryIndex, iAddProductCount
			,iNewProductIndex, iNewInventoryIndex, iNewProductCount, iSpecialCardIndex, iRarityLevel);

		if(ODBC_RETURN_SUCCESS == iResult)
		{
			SProductData ProductData;
			ProductData.iInventoryIndex = iNewInventoryIndex;
			ProductData.iProductIndex = iNewProductIndex;
			ProductData.iItemIDNumber =iItemIDNumber;
			ProductData.iItemType = iItemType;
			ProductData.iTendencyType = iTendencyType;
			ProductData.iGradeLevel = iGradeLevel;
			ProductData.sProductionDate = sProductionDate;
			ProductData.sExpireDate = sExpireDate;
			ProductData.iProductCount = iNewProductCount;
			ProductData.iSpecialCardIndex = iSpecialCardIndex;
			ProductData.iRarityLevel = iRarityLevel;
			AddProductToInventory(ProductData);

			SS2C_RANDOM_COACH_CARD_INFO_NOT info;
			info.iProductIndex = iNewProductIndex;
			info.iProductCount = iNewProductCount;
			info.bThreeKingdom = static_cast<BOOL>(iSpecialCardIndex == SPECIALCARD_INDEX_THREEKINGDOM);
			Send(S2C_RANDOM_COACH_CARD_INFO_NOT, &info, sizeof(SS2C_RANDOM_COACH_CARD_INFO_NOT));
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_COACHCARD, DB_DATA_LOAD, FAIL, "Fail! ITEM_AcceptCoachCard GameIDIndex = 11866902 , ItemIDNumber = 0,  Error = 18227200" , GetCurUsedAvatar()->iGameIDIndex, iItemIDNumber,iResult);
ALSE;
		}
	}
	else if( shopItem.iPropertyKind >= ITEM_PROPERTY_KIND_GUARANTEE_MIN && shopItem.iPropertyKind <= ITEM_PROPERTY_KIND_GUARANTEE_MAX )
	{
		CHECK_CONDITION_RETURN(GUARANTEE_SUCCESS != RANDOMCARDSHOP.GetGuaranteeCardInfo(GetUserIDIndex(), m_sGuaranteeCard), FALSE);
			
		int iGuaranteeCardType = RANDOMCARDSHOP.GetGuaranteeCardTypeWithPropertyKind(shopItem.iPropertyKind);

		int iGuaranteeCardValue = GetGuaranteeCardValue(iGuaranteeCardType+1);
		if( iGuaranteeCardValue > 0 )
		{
			iErrorCode = ACCEPT_PRESENT_GUARANTEE_CARD_EXISTS;
			return FALSE;
		}
		else
		{
			if( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptGuaranteeCard(GetUserIDIndex(), GetGameIDIndex(), iGuaranteeCardType, iMailType, present->GetIndex()) )
			{
				iErrorCode = ACCEPT_PRESENT_GUARANTEE_CARD_EXISTS;
				WRITE_LOG_NEW(LOG_TYPE_PRESENT, DB_DATA_UPDATE, FAIL, "ITEM_AcceptGuaranteeCard - GameIDIndex:11866902\tPresentIndex:0", GetGameIDIndex(), iIndex);
eturn FALSE;
			}
		}
	}
	else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_CHEERLEADER)
	{
		SAvatarCheerLeader AvatarCheerLeader;
		if(FALSE == GetUserCheerLeader()->GetCheerLeader(shopItem.iCheerLeaderIndex, AvatarCheerLeader))
		{
			AvatarCheerLeader.iCheerLeaderIndex = shopItem.iCheerLeaderIndex;
			AvatarCheerLeader.tBuyDate = _time64(NULL);
			AvatarCheerLeader.btTypeNum = 0;
		}

		if (ODBC_RETURN_SUCCESS == pDb->ITEM_AcceptCheerLeader(GetUserIDIndex(), GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, shopItem.iCheerLeaderIndex, AvatarCheerLeader.btTypeNum, AvatarCheerLeader.tExpireDate))
		{
			GetUserCheerLeader()->AddCheerLeader(AvatarCheerLeader);
			SetCheerLeaderCode(AvatarCheerLeader.iCheerLeaderIndex);
			SetCheerLeaderTypeNum(AvatarCheerLeader.btTypeNum);
			SendUserStat();
		}
	}
	else if( shopItem.iPropertyKind >= MIN_ITEM_PROPERTY_KIND_INCOMPLETED_PIECE_TYPE && shopItem.iPropertyKind <= MAX_ITEM_PROPERTY_KIND_INCOMPLETED_PIECE_TYPE )
	{
		BYTE btPieceType = SSP_INCOMPLETED_PIECE_TYPE_NONE;
		switch(shopItem.iPropertyKind)
		{
		case ITEM_PROPERTY_KIND_INCOMPLETED_PIECE_TYPE_NORMAL:
			btPieceType = SSP_INCOMPLETED_PIECE_TYPE_NONE;
			break;
		case ITEM_PROPERTY_KIND_INCOMPLETED_PIECE_TYPE_SKILL_UP:
			btPieceType = SSP_INCOMPLETED_PIECE_TYPE_SKILL_UP_KEY;
			break;
		case ITEM_PROPERTY_KIND_INCOMPLETED_PIECE_TYPE_CREATE_SPECIALAVATAR:
			btPieceType = SSP_INCOMPLETED_PIECE_TYPE_CREATE_SPECIALAVATAR_PIECE;
			break;
		case ITEM_PROPERTY_KIND_INCOMPLETED_PIECE_TYPE_SPECIALAVATAR_ITEM:
			btPieceType = SSP_INCOMPLETED_PIECE_TYPE_SPECIALAVATAR_ITEM_PIECE;
			break;
		default:
			return FALSE;
		}

		int iUpdatedPieceCount = 0;
		if(ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptSpecialPiece(GetUserIDIndex(), GetGameIDIndex(), iMailType, present->GetIndex(), btPieceType, iUpdatedPieceCount))
		{
			iErrorCode = ACCEPT_PRESENT_FAIL;
			WRITE_LOG_NEW(LOG_TYPE_PRESENT, DB_DATA_UPDATE, FAIL, "ITEM_AcceptSpecialPiece - GameIDIndex:11866902\tPresentIndex:0", GetGameIDIndex(), iIndex);
turn FALSE;
		}

		GetUserSpecialPiece()->SetUserIncompletedPiece(btPieceType, iUpdatedPieceCount);
	}
	else if( shopItem.iPropertyKind >= MIN_ITEM_PROPERTY_KIND_PIECE_TYPE && shopItem.iPropertyKind <= MAX_ITEM_PROPERTY_KIND_PIECE_TYPE )
	{
		BYTE btKeyType = SSP_KEY_TYPE_NONE;
		switch(shopItem.iPropertyKind)
		{
		case ITEM_PROPERTY_KIND_PIECE_TYPE_NORMAL:
			btKeyType = SSP_KEY_TYPE_NONE;
			break;
		case ITEM_PROPERTY_KIND_PIECE_TYPE_SKILL_UP:
			btKeyType = SSP_KEY_TYPE_SKILL_UP_BOX;
			break;
		case ITEM_PROPERTY_KIND_PIECE_TYPE_CREATE_SPECIALAVATAR:
			btKeyType = SSP_KEY_TYPE_CREATE_SPECIALAVATAR_BOX;
			break;
		case ITEM_PROPERTY_KIND_PIECE_TYPE_SPECIALAVATAR_ITEM:
			btKeyType = SSP_KEY_TYPE_SPECIALAVATAR_ITEM_BOX;
			break;
		default:
			return FALSE;
		}

		int iUpdatedKeyCount = 0;
		if(ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptSPBoxKey(GetUserIDIndex(), GetGameIDIndex(), iMailType, present->GetIndex(), btKeyType, iUpdatedKeyCount))
		{
			iErrorCode = ACCEPT_PRESENT_FAIL;
			WRITE_LOG_NEW(LOG_TYPE_PRESENT, DB_DATA_UPDATE, FAIL, "ITEM_AcceptSPBoxKey - GameIDIndex:11866902\tPresentIndex:0", GetGameIDIndex(), iIndex);
turn FALSE;
		}

		GetUserSpecialPiece()->SetUserBoxKey(btKeyType, iUpdatedKeyCount);
	}
	else
	{
		if((pAvatarInfo->iSpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX1 ||
			pAvatarInfo->iSpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX3) &&
			(shopItem.iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
		{
			iErrorCode = ACCEPT_PRESENT_NOT_USE_CHAR;
			return FALSE;
		}

		if (ITEM_SEXCONDITION_UNISEX != shopItem.iSexCondition && pAvatarInfo->iSex != shopItem.iSexCondition)
		{
			iErrorCode = ACCEPT_PRESENT_SEX_CONDITION;
			return FALSE;
		}

		SPremiumOutputItem AcceptOutputItem;
		time_t tExpireDate;

		pAvatarItemList->IncreaseEtchKindItemCount(shopItem.iSmallKind);
			
		if(shopItem.iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && shopItem.iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
		{
			int	iCntSupplyItem = 0;
			SUserItemInfo itemList[MAX_ITEMS_IN_PACKAGE];

			if(ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptChangeDressItem(GetCurUsedAvatar()->iUserIDIndex,GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, AcceptOutputItem, iCntSupplyItem, itemList))
			{
				return FALSE;
			}

			for(int i = 0; i < iCntSupplyItem; i++)
			{
				SUserItemInfo* pUserItemInfo = pAvatarItemList->GetItemWithItemIdx(itemList[i].iItemIdx);
				if(pUserItemInfo == NULL)
				{
					SShopItemInfo ShopItemInfo;			
					pItemShop->GetItem(itemList[i].iItemCode, ShopItemInfo);	

					itemList[i].iChannel = ShopItemInfo.iChannel;
					itemList[i].iSexCondition = ShopItemInfo.iSexCondition;		
					itemList[i].iPropertyType = itemList[i].iPropertyType;
					itemList[i].iPropertyTypeValue = itemList[i].iPropertyTypeValue;
					itemList[i].iPropertyKind = itemList[i].iPropertyKind;
					itemList[i].iBigKind = itemList[i].iBigKind;
					itemList[i].iStatus = ITEM_STATUS_INVENTORY;
					itemList[i].iSellType = REFUND_TYPE_POINT;

					pAvatarItemList->AddItem(itemList[i]);
				}
				else
				{
					pUserItemInfo->iPropertyTypeValue = itemList[i].iPropertyTypeValue;
					pUserItemInfo->ExpireDate = itemList[i].ExpireDate;
				}
			}
		}
		else if(shopItem.iPropertyKind >= BUFF_ITEM_KIND_EXP && shopItem.iPropertyKind <= BUFF_ITEM_KIND_STATUS)
		{
			int iItemIndex[MAX_BUFFITEM_CNT];
			memset(iItemIndex, -1, sizeof(int) * MAX_BUFFITEM_CNT);
			if(ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptBuffItem(GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, FALSE, tExpireDate, iItemIndex))
			{
				return FALSE;
			}

			CHECK_CONDITION_RETURN(0 >= iItemIndex[0], FALSE);
				
			pDb->FSGetAvatarItem(pAvatarItemList, GetGameIDIndex(), iItemIndex[0]);
			pDb->FSGetAvatarItemProperty(pAvatarItemList, GetGameIDIndex(), iItemIndex[0]);

			SUserItemInfo* pNewItem = pAvatarItemList->GetItemWithItemIdx(iItemIndex[0]);
			if (NULL != pNewItem)
			{
				SUserItemInfo addItemInfo;
				memcpy(&addItemInfo, pNewItem, sizeof(SUserItemInfo));

				vector< SUserItemProperty > vUserItemProperty;
				pAvatarItemList->GetItemProperty(iItemIndex[0], vUserItemProperty);
						
				for(int i = 1; iItemIndex[i] > 0 && i < MAX_BUFFITEM_CNT; i++)
				{
					addItemInfo.iItemIdx = iItemIndex[i];

					SUserItemInfo* pAddItem = pAvatarItemList->GetItemWithItemIdx(addItemInfo.iItemIdx);
					if (NULL == pAddItem)
					{
						pAvatarItemList->AddItem(addItemInfo);

						for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
						{
							vUserItemProperty[iPropertyCount].iItemIdx = addItemInfo.iItemIdx;
							pAvatarItemList->AddUserItemProperty(vUserItemProperty[iPropertyCount]);
						}
					}
				}
			}				
		}
		else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_HOTGIRLSPECIALBOX_ITEM)
		{
			int iUpdatedPropertyValue = 0;
			if ( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptHotGirlSpecialBoxItem( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iUpdatedPropertyValue) )
			{
				return FALSE;
			}

			GetUserHotGirlSpecialBoxEvent()->SetSpecialBoxCount(iUpdatedPropertyValue);
		}
		else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_LEGEND_KEY_ITEM)
		{
			int iUpdatedPropertyValue = 0;
			if ( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptLegendKeyItem( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iUpdatedPropertyValue) )
			{
				return FALSE;
			}

			GetUserLegendEvent()->SetLegendKeyCount(iUpdatedPropertyValue);
		}
		else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_RANDOM_ARROW_ITEM)
		{
			int iUpdatedPropertyValue = 0;
			if ( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptRandomArrowItem( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iUpdatedPropertyValue) )
			{
				return FALSE;
			}

			GetUserRandomArrowEvent()->SetArrowCount(iUpdatedPropertyValue);
		}
		else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_DEVILTEMTATION_NORMAL_ITEM || 
			shopItem.iPropertyKind == ITEM_PROPERTY_KIND_DEVILTEMTATION_PRIMIUM_ITEM)
		{
			int iUpdatedPropertyValue = 0;
			BYTE btTicketType = 0;
			if(ITEM_PROPERTY_KIND_DEVILTEMTATION_NORMAL_ITEM == shopItem.iPropertyKind)
			{
				btTicketType = NORMAL_TICKET;
			}
			else if(ITEM_PROPERTY_KIND_DEVILTEMTATION_PRIMIUM_ITEM == shopItem.iPropertyKind)
			{
				btTicketType = PRIMIUM_TICKET;
			}

			if ( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptDevilTemtationItem( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, btTicketType, iUpdatedPropertyValue) )
			{
				return FALSE;
			}

			GetUserDevilTemtationEvent()->SetTicketCount(btTicketType, iUpdatedPropertyValue);
		}
		else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_MISSION_MAKEITEM_CASH_ITEM)
		{
			int iUpdatedPropertyValue = 0;
			if ( ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptEventCashItem( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iUpdatedPropertyValue) )
			{
				return FALSE;
			}

			GetUserMissionMakeItemEvent()->SetMaterialEventItem(iUpdatedPropertyValue);
		}
		else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_SUMMON_STONE)
		{
			int iUpdatedPropertyValue = 0;
			if ( ODBC_RETURN_SUCCESS != pDb->EVENT_COVET_AcceptSummonStone( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iUpdatedPropertyValue) )
			{
				return FALSE;
			}

			GetUserCovet()->SetSummonStoneCount(iUpdatedPropertyValue);
		}
		else if( ITEM_PROPERTY_KIND_EVENT_HALFPRICE_NEEDLE == shopItem.iPropertyKind )
		{
			int iUpdatedPropertyValue = 0;
			if ( ODBC_RETURN_SUCCESS != pDb->EVENT_HALFPRICE_Accept( GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iIndex, iUpdatedPropertyValue) )
			{
				WRITE_LOG_NEW(LOG_TYPE_HALFPRICE, DB_DATA_UPDATE, FAIL, "EVENT_HALFPRICE_Accept - GameIDIndex:11866902\tPresentIndex:0", GetGameIDIndex(), iIndex);
eturn FALSE;
			}

			m_sHalfPriceInfo.iNeedleCount = iUpdatedPropertyValue;
		}
		else
		{
			int iItemIndex = 0;
			if(ODBC_RETURN_SUCCESS != pDb->ITEM_AcceptPresent(GetUserIDIndex(), GetGameIDIndex(), iMailType, iIndex, iItemIndex, AcceptOutputItem.iExtraBonusPoint, AcceptOutputItem.iExtraBonusTrophy))
			{
				WRITE_LOG_NEW(LOG_TYPE_PRESENT, DB_DATA_UPDATE, FAIL, "ITEM_AcceptPresent - GameIDIndex:11866902\tPresentIndex:0", GetGameIDIndex(), iIndex);
eturn FALSE;
			}

			if (ITEM_PROPERTY_EXHAUST == shopItem.iPropertyType)
			{
				bool bProperty = true;
				if(MIN_HEART_TATTO_KIND_NUM <= shopItem.iPropertyKind && MAX_HEART_TATTO_KIND_NUM >= shopItem.iPropertyKind)
				{
					if(nullptr != pAvatarItemList->GetItemWithItemIdx(iItemIndex))
					{
						bProperty = false;
					}
				}

				SUserItemInfo ItemInfo;
				pDb->FSGetAvatarItem(ItemInfo, GetGameIDIndex(), iItemIndex);

				if(true == bProperty)
					pDb->FSGetAvatarItemProperty(pAvatarItemList, GetGameIDIndex(), iItemIndex);

				pAvatarItemList->UpdateExhaustItemPropertyValue(ItemInfo);
			}
			else
			{
				pDb->FSGetAvatarItem(pAvatarItemList, GetGameIDIndex(), iItemIndex);
				pDb->FSGetAvatarItemProperty(pAvatarItemList, GetGameIDIndex(), iItemIndex);
			}
		}

		if(AcceptOutputItem.iExtraBonusPoint > 0)
		{
			SetSkillPoint(GetSkillPoint() + AcceptOutputItem.iExtraBonusPoint);

			int iTicketcount = GetUserItemList()->GetSkillTicketCount();
			SendUserGold(iTicketcount);
			AddPointAchievements(); 
		}

		if(AcceptOutputItem.iExtraBonusTrophy > 0)
		{
			int iTrophyCount = GetUserTrophy() + AcceptOutputItem.iExtraBonusTrophy;
			SetUserTrophy(iTrophyCount);
			SendCurAvatarTrophy();
		}
	}

	if(shopItem.iBigKind != ITEM_BIG_KIND_COACHCARD)
	{
		vector<int> vInventoryItemCounts;
		pAvatarItemList->GetInventoryItemCountforAchievement(vInventoryItemCounts);
		ProcessAchievementbyGroupAndComplete( ACHIEVEMENT_CONDITION_GROUP_INVENTORY, ACHIEVEMENT_LOG_GROUP_SHOP, vInventoryItemCounts);

		if(SKILL_TICKET_PROPERTYKIND == shopItem.iPropertyKind)
		{
			int iTicketcount = GetUserItemList()->GetSkillTicketCount();
			SendUserGold(iTicketcount);
		}
	}

	present->bIsCheck = 1;		

	iErrorCode = 0;
	if(ITEM_PROPERTY_KIND_CHEERLEADER == shopItem.iPropertyKind)
		iErrorCode = ACCEPT_PRESENT_CHEERLEADER;

	return TRUE;
}

BOOL CFSGameUser::GetChoosePropertyItemFromPresent(int iMailType, int iPresentIndex, int iChangeItemCode, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT], int& iErrorCode)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SPresent* present = GetPresentList(iMailType)->Find(iPresentIndex);
	CHECK_NULL_POINTER_BOOL(present);

	SShopItemInfo shopItem;		
	pItemShop->GetItem(present->iItemCode, shopItem);

	if(iChangeItemCode > 0)
	{
		if(shopItem.iItemCode0 != iChangeItemCode && shopItem.iItemCode1 != iChangeItemCode && shopItem.iItemCode2 != iChangeItemCode && shopItem.iItemCode3 != iChangeItemCode && shopItem.iItemCode4 != iChangeItemCode)
		{
			return FALSE; 
		}
	}

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	if(IsChoosePropertyItem(iMailType, iPresentIndex) == FALSE)
		return FALSE;

	int iOutIndx = 0;
	int iPropertyKind = 0, iPropertyNum = 0;
	SPremiumOutputItem AcceptOutputItem;

	if(shopItem.iPropertyKind == PREMIUM_ITEM_KIND_SUPER_ROOKIE || shopItem.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP || shopItem.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP_PLUS)
	{
		iPropertyKind = shopItem.iPropertyKind;

		// �ߺ� PowerUP �������̶�� ���� 
		if(shopItem.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP || shopItem.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP_PLUS)
		{
			//int iCheckPropertyKind = PREMIUM_ITEM_KIND_POWERUP == shopItem.iPropertyKind ? PREMIUM_ITEM_KIND_POWERUP_PLUS : PREMIUM_ITEM_KIND_POWERUP;
			SUserItemInfo* pCheckSameItem = pAvatarItemList->GetInventoryItemWithPropertyKind(PREMIUM_ITEM_KIND_POWERUP);
			SUserItemInfo* pCheckSameItem_Plus = pAvatarItemList->GetInventoryItemWithPropertyKind(PREMIUM_ITEM_KIND_POWERUP_PLUS);		

			if(pCheckSameItem != NULL || pCheckSameItem_Plus != NULL)
			{
				const int ACCEPT_ITEM_ERROR_POWERUP_ITEM_HAVE_ALREADY = -23;
				iErrorCode = ACCEPT_ITEM_ERROR_POWERUP_ITEM_HAVE_ALREADY;
				return FALSE;	
			}
		}

		{
			if(ODBC_RETURN_SUCCESS != pGameODBC->ITEM_AcceptPremiumItem(GetCurUsedAvatar()->iUserIDIndex, GetCurUsedAvatar()->iGameIDIndex, iMailType, iPresentIndex, aiPropertyIndex, iPropertyNum,iOutIndx, AcceptOutputItem))
			{
				WRITE_LOG_NEW(LOG_TYPE_PRESENT, DB_DATA_UPDATE, FAIL, "ITEM_AcceptPremiumItem - UserID:�4, GameID:(null), PresentIndex:18227200, ItemCode:-858993460", GetUserID(), GetGameID(), iPresentIndex, present->iItemCode);
			}

			SPremiumItem *pPremiumItemInfo = pItemShop->GetShopItemList()->GetPremiumItem(present->iItemCode);
			CHECK_NULL_POINTER_BOOL(pPremiumItemInfo);

			for(int iLoopIndex = 0; iLoopIndex < MAX_PREMIUMITEM_ITEMNUM; iLoopIndex++)
			{
				if(0 == AcceptOutputItem.iExtraBonusPoint)
					break;

				if(pPremiumItemInfo->iItemCode[iLoopIndex] > 0 && AcceptOutputItem.iItemIdx[iLoopIndex] > 0)
				{		
					SShopItemInfo  PremiumItemInfoOne;
					if(FALSE == pItemShop->GetItem(pPremiumItemInfo->iItemCode[iLoopIndex], PremiumItemInfoOne)) 
					{
						return FALSE;
					}

					SUserItemInfo UserItemInfo;
					UserItemInfo.iItemCode = pPremiumItemInfo->iItemCode[iLoopIndex];

					if(PremiumItemInfoOne.iPropertyType == ITEM_PROPERTY_TIME)
					{
						UserItemInfo.iPropertyTypeValue = pPremiumItemInfo->iUseTerm;
					}
					else if(PremiumItemInfoOne.iPropertyType == ITEM_PROPERTY_EXHAUST)
					{
						UserItemInfo.iPropertyTypeValue = 10;							
					}
					UserItemInfo.iItemIdx = AcceptOutputItem.iItemIdx[iLoopIndex];
					UserItemInfo.ExpireDate = AcceptOutputItem.ExpireDateInfo;

					m_UserItem->InsertPremiumItem(pAvatarItemList, PremiumItemInfoOne, UserItemInfo, pPremiumItemInfo->iPropertyIndex1[iLoopIndex], pPremiumItemInfo->iPropertyIndex2[iLoopIndex], pPremiumItemInfo->iPropertyIndex3[iLoopIndex]);
				}
			}

			if(AcceptOutputItem.iExtraBonusPoint > 0)
			{
				int iCurrentSkillPoint = GetSkillPoint() + AcceptOutputItem.iExtraBonusPoint;
				SetSkillPoint(iCurrentSkillPoint);
				SendUserGold();
				AddPointAchievements(); 
			}
		}

		SUserItemInfo* pCheckItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iPropertyKind);
		if( pCheckItem )
		{
			pAvatarItemList->ChangeItemPropertyValueAndExpireDateWithItemIndex(pCheckItem->iItemIdx, pCheckItem->iPropertyTypeValue + present->iPropertyValue, AcceptOutputItem.ExpireDateInfo);
		}
		else
		{
			SUserItemInfo UserItemInfo;
			UserItemInfo.iItemIdx = iOutIndx;
			UserItemInfo.iItemCode = present->iItemCode;
			UserItemInfo.iPropertyTypeValue = present->iPropertyValue;
			UserItemInfo.BuyDate = _time64(NULL);
			UserItemInfo.ExpireDate = AcceptOutputItem.ExpireDateInfo;

			m_UserItem->InsertPremiumItem( pAvatarItemList, shopItem, UserItemInfo, aiPropertyIndex[0], aiPropertyIndex[1], aiPropertyIndex[2] );
		}

		iErrorCode = GET_PROPERTY_SELECT_ITEM_SUCESS;
	}
	else if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_LV_UP)
	{
		iErrorCode = GET_PROPERTY_SELECT_ITEM_USE_LVUPITEM_FAIL;

		SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
		CHECK_NULL_POINTER_BOOL(pAvatarInfo);

		if(TRUE == IsJumpingPlayerAvatar())
		{
			iErrorCode = GET_PROPERTY_SELECT_ITEM_NOT_USE_EVENTCHAR;
			return FALSE;
		}

		if(FALSE == LVUPEVENT.CheckUseRewardItem(shopItem.iItemCode0, GetCurUsedAvtarLv()))
		{
			iErrorCode = GET_PROPERTY_SELECT_ITEM_NOT_USE_LV_CONDITION;
			return FALSE;
		}

		int iLv = GetCurUsedAvtarLv() + 1;
		int iExp = pServer->GetLvUpExp(iLv);
		if(ODBC_RETURN_SUCCESS != pGameODBC->EVENT_LV_UP_Jumping(GetUserIDIndex(), GetGameIDIndex(), iLv, iExp, iMailType, iPresentIndex))
			return FALSE;

		ProcessLevelUp(GetGameIDIndex(), pAvatarInfo->Status.iGamePosition, GetCurUsedAvtarLv(), iLv, iExp);

		iErrorCode = GET_PROPERTY_SELECT_ITEM_SUCESS_USE_LVUPITEM;
	}
	else
	{
		if(shopItem.iPropertyKind == ITEM_PROPERTY_KIND_EXP_BOX)
		{
			if(GetCurUsedAvtarLv() == GAME_LVUP_MAX)
			{
				iErrorCode = GET_PROPERTY_SELECT_ITEM_NOT_USE_EXPBOX_LV_CONDITION;
				return FALSE;
			}	
		}
		else
		{
			if(FALSE == CheckItemAcceptProperty(iMailType, iPresentIndex, aiPropertyIndex))
			{
				iErrorCode = GET_PROPERTY_SELECT_ITEM_NOT_EXISTS_PROPERTY_ITEM;
				WRITE_LOG_NEW(LOG_TYPE_PRESENT, GET_DATA, FAIL, "ITEM_AcceptPresent CheckItemAcceptProperty - GameIDIndex:11866902\tPresentIndex:0\tMailType:18227200", GetGameIDIndex(), iPresentIndex, iMailType);
n FALSE;
			}

			SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
			CHECK_NULL_POINTER_BOOL(pAvatarInfo);

			if((pAvatarInfo->iSpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX1 ||
				pAvatarInfo->iSpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX3) &&
				(shopItem.iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
			{
				iErrorCode = ACCEPT_PRESENT_NOT_USE_CHAR;
				return FALSE;
			}
		}

		int iItemIndex;
		int iBonusPoint, iBonusTrophy;

		if(ODBC_RETURN_SUCCESS != pGameODBC->ITEM_AcceptPresent(GetUserIDIndex(), GetGameIDIndex(), iMailType, iPresentIndex, iItemIndex, iBonusPoint, iBonusTrophy, iChangeItemCode, aiPropertyIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_PRESENT, DB_DATA_UPDATE, FAIL, "ITEM_AcceptPresent ChoosePreopty - GameIDIndex:11866902\tPresentIndex:0\tItemCode:18227200", GetGameIDIndex(), iPresentIndex, iChangeItemCode);
shopItem.iPropertyKind == ITEM_PROPERTY_KIND_EXP_BOX)
				iErrorCode = GET_PROPERTY_SELECT_ITEM_USE_EXPBOX_FAIL;

			return FALSE;
		}
		
		if(ITEM_PROPERTY_EXHAUST == shopItem.iPropertyType)
		{
			SUserItemInfo ItemInfo;
			pGameODBC->FSGetAvatarItem(ItemInfo, GetGameIDIndex(), iItemIndex);

			pAvatarItemList->RemoveItemProperty(iItemIndex);
			pGameODBC->FSGetAvatarItemProperty(pAvatarItemList, GetGameIDIndex(), iItemIndex);

			pAvatarItemList->UpdateExhaustItemPropertyValue(ItemInfo);
		}
		else
		{
			pGameODBC->FSGetAvatarItem(pAvatarItemList, GetGameIDIndex(), iItemIndex);
			pGameODBC->FSGetAvatarItemProperty(pAvatarItemList, GetGameIDIndex(), iItemIndex);
		}

		if(iBonusPoint > 0)
		{
			SetSkillPoint(GetSkillPoint() + iBonusPoint);
			SendUserGold();
			AddPointAchievements(); 
		}

		if(iBonusTrophy > 0)
		{
			SetUserTrophy(GetUserTrophy() + iBonusTrophy);
			SendCurAvatarTrophy();
		}

		iErrorCode = GET_PROPERTY_SELECT_ITEM_SUCESS;
	}

	present->bIsCheck = 1;	

	return TRUE;
}

void CFSGameUser::RecvPaper(int index)
{
	CFSGameODBC* pDB = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pDB);

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	SPaper* paper = new SPaper;
	CHECK_NULL_POINTER_VOID(paper);

	if(ODBC_RETURN_SUCCESS != pDB->spGetPaper(pAvatarInfo->iGameIDIndex, index, paper))
	{
		SAFE_DELETE(paper);
		return;
	}
	
	if(false == GetPaperList()->Add(paper))
	{
		SAFE_DELETE(paper);
		return;
	}
	
	if(IsPlaying()) 
	{
		CPacketComposer *Sp = new CPacketComposer(S2C_RECV_PAPER);		
		if(Sp == NULL)
		{
			return;
		}
		
		Sp->Add(paper->GetIndex());
		Sp->Add((BYTE*)paper->szSendID, MAX_GAMEID_LENGTH+1);	
		Sp->Add((BYTE*)paper->szTitle, MAX_PAPER_TITLE_LENGTH+1);
		Sp->Add((DWORD)paper->GetDate());	
		Sp->Add( (BYTE)paper->bFSOfficial );
		
		PushPacketQueue(Sp);
	}
	else
	{
		CPacketComposer PacketComposer(S2C_RECV_PAPER);
		
		PacketComposer.Add(paper->GetIndex());
		PacketComposer.Add((BYTE*)paper->szSendID, MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add((BYTE*)paper->szTitle, MAX_PAPER_TITLE_LENGTH+1);
		PacketComposer.Add((DWORD)paper->GetDate());	
		PacketComposer.Add( (BYTE)paper->bFSOfficial );
		
		Send(&PacketComposer);
		
	}
}

void CFSGameUser::SendFriendList()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bEnterChannelToSendFriend || FALSE == m_bReceiveAllFriend);

	int iFriendCnt = 0;
	int iCode = 0;
	CFriend* fr = NULL;
	std::vector<CFriend*> v;

	BEGIN_LOCK
		CLock Lock(m_FriendList.GetLockResource());
		fr = m_FriendList.FindFirst();
		while(fr)
		{
			if(fr->GetConcernStatus() != FRIEND_ST_REQUEST_USER) 
			{
				iFriendCnt++;
			}
			else
			{
				v.push_back(fr);
			}

			fr = m_FriendList.FindNext();		
		}	

		int iFriendMax = 100;
		int iCount = iFriendCnt / iFriendMax;
		int iLeftCnt = iFriendCnt - ( iCount * iFriendMax );	

		CPacketComposer PacketComposerBeg(S2C_FRIEND_LIST);
		PacketComposerBeg.Add( iCode );
		Send(&PacketComposerBeg);

		iCode = 1;
		fr = m_FriendList.FindFirst();
		for ( int i = 0; i < iCount; ++ i )
		{
			CPacketComposer PacketComposer(S2C_FRIEND_LIST);
			PacketComposer.Add(iCode);
			PacketComposer.Add(iFriendMax);
			for ( int j = 0; j < iFriendMax; )
			{
				if( fr )
				{
					if(fr->GetConcernStatus() != FRIEND_ST_REQUEST_USER) 
					{
						PacketComposer.Add((BYTE*)fr->GetGameID(), MAX_GAMEID_LENGTH+1);	
						PacketComposer.Add((BYTE)fr->GetConcernStatus());
						PacketComposer.Add((BYTE)fr->GetStatus());
						PacketComposer.Add((BYTE)fr->GetLocation());
						PacketComposer.Add((BYTE)fr->GetPosition());
						PacketComposer.Add((BYTE)fr->GetLevel());
						PacketComposer.Add(fr->GetFameLevel());
						PacketComposer.Add(fr->GetEquippedAchievementTitle());
						PacketComposer.Add((int)-1);
						PacketComposer.Add((int)-1);
						PacketComposer.Add((int)-1);
						PacketComposer.Add( fr->GetWithPlayCnt() );

						++ j;
					}
				}
				else
				{
					break;
				}

				fr = m_FriendList.FindNext();		
			}

			Send(&PacketComposer);
		}

		if ( iLeftCnt > 0 )
		{
			CPacketComposer PacketComposer(S2C_FRIEND_LIST);
			PacketComposer.Add(iCode);
			PacketComposer.Add(iLeftCnt);
			for ( int i = 0; i < iLeftCnt; )
			{
				if( fr )
				{
					if(fr->GetConcernStatus() != FRIEND_ST_REQUEST_USER) 
					{
						PacketComposer.Add((BYTE*)fr->GetGameID(), MAX_GAMEID_LENGTH+1);	
						PacketComposer.Add((BYTE)fr->GetConcernStatus());
						PacketComposer.Add((BYTE)fr->GetStatus());
						PacketComposer.Add((BYTE)fr->GetLocation());
						PacketComposer.Add((BYTE)fr->GetPosition());
						PacketComposer.Add((BYTE)fr->GetLevel());
						PacketComposer.Add(fr->GetFameLevel());
						PacketComposer.Add(fr->GetEquippedAchievementTitle());
						PacketComposer.Add((int)-1);
						PacketComposer.Add((int)-1);
						PacketComposer.Add((int)-1);
						PacketComposer.Add( fr->GetWithPlayCnt() );

						++ i;
					}
				}
				else
				{
					break;
				}

				fr = m_FriendList.FindNext();		
			}
			Send(&PacketComposer);
		}
	END_LOCK

	iCode = 2;
	CPacketComposer PacketComposerEnd(S2C_FRIEND_LIST);
	PacketComposerEnd.Add(iCode);
	Send(&PacketComposerEnd);

	for(int i = 0; i < v.size(); ++i)
	{
		fr = v[i];
		if(!fr) continue;

		CPacketComposer PacketComposer(S2C_ADD_FRIEND_RES);	
		PacketComposer.Add((BYTE)0);
		PacketComposer.Add((BYTE*)fr->GetGameID(), MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add((BYTE) 0 );
		PacketComposer.Add((BYTE)fr->GetPosition());
		PacketComposer.Add((BYTE)fr->GetLevel());
		PacketComposer.Add(fr->GetFameLevel());
		PacketComposer.Add(fr->GetEquippedAchievementTitle());
		PacketComposer.Add((int)-1);
		PacketComposer.Add((int)-1);
		PacketComposer.Add((int)-1);
		Send(&PacketComposer);
	}
}

void CFSGameUser::SendFriendAccountList()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bEnterChannelToSendFriendAccount || FALSE == m_bReceiveAllFriendAccount);

	std::vector<CFriendAccount> vWaitList;

	SS2C_FRIEND_ACCOUNT_LIST_NOT	ss;
	ss.tCurrentTime = _GetCurrentDBDate;
	strncpy_s(ss.szMyAccountID, _countof(ss.szMyAccountID), GetAccountID(), FriendAccount::MAX_ACCOUNT_ID_LENGTH);
	ss.bIsRecommend = m_UserFriendAccountInfo.bIsRecommend;	

	ss.bIsLast = true;
	ss.btFriendCnt = 0;

	CPacketComposer Packet(S2C_FRIEND_ACCOUNT_LIST_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_FRIEND_ACCOUNT_LIST_NOT));
	PBYTE pData = Packet.GetTail() - sizeof(SS2C_FRIEND_ACCOUNT_LIST_NOT);

	SFRIEND_ACCOUNT_INFO sInfo;
	bool bInitialize = true;

	const int MAX_SEND_CNT = FriendAccount::MAX_FRIEND_CNT / 2;

	BEGIN_LOCK

		CLock Lock(m_FriendAccountList.GetLockResource());

	for(CFriendAccount* fr = m_FriendAccountList.FindFirst(); 
		fr != nullptr && ss.btFriendCnt <= FriendAccount::MAX_FRIEND_CNT; 
		fr = m_FriendAccountList.FindNext())
	{
		if(FriendAccount::STATUS_REQ_RECV == fr->GetStatus())
		{
			vWaitList.push_back(*fr);
			continue;
		}

		if(ss.btFriendCnt >= MAX_SEND_CNT)
		{
			ss.bIsLast = false;
			memcpy(pData, &ss, sizeof(SS2C_FRIEND_ACCOUNT_LIST_NOT));
			Send(&Packet);

			Packet.Initialize(S2C_FRIEND_ACCOUNT_LIST_NOT);
			bInitialize = false;
		}

		if(false == bInitialize)
		{
			ss.bIsLast = true;
			ss.btFriendCnt = 0;
			Packet.Add((PBYTE)&ss, sizeof(SS2C_FRIEND_ACCOUNT_LIST_NOT));
			pData = Packet.GetTail() - sizeof(SS2C_FRIEND_ACCOUNT_LIST_NOT);

			bInitialize = true;
		}

		ZeroMemory(&sInfo, sizeof(SFRIEND_ACCOUNT_INFO));

		fr->GetData(sInfo, true);

		Packet.Add((PBYTE)&sInfo, sizeof(SFRIEND_ACCOUNT_INFO));

		ss.btFriendCnt++;
	}

	ss.bIsLast = true;
	memcpy(pData, &ss, sizeof(SS2C_FRIEND_ACCOUNT_LIST_NOT));
	Send(&Packet);

	END_LOCK

		SS2C_FRIEND_ACCOUNT_ADD_RES	ss2;
	ZeroMemory(&ss2, sizeof(SS2C_FRIEND_ACCOUNT_ADD_RES));

	ss2.btOptionType = FriendAccount::OPTION_TYPE_ADD_FRIEND_REQ;
	ss2.btResult = FriendAccount::RESULT_ADD_FRIEND_SUCCESS;
	ss2.tCurrentTime = _GetCurrentDBDate;

	ss2.sInfo.btStatus = FriendAccount::STATUS_REQ_RECV;
	ss2.sInfo.btChannelType = FriendAccount::CHANNEL_TYPE_OFFLINE;
	ss2.sInfo.tLastLoginTime = -1;

	for(int i = 0; i < vWaitList.size(); ++i)
	{
		ss2.sInfo.iUserIDIndex = vWaitList.at(i).GetUserIDIndex();
		strncpy_s(ss2.sInfo.szAccountID, _countof(ss2.sInfo.szAccountID), vWaitList.at(i).GetAccountID(), FriendAccount::MAX_ACCOUNT_ID_LENGTH);

		Send(S2C_FRIEND_ACCOUNT_ADD_RES, &ss2, sizeof(SS2C_FRIEND_ACCOUNT_ADD_RES));
	}
}

void CFSGameUser::LoadBindaccountItemList()
{
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CBindaccountItem *pBindaccountItem = GetBindaccountItem();
	CHECK_NULL_POINTER_VOID(pBindaccountItem);

	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	if (pAvatarInfo != NULL)
	{
		vector<SBindaccountItem> vecBindaccountItemList;

		int		iBindaccountItemNum = 0;
		pBindaccountItem->GetBindaccountItemList(iBindaccountItemNum, vecBindaccountItemList);

		if (iBindaccountItemNum <= 0)
		{
			return;
		}

		int iMaxItemIdx = (-50);

		BindaccountItemPropertyVector vecBindaccountItemProperty = GetBindaccountItemPropertyVector();

		for(int iLoopIndex = 0; iLoopIndex < iBindaccountItemNum; iLoopIndex++)
		{
			SShopItemInfo sShopItemInfo;
			if (TRUE == pItemShop->GetItem(vecBindaccountItemList[iLoopIndex].iItemCode, sShopItemInfo))
			{
				SUserItemInfo UserItemInfo;

				UserItemInfo.iItemIdx = iMaxItemIdx--;
				UserItemInfo.iItemCode = vecBindaccountItemList[iLoopIndex].iItemCode;
				UserItemInfo.iCategory = sShopItemInfo.iCategory;
				UserItemInfo.iBigKind = sShopItemInfo.iBigKind;
				UserItemInfo.iSmallKind = sShopItemInfo.iSmallKind;

				UserItemInfo.iSellType = sShopItemInfo.iSellType;
				UserItemInfo.iChannel = sShopItemInfo.iChannel;
				UserItemInfo.iSexCondition = sShopItemInfo.iSexCondition;
				UserItemInfo.iStatus = 1;
				UserItemInfo.iSellPrice = 0;

				UserItemInfo.iPropertyType = sShopItemInfo.iPropertyType;
				if(sShopItemInfo.iPropertyType == ITEM_PROPERTY_TIME)	// UserItemInfo.iPropertyTypeValue��  �����ϴ� �κ��� ���߿� �Լ�ȭ �Ұ�
				{
					UserItemInfo.iPropertyTypeValue = vecBindaccountItemList[iLoopIndex]._ExpireDate - vecBindaccountItemList[iLoopIndex]._StartDate;
				}
				else
				{
					UserItemInfo.iPropertyTypeValue = -1;
				}
				
				UserItemInfo.ExpireDate = vecBindaccountItemList[iLoopIndex]._ExpireDate;
				pItemShop->SetItemPropertyKind(sShopItemInfo.iPropertyKind,UserItemInfo.iPropertyKind);

				SBindaccountItemProperty* pBindaccountItemProperty = NULL;
				BindaccountItemPropertyVector vecBindaccountItemProperty;
				int iProperty = -1;
				int iValue = -1;

				GetBindaccountItemPropertyVectorbyInventoryIndex(vecBindaccountItemList[iLoopIndex].iUserInventoryIndex, vecBindaccountItemProperty);

				UserItemInfo.iPropertyNum = vecBindaccountItemProperty.size();

				if (vecBindaccountItemProperty.size() > 0)
				{
					for (int iLoopIndex2 = 0; iLoopIndex2 < vecBindaccountItemProperty.size(); iLoopIndex2++)
					{
						SUserItemProperty tempUserItemProperty;
						tempUserItemProperty.iGameIDIndex	= pAvatarInfo->iGameIDIndex;
						tempUserItemProperty.iItemIdx		= UserItemInfo.iItemIdx;
						tempUserItemProperty.iProperty		= vecBindaccountItemProperty[iLoopIndex2]->sProperty;
						tempUserItemProperty.iValue			= vecBindaccountItemProperty[iLoopIndex2]->iValue;

						pAvatarItemList->AddUserItemProperty(tempUserItemProperty);
					}
				}

				m_UserItem->InsertNewItem(UserItemInfo);

				pBindaccountItem->SetAvatarInventoryIndex(vecBindaccountItemList[iLoopIndex].iUserInventoryIndex, UserItemInfo.iItemIdx);
			}			
		}
	}
}

BOOL CFSGameUser::GetAvatarLinkItemStatus(int *iaStat)
{
	SAvatarInfo * pAvatarInfo = NULL;
	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( pAvatarInfo == NULL || NULL == pAvatarItemList ) return FALSE;
	
	for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != pAvatarInfo->AvatarFeature.FeatureInfo[i] )
		{
			SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]);
			if( pSlotItem != NULL )
			{			
				if(((pSlotItem->iPropertyKind >= MIN_LINK_ITEM_PROPERTY_KIND_NUM && pSlotItem->iPropertyKind <= MAX_LINK_ITEM_PROPERTY_KIND_NUM) || ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC == pSlotItem->iPropertyKind) &&
					FALSE != pAvatarItemList->CheckUserApplyAccItemProperty(pSlotItem->iItemIdx))
				{
					int iUserItemPropertyNum = pSlotItem->iPropertyNum;
					if( iUserItemPropertyNum > 0 )
					{
						vector< SUserItemProperty > vUserItemProperty;
						
						pAvatarItemList->GetItemProperty(pSlotItem->iItemIdx, vUserItemProperty);
						
						for( int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++ )
						{
							if( 0 <= vUserItemProperty[iPropertyCount].iProperty  && vUserItemProperty[iPropertyCount].iProperty < MAX_STAT_NUM )
							{
								if (iaStat[ vUserItemProperty[iPropertyCount].iProperty] < vUserItemProperty[iPropertyCount].iValue)								
									iaStat[ vUserItemProperty[iPropertyCount].iProperty ] = vUserItemProperty[iPropertyCount].iValue;
							}
						}						
					}
				}
			}
		}
	}
	return TRUE;
}

BOOL CFSGameUser::GetAvatarSentenceStatus(int *iaStat)
{
	GetUserCharacterCollection()->GetSentenceStatus(iaStat);

	return TRUE;
}

BOOL CFSGameUser::GetAvatarPowerupCapsuleStatus( int *iaStat )
{
	GetUserPowerupCapsule()->GetPowerupCapsuleStatus(iaStat);

	return TRUE;
}

BOOL CFSGameUser::GetLvIntervalBuffItemStatus(int* iaStat)
{
	SAvatarInfo AvatarInfo;
	CHECK_CONDITION_RETURN( FALSE == CurUsedAvatarSnapShot( AvatarInfo ), FALSE );

	int iUserCheckLv = 0;
	BOOL bResult = IsActiveLvIntervalBuffItem(ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF_ABILITY, iUserCheckLv);

	if(bResult)
	{
		CFSGameServer::GetInstance()->GetStatusUp(AvatarInfo.Status.iGamePosition, AvatarInfo.iLv, iUserCheckLv, iaStat, MAX_STAT_NUM);
	}

	return bResult;
}

SAvatarStatus* CFSGameUser::GetEqualizeStatus()
{
	return m_AvatarManager.GetEqualizeStatus();
}

BOOL CFSGameUser::GetAvatarSpecialPieceProperty( int *iaStat )
{
	GetUserSpecialPiece()->GetSpecialPieceProperty(iaStat);

	return TRUE;
}

int CFSGameUser::GetEqualizePosition( int iPosition )
{
	int iArenaPosition = -1;
	switch(iPosition)
	{
	case 0: iArenaPosition = 0; return iArenaPosition;		// ���� 
	case 1: iArenaPosition = 1; return iArenaPosition;		// ������
	case 2: iArenaPosition = 2; return iArenaPosition;		// ����
	case 3: iArenaPosition = 1; return iArenaPosition;		// �Ŀ� ������ -> ������ 
	case 4: iArenaPosition = 1; return iArenaPosition;		// ���� ������ -> ������ 
	case 5: iArenaPosition = 2; return iArenaPosition;		// ����Ʈ ���� -> ���� 
	case 6: iArenaPosition = 2; return iArenaPosition;		// ���ð��� -> ���� 
	default: return iPosition;
	}
	return 0;
}

BOOL CFSGameUser::CheckAvatarFeature()
{
	SAvatarInfo * pAvatarInfo = NULL;
	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( pAvatarInfo == NULL || NULL == pAvatarItemList ) return FALSE;
	
	for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != pAvatarInfo->AvatarFeature.FeatureInfo[i] )
		{
			SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]);
			if( pSlotItem != NULL )
			{
				if( pSlotItem->iStatus != 2 )
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
	}
	
	return TRUE;
}

void CFSGameUser::SendAllItemInfo( int iSex , int bCheckClone)
{
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	if( pAvatarInfo == NULL )
	{
		return;
	}
	
	if( FALSE == bCheckClone )
	{
		if( m_bMySex == TRUE && iSex == pAvatarInfo->iSex )
		{
			return;
		}
		else if( m_bMySex != TRUE && iSex == pAvatarInfo->iSex )
		{
			m_bMySex = TRUE;
		}
	}
	
	if( m_bAnotherSex == TRUE && iSex != pAvatarInfo->iSex )
	{
		return;
	}
	else if( m_bAnotherSex != TRUE && iSex != pAvatarInfo->iSex )
	{
		m_bAnotherSex = TRUE;
	}
// 	else if( FALSE == m_bAnotherSex && TRUE == bCheckClone )
// 	{
// 		m_bAnotherSex = TRUE;
// 	}
	
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	CLimitedEditionItemManager* pLimitedEditionItemManager = CFSGameServer::GetInstance()->GetLimitedEditionItemManager();
	CHECK_NULL_POINTER_VOID(pLimitedEditionItemManager);

	vector< SShopItemInfo* > vShopItemInfo;
	pItemShop->GetAllItemInfo( iSex, vShopItemInfo );
	if( vShopItemInfo.size() > 0 )
	{
		TRACE(" All ItemNum -> 11866902 \n", vShopItemInfo.size() );

int iOpCode = OP1_CREATE;	
	
	if( vShopItemInfo.empty() )
	{
		iOpCode = OP5_ERROR;	
		CPacketComposer PacketComposer( S2C_SHOPITEM_LIST_RES );
		PacketComposer.Add(iOpCode);
		Send(&PacketComposer);
		
		return;
	}
	
	iOpCode = OP1_CREATE;
	CPacketComposer PacketComposer( S2C_SHOPITEM_LIST_RES );
	PacketComposer.Add(iOpCode);
	PacketComposer.Add(iSex);

	const int cnSendMaxCount = 30;
	size_t iSendCount = (vShopItemInfo.size() / cnSendMaxCount) + 1;
	PacketComposer.Add(iSendCount);

	Send(&PacketComposer, FALSE);
	
	
	int iItemNumCount = 0;
	
	TRACE(" ------------- ItemSend Start\n ");
	
	iOpCode = OP2_UPDATE;
	size_t iCount = 0;
	
	TRACE(" ------------- ItemSend Size 11866902\n ", vShopItemInfo.size());
l bInitCheck = false;

	for( size_t i = 0 ; i < iSendCount ; ++i )
	{
		PacketComposer.Initialize( S2C_SHOPITEM_LIST_RES );
		PacketComposer.Add(iOpCode);
		PacketComposer.Add(iSex);

		int iItemNumCount = 0;

		PBYTE pbtShopItemLocation =  PacketComposer.GetTail();
		PacketComposer.Add(iItemNumCount);
		for( ; iCount < vShopItemInfo.size(); )
		{
			PacketComposer.Add(vShopItemInfo[iCount]->iCategory);
			PacketComposer.Add(vShopItemInfo[iCount]->iBigKind);
			PacketComposer.Add(vShopItemInfo[iCount]->iSmallKind);
			PacketComposer.Add(vShopItemInfo[iCount]->iChannel);
			PacketComposer.Add(vShopItemInfo[iCount]->iRecommend);
			PacketComposer.Add(vShopItemInfo[iCount]->iItemCode0);

			PacketComposer.Add(vShopItemInfo[iCount]->iItemCode1);
			PacketComposer.Add(vShopItemInfo[iCount]->iItemCode2);
			PacketComposer.Add(vShopItemInfo[iCount]->iItemCode3);
			PacketComposer.Add(vShopItemInfo[iCount]->iItemCode4);
			PacketComposer.Add(vShopItemInfo[iCount]->iSellType-1);
			PacketComposer.Add(vShopItemInfo[iCount]->iPrice);
				
			int iStatus = vShopItemInfo[iCount]->iStatus;

			if(ITEMSHOP_STATUS_SALE == iStatus && FALSE == GetAbleSaleItem())
			{
				iStatus = ITEMSHOP_STATUS_NONE;
			}
		
			PacketComposer.Add(iStatus);
		
			int iInMyItem = 0;
		
			if(vShopItemInfo[iCount]->iPropertyKind == ITEM_PROPERTY_KIND_CHEERLEADER)
			{
				BYTE btCheerLeaderTypeNum = 0;
				if(TRUE == GetUserCheerLeader()->CheckCheerLeader(vShopItemInfo[iCount]->iCheerLeaderIndex, btCheerLeaderTypeNum) &&
					btCheerLeaderTypeNum == vShopItemInfo[iCount]->btCheerLeaderTypeNum)
					iInMyItem = 1;
			}
			else
			{
				if( GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode0) ) iInMyItem = 1;
				else if( GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode1) ) iInMyItem = 1;
				else if( GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode2) ) iInMyItem = 1;
				else if( GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode3) ) iInMyItem = 1;
				else if( GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode4) ) iInMyItem = 1;

			}		

			PacketComposer.Add(iInMyItem);
		
			PacketComposer.Add(vShopItemInfo[iCount]->iLvCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iPositionCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iLeagueCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iFirstRankCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iLastRankCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iSendPropertyKind);
			PacketComposer.Add(vShopItemInfo[iCount]->iShopButtonType);
			PacketComposer.Add(vShopItemInfo[iCount]->iDisplayPriority);
			PacketComposer.Add(vShopItemInfo[iCount]->iUseOption);
			PacketComposer.Add(vShopItemInfo[iCount]->iSpecialIndexCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iChampionCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iTeamIndexCondition);
			PacketComposer.Add(vShopItemInfo[iCount]->iUnableTeamIndexCondition);
			PacketComposer.Add((BYTE)vShopItemInfo[iCount]->bIsDiscountTarget);

			BYTE btLimitedEditionItem = 0;
			int iLimitedEditionCount = 0;
			if(TRUE == pLimitedEditionItemManager->CheckLimitedEditionItem(vShopItemInfo[iCount]->iItemCode0))
			{
				btLimitedEditionItem = 1;
				iLimitedEditionCount = pLimitedEditionItemManager->GetLimitedEditionCount(vShopItemInfo[iCount]->iItemCode0);
			}
			PacketComposer.Add(btLimitedEditionItem);
			PacketComposer.Add(iLimitedEditionCount);
			PacketComposer.Add(vShopItemInfo[iCount]->btChangeDressItemType);
			PacketComposer.Add(vShopItemInfo[iCount]->iChangeItem);
			PacketComposer.Add((BYTE)vShopItemInfo[iCount]->bIsNotNewCharBuyItem );
			PacketComposer.Add(vShopItemInfo[iCount]->iSpecialPartsIndex);

			BYTE btIsNotGuanYuBuyItem = 0;
			if((vShopItemInfo[iCount]->iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
			{
				btIsNotGuanYuBuyItem = 1;
			}
			PacketComposer.Add(btIsNotGuanYuBuyItem);
			PacketComposer.Add(vShopItemInfo[iCount]->btEffectType);
			PacketComposer.Add(vShopItemInfo[iCount]->iCheerLeaderIndex);
			PacketComposer.Add(vShopItemInfo[iCount]->btCheerLeaderTypeNum);
			PacketComposer.Add(vShopItemInfo[iCount]->btIsWebRate);

			++iCount;
			++iItemNumCount;

			if( cnSendMaxCount == iItemNumCount )
				break;
		}

		memcpy(pbtShopItemLocation, &iItemNumCount, sizeof(iItemNumCount));
		Send(&PacketComposer, FALSE);
	}

	if( OPEN == ATTENDANCEITEMSHOP.IsOpen() )
	{
		SendAttendanceItemShop( iSex );
	}
	
	TRACE(" ------------- ItemSend End\n ");
	
	iOpCode = OP4_END;	
	PacketComposer.Initialize( S2C_SHOPITEM_LIST_RES );
	PacketComposer.Add(iOpCode);
	PacketComposer.Add(iSex);
	Send(&PacketComposer);
	
	TRACE(" ------------- ItemSend End    22\n ");
}

BOOL CFSGameUser::GetAvatarSingleItemStatus(int iItemCode, int *iaStat)
{
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;
	
	memset( iaStat , 0 , sizeof(int) * MAX_STAT_NUM );
	
	SUserItemInfo * pSecondBoostItem = pAvatarItemList->GetItemWithItemCode(iItemCode);
	
	if (pSecondBoostItem == NULL)	
	{
		return FALSE;
	}
	
	int iUserItemPropertyNum = pSecondBoostItem->iPropertyNum;					
	if( iUserItemPropertyNum > 0 )
	{
		vector< SUserItemProperty > vUserItemProperty;
		
		pAvatarItemList->GetItemProperty(pSecondBoostItem->iItemIdx, vUserItemProperty);
		
		for( int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++ )
		{
			if( 0 <= vUserItemProperty[iPropertyCount].iProperty  && vUserItemProperty[iPropertyCount].iProperty < 12 )
			{
				iaStat[ vUserItemProperty[iPropertyCount].iProperty ] += vUserItemProperty[iPropertyCount].iValue;
			}
		}
	}	
	
	return TRUE;
}

int CFSGameUser::IsSecondBoostItemEquipped(BYTE& bPropertyNum)
{
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if (NULL == pAvatarItemList) return FALSE;
	
	vector< SUserItemInfo* > vUserItemInfo;
	
	pAvatarItemList->GetEquipItemList( vUserItemInfo );
	
	for(int i=0 ; i< vUserItemInfo.size() ;i++)
	{
		if (vUserItemInfo[i]->iChannel == ITEMCHANNEL_BOOST2)					
			if (vUserItemInfo[i]->iStatus == 2)	
			{													
				int iUserItemPropertyNum = vUserItemInfo[i]->iPropertyNum;					
				if( iUserItemPropertyNum > 0 )
				{
					vector< SUserItemProperty > vUserItemProperty;
					
					pAvatarItemList->GetItemProperty(vUserItemInfo[i]->iItemIdx, vUserItemProperty);
					
					for( int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++ )
					{
						if( 0 <= vUserItemProperty[iPropertyCount].iProperty  && vUserItemProperty[iPropertyCount].iProperty < 12 )
						{
							bPropertyNum =(BYTE)vUserItemProperty[iPropertyCount].iProperty;
							break;
						}
					}
				}					
				return vUserItemInfo[i]->iPropertyTypeValue;
			}						
	}
	return 0;
}

void CFSGameUser::SendMyItemBag(CFSItemShop* pItemShop, int iUseStyle/* = -1*/)
{
	CFSGameODBC * pFSODBC= (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pFSODBC )

	SAvatarInfo * pAvatar = GetCurUsedAvatar();
	if( NULL == pAvatar ) return;
	
	CFSGameUserItem* pUserItemList = GetUserItemList();
	if( NULL == pUserItemList ) return;
	
	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;
	
	CFSGameServer *pServer = CFSGameServer::GetInstance();
	if( pServer == NULL ) return;
	
	CPacketComposer PacketComposer(S2C_MYITEM_BAG_RES);
	int iOpCode = OP1_CREATE;
	PacketComposer.Add(iOpCode);
	Send(&PacketComposer);

	time_t CurrentTime = _time64(NULL);

	pAvatarItemList->Lock();
	{
		CTypeList< SUserItemInfo* >::POSITION pos = pAvatarItemList->GetHeadPosition();
		CTypeList< SUserItemInfo* >::POSITION pEnd = pAvatarItemList->GetEndPosition();
		
		while( pos != pEnd )
		{
			SUserItemInfo* pItem = (*pos);
			
			if( pItem != NULL )
			{
				PacketComposer.Initialize(S2C_MYITEM_BAG_RES);
				iOpCode = OP2_UPDATE;
				PacketComposer.Add(iOpCode);
				
				// 20090610 ä�� 0�� ������ �ʰ��濡 �Ⱥ��̰� ��.
				if( ITEM_STATUS_RESELL == (*pos)->iStatus || 
					ITEM_STATUS_PRESENTBOX == (*pos)->iStatus || 
					ITEM_STATUS_ONLY_RESELL == (*pos)->iStatus ||
					ITEM_STATUS_EXPIRED == (*pos)->iStatus ||
					ITEM_STATUS_FACE == (*pos)->iStatus ||
					(0 == (*pos)->iSellType && ITEMCHANNEL_FACECHANGE != (*pos)->iChannel) || 
					0 == (*pos)->iChannel )
				{
					pos = pAvatarItemList->GetNextPosition(pos);
					continue;
				}
				
				// 20090523 Wondergirls Package Item
				if(	(pItem->iPropertyKind > PREMIUM_ITEM_KIND_START && pItem->iPropertyKind < PREMIUM_ITEM_KIND_END) ||
					(pItem->iPropertyKind > SHOUT_ITEM_KIND_START && SHOUT_ITEM_KIND_END > pItem->iPropertyKind) ||
					pItem->iPropertyKind == CHANGE_HEIGHT_KIND_NUM ||
					pItem->iPropertyKind == CHANGE_NAME_KIND_NUM ||
					pItem->iPropertyKind == CHANGE_NAME_SPECIAL_KIND_NUM ||
					pItem->iPropertyKind == REST_ITEM_KIND_NUM ||
					(pItem->iPropertyKind >= MIN_TREASURE_KIN_NUM && pItem->iPropertyKind <= MAX_TREASURE_KIN_NUM) ||
					(pItem->iPropertyKind >= MIN_STEP_BOX_KIN_NUM && pItem->iPropertyKind <= MAX_STEP_BOX_KIN_NUM) ||
					pItem->iPropertyKind == CHALLENGE_ITEM_KIND_NUM ||
					pItem->iPropertyKind == OFF_LINE_ITEM_KIND_NUM ||
					pItem->iPropertyKind == WONDER_GIRLS_CHARACTER_SLOT_KIND_NUM ||
					pItem->iPropertyKind == WONDER_GIRLS_CHARACTER_SLOT_KIND_NUM_EXPERT ||
					pItem->iPropertyKind == CASH_DISCOUNT_ITEM_KIND_NUM ||
					pItem->iPropertyKind == POINT_DISCOUNT_ITEM_KIND_NUM ||
					pItem->iPropertyKind == CHANGE_ITEM_COLOR_KIND_NUM
					)
				{
					pos = pAvatarItemList->GetNextPosition(pos);
					continue;
				}

				if(PacketComposer.GetFreeSize() < 26) 
				{
					pAvatarItemList->Unlock();
					return;
				}
				
				PacketComposer.Add((short)(*pos)->iItemIdx);
				PacketComposer.Add((int)(*pos)->iItemCode);
				PacketComposer.Add((BYTE)(*pos)->iBigKind);
				PacketComposer.Add((BYTE)(*pos)->iSmallKind);
				PacketComposer.Add((int)(*pos)->iChannel);
				
				if (0 < iUseStyle)
				{
					PacketComposer.Add((BYTE)CheckItemUseStyle(iUseStyle, pItem->iItemIdx));
				}
				else
				{
					int iStatus = (*pos)->iStatus;
					if( 1 == iStatus )		iStatus = 0;
					else if( 2 == iStatus )	iStatus = 1;
					PacketComposer.Add((BYTE)iStatus);
				}
				
				PacketComposer.Add((BYTE)(*pos)->iPropertyType);
				
				if( (*pos)->iPropertyType == 1 )
				{
					int iDay = 0, iHour = 0, iMin = 0;
					
					if(( (*pos)->iCategory == 3 || (*pos)->iCategory == 0) && ((*pos)->iPropertyTypeValue != -1))
					{
						int iDiffSecond = difftime((*pos)->ExpireDate, CurrentTime);	
						
						iDay = iDiffSecond / 86400;
						iHour = (iDiffSecond ) / 3600;
3600;
						iMin = (iDiffSecond ) / 60;
 60;
					}
					else
					{
						iDay = -1;
						iHour = 0;
						iMin = 0;
					}
					
					PacketComposer.Add((short)iDay);
					PacketComposer.Add((BYTE)iHour);
					PacketComposer.Add((BYTE)iMin);
				}
				else if( (*pos)->iPropertyType == 2)
				{
					PacketComposer.Add((int)(*pos)->iPropertyTypeValue);
				}
				
				int iPropertyKind = (*pos)->iPropertyKind;
				
				if( iPropertyKind >= MIN_WIN_TATTOO_KIND_NUM && iPropertyKind <= MAX_WIN_TATTOO_KIND_NUM )
				{
					iPropertyKind = 0;
				}
				
				int iUserItemPropertyNum = (*pos)->iPropertyNum;
				
				if( iPropertyKind > 0 &&
					ITEM_PROPERTY_KIND_HYBRID_STAT != iPropertyKind &&
					PROPERTY_KIND_PROPENSITY_ITEM != iPropertyKind &&
					((MIN_LINK_ITEM_PROPERTY_KIND_NUM > iPropertyKind || iPropertyKind > MAX_LINK_ITEM_PROPERTY_KIND_NUM) && ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC != iPropertyKind) &&
					(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN > iPropertyKind || iPropertyKind > ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX) &&
					(MIN_HEART_TATTO_KIND_NUM > iPropertyKind || iPropertyKind > MAX_HEART_TATTO_KIND_NUM) )
				{
					iPropertyKind = iUserItemPropertyNum;
				}
				
				PacketComposer.Add((short)iPropertyKind);

				BOOL bPCRoomItemStatOption = FALSE;
				if( (*pos)->iPropertyKind >= MIN_PREMIUM_STAT_OPTION_ITEM_NUM && (*pos)->iPropertyKind <= MAX_PREMIUM_STAT_OPTION_ITEM_NUM )	
				{
					bPCRoomItemStatOption = TRUE;
				}
				PacketComposer.Add(bPCRoomItemStatOption);
				
				vector< SUserItemProperty > vUserItemProperty;
				if( iUserItemPropertyNum > 0 )
				{
					pAvatarItemList->GetItemProperty((*pos)->iItemIdx, vUserItemProperty);
				}
				iUserItemPropertyNum = vUserItemProperty.size();
				PacketComposer.Add(&iUserItemPropertyNum);
				
				for (int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size() ; iPropertyCount++ )
				{
					int iProperty	= vUserItemProperty[iPropertyCount].iProperty;
					int iValue		= vUserItemProperty[iPropertyCount].iValue;
					
					PacketComposer.Add(&iProperty);
					PacketComposer.Add(&iValue);
					PacketComposer.Add(&vUserItemProperty[iPropertyCount].iAssignChannel);
				}
				
				PacketComposer.Add(pAvatarItemList->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));

				BYTE byAvailable = 1;
				SShopItemInfo ItemInfo;
				pItemShop->GetItem( (*pos)->iItemCode , ItemInfo );
				
				if (false == ItemInfo.IsLvCondition(pAvatar->iLv))
				{
					byAvailable = 0;
				}
				
				PacketComposer.Add(byAvailable);

				int iStyleIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > GetUseStyleIndex() ? 1 : 4 ;

				PacketComposer.Add(CheckItemUseStyle(iStyleIndex, pItem->iItemIdx));
				PacketComposer.Add(CheckItemUseStyle(iStyleIndex+1, pItem->iItemIdx));
				PacketComposer.Add(CheckItemUseStyle(iStyleIndex+2, pItem->iItemIdx));
				PacketComposer.Add(pItem->iSpecialPartsIndex);
			}
			
			Send(&PacketComposer);
			pos = pAvatarItemList->GetNextPosition(pos);
		}
	}
	pAvatarItemList->Unlock();
	
	iOpCode = OP4_END;	
	PacketComposer.Initialize( S2C_MYITEM_BAG_RES );
	PacketComposer.Add(iOpCode);
	Send(&PacketComposer);
}

void CFSGameUser::SendAllMyOwnItemInfo()
{
	
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	if( pAvatarInfo == NULL )
	{
		return;
	}
	
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	if( pItemShop == NULL ) 
		return;	
	
	vector< SShopItemInfo* > vShopItemInfo;
	pItemShop->GetAllItemInfo( pAvatarInfo->iSex, vShopItemInfo );
	if( vShopItemInfo.size() > 0 )
	{
		TRACE(" All ItemNum -> 11866902 \n", vShopItemInfo.size() );

int iOpCode = OP1_CREATE;	
	
	if( vShopItemInfo.empty() )
	{
		iOpCode = OP5_ERROR;	
		CPacketComposer PacketComposer( S2C_MY_OWN_ITEM_LIST_RES );
		PacketComposer.Add(iOpCode);
		Send(&PacketComposer);
		
		return;
	}
	
	iOpCode = OP1_CREATE;
	CPacketComposer PacketComposer( S2C_MY_OWN_ITEM_LIST_RES );
	PacketComposer.Add(iOpCode);
	Send(&PacketComposer);
	
	
	int iItemNumCount = GetUserItemList()->GetMyInventoryItemCount() + GetUserCheerLeader()->GetCheerLeaderCount();
	
	TRACE(" ------------- ItemSend Start\n ");
	
	iOpCode = OP2_UPDATE;
	PacketComposer.Initialize( S2C_MY_OWN_ITEM_LIST_RES );
	PacketComposer.Add(iOpCode);
	PacketComposer.Add(iItemNumCount);
	
	int iCount = 0;
	int iCount2 = 0;
	
	int iInMyItem = 0;

	for( iCount = 0 ; iCount < vShopItemInfo.size(); iCount++ )
	{
		iInMyItem = 0;
		if(vShopItemInfo[iCount]->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && vShopItemInfo[iCount]->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
		{
			if(vShopItemInfo[iCount]->iPropertyKind == ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_PACKAGE)
			{
				if(GetUserItemList()->IsInMyInventoryWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SET) &&
					GetUserItemList()->IsInMyInventoryWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_HAIR) &&
					GetUserItemList()->IsInMyInventoryWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_UPPER) &&
					GetUserItemList()->IsInMyInventoryWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_LOWER) &&
					GetUserItemList()->IsInMyInventoryWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SHOES))
				{
					iCount2++;
					iInMyItem = 1;
				}
			}
			else
			{
				if(GetUserItemList()->IsInMyInventoryWithPropertyKind(vShopItemInfo[iCount]->iPropertyKind))
				{
					iCount2++;
					iInMyItem = 1;
				}
			}
		}
		else if(vShopItemInfo[iCount]->iPropertyKind == ITEM_PROPERTY_KIND_CHEERLEADER)
		{
			BYTE btCheerLeaderTypeNum = 0;
			if(TRUE == GetUserCheerLeader()->CheckCheerLeader(vShopItemInfo[iCount]->iCheerLeaderIndex, btCheerLeaderTypeNum) &&
				btCheerLeaderTypeNum == vShopItemInfo[iCount]->btCheerLeaderTypeNum)
			{ 
				iCount2++;
				iInMyItem = 1;
			}
		}
		else
		{
			if( GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode0) || 
				GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode1) ||
				GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode2) ||
				GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode3) ||
				GetUserItemList()->IsInMyInventory(vShopItemInfo[iCount]->iItemCode4)	)
			{
				iCount2++;
				iInMyItem = 1;
			}
		}

		if(iInMyItem == 1)
		{
			PacketComposer.Add(vShopItemInfo[iCount]->iItemCode0);
			PacketComposer.Add(iInMyItem);
		}
	}

	
	iItemNumCount = iCount2;
	
	memcpy(PacketComposer.GetBodyBuffer()+sizeof(int), &iItemNumCount, sizeof(int));
	
	Send(&PacketComposer);
	
	TRACE(" ------------- OwnItemSend End --------->  11866902  0 \n ", iItemNumCount, iCount2);
OpCode = OP4_END;	
	PacketComposer.Initialize( S2C_MY_OWN_ITEM_LIST_RES );
	PacketComposer.Add(iOpCode);
	Send(&PacketComposer);
	
	TRACE(" ------------- OwnItemSend End    22\n ");
}

BOOL CFSGameUser::GetAndSetHeightStat( int& iNewHeight, int& iWeight, int& iOutMinHeight, int& iOutMaxHeight, int *iaChangeStat )
{
	
	SAvatarInfo * pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
	if( NULL == pAvatarInfo )	
		return FALSE;
	
	int iCharType = pAvatarInfo->iCharacterType;
	int iCurHeight = pAvatarInfo->Status.iHeight;
	
	int iaOldChangeStat[MAX_STAT_NUM];
	memset( iaOldChangeStat, 0, sizeof(int)*MAX_STAT_NUM );
	
	if( 0 != m_AvatarManager.GetAvatarChangeStat( g_pFSDBCfg, pAvatarInfo->Status.iGamePosition, iCharType, iCurHeight, iWeight, iOutMinHeight, iOutMaxHeight, iaOldChangeStat ) )
	{
		return FALSE;
	}
	
	int iaNewChangeStat[MAX_STAT_NUM];
	memset( iaNewChangeStat, 0, sizeof(int)*MAX_STAT_NUM );
	
	if( 0 != m_AvatarManager.GetAvatarChangeStat( g_pFSDBCfg, pAvatarInfo->Status.iGamePosition, iCharType, iNewHeight, iWeight, iOutMinHeight, iOutMaxHeight, iaNewChangeStat ) )
	{
		return FALSE;
	}
	
	iaChangeStat[0] =	iaNewChangeStat[0]		- iaOldChangeStat[0];
	iaChangeStat[1] =	iaNewChangeStat[1]		- iaOldChangeStat[1];
	iaChangeStat[2] =	iaNewChangeStat[2]		- iaOldChangeStat[2];
	iaChangeStat[3] =	iaNewChangeStat[3]		- iaOldChangeStat[3];
	iaChangeStat[4] =	iaNewChangeStat[4]		- iaOldChangeStat[4];
	iaChangeStat[5] =	iaNewChangeStat[5]		- iaOldChangeStat[5];
	iaChangeStat[6] =	iaNewChangeStat[6]		- iaOldChangeStat[6];
	iaChangeStat[7] =	iaNewChangeStat[7]		- iaOldChangeStat[7];
	iaChangeStat[8] =	iaNewChangeStat[8]		- iaOldChangeStat[8];
	iaChangeStat[9] =	iaNewChangeStat[9]		- iaOldChangeStat[9];
	iaChangeStat[10] =	iaNewChangeStat[10]		- iaOldChangeStat[10];
	iaChangeStat[11] =	iaNewChangeStat[11]		- iaOldChangeStat[11];

	if ( -1 != pAvatarInfo->iSpecialCharacterIndex )
	{		
		char szSpecialAvatarKey[10];
		memset( szSpecialAvatarKey, 0, 10 );
		sprintf_s(szSpecialAvatarKey, _countof(szSpecialAvatarKey), "11866902", pAvatarInfo->iSpecialCharacterIndex );
ecialAvatarStatInfo OldSpecialAvatarStatInfo;
		int iIsPropensityItem = -1;
		int iDataPosition = pAvatarInfo->Status.iGamePosition;
		if ( POSITION_POWER_FORWARD == iDataPosition || POSITION_SMALL_FORWARD == iDataPosition )
		{
			iDataPosition = POSITION_FORWARD;
		}
		else if ( POSITION_POINT_GUARD == iDataPosition || POSITION_SHOOTING_GUARD == iDataPosition )
		{
			iDataPosition = POSITION_GUARD;
		}

		if( AVATARCREATEMANAGER.GetSpecialAvatarStatus(szSpecialAvatarKey, 1, iCurHeight, iDataPosition, iIsPropensityItem, OldSpecialAvatarStatInfo ) )
		{
			SpecialAvatarStatInfo NewSpecialAvatarStatInfo;
			if( AVATARCREATEMANAGER.GetSpecialAvatarStatus(szSpecialAvatarKey, 1, iNewHeight, iDataPosition, iIsPropensityItem, NewSpecialAvatarStatInfo ) )
			{
				iaChangeStat[0] =	NewSpecialAvatarStatInfo.iaAddStat[0]		- OldSpecialAvatarStatInfo.iaAddStat[0];
				iaChangeStat[1] =	NewSpecialAvatarStatInfo.iaAddStat[1]		- OldSpecialAvatarStatInfo.iaAddStat[1];
				iaChangeStat[2] =	NewSpecialAvatarStatInfo.iaAddStat[2]		- OldSpecialAvatarStatInfo.iaAddStat[2];
				iaChangeStat[3] =	NewSpecialAvatarStatInfo.iaAddStat[3]		- OldSpecialAvatarStatInfo.iaAddStat[3];
				iaChangeStat[4] =	NewSpecialAvatarStatInfo.iaAddStat[4]		- OldSpecialAvatarStatInfo.iaAddStat[4];
				iaChangeStat[5] =	NewSpecialAvatarStatInfo.iaAddStat[5]		- OldSpecialAvatarStatInfo.iaAddStat[5];
				iaChangeStat[6] =	NewSpecialAvatarStatInfo.iaAddStat[6]		- OldSpecialAvatarStatInfo.iaAddStat[6];
				iaChangeStat[7] =	NewSpecialAvatarStatInfo.iaAddStat[7]		- OldSpecialAvatarStatInfo.iaAddStat[7];
				iaChangeStat[8] =	NewSpecialAvatarStatInfo.iaAddStat[8]		- OldSpecialAvatarStatInfo.iaAddStat[8];
				iaChangeStat[9] =	NewSpecialAvatarStatInfo.iaAddStat[9]		- OldSpecialAvatarStatInfo.iaAddStat[9];
				iaChangeStat[10] =	NewSpecialAvatarStatInfo.iaAddStat[10]		- OldSpecialAvatarStatInfo.iaAddStat[10];
				iaChangeStat[11] =	NewSpecialAvatarStatInfo.iaAddStat[11]		- OldSpecialAvatarStatInfo.iaAddStat[11];
			}
		}
	}

	return TRUE;
	
}

BOOL CFSGameUser::UpdateShoutCount(int iItemIdx)
{
	CFSGameUserItem* pUserItem = GetUserItemList();
	if( NULL == pUserItem ) 
		return FALSE;
	
	CAvatarItemList* pItemList = pUserItem->GetCurAvatarItemList();
	if( NULL == pItemList ) 
		return FALSE;
	
	SUserItemInfo* pItem = pItemList->GetItemWithItemIdx(iItemIdx);
	if( NULL == pItem ) 
		return FALSE;
	
	if( --pItem->iPropertyTypeValue <= 0 )
	{
		pItem->iPropertyTypeValue = 0;
		pItem->iStatus = 0;
	}
	
	return TRUE;
}


void CFSGameUser::SendMySkillBag(CFSSkillShop* pSkillShop)
{
	CFSGameODBC * pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pFSODBC );

	SAvatarInfo * pAvatar = GetCurUsedAvatar();
	if( NULL == pAvatar ) return;
	
	CFSGameUserSkill* pUserSkill = GetUserSkill();
	if( NULL == pUserSkill ) return;
	
	pUserSkill->CheckSkillSlotExpired();
	
	CPacketComposer PacketComposer(S2C_MYSKILL_BAG_RES);
	
	BYTE bySlotCount = pAvatar->GetTotalSkillSlotCount( 0 );
	BYTE byPCSlotCount = 0;
	BYTE byVIPSlotCount = 0;
	
	int iBindAccountItemPropertyKind = GetBindAccountItemPropertyKind();
	if(BIND_ACCOUNT_ITEM_KIND_VIP_PLUS == iBindAccountItemPropertyKind)
	{
		CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
		CHECK_NULL_POINTER_VOID(pItemShop);
		
		CShopItemList* pShopItemList = pItemShop->GetShopItemList();
		CHECK_NULL_POINTER_VOID(pShopItemList);
		SBindAccountItemBonusData* pBindAccountItemBonus = pShopItemList->GetBindAccountBonusData(iBindAccountItemPropertyKind);
		CHECK_NULL_POINTER_VOID(pBindAccountItemBonus);

		byVIPSlotCount  = (BYTE)pBindAccountItemBonus->iBonusSkillSlotCount;
	}
	else if( bySlotCount < MAX_SKILL_SLOT && PCROOM_KIND_PREMIUM == GetPCRoomKind() )
	{
		byPCSlotCount = PREMIUM_PCROOM_SKILL_SLOT_BONUS_KOREA;	
	}
	
	PBYTE pChangeLoc = PacketComposer.GetTail();
	PacketComposer.Add(bySlotCount);
	PacketComposer.Add(byPCSlotCount);
	PacketComposer.Add(byVIPSlotCount);
	
	for( int iType = SKILL_TYPE_SKILL; iType <= SKILL_TYPE_FREESTYLE; ++iType )
	{
		PacketComposer.Add((BYTE)iType);
		if( FALSE == pUserSkill->GetSlotSkillList(pSkillShop, iType, PacketComposer) 
			|| FALSE == pUserSkill->GetInventorySkillList(pSkillShop, iType, PacketComposer) )
		{
			PacketComposer.Initialize(S2C_MYSKILL_BAG_RES);
			PacketComposer.Add((BYTE)0);
			break;
		}
	}
	
	Send(&PacketComposer);
}

BOOL CFSGameUser::SetNewCharacterName( char* szNewGameID, int iItemIdx, int iPropertyKind)
{
	CFSGameODBC * pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pFSODBC );

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(pServer == NULL)
		return FALSE;
	
	BOOL bIsValidGameID = FALSE;
	
	bIsValidGameID = pServer->GetGameIDStringChecker()->IsValidGameIDCharacter(szNewGameID, iPropertyKind);
	
	if(bIsValidGameID == TRUE)
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spChangeCharacterName( m_iUserIDIndex, GetGameIDIndex(), szNewGameID, iItemIdx, GetFactionIndex() ) )
		{
			return FALSE;
		}
		
		SetAvatarGameID( szNewGameID );
	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL CFSGameUser::CheckGameID(char *szGameID)
{
	int iStrlen = strlen(szGameID);
	if( 0 ==  iStrlen ) 
		return FALSE;
	
	
	for(int i=0; i<iStrlen ; i++)
	{
		if( szGameID[i]&0x80 && szGameID[i+1] != NULL )
		{
			unsigned char	cBuf1=		szGameID[i];
			unsigned char	cBuf2=		szGameID[i+1];
			unsigned short	wBuf=		(unsigned short)cBuf1<<8 | cBuf2;
			
			if( !(wBuf >= 0xB0A1  &&  wBuf <= 0xC8FE) ) return FALSE;
			i++;
			
		}
		else
		{
			unsigned char cBuf = szGameID[i];
			if(cBuf >= 'a'  &&  cBuf <= 'z') {	; }
			else if(cBuf >= 'A'  &&  cBuf <= 'Z') {	; }
			else if(cBuf >= '0'  &&  cBuf <= '9') {	; }
			else return FALSE;
		}
		
	}
	
	return TRUE;
}

BOOL CFSGameUser::SetAvatarGameID(LPSTR szNewGameID)
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	if( NULL == pAvatar )	
		return FALSE;
	
	memcpy( pAvatar->szGameID, szNewGameID, MAX_GAMEID_LENGTH+1 );	
	
	return TRUE;
}

int CFSGameUser::CheckAndGetSelfProtectionRingCount()
{
	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	
	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if(NULL != pAvatarInfo && NULL != pAvatarItemList)
	{
		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != pAvatarInfo->AvatarFeature.FeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]);
				if(NULL != pSlotItem)
				{
					if(PROTECT_RING_ITEM_KIND_NUM == pSlotItem->iPropertyKind && 2 == pSlotItem->iStatus)
					{
						return pSlotItem->iPropertyTypeValue;
					}
				}
			}
		}
	}
	
	return -1;
}

BOOL CFSGameUser::CheckAndGetProtectHand( int& iPropertyKind, int& iNum )
{
	iPropertyKind = -1;
	iNum = 0;

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if(NULL != pAvatarInfo && NULL != pAvatarItemList)
	{
		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != pAvatarInfo->AvatarFeature.FeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]);
				if(NULL != pSlotItem)
				{
// 					if( ( PROTECT_HAND1_ITEM_KIND_NUM == pSlotItem->iPropertyKind  ||
// 						PROTECT_HAND2_ITEM_KIND_NUM == pSlotItem->iPropertyKind ||
// 						PROTECT_HAND3_ITEM_KIND_NUM == pSlotItem->iPropertyKind ) 
// 						&& 2 == pSlotItem->iStatus)
					if ( PROTECT_HAND1_ITEM_KIND_NUM <= pSlotItem->iPropertyKind &&
						 PROTECT_HAND7_ITEM_KIND_NUM >= pSlotItem->iPropertyKind &&
						 2 == pSlotItem->iStatus )
					{
						iPropertyKind = pSlotItem->iPropertyKind;
						iNum = pSlotItem->iPropertyTypeValue;
						return TRUE;
					}
				}
			}
		}
	}
	return FALSE;
}

BOOL CFSGameUser::CheckMacroChatItem()
{
	SAvatarInfo * pAvatarInfo = NULL;
	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( pAvatarInfo == NULL || NULL == pAvatarItemList ) 
		return FALSE;
	
	SUserItemInfo * pSlotItem = NULL;
	
	pSlotItem  = pAvatarItemList->GetPossessionItemWithPropertyKind(MCHAT_MACRO_EAR_RING_KIND_NUM);
	
	if (NULL != pSlotItem)
	{
		return TRUE;
	}
	
	return FALSE;
}

void CFSGameUser::SetEqualizeAvatarStatus(CFSGameODBC * pFSODBC,int iGameIDIndex, int iEqualizeLV)
{
	int iPosition = 0;

	CFSGameServer *pServer = CFSGameServer::GetInstance();
	if(pServer == NULL) return;

	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;

	if(GetCurUsedAvtarLv() < GAME_LEAGUE_UP_PRO)
	{
		iPosition = GetEqualizePosition( pAvatarInfo->Status.iGamePosition );
	}
	else
	{
		iPosition = pAvatarInfo->Status.iGamePosition;
	}

	if(iEqualizeLV > 0)
	{
		if( FALSE == m_AvatarManager.SetEqualizeAvatarStatus( pFSODBC, iGameIDIndex, iEqualizeLV, iPosition) )
		{
			WRITE_LOG_NEW(LOG_TYPE_AVATAR, EXEC_FUNCTION, FAIL, "SetEqualizeAvatarStatus - GameIDIndex:11866902", iGameIDIndex);
urn;
		}
	}
}

BOOL CFSGameUser::SetAvatarFameSkillStatus(int* iStat)
{
	BOOL bRet = FALSE;

	CFSGameUserSkill* pUserSkill = GetUserSkill();
	if(NULL != pUserSkill)
	{
		bRet = pUserSkill->GetFameSkillStatus(iStat);
	}

	return bRet;
}

BOOL CFSGameUser::SetAvatarFameSkillAddMinusStatus(int* iAddStat, int* iMinusStat)
{
	BOOL bRet = FALSE;

	CFSGameUserSkill* pUserSkill = GetUserSkill();
	if(NULL != pUserSkill)
	{
		bRet = pUserSkill->GetFameSkillAddMinusStatus(iAddStat, iMinusStat);
	}

	return bRet;
}

int CFSGameUser::GetSkillSlotCount(SExtraSkillSlot* pSkillSlot,int iPresentItemCode)
{
	int iSkillSlotItemCount = 5;
	
	if( iPresentItemCode != -1)
	{
		if(iPresentItemCode == 200003 || iPresentItemCode == 200004)
		{
			iSkillSlotItemCount += 10;
		}
		else if(iPresentItemCode == 200002)
		{
			iSkillSlotItemCount += 5;
		}
	}
	
	for(int i =0; i< MAX_SKILL_SLOT_ITEM; i++)
	{
		if(pSkillSlot[i].bUse == TRUE)
		{
			iSkillSlotItemCount+= pSkillSlot[i].sSlotCount;
		}
	}
	
	return iSkillSlotItemCount;
}

BOOL CFSGameUser::CheckSkillSlotExchangeStatus(SExtraSkillSlot* pSkillSlot, int iItemCode)
{
	int iSkillSlotItemCount = 5;
	if(SKILL_SLOT_ITEMCODE == iItemCode)
	{
		iSkillSlotItemCount += 5;
	}

	BOOL bIsTimeSlot = FALSE;
	for(int i = 0; i < MAX_SKILL_SLOT_ITEM; i++)
	{
		if(TRUE == pSkillSlot[i].bUse)
		{
			iSkillSlotItemCount += pSkillSlot[i].sSlotCount;

			if(-1 != pSkillSlot[i].iUseTerm)
				bIsTimeSlot = TRUE;
		}
	}

	if(iSkillSlotItemCount >= MAX_SKILL_SLOT_OTHERS &&
		TRUE == bIsTimeSlot)
	{
		return TRUE;
	}

	return FALSE;
}

// 20090818 �ѱ� Ű ����
void CFSGameUser::GetScaledHeight( int iOriginalHeight, int& iScaledHeight )
{
	int iValue = 0;
	float fTempHeight = (float)iOriginalHeight;
	
	if( fTempHeight > 200.0f )
	{	
		iValue = (int)fTempHeight ;
00;
		fTempHeight = 200.0f + (float)iValue / 1.80f;
	}

	iScaledHeight = (int)fTempHeight;
}

void CFSGameUser::SetDataForCheckHack(SAvatarHackCheckInfo& info)
{
	memcpy(&m_AvatarStatCheckInfo, &info, sizeof(SAvatarHackCheckInfo));
}

// 20090401 TWN service 'Quick Charge Item' task - modified member total stat in class game user for using boost item.
void CFSGameUser::UpdateTotalStatusByIndex( const int& iStatusIndex, const int& iStatusValue )
{
	if( iStatusIndex > -1 && iStatusIndex < MAX_STAT_NUM )
	{
		m_AvatarStatCheckInfo.iaTotalStat[iStatusIndex] += iStatusValue;
	}
}
// End

void CFSGameUser::ProcessCheckAvatarStatus(CReceivePacketBuffer* pRecvBuffer)
{
	CHECK_NULL_POINTER_VOID(pRecvBuffer);
	CHECK_CONDITION_RETURN_VOID(FALSE == HACKINGMANAGER.IsEnable(HACK_TYPE_CHANGE_STATUS));
	CHECK_CONDITION_RETURN_VOID(TRUE == IsHackUser());

	RC4Decrypt(GetRc4Key(),	(char*)pRecvBuffer->GetBuffer() ,pRecvBuffer->GetDataSize());

	int iRecvCommand = pRecvBuffer->GetCommand();
	if (GetCheckStatPacketNo() != iRecvCommand - 1)
	{
		SetCheckStatPacketNo(0);
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid packet No.");
		return;
	}
	SetCheckStatPacketNo(0);

	BYTE btSafeConstantFirstChkSum = 0;
	BYTE btCheckSeed = 0;
	float fCheckRimPosition[MAX_RIM_POSITION_IDX_COUNT] = {0};
	int iaClientStatus[MAX_STAT_NUM] = {0};
	int iaSkill[MAX_SKILL_STORAGE_COUNT];
	int iaFreeStyle[MAX_SKILL_STORAGE_COUNT];
	int iClientHeight = 0;	

	BYTE btSpecialtySkillCount = 0;
	SAvatarSpecialtySkill SpecialtySkill;
	BYTE btStatCount = 0;
	SSpecialtyStat SpecialtyStat;
	list<SAvatarSpecialtySkill> listSpecialtySkill;

	int iCRShootSuccessRate = 0, iCRBlockSuccessRate = 0;
	float fFrameRate = 0, fSentenceShootSuccessRate = 0, fSentenceBlockSuccessRate = 0
		, fSpecialPartsShootSuccessRate = 0, fSpecialPartsBlockSuccessRate = 0;

	int iTransformAbilityType = 0, iTransformAbilityValue = 0, iFaceID = 0;
	int iSpecialPieceStatCount = 0, iSpecialPieceProperty = -1;
	int iaSpecialPieceProperty[MAX_STAT_NUM];
	ZeroMemory(iaSpecialPieceProperty, sizeof(int)*MAX_STAT_NUM);

	pRecvBuffer->Read(&btSafeConstantFirstChkSum);
	pRecvBuffer->Read(&btCheckSeed);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_X]);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_Y]);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_Z]);
	for(int i = 0; i < MAX_STAT_NUM; i++ )
	{
		pRecvBuffer->Read(&iaClientStatus[i]);
	}
	pRecvBuffer->Read(&iClientHeight);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_1]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_2]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_2]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_3]);	//��ų��ȣ Ȯ�� Ŭ��� �Բ� �۾��� �ּ�����
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_0]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_1]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_0]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_3]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_4]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_4]);

	pRecvBuffer->Read(&fFrameRate);

	//���� ������� ������
	BYTE btAbilityCount = 0;
	pRecvBuffer->Read(&btAbilityCount);
	//
	pRecvBuffer->Read(&iCRShootSuccessRate);
	pRecvBuffer->Read(&iCRBlockSuccessRate);

	pRecvBuffer->Read(&btSpecialtySkillCount);
	for (int i = 0; i < btSpecialtySkillCount && i < MAX_SPECIALTY_SKILL_COUNT_ON_USER; ++i)
	{
		pRecvBuffer->Read(&SpecialtySkill.iKind);
		pRecvBuffer->Read(&SpecialtySkill.iSkillNo);
		pRecvBuffer->Read(&SpecialtySkill.btSpecialtyNo);

		pRecvBuffer->Read(&btStatCount);
		for (int j = 0; j < btStatCount && j < MAX_SPECIALTY_SKILL_STAT_COUNT; ++j)
		{
			pRecvBuffer->Read(&SpecialtyStat.btSpecialtyStatType);
			pRecvBuffer->Read(&SpecialtyStat.iStatValue);
			SpecialtySkill.vecStat.push_back(SpecialtyStat);
		}

		listSpecialtySkill.push_back(SpecialtySkill);
		SpecialtySkill.vecStat.clear();
	}

	pRecvBuffer->Read(&fSentenceShootSuccessRate);
	pRecvBuffer->Read(&fSentenceBlockSuccessRate);
	pRecvBuffer->Read(&fSpecialPartsShootSuccessRate);
	pRecvBuffer->Read(&fSpecialPartsBlockSuccessRate);

	pRecvBuffer->Read(&iTransformAbilityType);
	pRecvBuffer->Read(&iTransformAbilityValue);
	pRecvBuffer->Read(&iFaceID);

	pRecvBuffer->Read(&iSpecialPieceStatCount);

	int iCheckSpecialPieceStatCount = 0;
	for (int i = 0; i < iSpecialPieceStatCount && i < MAX_STAT_NUM; ++i)
	{
		pRecvBuffer->Read(&iSpecialPieceProperty);
		if(iSpecialPieceProperty >= 0 && iSpecialPieceProperty < MAX_STAT_NUM)
		{
			pRecvBuffer->Read(&iaSpecialPieceProperty[iSpecialPieceProperty]);
			iCheckSpecialPieceStatCount++;
		}
	}

	if (FALSE == CheckAvatarStatus(btCheckSeed, iaClientStatus, iClientHeight , iaSkill, iaFreeStyle, iFaceID) )
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "CheckAvatarStatus");
		return;
	}

	if (GetSafeConstantFirstChkSum() != btSafeConstantFirstChkSum) 
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Mismatch CheckSeed 1\tUse=11866902\tServer=0", btSafeConstantFirstChkSum, GetSafeConstantFirstChkSum());
urn;
	}

	if (0 < btSpecialtySkillCount)
	{
		if (FALSE == GetUserSkill()->CheckSpecialtySkill(btSpecialtySkillCount, listSpecialtySkill))
		{
			SetHackUser(TRUE);
			HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpeicialtySkill\tCount=11866902",btSpecialtySkillCount );
urn;
		}
	}

	if(FALSE == CheckCheerLeaderAbility(iCRShootSuccessRate, iCRBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid CLAbility");
		return;
	}

	////������Ʈ ��� ��ġ�� �˻�
	//if (DEFAULT_RIM_POSITION_X != fCheckRimPosition[RIM_POSITION_IDX_X] ||
	//	DEFAULT_RIM_POSITION_Y != fCheckRimPosition[RIM_POSITION_IDX_Y] ||
	//	DEFAULT_RIM_POSITION_Z != fCheckRimPosition[RIM_POSITION_IDX_Z])
	//{
	//	SetHackUser(TRUE);
	//	HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid RimPosition");
	//	return;
	//}

	if (DEFAULT_FRAME_RATE != fFrameRate)
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid FrameRate");
		return;
	}

	if(FALSE == GetUserCharacterCollection()->CheckSentenceStatus(fSentenceShootSuccessRate, fSentenceBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SentenceRate");
		return;
	}

	if(FALSE == CheckSpecialPartsOptionProperty(fSpecialPartsShootSuccessRate, fSpecialPartsBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPartsRate");
		return;
	}

	if(FALSE == CheckTransformAbility(iTransformAbilityType, iTransformAbilityValue))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid TransformAbility");
		return;
	}

	if(iCheckSpecialPieceStatCount != iSpecialPieceStatCount)
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPieceAbility Count");
		return;
	}

	if(FALSE == GetUserSpecialPiece()->CheckSpecialPieceProperty(iSpecialPieceStatCount, iaSpecialPieceProperty))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPieceAbility");
		return;
	}
}

void CFSGameUser::ProcessCheckSumStatus( CReceivePacketBuffer* pRecvBuffer )
{
	CHECK_NULL_POINTER_VOID(pRecvBuffer);
	CHECK_CONDITION_RETURN_VOID(FALSE == HACKINGMANAGER.IsEnable(HACK_TYPE_CHANGE_CHECKSUM));
	CHECK_CONDITION_RETURN_VOID(TRUE == IsHackUser());

	RC4Decrypt(GetChecksum_Rc4Key(),	(char*)pRecvBuffer->GetBuffer() ,pRecvBuffer->GetDataSize());

	int iRecvCommand = pRecvBuffer->GetCommand();
	if (GetCheckStatCheckSumPacketNo() != iRecvCommand - 1)
	{
		SetCheckStatCheckSumPacketNo(0);
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_CHECKSUM, this, "Invalid packet No.");
		return;
	}
	SetCheckStatCheckSumPacketNo(0);

	int iCheckSumVal = 0;
	pRecvBuffer->Read(&iCheckSumVal);

	if( 0 != iCheckSumVal )
	{
		int iServerCheckSumval = 0;
		switch(iRecvCommand)
		{
		case C2S_CHECK_STATUS_CHECKSUM_RES:		iServerCheckSumval = GetAvatarStateCheckSum(); break;
		case C2S_CHECK_STATUS_CHECKSUM1_RES:	iServerCheckSumval = GetAvatarStateCheckSum1(); break;
		case C2S_CHECK_STATUS_CHECKSUM2_RES:	iServerCheckSumval = GetAvatarStateCheckSum2(); break;
		default:	return;
		}

		if( iServerCheckSumval != iCheckSumVal )
		{
			SetHackUser(TRUE);
			HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_CHECKSUM, this, "Invalid CheckSum");
			return;
		}
	}
}

unsigned int CFSGameUser::GetAvatarStateCheckSum()
{
	unsigned int retval = 0;

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_CONDITION_RETURN( NULL == pAvatar , -1 );
	CHECK_CONDITION_RETURN( NULL == m_UserItem , -1 );

	m_CheckSum.Clear();
	
	CHECK_SUM_INIT();

	BYTE btSafeConstantFirstCheckSum = GetSafeConstantFirstChkSum();

	ADD_CHECK_SUM_VALUE(btSafeConstantFirstCheckSum);
	ADD_CHECK_SUM_VALUE(m_btStatCheckSeed);

	float fDefault_Rim_Pos_X = (float)DEFAULT_RIM_POSITION_X;
	float fDefault_Rim_Pos_Y = (float)DEFAULT_RIM_POSITION_Y;
	float fDefault_Rim_Pos_Z = (float)DEFAULT_RIM_POSITION_Z;

	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_X);
	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_Y);
	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_Z);
	
	for( int i = 0 ; i < MAX_STAT_NUM ; ++i )
	{
		ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaTotalStat[i]);
	}

	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iHeight);

	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_1]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_2]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_2]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_3]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_0]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_1]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_0]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_3]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_4]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_4]);

	BYTE btSpecialtyCount;
	list<SAvatarSpecialtySkill> listSpecialtySkill;
	GetUserSkill()->GetSpecialSkilllist(listSpecialtySkill ,btSpecialtyCount, pAvatar->Skill);

	float fFrame_Rate = (float)DEFAULT_FRAME_RATE;
	ADD_CHECK_SUM_VALUE(fFrame_Rate);

	BYTE btAbilityCount = 0;
	ADD_CHECK_SUM_VALUE(btAbilityCount);

	int iCheerShootSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderShootSuccessRate(m_AvatarStatCheckInfo.iCheerLeaderCode);
	int iCheerBlockSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderBlockSuccessRate(m_AvatarStatCheckInfo.iCheerLeaderCode);

	ADD_CHECK_SUM_VALUE(iCheerShootSuccessRate);
	ADD_CHECK_SUM_VALUE(iCheerBlockSuccessRate);

	ADD_CHECK_SUM_VALUE(btSpecialtyCount);
	
	if( 0 < listSpecialtySkill.size() )
	{
		listSpecialtySkill.sort(opSortSpecialty);
	}

	list<SAvatarSpecialtySkill>::iterator	 iter = listSpecialtySkill.begin();
	for( ; iter != listSpecialtySkill.end() ; ++iter )
	{
		ADD_CHECK_SUM_VALUE((*iter).iKind);
		ADD_CHECK_SUM_VALUE((*iter).iSkillNo);
		ADD_CHECK_SUM_VALUE((*iter).btSpecialtyNo);

		BYTE btStatCount = (BYTE)(*iter).vecStat.size();

		ADD_CHECK_SUM_VALUE(btStatCount);
		for( unsigned int i = 0 ; i < (*iter).vecStat.size() ; ++i )
		{
			ADD_CHECK_SUM_VALUE((*iter).vecStat[i].btSpecialtyStatType);
			ADD_CHECK_SUM_VALUE((*iter).vecStat[i].iStatValue);
		}
	}

	float fSentenceShootSuccessRate = 0, fSentenceBlockSuccessRate = 0;
	GetUserCharacterCollection()->GetSentenceStatus( fSentenceShootSuccessRate, fSentenceBlockSuccessRate );
	ADD_CHECK_SUM_VALUE(fSentenceShootSuccessRate);
	ADD_CHECK_SUM_VALUE(fSentenceBlockSuccessRate);

	float fSpecialPartsShootSuccessRate = 0, fSpecialPartsBlockSuccessRate = 0;
	GetSpecialPartsOptionProperty(fSpecialPartsShootSuccessRate, fSpecialPartsBlockSuccessRate);
	ADD_CHECK_SUM_VALUE(fSpecialPartsShootSuccessRate);
	ADD_CHECK_SUM_VALUE(fSpecialPartsBlockSuccessRate);
	
	ADD_CHECK_SUM_VALUE(pAvatar->Status.iTransformAbilityType);
	ADD_CHECK_SUM_VALUE(pAvatar->Status.iTransformAbilityValue);

	int iOrder = 0, iProperty = -1, iValue = 0;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}
	
	iOrder = 1;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	iOrder = 2;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	retval = m_CheckSum.Get();

	return retval;
}

BOOL CFSGameUser::CheckAvatarStatus(BYTE btCheckSeed, const int* pClientStatus,  int iHeightStatus, int iaSkill[MAX_SKILL_STORAGE_COUNT], int iaFreeStyle[MAX_SKILL_STORAGE_COUNT], const int iFaceID)
{
	if( pClientStatus == NULL)
	{
		return TRUE;
	}
	
	if (btCheckSeed != m_btStatCheckSeed)
	{
		WRITE_LOG_NEW(LOG_TYPE_HACKING, INVALED_DATA, CHECK_FAIL, "STAT_LOG\tMismatch CheckSeed 2\tUserIndex=11866902\tUserSeed=0\tServerSeed=18227200", GetUserIDIndex(), btCheckSeed, m_btStatCheckSeed);
LSE;
	}

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	if(TRANSFORM_DRAGONTIGER == pAvatar->GetTransformType())
	{
		for( int iStatIndex = 0; iStatIndex < MAX_STAT_NUM; ++iStatIndex )
		{
			// �ΰ��� ���� ���� �������� ���� ĳ���� �ΰ�, or �⺻���� ������ ��� ���� �ΰ�,
			int iServerStat = (iFaceID == pAvatar->iFace || iStatIndex >= MAX_STAT_TYPE_COUNT) ? m_AvatarStatCheckInfo.iaTotalStat[iStatIndex] : m_AvatarStatCheckInfo.iaTotalStat_Clone[iStatIndex];

			if(pClientStatus[iStatIndex] != iServerStat)
			{
				WRITE_LOG_NEW(LOG_TYPE_HACKING, INVALED_DATA, CHECK_FAIL, "STAT_LOG\tMismatch Stat\tGameID=�4\tStat[0]\tServer(18227200)!=Client(-858993460), FaceID=-858993460", GetGameID(), iStatIndex, iServerStat, pClientStatus[iStatIndex], iFaceID);
}
		}
	}
	else
	{
		for( int iStatIndex = 0; iStatIndex < MAX_STAT_NUM; ++iStatIndex )
		{
			if( pClientStatus[iStatIndex] != m_AvatarStatCheckInfo.iaTotalStat[iStatIndex] )
			{
				WRITE_LOG_NEW(LOG_TYPE_HACKING, INVALED_DATA, CHECK_FAIL, "STAT_LOG\tMismatch Stat\tGameID=�4\tStat[0]\tServer(18227200)!=Client(-858993460)", GetGameID(), iStatIndex, m_AvatarStatCheckInfo.iaTotalStat[iStatIndex], pClientStatus[iStatIndex]);
SE;
			}
		}
	}	
	
	if( m_AvatarStatCheckInfo.iHeight != iHeightStatus )
	{
		WRITE_LOG_NEW(LOG_TYPE_HACKING, INVALED_DATA, CHECK_FAIL, "STAT_LOG\tMismatch Height\tGameID=�4\tServer(0)!=Client(18227200)",GetGameID(),m_AvatarStatCheckInfo.iHeight ,iHeightStatus);
rn FALSE;
	}

	int iHackingCaseNumber = 0;
	int iIdx = 0;
	int iCheckSkillData, iSkillData;

	for (int i = 0; i < MAX_SKILL_STORAGE_COUNT; ++i)	//��ų��ȣ Ȯ�� Ŭ��� �Բ� �۾��� SKILL_STORAGE_3 - > MAX_SKILL_STORAGE_COUNT ����
	{
		++iIdx;
		if (m_AvatarStatCheckInfo.iaSkill[i] != iaSkill[i])
		{
			iCheckSkillData = m_AvatarStatCheckInfo.iaSkill[i];
			iSkillData = iaSkill[i];
			iHackingCaseNumber = iIdx;
			break;
		}
	}
	for (int i = 0; i < MAX_SKILL_STORAGE_COUNT; ++i)
	{
		++iIdx;
		if (m_AvatarStatCheckInfo.iaFreeStyle[i] != iaFreeStyle[i])
		{
			iCheckSkillData = m_AvatarStatCheckInfo.iaFreeStyle[i];
			iSkillData = iaFreeStyle[i];
			iHackingCaseNumber = iIdx;
			break;
		}
	}

	if ( iHackingCaseNumber != 0 )
	{
		WRITE_LOG_NEW(LOG_TYPE_HACKING, INVALED_DATA, CHECK_FAIL, "Hack User Detected\tMismatch Having Skills\tCase=11866902\tServerData=0\tClientData=18227200", iHackingCaseNumber , iCheckSkillData , iSkillData );
LSE;
	}

	return TRUE;	
}

// 20090610 Add Tournament Trophy System
void CFSGameUser::SetUserTrophy(int iUserTrophy, BOOL bNeedSync/* = FALSE*/)
{
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if(NULL != pAvatarInfo)
	{
		pAvatarInfo->nTrophyCount = iUserTrophy;
	}
}

void CFSGameUser::SubtractTrophy(int iSubtractCount)
{
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if(NULL != pAvatarInfo)
	{
		pAvatarInfo->nTrophyCount -= iSubtractCount;
	}
}
BOOL CFSGameUser::CheckLvUp(int iGetExp, CFSGameODBC* pGameODBC)
{
	SAvatarInfo AvatarInfo;
	if(FALSE == CurUsedAvatarSnapShot(AvatarInfo)) 
	{
		return FALSE;
	}

	BOOL bResult = FALSE;
	int iLevelUpCount = 0;
	int iLv = AvatarInfo.iLv + 1;
	int iUserExp = AvatarInfo.iExp + iGetExp;
	BOOL bExpUp = FALSE;

	while(GAME_LVUP_MAX >= iLv && iUserExp >= CFSGameServer::GetInstance()->GetLvUpExp(iLv))
	{
		iLevelUpCount++;
		iLv++;
	}

	if(iUserExp > AvatarInfo.iExp)
	{
		bExpUp = TRUE;
	}

	if(0 > iLevelUpCount || MAX_EANBLE_LEVLE_UP_COUNT < iLevelUpCount)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, CHECK_FAIL, "CheckLvUp - LevelUpCoun:11866902, GameIDIndex:0", iLevelUpCount, AvatarInfo.iGameIDIndex);
else if(0 < iLevelUpCount || bExpUp == TRUE)
	{
		SAvatarInfo* pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
		if(NULL != pAvatarInfo)
		{
			if(0 < iUserExp)
			{
				pAvatarInfo->iExp = iUserExp;
			}

			SetItemTypeLevelUpCount(iLevelUpCount);
			pAvatarInfo->iLv = pAvatarInfo->iLv + iLevelUpCount;

			bResult = TRUE;

			vector<int> vLevel;
			vLevel.push_back(pAvatarInfo->iLv);
			ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_LEVEL, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vLevel);

			if( m_iMaxAvatarLevel < pAvatarInfo->iLv )
				m_iMaxAvatarLevel = pAvatarInfo->iLv;
		}
	}
	
	return bResult;
}


BOOL CFSGameUser::ProcessStatUp(CFSGameODBC* pGameODBC)
{
	// 2010.02.23
	// ���Ⱥ���
	// ktKim
	if(TRUE == CFSGameServer::GetInstance()->GetOptionStatusMod())
	{
		return StatusUp(pGameODBC);
	}
	// End

	CHECK_NULL_POINTER_BOOL(pGameODBC);
		
	int iStartIdx = 0, iElementNum = 0;
	int iaStat[MAX_STAT_NUM];
	memset(iaStat, 0, sizeof(int)*MAX_STAT_NUM);
	
	SAvatarInfo AvatarInfo;
	if(FALSE == CurUsedAvatarSnapShot(AvatarInfo))
	{
		return FALSE;
	}
	
	SFSConfigInfo outGameCfgInfo;
	
	switch(AvatarInfo.Status.iGamePosition)
	{
	case POSITION_CODE_CENTER:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_CENTER, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_FOWARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_FOWARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_GUARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_GUARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_POWER_FOWARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_POWER_FOWARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_SMALL_FOWARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_SMALL_FOWARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_POINT_GUARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_POINT_GUARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_SHOOT_GUARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_SHOOT_GUARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_SWING_MAN:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_SWING_MAN, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	}
	
	int iaFastStatUp[10], iaNormalFastStatUp[10] , iaNormalStatUp[10], iaNormalSlowStatUp[10] , iaSlowStatUp[10];
	
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaFastStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+2,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaNormalFastStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+4,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaNormalStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+6,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaNormalSlowStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+8,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaSlowStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	
	int iLv = AvatarInfo.iLv + 1;
	int iLvUpCount = GetItemTypeLevelUpCount();
		
	/*if (iLvUpCount <=0 || iLvUpCount > MAX_EANBLE_LEVLE_UP_COUNT)
	{

	}*/

	SUserStatUpConfig outStatUpCfg;
	memset(&outStatUpCfg, 0, sizeof(SUserStatUpConfig));
	
	for (iLv;iLv < (iLvUpCount + AvatarInfo.iLv + 1);iLv++)
	{
		for(int i=0;i<MAX_STAT_NUM;i++)
		{
			if( TRUE == g_pFSDBCfg->GetStatUpCfg(iStartIdx + i, outStatUpCfg) )
			{
				for(int j=0;j<10;j++)
				{
					if( iaFastStatUp[j] !=0 && iLv11866902aFastStatUp[j]==0 && 5 == outStatUpCfg.iAddStat)

						iaStat[i]++;
						break;
					}
				}
				for(int j=0;j<10;j++)
				{
					if( iaNormalFastStatUp[j] !=0 && iLv11866902aNormalFastStatUp[j]==0 && 4 == outStatUpCfg.iAddStat)

						iaStat[i]++;
						break;
					}
				}
				for(int j=0;j<10;j++)
				{
					if( iaNormalStatUp[j] != 0 && iLv11866902aNormalStatUp[j]==0 && 3 == outStatUpCfg.iAddStat)

						iaStat[i]++;
						break;
					}
				}
				for(int j=0;j<10;j++)
				{
					if( iaNormalSlowStatUp[j] != 0 && iLv11866902aNormalSlowStatUp[j]==0 && 2 == outStatUpCfg.iAddStat)

						iaStat[i]++;
						break;
					}
				}
				for(int j=0;j<10;j++)
				{
					if( iaSlowStatUp[j] !=0 && iLv11866902aSlowStatUp[j]==0 && 1 == outStatUpCfg.iAddStat)

						iaStat[i]++;
						break;
					}
				}
				
			}
			
		}
	}
	
	if( AvatarInfo.Status.iStatRun == 99 ) iaStat[0] = 0;
	if( AvatarInfo.Status.iStatJump == 99 ) iaStat[1] = 0;
	if( AvatarInfo.Status.iStatStr == 99 ) iaStat[2] = 0;
	if( AvatarInfo.Status.iStatPass == 99 ) iaStat[3] = 0;
	if( AvatarInfo.Status.iStatDribble == 99 ) iaStat[4] = 0;
	if( AvatarInfo.Status.iStatRebound == 99 ) iaStat[5] = 0;
	if( AvatarInfo.Status.iStatBlock == 99 ) iaStat[6] = 0;
	if( AvatarInfo.Status.iStatSteal == 99 ) iaStat[7] = 0;
	if( AvatarInfo.Status.iStat2Point == 99 ) iaStat[8] = 0;
	if( AvatarInfo.Status.iStat3Point == 99 ) iaStat[9] = 0;
	if( AvatarInfo.Status.iStatDrive == 99 ) iaStat[10] = 0;
	if( AvatarInfo.Status.iStatClose == 99 ) iaStat[11] = 0; 
	
	UpdateAvatarStat(iaStat);
	if( ODBC_RETURN_SUCCESS != pGameODBC->spFSUpdateStat( AvatarInfo.iGameIDIndex , iaStat )) 
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL CFSGameUser::ProcessStatUp( int *iaStat )
{
	// 2010.02.23
	// ���Ⱥ���
	// ktKim
	if(TRUE == CFSGameServer::GetInstance()->GetOptionStatusMod())
	{
		return StatusUp(iaStat);
	}

	int iStartIdx=0 , iElementNum=0;

	SAvatarInfo AvatarInfo;
	if(FALSE == CurUsedAvatarSnapShot(AvatarInfo))
	{
		return FALSE;
	}


	SFSConfigInfo outGameCfgInfo;

	switch(AvatarInfo.Status.iGamePosition)
	{
	case POSITION_CODE_CENTER:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_CENTER, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_FOWARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_FOWARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_GUARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_GUARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_POWER_FOWARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_POWER_FOWARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_SMALL_FOWARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_SMALL_FOWARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_POINT_GUARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_POINT_GUARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_SHOOT_GUARD:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_SHOOT_GUARD, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	case POSITION_CODE_SWING_MAN:
		{
			if( FALSE == g_pFSDBCfg->GetStatUpCfgListGroup( CFGCODE_STAT_SWING_MAN, iStartIdx, iElementNum )) return FALSE;
		}
		break;
	}

	int iaFastStatUp[10], iaNormalFastStatUp[10] , iaNormalStatUp[10], iaNormalSlowStatUp[10] , iaSlowStatUp[10];

	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaFastStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+2,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaNormalFastStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+4,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaNormalStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+6,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaNormalSlowStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}
	for(int j=0;j<2;j++)
	{
		memset(&outGameCfgInfo, 0, sizeof(SFSConfigInfo));
		g_pFSDBCfg->GetCfg(CFGCODE_STATUP,j+8,outGameCfgInfo);
		for(int i=0;i<5;i++)
		{
			iaSlowStatUp[i+5*j] = outGameCfgInfo.iParam[i];
		}
	}

	int iLv = AvatarInfo.iLv+1; 

	SUserStatUpConfig outStatUpCfg;
	memset(&outStatUpCfg, 0, sizeof(SUserStatUpConfig));

	for(int i=0;i<MAX_STAT_NUM;i++)
	{
		if( TRUE == g_pFSDBCfg->GetStatUpCfg(iStartIdx + i, outStatUpCfg) )
		{
			for(int j=0;j<10;j++)
			{
				if( iaFastStatUp[j] !=0 && iLv11866902aFastStatUp[j]==0 && 5 == outStatUpCfg.iAddStat)

				iaStat[i]++;
					break;
				}
			}
			for(int j=0;j<10;j++)
			{
				if( iaNormalFastStatUp[j] !=0 && iLv11866902aNormalFastStatUp[j]==0 && 4 == outStatUpCfg.iAddStat)

				iaStat[i]++;
					break;
				}
			}
			for(int j=0;j<10;j++)
			{
				if( iaNormalStatUp[j] != 0 && iLv11866902aNormalStatUp[j]==0 && 3 == outStatUpCfg.iAddStat)

				iaStat[i]++;
					break;
				}
			}
			for(int j=0;j<10;j++)
			{
				if( iaNormalSlowStatUp[j] != 0 && iLv11866902aNormalSlowStatUp[j]==0 && 2 == outStatUpCfg.iAddStat)

				iaStat[i]++;
					break;
				}
			}
			for(int j=0;j<10;j++)
			{
				if( iaSlowStatUp[j] !=0 && iLv11866902aSlowStatUp[j]==0 && 1 == outStatUpCfg.iAddStat)

				iaStat[i]++;
					break;
				}
			}
		}
	}

	if( AvatarInfo.Status.iStatRun == 99 ) iaStat[0] = 0;
	if( AvatarInfo.Status.iStatJump == 99 ) iaStat[1] = 0;
	if( AvatarInfo.Status.iStatStr == 99 ) iaStat[2] = 0;
	if( AvatarInfo.Status.iStatPass == 99 ) iaStat[3] = 0;
	if( AvatarInfo.Status.iStatDribble == 99 ) iaStat[4] = 0;
	if( AvatarInfo.Status.iStatRebound == 99 ) iaStat[5] = 0;
	if( AvatarInfo.Status.iStatBlock == 99 ) iaStat[6] = 0;
	if( AvatarInfo.Status.iStatSteal == 99 ) iaStat[7] = 0;
	if( AvatarInfo.Status.iStat2Point == 99 ) iaStat[8] = 0;
	if( AvatarInfo.Status.iStat3Point == 99 ) iaStat[9] = 0;
	if( AvatarInfo.Status.iStatDrive == 99 ) iaStat[10] = 0;
	if( AvatarInfo.Status.iStatClose == 99 ) iaStat[11] = 0; 

	return TRUE;
}

// 2010.02.23
// ���Ⱥ���
// ktKim
BOOL CFSGameUser::StatusUp(CFSGameODBC* pGameODBC)
{
	int iaStat[MAX_STAT_NUM], iaStatAll[MAX_STAT_NUM];
	memset(iaStat, 0, sizeof(int)*MAX_STAT_NUM);
	memset(iaStatAll, 0, sizeof(int)*MAX_STAT_NUM);

	SAvatarInfo AvatarInfo;
	if(FALSE == CurUsedAvatarSnapShot(AvatarInfo))
	{
		return FALSE;
	}

	int iLv = AvatarInfo.iLv + 1;
	int iLvUpCount = GetItemTypeLevelUpCount();
	
	//if(iLvUpCount <=0 || iLvUpCount > MAX_EANBLE_LEVLE_UP_COUNT)
	//{
	//	
	//}

	for(; iLv < (iLvUpCount+AvatarInfo.iLv+1); iLv++)
	{
		CFSGameServer::GetInstance()->GetStatusUp(AvatarInfo.Status.iGamePosition, iLv, iaStat, MAX_STAT_NUM);
		for(int i=0; i<MAX_STAT_NUM; i++)
		{
			iaStatAll[i] += iaStat[i];
		}
	}

	int iIndex = 0;

	if(AvatarInfo.Status.iStatRun+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatRun;
	if(AvatarInfo.Status.iStatJump+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatJump;
	if(AvatarInfo.Status.iStatStr+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatStr;
	if(AvatarInfo.Status.iStatPass+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatPass;
	if(AvatarInfo.Status.iStatDribble+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatDribble;
	if(AvatarInfo.Status.iStatRebound+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatRebound;
	if(AvatarInfo.Status.iStatBlock+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatBlock;
	if(AvatarInfo.Status.iStatSteal+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatSteal;
	if(AvatarInfo.Status.iStat2Point+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStat2Point;
	if(AvatarInfo.Status.iStat3Point+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStat3Point;
	if(AvatarInfo.Status.iStatDrive+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatDrive;
	if(AvatarInfo.Status.iStatClose+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatClose;

	CHECK_NULL_POINTER_BOOL(pGameODBC);

	UpdateAvatarStat(iaStat);
	if(ODBC_RETURN_SUCCESS != pGameODBC->spFSUpdateStat(AvatarInfo.iGameIDIndex, iaStat))
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUser::StatusUp( int* iaStat )
{
	SAvatarInfo AvatarInfo;
	if( FALSE == CurUsedAvatarSnapShot( AvatarInfo ) )
	{
		return FALSE;
	}

	CFSGameServer::GetInstance()->GetStatusUp(AvatarInfo.Status.iGamePosition, AvatarInfo.iLv+1, iaStat, MAX_STAT_NUM);

	int iIndex = 0;

	if(AvatarInfo.Status.iStatRun+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatRun;
	if(AvatarInfo.Status.iStatJump+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatJump;
	if(AvatarInfo.Status.iStatStr+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatStr;
	if(AvatarInfo.Status.iStatPass+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatPass;
	if(AvatarInfo.Status.iStatDribble+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatDribble;
	if(AvatarInfo.Status.iStatRebound+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatRebound;
	if(AvatarInfo.Status.iStatBlock+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatBlock;
	if(AvatarInfo.Status.iStatSteal+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatSteal;
	if(AvatarInfo.Status.iStat2Point+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStat2Point;
	if(AvatarInfo.Status.iStat3Point+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStat3Point;
	if(AvatarInfo.Status.iStatDrive+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatDrive;
	if(AvatarInfo.Status.iStatClose+iaStat[iIndex++] > 99) iaStat[iIndex-1] = 99 - AvatarInfo.Status.iStatClose;

	return TRUE;
}
// End

void CFSGameUser::UpdateAvatarStat(int iaAddStat[])
{
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	if(NULL != pAvatarInfo)
	{
		// 2010.02.23
		// ���Ⱥ���
		// ktKim
		/*if(TRUE == CFSGameServer::GetInstance()->GetOptionStatusMod())
		{
			return ;
		}*/
		// End

		pAvatarInfo->Status.iStatRunBase  += iaAddStat[0];
		pAvatarInfo->Status.iStatJumpBase += iaAddStat[1];
		pAvatarInfo->Status.iStatStrBase += iaAddStat[2];
		pAvatarInfo->Status.iStatPassBase += iaAddStat[3];
		pAvatarInfo->Status.iStatDribbleBase += iaAddStat[4];
		pAvatarInfo->Status.iStatReboundBase += iaAddStat[5];
		pAvatarInfo->Status.iStatBlockBase += iaAddStat[6];
		pAvatarInfo->Status.iStatStealBase += iaAddStat[7];
		pAvatarInfo->Status.iStat2PointBase += iaAddStat[8];
		pAvatarInfo->Status.iStat3PointBase += iaAddStat[9];
		pAvatarInfo->Status.iStatDriveBase += iaAddStat[10];
		pAvatarInfo->Status.iStatCloseBase += iaAddStat[11];
		
		pAvatarInfo->Status.iStatRun  += iaAddStat[0];
		pAvatarInfo->Status.iStatJump += iaAddStat[1];
		pAvatarInfo->Status.iStatStr += iaAddStat[2];
		pAvatarInfo->Status.iStatPass += iaAddStat[3];
		pAvatarInfo->Status.iStatDribble += iaAddStat[4];
		pAvatarInfo->Status.iStatRebound += iaAddStat[5];
		pAvatarInfo->Status.iStatBlock += iaAddStat[6];
		pAvatarInfo->Status.iStatSteal += iaAddStat[7];
		pAvatarInfo->Status.iStat2Point += iaAddStat[8];
		pAvatarInfo->Status.iStat3Point += iaAddStat[9];
		pAvatarInfo->Status.iStatDrive += iaAddStat[10];
		pAvatarInfo->Status.iStatClose += iaAddStat[11];
	}
}

void CFSGameUser::SendUserStat(int *iaDummyStat/* = NULL*/)
{
	int iaStatAdd[MAX_STAT_NUM] = {0};
	int iaItemMinusStat[MAX_STAT_NUM] = {0};
	
	SAvatarInfo AvatarInfo;
	if( FALSE == CurUsedAvatarSnapShot( AvatarInfo ) )
	{
		return;
	}
	
	GetAvatarItemStatus(iaStatAdd, iaItemMinusStat);
	GetUserCharacterCollection()->GetSentenceStatus(iaStatAdd);
	GetUserPowerupCapsule()->GetPowerupCapsuleStatus(iaStatAdd);
	GetAvatarSpecialPieceProperty(iaStatAdd);
	GetCheerLeaderStatus(iaStatAdd);

	int iLinkItemAddStat[MAX_STAT_NUM]; 
	memset( iLinkItemAddStat , 0 , sizeof(int) * MAX_STAT_NUM);
	GetAvatarLinkItemStatus( iLinkItemAddStat );

	CPacketComposer PacketComposer(S2C_STAT_CHANGE_REQ);
	
	PacketComposer.Add((PBYTE)AvatarInfo.szGameID, sizeof(char) * (MAX_GAMEID_LENGTH+1) );	
	PacketComposer.Add(AvatarInfo.iLv);		// 20090610 Add Tournament Trophy System
	for (int i = 0; i < MAX_SKILL_STORAGE_COUNT; ++i)		//��ų��ȣ Ȯ�� Ŭ��� �Բ� �۾��� SKILL_STORAGE_3 - > MAX_SKILL_STORAGE_COUNT ����
		PacketComposer.Add(AvatarInfo.Skill.iaSkill[i]);
	PacketComposer.Add(AvatarInfo.Status.iStatRun);
	PacketComposer.Add(AvatarInfo.Status.iStatJump);
	PacketComposer.Add(AvatarInfo.Status.iStatStr);
	PacketComposer.Add(AvatarInfo.Status.iStatPass);
	PacketComposer.Add(AvatarInfo.Status.iStatDribble);
	PacketComposer.Add(AvatarInfo.Status.iStatRebound);
	PacketComposer.Add(AvatarInfo.Status.iStatBlock);
	PacketComposer.Add(AvatarInfo.Status.iStatSteal);
	PacketComposer.Add(AvatarInfo.Status.iStat2Point);
	PacketComposer.Add(AvatarInfo.Status.iStat3Point);
	PacketComposer.Add(AvatarInfo.Status.iStatDrive);
	PacketComposer.Add(AvatarInfo.Status.iStatClose);
	
	int iaLvIntervalBuffItemAddStat[MAX_STAT_NUM] = {0,};
	ZeroMemory(iaLvIntervalBuffItemAddStat, sizeof(int)*MAX_STAT_NUM);

	BOOL bResult = GetLvIntervalBuffItemStatus(iaLvIntervalBuffItemAddStat);
	PacketComposer.Add((BYTE)bResult);
	if(bResult)
	{
		for(int i = 0; i < MAX_STAT_NUM; ++i)
		{
			PacketComposer.Add(iaLvIntervalBuffItemAddStat[i]);
		}
	}
	
	int iFameSkillAddStat[MAX_STAT_NUM], iFameSkillMinusStat[MAX_STAT_NUM]; 
	ZeroMemory(iFameSkillAddStat, sizeof(int)*MAX_STAT_NUM);
	ZeroMemory(iFameSkillMinusStat, sizeof(int)*MAX_STAT_NUM);

	bResult = SetAvatarFameSkillAddMinusStatus(iFameSkillAddStat, iFameSkillMinusStat);
	PacketComposer.Add((BYTE)bResult);

	if(bResult)
	{
		if (NULL != iaDummyStat)
		{
			for(int i=0; i < MAX_STAT_NUM ; i++ )
			{
				if (0 < iaDummyStat[i])	iaStatAdd[i] += iaDummyStat[i];
				else					iaItemMinusStat[i] += iaDummyStat[i];
			}
		}

		for(int i=0; i < MAX_STAT_NUM ; i++ )
		{
			PacketComposer.Add(iaStatAdd[i] + iLinkItemAddStat[i] + iFameSkillAddStat[i]);
			PacketComposer.Add((short)(iFameSkillMinusStat[i] + iaItemMinusStat[i]));
		}
	}

	Send(&PacketComposer);
}
// End

void CFSGameUser::ProcessLevelUp(int iLvUpGameIDIndex, int iGamePosition, int iPrevLv, int iUpdateLv, int iUpdateExp, BOOL bGameEnd/* = FALSE*/)
{
	if(iPrevLv == iUpdateLv)
	{
		return;
	}

	CFSODBCBase* pBaseODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	if(GetGameIDIndex() == iLvUpGameIDIndex)
	{
		SAvatarInfo* pAvatar = GetCurUsedAvatar();
		if(NULL != pAvatar)
		{			
			if(ODBC_RETURN_SUCCESS == pBaseODBC->spGetAvatarStatus(pAvatar))
			{
				pAvatar->iLv = iUpdateLv;
				pAvatar->iExp = iUpdateExp;
			}				
		}

		vector<int> vLevel;
		vLevel.push_back(GetCurUsedAvtarLv());
		ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_LEVEL, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vLevel);
		SendLevelUpInfo(iPrevLv, iUpdateLv, bGameEnd);
		SendAvatarInfo();
		SendUserStat();
		CheckPositionSelect();

		if(bGameEnd)
		{
			GivePackageCrazyLevelUp(iPrevLv, iUpdateLv);
		}
	}

	UpdateAvatarExp(iLvUpGameIDIndex, iUpdateLv, iUpdateExp);

	/*for( int i = 0 ; i < (iUpdateLv - iPreLv) ; ++i )
		GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_LV_UP_CONTINUE);*/

	GetUserFriendInvite()->UpdateFriendInviteMission(FRIEND_INVITE_MISSION_INDEX_LV_ATTAINMENT);
	GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_LEVEL_UP, iUpdateLv - iPrevLv);
	GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_LEVEL_UP, iUpdateLv - iPrevLv);
	GetUserCharacterCollection()->CheckLvUp(iUpdateLv);
}

void CFSGameUser::SendLevelUpInfo(int iPrevLv, int iCurrentLv, BOOL bIsEventCheck, int iPosition/* = POSITION_CODE_NO_POSITION*/)
{
	if(POSITION_CODE_NO_POSITION == iPosition)
	{
		SAvatarInfo AvatarInfo;
		if(FALSE == CurUsedAvatarSnapShot(AvatarInfo))
		{
			return;
		}

		iPosition = AvatarInfo.Status.iGamePosition;
	}	
	
	CPacketComposer Packet(S2C_AVATAR_LEVEL_UP_INFO_NOT);

	SS2C_AVATAR_LEVEL_UP_INFO_NOT	ss;
	ss.iLv = iCurrentLv;
	ss.iEventKind = EVENT_KIND_NONE;
	ss.iStatUpNum = 0;

	if(TRUE == bIsEventCheck && TRUE == CRAZYLEVELUP.CheckCrazyLevelUp(iPrevLv))
	{
		ss.iEventKind = EVENT_KIND_CRAZY_LEVELUP;
	}

	CheckMaxAvatarLv(iCurrentLv);
	
	if(OPEN == LVUPEVENT.IsOpen() && TRUE == IsJumpingPlayerAvatar())
	{
		ss.iEventKind = EVENT_KIND_JUMPING_LEVELUP;

		GiveLvUpEventReward(iPrevLv);
	}

	Packet.Add((PBYTE)&ss, sizeof(SS2C_AVATAR_LEVEL_UP_INFO_NOT));
	PBYTE pCnt = Packet.GetTail() - sizeof(int);

	int iaStat[MAX_STAT_NUM] = {0,};

	CFSGameServer::GetInstance()->GetStatusUp(iPosition, iPrevLv, iCurrentLv, iaStat, MAX_STAT_NUM);

	SLEVEL_UP_STAT_INFO	sStat;
	int iCnt = 0;
	for(int i = 0; i < MAX_STAT_NUM; i++)
	{
		if(0 < iaStat[i])
		{
			sStat.shStatIdx = i;
			sStat.shCount = iaStat[i];

			Packet.Add((PBYTE)&sStat, sizeof(SLEVEL_UP_STAT_INFO));

			iCnt++;
		}
	}
	memcpy(pCnt, &iCnt, sizeof(int));
	Send(&Packet);
}

void CFSGameUser::GivePackageCrazyLevelUp(int iPrevLv, int iCurrentLv)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == CRAZYLEVELUP.CheckCrazyLevelUp(iPrevLv));

	CFSGameUserItem* pUserItem = GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItem);

	CAvatarItemList *pAvatarItemList = pUserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_VOID( pODBC );

	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SShopItemInfo sShopItem;
	CHECK_CONDITION_RETURN_VOID(FALSE == pItemShop->GetItemByPropertyKind(ITEM_PROPERTY_KIND_SPECIAL_LEVELUP_PACKAGE, sShopItem));

	SUserItemInfo sUserItem;
	sUserItem.SetBaseInfo(sShopItem);
	sUserItem.iItemCode = sShopItem.iItemCode0;
	sUserItem.iSellType = sShopItem.iSellType;
	sUserItem.iStatus = ITEM_STATUS_INVENTORY;

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_CRAZYLEVELUP_GivePackage(GetGameIDIndex(), iPrevLv + 1, MIN(iCurrentLv, 31), sUserItem, pAvatarItemList))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "EVENT_CRAZYLEVELUP_GivePackage - GameIDIndex:11866902, iPrevLv:0, iCurrentLv:18227200", GetGameIDIndex(), iPrevLv, iCurrentLv);
id CFSGameUser::SendTournamentChampionCnt()
{
	CPacketComposer PacketComposer(S2C_TOURNAMENT_CHAMPION_COUNT);
	PacketComposer.Add(GetTournamentChampionCnt());
	Send(&PacketComposer);
}

BOOL CFSGameUser::CheckBindAccountItemExpire(time_t& CurrentTime)
 {
	if (TRUE == CUser::CheckBindAccountItemExpire(CurrentTime))
	{
		return TRUE;
	}

	return FALSE;
 }

void CFSGameUser::CheckBindAccountInfo()
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pGameODBC);
	
	if(m_iUserIDIndex < 0)
	{
		return;
	}
	
	SBindAccountData BindAccountData;
	
	if(ODBC_RETURN_SUCCESS == pGameODBC->BINDACCOUNT_GetUserInfo(m_iUserIDIndex, BindAccountData))
	{
		if(BindAccountData.iBindAccountItemPropertyKind > 0)
		{
			SetBindAccountData(BindAccountData);
		}
	}
}

void CFSGameUser::SendBindAccountInfo()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_VOID( pODBC );

	time_t CurrentTime = _time64(NULL);
	int iDay = 0, iHour = 0, iMin = 0;
	
	SBindAccountData BindAccountData = GetBindAccountData();
	if( BindAccountData.iBindAccountItemPropertyKind > -1)
	{
		int iDiffSecond = difftime(BindAccountData.ExpireDate, CurrentTime);	
		
		iDay = iDiffSecond / 86400;
		iHour = (iDiffSecond ) / 3600;
3600;
		iMin = (iDiffSecond ) / 60;
 60;
	}
	
	CPacketComposer PacketComposer(S2C_BIND_ACCOUNT_INFO_RES);
	PacketComposer.Add(BindAccountData.iBindAccountItemPropertyKind);
	if(BindAccountData.iBindAccountItemPropertyKind > -1)
	{
		PacketComposer.Add(iDay);
		PacketComposer.Add(iHour);
		PacketComposer.Add(iMin);
	}
	Send(&PacketComposer);
}

void CFSGameUser::ResetSeasonRankAndRecord(int iSeasonIndex)
{
	BYTE ucRecordType;
	for( ucRecordType = RECORD_TYPE_HALFCOURT ; ucRecordType < MAX_RECORD_TYPE ; ++ucRecordType )
	{
		m_AvatarSeasonInfo.ResetSeason(ucRecordType, iSeasonIndex);	
		//reload total record&rank...
	}
}

void CFSGameUser::ResetTotalRankAndRecord()
{
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);
	SAvatarSeasonInfo* pAvatarSeasonInfo = GetAvatarSeasonInfo();
	
	m_AvatarSeasonInfo.Initialize();

	BYTE ucRecordType;
	for( ucRecordType = RECORD_TYPE_HALFCOURT ; ucRecordType < MAX_RECORD_TYPE ; ++ucRecordType )
	{
		pAvatarInfo->TotalInfo[ucRecordType].Initialize();
	}
}

void CFSGameUser::ResetRecordAndRank(int iRankType, int iSeasonIndex)
{
	if(RANK_TYPE_SEASON == iRankType)
	{
		ResetSeasonRankAndRecord(iSeasonIndex);
	}
	else if(RANK_TYPE_TOTAL == iRankType)
	{
		ResetTotalRankAndRecord();
	}
}

SBindAccountItemBonusData* CFSGameUser::GetBindAccountBonusData(int iBindAccountItemPropertyKind)
{
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);
	
	CShopItemList* pShopItemList = pItemShop->GetShopItemList();
	CHECK_NULL_POINTER_BOOL(pShopItemList);
	
	return pShopItemList->GetBindAccountBonusData(iBindAccountItemPropertyKind);
}

int CFSGameUser::CheckAndGetBonusSkillSlot()
{
	int iBonusSkillSlotCount = 0;

	SAvatarInfo AvatarInfo;
	if(TRUE == CurUsedAvatarSnapShot(AvatarInfo))
	{
		iBonusSkillSlotCount = AvatarInfo.Skill.iPCBangBonusSlot;

		if(BIND_ACCOUNT_ITEM_KIND_VIP_PLUS == GetBindAccountItemPropertyKind())
		{
			SBindAccountItemBonusData* pBindAccountItemBonusData = GetBindAccountBonusData(GetBindAccountItemPropertyKind());
			if(pBindAccountItemBonusData != NULL)
			{
				iBonusSkillSlotCount = pBindAccountItemBonusData->iBonusSkillSlotCount;
			}
		}
	}
	
	return iBonusSkillSlotCount;
}

int CFSGameUser::GetDiscountRate(int iType, int iCategory)
{
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if (NULL == pAvatarItemList)			
		return 0;

	int iKind;
	if (SELL_TYPE_CASH == iType)
		iKind = CASH_DISCOUNT_ITEM_KIND_NUM;
	else if(SELL_TYPE_POINT == iType)
		iKind = POINT_DISCOUNT_ITEM_KIND_NUM;
	else if(SELL_TYPE_CLUBCOIN != iType)
		return 0;

	SUserItemInfo* pItem = pAvatarItemList->GetPossessionItemWithPropertyKind(iKind);

	time_t CurrentTime = _time64(NULL);
	int iClubDisCountValue = 0;
	int iSaleItemValue = 0;

	if(ITEM_CATEGORY_CLUB == iCategory)
	{
		SAvatarInfo* pAvatar = GetCurUsedAvatar();
		if(pAvatar)
		{
			iClubDisCountValue = CLUBCONFIGMANAGER.GetShopDiscountRate(pAvatar->iClubLeagueSeasonRank);
		}
	}

	if (NULL != pItem && ITEM_CATEGORY_CLUB != iCategory)
	{
		if(CurrentTime > pItem->ExpireDate)
		{
			iSaleItemValue = CFSGameServer::GetInstance()->GetDiscountRate(iType);
		}
	}

	if(iClubDisCountValue == 0 && iSaleItemValue == 0)
		return 0;

	if ( SELL_TYPE_CASH == iType ||
		SELL_TYPE_CLUBCOIN == iType)
	{
		return 100 - (100 - iClubDisCountValue)*(100 - iSaleItemValue)/100;
	}

	return iSaleItemValue;
}

// 2010.02.02
// ���󰡱� ��� �߰�
// ktKim
void CFSGameUser::SetLocation(int n)
{
	int iOldLocation = m_iLocation;

	m_iLocation = n;

	if (iOldLocation != n)
	{
		SendUserStatusUpdate();
	}
}

void CFSGameUser::SendUserStatusUpdate()
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pCenter);

	pCenter->SendUserStatusUpdate(GetGameID(), USER_ST_SAMECHANNEL, GetLocation());
}

void CFSGameUser::GetUserItemInfo(SUserItemInfo* pSlotItemInfo)
{
	for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
	{
		pSlotItemInfo[i].Reset();
	}

	SAvatarInfo* pAvatarInfo = NULL;
	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if(NULL == pAvatarInfo || NULL == pAvatarItemList)
	{
		return;
	}

	for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
	{
		if(-1 != pAvatarInfo->AvatarFeature.FeatureInfo[i])
		{
			memcpy(&pSlotItemInfo[i], pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]), sizeof(SUserItemInfo));
		}
	}
}

SHORT CFSGameUser::GetGameOption()
{
	return GetCurUsedAvatar()->iGameOpt;
}

int CFSGameUser::DiscountPauseItem()
{
	return GetUserItemList()->DiscountPauseItem();
}

void CFSGameUser::SendToMatchAddFriendAll()
{
	BYTE friendCnt = 0;
	std::vector<CFriend*> v;
	
	CFriend* fr = NULL;

	CLock Lock(m_FriendList.GetLockResource());
	{
		fr = m_FriendList.FindFirst();
		while(NULL != fr)
		{
			if(fr->GetConcernStatus() == FRIEND_ST_FRIEND_USER || fr->GetConcernStatus() == FRIEND_ST_BANNED_USER) 
			{
				SendToMatchAddFriend(fr->GetGameID(), fr->GetLevel(), fr->GetEquippedAchievementTitle(), fr->GetConcernStatus());
			}
			fr = m_FriendList.FindNext();		
		}
	}
}

void CFSGameUser::SendToMatchAddFriend(const char* szGameID, int iLevel, int iEquippedAchievementTitle, int iConcernStatus)
{
	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_ADD_FRIEND info;
	info.iGameIDIndex = GetGameIDIndex();
	strncpy_s(info.szFriendName, _countof(info.szFriendName), szGameID, _countof(info.szFriendName)-1);
	info.iConcernStatus = iConcernStatus;
	info.iLevel = iLevel;
	info.iEquippedAchievementTitle = iEquippedAchievementTitle;

	pMatch->SendAddFriend(info);
}

void CFSGameUser::SendToMatchDelFriend(const char* szGameID)
{
	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_DEL_FRIEND info;
	info.iGameIDIndex = GetGameIDIndex();
	strncpy_s(info.szFriendName, _countof(info.szFriendName), szGameID, _countof(info.szFriendName)-1);

	pMatch->SendDelFriend(info);
}

void CFSGameUser::SendToMatchAddFriendAccountAll()
{
	if(true == m_UserFriendAccountInfo.bIsSecret_CurrentAvatar)
		return;

	int iCnt = 0;

	CLock Lock(m_FriendAccountList.GetLockResource());

	for(CFriendAccount* fr = m_FriendAccountList.FindFirst(); 
		fr != nullptr && iCnt <= FriendAccount::MAX_FRIEND_CNT; 
		fr = m_FriendAccountList.FindNext())
	{
		if(fr->GetChannelType() == FriendAccount::CHANNEL_TYPE_OFFLINE)
			continue;

		if(fr->GetStatus() != FriendAccount::STATUS_FRIEND && fr->GetStatus() != FriendAccount::STATUS_FRIEND_FAVORITE)
			continue;

		SendToMatchAddFriendAccount(
			fr->GetUserIDIndex()
			,	fr->GetGameID()
			,	fr->GetGamePosition()
			,	fr->GetLv()
			,	fr->GetStatus()
			,	fr->GetChannelType()
			,	fr->GetLocation()
			,	fr->GetWithPlayCnt()
			,	fr->GetLastLogoutTime());

		++iCnt;
	}
}

void CFSGameUser::SendToMatchAddFriendAccount(int iUserIDIndex, const char* szGameID, BYTE btGamePosition, BYTE btLv, BYTE btStatus, BYTE btChannelType, BYTE btLocation, int iWithPlayCnt, time_t tLastLogoutTime)
{
	if(true == m_UserFriendAccountInfo.bIsSecret_CurrentAvatar)
		return;

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_FRIEND_ACCOUNT_ADD_FRIEND_NOT	ss;
	ZeroMemory(&ss, sizeof(SG2M_FRIEND_ACCOUNT_ADD_FRIEND_NOT));

	ss.iGameIDIndex = GetGameIDIndex();

	ss.sFriend.iUserIDIndex = iUserIDIndex;
	strncpy_s(ss.sFriend.szGameID, _countof(ss.sFriend.szGameID), szGameID, MAX_GAMEID_LENGTH);
	ss.sFriend.btGamePosition = btGamePosition;
	ss.sFriend.btLv = btLv;
	ss.sFriend.btStatus = btStatus;
	ss.sFriend.btChannelType = btChannelType;
	ss.sFriend.btLocation = btLocation;
	ss.sFriend.iPlayWithCnt = iWithPlayCnt;
	ss.sFriend.tLastLoginTime = tLastLogoutTime;

	CPacketComposer Packet(G2M_FRIEND_ACCOUNT_ADD_FRIEND_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SG2M_FRIEND_ACCOUNT_ADD_FRIEND_NOT));
	pMatch->Send(&Packet);
}

void CFSGameUser::SendToMatchDelFriendAccount(int iFriendIDIndex)
{
	if(true == m_UserFriendAccountInfo.bIsSecret_CurrentAvatar)
		return;

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_FRIEND_ACCOUNT_DEL_FRIEND_NOT	ss;
	ZeroMemory(&ss, sizeof(SG2M_FRIEND_ACCOUNT_DEL_FRIEND_NOT));

	ss.iGameIDIndex = GetGameIDIndex();

	ss.iFriendIDIndex = iFriendIDIndex;

	CPacketComposer Packet(G2M_FRIEND_ACCOUNT_DEL_FRIEND_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SG2M_FRIEND_ACCOUNT_DEL_FRIEND_NOT));
	pMatch->Send(&Packet);
}

void CFSGameUser::UpdateGameRecord( BOOL bUpdateGameRecord, BOOL bActiveProtectRing, BOOL bActiveProtectHandRecord,
	int iDisconnectPoint, SFSGameRecord GameRecord, int iGamePlayerNum, int iaStat[MAX_STAT_NUM], BYTE _ucRecordType, BYTE btFactionReversalScore)
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	if(NULL != pAvatar)
	{
		pAvatar->DisconnectPoint = iDisconnectPoint;
		pAvatar->iExp += GameRecord.iaGameRecord[AVATAR_GAME_RECORD_EXP];
		if (0 < GameRecord.iaGameRecord[AVATAR_GAME_RECORD_LVUP])
		{
			UpdateAvatarLv(pAvatar->iLv + GameRecord.iaGameRecord[AVATAR_GAME_RECORD_LVUP]);
		}
		pAvatar->iFamePoint += GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FAMEPOINT];
		pAvatar->iFameLevel += GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FAMELEVELUP];
		SetSkillPoint(GetSkillPoint() + GameRecord.iaGameRecord[AVATAR_GAME_RECORD_SKILLPOINT]);
		UpdateAvatarStat(iaStat);
	}

	if( FALSE == bActiveProtectRing && bUpdateGameRecord == TRUE && FALSE == bActiveProtectHandRecord ) 
	{
		UpdateAvatarRecord(iGamePlayerNum, GameRecord, _ucRecordType);
	}

	if(PCROOM_KIND_PREMIUM == GetPCRoomKind() && 0 < GameRecord.iaGameRecord[AVATAR_GAME_RECORD_LVUP])
	{
		if(GAME_LEAGUE_UP_PRO == pAvatar->iLv || GAME_LVUP_BOUND2 == GetCurUsedAvtarLv())
		{
			UpdatePCRoomItemStat(pAvatar->iLv);
		}
	}

	m_AvatarAdvantageRecord.Update(GameRecord.iaGameRecord[AVATAR_GAME_RECORD_WIN]);

	if (0 < GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FACTION_POINT]) 
		GetUserFaction()->AddFactionPoint(GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FACTION_POINT]);

	if (0 < GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FACTION_SCORE]) 
	{
		if(TRUE == GetUserFaction()->CheckTodayFirstGamePlay())
		{
			GetUserFaction()->_bCheckFristGiveReward = TRUE;
		}

		int iAddScore = GameRecord.iaGameRecord[AVATAR_GAME_RECORD_FACTION_SCORE] + (int)btFactionReversalScore;
		GetUserFaction()->AddFactionScore(iAddScore, FACTION.GetDailyLimitFactionScore());
		GetUserFaction()->_btUserFactionGrade = FACTION.GetUserFactionGrade((CUser*)this, GetUserFaction()->_iTotalFactionScore);
	}

	time_t tLastPlayTime = _time64(NULL);
	GetUserFaction()->_tTimeLastPlayTime = tLastPlayTime;
	GetUserFaction()->_tDailyLastPlayTime = tLastPlayTime;
	GetUserFaction()->_tSeasonLastPlayTime = tLastPlayTime;
	UpdateFactionLastPlayTime(FALSE);
}

void CFSGameUser::UpdateAvatarRecord( int _iGamePlayerNum, SFSGameRecord _GameRecord, BYTE _ucRecodeType)
{
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);
	SAvatarSeasonInfo* pAvatarSeasonInfo = GetAvatarSeasonInfo();

	int iSeasonIndex = GetCurrentSeasonIndex();
	AvatarSeasonInfoMap::iterator pos = pAvatarSeasonInfo->mapSeasonInfo[_ucRecodeType].find(iSeasonIndex);

	SAvatarSeasonInfoEntry* pSeasonRecord;
	if(pos == pAvatarSeasonInfo->mapSeasonInfo[_ucRecodeType].end())
	{
		SAvatarSeasonInfoEntry Entry;
		pAvatarSeasonInfo->mapSeasonInfo[_ucRecodeType].insert(AvatarSeasonInfoMap::value_type(iSeasonIndex, Entry));
		pos = pAvatarSeasonInfo->mapSeasonInfo[_ucRecodeType].find(iSeasonIndex);
		if(pos == pAvatarSeasonInfo->mapSeasonInfo[_ucRecodeType].end())
		{
			return;
		}
	}
	pSeasonRecord = &pos->second;

	GetUserFriendInvite()->UpdateFriendInviteMission(FRIEND_INVITE_MISSION_INDEX_PLAYTIME, _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_PLAYTIME]);

	if(2 >= _iGamePlayerNum)   // 1:1 �϶� 
	{
		pSeasonRecord->Record[SEASON_RECORD_WINPOINT1ON1]	+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_WINPOINT1ON1];
		pSeasonRecord->Record[SEASON_RECORD_PLAYTIME]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_PLAYTIME];

		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_WINPOINT1ON1]	+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_WINPOINT1ON1];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_PLAYTIME]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_PLAYTIME];
	}
	else if(2 < _iGamePlayerNum) // 2���̻��̾�� ��� �ȴ� 
	{	
		if(1 == _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_WIN]) 
		{
			pSeasonRecord->Record[SEASON_RECORD_WIN]++;
			pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_WIN]++;
		}
		else
		{
			pSeasonRecord->Record[SEASON_RECORD_LOSE]++;
			pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_LOSE]++;
		}

		pSeasonRecord->Record[SEASON_RECORD_POINT2]			+= (_GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT2] - _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT2_FEVER]);
		pSeasonRecord->Record[SEASON_RECORD_POINT3]			+= (_GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT3] - _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT3_FEVER]);
		pSeasonRecord->Record[SEASON_RECORD_POINT2TRIAL]	+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT2TRIAL];
		pSeasonRecord->Record[SEASON_RECORD_POINT3TRIAL]	+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT3TRIAL];
		pSeasonRecord->Record[SEASON_RECORD_ASSIST]			+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_ASSIST];
		pSeasonRecord->Record[SEASON_RECORD_REBOUND]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_REBOUND];
		pSeasonRecord->Record[SEASON_RECORD_BLOCK]			+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_BLOCK];
		pSeasonRecord->Record[SEASON_RECORD_STEAL]			+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_STEAL];
		pSeasonRecord->Record[SEASON_RECORD_WINPOINT]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_WINPOINT];
		pSeasonRecord->Record[SEASON_RECORD_PLAYTIME]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_PLAYTIME];
		pSeasonRecord->Record[SEASON_RECORD_POGNUM]			+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POG];
		pSeasonRecord->Record[SEASON_RECORD_LOOSEBALL]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_LOOSEBALL];

		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_POINT2]		+= (_GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT2] - _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT2_FEVER]);
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_POINT3]		+= (_GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT3] - _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POINT3_FEVER]);
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_ASSIST]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_ASSIST];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_REBOUND]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_REBOUND];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_BLOCK]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_BLOCK];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_STEAL]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_STEAL];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_WINPOINT]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_WINPOINT];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_PLAYTIME]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_PLAYTIME];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_POGNUM]		+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_POG];
		pAvatarInfo->TotalInfo[_ucRecodeType].Record[TOTAL_RECORD_LOOSEBALL]	+= _GameRecord.iaGameRecord[AVATAR_GAME_RECORD_LOOSEBALL];
	}
}

void CFSGameUser::RemoveFeature(int iFeatureIndex)
{
	CHECK_CONDITION_RETURN_VOID(0 > iFeatureIndex || MAX_ITEMCHANNEL_NUM <= iFeatureIndex);

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());

	SUserItemInfo* pItem = GetAvatarItemList()->GetItemWithItemIdx(pAvatarInfo->AvatarFeature.FeatureItemIndex[iFeatureIndex]);
	CHECK_NULL_POINTER_VOID(pItem);

	int iSpecialPartsIndex = pItem->iSpecialPartsIndex;
	if(FALSE == m_UserItem->DeleteExhaustedItem(pItem, iFeatureIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "DeleteExhaustedItem - GameID:�4", pAvatarInfo->szGameID);
}
	
	if(iSpecialPartsIndex > 0)
		CheckSpecialPartsUserProperty();
}

void CFSGameUser::CheckFeatureAndRemoveItem(SUserItemInfo* pItem)
{
	CHECK_NULL_POINTER_VOID(pItem);

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	int iFeatureIndex = -1;
	for (int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
	{
		if (pItem->iItemIdx == pAvatarInfo->AvatarFeature.FeatureItemIndex[i])
		{
			iFeatureIndex = i;
			break;
		}
	}

	if (-1 != iFeatureIndex)
	{
		if(FALSE == m_UserItem->DeleteExhaustedItem(pItem, iFeatureIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "DeleteExhaustedItem - GameID:�4", pAvatarInfo->szGameID);
	}
	}
	else
	{
		GetAvatarItemList()->RemoveItem(pItem);
	}
}

void CFSGameUser::DecreaseFeature(int iFeatureIndex, int iDecreaseCount)
{
	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();

	if(NULL == pAvatarInfo || NULL == pAvatarItemList)
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "DecreaseFeature");
		return;
	}

	SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[iFeatureIndex]);

	if(NULL == pSlotItem)
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "DecreaseFeature - GameID:�4", pAvatarInfo->szGameID);
	return;
	}

	if(pSlotItem->iPropertyTypeValue > iDecreaseCount)
	{
		pSlotItem->iPropertyTypeValue -= iDecreaseCount;
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "DecreaseFeature - GameID:�4", pAvatarInfo->szGameID);
}
}

int CFSGameUser::GetEquippedAchievementTitlebyAvatarIdx(int iAvatarIdx)
{
	return m_AvatarManager.GetEquippedAchievementTitlebyAvatarIdx(iAvatarIdx);
}

BOOL CFSGameUser::ProcessMaxLevelStep()
{
	BYTE byPreviousMaxLevelStep = DEFAULT_MAX_LEVEL_STEP;
	BYTE byCurrentMaxLevelStep = DEFAULT_MAX_LEVEL_STEP;

	BOOL bResult = CheckMaxLevelStepUp(byPreviousMaxLevelStep,byCurrentMaxLevelStep);
	if(TRUE == bResult )
	{
		SetAvatarMaxLevelStep(byCurrentMaxLevelStep);
		ProcessMaxLevelStepReward(byPreviousMaxLevelStep , byCurrentMaxLevelStep);
	}	

	return bResult;
}

BOOL CFSGameUser::CheckMaxLevelStepUp(BYTE& byPreviousMaxLevelStep, BYTE& byCurrentMaxLevelStep)
{
	CFSGameServer* pGameServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pGameServer);

	SAvatarInfo AvatarInfo;
	CurUsedAvatarSnapShot(AvatarInfo);
	byPreviousMaxLevelStep = AvatarInfo.byMaxLevelStep;
	byCurrentMaxLevelStep = byPreviousMaxLevelStep;

	int iMaxLevelStepListSize = pGameServer->GetMaxLevelStepListSize();

	if(AvatarInfo.byMaxLevelStep >= iMaxLevelStepListSize)	// iMaxLevelStepListSize�� MaxStep�� �ǹ��ϱ⵵ �� MaxStep�̸� Check�� �ʿ� ��� �ٷ� ����
	{
		return FALSE;
	}

	BYTE byNextMaxLevelStep = byPreviousMaxLevelStep + 1;
	BOOL bResult = FALSE;
	int iUserLv = AvatarInfo.iLv;

	for(; byNextMaxLevelStep <= iMaxLevelStepListSize; byNextMaxLevelStep++)
	{
		int iRequireLv = 0;
		if( FALSE == pGameServer->GetMaxLevelStepUpLv(byNextMaxLevelStep,iRequireLv))
		{
			return FALSE;
		}

		if(iUserLv < iRequireLv)
		{
			break;
		}
		else
		{
			byCurrentMaxLevelStep = byNextMaxLevelStep;
			bResult = TRUE;
		}
	}

	return bResult;
}

void CFSGameUser::ProcessMaxLevelStepReward(BYTE byPreviousMaxLevelStep, BYTE byCurrentMaxLevelStep)
{
	BYTE bAchieveCount = CFSGameServer::GetInstance()->GetMaxLevelStepListSize();
	int iRewardItemCode = 0;

	CPacketComposer PacketComposer(S2C_TAKE_ITEM_REWARD_MAX_LEVEL_CONTENTS_NOT);
	PacketComposer.Add(bAchieveCount);

	for(BYTE byMaxLevelStepPosition = 1; byMaxLevelStepPosition <= bAchieveCount ; byMaxLevelStepPosition++)
	{
		int iRewardIndex = CFSGameServer::GetInstance()->GetMaxLevelStepUpRewardIndex(byMaxLevelStepPosition);

		BOOL bRewardState = FALSE;
		SRewardConfig* pReward = NULL;
		if (byPreviousMaxLevelStep < byMaxLevelStepPosition && byMaxLevelStepPosition <= byCurrentMaxLevelStep)
		{
			pReward = REWARDMANAGER.GiveReward(this, iRewardIndex);
			bRewardState = TRUE;
		}
		else
			pReward = REWARDMANAGER.GetReward(iRewardIndex);

		if (NULL != pReward)
		{
			iRewardItemCode = pReward->GetRewardValue();
		}

		PacketComposer.Add(byMaxLevelStepPosition);
		PacketComposer.Add(iRewardItemCode);
		PacketComposer.Add((BYTE)bRewardState);
	}

	Send(&PacketComposer);
}

void CFSGameUser::SetAvatarMaxLevelStep(BYTE byMaxLevelStep)
{
	if(byMaxLevelStep > -1)
	{
		SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
		pAvatarInfo->byMaxLevelStep = byMaxLevelStep;
	}
}

void CFSGameUser::UpdateMaxLevelStepInDB( int iMaxLevelStep)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pGameODBC);

	SAvatarInfo AvatarInfo;
	CurUsedAvatarSnapShot(AvatarInfo);
	if(iMaxLevelStep == -1)
	{
		iMaxLevelStep = AvatarInfo.byMaxLevelStep;
	}

	int iRequireLv, iBonusPoint;
	pGameODBC->spFSUpdateLvExp(GetUserIDIndex(), AvatarInfo.iGameIDIndex, AvatarInfo.iLv, AvatarInfo.byMaxLevelStep,0,AvatarInfo.iFameLevel,
								0,0,AvatarInfo.iDisconnected,iRequireLv,iBonusPoint);
}

int CFSGameUser::GetCurAvatarTotalSkillPoint()
{
	return GetCurUsedAvtarFameLevel();
}

int CFSGameUser::GetCurAvatarUsedSkillPointAllType()
{
	int iUsedSkillPoint = 0;

	CFSGameUserSkill* pUserSkill = GetUserSkill();
	if(NULL != pUserSkill)
	{
		iUsedSkillPoint = m_UserSkill->GetUsedSkillPointAllType();
	}

	return iUsedSkillPoint;
}

int CFSGameUser::GetCurAvatarUsedSkillPoint(int iType)
{
	int iUsedSkillPoint = 0;

	CFSGameUserSkill* pUserSkill = GetUserSkill();
	if(NULL != pUserSkill)
	{
		iUsedSkillPoint = m_UserSkill->GetUsedSkillPoint(iType);
	}

	return iUsedSkillPoint;
}

int CFSGameUser::GetCurAvatarRemainSkillPoint()
{
	return GetCurAvatarTotalSkillPoint() - GetCurAvatarUsedSkillPointAllType();
}

void CFSGameUser::EquipAchievementTitle(int iEquipAchievementTitle)
{
	CFSODBCBase* pFreeStyleODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pFreeStyleODBC);

	const int ERROR_CODE_UNKNOWN = -2;
	BOOL bResult = FALSE;
	int iErrorCode = ERROR_CODE_UNKNOWN;		// 0:����, -1:������ ����, -2:DB ����

	if (ODBC_RETURN_SUCCESS != pFreeStyleODBC->ACHIEVEMENT_EquipAvatarTitle(GetUserIDIndex(), GetGameIDIndex(), iEquipAchievementTitle, iErrorCode))
	{
		WRITE_LOG_NEW(LOG_TYPE_ACHIEVEMENT, DB_DATA_UPDATE, FAIL, "AVATARL\tACHIEVE\tCFSGameUser\tERROR_INFO\tACHIEVEMENT_EquipAvatarTitle=FAIL\tGameID=�4", GetGameID());
	bResult = FALSE;
	}
	else
	{
		bResult = TRUE;
		SetEquippedAchievementTitle(iEquipAchievementTitle);

		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());

		if (pMatch != NULL)
		{
			SG2M_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_REQ info;
			info.iGameIDIndex = GetGameIDIndex();
			strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), _countof(info.szGameID)-1);
			info.iAchievementTitleIndex = iEquipAchievementTitle;

			pMatch->SendEquippedAchievementTitleBroadcastReq(info);
		}		
	}	

	CPacketComposer PacketComposer(S2C_EQUIP_ACHIEVEMENT_TITLE_RES);
	PacketComposer.Add((int)bResult);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iEquipAchievementTitle);

	Send(&PacketComposer);
}

int CFSGameUser::GetProgressAmountofAchievement(int iSelectedAchievementIndex)
{
	int iReturnValue = DEFAULT_ACHIEVEMENT_VALUE_NOT;

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if (pServer == NULL || pAvatarInfo == NULL)
	{
		return iReturnValue;
	}

	CFSSkillShop* pSkillShop = pServer->GetSkillShop();

	switch (iSelectedAchievementIndex)
	{
	case ACHIEVEMENT_CODE_SHOP_SKILL_ALL:
		{
			int iShiftBitBase = 1;
			int iNormalSkillCount = 0;
			int iFameSkillCount = 0;

			for (int iLoopIndex = 0; iLoopIndex < MAX_SKILL_COUNT; iLoopIndex++)
			{
				if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_0] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameSkillCount++;						
					}
					else
					{
						iNormalSkillCount++;
					}
				}

				if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_1] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 32, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameSkillCount++;						
					}
					else
					{
						iNormalSkillCount++;
					}
				}

				if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_2] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 64, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameSkillCount++;						
					}
					else
					{
						iNormalSkillCount++;
					}
				}

				if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_3] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 96, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameSkillCount++;						
					}
					else
					{
						iNormalSkillCount++;
					}
				}

				if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_4] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 128, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameSkillCount++;						
					}
					else
					{
						iNormalSkillCount++;
					}
				}

				iShiftBitBase = iShiftBitBase << 1;
			}

			iReturnValue = iNormalSkillCount + iFameSkillCount;
		break;
		}
	case ACHIEVEMENT_CODE_SHOP_FREESTYLE_ALL:
		{
			int iShiftBitBase = 1;
			int iNormalFreeStyleCount = 0;
			int iFameFreeStyleCount = 0;

			for (int iLoopIndex = 0; iLoopIndex < MAX_SKILL_COUNT; iLoopIndex++)
			{
				if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_0] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameFreeStyleCount++;
					}
					else
					{
						if( SKILLSHOP_BUTTON_TYPE_BUY == sSkillinfo.iShopButtonType )
							iNormalFreeStyleCount++;
					}
				}

				if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_1] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 32, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameFreeStyleCount++;
					}
					else
					{
						if( SKILLSHOP_BUTTON_TYPE_BUY == sSkillinfo.iShopButtonType )
							iNormalFreeStyleCount++;
					}
				}

				if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_2] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 64, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameFreeStyleCount++;
					}
					else
					{
						if( SKILLSHOP_BUTTON_TYPE_BUY == sSkillinfo.iShopButtonType )
							iNormalFreeStyleCount++;
					}
				}

				if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_3] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 96, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameFreeStyleCount++;
					}
					else
					{
						if( SKILLSHOP_BUTTON_TYPE_BUY == sSkillinfo.iShopButtonType )
							iNormalFreeStyleCount++;
					}
				}

				if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_4] & iShiftBitBase)
				{
					SSkillInfo sSkillinfo;
					if (pSkillShop != NULL)
					{
						pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 128, sSkillinfo);
					}

					if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
					{
						iFameFreeStyleCount++;
					}
					else
					{
						iNormalFreeStyleCount++;
					}
				}

				iShiftBitBase = iShiftBitBase << 1;
			}

			iReturnValue = iNormalFreeStyleCount + iFameFreeStyleCount;
			break;
		}
	case ACHIEVEMENT_CODE_SHOP_INVENTORY_25:
		{
		}
	case ACHIEVEMENT_CODE_SHOP_INVENTORY_50:
		{
			if (IsCompletedAchievement(iSelectedAchievementIndex) == TRUE)
			{
				iReturnValue = ACHIEVEMENTMANAGER.GetSingleAchievementConditionValue(iSelectedAchievementIndex);
			}
			else
			{
				vector<SUserItemInfo*> vUserItemInfo;
				int iInventoryItemCount = 0;

				CAvatarItemList *pAvatarItemList = GetCurAvatarItemListWithGameID(GetGameID());

				if (pAvatarItemList != NULL)
				{
					pAvatarItemList->GetAllItemList(vUserItemInfo);

					if (vUserItemInfo.empty() == TRUE)
					{
						return iReturnValue;
					}

					for (int iLoopIndex = 0; iLoopIndex < vUserItemInfo.size(); iLoopIndex++)
					{
						SUserItemInfo* pUserItem = vUserItemInfo[iLoopIndex];

						if (pUserItem != NULL)
						{
							if ((0 != pUserItem->iStatus) && (9 != pUserItem->iStatus) && (4 != pUserItem->iStatus) && (0 != pUserItem->iSellType))
							{
								iInventoryItemCount++;
							}
						}
					}
				}			

				iReturnValue = iInventoryItemCount;
			}			
			break;
		}

	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_LOYAL_1:
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_LOYAL_5:
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_LOYAL_10:
		{
			iReturnValue = GetUserPurchaseGrade()->GetCurrentGradeCount(PURCHASE_GRADE_TYPE_ROYAL);
		}
		break;
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_PRESTIGE_1:
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_PRESTIGE_5:
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_PRESTIGE_10:
		{
			iReturnValue = GetUserPurchaseGrade()->GetCurrentGradeCount(PURCHASE_GRADE_TYPE_PRESTIGE);
		}
		break;
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_PREMIUM_1:
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_PREMIUM_5:
	case ACHIEVEMENT_CODE_SHOP_PURCHASE_GRADE_PREMIUM_10:
		{
			iReturnValue = GetUserPurchaseGrade()->GetCurrentGradeCount(PURCHASE_GRADE_TYPE_PREMIUM);
		}
		break;
	case ACHIEVEMENT_CODE_REVENGE_MATCHING_WIN_10:	
	case ACHIEVEMENT_CODE_REVENGE_MATCHING_WIN_50:	
	case ACHIEVEMENT_CODE_REVENGE_MATCHING_WIN_100:	
		{
			iReturnValue = GetRevengeMatchingWinCnt();
		}
		break;

	case ACHIEVEMENT_CODE_CONGRATULATION_EVENT_GIVE_BALLOON_CNT_1:	
	case ACHIEVEMENT_CODE_CONGRATULATION_EVENT_GIVE_BALLOON_CNT_10:	
		{
			iReturnValue = GetUserCongratulationEvent()->GetBalloonGiveCnt();
		}
		break;

	case ACHIEVEMENT_CODE_GAMEPLAY_WIN100_AVOBE:
		{
		}
	case ACHIEVEMENT_CODE_GAMEPLAY_LOSE100_BELOW:
		{
		}
	default:
		{
			if (IsCompletedAchievement(iSelectedAchievementIndex) == TRUE)
			{
				iReturnValue = DEFAULT_ACHIEVEMENT_VALUE_COMPLETE;
			}
			break;
		}
	}

	return iReturnValue;
}

void CFSGameUser::CheckEvent(int iPerformTimeIndex, void* lParam/*=NULL*/, int iEventIndex /*= 0*/)
{
	CLock lock(&m_csEvent);
	SetParam(lParam);

	if (iEventIndex != 0 )
	{
		CFSGameServer::GetInstance()->m_ComponentEventManager.CheckEvent(iPerformTimeIndex,this,iEventIndex);
	}
	else
	{
		CFSGameServer::GetInstance()->m_ComponentEventManager.CheckEvent(iPerformTimeIndex, this);
	}

	ResetParam();
}

BOOL CFSGameUser::EventStepComplateReward(int iParam[])
{

	CCompEventUserData* pUserData = GetEventUserData();
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();

	SAvatarInfo * pAvatarInfo = NULL;

	pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();

	if( NULL == pAvatarInfo )
	{
		return FALSE;
	}

	vector<int> vecRewardStep;
	int iRet = 0;

	if (pODBC->EVENT_LoadEventStepReward(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_USERMODE],GetEvent()->GetEventID(),GetUserIDIndex(),GetGameIDIndex(),vecRewardStep) == ODBC_RETURN_SUCCESS)
	{	
		
		if (vecRewardStep.size() <= 0)
		{
			return FALSE;
		}

		int iStep = vecRewardStep[0];
		int iLv = pAvatarInfo->iLv;
		int iPosition = pAvatarInfo->Status.iGamePosition;
		int iSex = pAvatarInfo->iSex;
		int iPublisher = GetPublisherIDIndex();
		int iSpecialCharacterIndex = GetCurUsedAvatar()->iSpecialCharacterIndex;

		vector<int> vecReward;

		if (GetEvent()->CheckEventReward(iStep,iSex,iPosition,iLv,iPublisher,iSpecialCharacterIndex,vecReward))
		{
			for (int i = 0 ; i < vecReward.size() ; i++)
			{
				PBONUS_ST_PRESENT psPresent = NULL;
				PEVENT_ST_BONUS psBonus = NULL;
				PEVENT_ST_COACHCARD psCoachCard = NULL;
				if (GetEvent()->GetRewardIndexToEventPresent(vecReward[i],psPresent,psBonus,psCoachCard) == FALSE)
				{
					continue;
				}		

				if(NULL != psPresent)
				{
					// ������ �ִ°�.
					int iPresentIndex = 0;

					if(ODBC_RETURN_SUCCESS == pODBC->EVENT_CompleteGivePresent(GetEvent()->GetEventID(), GetUserIDIndex(), GetGameIDIndex(), 
						psPresent->iItemCode, psPresent->iPropertyValue, psPresent->iPropertyIndex1, psPresent->iPropertyIndex2, iPresentIndex, iParam[EVENT_CODE_STEP_COMPLATE_REWARD_MAILTYPE], vecReward[i]))
					{
						if (RecvPresent(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_MAILTYPE], iPresentIndex))
						{
							iRet = 1;
						}
						else
						{
							WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_CompleteGivePresent - UserIDIndex:11866902, GameIDIndex:0, PresentIndex:18227200", GetUserIDIndex(),GetGameIDIndex(),iPresentIndex);
			}
				}

				if (NULL != psBonus)
				{ // ����Ʈ�� ĳ�� �ִ� ��

					if(psBonus->iPoint != 0)
					{
						if (ODBC_RETURN_SUCCESS == pODBC->EVENT_GivePoint(this->GetUserIDIndex(),psBonus->iPoint))
						{
							int iCurrentPoint = GetSkillPoint() + psBonus->iPoint;
							SetSkillPoint(iCurrentPoint);
							SendUserGold();
							AddPointAchievements(); 
							iRet = 1;
						}						
					}
					if (psBonus->iCash != 0)
					{
						if (ODBC_RETURN_SUCCESS == pODBC->EVENT_GiveCash(this->GetUserIDIndex(),psBonus->iCash,GetEvent()->GetEventID()))
						{
							int iCurrentCash = GetEventCoin() + psBonus->iCash;
							SetEventCoin(iCurrentCash);
							SendUserGold();
							iRet = 1;
						}
					}
				}

				if(NULL != psCoachCard)
				{ // ��ġī�� �ִ� ��

					int iGradeLevel = psCoachCard->iGradeLevel;
					int iItemType = 1;
					BYTE byTermType = TERM_TYPE_PERMANENT;

					for(int i=0; i<psCoachCard->iValues; i++)
					{
						SDateInfo sExpireDate;
						int iInputProductIndex = INVALID_IDINDEX;
						int iInputInventoryIndex = INVALID_IDINDEX;
						int iAddProductCount = 1;
						int iNewProductIndex = INVALID_IDINDEX;
						int iNewInventoryIndex = INVALID_IDINDEX;
						int iNewProductCount = 0;
						int iSpecialCharacterIndex = COACHCARD.GetSpecialCardIndex(psCoachCard->iCoachCardCode);

						SProductData* pProductData = new SProductData;
						if(ODBC_RETURN_SUCCESS == pODBC->ITEM_GiveCoachCardShopItem(GetGameIDIndex() , psCoachCard->iCoachCardCode, OWNERSHIP_TYPE_EVENT_GIVE, sExpireDate, pProductData->sProductionDate, RARITY_LEVEL_NONE ,TENDENCY_TYPE_NONE,
							iGradeLevel, iItemType, byTermType, iInputProductIndex, iInputInventoryIndex, iAddProductCount, iNewProductIndex, iNewInventoryIndex, iNewProductCount, iSpecialCharacterIndex))
						{
							iRet = 1;
						}

						pProductData->iGradeLevel = iGradeLevel;
						pProductData->iProductIndex = iNewProductIndex;
						pProductData->iInventoryIndex = iNewInventoryIndex;
						pProductData->iItemIDNumber = psCoachCard->iCoachCardCode;
						pProductData->iTendencyType = TENDENCY_TYPE_NONE;
						pProductData->byTermType = byTermType;
						pProductData->iItemType = iItemType;
						pProductData->iRarityLevel = RARITY_LEVEL_NONE;
						pProductData->iSpecialCardIndex = iSpecialCharacterIndex;

						CHECK_NULL_POINTER_BOOL(pProductData);
						AddProductToInventory(pProductData);
					}
				}
			}
		}		
	}

	if (iRet == 1)
	{
		return TRUE;
	}
	return FALSE;
}


BOOL CFSGameUser::EventStepComplateRewardPopup(int iParam[])
{
	
	CCompEventUserData* pUserData = GetEventUserData();
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();

	int iEventIndex = GetEvent()->GetEventID();

	SAvatarInfo * pAvatarInfo = NULL;

	pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();

	if( NULL == pAvatarInfo )
	{
		return FALSE;
	}

	if(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == STEP_COMPLATE_REWARD_POPUPMODE_PCROOM_CONNECT_EVENT)
	{
		int iTimeCheckType = 2;
		int iStatus = 0, iIsEventUser = 0;

		if (ODBC_RETURN_SUCCESS != pODBC->EVENT_CheckEventCompleteTimeLog(iEventIndex, iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUP_USERMODE], GetUserIDIndex(), GetGameIDIndex(), iTimeCheckType, iStatus, iIsEventUser))
			return FALSE;

		if (FALSE == iIsEventUser)
			return FALSE;

		CPacketComposer PacketComposer( S2C_REWARD_REQUEST_COMPONENT_RES );
		PacketComposer.Add(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE]);
		PacketComposer.Add(iEventIndex);
		PacketComposer.Add(iStatus);

		for( int iIndex = EVENT_CODE_STEP_COMPLATE_REWARD_REWARDINDEX_1 ; iIndex < EVENT_CODE_MAX_STEP_COMPLATE_REWARD_POPUP ; ++iIndex )
		{
			CHECK_CONDITION_RETURN( iIndex < EVENT_CODE_STEP_COMPLATE_REWARD_REWARDINDEX_1 || iIndex > EVENT_CODE_MAX_STEP_COMPLATE_REWARD_POPUP , FALSE);

			SRewardConfig* pReward = REWARDMANAGER.GetReward(iParam[iIndex]);
			CHECK_NULL_POINTER_BOOL(pReward);

			PacketComposer.Add(pReward->GetRewardValue());
		}

		Send(&PacketComposer);

		return TRUE;
	}

	if(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == STEP_COMPLATE_REWARD_POPUPMODE_PCROOM_CONNECT_EVENT_SPECIAL)
	{
		SRewardConfig* pReward = REWARDMANAGER.GetReward(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_REWARDINDEX_1]);
		CHECK_NULL_POINTER_BOOL(pReward);

		CPacketComposer PacketComposer( S2C_REWARD_REQUEST_COMPONENT_RES );
		PacketComposer.Add(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE]);
		PacketComposer.Add(iEventIndex);
		PacketComposer.Add((int)-1);
		PacketComposer.Add(pReward->GetRewardValue());
		Send(&PacketComposer);

		return TRUE;
	}
	
	if(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == STEP_COMPLATE_REWARD_POPUPMODE_PCROOM_CONNECT_EVENT_INIT)
	{
		int iStatus = -1;

		CPacketComposer PacketComposer( S2C_REWARD_REQUEST_COMPONENT_RES );
		PacketComposer.Add(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE]);
		PacketComposer.Add(iEventIndex);
		PacketComposer.Add(iStatus);
		Send(&PacketComposer);

		return TRUE;
	}

	vector<int> vecRewardStep;
	int iRet = 0;

	if (pODBC->EVENT_LoadEventStepReward(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUP_USERMODE],GetEvent()->GetEventID(),GetUserIDIndex(),GetGameIDIndex(),vecRewardStep) == ODBC_RETURN_SUCCESS)
	{
		if (vecRewardStep.size() <= 0)
		{
			return FALSE;
		}
		if (GetEvent()->CheckRewardStep(vecRewardStep[0]) == TRUE)
		{
			if (iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == 1 || iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == -1)
			{
				if (pAvatarInfo->iAvatarTeamIndex == SPECIAL_CHARACTER_TEAM_INDEX_SNSD)
				{// ���䳢
					CPacketComposer PacketComposer( S2C_EVENT_COMPONENT_MSG_RES );

					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 13 );
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);		
					PacketComposer.Add( 0);

					Send( &PacketComposer );

					return FALSE;
				}
				CPacketComposer PacketComposer( S2C_EVENT_COMPONENT_MSG_RES );

				PacketComposer.Add( 4 );
				PacketComposer.Add( 0);
				PacketComposer.Add( 2 );
				PacketComposer.Add( 6 );
				PacketComposer.Add( vecRewardStep[0] );
				PacketComposer.Add( -999999 );
				PacketComposer.Add( 0);
				PacketComposer.Add( 0);		
				PacketComposer.Add(iEventIndex);
				
				Send( &PacketComposer );
			}
			else if(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == 2)
			{
				if ( pAvatarInfo->iAvatarTeamIndex == SPECIAL_CHARACTER_TEAM_INDEX_SNSD)
				{
					CPacketComposer PacketComposer( S2C_EVENT_COMPONENT_MSG_RES );

					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 13 );
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);		
					PacketComposer.Add( 0);

					Send( &PacketComposer );


					return FALSE;
				}

				CPacketComposer PacketComposer( S2C_REWARD_REQUEST_COMPONENT_RES );
				PacketComposer.Add(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE]);
				int iSex = pAvatarInfo->iSex;
				int iLv = pAvatarInfo->iLv;
				int iPosition = pAvatarInfo->Status.iGamePosition;
				int iPublisher = GetPublisherIDIndex();
				int iSpecialCharacterIndex = GetCurUsedAvatar()->iSpecialCharacterIndex;
				
				vector<int> vecReward;
				if (GetEvent()->CheckEventReward(vecRewardStep[0],iSex,iPosition,iLv,iPublisher,iSpecialCharacterIndex,vecReward))
				{
					PBONUS_ST_PRESENT psPresent[3] = {NULL,NULL,NULL};
					PEVENT_ST_BONUS psBonus[3] = {NULL,NULL,NULL};
					PEVENT_ST_COACHCARD psCoachCard[3] = {NULL,NULL,NULL};
					for (int i = 0 ; i < vecReward.size() && i < 3 ; i++)
					{
						GetEvent()->GetRewardIndexToEventPresent(vecReward[i],psPresent[i] ,psBonus[i], psCoachCard[i]);
					}
					//���ػ��� ��ȯ ����˳�����
					PacketComposer.Add(iEventIndex);
					PacketComposer.Add((int)-1); //ToDo Delete
					PacketComposer.Add(vecRewardStep[0]);
					int iPoint = 0;
					int iCash = 0;
					int iItemCode = 0;
					if( psBonus[0] != NULL && psBonus[0]->iPoint != 0 )
					{
						iPoint = psBonus[0]->iPoint;
					}
					if( psBonus[0] != NULL && psBonus[0]->iCash != 0 )
					{
						iCash = psBonus[0]->iCash;
					}
					if( psBonus[1] != NULL && psBonus[1]->iCash != 0 )
					{
						iCash = psBonus[1]->iCash;
					}
					if (psPresent[0] != NULL )
					{
						iItemCode = psPresent[0]->iItemCode;
					}
					if (psPresent[1] != NULL )
					{
						iItemCode = psPresent[1]->iItemCode;
					}
					if (psPresent[2] != NULL )
					{
						iItemCode = psPresent[2]->iItemCode;
					}
					PacketComposer.Add(iPoint);
					PacketComposer.Add(iCash);
					PacketComposer.Add(iItemCode);
					Send( &PacketComposer );
					//---
				}
			}
			else if (iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == 3) // �ű� ���� �˾� ���
			{
				if (pAvatarInfo->iAvatarTeamIndex == SPECIAL_CHARACTER_TEAM_INDEX_SNSD)
				{// �ű�

					CPacketComposer PacketComposer( S2C_EVENT_COMPONENT_MSG_RES );

					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 14 );
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);		
					PacketComposer.Add( 0);

					Send( &PacketComposer );

					return FALSE;
				}
				CPacketComposer PacketComposer( S2C_REWARD_REQUEST_COMPONENT_RES );
				PacketComposer.Add(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE]);
				PacketComposer.Add(iEventIndex);
				Send(&PacketComposer);
			}
			else if (iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == 4) // �޸� ���� �˾� ���.
			{
				if (pAvatarInfo->iAvatarTeamIndex == SPECIAL_CHARACTER_TEAM_INDEX_SNSD)
				{// �޸�
					CPacketComposer PacketComposer( S2C_EVENT_COMPONENT_MSG_RES );

					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 0 );
					PacketComposer.Add( 14 );
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);
					PacketComposer.Add( 0);		
					PacketComposer.Add( 0);

					Send( &PacketComposer );


					return FALSE;
				}
				CPacketComposer PacketComposer( S2C_REWARD_REQUEST_COMPONENT_RES );
				PacketComposer.Add(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE]);
				PacketComposer.Add(iEventIndex);
				Send(&PacketComposer);
			}
			else if (iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE] == 9) // ���Ƚ�� �˾�
			{
				int iSex = pAvatarInfo->iSex;
				int iLv = pAvatarInfo->iLv;
				int iPosition = pAvatarInfo->Status.iGamePosition;
				int iPublisher = GetPublisherIDIndex();
				int iSpecialCharacterIndex = GetCurUsedAvatar()->iSpecialCharacterIndex;
				int iPoint = 0;

				vector<int> vecReward;
				if (GetEvent()->CheckEventReward(vecRewardStep[0],iSex,iPosition,iLv,iPublisher,iSpecialCharacterIndex,vecReward))
				{
					PBONUS_ST_PRESENT psPresent = NULL;
					PEVENT_ST_BONUS psBonus = NULL;
					PEVENT_ST_COACHCARD psCoachCard = NULL;
					GetEvent()->GetRewardIndexToEventPresent(vecReward[0],psPresent,psBonus,psCoachCard);
					if( psBonus != NULL && psBonus->iPoint != 0 )
						iPoint = psBonus->iPoint;
				}
				CPacketComposer PacketComposer( S2C_REWARD_REQUEST_COMPONENT_RES );
				PacketComposer.Add(iParam[EVENT_CODE_STEP_COMPLATE_REWARD_POPUPMODE]);
				PacketComposer.Add(iEventIndex);
				PacketComposer.Add(iPoint);
				Send(&PacketComposer);
			}
		}
		
		return TRUE;
	}
	return FALSE;
}

BOOL CFSGameUser::EventUpdateEventUserStep(int iParam[])
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CCompEventUserData* pEventUserData = GetEventUserData();

	if(NULL != pODBC)
	{
		int iResult = 0;

		vector<int> vecRewardStep;
		if (ODBC_RETURN_FAIL == pODBC->EVENT_LoadEventStepReward(iParam[0],GetEvent()->GetEventID(),GetUserIDIndex(),GetGameIDIndex(),vecRewardStep))
		{
				return FALSE;
		}

		if (vecRewardStep.size() <= 0)
		{
			return FALSE;
		}


		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_UpdateEventStep(iParam[0],GetEvent()->GetEventID(), GetUserIDIndex(), GetGameIDIndex(), vecRewardStep[0], iResult))
		{
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CFSGameUser::EventGiveAndSendSeventhEventCash(int iParam[])
{
	CCompEventUserData* pEventUserData = GetEventUserData();

	if (NULL != pEventUserData)
	{
		int iMode     =  iParam[0];
		int iEventCash = iParam[1];
		int iTargetEventCash = iParam[2];
		int iTargetUserIdindex = -1;
		int iTargetGameIdindex = -1;

		CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();

		if (NULL != pODBC && 0 != iEventCash)
		{
			if (ODBC_RETURN_SUCCESS == pODBC->EVENT_GiveAndSendSeventhEventCash(iMode,GetEvent()->GetEventID(),GetUserIDIndex(),GetGameIDIndex(), iEventCash,iTargetEventCash,iTargetUserIdindex,iTargetGameIdindex ))
			{
				SetEventCoin(GetEventCoin() + iEventCash);
				SendUserGold(GetCoin(), GetEventCoin(), GetSkillPoint(), 1);
				return TRUE;
			}
		}
	}


	return FALSE;
}
BOOL CFSGameUser::EventGameStartLoading(int iParam[])
{
	CPacketComposer PacketComposer(S2C_EVENT_LOADINGIMAGE_REQ);
	PacketComposer.Add(iParam[0]);	
	Send(&PacketComposer);
	return TRUE;
}

BOOL CFSGameUser::EventCheckUserMaxLv(int iParam[])
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CCompEventUserData* pEventUserData = GetEventUserData();
	int iConditionLv = iParam[EVENT_CODE_CONDITION_LV];
	int iMaxLv = 0;

	if (NULL != pEventUserData && iConditionLv > 0 && NULL != pODBC)
	{
		if ( ODBC_RETURN_SUCCESS == pODBC->spAvatarGetUserAvatarMaxLV( GetUserIDIndex(), iMaxLv) && iMaxLv >= iConditionLv )
		{
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CFSGameUser::EventCheckFriendPlayCountOnline(int iParam[])
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CCompEventUserData* pEventUserData = GetEventUserData();



	int iPlayCount = iParam[EVENT_CODE_CONDITION_FRIEND_PLAY_COUNT];
	int iFriendCount = iParam[EVENT_CODE_CONDITION_FRIEND_COUNT];

	int iFriends = 0;	

	CFriend* fr = NULL;

	CLock Lock(m_FriendList.GetLockResource());
	{
		fr = m_FriendList.FindFirst();

		while(fr)
		{
			if( fr->GetConcernStatus() == FRIEND_ST_FRIEND_USER && fr->GetStatus() != USER_ST_OFFLINE && fr->GetWithPlayCnt() >= iPlayCount ) 
			{
				if ( iFriendCount <= ++iFriends )
				{
					if (NULL != pEventUserData  && NULL != pODBC)
					{
						if ( ODBC_RETURN_SUCCESS == pODBC->EVENT_CheckFriendPlayCountComplate( GetEvent()->GetEventID(), GetGameIDIndex() ) )
						{
							return TRUE;
						}
					}
					return FALSE;
				}
			}
			fr = m_FriendList.FindNext();		
		}
	};

	return FALSE;
}

DEFINE_EVENT_FUNCTION(FSGameUser,EVENT_GIVE_PCROOM_EVENT_REWARD)
{
	int iEventIndex = GetEvent()->GetEventID();

	int iTimeCheckType = 2;
	int iStatus = 0, iIsEventUser = 0;

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	if (ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_CheckEventCompleteTimeLog(iEventIndex, iParam[EVENT_CODE_GIVE_PCROOM_EVENT_REWARD_USERMODE], GetUserIDIndex(), GetGameIDIndex(), iTimeCheckType, iStatus, iIsEventUser))
		return FALSE;

	CHECK_CONDITION_RETURN(EVENT_CODE_GIVE_PCROOM_EVENT_REWARD_REWARDINDEX_MAX <= iStatus && 0 > iStatus , FALSE);

	int iRewardIndex = iParam[EVENT_CODE_GIVE_PCROOM_EVENT_REWARD_REWARDINDEX_1 + iStatus];
	CHECK_CONDITION_RETURN(0 >= iRewardIndex, FALSE);

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, iRewardIndex);
	CHECK_NULL_POINTER_BOOL(pReward);

	int iRewardMsgLen = strlen(pReward->szRewardMessage);
	if (0 < iRewardMsgLen)
	{
		CPacketComposer Packet(S2C_RECEIVED_EVENT_REWARD_NOT);
		SS2C_RECEIVED_EVENT_REWARD_NOT not;
		not.iRewardType = pReward->iRewardType;
		not.iReward = pReward->GetRewardValue();
		not.iRewardCount = pReward->GetItemCount();
		not.iRewardMsgLen = iRewardMsgLen;

		Packet.Add((PBYTE)&not, sizeof(SS2C_RECEIVED_EVENT_REWARD_NOT));
		Packet.Add((PBYTE)pReward->szRewardMessage, not.iRewardMsgLen);
		Send(&Packet);
	}

	return TRUE;
}

DEFINE_EVENT_FUNCTION(FSGameUser,EVENT_GIVE_ITEMBUY_BONUS_REWARD)
{
	CHECK_CONDITION_RETURN(GetPayCash() <= 0, FALSE);

	int iPointRewardIndex = iParam[EVENT_CODE_GIVE_ITEMBUY_POINT_BONUS_RATE_REWARDINDEX];
	int iCashRewardIndex = iParam[EVENT_CODE_GIVE_ITEMBUY_CASH_BONUS_RATE_REWARDINDEX];
	int iEventPointBonus = 0;
	int iEventCashBonus = 0;

	if(iPointRewardIndex > -1)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, iPointRewardIndex, GetPayCash());
		CHECK_NULL_POINTER_BOOL(pReward);
	}

	if(iCashRewardIndex > -1)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, iCashRewardIndex, GetPayCash());
		CHECK_NULL_POINTER_BOOL(pReward);
	}

	return TRUE;
}

void CFSGameUser::UpdateTournamentVictory(int iSeasonIndex, BYTE ucScaleMode)
{
	if( ucScaleMode >= MAX_TOURNAMENT_VICTORY )
	{
		WRITE_LOG_NEW(LOG_TYPE_TOURNAMENT, INVALED_DATA, CHECK_FAIL, "UpdateTournamentVictory - ScaleMode");			
		return;
	}

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	pAvatarInfo->TounamentVictory.Victory[ucScaleMode]++;

	AvatarSeasonTournamentVictoryMap::iterator pos = m_AvatarSeasonInfo.mapSeasonTrounamentVictory.find(iSeasonIndex);
	if( pos != m_AvatarSeasonInfo.mapSeasonTrounamentVictory.end())
	{
		SAvatarTournamentVictoryEntry* pEntry = &pos->second;
		pEntry->Victory[ucScaleMode]++;
	}
	else
	{
		SAvatarTournamentVictoryEntry Entry;
		Entry.Victory[ucScaleMode]++;
		m_AvatarSeasonInfo.mapSeasonTrounamentVictory.insert(AvatarSeasonTournamentVictoryMap::value_type(iSeasonIndex,Entry));
	}
}


BOOL CFSGameUser::EventCountLoad( int iParam[] )
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CHECK_NULL_POINTER_BOOL(pODBC);

	CCompEventUserData* pEventUserData = GetEventUserData();

	int iCount;

	if (iParam[EVENT_CODE_COUNT_LOAD_TARGET_AVARTAR] == 0) //������
	{
		if (ODBC_RETURN_SUCCESS != pODBC->EVENT_LoadUserCount(pEventUserData->GetEventID(),GetUserIDIndex(),iCount))
		{
			return FALSE;
		}
		pEventUserData->SetCount(iCount);
	}
	else
	{

	}
	return TRUE;
}

BOOL CFSGameUser::EventCountIncrease( int iParam[] )
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CHECK_NULL_POINTER_BOOL(pODBC);

	CCompEventUserData* pEventUserData = GetEventUserData();
	pEventUserData->IncreaseCount();
	if (iParam[EVENT_CODE_COUNT_LOAD_TARGET_AVARTAR] == 0) //������
	{
		if (ODBC_RETURN_SUCCESS != pODBC->EVENT_UpdateUserCount (pEventUserData->GetEventID(),GetUserIDIndex(),pEventUserData->GetCount()))
		{
			return FALSE;
		}
	}
	else
	{

	}
	return TRUE;
}

BOOL CFSGameUser::EventGiveAndSendPresent( int iParam[] )
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iPresentIndex;

	if (ODBC_RETURN_SUCCESS == pODBC->EVENT_CompleteGivePresent(GetEvent()->GetEventID(), GetUserIDIndex(), GetGameIDIndex(), 
		iParam[EVENT_CODE_GIVE_AND_SEND_PRESENT_ITEMCODE], iParam[EVENT_CODE_GIVE_AND_SEND_PRESENT_PROPERTYVALUE], iParam[EVENT_CODE_GIVE_AND_SEND_PRESENT_PROPERTYINDEX1], iParam[EVENT_CODE_GIVE_AND_SEND_PRESENT_PROPERTYINDEX2], iPresentIndex, MAIL_PRESENT_ON_AVATAR))
	{
		RecvPresent(MAIL_PRESENT_ON_AVATAR, iPresentIndex);
		return TRUE;
	}
	return FALSE;
}


BOOL CFSGameUser::EventPopUp( int iParam[] )
{
	int iArgCnt = 0;

	int iPopupType = iParam[EVENT_CODE_POPUP_CONFIG_TYPE];
	int iPopupRequest = iParam[EVENT_CODE_POPUP_CONFIG_REQUEST_CONFIG];
	int iPopupIndex = iParam[EVENT_CODE_POPUP_INDEX];
	int iPopupArgFirst = iParam[EVENT_CODE_POPUP_ARG_1];
	int iPopupArgSecond = iParam[EVENT_CODE_POPUP_ARG_2];
	int iPopupArgThird = iParam[EVENT_CODE_POPUP_ARG_3];
	int iPopupArgFourth = iParam[EVENT_CODE_POPUP_ARG_4];

	if (iPopupType == -1)
		iPopupType = 0;
	if (iPopupRequest == -1)
		iPopupRequest = 0;
	if (iPopupIndex == -1)
		iPopupIndex = 0;

	if (iPopupArgFourth != -1)
		iArgCnt = 4;
	else if (iPopupArgThird != -1)
		iArgCnt = 3;
	else if (iPopupArgSecond != -1)
		iArgCnt = 2;
	else if (iPopupArgFirst != -1)
		iArgCnt = 1;

	SendEventComponentMsg(iPopupType , iPopupRequest , iArgCnt, iPopupIndex, iPopupArgFirst, iPopupArgSecond, iPopupArgThird, iPopupArgFourth);
	return TRUE;
}


BOOL CFSGameUser::EventCompleteNewUser( int iParam[] )
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS == pODBC->EVENT_CompleteUserFirstCharacter(GetUserIDIndex()))
	{		
		return TRUE;
	}

	WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_CompleteUserFirstCharacter");	
	return FALSE;
}


void CFSGameUser::SendEventComponentMsg( int iPopupType , int iPopupRequestType , int iArgCnt , int iPopupIndex, int iArgFirst, int iArgSecond, int iArgThird, int iArgFourth )
{
	CPacketComposer PacketComposer( S2C_EVENT_COMPONENT_MSG_RES );
	PacketComposer.Add( iPopupType );
	PacketComposer.Add( iPopupRequestType );
	PacketComposer.Add( iArgCnt );
	PacketComposer.Add( iPopupIndex );
	PacketComposer.Add( iArgFirst );
	PacketComposer.Add( iArgSecond );
	PacketComposer.Add( iArgThird );
	PacketComposer.Add( iArgFourth );
	PacketComposer.Add( 0);

	Send( &PacketComposer );
}

bool CFSGameUser::AddLuckItem()
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemListWithGameID( GetGameID() );
	CHECK_NULL_POINTER_BOOL( pAvatarItemList );
	SUserItemInfo * pLuckItem =  pAvatarItemList->GetPremiumItemWithPropertyKind(LICK_ITEM_KIND_NUM);
	if( pLuckItem	)
	{
		return false;
	}
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );
	SShopItemInfo ShopItemInfo;			
	pItemShop->GetItem(LICK_ITEM_ID , ShopItemInfo);	

	int iSetItemPropertyKind = -1;
	int iSetItemKind = -1;
	int iBuyTatooPriceCash = 0;
	int iBuyTatooPricePoint = 0;

	SUserItemInfo UserItemInfo;
	UserItemInfo.iItemCode = LICK_ITEM_ID;
	UserItemInfo.iChannel = ShopItemInfo.iChannel;
	UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;		
	UserItemInfo.iPropertyType = ShopItemInfo.iPropertyType;
	UserItemInfo.iPropertyKind = ShopItemInfo.iPropertyKind;
	UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
	UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
	UserItemInfo.iPropertyTypeValue = 1;
	UserItemInfo.iStatus = 1;
	UserItemInfo.iSellType = REFUND_TYPE_POINT;
	UserItemInfo.iSellPrice = 0;

	SUserItemInfo itemList[MAX_ITEMS_IN_PACKAGE];
	vector<SUserItemInfo> vUserTempItemInfo;
	int	iCntSupplyItem = 0;
	char szBillingKey[MAX_BILLING_KEY_LENGTH+1];
    szBillingKey[0] = '0'; szBillingKey[1] = '\0';
	int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {LICK_ITEM_KIND_ID, -1, -1, -1, -1, -1};
	char szProductCode[MAX_PRODUCTCODE_LENGTH+1] = "";
	int errorcode = 0;
	int iUpdatedTokenItemCount;
	int iUpdatedClubCoin = 0;
	if( ODBC_RETURN_SUCCESS == pGameODBC->spFSBuyItem( GetUserIDIndex(), GetGameIDIndex(),LICK_ITEM_ID, PRICE_TYPE_CASH, 0,0, 0, UserItemInfo, iaPropertyIndex, 
				PRICE_TYPE_CASH,0,PRICE_TYPE_POINT,0, szBillingKey,itemList, iCntSupplyItem, szProductCode, errorcode, -1/*TokenItemIdx*/, iUpdatedTokenItemCount, 0, GetClubSN(), iUpdatedClubCoin))
	{
		GetUserItemList()->ArrangeItemInfoToAddBuyItem( pAvatarItemList, this, pItemShop, ShopItemInfo, UserItemInfo, iCntSupplyItem, iaPropertyIndex,
			itemList, vUserTempItemInfo );
		GetUserItemList()->AddItemToUserItemList( pAvatarItemList, iCntSupplyItem, UserItemInfo, vUserTempItemInfo );
	}

	CPacketComposer PacketComposer(S2C_GOTLUCKYITEM_BYFAIL3);
	Send(&PacketComposer);
	SendUserStat();		
	SendAllMyOwnItemInfo();
	return true;
}

BOOL CFSGameUser::ProcessOpenNewPlayerGift( int iItemIndex )
{
	CFSGameODBC* pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL(pODBC);

	CFSGameUserItem* pUserItem = GetUserItemList();
	CHECK_NULL_POINTER_BOOL(pUserItem);

	CAvatarItemList *pAvatarItemList = pUserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );

	int iErrorCode = 0;
	CPacketComposer PacketComposer(S2C_OPEN_GIFT_RESULT);		

	int iCntSupplyItem = 0;
	SUserItemInfoEx ItemList[MAX_ITEMS_IN_PACKAGE];
	if( ODBC_RETURN_SUCCESS == pODBC->spOpenNewPlayerGift( GetGameIDIndex(),GetCurUsedAvtarLv(), GetCurUsedAvatarSex(), GetCurUsedAvatarPosition(), GetCurUsedAvatarSpecialCharacterIndex(),
			iItemIndex, ItemList, iCntSupplyItem, iErrorCode ))
	{
		if (0 <= iErrorCode)
		{
			pAvatarItemList->RemoveItem(iItemIndex);
			pAvatarItemList->RemoveItemProperty(iItemIndex);
			pUserItem->ArrangeItemInfoToAddItem( pItemShop, iCntSupplyItem, ItemList );
			if ( 1 == iErrorCode )
			{
				return TRUE;
			}
		}
	}

	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(EVENT_KIND_NONE);
	Send(&PacketComposer);

	return TRUE;
}

BOOL CFSGameUser::ProcessOpenCrazyLevelUpEvent( int iItemIndex )
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_BOOL(pODBC);

	CFSGameUserItem* pUserItem = GetUserItemList();
	CHECK_NULL_POINTER_BOOL(pUserItem);

	CAvatarItemList *pAvatarItemList = pUserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );

	int iErrorCode = 0;
	CPacketComposer PacketComposer(S2C_OPEN_GIFT_RESULT);		

	int iLv = 0;
	if( ODBC_RETURN_SUCCESS == pODBC->EVENT_CRAZYLEVELUP_OpenItem( GetGameIDIndex(), iItemIndex, iLv))
	{
		pAvatarItemList->RemoveItem(iItemIndex);

		PacketComposer.Add(iErrorCode);
		PacketComposer.Add(EVENT_KIND_CRAZY_LEVELUP);

		vector<SRewardInfo> vReward;
		CRAZYLEVELUP.GetRewardInfo(iLv, vReward);
		int iRewardCnt = vReward.size();

		PacketComposer.Add(iRewardCnt);

		SRewardConfig* pReward = NULL;
		for(int i = 0; i < iRewardCnt; ++i)
		{
			if(REWARD_TYPE_ITEM == vReward[i].btRewardType)
				continue;

			pReward = REWARDMANAGER.GiveReward(this, vReward[i].iRewardIndex);
			if(pReward == NULL)
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "EVENT_CRAZYLEVELUP_OpenItem GiveReward failed, UserIDIndex:11866902, RewardIndex:0", GetUserIDIndex(), vReward[i].iRewardIndex);


		SREWARD_INFO sReward;
			sReward.btRewardType = vReward[i].btRewardType;
			sReward.iItemCode = vReward[i].iItemCode;
			sReward.iPropertyType = vReward[i].iPropertyType;
			sReward.iPropertyValue = vReward[i].iPropertyValue;
			PacketComposer.Add((PBYTE)&sReward, sizeof(SREWARD_INFO));
		}
	}
	else
	{
		iErrorCode = -1;
		PacketComposer.Add(iErrorCode);
		PacketComposer.Add(EVENT_KIND_CRAZY_LEVELUP);
		PacketComposer.Add((int)0);
	}
	Send(&PacketComposer);

	return TRUE;
}

VOID CFSGameUser::ProcessAchievementbyGroupAndComplete( int iAchievementConditionGroupIndex, int iAchievementLogGroupIndex, vector<int>& vParameter, VOID* pConditionData/* = NULL*/)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_VOID(pODBC);
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	vector<int> vNewlyReachedAchievementIndex;
	vector<int> vAnnounceAchievementIndex;

	if (FALSE != ProcessAchievementbyGroup(iAchievementConditionGroupIndex, iAchievementLogGroupIndex, vParameter, vNewlyReachedAchievementIndex, pConditionData))
	{
		BOOL bIsCheckPlaying = FALSE;
		if (FALSE != ProcessCompletedAchievements( iAchievementLogGroupIndex, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex, bIsCheckPlaying) &&
			FALSE == vAnnounceAchievementIndex.empty())
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
			if(pCenter)
			{ 
				CPacketComposer packet(G2S_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT);
				SG2S_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT info;
				strncpy_s(info.GameID, _countof(info.GameID), GetGameID(), MAX_GAMEID_LENGTH);
				info.iGameIDIndex = GetGameIDIndex();
				info.iAchievementCount = vAnnounceAchievementIndex.size();
				packet.Add((PBYTE)&info, sizeof(SG2S_ANNOUNCE_COMPLETE_ACHIEVEMENT_NOT));

				for (int iLoopIndex = 0; iLoopIndex < info.iAchievementCount; iLoopIndex++)
				{
					ANNOUNCE_COMPLETE_ACHIEVEMENT_INDEX index;
					index.iAchievementIndex = vAnnounceAchievementIndex[iLoopIndex];
					packet.Add(index.iAchievementIndex);
				}
				pCenter->Send(&packet);
			}

			CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
			CHECK_NULL_POINTER_VOID(pMatch);

			SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
			info.iGameIDIndex = GetGameIDIndex();

			strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), _countof(info.szGameID)-1);
			info.iAchievementCount = vAnnounceAchievementIndex.size();

			for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
			{
				info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
			}

			CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
			PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));
			pMatch->Send(&PacketComposer);		
		}
	}
}

BOOL CFSGameUser::CheckAccItemSelectNotice()
{
	SAvatarInfo * pAvatarInfo = NULL;
	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	if( pAvatarInfo == NULL || NULL == pAvatarItemList ) return FALSE;
	
	BOOL bAccItemNoSel = TRUE, bHadAccItem = FALSE;

	for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != pAvatarInfo->AvatarFeature.FeatureInfo[i] )
		{
			SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatarInfo->AvatarFeature.FeatureInfo[i]);
			if( pSlotItem != NULL )
			{
				if( ( (pSlotItem->iBigKind == ITEM_BIG_KIND_ACC && pSlotItem->iPropertyKind > -1 && pSlotItem->iPropertyNum > 0 ) || 
					((MIN_LINK_ITEM_PROPERTY_KIND_NUM <= pSlotItem->iPropertyKind && pSlotItem->iPropertyKind <= MAX_LINK_ITEM_PROPERTY_KIND_NUM) || ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC == pSlotItem->iPropertyKind)) )
				{
					bHadAccItem = TRUE;
					if ( TRUE == pAvatarItemList->CheckUserApplyAccItemProperty(pSlotItem->iItemIdx) )
					{
						bAccItemNoSel = FALSE;
						break;
					}
				}
			}
		}
	}

	return ( bHadAccItem && bAccItemNoSel );
}
void  CFSGameUser::SendEventListInfo()
{
	CFSGameODBC *pGameODBC= (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	if ( NULL == pAvatarInfo )
	{
		return ;
	}

	int nEventNum= 0;
	int  nStatus[MAX_EVENT_LIST_COUNT] = {0};
	char szTitle[MAX_EVENT_LIST_COUNT][16] = {0};
	char szMsg[MAX_EVENT_LIST_COUNT][32] ={0};
	char szStep[MAX_EVENT_LIST_COUNT][32] = {0};
	if( ODBC_RETURN_SUCCESS != pGameODBC->GetUserEventInfoList(GetUserIDIndex(),GetGameIDIndex(),nEventNum,nStatus,szTitle,szMsg,szStep) )
	{
		return;
	}

	CPacketComposer PacketRes(S2C_EVENTLIST_INFO_RES); 
	PacketRes.Add(&nEventNum);
	for( int i = 0; i < nEventNum; ++i )
	{
		PacketRes.Add(&nStatus[i]);
		PacketRes.Add((BYTE*)szTitle[i],16);
		PacketRes.Add((BYTE*)szMsg[i],32);
		PacketRes.Add((BYTE*)szStep[i],32);
	}

	Send(&PacketRes);

}

void CFSGameUser::DisconnectUser()
{
	GetClient()->DisconnectClient();
}

CAvatarItemList* CFSGameUser::GetAvatarItemList()
{	
	return m_UserItem->GetCurAvatarItemList(); 
}

BOOL CFSGameUser::RecvPresent(int iMailType, int iPresentIndex)
{	
	CHECK_CONDITION_RETURN(MAIL_PRESENT_ON_AVATAR != iMailType && MAIL_PRESENT_ON_ACCOUNT != iMailType, FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_BOOL(pODBC);

	CAvatarItemList* pAvatarItemList = GetAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	SPresent* present = new SPresent;
	CHECK_NULL_POINTER_BOOL(present);

	int iGameIDIndex = GetGameIDIndex();

	if(ODBC_RETURN_SUCCESS != pODBC->spGetPresent(GetUserIDIndex(), iGameIDIndex, iMailType, iPresentIndex, present))
	{
		SAFE_DELETE(present);
		return FALSE;
	}

	if(false == GetPresentList(iMailType)->Add(present))
	{
		SAFE_DELETE(present);
		return FALSE;
	}
	
	BYTE btIsValid = PRESENT_INFO_ISVALID_STATUS_NOT_VALID;
	if ( present->tExpireDate > present->tSendTime + 5*60 )
	{
		btIsValid = PRESENT_INFO_ISVALID_STATUS_IS_VALID;
	}

	CPacketComposer PacketComposer(S2C_RECV_PRESENT);
	PacketComposer.Add((BYTE)iMailType);
	PacketComposer.Add(present->GetIndex());
	PacketComposer.Add((BYTE*)present->szSendID, MAX_GAMEID_LENGTH+1);	
	PacketComposer.Add((BYTE*)present->szTitle, MAX_PRESENT_TITLE_LENGTH+1);
	PacketComposer.Add((DWORD)present->GetDate());	
	PacketComposer.Add((BYTE)btIsValid);
	CheckAndSend(&PacketComposer, IsPlaying());	

	return TRUE;
}

BOOL CFSGameUser::RecvPresentList( int iMailType, list<int> listPresentIndex )
{
	CHECK_CONDITION_RETURN(MAIL_PRESENT_ON_AVATAR != iMailType && MAIL_PRESENT_ON_ACCOUNT != iMailType, FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_BOOL(pODBC);

	char szPresentIndex[MAX_PRESENT_INDEX_LENGTH+1];
	ZeroMemory(szPresentIndex, MAX_PRESENT_INDEX_LENGTH+1);

	ConvertDBInputStrWithList(szPresentIndex, MAX_PRESENT_INDEX_LENGTH, &listPresentIndex);

	list<SPresent> listPresent;
	if(ODBC_RETURN_SUCCESS != pODBC->spGetPresentList(GetUserIDIndex(), GetGameIDIndex(), iMailType,  szPresentIndex , listPresent))
	{
		WRITE_LOG_NEW(LOG_TYPE_PRESENT, CALL_SP, FAIL, "spGetPresentList  UserIDIndex(11866902)  GameIDIndex(0)  MailType(18227200)" , 
DIndex(), GetGameIDIndex(), iMailType);
		
		return FALSE;
	}

	for( auto iter = listPresent.begin() ; iter != listPresent.end() ; ++iter )
	{
		SPresent* present = new SPresent;
		CHECK_NULL_POINTER_BOOL(present);

		present->Copy((*iter));

		if(false == GetPresentList(iMailType)->Add(present))
		{
			SAFE_DELETE(present);
			return FALSE;
		}

		BYTE btIsValid = PRESENT_INFO_ISVALID_STATUS_NOT_VALID;
		if ( present->tExpireDate > present->tSendTime + 5*60 )
		{
			btIsValid = PRESENT_INFO_ISVALID_STATUS_IS_VALID;
		}

		/// Ŭ�� �����Ǽ� ��Ŷ�� ��ĥ�� �־����� �� ���ڽ��ϴ�..
		CPacketComposer PacketComposer(S2C_RECV_PRESENT);
		PacketComposer.Add((BYTE)iMailType);
		PacketComposer.Add(present->GetIndex());
		PacketComposer.Add((BYTE*)present->szSendID, MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add((BYTE*)present->szTitle, MAX_PRESENT_TITLE_LENGTH+1);
		PacketComposer.Add((DWORD)present->GetDate());	
		PacketComposer.Add((BYTE)btIsValid);
		CheckAndSend(&PacketComposer, IsPlaying());	

	}

	listPresent.clear();

	return TRUE;
}

BOOL CFSGameUser::GiveReward_ExhaustItemToInven(SRewardConfigItem* pReward, int iValue/* = 0*/)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo ItemInfo;
	if (FALSE == pItemShop->GetItem(pReward->iItemCode, ItemInfo))
	{
		return FALSE;
	}

	CHECK_CONDITION_RETURN(ITEM_PROPERTY_NORMAL != ItemInfo.iPropertyType && ITEM_PROPERTY_EXHAUST != ItemInfo.iPropertyType, FALSE);

	//ITEM_PROPERTY_NORMAL �Ӽ��� �߰��Ǹ� ItemCount Ƚ����ŭ ������ �����ؾ�
	for (int i = 0; i < pReward->iItemCount; ++i)
	{
		SUserItemInfo UserItemInfo;
		UserItemInfo.SetBaseInfo(ItemInfo);

		if (ODBC_RETURN_SUCCESS != pODBC->REWARD_GiveExhaustItemToInven(GetUserIDIndex(), GetGameIDIndex(), pReward->iRewardIndex, iValue, UserItemInfo.iItemIdx, UserItemInfo.iPropertyTypeValue, UserItemInfo.iPropertyNum))
		{
			WRITE_LOG_NEW(LOG_TYPE_REWARD, CALL_SP, FAIL, "REWARD_GiveExhaustItemToInven");
			return FALSE;
		}

		if (ItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MIN && ItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MAX)
		{
			CUserHighFrequencyItem* pHighFrequencyItem = GetUserHighFrequencyItem();
			if ( pHighFrequencyItem != NULL )
			{
				SUserHighFrequencyItem sUserHighFrequencyItem;
				sUserHighFrequencyItem.iPropertyKind = ItemInfo.iPropertyKind;
				sUserHighFrequencyItem.iPropertyTypeValue = UserItemInfo.iPropertyTypeValue;
				pHighFrequencyItem->AddUserHighFrequencyItem( sUserHighFrequencyItem );
				SendHighFrequencyItemCount(ItemInfo.iPropertyKind);
			}
		}
		else
		{
			UserItemInfo.iItemCode = pReward->iItemCode;
			UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;
			UserItemInfo.iSellType = REFUND_TYPE_POINT;

			if (RESULT_UPDATE_EXHAUST_ITEM_ADDED == GetAvatarItemList()->UpdateExhaustItemPropertyValue(UserItemInfo))
			{
				if (0 < UserItemInfo.iPropertyNum)
				{
					pODBC->FSGetAvatarItemProperty(GetAvatarItemList(), GetGameIDIndex(), UserItemInfo.iItemIdx);
				}
			}
		}
	}

	return TRUE;
}

BOOL CFSGameUser::GiveReward_ExhaustAndTimeItemToInven(SRewardConfigItem* pReward)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo ItemInfo;
	if (FALSE == pItemShop->GetItem(pReward->iItemCode, ItemInfo))
	{
		return FALSE;
	}

	CHECK_CONDITION_RETURN(ITEM_PROPERTY_TIME != ItemInfo.iPropertyType, FALSE);

	vector<int> vItemIdx;

	SUserItemInfo UserItemInfo;
	UserItemInfo.SetBaseInfo(ItemInfo);

	if (ODBC_RETURN_SUCCESS != pODBC->REWARD_GiveExhaustAndTimeItemToInven(GetGameIDIndex(), pReward->iRewardIndex, vItemIdx, UserItemInfo.iPropertyTypeValue, UserItemInfo.ExpireDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_REWARD, CALL_SP, FAIL, "REWARD_GiveExhaustAndTimeItemToInven");
		return FALSE;
	}

	for(int i = 0; i < vItemIdx.size(); i++)
	{
		UserItemInfo.iItemIdx = vItemIdx[i];
		UserItemInfo.iItemCode = pReward->iItemCode;
		UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;
		UserItemInfo.iSellType = REFUND_TYPE_POINT;

		GetAvatarItemList()->UpdateExhaustItemPropertyValue(UserItemInfo);
	}
	
	return TRUE;
}

BOOL CFSGameUser::GiveReward_CoachCard(SRewardConfigCoachCard* pReward, int iValue/*= 0*/)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iInventoryIndex = INVALID_IDINDEX;
	int iProductIndex = INVALID_IDINDEX;
	int iItemIDNumber = pReward->iItemCode;
	int iItemCount = pReward->iItemCount;
	if(iValue > 0)
	{
		iItemCount *= iValue;
	}
	int iItemType = COACHCARD.GetItemType( iItemIDNumber );
	int iTendencyType = TENDENCY_TYPE_NONE;
	int iGradeLevel = RANDOMCARD_GRADE_LEVEL_ALL;
	int iNewInventoryIndex = INVALID_IDINDEX;
	int iNewProductIndex = INVALID_IDINDEX;
	int iSpecialCardIndex = COACHCARD.GetSpecialCardIndex( iItemIDNumber );
	int iRarityLevel = RARITY_LEVEL_NONE;

	vector<int> vecGradeLevel;
	COACHCARD.GetGradeLevel(iItemIDNumber, vecGradeLevel);

	if (1 < vecGradeLevel.size() || 0 == vecGradeLevel.size())	
		iGradeLevel = RANDOMCARD_GRADE_LEVEL_ALL;
	else
		iGradeLevel = vecGradeLevel[0];

	SDateInfo sProductionDate, sExpireDate;
	int iNewProductCount;

	GetOwnedProductItemInfo(iItemIDNumber, iProductIndex, iInventoryIndex);

	if( ODBC_RETURN_SUCCESS == pODBC->REWARD_GiveCoachCard(GetGameIDIndex(), iItemIDNumber, iTendencyType, iGradeLevel, iItemType, iItemCount, 
		 iProductIndex, iInventoryIndex, iNewProductIndex, iNewInventoryIndex, iNewProductCount, sProductionDate, sExpireDate, iSpecialCardIndex, iRarityLevel))
	{
		SProductData ProductData;
		ProductData.iInventoryIndex = iNewInventoryIndex;
		ProductData.iProductIndex = iNewProductIndex;
		ProductData.iItemIDNumber =iItemIDNumber;
		ProductData.iItemType = iItemType;
		ProductData.iTendencyType = iTendencyType;
		ProductData.iGradeLevel = iGradeLevel;
		ProductData.sProductionDate = sProductionDate;
		ProductData.sExpireDate = sExpireDate;
		ProductData.iProductCount = iNewProductCount;
		ProductData.iSpecialCardIndex = iSpecialCardIndex;
		ProductData.iRarityLevel = iRarityLevel;

		AddProductToInventory(ProductData);

		return TRUE;
	}

	return FALSE;
}

void CFSGameUser::SendAvatarInfo()
{
	int iItemNum = 0, iResult = 1;
	int	iFace = -1, iCharacterType = -1;
	SFeatureInfo FeatureInfo;
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SAvatarInfo AvatarInfo;
	if( FALSE == CurUsedAvatarSnapShot( AvatarInfo ))
	{
		iResult = -1;
	}
	else
	{
		if (FS_ITEM == GetClient()->GetState() ||
			FS_CLUB == GetClient()->GetState())	//�̸����Ⱑ ������ ������
		{
			GetUserItemList()->GetTempItemFeature(FeatureInfo);
			GetUserItemList()->GetTempFace(iFace);
			GetUserItemList()->GetTempCharacterType(iCharacterType);
		}
		else
		{
			AvatarInfo.AvatarFeature.GetShowFeature(FeatureInfo.iaFeature, FeatureInfo.iChannel);
			iFace = AvatarInfo.iFace;
			iCharacterType = AvatarInfo.iCharacterType;
		}

		const int cnDefaultData = -1;
		if( cnDefaultData  == iFace || cnDefaultData == iCharacterType )
		{
			AvatarInfo.AvatarFeature.GetShowFeature(FeatureInfo.iaFeature, FeatureInfo.iChannel);
			iFace = AvatarInfo.iFace;
			iCharacterType = AvatarInfo.iCharacterType;
		}
	}
	
	CPacketComposer PacketComposer(S2C_FS_AVATAR_SELECT_RES);
	
	if( 1 == iResult )
	{
		for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
		{
			if( -1 != FeatureInfo.iaFeature[i] ) iItemNum++;
		}
		
		int iIsEmptySlot = 0, iSlotType = 0;
		int iCurSlotNum = 0, iTotalSlotNum = 0;
		
		if( AvatarInfo.iSlotType == NORMAL_EMPTY_CHARACTER_SLOT_TYPE || AvatarInfo.iSlotType == EXTRA_EMPTY_CHARACTER_SLOT_TYPE || AvatarInfo.iSlotType == FIRST_CHARACTER_SLOT_TYPE )
		{
			iIsEmptySlot = 0;
			
			iSlotType = AvatarInfo.iSlotType;
		}
		else if( AvatarInfo.iSlotType == NORMAL_CHARACTER_SLOT_TYPE || AvatarInfo.iSlotType == EXTRA_CHARACTER_SLOT_TYPE )
		{
			iIsEmptySlot = 1;
			iSlotType = AvatarInfo.iSlotType;
		}
		PacketComposer.Add(iResult);
		int iState = static_cast<int>(GetClient()->GetState());

		PacketComposer.Add(iState);
		PacketComposer.Add(iIsEmptySlot);
		PacketComposer.Add(iSlotType);
		PacketComposer.Add(iCurSlotNum);
		PacketComposer.Add(iTotalSlotNum);
		PacketComposer.Add((BYTE*)AvatarInfo.szGameID, MAX_GAMEID_LENGTH+1);	
		PacketComposer.Add(AvatarInfo.iLv);
		PacketComposer.Add(AvatarInfo.iFameLevel);
		PacketComposer.Add(AvatarInfo.iLeague);
		PacketComposer.Add(AvatarInfo.Status.iGamePosition);
		PacketComposer.Add(GetSkillPoint());
		PacketComposer.Add(GetCoin());
		int iSkillTiketCount = GetUserItemList()->GetSkillTicketCount();
		PacketComposer.Add(iSkillTiketCount);

		PacketComposer.Add(GetEventCoin());
		PacketComposer.Add((int)IsClubMember());
		PacketComposer.Add(iCharacterType);
		PacketComposer.Add(iFace);
		PacketComposer.Add(AvatarInfo.iSpecialCharacterIndex);
		PacketComposer.Add(AvatarInfo.iAvatarTeamIndex);
		PacketComposer.Add(AvatarInfo.sIsLock);
		PacketComposer.Add((short)0); //ToDo - Delete nIsPCRoomCharacter

		BYTE btCloneType = (BYTE)(CheckCloneCharacter_Index() + 1);
		PacketComposer.Add(btCloneType); // TODO - �ﱹ��2 ���� ��ȯ�� ������ ĳ������.
		PacketComposer.Add(AvatarInfo.Status.iHeight);
		PacketComposer.Add(iItemNum);
		
		BYTE btIsWearClubUniform = 0;
		for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
		{		
			if( -1 != FeatureInfo.iaFeature[i] ) 
			{				
				PacketComposer.Add(FeatureInfo.iaFeature[i]);
				SUserItemInfo* pItem = GetUserItemList()->GetCurAvatarItemList()->GetItemWithItemCodeInAll(FeatureInfo.iaFeature[i]);

				if( pItem )
					PacketComposer.Add(pItem->iChannel);
				else
					PacketComposer.Add((int)0);

				pItem = GetUserItemList()->GetCurAvatarItemList()->GetItemWithItemCodeNotChangeDressItem(FeatureInfo.iaFeature[i]);
				if(pItem)
				{
					PacketComposer.Add(GetUserItemList()->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
					if (0 < pItem->iPropertyNum)
					{
						vector< SUserItemProperty > vUserItemProperty;
						GetUserItemList()->GetCurAvatarItemList()->GetItemProperty(pItem->iItemIdx, vUserItemProperty);

						PacketComposer.Add((int)vUserItemProperty.size());
						for (int i = 0; i < vUserItemProperty.size() ; ++i )
						{
							PacketComposer.Add(&vUserItemProperty[i].iProperty);
							PacketComposer.Add(&vUserItemProperty[i].iValue);
							PacketComposer.Add(&vUserItemProperty[i].iAssignChannel);
						}
					}
					else
					{
						PacketComposer.Add((int)0);
					}
				}
				else
				{
					PacketComposer.Add((BYTE)ACCITEM_STATUS_NOT_CHECKBOX);
					PacketComposer.Add((int)0);	//PropertyNum
				}

				if(TRUE == CLUBCONFIGMANAGER.IsClubUniform(FeatureInfo.iaFeature[i]))
					btIsWearClubUniform = 1;
			}
		}

		PacketComposer.Add(AvatarInfo.iClubMarkCode);
		PacketComposer.Add(AvatarInfo.btClubPosition);

		PBYTE pIsWearClubUniform = PacketComposer.GetTail();
		PacketComposer.Add(btIsWearClubUniform);
		PacketComposer.Add(AvatarInfo.btUnifromNum);
		PacketComposer.Add(AvatarInfo.iEquippedAchievementTitle);
		PacketComposer.Add(AvatarInfo.SkillLv.iSkillLvCode);

		if (FS_ITEM == GetClient()->GetState() ||
			FS_CLUB == GetClient()->GetState())
		{
			int iOwnItemChannelNum = 0;
			PBYTE pOwnItemChannelNum = PacketComposer.GetTail();
			PacketComposer.Add(iOwnItemChannelNum);

			if(GetUserItemList()->GetMode() != ITEM_PAGE_MODE_CLUBSHOP)
			{
				AvatarInfo.AvatarFeature.GetShowFeature(FeatureInfo.iaFeature, FeatureInfo.iChannel);
			}

			for(int i = 0; i < MAX_ITEMCHANNEL_NUM; i++)
			{
				if( -1 != FeatureInfo.iaFeature[i] )
				{
					PacketComposer.Add(FeatureInfo.iaFeature[i]);
					iOwnItemChannelNum++;

					if(TRUE == CLUBCONFIGMANAGER.IsClubUniform(FeatureInfo.iaFeature[i]))
						btIsWearClubUniform = 1;
				}
			}
			memcpy(pOwnItemChannelNum, &iOwnItemChannelNum, sizeof(iOwnItemChannelNum));

			int iChangeDressItemChannelNum = 0;
			PBYTE pChangeDressItemChannelNum = PacketComposer.GetTail();
			PacketComposer.Add(iChangeDressItemChannelNum);

			if(GetUserItemList()->GetMode() == ITEM_PAGE_MODE_CLUBSHOP) // club shop
			{
				SFeatureInfo FeatureInfo;
				GetUserItemList()->GetTempChangeDressItemFeature(FeatureInfo);
				for(int i = 0; i < MAX_CHANGE_DRESS_NUM; i++)
				{
					if( -1 != FeatureInfo.iaChangeDressFeature[i] )
					{
						PacketComposer.Add(FeatureInfo.iaChangeDressFeature[i]);
						iChangeDressItemChannelNum++;

						if(TRUE == CLUBCONFIGMANAGER.IsClubUniform(FeatureInfo.iaChangeDressFeature[i]))
							btIsWearClubUniform = 1;
					}
				}
			}
			else 
			{
				for(int i = 0; i < MAX_CHANGE_DRESS_NUM; i++)
				{
					if( -1 != AvatarInfo.AvatarFeature.ChangeDressFeatureInfo[i] )
					{
						PacketComposer.Add(AvatarInfo.AvatarFeature.ChangeDressFeatureInfo[i]);
						iChangeDressItemChannelNum++;

						if(TRUE == CLUBCONFIGMANAGER.IsClubUniform(AvatarInfo.AvatarFeature.ChangeDressFeatureInfo[i]))
							btIsWearClubUniform = 1;
					}
				}
			}
			memcpy(pChangeDressItemChannelNum, &iChangeDressItemChannelNum, sizeof(iChangeDressItemChannelNum));
			memcpy(pIsWearClubUniform, &btIsWearClubUniform, sizeof(btIsWearClubUniform));
		}	
		else if(FS_SKILL == GetClient()->GetState())
		{
			BYTE btTransformType = AvatarInfo.GetTransformType();
			PacketComposer.Add(btTransformType);

			if(TRANSFORM_IMMORTALS == btTransformType)
			{
				PacketComposer.Add(AvatarInfo.iTransformFace);

				short stItemChannelNum = 0;
				PBYTE pItemChannelNum = PacketComposer.GetTail();
				PacketComposer.Add(stItemChannelNum);

				for(int i = 0; i < MAX_ITEMCHANNEL_IMMORTALS_TRANSFORM; i++)
				{
					if(-1 != AvatarInfo.AvatarFeature.TransformFeatureInfo[i])
					{
						PacketComposer.Add(&AvatarInfo.AvatarFeature.TransformFeatureInfo[i]);
						stItemChannelNum++;
					}
				}
				memcpy(pItemChannelNum, &stItemChannelNum, sizeof(stItemChannelNum));
			}				
			else if(TRANSFORM_DRAGONTIGER == btTransformType)
			{
				int iFace = 0;
				int iaTransformFeature[MAX_ITEMCHANNEL_NUM] = {0,};
				int iaTransformFeature_Show[MAX_ITEMCHANNEL_NUM] = {0,};
				GetTheOtherCloneFeature(AvatarInfo.iSpecialCharacterIndex, iFace, iaTransformFeature, iaTransformFeature_Show);

				PacketComposer.Add(iFace);

				short stItemChannelNum = 0;
				PBYTE pItemChannelNum = PacketComposer.GetTail();
				PacketComposer.Add(stItemChannelNum);

				for(int i = 0; i < MAX_ITEMCHANNEL_NUM; i++)
				{
					if(0 < iaTransformFeature_Show[i])
					{
						PacketComposer.Add(&iaTransformFeature_Show[i]);
						stItemChannelNum++;
					}
				}
				memcpy(pItemChannelNum, &stItemChannelNum, sizeof(stItemChannelNum));
			}	
		}
	}
	else 
	{
		iResult = -1;
		PacketComposer.Add(iResult);
	}
	
	Send(&PacketComposer);
}

void CFSGameUser::SendAvatarInfoUpdateToMatch(bool bIsRoomChangeClone/* = false*/)
{
	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	SG2M_AVATAR_INFO_UPDATE update;

	update.iGameIDIndex = GetGameIDIndex();
	update.btServerIndex = _GetServerIndex;
	memcpy(&update.AvatarInfo, pAvatar, sizeof(SAvatarInfo));
	GetAvatarItemStatus(update.iAvatarAddStat);
	SetAvatarFameSkillStatus(update.iAvatarAddStat);
	GetUserCharacterCollection()->GetSentenceStatus(update);
	GetAvatarLinkItemStatus(update.iLinkItemAddStat);
	update.iProtectionRing = CheckAndGetSelfProtectionRingCount();
	CheckAndGetProtectHand( update.iProtectHandKind, update.iProtectHandNum );
	GetUserBufferItemInfo( update.BufferItemInfo );
	memcpy(&update.UserFactionInfo, &m_UserFactionInfo, sizeof(SUserFactionBase));
	GetUserBasketBall()->GetBallPartSlotInfo(update.bBallUseOption, update.iaBallPartSlot);
	GetUserPowerupCapsule()->GetPowerupCapsuleStatus(update.iAvatarAddStat);
	GetCheerLeaderStatus(update.iAvatarAddStat);
	GetLvIntervalBuffItemStatus(update.iAvatarAddStat);
	GetAvatarSpecialPieceProperty(update.iAvatarAddStat);
	update.bIsLastGameBest = GetUserAppraisal()->GetLastGameBest();
	update.bIsRoomChangeClone = bIsRoomChangeClone;
	GetDragonTigerCloneInfo(update.AvatarInfo, update.iaDragonTigerCloneStat);		

	CPacketComposer PacketComposer(G2M_AVATAR_INFO_UPDATE);
	PacketComposer.Add((BYTE*)&update, sizeof(SG2M_AVATAR_INFO_UPDATE));
	GetUserSkill()->WriteSpecialtySkill(PacketComposer, update.AvatarInfo.Skill);

	pMatch->Send(&PacketComposer);
}

void CFSGameUser::GetDragonTigerCloneInfo(SAvatarInfo& sAvatar, int iaCloneStat[MAX_STAT_TYPE_COUNT])
{
	CHECK_CONDITION_RETURN_VOID(TRANSFORM_DRAGONTIGER != sAvatar.GetTransformType());

	CHECK_CONDITION_RETURN_VOID(FALSE == GetCloneCharacter());

	int iaTransformFeature[MAX_ITEMCHANNEL_NUM] = {0,};
	GetTheOtherCloneFeature(sAvatar.iSpecialCharacterIndex, sAvatar.iTransformFace, iaTransformFeature, sAvatar.AvatarFeature.TransformFeatureInfo);

	int iaTempClonStat[MAX_STAT_NUM] = {0,};
	BOOL bTemp = FALSE;

	// Add Stat ���
	GetAvatarItemStatus(iaTempClonStat, bTemp, NULL, iaTransformFeature);
	SetAvatarFameSkillStatus(iaTempClonStat);
	GetUserCharacterCollection()->GetSentenceStatus(iaTempClonStat);
	GetUserPowerupCapsule()->GetPowerupCapsuleStatus(iaTempClonStat);
	GetCheerLeaderStatus(iaTempClonStat);
	GetLvIntervalBuffItemStatus(iaTempClonStat);
	GetAvatarSpecialPieceProperty(iaTempClonStat);

	memcpy(iaCloneStat, iaTempClonStat, sizeof(int) * MAX_STAT_TYPE_COUNT);

	Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > GetUseStyleIndex() ? Clone_Index_Clone : Clone_Index_Mine ;// �Ųٷ� �ٲ���� ��.
	CHECK_CONDITION_RETURN_VOID(Clone_Index_None == eIndex);

	// ĳ���� �⺻ Stat ������
	m_sCloneCharacterInfo[eIndex].GetStatAdd(iaCloneStat);
}

void CFSGameUser::GetTheOtherCloneFeature(IN const int iSpecialAvatarIndex, OUT int& iFace, OUT int iaTransformFeature[MAX_ITEMCHANNEL_NUM], OUT int iaiaTransformFeature_Show[MAX_ITEMCHANNEL_NUM]/* = nullptr*/)
{
	int iCloneSpecialAvatarIndex = 0;
	CHECK_CONDITION_RETURN_VOID(FALSE == AVATARCREATEMANAGER.CheckCloneAvailableCharacter(iSpecialAvatarIndex, iCloneSpecialAvatarIndex));

	SSpecialAvatarConfig* pConfig = AVATARCREATEMANAGER.GetSpecialAvatarConfigByIndex(iCloneSpecialAvatarIndex);
	CHECK_CONDITION_RETURN_VOID(nullptr == pConfig);

	Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > GetUseStyleIndex() ? Clone_Index_Clone : Clone_Index_Mine ;// �Ųٷ� �ٲ���� ��.
	CHECK_CONDITION_RETURN_VOID(Clone_Index_None == eIndex);

	SAvatarStyleInfo* pStyleInfo = GetStyleInfo();
	CHECK_CONDITION_RETURN_VOID(nullptr == pStyleInfo);

	int iStyleIndex = GetCloneCharacterStyleIndex(eIndex) - 1;
	CHECK_CONDITION_RETURN_VOID(0 > iStyleIndex || MAX_STYLE_COUNT <= iStyleIndex);

	CAvatarItemList* pAvatarItemList = GetUserItemList()->GetCurAvatarItemList();
	CHECK_CONDITION_RETURN_VOID(nullptr == pAvatarItemList);

	SAvatarFeature AvatarFeature;
	int iFeatureIndex = 0, iChangeDressFeatureIndex = 0;

	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != pStyleInfo->iItemIdx[iStyleIndex][i] )
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(pStyleInfo->iItemIdx[iStyleIndex][i]);
			if(nullptr == pItem)
				continue;

			if(pItem->iStatus != ITEM_STATUS_INVENTORY && pItem->iStatus != ITEM_STATUS_WEAR)
				continue;

			int iPropertyKind = pItem->iPropertyKind;
			int iChannel = pItem->iChannel;
			int iItemCode = pItem->iItemCode;
			int iItemIdx = pItem->iItemIdx;

			iaTransformFeature[i] = iItemCode;

			if(nullptr != iaiaTransformFeature_Show)
			{
				if(iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX && iChangeDressFeatureIndex < MAX_CHANGE_DRESS_NUM)
				{
					AvatarFeature.ChangeDressFeatureInfo[iChangeDressFeatureIndex] = iItemCode;
					AvatarFeature.ChangeDressFeatureProtertyChannel[iChangeDressFeatureIndex] = iChannel;
					AvatarFeature.iTotalChangeDressChannel += iChannel;

					iChangeDressFeatureIndex++;
				}
				else if(iFeatureIndex > -1 && iFeatureIndex < MAX_ITEMCHANNEL_NUM)
				{
					if((AvatarFeature.iTotalChannel & iChannel) == 0)
					{
						AvatarFeature.FeatureItemIndex[iFeatureIndex] = iItemIdx;
						AvatarFeature.FeatureInfo[iFeatureIndex] = iItemCode;
						AvatarFeature.FeatureProtertyChannel[iFeatureIndex] = iChannel;
						AvatarFeature.iTotalChannel += iChannel;

						iFeatureIndex++;
					}
				}
			}
		}
	}

	if(nullptr != iaiaTransformFeature_Show)
	{
		int iChannel = 0;
		SAvatarInfo* pAvatar = GetCurUsedAvatar();
		if(pAvatar)
		{
			memcpy(AvatarFeature.BasicItemInfo, pAvatar->AvatarFeature.BasicItemInfo, sizeof(SFeatureItem) * MAX_ITEMCHANNEL_NUM);
		}
		AvatarFeature.GetShowFeature(iaiaTransformFeature_Show, iChannel);
	}

	iFace = pConfig->_iFace;
}

void CFSGameUser::SendAvatarNoBroadcastInfoUpdateToMatch()
{
	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CPacketComposer PacketComposer(G2M_AVATAR_NO_BROADCAST_INFO_UPDATE);

	SG2M_AVATAR_NO_BROADCAST_INFO_UPDATE info;
	info.iGameIDIndex = GetGameIDIndex();
	info.iAdvantageUserType = CheckAdvantageUserType();
	PacketComposer.Add((PBYTE)&info, sizeof(SG2M_AVATAR_NO_BROADCAST_INFO_UPDATE));

	MakeCoachCardActionInfluenceList( PacketComposer);
	MakeCoachCardPotentialComponentList( PacketComposer);

	pMatch->Send(&PacketComposer);
}

void CFSGameUser::InitClubInvitationList(vector<SClubInvitationData*>* vecClubInvitation)
{
	vector<SClubInvitationData*>::iterator itrClubInvitation = vecClubInvitation->begin();
	SClubInvitationData* pClubInvitationData = NULL;

	for( ; itrClubInvitation != vecClubInvitation->end() ; itrClubInvitation++)
	{
		pClubInvitationData = *itrClubInvitation;		
		if(pClubInvitationData) 
		{
			m_ClubInvitationList.Add(pClubInvitationData);
		}
	}
	vecClubInvitation->erase(vecClubInvitation->begin(), vecClubInvitation->end());
}

void CFSGameUser::RecvClubInvitation(int iPaperIndex, CFSClubBaseODBC* pClubBaseODBC)
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	SClubInvitationData* pClubInvitationData = new SClubInvitationData;
	CHECK_NULL_POINTER_VOID(pClubInvitationData);
	
	int iResult = 0;
	if(ODBC_RETURN_SUCCESS != pClubBaseODBC->SP_Club_GetInvitation(iPaperIndex, pAvatar->iGameIDIndex, pClubInvitationData, iResult))
	{
		SAFE_DELETE(pClubInvitationData);
		return;
	}
	else
	{
		if(iResult < 0)
		{
			SAFE_DELETE(pClubInvitationData);
			WRITE_LOG_NEW(LOG_TYPE_CLUB, DB_DATA_LOAD, FAIL, "SP_Club_GetInvitation");
			return;
		}
	}

	if(false == GetClubInvitationList()->Add(pClubInvitationData))
	{
		SAFE_DELETE(pClubInvitationData);
		return;
	}

	if(true == IsPlaying()) 
	{
		CPacketComposer *pPacketComposer = new CPacketComposer(S2C_CLUB_RECV_INVITE_PAPER);

		CHECK_NULL_POINTER_VOID(pPacketComposer);

		pPacketComposer->Add(pClubInvitationData->GetIndex());
		pPacketComposer->Add((BYTE*)pClubInvitationData->szSendGameID, MAX_GAMEID_LENGTH+1);
		pPacketComposer->Add((BYTE*)pClubInvitationData->szTitle, MAX_MESSAGE_TITLE_LENGTH+1);
		pPacketComposer->Add((DWORD)pClubInvitationData->GetDate());	

		PushPacketQueue(pPacketComposer);
	}
	else
	{
		CPacketComposer PacketComposer(S2C_CLUB_RECV_INVITE_PAPER);

		PacketComposer.Add(pClubInvitationData->GetIndex());
		PacketComposer.Add((BYTE*)pClubInvitationData->szSendGameID, MAX_GAMEID_LENGTH+1);
		PacketComposer.Add((BYTE*)pClubInvitationData->szTitle, MAX_MESSAGE_TITLE_LENGTH+1);
		PacketComposer.Add((DWORD)pClubInvitationData->GetDate());	

		Send(&PacketComposer);
	}
}

BOOL CFSGameUser::SendClubInvitationList()
{
	BYTE count = m_ClubInvitationList.GetSize();
	if(count < 0) 
	{
		return TRUE;
	}
	SClubInvitationData* pClubInvitationData= NULL;
	CPacketComposer PacketComposer(S2C_CLUB_INVITE_LIST_RES);

	pClubInvitationData = m_ClubInvitationList.FindFirst();

	int iPageNumber = count / MAX_SEND_COUNT_IN_PACKET;
	for(int i =0; i <= iPageNumber ; i++)
	{
		BYTE bySendCount = 0;
		if(i < iPageNumber)
		{
			bySendCount = MAX_SEND_COUNT_IN_PACKET;
		}
		else
		{
			bySendCount = count MAX_SEND_COUNT_IN_PACKET;

		}

		PacketComposer.Add(bySendCount);

		for(int j = 0 ; j < bySendCount ; j++)
		{				
			CHECK_NULL_POINTER_BOOL(pClubInvitationData);

			PacketComposer.Add(pClubInvitationData->GetIndex());		
			PacketComposer.Add((BYTE)pClubInvitationData->bIsCheck);		
			PacketComposer.Add((BYTE*)pClubInvitationData->szSendGameID, MAX_GAMEID_LENGTH+1);		
			PacketComposer.Add((BYTE*)pClubInvitationData->szTitle, MAX_MESSAGE_TITLE_LENGTH+1);	
			PacketComposer.Add((DWORD)pClubInvitationData->GetDate());	

			pClubInvitationData = m_ClubInvitationList.FindNext();
		}
		Send(&PacketComposer);
	}

	return TRUE;
}

void CFSGameUser::SendClubInvitation(int iPaperindex)
{
	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB );
	CHECK_NULL_POINTER_VOID( pClubBaseODBC );

	SAvatarInfo *pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	SClubInvitationData* pClubInvitationData = m_ClubInvitationList.Find(iPaperindex);
	CHECK_NULL_POINTER_VOID(pClubInvitationData);

	short	textLen = strlen(pClubInvitationData->szContent);
	if(pClubInvitationData->bIsCheck != 1)
	{
		pClubInvitationData->bIsCheck = 1;
		pClubBaseODBC->SP_Club_ReadInvitation(pAvatar->iGameIDIndex, iPaperindex);
	}

	CPacketComposer PacketComposer(S2C_CLUB_INVITE_DATA_RES);
	PacketComposer.Add(pClubInvitationData->GetIndex());
	PacketComposer.Add(pClubInvitationData->iClubIndex);
	PacketComposer.Add(textLen);
	PacketComposer.Add((BYTE*)pClubInvitationData->szContent, textLen);		

	Send(&PacketComposer);
}

BOOL CFSGameUser::CheckPaperExist( int iPaperIndex )
{
	SClubInvitationData* pClubInvitationData = m_ClubInvitationList.Find(iPaperIndex);
	if(NULL == pClubInvitationData)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CFSGameUser::GiveRandomReward( int iRewardGroupIdx, int iRewardRandomIdx , VectorRewardInfo& vecRewardInfo )
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	if( NULL == pODBC )
		return FALSE;

	if( ODBC_RETURN_SUCCESS != pODBC->spAvaterRandomReward( GetUserIDIndex(), GetGameIDIndex(), iRewardGroupIdx, iRewardRandomIdx , vecRewardInfo ) )
	{
		return FALSE;
	}

	for ( int i = 0; i < vecRewardInfo.size(); ++ i )
	{
		if ( REWARD_TYPE_POINT == vecRewardInfo[i].nRewardType )
		{
			SetSkillPoint( GetSkillPoint()  + vecRewardInfo[i].nRewardValue );
			AddPointAchievements(); 
		}
		else if ( REWARD_TYPE_TROPHY == vecRewardInfo[i].nRewardType )
		{
			SetUserTrophy( GetUserTrophy() + vecRewardInfo[i].nRewardValue );
		}
	}

	return TRUE;
}

void CFSGameUser::InitClub()
{
	SAvatarInfo *pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	pAvatar->bClubMember		= FALSE;
	pAvatar->iClubSN			= 0;
	memset(pAvatar->szClubName, 0, MAX_CLUB_NAME_LENGTH+1);
	pAvatar->iClubMarkCode		= 0;
	pAvatar->btClubGrade		= 0;
	pAvatar->btClubPosition		= 0;
	pAvatar->btUnifromNum		= 0;

	pAvatar->iClubCoin = 0;
	pAvatar->iClubPlayCount = 0;
	pAvatar->tClubPlayCountUpdateDate = -1;
	pAvatar->btClubLeagueGrade = 0;
	pAvatar->iClubContributionPoint = 0;

	pAvatar->iClubContributionPointRankSeasonIndex = 0;
	pAvatar->iClubContributionPointSeasonRank = 0;
	pAvatar->iClubLeagueRankSeasonIndex = 0;
	pAvatar->iClubLeagueSeasonRank = 0;

	pAvatar->iUserClubContributionPointRankSeasonIndex = 0;
	pAvatar->iUserClubContributionPointRank = 0;
	pAvatar->iUserClubLeagueRankSeasonIndex = 0;
	pAvatar->iUserClubLeagueRank = 0;

	if(pAvatar->iCheerLeaderCode > 0 &&
		FALSE == CHEERLEADEREVENT.CheckCheerLeader(pAvatar->iCheerLeaderCode))
	{
		pAvatar->iCheerLeaderCode	= GetUserCheerLeader()->GetDefaultCheerLeaderIndex();
		if(pAvatar->iCheerLeaderCode == GetUserCheerLeader()->GetClubCheerLeaderIndex())
			pAvatar->iCheerLeaderCode = 0;

		if (FALSE == IsPlaying())
		{
			CMatchSvrProxy* pMatchSvrProxy = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
			if(pMatchSvrProxy)
			{
				SG2M_CHEERLEADER_CHANGE_REQ info;
				info.iGameIDIndex = GetGameIDIndex();
				info.iCheerLeaderCode = pAvatar->iCheerLeaderCode;
				pMatchSvrProxy->SendCheerLeaderChangeReq(info);
			}
		}
	}

	GetUserCheerLeader()->SetClubCheerLeaderIndex(0);
	GetUserItemList()->RemoveClubItem();
}

void CFSGameUser::SetClubName(char* szClubName)
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	strncpy_s(pAvatar->szClubName, MAX_CLUB_NAME_LENGTH+1, szClubName, MAX_CLUB_NAME_LENGTH);
}

void CFSGameUser::SetClub(BYTE btKind, SClubInfo sInfo, int iCheerLeaderCode)
{
	SAvatarInfo *pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	pAvatar->bClubMember		= TRUE;
	pAvatar->iClubSN			= sInfo.iClubSN;
	memcpy(pAvatar->szClubName, sInfo.szClubName, MAX_CLUB_NAME_LENGTH+1);
	pAvatar->iClubMarkCode		= sInfo.iClubMarkCode;
	pAvatar->btClubGrade		= sInfo.btGrade;
	pAvatar->btClubPosition		= sInfo.btClubPosition;
	pAvatar->btUnifromNum		= sInfo.btUnifromNum;
	memcpy(&pAvatar->iPennantCode, &sInfo.iPennantCode, sizeof(int)*MAX_CLUB_PENNANT_SLOT_COUNT);
	pAvatar->btClubLeagueGrade	= sInfo.btLeagueGrade;
	pAvatar->iClubContributionPointRankSeasonIndex = sInfo.iContributionPointRankSeasonIndex;
	pAvatar->iClubContributionPointSeasonRank = sInfo.iContributionPointSeasonRank;
	pAvatar->iClubLeagueRankSeasonIndex = sInfo.iLeagueRankSeasonIndex;
	pAvatar->iClubLeagueSeasonRank = sInfo.iLeagueSeasonRank;
	pAvatar->iClubContributionPoint = sInfo.iUserContributionPointRecord;
	pAvatar->iUserClubContributionPointRankSeasonIndex = sInfo.iUserContributionPointRankSeasonIndex;
	pAvatar->iUserClubContributionPointRank = sInfo.iUserContributionPointSeasonRank;
	pAvatar->iUserClubLeagueRankSeasonIndex = sInfo.iUserLeagueRankSeasonIndex;
	pAvatar->iUserClubLeagueRank = sInfo.iUserLeagueSeasonRank;
	pAvatar->iLastSeasonContributionPoint = sInfo.iLastSeasonContributionPoint;
	pAvatar->iLastSeasonClubLeaguePoint = sInfo.iLastSeasonClubLeaguePoint;

	if(btKind == CLUB_INFO_REQUEST_KIND_LOGIN)
	{
		if(pAvatar->iCheerLeaderCode == 0 ||
			(pAvatar->iCheerLeaderCode > 0 &&
			FALSE == CHEERLEADEREVENT.CheckCheerLeader(pAvatar->iCheerLeaderCode) &&
			pAvatar->iCheerLeaderCode != iCheerLeaderCode))
			pAvatar->iCheerLeaderCode	= sInfo.sCheerLeaderInfo.iCheerLeaderCode;

		if(pAvatar->iCheerLeaderCode == 0)
			pAvatar->iCheerLeaderCode = GetUserCheerLeader()->GetDefaultCheerLeaderIndex();

		GetUserCheerLeader()->SetClubCheerLeaderIndex(sInfo.sCheerLeaderInfo.iCheerLeaderCode);
		GetUserCheerLeader()->SetClubCheerLeaderTypeNum(sInfo.sCheerLeaderInfo.btTypeNum);
		CheckGiveClubClothes(sInfo.iClothesItemCode, sInfo.tClothesItemExpireDate);

		GiveClubUserRankReward();
	}
}

void CFSGameUser::UpdateGameReultClubInfo(SCL2G_CLUB_USER_UPDATE_GAMERESULT_NOT& not)
{
	SAvatarInfo *pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	pAvatar->iClubContributionPoint = not.iClubContributionPoint;
}

void CFSGameUser::UpdateClubCoin(int iClubCoin, int iClubPlayCount, time_t tClubPlayCountUpdateDate)
{
	CHECK_CONDITION_RETURN_VOID(0 >= iClubPlayCount);

	SAvatarInfo *pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	if(ODBC_RETURN_SUCCESS == pODBC->CLUB_GiveUserClubCoin(pAvatar->iClubSN, pAvatar->iGameIDIndex, iClubCoin, iClubPlayCount, tClubPlayCountUpdateDate, pAvatar->iClubCoin))
	{
		pAvatar->iClubPlayCount		= iClubPlayCount;
		pAvatar->tClubPlayCountUpdateDate = tClubPlayCountUpdateDate;
	}
}

void CFSGameUser::CheckGiveClubClothes(int* iClothesItemCode, time_t* tExpireDate)
{
	CAvatarItemList* pAvatarItemList = GetUserItemList()->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	for(int iCnt = 0; iCnt < MAX_CLUB_CLOTHESITEM_COUNT; ++iCnt)
	{
		if(iClothesItemCode[iCnt] > 0)
		{
			SDateInfo sExpireDate;
			SUserItemInfo* pUserItemInfo = pAvatarItemList->GetItemWithItemCodeInAll(iClothesItemCode[iCnt]);
			if ((NULL != pUserItemInfo && 
				(pUserItemInfo->ExpireDate != tExpireDate[iCnt]) ||
				NULL == pUserItemInfo))
			{
				GiveClubClothes(iClothesItemCode[iCnt]);
			}
		}
	}
};

void CFSGameUser::GiveClubClothes(int iItemCode)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SShopItemInfo ShopItem;
	pItemShop->GetItem(iItemCode, ShopItem);

	CAvatarItemList* pAvatarItemList = GetUserItemList()->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iItemIdx = -1;
	SUserItemInfo* pUserItemInfo = pAvatarItemList->GetItemWithItemCodeInAll(iItemCode);
	if (NULL != pUserItemInfo)
	{
		iItemIdx = pUserItemInfo->iItemIdx;
	}

	int iUpdatedItemIdx = 0;
	time_t tExpireDate = 0;
	if(ODBC_RETURN_SUCCESS == pGameODBC->ITEM_GiveClubClothes(GetClubSN(), GetGameIDIndex(), iItemIdx, iItemCode, iUpdatedItemIdx, tExpireDate))
	{
		if(NULL != pUserItemInfo)
		{
			pUserItemInfo->ExpireDate = tExpireDate;
		}
		else
		{
			SUserItemInfo UserItemInfo;
			UserItemInfo.iCategory = ShopItem.iCategory;
			UserItemInfo.iBigKind = ShopItem.iBigKind;
			UserItemInfo.iSmallKind = ShopItem.iSmallKind;
			UserItemInfo.iChannel = ShopItem.iChannel;
			UserItemInfo.iSexCondition = ShopItem.iSexCondition;
			UserItemInfo.iPropertyType = ShopItem.iPropertyType;
			UserItemInfo.iPropertyKind = ShopItem.iPropertyKind;
			UserItemInfo.iPropertyTypeValue = 0;	
			UserItemInfo.iItemCode = iItemCode;
			UserItemInfo.iItemIdx = iUpdatedItemIdx;
			UserItemInfo.iSellType = ShopItem.iSellType;
			UserItemInfo.iSellPrice = 0;
			UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;
			UserItemInfo.ExpireDate = tExpireDate;
			pAvatarItemList->AddItem(UserItemInfo);
		}
	}	
}

BOOL CFSGameUser::CreateClub_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CLUB, INVALED_DATA, CHECK_FAIL, "CreateClub_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	CFSClubBaseODBC* pClubODBC	= (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_BOOL(pClubODBC);

	DECLARE_INIT_TCHAR_ARRAY(szClubName, MAX_CLUB_NAME_LENGTH+1);
	memcpy(szClubName, pBillingInfo->szItemName, MAX_CLUB_NAME_LENGTH+1);
	int iClubAreaIndex = pBillingInfo->iParam1;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;

	BYTE btResult = CLUB_CREATE_RESULT_SUCCESS;
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			int iClubSN = 0;
			btResult = pClubODBC->CLUB_Create(szClubName, GetUserIDIndex(), GetGameIDIndex(), iClubAreaIndex, iPrevCash, iPostCash, iClubSN);
			if(ODBC_RETURN_SUCCESS == btResult)
			{
				SetUserBillResultAtMem(pBillingInfo->iSellType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
				SendUserGold();

				CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
				if(pClubProxy != NULL)
				{ 
					SG2CL_CLUB_CREATE_REQ info;
					info.iClubSN = iClubSN;
					strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), MAX_GAMEID_LENGTH);
					strncpy_s(info.szClubName, _countof(info.szClubName), szClubName, MAX_CLUB_NAME_LENGTH);
					info.iClubAreaIndex = iClubAreaIndex;
					info.iGameIDIndex = GetGameIDIndex();
					info.iGamePosition = GetCurUsedAvatarPosition();
					info.iLv = GetCurUsedAvtarLv();

					pClubProxy->SendClubCreateReq(info); 
				}
			}
		}
		break;
	default:
		{
			btResult = CLUB_CREATE_RESULT_FAIL_SUPPLY_CASH;
		}
		break;
	}

	if ( CLUB_CREATE_RESULT_SUCCESS != btResult )
	{
		CPacketComposer	PacketComposer(S2C_CLUB_CREATE_RES);
		PacketComposer.Add(btResult);
		PacketComposer.Add(0);	
		Send(&PacketComposer);
	}
	return (BUY_SKILL_ERROR_SUCCESS == btResult);	
}

BOOL CFSGameUser::DonationClub_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CLUB, INVALED_DATA, CHECK_FAIL, "DonationClub_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	CFSClubBaseODBC* pClubODBC	= (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_BOOL(pClubODBC);

	int iAddClubCL = pBillingInfo->iParam1;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;

	BYTE btResult = CLUB_DONATION_RESULT_SUCCESS;
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			int iUpdatedClubCash = 0;
			if(ODBC_RETURN_SUCCESS == (btResult = pClubODBC->CLUB_BuyClubCash(GetClubSN(), GetUserIDIndex(), GetGameIDIndex(), iAddClubCL, iPrevCash, iPostCash, iUpdatedClubCash)))
			{
				SetUserBillResultAtMem(pBillingInfo->iSellType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
				SendUserGold();

				CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
				if(pClubProxy != NULL)
				{ 
					SG2CL_CLUB_DONATION_REQ info;
					info.iClubSN = GetClubSN();
					strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), MAX_GAMEID_LENGTH);
					info.iAddClubCL = iAddClubCL;
					info.iClubCL = iUpdatedClubCash;
					pClubProxy->SendClubDonationReq(info); 
				}
				else
				{
					CPacketComposer	PacketComposer(S2C_CLUB_DONATION_RES);
					PacketComposer.Add(btResult);
					PacketComposer.Add(iUpdatedClubCash);
					Send(&PacketComposer);
					SendUserGold();
				}
			}
		}
		break;
	default:
		{
			btResult = CLUB_CREATE_RESULT_FAIL_SUPPLY_CASH;
		}
		break;
	}

	return (BUY_SKILL_ERROR_SUCCESS == btResult);	
}

BOOL CFSGameUser::CheckCheerLeaderAbility(int iShootSuccessRate, int iBlockSuccessRate)
{
	if(iShootSuccessRate == 0 && iBlockSuccessRate == 0)
		return TRUE;

	CHECK_CONDITION_RETURN(0 == m_AvatarStatCheckInfo.iCheerLeaderCode, FALSE);

	return CLUBCONFIGMANAGER.CheckCheerLeaderAbillity(m_AvatarStatCheckInfo.iCheerLeaderCode, iShootSuccessRate, iBlockSuccessRate);
}

void CFSGameUser::ClubInfoUpdateToCenterSvr()
{
	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pCenter);

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	SG2S_USER_CLUB_STATUS_UPDATE info;
	strncpy_s(info.GameID, _countof(info.GameID), pAvatar->szGameID, MAX_GAMEID_LENGTH);	
	info.iClubSN = pAvatar->iClubSN;
	info.iClubMark = pAvatar->iClubMarkCode;

	pCenter->SendPacket(G2S_USER_CLUB_STATUS_UPDATE, &info, sizeof(SG2S_USER_CLUB_STATUS_UPDATE));
}

void CFSGameUser::Load_ClubBonusByProffer( CFSODBCBase* pODBCBase )
{
	CHECK_NULL_POINTER_VOID(pODBCBase);
	if( GetClubSN() > 0 )
	{
		pODBCBase->SP_GetClubBonusByProffer(GetUserIDIndex(),GetGameIDIndex(),GetClubSN());
	}
}

BOOL CFSGameUser::MakeShopItemSelectInfo(CPacketComposer& PacketComposer, int iOp, SShopItemInfo& ItemInfo, int iItemCode/* = 0*/, int iRenewItemIdx/*=0*/, int iRecvUserLv/* = 0*/, int iSecretStoreIndex/* = 0*/)
{
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	CShopItemList* pShopItemList = pItemShop->GetShopItemList();
	CHECK_NULL_POINTER_BOOL(pShopItemList);

	CFSGameUserItem* pUserItemList = (CFSGameUserItem*)GetUserItemList();
	CHECK_NULL_POINTER_BOOL(pUserItemList);

	CAvatarItemList* pAvatarItemList = (CAvatarItemList*)pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	vector<SUserItemProperty> vRechangeUserItemProperty;
	vector<BOOL>			  vRechangeUserItemProChecked;

	if( FS_ITEM_OP_BUY == iOp || FS_ITEM_OP_BUY_SECRETSTOREITEM == iOp || FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp)
		CheckExpiredEventCoin();

	PacketComposer.Add(iOp);

	int iItemMultiStatType = ITEM_MULTI_STAT_TYPE_NORMAL;
	PBYTE pLocationMultiStatType = PacketComposer.GetTail();
	PacketComposer.Add(iItemMultiStatType);

	if ( FS_ITEM_OP_RENEW == iOp )
	{
		SUserItemInfo* pUserItem = pAvatarItemList->GetItemWithItemIdx( iRenewItemIdx );
		CHECK_NULL_POINTER_BOOL(pUserItem);
		CHECK_CONDITION_RETURN(ITEM_STATUS_RESELL == pUserItem->iStatus, FALSE);
			
		pAvatarItemList->GetItemProperty(iRenewItemIdx, vRechangeUserItemProperty );
	}

	int iUserLv = -1;
	if( iRecvUserLv > 0 )
		iUserLv = iRecvUserLv;
	else
		iUserLv = GetCurUsedAvtarLv();

	if( ItemInfo.iSellType == SELL_TYPE_POINT && FS_ITEM_OP_SEND == iOp )
	{
		CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
		CHECK_NULL_POINTER_BOOL( pGameODBC );

		int iNewUserPointResult = 0;
		if( ODBC_RETURN_SUCCESS != pGameODBC->EVENT_CheckNewUserPoint( GetUserIDIndex(), iNewUserPointResult ) )
		{
			iNewUserPointResult = -6;
			PacketComposer.Initialize(S2C_SEND_GIFT_GAME_ID_RES);
			PacketComposer.Add(&iNewUserPointResult);
			PacketComposer.Add((PBYTE)GetGameID(), MAX_GAMEID_LENGTH+1);	
			return TRUE;
		}
			
		if( iNewUserPointResult == -1 )
		{
			iNewUserPointResult = -6;
			PacketComposer.Initialize(S2C_SEND_GIFT_GAME_ID_RES);
			PacketComposer.Add(&iNewUserPointResult);
			PacketComposer.Add((PBYTE)GetGameID(), MAX_GAMEID_LENGTH+1);	
			return TRUE;
		}
	}

	if (0 < iItemCode)
		PacketComposer.Add(iItemCode);
	else
		PacketComposer.Add(ItemInfo.iItemCode0);

	PacketComposer.Add((BYTE)ItemInfo.bIsRefundable);
	PacketComposer.Add(ItemInfo.iSexCondition);
	PacketComposer.Add(ItemInfo.iLvCondition);
	PacketComposer.Add(ItemInfo.iFirstRankCondition);
	PacketComposer.Add(ItemInfo.iLastRankCondition);
	PacketComposer.Add(ItemInfo.iPropertyType);
	
	if (FS_ITEM_OP_BUY == iOp)
		PacketComposer.Add(pAvatarItemList->GetDressItemTokenCount(ItemInfo.iSmallKind));
	else
		PacketComposer.Add((int)0);

	int iPropertyValue = 0;
	int iTypeNum = 0;
		
	CItemPriceList *pPrice = pItemShop->GetItemPriceList(ItemInfo.iItemCode0);
	CHECK_NULL_POINTER_BOOL(pPrice);

	pPrice->GetItemPropertyValue( ItemInfo.iSellType, iPropertyValue );
	if ( -1 == iPropertyValue && FS_ITEM_OP_RENEW == iOp )
	{
		int iReturnValue = BUY_ITEM_ERROR_PERMANENT_NORENEW;
		int iItemIndex = -1;
		int iEndOp = -1;

		PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
		PacketComposer.Add(iOp);
		PacketComposer.Add(&iReturnValue);
		PacketComposer.Add(iItemIndex);
		PacketComposer.Add(iEndOp);
		return TRUE;
	}

	BOOL bCheckAttenDanceItem = FALSE;

	vector< SItemPrice > vItemPrice;
	iTypeNum = pPrice->GetItemPriceVector(ItemInfo.iSellType, vItemPrice);
	if(FS_ITEM_OP_BUY_SECRETSTOREITEM == iOp || 
		FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp ||
		FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOp ||
		FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE == iOp)
		iTypeNum = 1;

	if( OPEN == ATTENDANCEITEMSHOP.IsOpen() &&
		TRUE == ATTENDANCEITEMSHOP.CheckAttendanceEventItemCode(ItemInfo.iItemCode0))
	{
		iTypeNum = 1;
		bCheckAttenDanceItem = TRUE;
	}

	PacketComposer.Add(iTypeNum);
		
	time_t CurrentTime = _time64(NULL);

	int iDiscountRate = 0;
	if (TRUE == ItemInfo.bIsDiscountTarget)
	{
		SetBuyState(iOp);
		iDiscountRate = GetDiscountRate(ItemInfo.iSellType, ItemInfo.iCategory);
	}
	
	if( FS_ITEM_OP_BUY_SECRETSTOREITEM == iOp )
	{
		if( FALSE == IsOpenSecretStore() )
		{
			PacketComposer.Add((int)BUY_ITEM_ERROR_NOT_SELL);
			return FALSE;
		}

		if( iSecretStoreIndex >= SECRET_STORE_INDEX_MAX || iSecretStoreIndex < SECRET_STORE_INDEX_0 )
		{
			PacketComposer.Add((int)BUY_ITEM_ERROR_NOT_SELL);
			return FALSE;
		}

		SSecretStore_ItemInfo	sItemInfo;
		if(FALSE == SECRETSTOREMANAGER.GetStoreItem(m_SecretStoreInfo.btMaxLv_NPU, m_SecretStoreInfo.iStoreGrade[iSecretStoreIndex], iSecretStoreIndex, m_SecretStoreInfo.iStoreItemCode[iSecretStoreIndex], sItemInfo))
		{	
			PacketComposer.Add((int)BUY_ITEM_ERROR_NOT_SELL);
			return FALSE;
		}
		else
		{
			PacketComposer.Add(ItemInfo.iSellType-1);

			PacketComposer.Add(sItemInfo.iPropertyTypeValue);
			PacketComposer.Add(sItemInfo.iSalePrice);
		}
	}
	else if(FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOp)
	{
		if(CLOSED == SALEPLUS.IsOpen())
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add((int)BUY_ITEM_ERROR_EVENT_SALEPLUS_FAIL);
			return TRUE;
		}

		int iYYYYMMDD = GetYYYYMMDD();
		BYTE btDayIndex = SALEPLUS.GetDayIndex(iYYYYMMDD);
		if(TRUE == GetUserSalePlusEvent()->GetBuyInfo(btDayIndex))
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add((int)BUY_ITEM_ERROR_EVENT_SALEPLUS_FAIL);
			return TRUE;
		}

		int iSalePrice = 0;
		BOOL bIsBuyYesterday = GetUserSalePlusEvent()->GetBuyInfo(btDayIndex - 1);
		if(FALSE == SALEPLUS.GetTodayProductSalePrice(iYYYYMMDD, ItemInfo.iItemCode0, iSecretStoreIndex, bIsBuyYesterday, iSalePrice))
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add((int)BUY_ITEM_ERROR_EVENT_SALEPLUS_FAIL);
			return TRUE;
		}

		PacketComposer.Add(ItemInfo.iSellType-1);
		PacketComposer.Add(iSecretStoreIndex);
		PacketComposer.Add(iSalePrice);

		if(	SKILL_SLOT_ITEMCODE == ItemInfo.iItemCode0)
		{
			BOOL bNoticePopUp = FALSE;
			SAvatarInfo* pAvatar = GetCurUsedAvatar();
			if(pAvatar != NULL)
			{
				if(TRUE == CheckSkillSlotExchangeStatus(pAvatar->Skill.SkillSlot, ItemInfo.iItemCode0))
				{
					bNoticePopUp = TRUE;
				}
			}
			PacketComposer.Add(bNoticePopUp);	// ��ų���� ������ ������ ��츸 ���
		}
	}
	else if(FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE == iOp)
	{
		if(CLOSED == DARKMARKET.IsOpen())
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add((int)BUY_ITEM_ERROR_NOT_SELL);
			return TRUE;
		}

		BYTE btProductIndex = static_cast<BYTE>(iSecretStoreIndex);
		int iPrice = vItemPrice[0].GetOriginalPrice();
		if(FALSE == DARKMARKET.CheckBuyPackageProduct(iItemCode, btProductIndex, iPrice))
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add((int)BUY_ITEM_ERROR_NOT_SELL);
			return TRUE;
		}

		PacketComposer.Add(ItemInfo.iSellType-1);
		PacketComposer.Add(iPropertyValue);
		PacketComposer.Add(iPrice);
	}
	else if( FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp )
	{
		int iReturnValue = BUY_ITEM_ERROR_SALE_RANDOMITEM_TIME_OVER;
		int iItemIndex = -1;
		int iEndOp = -1;

		if(FALSE == m_UserSaleRandomItemEvent.CanBuyStatus())
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add(&iReturnValue);
			PacketComposer.Add(iItemIndex);
			PacketComposer.Add(iEndOp);
			return TRUE;
		}

		int iRewardIndex = iSecretStoreIndex;
		if(iRewardIndex <= 0)
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add(&iReturnValue);
			PacketComposer.Add(iItemIndex);
			PacketComposer.Add(iEndOp);
			return TRUE;
		}

		SS2C_SALE_RANDOMITEM_INFO info; 
		if(FALSE == SALERANDOMITEMEVENT.GetSaleItemConfig(iRewardIndex, info))
		{
			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(iOp);
			PacketComposer.Add(&iReturnValue);
			PacketComposer.Add(iItemIndex);
			PacketComposer.Add(iEndOp);
			return TRUE;
		}

		PacketComposer.Add(ItemInfo.iSellType-1);
		PacketComposer.Add(info.iPropertyValue);
		PacketComposer.Add(info.iSalePrice);
	}
	else if( TRUE == bCheckAttenDanceItem )
	{
		int iAttendanceItemPropertyValue = ATTENDANCEITEMSHOP.GetAttendanceShopItemPropertyValue(ItemInfo.iItemCode0, _GetCurrentDBDate);
		CHECK_CONDITION_RETURN( -2 >= iAttendanceItemPropertyValue , FALSE );

		PacketComposer.Add(ItemInfo.iSellType-1);
		PacketComposer.Add(iAttendanceItemPropertyValue);
		PacketComposer.Add((int)0);
	}
	else
	{
		BOOL bHotDeal = FALSE;
		int iSaleDate = 0;
		SHotDealItem sInfo;
		if( FS_ITEM_OP_BUY_TODAYHOTDEAL == iOp )
		{
			bHotDeal= TODAYHOTDEALManager.GetTodayHotDealItem( iSecretStoreIndex, iSaleDate, sInfo);
		}

		for(int j=0;j<iTypeNum;j++)
		{
			int iPropertyValue;
			int iPrice;

			if(TRUE == bHotDeal && sInfo.iPropertyValue == vItemPrice[j].iPropertyValue)
			{
				iPrice = sInfo.iSalePrice;
			}
			else
			{
				if(FALSE == GetAbleSaleItem())
				{
					iPrice = vItemPrice[j].GetOriginalPrice();
				}
				else
				{
					iPrice = vItemPrice[j].GetPrice(CurrentTime, iDiscountRate);
				}
			}

			iPropertyValue = vItemPrice[j].iPropertyValue;

			PacketComposer.Add(ItemInfo.iSellType-1);
			PacketComposer.Add(iPropertyValue);
			PacketComposer.Add(iPrice);
		}

		if(FS_ITEM_OP_BUY == iOp &&
			SKILL_SLOT_ITEMCODE == ItemInfo.iItemCode0)
		{
			BOOL bNoticePopUp = FALSE;
			SAvatarInfo* pAvatar = GetCurUsedAvatar();
			if(pAvatar != NULL)
			{
				if(TRUE == CheckSkillSlotExchangeStatus(pAvatar->Skill.SkillSlot, ItemInfo.iItemCode0))
				{
					bNoticePopUp = TRUE;
				}
			}
			PacketComposer.Add(bNoticePopUp);	// ��ų���� ������ ������ ��츸 ���
		}
	}

	int iPropertyKind		= ItemInfo.iPropertyKind;
	int iSendPropertyKind	= -1;

	CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(iPropertyKind);

	if (FS_ITEM_OP_SEND == iOp && 
		(PREMIUM_ITEM_KIND_POWERUP == iPropertyKind || PREMIUM_ITEM_KIND_POWERUP_PLUS == iPropertyKind)		)
	{
		PacketComposer.Add(iSendPropertyKind);
	}
	else if(pItemPropertyBoxList != NULL && 
		(iPropertyKind < MIN_EXP_UP_ITEM_KIND_NUM || iPropertyKind > MAX_EXP_UP_ITEM_KIND_NUM) &&
		(iPropertyKind < MIN_LEVEL_UP_ITEM_KIND_NUM || iPropertyKind > MAX_LEVEL_UP_ITEM_KIND_NUM) &&
		(iPropertyKind < PROPENSITY_ITEM_KIND_START || iPropertyKind > PROPENSITY_ITEM_KIND_END) &&
		FALSE == bCheckAttenDanceItem)
	{
		CItemPropertyPriceList *pPropertyPriceList = pItemShop->GetItemPropertyPriceList(iPropertyKind);
		CHECK_NULL_POINTER_BOOL(pPropertyPriceList);
			
		// 문신
		if((iPropertyKind >= MIN_BONUS_EXP_PROPERTY_NUM && iPropertyKind <= MAX_BONUS_EXP_PROPERTY_NUM) ||	// 100~199
			(iPropertyKind >= MIN_WIN_TATTOO_KIND_NUM && iPropertyKind <= MAX_WIN_TATTOO_KIND_NUM))			// 200~299
		{
			iSendPropertyKind = 0; // ???? 0

			if( iPropertyKind >= MIN_HEART_TATTO_KIND_NUM && iPropertyKind <= MAX_HEART_TATTO_KIND_NUM )
				iSendPropertyKind = ItemInfo.iPropertyKind;
		}
		else
		{
			iSendPropertyKind = 1; // ?????? 1 ???? ???? iMaxBoxIdx· κ????.
		}
		PacketComposer.Add(iSendPropertyKind);

		list<int>* pListPropertyAssignType = pPropertyPriceList->GetPropertyAssignTypeList();
		CHECK_NULL_POINTER_BOOL(pListPropertyAssignType);

		PacketComposer.Add((BYTE)pListPropertyAssignType->size());

		if (1 < pListPropertyAssignType->size())	//1�̻��̸� �ɷ�ġ 2 ���� (����/��Ʈ ����) �ӵ�
		{
			iItemMultiStatType = ITEM_MULTI_STAT_TYPE_EXTEND;
			memcpy(pLocationMultiStatType, &iItemMultiStatType, sizeof(iItemMultiStatType));
		}

		for(int i=0 ; i<iTypeNum ; i++)
		{
			list<int>::iterator iter = pListPropertyAssignType->begin();
			list<int>::iterator endIter = pListPropertyAssignType->end();
			for ( ; iter != endIter; ++iter)
			{
				SItemPropertyPrice *pItemPropertyPrice = pPropertyPriceList->GetItemPropertyPrice(vItemPrice[i].iPropertyValue, iUserLv, *iter); 
				if (NULL == pItemPropertyPrice)
				{
					PacketComposer.Add((BYTE)0);
					PacketComposer.Add((int)0);
					PacketComposer.Add((int)0);
				}
				else
				{
					PacketComposer.Add((BYTE)pItemPropertyPrice->iPropertyAssignType);
					PacketComposer.Add(pItemPropertyPrice->iPriceCash);
					PacketComposer.Add(pItemPropertyPrice->iPricePoint);
				}
			}
		}
				
		list<int>::iterator iter = pListPropertyAssignType->begin();
		list<int>::iterator endIter = pListPropertyAssignType->end();
		for ( ; iter != endIter; ++iter)
		{
			int iPropertyAssignType = *iter;
			PacketComposer.Add((BYTE)iPropertyAssignType);	

			int iMaxBoxIdx = pItemPropertyBoxList->GetMaxBoxIdxCount(iUserLv, iPropertyAssignType);
			
			PacketComposer.Add(iMaxBoxIdx);
				
			for( int iBoxIdx = 0; iBoxIdx < iMaxBoxIdx ; iBoxIdx++ ) 
			{
				int iMaxCategoryCount = pItemPropertyBoxList->GetMaxCategoryCount(iBoxIdx, iUserLv, iPropertyAssignType );
				vector< SItemPropertyBox > vItemPropertyBox;
				short sSmallMaxNum = (short)pItemPropertyBoxList->GetItemPropertyBoxIdxCount(iBoxIdx, iUserLv, iPropertyAssignType); 
				pItemPropertyBoxList->GetItemPropertyBox(iBoxIdx, iUserLv, iPropertyAssignType, vItemPropertyBox );
				sort( vItemPropertyBox.begin(), vItemPropertyBox.end(), PropertyBoxSort() );

				int iAssignChannel = 0;
				if (!vItemPropertyBox.empty())	iAssignChannel = vItemPropertyBox[0].iAssignChannel;
				PacketComposer.Add(&iAssignChannel);
				PacketComposer.Add(&iMaxCategoryCount);
					
				if((iPropertyKind >= MIN_STAT_PROPERTY_NUM && iPropertyKind <= MAX_STAT_PROPERTY_NUM) ||							
					(iPropertyKind >= MIN_SPECIAL_ITEM_KIND_NUM && iPropertyKind <= MAX_SPECIAL_ITEM_KIND_NUM) ||						
					((iPropertyKind >= MIN_LINK_ITEM_PROPERTY_KIND_NUM && iPropertyKind <= MAX_LINK_ITEM_PROPERTY_KIND_NUM)) ||			
					(iPropertyKind >= MIN_POSITION_POWER_UP_ITEM_KIND_NUM && iPropertyKind <= MAX_POSITION_POWER_UP_ITEM_KIND_NUM) ||
					(iPropertyKind >= ITEM_PROPERTY_KIND_EVENT_ACC_MIN && iPropertyKind <= ITEM_PROPERTY_KIND_EVENT_ACC_MAX))	
				{
					sSmallMaxNum = sSmallMaxNum + 1;

					short	sPriceType = -1;
					int		iDontChooseNum = 1;
					int		iDontChooseSendPropertyIndex	= -1;
					short	sDontChooseSendCategory			= -1;
					short	sDontChooseSendProperty			= -1;
					int		iDontChooseSendValue			= -1;
					int		iDontChooseSendOrder			= -1;

					PacketComposer.Add((PBYTE)&sSmallMaxNum, sizeof(short));

					PacketComposer.Add((PBYTE)&sPriceType, sizeof(short));
					PacketComposer.Add(&iDontChooseNum);
					PacketComposer.Add(&iDontChooseSendPropertyIndex);
					PacketComposer.Add((PBYTE)&sDontChooseSendCategory, sizeof(short));
					PacketComposer.Add((PBYTE)&sDontChooseSendProperty, sizeof(short));
					PacketComposer.Add(&iDontChooseSendValue);
					PacketComposer.Add(&iDontChooseSendOrder);
				}
				else
				{
					PacketComposer.Add((PBYTE)&sSmallMaxNum, sizeof(short));
				}
					
				int iRechangeProperty = -1, iRechangeValue = -1;
				if ( iBoxIdx >=0 && iBoxIdx < vRechangeUserItemProperty.size() )
				{
					iRechangeProperty = vRechangeUserItemProperty[iBoxIdx].iProperty;
					iRechangeValue = vRechangeUserItemProperty[iBoxIdx].iValue;
				}

				for( int i = 0; i < vItemPropertyBox.size() ; i++ )
				{
					int iPropertyIndex = vItemPropertyBox[i].iPropertyIndex;
					CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList( iPropertyIndex );
					int iMaxPropertyNum = pItemProperty->GetPropertyMaxCount();

					SItemPropertyBox* pItemPropertyBox = pItemPropertyBoxList->GetItemPropertyBox( vItemPropertyBox[i].iCategory, iBoxIdx, iUserLv, iPropertyAssignType );
					CHECK_NULL_POINTER_BOOL(pItemPropertyBox);

					short sPriceType			= (short)( pItemPropertyBox->iPriceType - 1);
					if( FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp )
					{
						sPriceType = 2;
					}

					PacketComposer.Add((PBYTE)&sPriceType, sizeof(short));
					PacketComposer.Add(&iMaxPropertyNum);
					for( int iPropertyCount = 0; iPropertyCount < iMaxPropertyNum ; iPropertyCount++ )
					{
						vector< SItemProperty > vItemProperty;
						pItemProperty->GetItemProperty( vItemProperty );
						sort( vItemProperty.begin(), vItemProperty.end(), PropertySort() );

						int iSendPropertyIndex	= vItemProperty[iPropertyCount].iPropertyIndex;
						short sSendCategory		= (short)vItemPropertyBox[i].iCategory;
						short sSendProperty		= (short)vItemProperty[iPropertyCount].iProperty;
						int iSendValue			= vItemProperty[iPropertyCount].iValue;
						int iSendOrder			= vItemProperty[iPropertyCount].iOrder;

						if ( FS_ITEM_OP_RENEW == iOp )
						{  //�ж������Ƿ����
							if ( (sSendProperty == iRechangeProperty &&
								iSendValue == iRechangeValue) )
							{
								vRechangeUserItemProChecked.push_back( TRUE );
							}
						}

						TRACE(" iSendPropertyIndex	--> [11866902]\n", iSendPropertyIndex );
TRACE(" iSendCategory		--> [11866902]\n", sSendCategory );
TRACE(" iSendProperty		--> [11866902]\n", sSendProperty );
TRACE(" iSendValue			--> [11866902]\n", iSendValue );
TRACE(" iSendOrder			--> [11866902]\n", iSendOrder );
		PacketComposer.Add(&iSendPropertyIndex);
						PacketComposer.Add((PBYTE)&sSendCategory, sizeof(short));
						PacketComposer.Add((PBYTE)&sSendProperty, sizeof(short));
						PacketComposer.Add(&iSendValue);
						PacketComposer.Add(&iSendOrder);
					}
				}		
			}
		}
	}
	else if(CHARACTER_SLOT_TYPE_NORMAL <= ItemInfo.iCharacterSlotType)
	{
		iSendPropertyKind  = 3;
		PacketComposer.Add(iSendPropertyKind);
			
		if(FS_ITEM_OP_BUY == iOp || FS_ITEM_OP_BUY_TODAYHOTDEAL == iOp || FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp || FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOp)
		{
			int iResult = 0;

			if(false == ItemInfo.IsLvCondition(iUserLv))
			{
				iResult = -2;
			}
				
			if ( GetTournamentChampionCnt() < ItemInfo.iChampionCondition)
			{
				iResult = BUY_ITEM_ERROR_NOT_ENOUGH_CHAMPIONCOUNT;
			}

			if (CHARACTER_SLOT_TYPE_SPECIAL == ItemInfo.iCharacterSlotType ||
				CHARACTER_SLOT_TYPE_CHANGE_AVATAR == ItemInfo.iCharacterSlotType)
			{
				PacketComposer.Initialize(S2C_BUY_SPECIAL_ITEM_RES);

				if(FS_ITEM_OP_BUY_TODAYHOTDEAL == iOp)
				{
					int iSaleDate = 0;
					SHotDealItem sInfo;

					if(FALSE == TODAYHOTDEALManager.GetTodayHotDealItem(iSecretStoreIndex, iSaleDate, sInfo) ||
						TODAYHOTDEALManager.GetTodayHotDealSeason(iSaleDate) != m_UserHotDealCount._btSeason)
					{
						iResult = BUY_ITEM_ERROR_TODAYHOTDEAL_TIMEOVER;
					}
					else if(m_UserHotDealCount._iTodayRemainCount[sInfo.btIdx] <= 0) 
					{
						iResult = BUY_ITEM_ERROR_TODAYHOTDEAL_LIMITOVER;
					}
					else
					{
						CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
						CHECK_NULL_POINTER_BOOL(pODBCBase);

						if( ODBC_RETURN_SUCCESS != pODBCBase->TODAYHOTDEAL_UpdateLimitCountStatus(GetUserIDIndex(), m_UserHotDealCount._btSeason, sInfo.btIdx, TODAYHOTDEAL_USER_STATUS_Ready))
						{
							WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "TODAYHOTDEAL_UpdateLimitCountStatus2 - UserIDIndex:11866902,  SaleDate:0,  Status  :18227200"
tUserIDIndex(), iSaleDate, TODAYHOTDEAL_USER_STATUS_Ready );
							return FALSE;
						}
						
						iResult = BUY_ITEM_ERROR_SUCCESS_TODAYHOTDEAL_CHARSLOT;
					}
				}
				else if(FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp)
				{
					iResult = BUY_ITEM_ERROR_SUCCESS_SALEITEM_EVENT_CHARSLOT;
				}
				else if(FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOp)
				{
					iResult = BUY_ITEM_ERROR_SUCCESS_SALEPLUS_EVENT_CHARSLOT;
				}
				
				PacketComposer.Add(iResult);
				PacketComposer.Add(iItemCode);
				PacketComposer.Add(iPropertyKind);

				short sGroupIndex = AVATARCREATEMANAGER.GetGroupIndexByPropertyKind(iPropertyKind);
				if (-1 == sGroupIndex &&
					(SPECIAL_CHARACTER_SLOT_KIND_NUM == iPropertyKind ||
					SPECIAL_CHARACTER_SLOT_KIND_NUM_EXPERT == iPropertyKind ||
					SPECIAL_CHARACTER_SLOT_KIND_NUM_EXPERT2 == iPropertyKind ||
					SPECIAL_CHARACTER_SLOT_KIND_NUM_EXPERT3 == iPropertyKind))
				{
					sGroupIndex = 1;
				}
				int iCreateLv = AVATARCREATEMANAGER.GetSlotCreateLevel(sGroupIndex, iPropertyKind);
				PacketComposer.Add(sGroupIndex);
				PacketComposer.Add(iCreateLv);

				return TRUE;
			}
			else
			{
				CHECK_CONDITION_RETURN(1 != iTypeNum, FALSE);
					
				if(iResult < 0)
				{
					PacketComposer.Initialize(S2C_BUY_GENERAL_SLOTITEM_RES);
					PacketComposer.Add(iResult);
					PacketComposer.Add(iItemCode);
					PacketComposer.Add(iPropertyKind);
					return TRUE;
				}
					
				if(ItemInfo.iPropertyKind == CHARACTER_SLOT_KIND_NUM || ItemInfo.iPropertyKind == CHARACTER_SLOT_POINT_KIND_NUM)	
					iResult = 0;		
				else if ( ItemInfo.iPropertyKind == EXTRA_CHARACTER_SLOT_KIND_NUM || ItemInfo.iPropertyKind == EXTRA_CHARACTER_SLOT_POINT_KIND_NUM )
					iResult = 1;

				PacketComposer.Initialize(S2C_BUY_GENERAL_SLOTITEM_RES);
				PacketComposer.Add(iResult);
				PacketComposer.Add(iItemCode);
				return TRUE;
			}					
		}
			
		BYTE btPropertyAssignTypeCount = 0;
		PacketComposer.Add(btPropertyAssignTypeCount);
	}
	else if( ITEM_PROPERTY_KIND_CLONE_CHARACTER_ITEM == iPropertyKind )
	{
		PacketComposer.Initialize(S2C_BUY_SPECIAL_ITEM_RES);

		int iTargetSpecialAvatarIndex = 0;
		if( TRUE == AVATARCREATEMANAGER.CheckCloneAvailableCharacter(GetCurUsedAvatar()->iSpecialCharacterIndex, iTargetSpecialAvatarIndex))
		{
			if( m_bCloneCharacter == TRUE ) 
			{
				PacketComposer.Add((int)BUY_ITEM_ERROR_CLONE_CHAR_ITEM_HAVE_ALREADY);
				return TRUE;
			}
			
			PacketComposer.Add((int)BUY_ITEM_ERROR_SUCCESS);
			PacketComposer.Add(iItemCode);
			PacketComposer.Add(iPropertyKind);

			short sGroupIndex = AVATARCREATEMANAGER.GetGroupIndexByPropertyKind(iPropertyKind);
			if (-1 == sGroupIndex &&
				(SPECIAL_CHARACTER_SLOT_KIND_NUM == iPropertyKind ||
				SPECIAL_CHARACTER_SLOT_KIND_NUM_EXPERT == iPropertyKind ||
				SPECIAL_CHARACTER_SLOT_KIND_NUM_EXPERT2 == iPropertyKind ||
				SPECIAL_CHARACTER_SLOT_KIND_NUM_EXPERT3 == iPropertyKind))
			{
				sGroupIndex = 1;
			}
			int iCreateLv = AVATARCREATEMANAGER.GetSlotCreateLevel(sGroupIndex, iPropertyKind);
			PacketComposer.Add(sGroupIndex);
			PacketComposer.Add(iCreateLv);

			return TRUE;
		}
		else
		{
			PacketComposer.Add((int)BUY_ITEM_ERROR_NOT_CLONE_CHAR);
			return TRUE;
		}
	}
	else if((iPropertyKind >= PROPENSITY_ITEM_KIND_START && iPropertyKind <= PROPENSITY_ITEM_KIND_END) || 
		(iPropertyKind >= MIN_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM && iPropertyKind <= MAX_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM) || 
		iPropertyKind == ITEM_PROPERTY_KIND_PACKAGE_ITEM ||
		iPropertyKind == ITEM_PROPERTY_KIND_PACKAGE_DARKMARKET)
	{
		PacketComposer.Add(iPropertyKind);
	}
	else if(iPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN && iPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
	{
		PacketComposer.Add(iPropertyKind);

		int iCash;
		time_t tExpireDate;
		CHECK_CONDITION_RETURN(FALSE == SHOPPINGFESTIVAL.GetSaleCouponConfig(iPropertyKind, iCash, tExpireDate), FALSE);

		PacketComposer.Add(iCash);
		PacketComposer.Add(tExpireDate);
	}
	else
	{
		PacketComposer.Add(iSendPropertyKind);
	}

	if ( FS_ITEM_OP_RENEW == iOp )
	{
		BOOL bCheckPro = vRechangeUserItemProChecked.size() >= vRechangeUserItemProperty.size();
		if ( FALSE == bCheckPro )
		{
			int iReturnValue = BUY_ITEM_ERROR_ERRORPROPERTY;
			int iItemIndex = -1;
			int iEndOp = -1;

			PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
			PacketComposer.Add(FS_ITEM_OP_RENEW);
			PacketComposer.Add(&iReturnValue);
			PacketComposer.Add(iItemIndex);
			PacketComposer.Add(iEndOp);
			return TRUE;
		}
	}
	
	if ( 0 == ItemInfo.iUseOption && FS_ITEM_OP_RENEW == iOp )
	{
		int iReturnValue = BUY_ITEM_ERROR_NOT_SELL;
		int iItemIndex = -1;
		int iEndOp = -1;

		PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
		PacketComposer.Add(FS_ITEM_OP_RENEW);
		PacketComposer.Add(&iReturnValue);
		PacketComposer.Add(iItemIndex);
		PacketComposer.Add(iEndOp);
		return TRUE;
	}
	else if ( 0 == ItemInfo.iUseOption && 
		FS_ITEM_OP_BUY_SECRETSTOREITEM != iOp &&
		FS_ITEM_OP_BUY_TODAYHOTDEAL != iOp &&
		FS_ITEM_OP_BUY_EVENT_SALEPLUS != iOp &&
		FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE != iOp &&
		FS_ITEM_OP_BUY_SALE_RANDOMITEM != iOp &&
		ItemInfo.iPropertyKind != ITEM_PROPERTY_KIND_PREMIUN_CHALLENGE_TICKET &&
		ItemInfo.iBigKind != ITEM_BIG_KIND_EVENT_GIVEAWAY_ITEM &&
		ItemInfo.iBigKind != ITEM_BIG_KIND_EVENT_GIVEAWAY_RANDOM_ITEM &&
		ItemInfo.iPropertyKind != ITEM_PROPERTY_KIND_RANDOMITEM_BOARD_TICKET &&
		(ITEM_PROPERTY_KIND_SALE_COUPON_MIN > ItemInfo.iPropertyKind || ITEM_PROPERTY_KIND_SALE_COUPON_MAX < ItemInfo.iPropertyKind) &&
		(MIN_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM > ItemInfo.iPropertyKind || MAX_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM < ItemInfo.iPropertyKind))
	{
		int iTotalResult = -4;
		int iEndOp = -1;
			
		PacketComposer.Initialize(S2C_ITEM_SELECT_RES);
		PacketComposer.Add(FS_ITEM_OP_BUY);
		PacketComposer.Add(&iTotalResult);
		PacketComposer.Add(iEndOp);

		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_ITEM_BUYING, this, "ShopItemSelect\tInvalid UseOption");
		return TRUE;
	} 

	return TRUE;
}

BOOL CFSGameUser::GetShopItemSelectInfo(CPacketComposer& PacketComposer, int iOp, int iItemCode, int iRenewItemIdx, int iRecvUserLv, int iSecretStoreIndex)
{
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo ItemInfo;
	if (FALSE == pItemShop->GetItem(iItemCode , ItemInfo))
	{
		return FALSE;
	}

	if (FALSE == MakeShopItemSelectInfo(PacketComposer, iOp, ItemInfo, iItemCode, iRenewItemIdx, iRecvUserLv, iSecretStoreIndex))
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUser::GetShopItemSelectInfo(CPacketComposer& PacketComposer, int iPropertyKind)
{
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo ItemInfo;
	if (FALSE == pItemShop->GetItemByPropertyKind(iPropertyKind , ItemInfo))
	{
		return FALSE;
	}

	if (FALSE == MakeShopItemSelectInfo(PacketComposer, FS_ITEM_OP_BUY, ItemInfo))
	{
		return FALSE;
	}

	return TRUE;
}

void CFSGameUser::GetMatchLoginUserInfo(SG2M_USER_INFO &info, BYTE btMatchType)
{
	info.iPCRoomKind = GetPCRoomKind();
	info.iUserType = GetUserType();
	memcpy(&info.AvatarInfo, GetCurUsedAvatar(), sizeof(SAvatarInfo));
	GetAvatarItemStatus(info.iAvatarAddStat);
	SetAvatarFameSkillStatus(info.iAvatarAddStat);
	GetUserCharacterCollection()->GetSentenceStatus(info.iAvatarAddStat);
	GetAvatarLinkItemStatus(info.iLinkItemAddStat);
	memcpy(&info.EqualizeStausInfo, GetEqualizeStatus(), sizeof(SAvatarStatus));
	GetBasicItemCode(info.iBasicItem);
	info.iProtectionRing = CheckAndGetSelfProtectionRingCount();
	CheckAndGetProtectHand(info.iProtectHandKind, info.iProtectHandNum);
	strcpy_s(info.szIPAddress, _countof(info.szIPAddress), GetIPAddress());
	memcpy(&info.JoinTeamConfig, GetJoinTeamConfig()->GetConfig(), sizeof(SJoinTeamConfig));
	info.bIsAutoShutdownUser = IsAutoShutdownUser();
	info.iShutdownTime = GetShutdownTime();
	GetUserCharacterCollection()->GetSentenceStatus(info.fSentenceShootSuccessRate, info.fSentenceBlockSuccessRate);
	GetSpecialPartsOptionProperty(info.fSpecialPartsShootSuccessRate, info.fSpecialPartsBlockSuccessRate);
	memcpy(&info.UserFactionInfo, &m_UserFactionInfo, sizeof(SUserFactionBase));
	info.bEnableLoadingPattern = GetUserAppraisal()->CheckLoadingPatternGoodPoint(btMatchType);
	info.iFriendInviteUserIDIndex = GetUserFriendInvite()->GetFriendUserIDIndex();
	GetUserPowerupCapsule()->GetPowerupCapsuleStatus(info.iAvatarAddStat);
	GetCheerLeaderStatus(info.iAvatarAddStat);
	GetLvIntervalBuffItemStatus(info.iAvatarAddStat);
	info.iPromiseEventSpecialEXPCount = GetPromiseSpecialEXP();
	GetAvatarSpecialPieceProperty(info.iAvatarAddStat);

	GetUserBasketBall()->GetBallPartSlotInfo(info.bBallUseOption, info.iaBallPartSlot);
	info.bIsLastGameBest = GetUserAppraisal()->GetLastGameBest();
	
	GetDragonTigerCloneInfo(info.AvatarInfo, info.iaDragonTigerCloneStat);
}

void CFSGameUser::GetBasicItemCode(int* iBasicItem)
{
	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();

	if(pAvatarItemList == NULL)
	{
		iBasicItem[0] = -1;
		iBasicItem[1] = -1;
		iBasicItem[2] = -1;
		iBasicItem[3] = -1;
	}
	else
	{
		SAvatarInfo* pAvatar = GetCurUsedAvatar();
		CHECK_NULL_POINTER_VOID(pAvatar);

		SUserItemInfo* pItem = NULL;

		pItem = pAvatarItemList->GetBasicItem(1, pAvatar->iSex, pAvatar->iSpecialCharacterIndex);
		if(pItem == NULL)
			iBasicItem[0] = -1;
		else
			iBasicItem[0] = pItem->iItemCode;

		pItem = pAvatarItemList->GetBasicItem(4, pAvatar->iSex, pAvatar->iSpecialCharacterIndex);
		if(pItem == NULL)
			iBasicItem[1] = -1;
		else
			iBasicItem[1] = pItem->iItemCode;

		pItem = pAvatarItemList->GetBasicItem(8, pAvatar->iSex, pAvatar->iSpecialCharacterIndex);
		if(pItem == NULL)
			iBasicItem[2] = -1;
		else
			iBasicItem[2] = pItem->iItemCode;

		pItem = pAvatarItemList->GetBasicItem(16, pAvatar->iSex, pAvatar->iSpecialCharacterIndex);
		if(pItem == NULL)
			iBasicItem[3] = -1;
		else
			iBasicItem[3] = pItem->iItemCode;
	}
}

void CFSGameUser::SetRoomListUserSet(int iMatchSvrID, SM2G_TEAM_LIST_USER_SET& info)
{
	m_RoomListUserSet._iMatchSvrID = iMatchSvrID;
	m_RoomListUserSet._btMatchTeamScale = info.btMatchTeamScale;
	m_RoomListUserSet._btListMode = info.btListMode;
	m_RoomListUserSet._iPage = info.iPage;
	memcpy(m_RoomListUserSet._iaShowTeamID, info.iaShowTeamID, sizeof(int)*DEFAULT_TEAMNUM_PER_PAGE);
	m_RoomListUserSet._iSendCount = info.iSendCount;
}

void CFSGameUser::SetRoomListUserSet(BYTE  btTeamScale, int iStartTeamID, int iSendCount)
{
	m_RoomListUserSet._btListMode = ROOM_LIST_CHALLENGING_TEAM;
	m_RoomListUserSet._btMatchTeamScale = btTeamScale;
	m_RoomListUserSet._iSendCount = iSendCount;
}

BOOL CFSGameUser::UpdateUserLvExp( bool bIsLvUp, int iLv, int iPlusExp, int *iaStat )
{
	CFSODBCBase* pFreeStyleODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_BOOL( pFreeStyleODBC );

	SAvatarInfo* pAvatar = GetCurUsedAvatar();

	if( NULL == pAvatar ) return FALSE;

	int iRequireLv, iBonusPoint;
	if( ODBC_RETURN_FAIL == pFreeStyleODBC->spFSUpdateLvExp( GetUserIDIndex(), pAvatar->iGameIDIndex, iLv, pAvatar->byMaxLevelStep,iPlusExp, pAvatar->iFameLevel, 0, 0, pAvatar->iDisconnected, iRequireLv, iBonusPoint )  )
	{
		return FALSE;
	}

	m_BonusPointLevelUp.SetTerms(iRequireLv,iBonusPoint);

	UpdateAvatarLv(iLv);
	pAvatar->iLv = iLv;
	pAvatar->iExp += iPlusExp;

	if( !bIsLvUp ) return TRUE;

	if( ODBC_RETURN_FAIL == pFreeStyleODBC->spFSUpdateStat( pAvatar->iGameIDIndex, iaStat ) )
	{
		return FALSE;
	}

	pAvatar->Status.iStatRunBase  += iaStat[0];
	pAvatar->Status.iStatJumpBase += iaStat[1];
	pAvatar->Status.iStatStrBase += iaStat[2];
	pAvatar->Status.iStatPassBase += iaStat[3];
	pAvatar->Status.iStatDribbleBase += iaStat[4];
	pAvatar->Status.iStatReboundBase += iaStat[5];
	pAvatar->Status.iStatBlockBase += iaStat[6];
	pAvatar->Status.iStatStealBase += iaStat[7];
	pAvatar->Status.iStat2PointBase += iaStat[8];
	pAvatar->Status.iStat3PointBase += iaStat[9];
	pAvatar->Status.iStatDriveBase += iaStat[10];
	pAvatar->Status.iStatCloseBase += iaStat[11];

	pAvatar->Status.iStatRun  += iaStat[0];
	pAvatar->Status.iStatJump += iaStat[1];
	pAvatar->Status.iStatStr += iaStat[2];
	pAvatar->Status.iStatPass += iaStat[3];
	pAvatar->Status.iStatDribble += iaStat[4];
	pAvatar->Status.iStatRebound += iaStat[5];
	pAvatar->Status.iStatBlock += iaStat[6];
	pAvatar->Status.iStatSteal += iaStat[7];
	pAvatar->Status.iStat2Point += iaStat[8];
	pAvatar->Status.iStat3Point += iaStat[9];
	pAvatar->Status.iStatDrive += iaStat[10];
	pAvatar->Status.iStatClose += iaStat[11];

	return TRUE;
}

void CFSGameUser::UpdateAvatarLv(int iUpdatedLv)
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CHECK_CONDITION_RETURN_VOID(pAvatar->iLv == iUpdatedLv);

	pAvatar->iLv = iUpdatedLv;

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*) GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pCenter);

	SG2S_UPDATE_USER_INFO_NOT info;
	strncpy_s(info.GameID, MAX_GAMEID_LENGTH+1, pAvatar->szGameID, MAX_GAMEID_LENGTH);
	info.btUpdateType = CU_UPDATE_LV;
	info.iUpdateInfo = iUpdatedLv;
	pCenter->SendPacket(G2S_UPDATE_USER_INFO_NOT, &info, sizeof(SG2S_UPDATE_USER_INFO_NOT));

	CheckPositionSelect();
	SendRecordBoardStatus();
}

void CFSGameUser::SetOptionEnable(BYTE bOptionType, BOOL bSet)
{
	CUser::SetOptionEnable(bOptionType, bSet);

	if (OPTION_TYPE_INVITE == bOptionType)
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if (NULL != pCenter)
		{
			SG2S_UPDATE_USER_INFO_NOT info;
			strncpy_s(info.GameID, MAX_GAMEID_LENGTH+1, GetGameID(), MAX_GAMEID_LENGTH);
			info.btUpdateType = CU_UPDATE_INVITE_OPTION;
			info.iUpdateInfo = bSet;
			pCenter->SendPacket(G2S_UPDATE_USER_INFO_NOT, &info, sizeof(SG2S_UPDATE_USER_INFO_NOT));
		}
	}
}

void CFSGameUser::ExitChatChannel()
{
	if( NULL != m_pLobbyChatChannel )
		m_pLobbyChatChannel->Remove(this, CHAT_CHANNEL);

	ExitChatFactionChannel();
}

void CFSGameUser::ExitChatFactionChannel()
{
	if( NULL != m_pLobbyChatFactionChannel )
		m_pLobbyChatFactionChannel->Remove(this, CHAT_FACTION_CHANNEL);
}

void CFSGameUser::GetUserBufferItemInfo( SBufferItem pBufferItem[BUFFER_ITEM_NUM] )
{
	CAvatarItemList *pItemList = m_UserItem->GetCurAvatarItemList();
	if( NULL == pItemList ) return ;
	
	for( int iPropertyKind = BUFF_ITEM_KIND_EXP; iPropertyKind <= BUFF_ITEM_KIND_STATUS; iPropertyKind ++ )
	{
		int iCurrentKindNum = iPropertyKind - BUFF_ITEM_KIND_EXP;
		SUserItemInfo* pBuffItem = pItemList->GetItemWithPropertyKind(iPropertyKind);
		if(NULL != pBuffItem && pBuffItem->iStatus != ITEM_STATUS_EXPIRED && pBuffItem->iStatus != ITEM_STATUS_PRESENTBOX )
		{
			pBufferItem[iCurrentKindNum].iPropertyKind = pBuffItem->iPropertyKind;
			pBufferItem[iCurrentKindNum].iPropertyNum = pBuffItem->iPropertyNum;
			vector< SUserItemProperty > vUserItemProperty;
			pItemList->GetItemProperty(pBuffItem->iItemIdx, vUserItemProperty);
			for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size() && iPropertyCount < MAX_BUFFER_ITEM_PROPERTY; iPropertyCount++)
			{
				pBufferItem[iCurrentKindNum].iProperty[iPropertyCount]	= vUserItemProperty[iPropertyCount].iProperty;
				pBufferItem[iCurrentKindNum].iValue[iPropertyCount]		= vUserItemProperty[iPropertyCount].iValue;
			}
		}
	}
}

void CFSGameUser::UpdateRand_Rc4Key(int iHackType)
{
	if( HACK_TYPE_CHANGE_CHECKSUM == iHackType )
	{
		memset(&m_szCheckSum_Rc4Key, 0x00, sizeof(m_szCheckSum_Rc4Key));

		for( int i = 0 ; i < RC4_KEY_LENGTH ; ++i )
		{
			m_szCheckSum_Rc4Key[i] = (rand() ) + 49; // null ���� �߰��� �������ƺ��ϴ�
�
		}		
	}
	else if( HACK_TYPE_SOME_ACTION_CHANGE_STATUS == iHackType )
	{
		memset(&m_sz_AnyOperation_Rc4Key, 0x00, sizeof(m_sz_AnyOperation_Rc4Key));

		for( int i = 0 ; i < RC4_KEY_LENGTH ; ++i )
		{
			m_sz_AnyOperation_Rc4Key[i] = (rand() ) + 49;
;
		}		
	}
	else
	{
		memset(&m_szRc4Key, 0x00, sizeof(m_szRc4Key));

		for( int i = 0 ; i < RC4_KEY_LENGTH ; ++i )
		{
			m_szRc4Key[i] = (rand() ) + 49; // null ���� �߰��� �������ƺ��ϴ�
�
		}
	}
}

bool CFSGameUser::opSortSpecialty( SAvatarSpecialtySkill obj, SAvatarSpecialtySkill obj2 )
{
	return obj.iSkillNo < obj2.iSkillNo;
}

void CFSGameUser::SendHighFrequencyItemCount(int nType, BOOL bAcceptPresent)
{
	CUserHighFrequencyItem* pHighFrequencyItem = GetUserHighFrequencyItem();
	CHECK_NULL_POINTER_VOID(pHighFrequencyItem);

	SS2C_HIGH_FREQUENCYITEM_COUNT info;

	if((nType == ITEM_PROPERTY_KIND_GOLD_BAGS	||
		nType == ITEM_PROPERTY_KIND_RED_BAGS	||
		nType == ITEM_PROPERTY_KIND_GREEN_BAGS ||
		nType == ITEM_PROPERTY_KIND_BLUE_BAGS) && 
		bAcceptPresent == FALSE)
	{
		info.iType = ITEM_PROPERTY_KIND_GOLD_BAGS + ITEM_PROPERTY_KIND_RED_BAGS + ITEM_PROPERTY_KIND_GREEN_BAGS + ITEM_PROPERTY_KIND_BLUE_BAGS;
		info.iCount = (pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GOLD_BAGS ) +
			pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_RED_BAGS ) +
			pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GREEN_BAGS ) +
			pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_BLUE_BAGS ));
	}
	else
	{
		info.iType = nType;
		info.iCount = pHighFrequencyItem->GetUserHighFrequencyItemCount( nType );
	}

	Send(S2C_BINGO_TATOO_COUNT_NOT, &info, sizeof(SS2C_HIGH_FREQUENCYITEM_COUNT));
}

void CFSGameUser::SendRecordBoardStatus()
{
	CPacketComposer Packet(S2C_RECORDBOARD_STATUS_NOTICE);
	m_UserRecordBoard.MakePacketForStatusNotice(Packet);
	Send(&Packet);
}

BOOL CFSGameUser::BuyRecordBoardPage_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CLUB, INVALED_DATA, CHECK_FAIL, "BuyRecordBoardPage_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iPostMoney = pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;

	SS2C_RECORDBOARD_ADD_CASH_PAGE_RES rs;
	rs.btResult = RESULT_RECORDBOARD_ADD_CASH_PAGE_SUCCESS;
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			CFSRankODBC* pODBC = (CFSRankODBC*) ODBCManager.GetODBC(ODBC_RANK);
			CHECK_NULL_POINTER_BOOL(pODBC);

			SDate sLastOnePageCompletionDate;
			if(ODBC_RETURN_SUCCESS == pODBC->RECORDBOARD_BuyUserRecordBoardPage(GetGameIDIndex(), iPrevMoney, iPostMoney, sLastOnePageCompletionDate))
			{
				SetUserBillResultAtMem(pBillingInfo->iSellType, pBillingInfo->iCurrentCash-pBillingInfo->iCashChange, 0, 0, pBillingInfo->iBonusCoin);
				SendUserGold();

				GetUserRecordBoard()->AddPage();
				GetUserRecordBoard()->SetLastOnePageCompletionDate(sLastOnePageCompletionDate);

				rs.iChangedPage = GetUserRecordBoard()->GetProgressedPage();

				GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_RECORDBOARD_PAGE_CLEAR);
			}
		}
		break;
	default:
		{
			rs.btResult = RESULT_RECORDBOARD_ADD_CASH_PAGE_ENOUGH_CASH;
		}
		break;
	}

	CPacketComposer	Packet(S2C_RECORDBOARD_ADD_CASH_PAGE_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RECORDBOARD_ADD_CASH_PAGE_RES));
	Send(&Packet);
	
	SendRecordBoardStatus();

	return (RESULT_RECORDBOARD_ADD_CASH_PAGE_SUCCESS == rs.btResult);	
}

BOOL CFSGameUser::BuyHighFrequencyItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}
	CFSGameClient* pClient = (CFSGameClient*)GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC);

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	int iGameIDIndex = GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPropertyKind = pBillingInfo->iParam0;
	int iPropertyValue = pBillingInfo->iParam1;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iItemPriceCash = pBillingInfo->iCashChange;	
	int iItemPriceClubCoin = pBillingInfo->iClubCoinChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	int iOperationCode = pBillingInfo->iOperationCode;

	int iItemPrice = iItemPricePoint;
	if(iBuyPriceType == SELL_TYPE_TROPHY)
		iItemPrice = iItemPriceTrophy;
	else if(iBuyPriceType == SELL_TYPE_CLUBCOIN)
		iItemPrice = iItemPriceClubCoin;
	else if(iBuyPriceType == SELL_TYPE_CASH)
		iItemPrice = iItemPriceCash;

	// ������ ���̴� ������
	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;

	if( bIsSendItem == TRUE )
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
	}
	else
	{
		iPacketOperationCode = FS_ITEM_OP_BUY;
	}

	if(TRUE == pBillingInfo->bSaleRandomItemEvent)
	{
		iPacketOperationCode = FS_ITEM_OP_BUY_SALE_RANDOMITEM;
	}

	int iUpdatedPropertyValue = 0;
	SShopItemInfo ShopItemInfo;			
	/////////////////////////////////////// ���� ���� �κ� ///////////////////////////////////////
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			pItemShop->GetItem(iItemIndex, ShopItemInfo);	

			if( bIsSendItem == FALSE )
			{
				int iUpdatedClubCoin = 0;
				if ( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyHighFrequencyItem( iUserIDIndex, iGameIDIndex, iItemIndex, iPropertyKind, iPropertyValue, iBuyPriceType, iItemPrice, iPrevMoney, szBillingKey, iUpdatedPropertyValue, pBillingInfo->iItemPrice, GetClubSN(), iUpdatedClubCoin ))
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS; 
					CUserHighFrequencyItem* pHighFrequencyItem = GetUserHighFrequencyItem();
					if ( pHighFrequencyItem != NULL )
					{
						SUserHighFrequencyItem sUserHighFrequencyItem;
						sUserHighFrequencyItem.iPropertyKind = iPropertyKind;
						sUserHighFrequencyItem.iPropertyTypeValue = iUpdatedPropertyValue;
						pHighFrequencyItem->AddUserHighFrequencyItem( sUserHighFrequencyItem );
						SendHighFrequencyItemCount(iPropertyKind);
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_RANDOMITEM_BOARD_TICKET)
					{
						GetUserRandomItemBoardEvent()->SendRandomItemBoardEventInfo();
					}

					if(iPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
						iPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
					{
						GetUserShoppingFestivalEvent()->UpdateSaleCoupon(iPropertyKind, iUpdatedPropertyValue);
						GetUserShoppingFestivalEvent()->SendEventOpenStatus();
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_RANDOMITEM_DRAW_COIN)
					{
						SS2C_EVENT_RANDOMITEM_DRAW_UPDATE_COIN_NOT not;
						not.iUpdatedCoin = iUpdatedPropertyValue;
						Send(S2C_EVENT_RANDOMITEM_DRAW_UPDATE_COIN_NOT, &not, sizeof(SS2C_EVENT_RANDOMITEM_DRAW_UPDATE_COIN_NOT));
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_EVENT_SELECT_CLOTHES)
					{
						SS2C_EVENT_SELECT_CLOTHES_UPDATE_COIN_NOT not;
						not.iUpdateCoin = iUpdatedPropertyValue;
						Send(S2C_EVENT_SELECT_CLOTHES_UPDATE_COIN_NOT, &not, sizeof(SS2C_EVENT_SELECT_CLOTHES_UPDATE_COIN_NOT));
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_GOLD_POTION)
					{
						GetUserPotionMakingEvent()->UpdatePotionMakeCount(SEVENT_POTION_TYPE_GOLD, iUpdatedPropertyValue);
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_PRIMONBALL)
					{
						if(U_LOBBY == GetLocation())
							GetUserPrimonGoEvent()->SendEventInfo();
					}

					if(SELL_TYPE_CLUBCOIN == ShopItemInfo.iSellType &&
						GetClubSN() > 0)
					{
						SetUserClubCoin(iUpdatedClubCoin);
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY)
					{
						GetUserSummerCandyEvent()->BuyCandy(iPropertyValue);
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_EVENT_PREMIUM_PASS_TICKET)
					{
						GetUserPremiumPassEvent()->SendEventInfo();
						GetUserPremiumPassEvent()->SendGradeRewardInfo();
					}

					if(iPropertyKind == ITEM_PROPERTY_KIND_EVENT_SECRET_SHOP_SLOT)
					{
						GetUserSecretShopEvent()->UpdateSlotStatus();
						GetUserSecretShopEvent()->SendShopList();
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyHighFrequencyItem - ItemCode:11866902, UserID:(null), GameID:", iItemIndex, szUserID, szGameID);
		}
			else
			{
				int iPresentIdx = 0;
				if ( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_SendHighFrequencyItem( iUserIDIndex, iGameIDIndex,GetGameID(), szRecvGameID,iItemIndex, iPropertyKind, iPropertyValue, iBuyPriceType, iItemPrice, iPrevMoney, szBillingKey, pBillingInfo->szTitle, pBillingInfo->szText, pBillingInfo->iItemPrice, iPresentIdx ))
				{
					CFSGameServer* pServer = CFSGameServer::GetInstance();
					if( pServer != NULL )
					{
						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
						if( pCenter != NULL ) 
						{
							pCenter->SendUserNotify( szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR );				
						}
					}				
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_SendHighFrequencyItem - ItemCode:11866902, UserID:(null), GameID:", iItemIndex, szUserID, szGameID);

		}
			if (BUY_ITEM_ERROR_SUCCESS == iErrorCode)
			{
				int iPreSkillPoint = GetSkillPoint();
				int iPreEventCoin = GetEventCoin();

				SetPayCash(pBillingInfo->iCashChange);
				if(pBillingInfo->bUseEventCoin == TRUE)
					SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

				CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

				iEventBonusPoint = GetSkillPoint() - iPreSkillPoint;
				iEventBonusCash = GetEventCoin() - iPreEventCoin;
				if(iEventBonusPoint > 0 || iEventBonusCash > 0)
					iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

				SetUserBillResultAtMem( iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
			}
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		SendAllMyOwnItemInfo();
		SendUserGold();
		SendCurAvatarTrophy();

		if(ITEM_CATEGORY_CLUB == ShopItemInfo.iCategory)
			SendClubShopUserCashInfo();

		if(OPEN == RANDOMITEMGROUPEVENT.IsOpen() &&
			bIsSendItem == FALSE &&
			(iPropertyKind == ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_NORMAL_COIN ||
			iPropertyKind == ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_PREMIUM_COIN) &&
			pClient->GetState() == NEXUS_LOBBY)
		{
			SS2C_RANDOMITEM_GROUP_EVENT_BUY_COIN_NOT not;
			not.iCoinItemCode = iItemIndex;
			not.iCoinCount = iUpdatedPropertyValue;
			Send(S2C_RANDOMITEM_GROUP_EVENT_BUY_COIN_NOT, &not, sizeof(SS2C_RANDOMITEM_GROUP_EVENT_BUY_COIN_NOT));
		}
	}

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );	
}


BOOL CFSGameUser::CheckCapital(BYTE bySellType, int iPrice, int& iErrorCode)
{
	BOOL bResult = TRUE;
	switch(bySellType)
	{
	case SELL_TYPE_TROPHY:
		if( iPrice > GetUserTrophy())
		{
			bResult = FALSE;
		}
		break;
	default:
		break;
	}

	if(FALSE == bResult )
	{
		iErrorCode = ERROR_CODE_NOT_ENOUGH_CAPITAL;
	}

	return bResult;
}

void CFSGameUser::SubtractCapital(BYTE bySellType, int iPrice)
{
	BOOL bResult = TRUE;
	switch(bySellType)
	{
	case SELL_TYPE_TROPHY:
		SubtractTrophy(iPrice);
		break;
	default:
		break;
	}
}

void CFSGameUser::AddProductToInventory(SProductData* pProductData, BOOL bNewStatus/* = TRUE*/)
{
	CHECK_NULL_POINTER_VOID(pProductData);

	pProductData->bIsNew = bNewStatus;

	m_ProductInventory.AddProduct(pProductData);
}

void CFSGameUser::AddProductToInventory(SProductData& ProductData, BOOL bNewStatus/* = TRUE*/)
{
	ProductData.bIsNew = bNewStatus;

	m_ProductInventory.AddProduct(ProductData);
}

void CFSGameUser::RemoveProductToInventory(int iTendencyType, int iProductIndex, int iProductCount/* = 0*/)
{
	m_ProductInventory.RemoveProduct(iTendencyType,iProductIndex,iProductCount);
}

void CFSGameUser::SendCoachCardShopInventoryListRes( int iRequestPage, BYTE byInventoryRange,int iRequestTendencyType,int iSortPropertyType,int iSortOrder,int iRequestType )
{
	CPacketComposer PacketComposer(S2C_COACH_CARD_SHOP_INVENTORY_LIST_RES);
	m_ProductInventory.MakeCoachCardShopInventoryListResData(CFSGameServer::GetInstance()->GetItemShop(), PacketComposer, iRequestPage, byInventoryRange, iRequestTendencyType,iSortPropertyType,iSortOrder,iRequestType,
										GetClient()->GetState(), GetCurUsedAvtarLv());
	Send(&PacketComposer);
}

void CFSGameUser::SendCoachCardRewardNot(int iStateType , int iReceiveProductIndex, int iUseProductIndex, int iUpdatedProductCount)
{
	CPacketComposer PacketComposer(S2C_COACH_CARD_REWARD_NOT);
	PacketComposer.Add(iStateType);
	PacketComposer.Add((int)-1);
	PacketComposer.Add(iReceiveProductIndex);
	PacketComposer.Add(iUseProductIndex);
	PacketComposer.Add(iUpdatedProductCount);
	Send(&PacketComposer);
}

BOOL CFSGameUser::IsThrereProduct(int iProductnIndex)
{
	return m_ProductInventory.IsThereProduct(iProductnIndex);
}

void CFSGameUser::GetOwnedProductItemInfo(int iItemIDNumber, int& iProductIndex, int& iInventoryIndex)
{
	m_ProductInventory.GetOwnedProductItemInfo(iItemIDNumber, iProductIndex, iInventoryIndex);
}

BOOL CFSGameUser::LoadProductInventoryList()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC)

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	int iGameIDIndex = GetGameIDIndex();
	vector<SProductData*> vecProductData;
	if( ODBC_RETURN_FAIL == pBaseODBC->AVATAR_GetItemInventoryList(iGameIDIndex, vecProductData))
	{
		g_LogManager.WriteLogToFile("Fail AVATAR_GetItemInventoryList: 11866902 " , iGameIDIndex);
rn FALSE;
	}

	SProductData* pProductData = NULL;
	for(int i =0 ; i < vecProductData.size(); i++)
	{
		pProductData = vecProductData[i];
		if( NULL != pProductData)
		{
			AddProductToInventory(pProductData, FALSE);
		}
	}

	LoadActionInfluence();
	LoadPotentialComponent();

	if (ODBC_RETURN_SUCCESS != pBaseODBC->POTEN_GetAvatarPausedUpgradeInfo(iGameIDIndex, m_PausedPotenUpgradeInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_COACHCARD, DB_DATA_LOAD, FAIL, "POTEN_GetAvatarPausedUpgradeInfo GameIDIndex:11866902",iGameIDIndex);
rn FALSE;
	}

	return TRUE;
}

void CFSGameUser::SubBuyCost(BYTE bySellType, int iPrice)
{
	switch(bySellType)
	{
	case SELL_TYPE_CASH:
		{
			SetCoin(GetCoin()-iPrice);
		}
		break;
	case SELL_TYPE_POINT:
		{
			SetSkillPoint(GetSkillPoint()-iPrice);
		}
		break;
	case SELL_TYPE_TROPHY:
		{
			SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
			CHECK_NULL_POINTER_VOID(pAvatarInfo);

			pAvatarInfo->nTrophyCount -= iPrice;
		}
		break;
	default:
		break;
	}
}

BOOL CFSGameUser::LoadActionInfluence()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC)

	vector<SItemActionInfluence> vecMainActionInfluence;
	vector<SItemActionInfluence> vecSubActionInfluence;
	if(ODBC_RETURN_FAIL == ((CFSODBCBase *)pBaseODBC)->ITEM_GetActionInfluenceList(GetGameIDIndex(), vecMainActionInfluence, vecSubActionInfluence))
	{
		g_LogManager.WriteLogToFile("Load Fail ITEM_GetActionInfluenceList");
		return FALSE;
	}
	
	if(vecMainActionInfluence.size() > 0)
	{
		for(int i = 0 ; i < vecMainActionInfluence.size(); i++)
		{
			AddProductMainActionInfluence(vecMainActionInfluence[i]);
		}
	}

	if(vecSubActionInfluence.size() > 0)
	{
		for(int k = 0 ; k < vecSubActionInfluence.size(); k++)
		{
			AddProductSubActionInfluence(vecSubActionInfluence[k].iProductIndex, vecSubActionInfluence[k].iActionInfluenceIndex);
		}
	}
	
	return TRUE;
}

BOOL CFSGameUser::LoadPotentialComponent()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC)

	vector<SPotentialComponentList> vecPotentialListInfo;
	if( ODBC_RETURN_FAIL == ((CFSODBCBase *)pBaseODBC)->POTENTIAL_GetComponentList(GetGameIDIndex(), vecPotentialListInfo) )
	{
		g_LogManager.WriteLogToFile("Load Fail ITEM_GetActionInfluenceList");
		return FALSE;
	}

	int iPotentialInfoSize = vecPotentialListInfo.size();
	if( 0 < iPotentialInfoSize )
	{
		for( int i = 0; i < iPotentialInfoSize; ++i )
		{
			SetPotentialComponent( vecPotentialListInfo[i].iProductIndex, vecPotentialListInfo[i].sPotentialInfo );		
		}
	}

	return TRUE;
}

void CFSGameUser::AddProductMainActionInfluence(SItemActionInfluence& sItemActionInfluence)
{
	m_ProductInventory.SetMainActionInfluence(sItemActionInfluence.iProductIndex , sItemActionInfluence.iActionInfluenceIndex);
}

BOOL CFSGameUser::LoadItemSlotDataList()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC)

	BOOL bResult = TRUE;

	int iGameIDIndex = GetGameIDIndex();

	vector<SSlotData*> vecSlotData;
	if( ODBC_RETURN_FAIL == pBaseODBC->AVATAR_GetItemSlotPackageList(iGameIDIndex, vecSlotData))
	{
		g_LogManager.WriteLogToFile("Fail AVATAR_GetItemSlotPackageList: 11866902 " , iGameIDIndex);
ult = FALSE;
	}

	SSlotData* pSlotData = NULL;
	for(int i =0; i < vecSlotData.size(); i++)
	{
		SSlotData* pSlotData = vecSlotData[i];
		if(NULL != pSlotData)
		{
			if(pSlotData->iTendencyType != TENDENCY_TYPE_NONE && pSlotData->iProductIndex != INVALID_IDINDEX)
			{
				CProduct* pProduct = GetInventoryProduct(pSlotData->iProductIndex);
				if (NULL != pProduct)
				{
					m_SlotPackageList.AddItemSlotData(pSlotData);
					pProduct->SetEquipStatus(TRUE);
				}
			}
		}
	}

	return bResult;
}

BOOL CFSGameUser::SetSlotPackageInDB(int iTendencyType, int iProductIndex)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC)

	BOOL bRestult = TRUE;
	if( ODBC_RETURN_FAIL == pGameODBC->AVATAR_SetSlotPackage(GetGameIDIndex(), iTendencyType, iProductIndex))
	{
		g_LogManager.WriteLogToFile("Fail AVATAR_SetSlotPackage");
		bRestult = FALSE;
	}

	return bRestult;
}

BOOL CFSGameUser::EquipProduct(SSlotData& sSlotData, int& iErrorCode)
{	
	CProduct* pProduct = m_ProductInventory.GetProduct(sSlotData.iProductIndex);
	if (NULL == pProduct)
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	if (0 == pProduct->GetRemainTerm()/60)
	{
		iErrorCode = ERROR_CODE_EXPIRE_TIME;
		return FALSE;
	}

	int iUnEquipProductIndex = m_SlotPackageList.GetSlotProductIndex(sSlotData.iTendencyType);
	if( FALSE == PotentialProductReSet(iUnEquipProductIndex) )
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	if(	FALSE == m_SlotPackageList.SetSlotData(sSlotData.iTendencyType, INVALID_INDEX))
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	if( FALSE == PotentialProductSetting(sSlotData.iProductIndex) )
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	if( FALSE == SetSlotPackageInDB(sSlotData.iTendencyType, sSlotData.iProductIndex))
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}
	
	if(	FALSE == m_SlotPackageList.SetSlotData(sSlotData.iTendencyType, sSlotData.iProductIndex))
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	pProduct->SetEquipStatus(TRUE);
	CProduct* pUnEquipProduct = m_ProductInventory.GetProduct(iUnEquipProductIndex);
	if (NULL != pUnEquipProduct)
	{
		pUnEquipProduct->SetEquipStatus(FALSE);
	}

	return TRUE;
}

BOOL CFSGameUser::UnEquipProduct(int iTendencyType, int& iErrorCode)
{
	int iProductIndex = m_SlotPackageList.GetSlotProductIndex( iTendencyType );
	if( 0 > iProductIndex )
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	if( FALSE == PotentialProductReSet(iProductIndex) )
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	if( FALSE == SetSlotPackageInDB(iTendencyType, INVALID_IDINDEX))
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	if(	FALSE == m_SlotPackageList.SetSlotData(iTendencyType, INVALID_IDINDEX))
	{
		iErrorCode = ERROR_CODE_SYSTEM;
		return FALSE;
	}

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
	if (NULL != pMatch)
	{
		SG2M_UNEQUIP_PRODUCT_NOT req;
		req.iGameIDIndex = GetGameIDIndex();
		req.btServerIndex = _GetServerIndex;
		req.SlotData.iProductIndex = iProductIndex;
		req.SlotData.iTendencyType = iTendencyType;

		pMatch->SendPacket(G2M_UNEQUIP_PRODUCT_NOT, &req, sizeof(SG2M_UNEQUIP_PRODUCT_NOT));
	}

	CProduct* pProduct = m_ProductInventory.GetProduct(iProductIndex);
	if (NULL != pProduct)
	{
		pProduct->SetEquipStatus(FALSE);
	}
	return TRUE;
}

int CFSGameUser::GetEquipProductIndex(int iTendencyType)
{
	return m_SlotPackageList.GetSlotProductIndex( iTendencyType );
}

void CFSGameUser::SendEquipCoachCardRes(BOOL bResult,int iErrorCode,int iProductIndex)
{
	CPacketComposer PacketComposer(S2C_EQUIP_COACH_CARD_RES);
	PacketComposer.Add(bResult);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iProductIndex);
	Send(&PacketComposer);
}

void CFSGameUser::SendUnEquipCoachCardRes(BOOL bResult,int iErrorCode,int iProductIndex)
{
	CPacketComposer PacketComposer(S2C_UNEQUIP_COACH_CARD_RES);
	PacketComposer.Add(bResult);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iProductIndex);
	Send(&PacketComposer);
}

BOOL CFSGameUser::UnregisterItemCombine( int iProductIndex )
{
	CombineDataVec::iterator itrCurrentPosition = m_vecCombineData.begin();
	CombineDataVec::iterator itrEndPosition = m_vecCombineData.end();

	for (;itrCurrentPosition != itrEndPosition;itrCurrentPosition++)
	{
		if (itrCurrentPosition->iProductIndex == iProductIndex)
		{
			m_vecCombineData.erase(itrCurrentPosition);
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CFSGameUser::RegisterItemCombine(const SCombineData& AddCombineData)
{
	if(m_vecCombineData.size() < 0 || m_vecCombineData.size() > MAX_COMBINE_COUNT)
	{
		return FALSE;
	}

	if( ITEM_TYPE_COACH_CARD != m_ProductInventory.GetItemType(AddCombineData.iProductIndex) )
		return FALSE;

	for (int i = 0 ; i < m_vecCombineData.size();i++)
	{
		SCombineData& CombineData = m_vecCombineData[i];
		
		if (CombineData.iProductIndex == AddCombineData.iProductIndex)
		{
			return FALSE;
		}
	}


	m_vecCombineData.push_back(AddCombineData);
	return TRUE;
}

void CFSGameUser::ClearItemCombineList()
{
	m_vecCombineData.clear();
}

BOOL CFSGameUser::GetCombineLevel(int iIndex, int& iGradeLevel)
{
	if (m_vecCombineData.size() < MIN_COMBINE_COUNT || m_vecCombineData.size() > MAX_COMBINE_COUNT)
	{
		return FALSE;
	}

	if( 0 > iIndex && m_vecCombineData.size() <= iIndex )
		return FALSE;

	iGradeLevel = m_vecCombineData[iIndex].iGradeLevel;
	return TRUE;	
}

BOOL CFSGameUser::GetCombineInventoryIndex(CombineDataVec& vecCombineData)
{
	if (m_vecCombineData.size() < MIN_COMBINE_COUNT || m_vecCombineData.size() > MAX_COMBINE_COUNT)
	{
		return FALSE;
	}

	vecCombineData = m_vecCombineData;

	return TRUE;
}

CProduct* CFSGameUser::GetInventoryProduct(int iProductIndex)
{
	return m_ProductInventory.GetProduct(iProductIndex);
}

int CFSGameUser::GetProductInventoryCount()
{
	return m_ProductInventory.GetProductInventoryCount();
}

int CFSGameUser::GetInventoryGradeLevel(int iUseProductIndex )
{
	return m_ProductInventory.GetGradeLevel(iUseProductIndex);
}

BOOL CFSGameUser::GetPotentialComponent(int iProductIndex, SPotentialComponent& sPotentialComponent)
{
	return m_ProductInventory.GetPotentialComponent(iProductIndex, sPotentialComponent);
}

void CFSGameUser::GetInventorySubActionInfluence( int iUseProductIndex, vector<int>& vecSubActionInfluence )
{
	m_ProductInventory.GetSubActionInfluence( iUseProductIndex, vecSubActionInfluence );
}

BOOL CFSGameUser::GetInventoryIndex(int iProductIndex, int& iInventoryIndex)
{
	return m_ProductInventory.GetInventoryIndex(iProductIndex, iInventoryIndex);
}

int	CFSGameUser::GetInventoryTendencyType(int iUseProductIndex)
{
	return m_ProductInventory.GetTendencyType(iUseProductIndex);
}

BOOL CFSGameUser::GetItemIDNumber(int iProductIndex)
{
	return m_ProductInventory.GetItemIDNumber(iProductIndex);
}

BOOL CFSGameUser::IsPotentialComponent(int iProductIndex)
{
	return m_ProductInventory.IsPotentialComponent(iProductIndex);
}

void CFSGameUser::SendCoachCardSlotPackageListRes()
{
	CPacketComposer PacketComposer(S2C_COACH_CARD_SLOT_PACKAGE_LIST_RES);

	MakeEquipSlotPackageListRes(PacketComposer);

	Send(&PacketComposer);
}

BOOL CFSGameUser::BuyCoachCardTermExtend( int iProductIndex, int iTermTime,int iSellType,int iPrice, int& iRemainTime )
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC);
	SDateInfo sDateInfo;
	if (ODBC_RETURN_SUCCESS == pGameODBC->ITEM_ExtendTerm(GetUserIDIndex(),GetGameIDIndex(),iProductIndex,iPrice,iTermTime,sDateInfo))
	{
		if (iSellType == 2)
		{
			SubBuyCost(iSellType,iPrice);
		}
		SendUserGold();
		m_ProductInventory.UpdateProductExpireDate(iProductIndex , sDateInfo,iRemainTime);
		return TRUE;
	}
	return FALSE;

}

void CFSGameUser::UpgradePotenCard(SC2S_UPGRADE_POTENTIAL_CARD_REQ& req, SS2C_UPGRADE_POTENTIAL_CARD_RES& res)
{
	CProduct* pTargetProduct = GetInventoryProduct(req.iTargetProductIndex);
	CHECK_NULL_POINTER_VOID(pTargetProduct);

	if (POTENTIAL_COMPONENT_FREESTYLE != pTargetProduct->GetPotentialComponentIndex())
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_NOT_FREESTYLE_POTENTIAL;
		return;
	}

	CProduct* pMaterialProduct = GetInventoryProduct(req.iMaterialProductIndex);
	CHECK_NULL_POINTER_VOID(pMaterialProduct);

	if (-1 == pMaterialProduct->GetPotentialComponentIndex())
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_NOT_POTENTIAL_CARD;
		return;
	}

	if (TRUE == pTargetProduct->IsEquiped() ||
		TRUE == pMaterialProduct->IsEquiped())
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_EQUIPED;
		return;
	}

	const int iPointCost = COACHCARD.GetPotenUpgradePointCost();
	if (GetUserSkillPoint() < iPointCost)
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_NOT_ENOUGH_POINT;
		return;
	}

	SPotentialComponent sPotentialInfo = pTargetProduct->GetPotentialComponent();
	SPotentialComponentConfig sPotentialConfigInfo;
	COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );
	res.iFreestyleNo = sPotentialConfigInfo.iParam;

	int iNewUpgradeIndex, iNewUpgradeStep;
	if (FALSE == SKILLUPGRADE.GetPotenFreeStyleRandomUpgrade(res.iFreestyleNo, pTargetProduct->GetGradeLevel(), sPotentialInfo.iUpgradeIndex, sPotentialInfo.iUpgradeStep,
							iNewUpgradeIndex, iNewUpgradeStep))
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_FAIL;
		return;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);
	
	int iSPReturn;
	if (ODBC_RETURN_SUCCESS != (iSPReturn = pODBC->POTEN_Upgrade(GetUserIDIndex(), GetGameIDIndex(), pMaterialProduct->GetInventoryIndex(), pMaterialProduct->GetProductIndex(), iPointCost)))
	{
		res.btResult = iSPReturn;
		return;
	}

	int iProductCount = 0;
	RemoveProductToInventory(pMaterialProduct->GetTendencyType(), pMaterialProduct->GetProductIndex(), iProductCount);

	m_PausedPotenUpgradeInfo._bNeedSave = TRUE;
	m_PausedPotenUpgradeInfo._iTargetProductIndex = req.iTargetProductIndex;
	m_PausedPotenUpgradeInfo._iMaterialProductIndex = req.iMaterialProductIndex;
	m_PausedPotenUpgradeInfo._iNewUpgradeIndex = iNewUpgradeIndex;
	m_PausedPotenUpgradeInfo._iNewUpgradeStep = iNewUpgradeStep;

	SetSkillPoint(GetSkillPoint() - iPointCost);
	SendUserGold();

	res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_SUCCESS;
	res.iTargetProductIndex = req.iTargetProductIndex;
	res.iMaterialProductIndex = req.iMaterialProductIndex;
	res.btCurrentUpgradeIndex = sPotentialInfo.iUpgradeIndex;
	res.btCurrentUpgradeStep = sPotentialInfo.iUpgradeStep;
	res.btNewUpgradeIndex = iNewUpgradeIndex;
	res.btNewUpgradeStep = iNewUpgradeStep;

	if(iPointCost > 0)
	{
		GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_POINT, iPointCost);
		GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_POINT, iPointCost);
		GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USE_POINT, iPointCost);
	}
}

RESULT_CHOOSE_POTENTIAL_CARD_UPGRADE CFSGameUser::DecideUpgradePotenCard(int iProductIndex, BOOL bChooseNewUpgrade)
{
	CHECK_CONDITION_RETURN(m_PausedPotenUpgradeInfo._iTargetProductIndex != iProductIndex, RESULT_CHOOSE_POTENTIAL_CARD_UPGRADE_FAIL);

	CProduct* pProduct = GetInventoryProduct(iProductIndex);
	CHECK_CONDITION_RETURN(NULL == pProduct, RESULT_CHOOSE_POTENTIAL_CARD_UPGRADE_FAIL);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_CHOOSE_POTENTIAL_CARD_UPGRADE_FAIL);
	
	int iSPReturn;
	if (ODBC_RETURN_SUCCESS != (iSPReturn = pODBC->POTEN_DecideUpgrade(GetGameIDIndex(), iProductIndex, bChooseNewUpgrade, m_PausedPotenUpgradeInfo._iNewUpgradeIndex, m_PausedPotenUpgradeInfo._iNewUpgradeStep)))
	{
		return (RESULT_CHOOSE_POTENTIAL_CARD_UPGRADE)iSPReturn;
	}

	if (TRUE == bChooseNewUpgrade)
	{
		SPotentialComponent sPotentialInfo = pProduct->GetPotentialComponent();
		sPotentialInfo.iUpgradeIndex = m_PausedPotenUpgradeInfo._iNewUpgradeIndex;
		sPotentialInfo.iUpgradeStep = m_PausedPotenUpgradeInfo._iNewUpgradeStep;

		pProduct->SetPotentialComponent(sPotentialInfo);
	}

	m_PausedPotenUpgradeInfo.Clear();
	
	return RESULT_CHOOSE_POTENTIAL_CARD_UPGRADE_SUCCESS;
}

void CFSGameUser::CheckAndSendPausedPotenUpgradeInfo()
{
	if (0 < m_PausedPotenUpgradeInfo._iTargetProductIndex)
	{
		CProduct* pTargetProduct = GetInventoryProduct(m_PausedPotenUpgradeInfo._iTargetProductIndex);
		CHECK_NULL_POINTER_VOID(pTargetProduct);

		SPotentialComponent sPotentialInfo = pTargetProduct->GetPotentialComponent();

		SPotentialComponentConfig sPotentialConfigInfo;
		COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

		SS2C_UPGRADE_POTENTIAL_CARD_RES res;
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_SUCCESS;
		res.iTargetProductIndex = m_PausedPotenUpgradeInfo._iTargetProductIndex;
		res.iMaterialProductIndex = m_PausedPotenUpgradeInfo._iMaterialProductIndex;
		res.iFreestyleNo = sPotentialConfigInfo.iParam;
		res.btCurrentUpgradeIndex = sPotentialInfo.iUpgradeIndex;
		res.btCurrentUpgradeStep = sPotentialInfo.iUpgradeStep;
		res.btNewUpgradeIndex = m_PausedPotenUpgradeInfo._iNewUpgradeIndex;
		res.btNewUpgradeStep = m_PausedPotenUpgradeInfo._iNewUpgradeStep;

		Send(S2C_UPGRADE_POTENTIAL_CARD_RES, &res, sizeof(SS2C_UPGRADE_POTENTIAL_CARD_RES));
	}
}

BOOL CFSGameUser::PotentialProductSetting( int iProductIndex , BOOL bExpired /* = FALSE */ )
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL( pServer );

	CHECK_NULL_POINTER_BOOL( m_pUserAction );

	SPotentialComponent sPotentialInfo;
	SPotentialComponentConfig sPotentialConfigInfo;

	GetPotentialComponent( iProductIndex, sPotentialInfo );
	COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

	/// @note : Potential-component duplication check
	//			Param null data index check
	//			But, it's stop if it comes to the Potential-component Param multi data
	/// @date : 2013-04-22
	/// @name : M.G.
	if( 0 == GetSlotPotentialExistIndexCount(sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex) || TRUE == bExpired )
	{
		switch( sPotentialInfo.eComponentIndex )
		{
		case POTENTIAL_COMPONENT_ABILITY:
			{
			}break;
		case POTENTIAL_COMPONENT_ACTION:
			{
				SUserActionInventory sActionInventory;
				sActionInventory.iActionCode = sPotentialConfigInfo.iParam;
				sActionInventory.btStatus = 0;

				if (TRUE == m_pUserAction->InsertPotentialAction( m_iGameIDIndex, sActionInventory.iActionCode, 0, sActionInventory.i64ExpireDate))
				{
					m_pUserAction->AddActionInventory( sActionInventory );
				}
			}break;
		case POTENTIAL_COMPONENT_BALANCE:
			{
			}break;
		case POTENTIAL_COMPONENT_FREESTYLE:
			{
				CFSSkillShop* pSkillShop = pServer->GetSkillShop();
				CHECK_NULL_POINTER_BOOL( pSkillShop );
				SAvatarInfo* AvatarInfo = GetCurUsedAvatar();
				CHECK_NULL_POINTER_BOOL( AvatarInfo );

				int iSkillNo = sPotentialConfigInfo.iParam;

				SSkillInfo SkillInfo;
				if(FALSE == pSkillShop->GetSkill(AvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iSkillNo, SkillInfo))
				{
					return FALSE;
				}
			
				int iStorageType = 0;
				int iSkillShiftBit = 0;
				if (FALSE != m_UserSkill->GetDBColumnAndShiftBit(AvatarInfo, iSkillNo, iStorageType, iSkillShiftBit, SKILL_TYPE_FREESTYLE))
				{
					m_UserSkill->InsertFreestyleByPotentialComponent(pSkillShop, GetUserID(), GetUserIDIndex(), GetGameIDIndex(), GetGameID(), iStorageType, iSkillNo, iSkillShiftBit,
						SkillInfo.iBalancePoint, GetCoin(), GetCoin(), SkillInfo.iBalanceTrophy, SkillInfo.iCondition0);
				}
			}break;
		}	// !switch

	}	// !if

	return TRUE;
}

BOOL CFSGameUser::PotentialProductReSet( int iProductIndex )
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL( pServer );

	CHECK_NULL_POINTER_BOOL( m_pUserAction );

	SPotentialComponent sPotentialInfo;
	SPotentialComponentConfig sPotentialConfigInfo;

	GetPotentialComponent( iProductIndex, sPotentialInfo );
	COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

	/// @note : Potential-component duplication check
	//			Param data just once index check
	//			But, it's stop if it comes to the Potential-component Param multi data
	/// @date : 2013-04-22
	/// @name : M.G.
	if( 1 == GetSlotPotentialExistIndexCount(sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex) )
	{	
		switch( sPotentialInfo.eComponentIndex )
		{
		case POTENTIAL_COMPONENT_ABILITY:
			{
			}break;
		case POTENTIAL_COMPONENT_ACTION:
			{
				int iActionCode = sPotentialConfigInfo.iParam;
				CActionShop* pActionShop = pServer->GetActionShop();
				if( NULL == pActionShop )
				{
					g_LogManager.WriteLogToFile( "PotentialProductReSet ActionShop NULL - ActionCode : 11866902  : iGameIDIndex : 0 \n", iActionCode, m_iGameIDIndex );
break;
				}

				// The user have action information.
				BYTE btCategory = pActionShop->GetActionCategory( iActionCode );
				BYTE btStatus = m_pUserAction->GetActionStatus( iActionCode );
				BYTE btSlotNum = m_pUserAction->GetKeyActionIndex( iActionCode );
				
				if( TRUE == m_pUserAction->DeletePotentialAction(m_iGameIDIndex, iActionCode) )
				{
					CPacketComposer PacketComposer( S2C_USER_ACTION_CHANGE_RES );
					PacketComposer.Add( CHANGE_ACTION_SUCCESS );
					PacketComposer.Add( btCategory );
					PacketComposer.Add( btStatus );
					PacketComposer.Add( iActionCode );
					PacketComposer.Add( btSlotNum );
				}	// !if
				
			}break;
		case POTENTIAL_COMPONENT_BALANCE:
			{
			}break;
		case POTENTIAL_COMPONENT_FREESTYLE:
			{
				CFSSkillShop* pSkillShop = pServer->GetSkillShop();
				CHECK_NULL_POINTER_BOOL( pSkillShop );

				int iSkillNo = sPotentialConfigInfo.iParam;

				m_UserSkill->DeleteFreestyleByPotentialComponent(pSkillShop, iSkillNo);
			}break;
		}	// !switch
	}	// !if

	return TRUE;
}

void CFSGameUser::SetAndSortCoachCardInventory(int iSortPropertyType, int iSortOrder)
{
	m_ProductInventory.SetSortData(iSortPropertyType, iSortOrder);
	m_ProductInventory.ProcessSort();
}

void CFSGameUser::SetPotentialComponent( int iProductIndex, const SPotentialComponent& sPotentialInfo )
{
	m_ProductInventory.SetPotentialComponent( sPotentialInfo, iProductIndex );
}

int CFSGameUser::GetPotentialExistIndexCount( POTENTIAL_COMPONENT ePotential, int iRelateIndex )
{
	if( FALSE == CHECK_POTENTIAL_COMPONENT(ePotential) )	return 0;
	if( 0 > iRelateIndex )	return 0;

	return m_ProductInventory.GetPotentialExistIndexCount( ePotential, iRelateIndex );
}

int CFSGameUser::GetSlotPotentialExistIndexCount( POTENTIAL_COMPONENT ePotential, int iRelateIndex )
{
	if( FALSE == CHECK_POTENTIAL_COMPONENT(ePotential) )	return 0;
	if( 0 > iRelateIndex )	return 0;

	int iCount = 0;
	for( int i = 0; i < TENDENCY_MAX_COUNT; ++i )
	{
		int iProductIndex = m_SlotPackageList.GetSlotProductIndex( i );

		SPotentialComponent sPotentialInfo;
		m_ProductInventory.GetPotentialComponent( iProductIndex, sPotentialInfo );

		if( sPotentialInfo.eComponentIndex == ePotential &&
			sPotentialInfo.iRelateIndex == iRelateIndex )
		{
			iCount++;
		}
	}	// !for

	return iCount;
}

BOOL CFSGameUser::IsExistInSlotFreeStylePotentialCard(int iFreeStyleNo)
{
	SPotentialComponentConfig* pConfig = COACHCARD.GetFreeStylePotentialComponent(iFreeStyleNo);
	CHECK_NULL_POINTER_BOOL(pConfig);

	for( int i = 0; i < TENDENCY_MAX_COUNT; ++i )
	{
		int iProductIndex = m_SlotPackageList.GetSlotProductIndex( i );

		SPotentialComponent sPotentialInfo;
		m_ProductInventory.GetPotentialComponent( iProductIndex, sPotentialInfo );

		if( sPotentialInfo.eComponentIndex == pConfig->eComponentIndex &&
			sPotentialInfo.iRelateIndex == pConfig->iRelateIndex )
		{
			return TRUE;
		}
	}

	return FALSE;
}

void CFSGameUser::ProcessAfterRandomCoachCardUse(int iTendencyType,int iUseProductIndex, int iProductCount, SProductData* pProductData, vector<SActionInfluenceConfig>& vecAddedActionInfluence)
{
	CHECK_NULL_POINTER_VOID(pProductData);

	RemoveProductToInventory(iTendencyType, iUseProductIndex, iProductCount);
	AddProductToInventory(pProductData);

	for( vector<SActionInfluenceConfig>::const_iterator constIter = vecAddedActionInfluence.begin(); constIter != vecAddedActionInfluence.end(); ++constIter )
	{
		const SActionInfluenceConfig& sActionInfluenceConfig = (*constIter);
		
		if( PRIMARY_POSTION_SUB == sActionInfluenceConfig.iPrimaryPosition )
			AddProductSubActionInfluence(pProductData->iProductIndex, sActionInfluenceConfig.iActionInfluenceIndex);
	}

}

void CFSGameUser::AddProductSubActionInfluence(int iProductIndex, int iActionInfluenceIndex)
{
	m_ProductInventory.AddProductSubActionInfluence(iProductIndex, iActionInfluenceIndex);
}

void CFSGameUser::MakeCoachCardActionInfluenceList( CPacketComposer& PacketComposer)
{
	vector<SSlotData> vecEquipProductList;
	m_SlotPackageList.GetEquipProductDataAll(vecEquipProductList);

	vector<int> vecEquipActionInfluenceList;

	size_t iEquipProductCount = 0;
	PBYTE pCountLocation = PacketComposer.GetTail();
	PacketComposer.Add(iEquipProductCount);
	
	for(int i = 0; i < vecEquipProductList.size();i++)
	{
		vecEquipActionInfluenceList.clear();
		m_ProductInventory.GetEquipActionInfluence(vecEquipProductList[i].iProductIndex, vecEquipActionInfluenceList);

		CProduct* pProduct = GetInventoryProduct(vecEquipProductList[i].iProductIndex);
		if( NULL == pProduct )
			continue;

		if (0 == pProduct->GetRemainTerm()/60)
			continue;
	
		PacketComposer.Add(vecEquipProductList[i].iProductIndex);
		PacketComposer.Add(vecEquipProductList[i].iTendencyType);
		PacketComposer.Add(vecEquipActionInfluenceList.size());

		for(int k = 0; k < vecEquipActionInfluenceList.size(); k++)
		{
			PacketComposer.Add(vecEquipActionInfluenceList[k]);
		}

		++iEquipProductCount;
	}

	memcpy(pCountLocation, &iEquipProductCount, sizeof(int));
}

void CFSGameUser::MakeCoachCardPotentialComponentList(CPacketComposer& PacketComposer)
{
	vector<SSlotData> vecEquipProductList;
	m_SlotPackageList.GetEquipProductDataAll( vecEquipProductList );

	SPotentialComponent sPotentialInfo;
	SPotentialComponentConfig sPotentialConfigInfo;

	size_t iEquipProductCount = 0;
	PBYTE pCountLocation = PacketComposer.GetTail();
	PacketComposer.Add(iEquipProductCount);

	for( int i = 0; i < vecEquipProductList.size(); i++ )
	{
		m_ProductInventory.GetPotentialComponent( vecEquipProductList[i].iProductIndex, sPotentialInfo );
		COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

		CProduct* pProduct = GetInventoryProduct(vecEquipProductList[i].iProductIndex);
		if( NULL == pProduct )
			continue;

		if (0 == pProduct->GetRemainTerm()/60)
			continue;

		PacketComposer.Add( vecEquipProductList[i].iTendencyType );
		PacketComposer.Add( (int)sPotentialConfigInfo.eComponentIndex );
		PacketComposer.Add( sPotentialConfigInfo.iRelateIndex );
		PacketComposer.Add( sPotentialConfigInfo.iParam );

		++iEquipProductCount;
	}

	memcpy(pCountLocation, &iEquipProductCount, sizeof(int));
}

void CFSGameUser::GetEquipActionInfluence(int iProductIndex, vector<int>& vecEquipActionInfluenceIndex)
{
	m_ProductInventory.GetEquipActionInfluence(iProductIndex, vecEquipActionInfluenceIndex);
}

void CFSGameUser::GetPotentialFreeStyleUpgradeInfo(int iFreeStyleNo, int& iUpgradeIndex, int& iUpgradeStep)
{
	iUpgradeIndex = iUpgradeStep = 0;

	vector<int> vecEquipProduct;
	m_SlotPackageList.GetEquipItemAll( vecEquipProduct );

	SPotentialComponent sPotentialInfo;
	SPotentialComponentConfig sPotentialConfigInfo;
	for( int i = 0; i < vecEquipProduct.size(); ++i )
	{
		GetPotentialComponent( vecEquipProduct[i], sPotentialInfo );

		if (POTENTIAL_COMPONENT_FREESTYLE == sPotentialInfo.eComponentIndex)
		{
			COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

			if (sPotentialConfigInfo.iParam == iFreeStyleNo &&
				0 < sPotentialInfo.iUpgradeIndex)
			{
				iUpgradeIndex = sPotentialInfo.iUpgradeIndex;
				iUpgradeStep = sPotentialInfo.iUpgradeStep;
				break;
			}
		}
	} 
}

void CFSGameUser::GetPotentialFreeStyleUpgradeInfo(vector<SSkillUpgradeInfo>& vec)
{
	vector<int> vecEquipProduct;
	m_SlotPackageList.GetEquipItemAll( vecEquipProduct );

	SSkillUpgradeInfo UpgradeInfo;
	UpgradeInfo.iSkillKind = SKILL_TYPE_FREESTYLE;

	SPotentialComponent sPotentialInfo;
	SPotentialComponentConfig sPotentialConfigInfo;
	for( int i = 0; i < vecEquipProduct.size(); ++i )
	{
		GetPotentialComponent( vecEquipProduct[i], sPotentialInfo );

		if (POTENTIAL_COMPONENT_FREESTYLE == sPotentialInfo.eComponentIndex &&
			0 < sPotentialInfo.iUpgradeIndex)
		{
			COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

			BOOL bExist = FALSE;
			for (int j = 0; j < vec.size(); ++j)
			{
				if (vec[j].iSkillNo == sPotentialConfigInfo.iParam)
				{
					bExist = TRUE;
					break;
				}
			}
			if (FALSE == bExist)
			{
				UpgradeInfo.iSkillNo = sPotentialConfigInfo.iParam;
				UpgradeInfo.iUpgradeIndex = sPotentialInfo.iUpgradeIndex;
				UpgradeInfo.iUpgradeStep = sPotentialInfo.iUpgradeStep;

				vec.push_back(UpgradeInfo);
			}
		}
	}
}

int CFSGameUser::CompareSystemTime(SYSTEMTIME CurrentTime, SDateInfo& ExpireDate)
{
	SYSTEMTIME ExpireTime;
	ExpireTime.wYear = ExpireDate.iYear;			
	ExpireTime.wMonth = ExpireDate.iMonth;
	ExpireTime.wDay = ExpireDate.iDay;			
	ExpireTime.wHour = ExpireDate.iHour;
	ExpireTime.wMinute = ExpireDate.iMin;			
	ExpireTime.wSecond = 0;
	ExpireTime.wMilliseconds = 0;			
	ExpireTime.wDayOfWeek = 0;

	return (int)(::CompareSystemTime(&ExpireTime, &CurrentTime));	
}

void CFSGameUser::MakeEquipSlotPackageListRes( CPacketComposer &PacketComposer)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	vector<SSlotData> vecEquipProductData;
	
	m_SlotPackageList.GetEquipProductDataAll(vecEquipProductData);
	int iPackageCount = vecEquipProductData.size();
	PacketComposer.Add(iPackageCount);		

	SProductData sProductData;
	int iMinute = 0;
	SActionInfluenceConfig sActionInfluenceConfig;

	SYSTEMTIME CurrentTime;
	::GetLocalTime(&CurrentTime);

	for(int i = 0 ; i < vecEquipProductData.size(); i++)
	{
		m_ProductInventory.GetProductData(vecEquipProductData[i].iProductIndex, sProductData);


		PacketComposer.Add(sProductData.iTendencyType);
		PacketComposer.Add(sProductData.iProductIndex);
		PacketComposer.Add(sProductData.iRarityLevel);
		PacketComposer.Add(sProductData.iGradeLevel);

		if( TERM_TYPE_LIMITED == sProductData.byTermType )
		{
			iMinute= CompareSystemTime(CurrentTime, sProductData.sExpireDate) / 60;
		}

		PacketComposer.Add(iMinute);
		BOOL bSpecial = static_cast<BOOL>(sProductData.iSpecialCardIndex == SPECIALCARD_INDEX_THREEKINGDOM);
		PacketComposer.Add(bSpecial); // Client �� BOOL  ������ �ﱹ���� �����ϰ� ����

		BYTE btMainInfluenceCount = 0;
		PBYTE pCountLocation = PacketComposer.GetTail();
		PacketComposer.Add(btMainInfluenceCount);
		for (int iInfluenceIndex = 0; iInfluenceIndex < MAX_POTENCARD_MAIN_INFLUENCE_COUNT; ++iInfluenceIndex)
		{
			if (0 >= sProductData.iaActionInfluenceIndex[iInfluenceIndex])	
				continue;

			COACHCARD.GetActionInfluenceList()->FindAndGetActionInfluenceConfig(sProductData.iaActionInfluenceIndex[iInfluenceIndex],sActionInfluenceConfig);
			PacketComposer.Add(sActionInfluenceConfig.iActionType);
			PacketComposer.Add(sActionInfluenceConfig.iInfluenceType);
			++btMainInfluenceCount;
		}
		memcpy(pCountLocation, &btMainInfluenceCount, sizeof(BYTE));

		// packet add Potential-0rarity
		SPotentialComponent sPotentialInfo;
		GetPotentialComponent( vecEquipProductData[i].iProductIndex, sPotentialInfo );
		PacketComposer.Add(sPotentialInfo.eComponentIndex);

		SPotentialComponentConfig sPotentialConfigInfo;
		COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

		switch(sPotentialInfo.eComponentIndex)
		{
		case POTENTIAL_COMPONENT_FREESTYLE:
			{
				PacketComposer.Add(sPotentialConfigInfo.iParam);
				PacketComposer.Add((BYTE)sPotentialInfo.iUpgradeIndex);
				PacketComposer.Add((BYTE)sPotentialInfo.iUpgradeStep);
			}
			break;
		case POTENTIAL_COMPONENT_BALANCE:
		case POTENTIAL_COMPONENT_ACTION:
			{
				PacketComposer.Add(sPotentialConfigInfo.iParam);
			}
			break;
		case POTENTIAL_COMPONENT_ABILITY:
			{
				// The potential-card ability information output in the property-list
				int iPropertyIndex = sPotentialConfigInfo.iParam;
				CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList( iPropertyIndex );
				CHECK_NULL_POINTER_VOID(pItemProperty);
				
				vector<SItemProperty> vecItemProperty;
				pItemProperty->GetItemProperty( vecItemProperty );

				int iAbilityCount = vecItemProperty.size();
				const int MAX_POTENTIAL_ABILITY_COUNT = 1;
				if (MAX_POTENTIAL_ABILITY_COUNT < iAbilityCount)
				{
					WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeEquipSlotPackageListRes - iAbilityCount overflow:11866902", iAbilityCount);
AbilityCount = MAX_POTENTIAL_ABILITY_COUNT;
				}
				PacketComposer.Add((BYTE)iAbilityCount);

				for( int iAbilityIdx = 0; iAbilityIdx < iAbilityCount && iAbilityIdx < MAX_POTENTIAL_ABILITY_COUNT; ++iAbilityIdx )
				{
					PacketComposer.Add((BYTE)vecItemProperty[iAbilityIdx].iProperty);
					PacketComposer.Add((BYTE)vecItemProperty[iAbilityIdx].iValue);
				}
			}
			break;
		}
	}
}

void CFSGameUser::MakeCoachCardSlotPackageListRes( CPacketComposer &PacketComposer)
{
	SUserSInfo1 info;
	MakeDataForCoachCardSlotPackageList(info.Buffer, info.iDataSize);

	if (MAX_USER_INFO_RANK_DATA <= info.iDataSize)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeCoachCardSlotPackageListRes - DataSize overflow:11866902, GameIDIndex:0", info.iDataSize, GetGameIDIndex());
urn;
	}

	PacketComposer.Add(info.Buffer, info.iDataSize);
}

void CFSGameUser::MakeDataForCoachCardSlotPackageList(PBYTE pBuffer, int& iSize)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);
	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SActionInfluenceConfig sActionInfluenceConfig;
	vector<SSlotData> vecEquipProductData;
	m_SlotPackageList.GetEquipProductDataAll(vecEquipProductData);

	const int MAX_EQUIP_PRODUCT_COUNT = 3;
	for(int i = 0 ; i < vecEquipProductData.size() && i < MAX_EQUIP_PRODUCT_COUNT; i++)
	{
		SPotentialComponent sPotentialInfo;
		CHECK_CONDITION_RETURN_VOID(FALSE == GetPotentialComponent(vecEquipProductData[i].iProductIndex, sPotentialInfo));
	}

	int iPackageCount = vecEquipProductData.size();
	if (MAX_EQUIP_PRODUCT_COUNT < iPackageCount)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeDataForCoachCardSlotPackageList - iPackageCount overflow:11866902, GameIDIndex:0", iPackageCount, GetGameIDIndex());
ckageCount = MAX_EQUIP_PRODUCT_COUNT;
	}
	ADD_DATA(pBuffer, iSize, iPackageCount);

	SProductData sProductData;
	int iMinute = 0;

	SYSTEMTIME CurrentTime;
	::GetLocalTime(&CurrentTime);

	for(int i = 0 ; i < vecEquipProductData.size() && i < MAX_EQUIP_PRODUCT_COUNT; i++)
	{
		m_ProductInventory.GetProductData(vecEquipProductData[i].iProductIndex, sProductData);

		ADD_DATA(pBuffer, iSize, sProductData.iTendencyType);
		ADD_DATA(pBuffer, iSize, sProductData.iProductIndex);
		ADD_DATA(pBuffer, iSize, sProductData.iRarityLevel);
		ADD_DATA(pBuffer, iSize, sProductData.iGradeLevel);

		if( TERM_TYPE_LIMITED == sProductData.byTermType )
		{
			iMinute= CompareSystemTime(CurrentTime, sProductData.sExpireDate) / 60;
		}

		BOOL bSpecialCardIndex = static_cast<BOOL>(sProductData.iSpecialCardIndex == SPECIALCARD_INDEX_THREEKINGDOM);
		ADD_DATA(pBuffer, iSize, iMinute);
		ADD_DATA(pBuffer, iSize, bSpecialCardIndex);

		BYTE btMainInfluenceCount = 0;
		PBYTE pCountLocation = pBuffer + iSize;
		ADD_DATA(pBuffer, iSize, btMainInfluenceCount);

		for (int iInfluenceIndex = 0; iInfluenceIndex < MAX_POTENCARD_MAIN_INFLUENCE_COUNT; ++iInfluenceIndex)
		{
			if (0 >= sProductData.iaActionInfluenceIndex[iInfluenceIndex])
				continue;

			COACHCARD.GetActionInfluenceList()->FindAndGetActionInfluenceConfig(sProductData.iaActionInfluenceIndex[iInfluenceIndex], sActionInfluenceConfig);

			ADD_DATA(pBuffer, iSize, sActionInfluenceConfig.iActionType);
			ADD_DATA(pBuffer, iSize, sActionInfluenceConfig.iInfluenceType);
			++btMainInfluenceCount;
		}
		memcpy(pCountLocation, &btMainInfluenceCount, sizeof(BYTE));

		// packet add Potential-0rarity
		SPotentialComponent sPotentialInfo;
		GetPotentialComponent( vecEquipProductData[i].iProductIndex, sPotentialInfo );
		ADD_DATA(pBuffer, iSize, sPotentialInfo.eComponentIndex);

		SPotentialComponentConfig sPotentialConfigInfo;
		COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

		switch(sPotentialInfo.eComponentIndex)
		{
		case POTENTIAL_COMPONENT_FREESTYLE:
			{
				ADD_DATA(pBuffer, iSize, sPotentialConfigInfo.iParam);
				ADD_DATA(pBuffer, iSize, (BYTE)sPotentialInfo.iUpgradeIndex);
				ADD_DATA(pBuffer, iSize, (BYTE)sPotentialInfo.iUpgradeStep);
			}
			break;
		case POTENTIAL_COMPONENT_BALANCE:
		case POTENTIAL_COMPONENT_ACTION:
			{
				int iDummy = 0;
				ADD_DATA(pBuffer, iSize, iDummy);
				ADD_DATA(pBuffer, iSize, sPotentialConfigInfo.iParam);
				ADD_DATA(pBuffer, iSize, iDummy);
			}
			break;
		case POTENTIAL_COMPONENT_ABILITY:
			{
				// The potential-card ability information output in the property-list
				int iPropertyIndex = sPotentialConfigInfo.iParam;
				CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList( iPropertyIndex );
				CHECK_NULL_POINTER_VOID(pItemProperty);
				
				vector<SItemProperty> vecItemProperty;
				pItemProperty->GetItemProperty( vecItemProperty );

				int iAbilityCount = vecItemProperty.size();
				const int MAX_POTENTIAL_ABILITY_COUNT = 1;
				if (MAX_POTENTIAL_ABILITY_COUNT < iAbilityCount)
				{
					WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeDataForCoachCardSlotPackageList - iAbilityCount overflow:11866902, GameIDIndex:0", iAbilityCount, GetGameIDIndex());
iAbilityCount = MAX_POTENTIAL_ABILITY_COUNT;
				}
				ADD_DATA(pBuffer, iSize, iAbilityCount);

				for( int iAbilityIdx = 0; iAbilityIdx < iAbilityCount && iAbilityIdx < MAX_POTENTIAL_ABILITY_COUNT; ++iAbilityIdx )
				{
					ADD_DATA(pBuffer, iSize, vecItemProperty[iAbilityIdx].iProperty);
					ADD_DATA(pBuffer, iSize, vecItemProperty[iAbilityIdx].iValue);
				}
			}
			break;
		case POTENTIAL_COMPONENT_NOT:
			{
				int iDummy = 0;
				ADD_DATA(pBuffer, iSize, iDummy);
				ADD_DATA(pBuffer, iSize, iDummy);
				ADD_DATA(pBuffer, iSize, iDummy);
			}
			break;
		}
	}
}

BOOL CFSGameUser::LoadStyleInfo()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC)

	//StyleIndex
	int iUseStyle;
	if (ODBC_RETURN_SUCCESS != pBaseODBC->AVATAR_GetUseStyleIndex(GetGameIDIndex(), iUseStyle))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "AVATAR_GetUseStyleIndex failed");
		return FALSE;
	}
	CHECK_CONDITION_RETURN(iUseStyle <= 0 || iUseStyle > MAX_STYLE_COUNT, FALSE);

	//StyleItemList
	vector<SStyleInfo> vecStyleInfo;
	int	iStyleSubIndex[MAX_STYLE_COUNT] = {0};
	if(ODBC_RETURN_SUCCESS != pBaseODBC->AVATAR_GetStyleInfo(GetGameIDIndex(), vecStyleInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "AVATAR_GetStyleInfo failed");
		return FALSE;
	}

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	if (0 < pAvatar->iSpecialCharacterIndex)
	{
		CAvatarCreateManager* pAvatarCreateManager = &AVATARCREATEMANAGER;
		CHECK_NULL_POINTER_BOOL(pAvatarCreateManager);

		SSpecialAvatarConfig* pAvatarConfig = pAvatarCreateManager->GetSpecialAvatarConfigByIndex(pAvatar->iSpecialCharacterIndex);
		CHECK_NULL_POINTER_BOOL(pAvatarConfig);

		m_AvatarStyleInfo.iStyleCount = pAvatarConfig->GetMultipleCharacterCount();

		if (m_AvatarStyleInfo.iStyleCount > 1)
		{
			pAvatar->iFace = pAvatarConfig->GetFace(iUseStyle-1);
		}
	}

	CHECK_CONDITION_RETURN(MAX_STYLE_COUNT < m_AvatarStyleInfo.iStyleCount, FALSE);

	m_AvatarStyleInfo.iUseStyleOnDB = iUseStyle;

	vector<SStyleInfo>::iterator itr;
	vector<SStyleInfo>::iterator itrEnd = vecStyleInfo.end();
	for(itr = vecStyleInfo.begin(); itr != itrEnd; ++itr)
	{
		int iStyleIndex = itr->iStyleIndex - 1;
		if(iStyleIndex > -1 && iStyleIndex < MAX_STYLE_COUNT && iStyleSubIndex[iStyleIndex] < MAX_ITEMCHANNEL_NUM)
		{
			m_AvatarStyleInfo.iItemIdx[iStyleIndex][iStyleSubIndex[iStyleIndex]] = itr->iItemIndex;
			++iStyleSubIndex[iStyleIndex];
		}
	}

	//StyleName
	vector<SStyleNameInfo> vecStyleNameInfo;
	if(ODBC_RETURN_SUCCESS != pBaseODBC->AVATAR_GetStyleNameInfo(GetGameIDIndex(), vecStyleNameInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "AVATAR_GetStyleNameInfo failed");
		return FALSE;
	}

	vector<SStyleNameInfo>::iterator itrName;
	vector<SStyleNameInfo>::iterator itrNameEnd = vecStyleNameInfo.end();
	for(itrName = vecStyleNameInfo.begin(); itrName != itrNameEnd; ++itrName)
	{
		int iStyleIndex = itrName->iStyleIndex;
		if(iStyleIndex > 0 && iStyleIndex <= MAX_STYLE_COUNT)
		{
			memcpy(m_AvatarStyleInfo.szStyleName[iStyleIndex-1], itrName->szStyleName, MAX_STYLE_NAME_LENGTH+1);
		}
	}

	SetUseStyleIndex(iUseStyle);

	return TRUE;
}

void CFSGameUser::ChangeUseStyleName(char* szStyleName)
{
	CHECK_NULL_POINTER_VOID(szStyleName)

	int iUseStyleIndex = GetUseStyleIndex() - 1;

	if(iUseStyleIndex > -1 && iUseStyleIndex < MAX_STYLE_COUNT)
	{
		strncpy_s(m_AvatarStyleInfo.szStyleName[iUseStyleIndex], szStyleName, MAX_STYLE_NAME_LENGTH+1);
	}
}

char* CFSGameUser::GetUseStyleName()
{
	int iUseStyleIndex = GetUseStyleIndex() - 1;

	if(iUseStyleIndex > -1 && iUseStyleIndex < MAX_STYLE_COUNT)
	{
		return m_AvatarStyleInfo.szStyleName[iUseStyleIndex];
	}
	else
	{
		return NULL;
	}
}

BOOL CFSGameUser::SetUseStyleIndex(int iStyleIndex, BOOL bReLoad/* = TRUE*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_AvatarStyleInfo.IsValidStyleIndex(iStyleIndex), FALSE);

	 if( TRUE == m_bCloneCharacter )
	 {
		 if( CLONE_CHARACTER_DEFULAT_STYLE_INDEX <= iStyleIndex )
			 m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Clone] = iStyleIndex;
		 else
			 m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Mine] = iStyleIndex;
	 }
	 else
	 {
		 if (m_AvatarStyleInfo.iUseStyle != iStyleIndex)
		 {
			 CAvatarCreateManager* pManager = &AVATARCREATEMANAGER;
			 if (NULL != pManager)
			 {
				 pManager->UpdateMultipleCharacterInfo(GetCurUsedAvatar(), m_AvatarStyleInfo.iUseStyle, iStyleIndex);
			 }
		 }
	 }

	 m_AvatarStyleInfo.iUseStyle = iStyleIndex;

	 CHECK_CONDITION_RETURN(FALSE == bReLoad, TRUE);

	 return ReLoadFeatureInfo();
}

int CFSGameUser::GetUseStyleIndex()
{
	return m_AvatarStyleInfo.iUseStyle;
}

BOOL CFSGameUser::CheckItemUseStyle(int iStyleIndex, int iItemIdx)
{
	if(iStyleIndex > 0 && iStyleIndex <= MAX_STYLE_COUNT)
	{
		for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
		{
			if(m_AvatarStyleInfo.iItemIdx[iStyleIndex - 1][i] == iItemIdx)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

void CFSGameUser::SendUseStyleAndNameInfo()
{
	CPacketComposer PacketComposer(S2C_USER_STYLE_INFO);

	int iUseStyle =0;
	int ibaseSumIndex = 0;

	if( CLONE_CHARACTER_DEFULAT_STYLE_INDEX <= m_AvatarStyleInfo.iUseStyle)
	{
		iUseStyle = m_AvatarStyleInfo.iUseStyle - MAX_STYLESLOT_NUM;
		ibaseSumIndex = MAX_STYLESLOT_NUM;
	}
	else
		iUseStyle = m_AvatarStyleInfo.iUseStyle;

	PacketComposer.Add(iUseStyle);

	for(int i = 0 + ibaseSumIndex ; i < MAX_STYLESLOT_NUM + ibaseSumIndex ; ++i)
	{
		if( i >= MAX_STYLE_COUNT )
			return;

		PacketComposer.Add((BYTE*)m_AvatarStyleInfo.szStyleName[i], MAX_STYLE_NAME_LENGTH+1);
	}

	Send(&PacketComposer);
}

SAvatarStyleInfo* CFSGameUser::GetStyleInfo()
{
	return &m_AvatarStyleInfo;
}

BOOL CFSGameUser::ReLoadFeatureInfo(BOOL bSendStat /*=TRUE*/)
{
	int iStyleIndex = GetUseStyleIndex();
	CHECK_CONDITION_RETURN(0 >= iStyleIndex || MAX_STYLE_COUNT < iStyleIndex, FALSE);

	const int iGameIDIndex = GetGameIDIndex();

	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList)

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	m_UserItem->FeatureInfoInitilaize();
	m_UserItem->SetOriginalItem();

	if( FALSE == CheckExpireItem( ) )
	{
		g_LogManager.WriteLogToFile("CheckExpireItem Failed : 11866902 " , iGameIDIndex);
	//m_UserItem->RestoreUseFeatureInfoByIndex();	//���� Ȯ���� ����(���ʿ��ҵ���) - ��Ÿ�� 

	if( TRUE == bSendStat )
		SendUserStat();

	return TRUE;
}

BOOL CFSGameUser::ChangeUseTryItemFeature(int iStyleIndex)
{
	CFSGameUserItem* pUserItem = GetUserItemList();
	CHECK_NULL_POINTER_BOOL(pUserItem)

	pUserItem->ChangeUseTryItem(iStyleIndex);

	return TRUE;
}

void CFSGameUser::SendSpecialPartsInfo(map<int, BYTE> mapEquipCount, map<int, vector<int>> mapProperty, int iChangedUseStyle)
{
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList)

	map<int, SSpecialPartsItemEquipInfo*> mapSpecialPartsItemEquipInfo;

	pAvatarItemList->GetSpecialPartsItemEquipList(mapSpecialPartsItemEquipInfo);

	CPacketComposer PacketComposer(S2C_SPECIAL_PARTS_INFO_RES);

	int iPartsNum = mapSpecialPartsItemEquipInfo.size();

	PacketComposer.Add(iPartsNum);

	map<int, SSpecialPartsItemEquipInfo*>::iterator itrMap = mapSpecialPartsItemEquipInfo.begin();
	map<int, SSpecialPartsItemEquipInfo*>::iterator itrMapEnd = mapSpecialPartsItemEquipInfo.end();

	int iUseStyle = GetUseStyleIndex();
	if(iChangedUseStyle > 0 && iChangedUseStyle <= MAX_STYLE_COUNT)
		iUseStyle = iChangedUseStyle;

	while(itrMap != itrMapEnd)
	{
		SSpecialPartsItemEquipInfo* pSpecialPartsItemEquipInfo = itrMap->second;
		if(pSpecialPartsItemEquipInfo)
		{
			int iSpecialPartsIndex = pSpecialPartsItemEquipInfo->iSpecialPartsIndex;
			int iItemStatus[MAX_SPECIAL_PARTS_ITEM_COUNT] = {0};
			BYTE btEquipCount = 0;

			PacketComposer.Add(iSpecialPartsIndex);

			for(int i = 0; i < MAX_SPECIAL_PARTS_ITEM_COUNT; ++i)
			{
				int iItemStatus = ITEM_STATUS_INVENTORY;
				if(-1 != pSpecialPartsItemEquipInfo->iItemIndex[i] &&
					CheckItemUseStyle(iUseStyle, pSpecialPartsItemEquipInfo->iItemIndex[i]))
				{
					++btEquipCount;
					iItemStatus = ITEM_STATUS_WEAR;
				}
				PacketComposer.Add(iItemStatus);
				PacketComposer.Add(SPECIALPARTS.GetSpecialPartsItemCodeByIndex(iSpecialPartsIndex, i));
			}
			
			map<int, BYTE>::iterator iter = mapEquipCount.find(iSpecialPartsIndex);
			if(iter != mapEquipCount.end())
			{
				btEquipCount = iter->second;
			}

			PacketComposer.Add(btEquipCount);
			PacketComposer.Add((BYTE*)SPECIALPARTS.GetSpecialPartsNameByIndex(iSpecialPartsIndex), MAX_SPECIALPARTSNAME_LENGTH+1);

			SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
			vector<SSpecialPartsPropertyList> vecPropertyList;
			SPECIALPARTS.GetSpecialPartsPropertyList(sKey, vecPropertyList);
			int iStatNum = vecPropertyList.size();
			
			int iReqStatNum = 0;
			for(int i = 0; i < iStatNum; ++i)
			{
				if(vecPropertyList[i].btListType == SPECIALPARTS_PROPERTY_LIST_NORMAL_CHOICE ||
					vecPropertyList[i].btListType == SPECIALPARTS_PROPERTY_LIST_ALLSTAT_CHOICE)
					iReqStatNum++;
			}

			vector<int> vProperty;
			if(mapProperty.size() > 0)
			{
				map<int, vector<int>>::iterator iter = mapProperty.find(iSpecialPartsIndex);
				if(iter != mapProperty.end())
				{
					CHECK_CONDITION_RETURN_VOID(iter->second.size() != iReqStatNum);

					vProperty = iter->second;
				}				
			}

			PacketComposer.Add(iStatNum);

			for(int i = 0; i < iStatNum; ++i)
			{
				PacketComposer.Add(vecPropertyList[i].btListType);

				if(vecPropertyList[i].btListType != SPECIALPARTS_PROPERTY_LIST_NORMAL_CHOICE &&
					vecPropertyList[i].btListType != SPECIALPARTS_PROPERTY_LIST_ALLSTAT_CHOICE)
				{
					// �޺��ڽ��� �����ϴ� �ɷ�ġ�� �ƴ� �������ɷ�ġ �� ���,
					int iProperty = -1;
					int iValue = 0;

					SPECIALPARTS.GetSpecialPartsPropertyConfig(vecPropertyList[i].iPartsPropertyIndex, iProperty, iValue);

					PacketComposer.Add(iProperty);
					PacketComposer.Add(iValue);
				}
				else
				{
					if(vProperty.size() > 0)
					{
						int iProperty = -1;
						int iValue = 0;

						SPECIALPARTS.GetSpecialPartsPropertyConfig(vProperty[i], iProperty, iValue);

						PacketComposer.Add(iProperty);
						PacketComposer.Add(iValue);
					}
					else
					{
						map<int, BYTE>::iterator iter = mapEquipCount.find(iSpecialPartsIndex);
						if(iter != mapEquipCount.end())
						{
							PacketComposer.Add(-1);
							PacketComposer.Add(0);
						}
						else
						{
							SSpecialPartsUserProperty* pSpecialPartsUserProperty = GetSpecialPartsUserProperty(iUseStyle, iSpecialPartsIndex, vecPropertyList[i].btPropertyNum);
							if(pSpecialPartsUserProperty)
							{
								PacketComposer.Add(pSpecialPartsUserProperty->iProperty);
								PacketComposer.Add(pSpecialPartsUserProperty->iValue);
							}
							else
							{
								PacketComposer.Add(-1);
								PacketComposer.Add(0);
							}
						}					
					}		
				}				
			}

			delete pSpecialPartsItemEquipInfo;
		}

		++itrMap;
	}

	Send(&PacketComposer);
}

BOOL CFSGameUser::LoadSpecialPartsUserProperty()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC)

	m_vecSpecialPartsUserProperty.clear();

	if(ODBC_RETURN_SUCCESS == pBaseODBC->SPECIALPARTS_GetUserProperty(GetGameIDIndex(), m_vecSpecialPartsUserProperty))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


SSpecialPartsUserProperty* CFSGameUser::GetSpecialPartsUserProperty(int iUseStyleIndex, int iSpecialPartsIndex, int iSlotNum)
{
	vector<SSpecialPartsUserProperty*>::iterator itr;
	vector<SSpecialPartsUserProperty*>::iterator itrEnd = m_vecSpecialPartsUserProperty.end();

	for(itr = m_vecSpecialPartsUserProperty.begin(); itr != itrEnd; ++itr)
	{
		SSpecialPartsUserProperty* pSpecialPartsUserProperty = *itr;

		if(pSpecialPartsUserProperty)
		{
			if((pSpecialPartsUserProperty->iStyleIndex == iUseStyleIndex) && (pSpecialPartsUserProperty->iSpecialPartsIndex == iSpecialPartsIndex) && (pSpecialPartsUserProperty->iPropertySlotNumber == iSlotNum))
			{
				return *itr;
			}
		}
	}

	return NULL;
}

void CFSGameUser::UpdateSpecialPartsUserProperty(int iSpecialPartsIndex, vector<int>& vecProperty)
{
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);

	int iUseStyle = GetUseStyleIndex();
	BYTE btEquipCount = pAvatarItemList->GetSpecialPartsItemEquipCount(iSpecialPartsIndex);

	SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
	vector<SSpecialPartsPropertyList> vecPropertyList;
	SPECIALPARTS.GetSpecialPartsChoicePropertyList(sKey, vecPropertyList);
	int iStatNum = vecPropertyList.size();

	CHECK_CONDITION_RETURN_VOID(vecProperty.size() != iStatNum);

	for(int i = 0; i < iStatNum; ++i)
	{
		int iProperty = -1;
		int iValue = 0;

		if(vecPropertyList[i].btListType != SPECIALPARTS_PROPERTY_LIST_NORMAL_CHOICE &&
			vecPropertyList[i].btListType != SPECIALPARTS_PROPERTY_LIST_ALLSTAT_CHOICE)
			continue;

		if(FALSE == SPECIALPARTS.GetSpecialPartsPropertyConfig(vecPropertyList[i].iPartsPropertyIndex, iProperty, iValue))
			continue;
		
		if(PROPERTY_NONE == iProperty)
		{
			if(PROPERTY_RUN > vecProperty[i] || PROPERTY_CLOSE < vecProperty[i])
				continue;
		}
		else
		{
			if(iProperty != vecProperty[i])
				continue;
		}

		SSpecialPartsUserProperty* pSpecialPartsUserProperty = GetSpecialPartsUserProperty(iUseStyle, iSpecialPartsIndex, vecPropertyList[i].btPropertyNum);

		if(pSpecialPartsUserProperty)
		{
			pSpecialPartsUserProperty->iPartsPropertyIndex = vecProperty[i]; // Property �� PropertyIndex �� ������ �ӽ÷�..
			pSpecialPartsUserProperty->iProperty = vecProperty[i];
			pSpecialPartsUserProperty->iValue = iValue;
		}
		else
		{
			pSpecialPartsUserProperty = new SSpecialPartsUserProperty;

			pSpecialPartsUserProperty->iSpecialPartsIndex = iSpecialPartsIndex;
			pSpecialPartsUserProperty->iStyleIndex = iUseStyle;
			pSpecialPartsUserProperty->iPropertySlotNumber = i;
			pSpecialPartsUserProperty->iPartsPropertyIndex = vecProperty[i]; // Property �� PropertyIndex �� ������ �ӽ÷�..
			pSpecialPartsUserProperty->iProperty = vecProperty[i];
			pSpecialPartsUserProperty->iValue = iValue;

			m_vecSpecialPartsUserProperty.push_back(pSpecialPartsUserProperty);
		}

		if(ODBC_RETURN_SUCCESS != pBaseODBC->SPECIALPARTS_ApplyUserProperty(GetGameIDIndex(), iUseStyle, iSpecialPartsIndex, vecPropertyList[i].btPropertyNum, vecProperty[i]))
		{
			WRITE_LOG_NEW(LOG_TYPE_SPECIALPARTS, DB_DATA_UPDATE, FAIL, "SPECIALPARTS_ApplyUserProperty - GameIDIndex:11866902, SpecialPartsIndex:0", GetGameIDIndex(), iSpecialPartsIndex);
	}
}

void CFSGameUser::CheckSpecialPartsUserProperty()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pBaseODBC);
	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iUseStyle = GetUseStyleIndex();

	map<int, SSpecialPartsItemEquipInfo*> mapSpecialPartsItemEquipInfo;
	pAvatarItemList->GetSpecialPartsItemEquipList(mapSpecialPartsItemEquipInfo);

	map<int, SSpecialPartsItemEquipInfo*>::iterator itr = mapSpecialPartsItemEquipInfo.begin();
	map<int, SSpecialPartsItemEquipInfo*>::iterator itrEnd = mapSpecialPartsItemEquipInfo.end();
	for( ; itr != itrEnd; ++itr)
	{
		SSpecialPartsItemEquipInfo* pSpecialPartsItemEquipInfo = itr->second;
		if(pSpecialPartsItemEquipInfo)
		{
			int iSpecialPartsIndex = pSpecialPartsItemEquipInfo->iSpecialPartsIndex;
			/*BYTE btEquipCount = 0;

			for(int i = 0; i < MAX_SPECIAL_PARTS_ITEM_COUNT; ++i)
			{
			if(pSpecialPartsItemEquipInfo->iItemStatus[i] == ITEM_STATUS_WEAR)
			{
			++btEquipCount;
			}
			}

			SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
			vector<SSpecialPartsPropertyList> vecPropertyList;
			SPECIALPARTS.GetSpecialPartsChoicePropertyList(sKey, vecPropertyList);*/
			int iStatNum = 0; //SPECIALPARTS.CheckInitProperty(sKey) ? 0 : vecPropertyList.size();
			int iMaxStatCount = SPECIALPARTS.GetSpecialPartsMaxStatCount(iSpecialPartsIndex);

			for(int i = iStatNum; i < iMaxStatCount; ++i)
			{
				SSpecialPartsUserProperty* pSpecialPartsUserProperty = GetSpecialPartsUserProperty(iUseStyle, iSpecialPartsIndex, i);
				if(pSpecialPartsUserProperty && pSpecialPartsUserProperty->iPartsPropertyIndex != -1)
				{
					pSpecialPartsUserProperty->iPartsPropertyIndex = -1;
					pSpecialPartsUserProperty->iProperty = -1;
					pSpecialPartsUserProperty->iValue = 0;

					if(ODBC_RETURN_SUCCESS != pBaseODBC->SPECIALPARTS_ApplyUserProperty(GetGameIDIndex(), iUseStyle, iSpecialPartsIndex, i, -1))
					{
						WRITE_LOG_NEW(LOG_TYPE_SPECIALPARTS, DB_DATA_UPDATE, FAIL, "SPECIALPARTS_ApplyUserProperty - GameIDIndex:11866902, SpecialPartsIndex:0", GetGameIDIndex(), iSpecialPartsIndex);
}
				}
			}

			delete pSpecialPartsItemEquipInfo;
		}
	}
}

BOOL CFSGameUser::CheckSpecialPartsOptionProperty(float fShootSuccessRate, float fBlockSuccessRate)
{
	CHECK_CONDITION_RETURN(fShootSuccessRate == 0 && fBlockSuccessRate == 0, TRUE);

	BOOL bCheckShootSuccessRate = (fShootSuccessRate == 0);
	BOOL bCheckBlockSuccessRate = (fBlockSuccessRate == 0);

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);
	
	map<int, SSpecialPartsItemEquipInfo*> mapSpecialPartsItemEquipInfo;
	pAvatarItemList->GetSpecialPartsItemEquipList(mapSpecialPartsItemEquipInfo);

	map<int, SSpecialPartsItemEquipInfo*>::iterator itr = mapSpecialPartsItemEquipInfo.begin();
	map<int, SSpecialPartsItemEquipInfo*>::iterator itrEnd = mapSpecialPartsItemEquipInfo.end();
	for( ; itr != itrEnd; ++itr)
	{
		SSpecialPartsItemEquipInfo* pSpecialPartsItemEquipInfo = itr->second;
		if(pSpecialPartsItemEquipInfo)
		{
			int iSpecialPartsIndex = pSpecialPartsItemEquipInfo->iSpecialPartsIndex;
			BYTE btEquipCount = 0;

			for(int i = 0; i < MAX_SPECIAL_PARTS_ITEM_COUNT; ++i)
			{
				if(pSpecialPartsItemEquipInfo->iItemStatus[i] == ITEM_STATUS_WEAR)
				{
					++btEquipCount;
				}
			}

			SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
			vector<SSpecialPartsPropertyList> vecPropertyList;
			SPECIALPARTS.GetSpecialPartsOptionOnPropertyList(sKey, vecPropertyList);

			for(int i = 0; i < vecPropertyList.size(); ++i)
			{
				int iProperty = PROPERTY_NONE;
				int iValue = 0;
				SPECIALPARTS.GetSpecialPartsPropertyConfig(vecPropertyList[i].iPartsPropertyIndex, iProperty, iValue);

				if(PROPERTY_SHOOT_SUCCESS == iProperty)
				{
					bCheckShootSuccessRate = (fShootSuccessRate != iValue);
					if(FALSE == bCheckShootSuccessRate)
						break;
				}
				else if(PROPERTY_BLOCK_SUCCESS == iProperty)
				{
					bCheckBlockSuccessRate = (fBlockSuccessRate != iValue);
					if(FALSE == bCheckBlockSuccessRate)
						break;
				}
			}

			delete pSpecialPartsItemEquipInfo;
		}
	}

	return (bCheckShootSuccessRate == TRUE && bCheckBlockSuccessRate == TRUE);
}

void CFSGameUser::GetSpecialPartsOptionProperty(float& fShootSuccessRate, float& fBlockSuccessRate)
{
	fShootSuccessRate = 0;
	fBlockSuccessRate = 0;

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iUseStyle = GetUseStyleIndex();

	map<int, SSpecialPartsItemEquipInfo*> mapSpecialPartsItemEquipInfo;
	pAvatarItemList->GetSpecialPartsItemEquipList(mapSpecialPartsItemEquipInfo);

	map<int, SSpecialPartsItemEquipInfo*>::iterator itr = mapSpecialPartsItemEquipInfo.begin();
	map<int, SSpecialPartsItemEquipInfo*>::iterator itrEnd = mapSpecialPartsItemEquipInfo.end();
	for( ; itr != itrEnd; ++itr)
	{
		SSpecialPartsItemEquipInfo* pSpecialPartsItemEquipInfo = itr->second;
		if(pSpecialPartsItemEquipInfo)
		{
			int iSpecialPartsIndex = pSpecialPartsItemEquipInfo->iSpecialPartsIndex;
			BYTE btEquipCount = 0;

			for(int i = 0; i < MAX_SPECIAL_PARTS_ITEM_COUNT; ++i)
			{
				if(pSpecialPartsItemEquipInfo->iItemStatus[i] == ITEM_STATUS_WEAR)
				{
					++btEquipCount;
				}
			}

			SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
			vector<SSpecialPartsPropertyList> vecPropertyList;
			SPECIALPARTS.GetSpecialPartsOptionOnPropertyList(sKey, vecPropertyList);

			for(int i = 0; i < vecPropertyList.size(); ++i)
			{
				int iProperty = PROPERTY_NONE;
				int iValue = 0;
				SPECIALPARTS.GetSpecialPartsPropertyConfig(vecPropertyList[i].iPartsPropertyIndex, iProperty, iValue);

				switch(iProperty)				
				{
				case PROPERTY_SHOOT_SUCCESS:	fShootSuccessRate += static_cast<float>(iValue);	break;
				case PROPERTY_BLOCK_SUCCESS:	fBlockSuccessRate += static_cast<float>(iValue);	break;
				}
			}

			delete pSpecialPartsItemEquipInfo;
		}
	}
}

void CFSGameUser::GetSpecialPartsOptionProperty(SG2M_ADD_STAT_UPDATE& sInfo)
{
	sInfo.fSpecialPartsShootSuccessRate = 0.0f;
	sInfo.fSpecialPartsBlockSuccessRate = 0.0f;
	sInfo.iExpWinSpecialPartsBonusRate = 0;
	sInfo.iWinSpecialPartsBonusRate = 0;
	sInfo.iExpLoseSpecialPartsBonusRate = 0; 
	sInfo.iLoseSpecialPartsBonusRate = 0;

	CAvatarItemList* pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iUseStyle = GetUseStyleIndex();

	map<int, SSpecialPartsItemEquipInfo*> mapSpecialPartsItemEquipInfo;
	pAvatarItemList->GetSpecialPartsItemEquipList(mapSpecialPartsItemEquipInfo);

	map<int, SSpecialPartsItemEquipInfo*>::iterator itr = mapSpecialPartsItemEquipInfo.begin();
	map<int, SSpecialPartsItemEquipInfo*>::iterator itrEnd = mapSpecialPartsItemEquipInfo.end();
	for( ; itr != itrEnd; ++itr)
	{
		SSpecialPartsItemEquipInfo* pSpecialPartsItemEquipInfo = itr->second;
		if(pSpecialPartsItemEquipInfo)
		{
			int iSpecialPartsIndex = pSpecialPartsItemEquipInfo->iSpecialPartsIndex;
			BYTE btEquipCount = 0;

			for(int i = 0; i < MAX_SPECIAL_PARTS_ITEM_COUNT; ++i)
			{
				if(pSpecialPartsItemEquipInfo->iItemStatus[i] == ITEM_STATUS_WEAR)
				{
					++btEquipCount;
				}
			}

			SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
			vector<SSpecialPartsPropertyList> vecPropertyList;
			SPECIALPARTS.GetSpecialPartsOptionOnPropertyList(sKey, vecPropertyList);

			for(int i = 0; i < vecPropertyList.size(); ++i)
			{
				int iProperty = PROPERTY_NONE;
				int iValue = 0;
				SPECIALPARTS.GetSpecialPartsPropertyConfig(vecPropertyList[i].iPartsPropertyIndex, iProperty, iValue);

				switch(iProperty)				
				{
				case PROPERTY_SHOOT_SUCCESS:	sInfo.fSpecialPartsShootSuccessRate += static_cast<float>(iValue);	break;
				case PROPERTY_BLOCK_SUCCESS:	sInfo.fSpecialPartsBlockSuccessRate += static_cast<float>(iValue);	break;
				case PROPERTY_FAME_POINT:		sInfo.iFameSpecialIpartsBonusRate += iValue;	break;
				case PROPERTY_WIN_EXP:			sInfo.iExpWinSpecialPartsBonusRate += iValue;	break;
				case PROPERTY_WIN_POINT:		sInfo.iWinSpecialPartsBonusRate += iValue;		break;
				case PROPERTY_LOSE_EXP:			sInfo.iExpLoseSpecialPartsBonusRate += iValue;	break;
				case PROPERTY_LOSE_POINT:		sInfo.iLoseSpecialPartsBonusRate += iValue;		break;
				}
			}

			delete pSpecialPartsItemEquipInfo;
		}
	}
}

void CFSGameUser::CheckFullCrtUniform()
{
	//������ �Ⱦ���� �ߴ�
	//if( false == m_UserItem->CheckGetBasicFullCrtUniform())
	//{
	//	m_UserItem->InsertBasicFullCrtUniform();
	//}
	
	//�۾���� - 5vs5 uniform
	//CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	//if( NULL == pAvatarItemList ) return;
	//
	//SUserItemInfo * pUniform = pAvatarItemList->GetItemWithItemCodeInAll(FULL_COURT_BASIC_UNIFORM_ITEMCODE);
	//if( pUniform == NULL ) return;
	//
	//m_UserItem->SetUniformCode( pUniform->iItemCode );
}

void CFSGameUser::RemovePCRoomBenefits()
{
	if (PCROOM_KIND_PREMIUM != GetPCRoomKind()) return;

	SAvatarInfo * pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	//SkillSlot
	pAvatarInfo->Skill.iPCBangBonusSlot = 0;
	m_UserSkill->CheckSlotOverSkill();

	//Item
	m_UserItem->RemovePCRoomItem();

	SetPCRoomKind(PCROOM_KIND_NOT);

	SetExpirePremiumPCRoom(FALSE);

	//CheckSpecialPartsUserProperty();
}


void CFSGameUser::LoadPCRoomItemList(BOOL bRemoveItem)
{
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	CPCRoomItemList *pItemList = pItemShop->GetPCRoomItemList();
	CHECK_NULL_POINTER_VOID(pItemList);
	
	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	int iMaxItemIdx = -11;

	CPCRoomItemList::PCRoomItemMap* pPCRoomItemMap = pItemList->GetPCRoomItemMap();
	CHECK_NULL_POINTER_VOID(pPCRoomItemMap);

	if( TRUE == bRemoveItem )
	{
		if( FALSE == m_bFirstChangePCRoomItem )
		{
			m_UserItem->RemovePCRoomItem(pAvatar->iSex);
		
			m_bFirstChangePCRoomItem = TRUE;

			if( TRUE == m_bCloneCharacter )
			{
				const int cnPCRoomAvatarMaxItemCount = 3;
				if( pAvatar->iSex == ITEM_INFO_SEX_WOMAN )
				{
					m_iChangePCRoomDeffCount = cnPCRoomAvatarMaxItemCount;
				}
				else
				{
					m_iChangePCRoomDeffCount = -cnPCRoomAvatarMaxItemCount;
				}
			}
		}
		else
		{
			for (CPCRoomItemList::PCRoomItemMap::iterator iter = pPCRoomItemMap->begin()
				; iter != pPCRoomItemMap->end(); ++iter)
			{
				SPCRoomItem* pItem = &iter->second;
				if( NULL == pItem )
					continue;

				if (GetPCRoomKind() != pItem->iPCRoomKind)
					continue;

				if( NEW_FEMALE_CHARACTER_TYPE == pAvatar->iCharacterType && 
					PCROOMNEWCHAR_NOT_USED == pItem->btNewCharUse )
					continue;

				if( ITEM_INFO_SEX_COMMON == pItem->sSex || pItem->sSex == pAvatar->iSex )
				{
					SUserItemInfo* pItemInfo = pAvatarItemList->GetItemWithOnlyItemCode(pItem->iItemCode);
					if( NULL == pItemInfo ) continue;

					pAvatarItemList->ChangeItemStatus(pItemInfo, ITEM_STATUS_INVENTORY, 0, FALSE);	
				}
				else
				{
					SUserItemInfo* pItemInfo = pAvatarItemList->GetItemWithOnlyItemCode(pItem->iItemCode);
					if( NULL == pItemInfo ) continue;

					pAvatarItemList->ChangeItemStatus(pItemInfo, ITEM_STATUS_RESELL, 0, FALSE);
				}
			}

			

			return;
		}
	}

	CPCRoomItemList::PCRoomItemMap::iterator iter = pPCRoomItemMap->begin();
	for (; iter != pPCRoomItemMap->end(); ++iter)
	{
		SPCRoomItem* pItem = &iter->second;
		if( NULL == pItem )
			continue;

		if (ITEM_INFO_SEX_COMMON != pItem->sSex && pItem->sSex != pAvatar->iSex)
			continue;

		if (GetPCRoomKind() != pItem->iPCRoomKind)
			continue;

		if( NEW_FEMALE_CHARACTER_TYPE == pAvatar->iCharacterType && 
			PCROOMNEWCHAR_NOT_USED == pItem->btNewCharUse )
			continue;

		SUserItemInfo* pTempItemInfo = pAvatarItemList->GetItemWithOnlyItemCode(pItem->iItemCode);
		if( NULL !=  pTempItemInfo && ITEM_INFO_SEX_COMMON == pItem->sSex )
		{
			pAvatarItemList->IncreaseEtchKindItemCount(ITEM_SMALL_KIND_PCROOM_PREMIUM);
			pAvatarItemList->ChangeItemStatus(pTempItemInfo, ITEM_STATUS_INVENTORY, 0, FALSE);	
			continue;
		}

		SUserItemInfo UserItemInfo;
		UserItemInfo.iItemIdx = iMaxItemIdx--;
		UserItemInfo.iItemCode = pItem->iItemCode;
				
		if (PCROOM_KIND_NORMAL == pItem->iPCRoomKind)
			UserItemInfo.iSmallKind = ITEM_SMALL_KIND_PCROOM_NORMAL;
		else if (PCROOM_KIND_PREMIUM == pItem->iPCRoomKind)
			UserItemInfo.iSmallKind = ITEM_SMALL_KIND_PCROOM_PREMIUM;
				
		UserItemInfo.iSellType = SELL_TYPE_POINT;
		UserItemInfo.iChannel = pItem->iChannel;
		UserItemInfo.iSexCondition = pItem->sSex;
		UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;

		if(GetUserItemList()->CheckUseItem(UserItemInfo.iItemCode))
		{
			UserItemInfo.iStatus = ITEM_STATUS_WEAR;
		}

		UserItemInfo.iPropertyType = ITEM_PROPERTY_NORMAL;
		UserItemInfo.iPropertyTypeValue = -1;
		UserItemInfo.iPropertyKind = pItem->iPropertyKind;
				
		if( !( UserItemInfo.iPropertyKind >= MIN_PREMIUM_STAT_OPTION_ITEM_NUM && UserItemInfo.iPropertyKind <= MAX_PREMIUM_STAT_OPTION_ITEM_NUM ) )
		{
			CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList( pItem->iPropertyKind );
			CHECK_NULL_POINTER_VOID(pItemPropertyBoxList);
				
			int iPropertyIndex = pItemPropertyBoxList->GetPropertyIndexByLv( pAvatar->iLv );
			CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList( iPropertyIndex );
			CHECK_NULL_POINTER_VOID(pItemPropertyList);
				
			SItemProperty* pItemProperty = pItemPropertyList->GetItemProperty(0);
			CHECK_NULL_POINTER_VOID(pItemProperty);
				
			SUserItemProperty tempUserItemProperty;
			tempUserItemProperty.iGameIDIndex	= pAvatar->iGameIDIndex;
			tempUserItemProperty.iItemIdx		= UserItemInfo.iItemIdx;
			tempUserItemProperty.iProperty		= pItemProperty->iProperty;
			tempUserItemProperty.iValue			= pItemProperty->iValue;
				
			UserItemInfo.iPropertyNum = 1;
			pAvatarItemList->AddUserItemProperty( tempUserItemProperty );
		}
		
		m_UserItem->InsertNewItem(UserItemInfo);
	}
}

void CFSGameUser::UpdatePCRoomItemStat(int iLv)
{
	m_UserItem->UpdatePCRoomItemStat(iLv);
}

BOOL CFSGameUser::EventCheckPCRoomKind(int iParam[])
{
	int i_PCRoom_Kind = iParam[EVENT_CODE_CHECK_PCROOM_KIND];

	if(i_PCRoom_Kind == GetPCRoomKind())
	{
		return TRUE;
	}

	return FALSE;
}

BOOL CFSGameUser::EventLoadEventUserStep(int iParam[])
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();

	if (NULL != pODBC)
	{
		CCompEventUserData* pEventUserData = GetEventUserData();

		if (NULL != pEventUserData)
		{
			int iStep = 0;

			if(ODBC_RETURN_SUCCESS == pODBC->EVENT_CheckEventStep(iParam[EVENT_CODE_LOAD_EVENT_USER_STEP_TARGET_AVARTAR], GetEvent()->GetEventID(), GetUserIDIndex(), GetGameIDIndex(), iStep))
			{
				pEventUserData->SetUserEventStep(iStep);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CFSGameUser::EventInsertEventUserStep(int iParam[])
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();
	CCompEventUserData* pEventUserData = GetEventUserData();

	if (NULL != pODBC)
	{
		int iResult = 0;

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_InsertEventStep(iParam[0],GetEvent()->GetEventID(), GetUserIDIndex(), GetGameIDIndex(), pEventUserData->GetUserEventStep()-1, iResult))
		{	
			return TRUE;
		}
	}

	return FALSE;
}

DEFINE_EVENT_FUNCTION(FSGameUser,EVENT_JUMPING_EVENT_NOTICE)
{
	CCompEventUserData* pEventUserData = GetEventUserData();

	if (NULL != pEventUserData)
	{
		SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
		CHECK_NULL_POINTER_BOOL(pAvatarInfo);

		SS2C_JUMPING_EVENT_NOTICE rs;
		rs.iEventIndex = GetEvent()->GetEventID();
		rs.iMaxCount = GetEvent()->GetEventStepMax();
		rs.iCurrentCount = pEventUserData->GetUserEventStep()-1;
		rs.iUseableLv = iParam[EVENT_CODE_JUMPING_LV];
		rs.iStartMMDDHHMM = iParam[EVENT_CODE_JUMPING_NOTICE_START_MMDDHHMM];
		rs.iEndMMDDHHMM = iParam[EVENT_CODE_JUMPING_NOTICE_END_MMDDHHMM];
		Send(S2C_JUMPING_EVENT_NOTICE, &rs, sizeof(SS2C_JUMPING_EVENT_NOTICE));

		return TRUE;
	}

	return FALSE;
}

DEFINE_EVENT_FUNCTION(FSGameUser,EVENT_JUMPING)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();

	if (NULL != pODBC)
	{
		CCompEventUserData* pEventUserData = GetEventUserData();

		if (NULL != pEventUserData)
		{
			SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
			CHECK_NULL_POINTER_BOOL(pAvatarInfo);

			int iLv = iParam[EVENT_CODE_JUMPING_LV];
			int iExp = CFSGameServer::GetInstance()->GetLvUpExp(iLv);
			int iTrainingLv = iParam[EVENT_CODE_JUMPING_TRAINING_LV];
			int iSkillSlotCount = iParam[EVENT_CODE_JUMPING_SKILL_SLOT_COUNT];
			int iPoint = iParam[EVENT_CODE_JUMPING_POINT];

			CHECK_CONDITION_RETURN(pAvatarInfo->iLv >= iLv, FALSE);

			if(ODBC_RETURN_SUCCESS != pODBC->EVENT_Jumping(pEventUserData->GetEventID(), GetUserIDIndex(), GetGameIDIndex(), pEventUserData->GetUserEventStep()-1, iLv, iExp, iTrainingLv, iSkillSlotCount, iPoint))
				return FALSE;

			pAvatarInfo->iLv = iLv;
			pAvatarInfo->iExp = iExp;

			if(ODBC_RETURN_SUCCESS != pODBC->spGetAvatarTrainingInfo(pAvatarInfo))
				return FALSE;

			if(ODBC_RETURN_SUCCESS != pODBC->spGetAvatarStatus(pAvatarInfo))
				return FALSE;

			if(ODBC_RETURN_SUCCESS != pODBC->spGetUserSkillSlot(GetUserIDIndex(), GetGameIDIndex(), pAvatarInfo->Skill))	
				return FALSE;

			if(TRUE != ProcessStatUp(pGameODBC))
				return FALSE;
			
			vector<int> vLevel;
			vLevel.push_back(GetCurUsedAvtarLv());
			ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_LEVEL, ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vLevel);
				
			GetUserSkill()->ProcessAchievementBuyTraining();

			SetSkillPoint(GetSkillPoint()+iPoint);

			SS2C_JUMPING_EVENT_COMPLET rs;
			rs.iPoint = iPoint;
			Send(S2C_JUMPING_EVENT_COMPLET, &rs, sizeof(SS2C_JUMPING_EVENT_COMPLET));

			SendAvatarInfo();
			CheckPositionSelect();

			return TRUE;
		}
	}

	return FALSE;
}

void CFSGameUser::SendSkillTakeEnable(int iSkillNo/* = SKILL_USE_DIRECT_PASS*/)
{
	//���� ���� Ȱ��/��Ȱ�� �˸������� �����
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	bool bEnable = FALSE;
	for( int i = 0; i < MAX_SKILL_SLOT; ++i )
	{
		if( iSkillNo == pAvatar->Skill.iaUseSkill[i] )
		{
			bEnable = TRUE;
			break;
		}
	}

	CPacketComposer PacketComposer(S2C_SKILL_TAKE_ENABLE);
	PacketComposer.Add( iSkillNo );
	PacketComposer.Add( bEnable );
	Send(&PacketComposer);
}

void CFSGameUser::GetAvatarRankInfo(BYTE _btRecordType, int _iRankType, int _iCategory, int& iRank, int& iValue)
{
	iRank = -1;
	iValue = 0;

	CHECK_CONDITION_RETURN_VOID(MAX_RECORD_TYPE <= _btRecordType);
	CHECK_CONDITION_RETURN_VOID(MAX_RANK_TYPE_COUNT <= _iRankType);

	if( RANK_TYPE_SEASON == _iRankType)
	{
		CHECK_CONDITION_RETURN_VOID(_iCategory < 0 || _iCategory >= MAX_SEASON_RANK_NUM);

		AvatarSeasonInfoMap::iterator pos = m_AvatarSeasonInfo.mapSeasonInfo[_btRecordType].find(GetCurrentSeasonIndex());
		if(pos != m_AvatarSeasonInfo.mapSeasonInfo[_btRecordType].end())
		{
			SAvatarSeasonInfoEntry* pEntry = &pos->second;
			iRank	= pEntry->Rank[_iCategory].iRank;
			iValue	= pEntry->Rank[_iCategory].iRecordValue;
		}
	}
	else if( RANK_TYPE_TOTAL == _iRankType)
	{
		CHECK_CONDITION_RETURN_VOID(_iCategory < 0 || _iCategory >= MAX_TOTAL_RANK_NUM);
		SAvatarInfo* pAvatar = GetCurUsedAvatar();
		CHECK_NULL_POINTER_VOID(pAvatar);

		iRank	= pAvatar->TotalInfo[_btRecordType].Rank[_iCategory].iRank;
		iValue	= pAvatar->TotalInfo[_btRecordType].Rank[_iCategory].iRecordValue;
	}
}

void CFSGameUser::MakePacketForCoachCardSubActionInfluence(CPacketComposer& Packet, int iProductIndex)
{
	SUserSInfo1 info;
	MakeDataForCoachCardSubActionInfluence(info.Buffer, info.iDataSize, iProductIndex);

	if (MAX_USER_INFO_RANK_DATA <= info.iDataSize)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakeDataForCoachCardSubActionInfluence - DataSize overflow:11866902, GameIDIndex:0", info.iDataSize, GetGameIDIndex());
urn;
	}

	Packet.Add(info.Buffer, info.iDataSize);
}

void CFSGameUser::MakeDataForCoachCardSubActionInfluence(PBYTE pBuffer, int& iSize, int iProductIndex)
{
	CActionInfluenceList* pActionInfluence = COACHCARD.GetActionInfluenceList();
	CHECK_NULL_POINTER_VOID(pActionInfluence);

	// set Product-ActionInfluence information
	vector<SActionInfluenceConfig> vecSubActionInfluenceConfig;
	vector<int> vecSubActionInfluence;
	GetInventorySubActionInfluence( iProductIndex, vecSubActionInfluence );

	int iSubEffectSize = vecSubActionInfluence.size();
	for( int i = 0; i < iSubEffectSize; ++i )
	{
		SActionInfluenceConfig sActionInfluenceConfig;
		pActionInfluence->FindAndGetActionInfluenceConfig( vecSubActionInfluence[i], sActionInfluenceConfig );
		// Set Product-SubActionInfuence List push
		vecSubActionInfluenceConfig.push_back( sActionInfluenceConfig );
	}

	int iTendencyType = GetInventoryTendencyType( iProductIndex );
	int iSubEffectConfigSize = vecSubActionInfluenceConfig.size();
	
	memcpy(pBuffer+iSize, &iProductIndex, sizeof(int));
	iSize += sizeof(int);
	memcpy(pBuffer+iSize, &iTendencyType, sizeof(int));
	iSize += sizeof(int);
	memcpy(pBuffer+iSize, &iSubEffectConfigSize, sizeof(int));
	iSize += sizeof(int);
	
	for( int i = 0; i < iSubEffectConfigSize; ++i )
	{
		memcpy(pBuffer+iSize, &vecSubActionInfluenceConfig[i].iGradeLevel, sizeof(int));
		iSize += sizeof(int);
		memcpy(pBuffer+iSize, &vecSubActionInfluenceConfig[i].iActionType, sizeof(int));
		iSize += sizeof(int);
		memcpy(pBuffer+iSize, &vecSubActionInfluenceConfig[i].iInfluenceType, sizeof(int));
		iSize += sizeof(int);
	}
}

void CFSGameUser::MakePacketForCoachCardInfo(CPacketComposer& Packet, int iProductIndex)
{
	SUserSInfo1 info;
	MakeDataForCoachCardInfo(info.Buffer, info.iDataSize, iProductIndex);

	if (MAX_USER_INFO_RANK_DATA <= info.iDataSize)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakePacketForCoachCardInfo - DataSize overflow:11866902, GameIDIndex:0", info.iDataSize, GetGameIDIndex());
urn;
	}

	Packet.Add(info.Buffer, info.iDataSize);
}

void CFSGameUser::MakeDataForCoachCardInfo(PBYTE pBuffer, int& iSize, int iProductIndex)
{
	CActionInfluenceList* pActionInfluence = COACHCARD.GetActionInfluenceList();
	CHECK_NULL_POINTER_VOID(pActionInfluence);

	// set Product-ActionInfluence information
	CProduct* pProduct = GetInventoryProduct(iProductIndex);
	CHECK_NULL_POINTER_VOID(pProduct);

	SActionInfluenceConfig sActionInfluenceConfig;
	int iaMainActionInfluence[MAX_POTENCARD_MAIN_INFLUENCE_COUNT] = {0,};
	pProduct->GetActionInfluenceIndex( iaMainActionInfluence );

	// set Product-data information
	int iMinute= pProduct->GetRemainTerm() / 60;
	int iRarityLevel = pProduct->GetRarityLevel();
	int iGradeLevel = pProduct->GetGradeLevel();
	int iTendencyType = pProduct->GetTendencyType();
	BOOL bSpecialCardIndex = static_cast<BOOL>(pProduct->GetSpecialCardIndex() == SPECIALCARD_INDEX_THREEKINGDOM);
	POTENTIAL_COMPONENT eComponentIndex = pProduct->GetPotentialComponentIndex();

	ADD_DATA(pBuffer, iSize, iProductIndex);
	ADD_DATA(pBuffer, iSize, iRarityLevel);
	ADD_DATA(pBuffer, iSize, iGradeLevel);
	ADD_DATA(pBuffer, iSize, iTendencyType);
	ADD_DATA(pBuffer, iSize, iMinute);
	ADD_DATA(pBuffer, iSize, bSpecialCardIndex); // Client �� BOOL  ������ �ﱹ���� �����ϰ� ����

	BYTE btMainInfluenceCount = 0;
	PBYTE pCountLocation = pBuffer + iSize;
	ADD_DATA(pBuffer, iSize, btMainInfluenceCount);
	for (int i = 0; i < MAX_POTENCARD_MAIN_INFLUENCE_COUNT; ++i)
	{
		if (0 >= iaMainActionInfluence[i])	
			continue;

		pActionInfluence->FindAndGetActionInfluenceConfig(iaMainActionInfluence[i],sActionInfluenceConfig);
		ADD_DATA(pBuffer, iSize, sActionInfluenceConfig.iActionType);
		ADD_DATA(pBuffer, iSize, sActionInfluenceConfig.iInfluenceType);
		++btMainInfluenceCount;
	}
	memcpy(pCountLocation, &btMainInfluenceCount, sizeof(BYTE));

	ADD_DATA(pBuffer, iSize, eComponentIndex);

}

BOOL CFSGameUser::MakePacketForUserInfoCoachCard(CPacketComposer& Packet, int iCoachCardProductIndex, int iCoachCardFront)
{
	SUserSInfo1 info;
	MakeDataForUserInfoCoachCard(info.Buffer, info.iDataSize, iCoachCardProductIndex, iCoachCardFront);

	if (MAX_USER_INFO_RANK_DATA <= info.iDataSize)
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, NONE, "WARNING!!! MakePacketForUserInfoCoachCard - DataSize overflow:11866902, GameIDIndex:0", info.iDataSize, GetGameIDIndex());
urn FALSE;
	}

	Packet.Add(info.Buffer, info.iDataSize);

	return TRUE;
}

void CFSGameUser::MakeDataForUserInfoCoachCard(PBYTE pBuffer, int& iSize, int iCoachCardProductIndex, int iCoachCardFront)
{
	if (iCoachCardFront == 0)
	{ 
		memcpy(pBuffer+iSize, &iCoachCardFront, sizeof(int));
		iSize += sizeof(int);
		MakeDataForCoachCardSubActionInfluence(pBuffer, iSize, iCoachCardProductIndex);

		// packet add Potential-0rarity
		SPotentialComponent sPotentialInfo;
		GetPotentialComponent( iCoachCardProductIndex, sPotentialInfo );
		memcpy(pBuffer+iSize, &sPotentialInfo.eComponentIndex, sizeof(int));
		iSize += sizeof(int);
	}
	else if(iCoachCardFront == 1)
	{
		memcpy(pBuffer+iSize, &iCoachCardFront, sizeof(int));
		iSize += sizeof(int);
		MakeDataForCoachCardInfo(pBuffer, iSize, iCoachCardProductIndex);
	}
}

BOOL CFSGameUser::BuyCoachCardItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_NULL_POINTER_BOOL(pBillingInfo);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC);

	CHECK_CONDITION_RETURN(MAX_COACH_CARD_SHOP_INVENTORY_ITEMCOUNT <= GetProductInventoryCount(), FALSE);

	const int iSellType = (pBillingInfo->iCashChange > 0) ? SELL_TYPE_CASH : SELL_TYPE_TROPHY;
	const int iCost = (pBillingInfo->iCashChange > 0) ? pBillingInfo->iCashChange : pBillingInfo->iTrophyChange;
	const int iItemIDNumber = pBillingInfo->iParam0;
	const int iAddProductCount = pBillingInfo->iParam1;
	const int iItemType = COACHCARD.GetItemType( iItemIDNumber );

	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;

	int iInventoryIndex = INVALID_IDINDEX;
	int iProductIndex = INVALID_IDINDEX;
	int iTendencyType = TENDENCY_TYPE_NONE;
	int iGradeLevel = 0;
	SDateInfo sProductionDate, sExpireDate;
	int iNewInventoryIndex = INVALID_IDINDEX;
	int iNewProductIndex = INVALID_IDINDEX;
	int iNewProductCount = 0;

	vector<int> vecGradeLevel;
	COACHCARD.GetGradeLevel(iItemIDNumber, vecGradeLevel);

	if (1 < vecGradeLevel.size() || 0 == vecGradeLevel.size())	
		iGradeLevel = RANDOMCARD_GRADE_LEVEL_ALL;
	else
		iGradeLevel = vecGradeLevel[0];

	GetOwnedProductItemInfo(iItemIDNumber, iProductIndex, iInventoryIndex);
	int iSpecialCardIndex = COACHCARD.GetSpecialCardIndex(iItemIDNumber);

	int iRarityLevel = RARITY_LEVEL_NONE;

	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			if( ODBC_RETURN_SUCCESS != pGameODBC->ITEM_BuyCoachCardShopItem(GetUserIDIndex(), GetGameIDIndex(), iItemIDNumber, iTendencyType, iGradeLevel,iSellType, 
								iItemType, iCost, iProductIndex, iInventoryIndex, iAddProductCount, iNewProductIndex, iNewInventoryIndex, iNewProductCount, sProductionDate, sExpireDate, iSpecialCardIndex, iRarityLevel, iPrevCash, iPostCash))
			{
				return FALSE;
			}
		}
		break;
	default:
		{
			return FALSE;
		}
		break;
	}

	SProductData ProductData;
	ProductData.iInventoryIndex = iNewInventoryIndex;
	ProductData.iProductIndex = iNewProductIndex;
	ProductData.iItemIDNumber =iItemIDNumber;
	ProductData.iItemType = iItemType;
	ProductData.iTendencyType = iTendencyType;
	ProductData.iGradeLevel = iGradeLevel;
	ProductData.sProductionDate = sProductionDate;
	ProductData.sExpireDate = sExpireDate;
	ProductData.iProductCount = iNewProductCount;
	ProductData.iSpecialCardIndex = iSpecialCardIndex;
	ProductData.iRarityLevel = iRarityLevel;
	AddProductToInventory(ProductData);

	SubtractCapital(iSellType, iCost);
	SendCurAvatarTrophy();

	SS2C_RANDOM_COACH_CARD_INFO_NOT info;
	info.iProductIndex = iNewProductIndex;
	info.iProductCount = iNewProductCount;
	info.bThreeKingdom = static_cast<BOOL>(iSpecialCardIndex == SPECIALCARD_INDEX_THREEKINGDOM);
	Send(S2C_RANDOM_COACH_CARD_INFO_NOT, &info, sizeof(SS2C_RANDOM_COACH_CARD_INFO_NOT));

	if(SELL_TYPE_CASH == iSellType)
	{
		SetUserBillResultAtMem(iSellType, iPostCash, pBillingInfo->iPointChange, 0, 0);
		SendUserGold();
	}

	return TRUE;	
}

void CFSGameUser::AddPointAchievements()
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	vector<int> vNewlyReachedAchievementIndex;
	vector<int> vAnnounceAchievementIndex;
	vector<int> vPoint;

	vPoint.push_back(GetSkillPoint());

	if(TRUE == ProcessAchievementbyGroup(ACHIEVEMENT_CONDITION_GROUP_POINT, ACHIEVEMENT_LOG_GROUP_SHOP, vPoint, vNewlyReachedAchievementIndex))
	{
		if (TRUE == ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_SHOP, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex))
		{
			SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
			info.iGameIDIndex = GetGameIDIndex();

			strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), _countof(info.szGameID)-1);
			info.iAchievementCount = vAnnounceAchievementIndex.size();

			for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
			{
				info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
			}

			CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
			PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

			CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
			if (pMatch != NULL)
			{
				pMatch->Send(&PacketComposer);
			}	
		}
	}
}

void CFSGameUser::AddFriendCountAchievements()
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	vector<int> vNewlyReachedAchievementIndex;
	vector<int> vAnnounceAchievementIndex;
	vector<int> vFriendCounts;

	vFriendCounts.push_back(GetFriendCount());

	if(TRUE == ProcessAchievementbyGroup(ACHIEVEMENT_CONDITION_GROUP_FRIEND_COUNT, ACHIEVEMENT_LOG_GROUP_COMMUNATION, vFriendCounts, vNewlyReachedAchievementIndex))
	{
		if (TRUE == ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_COMMUNATION, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex))
		{
			SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
			info.iGameIDIndex = GetGameIDIndex();

			strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), _countof(info.szGameID)-1);
			info.iAchievementCount = vAnnounceAchievementIndex.size();

			for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
			{
				info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
			}

			CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
			PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

			CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
			if (pMatch != NULL)
			{
				pMatch->Send(&PacketComposer);
			}				
		}
	}
}

int CFSGameUser::GetFriendCount()
{
	return m_FriendList.GetFriendCount();
}

void CFSGameUser::AddTotalRankingAchievements()
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SAvatarInfo *pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	vector<int> vNewlyReachedAchievementIndex;
	vector<int> vAchievementParameter;

	int iAchievementCode	= ACHIEVEMENT_CODE_TOTAL_RANK_POINT;
	int iMaxAchievementCode = ACHIEVEMENT_CODE_TOTAL_RANK_POINT+ACHIEVEMENT_RECORDTYPE_TOTAL_RANK_COUNT;

	for(int iRecord = TOTAL_RECORD_POINT2; iRecord < MAX_TOTAL_RECORD; iRecord++)
	{
		vAchievementParameter.clear();
		vAchievementParameter.push_back(-1);

		if(iRecord == TOTAL_RECORD_POINT2)
		{
			vAchievementParameter.push_back(pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[iRecord]+pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[iRecord+1]);
		}
		else
		{
			vAchievementParameter.push_back(pAvatarInfo->TotalInfo[RECORD_TYPE_HALFCOURT].Record[iRecord]);
		}

		for(int i = iAchievementCode; i < iMaxAchievementCode; i++)
		{
			CACHIEVEMENT_LOG_GROUP_MAP::const_iterator cIterMap = m_mapAchievementLogGroup.find(ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO);
			if(cIterMap != m_mapAchievementLogGroup.end())
			{
				if(TRUE == cIterMap->second->FindAchievementLogbyAchievementIndex(i))
					continue;
			}

			if(ACHIEVEMENTMANAGER.CheckCompleteAchievementbyAchievementIndex(i, vAchievementParameter) == TRUE)
			{
				vNewlyReachedAchievementIndex.push_back(i);
			}
		}

		iAchievementCode += 100;
		iMaxAchievementCode += 100;
	}

	vector<int> vAnnounceAchievementIndex;
	if (TRUE == ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex))
	{
		SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
		info.iGameIDIndex = GetGameIDIndex();

		strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), _countof(info.szGameID)-1);
		info.iAchievementCount = vAnnounceAchievementIndex.size();

		for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
		{
			info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
		}

		CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
		PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
		if (pMatch != NULL)
		{
			pMatch->Send(&PacketComposer);
		}	
	}
}

void CFSGameUser::AddRevengeMatchingWinAchievements()
{
	if(AddRevengeMatchingWinCnt())
	{
		vector<int> vParameters;
		vParameters.push_back(m_iRevengeMatchingWinCnt);
		ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_REVENGE_MATCHING_WIN, ACHIEVEMENT_LOG_GROUP_GAMEPLAY, vParameters);
	}
}

int CFSGameUser::GetRevengeMatchingWinCnt()
{
	if(-1 == m_iRevengeMatchingWinCnt && FALSE == LoadRevengeMatchingWinCnt())
	{
		return 0;
	}

	return m_iRevengeMatchingWinCnt;
}

BOOL CFSGameUser::AddRevengeMatchingWinCnt(int iAddCnt/* = 1*/)
{
	if(iAddCnt < 0)
		return FALSE;

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	return (ODBC_RETURN_SUCCESS == pODBC->AVATAR_AddUserRevengeMatchingInfo(GetGameIDIndex(), iAddCnt, m_iRevengeMatchingWinCnt));
}

BOOL CFSGameUser::LoadRevengeMatchingWinCnt()
{
	return AddRevengeMatchingWinCnt(0);
}

void CFSGameUser::SetGuaranteeCardInfo( SGuaranteeInfo sInfo )
{
	memcpy(&m_sGuaranteeCard, &sInfo, sizeof(SGuaranteeInfo));
}

int CFSGameUser::GetGuaranteeCardValue( int iCategory )
{
	int iValue = GUARANTEECARD_NOGOT;

	switch( iCategory-1 )
	{
	case GF_PLATINUM :	iValue = m_sGuaranteeCard.iPlatinum;		break;
	case GF_GOLD	:	iValue	= m_sGuaranteeCard.iGold;			break;
	case GF_SILVER	:	iValue = m_sGuaranteeCard.iSilver;		break;
	case GF_NORMAL	:	iValue = m_sGuaranteeCard.iNormal;		break;
	case GF_EVENT	:	iValue = m_sGuaranteeCard.iEvent;		break;
	case GF_DIAMOND :	iValue = m_sGuaranteeCard.iDiamond;		break;
	case GF_BALLBACKGROUND:	iValue = m_sGuaranteeCard.iBallBackground;	break;
	case GF_BALLPATTERN:	iValue = m_sGuaranteeCard.iBallPattern;		break;
	case GF_BALLEFFECT:		iValue = m_sGuaranteeCard.iBallEffect;		break;
	default:	iValue = GUARANTEECARD_NOGOT; break;
	}

	return iValue;
}

BOOL CFSGameUser::GiveClubTournamentVoteUserBenefitItem(int iItemCode, int iItemIdx, int iUpdatedPropertyValue)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo ItemInfo;
	if (FALSE == pItemShop->GetItem(iItemCode, ItemInfo))
	{
		return FALSE;
	}

	CHECK_CONDITION_RETURN(ITEM_PROPERTY_EXHAUST != ItemInfo.iPropertyType, FALSE);

	SUserItemInfo UserItemInfo;
	UserItemInfo.SetBaseInfo(ItemInfo);

	UserItemInfo.iItemCode = iItemCode;
	UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;
	UserItemInfo.iSellType = REFUND_TYPE_POINT;
	UserItemInfo.iItemIdx = iItemIdx;
	UserItemInfo.iPropertyTypeValue = iUpdatedPropertyValue;
	UserItemInfo.iPropertyNum = 0;

	GetAvatarItemList()->UpdateExhaustItemPropertyValue(UserItemInfo);

	return TRUE;
}

BOOL CFSGameUser::LoadAvatarSecretStore( void )
{
	CHECK_CONDITION_RETURN(FALSE == SECRETSTOREMANAGER.GetSecretStoreEnable(),FALSE);
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if( ODBC_RETURN_SUCCESS != pODBC->SECRET_GetAvatarSecretshop(GetUserIDIndex(), m_SecretStoreInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_SECRETSTORE,DB_DATA_LOAD, FAIL,"SECRET_GetAvatarSecretshop / UserIDIndex = 11866902 ", GetUserIDIndex() );
cretStoreInfo.bSecretEventEnable = FALSE;
		return FALSE;
	}

	SuccessCheckSecretStoreItem();

	return TRUE;
}

BOOL CFSGameUser::SendSecretStoreOpen( void )
{
	time_t tCurrentTime = _time64(NULL);
	CHECK_CONDITION_RETURN(FALSE == IsOpenSecretStore(tCurrentTime),FALSE );	

	if(FALSE == SECRETSTOREMANAGER.GetStoreOpenTimeCheck(GetUserIDIndex(), tCurrentTime, m_SecretStoreInfo.tRecentOpenTime))
	{
		CHECK_CONDITION_RETURN(m_SecretStoreInfo.tTodayCloseTime <= 0, FALSE);
		CHECK_CONDITION_RETURN(m_SecretStoreInfo.tTodayCloseTime > tCurrentTime, FALSE);
	}
	else
	{
		int iOpenSec = SECRETSTOREMANAGER.GetOpenKeepTimeSec();
		m_SecretStoreInfo.tTodayCloseTime = tCurrentTime + iOpenSec;

		TIMESTAMP_STRUCT sResetTime;
		TimetToTimeStruct(m_SecretStoreInfo.tTodayCloseTime, sResetTime);
		const int SECRETSTORE_RESET_TIME = 7;
		sResetTime.hour = SECRETSTORE_RESET_TIME;
		sResetTime.minute = 0;
		sResetTime.second = 0;
		time_t tResetTime = TimeStructToTimet(sResetTime);

		if(tResetTime > m_SecretStoreInfo.tTodayCloseTime && 
			tResetTime - m_SecretStoreInfo.tTodayCloseTime < iOpenSec)
		{
			m_SecretStoreInfo.tTodayCloseTime = tResetTime;
		}
	}

	SS2C_SECRET_REMAIN_TIME_INFO info;
	info.iRemainSec = m_SecretStoreInfo.tTodayCloseTime - tCurrentTime;

	CPacketComposer	Packet(S2C_SECRETSTORE_OPEN_NOTICE);
	Packet.Add( (PBYTE)&info, sizeof(SS2C_SECRET_REMAIN_TIME_INFO) );
	Send(&Packet);

	return TRUE;
}

void CFSGameUser::SendSecretStoreInfoRes()
{
	//CHECK_CONDITION_RETURN_VOID(TRUE == sAvatarSecretStoreinfo.bCheckAllBuy);

	CPacketComposer	Packet(S2C_SECRETSTORE_ITEMLIST_RES);

	SECRETSTOREMANAGER.MakePacketSecretStoreInfoRes(m_SecretStoreInfo, Packet);

	Send(&Packet);
}

void CFSGameUser::BuySuccessSecretStoreItem( int iStoreIndex )
{
	CHECK_CONDITION_RETURN_VOID(iStoreIndex < 0 || SECRET_STORE_INDEX_MAX <= iStoreIndex );

	m_SecretStoreInfo.iStoreGrade[iStoreIndex]++;

	SuccessCheckSecretStoreItem();
}

void CFSGameUser::SuccessCheckSecretStoreItem()
{
	BOOL bAllBuyCheck = FALSE; 

	for( int iIndex = 0 ; iIndex < SECRET_STORE_INDEX_MAX ; ++iIndex )
	{
		if( m_SecretStoreInfo.iStoreGrade[iIndex] >= SECRET_STORE_INDEX_MAX )
		{
			bAllBuyCheck = TRUE;
		}	
		else 
		{
			bAllBuyCheck = FALSE;
			break;
		}
	}

	if( TRUE == bAllBuyCheck )
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		pODBC->SECRET_UpdateSecretShopAllBuy(GetUserIDIndex());	//�����ص� ���߿� �ٽ� �õ�

		m_SecretStoreInfo.bCheckAllBuy = TRUE;
	}
}

int	 CFSGameUser::GetSecretStoreGrade(int iItemCode)
{
	for(int i = 0; i < SECRET_STORE_INDEX_MAX; ++i)
	{
		if(iItemCode == m_SecretStoreInfo.iStoreItemCode[i])
			return m_SecretStoreInfo.iStoreGrade[i];
	}
	return -1;
}

int	 CFSGameUser::GetSecretStoreIndex(int iItemCode)
{
	for(int i = 0; i < SECRET_STORE_INDEX_MAX; ++i)
	{
		if(iItemCode == m_SecretStoreInfo.iStoreItemCode[i])
			return i;
	}
	return -1;
}

BOOL CFSGameUser::IsOpenSecretStore(time_t tCurrentTime)
{
	CHECK_CONDITION_RETURN(TRUE == m_SecretStoreInfo.bCheckAllBuy, FALSE );	
	CHECK_CONDITION_RETURN(m_SecretStoreInfo.tTodayCloseTime > tCurrentTime, TRUE);
	return SECRETSTOREMANAGER.GetSecretStoreEnable();
}

void CFSGameUser::GetFriendGameIDList(vector<LPTSTR>& vGameID)
{
	std::vector<CFriend*> v;
	CFriend* fr = NULL;

	CLock Lock(m_FriendList.GetLockResource());
	{
		fr = m_FriendList.FindFirst();
		while(fr)
		{
			if(fr->GetStatus() != USER_ST_OFFLINE &&
				fr->GetConcernStatus() == FRIEND_ST_FRIEND_USER) 
				vGameID.push_back(fr->GetGameID());

			fr = m_FriendList.FindNext();		
		}
	}
}

int CFSGameUser::CheckAdvantageUserType()
{
	return MATCH_ADVANTAGE.GetActiveUserType(m_iAdvantageUserType, m_AvatarAdvantageRecord);
}

void CFSGameUser::ProcessLogOut()
{
	//�α׾ƿ��� �������� ����Ǿ� �� �͵� ó��
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	pODBC->MATCH_AVATAR_UpdateAdvantageRecord(GetGameIDIndex(), m_AvatarAdvantageRecord);

	if (TRUE == m_PausedPotenUpgradeInfo._bNeedSave)
	{
		pODBC->POTEN_SetAvatarPausedUpgradeInfo(GetGameIDIndex(), m_PausedPotenUpgradeInfo);
	}

	FACTION.CheckUserMissionValue(this, FACTION_MISSION_CONDITION_TYPE_LOGIN_TIME);
}

BOOL CFSGameUser::EventLoadAccumulatedPalyCount( int iParam[] )
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();

	if (NULL != pODBC)
	{
		CCompEventUserData* pEventUserData = GetEventUserData();

		if (NULL != pEventUserData)
		{
			int iPlayCount = 0;

			if(ODBC_RETURN_SUCCESS == pODBC->EVENT_LoadAccumulatedPalyCount(iParam[EVENT_CODE_LOAD_ACCUMULATED_PLAY_COUNT_TARGET_AVARTAR], GetEvent()->GetEventID(), GetUserIDIndex(), GetGameIDIndex(), iPlayCount))
			{
				pEventUserData->SetAccumulatedData(iPlayCount);

				return TRUE;
			}
		}
	}

	return FALSE;
}

DEFINE_EVENT_FUNCTION(FSGameUser,EVENT_SEND_BURNING_INFO_NOTICE)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)GetParam();

	if( NULL != pODBC )
	{
		CCompEventUserData* pEventUserData = GetEventUserData();

		int iEventIndex = GetEvent()->GetEventID();
		SS2C_BURNING_INFO_NOTICE	sBurningInfo;

		if( NULL != pEventUserData )
		{
			CPacketComposer	Packet(S2C_BURNING_INFO_NOTICE);
			
			sBurningInfo.iBurningCurrStep = pEventUserData->GetUserEventStep() -1;
			sBurningInfo.iBurningPlayCount = pEventUserData->GetAccumulatedData();
			sBurningInfo.btRewardCnt = GetEvent()->GetEventStepMax(); 
			sBurningInfo.tEventStartTime = GetEvent()->GetStartDate();
			sBurningInfo.tEventEndTime = GetEvent()->GetEndDate();
			sBurningInfo.btBurningComplete = sBurningInfo.btRewardCnt <= sBurningInfo.iBurningCurrStep ? RESULT_BURNING_COMPLATE : RESULT_BURNING_INCOMPLETE;

			Packet.Add((BYTE*)&sBurningInfo,sizeof(SS2C_BURNING_INFO_NOTICE));

			const BYTE btMAX_BurningRewardCondition = 10;
			CHECK_CONDITION_RETURN(sBurningInfo.btRewardCnt < 0 || sBurningInfo.btRewardCnt > btMAX_BurningRewardCondition,FALSE);
			
			for( int iIndex = 0 ; iIndex < sBurningInfo.btRewardCnt ; ++iIndex )
			{
				 SRewardConfig* pReward = REWARDMANAGER.GetReward(iParam[EVENT_CODE_BURNING_CONFIG_INFO_REWARDINDEX_1 + iIndex]);
				
				if( NULL == pReward )	
					continue;

				SBURNING_REWARD_INFO	sRewardInfo;
				sRewardInfo.iIndex = iIndex;
				sRewardInfo.iRewardType	 = pReward->iRewardType;

				if( REWARD_TYPE_POINT == pReward->iRewardType || 
					REWARD_TYPE_TROPHY == pReward->iRewardType )
				{
					sRewardInfo.iReward = pReward->iRewardValue;
					sRewardInfo.iRewardValue = 0;
				}
				else
				{
					SRewardConfigItem* pRewardItem = dynamic_cast<SRewardConfigItem*>(pReward);
					
					sRewardInfo.iReward	 = pRewardItem->iItemCode;
					sRewardInfo.iRewardValue = pRewardItem->GetItemCount();
				}

				Packet.Add((BYTE*)&sRewardInfo, sizeof(SBURNING_REWARD_INFO));
			}

			Send(&Packet);
			return TRUE;
		}
	}

	return FALSE;
}

void CFSGameUser::SetMatchingPoolCareInfo(SMatchingPoolCareUserInfo& stMatchingPoolCareInfo)
{
	memcpy(&m_MatchingPoolCareInfo, &stMatchingPoolCareInfo, sizeof(SMatchingPoolCareUserInfo));
}

BOOL CFSGameUser::CheckMatchingPoolCareUser()
{
	if((m_MatchingPoolCareInfo.iMaxPlayCount - m_MatchingPoolCareInfo.iPlayCount) > 0)
		return TRUE;

	return FALSE;
}

void CFSGameUser::CheckMatchingPoolCareExpPointBonus(int& iExp, int& iPoint)
{
	iExp = m_MatchingPoolCareInfo.iExp;
	iPoint = m_MatchingPoolCareInfo.iPoint;
}

BOOL CFSGameUser::CheckAndRemoveMatchingPoolCareTarget()
{
	if(m_MatchingPoolCareInfo.iMaxPlayCount <= m_MatchingPoolCareInfo.iPlayCount)
	{
		ZeroMemory(&m_MatchingPoolCareInfo, sizeof(SMatchingPoolCareUserInfo));
		return TRUE;
	}

	return FALSE;
}

void CFSGameUser::AddMatchingPoolCarePlayCount()
{
	m_MatchingPoolCareInfo.iPlayCount++;
}

int CFSGameUser::GetMatchingPoolCarePlayCount()
{
	return m_MatchingPoolCareInfo.iPlayCount;
}

int CFSGameUser::GetMatchingPoolCareRemainPlayCount()
{
	return m_MatchingPoolCareInfo.iMaxPlayCount - m_MatchingPoolCareInfo.iPlayCount;
}

void CFSGameUser::LoadUserFaction()
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_VOID(pODBC);

	vector<int> vecUnlockedShopItem;	
	BOOL bIsReversal = FALSE;
	if (ODBC_RETURN_SUCCESS != pODBC->FACTION_GetUser(GetUserIDIndex(), GetGameIDIndex(), FACTION.GetCurrentSeasonIndex(), m_UserFactionInfo, bIsReversal) ||
		ODBC_RETURN_SUCCESS != pODBC->FACTION_GetUserShopItemBuyingCount(GetUserIDIndex(), m_UserFactionInfo) ||
		ODBC_RETURN_SUCCESS != pODBC->FACTION_GetUserMissionList(GetUserIDIndex(), m_UserFactionInfo) ||
		ODBC_RETURN_SUCCESS != pODBC->FACTION_GetUserMissionTime(GetUserIDIndex(), m_UserFactionInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_FACTION, DB_DATA_LOAD, FAIL, "FACTION_GetUser");
		return;
	}

	if(TRUE == bIsReversal)	
	{
		GetUserFaction()->_iReversalRate = FACTION.GetReversalBenefitRate_EXP();
	}
	else
	{
		GetUserFaction()->_iReversalRate = 0;
	}

	GetUserFaction()->_btUserFactionGrade = FACTION.GetUserFactionGrade((CUser*)this, GetUserFaction()->_iTotalFactionScore, TRUE);

	CheckFactionJoinItem();
	CheckFactionRaceRankReward();
	CheckFactionRankReward();

	m_UserFactionInfo.tTodayLoginTime = _time64(NULL);

	FACTION.CheckUserMissionValue(this, FACTION_MISSION_CONDITION_TYPE_LOGIN);
}

void CFSGameUser::CheckFactionJoinItem()
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_ON_GOING != FACTION.GetRaceStatus() &&
		FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME != FACTION.GetRaceStatus());

	if (FALSE == m_UserFactionInfo._bIsExistJoinlItem)
	{
		SUserItemInfo UserItem;
		int iPropertyIndex = 0;
		if (TRUE == FACTION.GetFactionJoinItem(m_UserFactionInfo._iFactionIndex, UserItem, iPropertyIndex))
		{
			CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
			CHECK_NULL_POINTER_VOID(pODBC);

			vector<SUserItemProperty> vecProperty;
			if (ODBC_RETURN_SUCCESS == pODBC->FACTION_GiveJoinItem(GetUserIDIndex(), GetGameIDIndex(), iPropertyIndex, UserItem, vecProperty))
			{
				if (ITEM_PROPERTY_NORMAL == UserItem.iPropertyType)
					UserItem.iPropertyType = ITEM_PROPERTY_TIME;
				UserItem.iPropertyNum = vecProperty.size();

				m_UserItem->GetCurAvatarItemList()->AddItem(UserItem);

				for (int i = 0; i < vecProperty.size(); ++i)
				{
					vecProperty[i].iGameIDIndex = GetGameIDIndex();
					vecProperty[i].iItemIdx = UserItem.iItemIdx;
					m_UserItem->GetCurAvatarItemList()->AddUserItemProperty(vecProperty[i]);
				}
				m_UserFactionInfo._bIsExistJoinlItem = TRUE;
			}
		}
	}

	if (0 != m_UserFactionInfo._iFactionIndex)
	{	
		if (FACTION_RACE_ON_GOING == FACTION.GetRaceStatus())
		{
			int iFactionAchievementTitle = FACTION.GetFactionAchievementTitle(m_UserFactionInfo._iFactionIndex);
			if (0 != iFactionAchievementTitle)
			{
				SetEquippedAchievementTitle(iFactionAchievementTitle);
			}
		}
	}
}

void CFSGameUser::CheckFactionRaceRankReward()
{
	SUserFactionInfo* pUserFaction = GetUserFaction();
	CHECK_CONDITION_RETURN_VOID(NULL == pUserFaction);

	CHECK_CONDITION_RETURN_VOID(FALSE == FACTION.GiveFactionRaceRankReward(this));

	if(pUserFaction->_iPreSeasonIndex > 0 &&
		pUserFaction->_iPreFactionIndex > 0)
	{
		UpdateFactionLastPlayTime(TRUE);

		pUserFaction->_iPreSeasonIndex = 0;
		pUserFaction->_iPreFactionIndex = 0;
	}
	else
		UpdateFactionLastPlayTime(FALSE);
}

void CFSGameUser::UpdateFactionLastPlayTime(BOOL bClearSeason)
{
	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_CLOSED == FACTION.GetRaceStatus());
	CHECK_CONDITION_RETURN_VOID(m_UserFactionInfo._iFactionIndex == 0);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_VOID(pODBC);

	if (ODBC_RETURN_SUCCESS == pODBC->FACTION_UpateUserLastPlayTime(GetUserIDIndex(), m_UserFactionInfo._tTimeLastPlayTime, m_UserFactionInfo._tDailyLastPlayTime, m_UserFactionInfo._tSeasonLastPlayTime, bClearSeason))
	{
		if(bClearSeason == TRUE)
			ClearUserFactionInfo();
	}
}

void CFSGameUser::CheckFactionRankReward()
{
	SUserFactionInfo* pUserFaction = GetUserFaction();
	CHECK_CONDITION_RETURN_VOID(NULL == pUserFaction);

	FACTION.GiveFactionRankReward(this);
}

void CFSGameUser::OnRecieveReward(SRewardConfig* pReward)
{
	CHECK_NULL_POINTER_VOID(pReward);

	switch (pReward->GetRewardType())
	{
	case REWARD_TYPE_FACTIONPOINT:
		{
			CHECK_CONDITION_RETURN_VOID(0 == m_UserFactionInfo._iFactionIndex);

			char szFactionNotice[MAX_FACTION_NEWS_LENGTH+1] = {0,};
			if (TRUE == FACTION.MakeNoticeString(FACTION_NOTICE_TYPE_REWARD_POINT, szFactionNotice, GetGameID(), m_UserFactionInfo._iFactionIndex, pReward->GetRewardValue()))
			{
				CCenterSvrProxy* pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
				if (NULL != pCenterProxy)
				{
					SG2S_FACTION_NEWSBOARD_NOT not;
					not.iNewsLength = strlen(szFactionNotice);

					CPacketComposer Packet(G2S_FACTION_NEWSBOARD_NOT);
					Packet.Add((PBYTE)&not, sizeof(SG2S_FACTION_NEWSBOARD_NOT));
					Packet.Add((PBYTE)szFactionNotice, not.iNewsLength);
					pCenterProxy->Send(&Packet);
				}
			}
		} break;
	case REWARD_TYPE_FACTIONSCORE:
		{
			CHECK_CONDITION_RETURN_VOID(0 == m_UserFactionInfo._iFactionIndex);

			char szFactionNotice[MAX_FACTION_NEWS_LENGTH+1] = {0,};
			if (TRUE == FACTION.MakeNoticeString(FACTION_NOTICE_TYPE_REWARD_SCORE, szFactionNotice, GetGameID(), m_UserFactionInfo._iFactionIndex, pReward->GetRewardValue()))
			{
				CCenterSvrProxy* pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
				if (NULL != pCenterProxy)
				{
					SG2S_FACTION_NEWSBOARD_NOT not;
					not.iNewsLength = strlen(szFactionNotice);

					CPacketComposer Packet(G2S_FACTION_NEWSBOARD_NOT);
					Packet.Add((PBYTE)&not, sizeof(SG2S_FACTION_NEWSBOARD_NOT));
					Packet.Add((PBYTE)szFactionNotice, not.iNewsLength);
					pCenterProxy->Send(&Packet);
				}
			}
		} break;
	}
}


BOOL CFSGameUser::GiveReward_Achievement( int iAchievementIndex , BOOL bAnnounce /*= TRUE*/)
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	BYTE btStorageType =ACHIEVEMENTMANAGER.GetAchievementStorageType(iAchievementIndex);
	CHECK_CONDITION_RETURN(btStorageType == ACHIEVEMENT_STORAGETYPE_NONE, FALSE);

	SAchievementLog sAchievementLog;
	sAchievementLog.iAchievementIndex = iAchievementIndex;
	sAchievementLog.iAchievementLogGroupIndex = ACHIEVEMENTMANAGER.GetAchievementGroupIndex(iAchievementIndex);

	vector<int> vecAnnounceAchievementIndex;
	if( ODBC_RETURN_SUCCESS == pBaseODBC->ACHIEVEMENT_InsertCompletedAchievement(GetUserIDIndex(), GetGameIDIndex(), sAchievementLog, btStorageType) )
	{
		CAchievementLogGroup* pAchievementLogGroup = FindAchievementLogGroupbyKey(sAchievementLog.iAchievementLogGroupIndex);

		if (pAchievementLogGroup != NULL){
			pAchievementLogGroup->AddAchievementLog(sAchievementLog);		
		}
		else
		{
			pAchievementLogGroup = new CAchievementLogGroup();
			if (NULL != pAchievementLogGroup)
			{
				pAchievementLogGroup->AddAchievementLog(sAchievementLog);
				m_mapAchievementLogGroup.insert(CACHIEVEMENT_LOG_GROUP_MAP::value_type(sAchievementLog.iAchievementLogGroupIndex, pAchievementLogGroup));
			}
		}
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_ACHIEVEMENT,DB_DATA_UPDATE, FAIL
			, "ACHIEVEMENT_InsertCompletedAchievement - GiveReward_Achievement,   iAchievementIndex = 11866902,   GameIDIndex = 0", iAchievementIndex, GetGameIDIndex());
urn FALSE;
	}

	if (sAchievementLog.iScore >= ANNONCE_CONDITION_SCORE && TRUE == bAnnounce)
	{
		SendAchievement_AnnounceProxy(sAchievementLog.iAchievementIndex);
	}

	const int cnSizeOne = 1;
	CPacketComposer PacketComposer(S2C_COMPLETE_ACHIEVEMENT_NOT);
	PacketComposer.Add(cnSizeOne);

	PacketComposer.Add(sAchievementLog.iAchievementIndex);
	PacketComposer.Add(sAchievementLog.iScore);
	PacketComposer.Add(sAchievementLog.wCompletedYear);
	PacketComposer.Add(sAchievementLog.wCompletedMonth);
	PacketComposer.Add(sAchievementLog.wCompletedDay);
	PacketComposer.Add(sAchievementLog.wCompletedHour);
	PacketComposer.Add(sAchievementLog.wCompletedMinute);

	if (sAchievementLog.iAchievementIndex == ACHIEVEMENT_CODE_GAMEPLAY_FIRST_POINT || sAchievementLog.iAchievementIndex == ACHIEVEMENT_CODE_GAMEPLAY_FIRST_WIN)
	{
		m_vAchievementIndexforPreChecking.push_back(sAchievementLog.iAchievementIndex);
	}

	CheckAndSend(&PacketComposer, IsPlaying());

	return TRUE;
}

void CFSGameUser::SendAchievement_AnnounceProxy( int iAchievementIndex )
{
	const int cnSizeOne = 1, cnDefaultArrayNul = 0;

	CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);

	SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
		
	info.iGameIDIndex = GetGameIDIndex();
	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));
	strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), _countof(info.szGameID)-1);
	info.iAchievementCount = cnSizeOne;
	info.aiAchievementIndex[cnDefaultArrayNul] = iAchievementIndex;
		
	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
		
	if (pMatch != NULL)
	{
		pMatch->Send(&PacketComposer);
	}	
}

void CFSGameUser::SendSuportGrowthInfo()
{
	SS2C_SUPPORT_GROWTH_INFO_NOTIFY info;
	info.iExpBonus = 0;

	SFSConfigInfo GameConfig;
	ZeroMemory(&GameConfig, sizeof(SFSConfigInfo));

	g_pFSDBCfg->GetCfg(CFGCODE_EXP_SUPPORT_GROWTH, 0, GameConfig);

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	if(pAvatarInfo->iLv < GAME_LEAGUE_UP_PRO)
	{
		info.iExpBonus = GameConfig.iParam[0];
	}
	else if(pAvatarInfo->iLv >= GAME_LEAGUE_UP_PRO && pAvatarInfo->iLv < GAME_LVUP_BOUND2)
	{
		info.iExpBonus = GameConfig.iParam[1];
	}
	else if(pAvatarInfo->iLv >= GAME_LVUP_BOUND2 && pAvatarInfo->iLv < GAME_LVUP_SEMI_PRO)
	{
		info.iExpBonus = GameConfig.iParam[2];
	}

	Send(S2C_SUPPORT_GROWTH_INFO_NOTIFY, &info, sizeof(SS2C_SUPPORT_GROWTH_INFO_NOTIFY));
}

void CFSGameUser::SendWordPuzzlesEventStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == WORDPUZZLES.IsOpen());

	SS2C_WORDPUZZLES_EVENT_OPEN_NOT info;
	info.btOpenStatus = WORDPUZZLES.IsOpen();
	Send(S2C_WORDPUZZLES_EVENT_OPEN_NOT, &info, sizeof(SS2C_WORDPUZZLES_EVENT_OPEN_NOT));
}

BOOL CFSGameUser::IsUseEquipItem()
{
	vector<int> vecEquipProduct;
	m_SlotPackageList.GetEquipItemAll( vecEquipProduct );

	if(vecEquipProduct.empty())
		return FALSE;

	return TRUE;
}

void CFSGameUser::SendRandomBoxStatus(BYTE btUserState)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());
	CUserHighFrequencyItem* pHighFrequencyItem = GetUserHighFrequencyItem();
	CHECK_NULL_POINTER_VOID(pHighFrequencyItem);

	SS2C_RANDOMBOX_OPEN_NOT info;
	info.btOpenStatus = RANDOMBOX.IsOpen();
	info.iBoxItemCount = (pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GOLD_BAGS ) +
		pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_RED_BAGS ) +
		pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GREEN_BAGS ) +
		pHighFrequencyItem->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_BLUE_BAGS ));
	info.btUserState = btUserState;
	Send(S2C_RANDOMBOX_OPEN_NOT, &info, sizeof(SS2C_RANDOMBOX_OPEN_NOT));
}

BOOL CFSGameUser::CheckInventory_ComboundingItemWithItemCode( int iItemCode, int iRequiredVal, int& iItemIdx )
{
	CAvatarItemList *pItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pItemList);

	return pItemList->CheckInventory_ComboundingItemWithItemCode(iItemCode, iRequiredVal, iItemIdx);
}

int CFSGameUser::ComboundingItem_Combine( int iItemIndex , SComboundingItem_Config	sConfig, SS2C_COMPOUNDINGITEM_COMBINE_RES& rinfo)
{
	int iResult = RESULT_COMPOUNDINGITEM_COMBINE_FAILED;	

	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_CONDITION_RETURN(NULL == pItemShop, RESULT_COMPOUNDINGITEM_COMBINE_FAILED);
	CHECK_CONDITION_RETURN(FALSE == GetUserItemList()->IsInMyInventory(sConfig.iItemCode), RESULT_COMPOUNDINGITEM_COMBINE_FAILED);

	int iMaterialCount[MAX_MATERIALINDEX] = {0,};
	iResult = RESULT_COMPOUNDINGITEM_COMBINE_SUCCESS;

	int iDelete_ItemIdx[MAX_MATERIALINDEX];
	memset( iDelete_ItemIdx , -1 , sizeof(int) * MAX_MATERIALINDEX );

	if( GetPresentList(MAIL_PRESENT_ON_AVATAR)->GetSize() >= MAX_PRESENT_LIST)
	{
		return static_cast<int>(RESULT_COMPOUNDINGITEM_SHORTAGE_PRESENT_STORAGE_FULL);
	}

	for( int iIndex = 0 ; iIndex < sConfig.iMaterialValue && iIndex < MAX_MATERIALINDEX ; ++iIndex )
	{	
		int iMaterialItemCode = pItemShop->GetMaterialItemCode(sConfig.iMaterialIndex[iIndex]);
		BOOL bItemCountCheck = (BOOL)CheckInventory_ComboundingItemWithItemCode(iMaterialItemCode,iMaterialCount[sConfig.iMaterialIndex[iIndex]], iDelete_ItemIdx[iIndex]);

		if( bItemCountCheck == TRUE )
			iMaterialCount[sConfig.iMaterialIndex[iIndex]]++;
		else
		{
			iResult = RESULT_COMPOUNDINGITEM_SHORTAGE_MATERIAL;
			break;
		}
	}

	if( RESULT_COMPOUNDINGITEM_COMBINE_SUCCESS == iResult )
	{	
		iResult = ComboundingItem_Combine_ProcessingResult(iItemIndex, sConfig, iDelete_ItemIdx, rinfo);
	}

	return iResult;
}

DEFINE_EVENT_FUNCTION(FSGameUser,EVENT_SEND_COMPOUNDINGITEM_NOTICE)
{
	CCompEventUserData* pEventUserData = GetEventUserData();
	CCompEvent* pEvent = GetEvent();

	if( NULL != pEventUserData && NULL != pEvent )
	{
		int iEventIndex = pEvent->GetEventID();
		CPacketComposer	Packet(S2C_COMPOUNDINGITEM_PRESENT_NOT);

		SS2C_COMPOUNDINGITEM_PRESENT_NOT	sInfo;
		ZeroMemory(&sInfo, sizeof(SS2C_COMPOUNDINGITEM_PRESENT_NOT));

		SRewardConfig* pReward = REWARDMANAGER.GiveReward(this,iParam[EVENT_CODE_COMPOUNDINGITEM_REWARDINDEX]);
		sInfo.btResult = RESULT_COMPOUNDINGITEM_PRESENT_NOT_SUCCESS;

		if( GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
		{
			sInfo.btResult = RESULT_COMPOUNDINGITEM_PRESENT_NOT_PRESENT_STORAGE_FULL;
			Packet.Add((PBYTE)&sInfo,sizeof(SS2C_COMPOUNDINGITEM_PRESENT_NOT));
			Send(&Packet);
			return FALSE;
		}

		if( NULL != pReward )
		{
			SRewardConfigItem* pRewardItem = (SRewardConfigItem*)pReward;

			sInfo.iItemCode = pRewardItem->iItemCode;
			sInfo.tStartTime = pEvent->GetStartDate();
			sInfo.tEndTime = pEvent->GetEndDate();

			Packet.Add((PBYTE)&sInfo,sizeof(SS2C_COMPOUNDINGITEM_PRESENT_NOT));
			Send(&Packet);
			return TRUE;
		}	
	}

	return FALSE;
}

DEFINE_EVENT_FUNCTION( FSGameUser,EVENT_ACCUMULATED_ATTENDANCE_CHECK_USER_DATA)
{
	CCompEvent* pEvent = GetEvent();
	CHECK_NULL_POINTER_BOOL(pEvent);

	CCompEventUserData* pEventUserData = GetEventUserData();
	CHECK_NULL_POINTER_BOOL(pEventUserData);

	const int iAttendanceIndex = *((int*)GetParam());
	const int iCheckStatus = iParam[EVENT_CODE_ACCUMULATED_ATTENDANCE_CHECK_USER_DATA_STATUS];

	if(pEventUserData->GetAttendanceStatus(iAttendanceIndex) != iCheckStatus)
	{
		return FALSE;
	}

	const int iRewardGroupIndex = *((int*)GetParam());
	list<int>	listReward;
	pEvent->GetRewardList(iRewardGroupIndex, listReward);	

	list<int>::iterator	iter = listReward.begin();

	if( iter != listReward.end() )
	{
		SRewardConfig*	pReward = REWARDMANAGER.GetReward((*iter));
		CHECK_NULL_POINTER_BOOL(pReward);

		if( REWARD_TYPE_ITEM == pReward->GetRewardType() )
		{
			SRewardConfigItem* pRewardItem = dynamic_cast<SRewardConfigItem*>(pReward);
			int iMailType = pRewardItem->iMailType;

			const int	iRequiredSize = pReward->GetItemCount();
			const size_t	iRemainingSize = MAX_PRESENT_LIST - GetPresentList(iMailType)->GetSize();

			if( iRemainingSize < iRequiredSize )
			{
				CPacketComposer	Packet(S2C_EVENT_PRESENT_STORAGE_NOT);

				SS2C_EVENT_PRESENT_STORAGE_NOT	info;
				info.btResult = static_cast<BYTE>(RESULT_EVENT_PRESENT_STORAGE_FULL);
				Packet.Add((byte*)&info, sizeof(SS2C_EVENT_PRESENT_STORAGE_NOT));
				Send(&Packet);

				return FALSE;
			}

		}
	}

	return TRUE;
}

int CFSGameUser::ComboundingItem_Combine_ProcessingResult( int iItemIndex, SComboundingItem_Config sConfig, int iDeleteIndex[],SS2C_COMPOUNDINGITEM_COMBINE_RES& rinfo)
{
	CFSODBCBase*	pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN( NULL == pODBCBase ,RESULT_COMPOUNDINGITEM_DBERROR );

	int iGameIDIndex = GetGameIDIndex();

	int iResult = RESULT_COMPOUNDINGITEM_COMBINE_SUCCESS;

	ODBCRETURN DBResult = pODBCBase->ITEM_Delete_ComboundingItem_Combination(iGameIDIndex, iItemIndex, iDeleteIndex, sConfig.iItemCode);
	if( ODBC_RETURN_SUCCESS == DBResult)
	{
		GetAvatarItemList()->RemoveItem(iItemIndex); //main item

		for( int iIndex = 0 ; iIndex < sConfig.iMaterialValue || iIndex < MAX_MATERIALINDEX ; ++iIndex )
		{
			if( -1 < iDeleteIndex[iIndex] )
			{
				GetAvatarItemList()->RemoveItem(iDeleteIndex[iIndex]);
			}
		}

		SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, sConfig.iCombineRewardIndex);

		if( pReward != NULL )
		{
			SRewardConfigItem* pRewardItem = (SRewardConfigItem*) pReward;

			rinfo.btRewardType = (BYTE)pRewardItem->iRewardType;
			rinfo.iReward = pRewardItem->GetRewardValue();
			rinfo.iCount = pRewardItem->GetItemCount();
		}
	}
	else
	{
		iResult = RESULT_COMPOUNDINGITEM_DBERROR;
		WRITE_LOG_NEW(LOG_TYPE_COMPOUNDINGITEM, DB_DATA_UPDATE, FAIL, "ITEM_Delete_ComboundingItem_Combination / GameIDIndex: 11866902, ItemIndex: 0, ItemCode: 18227200, Result: -858993460",
temIndex, sConfig.iItemCode, (int)DBResult);
	}

	return iResult;
}

void CFSGameUser::SendPuzzlesStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PUZZLES.IsOpen());

	SS2C_PUZZLES_OPEN_NOT info;
	info.btOpenStatus = PUZZLES.IsOpen();
	Send(S2C_PUZZLES_OPEN_NOT, &info, sizeof(SS2C_PUZZLES_OPEN_NOT));
}

void CFSGameUser::SendThreeKingdomsEventStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == THREEKINGDOMSEVENT.IsOpen());

	SS2C_EVENT_THREE_KINGDOMS_OPEN_NOT info;
	info.btOpenStatus = THREEKINGDOMSEVENT.IsOpen();
	Send(S2C_EVENT_THREE_KINGDOMS_OPEN_NOT, &info, sizeof(SS2C_EVENT_THREE_KINGDOMS_OPEN_NOT));
}

void CFSGameUser::SendMissionShopEventStatus()
{
	// �̼� ���±Ⱓ�� �� ���±Ⱓ�� �׻� ���ԵǹǷ� ���� ���¿��θ� üũ.
	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONSHOP.IsShopOpen());
	CHECK_CONDITION_RETURN_VOID(GetLocation() != U_LOBBY);

	SS2C_MISSIONSHOP_INFO_NOT	info;
	info.bIsMissionOpen = MISSIONSHOP.IsMissionOpen();
	info.bIsShopOpen = MISSIONSHOP.IsShopOpen();
	info.bIsMissionClear = m_UserMissionShopEvent.IsClearMission();
	Send(S2C_MISSIONSHOP_INFO_NOT, &info, sizeof(SS2C_MISSIONSHOP_INFO_NOT));
}

void CFSGameUser::GetSpecialSkinAdditionalEffectsInfo( vector<SSpecialSkinAdditionaleffectsInfo>& vec )
{
	GetUserSpecialSkin()->GetUsedSpecialSkinAdditionalEffects(vec);
}

int CFSGameUser::CustomizeItem_InsertLog( int iItemCode, int iInventoryIndex, int* iPropertyIndex, BYTE btColorType )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN( NULL == pODBCBase, RESULT_CUSTOMIZE_ITEM_MAKE_DB_FAIL );
	CHECK_CONDITION_RETURN( FALSE == CUSTOMIZEManager.CheckCustomItem( iItemCode ), RESULT_CUSTOMIZE_ITEM_MAKE_DB_FAIL);

	S_ODBC_CustomizeLog		sLogInfo;
	sLogInfo._iUserIDIndex = GetUserIDIndex();
	sLogInfo._iGameIDIndex = GetGameIDIndex();
	strncpy_s(sLogInfo._szUserID, sizeof(sLogInfo._szUserID), GetUserID(), MAX_USERID_LENGTH+1);
	strncpy_s(sLogInfo._szGameID, sizeof(sLogInfo._szGameID), GetGameID(), MAX_GAMEID_LENGTH+1);
	sLogInfo._iInventoryIndex = iInventoryIndex;
	sLogInfo._iItemCode = iItemCode / 10 * 10 + 1;

	if( NULL != iPropertyIndex )
	{
		if( iPropertyIndex[0] == iPropertyIndex[1] ) // �ɷ�ġ�� �Ѵ� ������ �ȵȴ�.
		{
			return RESULT_CUSTOMIZE_ITEM_MAKE_DB_FAIL;
		}

		sLogInfo._iPropertyIndex[0] = iPropertyIndex[0];
		sLogInfo._iPropertyIndex[1] = iPropertyIndex[1];
	}
	else{
		memset(sLogInfo._iPropertyIndex, -1, sizeof(int) * MAX_CUSTOMIZE_PROPERTYINDEX_NUM );
	}
	sLogInfo._btColor = btColorType;

	int iResult = pODBCBase->CUSTOMIZE_InsertLog(sLogInfo);

	if( iResult != ODBC_RETURN_SUCCESS )
	{
		if( RESULT_CUSTOMIZE_ITEM_MAKE_ALERADY_USED == iResult )
			CUSTOMIZEManager.UpdateCustomizeItemColorUsed( sLogInfo._iItemCode, sLogInfo._btColor);

		WRITE_LOG_NEW(LOG_TYPE_CUSTOMIZE, DB_DATA_UPDATE, FAIL 
			, "CUSTOMIZE_InsertLog Fail!  GameIDIndex: 11866902,  iItemCode: 0,  iInventoryIndex:  18227200,  ColorType:  -858993460,  Result: -858993460"
, sLogInfo._iItemCode, sLogInfo._iInventoryIndex, sLogInfo._btColor, iResult);
	}
	else 
	{
		m_listCustomReceiveditem.push_back(sLogInfo);
		CUSTOMIZEManager.UpdateCustomizeItemColorUsed( sLogInfo._iItemCode, sLogInfo._btColor);
	}

	return iResult;
}

BOOL CFSGameUser::GetCustomizeItem_Received( int iInventoryIndex, S_ODBC_CustomizeLog& sLogInfo )
{
	list<S_ODBC_CustomizeLog>::iterator	iter = m_listCustomReceiveditem.begin();

	for( ; iter != m_listCustomReceiveditem.end() ; ++iter )
	{
		if( (*iter)._iInventoryIndex == iInventoryIndex )
		{
			sLogInfo = (*iter);
			return TRUE;
		}
	}

	return FALSE;
}

void CFSGameUser::MakeDataForUserInfoCharacterCollection(PBYTE pBuffer, int& iSize)
{
	GetUserCharacterCollection()->MakeDataForUserInfoCharacterCollection(pBuffer, iSize);
}

void CFSGameUser::MakeDataForUserInfoCharacterCollection(CPacketComposer &Packet)
{
	int iSize = 0;
	BYTE Buffer[MAX_USER_INFO_CHARACTER_COLLECTION_DATA] = {0};

	GetUserCharacterCollection()->MakeDataForUserInfoCharacterCollection(Buffer, iSize);

	Packet.Add(Buffer, iSize);
}

void CFSGameUser::SendHotGirlTime()
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == HOTGIRLTIMEManager.IsOpen() );

	SHotGirlTimeConfig sConfig = HOTGIRLTIMEManager.GetHotGirlTimeConfig();	
	SS2C_HOTGIRLTIME_OPEN_NOT sNot;

	sNot.iStartTime = sConfig._iStartTime;
	sNot.iEndTime = sConfig._iEndTime;
	sNot.btStatus = (BYTE)HOTGIRLTIMEManager.IsOpen();

	CPacketComposer	Packet(S2C_HOTGIRLTIME_OPEN_NOT);
	Packet.Add((PBYTE)&sNot, sizeof(SS2C_HOTGIRLTIME_OPEN_NOT));

	Send(&Packet);
}

SAvatarInfo* CFSGameUser::ChangeCloneAvatar(Clone_Index eIndex)
{
	CHECK_CONDITION_RETURN( FALSE == m_bCloneCharacter , NULL );

	SAvatarInfo * pAvatarInfo = NULL;
	pAvatarInfo = m_AvatarManager.GetAvatarWithIdx(m_AvatarManager.GetCurUsedAvatarIdx());
	CHECK_CONDITION_RETURN( NULL == pAvatarInfo, NULL);
	CHECK_CONDITION_RETURN( 0 > eIndex || Max_Clone <= eIndex , NULL);
	
	int iIndex = (int)eIndex;

	CHECK_CONDITION_RETURN( FALSE == AVATARCREATEMANAGER.UpdateCloneCharacterStat( &m_sCloneCharacterInfo[iIndex], &pAvatarInfo->Status ), FALSE);
	pAvatarInfo->SetAvatarCloneData(m_sCloneCharacterInfo[iIndex]);
	ChangeCloneDefaultSeremony(GetUseStyleIndex(), false);

	return pAvatarInfo;
}

void CFSGameUser::ChangeCloneAvatarDiffSexItem( int iSex )
{
	m_UserItem->ChangeCloneCharacterItem(iSex);
}

void CFSGameUser::SendUserItemList( int iInventoryKind, int iBigKind, int iSmallKind, int iPage, BOOL bCloneChange)
{
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	CFSGameUserItem* pFSGameUserItem = GetUserItemList();
	CHECK_NULL_POINTER_VOID(pFSGameUserItem);

	int iStatus = 0;

	if( ITEM_PAGE_MODE_SHOP == pFSGameUserItem->GetMode() )
	{
		pFSGameUserItem->SetMode(ITEM_PAGE_MODE_MYITEM); //user

		if( FALSE == bCloneChange )
			SendAvatarInfo();
	}

	SUserItemInfo aItemList[MAX_USERITEMLIST_PER_PAGE];
	int iItemNum = 0;
	pFSGameUserItem->GetItemListPage(iInventoryKind, iPage, iBigKind, iSmallKind, aItemList, iItemNum);

	if( iPage > 0 && 0 == iItemNum) 
	{
		iPage = 0;
		pFSGameUserItem->GetItemListPage(iInventoryKind, iPage, iBigKind, iSmallKind, aItemList, iItemNum);
	}

	time_t CurrentTime = _time64(NULL);

	int iKindItemCount = GetUserItemList()->GetItemCountWithKind(iBigKind, iSmallKind);

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID( pAvatarInfo );

	if( TRUE == m_bCloneCharacter && 0 < m_iChangePCRoomDeffCount && 0 == iSmallKind && 0 == iBigKind && pAvatarInfo->iSex == ITEM_INFO_SEX_MAN ) 
		iKindItemCount += m_iChangePCRoomDeffCount;
	else if( TRUE == m_bCloneCharacter && 0 > m_iChangePCRoomDeffCount && 0 == iSmallKind && 0 == iBigKind && pAvatarInfo->iSex == ITEM_INFO_SEX_WOMAN )
		iKindItemCount += m_iChangePCRoomDeffCount;

	CPacketComposer PacketComposer(S2C_USERITEM_LIST_RES);
	PacketComposer.Add(iInventoryKind);
	PacketComposer.Add(iBigKind);
	PacketComposer.Add(iSmallKind);
	PacketComposer.Add(iKindItemCount);
	PacketComposer.Add(iPage);
	PacketComposer.Add(iItemNum);

	for(int i=0 ; i<iItemNum ;i++)
	{
		PacketComposer.Add(aItemList[i].iItemIdx);
		PacketComposer.Add(aItemList[i].iItemCode);
		int iSellPrice = 0;

		SShopItemInfo ShopItemInfo;
		if(aItemList[i].iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && aItemList[i].iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX &&
			aItemList[i].iCategory == 0)
		{
			pItemShop->GetItemByPropertyKind(aItemList[i].iPropertyKind, ShopItemInfo);
		}
		else
		{
			pItemShop->GetItem(aItemList[i].iItemCode, ShopItemInfo);
		}
		if( ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST && ShopItemInfo.iChannel != ITEMCHANNEL_BOOST1 && ShopItemInfo.iChannel != ITEMCHANNEL_BOOST2 )	// 개수???�이??// 20100108 ?�차지, ?�워?�낵 ?�외 
		{
			iSellPrice = ( ( aItemList[i].iSellPrice ) * aItemList[i].iPropertyTypeValue * 5 ) / 100 ;
		}
		else if( ShopItemInfo.iPropertyType == ITEM_PROPERTY_TIME && aItemList[i].iPropertyTypeValue != -1 )						// 기간???�이??
		{
			if (aItemList[i].iPropertyTypeValue == -1)
			{
				iSellPrice = (aItemList[i].iSellPrice * 5) / 100;
			}
			else if (ITEM_STATUS_EXPIRED == aItemList[i].iStatus)
			{
				iSellPrice = 0;
			}
			else
			{
				int iRemainHours = 0;
				double dDiffSeconds = difftime(CurrentTime, aItemList[i].ExpireDate);
				if( dDiffSeconds > 0 )
				{
					iSellPrice = 0;
				}
				else
				{
					iRemainHours = 	(int)(dDiffSeconds / 3600)*(-1);
					if( aItemList[i].iPropertyTypeValue != 0 )
					{
						iSellPrice = ((aItemList[i].iSellPrice * 5) / 100) * iRemainHours / aItemList[i].iPropertyTypeValue;
					}
					else
					{
						iSellPrice = 0;
					}
				}
			}
		}
		else
		{
			iSellPrice = (aItemList[i].iSellPrice * 5) / 100;
		}

		PacketComposer.Add(iSellPrice);
		PacketComposer.Add(aItemList[i].sIsLock);

		iStatus = aItemList[i].iStatus;
		//1: 미착??2: ?�른?��?착용 3:?��?착용
		//0: 미착??1: 착용
		if( 1 == iStatus )
			iStatus = 0;
		else if( 2 == iStatus )
			iStatus = 1;

		PacketComposer.Add(iStatus);
		PacketComposer.Add(aItemList[i].iPropertyType);

		if( aItemList[i].iPropertyType == 1 )
		{
			int iDay = -1;	//Unlimited
			int iHour = 0;
			int iMin = 0;

			if (aItemList[i].iPropertyTypeValue != -1)
			{
				if (ITEM_STATUS_EXPIRED == aItemList[i].iStatus)
				{
					iDay = 0;
				}
				else
				{
					int iDiffSecond = difftime(aItemList[i].ExpireDate, CurrentTime);

					iDay = iDiffSecond / 86400;
					iHour = (iDiffSecond ) / 3600;
3600;
					iMin = (iDiffSecond ) / 60;
 60;
				}
			}

			PacketComposer.Add(iDay);
			PacketComposer.Add(iHour);
			PacketComposer.Add(iMin);
		}
		if( aItemList[i].iPropertyType == -2 )
		{
			int iDay = -1; //Unlimited
			int iHour = 0;
			int iMin = 0;

			if (aItemList[i].iPropertyTypeValue != -1)
			{
				if (ITEM_STATUS_EXPIRED == aItemList[i].iStatus)
				{
					iDay = 0;
				}
				else
				{
					int iDiffSecond = difftime(aItemList[i].ExpireDate, CurrentTime);

					iDay = iDiffSecond / 86400;
					iHour = (iDiffSecond ) / 3600;
3600;
					iMin = (iDiffSecond ) / 60;
 60;
				}
			}

			PacketComposer.Add(iDay);
			PacketComposer.Add(iHour);
			PacketComposer.Add(iMin);
		}
		else if( aItemList[i].iPropertyType == 2)
		{
			PacketComposer.Add(aItemList[i].iPropertyTypeValue);
		}

		int iPropertyKind = aItemList[i].iPropertyKind;

		if(iPropertyKind == FACE_OFF_KIND_NUM ||													// 320 
			iPropertyKind == PROTECT_RING_ITEM_KIND_NUM ||											// 900
			(iPropertyKind >= MIN_TREASURE_KIN_NUM && iPropertyKind <= MAX_TREASURE_KIN_NUM) ||		// 600~619
			(iPropertyKind >= MIN_STEP_BOX_KIN_NUM && iPropertyKind <= MAX_STEP_BOX_KIN_NUM) ||		// 640~649
			(iPropertyKind >= MIN_RANK_ITEM_KIND_NUM && iPropertyKind <= MAX_RANK_ITEM_KIND_NUM))	// 700~799
		{
			iPropertyKind = -1;
		}
		else if(iPropertyKind >= MIN_WIN_TATTOO_KIND_NUM && iPropertyKind <= MAX_WIN_TATTOO_KIND_NUM)
		{	
			iPropertyKind = 0;
		}
		else if((iPropertyKind >= MIN_SPECIAL_ITEM_KIND_NUM && iPropertyKind <= MAX_SPECIAL_ITEM_KIND_NUM) || // 800~899
			((ShopItemInfo.iPropertyKind >= MIN_LINK_ITEM_PROPERTY_KIND_NUM && ShopItemInfo.iPropertyKind <= MAX_LINK_ITEM_PROPERTY_KIND_NUM) || ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC == ShopItemInfo.iPropertyKind) ||	// 920~939
			(iPropertyKind >= MIN_SECOND_BOOST_ITEM_KIND_NUM && iPropertyKind <= MAX_SECOND_BOOST_ITEM_KIND_NUM) ||								// 1000~1999
			(iPropertyKind >= MIN_ONE_GAME_BOOST_ITEM_KIND_NUM && iPropertyKind <= MAX_ONE_GAME_BOOST_ITEM_KIND_NUM) ||							// 2000~2999
			(iPropertyKind >= MIN_POSITION_POWER_UP_ITEM_KIND_NUM && iPropertyKind <= MAX_POSITION_POWER_UP_ITEM_KIND_NUM))						// 3000~3999

		{
			if(NO_STAT_SPECIAL_ITEM_KIND_NUM == iPropertyKind)
			{
				iPropertyKind = -1;
			}
			else
			{
				iPropertyKind = 1;
			}
		}

		int iUserItemPropertyNum = aItemList[i].iPropertyNum;

		if( iPropertyKind > 0 )
		{
			switch (iPropertyKind)
			{
			case CHANGE_HEIGHT_KIND_NUM:	
			case CHANGE_NAME_SPECIAL_KIND_NUM:
			case CHANGE_NAME_KIND_NUM:
			case MCHAT_MACRO_EAR_RING_KIND_NUM:
			case PROPERTY_KIND_PROPENSITY_ITEM:
			case ITEM_PROPERTY_KIND_HYBRID_STAT:
			case LICK_ITEM_KIND_NUM:
			case CASH_DISCOUNT_ITEM_KIND_NUM:
			case POINT_DISCOUNT_ITEM_KIND_NUM:
			case PROTECT_HAND1_ITEM_KIND_NUM:
			case PROTECT_HAND2_ITEM_KIND_NUM:
			case PROTECT_HAND3_ITEM_KIND_NUM:
			case PROTECT_HAND4_ITEM_KIND_NUM:
			case PROTECT_HAND5_ITEM_KIND_NUM:
			case PROTECT_HAND6_ITEM_KIND_NUM:
			case PROTECT_HAND7_ITEM_KIND_NUM:
			case CHANGE_ITEM_COLOR_KIND_NUM:
			case ITEM_PROPERTY_KIND_CHANNEL_BUFF:
			case ITEM_PROPERTY_KIND_GOLD_MASK:
			case ITEM_PROPERTY_KIND_PROPERTY_ADD_ITEM:
			case ITEM_PROPERTY_KIND_VOICE_SETTING_ITEM:
			case ITEM_PROPERTY_KIND_TOKEN:
			case ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN:
			case ITEM_PROPERTY_KIND_COMPOUNDING_ITEM:
			case ITEM_PROPERTY_KIND_CUSTOMIZE_ITEM:
			case ITEM_PROPERTY_KIND_EXP_BOX:
			case ITEM_PROPERTY_KIND_EXCHANGE_AVATAR_NAME:
			case ITEM_PROPERTY_KIND_CHANGE_BONUS_STAT:
			case RESET_SEASON_RECORD_ITEM_KIND_NUM:
			case RESET_TOTAL_RECORD_ITEM_KIND_NUM:
			case ITEM_PROPERTY_KIND_MOVE_POTENCARD:
			case ITEM_PROPERTY_KIND_SPECIAL_LEVELUP_PACKAGE:
				break;
			default:
				{
					if( PROPENSITY_ITEM_KIND_START <= iPropertyKind && iPropertyKind <= PROPENSITY_ITEM_KIND_END )
						break;
					if( MIN_BUFF_ITEM_KIND_NUM <= iPropertyKind && iPropertyKind <= MAX_BUFF_ITEM_KIND_NUM )
						break;
					if(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN <= iPropertyKind && iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
						break;
					if(ITEM_PROPERTY_KIND_DATEVALUE_MIN <= iPropertyKind && iPropertyKind <= ITEM_PROPERTY_KIND_DATEVALUE_MAX)
						break;
					if(MIN_PREMIUM_STAT_OPTION_ITEM_NUM <= iPropertyKind && iPropertyKind <= MAX_PREMIUM_STAT_OPTION_ITEM_NUM)	
						break;
					if(MIN_HEART_TATTO_KIND_NUM <= iPropertyKind && iPropertyKind <= MAX_HEART_TATTO_KIND_NUM)
						break;

					iPropertyKind = iUserItemPropertyNum;
				} break;
			}
		}

		PacketComposer.Add(iPropertyKind);
		int iPropertyDay = -1;
		int iPropertyHour = 0;
		int iPropertyMin = 0;
		if( ITEM_PROPERTY_KIND_DATEVALUE_MIN <= iPropertyKind && iPropertyKind <= ITEM_PROPERTY_KIND_DATEVALUE_MAX)
		{
			int iDiffSecond = difftime(aItemList[i].PropertyExpireDate, CurrentTime);
			iPropertyDay = iDiffSecond / 86400;
			iPropertyHour = (iDiffSecond ) / 3600;
3600;
			iPropertyMin = (iDiffSecond ) / 60;
 60;
		}
		PacketComposer.Add(iPropertyDay);
		PacketComposer.Add(iPropertyHour);
		PacketComposer.Add(iPropertyMin);

		BOOL bPCRoomItemStatOption = FALSE;
		if( iPropertyKind >= MIN_PREMIUM_STAT_OPTION_ITEM_NUM && iPropertyKind <= MAX_PREMIUM_STAT_OPTION_ITEM_NUM )	
		{
			bPCRoomItemStatOption = TRUE;
		}
		PacketComposer.Add(bPCRoomItemStatOption);

		int iInventoryButtonType = ShopItemInfo.iInventoryButtonType;
		if( ITEM_PROPERTY_KIND_CUSTOMIZE_ITEM == iPropertyKind )
		{
			S_ODBC_CustomizeLog	sLogInfo;
			if( TRUE == GetCustomizeItem_Received( aItemList[i].iItemIdx,sLogInfo))
			{
				iInventoryButtonType = INVENTORY_BUTTON_CHECK;
			}
		}

		PacketComposer.Add(iInventoryButtonType);		

		vector< SUserItemProperty > vUserItemProperty;
		if( aItemList[i].iPropertyKind == PREMIUM_ITEM_KIND_FLAME_MAN )
		{
			iUserItemPropertyNum = 0;
			PacketComposer.Add(&iUserItemPropertyNum);
		}
		else if (CASH_DISCOUNT_ITEM_KIND_NUM == aItemList[i].iPropertyKind)
		{
			PacketComposer.Add((int)1);
			PacketComposer.Add((int)CASH_DISCOUNT_ITEM_KIND_NUM);
			PacketComposer.Add(CFSGameServer::GetInstance()->GetDiscountRate(SELL_TYPE_CASH));
			PacketComposer.Add((int)-1);
		}
		else if (POINT_DISCOUNT_ITEM_KIND_NUM == aItemList[i].iPropertyKind)
		{
			PacketComposer.Add((int)1);
			PacketComposer.Add((int)POINT_DISCOUNT_ITEM_KIND_NUM);
			PacketComposer.Add(CFSGameServer::GetInstance()->GetDiscountRate(SELL_TYPE_POINT));
			PacketComposer.Add((int)-1);
		}
		else
		{
			if( iUserItemPropertyNum > 0 )
			{
				GetUserItemList()->GetCurAvatarItemList()->GetItemProperty(aItemList[i].iItemIdx, vUserItemProperty);
			}
			int iTempUserNum = vUserItemProperty.size();

			iUserItemPropertyNum = iTempUserNum;

			PacketComposer.Add(&iUserItemPropertyNum);

			TRACE( " ?��? ?�이???�성 : ItemCode - 11866902, PropertyKind - 0, PropertyType - 18227200, PropertyNum - -858993460, vPropertyNum - -858993460 \n", 
 aItemList[i].iPropertyKind, aItemList[i].iPropertyType, iUserItemPropertyNum, vUserItemProperty.size() );

			for (int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size() ; iPropertyCount++ )
			{
				int iProperty	= vUserItemProperty[iPropertyCount].iProperty;
				int iValue		= vUserItemProperty[iPropertyCount].iValue;

				PacketComposer.Add(&iProperty);
				PacketComposer.Add(&iValue);
				PacketComposer.Add(&vUserItemProperty[iPropertyCount].iAssignChannel);
			}
		}

		PacketComposer.Add(GetUserItemList()->GetUserPropertyAccItemStatus(aItemList[i].iItemIdx, aItemList[i].iBigKind, aItemList[i].iStatus, aItemList[i].iPropertyKind));

		int iChangeItem = ShopItemInfo.iChangeItem;
		if( aItemList[i].iPropertyTypeValue != -1 )
		{
			iChangeItem = 0;
		}
		PacketComposer.Add(iChangeItem);

		int iStyleIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > GetUseStyleIndex() ? 1 : 4 ;

		PacketComposer.Add(CheckItemUseStyle(iStyleIndex, aItemList[i].iItemIdx));
		PacketComposer.Add(CheckItemUseStyle(iStyleIndex+1, aItemList[i].iItemIdx));
		PacketComposer.Add(CheckItemUseStyle(iStyleIndex+2, aItemList[i].iItemIdx));
		PacketComposer.Add(aItemList[i].iSpecialPartsIndex);
	}

	PacketComposer.Add(GetUserItemList()->GetDifferentUserPropertyAccItemStatus());

	Send(&PacketComposer);
}

BOOL CFSGameUser::CheckAndGetCloneIndex( Clone_Index& eIndex )
{
	CHECK_CONDITION_RETURN( 0 > eIndex || Max_Clone <= eIndex , FALSE );
	CHECK_CONDITION_RETURN( FALSE == m_bCloneCharacter , FALSE );

	return TRUE;
}

void CFSGameUser::ChangeBasicItem_Sex( int iSex )
{
	m_UserItem->ChangeBasicItem_Sex(iSex);
}

Clone_Index CFSGameUser::CheckCloneCharacter_Index( void )
{
	Clone_Index eIndex = Clone_Index_None;

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
	CHECK_CONDITION_RETURN( NULL == pAvatarInfo, eIndex);

	if( TRUE == m_bCloneCharacter )
	{
		if( pAvatarInfo->iFace == m_sCloneCharacterInfo[Clone_Index_Mine].iFaceID )
		{
			eIndex = Clone_Index_Mine;
		}
		else
		{
			eIndex = Clone_Index_Clone;
		}
	}
	else if( 0 < pAvatarInfo->iSpecialCharacterIndex ) // �ߴ����ΰ�
	{
		CAvatarCreateManager* pAvatarCreateManager = &AVATARCREATEMANAGER;
		CHECK_CONDITION_RETURN( NULL == pAvatarCreateManager, eIndex);

		SSpecialAvatarConfig* pAvatarConfig = pAvatarCreateManager->GetSpecialAvatarConfigByIndex(pAvatarInfo->iSpecialCharacterIndex);
		CHECK_CONDITION_RETURN( NULL == pAvatarConfig, eIndex);

		m_AvatarStyleInfo.iStyleCount = pAvatarConfig->GetMultipleCharacterCount();

		CHECK_CONDITION_RETURN( 1 >= m_AvatarStyleInfo.iStyleCount , eIndex );
		int iIndex = pAvatarConfig->GetOrderNumber(pAvatarInfo->iFace);

		CHECK_CONDITION_RETURN( Clone_Index_Mine > iIndex || Clone_Index_Clone < iIndex , eIndex); // 0 ~ 1
		eIndex = static_cast<Clone_Index>(iIndex);
	}

	return eIndex;
}

void CFSGameUser::CheckAndSaveStyleInfo()
{
	// �ѱ��� Ŭ���� ��쿡��..
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	if( TRUE == m_bCloneCharacter )
	{
		if(m_sCloneCharacterInfo[Clone_Index_Mine].btStyleIndex[Clone_Index_Mine] != m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Mine] ||
			m_sCloneCharacterInfo[Clone_Index_Mine].btStyleIndex[Clone_Index_Clone] != m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Clone])
		{
			if( ODBC_RETURN_SUCCESS != pODBC->CLONE_CHARACTER_ChangeStyleIndex( GetGameIDIndex()
				, m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Mine]
			,m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Clone]))
			{
				WRITE_LOG_NEW(LOG_TYPE_AVATAR, SET_DATA, FAIL, "CheckAndSaveStyleInfo - CLONE_CHARACTER_ChangeStyleIndex, GameIDIndex:11866902, Style1 : 0, Style2 : 18227200"
ameIDIndex(),	m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Mine], m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Clone]);
			}
			else
			{
				m_sCloneCharacterInfo[Clone_Index_Mine].SetStyleIndex( 
					m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Mine]
					,m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Clone]);
			}
		}
	}

	if (m_AvatarStyleInfo.iUseStyleOnDB != m_AvatarStyleInfo.iUseStyle)
	{
		if(ODBC_RETURN_SUCCESS != pODBC->AVATAR_ChangeUseStyleIndex(GetGameIDIndex(), m_AvatarStyleInfo.iUseStyle))
		{
			WRITE_LOG_NEW(LOG_TYPE_AVATAR, SET_DATA, FAIL, "CheckAndSaveStyleInfo - AVATAR_ChangeUseStyleIndex, GameIDIndex:11866902", GetGameIDIndex());
	else
		{
			m_AvatarStyleInfo.iUseStyleOnDB = m_AvatarStyleInfo.iUseStyle;
		}
	}
}

int CFSGameUser::GetCloneCharacterStyleIndex( Clone_Index eIndex )
{
	CHECK_CONDITION_RETURN( FALSE == m_bCloneCharacter, 1 );
	int iUseStyle = 1;
	
	if( Clone_Index_Clone == eIndex )
	{
		iUseStyle = (int)m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Clone];
	}
	else
	{
		iUseStyle = (int)m_sCloneCharacterInfo[Clone_Index_Clone].btStyleIndex[Clone_Index_Mine];
	}

	return iUseStyle;
}

void CFSGameUser::SetCloneCharacterDefaultSeremony(int iUseStyle)
{
	CActionShop* pActionShop = CFSGameServer::GetInstance()->GetActionShop();
	CHECK_NULL_POINTER_VOID(pActionShop);

	memset(m_sCloneCharacterInfo[Clone_Index_Mine].iDefaultCeremony, -1, sizeof(int) * MAX_USER_ACTION_SEREMONY_COUNT);
	memset(m_sCloneCharacterInfo[Clone_Index_Clone].iDefaultCeremony, -1, sizeof(int) * MAX_USER_ACTION_SEREMONY_COUNT);

	vector<int> vecCeremonyCode;
	//vecCeremonyCode.reserve(MAX_USER_ACTION_SEREMONY_COUNT);
	pActionShop->CopyCloneCharacterDefaultSeremony(m_sCloneCharacterInfo[Clone_Index_Mine].iSpecialAvatarIndex,vecCeremonyCode);
	for( size_t iIndex = 0 ; iIndex < vecCeremonyCode.size() && iIndex < MAX_USER_ACTION_SEREMONY_COUNT ; ++iIndex ) 
	{
		m_sCloneCharacterInfo[Clone_Index_Mine].iDefaultCeremony[iIndex] = vecCeremonyCode[iIndex];
	}

	vecCeremonyCode.clear();
	pActionShop->CopyCloneCharacterDefaultSeremony(m_sCloneCharacterInfo[Clone_Index_Clone].iSpecialAvatarIndex,vecCeremonyCode);
	for( size_t iIndex = 0 ; iIndex < vecCeremonyCode.size() && iIndex < MAX_USER_ACTION_SEREMONY_COUNT ; ++iIndex ) 
	{
		m_sCloneCharacterInfo[Clone_Index_Clone].iDefaultCeremony[iIndex] = vecCeremonyCode[iIndex];
	}

	ChangeCloneDefaultSeremony(iUseStyle);
}

void CFSGameUser::ChangeCloneDefaultSeremony(int iUseStyle, bool bChangeStyle)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == m_bCloneCharacter );
	int iIndex_Remove, iIndex_Add;
	iIndex_Remove = CLONE_CHARACTER_DEFULAT_STYLE_INDEX <= iUseStyle ? Clone_Index_Clone : Clone_Index_Mine;

	if( true == bChangeStyle )
		iIndex_Add = iIndex_Remove == Clone_Index_Clone ?  Clone_Index_Clone : Clone_Index_Mine;
	else
		iIndex_Add = iIndex_Remove == Clone_Index_Clone ?  Clone_Index_Mine : Clone_Index_Clone;

	bool bCheckChagne = false;
	for( int i = 0 ; i < MAX_USER_ACTION_SEREMONY_COUNT ; ++i )
	{
		// ����δ� ������ ������� ������ ����.. 
		if( -1 < m_sCloneCharacterInfo[iIndex_Remove].iDefaultCeremony[i] && -1 < m_sCloneCharacterInfo[iIndex_Add].iDefaultCeremony[i] )
		{
			int iMineCeremony = m_sCloneCharacterInfo[Clone_Index_Mine].iDefaultCeremony[i];
			this->m_pUserAction->ChangeCloneCeremony( iMineCeremony, m_sCloneCharacterInfo[iIndex_Add].iDefaultCeremony[i]);

			bCheckChagne = true;
		}		
	}

	//if( true == bCheckChagne )
		//SendActionInventory();
}

int CFSGameUser::GetCloneCeremonyList( int iActionCode, int& iMineCeremony, int& iCloneCeremony )
{
	int iReturnValue = -1;
	iMineCeremony = iCloneCeremony = -1;
	CHECK_CONDITION_RETURN( FALSE == GetCloneCharacter(), -1 );
	CHECK_CONDITION_RETURN( Clone_Index_None == CheckCloneCharacter_Index(), -1 );

	for( int i = 0 ; i < MAX_USER_ACTION_SEREMONY_COUNT ; ++i )
	{
		if( iActionCode == m_sCloneCharacterInfo[Clone_Index_Mine].iDefaultCeremony[i]  ||
			iActionCode == m_sCloneCharacterInfo[Clone_Index_Clone].iDefaultCeremony[i])
		{
			iMineCeremony = m_sCloneCharacterInfo[Clone_Index_Mine].iDefaultCeremony[i];
			iCloneCeremony = m_sCloneCharacterInfo[Clone_Index_Clone].iDefaultCeremony[i];

			iReturnValue = Clone_Index_Mine == CheckCloneCharacter_Index() ? iMineCeremony : iCloneCeremony ;
			break;
		}
	}

	return iReturnValue;
}

void CFSGameUser::UpdateHeightAndStatus( Clone_Index eIndex )
{
	if( eIndex <= Clone_Index_None || Max_Clone <= eIndex )
		return;

	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);


	m_sCloneCharacterInfo[eIndex].Status = pAvatarInfo->Status;
	m_sCloneCharacterInfo[eIndex].SetAddStat();

}

void CFSGameUser::SetCloneCharacterAddStat( int iIndex )
{
	m_sCloneCharacterInfo[Clone_Index_Mine].SetAddStat(iIndex);
	m_sCloneCharacterInfo[Clone_Index_Clone].SetAddStat(iIndex);
}

void CFSGameUser::RemoveChangeDressItem( void )
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CAvatarItemList *pAvatarItemList = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iSize = pAvatarItemList->GetSize();

	vector<SUserItemInfo*>	vecItemList;
	vecItemList.reserve(iSize);
	pAvatarItemList->GetAllItemList(vecItemList);

	for( size_t iIndex = 0 ; iIndex < vecItemList.size() ; ++iIndex )
	{
		SUserItemInfo* pUserItem = vecItemList[iIndex];
		if( NULL != pUserItem )
		{
			if(pUserItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pUserItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)				
			{
				GetUserItemList()->RemoveItemWithItemIdx(pUserItem->iItemIdx);
			}
		}
	}
}

void CFSGameUser::SendFactionRaceStatus()
{
	SS2C_FACTION_RACE_STATUS_NOT not;
	not.btFactionRaceStatus = FACTION.GetRaceStatus();
	not.btMyFactionIndex = GetUserFaction()->_iFactionIndex;
	Send(S2C_FACTION_RACE_STATUS_NOT, &not, sizeof(SS2C_FACTION_RACE_STATUS_NOT));
}

void CFSGameUser::ClearUserFactionInfo()
{
	m_UserFactionInfo.InitializeSeason();
	SendAvatarInfoUpdateToMatch();
}

void CFSGameUser::AllDistrictUseBuffItem(time_t tBuffEndTime)
{
	if(TRUE == FACTION.AllDistrictUseBuffItem(this, tBuffEndTime))
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
		if(pCenter != NULL) 
		{
			CPacketComposer Packet(G2S_FACTION_USE_BUFFITEM_NOT);
			SG2S_FACTION_USE_BUFFITEM_NOT not;
			not.iType = 1;
			not.iFactionIndex = GetUserFaction()->_iFactionIndex;
			Packet.Add((BYTE*)&not, sizeof(SG2S_FACTION_USE_BUFFITEM_NOT));
			pCenter->Send(&Packet);
		}
		FACTION.SendFactionRaceBoardInfo(this);
	}
}

BOOL CFSGameUser::GetTodayHotDealRes( SS2C_EVENT_TODAY_HOTDEAL_INFO_RES& rs )
{
	int iSaleDate = 0;
	SToDayHotDealInfo sInfo;
	time_t tCurrentDate = _time64(NULL);

	CHECK_CONDITION_RETURN(FALSE == TODAYHOTDEALManager.GetTodayHotDealItemList(iSaleDate, sInfo, TimetToYYYYMMDD(tCurrentDate)), FALSE);

	if(m_UserHotDealCount._btSeason != TODAYHOTDEALManager.GetTodayHotDealSeason(iSaleDate) )
		HotDealItemGetLimitCount();

	rs.tSeasonOpenDate = YYYYMMDDHHMMSSToTimeT(iSaleDate, 0);
	rs.tSeasonCloseDate = rs.tSeasonOpenDate + (60*60*24 * HOTDEAL_PERIOD_DAY) - 1;
	rs.btSeason = sInfo.btSeason;

	int iRemainSec = difftime(rs.tSeasonCloseDate, tCurrentDate);
	rs.sRemainTime = (iRemainSec > 0) ? ((iRemainSec / 3600) * 100) + ((iRemainSec ) / 60) : 0;
: 0;

	for(int i = 0; i < HOTDEAL_ITEM_CNT; ++i)
	{
		rs.sItem[i].iItemCode = sInfo.sItem[i].iItemCode;
		rs.sItem[i].iPrice = sInfo.sItem[i].iPrice;
		rs.sItem[i].iSalePrice = sInfo.sItem[i].iSalePrice;
		rs.sItem[i].iLimitCount = m_UserHotDealCount._iTodayRemainCount[i];
		rs.sItem[i].iMaxLimitCount = sInfo.sItem[i].iLimitCount;
		rs.sItem[i].iPropertyType = sInfo.sItem[i].iPropertyType;
		rs.sItem[i].iPropertyValue = sInfo.sItem[i].iPropertyValue;
		rs.sItem[i].iPropertyKind = sInfo.sItem[i].iPropertyKind;
		rs.sItem[i].btViewOriginPrice = sInfo.sItem[i].btViewOriginPrice;
	}
	
	return TRUE;
}

void CFSGameUser::HotDealItemCheckAndUpdateLimitCount( int iItemCode, int iPropertyValue )
{
	int iSaleDate = 0;
	SHotDealItem sInfo;
	if( TRUE == TODAYHOTDEALManager.GetTodayHotDealItem(iItemCode, iPropertyValue, iSaleDate, sInfo) )
	{
		if( sInfo.iItemCode <= iItemCode && sInfo.iItemCode +4  >= iItemCode )
		{//update
			CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID(pODBCBase);

			if( ODBC_RETURN_SUCCESS != pODBCBase->TODAYHOTDEAL_UpdateLimitCount(GetUserIDIndex(), TODAYHOTDEALManager.GetTodayHotDealSeason(iSaleDate), sInfo.btIdx, m_UserHotDealCount._iTodayRemainCount[sInfo.btIdx]) )
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "TODAYHOTDEAL_UpdateLimitCount - UserIDIndex:11866902,  iSaleDate:0,  Count  :18227200"
erIDIndex(), iSaleDate, m_UserHotDealCount._iTodayRemainCount[sInfo.btIdx]);
				return;
			}
			
			SS2C_EVENT_TODAY_HOTDEAL_INFO_RES not;
			if( TRUE == GetTodayHotDealRes(not) )
			{
				CPacketComposer Packet(S2C_EVENT_TODAY_HOTDEAL_INFO_RES);
				Packet.Add((PBYTE)&not, sizeof(SS2C_EVENT_TODAY_HOTDEAL_INFO_RES));

				Send(&Packet);

				CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
				CHECK_NULL_POINTER_VOID(pItemShop);
				
				SShopItemInfo sShopInfo;
				if( TRUE == pItemShop->GetItem(sInfo.iItemCode, sShopInfo) )
				{
					if( PREMIUM_ITEM_KIND_POWERUP == sShopInfo.iPropertyKind ||
						PREMIUM_ITEM_KIND_POWERUP_PLUS == sShopInfo.iPropertyKind  ||
						BIND_ACCOUNT_ITEM_KIND_VIP == sShopInfo.iPropertyKind ||
						BIND_ACCOUNT_ITEM_KIND_VIP_PLUS == sShopInfo.iPropertyKind )
					{
						SendExposeUserItem();
					}
				}
			}
		}
	}
}

void CFSGameUser::HotDealItemGetLimitCount( )
{
	int iSaleDate = 0;
	SToDayHotDealInfo sInfo;
	if( TRUE == TODAYHOTDEALManager.GetTodayHotDealItemList(iSaleDate, sInfo) )
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);

		if( ODBC_RETURN_SUCCESS != pODBCBase->TODAYHOTDEAL_GetUserData(GetUserIDIndex(), sInfo.btSeason, m_UserHotDealCount) )
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "TODAYHOTDEAL_GetUserData - UserIDIndex:11866902,  SaleDate:0", GetUserIDIndex(), iSaleDate );
turn;
		}	
		
		m_UserHotDealCount._btSeason = sInfo.btSeason;

		CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
		CHECK_NULL_POINTER_VOID(pItemShop);

		SShopItemInfo	sShopItemInfo;

		for(int iIdx = 0; iIdx < HOTDEAL_ITEM_CNT; ++iIdx)
		{
			if(FALSE == pItemShop->GetItem(sInfo.sItem[iIdx].iItemCode, sShopItemInfo))
				continue;

			if (CHARACTER_SLOT_TYPE_SPECIAL == sShopItemInfo.iCharacterSlotType ||
				CHARACTER_SLOT_TYPE_CHANGE_AVATAR == sShopItemInfo.iCharacterSlotType)
			{
				if( ODBC_RETURN_SUCCESS != pODBCBase->TODAYHOTDEAL_UpdateLimitCountStatus(GetUserIDIndex(), m_UserHotDealCount._btSeason, iIdx, TODAYHOTDEAL_USER_STATUS_None) )
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "TODAYHOTDEAL_UpdateLimitCountStatus1 - UserIDIndex:11866902,  SaleDate:0,  Status  :18227200"
serIDIndex(), iSaleDate, m_UserHotDealCount._btAvatarBuyStatus[iIdx] );
				}
			}
		}
	}
}

void CFSGameUser::SendExposeUserItem( void )
{
	CFSGameUserItem* pUserItemList = GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList* pItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pItemList);

	int iDay = 0, iHour = 0, iMin = 0;
	time_t CurrentTime = _time64(NULL);

	CPacketComposer PacketComposer(S2C_EXPOSE_USERITEM_RET);

	// VIP
	SBindAccountData BindAccountData = GetBindAccountData();
	PacketComposer.Add(BindAccountData.iBindAccountItemPropertyKind);

	if(BindAccountData.iBindAccountItemPropertyKind > DONT_HAVE_VPI_ITEM)
	{
		int iDiffSecond = difftime(BindAccountData.ExpireDate, CurrentTime);	

		iDay = iDiffSecond / 86400;
		iHour = (iDiffSecond ) / 3600;
3600;
		iMin = (iDiffSecond ) / 60;
 60;

		PacketComposer.Add(iDay);
		PacketComposer.Add(iHour);
		PacketComposer.Add(iMin);
	}

	// ?�워??
	SUserItemInfo* pItem = pItemList->GetPowerUpItem();
	if(pItem != NULL)
	{
		iDay = 0, iHour = 0, iMin = 0;

		int iDiffSecond = difftime(pItem->ExpireDate, CurrentTime);

		iDay = iDiffSecond / 86400;
		iHour = (iDiffSecond ) / 3600;
3600;
		iMin = (iDiffSecond ) / 60;
 60;

		PacketComposer.Add(pItem->iPropertyKind);
		PacketComposer.Add(iDay);
		PacketComposer.Add(iHour);
		PacketComposer.Add(iMin);
		PacketComposer.Add(pItem->iSellPrice);
		PacketComposer.Add(pItem->iPropertyNum);

		vector< SUserItemProperty > vUserItemProperty;
		pItemList->GetItemProperty(pItem->iItemIdx, vUserItemProperty);
		for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
		{
			int iProperty	= vUserItemProperty[iPropertyCount].iProperty;
			int iValue		= vUserItemProperty[iPropertyCount].iValue;

			PacketComposer.Add(&iProperty);
			PacketComposer.Add(&iValue);
		}
	}
	else
	{
		PacketComposer.Add(-1);
	}
	//BuffItem
	for( int iPropertyKind = BUFF_ITEM_KIND_EXP; iPropertyKind <= BUFF_ITEM_KIND_STATUS; iPropertyKind ++ )
	{
		SUserItemInfo* pBuffItem = pItemList->GetBuffItem(iPropertyKind);
		if(NULL != pBuffItem && pBuffItem->iStatus != ITEM_STATUS_EXPIRED && pBuffItem->iStatus != ITEM_STATUS_PRESENTBOX )
		{
			iDay = 0, iHour = 0, iMin = 0;

			int iDiffSecond = difftime(pBuffItem->ExpireDate, CurrentTime);

			if( iDiffSecond < 0 )
			{
				PacketComposer.Add(-1);
			}
			else
			{
				PacketComposer.Add(pBuffItem->iPropertyKind);
				iDay = iDiffSecond / 86400;
				iHour = (iDiffSecond ) / 3600;
3600;
				iMin = (iDiffSecond ) / 60;
 60;

				PacketComposer.Add(iDay);
				PacketComposer.Add(iHour);
				PacketComposer.Add(iMin);
				PacketComposer.Add(pBuffItem->iPropertyNum);

				vector< SUserItemProperty > vUserItemProperty;
				pItemList->GetItemProperty(pBuffItem->iItemIdx, vUserItemProperty);
				for(int iPropertyCount = 0; iPropertyCount < vUserItemProperty.size(); iPropertyCount++)
				{
					int iProperty	= vUserItemProperty[iPropertyCount].iProperty;
					int iValue		= vUserItemProperty[iPropertyCount].iValue;

					PacketComposer.Add(&iProperty);
					PacketComposer.Add(&iValue);
				}
			}

		}
		else
		{
			PacketComposer.Add(-1);
		}
	}

	// PC�?
	PacketComposer.Add(GetPCRoomKind());

	Send(&PacketComposer);
}

BOOL CFSGameUser::CheckItemAcceptProperty( int iMailType, int iPresentIndex, int* aiPropertyIndex )
{
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SPresent* present = GetPresentList(iMailType)->Find(iPresentIndex);
	CHECK_NULL_POINTER_BOOL(present);
	CHECK_NULL_POINTER_BOOL(aiPropertyIndex);

	int aiPropertyIndex_CloneData[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1};
	bool bCheckEmptyPropertyIndex = false;

	for( int i = 0 ; i < MAX_ITEM_PROPERTYINDEX_COUNT ; ++i )
	{
		aiPropertyIndex_CloneData[i] = aiPropertyIndex[i];

		if( 0 < aiPropertyIndex[i] )
			bCheckEmptyPropertyIndex = true;	
	}

	CHECK_CONDITION_RETURN( false == bCheckEmptyPropertyIndex , TRUE );
	CHECK_CONDITION_RETURN( ITEM_PROPERTY_KIND_TWELVE_PROPERTY_ACC == present->iPropertyKind, TRUE);

	SShopItemInfo shopItem;		
	if( TRUE == pItemShop->GetItem(present->iItemCode, shopItem) )
	{
		CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(shopItem.iPropertyKind);
		if(pItemPropertyBoxList != NULL && 
			(shopItem.iPropertyKind < MIN_EXP_UP_ITEM_KIND_NUM || shopItem.iPropertyKind > MAX_EXP_UP_ITEM_KIND_NUM) &&
			(shopItem.iPropertyKind < MIN_LEVEL_UP_ITEM_KIND_NUM || shopItem.iPropertyKind > MAX_LEVEL_UP_ITEM_KIND_NUM))
		{
			int iPropertyIndex = pItemPropertyBoxList->GetPropertyIndexByLv(GetCurUsedAvtarLv());
			CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList(iPropertyIndex);
			CHECK_NULL_POINTER_BOOL(pItemPropertyList);
			CItemPropertyPriceList *pPropertyPriceList = pItemShop->GetItemPropertyPriceList(shopItem.iPropertyKind);
			CHECK_NULL_POINTER_BOOL(pPropertyPriceList);

			list<int>* pListPropertyAssignType = pPropertyPriceList->GetPropertyAssignTypeList();
			CHECK_NULL_POINTER_BOOL(pListPropertyAssignType);

			int iUserLv = GetCurUsedAvtarLv();

			list<int>::iterator		iter = pListPropertyAssignType->begin();
			list<int>::iterator		endtier = pListPropertyAssignType->end();

			int iArrayIndex = 0;
			for( ; iter != endtier; ++iter )
			{
				int iPropertyAssignType = *iter;
				int iMaxBoxIdx = pItemPropertyBoxList->GetMaxBoxIdxCount( iUserLv, iPropertyAssignType);
				
				for(int iBoxIdx = 0; iBoxIdx < iMaxBoxIdx ; iBoxIdx++)
				{
					int iMaxCategoryCount = pItemPropertyBoxList->GetMaxCategoryCount(iBoxIdx, iUserLv, iPropertyAssignType );
					vector< SItemPropertyBox > vItemPropertyBox;
					short sSmallMaxNum = (short)pItemPropertyBoxList->GetItemPropertyBoxIdxCount(iBoxIdx, iUserLv, iPropertyAssignType); 
					pItemPropertyBoxList->GetItemPropertyBox( iBoxIdx, iUserLv, iPropertyAssignType, vItemPropertyBox );
					sort( vItemPropertyBox.begin(), vItemPropertyBox.end(), PropertyBoxSort() );

					int iAssignChannel = 0;
					if (!vItemPropertyBox.empty())	iAssignChannel = vItemPropertyBox[0].iAssignChannel;
					short sVectorSize = (short)vItemPropertyBox.size();

					for(int i = 0; i < vItemPropertyBox.size() ; i++) 
					{
						CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList(vItemPropertyBox[i].iPropertyIndex);
						CHECK_NULL_POINTER_BOOL(pItemProperty);
						int iMaxPropertyNum = pItemProperty->GetPropertyMaxCount();
						SItemPropertyBox* pItemPropertyBox = pItemPropertyBoxList->GetItemPropertyBox(vItemPropertyBox[i].iCategory, iBoxIdx, iUserLv, iPropertyAssignType);
						CHECK_NULL_POINTER_BOOL(pItemPropertyBox);
												
						for(int iPropertyCount = 0; iPropertyCount < iMaxPropertyNum ; iPropertyCount++)
						{
							vector< SItemProperty > vItemProperty;
							pItemProperty->GetItemProperty( vItemProperty );
							sort( vItemProperty.begin(), vItemProperty.end(), PropertySort() );

							for( int i = 0 ; i < MAX_ITEM_PROPERTYINDEX_COUNT ; ++i )
							{
								if( vItemProperty[iPropertyCount].iPropertyIndex == aiPropertyIndex_CloneData[i] )
									aiPropertyIndex_CloneData[i] = -1;
							}
						}//for
					}//for
				}//for
			}
		}

		for( int i = 0 ; i < MAX_ITEM_PROPERTYINDEX_COUNT ; ++i )
		{
			if( 0 < aiPropertyIndex_CloneData[i] )
				return FALSE;
		}
	}

	return TRUE;
}

void CFSGameUser::AddLobbyClubUser()
{
	LOBBYCLUBUSER.AddUser(this);
}

void CFSGameUser::RemoveLobbyClubUser()
{
	LOBBYCLUBUSER.RemoveUser(GetGameIDIndex());
}

void CFSGameUser::SendPotenCardExpiredInfoNot( void )
{
	CHECK_CONDITION_RETURN_VOID( true == m_bCheckPotenCardExpiredSend );
	SS2C_POTENCARD_EXPIRED_INFO_NOT	not;
	
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SYSTEMTIME CurrentTime;
	::GetLocalTime(&CurrentTime);

	vector<SSlotData> vecEquipProductData;
	GetExpiredEquipPotenCard(vecEquipProductData);

	SProductData sProductData;
	int iMinute = 0;

	CHECK_CONDITION_RETURN_VOID( 0 == vecEquipProductData.size() );
	
	map<int /* propertyvalue */, SPOTENCARD_PRICE_INFO> mapPotenCardPrice;
	
	GetExpiredEquipPotenCardPriceList(vecEquipProductData, mapPotenCardPrice );

	CPacketComposer Packet(S2C_POTENCARD_EXPIRED_INFO_NOT);

	not.btExpiredPriceCount = (BYTE)mapPotenCardPrice.size();
	not.btItemCount = (BYTE)vecEquipProductData.size();

	Packet.Add((PBYTE)&not, sizeof(SS2C_POTENCARD_EXPIRED_INFO_NOT));

	for( map<int ,SPOTENCARD_PRICE_INFO>::iterator iter = mapPotenCardPrice.begin() ; 
		iter != mapPotenCardPrice.end() ; ++iter )
	{
		Packet.Add((PBYTE)&(iter->second), sizeof(SPOTENCARD_PRICE_INFO));
	}
	
	SActionInfluenceConfig sActionInfluenceConfig;
	
	list<SACTION_INFLUNCE_INFO>	listActionInfluenceConfig; 
	iMinute = 0;
	for(int i = 0 ; i < vecEquipProductData.size(); i++)
	{
		m_ProductInventory.GetProductData(vecEquipProductData[i].iProductIndex, sProductData);

		SPOTENCARD_EQUIP_ITEM_INFO	sPotenCardExpiredEquipInfo;

		sPotenCardExpiredEquipInfo.iProductIndex = sProductData.iProductIndex;
		sPotenCardExpiredEquipInfo.iTendencyType = sProductData.iTendencyType;
		sPotenCardExpiredEquipInfo.iRarityLevel = sProductData.iRarityLevel;
		sPotenCardExpiredEquipInfo.iGradeLevel = sProductData.iGradeLevel;
		
		if( TERM_TYPE_LIMITED == sProductData.byTermType )
		{
			iMinute = CompareSystemTime(CurrentTime, sProductData.sExpireDate) / 60;
		}
		
		sPotenCardExpiredEquipInfo.iExpireMinute = iMinute;
		sPotenCardExpiredEquipInfo.bThreeKingdomCard = static_cast<BOOL>(sProductData.iSpecialCardIndex == SPECIALCARD_INDEX_THREEKINGDOM);;
			
		sPotenCardExpiredEquipInfo.btMainInfluenceCount = 0;
		listActionInfluenceConfig.clear();

		for (int iInfluenceIndex = 0; iInfluenceIndex < MAX_POTENCARD_MAIN_INFLUENCE_COUNT; ++iInfluenceIndex)
		{
			if (0 >= sProductData.iaActionInfluenceIndex[iInfluenceIndex])	
				continue;

			COACHCARD.GetActionInfluenceList()->FindAndGetActionInfluenceConfig(sProductData.iaActionInfluenceIndex[iInfluenceIndex],sActionInfluenceConfig);

			SACTION_INFLUNCE_INFO	sAIInfo;
			sAIInfo.iActionType = sActionInfluenceConfig.iActionType;
			sAIInfo.iInfluenceType = sActionInfluenceConfig.iInfluenceType;

			listActionInfluenceConfig.push_back(sAIInfo);
			++sPotenCardExpiredEquipInfo.btMainInfluenceCount;
		}

		SPotentialComponent sPotentialInfo;
		GetPotentialComponent( vecEquipProductData[i].iProductIndex, sPotentialInfo );
		sPotenCardExpiredEquipInfo.iPotentialComponentIndex = sPotentialInfo.eComponentIndex;

		Packet.Add((PBYTE)&sPotenCardExpiredEquipInfo, sizeof(SPOTENCARD_EQUIP_ITEM_INFO));

		for( list<SACTION_INFLUNCE_INFO>::iterator	iterActionInfluence = listActionInfluenceConfig.begin() ;
			iterActionInfluence != listActionInfluenceConfig.end() ; ++iterActionInfluence )
		{
			Packet.Add((PBYTE)&(*iterActionInfluence), sizeof(SACTION_INFLUNCE_INFO));
		}
	}

	m_bCheckPotenCardExpiredSend = true;
	Send(&Packet);
}

int CFSGameUser::PotenCardTermExtendforEquipCard( int iPropertyValue )
{
	int iResult = POTENCARD_TERM_EXTEND_RESULT_SUCCESS;

	CFSODBCBase* pODBCBase = static_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN( NULL == pODBCBase , POTENCARD_TERM_EXTEND_RESULT_FAIL);
	
	SAvatarInfo* pAvatarInfo = m_AvatarManager.GetCurUsedAvatar();
	CHECK_CONDITION_RETURN( NULL == pAvatarInfo, POTENCARD_TERM_EXTEND_RESULT_FAIL);

	CFSGameUserSkill* pUserSkill = GetUserSkill();
	CHECK_CONDITION_RETURN( NULL == pUserSkill, POTENCARD_TERM_EXTEND_RESULT_FAIL);

	vector<SSlotData> vecEquipProductData;
	GetExpiredEquipPotenCard(vecEquipProductData);
	SProductData sProductData;
	
	map<int /* propertyvalue */, SPOTENCARD_PRICE_INFO> mapPotenCardPrice;
	GetExpiredEquipPotenCardPriceList(vecEquipProductData, mapPotenCardPrice);

	map<int , SPOTENCARD_PRICE_INFO>::iterator	iter = mapPotenCardPrice.find( iPropertyValue );
	CHECK_CONDITION_RETURN( iter == mapPotenCardPrice.end() , POTENCARD_TERM_EXTEND_RESULT_FAIL );

	SPOTENCARD_PRICE_INFO*	pPriceInfo = &(iter->second);
	CHECK_CONDITION_RETURN( NULL == pPriceInfo, POTENCARD_TERM_EXTEND_RESULT_FAIL );

	int iUserPoint = (pPriceInfo->iSellType == SELL_TYPE_POINT ? GetSkillPoint() : pPriceInfo->iSellType == SELL_TYPE_CASH ? GetCoin() : GetUserTrophy());
	CHECK_CONDITION_RETURN( pPriceInfo->iPrice > iUserPoint , POTENCARD_TERM_EXTEND_RESULT_NOT_ENOUGH_POINT);

	for( size_t iIndex = 0 ; iIndex < vecEquipProductData.size() ; ++iIndex )
	{
		m_ProductInventory.GetProductData(vecEquipProductData[iIndex].iProductIndex , sProductData);

		vector<STermExtend*> vecTermExtend;
		CHECK_CONDITION_RETURN( FALSE == COACHCARD.GetCoachCardTermExtendConfig(sProductData.iGradeLevel,vecTermExtend) , POTENCARD_TERM_EXTEND_RESULT_FAIL );

		const int cnMaxEquipCardCount = 3;
		for( size_t i = 0 ; i <  vecTermExtend.size() && i < cnMaxEquipCardCount ; ++i )
		{
			if( vecTermExtend[i]->iTermExtend == iPropertyValue && vecTermExtend[i]->iSellType == pPriceInfo->iSellType )
			{
				SDateInfo sDateInfo;
				int iPrice = vecTermExtend[i]->iPrice;
				if (ODBC_RETURN_SUCCESS == pODBCBase->ITEM_ExtendTerm(GetUserIDIndex(),GetGameIDIndex(), sProductData.iProductIndex ,iPrice, iPropertyValue,sDateInfo))
				{
					if( SELL_TYPE_POINT == pPriceInfo->iSellType )
						SubBuyCost(pPriceInfo->iSellType, iPrice);

					int iRemainTime = 0;
					m_ProductInventory.UpdateProductExpireDate(sProductData.iProductIndex , sDateInfo, iRemainTime);

					CheckEquipPotentialProductSetting( sProductData.iProductIndex );

					CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());

					if( NULL != pMatch )
					{
						// ActionInfluence list
						vector<int> vecEquipActionInfluenceIndex;
						GetEquipActionInfluence(sProductData.iProductIndex, vecEquipActionInfluenceIndex);

						// Potential list
						SPotentialComponent sPotentialInfo;
						SPotentialComponentConfig sPotentialConfigInfo;
						GetPotentialComponent(sProductData.iProductIndex, sPotentialInfo);
						COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

						SSlotData sSlotData;
						sSlotData.iProductIndex = sProductData.iProductIndex;
						sSlotData.iTendencyType = sProductData.iTendencyType;

						SG2M_EQUIP_PRODUCT_NOT req;
						req.iGameIDIndex = GetGameIDIndex();
						req.SlotData = sSlotData;

						CPacketComposer PacketComposer(G2M_EQUIP_PRODUCT_NOT);
						PacketComposer.Add((BYTE*)&req, sizeof(SG2M_EQUIP_PRODUCT_NOT));

						PacketComposer.Add(vecEquipActionInfluenceIndex.size());
						for(int i = 0; i < vecEquipActionInfluenceIndex.size(); i++)
						{
							PacketComposer.Add(vecEquipActionInfluenceIndex[i] );
						}

						PacketComposer.Add((int)sPotentialConfigInfo.eComponentIndex);
						PacketComposer.Add(sPotentialConfigInfo.iRelateIndex);
						PacketComposer.Add(sPotentialConfigInfo.iParam);

						pMatch->Send(&PacketComposer);

						if( POTENTIAL_COMPONENT_ABILITY == sPotentialConfigInfo.eComponentIndex )
						{
							SendAvatarInfoUpdateToMatch();
						}
						else if( POTENTIAL_COMPONENT_ACTION == sPotentialConfigInfo.eComponentIndex )
						{
							//pMatch->SendActionSlot(this);	
							SG2M_ACTION_SLOT_UPDATE info;
							SG2M_BASE infobase;

							infobase.iGameIDIndex = info.iGameIDIndex = GetGameIDIndex();
							infobase.btServerIndex = info.btServerIndex = _GetServerIndex;
							
							CheckExpireAction(GetGameIDIndex(), CFSGameServer::GetInstance()->GetActionShop());
							CopyActionSlotPacket(pODBCBase, CFSGameServer::GetInstance()->GetActionShop(), info);

							pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ACTION_SLOT, &info, sizeof(SG2M_ACTION_SLOT_UPDATE));
							pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TEAM_MEMBER_LIST_REQ, &infobase, sizeof(SG2M_BASE));		
						}
						else if( POTENTIAL_COMPONENT_FREESTYLE == sPotentialConfigInfo.eComponentIndex )
						{
							SG2M_AVATAR_SKILL_INFO_UPDATE update;
							memcpy(&update.Skill, &pAvatarInfo->Skill, sizeof(SAvatarSkill));
							PacketComposer.Initialize(G2M_AVATAR_SKILL_UPDATE);
							PacketComposer.Add((BYTE*)&update, sizeof(SG2M_AVATAR_SKILL_INFO_UPDATE));
							pUserSkill->WriteSpecialtySkill(PacketComposer, pAvatarInfo->Skill);

							pMatch->Send(&PacketComposer);
						}
					}
				}
				break;
			}
		}
	}

	SendUserGold();
	return iResult;
}

void CFSGameUser::GetExpiredEquipPotenCard( vector<SSlotData>& rvec )
{
	m_SlotPackageList.GetEquipProductDataAll(rvec);

	SYSTEMTIME CurrentTime;
	::GetLocalTime(&CurrentTime);
	SProductData sProductData;
	int iMinute = 0;

	vector<SSlotData>::iterator iter = rvec.begin();
	while( iter != rvec.end() )	// ����Ȱ��� �ֳ� Ȯ���մϴ�.
	{
		m_ProductInventory.GetProductData(iter->iProductIndex, sProductData);

		if( TERM_TYPE_LIMITED == sProductData.byTermType && 
			(CompareSystemTime(CurrentTime, sProductData.sExpireDate) / 60) > 0 )
		{
			iter = rvec.erase(iter);
		}
		else
			++iter;
	}
}

void CFSGameUser::GetExpiredEquipPotenCardPriceList( vector<SSlotData>& rvec, map<int /* TernmExtend */, SPOTENCARD_PRICE_INFO>& rmap )
{
	SProductData sProductData;
	for( size_t iIndex = 0 ; iIndex < rvec.size() ; ++iIndex )
	{
		m_ProductInventory.GetProductData(rvec[iIndex].iProductIndex, sProductData);

		vector<STermExtend*> vecTermExtend;
		if (FALSE == COACHCARD.GetCoachCardTermExtendConfig(sProductData.iGradeLevel ,vecTermExtend))
		{
			return;
		}

		for( size_t i = 0 ; i <  vecTermExtend.size() ; ++i )
		{
			map<int /* TernmExtend */, SPOTENCARD_PRICE_INFO>::iterator iter = rmap.find(vecTermExtend[i]->iTermExtend);

			if( iter == rmap.end())
			{
				SPOTENCARD_PRICE_INFO stPriceInfo;

				stPriceInfo.iPrice = vecTermExtend[i]->iPrice;
				stPriceInfo.iPropertyValue = vecTermExtend[i]->iTermExtend;
				stPriceInfo.iSellType = vecTermExtend[i]->iSellType;
				rmap.insert(  map<int ,SPOTENCARD_PRICE_INFO>::value_type(vecTermExtend[i]->iTermExtend, stPriceInfo) ) ;
			}
			else
			{
				iter->second.iPrice += vecTermExtend[i]->iPrice;
			}
		}	
	}
}

void CFSGameUser::SendPotenCardEventInfo()
{
	if( TRUE == COACHCARD.IsEventActive() )
	{
		if( FALSE == CheckEventButtonSend(EVENT_BUTTON_TYPE_POTENCARD, (BYTE)COACHCARD.IsEventActive()))
			return;

		CPacketComposer	Packet(S2C_POTENCARD_EVENT_OPEN_NOT);
		COACHCARD.MakePacketOpenPotenCardEventList(Packet);

		Send(&Packet);
	}
}

void CFSGameUser::CheckEquipPotentialProductSetting(int iProductIndex)
{
	// ����ÿ� ���ټ��� ������ �־�����մϴ�.
	CProduct* pProduct = m_ProductInventory.GetProduct(iProductIndex);
	CHECK_NULL_POINTER_VOID(pProduct);

	if( TRUE == pProduct->IsEquiped() ) // �������ΰ͸���.
	{
		PotentialProductSetting(iProductIndex, TRUE);
	}
}

void CFSGameUser::CheckCashItemBuyEvent(int iCashChange, BOOL bUseEventCoin, int iUseEventCoin, void* lParam)
{
	SetPayCash(iCashChange);
	if(bUseEventCoin == TRUE)
		SetPayCash(iCashChange + iUseEventCoin);

	CheckEvent(PERFORM_TIME_CASH_ITEMBUY, lParam);
}

void CFSGameUser::SendCheerLeaderEventStatus()
{
	SS2C_CHEERLEADER_EVENT_NOT info;
	info.btOpenStatus = CHEERLEADEREVENT.IsOpen();
	info.tRemainTime = CHEERLEADEREVENT.GetRemainTime();
	Send(S2C_CHEERLEADER_EVENT_NOT, &info, sizeof(SS2C_CHEERLEADER_EVENT_NOT));
}

BOOL CFSGameUser::CheckAndInitPayBackEvent( void )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	time_t tCurrentTime = _time64(NULL);
	time_t tCloseDate = 0;
	EVENTDATEMANAGER.GetEventEndDate(EVENT_KIND_PAYBACK,tCloseDate);
	
	CHECK_CONDITION_RETURN( tCurrentTime > tCloseDate , TRUE);

	if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_PAYBACK_GetUser(m_iUserIDIndex, m_sPayBackInfo ))
	{
		WRITE_LOG_NEW(LOG_TYPE_PAYBACK, DB_DATA_LOAD, FAIL, "EVENT_PAYBACK_GetUser  UserIDIndex : 11866902 " , m_iUserIDIndex );
rn FALSE;
	}

	return TRUE;
}

void CFSGameUser::MakeAndSendPaybackInfo( void )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PAYBACKEVENT.IsOpen());

	SS2C_EVENT_PAYBACK_INFO_RES	rs;
	ZeroMemory(&rs,sizeof(SS2C_EVENT_PAYBACK_INFO_RES));

	if( TRUE == PAYBACKEVENT.MakeResDataPaybackInfo(rs, m_sPayBackInfo) )
	{
		rs.iCumulativeCash = m_sPayBackInfo._iConsumptionCash; // ����ĳ��
		rs.iCashAcquired = m_sPayBackInfo._iReceivedCash;		// ����ĳ��

		Send(S2C_EVENT_PAYBACK_INFO_RES, &rs, sizeof( SS2C_EVENT_PAYBACK_INFO_RES ));
	}
}

void CFSGameUser::GiveAndSendUserWantPayback( void )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PAYBACKEVENT.IsOpen());

	SS2C_EVENT_PAYBACK_WANTPAYBACK_RES	rs;
	rs.iGiftCash = 0;
	rs.btResult = RESULT_PAYBACK_CUMULATIVECASH_LOW;

	if( TRUE == PAYBACKEVENT.CheckCashReceive(m_sPayBackInfo) )
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);
		int iPaidEventCash = 0;

		for( int iIndex = 0 ; iIndex < MAX_EVENT_PAYBACK_BOX_COUNT ; ++iIndex ) 
		{
			if( TRUE == PAYBACKEVENT.CheckPaidCash(m_sPayBackInfo , iIndex))
			{
				ODBCRETURN	iResult = ODBC_RETURN_FAIL;
				iResult = pODBCBase->EVENT_PAYBACK_UserEventCashPayment(GetUserIDIndex(), GetGameIDIndex(), iIndex, iPaidEventCash );
				if( ODBC_RETURN_SUCCESS == iResult )
				{
					rs.iGiftCash = iPaidEventCash;
					rs.btResult = RESULT_PAYBACK_SUCCESS;
					SetEventCoin(GetEventCoin() + iPaidEventCash, TRUE);

					// Send!
					Send(S2C_EVENT_PAYBACK_WANTPAYBACK_RES, &rs, sizeof(SS2C_EVENT_PAYBACK_WANTPAYBACK_RES));

					if( 0 < PAYBACKEVENT.GetAchievementIndexforPayBackIndex(iIndex) )
					{
						vector<int> vNewlyReachedAchievementIndex, vParameters, vAnnounceAchievementIndex;
						
						if (TRUE == ProcessAchievementbyGroup(ACHIEVEMENT_CONDITION_GROUP_PAYBACK_SPECIAL_USER, ACHIEVEMENT_LOG_GROUP_EVENT, vParameters, vNewlyReachedAchievementIndex))
						{
							ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_EVENT, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex);
							rs.btResult = RESULT_PAYBACK_SUCCESS_AND_GIVEACHIEVEMENT;
							Send(S2C_EVENT_PAYBACK_WANTPAYBACK_RES, &rs, sizeof(SS2C_EVENT_PAYBACK_WANTPAYBACK_RES));
						}
					}

					m_sPayBackInfo._iReceivedCash += iPaidEventCash;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_PAYBACK, DB_DATA_LOAD, FAIL
						, "EVENT_PAYBACK_UserEventCashPayment - UserIDIndex : 11866902,  GameIDIndex : 0,  iIndex : 18227200,  Result : -858993460 " 
dex(), GetGameIDIndex(), iIndex , iResult);
				}
			}
		}

		if( 0 < iPaidEventCash )
		{
			SendUserGold();
		}
	}

	if( RESULT_PAYBACK_CUMULATIVECASH_LOW == rs.btResult )
		Send(S2C_EVENT_PAYBACK_WANTPAYBACK_RES, &rs, sizeof(SS2C_EVENT_PAYBACK_WANTPAYBACK_RES));
}

void CFSGameUser::EventPayBackBuyItem( int iUseCash )
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == PAYBACKEVENT.IsOpen() );

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	int iUsedCashValue = 0;
	if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_PAYBACK_UpdateUser(GetUserIDIndex(), iUseCash, iUsedCashValue))
	{
		WRITE_LOG_NEW(LOG_TYPE_PAYBACK, EXEC_SP, FAIL, "Fail - EVENT_PAYBACK_UpdateUser  UserIDIndex : 11866902,  UseCash : 0" , GetUserIDIndex(), iUseCash );
urn;
	}
	else
	{
		m_sPayBackInfo._iConsumptionCash = iUsedCashValue;
	}
}

BOOL CFSGameUser::CheckAndInitGoldenCrushEvent( void )
{
	if( OPEN == GOLDENCRUSH.IsOpen() )
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_BOOL(pODBCBase);

		vector<int> vecCrushItemIndex;
		if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_GOLDENCRUSH_GetUser(GetUserIDIndex(), m_sGoldenCrushInfo._iGoldenGauge, vecCrushItemIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_GOLDENCRUSH, DB_DATA_LOAD, FAIL, "EVENT_GOLDENCRUSH_GetUser - UserIDIndex:11866902 " , m_iUserIDIndex );
urn FALSE;
		}	

		GOLDENCRUSH.UpdateAndInsertUserItemList(vecCrushItemIndex, &m_sGoldenCrushInfo._mapUserItemList);

		m_sGoldenCrushInfo._bSpecialReward = FALSE; 
		return TRUE;
	}

	return TRUE;
}

BOOL CFSGameUser::CheckAndSendGoldenCrushInfo( void )
{
	CHECK_CONDITION_RETURN( CLOSED == GOLDENCRUSH.IsOpen() , FALSE );

	SS2C_EVENT_GOLDENCRUSH_INFO_RES rs;
	const SGOLDENCRUSHCONFIG sGoldenCrushConfig = GOLDENCRUSH.GetGoldenCrushConfig();

	rs.btHammerCount = (BYTE)(GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GOLDENCRUSH_HAMMNER ));
	rs.iGoldenGauge = m_sGoldenCrushInfo._iGoldenGauge;
	rs.tEndDate = sGoldenCrushConfig._tCloseDate;

	int iUserRewardItemSize = m_sGoldenCrushInfo.GetRewardItemSize();
	if( sGoldenCrushConfig._iMinGoldenCardCount >= iUserRewardItemSize || iUserRewardItemSize > MAX_GOLDENCRUSH_SAVEINDEX )
	{
		m_sGoldenCrushInfo.InitializeUserItemList();
		if( GOLDENCRUSH.GetMaxCrushGauge() <= m_sGoldenCrushInfo._iGoldenGauge )
		{
			rs.btGoldenWonboCount = GOLDENCRUSH.GetMaxSpecialWonboCount();		
			
			//������ ������. ����ȸ���������.
			m_sGoldenCrushInfo._bSpecialReward = TRUE;
		}
		else
			rs.btGoldenWonboCount = GOLDENCRUSH.GetMaxWonboCount();
	}
	else
	{
		rs.btGoldenWonboCount = m_sGoldenCrushInfo.GetRewardItemSize();
	}

	Send(S2C_EVENT_GOLDENCRUSH_INFO_RES, &rs, sizeof(SS2C_EVENT_GOLDENCRUSH_INFO_RES));

	return TRUE;
}

void CFSGameUser::InitializeGoldenCrushAndSend( void )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	SS2C_EVENT_GOLDENCRUSH_INITIALIZE_RES	rs;
	if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_GOLDENCRUSH_Initialize( GetUserIDIndex() ) )
	{
		rs.iResult = RESULT_GOLDENCRUSH_INITIALIZE_FAIL;
		WRITE_LOG_NEW(LOG_TYPE_GOLDENCRUSH, DB_DATA_UPDATE, FAIL, "EVENT_GOLDENCRUSH_Initialize  UserIDIndex:11866902 " , GetUserIDIndex());
lse
	{
		rs.iResult = RESULT_GOLDENCRUSH_INITIALIZE_SUCCESS;
		m_sGoldenCrushInfo.InitializeUserItemList(); // 1
	}

	Send(S2C_EVENT_GOLDENCRUSH_INITIALIZE_RES, &rs, sizeof(SS2C_EVENT_GOLDENCRUSH_INITIALIZE_RES));

	//if( RESULT_GOLDENCRUSH_INITIALIZE_SUCCESS == rs.iResult )
	//	CheckAndSendGoldenCrushInfo(); // 2
}

void CFSGameUser::CrushEventGoldenCrush( BYTE btType , SS2C_EVENT_GOLDENCRUSH_CRUSH_RES& rs)
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	rs.iGoldenGauge = m_sGoldenCrushInfo._iGoldenGauge;
	rs.btHammerCount = (BYTE)(GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GOLDENCRUSH_HAMMNER ));
	int iCurrentWonboSize = m_sGoldenCrushInfo.GetRewardItemSize();
	
	const SGOLDENCRUSHCONFIG sGoldenCrushConfig = GOLDENCRUSH.GetGoldenCrushConfig();

	if( iCurrentWonboSize <= sGoldenCrushConfig._iMinGoldenCardCount &&
		m_sGoldenCrushInfo._iGoldenGauge >= sGoldenCrushConfig._iMaxGoldenGauge )
	{
		m_sGoldenCrushInfo._bSpecialReward = TRUE;
		m_sGoldenCrushInfo.InitializeUserItemList();
	}

	if( iCurrentWonboSize > sGoldenCrushConfig._iMaxGoldenCardCount &&
		FALSE == m_sGoldenCrushInfo._bSpecialReward )
		m_sGoldenCrushInfo.InitializeUserItemList();

	if( FALSE == m_sGoldenCrushInfo._bSpecialReward  )
	{
		if( sGoldenCrushConfig._iMinGoldenCardCount >= iCurrentWonboSize )
			iCurrentWonboSize = GOLDENCRUSH.GetMaxWonboCount();
	}
	else
		iCurrentWonboSize = GOLDENCRUSH.GetMaxSpecialWonboCount();

	rs.iResult = RESULT_GOLDENCRUSH_CRUSH_NOT_EVENT_DATE;
	CHECK_CONDITION_RETURN_VOID( CLOSED == GOLDENCRUSH.IsOpen() );

	if( FALSE == m_sGoldenCrushInfo._bSpecialReward )
	{
		rs.iResult = RESULT_GOLDENCRUSH_CRUSH_HAMMER_LOW;
		CHECK_CONDITION_RETURN_VOID( USECRUSHITEMCOUNT > rs.btHammerCount );
	}

	rs.iResult = RESULT_GOLDENCRUSH_CRUSH_MAILBOX_FULL;
	CHECK_CONDITION_RETURN_VOID( GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST ) ;

	rs.iResult = RESULT_GOLDENCRUSH_CRUSH_FAIL;
	SGOLDENCRUSHITEMINFO sRewardItemInfo;
	
	int iSaveReward[MAX_GOLDENCRUSH_SAVEINDEX] = {-1,-1,-1,-1};
	int iUserRewardItemSize = m_sGoldenCrushInfo.GetRewardItemSize();
	
	CHECK_CONDITION_RETURN_VOID( FALSE == GOLDENCRUSH.GetUserCrushRewardItem(&m_sGoldenCrushInfo._mapUserItemList, m_sGoldenCrushInfo._bSpecialReward , sRewardItemInfo));
		
	if( sGoldenCrushConfig._iMinGoldenCardCount < iUserRewardItemSize && iUserRewardItemSize <= MAX_GOLDENCRUSH_SAVEINDEX )
	{
		GOLDENCRUSH.GetSaveRewardforRewardIndex( &m_sGoldenCrushInfo._mapUserItemList, iSaveReward, sRewardItemInfo._iItemIndex );
	}
	else // �ʱ�ȭ ���.
	{
		m_sGoldenCrushInfo.InitializeUserItemList();
		
		if( FALSE == m_sGoldenCrushInfo._bSpecialReward )
		{
			GOLDENCRUSH.GetSaveReward( &m_sGoldenCrushInfo._mapUserItemList, sRewardItemInfo._iItemIndex );
			GOLDENCRUSH.GetSaveRewardforRewardIndex( &m_sGoldenCrushInfo._mapUserItemList, iSaveReward, sRewardItemInfo._iItemIndex );
		}
		else
			GOLDENCRUSH.GetSpecialSaveReward( &m_sGoldenCrushInfo._mapUserItemList , sRewardItemInfo._iItemIndex);
	}

	int iOutGauge, iOutPresentIndex, iOutHammerCount;
	iOutGauge = iOutPresentIndex = iOutHammerCount = 0;
	if(ODBC_RETURN_SUCCESS != pODBCBase->EVENT_GOLDENCRUSH_Crush(GetUserIDIndex(), GetGameIDIndex(), iCurrentWonboSize, sGoldenCrushConfig._iOnceGaugeValue
		, sRewardItemInfo._iItemIndex, iSaveReward, iOutGauge, iOutPresentIndex , iOutHammerCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_GOLDENCRUSH, CALL_SP, FAIL, "EVENT_GOLDENCRUSH_Crush  UserIDIndex:11866902, GameIDIndex:0,  WonboSize:18227200,  RewardIndex:-858993460 , HammerCount:-858993460" 
ameIDIndex(), iCurrentWonboSize, sRewardItemInfo._iItemIndex, iOutHammerCount );
		
		if( TRUE == m_sGoldenCrushInfo._bSpecialReward  )
		{
			m_sGoldenCrushInfo.InitializeUserItemList();
		}
		return;
	}
	else
	{
		rs.iResult = RESULT_GOLDENCRUSH_CRUSH_SUCCESS; 
		m_sGoldenCrushInfo.EraseRewardItem(sRewardItemInfo._iItemIndex);
		m_sGoldenCrushInfo._iGoldenGauge = rs.iGoldenGauge = iOutGauge;
		
		RecvPresent( MAIL_PRESENT_ON_ACCOUNT, iOutPresentIndex ); //todo

		int iPropertyValue = rs.btHammerCount = iOutHammerCount;
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_GOLDENCRUSH_HAMMNER;
		sUserHighFrequencyItem.iPropertyTypeValue = iPropertyValue;
		GetUserHighFrequencyItem()->AddUserHighFrequencyItem( sUserHighFrequencyItem );
	}
	
	if( m_sGoldenCrushInfo.GetRewardItemSize() <= sGoldenCrushConfig._iMinGoldenCardCount &&
		sGoldenCrushConfig._iMaxGoldenGauge <= m_sGoldenCrushInfo._iGoldenGauge )
	{
		m_sGoldenCrushInfo.InitializeUserItemList();
		rs.btRewardCount = rs.btGoldenWonboCount = sGoldenCrushConfig._iMaxSpecialGoldenCardCount;
	}
	else
	{
		int iUserItemSize = m_sGoldenCrushInfo.GetRewardItemSize();

		if( sGoldenCrushConfig._iMinGoldenCardCount >= iUserItemSize || 
			sGoldenCrushConfig._iMaxSpecialGoldenCardCount -1 <= iUserItemSize )
		{
			rs.btGoldenWonboCount = sGoldenCrushConfig._iMaxGoldenCardCount;
		}
		else
			rs.btGoldenWonboCount = iUserItemSize;
		
		rs.btRewardCount = iUserItemSize == 0 ? sGoldenCrushConfig._iMaxGoldenCardCount : iUserItemSize;
	}
	m_sGoldenCrushInfo._bSpecialReward = FALSE;
	rs.btPaidItemCount = 1; // ��÷�Ǵ°� ���� ������ �ϳ�

	CPacketComposer	Packet(S2C_EVENT_GOLDENCRUSH_CRUSH_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_GOLDENCRUSH_CRUSH_RES));

	SGOLDENCRUSH_REWARD_ITEM_INFO	sRewardPacketInfo; //��÷ | �����ִ� ������
	sRewardPacketInfo.btRewardType = (BYTE)(sRewardItemInfo._iItemCode == POINT_ITEMCODE ? REWARD_TYPE_POINT : REWARD_TYPE_ITEM);
	sRewardPacketInfo.iItemCode = sRewardItemInfo._iItemCode; 
	sRewardPacketInfo.iPropertyType = sRewardItemInfo._iPropertyType;
	sRewardPacketInfo.iPropertyTypeValue = sRewardItemInfo._iPropertyValue;

	Packet.Add((PBYTE)&sRewardPacketInfo, sizeof(SGOLDENCRUSH_REWARD_ITEM_INFO));
	
	GOLDENCRUSH.MakePacketRewardItem(Packet, &m_sGoldenCrushInfo._mapUserItemList);

	Send(&Packet);

	if( sGoldenCrushConfig._iMinGoldenCardCount >= m_sGoldenCrushInfo.GetRewardItemSize() || TRUE == m_sGoldenCrushInfo._bSpecialReward)
		m_sGoldenCrushInfo.InitializeUserItemList();
}

void CFSGameUser::SendGoldenCrushEventStatus()
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == GOLDENCRUSH.IsOpen() );

	SS2C_EVENT_GOLDENCRUSH_EVENT_NOT not;

	not.btIsOpen = GOLDENCRUSH.IsOpen();

	Send(S2C_EVENT_GOLDENCRUSH_EVENT_NOT, &not, sizeof(SS2C_EVENT_GOLDENCRUSH_EVENT_NOT ));
}

void CFSGameUser::SendMatchingCardEventStatus()
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == MATCHINGCARD.IsOpen() );

	SS2C_EVENT_MATCHINGCARD_STATUS_NOT	not;

	not.btIsOpen = MATCHINGCARD.IsOpen();
	not.iCashPrize = MATCHINGCARD.GetCumulativeCash(); 

	Send(S2C_EVENT_MATCHINGCARD_STATUS_NOT, &not, sizeof(SS2C_EVENT_MATCHINGCARD_STATUS_NOT));
}

BOOL CFSGameUser::LoadEventCoin()
{
	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	
	m_sUserEventCoinInfo.listEventCoinData.clear();

	if(ODBC_RETURN_SUCCESS != pODBCBase->spGetUserEventCoin(GetUserIDIndex(), m_sUserEventCoinInfo.listEventCoinData))
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, CALL_SP, FAIL, "spGetUserEventCoin, UserIDIndex:11866902", GetUserIDIndex());
rn FALSE;
	}

	UpdateUserEventCoin();

	return TRUE;
}

void CFSGameUser::UpdateUserEventCoin()
{
	m_sUserEventCoinInfo.iTotalEventCash = 0;
	LIST_EVENTCOIN::iterator	iter = m_sUserEventCoinInfo.listEventCoinData.begin();

	while( iter != m_sUserEventCoinInfo.listEventCoinData.end() )
	{
		m_sUserEventCoinInfo.iTotalEventCash += (*iter).iEventCash;

		++iter;
	}

	SetEventCoin(m_sUserEventCoinInfo.iTotalEventCash);
}

void CFSGameUser::CheckAndSendEventCoinInfo( void )
{
	BOOL bLoad = FALSE;
	CheckExpiredEventCoin();
	if( GetEventCoin() != m_sUserEventCoinInfo.iTotalEventCash )
	{
		LoadEventCoin();
		bLoad = TRUE;
	}

	CPacketComposer	Packet(S2C_EVENTCASH_INFO_RES);

	SS2C_EVENTCASH_INFO_RES	rs;
	int iCurrentDBDate = TimetToYYYYMMDD(_GetCurrentDBDate);
	
	rs.btCount = (BYTE)m_sUserEventCoinInfo.listEventCoinData.size();
	Packet.Add((PBYTE)&rs, sizeof( SS2C_EVENTCASH_INFO_RES ));

	for( LIST_EVENTCOIN::iterator iter = m_sUserEventCoinInfo.listEventCoinData.begin(); 
		iter != m_sUserEventCoinInfo.listEventCoinData.end() ; ++iter )
	{
		LPSUSEREVENTCOINDATA	pUserCoinInfo = &(*iter);
		SEVENTCASH_INFO	sInfo;

		sInfo.iEventCash		= pUserCoinInfo->iEventCash;
		sInfo.iAcquisitionDay	= pUserCoinInfo->iAcquisitionDay;
		sInfo.iExpireDay		= pUserCoinInfo->iExpireDay;
		sInfo.iRemainDay		= pUserCoinInfo->GetRemainDay(iCurrentDBDate);
		
		Packet.Add((PBYTE)&sInfo, sizeof( SEVENTCASH_INFO ));
	}

	Send(&Packet);

	if( TRUE == bLoad )
		SendUserGold();
}

void CFSGameUser::CheckExpiredEventCoin( void )
{
	time_t tCurrentDate = _GetCurrentDBDate;

	int iCurrentDate = TimetToYYYYMMDD(tCurrentDate);

	bool bExpiredCheck = false;

	LIST_EVENTCOIN::iterator iter = m_sUserEventCoinInfo.listEventCoinData.begin();
	LIST_EVENTCOIN::iterator iterEnd = m_sUserEventCoinInfo.listEventCoinData.end();
	while( iter != iterEnd )
	{
		if( iCurrentDate >= (*iter).iExpireDay )
		{
			bExpiredCheck = true;
		}
		++iter;
	}
	
	if( true == bExpiredCheck )
	{
		LoadEventCoin();
		SendUserGold();
	}
}

void CFSGameUser::ProcessCheckAvatarSomeAction_ChangeStatus( BYTE* pRecvBuffer, int iSize , int iHackType /*= HACK_TYPE_SOME_ACTION_CHANGE_STATUS*/ )
{
	CHECK_NULL_POINTER_VOID(pRecvBuffer);
	CHECK_CONDITION_RETURN_VOID(FALSE == HACKINGMANAGER.IsEnable(iHackType));
	CHECK_CONDITION_RETURN_VOID(TRUE == IsHackUser());

	RC4Decrypt(GetAnyOperation_Rc4Key(), (char*)pRecvBuffer ,iSize);

	int iFaceID = 0;
	int iaClientStatus[MAX_STAT_NUM] = {0};
	int iBufferPosition = 0;

	memcpy(&iFaceID, pRecvBuffer + iBufferPosition, sizeof(int));
	iBufferPosition += sizeof(int);

	for(int i = 0; i < MAX_STAT_NUM; ++i )
	{
		memcpy(&iaClientStatus[i], pRecvBuffer + iBufferPosition, sizeof(int));
		iBufferPosition += sizeof(int);
	}

	if (FALSE == CheckAvatarStat(iaClientStatus, iFaceID))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(iHackType, this, "SomeAction Check AvatarStat");
		return;
	}
}

BOOL CFSGameUser::CheckAvatarStat( const int* pClientStatus, const int iFaceID )
{
	if( NULL == pClientStatus )
	{
		return TRUE;
	}

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	if(TRANSFORM_DRAGONTIGER == pAvatar->GetTransformType())
	{
		for( int iStatIndex = 0; iStatIndex < MAX_STAT_NUM; ++iStatIndex )
		{
			// �ΰ��� ���� ���� �������� ���� ĳ���� �ΰ�, or �⺻���� ������ ��� ���� �ΰ�,
			int iServerStat = (iFaceID == pAvatar->iFace || iStatIndex >= MAX_STAT_TYPE_COUNT) ? m_AvatarStatCheckInfo.iaTotalStat[iStatIndex] : m_AvatarStatCheckInfo.iaTotalStat_Clone[iStatIndex];

			if(pClientStatus[iStatIndex] != iServerStat)
			{
				WRITE_LOG_NEW(LOG_TYPE_HACKING, INVALED_DATA, CHECK_FAIL, "ONLY STAT_LOG\tMismatch Stat\tGameID=�4\tStat[0]\tServer(18227200)!=Client(-858993460), FaceID=-858993460", GetGameID(), iStatIndex, iServerStat, pClientStatus[iStatIndex], iFaceID);
}
		}
	}
	else
	{
		for( int iStatIndex = 0; iStatIndex < MAX_STAT_NUM; ++iStatIndex )
		{
			if( pClientStatus[iStatIndex] != m_AvatarStatCheckInfo.iaTotalStat[iStatIndex] )
			{
				WRITE_LOG_NEW(LOG_TYPE_HACKING, INVALED_DATA, CHECK_FAIL, "ONLY STAT_LOG\tMismatch Stat\tGameID=�4\tStat[0]\tServer(18227200)!=Client(-858993460)", GetGameID(), iStatIndex, m_AvatarStatCheckInfo.iaTotalStat[iStatIndex], pClientStatus[iStatIndex]);
SE;
			}
		}
	}	

	return TRUE;
}

void CFSGameUser::SendRookieItemNotice(BOOL bBuying)
{
	CAvatarItemList *pAvatarItem = m_UserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID( pAvatarItem );

	int iItemCode = 52113241;		//������Ű��������

	CHECK_CONDITION_RETURN_VOID(FALSE == bBuying && pAvatarItem->GetBuyCountLimitedTimeItem( iItemCode ) > 0);

	BYTE btFlashing = 1, btOpen = 1;

	CPacketComposer pComposer( S2C_ROOKIE_ITEM_NOTICE_NOT );

	if(TRUE == bBuying)
	{
		// ���� �� ��ư Ŭ�����û ��Ŷ ����.
		pComposer.Add( (BYTE)0 );
		pComposer.Add( (BYTE)0 );
	}
	else
	{
		pComposer.Add( btOpen );
		pComposer.Add( btFlashing );
	}
	
	Send( &pComposer );
}

void CFSGameUser::ProcessCheckAvatarStatus1( CReceivePacketBuffer* pRecvBuffer )
{
	CHECK_NULL_POINTER_VOID(pRecvBuffer);
	CHECK_CONDITION_RETURN_VOID(FALSE == HACKINGMANAGER.IsEnable(HACK_TYPE_CHANGE_STATUS));
	CHECK_CONDITION_RETURN_VOID(TRUE == IsHackUser());

	RC4Decrypt(GetRc4Key(),	(char*)pRecvBuffer->GetBuffer() ,pRecvBuffer->GetDataSize());

	int iRecvCommand = pRecvBuffer->GetCommand();
	if (GetCheckStatPacketNo() != iRecvCommand - 1)
	{
		SetCheckStatPacketNo(0);
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid packet No.");
		return;
	}
	SetCheckStatPacketNo(0);

	BYTE btSafeConstantFirstChkSum = 0;
	BYTE btCheckSeed = 0;
	float fCheckRimPosition[MAX_RIM_POSITION_IDX_COUNT] = {0};
	int iaClientStatus[MAX_STAT_NUM] = {0};
	int iaSkill[MAX_SKILL_STORAGE_COUNT];
	int iaFreeStyle[MAX_SKILL_STORAGE_COUNT];
	int iClientHeight = 0;	

	BYTE btSpecialtySkillCount = 0;
	SAvatarSpecialtySkill SpecialtySkill;
	BYTE btStatCount = 0;
	SSpecialtyStat SpecialtyStat;
	list<SAvatarSpecialtySkill> listSpecialtySkill;

	int iCRShootSuccessRate = 0, iCRBlockSuccessRate = 0;
	float fFrameRate = 0, fSentenceShootSuccessRate = 0, fSentenceBlockSuccessRate = 0
		, fSpecialPartsShootSuccessRate = 0, fSpecialPartsBlockSuccessRate = 0;

	int iTransformAbilityType = 0, iTransformAbilityValue = 0, iFaceID = 0;
	int iSpecialPieceStatCount = 0, iSpecialPieceProperty = -1;
	int iaSpecialPieceProperty[MAX_STAT_NUM];
	ZeroMemory(iaSpecialPieceProperty, sizeof(int)*MAX_STAT_NUM);

	pRecvBuffer->Read(&btSafeConstantFirstChkSum);
	pRecvBuffer->Read(&btCheckSeed);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_X]);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_Y]);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_Z]);
	for(int i = 0; i < MAX_STAT_NUM; i++ )
	{
		pRecvBuffer->Read(&iaClientStatus[i]);
	}
	pRecvBuffer->Read(&iClientHeight);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_0]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_2]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_1]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_3]);	//��ų��ȣ Ȯ�� Ŭ��� �Բ� �۾��� �ּ�����
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_0]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_2]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_1]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_3]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_4]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_4]);

	pRecvBuffer->Read(&iCRShootSuccessRate);
	pRecvBuffer->Read(&iCRBlockSuccessRate);
	pRecvBuffer->Read(&fFrameRate);
	pRecvBuffer->Read(&fSentenceShootSuccessRate);
	pRecvBuffer->Read(&fSentenceBlockSuccessRate);
	pRecvBuffer->Read(&fSpecialPartsShootSuccessRate);
	pRecvBuffer->Read(&fSpecialPartsBlockSuccessRate);

	//���� ������� ������
	BYTE btAbilityCount = 0;
	pRecvBuffer->Read(&btAbilityCount);
	//

	pRecvBuffer->Read(&btSpecialtySkillCount);
	for (int i = 0; i < btSpecialtySkillCount && i < MAX_SPECIALTY_SKILL_COUNT_ON_USER; ++i)
	{
		pRecvBuffer->Read(&SpecialtySkill.iKind);
		pRecvBuffer->Read(&SpecialtySkill.iSkillNo);
		pRecvBuffer->Read(&SpecialtySkill.btSpecialtyNo);

		pRecvBuffer->Read(&btStatCount);
		for (int j = 0; j < btStatCount && j < MAX_SPECIALTY_SKILL_STAT_COUNT; ++j)
		{
			pRecvBuffer->Read(&SpecialtyStat.btSpecialtyStatType);
			pRecvBuffer->Read(&SpecialtyStat.iStatValue);
			SpecialtySkill.vecStat.push_back(SpecialtyStat);
		}

		listSpecialtySkill.push_back(SpecialtySkill);
		SpecialtySkill.vecStat.clear();
	}

	pRecvBuffer->Read(&iTransformAbilityType);
	pRecvBuffer->Read(&iTransformAbilityValue);
	pRecvBuffer->Read(&iFaceID);

	pRecvBuffer->Read(&iSpecialPieceStatCount);

	int iCheckSpecialPieceStatCount = 0;
	for (int i = 0; i < iSpecialPieceStatCount && i < MAX_STAT_NUM; ++i)
	{
		pRecvBuffer->Read(&iSpecialPieceProperty);
		if(iSpecialPieceProperty >= 0 && iSpecialPieceProperty < MAX_STAT_NUM)
		{
			pRecvBuffer->Read(&iaSpecialPieceProperty[iSpecialPieceProperty]);
			iCheckSpecialPieceStatCount++;
		}
	}

	if (FALSE == CheckAvatarStatus(btCheckSeed, iaClientStatus, iClientHeight , iaSkill, iaFreeStyle, iFaceID) )
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "CheckAvatarStatus");
		return;
	}

	if (GetSafeConstantFirstChkSum() != btSafeConstantFirstChkSum) 
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Mismatch CheckSeed 1\tUse=11866902\tServer=0", btSafeConstantFirstChkSum, GetSafeConstantFirstChkSum());
urn;
	}

	if (0 < btSpecialtySkillCount)
	{
		if (FALSE == GetUserSkill()->CheckSpecialtySkill(btSpecialtySkillCount, listSpecialtySkill))
		{
			SetHackUser(TRUE);
			HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpeicialtySkill\tCount=11866902",btSpecialtySkillCount );
urn;
		}
	}

	if(FALSE == CheckCheerLeaderAbility(iCRShootSuccessRate, iCRBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid CLAbility");
		return;
	}

	////������Ʈ ��� ��ġ�� �˻�
	//if (DEFAULT_RIM_POSITION_X != fCheckRimPosition[RIM_POSITION_IDX_X] ||
	//	DEFAULT_RIM_POSITION_Y != fCheckRimPosition[RIM_POSITION_IDX_Y] ||
	//	DEFAULT_RIM_POSITION_Z != fCheckRimPosition[RIM_POSITION_IDX_Z])
	//{
	//	SetHackUser(TRUE);
	//	HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid RimPosition");
	//	return;
	//}

	if (DEFAULT_FRAME_RATE != fFrameRate)
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid FrameRate");
		return;
	}

	if(FALSE == GetUserCharacterCollection()->CheckSentenceStatus(fSentenceShootSuccessRate, fSentenceBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SentenceRate");
		return;
	}

	if(FALSE == CheckSpecialPartsOptionProperty(fSpecialPartsShootSuccessRate, fSpecialPartsBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPartsRate");
		return;
	}

	if(FALSE == CheckTransformAbility(iTransformAbilityType, iTransformAbilityValue))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid TransformAbility");
		return;
	}

	if(iCheckSpecialPieceStatCount != iSpecialPieceStatCount)
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPieceAbility Count");
		return;
	}

	if(FALSE == GetUserSpecialPiece()->CheckSpecialPieceProperty(iSpecialPieceStatCount, iaSpecialPieceProperty))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPieceAbility");
		return;
	}
}

void CFSGameUser::ProcessCheckAvatarStatus2( CReceivePacketBuffer* pRecvBuffer )
{
	CHECK_NULL_POINTER_VOID(pRecvBuffer);
	CHECK_CONDITION_RETURN_VOID(FALSE == HACKINGMANAGER.IsEnable(HACK_TYPE_CHANGE_STATUS));
	CHECK_CONDITION_RETURN_VOID(TRUE == IsHackUser());

	RC4Decrypt(GetRc4Key(),	(char*)pRecvBuffer->GetBuffer() ,pRecvBuffer->GetDataSize());

	int iRecvCommand = pRecvBuffer->GetCommand();
	if (GetCheckStatPacketNo() != iRecvCommand - 1)
	{
		SetCheckStatPacketNo(0);
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid packet No.");
		return;
	}
	SetCheckStatPacketNo(0);

	BYTE btSafeConstantFirstChkSum = 0;
	BYTE btCheckSeed = 0;
	float fCheckRimPosition[MAX_RIM_POSITION_IDX_COUNT] = {0};
	int iaClientStatus[MAX_STAT_NUM] = {0};
	int iaSkill[MAX_SKILL_STORAGE_COUNT];
	int iaFreeStyle[MAX_SKILL_STORAGE_COUNT];
	int iClientHeight = 0;	

	BYTE btSpecialtySkillCount = 0;
	SAvatarSpecialtySkill SpecialtySkill;
	BYTE btStatCount = 0;
	SSpecialtyStat SpecialtyStat;
	list<SAvatarSpecialtySkill> listSpecialtySkill;

	int iCRShootSuccessRate = 0, iCRBlockSuccessRate = 0;
	float fFrameRate = 0, fSentenceShootSuccessRate = 0, fSentenceBlockSuccessRate = 0
		, fSpecialPartsShootSuccessRate = 0, fSpecialPartsBlockSuccessRate = 0;

	int iTransformAbilityType = 0, iTransformAbilityValue = 0, iFaceID = 0;
	int iSpecialPieceStatCount = 0, iSpecialPieceProperty = -1;
	int iaSpecialPieceProperty[MAX_STAT_NUM];
	ZeroMemory(iaSpecialPieceProperty, sizeof(int)*MAX_STAT_NUM);

	for(int i = 0; i < MAX_STAT_NUM; i++ )
	{
		pRecvBuffer->Read(&iaClientStatus[i]);
	}
	pRecvBuffer->Read(&iClientHeight);

	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_3]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_3]);	//��ų��ȣ Ȯ�� Ŭ��� �Բ� �۾��� �ּ�����
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_0]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_1]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_2]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_0]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_1]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_2]);
	pRecvBuffer->Read(&iaSkill[SKILL_STORAGE_4]);
	pRecvBuffer->Read(&iaFreeStyle[SKILL_STORAGE_4]);

	pRecvBuffer->Read(&fSentenceShootSuccessRate);
	pRecvBuffer->Read(&fSentenceBlockSuccessRate);
	pRecvBuffer->Read(&fSpecialPartsShootSuccessRate);
	pRecvBuffer->Read(&fSpecialPartsBlockSuccessRate);

	//���� ������� ������
	BYTE btAbilityCount = 0;
	pRecvBuffer->Read(&btAbilityCount);
	//

	pRecvBuffer->Read(&btSpecialtySkillCount);
	for (int i = 0; i < btSpecialtySkillCount && i < MAX_SPECIALTY_SKILL_COUNT_ON_USER; ++i)
	{
		pRecvBuffer->Read(&SpecialtySkill.iKind);
		pRecvBuffer->Read(&SpecialtySkill.iSkillNo);
		pRecvBuffer->Read(&SpecialtySkill.btSpecialtyNo);

		pRecvBuffer->Read(&btStatCount);
		for (int j = 0; j < btStatCount && j < MAX_SPECIALTY_SKILL_STAT_COUNT; ++j)
		{
			pRecvBuffer->Read(&SpecialtyStat.btSpecialtyStatType);
			pRecvBuffer->Read(&SpecialtyStat.iStatValue);
			SpecialtySkill.vecStat.push_back(SpecialtyStat);
		}

		listSpecialtySkill.push_back(SpecialtySkill);
		SpecialtySkill.vecStat.clear();
	}

	pRecvBuffer->Read(&iCRShootSuccessRate);
	pRecvBuffer->Read(&iCRBlockSuccessRate);
	pRecvBuffer->Read(&fFrameRate);
	
	pRecvBuffer->Read(&btSafeConstantFirstChkSum);
	pRecvBuffer->Read(&btCheckSeed);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_X]);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_Y]);
	pRecvBuffer->Read(&fCheckRimPosition[RIM_POSITION_IDX_Z]);

	pRecvBuffer->Read(&iTransformAbilityType);
	pRecvBuffer->Read(&iTransformAbilityValue);
	pRecvBuffer->Read(&iFaceID);

	pRecvBuffer->Read(&iSpecialPieceStatCount);

	int iCheckSpecialPieceStatCount = 0;
	for (int i = 0; i < iSpecialPieceStatCount && i < MAX_STAT_NUM; ++i)
	{
		pRecvBuffer->Read(&iSpecialPieceProperty);
		if(iSpecialPieceProperty >= 0 && iSpecialPieceProperty < MAX_STAT_NUM)
		{
			pRecvBuffer->Read(&iaSpecialPieceProperty[iSpecialPieceProperty]);
			iCheckSpecialPieceStatCount++;
		}
	}

	if (FALSE == CheckAvatarStatus(btCheckSeed, iaClientStatus, iClientHeight , iaSkill, iaFreeStyle, iFaceID) )
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "CheckAvatarStatus");
		return;
	}

	if (GetSafeConstantFirstChkSum() != btSafeConstantFirstChkSum) 
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Mismatch CheckSeed 1\tUse=11866902\tServer=0", btSafeConstantFirstChkSum, GetSafeConstantFirstChkSum());
urn;
	}

	if (0 < btSpecialtySkillCount)
	{
		if (FALSE == GetUserSkill()->CheckSpecialtySkill(btSpecialtySkillCount, listSpecialtySkill))
		{
			SetHackUser(TRUE);
			HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpeicialtySkill\tCount=11866902",btSpecialtySkillCount );
urn;
		}
	}

	if(FALSE == CheckCheerLeaderAbility(iCRShootSuccessRate, iCRBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid CLAbility");
		return;
	}

	////������Ʈ ��� ��ġ�� �˻�
	//if (DEFAULT_RIM_POSITION_X != fCheckRimPosition[RIM_POSITION_IDX_X] ||
	//	DEFAULT_RIM_POSITION_Y != fCheckRimPosition[RIM_POSITION_IDX_Y] ||
	//	DEFAULT_RIM_POSITION_Z != fCheckRimPosition[RIM_POSITION_IDX_Z])
	//{
	//	SetHackUser(TRUE);
	//	HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid RimPosition");
	//	return;
	//}

	if (DEFAULT_FRAME_RATE != fFrameRate)
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid FrameRate");
		return;
	}

	if(FALSE == GetUserCharacterCollection()->CheckSentenceStatus(fSentenceShootSuccessRate, fSentenceBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SentenceRate");
		return;
	}

	if(FALSE == CheckSpecialPartsOptionProperty(fSpecialPartsShootSuccessRate, fSpecialPartsBlockSuccessRate))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPartsRate");
		return;
	}

	if(FALSE == CheckTransformAbility(iTransformAbilityType, iTransformAbilityValue))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid TransformAbility");
		return;
	}

	if(iCheckSpecialPieceStatCount != iSpecialPieceStatCount)
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPieceAbility Count");
		return;
	}

	if(FALSE == GetUserSpecialPiece()->CheckSpecialPieceProperty(iSpecialPieceStatCount, iaSpecialPieceProperty))
	{
		SetHackUser(TRUE);
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_CHANGE_STATUS, this, "Invalid SpecialPieceAbility");
		return;
	}
}

unsigned int CFSGameUser::GetAvatarStateCheckSum1()
{
	unsigned int retval = 0;

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_CONDITION_RETURN( NULL == pAvatar , -1 );
	CHECK_CONDITION_RETURN( NULL == m_UserItem , -1 );

	m_CheckSum.Clear();

	CHECK_SUM_INIT();

	BYTE btSafeConstantFirstCheckSum = GetSafeConstantFirstChkSum();

	ADD_CHECK_SUM_VALUE(btSafeConstantFirstCheckSum);
	ADD_CHECK_SUM_VALUE(m_btStatCheckSeed);

	float fDefault_Rim_Pos_X = (float)DEFAULT_RIM_POSITION_X;
	float fDefault_Rim_Pos_Y = (float)DEFAULT_RIM_POSITION_Y;
	float fDefault_Rim_Pos_Z = (float)DEFAULT_RIM_POSITION_Z;

	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_X);
	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_Y);
	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_Z);

	for( int i = 0 ; i < MAX_STAT_NUM ; ++i )
	{
		ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaTotalStat[i]);
	}

	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iHeight);

	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_0]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_2]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_1]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_3]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_0]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_2]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_1]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_3]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_4]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_4]);

	int iCheerShootSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderShootSuccessRate(m_AvatarStatCheckInfo.iCheerLeaderCode);
	int iCheerBlockSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderBlockSuccessRate(m_AvatarStatCheckInfo.iCheerLeaderCode);
	float fFrame_Rate = (float)DEFAULT_FRAME_RATE;

	ADD_CHECK_SUM_VALUE(iCheerShootSuccessRate);
	ADD_CHECK_SUM_VALUE(iCheerBlockSuccessRate);
	ADD_CHECK_SUM_VALUE(fFrame_Rate);

	float fSentenceShootSuccessRate = 0, fSentenceBlockSuccessRate = 0;
	GetUserCharacterCollection()->GetSentenceStatus( fSentenceShootSuccessRate, fSentenceBlockSuccessRate );
	ADD_CHECK_SUM_VALUE(fSentenceShootSuccessRate);
	ADD_CHECK_SUM_VALUE(fSentenceBlockSuccessRate);

	float fSpecialPartsShootSuccessRate = 0, fSpecialPartsBlockSuccessRate = 0;
	GetSpecialPartsOptionProperty(fSpecialPartsShootSuccessRate, fSpecialPartsBlockSuccessRate);
	ADD_CHECK_SUM_VALUE(fSpecialPartsShootSuccessRate);
	ADD_CHECK_SUM_VALUE(fSpecialPartsBlockSuccessRate);

	BYTE btAbilityCount = 0;
	ADD_CHECK_SUM_VALUE(btAbilityCount);

	BYTE btSpecialtyCount;
	list<SAvatarSpecialtySkill> listSpecialtySkill;
	GetUserSkill()->GetSpecialSkilllist(listSpecialtySkill ,btSpecialtyCount, pAvatar->Skill);

	ADD_CHECK_SUM_VALUE(btSpecialtyCount);

	if( 0 < listSpecialtySkill.size() )
	{
		listSpecialtySkill.sort(opSortSpecialty);
	}

	list<SAvatarSpecialtySkill>::iterator	 iter = listSpecialtySkill.begin();
	for( ; iter != listSpecialtySkill.end() ; ++iter )
	{
		ADD_CHECK_SUM_VALUE((*iter).iKind);
		ADD_CHECK_SUM_VALUE((*iter).iSkillNo);
		ADD_CHECK_SUM_VALUE((*iter).btSpecialtyNo);

		BYTE btStatCount = (BYTE)(*iter).vecStat.size();

		ADD_CHECK_SUM_VALUE(btStatCount);
		for( unsigned int i = 0 ; i < (*iter).vecStat.size() ; ++i )
		{
			ADD_CHECK_SUM_VALUE((*iter).vecStat[i].btSpecialtyStatType);
			ADD_CHECK_SUM_VALUE((*iter).vecStat[i].iStatValue);
		}
	}

	ADD_CHECK_SUM_VALUE(pAvatar->Status.iTransformAbilityType);
	ADD_CHECK_SUM_VALUE(pAvatar->Status.iTransformAbilityValue);

	int iOrder = 0, iProperty = -1, iValue = 0;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	iOrder = 1;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	iOrder = 2;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	return retval;
}

unsigned int CFSGameUser::GetAvatarStateCheckSum2()
{
	unsigned int retval = 0;

	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_CONDITION_RETURN( NULL == pAvatar , -1 );
	CHECK_CONDITION_RETURN( NULL == m_UserItem , -1 );

	m_CheckSum.Clear();

	CHECK_SUM_INIT();

	for( int i = 0 ; i < MAX_STAT_NUM ; ++i )
	{
		ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaTotalStat[i]);
	}

	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iHeight);

	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_3]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_3]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_0]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_1]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_2]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_0]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_1]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_2]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaSkill[SKILL_STORAGE_4]);
	ADD_CHECK_SUM_VALUE(m_AvatarStatCheckInfo.iaFreeStyle[SKILL_STORAGE_4]);
	
	float fSentenceShootSuccessRate = 0, fSentenceBlockSuccessRate = 0;
	GetUserCharacterCollection()->GetSentenceStatus( fSentenceShootSuccessRate, fSentenceBlockSuccessRate );
	ADD_CHECK_SUM_VALUE(fSentenceShootSuccessRate);
	ADD_CHECK_SUM_VALUE(fSentenceBlockSuccessRate);

	float fSpecialPartsShootSuccessRate = 0, fSpecialPartsBlockSuccessRate = 0;
	GetSpecialPartsOptionProperty(fSpecialPartsShootSuccessRate, fSpecialPartsBlockSuccessRate);
	ADD_CHECK_SUM_VALUE(fSpecialPartsShootSuccessRate);
	ADD_CHECK_SUM_VALUE(fSpecialPartsBlockSuccessRate);

	BYTE btAbilityCount = 0;
	ADD_CHECK_SUM_VALUE(btAbilityCount);

	BYTE btSpecialtyCount;
	list<SAvatarSpecialtySkill> listSpecialtySkill;
	GetUserSkill()->GetSpecialSkilllist(listSpecialtySkill ,btSpecialtyCount, pAvatar->Skill);

	ADD_CHECK_SUM_VALUE(btSpecialtyCount);

	if( 0 < listSpecialtySkill.size() )
	{
		listSpecialtySkill.sort(opSortSpecialty);
	}

	list<SAvatarSpecialtySkill>::iterator	 iter = listSpecialtySkill.begin();
	for( ; iter != listSpecialtySkill.end() ; ++iter )
	{
		ADD_CHECK_SUM_VALUE((*iter).iKind);
		ADD_CHECK_SUM_VALUE((*iter).iSkillNo);
		ADD_CHECK_SUM_VALUE((*iter).btSpecialtyNo);

		BYTE btStatCount = (BYTE)(*iter).vecStat.size();

		ADD_CHECK_SUM_VALUE(btStatCount);
		for( unsigned int i = 0 ; i < (*iter).vecStat.size() ; ++i )
		{
			ADD_CHECK_SUM_VALUE((*iter).vecStat[i].btSpecialtyStatType);
			ADD_CHECK_SUM_VALUE((*iter).vecStat[i].iStatValue);
		}
	}

	int iCheerShootSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderShootSuccessRate(m_AvatarStatCheckInfo.iCheerLeaderCode);
	int iCheerBlockSuccessRate = CLUBCONFIGMANAGER.GetCheerLeaderBlockSuccessRate(m_AvatarStatCheckInfo.iCheerLeaderCode);
	float fFrame_Rate = (float)DEFAULT_FRAME_RATE;

	ADD_CHECK_SUM_VALUE(iCheerShootSuccessRate);
	ADD_CHECK_SUM_VALUE(iCheerBlockSuccessRate);
	ADD_CHECK_SUM_VALUE(fFrame_Rate);

	BYTE btSafeConstantFirstCheckSum = GetSafeConstantFirstChkSum();

	ADD_CHECK_SUM_VALUE(btSafeConstantFirstCheckSum);
	ADD_CHECK_SUM_VALUE(m_btStatCheckSeed);

	float fDefault_Rim_Pos_X = (float)DEFAULT_RIM_POSITION_X;
	float fDefault_Rim_Pos_Y = (float)DEFAULT_RIM_POSITION_Y;
	float fDefault_Rim_Pos_Z = (float)DEFAULT_RIM_POSITION_Z;

	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_X);
	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_Y);
	ADD_CHECK_SUM_VALUE(fDefault_Rim_Pos_Z);
	
	ADD_CHECK_SUM_VALUE(pAvatar->Status.iTransformAbilityType);
	ADD_CHECK_SUM_VALUE(pAvatar->Status.iTransformAbilityValue);

	int iOrder = 0, iProperty = -1, iValue = 0;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	iOrder = 1;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	iOrder = 2;
	if(TRUE == GetUserSpecialPiece()->GetSpecialPieceProperty(iOrder, iProperty, iValue))
	{
		ADD_CHECK_SUM_VALUE(iProperty);
		ADD_CHECK_SUM_VALUE(iValue);
	}

	retval = m_CheckSum.Get();

	return retval;
}

void CFSGameUser::SendPuzzleRewardInfo( void )
{
	SS2C_PUZZLES_REWARD_INFO_RES	info;

	info.iTokenItemCode = PUZZLES.GetTokenItemCode();

	const ClubTournamentTokenCountConfigVector* pVecItem = CLUBCONFIGMANAGER.GetTokenConfigVector(info.iTokenItemCode);
	CHECK_NULL_POINTER_VOID(pVecItem);

	info.btCount = (BYTE)pVecItem->size();

	CPacketComposer	Packet(S2C_PUZZLES_REWARD_INFO_RES);
	Packet.Add((BYTE*)&info, sizeof(SS2C_PUZZLES_REWARD_INFO_RES));


	for( size_t iIndex = 0 ; iIndex < pVecItem->size() ; ++iIndex )
	{
		SPUZZLES_TOKEN_INFO	sTokenInfo;

		sTokenInfo.btIndex = (BYTE)iIndex;
		sTokenInfo.iTokenCount = (*pVecItem)[iIndex].iTokenCount;
		sTokenInfo.iPropertyType = (*pVecItem)[iIndex].iPropertyType;
		sTokenInfo.iItemCode = (*pVecItem)[iIndex].iExchangeItemCode;
		sTokenInfo.iValue = (*pVecItem)[iIndex].iValue;

		Packet.Add((PBYTE)&sTokenInfo, sizeof(SPUZZLES_TOKEN_INFO));
	}

	Send(&Packet);	
}

void CFSGameUser::GiveAndSendEventPuzzleReward( int iTokenIndex )
{
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SS2C_PUZZLES_WANT_REWARD_RES	rs;
	rs.btResult = RESULT_PUZZLE_REWARD_SUCCESS;

	CPacketComposer	Packet(S2C_PUZZLES_WANT_REWARD_RES);

	int iUserTokenCount = GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN );

	SShopItemInfo	sItemInfo;
	int iTokenCount = 0, iItemCode = -1, iValue = -1;

	if(FALSE == CLUBCONFIGMANAGER.GetTokenExchangeItem(PUZZLES.GetTokenItemCode(), iTokenIndex, iTokenCount, iItemCode, iValue))
		rs.btResult = RESULT_PUZZLE_REWARD_FAIL;
	else if( iUserTokenCount < iTokenCount )
		rs.btResult = RESULT_PUZZLE_REWARD_NO_TOKEN;
	else if( iItemCode > 0 && FALSE == pItemShop->GetItem(iItemCode, sItemInfo) )
		rs.btResult = RESULT_PUZZLE_REWARD_FAIL;
	else if( GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST )
		rs.btResult = RESULT_PUZZLE_REWARD_MAILBOX_FULL;


	if( RESULT_PUZZLE_REWARD_SUCCESS == rs.btResult )
	{
		CFSODBCBase* pODBC	= (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		int iOUTPresentIndex , iOUTUpdateTokenCount;
		iOUTUpdateTokenCount = iOUTPresentIndex = 0;
		if( ODBC_RETURN_SUCCESS != pODBC->EVENT_PUZZLES_ExchangeItem( GetUserIDIndex(), GetGameIDIndex() ,
			ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN, iTokenCount, iItemCode, iValue, iOUTPresentIndex, iOUTUpdateTokenCount))
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, CALL_SP, FAIL, "EVENT_PUZZLES_ExchangeItem UserIDIndex:11866902, GameIDIndex:0, iTokenIndex:18227200"
rIDIndex(), GetGameIDIndex() , iTokenIndex);
			rs.btResult = RESULT_PUZZLE_REWARD_FAIL;
		}
		else
		{
			if( -1 < iOUTPresentIndex )
				RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iOUTPresentIndex);
	
			if( 0 < iOUTUpdateTokenCount  )
			{
				SUserHighFrequencyItem sUserHighFrequencyItem;
				sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN;
				sUserHighFrequencyItem.iPropertyTypeValue = iOUTUpdateTokenCount;
				GetUserHighFrequencyItem()->AddUserHighFrequencyItem( sUserHighFrequencyItem );
			}
			else
			{
				GetUserHighFrequencyItem()->DeleteUserHighFrequencyItem(ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN);
			}
		}
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_PUZZLES_WANT_REWARD_RES));
	Send(&Packet);
}

BOOL CFSGameUser::SendSonOfChoiceEventProductList()
{
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_SON_OF_CHOICE), FALSE);

	CPacketComposer Packet(S2C_SONOFCHOICE_PRODUCT_LIST_RES);
	SONOFCHOICE.MakePacketProductList(Packet);
	Send(&Packet);

	return TRUE;
}

BOOL CFSGameUser::BuySonOfChoiceEventRandomProduct(SC2S_SONOFCHOICE_BUY_RANDOM_PRODUCT_REQ& rs)
{
	CHECK_CONDITION_RETURN(rs.btProductGrade >= MAX_SONOFCHOICE_PRODUCT_GRADE, FALSE);

	SS2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES));

	int iSellType = SELL_TYPE_NOT_SELL, iPrice = 0;
	SONOFCHOICE.GetProductPrice(rs.btProductGrade, iSellType, iPrice);
	CHECK_CONDITION_RETURN(iPrice <= 0, FALSE);

	if(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_SON_OF_CHOICE))
	{
		ss.btResult = SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_FAIL_CLOSED_EVENT;
	}
	else if(SELL_TYPE_CASH == iSellType && 
		GetCoin() + GetEventCoin() < iPrice)
	{
		ss.btResult = SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_FAIL_LACK_MONEY;
	}
	else if(GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
	{
		ss.btResult = SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_FAIL_FULL_MAILBOX;
	}
	else
	{
		ss.btResult = SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_FAIL_WRONG_INDEX;
		set<int/*iChoiceProductIndex*/> setInfo;
		for(int i = 0; i < SONOFCHOICE_PRODUCT_CHOICE_CNT; ++i)
		{
			if(rs.iChoiceProductIndex[i] < 0 || 
				rs.iChoiceProductIndex[i] >= SONOFCHOICE_GRADE_PRODUCT_CNT ||
				setInfo.end() != setInfo.find(rs.iChoiceProductIndex[i]))
			{
				break;
			}
			setInfo.insert(rs.iChoiceProductIndex[i]);
		}

		if(SONOFCHOICE_PRODUCT_CHOICE_CNT == setInfo.size())
		{
			int iChoiceProductIndex[SONOFCHOICE_PRODUCT_CHOICE_CNT] = {0,};
			set<int/*iChoiceProductIndex*/>::iterator itSet = setInfo.begin();
			set<int/*iChoiceProductIndex*/>::iterator itSetEnd = setInfo.end();
			for(int i = 0; itSet != itSetEnd; ++itSet, ++i)
				iChoiceProductIndex[i] = (*itSet);

			int iRandomIndex = rand() % SONOFCHOICE_PRODUCT_CHOICE_CNT;
			ss.iResultProductIndex = iChoiceProductIndex[iRandomIndex];
			ss.btResult = SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_SUCCESS;

			SBillingInfo BillingInfo;

			memcpy( BillingInfo.szUserID, GetUserID(), MAX_USERID_LENGTH + 1 );	
			memcpy( BillingInfo.szGameID, GetGameID(), MAX_GAMEID_LENGTH + 1 );	
			BillingInfo.pUser = this;
			BillingInfo.iEventCode = EVENT_BUY_SONOFCHOICE_PRODUCT;
			BillingInfo.iSellType = iSellType;
			BillingInfo.iSerialNum = GetUserIDIndex();

			if(SELL_TYPE_CASH == iSellType)
				BillingInfo.iCashChange = iPrice;
			else if(SELL_TYPE_POINT == iSellType)
				BillingInfo.iPointChange = iPrice;

			BillingInfo.iParam0 = iChoiceProductIndex[0];
			BillingInfo.iParam1 = iChoiceProductIndex[1];
			BillingInfo.iParam2 = iChoiceProductIndex[2];
			BillingInfo.iParam3 = iChoiceProductIndex[3];
			BillingInfo.iParam4 = rs.btProductGrade;
			BillingInfo.iParam5 = ss.iResultProductIndex;
			BillingInfo.iParam6 = SONOFCHOICE.GetRandomRewardIndex(rs.btProductGrade, ss.iResultProductIndex);
			BillingInfo.iCurrentCash = GetCoin();
			BillingInfo.iItemPrice = iPrice;

			CHECK_CONDITION_RETURN(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo), FALSE);
		}
	}	

	if(SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_SUCCESS != ss.btResult)
	{
		CPacketComposer Packet(S2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES);
		Packet.Add((PBYTE)&ss, sizeof(SS2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES));
		Send(&Packet);
	}

	return TRUE;
}

BOOL CFSGameUser::BuySonOfChoiceEventRandomProduct_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if( FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)) )
	{
		WRITE_LOG_NEW(LOG_TYPE_SONOFCHOICEEVENT, INVALED_DATA, CHECK_FAIL, "BuySonOfChoiceEventRandomProduct_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	int iSellType = pBillingInfo->iSellType;
	int iPrevMoney = 0;
	int iPrice = 0;
	SS2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES));

	if(SELL_TYPE_CASH == iSellType)
	{
		iPrevMoney = pBillingInfo->iCurrentCash;
		iPrice = pBillingInfo->iCashChange;
	}
	else if(SELL_TYPE_POINT == iSellType)
	{
		iPrevMoney = GetSkillPoint();
		iPrice = pBillingInfo->iPointChange;
	}

	int iPostMoney = iPrevMoney - iPrice;
	int iChoiceProductIndex[SONOFCHOICE_PRODUCT_CHOICE_CNT] = {0,};
	iChoiceProductIndex[0] = pBillingInfo->iParam0;
	iChoiceProductIndex[1] = pBillingInfo->iParam1;
	iChoiceProductIndex[2] = pBillingInfo->iParam2;
	iChoiceProductIndex[3] = pBillingInfo->iParam3;
	BYTE btProductGrade = pBillingInfo->iParam4;
	ss.iResultProductIndex = pBillingInfo->iParam5;
	int iRewardIndex = pBillingInfo->iParam6;
	ss.btResult = SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_SUCCESS;

	if(iPayResult == PAY_RESULT_SUCCESS)
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_BOOL(pODBCBase);

		if(ODBC_RETURN_SUCCESS == pODBCBase->EVENT_SONOFCHOICE_BuyRandomProduct( 
			GetUserIDIndex()
			, GetGameIDIndex()
			, btProductGrade
			, iChoiceProductIndex
			, ss.iResultProductIndex
			, iSellType
			, iPrevMoney
			, iPostMoney
			, iPrice))
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, iRewardIndex);
			if(NULL == pReward)
			{
				WRITE_LOG_NEW(LOG_TYPE_SONOFCHOICEEVENT, DB_DATA_UPDATE, FAIL
					, "EVENT_SONOFCHOICE_BuyRandomProduct failed UserIDIndex:%d, ProductGrade:%d, ResultProductIndex:%d, RewardIndex:%d"
					, GetUserIDIndex(), btProductGrade, ss.iResultProductIndex, iRewardIndex);

				return FALSE;
			}

			SetUserBillResultAtMem(iSellType, iPostMoney, pBillingInfo->iPointChange, 0, 0);
			SendUserGold();
		}		
	}
	else if(iPayResult == PAY_RESULT_FAIL_CASH)
	{
		ss.btResult = SONOFCHOICE_BUY_RANDOM_PRODUCT_RESULT_FAIL_LACK_MONEY;
	}

	CPacketComposer Packet(S2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_SONOFCHOICE_BUY_RANDOM_PRODUCT_RES));
	Send(&Packet);

	return TRUE;
}

BOOL CFSGameUser::BuyPackageItemGiveInventory_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if( FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)) )
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INVALED_DATA, CHECK_FAIL, "BuyPackageItemGiveInventory_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	int iItemCode = pBillingInfo->iItemCode;
	int iSellType = pBillingInfo->iSellType;
	int iPrevMoney = 0;
	int iPrice = 0;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	if(SELL_TYPE_CASH == iSellType)
	{
		iPrevMoney = pBillingInfo->iCurrentCash;
		iPrice = pBillingInfo->iCashChange;
	}
	else if(SELL_TYPE_POINT == iSellType)
	{
		iPrevMoney = GetSkillPoint();
		iPrice = pBillingInfo->iPointChange;
	}

	int iPostMoney = iPrevMoney - iPrice;

	if(iPayResult == PAY_RESULT_SUCCESS)
	{
		CFSGameUserItem* pUserItem = GetUserItemList();
		CHECK_NULL_POINTER_BOOL(pUserItem);

		CAvatarItemList *pAvatarItemList = pUserItem->GetCurAvatarItemList();
		CHECK_NULL_POINTER_BOOL(pAvatarItemList);

		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
		CHECK_NULL_POINTER_BOOL( pODBC );

		if(ODBC_RETURN_SUCCESS == pODBC->ITEM_BuyPackageGiveInventory( 
			GetUserIDIndex()
			, GetGameIDIndex()
			, iSellType
			, iPrevMoney
			, iPostMoney
			, iPrice
			, iItemCode
			, pAvatarItemList))
		{
			iErrorCode = BUY_ITEM_ERROR_SUCCESS;
			iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "ITEM_BuyPackageGiveInventory - GameIDIndex:%d, ItemCode:%d", GetGameIDIndex(), iItemCode);
		}		
	}

	// ������ ���ſ�û Result ��Ŷ ����.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	Send(&PacketComposer);

	SetUserBillResultAtMem(iSellType, iPostMoney, pBillingInfo->iPointChange, 0, 0);
	SendUserGold();

	return TRUE;
}

BOOL CFSGameUser::CheckAndInitAttendanceItemShopEvent( void )
{
	CHECK_CONDITION_RETURN( CLOSED == ATTENDANCEITEMSHOP.IsOpen() , TRUE );

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_ATTENDANCE_ITEMSHOP_GetUserData(GetUserIDIndex(), m_sUserAttendanceItemShop_Info) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ATTENDANCEITEMSHOP, DB_DATA_LOAD, FAIL, "EVENT_ATTENDANCE_ITEMSHOP_GetUserData  UserIDIndex=%d", GetUserIDIndex());
		return FALSE;
	}

	return TRUE;
}

void CFSGameUser::SendAttendaceItemshopStatus( void )
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == ATTENDANCEITEMSHOP.IsOpen() );

	SS2C_EVENT_ATTENDANCE_ITEMSHOP_LOBBY_STATUS_NOT	not;
	time_t tCurrentDate = _GetCurrentDBDate;

	not.btIsOpen = (BYTE)ATTENDANCEITEMSHOP.IsOpen();
	not.btCometoShop = (m_sUserAttendanceItemShop_Info.iLastUpdateItemIndex < ATTENDANCEITEMSHOP.GetTodayItemIndex(tCurrentDate)) ? TRUE : FALSE;

	Send(S2C_EVENT_ATTENDANCE_ITEMSHOP_LOBBY_STATUS_NOT , (PBYTE)&not, sizeof(SS2C_EVENT_ATTENDANCE_ITEMSHOP_LOBBY_STATUS_NOT));

	if( CLOSED == not.btIsOpen )
	{
		CPacketComposer Packet(S2C_EVENT_ATTENDANCE_ITEMSHOP_SHOP_REFRESH_NOT);
		Send(&Packet);
	}
}

BOOL CFSGameUser::BuyAttendanceItemShop( int iItemCode , int& iErrorCode , int iOpCode )
{
	iErrorCode = BUY_ITEM_ERROR_EVENT_ATTENDANCE_ITEMSHOP_TIMEOVER;

	CHECK_CONDITION_RETURN( CLOSED == ATTENDANCEITEMSHOP.IsOpen() , FALSE);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);
		
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	SShopItemInfo	sShopItemInfo;
	CHECK_CONDITION_RETURN( FALSE == pItemShop->GetItem( iItemCode, sShopItemInfo ), FALSE);
	
	SEVENTATTENDANCEITEMSHOP_ITEMINFO* pItemInfo = ATTENDANCEITEMSHOP.GetAttendanceItemInfo( iItemCode, _GetCurrentDBDate );
	CHECK_NULL_POINTER_BOOL(pItemInfo);

	iErrorCode = BUY_ITEM_ERROR_EVENT_ATTENDANCE_ITEMSHOP_ALREADY_PURCHASED;
	CHECK_CONDITION_RETURN(m_sUserAttendanceItemShop_Info.iLastUpdateItemIndex >= pItemInfo->_iItemIndex, FALSE);

	int iRealItemCode = ATTENDANCEITEMSHOP.GetAttenDanceRealItemCode(sShopItemInfo.iItemCode0);
	CHECK_CONDITION_RETURN( 0 > iRealItemCode, FALSE);

	iErrorCode = BUY_ITEM_ERROR_EVENT_ATTENDANCE_ITEMSHOP_FULL_MAILBOX;
	CHECK_CONDITION_RETURN(GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, FALSE);
	
	int iOutPresentIndex = -1;
	int iODBCReturn = pODBCBase->EVENT_ATTENDANCE_ITEMSHOP_BuyItem(GetUserIDIndex(), GetGameIDIndex(), iRealItemCode
		, pItemInfo->_iPropertyValue, pItemInfo->_iItemIndex, 0/*point*/, iOutPresentIndex);

	iErrorCode = BUY_ITEM_ERROR_EVENT_ATTENDANCE_ITEMSHOP_ALREADY_PURCHASED;
	CHECK_CONDITION_RETURN( 99 == iODBCReturn , FALSE ); // �̹� ����

	if( ODBC_RETURN_SUCCESS == iODBCReturn )
	{
		m_sUserAttendanceItemShop_Info.iLastUpdateItemIndex = pItemInfo->_iItemIndex;

		if( -1 < iOutPresentIndex )
			RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iOutPresentIndex);

		CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
		PacketComposer.Add((int)iOpCode);
		PacketComposer.Add((int)BUY_ITEM_ERROR_SUCCESS);
		PacketComposer.Add(iItemCode);
		PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
		PacketComposer.Add((BYTE)SEND_ITEM_ERROR_GENERAL);			

		char szRecvGameID[MAX_GAMEID_LENGTH+1] = {NULL};
		PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
		Send(&PacketComposer);

		// refresh
		SendAttendaceItemshopStatus();
		
		return TRUE;
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_ATTENDANCEITEMSHOP, CALL_SP, FAIL, "EVENT_ATTENDANCE_ITEMSHOP_BuyItem  GameIDIndex:%d,  ItemCode:%d,  PropertyValue:%d,  Error:%d"
			,GetGameIDIndex(), pItemInfo->_iItemCode, pItemInfo->_iPropertyValue , iODBCReturn);
	}

	return FALSE;
}

BOOL CFSGameUser::BuyAttendanceRandomItem( int iItemCode, int& iErrorCode )
{
	iErrorCode = BUY_RANDOMCARD_SUCCESS;

	CHECK_CONDITION_RETURN( CLOSED == ATTENDANCEITEMSHOP.IsOpen() , FALSE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo	sShopItemInfo;
	CHECK_CONDITION_RETURN( FALSE == pItemShop->GetItem( iItemCode, sShopItemInfo ), FALSE);

	iErrorCode = BUY_ITEM_ERROR_EVENT_ATTENDANCE_ITEMSHOP_ALREADY_PURCHASED;
	SEVENTATTENDANCEITEMSHOP_ITEMINFO* pItemInfo = ATTENDANCEITEMSHOP.GetAttendanceItemInfo( iItemCode, _GetCurrentDBDate );
	CHECK_CONDITION_RETURN( NULL == pItemInfo, FALSE);
	CHECK_CONDITION_RETURN(m_sUserAttendanceItemShop_Info.iLastUpdateItemIndex >= pItemInfo->_iItemIndex, FALSE);

	int iRealItemCode = ATTENDANCEITEMSHOP.GetAttenDanceRealItemCode(sShopItemInfo.iItemCode0);
	CHECK_CONDITION_RETURN( 0 > iRealItemCode, FALSE);

	iErrorCode = BUY_ITEM_ERROR_EVENT_ATTENDANCE_ITEMSHOP_FULL_MAILBOX;
	CHECK_CONDITION_RETURN(GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, FALSE);

	CHECK_CONDITION_RETURN( 0 >= pItemInfo->_iRate , TRUE );

	SEVENTATTENDANCEITEMSHOP_RATEINFO* pRateInfo = ATTENDANCEITEMSHOP.GetAttendanceRandomItemInfo( pItemInfo->_iItemIndex, pItemInfo->_iItemCode );
	CHECK_CONDITION_RETURN( NULL == pRateInfo , TRUE);

	int iPoint = 0;
	if( REWARD_TYPE_POINT == pRateInfo->_iRewardType )
		iPoint = pRateInfo->_iPropertyValue;
		
	int iOutPresentIndex = 0;
	int iODBCReturn = pODBCBase->EVENT_ATTENDANCE_ITEMSHOP_BuyItem(GetUserIDIndex(), GetGameIDIndex(), iRealItemCode
		, pItemInfo->_iPropertyValue, pItemInfo->_iItemIndex, iPoint , iOutPresentIndex);

	CPacketComposer Packet(S2C_EPORTSCARD_BUY_RES);

	if ( ODBC_RETURN_SUCCESS == iODBCReturn )
	{
		m_sUserAttendanceItemShop_Info.iLastUpdateItemIndex = pItemInfo->_iItemIndex;

		if( -1 < iOutPresentIndex )
			RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iOutPresentIndex);

		iErrorCode = BUY_RANDOMCARD_SUCCESS;
		Packet.Add(iErrorCode);	

		BYTE btRandomCardRewardType = (pRateInfo->_iRewardType == REWARD_TYPE_ITEM ? RANDOMCARD_REWARD_TYPE_ITEM : RANDOMCARD_REWARD_TYPE_POINT);
		
		Packet.Add(btRandomCardRewardType);

		if(btRandomCardRewardType == RANDOMCARD_REWARD_TYPE_ITEM)
		{
			Packet.Add(pItemInfo->_iItemCode);
			Packet.Add((BYTE)0);//Packet.Add((BYTE)pItemInfo->_iPropertyType);
			Packet.Add(pItemInfo->_iPropertyValue);	
		}
		else
			Packet.Add(pRateInfo->_iPropertyValue);

		Packet.Add(0/*iGuaranteeVal*/);
		Packet.Add((int)GUARANTEECARD_NOGOT);
		Packet.Add((int)0);

		Send(&Packet);
		
		if( 0 < iPoint )
		{
			SetSkillPoint(GetSkillPoint()+iPoint);
			SendUserGold();
		}
		// refresh
		SendAttendaceItemshopStatus();
		return TRUE;
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_ATTENDANCEITEMSHOP, CALL_SP, FAIL, "EVENT_ATTENDANCE_ITEMSHOP_BuyItem  GameIDIndex:%d,  ItemCode:%d,  PropertyValue:%d,  Error:%d"
			,GetGameIDIndex(), pItemInfo->_iItemCode, pItemInfo->_iPropertyValue , iODBCReturn);
	}

	return FALSE;
}

void CFSGameUser::SendAttendanceItemShop( int iSex )
{
	CPacketComposer PacketComposer( S2C_SHOPITEM_LIST_RES );
	PacketComposer.Add((int)OP2_UPDATE);
	PacketComposer.Add(iSex);
	PacketComposer.Add((int)1); // count

//	CHECK_CONDITION_RETURN_VOID( FALSE == ATTENDANCEITEMSHOP.MakePacketAttenDanceSubItem(PacketComposer));
	CHECK_CONDITION_RETURN_VOID( FALSE == ATTENDANCEITEMSHOP.CheckAndMakePacketAttenDanceTodayItem(_GetCurrentDBDate, PacketComposer) );

	Send(&PacketComposer, FALSE);
}

void CFSGameUser::GiveLvUpEventReward(int iPreLv, BOOL bLogin)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == LVUPEVENT.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == IsJumpingPlayerAvatar());

	if(bLogin == TRUE)
	{
		LVUPEVENT.GiveRewardLoginTime(this);
	}
	else
	{
		LVUPEVENT.GiveReward(this, iPreLv);
	}
}

void CFSGameUser::SendJumpingLvUpEventUserInfo()
{
	SS2C_LVUPEVENT_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_LVUPEVENT_USER_INFO_RES));

	ss.bIsChoicePlayer = GetJumpingPlayerGameID(ss.szGameID);

	Send(S2C_LVUPEVENT_USER_INFO_RES, &ss, sizeof(SS2C_LVUPEVENT_USER_INFO_RES));
}

int		CFSGameUser::GetJumpingPlayerGameIDIndex()
{
	if(0 >= m_iJumpingPlayerGameIDIndex)
	{
		CHECK_CONDITION_RETURN(FALSE == LoadJumpingPlayerAvatarInfo(m_iJumpingPlayerGameIDIndex), -1);
	}

	return m_iJumpingPlayerGameIDIndex;
}

BOOL	CFSGameUser::GetJumpingPlayerGameID(char* szGameID)
{
	return GetMyAvatarGameID(GetJumpingPlayerGameIDIndex(), szGameID);
}

BOOL	CFSGameUser::IsJumpingPlayerAvatar()
{
	return (GetJumpingPlayerGameIDIndex() == GetGameIDIndex());
}

void CFSGameUser::SendChangeFaceNotice()
{
	CHECK_CONDITION_RETURN_VOID(GetUserItemList()->GetMode() != ITEM_PAGE_MODE_MYITEM);

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	int iFaceItemCode = pAvatarInfo->AvatarFeature.GetFaceItemCode(pAvatarInfo->iSpecialCharacterIndex);

	int iFaceIndex = AVATARCREATEMANAGER.GetMultipleFaceCharacterFaceIndex(pAvatarInfo->iSpecialCharacterIndex, iFaceItemCode);
	CHECK_CONDITION_RETURN_VOID(iFaceIndex == -1);

	SS2C_CHARACTER_FACE_STATUS_NOT rs;
	rs.btFaceIndex = (BYTE)iFaceIndex;
	Send(S2C_CHARACTER_FACE_STATUS_NOT, &rs, sizeof(SS2C_CHARACTER_FACE_STATUS_NOT));	
}

BOOL CFSGameUser::LuckyStar_GiveReward( int iRewardIndex )
{
	CHECK_CONDITION_RETURN(0 >= iRewardIndex, FALSE);

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, iRewardIndex);
	CHECK_NULL_POINTER_BOOL(pReward);

	SG2C_LUCKYSTAR_REWARD_INFO_NOT info;
	SRewardConfigItem* pRewardItem = (SRewardConfigItem*)pReward;

	info.iItemCode = pRewardItem->iItemCode;

	if(IsPlaying()) 
	{
		CPacketComposer *Sp = new CPacketComposer(S2C_LUCKYSTAR_REWARD_INFO_NOT);		
		if(Sp == NULL)
		{
			return FALSE;
		}
		Sp->Add( (PBYTE)&info, sizeof(SG2C_LUCKYSTAR_REWARD_INFO_NOT) );
		PushPacketQueue(Sp);
	}
	else
	{
		CPacketComposer Packet(S2C_LUCKYSTAR_REWARD_INFO_NOT);
		Packet.Add( (PBYTE)&info, sizeof(SG2C_LUCKYSTAR_REWARD_INFO_NOT) );
		Send(&Packet);
	}
	return TRUE;
}

void	CFSGameUser::CheckAndGiveSkillAndSlot()
{
	SAvatarInfo * pAvatar = GetCurUsedAvatar();

	if(pAvatar && 
		(pAvatar->iAvatarTeamIndex == 1001/*�ɿ�����*/ || 
		pAvatar->iAvatarTeamIndex == 1002/*����Ż��*/ ||
		pAvatar->iAvatarTeamIndex == 1003/*��ȣ���*/) &&
		pAvatar->iLv >= 31)
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID( pODBC );

		// ĳ���� ���� �������� ���԰����� ��� ��ų�� ��ų���� ����. 
		if(ODBC_RETURN_SUCCESS != pODBC->SP_SetAvatarAllSkillAndSlot(GetUserIDIndex(), GetGameIDIndex(), 4))
		{
			return;
		}

		// ��ų �������� �ٽ� �ε�.
		if(pAvatar) 
		{
			if(ODBC_RETURN_SUCCESS != pODBC->spGetAvatarSkillInfo(pAvatar) ||
				ODBC_RETURN_SUCCESS != pODBC->spGetUserSkillSlot(GetUserIDIndex(), GetGameIDIndex(), pAvatar->Skill))
			{
				return;
			}

			pAvatar->Skill.Convert();
		}
	}
}

void	CFSGameUser::SendEventPlusCardInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PLUSCARD_EVENT));

	CPacketComposer Packet(S2C_PLUSCARD_EVENT_INFO_NOT);

	RANDOMCARDSHOP.MakeEventPlusCardInfo(Packet);

	Send(&Packet);
}

SUserPingInfo* CFSGameUser::GetUserPingInfo()
{
	return &m_UserPingInfo;
}

void CFSGameUser::SendExchangeNameAvatarList()
{
	const int MAX_SEND_AVATAR_LIST_SIZE = 200;

	SS2C_EXCHANGE_NAME_INFO_RES rs;
	rs.bAddList = FALSE;
	rs.iAvatarCnt = 0;

	CPacketComposer Packet(S2C_EXCHANGE_NAME_INFO_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EXCHANGE_NAME_INFO_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	if(m_listMyAvatar.empty())
	{
		LoadMyAvatarList();
	}

	int iCount = 0;
	MY_AVATAR_LIST::const_iterator iter = m_listMyAvatar.cbegin();
	for( ; iter != m_listMyAvatar.cend(); ++iter)
	{
		SUSER_EXCHANGE_NAME_AVATAR info;
		info.iGameIDIndex = (*iter)->iGameIDIndex;
		strncpy_s(info.szGameID, _countof(info.szGameID), (*iter)->szGameID, MAX_GAMEID_LENGTH);
		info.iGamePosition = (*iter)->iGamePosition;
		info.btSex = (*iter)->btSex;
		info.iLv = (*iter)->iLv;
		Packet.Add((PBYTE)&info, sizeof(SUSER_EXCHANGE_NAME_AVATAR));

		++rs.iAvatarCnt;

		if(++iCount >= MAX_SEND_AVATAR_LIST_SIZE)
			break;
	}

	memcpy(pCountLocation, &rs.iAvatarCnt, sizeof(int));
	Send(&Packet);

	CHECK_CONDITION_RETURN_VOID(iCount == m_listMyAvatar.size());

	Packet.Initialize(S2C_EXCHANGE_NAME_INFO_RES);
	rs.bAddList = TRUE;
	rs.iAvatarCnt = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EXCHANGE_NAME_INFO_RES));
	pCountLocation = Packet.GetTail() - sizeof(int);

	iter++;
	iCount = 0;
	for( ; iter != m_listMyAvatar.end(); ++iter)
	{
		SUSER_EXCHANGE_NAME_AVATAR info;
		info.iGameIDIndex = (*iter)->iGameIDIndex;
		strncpy_s(info.szGameID, _countof(info.szGameID), (*iter)->szGameID, MAX_GAMEID_LENGTH);
		info.iGamePosition = (*iter)->iGamePosition;
		info.btSex = (*iter)->btSex;
		info.iLv = (*iter)->iLv;
		Packet.Add((PBYTE)&info, sizeof(SUSER_EXCHANGE_NAME_AVATAR));

		++rs.iAvatarCnt;

		if(++iCount >= MAX_SEND_AVATAR_LIST_SIZE)
			break;
	}

	memcpy(pCountLocation, &rs.iAvatarCnt, sizeof(int));
	Send(&Packet);
}

void CFSGameUser::UpdateExchangeNameItem(char* szNewGameID)
{
	MY_AVATAR_NAME_MAP::iterator iter = m_mapMyAvatarName.find(szNewGameID);
	if(iter == m_mapMyAvatarName.end())
	{
		return;
	}

	strncpy_s(iter->second->szGameID, _countof(iter->second->szGameID), szNewGameID, MAX_GAMEID_LENGTH+1);
}

RESULT_EXCHANGE_NAME CFSGameUser::ExchangeNameReq(SC2S_EXCHANGE_NAME_REQ rq)
{
	CHECK_CONDITION_RETURN(rq.iGameIDIndex1 == rq.iGameIDIndex2, RESULT_EXCHANGE_NAME_FAIL);

	MY_AVATAR_MAP::iterator iter1 = m_mapMyAvatar.find(rq.iGameIDIndex1);
	CHECK_CONDITION_RETURN(iter1 == m_mapMyAvatar.end(), RESULT_EXCHANGE_NAME_FAIL);

	MY_AVATAR_MAP::iterator iter2 = m_mapMyAvatar.find(rq.iGameIDIndex2);
	CHECK_CONDITION_RETURN(iter2 == m_mapMyAvatar.end(), RESULT_EXCHANGE_NAME_FAIL);

	CFSGameUserItem* pUserItemList = GetUserItemList();
	CHECK_CONDITION_RETURN(NULL == pUserItemList, RESULT_EXCHANGE_NAME_FAIL);

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_CONDITION_RETURN(NULL == pAvatarItemList, RESULT_EXCHANGE_NAME_FAIL);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(rq.iItemIndex);
	CHECK_CONDITION_RETURN(NULL == pItem, RESULT_EXCHANGE_NAME_FAIL);

	CHECK_CONDITION_RETURN(pItem->iPropertyKind != ITEM_PROPERTY_KIND_EXCHANGE_AVATAR_NAME, RESULT_EXCHANGE_NAME_FAIL);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EXCHANGE_NAME_FAIL);

	int iClubIndex1 = 0, iClubIndex2 = 0;
	if(ODBC_RETURN_SUCCESS != pODBC->AVATAR_ExchangeGameID(GetUserIDIndex(), GetGameIDIndex(), rq.iGameIDIndex1, rq.iGameIDIndex2, rq.iItemIndex, FACTION.GetCurrentSeasonIndex(), GetFactionIndex(), iClubIndex1, iClubIndex2))
	{
		return RESULT_EXCHANGE_NAME_FAIL;
	}

	GetAvatarItemList()->RemoveItem(rq.iItemIndex);

	char szGameID[MAX_GAMEID_LENGTH+1];
	strncpy_s(szGameID, _countof(szGameID), iter1->second.szGameID, MAX_GAMEID_LENGTH+1);
	strncpy_s(iter1->second.szGameID, _countof(iter1->second.szGameID), iter2->second.szGameID, MAX_GAMEID_LENGTH+1);
	strncpy_s(iter2->second.szGameID, _countof(iter2->second.szGameID), szGameID, MAX_GAMEID_LENGTH+1);

	char szOldGameID[MAX_GAMEID_LENGTH+1];
	char szNewGameID[MAX_GAMEID_LENGTH+1];
	if(iter1->second.iGameIDIndex == GetGameIDIndex())
	{
		strncpy_s(szOldGameID, _countof(szOldGameID), GetGameID(), MAX_GAMEID_LENGTH+1);
		SetAvatarGameID(iter1->second.szGameID);
		strncpy_s(szNewGameID, _countof(szNewGameID), iter1->second.szGameID, MAX_GAMEID_LENGTH+1);
	}
	else if(iter2->second.iGameIDIndex == GetGameIDIndex())
	{
		strncpy_s(szOldGameID, _countof(szOldGameID), GetGameID(), MAX_GAMEID_LENGTH+1);
		SetAvatarGameID(iter2->second.szGameID);
		strncpy_s(szNewGameID, _countof(szNewGameID), iter2->second.szGameID, MAX_GAMEID_LENGTH+1);
	}

	if(iter1->second.iGameIDIndex == GetGameIDIndex() ||
		iter2->second.iGameIDIndex == GetGameIDIndex())
	{
		CFSGameServer* pServer = CFSGameServer::GetInstance();
		CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EXCHANGE_NAME_FAIL);

		pServer->LockUserManager();
		pServer->RemoveUser(this, FALSE);
		SetGameID(szNewGameID);
		if(!pServer->AddUser((CUser *)this)) 
		{
			WRITE_LOG_NEW(LOG_TYPE_AVATAR, EXEC_FUNCTION, FAIL, "ExchangeNameReq::AddUser - NewGameID:%s", szNewGameID);
		}
		pServer->UnlockUserManager();

		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if(pCenter)
		{
			BOOL bLogin = FALSE;
			if(iter1->second.iGameIDIndex == GetGameIDIndex())
				bLogin = TRUE;

			int iFactionIndex1 = FACTION.CheckScoreRankGameID(this, iter2->second.szGameID);
			int iFactionIndex2 = FACTION.CheckScoreRankGameID(this, iter1->second.szGameID);

			pCenter->SendRequestGameIDChange(GetUserIDIndex(), iter1->second.iGameIDIndex, (LPSTR)iter2->second.szGameID, (LPSTR)iter1->second.szGameID, iFactionIndex1, bLogin);
			
			bLogin = FALSE;
			if(iter2->second.iGameIDIndex == GetGameIDIndex())
				bLogin = TRUE;

			pCenter->SendRequestGameIDChange(GetUserIDIndex(), iter2->second.iGameIDIndex, (LPSTR)iter1->second.szGameID, (LPSTR)iter2->second.szGameID, iFactionIndex2, bLogin);
		}

		CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(GetMatchLocation());
		if(pMatch) 
		{
			SG2M_CHANGE_GAMEID_REQ Info;
			Info.iGameIDIndex = GetGameIDIndex();
			Info.btServerIndex = _GetServerIndex;

			strncpy_s(Info.szGameIDNew, _countof(Info.szGameIDNew), GetGameID(), MAX_GAMEID_LENGTH+1);

			pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_CHANGE_GAMEID_REQ, &Info, sizeof(SG2M_CHANGE_GAMEID_REQ));			

		}

		CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
		if(pChat)
		{
			SS2T_CHANGENAME info;
			info.iGameIDIndex = GetGameIDIndex();
			strcpy_s(info.szGameID, GetGameID());
			CPacketComposer Packet(S2T_CHANGENAME); 
			Packet.Add((PBYTE)&info, sizeof(SS2T_CHANGENAME));
			pChat->Send(&Packet);
		}

		CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
		if(pClubProxy)
		{ 
			SG2CL_CHANGE_MEMBERID_REQ info;
			info.iClubSN = iClubIndex1;
			strncpy_s(info.szGameID, _countof(info.szGameID), iter2->second.szGameID, MAX_GAMEID_LENGTH+1);
			info.iGameIDIndex = iter1->second.iGameIDIndex;
			strncpy_s(info.szGameIDNew, _countof(info.szGameIDNew), iter1->second.szGameID, MAX_GAMEID_LENGTH+1);
			info.btStatus = CLUB_MEMBER_OFFLINE;
			if(GetGameIDIndex() == iter1->second.iGameIDIndex)
				info.btStatus = CLUB_MEMBER_LOCATION_DIFF;
			info.bExchangeGameID = TRUE;
			pClubProxy->SendGameIDChangeReq(info); 

			info.iClubSN = iClubIndex2;
			strncpy_s(info.szGameID, _countof(info.szGameID), iter1->second.szGameID, MAX_GAMEID_LENGTH+1);
			info.iGameIDIndex = iter2->second.iGameIDIndex;
			strncpy_s(info.szGameIDNew, _countof(info.szGameIDNew), iter2->second.szGameID, MAX_GAMEID_LENGTH+1);
			info.btStatus = CLUB_MEMBER_OFFLINE;
			if(GetGameIDIndex() == iter2->second.iGameIDIndex)
				info.btStatus = CLUB_MEMBER_LOCATION_DIFF;
			info.bExchangeGameID = TRUE;
			pClubProxy->SendGameIDChangeReq(info); 
		}

		SendAvatarInfo();
	}

	if(rq.bNotice == TRUE)
	{
		vector<SG2S_EXCHANGE_GAMEID_NOTICE> vNotice;
		if(ODBC_RETURN_SUCCESS == pODBC->AVATAR_ExchangeGameID_Notice(iter1->second.iGameIDIndex, iter2->second.szGameID, iter1->second.szGameID, vNotice))
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
			if(pCenter)
			{
				for(int i = 0; i < vNotice.size(); ++i)
				{
					CPacketComposer Packet(G2S_EXCHANGE_GAMEID_NOTICE);
					Packet.Add((BYTE*)&vNotice[i], sizeof(SG2S_EXCHANGE_GAMEID_NOTICE));
					pCenter->Send(&Packet);	
				}			
			}
		}

		vNotice.clear();
		if(ODBC_RETURN_SUCCESS == pODBC->AVATAR_ExchangeGameID_Notice(iter2->second.iGameIDIndex, iter1->second.szGameID, iter2->second.szGameID, vNotice))
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
			if(pCenter)
			{
				for(int i = 0; i < vNotice.size(); ++i)
				{
					CPacketComposer Packet(G2S_EXCHANGE_GAMEID_NOTICE);
					Packet.Add((BYTE*)&vNotice[i], sizeof(SG2S_EXCHANGE_GAMEID_NOTICE));
					pCenter->Send(&Packet);	
				}			
			}
		}	
	}

	if(OPEN == HIPPOCAMPUS.IsOpen())
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			LoadRepresentInfo(TRUE);

			SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT not;
			not.btServerIndex = _GetServerIndex;
			GetUserHippocampusEvent()->MakeIntegrateDB_UpdateUserLog(not);

			CPacketComposer	Packet(S2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT);
			Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT));

			pODBCSvr->Send(&Packet);
		}		
	}

	if(OPEN == GAMEOFDICEEVENT.IsOpen())
	{
		LoadRepresentInfo(TRUE);
		GetUserGameOfDiceEvent()->CheckAndUpdateRankGameID();
	}

	if(OPEN == TRANSFERJOYCITY100DREAM.IsOpen())
	{
		LoadRepresentInfo(TRUE);
		GetUserTransferJoycity100DreamEvent()->CheckAndUpdateRankGameID();
	}

	GetUserPVEMission()->UpdateRankGameID(iter1->second.iGameIDIndex, szNewGameID);
	GetUserPVEMission()->UpdateRankGameID(iter2->second.iGameIDIndex, szNewGameID);

	return RESULT_EXCHANGE_NAME_SUCCESS;
}

void CFSGameUser::SendChangeCharStatInfo()
{
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	int iConvertCurrentStyle = (m_AvatarStyleInfo.iUseStyle < CLONE_CHARACTER_DEFULAT_STYLE_INDEX) ? 0 : 1;

	SpecialAvatarStatInfo* pStat = AVATARCREATEMANAGER.GetSpecialAvatarAddStat(pAvatarInfo->iSpecialCharacterIndex, pAvatarInfo->Status.iHeight, pAvatarInfo->Status.iGamePosition, iConvertCurrentStyle);
	CHECK_NULL_POINTER_VOID(pStat);

	CHECK_CONDITION_RETURN_VOID(pStat->iBonusStat == 0);

	int iaTrainingStatAdd[MAX_STAT_TYPE_COUNT];
	int iTrainingStep[STAT_TRAINING_STEP_4 + 1];
	memset(iaTrainingStatAdd, 0, sizeof(int)*MAX_STAT_TYPE_COUNT);
	for ( int i = STAT_TRAINING_STEP_1 ; i < STAT_TRAINING_STEP_4 + 1; ++i )
	{
		iTrainingStep[i] = 1 << i;
	}

	for (int i = STAT_TRAINING_CATEGOTY_RUN; i <= STAT_TRAINING_CATEGOTY_CLOSE; ++i)
	{
		for ( int j = STAT_TRAINING_STEP_1; j < STAT_TRAINING_STEP_4 + 1; ++j )
		{
			if ( pAvatarInfo->Skill.iaTraining[i] & iTrainingStep[j] )
			{
				switch(i)
				{
				case STAT_TRAINING_CATEGOTY_RUN:		++iaTrainingStatAdd[STAT_TYPE_RUN];			break;
				case STAT_TRAINING_CATEGOTY_JUMP:		++iaTrainingStatAdd[STAT_TYPE_JUMP];		break;
				case STAT_TRAINING_CATEGOTY_STR:		++iaTrainingStatAdd[STAT_TYPE_STR];			break;
				case STAT_TRAINING_CATEGOTY_PASS:		++iaTrainingStatAdd[STAT_TYPE_PASS];		break;
				case STAT_TRAINING_CATEGOTY_DRIBBLE:	++iaTrainingStatAdd[STAT_TYPE_DRIBBLE];		break;
				case STAT_TRAINING_CATEGOTY_REBOUND:	++iaTrainingStatAdd[STAT_TYPE_REBOUND];		break;
				case STAT_TRAINING_CATEGOTY_BLOCK:		++iaTrainingStatAdd[STAT_TYPE_BLOCK];		break;	
				case STAT_TRAINING_CATEGOTY_STEAL:		++iaTrainingStatAdd[STAT_TYPE_STEAL];		break;
				case STAT_TRAINING_CATEGOTY_2POINT:		++iaTrainingStatAdd[STAT_TYPE_2POINT];		break;
				case STAT_TRAINING_CATEGOTY_3POINT:		++iaTrainingStatAdd[STAT_TYPE_3POINT];		break;
				case STAT_TRAINING_CATEGOTY_DRIVE:		++iaTrainingStatAdd[STAT_TYPE_DRIVE];		break;
				case STAT_TRAINING_CATEGOTY_CLOSE:		++iaTrainingStatAdd[STAT_TYPE_CLOSE];		break;
				default: break;
				}
			}
		}
	}

	int iaItemStatAdd[MAX_STAT_NUM];
	int iaItemMinusStat[MAX_STAT_NUM];
	memset(iaItemStatAdd, 0, sizeof(int)*MAX_STAT_NUM);
	memset(iaItemMinusStat, 0, sizeof(int)*MAX_STAT_NUM);
	GetAvatarItemStatus(iaItemStatAdd, iaItemMinusStat);
	GetAvatarSentenceStatus(iaItemStatAdd);
	GetAvatarPowerupCapsuleStatus(iaItemStatAdd);
	GetLvIntervalBuffItemStatus(iaItemStatAdd);
	GetAvatarSpecialPieceProperty(iaItemStatAdd);
	GetCheerLeaderStatus(iaItemStatAdd);

	int iaLinkStatAdd[MAX_STAT_NUM];
	memset(iaLinkStatAdd, 0, sizeof(int)*MAX_STAT_NUM);

	GetAvatarLinkItemStatus(iaLinkStatAdd);

	int iaFameSkillAddStat[MAX_STAT_NUM] = {0};
	int iaFameSkillMinusStat[MAX_STAT_NUM] = {0}; 
	SetAvatarFameSkillAddMinusStatus(iaFameSkillAddStat, iaFameSkillMinusStat);

	SS2C_CHAGNE_CHAR_STAT_INFO_RES rs;
	rs.iaStat[STAT_TYPE_RUN] = pAvatarInfo->Status.iStatRunBase + pStat->iaAddStat[STAT_TYPE_RUN] + iaTrainingStatAdd[STAT_TYPE_RUN];
	rs.iaStat[STAT_TYPE_JUMP] = pAvatarInfo->Status.iStatJumpBase + pStat->iaAddStat[STAT_TYPE_JUMP] + iaTrainingStatAdd[STAT_TYPE_JUMP];
	rs.iaStat[STAT_TYPE_STR] = pAvatarInfo->Status.iStatStrBase + pStat->iaAddStat[STAT_TYPE_STR] + iaTrainingStatAdd[STAT_TYPE_STR];
	rs.iaStat[STAT_TYPE_PASS] = pAvatarInfo->Status.iStatPassBase + pStat->iaAddStat[STAT_TYPE_PASS] + iaTrainingStatAdd[STAT_TYPE_PASS];
	rs.iaStat[STAT_TYPE_DRIBBLE] = pAvatarInfo->Status.iStatDribbleBase + pStat->iaAddStat[STAT_TYPE_DRIBBLE] + iaTrainingStatAdd[STAT_TYPE_DRIBBLE];
	rs.iaStat[STAT_TYPE_REBOUND] = pAvatarInfo->Status.iStatReboundBase + pStat->iaAddStat[STAT_TYPE_REBOUND] + iaTrainingStatAdd[STAT_TYPE_REBOUND];
	rs.iaStat[STAT_TYPE_BLOCK] = pAvatarInfo->Status.iStatBlockBase + pStat->iaAddStat[STAT_TYPE_BLOCK] + iaTrainingStatAdd[STAT_TYPE_BLOCK];
	rs.iaStat[STAT_TYPE_STEAL] = pAvatarInfo->Status.iStatStealBase + pStat->iaAddStat[STAT_TYPE_STEAL] + iaTrainingStatAdd[STAT_TYPE_STEAL];
	rs.iaStat[STAT_TYPE_2POINT] = pAvatarInfo->Status.iStat2PointBase + pStat->iaAddStat[STAT_TYPE_2POINT] + iaTrainingStatAdd[STAT_TYPE_2POINT];
	rs.iaStat[STAT_TYPE_3POINT] = pAvatarInfo->Status.iStat3PointBase + pStat->iaAddStat[STAT_TYPE_3POINT] + iaTrainingStatAdd[STAT_TYPE_3POINT];
	rs.iaStat[STAT_TYPE_DRIVE] = pAvatarInfo->Status.iStatDriveBase + pStat->iaAddStat[STAT_TYPE_DRIVE] + iaTrainingStatAdd[STAT_TYPE_DRIVE];
	rs.iaStat[STAT_TYPE_CLOSE] = pAvatarInfo->Status.iStatCloseBase + pStat->iaAddStat[STAT_TYPE_CLOSE] + iaTrainingStatAdd[STAT_TYPE_CLOSE];

	for(int i = 0; i < MAX_STAT_TYPE_COUNT; ++i)
	{
		rs.iaStatAdd[i] = iaItemStatAdd[i] + iaItemMinusStat[i] + iaLinkStatAdd[i]  + iaFameSkillAddStat[i] + iaFameSkillMinusStat[i];
	}

	rs.iBonusStat = pStat->iBonusStat;
	rs.iMaxUseableBonusStat = pStat->iMaxUseableBonusStat;

	CPacketComposer Packet(S2C_CHAGNE_CHAR_STAT_INFO_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CHAGNE_CHAR_STAT_INFO_RES));
	Send(&Packet);
}

RESULT_CHAGNE_CHAR_STAT CFSGameUser::ChangeCharStatReq(SC2S_CHAGNE_CHAR_STAT_REQ rq)
{
	CFSGameUserItem* pUserItemList = GetUserItemList();
	CHECK_CONDITION_RETURN(NULL == pUserItemList, RESULT_CHAGNE_CHAR_STAT_FAIL);

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_CONDITION_RETURN(NULL == pAvatarItemList, RESULT_CHAGNE_CHAR_STAT_FAIL);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(rq.iItemIndex);
	CHECK_CONDITION_RETURN(NULL == pItem, RESULT_CHAGNE_CHAR_STAT_FAIL);

	CHECK_CONDITION_RETURN(pItem->iPropertyKind != ITEM_PROPERTY_KIND_CHANGE_BONUS_STAT, RESULT_CHAGNE_CHAR_STAT_FAIL);

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_CONDITION_RETURN(NULL == pAvatarInfo, RESULT_CHAGNE_CHAR_STAT_FAIL);

	SpecialAvatarStatInfo* pStat = AVATARCREATEMANAGER.GetSpecialAvatarAddStat(pAvatarInfo->iSpecialCharacterIndex, pAvatarInfo->Status.iHeight, pAvatarInfo->Status.iGamePosition);
	CHECK_CONDITION_RETURN(NULL == pStat, RESULT_CHAGNE_CHAR_STAT_FAIL);
	CHECK_CONDITION_RETURN(pStat->iBonusStat == 0, RESULT_CHAGNE_CHAR_STAT_FAIL);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_CHAGNE_CHAR_STAT_FAIL);

	int iTotalBonusStat = 0;
	for(int i = 0; i < MAX_STAT_TYPE_COUNT; ++i)
	{
		CHECK_CONDITION_RETURN(rq.iaBonusStat[i] > pStat->iMaxUseableBonusStat, RESULT_CHAGNE_CHAR_STAT_FAIL);
		iTotalBonusStat += rq.iaBonusStat[i];
	}

	CHECK_CONDITION_RETURN(iTotalBonusStat < pStat->iBonusStat, RESULT_CHAGNE_CHAR_STAT_FAIL_NOT_ALL_USE_STAT);
	CHECK_CONDITION_RETURN(iTotalBonusStat > pStat->iBonusStat, RESULT_CHAGNE_CHAR_STAT_FAIL);

	int iaTrainingStatAdd[MAX_STAT_TYPE_COUNT];
	int iTrainingStep[STAT_TRAINING_STEP_4 + 1];
	memset(iaTrainingStatAdd, 0, sizeof(int)*MAX_STAT_TYPE_COUNT);
	for ( int i = STAT_TRAINING_STEP_1 ; i < STAT_TRAINING_STEP_4 + 1; ++i )
	{
		iTrainingStep[i] = 1 << i;
	}

	BOOL bIsClone = FALSE;
	if(TRUE == GetCloneCharacter())
	{
		int iUseStyle = GetUseStyleIndex();
		Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > iUseStyle ? Clone_Index_Mine : Clone_Index_Clone;

		if(eIndex == Clone_Index_Mine)
			bIsClone = FALSE;
		else 
			bIsClone = TRUE;
	}

	for (int i = STAT_TRAINING_CATEGOTY_RUN; i <= STAT_TRAINING_CATEGOTY_CLOSE; ++i)
	{
		for ( int j = STAT_TRAINING_STEP_1; j < STAT_TRAINING_STEP_4 + 1; ++j )
		{
			if ( pAvatarInfo->Skill.iaTraining[i] & iTrainingStep[j] )
			{
				switch(i)
				{
				case STAT_TRAINING_CATEGOTY_RUN:		++iaTrainingStatAdd[STAT_TYPE_RUN];			break;
				case STAT_TRAINING_CATEGOTY_JUMP:		++iaTrainingStatAdd[STAT_TYPE_JUMP];		break;
				case STAT_TRAINING_CATEGOTY_STR:		++iaTrainingStatAdd[STAT_TYPE_STR];			break;
				case STAT_TRAINING_CATEGOTY_PASS:		++iaTrainingStatAdd[STAT_TYPE_PASS];		break;
				case STAT_TRAINING_CATEGOTY_DRIBBLE:	++iaTrainingStatAdd[STAT_TYPE_DRIBBLE];		break;
				case STAT_TRAINING_CATEGOTY_REBOUND:	++iaTrainingStatAdd[STAT_TYPE_REBOUND];		break;
				case STAT_TRAINING_CATEGOTY_BLOCK:		++iaTrainingStatAdd[STAT_TYPE_BLOCK];		break;	
				case STAT_TRAINING_CATEGOTY_STEAL:		++iaTrainingStatAdd[STAT_TYPE_STEAL];		break;
				case STAT_TRAINING_CATEGOTY_2POINT:		++iaTrainingStatAdd[STAT_TYPE_2POINT];		break;
				case STAT_TRAINING_CATEGOTY_3POINT:		++iaTrainingStatAdd[STAT_TYPE_3POINT];		break;
				case STAT_TRAINING_CATEGOTY_DRIVE:		++iaTrainingStatAdd[STAT_TYPE_DRIVE];		break;
				case STAT_TRAINING_CATEGOTY_CLOSE:		++iaTrainingStatAdd[STAT_TYPE_CLOSE];		break;
				default: break;
				}
			}
		}
	}

	int iaAddStat[MAX_STAT_TYPE_COUNT];
	memset(iaAddStat, 0, sizeof(int)*MAX_STAT_TYPE_COUNT);

	for(int i = 0; i < MAX_STAT_TYPE_COUNT; ++i)
	{
		if( TRUE == bIsClone )
			iaAddStat[i] = pStat->iaAddStat[i] + rq.iaBonusStat[i];
		
		else
			iaAddStat[i] = pStat->iaAddStat[i] + iaTrainingStatAdd[i] + rq.iaBonusStat[i];	
	}

	if(ODBC_RETURN_SUCCESS != pODBC->AVATAR_ChangeBonusStat(GetUserIDIndex(), GetGameIDIndex(), iaAddStat, rq.iItemIndex, pAvatarInfo->iSpecialCharacterIndex))
	{
		return RESULT_CHAGNE_CHAR_STAT_FAIL;
	}

	int iConvertCurrentStyle = (m_AvatarStyleInfo.iUseStyle < CLONE_CHARACTER_DEFULAT_STYLE_INDEX) ? 0 : 1;

	pStat = AVATARCREATEMANAGER.GetSpecialAvatarAddStat(pAvatarInfo->iSpecialCharacterIndex, pAvatarInfo->Status.iHeight, pAvatarInfo->Status.iGamePosition, iConvertCurrentStyle);
	CHECK_CONDITION_RETURN(NULL == pStat, RESULT_CHAGNE_CHAR_STAT_FAIL);

	memset(iaAddStat, 0, sizeof(int)*MAX_STAT_TYPE_COUNT);

	for(int i = 0; i < MAX_STAT_TYPE_COUNT; ++i)
	{
		if( TRUE == bIsClone )
			iaAddStat[i] = pStat->iaAddStat[i] + rq.iaBonusStat[i];

		else
			iaAddStat[i] = pStat->iaAddStat[i] + iaTrainingStatAdd[i] + rq.iaBonusStat[i];
	}

	if( TRUE == GetCloneCharacter() )
	{
		Clone_Index CloneIndex = TRUE == bIsClone ? Clone_Index_Clone : Clone_Index_Mine;
		for(int j = 0; j < MAX_STAT_TYPE_COUNT; ++j)
		{
			m_sCloneCharacterInfo[CloneIndex].iaAddStat[j] = iaAddStat[j] = pStat->iaAddStat[j] + iaTrainingStatAdd[j] + rq.iaBonusStat[j];
		}
	}

	pAvatarInfo->Status.iStatRun = pAvatarInfo->Status.iStatRunBase + iaAddStat[STAT_TYPE_RUN];
	pAvatarInfo->Status.iStatJump = pAvatarInfo->Status.iStatJumpBase + iaAddStat[STAT_TYPE_JUMP];
	pAvatarInfo->Status.iStatStr = pAvatarInfo->Status.iStatStrBase + iaAddStat[STAT_TYPE_STR];
	pAvatarInfo->Status.iStatPass = pAvatarInfo->Status.iStatPassBase + iaAddStat[STAT_TYPE_PASS];
	pAvatarInfo->Status.iStatDribble = pAvatarInfo->Status.iStatDribbleBase + iaAddStat[STAT_TYPE_DRIBBLE];
	pAvatarInfo->Status.iStatRebound = pAvatarInfo->Status.iStatReboundBase + iaAddStat[STAT_TYPE_REBOUND];
	pAvatarInfo->Status.iStatBlock = pAvatarInfo->Status.iStatBlockBase + iaAddStat[STAT_TYPE_BLOCK];
	pAvatarInfo->Status.iStatSteal = pAvatarInfo->Status.iStatStealBase + iaAddStat[STAT_TYPE_STEAL];
	pAvatarInfo->Status.iStat2Point = pAvatarInfo->Status.iStat2PointBase + iaAddStat[STAT_TYPE_2POINT];
	pAvatarInfo->Status.iStat3Point = pAvatarInfo->Status.iStat3PointBase + iaAddStat[STAT_TYPE_3POINT];
	pAvatarInfo->Status.iStatDrive = pAvatarInfo->Status.iStatDriveBase + iaAddStat[STAT_TYPE_DRIVE];
	pAvatarInfo->Status.iStatClose = pAvatarInfo->Status.iStatCloseBase + iaAddStat[STAT_TYPE_CLOSE];

	GetAvatarItemList()->RemoveItem(rq.iItemIndex);

	return RESULT_CHAGNE_CHAR_STAT_SUCCESS;
}

void CFSGameUser::SendFirstCashBackEventStatus()
{
	OPEN_STATUS eOpenStatus = EVENTDATEMANAGER.IsOpen(EVENT_KIND_FIRST_CASHBACK);
	CHECK_CONDITION_RETURN_VOID(CLOSED == eOpenStatus);

	if(FALSE == CheckFirstCashBackEventUser())
		eOpenStatus = CLOSED;

	SS2C_FIRST_CASHBACK_EVENT_NOT info;
	info.btOpenStatus = eOpenStatus;
	Send(S2C_FIRST_CASHBACK_EVENT_NOT, &info, sizeof(SS2C_FIRST_CASHBACK_EVENT_NOT));
}

BOOL CFSGameUser::IsSkyLuckyEventAllClear()
{
	return GetUserSkyLuckyEvent()->IsEventAllClear();
}

BOOL CFSGameUser::IsGoldenSafeEventButtonOpen()
{
	return GetUserGoldenSafeEvent()->IsGoldenSafeEventButtonOpen();
}

BOOL CFSGameUser::IsHoneyWalletEventOnGoing()
{
	return GetUserHoneyWalletEvent()->IsEventOnGoing();
}

BOOL CFSGameUser::IsBigWheelLoginEventAllClear()
{
	return GetUserBigWheelLoginEvent()->IsEventAllClear();
}

void CFSGameUser::SendRandomCardEventStatus()
{
	OPEN_STATUS eOpenStatus = EVENTDATEMANAGER.IsOpen(EVENT_KIND_RANDOMCARD);
	if(CLOSED == eOpenStatus)
		eOpenStatus =  EVENTDATEMANAGER.IsOpen(EVENT_KIND_RANDOMCARD_SALE);

	SS2C_EVENT_RANDOMCARD_NOT not;
	not.btOpenStatus = eOpenStatus;

	if( FALSE == CheckEventButtonSend(EVENT_BUTTON_TYPE_RANDOMCARD, not.btOpenStatus))
		return;

	Send(S2C_EVENT_RANDOMCARD_NOT, &not, sizeof(SS2C_EVENT_RANDOMCARD_NOT));
}

void CFSGameUser::SendItemShopEventButton()
{
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	if( TRUE == CFSGameServer::GetInstance()->CheckItemShopEventButtonRender()) 
	{
		CPacketComposer	Packet(S2C_ITEMSHOP_EVENTBUTTON_RENDER_NOT);
		SS2C_ITEMSHOP_EVENTBUTTON_RENDER_NOT	not;

		not.btOpenStatus = (FALSE == pItemShop->IsEmptyActiveSaleItem()) ? OPEN : CLOSED;

		if(CLOSED == not.btOpenStatus)
		{
			not.btOpenStatus = EVENTDATEMANAGER.IsView(EVENT_KIND_ITEMSHOP_PACKAGEITEM);
		}

		if( FALSE == CheckEventButtonSend(EVENT_BUTTON_TYPE_ITEMSHOP, not.btOpenStatus))
			return;

		Packet.Add((PBYTE)&not, sizeof(SS2C_ITEMSHOP_EVENTBUTTON_RENDER_NOT));

		Send(&Packet);
	}
}


void CFSGameUser::SetLobbyEventButtonType( BYTE btEventButtonType, BYTE btStatus )
{
	if( 0 >= btEventButtonType || btEventButtonType >= MAX_EVENT_BUTTON_TYPE )
	{
		return;
	}

	m_sUserEventButtonInfo[btEventButtonType].btStatus = btStatus;
}

BYTE CFSGameUser::GetLobbyEventButtonStatus( BYTE btEventButtonType )
{
	if( 0 >= btEventButtonType || btEventButtonType >= MAX_EVENT_BUTTON_TYPE )
	{
		return (BYTE)CLOSED;
	}

	return m_sUserEventButtonInfo[btEventButtonType].btStatus;
}

BOOL CFSGameUser::CheckEventButtonSend( BYTE btEventButtonType, BYTE btCurrentStatus )
{
	if( 0 >= btEventButtonType || btEventButtonType >= MAX_EVENT_BUTTON_TYPE )
	{
		return FALSE;
	}

	BYTE btOldStatus = GetLobbyEventButtonStatus(btEventButtonType);
	SetLobbyEventButtonType(btEventButtonType, btCurrentStatus);

	if( CLOSED == btCurrentStatus && btCurrentStatus == btOldStatus )
		return FALSE;

	return TRUE;
}

void CFSGameUser::SetCloneStatToArray( int* ipaBaseStat, BOOL bIsClone )
{
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	pAvatarInfo->SetCloneStatToArray( ipaBaseStat, bIsClone );

	if( TRUE == bIsClone )
	{
		m_sCloneCharacterInfo[Clone_Index_Clone].SubtractionAddStat(ipaBaseStat);

		int iTrainingStep[STAT_TRAINING_STEP_4 + 1];
		for ( int i = STAT_TRAINING_STEP_1 ; i < STAT_TRAINING_STEP_4 + 1; ++i )
		{
			iTrainingStep[i] = 1 << i;
		}

		for (int i = STAT_TRAINING_CATEGOTY_RUN; i <= STAT_TRAINING_CATEGOTY_CLOSE; ++i)
		{
			for ( int j = STAT_TRAINING_STEP_1; j < STAT_TRAINING_STEP_4 + 1; ++j )
			{
				if ( pAvatarInfo->Skill.iaTraining[i] & iTrainingStep[j] )
				{
					switch( i )
					{
					case STAT_TRAINING_CATEGOTY_RUN:		++ipaBaseStat[STAT_TYPE_RUN];		break;
					case STAT_TRAINING_CATEGOTY_JUMP:		++ipaBaseStat[STAT_TYPE_JUMP];		break;
					case STAT_TRAINING_CATEGOTY_STR:		++ipaBaseStat[STAT_TYPE_STR];		break;
					case STAT_TRAINING_CATEGOTY_PASS:		++ipaBaseStat[STAT_TYPE_PASS];		break;
					case STAT_TRAINING_CATEGOTY_DRIBBLE:	++ipaBaseStat[STAT_TYPE_DRIBBLE];	break;
					case STAT_TRAINING_CATEGOTY_REBOUND:	++ipaBaseStat[STAT_TYPE_REBOUND];	break;
					case STAT_TRAINING_CATEGOTY_BLOCK:		++ipaBaseStat[STAT_TYPE_BLOCK];		break;	
					case STAT_TRAINING_CATEGOTY_STEAL:		++ipaBaseStat[STAT_TYPE_STEAL];		break;
					case STAT_TRAINING_CATEGOTY_2POINT:		++ipaBaseStat[STAT_TYPE_2POINT];	break;
					case STAT_TRAINING_CATEGOTY_3POINT:		++ipaBaseStat[STAT_TYPE_3POINT];	break;
					case STAT_TRAINING_CATEGOTY_DRIVE:		++ipaBaseStat[STAT_TYPE_DRIVE];		break;
					case STAT_TRAINING_CATEGOTY_CLOSE:		++ipaBaseStat[STAT_TYPE_CLOSE];		break;
					default: break;
					}
				}
			}
		}
	}
}

RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM CFSGameUser::CheckAddShoppingBasketItemReq(SS2C_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_REQ rq, SODBCEventUserShoppingBasketItem& Item)
{
	Item._btKind = rq.btKind;
	Item._btSkillKind = rq.btSkillKind;
	Item._iItemCode = rq.iItemCode;
	Item._iPropertyType = 0;
	Item._iPropertyValue = rq.iPropertyValue;
	Item._iPropertyIndex1 = rq.iPropertyIndex1;
	Item._iPropertyIndex2 = rq.iPropertyIndex2;
	Item._iPropertyIndex3 = rq.iPropertyIndex3;
	Item._iPropertyIndex4 = rq.iPropertyIndex4;
	Item._iPropertyIndex5 = rq.iPropertyIndex5;
	Item._iPropertyIndex6 = rq.iPropertyIndex6;
	Item._tPushDate = _time64(NULL);

	RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM btResult = GetUserShoppingFestivalEvent()->CheckAddShoppingBasketItemReq(rq);
	if(RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_SUCCESS != btResult)
	{
		return btResult;
	}

	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_CONDITION_RETURN(NULL == pAvatarInfo, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

	if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_ITEM == Item._btKind)
	{
		SShopItemInfo sItemInfo;
		CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
		CHECK_CONDITION_RETURN(NULL == pItemShop, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(FALSE == pItemShop->GetItem(Item._iItemCode, sItemInfo), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		Item._iPropertyType = sItemInfo.iPropertyType;
		Item._iSex = sItemInfo.iSexCondition;

		CHECK_CONDITION_RETURN(0 == sItemInfo.iUseOption, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(SELL_TYPE_CASH != sItemInfo.iSellType, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_POINT_ITEM);
		CHECK_CONDITION_RETURN(ITEM_BIG_KIND_DRESS != sItemInfo.iBigKind && ITEM_BIG_KIND_ACC != sItemInfo.iBigKind && ITEM_BIG_KIND_ETC != sItemInfo.iBigKind, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(!(SHOP_BUTTON_TYPE_BUY & sItemInfo.iShopButtonType), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(sItemInfo.iSexCondition != pAvatarInfo->iSex && sItemInfo.iSexCondition != ITEM_SEXCONDITION_UNISEX, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_SEX_CONDITON);
		CHECK_CONDITION_RETURN(false == sItemInfo.IsLvCondition(pAvatarInfo->iLv), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_LV_CONDITON);
		CHECK_CONDITION_RETURN(!sItemInfo.IsPositionCondition(GetCurUsedAvatarPosition()), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_POSITION_CONDITON);

		CHECK_CONDITION_RETURN(FACE_OFF_KIND_NUM == sItemInfo.iPropertyKind, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(CHANGE_BODYSHAPE_KIND_NUM == sItemInfo.iPropertyKind, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(RESET_SEASON_RECORD_ITEM_KIND_NUM == sItemInfo.iPropertyKind, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(RESET_TOTAL_RECORD_ITEM_KIND_NUM == sItemInfo.iPropertyKind, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(MIN_ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF <= sItemInfo.iPropertyKind && MAX_ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF >= sItemInfo.iPropertyKind, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
		CHECK_CONDITION_RETURN(ITEM_PROPERTY_KIND_PACKAGE_ITEM_GIVE_INVENTORY == sItemInfo.iPropertyKind, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		CHECK_CONDITION_RETURN(6 == pAvatarInfo->iCharacterType && TRUE == sItemInfo.bIsNotNewCharBuyItem, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);
		CHECK_CONDITION_RETURN(403 <= pAvatarInfo->iSpecialCharacterIndex && 406 >= pAvatarInfo->iSpecialCharacterIndex && TRUE == sItemInfo.bIsNotNewCharBuyItem, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);
		CHECK_CONDITION_RETURN((THREEKINGDOM_CHARACTER_SPECIALINDEX1 == pAvatarInfo->iSpecialCharacterIndex || THREEKINGDOM_CHARACTER_SPECIALINDEX3 == pAvatarInfo->iSpecialCharacterIndex) && (sItemInfo.iChannel & ITEMCHANNEL_FACE_ACC2) > 0, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);
		CHECK_CONDITION_RETURN(sItemInfo.iSpecialIndexCondition != -1 && sItemInfo.iSpecialIndexCondition != pAvatarInfo->iSpecialCharacterIndex, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);
		CHECK_CONDITION_RETURN(sItemInfo.iTeamIndexCondition != -1 && sItemInfo.iTeamIndexCondition != pAvatarInfo->iAvatarTeamIndex, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);
		CHECK_CONDITION_RETURN(sItemInfo.iUnableTeamIndexCondition != -1 && sItemInfo.iUnableTeamIndexCondition == pAvatarInfo->iAvatarTeamIndex, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);
		CHECK_CONDITION_RETURN(CHANGE_HEIGHT_KIND_NUM == sItemInfo.iPropertyKind && 0 == pAvatarInfo->iEnableChangeHeight && -1 != pAvatarInfo->iSpecialCharacterIndex, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);	

		if(ITEM_PROPERTY_KIND_CHANGE_BONUS_STAT == sItemInfo.iPropertyKind)
		{			
			SpecialAvatarStatInfo* pStat = AVATARCREATEMANAGER.GetSpecialAvatarAddStat(pAvatarInfo->iSpecialCharacterIndex, pAvatarInfo->Status.iHeight, pAvatarInfo->Status.iGamePosition);
			CHECK_CONDITION_RETURN(NULL == pStat, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);

			CHECK_CONDITION_RETURN(pStat->iBonusStat == 0, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_CHARACTER_TYPE);
		}

		CFSGameUserItem* pUserItemList = GetUserItemList();
		CHECK_CONDITION_RETURN(NULL == pUserItemList, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
		CHECK_CONDITION_RETURN(NULL == pAvatarItemList, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		if(PROPENSITY_ITEM_KIND_START <= sItemInfo.iPropertyKind && sItemInfo.iPropertyKind <= PROPENSITY_ITEM_KIND_END)
		{
			SUserItemInfo* pCheckItem = pAvatarItemList->GetInventoryItemWithPropertyKind(sItemInfo.iPropertyKind);
			if(pCheckItem != NULL)
			{
				CHECK_CONDITION_RETURN(PROPENSITY_ITEM_KIND_START <= pCheckItem->iPropertyKind && pCheckItem->iPropertyKind <= PROPENSITY_ITEM_KIND_END, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_EXIST_SAME_ITEM);
			}
			CHECK_CONDITION_RETURN(RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_SUCCESS != (btResult = GetUserShoppingFestivalEvent()->CheckAddShoppingBasketItemReq(rq.btKind, rq.btSkillKind, rq.iItemCode)), btResult);
		}
	
		if(SKILL_SLOT_KIND_NUM == sItemInfo.iPropertyKind)
		{
			int nMaxSkillSlot = MAX_SKILL_SLOT_OTHERS;
			if(GetSkillSlotCount(pAvatarInfo->Skill.SkillSlot, sItemInfo.iItemCode0) >= nMaxSkillSlot)
			{
				if(-1 != rq.iPropertyValue ||
					(-1 == rq.iPropertyValue && FALSE == CheckSkillSlotExchangeStatus(pAvatarInfo->Skill.SkillSlot, sItemInfo.iItemCode0)))
				{
					return RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_EXIST_SAME_ITEM;
				}			
			}

			CHECK_CONDITION_RETURN(RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_SUCCESS != (btResult = GetUserShoppingFestivalEvent()->CheckAddShoppingBasketItemReq(rq.btKind, rq.btSkillKind, rq.iItemCode)), btResult);
		}

		if(1 == sItemInfo.iIsUniform || 2 == sItemInfo.iIsUniform)
		{
			Item._iItemCode = sItemInfo.iItemCode0;
		}

		if(CHARACTER_SLOT_TYPE_NORMAL <= sItemInfo.iCharacterSlotType)
		{
			Item._iPropertyValue = -1;
		}

		CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
		CHECK_CONDITION_RETURN(NULL == pLogODBC, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		CFSLoginGlobalODBC* pLoginODBC = (CFSLoginGlobalODBC*)ODBCManager.GetODBC( ODBC_LOGINGLOBAL );
		CHECK_CONDITION_RETURN(NULL == pLoginODBC, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {rq.iPropertyIndex1, rq.iPropertyIndex2, rq.iPropertyIndex3, rq.iPropertyIndex4, rq.iPropertyIndex5, rq.iPropertyIndex6};
		CHECK_CONDITION_RETURN(FALSE == pUserItemList->BuyItemProcess_CheckHacking(pItemShop, pLogODBC, pLoginODBC, sItemInfo, pAvatarInfo->iLv, Item._iPropertyValue, rq.iPropertyAssignType, iaPropertyIndex), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		vector< SItemProperty > vItemProperty;
		pItemShop->GetItemPropertyList(iaPropertyIndex, vItemProperty);

		if(!vItemProperty.empty())
		{
			if(vItemProperty.size() > 0)
			{
				Item._iProperty1 = vItemProperty[0].iProperty;
				Item._iValue1 = vItemProperty[0].iValue;
			}

			if(vItemProperty.size() > 1)
			{
				Item._iProperty2 = vItemProperty[1].iProperty;
				Item._iValue2 = vItemProperty[1].iValue;
			}

			if(vItemProperty.size() > 2)
			{
				Item._iProperty3 = vItemProperty[2].iProperty;
				Item._iValue3 = vItemProperty[2].iValue;
			}

			if(vItemProperty.size() > 3)
			{
				Item._iProperty4 = vItemProperty[3].iProperty;
				Item._iValue4 = vItemProperty[3].iValue;
			}

			if(vItemProperty.size() > 4)
			{
				Item._iProperty5 = vItemProperty[4].iProperty;
				Item._iValue5 = vItemProperty[4].iValue;
			}

			if(vItemProperty.size() > 5)
			{
				Item._iProperty6 = vItemProperty[5].iProperty;
				Item._iValue6 = vItemProperty[5].iValue;
			}
		}

		int iBuyPriceTrophy, iBuyPriceClubCoin, iErrorCode;
		CHECK_CONDITION_RETURN(FALSE == pUserItemList->SetItemPriceBySellType(pItemShop, sItemInfo, GetIsLocalPubliser(), Item._iPropertyValue, Item._iCashPrice, Item._iPointPrice, iBuyPriceTrophy, iBuyPriceClubCoin, iErrorCode), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		int iItemPropertyPriceCash = 0, iItemPropertyPricePoint = 0;
		CHECK_CONDITION_RETURN(FALSE == pUserItemList->SetPropertyPriceAndType(pItemShop, sItemInfo, Item._iPropertyValue, rq.iPropertyAssignType, iaPropertyIndex, pAvatarInfo->iLv, Item._iCashPrice, Item._iPointPrice,
			iItemPropertyPriceCash, iItemPropertyPricePoint, iErrorCode), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);
	}
	else if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_SKILL == Item._btKind)
	{
		CFSGameServer* pServer = CFSGameServer::GetInstance();
		CHECK_CONDITION_RETURN(NULL == pServer, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		CFSSkillShop* pSkillShop = pServer->GetSkillShop();
		CHECK_CONDITION_RETURN(NULL == pSkillShop, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		int iCategory = SKILL_TYPE_SKILL, iKind = SKILL_TYPE_SKILL;
		if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_SKILL_KIND_FREESTYLE == Item._btSkillKind)
		{
			iCategory = SKILL_TYPE_FREESTYLE;
			iKind = SKILL_TYPE_FREESTYLE;
		}

		int iConditionSkillIndex = 0;
		CHECK_CONDITION_RETURN(FALSE == GetUserSkill()->CheckSkillBuyCondition(pSkillShop, iCategory, Item._iItemCode, iKind, iConditionSkillIndex), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_EXIST_SAME_ITEM);

		CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
		CHECK_CONDITION_RETURN(NULL == pGameODBC, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		SSkillInfo SkillInfo;
		CHECK_CONDITION_RETURN(FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, iKind, Item._iItemCode, SkillInfo), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_POSITION_CONDITON);	
		CHECK_CONDITION_RETURN(SkillInfo.GetLvCondition() > pAvatarInfo->iLv, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_LV_CONDITON);	
		CHECK_CONDITION_RETURN(0 >= SkillInfo.iBalanceCash, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_POINT_ITEM);

		int iStorageType = 0, iSkillShiftBit = 0;
		CHECK_CONDITION_RETURN(FALSE == GetUserSkill()->GetDBColumnAndShiftBit(pAvatarInfo, Item._iItemCode, iStorageType, iSkillShiftBit, iKind), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_EXIST_SAME_ITEM);

		Item._iCashPrice = SkillInfo.iBalanceCash;
		Item._iPointPrice = 0;
		Item._iSex = ITEM_SEXCONDITION_UNISEX;
	}
	else if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_ACTION == Item._btKind)
	{
		CFSGameServer* pServer = CFSGameServer::GetInstance();
		CHECK_CONDITION_RETURN(NULL == pServer, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		CActionShop* pActionShop = pServer->GetActionShop();
		CHECK_CONDITION_RETURN(NULL == pActionShop, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		CHECK_CONDITION_RETURN(SELL_TYPE_CASH != pActionShop->GetActionSellType(Item._iItemCode), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_POINT_ITEM);

		int iTrophyPrice = 0;
		CHECK_CONDITION_RETURN(FALSE == pActionShop->CheckBuyAction(Item._iItemCode, Item._iPropertyValue, pAvatarInfo->iSex, pAvatarInfo->iLv, pAvatarInfo->iAvatarTeamIndex, pAvatarInfo->iSpecialCharacterIndex, 
			SELL_TYPE_CASH, Item._iCashPrice, Item._iPointPrice, iTrophyPrice, Item._iSex), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_EXIST_SAME_ITEM);

		CUserAction* pUserAction = GetUserAction();
		CHECK_CONDITION_RETURN(NULL == pUserAction, RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_FAIL);

		CHECK_CONDITION_RETURN(FALSE == pUserAction->CheckOwnUnLimitAction(Item._iItemCode, Item._iPropertyValue), RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_EXIST_SAME_ITEM);
	}

	return RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_SUCCESS;
}

RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET CFSGameUser::BuyShoppingBasket()
{
	SBillingInfo BillingInfo;
	BillingInfo.pUser = this;
	memcpy(BillingInfo.szUserID, GetUserID(), MAX_USERID_LENGTH + 1);	
	memcpy(BillingInfo.szGameID, GetGameID(), MAX_GAMEID_LENGTH + 1);	
	memcpy(BillingInfo.szPublisherUserNo, GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1);
	memcpy(BillingInfo.szIPAddress, GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	BillingInfo.iPropertyType = 0;
	BillingInfo.iSellType = SELL_TYPE_CASH;
	BillingInfo.iTrophyChange = 0;
	BillingInfo.iSerialNum = GetUserIDIndex();
	BillingInfo.iAvatarLv = GetCurUsedAvatar()->iLv;

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_CONDITION_RETURN(NULL == pServer, RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_CONDITION_RETURN(NULL == pGameODBC, RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);

	vector<SODBCEventUserShoppingBasketItem> vItem;
	GetUserShoppingFestivalEvent()->GetShoppingBasket(vItem);
	for(int i = 0; i < vItem.size(); ++i)
	{
		SODBCEventUserShoppingBasketItem UserItem = vItem[i];

		BillingInfo.iParam5 = UserItem._iIndex;
		BillingInfo.iItemCode = UserItem._iItemCode;
		BillingInfo.iCurrentCash = GetCoin();
		BillingInfo.iCashChange = UserItem._iSaleCashPrice;
		BillingInfo.iItemPrice = UserItem._iSaleCashPrice;
		BillingInfo.iPointChange = UserItem._iPointPrice;
		
		if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_ITEM == UserItem._btKind)
		{		
			SShopItemInfo sItemInfo;
			CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
			CHECK_CONDITION_RETURN(NULL == pItemShop, RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);

			if(TRUE == pItemShop->GetItem(UserItem._iItemCode, sItemInfo))
			{
				BillingInfo.iEventCode = EVENT_BUY_EVENT_SHOPPINGBASKET_ITEM;
				BillingInfo.iPropertyType = GetUserItemList()->CheckAndGetPropertyType(sItemInfo.iPropertyType, UserItem._iPropertyValue);

				if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
				{
					return RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL;
				}

				if(BillingInfo.iPointChange > 0)
				{
					GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
					GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_BUY_POINT_ITEM);
				}
			}
		}
		else if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_SKILL == UserItem._btKind)
		{
			SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
			CHECK_CONDITION_RETURN(NULL == pAvatarInfo, RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);

			CFSSkillShop* pSkillShop = pServer->GetSkillShop();
			CHECK_CONDITION_RETURN(NULL == pSkillShop, RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);

			int iKind = SKILL_TYPE_SKILL;
			if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_SKILL_KIND_FREESTYLE == UserItem._btSkillKind)
				iKind = SKILL_TYPE_FREESTYLE;

			SSkillInfo SkillInfo;
			int iStorageType = 0, iSkillShiftBit = 0;
			CHECK_CONDITION_RETURN(FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, iKind, UserItem._iItemCode, SkillInfo), RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);
			CHECK_CONDITION_RETURN(FALSE == GetUserSkill()->GetDBColumnAndShiftBit(pAvatarInfo, UserItem._iItemCode, iStorageType, iSkillShiftBit, iKind), RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);

			if(SKILL_TYPE_SKILL == SkillInfo.iKind)
				BillingInfo.iEventCode = EVENT_BUY_SKILL;
			else if(SKILL_TYPE_FREESTYLE == SkillInfo.iKind)
				BillingInfo.iEventCode = EVENT_BUY_FREESTYLE;

			BillingInfo.iSerialNum = GetUserIDIndex();

			BillingInfo.iParam0 = iStorageType;
			BillingInfo.iParam1 = iSkillShiftBit;
			BillingInfo.iParam2 = SKILL_TYPE_SKILL == SkillInfo.iKind ? SkillInfo.iCondition0 : SkillInfo.iCondition1;
			BillingInfo.iParam3 = SkillInfo.iKind;
			BillingInfo.iParam4 = SkillInfo.bIsFameSkill;
			BillingInfo.iContentsCode = SkillInfo.iContentsCode;
			BillingInfo.iOperationCode = FS_ITEM_OP_BUY_EVENT_SHOPPINGFESTIVAL;

			if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
			{
				return RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL;
			}

			if(BillingInfo.iPointChange > 0)
			{
				GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
			}
		}
		else if(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_ACTION == UserItem._btKind)
		{
			CActionShop* pActionShop = pServer->GetActionShop();
			CHECK_CONDITION_RETURN(NULL == pActionShop, RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL);

			BillingInfo.iEventCode = EVENT_BUY_ACTION;	
			char* szActionName = pActionShop->GetActionName(UserItem._iItemCode);
			memcpy(BillingInfo.szItemName, szActionName, MAX_ACTION_NAME+1);
			BillingInfo.iParam0 = GetGameIDIndex();
			BillingInfo.iParam1 = pActionShop->GetActionCategory(UserItem._iItemCode);
			BillingInfo.iParam2 = pActionShop->GetActionKind(UserItem._iItemCode);
			BillingInfo.iParam3 = UserItem._iPropertyValue;
			BillingInfo.iOperationCode = FS_ITEM_OP_BUY_EVENT_SHOPPINGFESTIVAL;

			if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
			{
				return RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_FAIL;
			}

			if(BillingInfo.iPointChange > 0)
			{
				GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
			}
		}
	}

	GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);

	return RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_SUCCESS;
};

void CFSGameUser::SendEventHalfPriceUserInfo()
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_HALFPRICE));

	int iIsBonusStage = 0, iIsFirst = FALSE;

	if( FALSE == m_sHalfPriceInfo.bIsDBLoad )
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);

		if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_HALFPRICE_GetUserData(GetUserIDIndex(), m_sHalfPriceInfo.iNeedleCount, iIsBonusStage, iIsFirst))
		{
			WRITE_LOG_NEW(LOG_TYPE_HALFPRICE, DB_DATA_LOAD, FAIL, "EVENT_HALFPRICE_GetUserData  UserIDIndex(%d)", GetUserIDIndex());
			return;
		}

		m_sHalfPriceInfo.btIsBonusStage = (BYTE)iIsBonusStage;
		m_sHalfPriceInfo.bIsDBLoad = TRUE;
		m_sHalfPriceInfo.btIsFirst = FALSE; 
	}

	CPacketComposer Packet(S2C_EVENT_HALFPRICE_INFO_RES);
	SS2C_EVENT_HALFPRICE_INFO_RES sInfo;

	m_sHalfPriceInfo.MakeInfoData(sInfo);
	sInfo.btIsFirst = iIsFirst;

	if ( TRUE == HALFPRICE.MakePacketTopViewItemList(Packet, sInfo) )
	{
		Send(&Packet);
	}
}

void CFSGameUser::UseHalfPrice( SC2S_EVENT_HALFPRICE_USE_REQ& req )
{
	CHECK_CONDITION_RETURN_VOID( CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_HALFPRICE));

	BYTE btResult = RESULT_EVENT_HALFPRICE_TYPE_SUCCESS;
	char szRewardIndex[MAX_HALFPRICE_REWARD_LENGTH+1];
	ZeroMemory(szRewardIndex, MAX_HALFPRICE_REWARD_LENGTH+1);

	btResult = RESULT_EVENT_HALFPRICE_TYPE_SUCCESS;

	int iRealUseCount = HALFPRICE.GetRewardCountWithUseIndex(req.btUseIndex);
	BYTE btUseIndex = req.btUseIndex;
	
	if( EVENT_HALFPRICE_BONUSSTAGE_INDEX_NONE < m_sHalfPriceInfo.btIsBonusStage )
	{ // ���ʽ���
		iRealUseCount = 0;

		if( EVENT_HALFPRICE_BONUSSTAGE_INDEX_30USE == m_sHalfPriceInfo.btIsBonusStage )
			btUseIndex = INDEX_EVENT_HALFPRICE_USE_ITEM_FREE;
		else if(EVENT_HALFPRICE_BONUSSTAGE_INDEX_10USE == m_sHalfPriceInfo.btIsBonusStage)
			btUseIndex = INDEX_EVENT_HALFPRICE_USE_ITEM_FREE_1;
	}

	{
		int iUseCount = HALFPRICE.GetRewardCountWithUseIndex_forPresent(btUseIndex);
		int iMailClearance = MAX_PRESENT_LIST - this->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();

		if( iRealUseCount > m_sHalfPriceInfo.iNeedleCount )
			btResult = RESULT_EVENT_HALFPRICE_TYPE_NEEDLE_SHORTAGE;
		else if( iMailClearance < iUseCount )
			btResult = RESULT_EVENT_HALFPRICE_TYPE_MAILBOXISFULL;
		else if( EVENT_HALFPRICE_BONUSSTAGE_INDEX_NONE == m_sHalfPriceInfo.btIsBonusStage && INDEX_EVENT_HALFPRICE_USE_ITEM_FREE == req.btUseIndex )
			btResult = RESULT_EVENT_HALFPRICE_TYPE_ERROR_BONUSSTAGE;
		
	}
	
	CPacketComposer Packet(S2C_EVENT_HALFPRICE_USE_RES);

	if( RESULT_EVENT_HALFPRICE_TYPE_SUCCESS != btResult )
	{
		SS2C_EVENT_HALFPRICE_USE_RES res;
		res.iRewardCount =0;
		res.btResult = btResult;
		Packet.Add((PBYTE)&res, sizeof(SS2C_EVENT_HALFPRICE_USE_RES));
		Send(&Packet);
		return;
	}

	list<SSEventHalfPrice_Reward*> listReward;
	BOOL bSuperLegend = FALSE;
	if( RESULT_EVENT_HALFPRICE_TYPE_SUCCESS == btResult && 
		TRUE == HALFPRICE.UseHalfPrice(btUseIndex, szRewardIndex, bSuperLegend, listReward) )
	{
		int iMailClearance = MAX_PRESENT_LIST - this->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();
		int iRewardSize = listReward.size();
		if( iMailClearance < iRewardSize )
		{
			SS2C_EVENT_HALFPRICE_USE_RES res;
			res.iRewardCount =0;
			res.btResult = RESULT_EVENT_HALFPRICE_TYPE_MAILBOXISFULL;
			Packet.Add((PBYTE)&res, sizeof(SS2C_EVENT_HALFPRICE_USE_RES));
			Send(&Packet);
			return;
		}

		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID( pODBCBase );

		SEventHalfPrice_UseInfo sUseInfo;

		sUseInfo._iUserIDIndex = GetUserIDIndex();
		sUseInfo._iGameIDIndex = GetGameIDIndex();
		sUseInfo._bIsEffect = req.btIsEffect;
		sUseInfo._iPrevNeedle = sUseInfo._iNeedleCount = m_sHalfPriceInfo.iNeedleCount;
		sUseInfo._iPostNeedle = m_sHalfPriceInfo.iNeedleCount - iRealUseCount;
		if( 0 > sUseInfo._iPostNeedle )
			return;

		sUseInfo._iIsBonusStage = FALSE;
		sUseInfo._iResultCount = 0;
		sUseInfo._iUseType = btUseIndex;
		strcpy_s(sUseInfo._szRewardData, _countof(sUseInfo._szRewardData), szRewardIndex);

		list<int> listPresentIndex;
		if( ODBC_RETURN_SUCCESS != pODBCBase->EVENT_HALFPRICE_Use(sUseInfo, listPresentIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_HALFPRICE, DB_DATA_LOAD, FAIL, "EVENT_HALFPRICE_Use UserIDIndex(%d)", GetUserIDIndex());
			return;
		}

		if ( TRUE == bSuperLegend )
		{
			CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
			if( pCenter )
			{
				CPacketComposer Packet(G2S_EVENT_HALFPRICE_SHOUT_REQ);
				SS2S_DEFAULT Centerinfo;
				strncpy_s(Centerinfo.GameID, _countof(Centerinfo.GameID), GetGameID(), MAX_GAMEID_LENGTH);
				Centerinfo.iGameIDIndex = GetGameIDIndex();
				Packet.Add((PBYTE)&Centerinfo, sizeof(SS2S_DEFAULT));
				pCenter->Send(&Packet);
			}
		}

		m_sHalfPriceInfo.iNeedleCount = sUseInfo._iResultCount;
		m_sHalfPriceInfo.btIsBonusStage = sUseInfo._iIsBonusStage;
		if( INDEX_EVENT_HALFPRICE_USE_ITEM_FREE == btUseIndex )
			btResult = RESULT_EVENT_HALFPRICE_TYPE_SUCCESS_BONUSSTAGE_30;
		else if( INDEX_EVENT_HALFPRICE_USE_ITEM_FREE_1 == btUseIndex )
			btResult = RESULT_EVENT_HALFPRICE_TYPE_SUCCESS_BONUSSTAGE_1;

		RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);

		HALFPRICE.MakePacketRewardList(Packet,btResult, listReward);
		Send(&Packet);

		SendEventHalfPriceUserInfo();
	}
}

BOOL CFSGameUser::BuyHalfPriceItem( SBillingInfo* pBillingInfo, int iPayResult )
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, INVALED_DATA, CHECK_FAIL, "BuyHalfPriceItem - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	BOOL bResult = TRUE;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	SEventHalfPrice_BuyInfo BuyInfo;
	BuyInfo._iUserIDIndex = GetUserIDIndex();
	BuyInfo._iGameIDIndex = GetGameIDIndex();
	BuyInfo._iItemCode = pBillingInfo->iItemCode;
	BuyInfo._iPrevCash = pBillingInfo->iCurrentCash;
	BuyInfo._iPostCash = BuyInfo._iPrevCash - pBillingInfo->iCashChange;
	BuyInfo._iPrice = pBillingInfo->iItemPrice;
	BuyInfo._iResultCount = 0;
	BuyInfo._iBuyCount = pBillingInfo->iParam1;

	SS2C_LEGEND_EVENT_KEY_BUY_RESULT_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_LEGEND_EVENT_KEY_BUY_RESULT_NOT));

	if(ODBC_RETURN_SUCCESS != pODBCBase->EVENT_HALFPRICE_Buy(BuyInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_HALFPRICE, LA_DEFAULT, NONE, "EVENT_HALFPRICE_Buy Fail, User:%d, GameID:%s", GetUserIDIndex(), GetGameID());
		bResult = FALSE;
	}
	else
	{
		m_sHalfPriceInfo.iNeedleCount = BuyInfo._iResultCount;
		
		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// ������ ���ſ�û Result ��Ŷ ����.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(BuyInfo._iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	Send(&PacketComposer);

	if( NEXUS_LOBBY == GetClient()->GetState())
		SendEventHalfPriceUserInfo();

	SetUserBillResultAtMem(pBillingInfo->iSellType, pBillingInfo->iCurrentCash-pBillingInfo->iCashChange, 0, 0, pBillingInfo->iBonusCoin);

	SendUserGold();

	return bResult;
}

void CFSGameUser::SendMovePotenCardInfo( SC2S_MOVE_POTENCARD_INFO_REQ& req )
{
	CPacketComposer Packet(S2C_MOVE_POTENCARD_INFO_RES);

	vector<int> vecEquipItem;
	m_SlotPackageList.GetEquipItemAll(vecEquipItem);

	m_ProductInventory.MakeCoachCardShopInventoryListResData(CFSGameServer::GetInstance()->GetItemShop(), Packet, req.iSelectPotenPage, req.btInventoryRange, req.iRequestTendencyType, 
		req.iSortPropertyType, req.iSortOrder, req.iReQuestType , GetClient()->GetState(), GetCurUsedAvtarLv(), &vecEquipItem);

	Send(&Packet);
}

void CFSGameUser::SendMovePotenCardCharacterInfo( int iSelectPage )
{
	SS2C_MOVE_POTENCARD_CHARACTER_INFO_RES	res;
	CPacketComposer Packet(S2C_MOVE_POTENCARD_CHARACTER_INFO_RES);
	const int cMaxCharacterCount = 12;

	// avatarlist�� �ҷ��´�. ������ ���� ���� �ɰ� ����.
	if(m_listMyAvatar.empty())
	{
		LoadMyAvatarList();
	}

	size_t iAvatarSize = m_listMyAvatar.size();
	res.iMaxCharacterPage = (iAvatarSize ? iAvatarSize-1 : iAvatarSize) / cMaxCharacterCount;
	res.iCurrentPage = iSelectPage;
	res.iCharacterCount = 0;

	int iContinuesize = iSelectPage * cMaxCharacterCount;
	int iIndex = 0 ;

	Packet.Add((PBYTE)&res, sizeof(SS2C_MOVE_POTENCARD_CHARACTER_INFO_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);


	int iCharacterCount = 0;
	for( auto iter = m_listMyAvatar.begin(); 
		iter != m_listMyAvatar.end(); ++iter , ++iIndex )
	{
		if( iIndex < iContinuesize )
			continue;

		if(0 == strcmp((*iter)->szGameID, GetGameID()))
			continue;

		SMOVE_POTEANCARD_CHARACTER_INFO info;

		info.iGameIDIndex = (*iter)->iGameIDIndex;
		info.iGamePosition = (*iter)->iGamePosition;
		info.btLevel = (*iter)->iLv;
		strncpy_s(info.szGameID, _countof(info.szGameID), (*iter)->szGameID, MAX_GAMEID_LENGTH);

		Packet.Add((PBYTE)&info, sizeof(SMOVE_POTEANCARD_CHARACTER_INFO));
		++iCharacterCount;

		if( cMaxCharacterCount <= iCharacterCount )
			break;
	}

	memcpy(pCountLocation, &iCharacterCount, sizeof(int));

	Send(&Packet);
}

void CFSGameUser::UpdateMovePotenCard( SC2S_MOVE_POTENCARD_SELECT_REQ req)
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	CFSGameUserItem* pUserItemList = GetUserItemList();
	CHECK_CONDITION_RETURN_VOID(NULL == pUserItemList);

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_CONDITION_RETURN_VOID(NULL == pAvatarItemList);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(req.iItemIndex);
	CHECK_CONDITION_RETURN_VOID(NULL == pItem);
	CHECK_CONDITION_RETURN_VOID(pItem->iPropertyKind != ITEM_PROPERTY_KIND_MOVE_POTENCARD);

	SS2C_MOVE_POTENCARD_SELECT_RES res;
	res.btResult = RESULT_MOVE_POTENCARD_FAIL;

	MY_AVATAR_MAP::iterator iter = m_mapMyAvatar.find(req.iGameIDIndex);
	CHECK_CONDITION_RETURN_VOID(iter == m_mapMyAvatar.end());

	CProduct* pProduct = m_ProductInventory.GetProduct(req.iProductIndex);
	CHECK_NULL_POINTER_VOID(pProduct);

	if(0 == strcmp(iter->second.szGameID, GetGameID()))
		return;

	int iTendencyType = pProduct->GetTendencyType();

	if( ODBC_RETURN_SUCCESS != pODBCBase->POTEN_MoveItem(GetGameIDIndex(), req.iGameIDIndex, req.iProductIndex, req.iItemIndex ))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, CALL_SP, FAIL, "POTEN_MoveItem GameIDIndex:%d,  TargetGameID:%d,  ProductIndex:%d" ,
			GetGameIDIndex(), req.iGameIDIndex, req.iProductIndex);
		return;
	}
	
	res.btResult = RESULT_MOVE_POTENCARD_SUCCESS;

	RemoveProductToInventory(iTendencyType, req.iProductIndex);

	GetAvatarItemList()->RemoveItem(req.iItemIndex);	

	CPacketComposer Packet(S2C_MOVE_POTENCARD_SELECT_RES);
	Packet.Add((PBYTE)&res, sizeof(SS2C_MOVE_POTENCARD_SELECT_RES));
	Send(&Packet);
}

void CFSGameUser::CheckAndGiveGameOfDiceEventRankReward()
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	vector<SODBCEventGameOfDiceRankWaitReward> vReward;
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_GetRankWaitGiveRewardList(GetUserIDIndex(), -1, vReward));

	for(int i = 0; i < vReward.size(); ++i)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, vReward[i]._iRewardIndex);
		if (NULL != pReward)
		{
			pODBC->EVENT_GAMEOFDICE_DeleteRankWaitGiveRewardList(GetUserIDIndex(), -1, vReward[i]._iRewardIndex);
			pODBC->EVENT_GAMEOFDICE_RANK_InsertRewardLog(vReward[i]._btType, vReward[i]._iRank, GetUserIDIndex(), -1, GetGameID(), vReward[i]._iRewardIndex);
		}
	}

	vReward.clear();
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_GAMEOFDICE_GetRankWaitGiveRewardList(GetUserIDIndex(), GetGameIDIndex(), vReward));

	for(int i = 0; i < vReward.size(); ++i)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, vReward[i]._iRewardIndex);
		if (NULL != pReward)
		{
			pODBC->EVENT_GAMEOFDICE_DeleteRankWaitGiveRewardList(GetUserIDIndex(), GetGameIDIndex(), vReward[i]._iRewardIndex);
			pODBC->EVENT_GAMEOFDICE_RANK_InsertRewardLog(vReward[i]._btType, vReward[i]._iRank, GetUserIDIndex(), GetGameIDIndex(), GetGameID(), vReward[i]._iRewardIndex);
		}
	}
}

void CFSGameUser::GiveReward_BasketBall(BYTE btBallPartType, int iBallPartIndex, int iPropertyValue)
{
	GetUserBasketBall()->GiveReward_BasketBall(btBallPartType, iBallPartIndex, iPropertyValue);
}

BOOL CFSGameUser::CheckDeveloperEventUser()
{
	return GetUserPresentFromDeveloperEvent()->CheckEventUser();
}

BOOL CFSGameUser::CheckDiscountItemEventUser()
{
	return GetUserDiscountItemEvent()->CheckEventUser();
}

BOOL CFSGameUser::CheckAndGiveEventRandomItemDraw_Coin()
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMDRAWEVENT.IsOpen(), FALSE);

	int iUpdatedCoin = 0;
	if(FALSE == GetUserRandomItemDrawEvent()->CheckRandomDrawItemGamePlay(iUpdatedCoin)/* &&
		FALSE == GetUserRandomItemDrawEvent()->CheckRandomDrawItemConnect(iUpdatedCoin)*/)	// ���� ���ӽ� �����ִ� �κ� ����
	{
		return FALSE;
	}

	CUserHighFrequencyItem* pHighFrequencyItem = GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_RANDOMITEM_DRAW_COIN;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdatedCoin;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	CPacketComposer Packet(S2C_EVENT_RANDOMITEM_DRAW_GET_COIN_NOT);
	Send(&Packet);

	return TRUE;
}

void CFSGameUser::EventRandomItemDrawReq(int iProductGroupIndex)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iCoin = 0;
	SUserHighFrequencyItem* pItem = GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_DRAW_COIN);
	if(NULL != pItem)
		iCoin = pItem->iPropertyTypeValue;

	SS2C_EVENT_RANDOMITEM_DRAW_RES rs;
	rs.iProductGroupIndex = iProductGroupIndex;

	int iUseCoin = 0, iRewardIndex = 0, iMaxRewardCnt = 0;
	rs.btResult = RANDOMITEMDRAWEVENT.DrawReq(iProductGroupIndex, iCoin, rs, iUseCoin, iRewardIndex, iMaxRewardCnt);
	if(rs.btResult != RESULT_EVENT_RANDOMITEM_DRAW_SUCCESS)
	{
		Send(S2C_EVENT_RANDOMITEM_DRAW_RES, &rs, sizeof(SS2C_EVENT_RANDOMITEM_DRAW_RES));
		return;
	}

	if(GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + iMaxRewardCnt > MAX_PRESENT_LIST)
	{
		rs.btResult = RESULT_EVENT_RANDOMITEM_DRAW_FULL_MAILBOX;
		Send(S2C_EVENT_RANDOMITEM_DRAW_RES, &rs, sizeof(SS2C_EVENT_RANDOMITEM_DRAW_RES));
		return;
	}

	int iUpdatedCoin = 0;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_DRAW_UseCoin(GetUserIDIndex(), GetGameIDIndex(), iProductGroupIndex, iRewardIndex, iUseCoin, iUpdatedCoin))
	{
		rs.btResult = RESULT_EVENT_RANDOMITEM_DRAW_FAIL;
		Send(S2C_EVENT_RANDOMITEM_DRAW_RES, &rs, sizeof(SS2C_EVENT_RANDOMITEM_DRAW_RES));
		return;
	}

	rs.iUpdatedCoin = iUpdatedCoin;

	CUserHighFrequencyItem* pHighFrequencyItem = GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_RANDOMITEM_DRAW_COIN;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdatedCoin;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, iRewardIndex);
	if (NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMDRAW_EVENT, SET_DATA, FAIL, "DrawReq error - UserIDIndex:%d, GameIDIndex:%d, RewardIndex:%d", 
			GetUserIDIndex(), GetGameIDIndex(), iRewardIndex);	
	}

	Send(S2C_EVENT_RANDOMITEM_DRAW_RES, &rs, sizeof(SS2C_EVENT_RANDOMITEM_DRAW_RES));
}

BOOL CFSGameUser::CheckTransformAbility(int iTransformAbilityType, int iTransformAbilityValue)
{
	SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
	CHECK_CONDITION_RETURN(NULL == pAvatarInfo, FALSE);

	CHECK_CONDITION_RETURN(pAvatarInfo->iTransformFace <= 0, TRUE);

	CHECK_CONDITION_RETURN(pAvatarInfo->Status.iTransformAbilityType != iTransformAbilityType, FALSE);
	CHECK_CONDITION_RETURN(pAvatarInfo->Status.iTransformAbilityValue != iTransformAbilityValue, FALSE);

	return TRUE;
}

BOOL	CFSGameUser::IsActiveLvIntervalBuffItem(int iPropertyKind, int& iUserCheckLv)
{
	CFSGameUserItem* pUserItemList = (CFSGameUserItem*)GetUserItemList();
	CHECK_NULL_POINTER_BOOL(pUserItemList);

	CAvatarItemList* pAvatarItemList = (CAvatarItemList*)pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	SUserItemInfo * pLvIntervalBuffItem = pAvatarItemList->GetLvIntervalBuffItem(iPropertyKind);
	CHECK_NULL_POINTER_BOOL(pLvIntervalBuffItem);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo shopItem;	
	CHECK_CONDITION_RETURN(FALSE == pItemShop->GetItem(pLvIntervalBuffItem->iItemCode, shopItem), FALSE);

	if(shopItem.IsLvCondition(GetCurUsedAvtarLv()))
	{
		iUserCheckLv = shopItem.iMaxLvCondition + 1;	// ���� ���Ű��� ���Ƿ��� +1 �� ���������� �������.
		return TRUE;
	}

	return FALSE;
}

BOOL CFSGameUser::LoadMyAvatarList()
{
	vector<SMyAvatar>	vAvatar;
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_RANK_GetMyAvatarList(GetUserIDIndex(), vAvatar))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INITIALIZE_DATA, FAIL, "EVENT_RANK_GetMyAvatarList error, UserIDIndex:%d", GetUserIDIndex());
		return FALSE;
	}

	m_listMyAvatar.clear();
	m_mapMyAvatarName.clear();
	m_mapMyAvatar.clear();

	for(int i = 0; i < vAvatar.size(); ++i)
	{
		m_mapMyAvatar[vAvatar[i].iGameIDIndex] = vAvatar[i];
		m_mapMyAvatarName[vAvatar[i].szGameID] = &m_mapMyAvatar[vAvatar[i].iGameIDIndex];
		m_listMyAvatar.push_back(&m_mapMyAvatar[vAvatar[i].iGameIDIndex]);

		if(vAvatar[i].iLv > m_iMaxAvatarLevel)
			m_iMaxAvatarLevel = vAvatar[i].iLv;
	}
	m_listMyAvatar.sort(SMyAvatar::SortByLv);

	return TRUE;
}

void CFSGameUser::SendMyAvatarList()
{
	if(m_listMyAvatar.empty() && FALSE == LoadMyAvatarList())
	{
		return;
	}

	SS2C_MY_AVATAR_LIST_INFO_RES	ss;
	ss.iAvatarCnt = 0;

	CPacketComposer Packet(S2C_MY_AVATAR_LIST_INFO_RES);

	ss.bIsLast = TRUE;
	ss.iAvatarCnt = 0;
	Packet.Add((PBYTE)&ss, sizeof(SS2C_MY_AVATAR_LIST_INFO_RES));
	PBYTE pData = Packet.GetTail() - sizeof(SS2C_MY_AVATAR_LIST_INFO_RES);
	BOOL bInitialize = TRUE;

	SMY_AVATAR_ALL_INFO sInfo;
	MY_AVATAR_LIST::const_iterator iter = m_listMyAvatar.cbegin();
	MY_AVATAR_LIST::const_iterator iterEnd = m_listMyAvatar.cend();

	for(; iter != iterEnd; ++iter, ++ss.iAvatarCnt)
	{
		if(ss.iAvatarCnt >= MAX_SEND_AVATAR_LIST_SIZE)
		{
			ss.bIsLast = FALSE;
			memcpy(pData, &ss, sizeof(SS2C_MY_AVATAR_LIST_INFO_RES));
			Send(&Packet);

			Packet.Initialize(S2C_MY_AVATAR_LIST_INFO_RES);

			bInitialize = FALSE;
		}

		if(FALSE == bInitialize)
		{
			ss.bIsLast = TRUE;
			ss.iAvatarCnt = 0;
			Packet.Add((PBYTE)&ss, sizeof(SS2C_MY_AVATAR_LIST_INFO_RES));
			pData = Packet.GetTail() - sizeof(SS2C_MY_AVATAR_LIST_INFO_RES);

			bInitialize = TRUE;
		}

		ZeroMemory(&sInfo, sizeof(SMY_AVATAR_ALL_INFO));
		sInfo.iGameIDIndex = (*iter)->iGameIDIndex;
		strncpy_s(sInfo.szGameID, _countof(sInfo.szGameID), (*iter)->szGameID, MAX_GAMEID_LENGTH);
		sInfo.iGamePosition = (*iter)->iGamePosition;
		sInfo.btSex = (*iter)->btSex;
		sInfo.iLv = (*iter)->iLv;
		sInfo.iMinExp = (*iter)->iMinExp;
		sInfo.iMaxExp = (*iter)->iMaxExp;
		sInfo.iExp = (*iter)->iExp;
		sInfo.IsSecret = (*iter)->bIsSecret;

		Packet.Add((PBYTE)&sInfo, sizeof(SMY_AVATAR_ALL_INFO));
	}

	ss.bIsLast = TRUE;
	memcpy(pData, &ss, sizeof(SS2C_MY_AVATAR_LIST_INFO_RES));
	Send(&Packet);
}

BOOL CFSGameUser::GetMyAvatar(int iGameIDIndex, SMyAvatar& sAvatar)
{
	MY_AVATAR_MAP::iterator iter = m_mapMyAvatar.find(iGameIDIndex);
	if(iter == m_mapMyAvatar.end())
	{
		return FALSE;
	}

	sAvatar = iter->second;

	return TRUE;
}

BOOL CFSGameUser::GetMyAvatar(char* szGameID, SMyAvatar& sAvatar)
{
	if(nullptr == szGameID)
	{
		return FALSE;
	}

	MY_AVATAR_NAME_MAP::const_iterator Iter = m_mapMyAvatarName.find(szGameID);
	if(Iter == m_mapMyAvatarName.end())
	{
		return FALSE;
	}

	sAvatar = *(Iter->second);

	return TRUE;
}

void CFSGameUser::UpdateAvatarExp(int iGameIDIndex, int iLv, int iExp)
{
	MY_AVATAR_MAP::iterator iter = m_mapMyAvatar.find(iGameIDIndex);

	CHECK_CONDITION_RETURN_VOID(iter == m_mapMyAvatar.end());

	if(iter->second.iLv != iLv)
	{
		iter->second.iMinExp = CFSGameServer::GetInstance()->GetLvUpExp(iLv);
		iter->second.iMaxExp = CFSGameServer::GetInstance()->GetLvUpExp(iLv+1);
	}
	iter->second.iLv = iLv;
	iter->second.iExp = iExp;

	m_listMyAvatar.sort(SMyAvatar::SortByLv);
}

void CFSGameUser::SendEventRankMyAvatarList()
{
	if(m_listMyAvatar.empty())
	{
		LoadMyAvatarList();
	}

	SS2C_EVENT_RANK_MY_AVATAR_LIST_RES	ss;
	ss.btListIndex = 0;
	ss.bLastList = FALSE;
	if(m_listMyAvatar.size() <= MAX_EVENT_RANK_LIST_SIZE)
		ss.bLastList = TRUE;

	ss.iTotalAvatarCnt = m_listMyAvatar.size();
	ss.iAvatarCnt = 0;

	CPacketComposer Packet(S2C_EVENT_RANK_MY_AVATAR_LIST_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_EVENT_RANK_MY_AVATAR_LIST_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	int iCount = 0;
	MY_AVATAR_LIST::const_iterator iter = m_listMyAvatar.begin();
	MY_AVATAR_LIST::const_iterator iterEnd = m_listMyAvatar.end();
	for( ; iter != iterEnd; ++iter)
	{
		(*iter)->SetEventRankPacketStruct(Packet);

		++ss.iAvatarCnt;

		if(++iCount >= MAX_EVENT_RANK_LIST_SIZE)
			break;
	}

	memcpy(pCountLocation, &ss.iAvatarCnt, sizeof(int));
	Send(&Packet);

	CHECK_CONDITION_RETURN_VOID(iCount == m_listMyAvatar.size());

	Packet.Initialize(S2C_EVENT_RANK_MY_AVATAR_LIST_RES);
	ss.btListIndex = 1;
	ss.bLastList = TRUE;
	ss.iTotalAvatarCnt = m_listMyAvatar.size();
	ss.iAvatarCnt = 0;

	Packet.Add((PBYTE)&ss, sizeof(SS2C_EVENT_RANK_MY_AVATAR_LIST_RES));
	pCountLocation = Packet.GetTail() - sizeof(int);

	iter++;
	iCount = 0;
	for( ; iter != iterEnd; ++iter)
	{
		(*iter)->SetEventRankPacketStruct(Packet);

		++ss.iAvatarCnt;

		if(++iCount >= MAX_EVENT_RANK_LIST_SIZE)
			break;
	}

	memcpy(pCountLocation, &ss.iAvatarCnt, sizeof(int));
	Send(&Packet);
}

void CFSGameUser::UpdateEventRankMyAvatarName(char* szNewGameID)
{
	MY_AVATAR_MAP::iterator iter = m_mapMyAvatar.find(GetGameIDIndex());
	CHECK_CONDITION_RETURN_VOID(iter == m_mapMyAvatar.end());

	strncpy_s(iter->second.szGameID, _countof(iter->second.szGameID), szNewGameID, MAX_GAMEID_LENGTH+1);
}

void CFSGameUser::UpdateEventRankMyAvatarNameExchange(int iGameIDIndex_L, int iGameIDIndex_R)
{
	MY_AVATAR_MAP::iterator iter_L = m_mapMyAvatar.find(iGameIDIndex_L);
	MY_AVATAR_MAP::iterator iter_R = m_mapMyAvatar.find(iGameIDIndex_R);

	CHECK_CONDITION_RETURN_VOID(iter_L == m_mapMyAvatar.end() || iter_R == m_mapMyAvatar.end());

	char	szTempGameID[MAX_GAMEID_LENGTH+1] = {0,};
	strncpy_s(szTempGameID, _countof(szTempGameID), iter_L->second.szGameID, MAX_GAMEID_LENGTH+1);
	strncpy_s(iter_L->second.szGameID, _countof(iter_L->second.szGameID), iter_R->second.szGameID, MAX_GAMEID_LENGTH+1);
	strncpy_s(iter_R->second.szGameID, _countof(iter_R->second.szGameID), szTempGameID, MAX_GAMEID_LENGTH+1);
}

bool CFSGameUser::UpdateMyAvatarSecretInfo(const vector<int>& vAvatar IN, bool& bOpt OUT)
{
	auto It = vAvatar.cbegin();
	auto ItEnd = vAvatar.cend();

	bool bRet = false;
	for(; It != ItEnd; ++It)
	{
		auto Iter = m_mapMyAvatar.find(*It);

		if(Iter != m_mapMyAvatar.end())
		{
			Iter->second.bIsSecret = (! Iter->second.bIsSecret);

			if((*It) == GetGameIDIndex())
			{
				bRet = true;
				bOpt = Iter->second.bIsSecret;
			}
		}
	}

	return bRet;
}

void CFSGameUser::SelectEventRankMainGameID(SC2S_EVENT_RANK_MAIN_GAMEID_SELECT_REQ& rs)
{
	SS2C_EVENT_RANK_MAIN_GAMEID_SELECT_RES	ss;
	ss.btResult = RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_WRONG_INFO;

	MY_AVATAR_NAME_MAP::const_iterator Iter = m_mapMyAvatarName.find(rs.szGameID);

	if(Iter != m_mapMyAvatarName.end())
	{
		switch(rs.iEventKind)
		{
		case EVENT_KIND_MINIGAMEZONE:
			{
				SMyAvatar* pAvatar = Iter->second;
				if(NULL != pAvatar)
				{
					ss.btResult = m_UserMiniGameZoneEvent.UpdateMainGameID(pAvatar->iGameIDIndex);

					if( RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_SUCCESS == ss.btResult )
					{
						CODBCSvrProxy* pODBCSvr = dynamic_cast<CODBCSvrProxy*>(GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY));
						if(NULL != pODBCSvr)
						{
							CPacketComposer Packet(S2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT);

							SS2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT not;
							not.iUserIDIndex = GetUserIDIndex();
							not.btServerIndex = _GetServerIndex;
							not.iSeason = MINIGAMEZONE.GetCurrentSeason();
							strncpy_s(not.szGameID, _countof(not.szGameID), pAvatar->szGameID, MAX_GAMEID_LENGTH);	
							Packet.Add((BYTE*)&not, sizeof(SS2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT));

							pODBCSvr->Send(&Packet);
						}
					}
				}
			}			
			break;

		case EVENT_KIND_JUMPING_LEVELUP:
			{
				if(CLOSED == LVUPEVENT.IsOpen())
				{
					ss.btResult = RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_EVENT_CLOSED;
					break;
				}
				else if(m_iJumpingPlayerGameIDIndex > 0)
				{
					ss.btResult = RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_ALREADY_CHOICE;
					break;
				}

				SMyAvatar* pAvatar = Iter->second;
				if(NULL != pAvatar)
				{
					if(GAME_LVUP_MAX == pAvatar->iLv)
					{
						ss.btResult = RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_DONT_USE_MAX_LEVEL;
						break;
					}

					CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
					CHECK_NULL_POINTER_VOID( pODBC );

					if(ODBC_RETURN_SUCCESS != pODBC->EVENT_LV_UP_SetPlayerAvatar(GetUserIDIndex(), pAvatar->iGameIDIndex))
					{
						ss.btResult = RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_WRONG_INFO;
						break;
					}

					ss.btResult = RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_SUCCESS;
					m_iJumpingPlayerGameIDIndex = pAvatar->iGameIDIndex;
				}				
			}
			break;

		default:
			ss.btResult = RESULT_EVENT_RANK_MAIN_GAMEID_SELECT_FAIL_WRONG_INFO;
			break;
		}
	}

	Send(S2C_EVENT_RANK_MAIN_GAMEID_SELECT_RES, &ss, sizeof(SS2C_EVENT_RANK_MAIN_GAMEID_SELECT_RES));
}

BOOL CFSGameUser::GetMyAvatarGameID(int iGameIDIndex, SMyAvatar& sAvatar)
{
	CHECK_CONDITION_RETURN(m_listMyAvatar.empty() && FALSE == LoadMyAvatarList(), FALSE);

	MY_AVATAR_MAP::const_iterator Iter = m_mapMyAvatar.find(iGameIDIndex);
	CHECK_CONDITION_RETURN(Iter == m_mapMyAvatar.end(), FALSE);

	sAvatar = Iter->second;

	return TRUE;
}

BOOL CFSGameUser::GetMyAvatarGameID(int iGameIDIndex, char* szGameID)
{
	CHECK_CONDITION_RETURN(0 >= iGameIDIndex, FALSE);
	CHECK_CONDITION_RETURN(m_listMyAvatar.empty() && FALSE == LoadMyAvatarList(), FALSE);

	MY_AVATAR_MAP::const_iterator Iter = m_mapMyAvatar.find(iGameIDIndex);
	CHECK_CONDITION_RETURN(Iter == m_mapMyAvatar.end(), TRUE);		// ĳ���� ������ ���

	strncpy_s(szGameID, MAX_GAMEID_LENGTH+1, Iter->second.szGameID, MAX_GAMEID_LENGTH);

	return TRUE;
}

int CFSGameUser::GetSkillTokenCount( int iCategory )
{
	CFSSkillShop* pSkillShop = CFSGameServer::GetInstance()->GetSkillShop();
	CHECK_NULL_POINTER_BOOL(pSkillShop);

	CFSGameUserItem* pUserItemList = (CFSGameUserItem*)GetUserItemList();
	CHECK_NULL_POINTER_BOOL(pUserItemList);

	CAvatarItemList* pAvatarItemList = (CAvatarItemList*)pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);	

	int iTargetPropertyKind = pSkillShop->ConvertTokenPropertyKind(iCategory);
	if( 0 > iTargetPropertyKind )
		return 0;

	return pAvatarItemList->GetSkillTokenCount(iTargetPropertyKind);
}

int CFSGameUser::GetSkillTokenConditionLevel( int iCategory )
{
	CFSSkillShop* pSkillShop = CFSGameServer::GetInstance()->GetSkillShop();
	CHECK_NULL_POINTER_BOOL(pSkillShop);

	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	int iTargetPropertyKind = pSkillShop->ConvertTokenPropertyKind(iCategory);
	if( 0 > iTargetPropertyKind )
		return 0;

	CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(iTargetPropertyKind);
	CHECK_NULL_POINTER_BOOL(pItemPropertyBoxList);

	return pItemPropertyBoxList->GetPropertyIndexMaxLv();
}

int	 CFSGameUser::GetMaxAvatarLevel(void)
{
	if(0 == m_iMaxAvatarLevel)
	{
		LoadMyAvatarList();
	}

	return m_iMaxAvatarLevel;
}

void CFSGameUser::CheckMaxAvatarLv( int iLv )
{
	if( iLv > m_iMaxAvatarLevel )
		m_iMaxAvatarLevel = iLv;

	if( m_iMaxAvatarLevel > GAME_LVUP_MAX)
		m_iMaxAvatarLevel = GAME_LVUP_MAX;
}

BOOL CFSGameUser::IsCharacterSlot( int iMailType, int iPresentIndex )
{
	SPresent* present = GetPresentList(iMailType)->Find(iPresentIndex);
	CHECK_NULL_POINTER_BOOL(present);

	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	SShopItemInfo shopItem;		
	CHECK_CONDITION_RETURN(FALSE == pItemShop->GetItem(present->iItemCode, shopItem), FALSE);

	if( ITEM_PROPERTY_KIND_CLONE_CHARACTER_ITEM == shopItem.iPropertyKind )
	{
		return TRUE;
	}
	else if (  CHARACTER_SLOT_TYPE_NORMAL <= shopItem.iCharacterSlotType )
	{
		return TRUE;
	}

	return FALSE;
}

void CFSGameUser::SendHotGirlMissionStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == HOTGIRLMISSION.IsOpen());

	SS2C_EVENT_HOTGIRL_MISSION_OPEN_NOT info;
	info.btOpenStatus = HOTGIRLMISSION.IsOpen();
	info.btMissionStatus = GetUserHotGirlMissionEvent()->GetMissionStatus();
	Send(S2C_EVENT_HOTGIRL_MISSION_OPEN_NOT, &info, sizeof(SS2C_EVENT_HOTGIRL_MISSION_OPEN_NOT));
}

void CFSGameUser::LoadEventGiveMagicBallStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_NEW_CHARACTER_IANUS));

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_VOID(pODBC);

	pODBC->EVENT_GIVE_MAGICBALL_GetUserLog(GetUserIDIndex(), m_tReciveMagicballTime);
}

void CFSGameUser::SendEventGiveMagicBallStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_NEW_CHARACTER_IANUS));

	SS2C_EVENT_GIVE_MAGICBALL_STATUS_RES info;
	info.bRecived = TRUE;
	if(TRUE == ResetEventGiveMagicBallRewardTime())
		info.bRecived = FALSE;

	Send(S2C_EVENT_GIVE_MAGICBALL_STATUS_RES, &info, sizeof(SS2C_EVENT_GIVE_MAGICBALL_STATUS_RES));
}

RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD CFSGameUser::GetEventGiveMagicBallReward()
{
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_NEW_CHARACTER_IANUS), RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD_FAIL_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == ResetEventGiveMagicBallRewardTime(), RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD_FAIL_ALREADY_GET_REWARD);
	CHECK_CONDITION_RETURN(GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD_FAIL_MAILBOX_FULL);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD_FAIL);

	time_t tCurrentTime = _time64(NULL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_GIVE_MAGICBALL_InsertUserLog(GetUserIDIndex(), tCurrentTime), RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD_FAIL);

	int iRewardIndex = 11886;

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(this, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD_FAIL);

	m_tReciveMagicballTime = tCurrentTime;

	return RESULT_EVENT_GIVE_MAGICBALL_GET_REWARD_SUCCESS;
}

BOOL CFSGameUser::ResetEventGiveMagicBallRewardTime()
{
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_NEW_CHARACTER_IANUS), FALSE);

	time_t tCurrentTime = _time64(NULL);

	TIMESTAMP_STRUCT reciveMagicballTime;
	TimetToTimeStruct(m_tReciveMagicballTime, reciveMagicballTime);
	reciveMagicballTime.hour = 7;
	reciveMagicballTime.minute = 0;
	reciveMagicballTime.second = 0;

	BOOL bRecived = TRUE;
	if(-1 < m_tReciveMagicballTime)
	{
		for(time_t tTime = TimeStructToTimet(reciveMagicballTime); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tReciveMagicballTime <= tTime && tTime <= tCurrentTime)
			{
				bRecived = FALSE;
			}
		}
	}

	CHECK_CONDITION_RETURN(-1 < m_tReciveMagicballTime && 
		TRUE == bRecived, FALSE);

	return TRUE;
}

void CFSGameUser::LoadLastPlayTime()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	pODBC->USER_GetLastPlayTime(GetUserIDIndex(), m_tLastPlayTime, m_iHeartTatooDailyPlayCount);
}

void CFSGameUser::LoadLastCheckHeartTatooTime()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	pODBC->USER_GetLastCheckHeartTatooTime(GetGameIDIndex(), m_tLastCheckHeartTatooTime);

	CheckHeartTatto();
}

void CFSGameUser::UpdateLastPlayTime(BYTE btMatchType)
{
	CHECK_CONDITION_RETURN_VOID(MATCH_TYPE_NONE != btMatchType &&
		MATCH_TYPE_RATING != btMatchType && 
		MATCH_TYPE_RANKMATCH != btMatchType &&
		MATCH_TYPE_CLUB_LEAGUE != btMatchType);

	TIMESTAMP_STRUCT lastplayDate;
	TimetToTimeStruct(m_tLastPlayTime, lastplayDate);
	lastplayDate.hour = 7;
	lastplayDate.minute = 0;
	lastplayDate.second = 0;

	time_t tCurrentTime = _time64(NULL);
	int iAddGamePlayDay = 0, iGamePlayProgressDay = 0;
	if(-1 < m_tLastPlayTime)
	{
		for(time_t tTime = TimeStructToTimet(lastplayDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tLastPlayTime <= tTime && tTime <= tCurrentTime)
			{
				iGamePlayProgressDay++;
			}
		}

		if(iGamePlayProgressDay > 0)
			iAddGamePlayDay = 1;
	}
	else 
	{
		iAddGamePlayDay = 1;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iUpdated_GamePlayDay = 0;
	if(ODBC_RETURN_SUCCESS == pODBC->USER_UpdateLastPlayTime(GetUserIDIndex(), tCurrentTime, iAddGamePlayDay, iUpdated_GamePlayDay))
	{
		m_tLastPlayTime = tCurrentTime;
		m_iHeartTatooDailyPlayCount = iUpdated_GamePlayDay;
	}
}

void CFSGameUser::UpdateLastCheckHeartTatooTime()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	time_t tCurrentTime = _time64(NULL);
	if(ODBC_RETURN_SUCCESS == pODBC->USER_UpdateLastCheckHeartTatooTime(GetGameIDIndex(), tCurrentTime))
	{
		m_tLastCheckHeartTatooTime = tCurrentTime;
	}
}

void CFSGameUser::CheckHeartTatto()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	CFSGameUserItem* pUserItemList = (CFSGameUserItem*)GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	time_t tCurrentTime = _time64(NULL);

	TIMESTAMP_STRUCT lastCheckHeartTatooTime;
	TimetToTimeStruct(m_tLastCheckHeartTatooTime, lastCheckHeartTatooTime);
	lastCheckHeartTatooTime.hour = 7;
	lastCheckHeartTatooTime.minute = 0;
	lastCheckHeartTatooTime.second = 0;

	BOOL bChecked = TRUE;
	int iLastCheckHeartTatooProgressDay = 0;
	if(-1 == m_tLastCheckHeartTatooTime)
	{
		bChecked = FALSE;
	}
	else
	{
		for(time_t tTime = TimeStructToTimet(lastCheckHeartTatooTime); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tLastCheckHeartTatooTime <= tTime && tTime <= tCurrentTime)
			{
				iLastCheckHeartTatooProgressDay++;
				bChecked = FALSE;
			}
		}
	}
	CHECK_CONDITION_RETURN_VOID(TRUE == bChecked);

	TIMESTAMP_STRUCT lastplayDate;
	TimetToTimeStruct(m_tLastPlayTime, lastplayDate);
	lastplayDate.hour = 7;
	lastplayDate.minute = 0;
	lastplayDate.second = 0;

	int iGamePlayProgressDay = 0;
	if(-1 < m_tLastPlayTime)
	{
		for(time_t tTime = TimeStructToTimet(lastplayDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tLastPlayTime <= tTime && tTime <= tCurrentTime)
			{
				iGamePlayProgressDay++;
			}
		}
	}

	BOOL bUpdated = FALSE;

	vector<int> VINT; 
	pAvatarItemList->Lock();

	int iVectorReserve = pAvatarItemList->GetSize();
	if(5 < iVectorReserve)
		VINT.reserve(iVectorReserve);

	CTypeList< SUserItemInfo* >::POSITION pos = pAvatarItemList->GetHeadPosition();
	CTypeList< SUserItemInfo* >::POSITION pEnd = pAvatarItemList->GetEndPosition();
	while( pos != pEnd )
	{
		SUserItemInfo* pItem = (*pos);
		if( pItem != NULL )
		{
			VINT.push_back(pItem->iItemIdx);
		}
		pos = pAvatarItemList->GetNextPosition(pos);
	}
	int nSzie = VINT.size();
	int iChangedSpecialPartsItemCnt = 0;

	for( int n = 0; n < nSzie; n++  )
	{
		SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(VINT[n]);
		if (nullptr != pItem)
		{
			if(MIN_HEART_TATTO_KIND_NUM > pItem->iPropertyKind || MAX_HEART_TATTO_KIND_NUM < pItem->iPropertyKind)
				continue;
			TIMESTAMP_STRUCT buyDate;
			TimetToTimeStruct(pItem->BuyDate, buyDate);
			buyDate.hour = 7;
			buyDate.minute = 0;
			buyDate.second = 0;

			int iBuyItemProgressDay = 0;
			for(time_t tTime = TimeStructToTimet(buyDate); tTime < tCurrentTime; tTime += (24*60*60))
			{
				if(pItem->BuyDate <= tTime && tTime <= tCurrentTime)
				{
					iBuyItemProgressDay++;
				}
			}

			if(0 == iBuyItemProgressDay)
				continue;

			int iDecreaseCount = iGamePlayProgressDay-1;
			if(iDecreaseCount < 0)
				iDecreaseCount = 0;

			// ���� ���� �� 1�� �̻� ����, ��⸦ ���� �̻� �Ϸ���
			if(1 <= iBuyItemProgressDay &&
				-1 < m_tLastPlayTime)
			{
				// ������ ��� �� ���� �ϼ� < ������ ��Ʈ���� ���� �� ���� �ϼ�
				if(iGamePlayProgressDay < iLastCheckHeartTatooProgressDay)
				{
					// ������ ��Ʈ���� ���� �� ���� �ϼ� > ���� ���� �� ���� �ϼ�
					if(iLastCheckHeartTatooProgressDay > iBuyItemProgressDay)
					{
						// ������Ʈ = ���� ���� �� ���� �ϼ� - ������ ��� �� ���� �ϼ� >= 0 ? ������ ��� �� ���� �ϼ�-1 : ���� ���� �� ���� �ϼ�-1;
						iDecreaseCount = iBuyItemProgressDay - iGamePlayProgressDay >= 0 ? iGamePlayProgressDay-1 : iBuyItemProgressDay-1;
					}

					if(iDecreaseCount <= 0)
						continue;
				}
				// ������ ��� �� ���� �ϼ� > ������ ��Ʈ���� ���� �� ���� �ϼ�
				else if(iGamePlayProgressDay > iLastCheckHeartTatooProgressDay)
				{
					// ������Ʈ = ������ ��Ʈ���� ���� �� ���� �ϼ�
					iDecreaseCount = iLastCheckHeartTatooProgressDay;
				}
			}
			else if(0 == iDecreaseCount)
			{
				if(1 == iBuyItemProgressDay &&
					-1 < m_tLastPlayTime &&
					m_tLastPlayTime < pItem->BuyDate)
				{
					// ��� �� ���� ���� ��Ʈ 1�� ����
					iDecreaseCount = 1;
				}
				else if(1 <= iBuyItemProgressDay &&
					-1 == m_tLastPlayTime)
				{
					iDecreaseCount = iLastCheckHeartTatooProgressDay;

					if(-1 == m_tLastCheckHeartTatooTime)
						iDecreaseCount = iBuyItemProgressDay;
					else if(iLastCheckHeartTatooProgressDay > iBuyItemProgressDay)
						iDecreaseCount = iBuyItemProgressDay;
				}
				else
				{
					continue;
				}
			}

			if(pItem->iPropertyTypeValue - iDecreaseCount > 0)
			{
				if(ODBC_RETURN_SUCCESS == pODBC->spUpdateItemCount(GetGameIDIndex(), pItem->iItemIdx, pItem->iItemCode, iDecreaseCount))
				{
					pItem->iPropertyTypeValue -= iDecreaseCount; 
				}
			}
			else 
			{
				GetUserItemList()->ExpireItem((CFSGameODBC*)pODBC, pItem->iItemIdx, ITEM_STATUS_RESELL);
			}

			bUpdated = TRUE;
		}
	}

	pAvatarItemList->Unlock();

	if(-1 == m_tLastCheckHeartTatooTime ||
		TRUE == bUpdated)
		UpdateLastCheckHeartTatooTime();
}

void CFSGameUser::LoadLoseLeadUserTime()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	m_tLoseLeadUpdateTime = -1;
	if(ODBC_RETURN_SUCCESS != pODBCBase->LOSE_LEAD_GetUserData(GetGameIDIndex(), m_tLoseLeadUpdateTime))
	{
		WRITE_LOG_NEW(LOG_TYPE_AVATAR, DB_DATA_LOAD, FAIL, "LOSE_LEAD_GetUserData - GameIDIndex:%d", GetGameIDIndex());
	}
}

BOOL CFSGameUser::CheckNeedPauseAndAIComePopUp()
{
	if(READY_AICOME_POPUP_STATUS_READY == GetReadyAIComePopup())
	{
		if(RevengeMatching::REVENGE_MATCHING_TYPE_REVENGE_MATCH_START == GetRoomRevengeMatchingType())
		{
			SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_PAUSE);
			return TRUE;
		}
	}
	else if(READY_AICOME_POPUP_STATUS_PAUSE == GetReadyAIComePopup())
	{
		if(RevengeMatching::REVENGE_MATCHING_TYPE_REVENGE_REWARD_INFO == GetRoomRevengeMatchingType())
		{
			SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_SEND);
			return TRUE;
		}
	}

	return FALSE;
}

void CFSGameUser::LoseLeadOrSend(BOOL bPass /*= FALSE*/)
{
	SLOSE_LEAD_CONFIG config;
	CFSGameServer::GetInstance()->GetLoseLoadConfigInfo(config);
	CHECK_CONDITION_RETURN_VOID(FALSE == config.bLoad);
	CHECK_CONDITION_RETURN_VOID(READY_AICOME_POPUP_STATUS_PAUSE == GetReadyAIComePopup());

	if(READY_AICOME_POPUP_STATUS_READY == GetReadyAIComePopup())
	{
		if(FALSE == CheckBeenOverDay(config.iLeadTime, m_tLoseLeadUpdateTime, _GetCurrentDBDate))
		{
			SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_NONE);
			return;
		}

		m_bAIComePass = bPass;

		SetReadyAICome(FALSE);

		if(FALSE == CheckNeedPauseAndAIComePopUp())
		{
			SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_SEND);
		}
	}
	else if(READY_AICOME_POPUP_STATUS_SEND == GetReadyAIComePopup())
	{
		float fLoginRating = GetLoginRating();
		float fPopupRating = fLoginRating - (fLoginRating * static_cast<float>(config.iRatingRate*0.001f));

		if(FALSE == m_bAIComePass && GetRatingPoint() > fPopupRating)
		{
			SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_NONE);
			return;
		}

		if(TRUE == m_bAIComePass)
		{
			SetLoginRating(GetRatingPoint());
		}
		else
		{
			SetLoginRating(fPopupRating);
		}

		SRewardConfigItem* pReward = (SRewardConfigItem*)REWARDMANAGER.GetReward(config.iRewardIndex);
		if(pReward)
		{
			CPacketComposer Packet(S2C_LOWRATING_USER_LOSEGAME_NOT);

			SS2C_LOWRATING_USER_LOSEGAME_NOT not;
			not.btRewardType = pReward->iRewardType;
			not.iItemCode = pReward->iItemCode;
			not.iPropertyType = pReward->iPropertyType;
			not.iValue = pReward->iPropertyValue;
			Packet.Add((PBYTE)&not, sizeof(SS2C_LOWRATING_USER_LOSEGAME_NOT));
			Send(&Packet);
		}

		SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_GAME);
	}
}

void CFSGameUser::SetReadyAICome( BOOL b )
{
	m_bReadyAICome = b;
}

BOOL CFSGameUser::GetReadyAICome()
{
	return m_bReadyAICome;
}

void CFSGameUser::UpdateLoseLeadNotOpenToday()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	if(ODBC_RETURN_SUCCESS != pODBCBase->LOSE_LEAD_UpdateTime(GetGameIDIndex()))
	{
		WRITE_LOG_NEW(LOG_TYPE_AVATAR, DB_DATA_LOAD, FAIL, "LOSE_LEAD_UpdateTime - GameIDIndex:%d", GetGameIDIndex());
		return;
	}

	m_tLoseLeadUpdateTime = _GetCurrentDBDate;
}

void CFSGameUser::SetLoginRating( const float& f )
{
	m_fLoginRating = f;
}

float CFSGameUser::GetLoginRating()
{
	return m_fLoginRating;
}

void CFSGameUser::SetReadyAIComePopup(int istatus)
{
	m_ReadyAIComePopupStatus = istatus;
}

int CFSGameUser::GetReadyAIComePopup()
{
	return m_ReadyAIComePopupStatus;
}

void CFSGameUser::UpdateLoseLeadPveModeReady()
{
	if(READY_AICOME_POPUP_STATUS_SEND <= GetReadyAIComePopup() && FALSE == GetReadyAICome())
	{
		SetReadyAICome(TRUE);
	}
}

void CFSGameUser::LoseLeadGiveReward()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == GetReadyAICome());

	SLOSE_LEAD_CONFIG config;
	CFSGameServer::GetInstance()->GetLoseLoadConfigInfo(config);
	CHECK_CONDITION_RETURN_VOID(FALSE == config.bLoad);

	SRewardConfigItem* pReward = (SRewardConfigItem*)REWARDMANAGER.GiveReward(this, config.iRewardIndex);

	if(nullptr == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_REWARD, DB_DATA_UPDATE, FAIL, "LoseLeadGiveReward GameIDIndex:%d", GetGameIDIndex());
	}
	else
	{
		SS2C_LOSEGAME_GAVEREWARD_NOT not;
		not.sGiveinfo.btRewardType = pReward->iRewardType;
		not.sGiveinfo.iItemCode = pReward->iItemCode;
		not.sGiveinfo.iPropertyType = pReward->iPropertyType;
		not.sGiveinfo.iPropertyValue = pReward->iPropertyValue;

		CPacketComposer Packet(S2C_LOSEGAME_GAVEREWARD_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_LOSEGAME_GAVEREWARD_NOT));
		Send(&Packet);
	}

	SetReadyAICome(FALSE);
	SetReadyAIComePopup(READY_AICOME_POPUP_STATUS_NONE);
}

void CFSGameUser::SendPVEModeOpenStatus()
{
	CPacketComposer Packet(S2C_PVE_OPEN_STATUS);
	PVEMANAGER.MakeOpenStatus(Packet);

	Send(&Packet);
}

void CFSGameUser::SendPromiseUserInfo()
{
	CheckPromiseEventInfo();

	CPacketComposer Packet(S2C_EVENT_PROMISE_INFO_RES);
	SS2C_EVENT_PROMISE_INFO_RES rs;

	PROMISE.GetRewardPromise(rs._Reward);
	m_sPromiseInfo.GetButtonEnable(rs.btButtonEnable);
	m_sPromiseInfo.GetCumulativeCount(rs.btCumulativeCount);
	m_sPromiseInfo.GetSuccessPlayCount(rs.iSuccessPlayCount);
	m_sPromiseInfo.GetContinuityPlayDay(rs.iContinuityPlayDay);

	Packet.Add((PBYTE)&rs,sizeof(SS2C_EVENT_PROMISE_INFO_RES));
	Send(&Packet);
}

BOOL CFSGameUser::LoadPromiseEvent()
{
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PROMISE), TRUE);

	CFSEventODBC* pODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_PROMISE_GetUserInfo(GetUserIDIndex(), m_sPromiseInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_PROMISE, DB_DATA_LOAD, FAIL, "EVENT_PROMISE_GetUserInfo UserIDIndex:d", GetUserIDIndex());
		return FALSE;
	}

	CheckPromiseEventInfo();

	return TRUE;
}

void CFSGameUser::GivePromiseReward( const SC2S_EVENT_PROMISE_RECEIVING_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID
		(
		rq.iPromiseType >= MAX_EVENT_PROMISE_TYPE ||
		rq.iPromiseButton >= MAX_EVENT_PROMISE_BUTTON 
		);
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PROMISE));


	SS2C_EVENT_PROMISE_RECEIVING_RES rs;
	rs.btResult = RESULT_EVENT_PROMISE_RECEIVING_FAIL;

	CPacketComposer Packet(S2C_EVENT_PROMISE_RECEIVING_RES);

	int iGiveMailCount = MAX_PROMISE_REWARD_NUM;
	int iMailClearance = MAX_PRESENT_LIST - GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();

	if(iMailClearance < iGiveMailCount)
		rs.btResult = RESULT_EVENT_PROMISE_RECEIVING_FULL_MAILBOX;
	else if(TRUE == m_sPromiseInfo.CheckAndRemovePromiseReward(rq.iPromiseType, rq.iPromiseButton))
	{
		CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
		CHECK_NULL_POINTER_VOID(pEventODBC);

		SEVENTPROMISE_UpdateUserInfo info;
		info.iUserIDIndex = GetUserIDIndex();
		info.tOnePlayDate	= m_sPromiseInfo.tOnePlayDate;
		info.iOnePlayCount	= m_sPromiseInfo.iOnePlayCount;
		info.iOneContinuity	= m_sPromiseInfo.iOneContinuity;
		info.tTenPlayDate	= m_sPromiseInfo.tTenPlayDate;
		info.iTenPlayCount	= m_sPromiseInfo.iTenPlayCount;
		info.iTenContinuity	= m_sPromiseInfo.iTenContinuity;
		info.iOneGiveReward	= m_sPromiseInfo.iOneGiveReward;
		info.iTenGiveReward	= m_sPromiseInfo.iTenGiveReward;
		info.iSpecialEXPCount = m_sPromiseInfo.iSpecialEXPCount;
		info.iSpecialEXPDate = m_sPromiseInfo.iSpecialEXPDate;

		if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_PROMISE_UpdateUserInfo(info))
		{
			WRITE_LOG_NEW(LOG_TYPE_PROMISE, DB_DATA_LOAD, FAIL, "EVENT_PROMISE_UpdateUserInfo UserIDIndex:d", GetUserIDIndex());
			m_sPromiseInfo.AddPromiseReward(rq.iPromiseType, rq.iPromiseButton);
		}
		else
		{
			vector<int> vecRewardIndex;
			if(TRUE == PROMISE.GetPromiseRewardIndex(rq.iPromiseType, rq.iPromiseButton, vecRewardIndex))
			{
				BOOL bSuccess = TRUE;
				for(size_t i = 0; i < vecRewardIndex.size() ; ++i)
				{
					if( nullptr == REWARDMANAGER.GiveReward(this, vecRewardIndex[i]))
					{
						WRITE_LOG_NEW(LOG_TYPE_PROMISE, DB_DATA_LOAD, FAIL
							, "EVENT_PROMISE_GiveReward UserIDIndex:d, RewardIndex:%d", GetUserIDIndex(),vecRewardIndex[i]);

						bSuccess = FALSE;

						if( i == 0 )
						{
							m_sPromiseInfo.AddPromiseReward(rq.iPromiseType, rq.iPromiseButton);
							info.iOneGiveReward	= m_sPromiseInfo.iOneGiveReward;
							info.iTenGiveReward	= m_sPromiseInfo.iTenGiveReward;
							pEventODBC->EVENT_PROMISE_UpdateUserInfo(info);
							break;
						}
					}
				}

				if(TRUE == bSuccess)
				{
					rs.btResult = RESULT_EVENT_PROMISE_RECEIVING_SUCCESS;


					SEVENTPROMISE_InsertLog log;
					log.iUserIDIndex = GetUserIDIndex();
					log.iGameIDIndex = GetGameIDIndex();

					for(size_t iIndex = 0 ; iIndex < vecRewardIndex.size() ; ++iIndex)
					{
						if(0 == iIndex)	log.iRewardIndex1 = vecRewardIndex[iIndex];
						else if(1 == iIndex) log.iRewardIndex2 = vecRewardIndex[iIndex];
					}

					log.iType = rq.iPromiseButton;
					log.iDayType = rq.iPromiseType;

					pEventODBC->EVENT_PROMISE_InsertLog(log);
				}
			}
		}
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_PROMISE_RECEIVING_RES));
	Send(&Packet);
}

void CFSGameUser::CheckAndUpdatePromisePlayCount()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PROMISE));

	CheckPromiseEventInfo();

	PROMISE.AddPromisePlayCount(m_sPromiseInfo, _GetCurrentDBDate);
	m_sPromiseInfo._bDBSave = TRUE;
}

void CFSGameUser::SavePromiseUserInfo( CFSEventODBC* pODBC /*= nullptr*/ )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_sPromiseInfo._bDBSave);
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PROMISE));

	if(nullptr == pODBC)
	{
		pODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	}
	CHECK_NULL_POINTER_VOID(pODBC);

	SEVENTPROMISE_UpdateUserInfo info;
	info.iUserIDIndex = GetUserIDIndex();
	info.tOnePlayDate	= m_sPromiseInfo.tOnePlayDate;
	info.iOnePlayCount	= m_sPromiseInfo.iOnePlayCount;
	info.iOneContinuity	= m_sPromiseInfo.iOneContinuity;
	info.tTenPlayDate	= m_sPromiseInfo.tTenPlayDate;
	info.iTenPlayCount	= m_sPromiseInfo.iTenPlayCount;
	info.iTenContinuity	= m_sPromiseInfo.iTenContinuity;
	info.iOneGiveReward	= m_sPromiseInfo.iOneGiveReward;
	info.iTenGiveReward	= m_sPromiseInfo.iTenGiveReward;
	info.iSpecialEXPCount = m_sPromiseInfo.iSpecialEXPCount;
	info.iSpecialEXPDate = m_sPromiseInfo.iSpecialEXPDate;

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_PROMISE_UpdateUserInfo(info))
	{
		WRITE_LOG_NEW(LOG_TYPE_PROMISE, DB_DATA_LOAD, FAIL
			, "SAVE_PROMISE_EVENT_USERINFO UserIDIndex:d", GetUserIDIndex());
	}
}

int CFSGameUser::GetPromiseSpecialEXP()
{
	if(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PROMISE))
		return 0;

	if(FALSE == PROMISE.CheckPromiseSpecialExp())
	{
		m_sPromiseInfo.iSpecialEXPCount = 0;
	}
	else if(m_sPromiseInfo.iSpecialEXPDate != GetYYYYMMDD())
	{
		m_sPromiseInfo._bDBSave = TRUE;
		m_sPromiseInfo.iSpecialEXPCount = 0;
		m_sPromiseInfo.iSpecialEXPDate = GetYYYYMMDD();
	}

	return m_sPromiseInfo.iSpecialEXPCount;
}

void CFSGameUser::AddAndUpdatePromiseSpecialEXP(int iExpCount)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PROMISE));

	int iExpCountResult = iExpCount+1;
	int iLimit = PROMISE.MAXPromiseSpecialEXPCount();

	if(iExpCountResult <= iLimit)
	{
		m_sPromiseInfo._bDBSave = TRUE;
		m_sPromiseInfo.iSpecialEXPCount = iExpCountResult;
		m_sPromiseInfo.iSpecialEXPDate = GetYYYYMMDD();
	}
}

void CFSGameUser::CheckPromiseEventInfo()
{
	const int iHour = PROMISE.GetPromiseHour();
	const time_t tCurrentDBDate = _GetCurrentDBDate;

	if(TRUE == TodayCheck(iHour, m_sPromiseInfo.tOnePlayDate, tCurrentDBDate))
	{
		/*do nothing*/
	}
	else
	{ 
		// 1�� ����
		if(TRUE == CheckBeenContinuityDay(iHour, m_sPromiseInfo.tOnePlayDate, tCurrentDBDate))
		{
			if(FALSE == PROMISE.CheckPromisePlayCountSuccess(EVENT_PROMISE_TYPE_ONEDAY, m_sPromiseInfo.iOnePlayCount))
			{
				// ���� �⼮�ߴµ� �޼��� �� �ߴٸ� //���Ӽ� �ʱ�ȭ
				m_sPromiseInfo.iOneContinuity = 0; 
			}
			else if(TRUE == PROMISE.CheckPromiseMaxContinuity(EVENT_PROMISE_TYPE_ONEDAY, m_sPromiseInfo.iOneContinuity))
			{
				m_sPromiseInfo.iOneContinuity = 0;
			}

			m_sPromiseInfo.iOnePlayCount = 0;
		}
		else
		{
			m_sPromiseInfo.iOnePlayCount = 0;
			m_sPromiseInfo.iOneContinuity = 0;
		}

		m_sPromiseInfo.tOnePlayDate = tCurrentDBDate;
	}

	if(TRUE == TodayCheck(iHour, m_sPromiseInfo.tTenPlayDate, tCurrentDBDate))
	{
		/*do nothing*/
	}
	else
	{ 
		// 10�� ����
		if(TRUE == CheckBeenContinuityDay(iHour, m_sPromiseInfo.tTenPlayDate, tCurrentDBDate))
		{
			if(FALSE == PROMISE.CheckPromisePlayCountSuccess(EVENT_PROMISE_TYPE_TENDAY, m_sPromiseInfo.iTenPlayCount))
			{
				// ���� �⼮�ߴµ� �޼��� �� �ߴٸ� //���Ӽ� �ʱ�ȭ
				m_sPromiseInfo.iTenContinuity = 0; 
			}
			else if(TRUE == PROMISE.CheckPromiseMaxContinuity(EVENT_PROMISE_TYPE_TENDAY, m_sPromiseInfo.iTenContinuity))
			{
				m_sPromiseInfo.iTenContinuity = 0;
			}

			m_sPromiseInfo.iTenPlayCount = 0;
		}
		else
		{
			m_sPromiseInfo.iTenPlayCount = 0;
			m_sPromiseInfo.iTenContinuity = 0;
		}

		m_sPromiseInfo.tTenPlayDate = tCurrentDBDate;
	}
}

void CFSGameUser::CheckLoadPCRoomItemList()
{
	if( PCROOM_KIND_PREMIUM == GetPCRoomKind())
	{
		SAvatarInfo* pAvatarInfo = GetCurUsedAvatar();
		if( pAvatarInfo )
		{
			int SkillSlotCount = pAvatarInfo->GetTotalSkillSlotCount( 0 );
			if( SkillSlotCount < MAX_SKILL_SLOT ) 
			{
				pAvatarInfo->Skill.iPCBangBonusSlot = PREMIUM_PCROOM_SKILL_SLOT_BONUS_KOREA;
			}
		}

		LoadPCRoomItemList();
	}
};

void CFSGameUser::ProcessPCRoomEvent()
{
	CFSGameODBC* pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pODBC);

	CheckEvent(PERFORM_TIME_LOGIN_PCROOM_NORMALLOBBY,pODBC);

	CHECK_CONDITION_RETURN_VOID(PCROOM_KIND_PREMIUM != GetPCRoomKind());

	GetUserPuzzle()->GivePuzzleFromRewardType(PUZZLE_REWARD_TYPE_DAILY_LOGIN_PCROOM);
	GetUserMatchingCard()->CheckAndGivePlayCountWithPcRoom();

	if( FALSE == m_UserPCRoomEvent.Load())
	{
		WRITE_LOG_NEW(LOG_TYPE_PCROOM_EVENT, DB_DATA_LOAD, FAIL, "m_UserPCRoomEvent - UserIDIndex:%d, GameIDIndex:%d", m_iUserIDIndex, GetGameIDIndex());
	}
}

void CFSGameUser::CheckAndGiveFactionDailyGameReward()
{
	if(TRUE == GetUserFaction()->_bCheckFristGiveReward)
	{
		FACTION.SendFactionRewardNot((CUser*)this, FACTION_POINT_REWARD_TYPE_DAILY_GAMEPLAY);
		GetUserFaction()->_bCheckFristGiveReward = FALSE;
	}
}

void CFSGameUser::ProcessKyoungLeeCharacter( int iItemIdx )
{
	time_t tEndDate = 0;
	EVENTDATEMANAGER.GetEventViewEndDate(EVENT_KIND_KYOUNGLEE_PRE_PURCHASE, tEndDate); 
	CPacketComposer Packet(S2C_OPEN_T_BOX_RESULT);

	if(_GetCurrentDBDate < tEndDate)
	{
		const int iErroCode_NoUseTime = -7;
		Packet.Add(iErroCode_NoUseTime);
		Packet.Add((int)EVENT_KIND_KYOUNGLEE_PRE_PURCHASE);
		Send(&Packet);
	}
	else
	{
		// ����� �� �ִ�.
		CFSGameUserItem* pUserItem = GetUserItemList();
		CHECK_NULL_POINTER_VOID(pUserItem);

		CAvatarItemList* pItemList = pUserItem->GetCurAvatarItemList();
		CHECK_NULL_POINTER_VOID(pItemList);

		SUserItemInfo* pItem = pItemList->GetItemWithItemIdx(iItemIdx);
		CHECK_NULL_POINTER_VOID(pItem);

		if( pItem->iPropertyKind == ITEM_PROPERTY_KIND_KYOUNGLEE_PRE_PURCHASE && 
			pItem->iStatus != ITEM_STATUS_RESELL)
		{
			int iErrorCode = ITEM_USE_SUCCESS_KYOUNGLEE_ITEM_GET;
			const int ERROR_FULL_MAILBOX = -8;
			static const int MAX_KYOUNGLEE_REWARD_COUNT = 3;
			if(GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+MAX_KYOUNGLEE_REWARD_COUNT > MAX_PRESENT_LIST)
			{
				iErrorCode = ERROR_FULL_MAILBOX;
			}

			if(iErrorCode == ITEM_USE_SUCCESS_KYOUNGLEE_ITEM_GET)
			{
				CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
				CHECK_NULL_POINTER_VOID(pODBCBase);
				
				list<int> listPresentIndex;
				if(ODBC_RETURN_SUCCESS != pODBCBase->ITEM_UseKyoungLeePreItem(GetGameIDIndex(), iItemIdx, listPresentIndex))
				{
					WRITE_LOG_NEW(LOG_TYPE_SYSTEM, CALL_SP, FAIL, "ITEM_UseKyoungLeePreItem GameIDIndex(%d), ItemIdx(%d)", GetGameIDIndex(), iItemIdx);
					return;
				}
				else
				{
					RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);
					pItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_RESELL);
				}
			}

			Packet.Add(iErrorCode);
			Packet.Add((int)EVENT_KIND_KYOUNGLEE_PRE_PURCHASE);
			Send(&Packet);
		}
	}
}

void CFSGameUser::LoadAndSendLobbyButtonType()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	list<SLobbyButtonType> listLobbyButtonType;
	if(ODBC_RETURN_SUCCESS != pODBCBase->LOBBY_GetButtonType(GetUserIDIndex(), listLobbyButtonType))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, CALL_SP, FAIL, "LOBBY_GetButtonType UserIDIndex(%d)", GetUserIDIndex());
		return;
	}

	if(true == listLobbyButtonType.empty())
	{
		for(int i = 0 ; i < eMENUBUTTON_TYPE_MAX ; ++i)
		{
			m_btLobbyButtonIndex[i] = i;
		}

		m_bLobbyButtonSave = true;
	}
	else
	{
		int iIndex = 0;
		for(auto iter = listLobbyButtonType.begin() ; 
			iter != listLobbyButtonType.end() && iIndex < eMENUBUTTON_TYPE_MAX ;
			++iter, ++iIndex)
		{
			SLobbyButtonType const * pType = &(*iter);
			if( 0 <= pType->btButtonType && eMENUBUTTON_TYPE_MAX > pType->btButtonType)
			{
				m_btLobbyButtonIndex[pType->btButtonType] = pType->iOrder;
			}
		}

		int iRowSize = listLobbyButtonType.size();
		if(iRowSize != eMENUBUTTON_TYPE_MAX)
		{
			for( int i= 0 ; i < eMENUBUTTON_TYPE_MAX; ++i)
			{
				if(255 == m_btLobbyButtonIndex[i])
				{
					m_btLobbyButtonIndex[i] = i;	
				}
			}
		}
	}

	SC2S_LOBBY_BUTTON_TYPE_SAVE_NOT not;
	memcpy_s(not.btButtonIndex, _countof(not.btButtonIndex), m_btLobbyButtonIndex, sizeof(BYTE)*eMENUBUTTON_TYPE_MAX);

	CPacketComposer Packet(S2C_LOBBY_BUTTON_TYPE_NOT);
	Packet.Add((PBYTE)&not, sizeof(SC2S_LOBBY_BUTTON_TYPE_SAVE_NOT));

	Send(&Packet);
}

void CFSGameUser::SaveLobbyButtonType(BOOL bDBSave, const SC2S_LOBBY_BUTTON_TYPE_SAVE_NOT * pinfo)
{
	if(FALSE == bDBSave && nullptr != pinfo)
	{
		for(int i = 0; i < eMENUBUTTON_TYPE_MAX; ++i){
			m_btLobbyButtonIndex[i] = pinfo->btButtonIndex[i];
		}

		m_bLobbyButtonSave = TRUE;
	}
	else if(bDBSave && m_bLobbyButtonSave)
	{
		list<int> listButtonType;
		for( int i = 0 ; i < eMENUBUTTON_TYPE_MAX; ++i)
			listButtonType.push_back(static_cast<int>(m_btLobbyButtonIndex[i]));

		char szButtonData[MAX_LOBBY_BUTTON_TYPE_LENGTH+1] =  {NULL,};

		if(TRUE == ConvertRewardIndexStr(szButtonData, &listButtonType, eMENUBUTTON_TYPE_MAX, MAX_LOBBY_BUTTON_TYPE_LENGTH))
		{
			CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID(pODBCBase);

			if(ODBC_RETURN_SUCCESS != pODBCBase->LOBBY_ButtonType_Update(GetUserIDIndex(), szButtonData))
			{
				WRITE_LOG_NEW(LOG_TYPE_SYSTEM, CALL_SP, FAIL, "LOBBY_ButtonType_Update UserIDIndex(%d)", GetUserIDIndex());
				return;
			}
		}
	}
}

void CFSGameUser::GiveReward_IncompletePieceKey( SRewardConfig* pReward )
{
	CHECK_NULL_POINTER_VOID(pReward);

	GetUserSpecialPiece()->GiveIncompletePiece(pReward);	
}

BOOL CFSGameUser::CheckBuyUser_KyoungLeePackageItem()
{
	if(true == m_bBuyKyoungLeePackge)
		return FALSE;

	CFSEventODBC* pODBCEvent = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_NULL_POINTER_BOOL(pODBCEvent);
	static const int RESULT_KYOUNGLEE_BUY = 1;

	int iResult = static_cast<int>(pODBCEvent->EVENT_KYOUNGLEE_PACKAGE_GetLog(GetUserIDIndex()));

	if(iResult == RESULT_KYOUNGLEE_BUY)
	{
		// �����߾��ٸ�
		m_bBuyKyoungLeePackge = true;
		return FALSE;
	}

	return TRUE;
}

void CFSGameUser::SendClubLeagueOpenStatus()
{
	SS2C_CLUB_LEAGUE_OPEN_NOT not;
	not.btOpenStatus = CLUBCONFIGMANAGER.GetClubLeagueOpenState();

	CPacketComposer Packet(S2C_CLUB_LEAGUE_OPEN_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_CLUB_LEAGUE_OPEN_NOT));
	Send(&Packet);
}

BOOL CFSGameUser::LoadClubUserRankRewardLog()
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CHECK_CONDITION_RETURN(0 >= pAvatar->iClubSN, TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<SClubUserRankRewardLog> vLog;
	if(ODBC_RETURN_SUCCESS != pODBC->CLUB_RANK_SEASON_REWARD_GetGiveUserRewardLog(GetGameIDIndex(), vLog))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, CALL_SP, FAIL, "CLUB_RANK_SEASON_REWARD_GetGiveUserRewardLog GameIDIndex(%d)", GetGameIDIndex());
		return FALSE;
	}

	for(int i = 0; i < vLog.size(); ++i)
	{
		m_mapClubUserRankRewardLog[vLog[i].iRewardIndex] = vLog[i];
	}

	return TRUE;
}

void CFSGameUser::GiveClubUserRankReward()
{
	SAvatarInfo* pAvatar = GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CHECK_CONDITION_RETURN_VOID(0 >= pAvatar->iClubSN);

	time_t tCurrentTime = _time64(NULL);
	const int iMaxClubSeasonContriButionPoint =  CLUBCONFIGMANAGER.GetMaxClubSeasonRewardContriButionPoint();

	if(pAvatar->iClubContributionPointRankSeasonIndex > 0 &&
		pAvatar->iClubContributionPointSeasonRank > 0 &&
		pAvatar->iLastSeasonContributionPoint >= iMaxClubSeasonContriButionPoint)
	{
		SYSTEMTIME SystemTime;
		::GetLocalTime(&SystemTime);

		int iSeasonRank = pAvatar->iClubContributionPointSeasonRank;
		if(pAvatar->iClubContributionPointRankSeasonIndex == 20180401)
		{
			if(pAvatar->iClubContributionPointSeasonRank >= 12 &&
				pAvatar->iClubContributionPointSeasonRank <= 16)
				iSeasonRank = 17;

			if(pAvatar->iClubContributionPointSeasonRank >= 51 &&
				pAvatar->iClubContributionPointSeasonRank <= 123)
				iSeasonRank = 17;
		}

		ClubSeasonRankRewardVector vReward;
		CLUBCONFIGMANAGER.GetRewardListForSeasonRank(CLUB_SEASON_RANK_REWARD_TYPE_CLUB_CONTRIBUTION_POINT, iSeasonRank, vReward);

		if(vReward.size() > 0)
		{
			for(int i = 0; i < vReward.size(); ++i)
			{
				CLUBUSER_RANK_REARD_LOG_MAP::iterator iter = m_mapClubUserRankRewardLog.find(vReward[i].iRewardIndex);
				if(iter != m_mapClubUserRankRewardLog.end() &&
					iter->second.iClubSeasonIndex == pAvatar->iClubContributionPointRankSeasonIndex)
					continue;

				SRewardConfigItem* pReward = (SRewardConfigItem*)REWARDMANAGER.GetReward(vReward[i].iRewardIndex);
				if(NULL == pReward)
					continue;

				CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
				if(NULL == pODBC)
					continue;

				if(CLUB_SEASON_REWARD_TYPE_USER_EVENT_CASH == vReward[i].btRewardType)
				{
					BYTE btMailType;
					time_t tUpdateDate;
					int iPresentIndex;
					if(ODBC_RETURN_SUCCESS != pODBC->CLUB_RANK_SEASON_REWARD_GiveUserRewardItem(GetUserIDIndex(), GetGameIDIndex(), pAvatar->iClubContributionPointRankSeasonIndex, vReward[i].iRewardIndex, btMailType, tUpdateDate, iPresentIndex))
						continue;

					if(iter != m_mapClubUserRankRewardLog.end())
					{
						iter->second.iClubSeasonIndex = pAvatar->iClubContributionPointRankSeasonIndex;
						iter->second.tUpdateDate = tUpdateDate;
					}
					else
					{
						SClubUserRankRewardLog Log;
						Log.iRewardIndex = vReward[i].iRewardIndex;
						Log.iClubSeasonIndex = pAvatar->iClubContributionPointRankSeasonIndex;
						Log.tUpdateDate = tUpdateDate;
						m_mapClubUserRankRewardLog[Log.iRewardIndex] = Log;
					}

					RecvPresent(btMailType, iPresentIndex);

					SS2C_CLUB_RANKING_GIVE_REWARD_NOT not;
					not.btRankRewardType = CLUB_SEASON_RANK_REWARD_TYPE_CLUB_CONTRIBUTION_POINT;
					Send(S2C_CLUB_RANKING_GIVE_REWARD_NOT, &not, sizeof(SS2C_CLUB_RANKING_GIVE_REWARD_NOT));
				}
			}
		}
	}

	if(pAvatar->iClubLeagueRankSeasonIndex > 0 &&
		pAvatar->iClubLeagueSeasonRank > 0)
	{
		ClubSeasonRankRewardVector vReward;
		CLUBCONFIGMANAGER.GetRewardListForSeasonRank(CLUB_SEASON_RANK_REWARD_TYPE_CLUB_LEAGUE_POINT, pAvatar->iClubLeagueSeasonRank, vReward);

		if(vReward.size() > 0)
		{
			for(int i = 0; i < vReward.size(); ++i)
			{
				CLUBUSER_RANK_REARD_LOG_MAP::iterator iter = m_mapClubUserRankRewardLog.find(vReward[i].iRewardIndex);
				if(iter != m_mapClubUserRankRewardLog.end() &&
					iter->second.iClubSeasonIndex == pAvatar->iClubLeagueRankSeasonIndex)
					continue;

				SRewardConfigItem* pReward = (SRewardConfigItem*)REWARDMANAGER.GetReward(vReward[i].iRewardIndex);
				if(NULL == pReward)
					continue;

				CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
				if(NULL == pODBC)
					continue;
			}
		}
	}

	if(pAvatar->iUserClubContributionPointRankSeasonIndex > 0 &&
		pAvatar->iUserClubContributionPointRank > 0)
	{
		ClubSeasonRankRewardVector vReward;
		CLUBCONFIGMANAGER.GetRewardListForSeasonRank(CLUB_SEASON_RANK_REWARD_TYPE_USER_CONTRIBUTION_POINT, pAvatar->iUserClubContributionPointRank, vReward);

		if(vReward.size() > 0)
		{
			int iGiveRewardCount = 0;
			for(int i = 0; i < vReward.size(); ++i)
			{
				CLUBUSER_RANK_REARD_LOG_MAP::iterator iter = m_mapClubUserRankRewardLog.find(vReward[i].iRewardIndex);
				if(iter != m_mapClubUserRankRewardLog.end() &&
					iter->second.iClubSeasonIndex == pAvatar->iUserClubContributionPointRankSeasonIndex)
				{
					if(CLUB_SEASON_REWARD_TYPE_DAILY_USER_POINT == vReward[i].btRewardType)
					{
						if(TRUE == TodayCheck(EVENT_RESET_HOUR, iter->second.tUpdateDate, _GetCurrentDBDate))
							continue;
					}
					else
					{
						continue;
					}
				}

				SRewardConfigItem* pReward = (SRewardConfigItem*)REWARDMANAGER.GetReward(vReward[i].iRewardIndex);
				if(NULL == pReward)
					continue;

				CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
				if(NULL == pODBC)
					continue;

				if(CLUB_SEASON_REWARD_TYPE_DAILY_USER_POINT == vReward[i].btRewardType)
				{
					BYTE btMailType;
					time_t tUpdateDate;
					int iPresentIndex;
					if(ODBC_RETURN_SUCCESS != pODBC->CLUB_RANK_SEASON_REWARD_GiveUserRewardItem(GetUserIDIndex(), GetGameIDIndex(), pAvatar->iUserClubContributionPointRankSeasonIndex, vReward[i].iRewardIndex, btMailType, tUpdateDate, iPresentIndex))
						continue;

					if(iter != m_mapClubUserRankRewardLog.end())
					{
						iter->second.iClubSeasonIndex = pAvatar->iUserClubContributionPointRankSeasonIndex;
						iter->second.tUpdateDate = tUpdateDate;
					}
					else
					{
						SClubUserRankRewardLog Log;
						Log.iRewardIndex = vReward[i].iRewardIndex;
						Log.iClubSeasonIndex = pAvatar->iUserClubContributionPointRankSeasonIndex;
						Log.tUpdateDate = tUpdateDate;
						m_mapClubUserRankRewardLog[Log.iRewardIndex] = Log;
					}

					RecvPresent(btMailType, iPresentIndex);

					iGiveRewardCount++;
				}
				else if(CLUB_SEASON_REWARD_TYPE_ACHIEVEMENT == vReward[i].btRewardType)
				{
					int iAchievementIndex = pReward->GetRewardValue();
					BYTE btStorageType = ACHIEVEMENTMANAGER.GetAchievementStorageType(iAchievementIndex);
					if(ACHIEVEMENT_STORAGETYPE_NONE == btStorageType)
						continue;

					SAchievementLog sAchievementLog;
					sAchievementLog.iAchievementIndex = iAchievementIndex;
					sAchievementLog.iAchievementLogGroupIndex = ACHIEVEMENTMANAGER.GetAchievementGroupIndex(iAchievementIndex);

					auto iterAchievement = m_mapAchievementLogGroup.find(sAchievementLog.iAchievementLogGroupIndex);
					if(iterAchievement != m_mapAchievementLogGroup.end())
					{	
						if(iterAchievement->second->FindAchievementLogbyAchievementIndex(iAchievementIndex))
							continue;
					}

					btStorageType;
					time_t tUpdateDate;
					if(ODBC_RETURN_SUCCESS != pODBC->CLUB_RANK_SEASON_REWARD_GiveUserAchievement(GetUserIDIndex(), GetGameIDIndex(), pAvatar->iClubContributionPointRankSeasonIndex, vReward[i].iRewardIndex, btStorageType, tUpdateDate, sAchievementLog))
						continue;

					if(iter != m_mapClubUserRankRewardLog.end())
					{
						iter->second.iClubSeasonIndex = pAvatar->iClubContributionPointRankSeasonIndex;
						iter->second.tUpdateDate = tUpdateDate;
					}
					else
					{
						SClubUserRankRewardLog Log;
						Log.iRewardIndex = vReward[i].iRewardIndex;
						Log.iClubSeasonIndex = pAvatar->iClubContributionPointRankSeasonIndex;
						Log.tUpdateDate = tUpdateDate;
						m_mapClubUserRankRewardLog[Log.iRewardIndex] = Log;
					}

					CAchievementLogGroup* pAchievementLogGroup = FindAchievementLogGroupbyKey(sAchievementLog.iAchievementLogGroupIndex);
					if (pAchievementLogGroup != NULL)
					{
						pAchievementLogGroup->AddAchievementLog(sAchievementLog);		
					}
					else
					{
						pAchievementLogGroup = new CAchievementLogGroup();
						if (NULL != pAchievementLogGroup)
						{
							pAchievementLogGroup->AddAchievementLog(sAchievementLog);
							m_mapAchievementLogGroup.insert(CACHIEVEMENT_LOG_GROUP_MAP::value_type(sAchievementLog.iAchievementLogGroupIndex, pAchievementLogGroup));
						}
					}

					iGiveRewardCount++;
				}
			}

			if(iGiveRewardCount > 0)
			{
				SS2C_CLUB_RANKING_GIVE_REWARD_NOT not;
				not.btRankRewardType = CLUB_SEASON_RANK_REWARD_TYPE_USER_CONTRIBUTION_POINT;
				Send(S2C_CLUB_RANKING_GIVE_REWARD_NOT, &not, sizeof(SS2C_CLUB_RANKING_GIVE_REWARD_NOT));
			}		
		}
	}

	if(pAvatar->iUserClubLeagueRankSeasonIndex > 0 &&
		pAvatar->iUserClubLeagueRank > 0)
	{
		ClubSeasonRankRewardVector vReward;
		CLUBCONFIGMANAGER.GetRewardListForSeasonRank(CLUB_SEASON_RANK_REWARD_TYPE_USER_LEAGUE_POINT, pAvatar->iUserClubLeagueRank, vReward);

		if(vReward.size() > 0)
		{
			for(int i = 0; i < vReward.size(); ++i)
			{
				CLUBUSER_RANK_REARD_LOG_MAP::iterator iter = m_mapClubUserRankRewardLog.find(vReward[i].iRewardIndex);
				if(iter != m_mapClubUserRankRewardLog.end() &&
					iter->second.iClubSeasonIndex == pAvatar->iUserClubLeagueRankSeasonIndex)
					continue;

				SRewardConfigItem* pReward = (SRewardConfigItem*)REWARDMANAGER.GetReward(vReward[i].iRewardIndex);
				if(NULL == pReward)
					continue;

				CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
				if(NULL == pODBC)
					continue;

				if(CLUB_SEASON_REWARD_TYPE_ACHIEVEMENT == vReward[i].btRewardType)
				{
					int iAchievementIndex = pReward->GetRewardValue();
					BYTE btStorageType = ACHIEVEMENTMANAGER.GetAchievementStorageType(iAchievementIndex);
					if(ACHIEVEMENT_STORAGETYPE_NONE == btStorageType)
						continue;

					SAchievementLog sAchievementLog;
					sAchievementLog.iAchievementIndex = iAchievementIndex;
					sAchievementLog.iAchievementLogGroupIndex = ACHIEVEMENTMANAGER.GetAchievementGroupIndex(iAchievementIndex);

					auto iterAchievement = m_mapAchievementLogGroup.find(sAchievementLog.iAchievementLogGroupIndex);
					if(iterAchievement != m_mapAchievementLogGroup.end())
					{	
						if(iterAchievement->second->FindAchievementLogbyAchievementIndex(iAchievementIndex))
							continue;
					}

					btStorageType;
					time_t tUpdateDate;
					if(ODBC_RETURN_SUCCESS != pODBC->CLUB_RANK_SEASON_REWARD_GiveUserAchievement(GetUserIDIndex(), GetGameIDIndex(), pAvatar->iUserClubLeagueRankSeasonIndex, vReward[i].iRewardIndex, btStorageType, tUpdateDate, sAchievementLog))
						continue;

					if(iter != m_mapClubUserRankRewardLog.end())
					{
						iter->second.iClubSeasonIndex = pAvatar->iUserClubLeagueRankSeasonIndex;
						iter->second.tUpdateDate = tUpdateDate;
					}
					else
					{
						SClubUserRankRewardLog Log;
						Log.iRewardIndex = vReward[i].iRewardIndex;
						Log.iClubSeasonIndex = pAvatar->iUserClubLeagueRankSeasonIndex;
						Log.tUpdateDate = tUpdateDate;
						m_mapClubUserRankRewardLog[Log.iRewardIndex] = Log;
					}

					CAchievementLogGroup* pAchievementLogGroup = FindAchievementLogGroupbyKey(sAchievementLog.iAchievementLogGroupIndex);
					if (pAchievementLogGroup != NULL)
					{
						pAchievementLogGroup->AddAchievementLog(sAchievementLog);		
					}
					else
					{
						pAchievementLogGroup = new CAchievementLogGroup();
						if (NULL != pAchievementLogGroup)
						{
							pAchievementLogGroup->AddAchievementLog(sAchievementLog);
							m_mapAchievementLogGroup.insert(CACHIEVEMENT_LOG_GROUP_MAP::value_type(sAchievementLog.iAchievementLogGroupIndex, pAchievementLogGroup));
						}
					}

					SS2C_CLUB_RANKING_GIVE_REWARD_NOT not;
					not.btRankRewardType = CLUB_SEASON_RANK_REWARD_TYPE_USER_LEAGUE_POINT;
					Send(S2C_CLUB_RANKING_GIVE_REWARD_NOT, &not, sizeof(SS2C_CLUB_RANKING_GIVE_REWARD_NOT));
				}
			}
		}
	}
}

int	CFSGameUser::GetUserClubPlayCount()				
{ 
	return GetCurUsedAvatar()->iClubPlayCount; 
}

void CFSGameUser::SetUserClubPlayCount(int iClubPlayCount)
{
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if(NULL != pAvatarInfo)
	{
		pAvatarInfo->iClubPlayCount = iClubPlayCount;
	}
}

time_t	CFSGameUser::GetUserClubPlayCountUpdateDate()				
{ 
	return GetCurUsedAvatar()->tClubPlayCountUpdateDate; 
}

void CFSGameUser::SetUserClubPlayCountUpdateDate(time_t tClubPlayCountUpdateDate)
{
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if(NULL != pAvatarInfo)
	{
		pAvatarInfo->tClubPlayCountUpdateDate = tClubPlayCountUpdateDate;
	}
}

int	CFSGameUser::GetUserClubCoin()				
{ 
	return GetCurUsedAvatar()->iClubCoin; 
}

void CFSGameUser::SetUserClubCoin(int iClubCoin)
{
	SAvatarInfo * pAvatarInfo = GetCurUsedAvatar();
	if(NULL != pAvatarInfo)
	{
		pAvatarInfo->iClubCoin = iClubCoin;
	}
}

void CFSGameUser::SendClubShopUserCashInfo()
{
	SS2C_CLUB_SHOP_USER_CASH_INFO_RES rs;
	strncpy_s(rs.szGameID, _countof(rs.szGameID), GetGameID(), MAX_GAMEID_LENGTH);	
	rs.iGamePosition = GetCurUsedAvatarPosition();
	rs.btLv = GetCurUsedAvtarLv();
	rs.iFameLevel = GetCurUsedAvtarFameLevel();
	rs.iCash = GetCoin();
	rs.iEventCash = GetEventCoin();
	rs.iPoint = GetSkillPoint();
	rs.iClubCoin = GetUserClubCoin();
	Send(S2C_CLUB_SHOP_USER_CASH_INFO_RES, &rs, sizeof(SS2C_CLUB_SHOP_USER_CASH_INFO_RES));
}

BOOL CFSGameUser::BuyClubMark_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, INVALED_DATA, CHECK_FAIL, "BuyClubMark_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	CFSClubBaseODBC* pClubODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_BOOL(pClubODBC);

	int iClubMarkCode = pBillingInfo->iParam0;
	int iPeriod = pBillingInfo->iParam1;
	int iPostCash = pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;
	int iPrice = pBillingInfo->iItemPrice;

	BYTE btResult = RESULT_CLUB_BUY_CLUBMARK_SUCCESS;
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			time_t tExprieDate = 0;
			if(ODBC_RETURN_SUCCESS == (btResult = pClubODBC->CLUB_BuyClubLogo(GetClubSN(), GetUserIDIndex(), GetGameIDIndex(), iClubMarkCode, iPeriod, pBillingInfo->iCurrentCash, pBillingInfo->iCashChange, iPrice, tExprieDate)))
			{
				SetUserBillResultAtMem(pBillingInfo->iSellType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
				SendClubShopUserCashInfo();

				CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
				if(pClubProxy != NULL)
				{ 
					SG2CL_CLUB_BUY_CLUBMARK_REQ rq;
					rq.iClubSN = GetClubSN();
					strncpy_s(rq.szGameID, _countof(rq.szGameID), GetGameID(), MAX_GAMEID_LENGTH);
					rq.info.iClubMarkCode = iClubMarkCode;
					rq.info.iPeriod = iPeriod;
					rq.tExprieDate = tExprieDate;
					rq.bGive = true;
					pClubProxy->SendPacket(G2CL_CLUB_BUY_CLUBMARK_REQ, &rq, sizeof(SG2CL_CLUB_BUY_CLUBMARK_REQ)); 
				}
			}
			else
			{
				btResult = RESULT_CLUB_BUY_CLUBMARK_FAILED;
			}
		}
		break;
	default:
		{
			btResult = RESULT_CLUB_BUY_CLUBMARK_ENOUGH_CASH;
		}
		break;
	}

	return (BUY_SKILL_ERROR_SUCCESS == btResult);	
}

void CFSGameUser::LoadEventNewClubRewardLog()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_NEW_CLUB));

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBC);

	pODBC->CLUB_GetClubUserGiveEventCoinLog(GetGameIDIndex(), m_bClubUserGiveEventCoin);
}

void CFSGameUser::SendEventNewClubInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_NEW_CLUB));

	SS2C_EVENT_NEW_CLUB_INFO_RES rs;
	rs.btRewardButtonType = m_bClubUserGiveEventCoin ? EVENT_NEW_CLUB_BUTTON_TYPE_DISABLE : EVENT_NEW_CLUB_BUTTON_TYPE_ENABLE;

	Send(S2C_EVENT_NEW_CLUB_INFO_RES, &rs, sizeof(SS2C_EVENT_NEW_CLUB_INFO_RES));
}

void CFSGameUser::GetEventNewClubReward()
{
	SS2C_EVENT_NEW_CLUB_INFO_GET_REWARD_RES rs;
	rs.btResult = RESULT_EVENT_NEW_CLUB_INFO_GET_REWARD_FAIL;

	if(OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_NEW_CLUB) &&
		GetClubSN() > 0 &&
		FALSE == m_bClubUserGiveEventCoin)
	{
		CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBC);

		int iGiveClubCoin = 500;
		int iUpdatedClubCoin;
		if(ODBC_RETURN_SUCCESS == pODBC->CLUB_GiveUserEventClubCoin(GetGameIDIndex(), GetClubSN(), iGiveClubCoin, iUpdatedClubCoin))
		{
			SetUserClubCoin(iUpdatedClubCoin);
			m_bClubUserGiveEventCoin = TRUE;
			rs.btResult = RESULT_EVENT_NEW_CLUB_INFO_GET_REWARD_SUCCESS;
		}
	}

	Send(S2C_EVENT_NEW_CLUB_INFO_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_NEW_CLUB_INFO_GET_REWARD_RES));
}

BOOL CFSGameUser::ClubSetUp_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, INVALED_DATA, CHECK_FAIL, "ClubSetUp_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	CFSClubBaseODBC* pClubODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_BOOL(pClubODBC);

	int iClubAreaIndex = pBillingInfo->iParam0;
	char szClubName[MAX_CLUB_NAME_LENGTH+1];
	strncpy_s(szClubName, MAX_CLUB_NAME_LENGTH+1, pBillingInfo->szClubName, MAX_CLUB_NAME_LENGTH);
	int iPostCash = pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;
	int iPrice = pBillingInfo->iItemPrice;

	BYTE btResult = CLUB_SETUP_RESULT_SUCCESS;
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			time_t tExprieDate = 0;
			if(ODBC_RETURN_SUCCESS == pClubODBC->CLUB_ChangeClubInfo(GetClubSN(), GetUserIDIndex(), GetGameIDIndex(), szClubName, iClubAreaIndex, pBillingInfo->iCurrentCash, pBillingInfo->iCashChange, iPrice))
			{
				SetUserBillResultAtMem(pBillingInfo->iSellType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
				
				CPacketComposer Packet(S2C_CLUB_SETUP_RES);
				Packet.Add((BYTE)CLUB_SETUP_RESULT_SUCCESS);
				Packet.Add((PBYTE)&szClubName, MAX_CLUB_NAME_LENGTH+1);
				Packet.Add(iClubAreaIndex);
				Send(&Packet);

				CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
				if(pClubProxy != NULL)
				{ 
					SG2CL_CLUB_SETUP_REQ rq;
					rq.iClubSN = GetClubSN();
					strncpy_s(rq.szClubName, MAX_CLUB_NAME_LENGTH+1, szClubName, MAX_CLUB_NAME_LENGTH);
					rq.iClubAreaIndex = iClubAreaIndex;
					pClubProxy->SendPacket(G2CL_CLUB_SETUP_REQ, &rq, sizeof(SG2CL_CLUB_SETUP_REQ)); 
				}
			}
			else
			{
				btResult = CLUB_SETUP_RESULT_FAIL_SUPPLY_CASH;
			}
		}
		break;
	default:
		{
			btResult = CLUB_SETUP_RESULT_FAIL_SUPPLY_CASH;
		}
		break;
	}

	return (BUY_SKILL_ERROR_SUCCESS == btResult);	
}

BOOL CFSGameUser::ClubChangePR_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, INVALED_DATA, CHECK_FAIL, "ClubChangePR_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	CFSClubBaseODBC* pClubODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_BOOL(pClubODBC);

	int iKind = pBillingInfo->iParam0;
	char szPRTitle[MAX_CLUB_PR_TITLE_LENGTH+1];
	char szContents[MAX_CLUB_PR_CONTENTS_LENGTH+1];
	strncpy_s(szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1, pBillingInfo->szPRTitle, MAX_CLUB_PR_TITLE_LENGTH);
	strncpy_s(szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1, pBillingInfo->szContents, MAX_CLUB_PR_CONTENTS_LENGTH);

	int iPostCash = pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;
	int iPrice = pBillingInfo->iItemPrice;

	BYTE btResult = CLUB_PR_CHANGE_RESULT_SUCCESS;
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			time_t tExprieDate = 0;
			if(ODBC_RETURN_SUCCESS == pClubODBC->CLUB_UpdateClubPR(GetClubSN(), GetUserIDIndex(), GetGameIDIndex(), iKind, szPRTitle, szContents, pBillingInfo->iCurrentCash, pBillingInfo->iCashChange, iPrice))
			{
				SetUserBillResultAtMem(pBillingInfo->iSellType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);

				CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
				if(pClubProxy != NULL)
				{ 
					SG2CL_CLUB_PR_CHANGE_REQ info;
					info.iClubSN = GetClubSN();
					strncpy_s(info.szGameID, _countof(info.szGameID), GetGameID(), MAX_GAMEID_LENGTH);
					info.btKind = iKind;
					strncpy_s(info.szPRTitle, _countof(info.szPRTitle), szPRTitle, MAX_CLUB_PR_TITLE_LENGTH);
					strncpy_s(info.szContents, _countof(info.szContents), szContents, MAX_CLUB_PR_CONTENTS_LENGTH);
					info.iUserIDIndex = GetUserIDIndex();
					info.iGameIDIndex = GetGameIDIndex();
					pClubProxy->SendClubPRChangeReq(info); 
				}
			}
			else
			{
				btResult = CLUB_PR_CHANGE_RESULT_FAIL_SUPPLY_CASH;
			}
		}
		break;
	default:
		{
			btResult = CLUB_PR_CHANGE_RESULT_FAIL_SUPPLY_CASH;
		}
		break;
	}

	return (BUY_SKILL_ERROR_SUCCESS == btResult);	
}

BOOL CFSGameUser::GetCheerLeaderStatus( int* iaStat )
{
	CHECK_NULL_POINTER_BOOL(iaStat);
	return CLUBCONFIGMANAGER.GetCheerLeaderStat(GetCheerLeaderCode(),GetCheerLeaderTypeNum(), iaStat);
}

void CFSGameUser::LoadTopRatingCharacter()
{
	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBC);

	vector<SUserTopRatingCharacter> vCharacter;
	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->MATCHMAKING_GetTopRatingInfo(GetUserIDIndex(), vCharacter));

	for(int i = 0; i < vCharacter.size(); ++i)
	{
		if(vCharacter[i].iGamePosition >= POSITION_CODE_MAX)
			continue;

		int iRatingPositionCode;
		switch(vCharacter[i].iGamePosition)
		{
		case POSITION_CODE_CENTER:			iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_CENTER;			break;
		case POSITION_CODE_POWER_FOWARD:	iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_POWER_FOWARD;	break;
		case POSITION_CODE_SMALL_FOWARD:	iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SMALL_FOWARD;	break;
		case POSITION_CODE_POINT_GUARD:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_POINT_GUARD;	break;
		case POSITION_CODE_SHOOT_GUARD:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SHOOT_GUARD;	break;
		case POSITION_CODE_SWING_MAN:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SWING_MAN;		break;
		default: 
			continue;
		}

		m_UserTopRatingCharacter[iRatingPositionCode] = vCharacter[i];
	}

	m_bTopRatingCharacterLoaded = TRUE;
}

void CFSGameUser::MakeTopRatingCharacterList(SS2C_USER_RATING_TOP_CHARACTER_LIST_RES& rs)
{
	if(FALSE == m_bTopRatingCharacterLoaded)
	{
		LoadTopRatingCharacter();
	}

	for(int i = 0; i < MAX_USER_TOP_RATING_POSITION_CODE; ++i)
	{
		rs.sCharacter[i].iGameIDIndex = m_UserTopRatingCharacter[i].iGameIDIndex;
		strncpy_s(rs.sCharacter[i].szGameID, _countof(rs.sCharacter[i].szGameID), m_UserTopRatingCharacter[i].szGameID, MAX_GAMEID_LENGTH);
		rs.sCharacter[i].iGamePosition = m_UserTopRatingCharacter[i].iGamePosition;
		rs.sCharacter[i].btRatingGrade = m_UserTopRatingCharacter[i].btRatingGrade;
		rs.sCharacter[i].iRatingPoint = m_UserTopRatingCharacter[i].iRatingPoint;

		if(USER_TOP_RATING_GRADE_NONE == rs.sCharacter[i].btRatingGrade)
			rs.sCharacter[i].iRatingPoint = 0;
	}
}

BOOL CFSGameUser::GetUserTopRatingCharacter(int iGamePosition, SUserTopRatingCharacter& sCharacter)
{
	if(FALSE == m_bTopRatingCharacterLoaded)
	{
		LoadTopRatingCharacter();
	}

	int iRatingPositionCode;
	switch(iGamePosition)
	{
	case POSITION_CODE_CENTER:			iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_CENTER;			break;
	case POSITION_CODE_POWER_FOWARD:	iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_POWER_FOWARD;	break;
	case POSITION_CODE_SMALL_FOWARD:	iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SMALL_FOWARD;	break;
	case POSITION_CODE_POINT_GUARD:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_POINT_GUARD;	break;
	case POSITION_CODE_SHOOT_GUARD:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SHOOT_GUARD;	break;
	case POSITION_CODE_SWING_MAN:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SWING_MAN;		break;
	default: 
		return FALSE;
	}

	memcpy(&sCharacter, &m_UserTopRatingCharacter[iRatingPositionCode], sizeof(SUserTopRatingCharacter));

	return TRUE;
}

BYTE CFSGameUser::GetUserTopRatingCharacterRatingGrade(int iGamePosition)
{
	if(FALSE == m_bTopRatingCharacterLoaded)
	{
		LoadTopRatingCharacter();
	}

	int iRatingPositionCode;
	switch(iGamePosition)
	{
	case POSITION_CODE_CENTER:			iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_CENTER;			break;
	case POSITION_CODE_POWER_FOWARD:	iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_POWER_FOWARD;	break;
	case POSITION_CODE_SMALL_FOWARD:	iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SMALL_FOWARD;	break;
	case POSITION_CODE_POINT_GUARD:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_POINT_GUARD;	break;
	case POSITION_CODE_SHOOT_GUARD:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SHOOT_GUARD;	break;
	case POSITION_CODE_SWING_MAN:		iRatingPositionCode = USER_TOP_RATING_POSITION_CODE_SWING_MAN;		break;
	default: 
		return FALSE;
	}

	return m_UserTopRatingCharacter[iRatingPositionCode].btRatingGrade;
}

void CFSGameUser::GiveReward_SummerCandy( SRewardConfig* pReward )
{
	CHECK_NULL_POINTER_VOID(pReward);

	GetUserSummerCandyEvent()->GiveRewardCandy(pReward->GetPropertyValue());
}

void CFSGameUser::GiveReward_PremiumPassXP(SRewardConfig* pReward)
{
	CHECK_NULL_POINTER_VOID(pReward);

	GetUserPremiumPassEvent()->GiveRewardXP(pReward->GetPropertyValue());
}

void CFSGameUser::SendSpeechBubbleInfo()
{
	SPEECHBUBBLE.SendSpeechBubbleInfo((CUser*)this, m_iIsHave_Speechbubble);
}

BOOL CFSGameUser::LoadEventSpeechBubble()
{
	if(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_SPEECH_BUBBLE))
		return TRUE;

	return SPEECHBUBBLE.LoadSpeechBubbleUserData(GetGameIDIndex(),m_iIsHave_Speechbubble);
}

BOOL CFSGameUser::CheckHaveBubbleItemWithManager( const int& iItemCode )
{
	return SPEECHBUBBLE.CheckHaveBubbleItem(m_iIsHave_Speechbubble, iItemCode);
}

void CFSGameUser::UpdateBubbleItem( const int& iItemCode , int iProperty1, int iProperty2 )
{
	int iStatType = SPEECHBUBBLE.UpdateBubbleItem(GetGameIDIndex(), iItemCode, iProperty1,iProperty2);
	if(iStatType >= 0 && iStatType < MAX_STAT_TYPE_COUNT)
	{
		m_iIsHave_Speechbubble[iStatType] = TRUE;
	}
}