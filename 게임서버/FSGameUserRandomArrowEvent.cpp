qinclude "stdafx.h"
qinclude "FSGameUserRandomArrowEvent.h"
qinclude "RandomArrowEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "CenterSvrProxy.h"
qinclude "GameProxyManager.h"

CFSGameUserRandomArrowEvent::CFSGameUserRandomArrowEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_bIsFirstLogin(FALSE)
	, m_iMyArrowCnt(0)
	, m_iMyPotCnt(0)
{
}


CFSGameUserRandomArrowEvent::~CFSGameUserRandomArrowEvent(void)
{
}

BOOL CFSGameUserRandomArrowEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMARROW.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iRet = pODBC->EVENT_RANDOMARROW_GetUserData(m_pUser->GetUserIDIndex(), m_iMyArrowCnt, m_iMyPotCnt);
	if (ODBC_RETURN_SUCCESS != iRet)
	{
		if(iRet == 1)
		{
			m_bIsFirstLogin = TRUE;
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_RANDOMARROW_EVENT, INITIALIZE_DATA, FAIL, "EVENT_RANDOMARROW_GetUserData error");
			return FALSE;
		}
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserRandomArrowEvent::SendEventInfo_Product()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMARROW.IsOpen());
	CPacketComposer Packet(S2C_RANDOMARROW_PRODUCT_LIST_RES);
	RANDOMARROW.MakePacketProductInfo(Packet);
	m_pUser->Send(&Packet);
}

void CFSGameUserRandomArrowEvent::SendEventInfo_Pot()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMARROW.IsOpen());
	CPacketComposer Packet(S2C_RANDOMARROW_POT_LIST_RES);
	RANDOMARROW.MakePacketPotInfo(Packet);
	m_pUser->Send(&Packet);
}

void CFSGameUserRandomArrowEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMARROW.IsOpen());

	SS2C_RANDOMARROW_USER_INFO_RES	ss;
	ss.iMyArrowCnt = m_iMyArrowCnt;
	ss.iMyPotCnt = m_iMyPotCnt;
	ss.bIsFirstLoginReward = m_bIsFirstLogin;
	m_bIsFirstLogin = FALSE;

	CPacketComposer Packet(S2C_RANDOMARROW_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_RANDOMARROW_USER_INFO_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserRandomArrowEvent::SetArrowCount(int iCount)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMARROW.IsOpen());

	m_iMyArrowCnt = iCount;
}

BOOL CFSGameUserRandomArrowEvent::BuyRandomArrow_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMARROW_EVENT, INVALED_DATA, CHECK_FAIL, "BuyRandomArrow_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int	iBuyArrowCount = pBillingInfo->iParam1;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	SS2C_RANDOMARROW_BUY_ARROW_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_RANDOMARROW_BUY_ARROW_NOT));

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_RANDOMARROW_BuyArrow(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, iBuyArrowCount, iPrevCash, iPostCash, pBillingInfo->iItemPrice))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMARROW_EVENT, LA_DEFAULT, NONE, "EVENT_RANDOMARROW_BuyArrow Fail, User:10752790, GameID:(null)", m_pUser->GetUserIDIndex(), m_pUser->GetGameID());
ult = RESULT_LEGEND_BUY_KEY_FAIL;
	}
	else
	{
		m_iMyArrowCnt += iBuyArrowCount;
		ss.btResult = RANDOMARROW_BUY_ARROW_RESULT_SUCCESS;
		ss.iArrowCnt = m_iMyArrowCnt;

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);

	// 화살뽑기아이템 구매 결과 알림 패킷 보냄.
	CPacketComposer	Packet(S2C_RANDOMARROW_BUY_ARROW_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_RANDOMARROW_BUY_ARROW_NOT));

	m_pUser->Send(&Packet);

	int iCostType = pBillingInfo->iSellType;
	m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
	m_pUser->SendUserGold();

	return (ss.btResult == RANDOMARROW_BUY_ARROW_RESULT_SUCCESS);
}

BOOL CFSGameUserRandomArrowEvent::UseArrow(SC2S_RANDOMARROW_USE_ARROW_REQ& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == RANDOMARROW.IsOpen(), FALSE);

	SS2C_RANDOMARROW_USE_ARROW_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_RANDOMARROW_USE_ARROW_RES));

	if(m_iMyArrowCnt < (int)rs.eUseArrowType)
	{
		ss.btResult = RANDOMARROW_USE_ARROW_RESULT_FAIL_LACK_ARROW;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + (int)rs.eUseArrowType > MAX_PRESENT_LIST)
	{
		ss.btResult = RANDOMARROW_USE_ARROW_RESULT_FAIL_FULL_MAILBOX;
	}
	else
	{
		int iBonusPot = (int)rs.eUseArrowType;
		BYTE btRateType = RANDOMARROW_RATE_TYPE_NORMAL;

		switch (rs.eUseArrowType)
		{
		case RANDOMARROW_USE_ARROW_1:
			{
				break;
			}
		case RANDOMARROW_USE_ARROW_5:
			{
				// 5개뽑기 시 보너스 독(+1)
				iBonusPot += RANDOMARROW_USE_5_ARROW_BONUS_POT;
				break;
			}
		case RANDOMARROW_USE_ARROW_10:
			{
				// 10개뽑기 시 보너스 독(+3), S등급 확률2배 혜택
				iBonusPot += RANDOMARROW_USE_10_ARROW_BONUS_POT;
				btRateType = RANDOMARROW_RATE_TYPE_S_GRADE_DOUBLE;
				break;
			}
		default:
			{
				return FALSE;
			}
		}

		vector<BYTE> vecResultProductIndex;
		vector<int> vecResultRewardIndex;
		BOOL bIsShout = FALSE;

		RANDOMARROW.GetRandomProduct(rs.eUseArrowType, btRateType, vecResultProductIndex, vecResultRewardIndex, bIsShout);

		for(int i = 0; i < vecResultProductIndex.size(); ++i)
		{
			ss.btProductIndexList[i] = vecResultProductIndex[i];
		}

		ss.btResult = RANDOMARROW_USE_ARROW_RESULT_FAIL_UNKNOWN;
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_RANDOMARROW_UseArrow(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), (int)rs.eUseArrowType, (BOOL)rs.bIsEffectViewOn, iBonusPot, ss.btProductIndexList))
		{
			ss.btResult = RANDOMARROW_USE_ARROW_RESULT_SUCCESS;
			m_iMyArrowCnt	-= (int)rs.eUseArrowType;
			m_iMyPotCnt		+= iBonusPot;

			for(int i = 0; i < vecResultRewardIndex.size(); ++i)
			{
				SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, vecResultRewardIndex[i]);
				if(NULL == pReward)
				{
					WRITE_LOG_NEW(LOG_TYPE_RANDOMARROW_EVENT, LA_DEFAULT, NONE, "REWARDMANAGER.GiveReward UseArrow, UserIDIndex:10752790, RewardIndex:0"
	, m_pUser->GetUserIDIndex(), vecResultRewardIndex[i]);
				}
			}

			if(TRUE == bIsShout)
			{
				CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
				if( NULL != pCenter )
				{
					SS2S_DEFAULT  info;
					strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), _countof(info.GameID)-1);
					pCenter->SendPacket(G2S_RANDOMARROW_EVENT_S_REWARD_SHOUT_REQ, &info, sizeof(SS2S_DEFAULT));
				}
			}
		}
	}

	CPacketComposer Packet(S2C_RANDOMARROW_USE_ARROW_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_RANDOMARROW_USE_ARROW_RES));
	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserRandomArrowEvent::UsePot(SC2S_RANDOMARROW_USE_POT_REQ& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == RANDOMARROW.IsOpen(), FALSE);

	SS2C_RANDOMARROW_USE_POT_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_RANDOMARROW_USE_POT_RES));

	int iPrice = 0, iRewardIndex = 0;

	if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = RANDOMARROW_USE_POT_RESULT_FAIL_FULL_MAILBOX;
	}
	else if(FALSE == RANDOMARROW.GetPotReward(rs.btPotIndex, iPrice, iRewardIndex))
	{
		ss.btResult = RANDOMARROW_USE_POT_RESULT_FAIL_UNKNOWN;
	}
	else if(m_iMyPotCnt < iPrice)
	{
		ss.btResult = RANDOMARROW_USE_POT_RESULT_FAIL_LACK_POT;
	}
	else
	{
		ss.btResult = RANDOMARROW_USE_POT_RESULT_FAIL_UNKNOWN;

		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_RANDOMARROW_UsePot(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iPrice, rs.btPotIndex))
		{
			ss.btResult = RANDOMARROW_USE_POT_RESULT_SUCCESS;
			m_iMyPotCnt	-= iPrice;

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(NULL == pReward)
			{
				WRITE_LOG_NEW(LOG_TYPE_RANDOMARROW_EVENT, LA_DEFAULT, NONE, "REWARDMANAGER.GiveReward UsePot, UserIDIndex:10752790, RewardIndex:0"
, m_pUser->GetUserIDIndex(), iRewardIndex);
			}
		}
	}

	CPacketComposer Packet(S2C_RANDOMARROW_USE_POT_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_RANDOMARROW_USE_POT_RES));
	m_pUser->Send(&Packet);

	return TRUE;
}