// CFSGameUserSkill.cpp: implementation of the CFSGameUserSkill class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameServer.h"
qinclude "CFSSkillShop.h"
qinclude "CFSTrainingShop.h"
qinclude "CFSGameClient.h"
qinclude "MatchSvrProxy.h"
qinclude "CFSGameUserItem.h"
qinclude "ShoppingFestivalEventManager.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFSGameUserSkill::CFSGameUserSkill(): m_SyncObject("UserSkill")
{
	
	m_pUser = NULL;
	
	///////////// WCG ////////////////
	m_bSetSkillForWCG = false;
	/////////////////////////////////
}

CFSGameUserSkill::~CFSGameUserSkill()
{
	
}

BOOL CFSGameUserSkill::GetAvatarInfoInSkillPage(SAvatarInfo & AvatarInfo )
{
	if( NULL == m_pUser ) return NULL;
	
	memset(&AvatarInfo, 0, sizeof(SAvatarInfo));
	
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if(pAvatar)
	{
		memcpy(&AvatarInfo, pAvatar, sizeof(SAvatarInfo));
		return TRUE;
	}
	
	return FALSE;
	
}


// 20090815 스킬 및 프리스타일 구입
BOOL CFSGameUserSkill::BuySkillORFreeStyle(CFSGameODBC* pFSODBC, int iSkillKind, int iSkillIndex, int iSellType, int iUseCouponPropertyKind, CFSSkillShop* pSkillShop, int &iErrorCode, int iCategory, BYTE btUseToken)
{
	// iErrorCode는 이 함수로 들어올 때 BUY_SKILL_ERROR_GENERAL(-5)로 가지고 들어옴, 즉 그냥 리턴되면 일반 에러가 됨
	CHECK_NULL_POINTER_BOOL(m_pUser);
	CHECK_NULL_POINTER_BOOL(pFSODBC);
	CHECK_NULL_POINTER_BOOL(pSkillShop);
	
	SAvatarInfo* pAvatarInfo = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);
	
	SSkillInfo SkillInfo;
	if(FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, iSkillKind, iSkillIndex, SkillInfo, iCategory ))		// iSkillKind : SKILL_TYPE_SKILL(2), SKILL_TYPE_FREESTYLE(3) , SKILL_TYPE_FAME(4)
	{
		return FALSE;
	}

	/////////////////////////////////////// 구입 조건 검사 ///////////////////////////////////////
	std::string	strSkillName;
	int iSkillPriceCash = 0;
	int iSkillPricePoint = 0;
	int iSkillPriceTrophy = 0;
	int iSaleCouponCash = 0;

	if(iUseCouponPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
		iUseCouponPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
	{
		if(CLOSED == SHOPPINGFESTIVAL.IsOpen())
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_USE_EVENT_SALE_COUPON;
			return FALSE;
		}

		if(SKILL_TYPE_SKILL != SkillInfo.iKind && SKILL_TYPE_FREESTYLE != SkillInfo.iKind)
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_USE_EVENT_SALE_COUPON;
			return FALSE;
		}

		if(BUY_SKILL_SELL_TYPE_CASH != iSellType)
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_USE_EVENT_SALE_COUPON;
			return FALSE;
		}

		if(FALSE == m_pUser->GetUserShoppingFestivalEvent()->CheckUseableSaleCoupon(iUseCouponPropertyKind, SkillInfo.iBalanceCash))
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_USE_EVENT_SALE_COUPON;
			return FALSE;
		}

		if(-1 == (iSaleCouponCash = SHOPPINGFESTIVAL.GetSaleCouponCash(iUseCouponPropertyKind)))
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_USE_EVENT_SALE_COUPON;
			return FALSE;
		}
	}

	int iTokenIdx = -1;
	if( TRUE == btUseToken )
	{
		CHECK_CONDITION_RETURN(iSellType == BUY_SKILL_SELL_TYPE_TICKET,FALSE);
		CHECK_CONDITION_RETURN(iSkillKind != SKILL_TYPE_SKILL && iSkillKind != SKILL_TYPE_FREESTYLE , FALSE);

		CFSGameUserItem* pUserItemList = (CFSGameUserItem*)m_pUser->GetUserItemList();
		CHECK_NULL_POINTER_BOOL(pUserItemList);

		CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
		CHECK_NULL_POINTER_BOOL(pAvatarItemList);

		int iTargetPropertyKind = (iSkillKind == SKILL_TYPE_SKILL ? ITEM_PROPERTY_KIND_SKILL_TOKEN_SKILL : ITEM_PROPERTY_KIND_SKILL_TOKEN_FREESTYLE);

		SUserItemInfo* pUserItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iTargetPropertyKind);
		CHECK_NULL_POINTER_BOOL(pUserItem);
		CHECK_CONDITION_RETURN(0 >= pUserItem->iPropertyTypeValue, FALSE);

		CItemPropertyBoxList* pItemPropertyBoxList = CFSGameServer::GetInstance()->GetItemShop()->GetItemPropertyBoxList(iTargetPropertyKind);
		CHECK_NULL_POINTER_BOOL(pItemPropertyBoxList);

		int iConditionLv = pItemPropertyBoxList->GetPropertyIndexByTokenLv(SkillInfo.iGameLevel[0]);
		CHECK_CONDITION_RETURN(-1 == iConditionLv, FALSE);

		iTokenIdx = pUserItem->iItemIdx;
		iSkillPriceCash = iSkillPricePoint = iSkillPriceTrophy = 0;
	}
	else if(FALSE == CheckPayAbiility(iSellType, &SkillInfo, iSkillPriceCash, iSkillPricePoint, iSkillPriceTrophy, iSaleCouponCash, iErrorCode))
	{
		return FALSE;
	}

	if(FALSE == BuySkillProcess_BeforePay(pAvatarInfo, SkillInfo, iErrorCode))
	{		
		return FALSE;
	}

	/////////////////////////////////////// DB를 위한 데이터 작성 ///////////////////////////////////////
	int iStorageType = 0;
	int iSkillShiftBit = 0;

	if( FALSE == GetDBColumnAndShiftBit(pAvatarInfo, SkillInfo.iSkillNo, iStorageType, iSkillShiftBit, SkillInfo.iKind))
	{
		return FALSE;
	}
	
	/////////////////////////////////////// Pay관련 프로세스 ///////////////////////////////////////	
	SBillingInfo BillingInfo;
			
	BillingInfo.pUser = m_pUser;
	memcpy(BillingInfo.szUserID, m_pUser->GetUserID(), MAX_USERID_LENGTH+1);
	if(SKILL_TYPE_SKILL == SkillInfo.iKind)
	{
		BillingInfo.iEventCode = EVENT_BUY_SKILL;		// ContentsCode
	}
	else if(SKILL_TYPE_FREESTYLE == SkillInfo.iKind)
	{
		BillingInfo.iEventCode = EVENT_BUY_FREESTYLE;	// ContentsCode
	}

	char* szSkillName = pItemShop->GetItemText(SkillInfo.iSkillNo + (SkillInfo.iKind * 1000));

	if( NULL != szSkillName )
		strSkillName = szSkillName;
	else
	{
		szSkillName = pItemShop->GetEventCode_Drscription(BillingInfo.iEventCode);
		if( NULL != szSkillName )
			strSkillName = szSkillName;		
	}

	BillingInfo.iItemCode = SkillInfo.iSkillNo;
	BillingInfo.iSellType = iSellType;
	BillingInfo.iCashChange = iSkillPriceCash;
	if(BillingInfo.iCashChange < 0)
		BillingInfo.iCashChange = 0;

	BillingInfo.iPointChange = iSkillPricePoint;
	BillingInfo.iTrophyChange = iSkillPriceTrophy;
	
	BillingInfo.iSerialNum = m_pUser->GetUserIDIndex();
	memcpy( BillingInfo.szGameID, m_pUser->GetGameID(), MAX_GAMEID_LENGTH + 1 );	
	memcpy( BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1 );
	
	BillingInfo.iParam0 = iStorageType;
	BillingInfo.iParam1 = iSkillShiftBit;
	BillingInfo.iParam2 = SKILL_TYPE_SKILL == SkillInfo.iKind ? SkillInfo.iCondition0 : SkillInfo.iCondition1;
	BillingInfo.iParam3 = SkillInfo.iKind;
	BillingInfo.iParam4 = SkillInfo.bIsFameSkill;
	BillingInfo.iCurrentCash = m_pUser->GetCoin();
	BillingInfo.iContentsCode = SkillInfo.iContentsCode;
	strSkillName.copy(BillingInfo.szItemName, MAX_SKILL_NAME_LEN + 1, 0);
	BillingInfo.iTokenItemIdx = iTokenIdx;

	if(iSellType == SELL_TYPE_CASH)
	{
		BillingInfo.iItemPrice = iSkillPriceCash;
	}
	else if(iSellType == SELL_TYPE_POINT)
	{
		BillingInfo.iItemPrice = iSkillPricePoint;
	}
	else if(iSellType == SELL_TYPE_TROPHY)
	{
		BillingInfo.iItemPrice = iSkillPriceTrophy;
	}

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		return FALSE;
	}
	if( BillingInfo.iCashChange > 0 && BillingInfo.iSellType == SELL_TYPE_CASH )
	{
		m_pUser->SetItemCode( BillingInfo.iItemCode );
		m_pUser->SetPayCash( BillingInfo.iCashChange );
		m_pUser->CheckEvent(PERFORM_TIME_ITEMBUY,pFSODBC);
		m_pUser->ResetUserData();
	}

	iErrorCode = BUY_SKILL_ERROR_SUCCESS;

	if(BillingInfo.iPointChange > 0)
	{
		m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
		m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_BUY_POINT_ITEM);
	}

	if(iUseCouponPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
		iUseCouponPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
	{
		m_pUser->GetUserShoppingFestivalEvent()->UseSaleCoupon(BillingInfo.iEventCode, iUseCouponPropertyKind, BillingInfo.iItemCode);
		m_pUser->GetUserShoppingFestivalEvent()->SendEventOpenStatus();
	}

	return TRUE;
}

BOOL CFSGameUserSkill::BuySpecialtySkill(CFSGameODBC* pFSODBC, int iSkillKind, int iSkillIndex, int iSpecialtyNo, int iSellType, CFSSkillShop* pSkillShop, int &iErrorCode)
{
	CHECK_NULL_POINTER_BOOL(m_pUser);
	CHECK_NULL_POINTER_BOOL(pFSODBC);
	CHECK_NULL_POINTER_BOOL(pSkillShop);
	
	SAvatarInfo* pAvatarInfo = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);
	
	SSpecialtySkill* pSkill = (SSpecialtySkill*) pSkillShop->GetSkill(SKILL_TYPE_SPECIALTY, iSkillKind, pAvatarInfo->Status.iGamePosition, iSkillIndex);
	CHECK_NULL_POINTER_BOOL(pSkill);

	CHECK_CONDITION_RETURN(!pSkill->IsValidSpecialtyNo(iSpecialtyNo), FALSE);

	int iSkillPriceCash = 0;
	int iSkillPricePoint = 0;
	int iSkillPriceTrophy = 0;
	
	if (FALSE == CheckPayAbiility(iSellType, pSkill, iSkillPriceCash, iSkillPricePoint, iSkillPriceTrophy, 0, iErrorCode))
	{
		return FALSE;
	}

	if (pSkill->iLvCondition > pAvatarInfo->iLv)
	{
		iErrorCode = BUY_SKILL_ERROR_MISMATCH_LEVEL;
		return FALSE;
	}

	SBillingInfo BillingInfo;
			
	BillingInfo.pUser = m_pUser;
	memcpy(BillingInfo.szUserID, m_pUser->GetUserID(), MAX_USERID_LENGTH+1);
	BillingInfo.iEventCode = EVENT_BUY_SPECIALTY_SKILL;
	BillingInfo.iItemCode = pSkill->iSkillNo;
	BillingInfo.iSellType = iSellType;
	BillingInfo.iCashChange = iSkillPriceCash;
	BillingInfo.iPointChange = iSkillPricePoint;
	BillingInfo.iTrophyChange = iSkillPriceTrophy;
	
	BillingInfo.iSerialNum = m_pUser->GetUserIDIndex();
	memcpy( BillingInfo.szGameID, m_pUser->GetGameID(), MAX_GAMEID_LENGTH + 1 );	
	
	BillingInfo.iParam0 = pSkill->iKind;
	BillingInfo.iParam1 = pAvatarInfo->Status.iGamePosition;
	BillingInfo.iParam2 = pAvatarInfo->iLv;
	BillingInfo.iParam3 = iSpecialtyNo;
	BillingInfo.iCurrentCash = m_pUser->GetCoin();

	char* szSkillName = pItemShop->GetItemText(pSkill->iSkillNo + (SKILL_TYPE_SPECIALTY * 1000));

	if( NULL != szSkillName )
	{
	 	memcpy( BillingInfo.szItemName, szSkillName, MAX_ITEMNAME_LENGTH + 1 );	
	}
	else
	{
		szSkillName = pItemShop->GetEventCode_Drscription(BillingInfo.iEventCode);

		if( NULL != szSkillName )
		{
			memcpy( BillingInfo.szItemName, szSkillName, MAX_ITEMNAME_LENGTH + 1 );	
		}
		else
		{
			strcpy_s(BillingInfo.szItemName, "SpecialtySkill");
		}
	}

	if(iSellType == SELL_TYPE_CASH)	
	{
		BillingInfo.iItemPrice = iSkillPriceCash;
	}
	else if(iSellType == SELL_TYPE_POINT)	
	{
		BillingInfo.iItemPrice = iSkillPricePoint;
	}
	else if(iSellType == SELL_TYPE_TROPHY)	
	{
		BillingInfo.iItemPrice = iSkillPriceTrophy;
	}

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		return FALSE;
	}
	if( BillingInfo.iCashChange > 0 && BillingInfo.iSellType == SELL_TYPE_CASH )
	{
		m_pUser->SetItemCode( BillingInfo.iItemCode );
		m_pUser->SetPayCash( BillingInfo.iCashChange );
		m_pUser->CheckEvent(PERFORM_TIME_ITEMBUY,pFSODBC);
		m_pUser->ResetUserData();
	}

	if(BillingInfo.iPointChange > 0)
	{
		m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
		m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_BUY_POINT_ITEM);
	}

	iErrorCode = BUY_SKILL_ERROR_SUCCESS;
	return TRUE;
}

void CFSGameUserSkill::InsertSkill( char *szGameID, int iSkillIndex, int iSkillShiftBit, BOOL bFameSkill )
{
	CLock lock(&m_SyncObject);

	SAvatarInfo * pAvatarInfo =  m_pUser->GetAvatarInfoWithGameID(szGameID);
	if(NULL == pAvatarInfo) return;
	
	if( iSkillIndex < 32 )
	{
		pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_0] |= iSkillShiftBit;
		pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_0] |= iSkillShiftBit;
	}
	else if( iSkillIndex < 64 )
	{
		pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_1] |= iSkillShiftBit;
		pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_1] |= iSkillShiftBit;
	}
	else if( iSkillIndex < 96 )
	{
		pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_2] |= iSkillShiftBit;
		pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_2] |= iSkillShiftBit;
	}
	else if( iSkillIndex < 128 )
	{
		pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_3] |= iSkillShiftBit;
		pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_3] |= iSkillShiftBit;
	}
	else if( iSkillIndex < 160 )
	{
		pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_4] |= iSkillShiftBit;
		pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_4] |= iSkillShiftBit;
	}
}

BOOL CFSGameUserSkill::BuyTraining(CFSGameODBC *pFSODBC,  int iTrainingNo , CFSTrainingShop *pTrainigShop, int& iNextSkillNo, int & iErrorCode, BYTE btUseToken)
{
	
	if( m_pUser == NULL ) return FALSE;
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pFSODBC || NULL == pTrainigShop || NULL == pAvatarInfo )
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	STrainingInfo Training;
	if( FALSE == pTrainigShop->GetTraining( iTrainingNo , Training ))
	{
		iErrorCode = -5;
		return FALSE;
	}

	int iPointCost = Training.GetCurrentPrice();

	int iTokenIdx = -1;
	if( TRUE == btUseToken )
	{
		iErrorCode = -5;
		CFSGameUserItem* pUserItemList = (CFSGameUserItem*)m_pUser->GetUserItemList();
		CHECK_NULL_POINTER_BOOL(pUserItemList);

		CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
		CHECK_NULL_POINTER_BOOL(pAvatarItemList);

		iErrorCode = -1;
		SUserItemInfo* pUserItem = pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_SKILL_TOKEN_TRAINING);
		CHECK_NULL_POINTER_BOOL(pUserItem);
		CHECK_CONDITION_RETURN(0 >= pUserItem->iPropertyTypeValue, FALSE);

		CItemPropertyBoxList* pItemPropertyBoxList = CFSGameServer::GetInstance()->GetItemShop()->GetItemPropertyBoxList(ITEM_PROPERTY_KIND_SKILL_TOKEN_TRAINING);
		CHECK_NULL_POINTER_BOOL(pItemPropertyBoxList);

		int iPropertyIndex = pItemPropertyBoxList->GetPropertyIndexByTokenLv(Training.iConditionLv);
		CHECK_CONDITION_RETURN(-1 == iPropertyIndex, FALSE);

		iTokenIdx = pUserItem->iItemIdx;
		iErrorCode = 0;
	}
	else if( m_pUser->GetSkillPoint() < iPointCost )
	{
		iErrorCode = -1;
		return FALSE;
	}
	
	
	if(  Training.iConditionLv > pAvatarInfo->iLv)
	{
		iErrorCode = -8;
		return FALSE;
	}
	
	
	int iTrainingLv = 1 << Training.iTrainingLv;
	if( iTrainingLv & pAvatarInfo->Skill.iaTraining[Training.iCategory] )
	{
		iErrorCode = -5;
		return FALSE;
	}
	int iPrevTrainingLv = pAvatarInfo->Skill.iaTraining[Training.iCategory] << 1;
	if( (iTrainingLv > 1) && ((iTrainingLv & iPrevTrainingLv) == 0) )
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	
	int iPrevMoney = 0 , iPostMoney = 0;
	
	iPrevMoney = m_pUser->GetCoin();
	iPostMoney = iPrevMoney;
	
	int iTrainingShift = 1 << Training.iTrainingLv;
	
	int iUpdateTokenCount = 0;
	if( ODBC_RETURN_SUCCESS != pFSODBC->spBuyTraining( m_pUser->GetUserIDIndex(), pAvatarInfo->iGameIDIndex , iTrainingNo , iTrainingShift, iPointCost, iPrevMoney , iPostMoney, iTokenIdx, iUpdateTokenCount))
	{
		iErrorCode = -5;
		return FALSE;
	}
	else
	{
		if( 0 < iTokenIdx )
		{
			CFSGameUserItem* pUserItemList = (CFSGameUserItem*)m_pUser->GetUserItemList();
			if( pUserItemList )
			{
				CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
				if( pAvatarItemList)
				{
					if( 0 == iUpdateTokenCount )
						pAvatarItemList->RemoveItem(iTokenIdx);
					else
						pAvatarItemList->UpdateItemCount(iTokenIdx, iUpdateTokenCount);
				}
			}
		}
	}

	iNextSkillNo = pTrainigShop->GetNextTrainingNo(Training.iCategory, Training.iTrainingLv, pAvatarInfo->iLv);

	pAvatarInfo->Skill.iaTraining[Training.iCategory] |= iTrainingShift;
	
	
	int iParamNum = 0;
	if( 0 == Training.iType ) iParamNum = 1;
	else if( 1 == Training.iType  ) iParamNum = 2;
	
	for(int i=0;i<iParamNum;i++)
	{
		if( Training.iaStat[i] == 0 && pAvatarInfo->Status.iStatRun < 99)
			pAvatarInfo->Status.iStatRun += Training.iaValue[i];
		else if( Training.iaStat[i] == 1 && pAvatarInfo->Status.iStatJump < 99 )
			pAvatarInfo->Status.iStatJump += Training.iaValue[i];
		else if( Training.iaStat[i] == 2 && pAvatarInfo->Status.iStatStr < 99 )
			pAvatarInfo->Status.iStatStr += Training.iaValue[i];
		else if( Training.iaStat[i] == 3 && pAvatarInfo->Status.iStatPass  < 99 )
			pAvatarInfo->Status.iStatPass += Training.iaValue[i];
		else if( Training.iaStat[i] == 4 && pAvatarInfo->Status.iStatDribble < 99 )
			pAvatarInfo->Status.iStatDribble += Training.iaValue[i];
		else if( Training.iaStat[i] == 5 && pAvatarInfo->Status.iStatRebound < 99 )
			pAvatarInfo->Status.iStatRebound += Training.iaValue[i];
		else if( Training.iaStat[i] == 6 && pAvatarInfo->Status.iStatBlock  < 99 )
			pAvatarInfo->Status.iStatBlock += Training.iaValue[i];
		else if( Training.iaStat[i] == 7 && pAvatarInfo->Status.iStatSteal  < 99 )
			pAvatarInfo->Status.iStatSteal += Training.iaValue[i];
		else if( Training.iaStat[i] == 8 && pAvatarInfo->Status.iStat2Point  < 99 )
			pAvatarInfo->Status.iStat2Point += Training.iaValue[i];
		else if( Training.iaStat[i] == 9 && pAvatarInfo->Status.iStat3Point < 99 )
			pAvatarInfo->Status.iStat3Point += Training.iaValue[i];
		else if( Training.iaStat[i] == 10 && pAvatarInfo->Status.iStatDrive < 99 )
			pAvatarInfo->Status.iStatDrive += Training.iaValue[i];
		else if( Training.iaStat[i] == 11 && pAvatarInfo->Status.iStatClose  < 99 )
			pAvatarInfo->Status.iStatClose += Training.iaValue[i];
		
	}

	if( m_pUser->GetCloneCharacter() )
	{
		m_pUser->SetCloneCharacterAddStat(Training.iCategory);
	}
	
	if( 0 < iTokenIdx )
	{
		/*do nothing*/
	} 
	else
		m_pUser->SetSkillPoint( m_pUser->GetSkillPoint() - iPointCost );

	m_pUser->AddPointAchievements(); 

	m_pUser->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_POINT_USE, Training.iPrice);
	m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_POINT, Training.iPrice);
	m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_POINT, Training.iPrice);
	m_pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POINT_ITEM);
	m_pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_USEPOINT, Training.iPrice);
	m_pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_POINT, Training.iPrice);
	m_pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USE_POINT, Training.iPrice);
	
	if(Training.iPrice > 0)
	{
		m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
		m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_BUY_POINT_ITEM);
	}

	m_pUser->GetUserFriendInvite()->UpdateFriendInviteMission(FRIEND_INVITE_MISSION_INDEX_END_OF_TRAINING);

	iErrorCode = BUY_SKILL_ERROR_SUCCESS;
	return TRUE;
}

void CFSGameUserSkill::GetTrainingCountsforAchievement(SAvatarInfo* pAvatarInfo, vector<int>& vTraingCounts)
{
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	int iTrainingStep1 = 1;
	int iTrainingStep2 = 1 << 1;
	int iTrainingStep3 = 1 << 2;
	int iTrainingStep4 = 1 << 3;

	for (int iLoopIndex = STAT_TRAINING_CATEGOTY_RUN; iLoopIndex <= STAT_TRAINING_CATEGOTY_CLOSE; iLoopIndex++)
	{
		if (pAvatarInfo->Skill.iaTraining[iLoopIndex] & iTrainingStep1)
		{
			vTraingCounts[PARAMETER_INDEX_TRAINING_STEP1]++;
		}

		if (pAvatarInfo->Skill.iaTraining[iLoopIndex] & iTrainingStep2)
		{
			vTraingCounts[PARAMETER_INDEX_TRAINING_STEP2]++;
		}

		if (pAvatarInfo->Skill.iaTraining[iLoopIndex] & iTrainingStep3)
		{
			vTraingCounts[PARAMETER_INDEX_TRAINING_STEP3]++;
		}

		if (pAvatarInfo->Skill.iaTraining[iLoopIndex] & iTrainingStep4)
		{
			vTraingCounts[PARAMETER_INDEX_TRAINING_STEP4]++;
			vTraingCounts[iLoopIndex + 4] = 4;
		}
	}
}

void CFSGameUserSkill::InsertFreestyle(char *szGameID, int iFreestyleNo, int iFreestyle, BOOL bFameSkill)
{
	CLock lock(&m_SyncObject);

	SAvatarInfo * pAvatarInfo =  m_pUser->GetAvatarInfoWithGameID(szGameID);
	if (NULL == pAvatarInfo) return;
	
	if( iFreestyleNo < 32 )
	{
		pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_0] |= iFreestyle;
		pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_0] |= iFreestyle;
	}
	else if( iFreestyleNo < 64 )
	{
		pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_1] |= iFreestyle;
		pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_1] |= iFreestyle;
	}
	else if( iFreestyleNo < 96 )
	{
		pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_2] |= iFreestyle;
		pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_2] |= iFreestyle;
	}
	else if( iFreestyleNo < 128 )
	{
		pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_3] |= iFreestyle;
		pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_3] |= iFreestyle;
	}
	else if( iFreestyleNo < 160 )
	{
		pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_4] |= iFreestyle;
		pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_4] |= iFreestyle;
	}
}

BOOL CFSGameUserSkill::InsertToSlot(int iSkillNo , CFSSkillShop *pSkillShop , int & iErrorCode, int iSlot /*= -1*/ )
{
	CLock lock(&m_SyncObject);

	CFSGameODBC *pFSODBC  = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC || NULL == pSkillShop || NULL == m_pUser)
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	SSkillInfo SkillInfo;
	if( FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition , SKILL_TYPE_SKILL , iSkillNo , SkillInfo ))
	{
		iErrorCode = -5;
		return FALSE;
	}

	if(SkillInfo.iRequiredSkillPoint > m_pUser->GetCurAvatarRemainSkillPoint())
	{
		iErrorCode = -2;
		return FALSE;
	}

	int iSkillType = SkillInfo.iType;
	
	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	
	int   iaUseSkill[MAX_SKILL_SLOT];
	int   iaUseSkillType[MAX_SKILL_SLOT];
	char  szUseSkill[MAX_SKILL_STOCK_LEGNTH+1];
	
	memcpy( iaUseSkill , pAvatarInfo->Skill.iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( iaUseSkillType , pAvatarInfo->Skill.iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );
	
	if (FALSE == pAvatarInfo->Skill.IsOwnedInStock(iSkillNo, SKILL_TYPE_SKILL) ||
		TRUE == pAvatarInfo->Skill.IsUsedSkill(iSkillNo, SKILL_TYPE_SKILL))
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	int iFindSlot = FindEmptySlot(iSlot , iaUseSkill, iSlotCount);
	//
	if( iFindSlot == -1 )
	{
		iErrorCode = -4;
		
		return FALSE;
	}
	
	iaUseSkill[iFindSlot] = iSkillNo;
	iaUseSkillType[iFindSlot] = iSkillType;
	
	memcpy( szUseSkill , pAvatarInfo->Skill.szUseSkill , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
		
	//Convert
	char szItemSlot[20];
	memset(szItemSlot, 0, 20);
	
	sprintf_s(szItemSlot, _countof(szItemSlot), "/10752790:0:7106560", iFindSlot, iSkillNo, iSkillType);
ill[MAX_SKILL_STOCK_LEGNTH] = 0;
	if((strlen(szUseSkill)+strlen(szItemSlot)) > MAX_SKILL_STOCK_LEGNTH)
	{
		int iSuccessCode = -1;
		int iKind = SKILL_TYPE_SKILL;
		int iOp = INSERT_INVENTORY_ALL;

		CPacketComposer Packet(S2C_INSERT_TO_INVENTORY_RES);
		if( m_pUser->GetUserSkill()->InsertToInventoryAllSkill(iSuccessCode) )
		{
			iSuccessCode = 0;
			Packet.Add(iSuccessCode);
			Packet.Add(iKind);
			Packet.Add(iOp);
		}
		else
		{
			Packet.Add(iSuccessCode);
			Packet.Add(iKind);
			Packet.Add(iOp);
		}
		m_pUser->Send(&Packet);
		return FALSE;
	}

	strcat_s(szUseSkill, _countof(szUseSkill), szItemSlot);
	
	int iaSkillStock[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaSkillStock, pAvatarInfo->Skill.iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	SAvatarSkill::RemoveSkill(iaSkillStock, iSkillNo);
	
	/// DB Chanage 
	//////////// WCG /////////////////////////////  WCG서버는 디비 변경 안한다
	if( m_bSetSkillForWCG == false )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserSkill(pAvatarInfo->iGameIDIndex ,szUseSkill ) )
		{
			iErrorCode = -5;
			return FALSE;
		}
	}
	//
	
	memcpy( pAvatarInfo->Skill.iaUseSkill , iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.iaUseSkillType , iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.szUseSkill , szUseSkill , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	memcpy(pAvatarInfo->Skill.iaSkillStock, iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	m_pUser->SendSkillTakeEnable();
	return TRUE;
}


BOOL CFSGameUserSkill::InsertToInventory(int iSkillNo, CFSSkillShop *pSkillShop, int & iErrorCode, int iSlot /* = -1 */ )
{
	CLock lock(&m_SyncObject);

	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);

	if( NULL == pFSODBC )
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	int   iaUseSkill[MAX_SKILL_SLOT];
	int   iaUseSkillType[MAX_SKILL_SLOT];
	char  szUseSkill[MAX_SKILL_STOCK_LEGNTH+1];
	memset(szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	
	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	// 20080401 Add Skill Slot
	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	// End
	
	memcpy( iaUseSkill , pAvatarInfo->Skill.iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( iaUseSkillType , pAvatarInfo->Skill.iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );
	
	if( iSlot == -1 )
	{
		
		iSlot = FindUsedSlot(iSkillNo , iaUseSkill, iSlotCount);
		//
	}
	
	/// not in slot
	if( iSlot == -1 )
	{
		iErrorCode = -5;

		return FALSE;
	}
	
	iaUseSkill[iSlot] = -1;
	iaUseSkillType[iSlot] = -1;

	
	int iaSkillStock[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaSkillStock, pAvatarInfo->Skill.iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	SAvatarSkill::AddSkill(iaSkillStock, iSkillNo);
	SSkillInfo SkillInfo;
	
	// TODO 리팩토링 필요
	if(TRUE == pSkillShop->GetSkillCondition(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iSkillNo, SkillInfo))
	{
		int iaUseFreestyle[MAX_SKILL_SLOT];
		int iaUseFreestyleType[MAX_SKILL_SLOT];
		char szUseFreestyle[MAX_SKILL_STOCK_LEGNTH+1];
		int iaFreeStyleStock[MAX_SKILL_STORAGE_COUNT];

		memcpy( iaUseFreestyle , pAvatarInfo->Skill.iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
		memcpy( iaUseFreestyleType , pAvatarInfo->Skill.iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );
		memcpy( iaFreeStyleStock, pAvatarInfo->Skill.iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT );
		memset( szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );

		iSlot = FindUsedSlot(SkillInfo.iSkillNo, iaUseFreestyle, iSlotCount);

		if(-1 != iSlot)
		{
			iaUseFreestyle[iSlot] = -1;
			iaUseFreestyleType[iSlot] = -1;

			SAvatarSkill::AddSkill(iaFreeStyleStock, SkillInfo.iSkillNo);
		}

		ConvertItemSlotToString(szUseFreestyle, _countof(szUseFreestyle), iaUseFreestyle, iaUseFreestyleType);

		if(m_bSetSkillForWCG == false)
		{
			if(ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserFreestyle(pAvatarInfo->iGameIDIndex,szUseFreestyle))
			{
				iErrorCode = -5;
				return FALSE;
			}
		}

		memcpy( pAvatarInfo->Skill.iaUseFreestyle , iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
		memcpy( pAvatarInfo->Skill.iaUseFreestyleType , iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );	
		memcpy( pAvatarInfo->Skill.szUseFreestyle , szUseFreestyle , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
		memcpy( pAvatarInfo->Skill.iaFreestyleStock, iaFreeStyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

	}
	
	if(TRUE == pSkillShop->GetSkillCondition(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iSkillNo, SkillInfo))
	{
		iSlot = FindUsedSlot(SkillInfo.iSkillNo , iaUseSkill, iSlotCount);
		
		if(-1 != iSlot)
		{
			iaUseSkill[iSlot] = -1;
			iaUseSkillType[iSlot] = -1;

			
			SAvatarSkill::AddSkill(iaSkillStock, SkillInfo.iSkillNo);
		}

		iSkillNo = SkillInfo.iSkillNo;
	}
	
	//Convert
	ConvertItemSlotToString(szUseSkill, _countof(szUseSkill), iaUseSkill, iaUseSkillType );
	
	/// DB Chanage 
	
	//////////// WCG /////////////////////////////  WCG서버는 디비 변경 안한다
	if(  m_bSetSkillForWCG == false  )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserSkill(pAvatarInfo->iGameIDIndex, szUseSkill ) )
		{
			iErrorCode = -5;
			return FALSE;
		}
	}
	
	memcpy( pAvatarInfo->Skill.iaUseSkill , iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.iaUseSkillType , iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.szUseSkill , szUseSkill , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	memcpy(pAvatarInfo->Skill.iaSkillStock, iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	m_pUser->SendSkillTakeEnable();
	return TRUE;
}


BOOL CFSGameUserSkill::InsertToInventoryAllSkill(int & iErrorCode , int iMode , int iStartSlotNumber )
{
	CLock lock(&m_SyncObject);

	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC )
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	int   iaUseSkill[MAX_SKILL_SLOT];
	int   iaUseSkillType[MAX_SKILL_SLOT];
	char  szUseSkill[MAX_SKILL_STOCK_LEGNTH+1];
	memset(szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	// 20080401 Add Skill Slot
	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	// End 
	
	memcpy( iaUseSkill , pAvatarInfo->Skill.iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( iaUseSkillType , pAvatarInfo->Skill.iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );


	int iaSkillStock[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaSkillStock, pAvatarInfo->Skill.iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

	int iSkillNo, iSlot;
		
	bool bChanged = false;
	
	if( iMode == 1 ) iSlotCount = MAX_SKILL_SLOT;
	
	for(int i=iStartSlotNumber ; i< iSlotCount ; i++ )
	{
		if( iaUseSkill[i] != -1 )
		{
			iSkillNo = iaUseSkill[i];
			iSlot = FindUsedSlot(iSkillNo , iaUseSkill, iSlotCount);
			//
			if( iSlot != -1 )
			{
				iaUseSkill[iSlot] = -1;
				iaUseSkillType[iSlot] = -1;
				
				SAvatarSkill::AddSkill(iaSkillStock, iSkillNo);
				
				/// 장착조건에 걸리는 기술까지 해제  /////////////
				CFSGameServer* pServer = CFSGameServer::GetInstance();
				if( pServer )
				{
					CFSSkillShop* pSkillShop = pServer->GetSkillShop();
					if( pSkillShop )
					{
						SSkillInfo SkillInfo;
						if( pSkillShop->GetSkillCondition(pAvatarInfo->Status.iGamePosition , SKILL_TYPE_SKILL , iSkillNo , SkillInfo ))
						{
							iSlot = FindUsedSlot(SkillInfo.iSkillNo , iaUseSkill, iSlotCount);
							
							if( iSlot != -1 )
							{
								iaUseSkill[iSlot] = -1;
								iaUseSkillType[iSlot] = -1;

								
								SAvatarSkill::AddSkill(iaSkillStock, SkillInfo.iSkillNo);
							}
						}
					}
				}
				/////////////////////////////////////////////////
				
				bChanged = true;
				//
			}
		}
	}
	
	if( !bChanged )
	{
		return TRUE;
	}
	
	ConvertItemSlotToString(szUseSkill, _countof(szUseSkill), iaUseSkill, iaUseSkillType );
	
	//////////// WCG /////////////////////////////  WCG서버는 디비 변경 안한다
	if( m_bSetSkillForWCG == false  )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserSkill(pAvatarInfo->iGameIDIndex, szUseSkill ) )
		{
			iErrorCode = -5;
			return FALSE;
		}
	}

	memcpy( pAvatarInfo->Skill.iaUseSkill , iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.iaUseSkillType , iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.szUseSkill , szUseSkill , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	memcpy( pAvatarInfo->Skill.iaSkillStock, iaSkillStock, sizeof(int) * MAX_SKILL_STORAGE_COUNT);
	
	m_pUser->SendSkillTakeEnable();
	return TRUE;
}



BOOL CFSGameUserSkill::InsertToSlotFreestyle(int iFreestyleNo, CFSSkillShop *pSkillShop , int & iErrorCode , int iSlot)
{
	CLock lock(&m_SyncObject);

	CFSGameODBC *pFSODBC  = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC || NULL == pSkillShop )
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	SSkillInfo FreestyleInfo;
	if( FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition , SKILL_TYPE_FREESTYLE , iFreestyleNo , FreestyleInfo ))
	{
		iErrorCode = -5;
		return FALSE;
	}

	if(FreestyleInfo.iRequiredSkillPoint > m_pUser->GetCurAvatarRemainSkillPoint())
	{
		iErrorCode = -2;
		return FALSE;
	}

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	
	int iFreestyleType = FreestyleInfo.iType;	
	int iaUseFreestyle[MAX_SKILL_SLOT];
	int iaUseFreestyleType[MAX_SKILL_SLOT];
	char szUseFreestyle[MAX_SKILL_STOCK_LEGNTH+1];
	
	memcpy( iaUseFreestyle , pAvatarInfo->Skill.iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( iaUseFreestyleType , pAvatarInfo->Skill.iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );
	
	if (FALSE == pAvatarInfo->Skill.IsOwnedInStock(iFreestyleNo, SKILL_TYPE_FREESTYLE) ||
		TRUE == pAvatarInfo->Skill.IsUsedSkill(iFreestyleNo, SKILL_TYPE_FREESTYLE))
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	int iFindSlot = FindEmptySlot(iSlot , iaUseFreestyle, iSlotCount);
	
	if( iFindSlot == -1 )
	{
		iErrorCode = -4;
		return FALSE;
	}
	
	iaUseFreestyle[iFindSlot] = iFreestyleNo;
	iaUseFreestyleType[iFindSlot] = iFreestyleType;
	
	memcpy( szUseFreestyle , pAvatarInfo->Skill.szUseFreestyle , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	
	//Convert
	char szItemSlot[20];
	memset(szItemSlot, 0, 20);
	
	sprintf_s(szItemSlot, _countof(szItemSlot), "/10752790:0:7106560", iFindSlot, iFreestyleNo, iFreestyleType);
eestyle[MAX_SKILL_STOCK_LEGNTH] = 0;
	if((strlen(szUseFreestyle)+strlen(szItemSlot)) > MAX_SKILL_STOCK_LEGNTH)
	{
		int iSuccessCode = -1;
		int iKind = SKILL_TYPE_FREESTYLE;
		int iOp = INSERT_INVENTORY_ALL;

		CPacketComposer Packet(S2C_INSERT_TO_INVENTORY_RES);
		if( m_pUser->GetUserSkill()->InsertToInventoryAllFreestyle( iSuccessCode) )
		{
			iSuccessCode = 0;
			Packet.Add(iSuccessCode);
			Packet.Add(iKind);
			Packet.Add(iOp);
		}
		else
		{
			Packet.Add(iSuccessCode);
			Packet.Add(iKind);
			Packet.Add(iOp);
		}
		m_pUser->Send(&Packet);
		return FALSE;
	}

	strcat_s(szUseFreestyle, _countof(szUseFreestyle), szItemSlot);
	
	int iaFreeStyleStock[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaFreeStyleStock, pAvatarInfo->Skill.iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	SAvatarSkill::RemoveSkill(iaFreeStyleStock, iFreestyleNo);
	
	/// DB Chanage
	//////////// WCG /////////////////////////////  WCG서버는 디비 변경 안한다
	
	if( m_bSetSkillForWCG == false )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserFreestyle(pAvatarInfo->iGameIDIndex,szUseFreestyle) )
		{
			iErrorCode = -5;
			return FALSE;
		}
	}
	
	memcpy( pAvatarInfo->Skill.iaUseFreestyle , iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.iaUseFreestyleType , iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );	
	memcpy( pAvatarInfo->Skill.szUseFreestyle , szUseFreestyle , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	memcpy(pAvatarInfo->Skill.iaFreestyleStock, iaFreeStyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	return TRUE;
}

BOOL CFSGameUserSkill::InsertToInventoryFreestyle(int iFreestyleNo , CFSSkillShop *pSkillShop, int & iErrorCode, int iSlot)
{
	CLock lock(&m_SyncObject);

	CFSGameODBC *pFSODBC  = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC )
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	int iaUseFreestyle[MAX_SKILL_SLOT];
	int iaUseFreestyleType[MAX_SKILL_SLOT];
	char szUseFreestyle[MAX_SKILL_STOCK_LEGNTH+1];
	memset(szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	// 20080401 Add Skill Slot
	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	// End 
	
	memcpy( iaUseFreestyle , pAvatarInfo->Skill.iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( iaUseFreestyleType , pAvatarInfo->Skill.iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );
	
	if( iSlot == -1 )
	{
		iSlot = FindUsedSlot(iFreestyleNo , iaUseFreestyle, iSlotCount);
	}
	
	if( iSlot == -1 )
	{
		iErrorCode = -5;

		return FALSE;
	}
	
	iaUseFreestyle[iSlot] = -1;
	iaUseFreestyleType[iSlot] = -1;

	int iaFreeStyleStock[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaFreeStyleStock, pAvatarInfo->Skill.iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	SAvatarSkill::AddSkill(iaFreeStyleStock, iFreestyleNo);

	SSkillInfo FreestyleInfo;
	while(TRUE == pSkillShop->GetSkillCondition(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iFreestyleNo, FreestyleInfo))
	{
		iSlot = FindUsedSlot(FreestyleInfo.iSkillNo , iaUseFreestyle, iSlotCount );

		if(-1 != iSlot)
		{
			iaUseFreestyle[iSlot] = -1;
			iaUseFreestyleType[iSlot] = -1;
			
			SAvatarSkill::AddSkill(iaFreeStyleStock, FreestyleInfo.iSkillNo);
		}

		iFreestyleNo = FreestyleInfo.iSkillNo;
	}
	
	ConvertItemSlotToString(szUseFreestyle, _countof(szUseFreestyle), iaUseFreestyle, iaUseFreestyleType );
	
	//////////// WCG /////////////////////////////  WCG서버는 디비 변경 안한다
	if( m_bSetSkillForWCG == false  )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserFreestyle(pAvatarInfo->iGameIDIndex, szUseFreestyle) )
		{
			iErrorCode = -5;
			return FALSE;
		}
	}
	
	memcpy( pAvatarInfo->Skill.iaUseFreestyle , iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.iaUseFreestyleType , iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.szUseFreestyle , szUseFreestyle , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	memcpy(pAvatarInfo->Skill.iaFreestyleStock, iaFreeStyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

	return TRUE;
}


BOOL CFSGameUserSkill::InsertToInventoryAllFreestyle(int & iErrorCode, int iMode , int iStartSlotNumber )
{
	CLock lock(&m_SyncObject);

	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC )
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	
	int iaUseFreestyle[MAX_SKILL_SLOT];
	int iaUseFreestyleType[MAX_SKILL_SLOT];
	char szUseFreestyle[MAX_SKILL_STOCK_LEGNTH+1];
	memset(szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();
	
	// 20080401 Add Skill Slot
	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	// End
	
	memcpy( iaUseFreestyle , pAvatarInfo->Skill.iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( iaUseFreestyleType , pAvatarInfo->Skill.iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );
	
	int iaFreeStyleStock[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaFreeStyleStock, pAvatarInfo->Skill.iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

	int iFreestyleNo, iSlot;
	bool bChanged = false;	
	
	if( iMode == 1 ) iSlotCount = MAX_SKILL_SLOT;
	
	for(int i=iStartSlotNumber ; i< iSlotCount ; i++ )
	{
		
		if( iaUseFreestyle[i] != -1 )
		{
			iFreestyleNo = iaUseFreestyle[i];
			
			iSlot = FindUsedSlot(iFreestyleNo , iaUseFreestyle, iSlotCount);
			if( iSlot != -1 )
			{
				
				iaUseFreestyle[iSlot] = -1;
				iaUseFreestyleType[iSlot] = -1;
				
				SAvatarSkill::AddSkill(iaFreeStyleStock, iFreestyleNo);
				
				//////////////////// 장착조건에 걸리는 프리스타일까지 해제 
				/// 장착조건에 걸리는 기술까지 해제  /////////////
				CFSGameServer* pServer = CFSGameServer::GetInstance();
				if( pServer )
				{
					CFSSkillShop* pSkillShop = pServer->GetSkillShop();
					if( pSkillShop )
					{
						SSkillInfo FreestyleInfo;
						if( pSkillShop->GetSkillCondition(pAvatarInfo->Status.iGamePosition , SKILL_TYPE_FREESTYLE , iFreestyleNo , FreestyleInfo ))
						{
							iSlot = FindUsedSlot(FreestyleInfo.iSkillNo , iaUseFreestyle, iSlotCount);
							if ( -1 != iSlot )
							{
								iaUseFreestyle[iSlot] = -1;
								iaUseFreestyleType[iSlot] = -1;

								SAvatarSkill::AddSkill(iaFreeStyleStock, FreestyleInfo.iSkillNo);
							}
						}
					}
				}
				/////////////////////////////////////////////////////////////
				
				bChanged = true;
				//
			}
		}
	}
	
	if( !bChanged )
	{
		return TRUE;
	}
	
	//Convert
	ConvertItemSlotToString(szUseFreestyle, _countof(szUseFreestyle), iaUseFreestyle, iaUseFreestyleType );
	
	//////////// WCG /////////////////////////////  WCG서버는 디비 변경 안한다
	if( m_bSetSkillForWCG == false  )
	{	
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserFreestyle(pAvatarInfo->iGameIDIndex, szUseFreestyle) )
		{
			iErrorCode = -5;
			return FALSE;
		}
	}
	
	memcpy( pAvatarInfo->Skill.iaUseFreestyle , iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.iaUseFreestyleType , iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.szUseFreestyle , szUseFreestyle , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	memcpy(pAvatarInfo->Skill.iaFreestyleStock, iaFreeStyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	return TRUE;
}

int CFSGameUserSkill::FindEmptySlot(int iSlot , int *iaUserSkill, int iMaxSlotNum)
{
	int iRetSlot = 0;
	
	int i=0;
	for(i=0; i<iMaxSlotNum; i++)
	{
		if( iaUserSkill[i] == -1 && (iSlot == i || iSlot == -1) )
		{
			iRetSlot = i;
			break;
		}
		
	}
	if( i == iMaxSlotNum )
		iRetSlot = -1;
	
	return iRetSlot;
}


int CFSGameUserSkill::FindUsedSlot(int iSkillNo , int *iaUserSkill, int iMaxSlotNum)
{
	int iRetSlot = 0;
	
	int i=0;
	for(i=0; i<iMaxSlotNum; i++)
	{
		if( iaUserSkill[i] == iSkillNo )
		{
			iRetSlot = i;
			break;
		}
		
	}
	if( i == iMaxSlotNum )
		iRetSlot = -1;
	
	return iRetSlot;
}

// 슬롯 배열 형태로 되어 있는 아이템을 문자열 형태로 보관 
// 2010.01.12
// Modify for Migration 2005
// ktKim
void CFSGameUserSkill::ConvertItemSlotToString(char *szUseSkill, size_t sizeOfBuffer, int *iaUseSkill, int *iaUseSkillType)
{
	char szItemSlot[20];
	
	for(int i=0; i<MAX_SKILL_SLOT; i++)
	{
		if( iaUseSkill[i] != -1)
		{
			sprintf_s(szItemSlot, _countof(szItemSlot), "/10752790:0:7106560", i, iaUseSkill[i], iaUseSkillType[i]);
trlen(szUseSkill)+strlen(szItemSlot)) <= MAX_SKILL_STOCK_LEGNTH)
				strcat_s(szUseSkill, sizeOfBuffer, szItemSlot);
		}
	}
}
// End

BOOL CFSGameUserSkill::SendSkillListInventory( int iType, CFSSkillShop *pSkillShop  )
{
	if( pSkillShop == NULL ) return FALSE;
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return FALSE;
	
	CPacketComposer PacketComposer(S2C_USER_SKILL_INVENTORY_RES);
	
	SSkillInfo SkillInfo;
	int iSkillComp = 1;
	int iaSkill[MAX_SKILL_STORAGE_COUNT];

	if (SKILL_TYPE_SKILL == iType)
	{
		memcpy(iaSkill, pAvatarInfo->Skill.iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	}
	else if (SKILL_TYPE_FREESTYLE == iType)
	{
		memcpy(iaSkill, pAvatarInfo->Skill.iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	}
	else
	{
		return FALSE;
	}

	int iSkillNum = 0; 
	PBYTE pCountLocation = PacketComposer.GetTail();
	PacketComposer.Add(iSkillNum);
	
	for (int iSkillSumIdx = 0; iSkillSumIdx < MAX_SKILL_STORAGE_COUNT; ++iSkillSumIdx)
	{
		for (int i =0 , iSkillComp = 1; i < MAX_SKILL_COUNT; ++i, iSkillComp = iSkillComp << 1)
		{
			if( (iSkillComp & iaSkill[iSkillSumIdx]) == 0 )
				continue;

			if( FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition , iType , i + (iSkillSumIdx*MAX_SKILL_COUNT) , SkillInfo ))
			{
				continue;
			}

			if (SKILL_TYPE_FREESTYLE == iType &&
				SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType &&
				FALSE == m_pUser->IsExistInSlotFreeStylePotentialCard(SkillInfo.iSkillNo))
			{
				continue;
			}

			PacketComposer.Add(iType);
			PacketComposer.Add(SkillInfo.iSkillNo);
			PacketComposer.Add(SkillInfo.iRequiredSkillPoint);
			PacketComposer.Add(SkillInfo.btRecommend);

			int iStatCount = 0;
			PBYTE pStatCountLocation = PacketComposer.GetTail();
			PacketComposer.Add(iStatCount);

			for(int iIdx=0; iIdx<MAX_STAT_NUM; iIdx++)
			{
				if(0 != SkillInfo.iStat[0][iIdx])
				{
					PacketComposer.Add(iIdx);
					PacketComposer.Add(SkillInfo.iStat[0][iIdx]);

					++iStatCount;
				}
			}
			memcpy(pStatCountLocation, &iStatCount, sizeof(iStatCount));
			PacketComposer.Add(SkillInfo.iShopButtonType);

			MakePacketForSpecialtyInfo(PacketComposer, SkillInfo.iSkillNo, SkillInfo.iKind);

			if (SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType)
			{
				int iUpgradeIndex, iUpgradeStep;
				m_pUser->GetPotentialFreeStyleUpgradeInfo(SkillInfo.iSkillNo, iUpgradeIndex, iUpgradeStep);
				PacketComposer.Add((BYTE)iUpgradeIndex);
				PacketComposer.Add((BYTE)iUpgradeStep);
			}

			++iSkillNum;
		}
	}
	
	memcpy(pCountLocation, &iSkillNum, sizeof(iSkillNum));

	m_pUser->Send(&PacketComposer);
		
	return TRUE;
}

BOOL CFSGameUserSkill::SendSkillListSlot( int iType, CFSSkillShop *pSkillShop  )
{
	if( pSkillShop == NULL ) return FALSE;
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return FALSE;
	
	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();
	int iTotalSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iTotalSlotCount > MAX_SKILL_SLOT ) iTotalSlotCount = MAX_SKILL_SLOT;
	
	int iSkillSlotIndex = 0;
	short aRemainDate[MAX_SKILL_SLOT_ITEM];
	
	for(int i =0;i< MAX_SKILL_SLOT_ITEM; i++)
	{
		aRemainDate[i] = -2;
	}
	
	for(int i=0; i < MAX_SKILL_SLOT_ITEM; i++)
	{
		if( pAvatarInfo->Skill.SkillSlot[i].bUse )
		{
			if( pAvatarInfo->Skill.SkillSlot[i].iUseTerm == -1 )
			{
				aRemainDate[iSkillSlotIndex] = -1;
			}
			else
			{
				aRemainDate[iSkillSlotIndex] = GetRemainDate( pAvatarInfo->Skill.SkillSlot[i].EndDate );
			}
			iSkillSlotIndex++;
		}
	}
	
	CPacketComposer PacketComposer(S2C_USER_SKILL_SLOT_RES);
	
	PacketComposer.Add( iTotalSlotCount );
	
	int iSlotItemCount = 0;
	for(int i = 0; i< MAX_SKILL_SLOT_ITEM; i++)
	{
		if(pAvatarInfo->Skill.SkillSlot[i].bUse)
		{
			iSlotItemCount++;
		}
	}
	
	PacketComposer.Add( iSlotItemCount );
	for(int i = 0; i< iSlotItemCount; i++)
	{
		PacketComposer.Add((int)pAvatarInfo->Skill.SkillSlot[i].sSlotCount);
	}
	
	for(int i =0;i < MAX_SKILL_SLOT_ITEM; i++)
	{
		PacketComposer.Add(aRemainDate[i]);
	}

	SSkillInfo SkillInfo;
	int* iaUseSkill = NULL;
	if (SKILL_TYPE_SKILL == iType)
	{
		iaUseSkill = pAvatarInfo->Skill.iaUseSkill;
	}
	else if (SKILL_TYPE_FREESTYLE == iType)
	{
		iaUseSkill = pAvatarInfo->Skill.iaUseFreestyle;
	}
	else 
	{
		return FALSE;
	}
	
	int iUseSkillNum = 0;
	PBYTE pCountLocation = PacketComposer.GetTail();
	PacketComposer.Add(iUseSkillNum);
	
	for(int i=0 ; i< iTotalSlotCount ; i++)
	{
		if( iaUseSkill[i] == -1 )
			continue;

		if( FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition , iType , iaUseSkill[i] , SkillInfo ))
		{
			continue;
		}

		if (SKILL_TYPE_FREESTYLE == iType &&
			SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType &&
			FALSE == m_pUser->IsExistInSlotFreeStylePotentialCard(SkillInfo.iSkillNo))
		{
			continue;
		}
			
		PacketComposer.Add(iType);
		PacketComposer.Add(SkillInfo.iSkillNo);
		PacketComposer.Add(SkillInfo.iRequiredSkillPoint);
		
		int iStatCount = 0;
		PBYTE pStatCountLocation = PacketComposer.GetTail();
		PacketComposer.Add(iStatCount);

		for(int iIdx=0; iIdx<MAX_STAT_NUM; iIdx++)
		{
			if(0 != SkillInfo.iStat[0][iIdx])
			{
				PacketComposer.Add(iIdx);
				PacketComposer.Add(SkillInfo.iStat[0][iIdx]);

				++iStatCount;
			}
		}
		memcpy(pStatCountLocation, &iStatCount, sizeof(iStatCount));
		PacketComposer.Add(SkillInfo.iShopButtonType);

		MakePacketForSpecialtyInfo(PacketComposer, SkillInfo.iSkillNo, SkillInfo.iKind);

		if (SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType)
		{
			int iUpgradeIndex, iUpgradeStep;
			m_pUser->GetPotentialFreeStyleUpgradeInfo(SkillInfo.iSkillNo, iUpgradeIndex, iUpgradeStep);
			PacketComposer.Add((BYTE)iUpgradeIndex);
			PacketComposer.Add((BYTE)iUpgradeStep);
		}

		++iUseSkillNum;
	}
	
	memcpy(pCountLocation, &iUseSkillNum, sizeof(iUseSkillNum));

	m_pUser->Send(&PacketComposer);
	
	return TRUE;
}

BOOL CFSGameUserSkill::CheckSkillSlotCondition(CFSSkillShop *pSkillShop, int iType, int iSkillNo, int& iConditionType, int& iConditionSkillNo)
{
	BOOL bRet = FALSE;
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo || NULL == pSkillShop ) return FALSE;

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	// 20080401 Add Skill Slot
	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	// End
	//
	SSkillInfo SkillInfo;
	
	if( FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition , iType , iSkillNo , SkillInfo ))
	{
		return FALSE;
	}
	
	iConditionSkillNo = static_cast<short>(SkillInfo.iCondition0 > -1 ? SkillInfo.iCondition0 : SkillInfo.iCondition1);
	
	if( iConditionSkillNo == -1 )
	{
		bRet = TRUE;
	}
	else
	{
		iConditionType = SKILL_TYPE_SKILL;
		if(SkillInfo.iCondition0 == -1)
			iConditionType = SKILL_TYPE_FREESTYLE;

		int* iaUseSkill = iConditionType == SKILL_TYPE_SKILL ? pAvatarInfo->Skill.iaUseSkill : pAvatarInfo->Skill.iaUseFreestyle;
		for( int i = 0 ; i < iSlotCount ; ++i )
		{
			if ( iaUseSkill[i] == iConditionSkillNo )
			{
				bRet = TRUE;
				break;
			}
		}
	}
	
	return bRet;
}

BOOL CFSGameUserSkill::CheckSkillBuyCondition(CFSSkillShop* pSkillShop, int iCategory, int iSkillNo, int iType, int &iConditionSkillNo)
{
	CHECK_NULL_POINTER_BOOL(pSkillShop);

	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);
	
	if (SKILL_TYPE_SPECIALTY == iCategory)
	{
		return pAvatarInfo->Skill.IsOwned(iSkillNo, iType);
	}
	else
	{
		BOOL bRet = FALSE;
		SSkillInfo SkillInfo;
	
		if(FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, iType, iSkillNo, SkillInfo, iCategory))
		{
			return FALSE;
		}
	
		iConditionSkillNo = SkillInfo.iCondition0;
	
		if(-1 != iConditionSkillNo)
		{
			if(32 > iConditionSkillNo)
			{
				if(((pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_0] >> iConditionSkillNo) & 1) == 0)
				{
					bRet = FALSE;
				}
				else
				{
					bRet = TRUE;
				}
			}
			else if(64 > iConditionSkillNo)
			{
				if(((pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_1] >> iConditionSkillNo) & 1) == 0)
				{
					bRet = FALSE;
				}
				else
				{
					bRet = TRUE;
				}
			}
			else if(96 > iConditionSkillNo)
			{
				if(((pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_2] >> iConditionSkillNo) & 1) == 0)
				{
					bRet = FALSE;
				}
				else
				{
					bRet = TRUE;
				}
			}
			else if(128 > iConditionSkillNo)
			{
				if(((pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_3] >> iConditionSkillNo) & 1) == 0)
				{
					bRet = FALSE;
				}
				else
				{
					bRet = TRUE;
				}
			}
			else if(160 > iConditionSkillNo)
			{
				if(((pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_4] >> iConditionSkillNo) & 1) == 0)
				{
					bRet = FALSE;
				}
				else
				{
					bRet = TRUE;
				}
			}
		}
		else
		{
			bRet = TRUE;
		}
	
		return bRet;
	}

	return FALSE;
}

bool CFSGameUserSkill::CheckSkillSlotExpired()
{
	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC )
	{
		return false;
	}
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		return false;
	}

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();
	BYTE bSlotCount = pAvatarInfo->Skill.iMaxSkillSlotNum + iBonusSkillSlotCount;
	int iErrorCode = 0;
	
	// 20080401 Add Skill Slot
	for(int i=0;i<MAX_SKILL_SLOT_ITEM;i++)
	{
		if( pAvatarInfo->Skill.SkillSlot[i].bUse &&  pAvatarInfo->Skill.SkillSlot[i].iUseTerm != -1)
		{
			SYSTEMTIME SystemTime;
			::GetLocalTime(&SystemTime);
			
			if( (pAvatarInfo->Skill.SkillSlot[i].EndDate.iYear < SystemTime.wYear) ||
				( pAvatarInfo->Skill.SkillSlot[i].EndDate.iYear == SystemTime.wYear &&  pAvatarInfo->Skill.SkillSlot[i].EndDate.iMonth < SystemTime.wMonth ) ||
				( pAvatarInfo->Skill.SkillSlot[i].EndDate.iYear == SystemTime.wYear &&  pAvatarInfo->Skill.SkillSlot[i].EndDate.iMonth == SystemTime.wMonth && pAvatarInfo->Skill.SkillSlot[i].EndDate.iDay < SystemTime.wDay ) ||
				( pAvatarInfo->Skill.SkillSlot[i].EndDate.iYear == SystemTime.wYear &&  pAvatarInfo->Skill.SkillSlot[i].EndDate.iMonth == SystemTime.wMonth && pAvatarInfo->Skill.SkillSlot[i].EndDate.iDay == SystemTime.wDay &&  pAvatarInfo->Skill.SkillSlot[i].EndDate.iHour < SystemTime.wHour ) ||
				( pAvatarInfo->Skill.SkillSlot[i].EndDate.iYear == SystemTime.wYear &&  pAvatarInfo->Skill.SkillSlot[i].EndDate.iMonth == SystemTime.wMonth && pAvatarInfo->Skill.SkillSlot[i].EndDate.iDay == SystemTime.wDay &&  pAvatarInfo->Skill.SkillSlot[i].EndDate.iHour == SystemTime.wHour && pAvatarInfo->Skill.SkillSlot[i].EndDate.iMin < SystemTime.wMinute) )
			{

				////// 사용기간 지났다
				/// 슬롯에 있던거 인벤토리로 보내자 
								
				InsertToInventoryAllSkill( iErrorCode , 1 , pAvatarInfo->Skill.iMaxSkillSlotNum + iBonusSkillSlotCount);
				InsertToInventoryAllFreestyle(iErrorCode , 1 , pAvatarInfo->Skill.iMaxFreestyleSlotNum + iBonusSkillSlotCount);
				
				if( ODBC_RETURN_SUCCESS == pFSODBC->spExpireSkillSlot( m_pUser->GetUserIDIndex(), pAvatarInfo->iGameIDIndex, i ))
				{
					pAvatarInfo->Skill.SkillSlot[i].Reset();					
				}
			}
			else
			{
				bSlotCount = bSlotCount + pAvatarInfo->Skill.SkillSlot[i].sSlotCount;
			}			
		}
		else
		{
			bSlotCount = bSlotCount + pAvatarInfo->Skill.SkillSlot[i].sSlotCount;
		}		
	}

	InsertToInventoryAllSkill( iErrorCode, 1, bSlotCount );
	InsertToInventoryAllFreestyle( iErrorCode, 1, bSlotCount );

	CheckSlotOverSkill(pFSODBC);
	
	return true;
}


//////// 스킬슬롯 개수를 넘어서는 장착된 스킬을 제거한다
bool CFSGameUserSkill::CheckSlotOverSkill(CFSGameODBC *pFSODBC/* = NULL*/)
{
	if( NULL == pFSODBC )
	{
		pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
		CHECK_NULL_POINTER_BOOL(pFSODBC);
	}
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		return false;
	}

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();
	// 20080401 Add Skill Slot
	int iSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	// End 
	
	int iErrorCode = 0;
	
	InsertToInventoryAllSkill( iErrorCode , 1 , iSlotCount );
	InsertToInventoryAllFreestyle( iErrorCode , 1 , iSlotCount );
	
	
	return true;
}


short CFSGameUserSkill::GetRemainDate(SDateInfo &DateInfo)
{
	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);
	
	
	SYSTEMTIME ExpireTime;
	
	ExpireTime.wYear	=	DateInfo.iYear;
	ExpireTime.wMonth	=	DateInfo.iMonth;
	ExpireTime.wDay		=	DateInfo.iDay;
	ExpireTime.wHour	=	DateInfo.iHour;// 20060406 727 Expired Character Slot Bug
	ExpireTime.wMinute	=	0;
	ExpireTime.wDayOfWeek	=	0;
	ExpireTime.wMinute	=	0;
	ExpireTime.wSecond	=	0;
	ExpireTime.wMilliseconds = 0;
	
	
	
	// 20060406 727 Expired Character Slot Bug
	
    CTime ct1(SystemTime);
    CTime ct2(ExpireTime);
	
    CTimeSpan TimeSpan = ct2 - ct1;
	
	return (short) TimeSpan.GetTotalHours() + 1;// 20060407 729 Expired Character Slot Bug
}


void CFSGameUserSkill::LoadSkillForWCG()
{
	if( false == CFSGameServer::IsWCGServer() ) return;
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) 
	{
		return;
	}
	
	switch( pAvatarInfo->Status.iGamePosition )
	{
	case 0:
		{
			memset( pAvatarInfo->Skill.iaUseSkill , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseSkillType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_0] = 1611599560;
			
			memset( pAvatarInfo->Skill.iaUseFreestyle , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseFreestyleType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_0] = 302237195;
		}
		break;
	case 3:
		{
			memset( pAvatarInfo->Skill.iaUseSkill , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseSkillType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_0] = 806293080;
			
			memset( pAvatarInfo->Skill.iaUseFreestyle , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseFreestyleType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_0] = 607914548;
		}
		break;
	case 4:
		{
			memset( pAvatarInfo->Skill.iaUseSkill , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseSkillType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_0] = 101395538;
			
			memset( pAvatarInfo->Skill.iaUseFreestyle , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseFreestyleType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_0] = 607914548;
		}
		break;
	case 5:
		{
			memset( pAvatarInfo->Skill.iaUseSkill , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseSkillType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_0] = 218849600;
			
			memset( pAvatarInfo->Skill.iaUseFreestyle , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseFreestyleType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_0] = 1237594560;
		}
		break;
	case 6:
		{
			memset( pAvatarInfo->Skill.iaUseSkill , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseSkillType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaSkillStock[SKILL_STORAGE_0] = 51061062;
			
			memset( pAvatarInfo->Skill.iaUseFreestyle , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.iaUseFreestyleType , -1 , sizeof(int) * MAX_SKILL_SLOT );
			memset( pAvatarInfo->Skill.szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
			
			pAvatarInfo->Skill.iaFreestyleStock[SKILL_STORAGE_0] = 1237594560;
		}
		break;
	}
	
	
	/// 센터 1611599560
	// 	      302237195
	
	
	/// 파포 806293080
	//		 607914548
	
	
	/// 스포 101395538
	//		 607914548
	
	
	
	/// 포가 218849600
	//	 	 1237594560
	
	
	
	/// 슈가 51061062
	//		 1237594560
	
}
//

// 20080213 Create Extra Chracter
BOOL CFSGameUserSkill::ApplyBagSkill(CFSSkillShop* pSkillShop, int iSkillKind, short nSkillCnt, int* iaUseSkill, short nFreeStyleCnt, int* iaUseFreeStyle)
{
	CFSGameODBC* pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC || NULL == pSkillShop || NULL == m_pUser) return FALSE;
	
	SAvatarInfo * pAvatar =  m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar ) return FALSE;
	
	int iTempSkill;
	int iaUseSkillType[MAX_SKILL_SLOT];
	memset(iaUseSkillType, -1, sizeof(iaUseSkillType));
	
	int iPosition = pAvatar->Status.iGamePosition;
	
	int iUsedSkillPoint = 0;
	int iaSkillStock[MAX_SKILL_STORAGE_COUNT] = {0,};

	if( iSkillKind == SKILL_TYPE_SKILL )
	{
		memcpy(iaSkillStock, pAvatar->Skill.iaSkill, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

		iUsedSkillPoint = iUsedSkillPoint + m_pUser->GetCurAvatarUsedSkillPoint(SKILL_TYPE_FREESTYLE);
	}
	else if( iSkillKind == SKILL_TYPE_FREESTYLE )
	{
		memcpy(iaSkillStock, pAvatar->Skill.iaFreestyle, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

		iUsedSkillPoint = iUsedSkillPoint + m_pUser->GetCurAvatarUsedSkillPoint(SKILL_TYPE_SKILL);
	}
	else
	{
		return FALSE;
	}

	int iaStockCheck[MAX_SKILL_STORAGE_COUNT] = {0,};
	for (int i = 0; i < MAX_SKILL_STORAGE_COUNT; ++i)
	{
		iaStockCheck[i] = ~iaSkillStock[i];
	}
	
	int iaDuplicationCheck[MAX_SKILL_STORAGE_COUNT] = {0,};

	for(int i=0; i<nSkillCnt; ++i)
	{
		SSkillInfo SkillInfo;
		if( FALSE == pSkillShop->GetSkill(iPosition, iSkillKind , iaUseSkill[i] , SkillInfo ))
		{
			return FALSE;
		}

		// skillcondition
		if( SkillInfo.iCondition0 != -1 )
		{
			int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();
			int iSlotCount = pAvatar->GetTotalSkillSlotCount( iBonusSkillSlotCount );

			BOOL bExistCondition = FALSE;
			for(int j=0; j<iSlotCount; j++)
			{
				if( pAvatar->Skill.iaUseSkill[j] == SkillInfo.iCondition0 ||
					(SKILL_TYPE_SKILL == iSkillKind && iaUseSkill[j] == SkillInfo.iCondition0))
				{
					bExistCondition = TRUE;
					break;
				}
			}

			if(!bExistCondition)
				return FALSE;
		}

		// overlap command
		list<int>  listOverlapCommandSkillNo;
		if( pSkillShop->GetOverLapCommandSkillNo(SkillInfo.iSkillNo, SkillInfo.iKind, listOverlapCommandSkillNo) &&
			false == listOverlapCommandSkillNo.empty() )
		{
			BOOL bOverLapCollision = FALSE;
			for( list<int>::iterator iter = listOverlapCommandSkillNo.begin() ;
				iter != listOverlapCommandSkillNo.end() ; ++iter )
			{
				int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();
				int iSlotCount = pAvatar->GetTotalSkillSlotCount( iBonusSkillSlotCount );

				int iOverlapCollisionSkillNo = (*iter);
				for(int j=0; j<iSlotCount; j++)
				{
					if( iOverlapCollisionSkillNo == iaUseSkill[j] ) // ?
					{
						bOverLapCollision = TRUE;
						break;
					}
				}
			}

			if( TRUE == bOverLapCollision ) 
				return FALSE;
		}
		
		if( iaUseSkill[i] < MAX_SKILL_COUNT )
		{
			iTempSkill = 1 << iaUseSkill[i];
			
			if( (iaStockCheck[SKILL_STORAGE_0] & iTempSkill) != 0 )	return FALSE;
			if( (iaDuplicationCheck[SKILL_STORAGE_0] & iTempSkill) != 0 ) return FALSE;
			
			iaSkillStock[SKILL_STORAGE_0] ^= iTempSkill;
			iaDuplicationCheck[SKILL_STORAGE_0] |= iTempSkill;
		}
		else if( iaUseSkill[i] < MAX_SKILL_COUNT*2 )
		{
			iTempSkill = 1 << (iaUseSkill[i]-MAX_SKILL_COUNT);
			
			if( (iaStockCheck[SKILL_STORAGE_1] & iTempSkill) != 0 )	return FALSE;
			if( (iaDuplicationCheck[SKILL_STORAGE_1] & iTempSkill) != 0 ) return FALSE;
			
			iaSkillStock[SKILL_STORAGE_1] ^= iTempSkill;
			iaDuplicationCheck[SKILL_STORAGE_1] |= iTempSkill;
		}
		else if( iaUseSkill[i] < MAX_SKILL_COUNT*3 )
		{
			iTempSkill = 1 << (iaUseSkill[i]-MAX_SKILL_COUNT*2);
			
			if( (iaStockCheck[SKILL_STORAGE_2] & iTempSkill) != 0 )	return FALSE;
			if( (iaDuplicationCheck[SKILL_STORAGE_2] & iTempSkill) != 0 ) return FALSE;
			
			iaSkillStock[SKILL_STORAGE_2] ^= iTempSkill;
			iaDuplicationCheck[SKILL_STORAGE_2] |= iTempSkill;
		}
		else if( iaUseSkill[i] < MAX_SKILL_COUNT*4 )
		{
			iTempSkill = 1 << (iaUseSkill[i]-MAX_SKILL_COUNT*3);
			
			if( (iaStockCheck[SKILL_STORAGE_3] & iTempSkill) != 0 )	return FALSE;
			if( (iaDuplicationCheck[SKILL_STORAGE_3] & iTempSkill) != 0 ) return FALSE;
			
			iaSkillStock[SKILL_STORAGE_3] ^= iTempSkill;
			iaDuplicationCheck[SKILL_STORAGE_3] |= iTempSkill;
		}
		else if( iaUseSkill[i] < MAX_SKILL_COUNT*5 )
		{
			iTempSkill = 1 << (iaUseSkill[i]-MAX_SKILL_COUNT*4);

			if( (iaStockCheck[SKILL_STORAGE_4] & iTempSkill) != 0 )	return FALSE;
			if( (iaDuplicationCheck[SKILL_STORAGE_4] & iTempSkill) != 0 ) return FALSE;

			iaSkillStock[SKILL_STORAGE_4] ^= iTempSkill;
			iaDuplicationCheck[SKILL_STORAGE_4] |= iTempSkill;
		}
		
		iaUseSkillType[i] = SkillInfo.iType;
		
		iUsedSkillPoint = iUsedSkillPoint + SkillInfo.iRequiredSkillPoint;
	}

	// TODO 리팩토링 필요
	if(SKILL_TYPE_SKILL == iSkillKind)
	{
		for(int i=0; i<nFreeStyleCnt; i++)
		{
			SSkillInfo SkillInfo;
			if( FALSE == pSkillShop->GetSkill(iPosition, SKILL_TYPE_FREESTYLE, iaUseFreeStyle[i], SkillInfo))
			{
				continue;;
			}

			if(SkillInfo.iCondition0 != -1)
			{
				BOOL bExistCondition = FALSE;
				for(int j=0; j<nSkillCnt; ++j)
				{
					if(iaUseSkill[j] == SkillInfo.iCondition0)
					{
						bExistCondition = TRUE;
					}
				}

				if(!bExistCondition)
					return FALSE;
			}
		}
	}

	if(iUsedSkillPoint > m_pUser->GetCurAvatarTotalSkillPoint())
	{
		return FALSE;
	}
	
	char  szUseSkill[MAX_SKILL_STOCK_LEGNTH+1];
	memset(szUseSkill , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	
	ConvertItemSlotToString(szUseSkill, _countof(szUseSkill), iaUseSkill, iaUseSkillType);
	
	if( iSkillKind == SKILL_TYPE_SKILL )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserSkill(pAvatar->iGameIDIndex, szUseSkill ) )
		{
			return FALSE;
		}
		
		memcpy(pAvatar->Skill.iaSkillStock, iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
		memcpy( pAvatar->Skill.iaUseSkill , iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
		memcpy( pAvatar->Skill.iaUseSkillType , iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );
		memcpy( pAvatar->Skill.szUseSkill , szUseSkill , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );

		m_pUser->SendSkillTakeEnable();
	}
	else if( iSkillKind == SKILL_TYPE_FREESTYLE )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spUpdateUserFreestyle(pAvatar->iGameIDIndex, szUseSkill) )
		{
			return FALSE;
		}
		
		memcpy(pAvatar->Skill.iaFreestyleStock, iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
		memcpy( pAvatar->Skill.iaUseFreestyle , iaUseSkill , sizeof(int) * MAX_SKILL_SLOT );
		memcpy( pAvatar->Skill.iaUseFreestyleType , iaUseSkillType , sizeof(int) * MAX_SKILL_SLOT );
		memcpy( pAvatar->Skill.szUseFreestyle , szUseSkill , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	}

	return TRUE;
}
// End

BOOL CFSGameUserSkill::GetSlotSkillList( CFSSkillShop *pSkillShop, int iSkillType, CPacketComposer& PacketComposer )
{
	CHECK_NULL_POINTER_BOOL(pSkillShop);
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);
	
	BYTE bySkillCnt = 0; 
	int* iaUseSkill;
	
	if( iSkillType == SKILL_TYPE_SKILL )
	{
		iaUseSkill = pAvatarInfo->Skill.iaUseSkill;
	}
	else if( iSkillType == SKILL_TYPE_FREESTYLE )
	{
		iaUseSkill = pAvatarInfo->Skill.iaUseFreestyle;
	}
	else
	{
		return FALSE;
	}

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	int iTotalSlotCount = pAvatarInfo->GetTotalSkillSlotCount( iBonusSkillSlotCount );
	
	if( iTotalSlotCount > MAX_SKILL_SLOT ) iTotalSlotCount = MAX_SKILL_SLOT;
	
	PacketComposer.Add((BYTE)1); //착용
	PBYTE pChangeLoc = PacketComposer.GetTail();
	PacketComposer.Add(bySkillCnt);
	
	SSkillInfo SkillInfo;
	SSkillInfo SkillConditionInfo;

	for(int i=0 ; i< iTotalSlotCount ; i++)
	{
		if( iaUseSkill[i] != -1 )
		{
			if( FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition , iSkillType , iaUseSkill[i] , SkillInfo ))
			{
				continue;
			}

			if (SKILL_TYPE_FREESTYLE == iSkillType &&
				SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType &&
				FALSE == m_pUser->IsExistInSlotFreeStylePotentialCard(SkillInfo.iSkillNo))
			{
				continue;
			}
			
			PacketComposer.Add((BYTE)SkillInfo.iSkillNo);
			PacketComposer.Add((BYTE)SkillInfo.iGameLevel[0]);

			// 클라이언트에선 이 패킷을 받을 때 0을 스킬로쓰고 1을 프리스타일로 씁니다. 
			BYTE btClientSkillKind = iSkillType - 2;

			short shSkillCondition = -1;
			if( SkillInfo.iCondition0 > -1 || SkillInfo.iCondition1 > -1 )
			{
				shSkillCondition = static_cast<short>(SkillInfo.iCondition0 > -1 ? SkillInfo.iCondition0 : SkillInfo.iCondition1);

				if(SkillInfo.iCondition0 > -1)
					btClientSkillKind = 0;
				else if(SkillInfo.iCondition1 > -1)
					btClientSkillKind = 1;
			}

			PacketComposer.Add(btClientSkillKind);
			PacketComposer.Add(shSkillCondition);
			PacketComposer.Add(SkillInfo.iRequiredSkillPoint);
			
			int iStatCount = 0;
			PBYTE pStatCountLocation = PacketComposer.GetTail();
			PacketComposer.Add(iStatCount);

			for(int iIdx=0; iIdx<MAX_STAT_NUM; iIdx++)
			{
				if(0 != SkillInfo.iStat[0][iIdx])
				{
					PacketComposer.Add(iIdx);
					PacketComposer.Add(SkillInfo.iStat[0][iIdx]);

					++iStatCount;
				}
			}
			memcpy(pStatCountLocation, &iStatCount, sizeof(iStatCount));
			PacketComposer.Add(SkillInfo.iShopButtonType);

			MakePacketForSpecialtyInfo(PacketComposer, SkillInfo.iSkillNo, SkillInfo.iKind);
			
			if (SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType)
			{
				int iUpgradeIndex, iUpgradeStep;
				m_pUser->GetPotentialFreeStyleUpgradeInfo(SkillInfo.iSkillNo, iUpgradeIndex, iUpgradeStep);
				PacketComposer.Add((BYTE)iUpgradeIndex);
				PacketComposer.Add((BYTE)iUpgradeStep);
			}

			//** OverlapCommand Skill
			BYTE btOverLapCount = 0;
			PBYTE pOverLapCountLocation = PacketComposer.GetTail();
			PacketComposer.Add(btOverLapCount);

			list<int> listOverLapCommandSkillNo;
			if( pSkillShop->GetOverLapCommandSkillNo(SkillInfo.iSkillNo, SkillInfo.iKind, listOverLapCommandSkillNo))
			{
				if( false == listOverLapCommandSkillNo.empty() )
				{
					for( list<int>::iterator iter = listOverLapCommandSkillNo.begin() ;
						iter != listOverLapCommandSkillNo.end()  ; ++iter ) 
					{
						int iOverlapConditionSkillNo = (*iter);

						PacketComposer.Add( (BYTE)iOverlapConditionSkillNo);

						btOverLapCount++;
					}

					memcpy(pOverLapCountLocation, &btOverLapCount, sizeof(btOverLapCount) );
				}
			}

			++bySkillCnt;
		}
	}
	
	memcpy(pChangeLoc, &bySkillCnt, sizeof(bySkillCnt) );
	
	return TRUE;
}

BOOL CFSGameUserSkill::GetInventorySkillList( CFSSkillShop *pSkillShop, int iSkillType, CPacketComposer& PacketComposer )
{
	CHECK_NULL_POINTER_BOOL(pSkillShop);
	
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);
	
	BYTE bySkillCnt = 0; 
	int iaSkill[MAX_SKILL_STORAGE_COUNT];

	if (SKILL_TYPE_SKILL == iSkillType)
	{
		memcpy(iaSkill, pAvatarInfo->Skill.iaSkillStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	}
	else if (SKILL_TYPE_FREESTYLE == iSkillType)
	{
		memcpy(iaSkill, pAvatarInfo->Skill.iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	}
	else
	{
		return FALSE;
	}
	
	int iSkillComp = 1;
	
	PacketComposer.Add((BYTE)0); //미착용
	PBYTE pChangeLoc = PacketComposer.GetTail();
	PacketComposer.Add(bySkillCnt);
	
	SSkillInfo SkillInfo;
	SSkillInfo SkillConditionInfo;

	for (int iSkillSumIdx = 0; iSkillSumIdx < MAX_SKILL_STORAGE_COUNT; ++iSkillSumIdx)
	{
		for(int i = 0 , iSkillComp = 1 ; i < MAX_SKILL_COUNT; i++, iSkillComp = iSkillComp << 1)
		{
			if( (iSkillComp & iaSkill[iSkillSumIdx]) == 0 )
				continue;

			if( FALSE == pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition , iSkillType , i + (iSkillSumIdx*MAX_SKILL_COUNT) , SkillInfo ))
			{
				continue;
			}

			if (SKILL_TYPE_FREESTYLE == iSkillType &&
				SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType &&
				FALSE == m_pUser->IsExistInSlotFreeStylePotentialCard(SkillInfo.iSkillNo))
			{
				continue;
			}
			
			PacketComposer.Add((BYTE)SkillInfo.iSkillNo);
			PacketComposer.Add((BYTE)SkillInfo.iGameLevel[0]);

			// 클라이언트에선 이 패킷을 받을 때 0을 스킬로쓰고 1을 프리스타일로 씁니다. 
			BYTE btClientSkillKind = iSkillType - 2;
			
			short shSkillCondition = -1;
			if( SkillInfo.iCondition0 > -1 || SkillInfo.iCondition1 > -1 )
			{
				shSkillCondition = static_cast<short>(SkillInfo.iCondition0 > -1 ? SkillInfo.iCondition0 : SkillInfo.iCondition1);

				if(SkillInfo.iCondition0 > -1)
					btClientSkillKind = 0;
				else if(SkillInfo.iCondition1 > -1)
					btClientSkillKind = 1;
			}

			PacketComposer.Add(btClientSkillKind);
			PacketComposer.Add(shSkillCondition);		
			PacketComposer.Add(SkillInfo.iRequiredSkillPoint);
			
			int iStatCount = 0;
			PBYTE pStatCountLocation = PacketComposer.GetTail();
			PacketComposer.Add(iStatCount);

			for(int iIdx=0; iIdx<MAX_STAT_NUM; iIdx++)
			{
				if(0 != SkillInfo.iStat[0][iIdx])
				{
					PacketComposer.Add(iIdx);
					PacketComposer.Add(SkillInfo.iStat[0][iIdx]);

					++iStatCount;
				}
			}
			memcpy(pStatCountLocation, &iStatCount, sizeof(iStatCount));
			PacketComposer.Add(SkillInfo.iShopButtonType);

			MakePacketForSpecialtyInfo(PacketComposer, SkillInfo.iSkillNo, SkillInfo.iKind);

			if (SKILLSHOP_BUTTON_TYPE_POTENTIAL == SkillInfo.iShopButtonType)
			{
				int iUpgradeIndex, iUpgradeStep;
				m_pUser->GetPotentialFreeStyleUpgradeInfo(SkillInfo.iSkillNo, iUpgradeIndex, iUpgradeStep);
				PacketComposer.Add((BYTE)iUpgradeIndex);
				PacketComposer.Add((BYTE)iUpgradeStep);
			}

			//** OverlapCommand Skill
			BYTE btOverLapCount = 0;
			PBYTE pOverLapCountLocation = PacketComposer.GetTail();
			PacketComposer.Add(btOverLapCount);

			list<int> listOverLapCommandSkillNo;
			if( pSkillShop->GetOverLapCommandSkillNo(SkillInfo.iSkillNo, SkillInfo.iKind, listOverLapCommandSkillNo))
			{
				if( false == listOverLapCommandSkillNo.empty() )
				{
					for( list<int>::iterator iter = listOverLapCommandSkillNo.begin() ;
						iter != listOverLapCommandSkillNo.end()  ; ++iter ) 
					{
						int iOverlapConditionSkillNo = (*iter);

						PacketComposer.Add( (BYTE)iOverlapConditionSkillNo);

						btOverLapCount++;
					}

					memcpy(pOverLapCountLocation, &btOverLapCount, sizeof(btOverLapCount) );
				}
			}

			++bySkillCnt;
		}
	}

	memcpy(pChangeLoc, &bySkillCnt, sizeof(bySkillCnt) );
	
	return TRUE;
}

BOOL CFSGameUserSkill::CheckPayAbiility( int &iSellType, SSkillBase* pSkillBase, int& iSkillPriceCash,
									int& iSkillPricePoint, int& iSkillPriceTrophy, int iSaleCouponCash, int &iErrorCode )
{
	CHECK_NULL_POINTER_BOOL(pSkillBase);

	if( iSellType == BUY_SKILL_SELL_TYPE_CASH && pSkillBase->iBalanceCash != 1 )
	{
		iSellType = SELL_TYPE_CASH;
		iSkillPriceCash = pSkillBase->GetCurrentPrice(BUY_SKILL_SELL_TYPE_CASH);
		iSkillPriceCash -= iSaleCouponCash;
		if(iSkillPriceCash < 0)
			iSkillPriceCash = 0;

		CHECK_CONDITION_RETURN(-1 == iSkillPriceCash, FALSE);

		if( m_pUser->GetCoin() + m_pUser->GetEventCoin() < iSkillPriceCash ) 
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_ENOUGH_CASH;
			return FALSE;
		}
	}
	else if( iSellType == BUY_SKILL_SELL_TYPE_POINT && pSkillBase->iBalancePoint != -1 )
	{
		iSellType = SELL_TYPE_POINT;
		iSkillPricePoint = pSkillBase->GetCurrentPrice(BUY_SKILL_SELL_TYPE_POINT);

		CHECK_CONDITION_RETURN(-1 == iSkillPricePoint, FALSE);

		if( m_pUser->GetSkillPoint() < iSkillPricePoint )
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_ENOUGH_POINT;
			return FALSE;
		}
	}
	else if( iSellType == BUY_SKILL_SELL_TYPE_TROPHY && pSkillBase->iBalanceTrophy != -1)
	{
		iSellType = SELL_TYPE_TROPHY;
		iSkillPriceTrophy = pSkillBase->GetCurrentPrice(BUY_SKILL_SELL_TYPE_TROPHY);

		CHECK_CONDITION_RETURN(-1 == iSkillPriceTrophy, FALSE);

		if( m_pUser->GetUserTrophy() < iSkillPriceTrophy )
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_ENOUGH_TROPHY;
			return FALSE;
		}
	}
	else if( iSellType == BUY_SKILL_SELL_TYPE_TICKET)
	{
		if( !(m_pUser->GetUserItemList()->GetSkillTicketCount()) )
		{
			iErrorCode = BUY_SKILL_ERROR_NOT_ENOUGH_TICKET;
			return FALSE;
		}
	}
	else 
	{
		iErrorCode = BUY_SKILL_ERROR_GENERAL;
		return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUserSkill::GetDBColumnAndShiftBit( SAvatarInfo *pAvatarInfo, int iSkillIndex, int &iStorageType, int &iSkillShiftBit, int iKind )
{
	if(SKILL_TYPE_SKILL == iKind)
	{
		if( iSkillIndex < 32 )
		{
			iSkillShiftBit = (1 << iSkillIndex);
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_0]) )
			{
				return FALSE;
			}
			
			iStorageType = SKILL_STORAGE_0;
		}
		else if( iSkillIndex < 64 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 32));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_1]) )
			{
				return FALSE;
			}
			
			iStorageType = SKILL_STORAGE_1;
		}
		else if( iSkillIndex < 96 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 64));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_2]) )
			{
				return FALSE;
			}

			iStorageType = SKILL_STORAGE_2;
		}
		else if( iSkillIndex < 128 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 96));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_3]) )
			{
				return FALSE;
			}

			iStorageType = SKILL_STORAGE_3;
		}
		else if( iSkillIndex < 160 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 128));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_4]) )
			{
				return FALSE;
			}

			iStorageType = SKILL_STORAGE_4;
		}
	}
	else if(SKILL_TYPE_FREESTYLE == iKind)
	{
		if( iSkillIndex < 32 )
		{
			iSkillShiftBit = (1 << iSkillIndex);
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_0]) )
			{
				return FALSE;
			}
			
			iStorageType = SKILL_STORAGE_0;
		}
		else if( iSkillIndex < 64 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 32));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_1]) )
			{
				return FALSE;
			}
			
			iStorageType = SKILL_STORAGE_1;
		}		
		else if( iSkillIndex < 96 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 64));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_2]) )
			{
				return FALSE;
			}
			
			iStorageType = SKILL_STORAGE_2;
		}
		else if( iSkillIndex < 128 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 96));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_3]) )
			{
				return FALSE;
			}
			
			iStorageType = SKILL_STORAGE_3;
		}
		else if( iSkillIndex < 160 )
		{
			iSkillShiftBit = (1 << (iSkillIndex - 128));
			if( (iSkillShiftBit & pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_4]) )
			{
				return FALSE;
			}

			iStorageType = SKILL_STORAGE_4;
		}
	}

	return TRUE;
}

BOOL CFSGameUserSkill::BuySkillProcess_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)) )
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, INVALED_DATA, CHECK_FAIL, "BuySkillProcess_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}
	
	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSSkillShop *pSkillShop = CFSGameServer::GetInstance()->GetSkillShop();
	CHECK_NULL_POINTER_BOOL( pSkillShop );
	CFSGameODBC *pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_SKILL_ERROR_GENERAL;	
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;
	int iTicketInventoryIndex = 0;
	
	char* szUserID = pBillingInfo->szUserID;
	int iEventCode = pBillingInfo->iEventCode;
	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szGameID = pBillingInfo->szGameID;	
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iSkillIndex = pBillingInfo->iItemCode;
	int iSkillPricePoint = pBillingInfo->iPointChange;
	int iSkillPriceTrophy = pBillingInfo->iTrophyChange;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iSellType = pBillingInfo->iSellType;
	int iSkillPriceCash = pBillingInfo->iCashChange;
	int iPostMoney = iPrevMoney - iSkillPriceCash;
	int iStorageType = pBillingInfo->iParam0;
	int iSkillShiftBit = pBillingInfo->iParam1;
	int iCondition = pBillingInfo->iParam2;
	int iBuySkillKind = pBillingInfo->iParam3;
	BOOL bFameSkill = pBillingInfo->iParam4;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szProductCode = pBillingInfo->szProductCode;
	int iEventIndex = pBillingInfo->iParam5;
	int iTokenIdx = pBillingInfo->iTokenItemIdx;
	int iUpdateTokenCount = 0;
	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	
	if( iPayResult == PAY_RESULT_SUCCESS )
	{			
		switch( iEventCode )
		{
		case EVENT_BUY_SKILL:
			if( ODBC_RETURN_SUCCESS == pGameODBC->spBuySkill( szUserID, iUserIDIndex, iGameIDIndex, iStorageType, iSkillIndex, iSkillShiftBit,
															iSkillPricePoint, iPrevMoney, iPostMoney, iSkillPriceTrophy, szBillingKey, szProductCode, pBillingInfo->iItemPrice, 
															pBillingInfo->iOperationCode, iEventIndex, iTicketInventoryIndex  , iTokenIdx, iUpdateTokenCount) )
			{
				iErrorCode = BUY_SKILL_ERROR_SUCCESS;
				InsertSkill(szGameID, iSkillIndex, iSkillShiftBit, bFameSkill );

				if(iSellType == SELL_TYPE_SKILL_TICKET)
					pUser->GetUserItemList()->RemoveSkillTicket(iTicketInventoryIndex);

				if( iCondition == -1 )
				{
					if( TRUE == pUser->GetUserSkill()->CheckOverLapCommandCondition(pSkillShop, SKILL_TYPE_SKILL, iSkillIndex))
					{
						int iTempErrorCode = 0;
						InsertToSlot( iSkillIndex, pSkillShop, iTempErrorCode );
					}
				}

				if(pBillingInfo->iOperationCode == FS_ITEM_OP_BUY_EVENT_SHOPPINGFESTIVAL)
				{
					pUser->GetUserShoppingFestivalEvent()->RemoveShoppingBasketItem(iEventIndex);
					pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
				}	
				pUser->GetUserShoppingFestivalEvent()->RemoveShoppingBasketItem(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_SKILL, EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_SKILL_KIND_SKILL, iSkillIndex);
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_SKILL, DB_DATA_UPDATE, FAIL, "spBuySkill - SkillCode:10752790, UserID:(null), GameID:", iSkillIndex, m_pUser->GetUserID(), m_pUser->GetGameID());
turn FALSE;				
			}
			break;
		case EVENT_BUY_FREESTYLE:
			if( ODBC_RETURN_SUCCESS == pGameODBC->spBuyFreestyle( szUserID, iUserIDIndex, iGameIDIndex, iStorageType, iSkillIndex, iSkillShiftBit,
															iSkillPricePoint, iPrevMoney, iPostMoney, iSkillPriceTrophy, szBillingKey, szProductCode, pBillingInfo->iItemPrice, 
															pBillingInfo->iOperationCode, iEventIndex, iTicketInventoryIndex, iTokenIdx, iUpdateTokenCount ) )
			{
				iErrorCode = BUY_SKILL_ERROR_SUCCESS;		
				InsertFreestyle(szGameID, iSkillIndex, iSkillShiftBit, bFameSkill );

				if(iSellType == SELL_TYPE_SKILL_TICKET)
					pUser->GetUserItemList()->RemoveSkillTicket(iTicketInventoryIndex);

				if( iCondition == -1 )
				{
					if(	TRUE == pUser->GetUserSkill()->CheckOverLapCommandCondition(pSkillShop, SKILL_TYPE_FREESTYLE, iSkillIndex))
					{			
						int iTempErrorCode = 0;
						InsertToSlotFreestyle( iSkillIndex, pSkillShop, iTempErrorCode );
					}
				}

				if(pBillingInfo->iOperationCode == FS_ITEM_OP_BUY_EVENT_SHOPPINGFESTIVAL)
				{
					pUser->GetUserShoppingFestivalEvent()->RemoveShoppingBasketItem(iEventIndex);
					pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
				}				
				pUser->GetUserShoppingFestivalEvent()->RemoveShoppingBasketItem(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_SKILL, EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_SKILL_KIND_FREESTYLE, iSkillIndex);
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_SKILL, DB_DATA_UPDATE, FAIL, "spBuyFreestyle - SkillCode:10752790, UserID:(null), GameID:", iSkillIndex, m_pUser->GetUserID(), m_pUser->GetGameID());
turn FALSE;
			}
			break;
		default:
			break;
		};
		
		int iPreSkillPoint = pUser->GetSkillPoint();
		int iPreEventCoin = pUser->GetEventCoin();

		pUser->SetPayCash(pBillingInfo->iCashChange);
		if(pBillingInfo->bUseEventCoin == TRUE)
			pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

		pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

		iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
		iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
		if(iEventBonusPoint > 0 || iEventBonusCash > 0)
			iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

		SetUserBillResultAtMem( pUser, iSellType, iPostMoney, iSkillPriceCash, iSkillPricePoint, iSkillPriceTrophy, pBillingInfo->iBonusCoin );	
	}
	else if ( iPayResult == PAY_RESULT_FAIL_CASH )
	{
		iErrorCode = BUY_SKILL_ERROR_BILLING_FAIL;
	}
	
	int iCategory = bFameSkill ? SKILL_TYPE_FAME : iBuySkillKind;

	CPacketComposer PacketComposer(S2C_BUY_SKILL_RES);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iCategory);
	PacketComposer.Add(iBuySkillKind);
	PacketComposer.Add(iSkillIndex);
	PacketComposer.Add((int)0);
	if( iErrorCode == BUY_SKILL_ERROR_SUCCESS_WITH_BONUSPOINT )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	
	if(0 >= iTokenIdx && 0 == iUpdateTokenCount) // 이용권을 사용하지 않음
	{
		iUpdateTokenCount = pUser->GetSkillTokenCount(iCategory);
	}

	PacketComposer.Add(iUpdateTokenCount);

	pClient->Send(&PacketComposer);
	
	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		iErrorCode = 0;
		switch(iEventCode)
		{
		case EVENT_BUY_SKILL:
			{
				ProcessAchievementBuySkill(pGameODBC, pSkillShop, szGameID);
				break;
			}
		case EVENT_BUY_FREESTYLE:
			{
				ProcessAchievementBuyFreeStyle(pGameODBC, pSkillShop, szGameID);
				break;
			}
		default:
			break;
		}

		if( 0 < iTokenIdx )
		{
			CFSGameUserItem* pUserItemList = (CFSGameUserItem*)m_pUser->GetUserItemList();
			if( pUserItemList )
			{
				CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
				if( pAvatarItemList)
				{
					if( 0 == iUpdateTokenCount )
						pAvatarItemList->RemoveItem(iTokenIdx);
					else
						pAvatarItemList->UpdateItemCount(iTokenIdx, iUpdateTokenCount);
				}
			}
		}

		int iTicketcount = pUser->GetUserItemList()->GetSkillTicketCount();
		pUser->SendUserGold(iTicketcount);
		pUser->SendCurAvatarTrophy();
		pUser->SendCurAvatarSkillPoint(pUser->GetCurAvatarRemainSkillPoint(), pUser->GetCurAvatarUsedSkillPointAllType(), pUser->IsPlaying());
	}

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );
}


BOOL CFSGameUserSkill::BuySpecialtySkill_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)) )
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, INVALED_DATA, CHECK_FAIL, "BuySpecialtySkill_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}
	
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSSkillShop *pSkillShop = CFSGameServer::GetInstance()->GetSkillShop();
	CHECK_NULL_POINTER_BOOL( pSkillShop );
	CFSGameODBC *pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_SKILL_ERROR_GENERAL;	
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;
	
	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szGameID = pBillingInfo->szGameID;	
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iSkillIndex = pBillingInfo->iItemCode;
	int iSellType = pBillingInfo->iSellType;
	int iPointCost = pBillingInfo->iPointChange;
	int iTrophyCost = pBillingInfo->iTrophyChange;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int iKind = pBillingInfo->iParam0;
	int iGamePosition = pBillingInfo->iParam1;
	int iLv = pBillingInfo->iParam2;
	int iSpecialtyNo = pBillingInfo->iParam3;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szProductCode = pBillingInfo->szProductCode;
	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	
	if( iPayResult == PAY_RESULT_SUCCESS )
	{			
		if( ODBC_RETURN_SUCCESS == pGameODBC->SKILL_BuySpecialtySkill(iUserIDIndex, iGameIDIndex, iLv, iGamePosition, iSkillIndex, iKind,
											iSpecialtyNo, iPrevCash, iPostCash, iPointCost, iTrophyCost, szBillingKey, szProductCode, pBillingInfo->iItemPrice))
		{
			iErrorCode = BUY_SKILL_ERROR_SUCCESS;

			UpdateSpecialtySkill(pSkillShop, iGamePosition, iSkillIndex, iKind, iSpecialtyNo);
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_SKILL, DB_DATA_UPDATE, FAIL, "SKILL_BuySpecialtySkill - Skill:10752790, Kind:0, UserIDIndex:7106560, GameIDIndex:-858993460", iSkillIndex, iKind, iUserIDIndex, iGameIDIndex );
		
		}

		int iPreSkillPoint = pUser->GetSkillPoint();
		int iPreEventCoin = pUser->GetEventCoin();

		pUser->SetPayCash(pBillingInfo->iCashChange);
		if(pBillingInfo->bUseEventCoin == TRUE)
			pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

		pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

		iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
		iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
		if(iEventBonusPoint > 0 || iEventBonusCash > 0)
			iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

		SetUserBillResultAtMem( pUser, iSellType, iPostCash, pBillingInfo->iCashChange, iPointCost, iTrophyCost, pBillingInfo->iBonusCoin );	
	}
	else if ( iPayResult == PAY_RESULT_FAIL_CASH )
	{
		iErrorCode = BUY_SKILL_ERROR_BILLING_FAIL;
	}
	
	CPacketComposer PacketComposer(S2C_BUY_SKILL_RES);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add((int)SKILL_TYPE_SPECIALTY);
	PacketComposer.Add(iKind);
	PacketComposer.Add(iSkillIndex);
	PacketComposer.Add(iSpecialtyNo);
	if( iErrorCode == BUY_SKILL_ERROR_SUCCESS_WITH_BONUSPOINT )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add((int)0); // token

	pUser->Send(&PacketComposer);
	
	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		int iTicketcount = pUser->GetUserItemList()->GetSkillTicketCount();
		pUser->SendUserGold(iTicketcount);
		pUser->SendCurAvatarTrophy();
	}

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );
}

void CFSGameUserSkill::SetUserBillResultAtMem( CFSGameUser* pUser, int iSellType, int iPostMoney, int iSkillPriceCash, int iSkillPricePoint,
											 int iSkillPriceTrophy, int iBonusCash )
{
	CHECK_NULL_POINTER_VOID( pUser );

	int iUseCoin = 0;
	int	iUsePoint = 0;

	if( SELL_TYPE_CASH == iSellType )
	{
		iUseCoin = pUser->GetCoin() - iPostMoney;

		pUser->SetCoin( iPostMoney );
		pUser->SetBonusCoin( iBonusCash );
	}
	else if( SELL_TYPE_POINT == iSellType )
	{
		iUsePoint = iSkillPricePoint;

		pUser->SetSkillPoint( pUser->GetSkillPoint() - iSkillPricePoint );			
	}
	else if( SELL_TYPE_TROPHY == iSellType )
	{
		pUser->SetUserTrophy( pUser->GetUserTrophy() - iSkillPriceTrophy );
	}

	if(SELL_TYPE_CASH == iSellType ||
		SELL_TYPE_POINT == iSellType)
	{
		if(iUsePoint > 0)
		{
			pUser->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_POINT_USE, iUsePoint);
			pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_POINT, iUsePoint);
			pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_POINT, iUsePoint);
			pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POINT_ITEM);
			pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_USEPOINT, iUsePoint);
			pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_POINT, iUsePoint);
			pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USE_POINT, iUsePoint);
		}

		if(iUseCoin > 0)
		{
			pUser->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_USE_CASH, iUseCoin);
			pUser->GetUserPotionMakingEvent()->CheckMission(SEVENT_POTION_MAKING_MISSION_TYPE_USE_CASH, iUseCoin);	
			pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_CASH);
		}
	}
}

BOOL CFSGameUserSkill::BuySkillProcess_BeforePay( SAvatarInfo *pAvatarInfo, SSkillInfo &SkillInfo, int &iErrorCode )
{
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);

	int iUserCheckLv = 0;

	if(FALSE == m_pUser->IsActiveLvIntervalBuffItem(ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF_SKILL, iUserCheckLv))
	{
		// 스킬훔치기 아이템 적용중이 아니면 원래 레벨로 체크.
		iUserCheckLv = pAvatarInfo->iLv;
	}

	if(SkillInfo.iGameLevel[0] > iUserCheckLv)
	{
		iErrorCode = BUY_SKILL_ERROR_MISMATCH_LEVEL;
		return FALSE;
	}

	if(SkillInfo.iFameLevel[0] > pAvatarInfo->iFameLevel)
	{
		iErrorCode = BUY_SKILL_ERROR_GENERAL;
		return FALSE;
	}

	if(SkillInfo.iGamePosition[0] != pAvatarInfo->Status.iGamePosition)
	{
		iErrorCode = BUY_SKILL_ERROR_MISMATCH_POSITION;
		return FALSE;
	}
	
	if( SkillInfo.iKind == SKILL_TYPE_FREESTYLE && SkillInfo.iCondition0 != -1 )
	{
		int iCondition = GET_FS_CONDITION0(pAvatarInfo->Status.iHeight,pAvatarInfo->Status.iStatJump); 
		if( SkillInfo.iCondition0 > iCondition )
		{
			return FALSE;
		}		
	}

	return TRUE;
}

int CFSGameUserSkill::GetUsedSkillPointAllType()
{
	int iUsedSkillPoint = 0;

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(NULL != pServer)
	{
		CFSSkillShop* pShop = pServer->GetSkillShop();
		if(NULL != pShop)
		{
			SAvatarInfo* pAvatarInfo =  m_pUser->GetCurUsedAvatar();
			if(NULL != pAvatarInfo)
			{
				for(int i=0; i<MAX_SKILL_SLOT; i++)
				{
					if(-1 < pAvatarInfo->Skill.iaUseSkill[i])
					{
						SSkillInfo SkillInfo;
						if(TRUE == pShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, pAvatarInfo->Skill.iaUseSkill[i], SkillInfo))
						{
							if(0 < SkillInfo.iRequiredSkillPoint)
							{
								iUsedSkillPoint = iUsedSkillPoint + SkillInfo.iRequiredSkillPoint;
							}
						}
					}
					if(-1 < pAvatarInfo->Skill.iaUseFreestyle[i])
					{
						SSkillInfo SkillInfo;
						if(TRUE == pShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, pAvatarInfo->Skill.iaUseFreestyle[i], SkillInfo))
						{
							if(0 < SkillInfo.iRequiredSkillPoint)
							{
								iUsedSkillPoint = iUsedSkillPoint + SkillInfo.iRequiredSkillPoint;
							}
						}
					}
				}
			}
		}
	}

	return iUsedSkillPoint;
}

int CFSGameUserSkill::GetUsedSkillPoint(int iType)
{
	int iUsedSkillPoint = 0;

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(NULL != pServer)
	{
		CFSSkillShop* pShop = pServer->GetSkillShop();
		if(NULL != pShop)
		{
			SAvatarInfo* pAvatarInfo =  m_pUser->GetCurUsedAvatar();
			if(NULL != pAvatarInfo)
			{
				int* iaUseSkill = NULL;
				if(SKILL_TYPE_SKILL == iType)
				{
					iaUseSkill = pAvatarInfo->Skill.iaUseSkill;
				}
				else if(SKILL_TYPE_FREESTYLE == iType)
				{
					iaUseSkill = pAvatarInfo->Skill.iaUseFreestyle;
				}
				else
				{
					return iUsedSkillPoint;
				}

				for(int i=0; i<MAX_SKILL_SLOT; i++)
				{
					if(-1 < iaUseSkill[i])
					{
						SSkillInfo SkillInfo;
						if(TRUE == pShop->GetSkill(pAvatarInfo->Status.iGamePosition, iType, iaUseSkill[i], SkillInfo))
						{
							if(0 < SkillInfo.iRequiredSkillPoint)
							{
								iUsedSkillPoint = iUsedSkillPoint + SkillInfo.iRequiredSkillPoint;
							}
						}
					}
				}
			}
		}
	}

	return iUsedSkillPoint;
}

int CFSGameUserSkill::GetFameSkillStatus(int* iStat)
{
	BOOL bRet = FALSE;

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(NULL != pServer)
	{
		CFSSkillShop* pShop = pServer->GetSkillShop();
		if(NULL != pShop)
		{
			SAvatarInfo* pAvatarInfo =  m_pUser->GetCurUsedAvatar();
			if(NULL != pAvatarInfo)
			{
				for(int i=0; i<MAX_SKILL_SLOT; i++)
				{
					if(-1 < pAvatarInfo->Skill.iaUseSkill[i])
					{
						SSkillInfo SkillInfo;
						if(TRUE == pShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, pAvatarInfo->Skill.iaUseSkill[i], SkillInfo))
						{
							for(int j=0; j<MAX_STAT_NUM; j++)
							{
								iStat[j] = iStat[j] + SkillInfo.iStat[0][j];
							}
						}
					}
					if(-1 < pAvatarInfo->Skill.iaUseFreestyle[i])
					{
						SSkillInfo SkillInfo;
						if(TRUE == pShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, pAvatarInfo->Skill.iaUseFreestyle[i], SkillInfo))
						{
							for(int j=0; j<MAX_STAT_NUM; j++)
							{
								iStat[j] = iStat[j] + SkillInfo.iStat[0][j];
							}
						}
					}
				}

				bRet = TRUE;
			}
		}
	}

	return bRet;
}

int CFSGameUserSkill::GetFameSkillAddMinusStatus(int* iAddStat, int* iMinusStat)
{
	BOOL bRet = FALSE;

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	if(NULL != pServer)
	{
		CFSSkillShop* pShop = pServer->GetSkillShop();
		if(NULL != pShop)
		{
			SAvatarInfo* pAvatarInfo =  m_pUser->GetCurUsedAvatar();
			if(NULL != pAvatarInfo)
			{
				for(int i=0; i<MAX_SKILL_SLOT; i++)
				{
					if(-1 < pAvatarInfo->Skill.iaUseSkill[i])
					{
						SSkillInfo SkillInfo;
						if(TRUE == pShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, pAvatarInfo->Skill.iaUseSkill[i], SkillInfo))
						{
							for(int j=0; j<MAX_STAT_NUM; j++)
							{
								if(0 <= SkillInfo.iStat[0][j])
								{
									iAddStat[j] = iAddStat[j] + SkillInfo.iStat[0][j];
								}
								else
								{
									iMinusStat[j] = iMinusStat[j] + SkillInfo.iStat[0][j];
								}
							}
						}
					}
					if(-1 < pAvatarInfo->Skill.iaUseFreestyle[i])
					{
						SSkillInfo SkillInfo;
						if(TRUE == pShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, pAvatarInfo->Skill.iaUseFreestyle[i], SkillInfo))
						{
							for(int j=0; j<MAX_STAT_NUM; j++)
							{
								if(0 <= SkillInfo.iStat[0][j])
								{
									iAddStat[j] = iAddStat[j] + SkillInfo.iStat[0][j];
								}
								else
								{
									iMinusStat[j] = iMinusStat[j] + SkillInfo.iStat[0][j];
								}
							}
						}
					}
				}

				bRet = TRUE;
			}
		}
	}

	return bRet;
}

void CFSGameUserSkill::ProcessAchievementBuySkill( CFSGameODBC *pFSODBC, CFSSkillShop *pSkillShop, char *szGameID)
{
	int iShiftBitBase = 1;
	int iNormalSkillCount = 0;
	int iFameSkillCount = 0;

	CLock lock(&m_SyncObject);

	SAvatarInfo * pAvatarInfo =  m_pUser->GetAvatarInfoWithGameID(szGameID);
	if( NULL == pFSODBC || NULL == pSkillShop || NULL == pAvatarInfo ) return;

	for (int iLoopIndex = 0; iLoopIndex < MAX_SKILL_COUNT; iLoopIndex++)
	{
		if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_0] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameSkillCount++;						
			}
			else
			{
				iNormalSkillCount++;
			}
		}

		if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_1] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 32, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameSkillCount++;						
			}
			else
			{
				iNormalSkillCount++;
			}
		}

		if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_2] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 64, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameSkillCount++;						
			}
			else
			{
				iNormalSkillCount++;
			}
		}

		if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_3] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 96, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameSkillCount++;						
			}
			else
			{
				iNormalSkillCount++;
			}
		}

		if (pAvatarInfo->Skill.iaSkill[SKILL_STORAGE_4] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_SKILL, iLoopIndex + 128, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameSkillCount++;						
			}
			else
			{
				iNormalSkillCount++;
			}
		}

		iShiftBitBase = iShiftBitBase << 1;
	}

	vector<int> vSkillCounts;
	int iTotalSkillCount = iNormalSkillCount + iFameSkillCount;
	vSkillCounts.push_back(iTotalSkillCount);
	vSkillCounts.push_back(m_pUser->GetCurUsedAvatarPosition());
	m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_SKILL, ACHIEVEMENT_LOG_GROUP_SHOP, vSkillCounts, CFSGameServer::GetInstance()->GetSkillShop());
}

void CFSGameUserSkill::ProcessAchievementBuyFreeStyle( CFSGameODBC *pFSODBC, CFSSkillShop *pSkillShop, char *szGameID)
{
	int iShiftBitBase = 1;
	int iNormalFreeStyleCount = 0;
	int iFameFreeStyleCount = 0;

	CLock lock(&m_SyncObject);

	SAvatarInfo * pAvatarInfo =  m_pUser->GetAvatarInfoWithGameID(szGameID);
	if( NULL == pFSODBC || NULL == pSkillShop || NULL == pAvatarInfo ) return;

	for (int iLoopIndex = 0; iLoopIndex < MAX_SKILL_COUNT; iLoopIndex++)
	{
		if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_0] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameFreeStyleCount++;
			}
			else
			{
				if( SKILLSHOP_BUTTON_TYPE_POTENTIAL != sSkillinfo.iShopButtonType)
					iNormalFreeStyleCount++;
			}
		}

		if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_1] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 32, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameFreeStyleCount++;
			}
			else
			{
				if( SKILLSHOP_BUTTON_TYPE_POTENTIAL != sSkillinfo.iShopButtonType)
					iNormalFreeStyleCount++;
			}
		}

		if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_2] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 64, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameFreeStyleCount++;
			}
			else
			{
				if( SKILLSHOP_BUTTON_TYPE_POTENTIAL != sSkillinfo.iShopButtonType)
					iNormalFreeStyleCount++;
			}
		}

		if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_3] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 96, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameFreeStyleCount++;
			}
			else
			{
				if( SKILLSHOP_BUTTON_TYPE_POTENTIAL != sSkillinfo.iShopButtonType)
					iNormalFreeStyleCount++;
			}
		}

		if (pAvatarInfo->Skill.iaFreestyle[SKILL_STORAGE_4] & iShiftBitBase)
		{
			SSkillInfo sSkillinfo;
			pSkillShop->GetSkill(pAvatarInfo->Status.iGamePosition, SKILL_TYPE_FREESTYLE, iLoopIndex + 128, sSkillinfo);

			if ((sSkillinfo.iGamePosition[0] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[0] > 0) || (sSkillinfo.iGamePosition[1] == pAvatarInfo->Status.iGamePosition && sSkillinfo.iFameLevel[1] > 0))
			{
				iFameFreeStyleCount++;
			}
			else
			{
				iNormalFreeStyleCount++;
			}
		}

		iShiftBitBase = iShiftBitBase << 1;
	}

	vector<int> vFreeStyleCounts;
	int iTotalFreeStyleCount = iNormalFreeStyleCount + iFameFreeStyleCount;
	vFreeStyleCounts.push_back(iTotalFreeStyleCount);
	vFreeStyleCounts.push_back(m_pUser->GetCurUsedAvatarPosition());
	m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_FREESTYLE, ACHIEVEMENT_LOG_GROUP_SHOP, vFreeStyleCounts, CFSGameServer::GetInstance()->GetSkillShop());
}

void CFSGameUserSkill::ProcessAchievementBuyTraining()
{
	CLock lock(&m_SyncObject);

	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pFSODBC );

	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	if (NULL == pFSODBC || NULL == pAvatarInfo)
	{
		return;
	}

	vector<int> vTraingCounts;
	for (int iLoopIndex = 0; iLoopIndex < PARAMETER_INDEX_TRAINING_TOTAL_NUM; iLoopIndex++)
	{
		vTraingCounts.push_back(0);
	}

	GetTrainingCountsforAchievement(pAvatarInfo, vTraingCounts);
	m_pUser->ProcessAchievementbyGroupAndComplete( ACHIEVEMENT_CONDITION_GROUP_TRAINING, ACHIEVEMENT_LOG_GROUP_SHOP, vTraingCounts);
}

void CFSGameUserSkill::ApplyPreviewSet( int iGameIDIndex ,CPacketComposer& PacketComposer, int iPreview[] )
{
	CFSGameODBC* pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->PREVIRW_ApplyPreviewSet(iGameIDIndex, iPreview))
	{
		PacketComposer.Add(FREESTYLE_PREVIEW_SET_FAIL);
		return;
	}

	PacketComposer.Add(FREESTYLE_PREVIEW_SET_SUCCESS);
}

void CFSGameUserSkill::GetUseFreeStylePreview( int iGameIDIndex, CPacketComposer& PacketComposer, int iPreview[] )
{
	CFSGameODBC* pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->PREVIEW_GetUsePreview(iGameIDIndex, iPreview))
	{
		PacketComposer.Add(FREESTYLE_PREVIEW_GET_FAIL);
		return;
	}
	PacketComposer.Add(FREESTYLE_PREVIEW_GET_SUCCESS);

	for(int i = 0 ; i < FREESTYLE_PREVIEW_MAX_CHECK ; ++i)
		PacketComposer.Add(iPreview[i]);
}


void CFSGameUserSkill::InsertFreestyleByPotentialComponent(CFSSkillShop* pSkillShop, char *szUserID, int iUserIDIndex, int iGameIDIndex, char* szGameID, int iStorageType, int iSkillNo, int iSkillShiftBit, int iSkillPricePoint, int iPrevMoney, int iPostMoney, int iSkillPriceTrophy, int iCondition)
{
	CFSGameODBC *pGameODBC = (CFSGameODBC *)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC);
	CHECK_NULL_POINTER_VOID(m_pUser);
	CHECK_NULL_POINTER_VOID(pSkillShop);

	int iItemPrice = 0;
	if( (iPostMoney - iPrevMoney) > 0 )
	{
		iItemPrice = iPostMoney - iPrevMoney;
	}
	else if( iSkillPricePoint > 0 )	
	{
		iItemPrice = iSkillPricePoint;
	}
	else if( iSkillPriceTrophy > 0 )	
	{
		iItemPrice = iSkillPriceTrophy;
	}

	int iTicketInventoryIndex = 0;
	int iTokenIdx = -1;
	int iUpdateTokenCount = 0;

	if( ODBC_RETURN_SUCCESS == pGameODBC->spBuyFreestyle( szUserID, iUserIDIndex, iGameIDIndex, iStorageType, iSkillNo, iSkillShiftBit,
		iSkillPricePoint, iPrevMoney, iPostMoney, iSkillPriceTrophy, "", "", iItemPrice, 0, 0, iTicketInventoryIndex ,iTokenIdx, iUpdateTokenCount) )
	{
		InsertFreestyle(szGameID, iSkillNo, iSkillShiftBit, FALSE );

		if( iCondition == -1 )
		{
			int iTempErrorCode = 0;
			InsertToSlotFreestyle(iSkillNo, pSkillShop, iTempErrorCode );
		}
	}
}

void CFSGameUserSkill::DeleteFreestyleByPotentialComponent(CFSSkillShop* pSkillShop, int iSkillNo)
{
	DeletePotentialFreestyle(iSkillNo, pSkillShop);
}

BOOL CFSGameUserSkill::DeletePotentialFreestyle(int iFreestyleNo , CFSSkillShop *pSkillShop, int iSlot)
{
	CFSGameODBC *pGameODBC = (CFSGameODBC *)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC);
	CLock lock(&m_SyncObject);

	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);

	int iFreestyleShiftBit = 0, iFreestyle2 = 0;
	int iaUseFreestyle[MAX_SKILL_SLOT];
	int iaUseFreestyleType[MAX_SKILL_SLOT];
	char szUseFreestyle[MAX_SKILL_STOCK_LEGNTH+1];
	memset(szUseFreestyle , 0 , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );

	int iBonusSkillSlotCount = m_pUser->CheckAndGetBonusSkillSlot();

	// 20080401 Add Skill Slot
	int iSlotCount = pAvatarInfo->Skill.iMaxSkillSlotNum + iBonusSkillSlotCount
		+ pAvatarInfo->Skill.SkillSlot[0].sSlotCount + pAvatarInfo->Skill.SkillSlot[1].sSlotCount + pAvatarInfo->Skill.SkillSlot[2].sSlotCount + pAvatarInfo->Skill.SkillSlot[3].sSlotCount;

	if( iSlotCount > MAX_SKILL_SLOT ) iSlotCount = MAX_SKILL_SLOT;
	// End 

	memcpy( iaUseFreestyle , pAvatarInfo->Skill.iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( iaUseFreestyleType , pAvatarInfo->Skill.iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );

	if( iSlot == -1 )
	{
		iSlot = FindUsedSlot(iFreestyleNo , iaUseFreestyle, iSlotCount);
	}

	if( iSlot != -1 )
	{
		iaUseFreestyle[iSlot] = -1;
		iaUseFreestyleType[iSlot] = -1;
	}

	ConvertItemSlotToString(szUseFreestyle, _countof(szUseFreestyle), iaUseFreestyle, iaUseFreestyleType );	

	int iaFreestyle[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaFreestyle, pAvatarInfo->Skill.iaFreestyle, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	int iaFreestyleStock[MAX_SKILL_STORAGE_COUNT];
	memcpy(iaFreestyleStock, pAvatarInfo->Skill.iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	SAvatarSkill::RemoveSkill(iaFreestyle, iFreestyleNo);
	SAvatarSkill::RemoveSkill(iaFreestyleStock, iFreestyleNo);

	//////////// WCG /////////////////////////////  WCG서버는 디비 변경 안한다
	if( m_bSetSkillForWCG == false  )
	{
		if( ODBC_RETURN_SUCCESS != pGameODBC->spDeleteUserFreestyle(pAvatarInfo->iGameIDIndex, szUseFreestyle, iaFreestyle) )
		{
			return FALSE;
		}
	}
	//

	memcpy( pAvatarInfo->Skill.iaUseFreestyle , iaUseFreestyle , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.iaUseFreestyleType , iaUseFreestyleType , sizeof(int) * MAX_SKILL_SLOT );
	memcpy( pAvatarInfo->Skill.szUseFreestyle , szUseFreestyle , sizeof(char) * (MAX_SKILL_STOCK_LEGNTH+1) );
	memcpy(pAvatarInfo->Skill.iaFreestyle, iaFreestyle, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	memcpy(pAvatarInfo->Skill.iaFreestyleStock, iaFreestyleStock, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
	
	m_pUser->SendAvatarInfoUpdateToMatch();

	return TRUE;
}

BOOL CFSGameUserSkill::CheckOverLapCommandCondition(CFSSkillShop* pSkillShop, int iKind, int iSkillNo)
{
	CHECK_NULL_POINTER_BOOL(pSkillShop);
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo);

	list<int> listOverLapCommandSkillNo;
	CHECK_CONDITION_RETURN(FALSE == pSkillShop->GetOverLapCommandSkillNo(iSkillNo, iKind, listOverLapCommandSkillNo), FALSE);
	
	CHECK_CONDITION_RETURN( true == listOverLapCommandSkillNo.empty() , TRUE);

	return TRUE;
}

BOOL CFSGameUserSkill::ChangeSkillSlot( CFSSkillShop* pSkillShop , int iSkillNo, int iKind, int iSwapSkillNo )
{
	CHECK_NULL_POINTER_BOOL( pSkillShop );
	
	int iErrorCode = 0;

	if( SKILL_TYPE_SKILL == iKind )
	{
		CHECK_CONDITION_RETURN( FALSE == InsertToInventory(iSkillNo, pSkillShop, iErrorCode), FALSE);
		CHECK_CONDITION_RETURN( FALSE == InsertToSlot(iSwapSkillNo, pSkillShop,iErrorCode ), FALSE);
	}
	else if( SKILL_TYPE_FREESTYLE == iKind )
	{
		CHECK_CONDITION_RETURN( FALSE == InsertToInventoryFreestyle(iSkillNo, pSkillShop, iErrorCode), FALSE);
		CHECK_CONDITION_RETURN( FALSE == InsertToSlotFreestyle(iSwapSkillNo, pSkillShop,iErrorCode ), FALSE);
	}
	else 
	{
		return FALSE;
	}

	return TRUE;
}
