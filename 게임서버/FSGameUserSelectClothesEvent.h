qpragma once

class CFSGameUser;
class CFSGameUserSelectClothesEvent
{
private:
	CFSGameUser* m_pUser;
	BOOL m_bDataLoaded;

	SEventSelectClothesUserInfo m_tUserEventInfo;

public:
	BOOL Load(void);

	void SendUserInfo(void);
	void SendAvatarFeatureInfo(void);

	void SendRankRewardInfo(void);
	void SendGachaRewardInfo(void);

	void SendOpenBoxReq(BYTE btOpenType, BYTE btSelectFirstIndex, BYTE btSelectSecondIndex);

	void SendRankList(void);

private:
	BOOL CheckGiveCountReward(void);
	BOOL CheckGiveRankReward(void);

public:
	CFSGameUserSelectClothesEvent(CFSGameUser* pUser);
	~CFSGameUserSelectClothesEvent(void);
};
