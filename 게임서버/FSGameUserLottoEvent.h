qpragma once

class CUser;
class CFSODBCBase;

struct SLottoEventUserNumber
{
	int _iLotteryNumber1;
	int _iLotteryNumber2;
	int _iUpdateDate;

	SLottoEventUserNumber()
	{
		ZeroMemory(this, sizeof(SLottoEventUserNumber));
	}

	void SetUserNumber(int iLotteryNumber1, int iLotteryNumber2, int iUpdateDate)
	{
		_iLotteryNumber1 = iLotteryNumber1;
		_iLotteryNumber2 = iLotteryNumber2;
		_iUpdateDate = iUpdateDate;

	}
};

struct SLottoEventUserStatus
{
	int _iSlotNumber;
	BYTE _btSlotStatus;
	int _iRandomNumber;
	int _iRewardIndex;
	BYTE _btRewardStatus;

	SLottoEventUserStatus()
	{
		ZeroMemory(this, sizeof(SLottoEventUserStatus));
	}

	void SetUserStatus(int iSlotNumber, BYTE btSlotStatus, int iRandomNumber, int iRewardIndex, BYTE btRewardStatus)
	{
		_iSlotNumber = iSlotNumber;
		_btSlotStatus = btSlotStatus;
		_iRandomNumber = iRandomNumber;
		_iRewardIndex = iRewardIndex;
		_btRewardStatus = btRewardStatus;
	}
};
typedef map<int/*_iSlotNumber*/, SLottoEventUserStatus> EVENT_LOTTO_USER_STATUS_MAP;

class CFSGameUserLottoEvent
{
public:
	CFSGameUserLottoEvent(CUser* pUser);
	~CFSGameUserLottoEvent(void);

	BOOL Load();
	void SetUserStatus(int iSlotNumber, int iRandomNumber, int iRewardIndex, BYTE btRewardStatus);
	void SendEventInfo();
	BOOL CheckAndGiveReward();
	RESULT_EVENT_LOTTO_OPEN_LOTTERY_NUMBER OpenLotteryNumberReq(int iSlot, int& iLotteryNumber);
	RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER OpenRandomNumberReq(SC2S_EVENT_LOTTO_OPEN_RANDOM_NUMBER_REQ rq);
	RESULT_EVENT_LOTTO_OPEN_REWARD OpenRewardReq(SC2S_EVENT_LOTTO_OPEN_REWARD_REQ rq);

private:
	CUser* m_pUser;
	BOOL m_bDataLoaded;

	SLottoEventUserNumber m_UserLottoNumber;
	EVENT_LOTTO_USER_STATUS_MAP m_mapUserLottoStatus;
};

