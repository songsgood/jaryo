qinclude "stdafx.h"
qinclude "LobbyClubUserManager.h"
qinclude "CFSGameUser.h"
qinclude "MatchSvrProxy.h"
qinclude "CheerLeaderEventManager.h"
qinclude "GameProxyManager.h"
qinclude "CFSGameUserCheerLeader.h"

CLobbyClubUserManager::CLobbyClubUserManager(void)
{
}

CLobbyClubUserManager::~CLobbyClubUserManager(void)
{
}

void CLobbyClubUserManager::AddUser(CFSGameUser* pUser)
{
	CHECK_NULL_POINTER_VOID(pUser);
	CHECK_CONDITION_RETURN_VOID(pUser->GetClubSN() <= 0);

	CLock Lock(&m_csUserMap);

	m_mapUser[pUser->GetGameIDIndex()] = pUser;
}

void CLobbyClubUserManager::RemoveUser(int iGameIDIndex)
{
	CLock Lock(&m_csUserMap);

	m_mapUser.erase(iGameIDIndex);
}

void CLobbyClubUserManager::Broadcast(int iClubSN, CPacketComposer& Packet, BOOL bNotInPlaying)
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL &&
			iter->second->GetClubSN() == iClubSN)
		{
			if (bNotInPlaying)
				iter->second->CheckAndSend(&Packet, iter->second->IsPlaying());
			else
				iter->second->Send(&Packet); 
		}
	}
}

void CLobbyClubUserManager::SetClubCheerLeaderCode(int iClubSN, int iExpiredCheerLeaderCode, int iChangedCheerLeaderCode)
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL &&
			iter->second->GetClubSN() == iClubSN)
		{
			iter->second->GetUserCheerLeader()->SetClubCheerLeaderIndex(iChangedCheerLeaderCode);

			if(iter->second->GetCheerLeaderCode() == iExpiredCheerLeaderCode)
			{
				if(iChangedCheerLeaderCode == 0)
				{
					iter->second->SetCheerLeaderCode(0);
					iter->second->GetUserCheerLeader()->CheckExpiredCheerLeader();
				}
				else
					iter->second->SetCheerLeaderCode(iChangedCheerLeaderCode);

				if(FALSE == iter->second->IsPlaying())
				{
					CMatchSvrProxy* pMatchSvrProxy = (CMatchSvrProxy*)GAMEPROXY.FindProxy(iter->second->GetMatchLocation());
					if(pMatchSvrProxy)
					{
						SG2M_CHEERLEADER_CHANGE_REQ info;
						info.iGameIDIndex = iter->second->GetGameIDIndex();
						info.iCheerLeaderCode = iter->second->GetCheerLeaderCode();
						pMatchSvrProxy->SendCheerLeaderChangeReq(info);
					}
				}
			}
		}
	}
}

void CLobbyClubUserManager::SetClubCheerLeaderCode(int iClubSN, int iChangedCheerLeaderCode, BYTE btTypeNum)
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL &&
			iter->second->GetClubSN() == iClubSN)
		{
			iter->second->GetUserCheerLeader()->SetClubCheerLeaderIndex(iChangedCheerLeaderCode);
			iter->second->GetUserCheerLeader()->SetClubCheerLeaderTypeNum(btTypeNum);

			if(FALSE == CHEERLEADEREVENT.CheckCheerLeader(iter->second->GetCheerLeaderCode()))
			{
				iter->second->SetCheerLeaderCode(iChangedCheerLeaderCode);
				iter->second->SetCheerLeaderTypeNum(btTypeNum);

				if (FALSE == iter->second->IsPlaying())
				{
					CMatchSvrProxy* pMatchSvrProxy = (CMatchSvrProxy*)GAMEPROXY.FindProxy(iter->second->GetMatchLocation());
					if(pMatchSvrProxy)
					{
						SG2M_CHEERLEADER_CHANGE_REQ info;
						info.iGameIDIndex = iter->second->GetGameIDIndex();
						info.iCheerLeaderCode = iChangedCheerLeaderCode;
						info.btTypeNum = btTypeNum;
						pMatchSvrProxy->SendCheerLeaderChangeReq(info);
					}
				}	
			}
		}
	}
}

void CLobbyClubUserManager::SetClubPennantCode(int iClubSN, short stSlotIndex, int iPennantCode)
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL &&
			iter->second->GetClubSN() == iClubSN)
		{
			iter->second->SetPennantCode(stSlotIndex, iPennantCode);
		}
	}
}

void CLobbyClubUserManager::GiveClubClothes(int iClubSN, vector<int>& vGameIDIndex)
{
	BEGIN_SCOPE_LOCK(m_csUserMap)
		GAMEUSERMAP::iterator iter = m_mapUser.begin();
		GAMEUSERMAP::iterator endIter = m_mapUser.end();
		for (; iter != endIter; ++iter)
		{
			if(iter->second != NULL &&
				iter->second->GetClubSN() == iClubSN)
			{
				vGameIDIndex.push_back(iter->second->GetGameIDIndex());
			}
		}
	END_SCOPE_LOCK
}

void CLobbyClubUserManager::UpdateClubContributionPointRankSeasonIndex(int iSeasonIndex)
{
	BEGIN_SCOPE_LOCK(m_csUserMap)

		GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter != m_mapUser.end() &&
			NULL != iter->second)
		{
			SAvatarInfo* pAvatar = iter->second->GetCurUsedAvatar();
			if(NULL != pAvatar)
			{
				pAvatar->iClubContributionPointRankSeasonIndex = iSeasonIndex;
			}
		}
	}

	END_SCOPE_LOCK
}

void CLobbyClubUserManager::UpdateClubLeagueRankSeasonIndex(int iSeasonIndex)
{
	BEGIN_SCOPE_LOCK(m_csUserMap)

		GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter != m_mapUser.end() &&
			NULL != iter->second)
		{
			SAvatarInfo* pAvatar = iter->second->GetCurUsedAvatar();
			if(NULL != pAvatar)
			{
				pAvatar->iClubLeagueRankSeasonIndex = iSeasonIndex;
			}
		}
	}

	END_SCOPE_LOCK
}

void CLobbyClubUserManager::UpdateUserClubContributionPointRank(int iSeasonIndex, vector<SCLUB_UPDATE_MEMBER_RANKING>& vRank)
{
	BEGIN_SCOPE_LOCK(m_csUserMap)

		GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter != m_mapUser.end() &&
			NULL != iter->second)
		{
			SAvatarInfo* pAvatar = iter->second->GetCurUsedAvatar();
			if(NULL != pAvatar)
			{
				pAvatar->iUserClubContributionPointRankSeasonIndex = 0;
				pAvatar->iUserClubContributionPointRank = 0;
			}
		}
	}

	for(int i = 0; i < vRank.size(); ++i)
	{
		GAMEUSERMAP::iterator iter = m_mapUser.find(vRank[i].iGameIDIndex);
		if(iter != m_mapUser.end() &&
			NULL != iter->second)
		{
			SAvatarInfo* pAvatar = iter->second->GetCurUsedAvatar();
			if(NULL != pAvatar)
			{
				pAvatar->iUserClubContributionPointRankSeasonIndex = iSeasonIndex;
				pAvatar->iUserClubContributionPointRank = vRank[i].iRank;
			}
		}
	}

	END_SCOPE_LOCK
}

void CLobbyClubUserManager::UpdateUserClubLeagueRank(int iSeasonIndex, vector<SCLUB_UPDATE_MEMBER_RANKING>& vRank)
{
	BEGIN_SCOPE_LOCK(m_csUserMap)

		GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter != m_mapUser.end() &&
			NULL != iter->second)
		{
			SAvatarInfo* pAvatar = iter->second->GetCurUsedAvatar();
			if(NULL != pAvatar)
			{
				pAvatar->iUserClubLeagueRankSeasonIndex = 0;
				pAvatar->iUserClubLeagueRank = 0;
			}
		}
	}

	for(int i = 0; i < vRank.size(); ++i)
	{
		GAMEUSERMAP::iterator iter = m_mapUser.find(vRank[i].iGameIDIndex);
		if(iter != m_mapUser.end() &&
			NULL != iter->second)
		{
			SAvatarInfo* pAvatar = iter->second->GetCurUsedAvatar();
			if(NULL != pAvatar)
			{
				pAvatar->iUserClubLeagueRankSeasonIndex = iSeasonIndex;
				pAvatar->iUserClubLeagueRank = vRank[i].iRank;
			}
		}
	}

	END_SCOPE_LOCK
}

void CLobbyClubUserManager::SetClubName(int iClubSN, char* szClubName)
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL &&
			iter->second->GetClubSN() == iClubSN)
		{
			iter->second->SetClubName(szClubName);
		}
	}
}


