qinclude "StdAfx.h"
qinclude "CFSCommonFunc.h"
qinclude "XignCodeManager.h"

xbool XCALL SendProc(xpvoid uid, xpvoid meta, xpcch buf, xulong size)
{
	return XIGNCODEMANAGER.OnSend(uid, meta, buf, size);
}

void XCALL DisconnectProc(xpvoid uid, xpvoid meta, xint code, xctstr report)
{
	XIGNCODEMANAGER.OnDisconnect(uid, meta, code, report);
}
