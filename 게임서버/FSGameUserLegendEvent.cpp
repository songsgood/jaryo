qinclude "stdafx.h"
qinclude "FSGameUserLegendEvent.h"
qinclude "LegendEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "GameProxyManager.h"
qinclude "CenterSvrProxy.h"
qinclude "Util.h"

CFSGameUserLegendEvent::CFSGameUserLegendEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_bIsFirstLogin(FALSE)
{
	ZeroMemory(&m_sUserInfo, sizeof(SLegendUserInfo));
}


CFSGameUserLegendEvent::~CFSGameUserLegendEvent(void)
{
}

BOOL CFSGameUserLegendEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == LEGEND.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iRet = pODBC->EVENT_LEGEND_GetUserData(m_pUser->GetUserIDIndex(), m_sUserInfo);
	if (ODBC_RETURN_SUCCESS != iRet)
	{
		if(iRet == 1)
		{
			//m_bIsFirstLogin = TRUE;
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, INITIALIZE_DATA, FAIL, "EVENT_LEGEND_GetUserData error - UserIDIndex:10752790", m_pUser->GetUserIDIndex());
urn FALSE;
		}		
	}

	vector<SLegendTryRewardUserInfo> vInfo;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_LEGEND_GetUserTryRewardData(m_pUser->GetUserIDIndex(), vInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, INITIALIZE_DATA, FAIL, "EVENT_LEGEND_GetUserTryRewardData error - UserIDIndex:10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	for(int i = 0; i < vInfo.size(); ++i)
	{
		SLegendTryRewardUserInfo info;
		info.iRewardIndex = vInfo[i].iRewardIndex;
		info.btStatus = vInfo[i].btStatus;
		m_mapTryRewardInfo[info.iRewardIndex] = info;
	}

	m_bDataLoaded = TRUE;
	
	return TRUE;
}

void CFSGameUserLegendEvent::SendLegendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == LEGEND.IsOpen());

	CPacketComposer Packet(S2C_LEGEND_EVENT_INFO_RES);
	LEGEND.MakePacketLegendEventInfo(Packet);
	m_pUser->Send(&Packet);
}

void CFSGameUserLegendEvent::SendLegendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == LEGEND.IsOpen());

	SS2C_LEGEND_EVENT_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_LEGEND_EVENT_USER_INFO_RES));

	ss.iKeyCount = m_sUserInfo.iCurrentKey;
	ss.iStarCount = m_sUserInfo.iCurrentStar;
	ss.btBonusPoint = m_sUserInfo.btBonusPoint;
	ss.bIsFirstLoginReward = m_bIsFirstLogin;
	ss.iTryCount = m_sUserInfo.iTryCount;

	vector<SLEGEND_TRY_REWARD_INFO> vInfo;
	LEGEND.GetTryRewardList(vInfo);

	ss.iTryRewardCount = vInfo.size();

	CPacketComposer Packet(S2C_LEGEND_EVENT_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_LEGEND_EVENT_USER_INFO_RES));

	for(int i = 0; i < vInfo.size(); ++i)
	{
		SLEGEND_TRY_REWARD_USER_INFO info;
		info.iRewardIndex = vInfo[i].iRewardIndex;
		info.btStatus = LEGEND_TRY_REWARD_STATUS_DISABLE;

		LEGEND_TRY_REWARD_USER_INFO_MAP::iterator iter = m_mapTryRewardInfo.find(info.iRewardIndex);
		if(iter != m_mapTryRewardInfo.end())
		{
			info.btStatus = iter->second.btStatus;
		}

		if(LEGEND_TRY_REWARD_STATUS_DISABLE == info.btStatus &&
			m_sUserInfo.iTryCount >= vInfo[i].iTryCount)
		{
			info.btStatus = LEGEND_TRY_REWARD_STATUS_ENABLE;
		}
		
		Packet.Add((PBYTE)&info, sizeof(SLEGEND_TRY_REWARD_USER_INFO));
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserLegendEvent::SetLegendKeyCount(int iCount)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == LEGEND.IsOpen());

	m_sUserInfo.iCurrentKey = iCount;
}

BOOL CFSGameUserLegendEvent::BuyLegendKey_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, INVALED_DATA, CHECK_FAIL, "BuyLegendKey_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int	iBuyKeyCount = pBillingInfo->iParam1;
	int	iSumKeyCount = 0;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	SS2C_LEGEND_EVENT_KEY_BUY_RESULT_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_LEGEND_EVENT_KEY_BUY_RESULT_NOT));

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_LEGEND_BuyKey(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, iBuyKeyCount, iPrevCash, iPostCash, pBillingInfo->iItemPrice, iSumKeyCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, LA_DEFAULT, NONE, "EVENT_LEGEND_BuyKey Fail, User:10752790, GameID:(null)", m_pUser->GetUserIDIndex(), m_pUser->GetGameID());
ult = RESULT_LEGEND_BUY_KEY_FAIL;
	}
	else
	{
		m_sUserInfo.iCurrentKey = iSumKeyCount;
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_BUY_SUCCESS;
		ss.iLegendKeyCount = m_sUserInfo.iCurrentKey;

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);

	// 특급상자 구매 결과 알림 패킷 보냄.
	CPacketComposer	Packet(S2C_LEGEND_EVENT_KEY_BUY_RESULT_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_LEGEND_EVENT_KEY_BUY_RESULT_NOT));

	m_pUser->Send(&Packet);

	int iCostType = pBillingInfo->iSellType;
	m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
	m_pUser->SendUserGold();

	return (ss.btResult == RESULT_HOTGIRLSPECIALBOX_BUY_SUCCESS);
}

BOOL CFSGameUserLegendEvent::UseKey(SC2S_LEGEND_EVENT_USE_KEY_REQ& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == LEGEND.IsOpen(), LEGEND_USE_KEY_CLOSED_EVENT);

	SS2C_LEGEND_EVENT_USE_KEY_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_LEGEND_EVENT_USE_KEY_RES));

	BYTE btArrayIndex = 0;
	vector<int/*iRewardIndex*/> vecRewardIndex;
	BOOL bIsShout = FALSE;
	int iRewardCnt = (LEGEND_EVENT_USE_BONUSPOINT == rs.btUseKeyType) ? 1 : static_cast<int>(rs.btUseKeyType);

	if(LEGEND_EVENT_USE_BONUSPOINT != rs.btUseKeyType && 
		m_sUserInfo.iCurrentKey < static_cast<int>(rs.btUseKeyType))
	{
		ss.btResult = LEGEND_USE_KEY_LACK_KEY_FAIL;
	}
	else if(LEGEND_EVENT_USE_BONUSPOINT != rs.btUseKeyType && 
		m_sUserInfo.btBonusPoint == LEGEND.GetMaxBonusPoint())
	{
		ss.btResult = LEGEND_USE_KEY_WRONG_CHOICE_FAIL;
	}
	else if(LEGEND_EVENT_USE_BONUSPOINT == rs.btUseKeyType && 
		m_sUserInfo.btBonusPoint != LEGEND.GetMaxBonusPoint())
	{
		ss.btResult = LEGEND_USE_KEY_WRONG_CHOICE_FAIL;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + iRewardCnt > MAX_PRESENT_LIST)
	{
		ss.btResult = LEGEND_USE_KEY_FULL_MAILBOX_FAIL;
	}
	else if(FALSE == LEGEND.GetRandomProduct(rs.btUseKeyType, rs.btChoiceProductIndex, ss.btResultProductIndex, vecRewardIndex, bIsShout))
	{
		ss.btResult = LEGEND_USE_KEY_WRONG_CHOICE_FAIL;
	}
	else
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);

		int iChangeStar = LEGEND.GetGiveStarCount(rs.btUseKeyType);
		BYTE btBonusPoint = m_sUserInfo.btBonusPoint;
		if(LEGEND_EVENT_USE_KEY_10 == rs.btUseKeyType)
		{
			btBonusPoint++;
		}
		else if(LEGEND_EVENT_USE_BONUSPOINT == rs.btUseKeyType && LEGEND.GetMaxBonusPoint() == m_sUserInfo.btBonusPoint)
		{
			btBonusPoint = 0;
		}

		ss.btResult = LEGEND_USE_KEY_GIVE_REWARD_FAIL;

		char szReward[MAX_LEGEND_REWARD_LENGTH+1] = {NULL,};
		ConvertRewardIndexStr(szReward, &vecRewardIndex, static_cast<int>(LEGEND_EVENT_USE_KEY_10), MAX_LEGEND_REWARD_LENGTH);
		list<int> listPresentIndex; 
		int iReturn = pODBC->EVENT_LEGEND_GiveProductReward(m_pUser->GetUserIDIndex(), rs.btUseKeyType, iChangeStar, btBonusPoint, rs.bIsEffectViewOn, rs.btChoiceProductIndex, ss.btResultProductIndex, listPresentIndex, szReward, m_sUserInfo.iTryCount);

		if(ODBC_RETURN_SUCCESS == iReturn)
		{ 
			ss.btResult = LEGEND_USE_KEY_SUCCESS;
			m_sUserInfo.btBonusPoint = btBonusPoint;
			m_sUserInfo.iCurrentKey -= static_cast<int>(rs.btUseKeyType);
			m_sUserInfo.iCurrentStar += iChangeStar;

			m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);			

			if(TRUE == bIsShout)
			{
				CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
				if( NULL != pCenter )
				{
					SG2S_EVENT_SHOUT_REQ  info;
					info.iEventKind = EVENT_KIND_LEGEND;
					info.btContentsType = LEGEND_SHOUT_CONTENTS_TYPE_S_GRADE_REWARD;
					strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), _countof(info.GameID)-1);
					pCenter->SendPacket(G2S_EVENT_SHOUT_REQ, &info, sizeof(SG2S_EVENT_SHOUT_REQ));
				}
			}
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, LA_DEFAULT, NONE, "EVENT_LEGEND_GiveProductReward Fail, UserIDIndex:10752790, iReturn:0"
 m_pUser->GetUserIDIndex(), iReturn);
		}
	}

	m_pUser->Send(S2C_LEGEND_EVENT_USE_KEY_RES, &ss, sizeof(SS2C_LEGEND_EVENT_USE_KEY_RES));

	return TRUE;
}

BOOL CFSGameUserLegendEvent::UseStar(SC2S_LEGEND_EVENT_USE_STAR_REQ& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == LEGEND.IsOpen(), LEGEND_USE_STAR_CLOSED_EVENT);

	SS2C_LEGEND_EVENT_USE_STAR_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_LEGEND_EVENT_USE_STAR_RES));

	int iStarPrice = 0;
	int iRewardIndex = 0;

	if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + LEGEND.GetMaxStarShopRewardCount() > MAX_PRESENT_LIST)
	{
		ss.btResult = LEGEND_USE_STAR_FULL_MAILBOX_FAIL;
	}
	else if(FALSE == LEGEND.GetStarProduct(rs.btChoiceProductIndex, iStarPrice, iRewardIndex))
	{
		ss.btResult = LEGEND_USE_STAR_WRONG_CHOICE_FAIL;
	}
	else if(m_sUserInfo.iCurrentStar < iStarPrice)
	{
		ss.btResult = LEGEND_USE_STAR_LACK_STAR_FAIL;
	}
	else
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);

		ss.btResult = LEGEND_USE_STAR_GIVE_REWARD_FAIL;

		BYTE btProductIndexDummy[LEGEND_PRODUCT_CHOICE_COUNT] = {0,};
		btProductIndexDummy[0] = rs.btChoiceProductIndex;

		char szReward[MAX_LEGEND_REWARD_LENGTH+1] = {NULL,};
		sprintf_s(szReward, _countof(szReward), "10752790", iRewardIndex);
st<int> listPresentIndex; 
		int iReturn = pODBC->EVENT_LEGEND_GiveProductReward(m_pUser->GetUserIDIndex(), 0, -iStarPrice, m_sUserInfo.btBonusPoint, 0, btProductIndexDummy, btProductIndexDummy, listPresentIndex, szReward, m_sUserInfo.iTryCount);

		if(ODBC_RETURN_SUCCESS == iReturn)
		{
			ss.btResult = LEGEND_USE_STAR_SUCCESS;

			m_sUserInfo.iCurrentStar -= iStarPrice;

			m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, LA_DEFAULT, NONE, "EVENT_LEGEND_GiveProductReward Fail, UserIDIndex:10752790, iReturn:0"
 m_pUser->GetUserIDIndex(), iReturn);
		}
	}

	m_pUser->Send(S2C_LEGEND_EVENT_USE_STAR_RES, &ss, sizeof(SS2C_LEGEND_EVENT_USE_STAR_RES));

	return TRUE;
}

eLEGEND_GET_TRY_REWARD_ITEM_RESULT CFSGameUserLegendEvent::GetTryRewardReq(int iRewardIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, LEGEND_GET_TRY_REWARD_ITEM_FAIL);
	CHECK_CONDITION_RETURN(CLOSED == LEGEND.IsOpen(), LEGEND_GET_TRY_REWARD_ITEM_CLOSED_EVENT);

	SLEGEND_TRY_REWARD_INFO sInfo;
	CHECK_CONDITION_RETURN(FALSE == LEGEND.GetTryRewardInfo(iRewardIndex, sInfo), LEGEND_GET_TRY_REWARD_ITEM_FAIL);
	CHECK_CONDITION_RETURN(sInfo.iTryCount > m_sUserInfo.iTryCount, LEGEND_GET_TRY_REWARD_ITEM_TRY_COUNT_FAIL);

	LEGEND_TRY_REWARD_USER_INFO_MAP::iterator iter = m_mapTryRewardInfo.find(iRewardIndex);
	if(iter != m_mapTryRewardInfo.end())
		CHECK_CONDITION_RETURN(LEGEND_TRY_REWARD_STATUS_COMPLETED == iter->second.btStatus, LEGEND_GET_TRY_REWARD_ITEM_ALREADY_REWARD);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, LEGEND_GET_TRY_REWARD_ITEM_FULL_MAILBOX_FAIL);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, LEGEND_GET_TRY_REWARD_ITEM_FAIL);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, LEGEND_GET_TRY_REWARD_ITEM_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_LEGEND_UpdateUserTryRewardData(m_pUser->GetUserIDIndex(), iRewardIndex), LEGEND_GET_TRY_REWARD_ITEM_FAIL);
	
	if(iter != m_mapTryRewardInfo.end())
		iter->second.btStatus = LEGEND_TRY_REWARD_STATUS_COMPLETED;
	else
	{
		SLegendTryRewardUserInfo info;
		info.iRewardIndex = iRewardIndex;
		info.btStatus = LEGEND_TRY_REWARD_STATUS_COMPLETED;
		m_mapTryRewardInfo[info.iRewardIndex] = info;
	}

	pReward = REWARDMANAGER.GiveReward(m_pUser, pReward);
	CHECK_CONDITION_RETURN(NULL == pReward, LEGEND_GET_TRY_REWARD_ITEM_FAIL);

	return LEGEND_GET_TRY_REWARD_ITEM_SUCCESS;
}
