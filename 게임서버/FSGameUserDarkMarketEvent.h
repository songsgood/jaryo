qpragma once

using namespace DarkMarketEvent;

class CFSGameUser;
class CFSGameUserDarkMarketEvent
{
public:
	CFSGameUserDarkMarketEvent(CFSGameUser* pUser);
	~CFSGameUserDarkMarketEvent(void);

	BOOL Load();
	void SendEventInfo(BYTE btMarketType);
	void SendUserInfo();
	BOOL CheckUseCash(int iUseCash, int iBonusCnt);
	BOOL BuyDiamondProduct(BYTE btProductIndex);
	BOOL BuyPackageProduct_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL CheckCanBuyPackageProduct(int iPackageItemCode, BYTE& btProductIndex, int& iBonusCnt, int& iErrorCode);

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;

	int				m_iMyDiamond;
	int				m_iDiamondProductRemainCnt[DIAMOND_PRODUCT_CNT];
	int				m_iPackageProductRemainCnt[PACKAGE_PRODUCT_CNT];
};
