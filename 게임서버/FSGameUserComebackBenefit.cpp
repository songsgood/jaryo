qinclude "stdafx.h"
qinclude "FSGameUserComebackBenefit.h"
qinclude "ThreadODBCManager.h"
qinclude "CFSGameUser.h"
qinclude "ComebackBenefitManager.h"
qinclude "CFSGameServer.h"

CFSGameUserComebackBenefit::CFSGameUserComebackBenefit( CFSGameUser* pUser )
	: m_pUser(pUser)
	, m_bComeback(FALSE)
	, m_iAttendanceCount(0)
	, m_iGamePlayCount(0)
	, m_tAttendanceDate(0)
	, m_iGaveIndex(0)
	, m_bDBSave(FALSE)
	, m_iGaveAttendance(0)
{

}


CFSGameUserComebackBenefit::~CFSGameUserComebackBenefit(void)
{
}

BOOL CFSGameUserComebackBenefit::LoadComebackBenefit()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	if(ODBC_RETURN_SUCCESS != pODBCBase->COMEBACK_BENEFIT_GetUserInfo(
		m_pUser->GetUserIDIndex(), m_iAttendanceCount, m_iGamePlayCount, (int)m_bComeback, m_iGaveIndex
		,m_iGaveAttendance, m_tAttendanceDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_COMEBACKBENEFIT, INITIALIZE_DATA, FAIL, "COMEBACK_BENEFIT_GetUserInfo (10752790)", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	if(TRUE == m_bComeback) 
	{		
		if(TRUE ==COMEBACK.CheckComebackAllGaveReward(m_iGaveAttendance, m_iGaveIndex))
		{// 컴백유저이고 전부 보상획득 했으면
			m_bComeback = FALSE;
		}
	}

	return TRUE;
}

void CFSGameUserComebackBenefit::SendComebackBenefitInfoNot( void )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);

	CPacketComposer Packet(S2C_COMEBACK_BENEFIT_LOBBY_BUTTON_NOT);

	SS2C_COMEBACK_BENEFIT_LOBBY_BUTTON_NOT not;
	not.btButtonEnable = static_cast<BYTE>(m_bComeback);
	Packet.Add((PBYTE)&not, sizeof(SS2C_COMEBACK_BENEFIT_LOBBY_BUTTON_NOT));

	m_pUser->Send(&Packet);
}

void CFSGameUserComebackBenefit::SendComebackBenefitList( void )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);

	CheckAttendance(TRUE);

	SS2C_COMEBACK_BENEFIT_LIST_RES rs;

	rs.shCompletedAttendanceValue = static_cast<short>(m_iAttendanceCount);
	rs.shCompletedGamePlayCount = static_cast<short>(m_iGamePlayCount);
	
	ZeroMemory(rs.btLoginBenefit, sizeof(BYTE)*MAX_COMEBACK_BENEFIT_ATTENDANCE_COUNT);
	ZeroMemory(rs.btGamePlayList, sizeof(BYTE)*MAX_COMEBACK_BENEFIT_GAMEPLAY_LIST_COUNT);

	COMEBACK.MakeLoginBenefit(m_iGaveAttendance, rs.btLoginBenefit);
	for( int i = 0 ; i < MAX_COMEBACK_BENEFIT_GAMEPLAY_LIST_COUNT ; ++i )
	{
		if(i <= m_iGaveIndex)
			rs.btGamePlayList[i] = TRUE;
	}

	rs.btFocusGamePlayIndex = static_cast<BYTE>(m_iGaveIndex+1); // next
	if(MAX_COMEBACK_BENEFIT_GAMEPLAY_LIST_COUNT <= rs.btFocusGamePlayIndex)
		rs.btFocusGamePlayIndex = MAX_COMEBACK_BENEFIT_GAMEPLAY_LIST_COUNT-1;

	CPacketComposer Packet(S2C_COMEBACK_BENEFIT_LIST_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_COMEBACK_BENEFIT_LIST_RES));

	m_pUser->Send(&Packet);
}

void CFSGameUserComebackBenefit::SendComebackBenefitRewardList( SC2S_COMEBACK_BENEFIT_REWARD_INFO_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);

	SS2C_COMEBACK_BENEFIT_REWARD_INFO_RES rs;
	rs.btGamePlayIndex = rq.btGamePlayIndex;
	rs.btRewardGaveEnable = (rq.btGamePlayIndex <= m_iGaveIndex ? TRUE : FALSE);
	rs.btRewardButtonEnable = FALSE;
	
	int iNextGiveIndex = m_iGaveIndex+1;//COMEBACK.GetNextPlayIndex(m_iGaveIndex);
	if( FALSE == rs.btRewardGaveEnable && // 아직 받지 않았고,
		COMEBACK.CheckGamePlayCondition((int)rq.btGamePlayIndex, m_iGamePlayCount) &&// 조건달성이고
		iNextGiveIndex < MAX_COMEBACK_BENEFIT_GAMEPLAY_LIST_COUNT && // 마지막 보상이 아직 아니고
		iNextGiveIndex == rq.btGamePlayIndex) // 다음번에 받아야 할 인덱스라면
	{
		rs.btRewardButtonEnable = TRUE;
	}
	
	CPacketComposer Packet(S2C_COMEBACK_BENEFIT_REWARD_INFO_RES);
	rs.btRewardCount = COMEBACK.GetRewardCount_PlayIndex((int)rq.btGamePlayIndex);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_COMEBACK_BENEFIT_REWARD_INFO_RES));

	if( 0 < COMEBACK.MakePacketRewardList(Packet, (int)rq.btGamePlayIndex) )
	{
		m_pUser->Send(&Packet);
	}
}

void CFSGameUserComebackBenefit::GiveRewardComebackBenefit( SC2S_COMEBACK_BENEFIT_RECEIVING_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);
	CPacketComposer Packet(S2C_COMEBACK_BENEFIT_RECEIVING_RES);
	SS2C_COMEBACK_BENEFIT_RECEIVING_RES rs;

	int iMailClearance = MAX_PRESENT_LIST - m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();
	const int iGivePlayCondition = COMEBACK.GetPlayCountCondition((int)rq.btGamePlayIndex);
	const int iGamePlayIndex = static_cast<int>(rq.btGamePlayIndex);

	if(m_iGamePlayCount < iGivePlayCondition)
		rs.btResult = RESULT_COMEBACK_BENEFIT_REWARD_GAMEPLAY_SHORTAGE;
	if(m_iGaveIndex >= iGamePlayIndex || 
		m_iGamePlayCount < COMEBACK.GetPlayCountCondition(iGamePlayIndex))
		rs.btResult = RESULT_COMEBACK_BENEFIT_REWARD_FAIL;
	else if( iMailClearance < COMEBACK.GetRewardCount_PlayIndex(iGamePlayIndex))
		rs.btResult = RESULT_COMEBACK_BENEFIT_REWARD_MAILBOX_FULL;
	else
	{
		int iGaveOutIndex = 0;
		list<int> listPresentIndex;
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);

		if( ODBC_RETURN_SUCCESS != pODBCBase->COMEBACK_BENEFIT_UpdateUserComplete(
			m_pUser->GetUserIDIndex()
			,m_pUser->GetGameIDIndex()
			,COMEBACK_DB_UPDATE_TYPE_PLAYCOUNT
			,COMEBACK.ConvertPlayIndex((int)rq.btGamePlayIndex)//(int)rq.btGamePlayIndex
			,m_iGamePlayCount
			,iGaveOutIndex
			,listPresentIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_COMEBACKBENEFIT, CALL_SP, FAIL, 
				"COMEBACK_BENEFIT_UpdateUserComplete UserIDIndex:(10752790), PlayIndex(0)"
 m_pUser->GetUserIDIndex(), rq.btGamePlayIndex);

			rs.btResult = RESULT_COMEBACK_BENEFIT_REWARD_FAIL;			
		}
		else
		{
			m_iGaveIndex = iGaveOutIndex;

			rs.btResult = RESULT_COMEBACK_BENEFIT_REWARD_SUCCESS;

			m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);

			m_bDBSave = FALSE;
		}
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_COMEBACK_BENEFIT_RECEIVING_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserComebackBenefit::IncreaseGamePlay()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);
	CHECK_CONDITION_RETURN_VOID(m_iGamePlayCount >= COMEBACK.GetComebackMaximumGamePlay());

	++m_iGamePlayCount;
	m_bDBSave = TRUE;
}

void CFSGameUserComebackBenefit::IncreaseAttendance()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);
	CHECK_CONDITION_RETURN_VOID(m_iAttendanceCount >= COMEBACK.GetComebackMaximumAttendance());

	++m_iAttendanceCount;
	m_bDBSave = TRUE;
}

void CFSGameUserComebackBenefit::SaveComebackBenefit(BOOL bSaveDate, CFSODBCBase* pODBCBase /*= nullptr*/)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDBSave);

	if(nullptr == pODBCBase)
	{
		pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);
	}

	if( ODBC_RETURN_SUCCESS != pODBCBase->COMEBACK_BENEFIT_UpdateUserInfo(
		m_pUser->GetUserIDIndex()
		,m_iAttendanceCount
		,m_iGamePlayCount
		,(int)bSaveDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_COMEBACKBENEFIT, CALL_SP, FAIL, "COMEBACK_BENEFIT_UpdateUserInfo (10752790)", m_pUser->GetUserIDIndex());
rn;
	}

	m_bDBSave = FALSE;
}

BOOL CFSGameUserComebackBenefit::CheckAndGiveRewardComebackBenefitwithAttendance(BOOL bCheckMailPopup)
{
	// 이 함수의 리턴값은 DB저장을 의미.
	CHECK_CONDITION_RETURN(FALSE == m_bComeback, FALSE);

	int iGiveIndex = COMEBACK.ConvertAttendanceGiveIndex(m_iAttendanceCount,m_iGaveAttendance);
	if( 0 < iGiveIndex )
	{
		int iMailClearance = MAX_PRESENT_LIST - m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();
		if(iMailClearance < COMEBACK.GetRewardCount(iGiveIndex))
		{
			if(TRUE == bCheckMailPopup)
			{
				SS2C_COMEBACK_BENEFIT_RECEIVING_RES rs;
				rs.btResult = RESULT_COMEBACK_BENEFIT_REWARD_ATTENDANCE_MAILBOX_FULL;
				CPacketComposer Packet(S2C_COMEBACK_BENEFIT_RECEIVING_RES);
				Packet.Add((PBYTE)&rs, sizeof(SS2C_COMEBACK_BENEFIT_RECEIVING_RES));
				m_pUser->Send(&Packet);
			}
			//메일이 가득찼다면 출석정보만 저장하고.. 지급은 하지 않은걸로
			return TRUE;
		}

		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_BOOL(pODBCBase);

		int iGaveOutIndex = 0;
		list<int> listPresentIndex;

		if( ODBC_RETURN_SUCCESS != pODBCBase->COMEBACK_BENEFIT_UpdateUserComplete(
			m_pUser->GetUserIDIndex()
			,m_pUser->GetGameIDIndex()
			,COMEBACK_DB_UPDATE_TYPE_ATTENDANCE
			,iGiveIndex
			,m_iAttendanceCount
			,iGaveOutIndex
			,listPresentIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_COMEBACKBENEFIT, CALL_SP, FAIL, 
				"COMEBACK_BENEFIT_UpdateUserComplete Attendance UserIDIndex:(10752790), GiveIndex(0)"
 m_pUser->GetUserIDIndex(), iGiveIndex);
		}
		else
		{
			m_iGaveAttendance = iGaveOutIndex;

			m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);

			m_bDBSave = FALSE;
		}

		return FALSE;
	}
	else
	{
		// 바로 저장
		return TRUE;
	}

	return FALSE;
}

void CFSGameUserComebackBenefit::CheckAttendance(BOOL bCheckMailPopup)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);

	BOOL bAttendance = FALSE;
	int iHour = GetHHMM() / 100;
	int iAddHour = COMEBACK.GetAttendanceHour();
	if(iHour < iAddHour )  // n시가 안되었다면
	{
		if(!(GetYesterdayYYYYMMDD() <=  TimetToYYYYMMDD(m_tAttendanceDate - (60*60*iAddHour))))
		{
			bAttendance = TRUE;
		}
	}
	else
	{
		if(TimetToYYYYMMDD(_GetCurrentDBDate) != TimetToYYYYMMDD(m_tAttendanceDate - (60*60*iAddHour)) )
		{
			bAttendance = TRUE;
		}
	}

	if(TRUE == bAttendance)
	{
		IncreaseAttendance();

		m_tAttendanceDate = _GetCurrentDBDate;
	}

	if( TRUE == CheckAndGiveRewardComebackBenefitwithAttendance(bCheckMailPopup) && TRUE == bAttendance)
	{
		SaveComebackBenefit(TRUE);
	}
}

void CFSGameUserComebackBenefit::DecreaseAttendance()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);

	if(0 < m_iAttendanceCount)
	{
		--m_iAttendanceCount;
	}	
}

void CFSGameUserComebackBenefit::LoginAttendanceGiveBenefit()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bComeback);

	CheckAttendance(FALSE);

	// 복귀유저인데 신규유저 혜택 대상자라면 날려야함
	if(true == m_pUser->GetUserNewbieBenefit()->IsNewbie())
	{
		m_pUser->GetUserNewbieBenefit()->AllRemoveNewbieBenefit();
	}
}