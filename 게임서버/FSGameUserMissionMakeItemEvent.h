qpragma once

using namespace MissionMakeItemEvent;

const int MISSIONMAKEITEM_DAILY_MAX_CNT = 15;

class CFSGameUser;
class CFSGameUserMissionMakeItemEvent
{
public:
	CFSGameUserMissionMakeItemEvent(CFSGameUser* pUser);
	~CFSGameUserMissionMakeItemEvent(void);

	BOOL			Load();
	BOOL			CheckResetDate(time_t tCurrentTime = _time64(NULL), BOOL bIsLogin = FALSE);
	void			SendEventProductInfo();
	void			SendUserInfo();
	BOOL			CheckAndUpdateMission(BYTE btMissionConditionType, BYTE& btMaterialType, int& iRewardCnt, int iAddValue = 1, time_t tCurrentTime = _time64(NULL));
	void			MakeItem(BYTE btProductIndex);
	BOOL			BuyCashItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL			SaveStartConnectTime();
	void			SetMaterialEventItem(int iCount);
	
private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;

	time_t			m_tLastResetTime;
	time_t			m_tStartConnectTime;
	int				m_iConnectSecond;
	int				m_iTodayMakeCnt;
	int				m_iMaterial[MAX_MATERIAL_TYPE];
};

