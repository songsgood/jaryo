qinclude "stdafx.h"
qinclude "CFSGameUserCheerLeader.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "CheerLeaderEventManager.h"
qinclude "CBillingManager.h"
qinclude "ClubConfigManager.h"

CFSGameUserCheerLeader::CFSGameUserCheerLeader(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iClubCheerLeaderIndex(0)
	, m_btClubCheerLeaderTypeNum(0)
{
}

CFSGameUserCheerLeader::~CFSGameUserCheerLeader(void)
{
}

BOOL CFSGameUserCheerLeader::Load()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	vector<SAvatarCheerLeader> vAvatarCheerLeader;
	int iCheerLeaderIndex = 0;
	if (ODBC_RETURN_SUCCESS != pBaseODBC->AVATAR_GetUserCheerLeader(m_pUser->GetGameIDIndex(), vAvatarCheerLeader) ||
		ODBC_RETURN_SUCCESS != pBaseODBC->AVATAR_GetUserDefaultCheerLeader(m_pUser->GetGameIDIndex(), iCheerLeaderIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, DB_DATA_LOAD, FAIL, "AVATAR_GetUserCheerLeader failed");
		return FALSE;
	}

	for(int i = 0; i < vAvatarCheerLeader.size(); ++i)
	{
		AddCheerLeader(vAvatarCheerLeader[i]);
	}

	if(iCheerLeaderIndex > 0)
	{
		m_pUser->SetCheerLeaderCode(iCheerLeaderIndex);

		AVATAR_CHEERLEADER_MAP::const_iterator ItMap = m_mapAvatarCheerLeader.find(iCheerLeaderIndex);
		m_pUser->SetCheerLeaderTypeNum((ItMap != m_mapAvatarCheerLeader.end()) ? ItMap->second.btTypeNum : 0);
	}
	else
	{
		if(!vAvatarCheerLeader.empty())
		{
			// 구입날짜가 가장 빠른 치어리더
			m_pUser->SetCheerLeaderCode(vAvatarCheerLeader[0].iCheerLeaderIndex);
			m_pUser->SetCheerLeaderTypeNum(vAvatarCheerLeader[0].btTypeNum);
		}
	}

	for(int i = 0; i < vAvatarCheerLeader.size(); ++i)
	{
		int iPresentIndex = -1;
		int iAddTrophyCount = CLUBCONFIGMANAGER.GetCheerLeaderProbabillityByAbillity(vAvatarCheerLeader[i].iCheerLeaderIndex, vAvatarCheerLeader[i].btTypeNum, CHEERLEADR_ABILLITY_LOGIN_GIVE_TROPHY);

		if(iAddTrophyCount > 0)
		{
			time_t tCurrentTime = _time64(NULL);
			if(TRUE == TodayCheck(EVENT_RESET_HOUR, vAvatarCheerLeader[i].tBuyDate, tCurrentTime))
			{
				continue;
			}

			time_t tLastGiveDate = -1;
			if (ODBC_RETURN_SUCCESS == pBaseODBC->AVATAR_GetUserCheerLeaderLastGiveTrophyTime(m_pUser->GetGameIDIndex(), vAvatarCheerLeader[i].iCheerLeaderIndex, tLastGiveDate))
			{
				if(tLastGiveDate > -1)
				{
					if(TRUE == TodayCheck(EVENT_RESET_HOUR, tLastGiveDate, tCurrentTime))
					{
						continue;
					}
				}
			}

			if (ODBC_RETURN_SUCCESS == pBaseODBC->ITEM_GiveTrophy_Login_CheerLeaderReward(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), vAvatarCheerLeader[i].iCheerLeaderIndex, iAddTrophyCount, iPresentIndex))
			{
				if(iPresentIndex > -1)
				{
					m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iPresentIndex);
				}
			}
		}		
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserCheerLeader::AddCheerLeader(SAvatarCheerLeader AvatarCheerLeader)
{
	m_mapAvatarCheerLeader[AvatarCheerLeader.iCheerLeaderIndex] = AvatarCheerLeader;
}

BOOL CFSGameUserCheerLeader::CheckCheerLeader(int iCheerLeaderIndex, BYTE& btTypeNum)
{
	AVATAR_CHEERLEADER_MAP::iterator iter = m_mapAvatarCheerLeader.find(iCheerLeaderIndex);
	CHECK_CONDITION_RETURN(iter == m_mapAvatarCheerLeader.end(), FALSE);
	btTypeNum = iter->second.btTypeNum;

	return TRUE;
}

BOOL CFSGameUserCheerLeader::CheckCheerLeader(int iCheerLeaderIndex)
{
	AVATAR_CHEERLEADER_MAP::iterator iter = m_mapAvatarCheerLeader.find(iCheerLeaderIndex);
	CHECK_CONDITION_RETURN(iter == m_mapAvatarCheerLeader.end(), FALSE);

	return TRUE;
}

BOOL CFSGameUserCheerLeader::GetCheerLeader(int iCheerLeaderIndex, SAvatarCheerLeader& sCheerLeader)
{
	AVATAR_CHEERLEADER_MAP::iterator iter = m_mapAvatarCheerLeader.find(iCheerLeaderIndex);
	CHECK_CONDITION_RETURN(iter == m_mapAvatarCheerLeader.end(), FALSE);

	memcpy(&sCheerLeader, &iter->second, sizeof(SAvatarCheerLeader));

	return TRUE;
}

int CFSGameUserCheerLeader::GetCheerLeaderEffectRemainCnt(int iCheerLeaderIndex, BYTE btTypeNum)
{
	AVATAR_CHEERLEADER_MAP::const_iterator citer = m_mapAvatarCheerLeader.find(iCheerLeaderIndex);
	CHECK_CONDITION_RETURN(citer == m_mapAvatarCheerLeader.end(), -1);
	CHECK_CONDITION_RETURN(citer->second.btTypeNum != btTypeNum, -1);

	int iEffectRemainCnt = -1;
	BYTE btAbilityIndex = 0;
	if(TRUE == CHEERLEADEREVENT.GetDailyEffectAbilityIndex(iCheerLeaderIndex, btTypeNum, btAbilityIndex))
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_CONDITION_RETURN(pODBC && 
			ODBC_RETURN_SUCCESS != pODBC->AVATAR_GetCheerLeaderDailyEffectCnt(m_pUser->GetGameIDIndex(), iCheerLeaderIndex, btAbilityIndex, iEffectRemainCnt), -1);
	}

	return iEffectRemainCnt;
}

int CFSGameUserCheerLeader::GetDefaultCheerLeaderIndex()
{
	CheckExpiredCheerLeader();

	return m_pUser->GetCheerLeaderCode();
}

BUY_EVENT_CHEERLEADER_RESULT CFSGameUserCheerLeader::BuyEventCheerLeader(int iCheerLeaderIndex, BYTE btTypeNum, int iPeriod)
{
	CHECK_CONDITION_RETURN(CLOSED == CHEERLEADEREVENT.IsOpen(), BUY_EVENT_CHEERLEADER_RESULT_NOT_EVENT_DATE);

	SEventCheerLeaderConfig Config;
	CHECK_CONDITION_RETURN(FALSE == CHEERLEADEREVENT.GetCheerLeader(iCheerLeaderIndex, Config), BUY_EVENT_CHEERLEADER_RESULT_FAIL);

	int iPrice = CHEERLEADEREVENT.GetPrice(iCheerLeaderIndex, iPeriod);
	CHECK_CONDITION_RETURN(-1 == iPrice, BUY_EVENT_CHEERLEADER_RESULT_FAIL);
	CHECK_CONDITION_RETURN(iPrice > (m_pUser->GetCoin()+m_pUser->GetEventCoin()), BUY_EVENT_CHEERLEADER_RESULT_SUPPLY_CASH);
	CHECK_CONDITION_RETURN(TRUE == CheckCheerLeader(iCheerLeaderIndex, btTypeNum), BUY_EVENT_CHEERLEADER_RESULT_ALREADY_EXIST);

	int iAddTrophyCount = CLUBCONFIGMANAGER.GetCheerLeaderProbabillityByAbillity(iCheerLeaderIndex, btTypeNum, CHEERLEADR_ABILLITY_BUY_GIVE_TROPHY);
	if(iAddTrophyCount > 0)
	{
		CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, BUY_EVENT_CHEERLEADER_RESULT_FULL_MAILBOX);
	}

	SBillingInfo BillingInfo;
	BillingInfo.iEventCode = EVENT_BUY_EVENT_CHEERLEADER;
	m_pUser->SetBillingInfo(BillingInfo, SELL_TYPE_CASH, iPrice);
	strcpy_s(BillingInfo.szItemName, "Buy EventCheerLeader");
	BillingInfo.iParam0 = iCheerLeaderIndex;
	BillingInfo.iParam1 = iPeriod;
	BillingInfo.iParam2 = iPrice;
	BillingInfo.iParam3 = btTypeNum;

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		return BUY_EVENT_CHEERLEADER_RESULT_FAIL;
	}

	return BUY_EVENT_CHEERLEADER_RESULT_SUCCESS;
}

BOOL CFSGameUserCheerLeader::BuyEventCheerLeader_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, INVALED_DATA, CHECK_FAIL, "BuyEventCheerLeader_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	SAvatarCheerLeader AvatarCheerLeader;
	AvatarCheerLeader.iCheerLeaderIndex = pBillingInfo->iParam0;
	AvatarCheerLeader.tBuyDate = _time64(NULL);
	AvatarCheerLeader.tExpireDate = AvatarCheerLeader.tBuyDate + (pBillingInfo->iParam1 * 24 * 60 * 60);
	AvatarCheerLeader.btTypeNum = pBillingInfo->iParam3;

	int iPostCash = pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;

	int iResult = 0;
	int iPresentIndex = -1;
	int iAddTrophyCount = CLUBCONFIGMANAGER.GetCheerLeaderProbabillityByAbillity(AvatarCheerLeader.iCheerLeaderIndex, AvatarCheerLeader.btTypeNum, CHEERLEADR_ABILLITY_BUY_GIVE_TROPHY);

	if (ODBC_RETURN_SUCCESS != (iResult = pODBC->EVENT_CheerLeader_BuyCheerLeader(m_pUser->GetGameIDIndex(), AvatarCheerLeader, pBillingInfo->iParam1, pBillingInfo->iParam2,
		pBillingInfo->iCurrentCash, iPostCash, iAddTrophyCount, iPresentIndex)))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, DB_DATA_UPDATE, FAIL, "BuyEventCheerLeader_AfterPay - GameIDIndex:11866902", m_pUser->GetGameIDIndex());
2C_BUY_EVENT_CHEERLEADER_RES rs;
		rs.btResult = iResult;
		m_pUser->Send(S2C_BUY_EVENT_CHEERLEADER_RES, &rs, sizeof(SS2C_BUY_EVENT_CHEERLEADER_RES));

		return iResult;		
	}

	if(iPresentIndex > -1)
	{
		m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iPresentIndex);
	}

	m_pUser->SetUserBillResultAtMem(SELL_TYPE_CASH, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
	m_pUser->SendUserGold();

	AddCheerLeader(AvatarCheerLeader);
	m_pUser->SetCheerLeaderCode(AvatarCheerLeader.iCheerLeaderIndex);
	m_pUser->SetCheerLeaderTypeNum(AvatarCheerLeader.btTypeNum);

	SS2C_BUY_EVENT_CHEERLEADER_RES rs;
	rs.btResult = BUY_EVENT_CHEERLEADER_RESULT_SUCCESS;
	m_pUser->Send(S2C_BUY_EVENT_CHEERLEADER_RES, &rs, sizeof(SS2C_BUY_EVENT_CHEERLEADER_RES));

	return TRUE;
}

void CFSGameUserCheerLeader::CheckExpiredCheerLeader()
{
	CHECK_CONDITION_RETURN_VOID(m_mapAvatarCheerLeader.empty());

	AVATAR_CHEERLEADER_MAP::iterator iter = m_mapAvatarCheerLeader.begin();
	for( ; iter != m_mapAvatarCheerLeader.end(); )
	{
		if(_time64(NULL) >= iter->second.tExpireDate)
		{
			if(m_pUser->GetCheerLeaderCode() == iter->second.iCheerLeaderIndex)
			{
				m_pUser->SetCheerLeaderCode(0);
				m_pUser->SetCheerLeaderTypeNum(0);
			}
			
			m_mapAvatarCheerLeader.erase(iter++);
		}
		else
		{
			++iter;
		}
	}

	if(m_pUser->GetCheerLeaderCode() == 0)
	{
		if(!m_mapAvatarCheerLeader.empty())
		{
			time_t tBuyDate = 0;
			AVATAR_CHEERLEADER_MAP::iterator iter = m_mapAvatarCheerLeader.begin();
			for( ; iter != m_mapAvatarCheerLeader.end(); ++iter)
			{
				if(tBuyDate == 0 ||
					tBuyDate > iter->second.tBuyDate)
				{
					m_pUser->SetCheerLeaderCode(iter->second.iCheerLeaderIndex);
					m_pUser->SetCheerLeaderTypeNum(iter->second.btTypeNum);
				}
			}
		}
		else if(m_iClubCheerLeaderIndex > 0)
		{
			m_pUser->SetCheerLeaderCode(m_iClubCheerLeaderIndex);
			m_pUser->SetCheerLeaderTypeNum(m_btClubCheerLeaderTypeNum);
		}
	}
}

time_t CFSGameUserCheerLeader::GetRemainTime(int iCheerLeaderIndex)
{ 
	AVATAR_CHEERLEADER_MAP::iterator iter = m_mapAvatarCheerLeader.find(iCheerLeaderIndex);
	CHECK_CONDITION_RETURN(iter == m_mapAvatarCheerLeader.end(), 0);

	return difftime(iter->second.tExpireDate, _time64(NULL)); 
}

void CFSGameUserCheerLeader::MakeCheerLeaderList(CPacketComposer& Packet, BYTE btCheerLeaderCnt)
{
	btCheerLeaderCnt += m_mapAvatarCheerLeader.size();
	Packet.Add(btCheerLeaderCnt);

	SCL2G_CLUB_CHEERLEADER_INFO info;
	AVATAR_CHEERLEADER_MAP::iterator iter = m_mapAvatarCheerLeader.begin();
	for( ; iter != m_mapAvatarCheerLeader.end(); ++iter)
	{
		info.iCheerLeaderCode = iter->second.iCheerLeaderIndex;
		info.btTypeNum = iter->second.btTypeNum;
		info.iItemCode = CLUBCONFIGMANAGER.GetCheerLeaderItemCode(info.iCheerLeaderCode, info.btTypeNum);
		info.iCheerLeaderTime = GetRemainTime(info.iCheerLeaderCode);
		info.iRemainEffectCnt = GetCheerLeaderEffectRemainCnt(info.iCheerLeaderCode, info.btTypeNum);
		Packet.Add((PBYTE)&info, sizeof(SCL2G_CLUB_CHEERLEADER_INFO));
	}
}
