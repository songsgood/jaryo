qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserHotGirlSpecialBoxEvent
{
public:
	CFSGameUserHotGirlSpecialBoxEvent(CFSGameUser* pUser);
	~CFSGameUserHotGirlSpecialBoxEvent(void);

	BOOL			Load();
	void			SendHotGirlSpecialBoxEventInfo();
	void			SendHotGirlSpecialBoxUserInfo();
	BOOL			BuySpecialBox_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL			OpenSpecialBox(BYTE btUserCurrentSeason);
	BOOL			GivePotion(BYTE btPotionRewardType, BYTE btCurrentSeason = SEASON_NONE);
	BOOL			GiveMainReward(BYTE btUserCurrentSeason);

	void			SetSpecialBoxCount(int iCount);

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;
	int				m_iSpecialBoxCount;
	BYTE			m_btNormalPotion;
	BYTE			m_btSpecialPotion;
	BOOL			m_bIsGetMainReward;
	BOOL			m_bIsFirstLogin;
};

