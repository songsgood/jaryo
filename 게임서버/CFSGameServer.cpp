// CFSGameServer.cpp: implementation of the CFSGameServer class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CFSGameServer.h"

qinclude "CFSGameDBCfg.h"
qinclude "CFSFileLog.h"

qinclude "CFSSvrList.h"

qinclude "CFSRankManager.h"
qinclude "CContentsManager.h"

qinclude "CFSSkillShop.h"
qinclude "CFSTrainingShop.h"

qinclude "CFSItemList.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameUserItem.h"

qinclude "CFSGameClient.h"

qinclude "CFSGameODBC.h"

qinclude "MatchSvrProxy.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"

qinclude "CReloadDataManager.h"
qinclude "HackingManager.h"
qinclude "RewardCardManager.h"
qinclude "CActionShop.h"
qinclude "CChannelBuffManager.h"
qinclude "ClubConfigManager.h"
qinclude "UserPatternManager.h"
qinclude "CFSLobby.h"
qinclude "CRecordBoardManager.h"
qinclude "BingoManager.h"
qinclude "RewardManager.h"
qinclude "CoachCardShop.h"
qinclude "RageBalance.h"
qinclude "RandomBoxManager.h"
qinclude "GameGuideItemConfig.h"
qinclude "SportBigEventManager.h"
qinclude "SecretStoreManager.h"
qinclude "CIntensivePracticeManager.h"
qinclude "MatchAdvantageManager.h"
qinclude "MatchingPoolCareManager.h"
qinclude "FactionManager.h"
qinclude "LuckyBoxManager.h"
qinclude "WordPuzzlesManager.h"
qinclude "SkillUpgrade.h"
qinclude "RandomCardShopManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "PuzzlesManager.h"
qinclude "ThreeKingdomsEventManager.h"
qinclude "MissionShopManager.h"
qinclude "SpecialSkinManager.h"
qinclude "HoneyWalletEventManager.h"
qinclude "CustomizeManager.h"
qinclude "CharacterCollectionConfigManager.h"
qinclude "HotGirlGiftEventManager.h"
qinclude "HotGirlTimeManager.h"
qinclude "HotGirlMissionEventManager.h"
qinclude "HotGirlSpecialBoxEventManager.h"
qinclude "SkyLuckyEventManager.h"
qinclude "GoldenSafeEventManager.h"
qinclude "CReceivePacketBuffer.h"
qinclude "TodayHotDeal.h"
qinclude "LobbyClubUserManager.h"
qinclude "CheerLeaderEventManager.h"
qinclude "PayBackManager.h"
qinclude "GoldenCrushManager.h"
qinclude "MatchingCardManager.h"
qinclude "AttendanceItemShop.h"
qinclude "XignCodeManager.h"
qinclude "LvUpEventManager.h"
qinclude "RandomItemBoardEventManager.h"
qinclude "ShoppingEventManager.h"
qinclude "LegendEventManager.h"
qinclude "MiniGameZoneEventManager.h"
qinclude "LovePaybackEventManager.h"
qinclude "BigWheelLoginEventManager.h"
qinclude "DarkMarketEventManager.h"
qinclude "ColoringPlayEventManager.h"
qinclude "CongratulationEventManager.h"
qinclude "HelloNewYearEventManager.h"
qinclude "SalePlusEventManager.h"
qinclude "MissionCashLimitEventManager.h"
qinclude "DriveMissionEventManager.h"
qinclude "DevilTemtationEventManager.h"
qinclude "CrazyLevelUpEventManager.h"
qinclude "GamePlayEventManager.h"
qinclude "EventDateManager.h"
qinclude "CUserAppraisalManager.h"
qinclude "MissionEventManager.h"
qinclude "MissionBingoEventManager.h"
qinclude "SonOfChoiceEventManager.h"
qinclude "CUserAppraisalManager.h"
qinclude "ChatSvrProxy.h"
qinclude "UserChoiceMissionEventManager.h"
qinclude "UserChoiceMissionEvent.h"
qinclude "RandomItemTreeEventManger.h"
qinclude "ExpBoxItemEventManager.h"
qinclude "SpecialAvatarPieceEventManager.h"
qinclude "RandomArrowEventManager.h"
qinclude "VendingMachineEventManager.h"
qinclude "PurchaseGradeManager.h"
qinclude "RandomItemGroupEventManger.h"
qinclude "SaleRandomItemEventManager.h"
qinclude "RainbowWeekEventManager.h"
qinclude "GameOfDiceEventManager.h"
qinclude "WelcomeUserEventManager.h"
qinclude "RankMatchManager.h"
qinclude "LeagueSvrProxy.h"
qinclude "ChatSvrProxy.h"
qinclude "PCRoomEventManager.h"
qinclude "CashItemRewardEventManager.h"
qinclude "PasswordEventManager.h"
qinclude "HalloweenGamePlayEventManager.h"
qinclude "ShoppingFestivalEventManager.h"
qinclude "EventHalfPrice.h"
qinclude "LottoEventManager.h"
qinclude "ODBCSvrProxy.h"
qinclude "PresentFromDeveloperEventManager.h"
qinclude "DiscountItemEventManager.h"
qinclude "RandomItemDrawEventManager.h"
qinclude "OpenBoxEventManager.h"
qinclude "NewbieBenefitManager.h"
qinclude "FriendInviteManager.h"
qinclude "MissionMakeItemEventManager.h"
qinclude "ShutdownUserManager.h"
qinclude "GamePlayLoginEventManager.h"
qinclude "PotionMakingEventManager.h"
qinclude "PrimonGoEventManager.h"
qinclude "PrivateRoomEventManager.h"
qinclude "HippocampusEventManager.h"
qinclude "SteelBagMissionEventManager.h"
qinclude "ComebackBenefitManager.h" 
qinclude "PowerupCapsule.h"
qinclude "UnlimitedTatooEventManager.h"
qinclude "PVEManager.h"
qinclude "CovetEventManager.h"
qinclude "PromiseManager.h"
qinclude "MagicMissionEventManager.h"
qinclude "ShoppingGodEventManager.h"
qinclude "SpecialPieceManager.h"
qinclude "PackageItemEventManager.h"
qinclude "WhitedayEventManager.h"
qinclude "SelectClothesEventManager.h"
qinclude "SubCharDevelopManager.h"
qinclude "SummerCandyEventManager.h"
qinclude "ItemShopPackageItemEventManager.h"
qinclude "DailyLoginRewardEventManager.h"
qinclude "PremiumPassEventManager.h"
qinclude "SecretShopEventManager.h"
qinclude "SpeechBubbleManager.h"
qinclude "TransferJoycityPackageEventManager.h"
qinclude "TransferJoycityShopEventManager.h"
qinclude "TransferJoycity100DreamEventManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CFSGameServer*	CFSGameServer::m_pInstance = NULL;

bool			CFSGameServer::m_bWCGServer = false;    ///// WCG 

extern CFSGameDBCfg*	g_pFSDBCfg;

typedef	std::map<int,CFSGameUser*>	UserIndexMap;

CFSGameServer::CFSGameServer(CIOCP* pIOCP, LPCTSTR szServiceName, DWORD dwServerTimerInterval) 
: CNexusServer(pIOCP, szServiceName, dwServerTimerInterval)
, m_SingleContentsList("SingleContents")
, m_SingleContentBonusList("SingleContentBonus")
, m_bUseSingleTrainingLastStageBonus(TRUE)
, m_bEnableAvatarFame(FALSE)
{
	CFSGameClient::InitializeAvailableUserClientState();

	m_pItemShop = NULL;
	m_pSkillShop = NULL;
	m_pTrainingShop = NULL;
	m_iFSTimeCount = 0;
	m_bSetUserLog = FALSE;
	
	m_bEnable = FALSE;

	CFSGameServer::m_pInstance = this;
		
	m_iMatchMode = 0;			// 기본 : 0, 3대3 제한 : 1
	m_dwShoutTime = 0;
	m_bShout = FALSE;
	
	m_bNoWithdraw = FALSE;	// 20080616 초보유저 채널 강퇴 금지
	
	m_iEqualizeLV = -1;
	m_bIsEqualizeChannel = FALSE;
	
	m_bTournamentEnable = FALSE;	// 20090316 Tournament StatusFlag
	// 20090402 Write Login Request Count Log
	m_bUseLoginRequestLog = FALSE;
	m_dwLoginRequestCount = 0;
	// End

	m_bAbusePenaltyReinforcementEnable = FALSE;			// 20090518 Abuse user penalty reinforcement status flag.
	m_iAbusePenaltyDisconnectPointCondition = -1;

	// 2010.02.23
	// 옵션화
	// ktKim
	m_bOptionFlagStatusMod = FALSE;

	m_vecMaxLevelStepLv.clear();

	// 20090619 Add Episode Group
	for(int i = 0 ; i< MAX_EPISODE_GROUP_COUNT; i++)
	{	
		char szLockName[128] = "Episode";
		char szAfterNumber[10] = {0};

		_itoa_s(i, szAfterNumber, _countof(szAfterNumber), 10);
		strcat_s(szLockName, _countof(szLockName), szAfterNumber);		
		m_paEpisodeList[i] = new CTypeList< SEpisode >(szLockName);
	}
	// End

	ZeroMemory(m_CrashCheckPacketNum,	sizeof(DWORD)*CRASH_CHECK_NUM);
	ZeroMemory(m_CrashCheckTickAvg,		sizeof(DWORD)*CRASH_CHECK_NUM);
	m_dwForbidChatInterval = 0;

	m_bUseLuckItem = false;   
	m_iUserInfoProtVerID = 0;

	m_pActionShop = NULL;
	m_pLimitedEditionItemManager = NULL;
	m_iMatchingPoolCareTimeCount = 0;
	m_bItemShopEventButton = FALSE;
	SetNewPCRoom(FALSE);
}

CFSGameServer::~CFSGameServer()
{
	SAFE_DELETE(m_pItemShop);
	SAFE_DELETE(m_pSkillShop);
	SAFE_DELETE(m_pTrainingShop);
	SAFE_DELETE(m_pOpUserInfoList);		// 20060329  OpUser Enter Channel		Op 유저 무한 채널 입장
	SAFE_DELETE(m_pActionShop);
	SAFE_DELETE(m_pLimitedEditionItemManager);
}

CSocketClient * CFSGameServer::IAllocateClient()
{
	CFSGameClient * pFSClient = new CFSGameClient();
	
	while( pFSClient == NULL )
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, ALLOCATION, FAIL, "CFSGameClient");	
		Sleep(500);
		pFSClient = new CFSGameClient();
	}	
	
	pFSClient->InitializeInstance();
	
	return (CSocketClient *)pFSClient;
}

void CFSGameServer::FreeClient(CSocketClient* pSocketClient)
{
	if (FS_PROXY < ((CBaseClient*) pSocketClient)->GetState())
	{
		CSvrProxy* pProxy = (CSvrProxy*)pSocketClient;
		pProxy->SetConnected(FALSE);
		//pProxy->CleanupInstance(); // 여기서 Clean 시킬 경우 Send후 SendBuffer에서 보낸 데이터 삭제할때 이미 데이터가 없어서 오류가 발생하는 경우가 있음. 근데 여기서 안하도 ProxyManager::OnTimer에서 Connect하면서 해도 동기화 문제는 발생할 수 있는데..
		return;
	}

	//동기화문제가 있음. 객체 삭제 시점에 한번더 체크해야됨
	CFSGameUser* pUser = (CFSGameUser*) ((CFSGameClient*) pSocketClient)->GetUser();
	if (NULL != pUser)
	{
		pUser->ExitChatChannel();
		LOBBYCLUBUSER.RemoveUser(pUser->GetGameIDIndex());
		SHUTDOWNUSER.RemoveUser(pUser->GetUserIDIndex());
	}

	CNexusServer::FreeClient(pSocketClient);
}

BOOL CFSGameServer::RemoveUser(CFSGameUser* pUser, BOOL bExitChatChannel/* = TRUE*/)
{
	if(NULL != pUser &&
		TRUE == bExitChatChannel)
	{
		pUser->ExitChatChannel();
		LOBBYCLUBUSER.RemoveUser(pUser->GetGameIDIndex());
		SHUTDOWNUSER.RemoveUser(pUser->GetUserIDIndex());
	}

	return RemoveUser((CUser*)pUser);
}

BOOL CFSGameServer::RemoveUser(CUser* pUser)
{
	return CNexusServer::RemoveUser(pUser);
}

BOOL GetLogicalAddress(PVOID addr, PTSTR szModule, DWORD len, DWORD &section, DWORD &offset)
{
	MEMORY_BASIC_INFORMATION mbi;
	if(!VirtualQuery(addr, &mbi, sizeof(mbi))) return FALSE;
	DWORD hMod = (DWORD)mbi.AllocationBase;
	if(!GetModuleFileName((HMODULE)hMod, szModule, len)) return FALSE;
	PIMAGE_DOS_HEADER pDosHdr = (PIMAGE_DOS_HEADER)hMod;
	PIMAGE_NT_HEADERS pNtHdr = (PIMAGE_NT_HEADERS)(hMod + pDosHdr->e_lfanew);
	PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION(pNtHdr);
	DWORD rva = (DWORD)addr - hMod; // RVA is offset from module load address
	for(unsigned i = 0; i < pNtHdr->FileHeader.NumberOfSections; i++, pSection++)
	{
		DWORD sectionStart = pSection->VirtualAddress;
		DWORD sectionEnd = sectionStart + max(pSection->SizeOfRawData, pSection->Misc.VirtualSize);
		if((rva >= sectionStart) && (rva <= sectionEnd))
		{
			section = i + 1;
			offset = rva - sectionStart;
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CFSGameServer::OnInitialize()
{
	char szAppName[128];
	sprintf_s(szAppName, _countof(szAppName), "FS[11866902]", GetProcessID());
Manager.SetAppName(szAppName);
	
	if (FALSE == CNexusServer::OnInitialize())
	{
		return FALSE;
	}

	int iErrorCode = 0;
	
	CFSRankODBC*	pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC( ODBC_RANK );
	CHECK_NULL_POINTER_BOOL( pRankODBC );

	CFSClubBaseODBC*	pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC( ODBC_CLUB );
	CHECK_NULL_POINTER_BOOL( pClubBaseODBC );

	CFSLogODBC*	pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
	CHECK_NULL_POINTER_BOOL( pLogODBC );

	CNexusODBC*	pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_BOOL( pNexusODBC );

	time_t tCurrentDBTime = -1;
	DECLARE_INIT_TCHAR_ARRAY(szSessionKey, MAX_SESSIONKEY_LENGTH+1);	
	pLogODBC->USER_DeleteSession(0, GetProcessID(), 0, szSessionKey, tCurrentDBTime, iErrorCode);
	
	if(g_pFSDBCfg)
	{
		g_pFSDBCfg->LoadCfgInfo();
	}
	
	int iContentsResult = -1;
	
	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC*)pNexusODBC)->CONFIG_GetContentsConfig( m_ContentsConfig, iContentsResult ) )
	{	
		return FALSE;
	}
	
	if( iContentsResult == -1 )
	{	
		return FALSE;
	}
	
	int iNoWithDraw = 0;
	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( 41 , iNoWithDraw, GetProcessID() ))
	{
		if( iNoWithDraw == 1)
		{
			m_bNoWithdraw = TRUE;
		}
	}
	
	// 20090316 Tournament StatusFlag
	int iTournamentEnable = 0;
	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( TOURNAMENT_ENABLE_FLAG_NUM , iTournamentEnable, 0 ))
	{
		if( iTournamentEnable != 0)
		{
			WRITE_LOG_NEW(LOG_TYPE_STATUS_FLAG, DB_DATA_LOAD, NONE, "Tournament Enable");	
			m_bTournamentEnable = TRUE;
		}
	}
	
	// 20090402 Write Login Request Count Log
	int iUseLoginRequestLog = 0;
	if( ODBC_RETURN_SUCCESS ==((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( LOGIN_REQUEST_COUNT_LOG_FLAG_NUM , iUseLoginRequestLog, 0 ))
	{
		if( iUseLoginRequestLog != 0 )
		{
			WRITE_LOG_NEW(LOG_TYPE_STATUS_FLAG, DB_DATA_LOAD, NONE, "LoginRequestLog Enable");		
			m_bUseLoginRequestLog = TRUE;
		}
	}
		
	int iMatchMode = 0;
	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( 40 , iMatchMode, GetProcessID() ))
	{
		if( iMatchMode == 1)
		{
			m_iMatchMode = 1;
		}
		
	}
	
	SetCodePage();
	
	if (FALSE == GetServerList()->GetGameSvrInfoSnapShot(GetProcessID(), m_ProcessInfo))
	{
		ASSERT(0);
		return FALSE;
	}
	
	m_pOpUserInfoList = new COpUserInfoList();
	if( FALSE == m_pOpUserInfoList->LoadOpUserInfoList() )
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, FAIL, "LoadOpUserInfoList");		
	}
	
	int iEqualizeLv = 0;
	if(ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus(STATUSFLAG_EQUALIZE_CHANNEL, iEqualizeLv, GetProcessID()))
	{
		if(iEqualizeLv > 0 && iEqualizeLv <= GAME_LVUP_MAX)
		{
			m_iEqualizeLV = iEqualizeLv;
			m_bIsEqualizeChannel = TRUE;
		}
	}

	SFSConfigInfo sFunctionConfig;
	// 20090518 강종 페널티 강화 수정.	
	memset( &sFunctionConfig, 0, sizeof(SFSConfigInfo) );
	if( g_pFSDBCfg->GetCfg( CFGCODE_DISCONNECT, IS_USE_ABUSE_PENALTY, sFunctionConfig ) == TRUE )
	{
		if( sFunctionConfig.iParam[ABUSE_PENALTY_ACTIVE] == 1 )
		{			
			m_bAbusePenaltyReinforcementEnable = TRUE;
			SetAbusePenaltyDisconnectPointCondition( sFunctionConfig.iParam[ABUSE_PENALTY_CONDITION] );
			if( sFunctionConfig.iParam[ABUSE_PENALTY_LOSS_DECISION] == 1 )
			{
				WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, NONE, "Abuse User Penalty Reinforcement Enable : AbusePoint 11866902, Loss Judgment.",sFunctionConfig.iParam[ABUSE_PENALTY_CONDITION]);		
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, NONE, "Abuse User Penalty Reinforcement Enable : AbusePoint 11866902, No Loss Judgment.",sFunctionConfig.iParam[ABUSE_PENALTY_CONDITION]);
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, NONE, "Abuse User Penalty Reinforcement Disable");		
		}
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, NONE, "Abuse User Penalty Reinforcement Disable");		
	}

	DWORD dwStart = GetTickCount();
	
	pRankODBC->spFSSetFlagStatus(3, 0, GetProcessID());
	
	{
		if(FALSE == RANK.LoadRank(pRankODBC))
		{
			WRITE_LOG_NEW(LOG_TYPE_RANK, DB_DATA_LOAD, FAIL, "LoadRank");		
			return FALSE;
		}

		pRankODBC->spFSSetFlagStatus(0, 0, GetProcessID());
	}

	//Shop의 정보를 가져온다
	m_pItemShop = new CFSItemShop();
	
	if( FALSE == m_pItemShop->LoadShopItemList())
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "LoadShopItemList");		
		return FALSE;
	}
	
	m_pSkillShop = new CFSSkillShop();
	if( FALSE == m_pSkillShop->LoadSkillShopList())
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, DB_DATA_LOAD, FAIL, "LoadSkillShopList");		
		return FALSE;
	}
	
	m_pTrainingShop = new CFSTrainingShop();
	if( FALSE == m_pTrainingShop->LoadTrainingShopList()) 
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, DB_DATA_LOAD, FAIL, "LoadTrainingShopList");		
		return FALSE;
	}

	m_pActionShop = new CActionShop();
	if(FALSE == m_pActionShop->LoadActionConfig())
	{
		WRITE_LOG_NEW(LOG_TYPE_ACTION, DB_DATA_LOAD, FAIL, "LoadActionConfig");	
		return FALSE;
	}

	m_pLimitedEditionItemManager = new CLimitedEditionItemManager();
	if(FALSE == m_pLimitedEditionItemManager->LoadLimitedEditionItem())
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "LoadLimitedEditionItem");	
		return FALSE;
	}

	m_pLimitedTimeItemManager = new CLimitedTimeItemManager();
	if(FALSE == m_pLimitedTimeItemManager->LoadLimitedTimeItem())
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_LOAD, FAIL, "LoadLimitedTimeItem");	
		return FALSE;
	}

	int iStatus = 0;
	
	if(pRankODBC != NULL && ODBC_RETURN_SUCCESS == pRankODBC->spFSCheckFlagStatus( 2 , iStatus , GetProcessID() ))
	{
		if( iStatus == 1)
		{
			m_bSetUserLog = TRUE;
		}
		else if( iStatus == 0)
		{
			m_bSetUserLog = FALSE;
		}
	}

	m_bItemShopEventButton = FALSE;
	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( STATUSFLAG_ITEMSHOP_EVENTBUTTON_RENDER , iStatus , GetProcessID() ))
	{
		if( iStatus )
			m_bItemShopEventButton = TRUE;
	}
		
	m_bCheckLog = false;
	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( STATUSFLAG_CHECK_LOG , iStatus , GetProcessID() ))
	{
		if( iStatus )
			m_bCheckLog = true;
	}
	if (m_bCheckLog)
		WRITE_LOG_NEW(LOG_TYPE_STATUS_FLAG, DB_DATA_LOAD, NONE, "User Log Enabled");	
	else
		WRITE_LOG_NEW(LOG_TYPE_STATUS_FLAG, DB_DATA_LOAD, NONE, "User Log Disabled");	
	
	if (FALSE == BILLING_GAME.InitializeInstance(this))
	{
		return FALSE;
	}
	
	// 20090917 Leveling
	if (FALSE == LoadLevelingCurve())
	{
		return FALSE;
	}
	// End

	LoadMaxLevelPlayCountData();

	if(FALSE == LoadDataForContentsManager()) //COACHCARD.Initialize() 이전에 호출
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, FAIL, "LoadDataForContentsManager");	
		return FALSE;
	}

	FLOAT fEnable = 0;
	if(TRUE == CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_FAME, 1, fEnable) &&
		1 == fEnable)
	{
		m_bEnableAvatarFame = TRUE;
	}

	fEnable = 0;
	if(TRUE == CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_NEWPCROOM, 1, (float)fEnable))
	{
		SetNewPCRoom((BOOL)fEnable);
	}

	if(FALSE == COACHCARD.Initialize())	//REWARDMANAGER.Initialize 이전에 호출
	{
		WRITE_LOG_NEW(LOG_TYPE_COACHCARD, DB_DATA_LOAD, FAIL,"COACHCARD");
		return FALSE;
	}

	if (FALSE == REWARDMANAGER.Initialize())
	{
		return FALSE;
	}

	if (FALSE == GAMEPROXY.Initialize(this, m_pServerList))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, INITIALIZE_DATA, FAIL, "GAMEPROXY.Initialize");
		return FALSE;
	}

	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( CHAT_INTERVAL_TIMEOUT_FLAG_NUM, iStatus , -1 ))
	{	
		if ( iStatus > 0 )
		{
			m_dwForbidChatInterval = iStatus*1000;
		}
	}

	if(FALSE == ACHIEVEMENTMANAGER.LoadAchievementConfigList())
	{
		WRITE_LOG_NEW(LOG_TYPE_ACHIEVEMENT, DB_DATA_LOAD, FAIL, "LoadAchievementConfigList");	
		ACHIEVEMENTMANAGER.RemoveAll();
		return FALSE;
	}
	else if (FALSE == ACHIEVEMENTMANAGER.LoadAchievementConditionList())
	{
		WRITE_LOG_NEW(LOG_TYPE_ACHIEVEMENT, DB_DATA_LOAD, FAIL, "LoadAchievementConditionList");	
		ACHIEVEMENTMANAGER.RemoveAll();
		return FALSE;
	}

	// 20070809 Single Contents Merge
	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC*)pNexusODBC)->spGetMiniGameConfig( m_SingleContentsList ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAME, DB_DATA_LOAD, FAIL, "spGetMiniGameConfig");	
	}

	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC*)pNexusODBC)->spGetMiniGameBonus( m_SingleContentBonusList ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAME, DB_DATA_LOAD, FAIL, "spGetMiniGameBonus");	
	}
	// End


	// 2010.02.24
	// 서버 Config
	// ktKim
	SetConfig();
	// End

	// 20070809 Single Contents Merge
	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC*)pNexusODBC)->EP_GetEpisodeList( m_paEpisodeList ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAME, DB_DATA_LOAD, FAIL, "EP_GetEpisodeList");	
	}
	// End
	if( ODBC_RETURN_SUCCESS != ((CFSODBCBase*)pNexusODBC)->LOSE_LEAD_GetConfig(
		m_sLoseLeadConfig.iLeadTime,m_sLoseLeadConfig.iLeadRating,m_sLoseLeadConfig.iRewardIndex, m_sLoseLeadConfig.iRatingRate))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, FAIL, "LOSE_LEAD_GetConfig");
		m_sLoseLeadConfig.bLoad = FALSE;
	}
	else
		m_sLoseLeadConfig.bLoad = TRUE;

	((CFSGameODBC*)pNexusODBC)->EVENT_GetComponentList(&m_ComponentEventManager);

	CCompEventComponentFunction CompEventFunction;
	CompEventFunction.SetParam(pNexusODBC);
	m_ComponentEventManager.CheckEvent(PERFORM_TIME_INITIALIZE_SERVER, &CompEventFunction);
	CompEventFunction.ResetParam();

	if (FALSE == AVATARCREATEMANAGER.Initialize())
		return FALSE;

	if (FALSE == m_BasketBallManager.Initialize())
		return FALSE;

	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( STATUSFLAG_LUCK_ITEM , iStatus, GetProcessID() ))
	{
		if( iStatus == 1)	m_bUseLuckItem = true;
	}

	if ( ODBC_RETURN_SUCCESS == ((CFSGameODBC*)pNexusODBC)->spFSCheckFlagStatus( USER_INFO_PROT_VER_ID_FLAG_NUM, iStatus, GetProcessID()) )
	{
		if ( iStatus > 0 ) m_iUserInfoProtVerID = 1;
	}

	HACKINGMANAGER.LoadHackingManagerConfig();

	if (FALSE == XIGNCODEMANAGER.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_XIGNCODE, INITIALIZE_DATA, FAIL, "XIGNCODEMANAGER");	
	}

	REWARDCARDMANAGER.Initialize();

	if(FALSE == CHANNELBUFFMANAGER.LoadChannelBuff())
	{
		WRITE_LOG_NEW(LOG_TYPE_CHANNELBUFF, DB_DATA_LOAD, FAIL, "LoadChannelBuff");	
	}

	if (FALSE == CLUBCONFIGMANAGER.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_CLUB, INITIALIZE_DATA, FAIL, "CLUBCONFIGMANAGER");	
		return FALSE;
	}

	if(FALSE == USERPATTERNMANAGER.LoadUserPatternConfig())
	{
		WRITE_LOG_NEW(LOG_TYPE_USERPATTERN, DB_DATA_LOAD, FAIL,"LoadUserPatternConfig");
		return FALSE;
	}

	if( FALSE == EVENTDATEMANAGER.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INITIALIZE_DATA, FAIL, "EVENTDATEMANAGER");
		return FALSE;
	}

	if (FALSE == BINGO.Initialize(this)) // rewardmanager, LoadShopItemList 이후에 호출
	{
		WRITE_LOG_NEW(LOG_TYPE_BINGO, DB_DATA_LOAD, FAIL,"BINGO.Initialize");
		return FALSE;
	}

	if(FALSE == RECORDBOARDMANAGER.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_RECORDBOARD, DB_DATA_LOAD, FAIL,"RECORDBOARDMANAGER");
		return FALSE;
	}

	if(FALSE == RAGE.initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_RAGE, DB_DATA_LOAD, FAIL,"RAGE");
		return FALSE;
	}

	if(FALSE == SECRETSTOREMANAGER.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SECRETSTORE, DB_DATA_LOAD, FAIL,"SECRETSTOREMANAGER");
	}

	if(FALSE == RANDOMCARDSHOP.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, DB_DATA_LOAD, FAIL,"RANDOMCARDSHOPMANAGER");
		return FALSE;
	}

	SGameGuideItemConfig	stGameGuideInfo;
	if( ODBC_RETURN_SUCCESS != ((CFSGameODBC*)pNexusODBC)->GetGameGuideItemConfig(stGameGuideInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEGUIDE, DB_DATA_LOAD, FAIL, "GAMEGUIDEITEMConfig");
	}
	else
		GAMEGUIDEITEMConfig.SetGameGuideInfo(stGameGuideInfo);

	if( FALSE == SBIGEVENTManager.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INITIALIZE_DATA, FAIL, "SBIGEVENTManager");	
	}

	if(FALSE == INTENSIVEPRACTICEMANAGER.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_INTENSIVEPRACTICE, INITIALIZE_DATA, FAIL, "INTENSIVEPRACTICEMANAGER");
		return FALSE;
	}

	if(FALSE == MATCHINGPOOLCAREMANAGER.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCHINGPOOLCARE, INITIALIZE_DATA, FAIL, "MATCHINGPOOLCAREMANAGER");	
		return FALSE;
	}

	MATCH_ADVANTAGE.Initialize();

	if (FALSE == FACTION.Initialize(SERVER_TYPE_NORMAL, m_pItemShop))
	{
		WRITE_LOG_NEW(LOG_TYPE_FACTION, INITIALIZE_DATA, FAIL, "FACTION");
		return FALSE;
	}

	if (FALSE == LUCKYBOX.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_LUCKYBOX, DB_DATA_LOAD, FAIL,"LUCKYBOX.Initialize");
		return FALSE;
	}

	if (FALSE == WORDPUZZLES.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_WORDPUZZLES, DB_DATA_LOAD, FAIL,"WORDPUZZLES.Initialize");
		return FALSE;
	}

	if (FALSE == SKILLUPGRADE.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SKILL, INITIALIZE_DATA, FAIL, "SKILLUPGRADE");
		return FALSE;
	}

	if (FALSE == RANDOMBOX.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMBOX, DB_DATA_LOAD, FAIL,"RANDOMBOX.Initialize");
	}

	if (FALSE == PUZZLES.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_PUZZLES, DB_DATA_LOAD, FAIL,"PUZZLES.Initialize");
	}

	if (FALSE == THREEKINGDOMSEVENT.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_THREEKINGDOMEVENT, DB_DATA_LOAD, FAIL,"THREEKINGDOMSEVENT.Initialize");
	}

	if(FALSE == MISSIONSHOP.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_MISSIONSHOP, DB_DATA_LOAD, FAIL,"MISSIONSHOP.Initialize");
	}

	if( FALSE == SPECIALSKINManager.Initialize(m_pSkillShop)) // skillShop 아래에 있어야합니다.
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, DB_DATA_LOAD, FAIL,"SPECIALSKINManager.Initialize");
		return FALSE;
	}

	if( FALSE == HONEYWALLET.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_HONEYWALLET, DB_DATA_LOAD, FAIL,"HONEYWALLET.Initialize");
		return FALSE;
	}

	if( FALSE == CUSTOMIZEManager.Initialize(m_pItemShop))
	{
		WRITE_LOG_NEW(LOG_TYPE_CUSTOMIZE, DB_DATA_LOAD, FAIL, "CUSTOMIZEManager.Initialize");
		return FALSE;
	}
	
	if( FALSE == CCHARACTERCOLLECTIONCONFIG.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_CHARACTERCOLLECTION, DB_DATA_LOAD, FAIL, "CCHARACTERCOLLECTIONCONFIG.Initialize");
		return FALSE;
	}

	if( FALSE == HOTGIRLTIMEManager.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENTHOTGIRLTIME, INITIALIZE_DATA, FAIL, "HOTGIRLTIMEManager");
		return FALSE;
	}

	if(FALSE == HOTGIRLSPECIALBOX.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_HOTGIRLSPECIALBOX, INITIALIZE_DATA, FAIL, "HOTGIRLSPECIALBOX");
		return FALSE;
	}

	if(FALSE == HOTGIRLMISSION.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENTHOTGIRLMISSION, DB_DATA_LOAD, FAIL,"HOTGIRLMISSION.Initialize");
		return FALSE;
	}

	if(FALSE == SKYLUCKY.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, DB_DATA_LOAD, FAIL,"SKYLUCKY.Initialize");
		return FALSE;
	}

	if(FALSE == CHEERLEADEREVENT.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, DB_DATA_LOAD, FAIL,"CHEERLEADEREVENT.Initialize");
		return FALSE;
	}

	if(FALSE == PAYBACKEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_PAYBACK, DB_DATA_LOAD, FAIL,"PAYBACKEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == GOLDENCRUSH.Initialize(m_pItemShop))
	{
		WRITE_LOG_NEW(LOG_TYPE_GOLDENCRUSH, DB_DATA_LOAD, FAIL,"GOLDENCRUSH.Initialize");
		return FALSE;
	}

	if(FALSE == GOLDENSAFE.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_GOLDENSAFE, DB_DATA_LOAD, FAIL,"GOLDENSAFE.Initialize");
		return FALSE;
	}

	if(FALSE == SHOPPING.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGEVENT, DB_DATA_LOAD, FAIL,"SHOPPING.Initialize");
		return FALSE;
	}

	if(FALSE == LEGEND.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, DB_DATA_LOAD, FAIL,"LEGEND.Initialize");
		return FALSE;
	}

	if(FALSE == MINIGAMEZONE.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_MINIGAMEZONE_EVENT, DB_DATA_LOAD, FAIL,"MINIGAMEZONE.Initialize");
		return FALSE;
	}

	if(FALSE == LOVEPAYBACK.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_LOVEPAYBACK_EVENT, DB_DATA_LOAD, FAIL,"LOVEPAYBACK.Initialize");
		return FALSE;
	}

	if(FALSE == HOTGIRLGIFT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_HOTGIRLGIFT, DB_DATA_LOAD, FAIL,"HOTGIRLGIFT.Initialize");
		return FALSE;
	}

	if(FALSE == BIGWHEELLOGIN.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_BIGWHEELLOGIN_EVENT, DB_DATA_LOAD, FAIL,"BIGWHEELLOGIN.Initialize");
		return FALSE;
	}

	if(FALSE == DARKMARKET.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL,"DARKMARKET.Initialize");
		return FALSE;
	}	

	if(FALSE == COLORINGPLAY.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL,"COLORINGPLAY.Initialize");
		return FALSE;
	}	

	if(FALSE == CONGRATULATION.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL,"CONGRATULATION.Initialize");
		return FALSE;
	}	

	if(FALSE == HELLONEWYEAR.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL,"HELLONEWYEAR.Initialize");
		return FALSE;
	}	

	if( FALSE == SALEPLUS.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "SALEPLUS.Initialize");
		return FALSE;
	}

	if( FALSE == MISSIONCASHLIMIT.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "MISSIONCASHLIMIT.Initialize");
		return FALSE;
	}

	if(FALSE == CRAZYLEVELUP.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL,"CRAZYLEVELUP.Initialize");
		return FALSE;
	}

	if(FALSE == DRIVEMISSIONEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_DRIVEMISSION_EVENT, DB_DATA_LOAD, FAIL,"DRIVEMISSIONEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == MATCHINGCARD.Initialize( m_pItemShop ))
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCHINGCARD, DB_DATA_LOAD, FAIL,"MATCHINGCARD.Initialize");
		return FALSE;
	}

	if( FALSE == ATTENDANCEITEMSHOP.Initialize( m_pItemShop ))
	{
		WRITE_LOG_NEW(LOG_TYPE_ATTENDANCEITEMSHOP, DB_DATA_LOAD, FAIL, "CAttendanceItemShop::Initialize");
		return FALSE;
	}

	if(FALSE == LVUPEVENT.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_LVUP_EVENT, DB_DATA_LOAD, FAIL, "LVUPEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == MISSIONMAKEITEM.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "MISSIONMAKEITEM.Initialize");
		return FALSE;
	}

	if(FALSE == RANDOMITEMBOARDEVENT.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMBOARD_EVENT, DB_DATA_LOAD, FAIL, "RANDOMITEMBOARDEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == GAMEPLAYEVENT.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEPLAYEVENT, DB_DATA_LOAD, FAIL, "GAMEPLAYEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == USERAPPRAISAL.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_USER_APPRAISAL, DB_DATA_LOAD, FAIL, "USERAPPRAISAL.Initialize");
		return FALSE;
	}

	if(FALSE == MISSIONEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_MISSIONEVENT, DB_DATA_LOAD, FAIL, "MISSIONEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == MISSIONBINGOEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_MISSIONBINGOEVENT, DB_DATA_LOAD, FAIL, "MISSIONBINGOEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == SONOFCHOICE.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SONOFCHOICEEVENT, DB_DATA_LOAD, FAIL, "SONOFCHOICE.Initialize");
		return FALSE;
	}

	if(FALSE == SPECIALAVATARPIECE.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIAL_AVATAR_PIECE_EVENT, DB_DATA_LOAD, FAIL, "SPECIALAVATARPIECE.Initialize");
		return FALSE;
	}

	SShopItemInfo ArrowItemInfo;
	if(FALSE == m_pItemShop->GetItemByPropertyKind(ITEM_PROPERTY_KIND_RANDOM_ARROW_ITEM , ArrowItemInfo) ||
		FALSE == RANDOMARROW.Initialize(ArrowItemInfo.iItemCode0))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMARROW_EVENT, DB_DATA_LOAD, FAIL, "RANDOMARROW.Initialize");
		return FALSE;
	}

	SShopItemInfo DevilItemInfo_Normal;
	SShopItemInfo DevilItemInfo_Primium;
	if(FALSE == m_pItemShop->GetItemByPropertyKind(ITEM_PROPERTY_KIND_DEVILTEMTATION_NORMAL_ITEM, DevilItemInfo_Normal) ||
		FALSE == m_pItemShop->GetItemByPropertyKind(ITEM_PROPERTY_KIND_DEVILTEMTATION_PRIMIUM_ITEM, DevilItemInfo_Primium) ||
		FALSE == DEVILTEMTATION.Initialize(DevilItemInfo_Normal.iItemCode0, DevilItemInfo_Primium.iItemCode0))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL,"DEVILTEMTATION.Initialize");
		return FALSE;
	}

	if(FALSE == VENDINGMACHINE.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_VENDINGMACHINEEVENT, DB_DATA_LOAD, FAIL, "VENDINGMACHINE.Initialize()");
		return FALSE;
	}

	if(FALSE == PURCHASEGRADE.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_PURCHASE_GRADE, DB_DATA_LOAD, FAIL, "PURCHASEGRADE.Initialize()");
		return FALSE;
	}

	if(FALSE == USERCHOICEMISSIONEVENT.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_USERCHOICE_MISSIONEVENT, DB_DATA_LOAD, FAIL, "USERCHOICEMISSIONEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == RANDOMITEMTREEEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMTREE_EVENT, DB_DATA_LOAD, FAIL, "RANDOMITEMTREEEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == EXPBOXITEMEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EXPBOXITEM_EVENT, DB_DATA_LOAD, FAIL, "EXPBOXITEMEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == RANDOMITEMGROUPEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMGROUP_EVENT, DB_DATA_LOAD, FAIL, "RANDOMITEMGROUPEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == SALERANDOMITEMEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SALE_RANDOMITEM_EVENT, DB_DATA_LOAD, FAIL, "SALERANDOMITEMEVENT.Initialize");
		return FALSE;

	}

	if(FALSE == RAINBOWWEEKEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_RAINBOW_WEEK_EVENT, DB_DATA_LOAD, FAIL, "RAINBOWWEEKEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == GAMEOFDICEEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEOFDICE, DB_DATA_LOAD, FAIL, "GAMEOFDICEEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == WELCOMEUSEREVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_WELCOME_USER_EVENT, DB_DATA_LOAD, FAIL, "WELCOMEUSEREVENT.Initialize");
		return FALSE;
	}

	if( FALSE == RANKMATCH.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "RANKMATCH.Initialize");
		return FALSE;

	}

	if( FALSE == PCROOMEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_PCROOM_EVENT, DB_DATA_LOAD, FAIL, "PCROOMEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == CASHITEMREWARDEVENT.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_CASHITEM_REWARD_EVENT, DB_DATA_LOAD, FAIL, "CASHITEMREWARDEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == PASSWORDEVENT.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_SET_PASSWORD_EVENT, DB_DATA_LOAD, FAIL, "PASSWORDEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == HALLOWEENGAMEPLAYEVENT.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_HALLOWEENGAMEPLAYEVENT, DB_DATA_LOAD, FAIL, "HALLOWEENGAMEPLAYEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == SHOPPINGFESTIVAL.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGFESTIVAL, DB_DATA_LOAD, FAIL,"SHOPPINGFESTIVAL.Initialize");
		return FALSE;
	}

 	if( FALSE == HALFPRICE.Initialize())
 	{
 		WRITE_LOG_NEW(LOG_TYPE_HALFPRICE, DB_DATA_LOAD, FAIL,"HALFPRICE.Initialize");
 		return FALSE;
 	}

	if( FALSE == LOTTOEVENT.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_LOTTOEVENT, DB_DATA_LOAD, FAIL, "LOTTOEVENT.Initialize");
		return FALSE;
	}

	if( FALSE == DEVELOPEREVENT.Initialize() )
	{
		WRITE_LOG_NEW(LOG_TYPE_DEVELOPEREVENT, DB_DATA_LOAD, FAIL, "DEVELOPEREVENT.Initialize");
		return FALSE;
	}

	if (FALSE == DISCOUNTITEMEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_DISCOUNTITEM_EVENT, DB_DATA_LOAD, FAIL,"DISCOUNTITEMEVENT.Initialize");
		return FALSE;
	}

	if (FALSE == RANDOMITEMDRAWEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMDRAW_EVENT, DB_DATA_LOAD, FAIL,"RANDOMITEMDRAWEVENT.Initialize");
		return FALSE;
	}

	if (FALSE == OPENBOXEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_OPENBOX_EVENT, DB_DATA_LOAD, FAIL,"OPENBOXEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == NEWBIE.InitializeNewbieBenefit())
	{
		WRITE_LOG_NEW(LOG_TYPE_NEWBIEBENEFIT, DB_DATA_LOAD, FAIL, "NEWBIE.InitializeNewbieBenefit");
		return FALSE;
	}

	if(FALSE == FRIENDINVITE.InitializeFriendInvite())
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "FRIENDINVITE.InitializeFriendInvite");
		return FALSE;
	}

	if( FALSE == SHUTDOWNUSER.Initialize(SERVER_TYPE_NORMAL))
	{
		WRITE_LOG_NEW(LOG_TYPE_SHUTDOWN, DB_DATA_LOAD, FAIL,"SHUTDOWNUSER.Initialize");
		return FALSE;
	}

	if( FALSE == GAMEPLAYLOGINEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_GAMEPLAY_LOGIN_EVENT, DB_DATA_LOAD, FAIL, "GAMEPLAYLOGINEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == POTIONMAKEEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_POTION_MAKING_EVENT, DB_DATA_LOAD, FAIL, "POTIONMAKEEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == PRIMONGO.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_PRIMONGO_EVENT, DB_DATA_LOAD, FAIL, "PRIMONGO.Initialize");
		return FALSE;
	}

	if(FALSE == PRIVATEROOM.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_PRIVATEROOM_EVENT, DB_DATA_LOAD, FAIL, "PRIVATEROOM.Initialize");
		return FALSE;
	}

	if(FALSE == HIPPOCAMPUS.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_HIPPOCAMPUS_EVENT, DB_DATA_LOAD, FAIL, "HIPPOCAMPUS.Initialize");
		return FALSE;
	}

	if(FALSE == STEELBAGMISSION.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_STEELBAG_MISSION_EVENT, DB_DATA_LOAD, FAIL, "STEELBAGMISSION.Initialize");
		return FALSE;
	}

	if(FALSE == COMEBACK.InitalizeComebackBenefit())
	{
		WRITE_LOG_NEW(LOG_TYPE_COMEBACKBENEFIT, DB_DATA_LOAD, FAIL, "COMEBACK.InitalizeComebackBenefit");
		return FALSE;
	}

	if(FALSE == POWERUPCAPSULE.InitializePowerupCapsule())
	{
		WRITE_LOG_NEW(LOG_TYPE_POWERUP_CAPSULE, DB_DATA_LOAD, FAIL, "POWERUPCAPSULE.InitializePowerupCapsule");
		return FALSE;
	}

	if(FALSE == UNLIMITEDTATOO.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_UNLIMITED_TATOO_EVENT, DB_DATA_LOAD, FAIL, "UNLIMITEDTATOO.Initialize");
		return FALSE;
	}

	if(FALSE == PVEMANAGER.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_PVE, DB_DATA_LOAD, FAIL, "PVEMANAGER.Initialize");
		return FALSE;
	}

	if(FALSE == COVET.InitializeCovet())
	{
		WRITE_LOG_NEW(LOG_TYPE_COVET_EVENT, DB_DATA_LOAD, FAIL, "COVET.InitializeCovet");
		return FALSE;
	}

	if(FALSE == PROMISE.InitializePromise())
	{
		WRITE_LOG_NEW(LOG_TYPE_PROMISE, DB_DATA_LOAD, FAIL, "PROMISE.InitializePromise");
		return FALSE;
	}

	if(FALSE == MAGICMISSIONEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_MAGIC_MISSION, DB_DATA_LOAD, FAIL, "MAGICMISSIONEVENT.InitializePromise");
		return FALSE;
	}

	if(FALSE == SHOPPINGGOD.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGGOD_EVENT, DB_DATA_LOAD, FAIL,"SHOPPINGGOD.Initialize");
		return FALSE;
	}

	if(FALSE == SPECIALPIECE.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALPIECE, DB_DATA_LOAD, FAIL,"SPECIALPIECE.Initialize");
		return FALSE;
	}

	if(FALSE == PACKAGEITEMEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_PACKAGEITEM_EVENT, DB_DATA_LOAD, FAIL,"PACKAGEITEMEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == WHITEDAY.InitializeWhiteday())
	{
		WRITE_LOG_NEW(LOG_TYPE_WHITEDAY_EVENT, DB_DATA_LOAD, FAIL,"WHITEDAY.InitializeWhiteday");
		return FALSE;
	}

	if(FALSE == SELECTCLOTHESEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SELECTCLOTHES_EVENT, DB_DATA_LOAD, FAIL,"SELECTCLOTHESEVENT.InitializeWhiteday");
		return FALSE;
	}

	if(FALSE == SUBCHARDEVMANAGER.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SUBCHAR_DEVELOP, DB_DATA_LOAD, FAIL,"SUBCHARDEVMANAGER.Initialize");
		return FALSE;
	}

	if(FALSE == SUMMERCANDYEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SUMMERCANDY_EVENT, DB_DATA_LOAD, FAIL,"SUMMERCANDYEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == ITEMSHOPPACKAGEITEMEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEMSHOP_PACKAGEITEM_EVENT, DB_DATA_LOAD, FAIL,"ITEMSHOPPACKAGEITEMEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == DAILYLOGINREWARDEVENT.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL,"DAILYLOGINREWARDEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == PREMIUMPASSEVENT.Initialize(this))
	{
		WRITE_LOG_NEW(LOG_TYPE_PREMIUM_PASS_EVENT, DB_DATA_LOAD, FAIL,"PREMIUMPASSEVENT.Initialize");
		return FALSE;
	}

	if(FALSE == SECRETSHOP.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_SECRET_SHOP, DB_DATA_LOAD, FAIL,"SECRETSHOP.Initialize");
		return FALSE;
	}

	if(FALSE == SPEECHBUBBLE.InitializeSpeechBubble(GetItemShop()))
	{
		WRITE_LOG_NEW(LOG_TYPE_SPEECH_BUBBLE, DB_DATA_LOAD, FAIL,"SPEECHBUBBLE.InitializeSpeechBubble");
		return FALSE;
	}

	if(FALSE == TRANSFERJOYCITYPACKAGE.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITYPACKAGE, DB_DATA_LOAD, FAIL,"TRANSFERJOYCITYPACKAGE.Initialize");
		return FALSE;
	}

	if(FALSE == TRANSFERJOYCITYSHOP.Initialize())
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITYSHOP, DB_DATA_LOAD, FAIL,"TRANSFERJOYCITYSHOP.Initialize");
		return FALSE;
	}

	if(FALSE == TRANSFERJOYCITY100DREAM.Initialize(SERVER_TYPE_NORMAL))
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITY100DREAM, DB_DATA_LOAD, FAIL,"TRANSFERJOYCITY100DREAM.Initialize");
		return FALSE;
	}

	if( FALSE == UpdateCurrentDBDate() )
		return FALSE;

	WRITE_LOG_NEW(LOG_TYPE_SYSTEM, SERVER_START, SUCCESS);	

	CreateTimer();

	m_bEnable = TRUE;	// 이넘은 항상 마지막에 있어야 한다. Server가 준비됐다는 의미기 때문에

#ifdef _DEBUG
	printf("GameServer Start Success!");
#endif

	return TRUE;
}

BOOL CFSGameServer::LoadDataForContentsManager()
{
	return CONTENTSMANAGER.Initialize(GetProcessID());
}

void CFSGameServer::OnClose()
{
	CNexusServer::OnClose();

	if( OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_DISCOUNT_ITEM) )
	{
		m_mapUserTotal.Lock(); 
		mPosition pEnd = m_mapUserTotal.GetEnd(); 
		for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
		{
			CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
			if(NULL != pUser)
			{
				pUser->GetUserDiscountItemEvent()->UpdateItemPrice(TRUE);
			}
		}
		m_mapUserTotal.Unlock(); 
	}

	BEGIN_LOCK
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));

		m_mapUserTotal.Lock(); 
		mPosition pEnd = m_mapUserTotal.GetEnd(); 
		for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
		{
			CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
			if(NULL != pUser)
			{
				pUser->GetUserFriendInvite()->UpdateAllMissionUpdate(pODBCBase);
				pUser->GetUserComebackBenefit()->SaveComebackBenefit(FALSE, pODBCBase);
				pUser->GetUserCovet()->CheckAndSaveCovetJinn(pODBCBase);
				pUser->SavePromiseUserInfo(pEventODBC);
				pUser->GetUserSummerCandyEvent()->SaveUserInfo(pEventODBC);
			}
		}
		m_mapUserTotal.Unlock();
	END_LOCK

	int iErrorCode = 0;

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC(ODBC_LOG);
	CHECK_NULL_POINTER_VOID( pLogODBC );
	
	time_t tCurrentDBTime = -1;
	DECLARE_INIT_TCHAR_ARRAY(szSessionKey, MAX_SESSIONKEY_LENGTH+1);	
	pLogODBC->USER_DeleteSession(0, GetProcessID(), 0, szSessionKey, tCurrentDBTime, iErrorCode);
}

void CFSGameServer::OnCheckUserTimeOut()
{
	DWORD dwTick = GetTickCount(); 
	m_mapUserTotal.Lock(); 
	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			if ( NULL != pUser->GetClient() && pUser->GetClient()->CheckTimeOut(dwTick) ) 			
			{
				if(TRUE == CFSGameServer::GetInstance()->IsCheckLog())
				{
					WRITE_LOG_NEW(LOG_TYPE_USER, USER_DESTORY, NONE, "NETLOG\tServer Timeout");
				}

				((CFSGameClient *)pUser->GetClient())->DisconnectClient();
			}
		}
	}
	m_mapUserTotal.Unlock(); 
}

void CFSGameServer::OnInitLimitedEdtionItem()
{
	SYSTEMTIME systemTime;
	::GetLocalTime(&systemTime);

	if(systemTime.wHour == 0 && systemTime.wMinute == 0)
	{
		vector<int> vItemCode;
		m_pLimitedEditionItemManager->InitLimitedEdtionItem(vItemCode);

		int iItemCode = 0, iCurrentCount = 0, iLimitedEditionCount = 0;
		CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);

		vector<int>::iterator itr = vItemCode.begin();
		vector<int>::iterator endItr = vItemCode.end();
		for( ; itr != endItr; ++itr)
		{
			iItemCode = *itr;
			iCurrentCount = m_pLimitedEditionItemManager->GetCurrentCount(iItemCode);
			iLimitedEditionCount = m_pLimitedEditionItemManager->GetMaxCount(iItemCode) - iCurrentCount;

			if(pCenterSvrProxy)
			{
				CPacketComposer PacketComposer(G2S_LIMITED_EDITION_ITEM_COUNT_REQ);
				SG2S_LIMITED_EDITION_ITEM_COUNT info;
				info.btKind = 0;
				info.iItemCode = iItemCode;
				info.iCurrentCount  = iCurrentCount;
				info.iLimitedEditionCount = iLimitedEditionCount;
				PacketComposer.Add( (BYTE*)&info,  sizeof(SG2S_LIMITED_EDITION_ITEM_COUNT));
				pCenterSvrProxy->Send(&PacketComposer);
			}

			CPacketComposer PacketComposer(S2C_LIMITED_EDITION_ITEM_COUNT_INFO);
			PacketComposer.Add(iItemCode);
			PacketComposer.Add(iLimitedEditionCount);
			CFSGameServer::GetInstance()->BroadCast(PacketComposer);
		}
	}
}

void CFSGameServer::UpdateChannelBuffTime()
{
	BYTE btKind = 0; 
	int iNum = 0, iChangedNum = 0, iUseCount = 0;

	if(FALSE == CHANNELBUFFMANAGER.UpdateChannelBuffTime(GetProcessID(), btKind, iUseCount))
	{
		return;
	}

	if(btKind == CHANNEL_BUFF_UPDATE_TYPE_STATUS || btKind == CHANNEL_BUFF_UPDATE_TYPE_REMOVE)
	{
		CPacketComposer PacketComposer(S2C_CHANNEL_BUFF_INFO_NOTIFY);
		CHANNELBUFFMANAGER.MakeChannelBuffTimeInfo(PacketComposer);

		BroadCast(PacketComposer);
	}

	if(btKind == CHANNEL_BUFF_UPDATE_TYPE_STATUS)
	{
		CPacketComposer PacketComposer(G2S_USE_CHANNEL_BUFF_ITEM_REQ);
		if(CHANNELBUFFMANAGER.MakeUserChannelBuffChangedInfo(PacketComposer, GetProcessID()) == TRUE)
		{
			CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
			if(pCenterSvrProxy)
				pCenterSvrProxy->Send(&PacketComposer);
		}
	}
}

void CFSGameServer::OnTimer()
{
	/////////////////////////// 서버 타이머 처리,증가 및 초기화 부분 ///////////////////////////
	// * 10분 주기로 초기화 됨. 중복 실행 안되는 내용을 이 안에 넣기 바랍니다(변경시 공지 바람).
	// * 중요!!!!!! TimeCount는 0으로 초기화 되며 이 함수에서 증가 부터 한 후 시간을 체크 합니다.
	// * 같은 시간에 중복 실행 되는 내용은 거의 없습니다. 부득이하게 추가 될 경우 주의 하세요.
	// * 함수 성격상 길어 질 수 있으므로 블럭 처리합니다.
	// * CONTENTS_시리즈 테이블을 이용하려면 CheckAndUpdateSingleContentsSetting()을 응용 하세요.

	IncreaseGameServerTimeCount();

	{
		// 5분 간격으로 수행 되는 시간 체크
		switch(GetGameServerTimeCount() TIME_COUNT_MINUTE_5)

		{
		case 110:
			{
				OnCheckUserTimeOut();
				break;
			}
		case 125:
			{
				CheckAndLoadRankList();
				break;
			}
		case 135:
			{
				CheckAndSetOptionWriteUserLogToDB();
				break;
			}
		case 145:
			{
				CheckContentsDefaultSetting();
				break;
			}
		case 155:
			{
				UpdateRecordBoard();
				break;
			}
		case 165:
			{
				CheckAndLoadIntensivePracticeRanking();
				break;
			}
		case 175:
			{
				CheckAndLoadUserAppraisal();
				break;
			}
		case 185:
			{
				CheckAndLoadGameOfDiceRankList();
				break;
			}
		case 195:
			{
				CheckAndUpdateUserAppraisalRanking();
				break;
			}
		case 225:
			{
				CheckAndLoadMiniGameZoneRankList();
				break;
			}
		case 235:
			{
				CheckAndSendFriendInviteKingRankList();
				break;
			}
		case 250:
			{
				CheckAndLoadHippocampusRankList();
				break;
			}
		case 270:
			{
				CheckComebackBenefitAttendance();				
			}
		case 280:
			{
				PVEMANAGER.ReLoadRank(GetProcessID());
				break;
			}
		case 290:
			{
				SELECTCLOTHESEVENT.ReLoadRank(GetProcessID());
				break;
			}
		default:
			break;
		}

		switch(GetGameServerTimeCount() TIME_COUNT_MINUTE_1)

		{
		case 1:
			{
				OnEventTimer();
			}
			break;
		case 3:
			{
				SHUTDOWNUSER.OnTimer();
			}
			break;
		case 7:
			{
				CReloadDataManager::GetInstance().CheckReload();
			} 
			break;
		case 10:
			{
				OnInitLimitedEdtionItem();
			}
			break;
		case 13:
			{
				m_pItemShop->UpdateActiveSaleItemPrice();	//1분에 한번 현재 세일아이템 리스트 갱신
			} 
			break;
		case 16:
			{
				m_pServerList->UpdateServerState();
			}	
			break;
		case 19:
			{
				ResetIntensivePracticOnedayRecord();
			}
			break;
		case 22:
			{
				UpdateChannelBuffTime();
			}
			break;
		case 25:
			{
				EVENTDATEMANAGER.CheckSchedule();
			}
			break;
		case 28:
			{
				CheckCoachCardStatus();
			}
			break;
		case 29:
			{
				CheckItemShopEventButtonSchedule();
			}
			break;
		case 30:
			{
				UpdateLeagueModeSwitch();
				CheckLeagueModeStatus();
			}
			break;
		case 31:
			{
				CheckLeagueModeMatchOpenTime();	
			}
			break;
		case 35:
			{
				CheckHotGirlTime();
			}
			break;
		case 36:
			{
				CheckBasketBallEventButtonSchedule();
			}
			break;
		case 39:
			{
				FactionTimer();
			}
			break;
		case 41:
			{
				CheckEventAttendanceItemShopStatus();
			}
			break;
		case 43:
			{
				CheckEventAttendanceTodayItemReset();
			}
			break;
		case 46:
			{
				UpdateShoppingEventLoginTimeMission();
			}
			break;
		case 49:
			{
				UpdatePotionMakingEventLoginTimeMission();
			}
			break;
		case 52:
			{
				ResetSteelBagMission();
			}
			break;
		case 55:
			{
				UpdateDiscountItemEventItemPrice();
			}
			break;
			/*	TODO : Event 재사용시 확인 
				PUZZLES.CheckSchedule();
				RANDOMBOX.CheckSchedule();		
				THREEKINGDOMSEVENT.CheckSchedule();
				MISSIONSHOP.CheckSchedule();		
				CheckTodayHotDealStatus();
				
				CheckEventGoldenCrushStatus();
				CheckEventMatchingCardStatus();
				CHEERLEADEREVENT.CheckSchedule();
				LVUPEVENT.CheckSchedule();
			*/
		default:
			break;
		}

		switch(GetGameServerTimeCount() TIME_COUNT_SECOND_15)

		{
		case 1:		UpdateCurrentDBDate();		break; 
		default:	++m_tCurrentDBDate;	break;
		}
	}

	BILLING_GAME.UpdateBillingCheck( GetGameServerTimeCount() );

	AddMatchingPoolCareTimeCount();
	USERAPPRAISAL.DecreasePenaltyTime();
	GivePCRoomUserLoginTimeReward();
	DecreaseRankMatchLimitTime();
	SHOPPINGFESTIVAL.DecreaseOpenRemainTime();
	DISCOUNTITEMEVENT.UpdateCurrentStep();

	// TODO
	// FSLOBBY.CheckIntensivePracticeGameTime();

	// 타이머 초기화 3600초(1시간)
	if (GetGameServerTimeCount() >= TIME_COUNT_MAX)
	{
		ResetGameServerTimeCount();
	}
}

void	CFSGameServer::OnEventTimer()
{
	//1 이벤트 종료시
	vector<int> vecInactiveEventIndex;
	m_ComponentEventManager.CheckEventActive(vecInactiveEventIndex);
	for (int i = 0; i < vecInactiveEventIndex.size() ; ++i)
	{
		int iEventIndex = vecInactiveEventIndex[i];

		m_mapUserTotal.Lock();
		mPosition pEnd = m_mapUserTotal.GetEnd(); 
		for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
		{
			CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
			if(NULL == pUser)	continue;
			
			pUser->CheckEvent(PERFORM_TIME_EVENT_END, NULL, iEventIndex);
		}

		m_mapUserTotal.Unlock();
	}

	//2 자정에만
	SYSTEMTIME systemTime;
	::GetLocalTime(&systemTime);

	CHECK_CONDITION_RETURN_VOID(systemTime.wHour != 0 || systemTime.wMinute != 0);	

	m_mapUserTotal.Lock();
	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL == pUser)	continue;
			
		pUser->CheckEvent(PERFORM_TIME_SERVER_TIMER);
	}

	m_mapUserTotal.Unlock();
}

void	CFSGameServer::BroadCastShout(CPacketComposer& PacketComposer)
{
	m_mapUserTotal.Lock(); 
	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->Send(&PacketComposer); 
		}

	}
	m_mapUserTotal.Unlock(); 
}

void	CFSGameServer::BroadCast(CPacketComposer& PacketComposer, BOOL bExceptPlayingUser/* = FALSE*/)
{
	m_mapUserTotal.Lock(); 
	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			if (FALSE == bExceptPlayingUser || FALSE == pUser->IsPlaying())
				pUser->Send(&PacketComposer); 
		}
		
	}
	m_mapUserTotal.Unlock(); 
}

void CFSGameServer::BroadCastNotifyMsg(int iOutputType, LPCTSTR szMsg , int iMsgLength)
{	
	if ( iMsgLength <= 0 )
		return;

	CPacketComposer PacketComposer(S2C_ANNOUNCE); 
	PacketComposer.Add(&iOutputType);
	PacketComposer.Add(&iMsgLength);
	PacketComposer.Add((PBYTE)szMsg, iMsgLength);

	BroadCast(PacketComposer);
}

void CFSGameServer::BroadCastShoutMsg(char* szOperatorID, int iErrorCode, int iShoutItemMode, LPCTSTR szMsg)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	if(pGameODBC ==NULL)
	{
		WRITE_LOG_NEW(LOG_TYPE_CHAT, INVALED_DATA, CHECK_FAIL, "BroadCastShoutMsg::pGameODBC");
		return;
	}
	
	CScopedRefGameUser pUser(szOperatorID);
	if( pUser != NULL )
	{
		if( iErrorCode == 0 ) // 소모 
		{
			int iReturn = pUser->GetUserItemList()->CheckShoutItem(pGameODBC,iShoutItemMode);
			if( iReturn == 0 )
			{
				m_bShout = TRUE;
				m_dwShoutTime = GetTickCount();
				
				CPacketComposer PacketComposer(S2C_SHOUT);
				PacketComposer.Add(iErrorCode);
				PacketComposer.Add(iShoutItemMode);
				PacketComposer.Add((PBYTE)szOperatorID, MAX_GAMEID_LENGTH+1);	
				PacketComposer.Add((PBYTE)szMsg, MAX_CHATBUFF_LENGTH+1);

				BroadCast(PacketComposer);
			}
			else if( iReturn == -2 )
			{
				int iErrorCode2 = -2;
				CPacketComposer PacketComposer(S2C_SHOUT);
				PacketComposer.Add(iErrorCode2);
				PacketComposer.Add(iShoutItemMode);
				
				pUser->Send(&PacketComposer);
			}
			else if( iReturn == -4 )
			{
				int iErrorCode2 = -4;
				CPacketComposer PacketComposer(S2C_SHOUT);
				PacketComposer.Add(iErrorCode2);
				PacketComposer.Add(iShoutItemMode);
				pUser->Send(&PacketComposer);
			}
		}
		else if( iErrorCode == -1 )
		{
			CPacketComposer PacketComposer(S2C_SHOUT);
			PacketComposer.Add(iErrorCode);
			PacketComposer.Add(iShoutItemMode);
			pUser->Send(&PacketComposer);
		}
	}
	else
	{
		if( iErrorCode == 0 )
		{
			int iErrorCode2 = 0;
			CPacketComposer PacketComposer(S2C_SHOUT);
			PacketComposer.Add(iErrorCode2);
			PacketComposer.Add(iShoutItemMode);
			PacketComposer.Add((PBYTE)szOperatorID, MAX_GAMEID_LENGTH+1);	
			PacketComposer.Add((PBYTE)szMsg, MAX_CHATBUFF_LENGTH+1);
			
			BroadCast(PacketComposer);
		}
	}
}

BOOL CFSGameServer::CheckOpUser( LPCTSTR szOpUserID )
{
	if( m_pOpUserInfoList->CheckOPUser( szOpUserID ) == FALSE )
		return FALSE;
	
	return TRUE;
}

CFSGameUser* CFSGameServer::IsSameUserGameServer( LPCTSTR szUserID,int iPublisherIDIndex )
{
	m_mapUserTotal.Lock();
	mPosition pos = m_mapUserTotal.GetHead();
	mPosition endpos = m_mapUserTotal.GetEnd();
	for( ; pos != endpos ; pos = m_mapUserTotal.GetNextPosition(pos) )
	{
		CFSGameUser* pUser = (CFSGameUser*)pos->second;
		if( strcmp( pUser->GetUserID(), szUserID ) == 0 && pUser->GetPublisherIDIndex() == iPublisherIDIndex) // Channelling
		{
			m_mapUserTotal.Unlock();
			return pUser;
		}
	}
	
	m_mapUserTotal.Unlock();
	return NULL;
}

void CFSGameServer::AllUserForceOut()
{
	CPacketComposer PacketComposer(S2C_USER_FORCEOUT_REQ);
	m_mapUserTotal.Lock(); 
	
	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if( pUser && pUser->GetClient() )
			pUser->CheckAndSend(&PacketComposer, pUser->IsPlaying());
	}
	
	m_mapUserTotal.Unlock(); 
}
BOOL CFSGameServer::AllUserForceOutExceptMe(CFSGameUser* pUser)
{
	CPacketComposer PacketComposer(S2C_MANAGER_USER_KICK_RES);
	if(m_mapUserTotal.Lock() == FALSE)
		return FALSE;

	int iResult = -10;
	PacketComposer.Add(iResult);

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* posUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);

		if(posUser == pUser)
			continue;

		if(posUser != NULL)
		{
			CFSGameClient* pUserClient = (CFSGameClient*)posUser->GetClient();

			if(pUserClient)
			{
				pUserClient->Send(&PacketComposer);
				pUserClient->DisconnectClient();
			}
		}
	}
	m_mapUserTotal.Unlock(); 

	return TRUE;
}

BOOL CFSGameServer::AllUserSendPaper(int iSendGameIDIndex, char* szTitle, char* szText)
{
	CFSGameODBC* pODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pODBC);

	BOOL res = TRUE;

	//UserMap Lock 상태에서의 DB콜은 피하자.
	struct _UserInfoForPaper
	{
		int iGameIDIndex;
		char szGameID[MAX_GAMEID_LENGTH+1];
	};
	list<_UserInfoForPaper> listUser;

	_UserInfoForPaper UserInfo;

	m_mapUserTotal.Lock();
		mPosition pEnd = m_mapUserTotal.GetEnd(); 
		for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
		{
			CFSGameUser* posUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);

			if(posUser != NULL)
			{
				UserInfo.iGameIDIndex = posUser->GetGameIDIndex();
				strncpy_s(UserInfo.szGameID, MAX_GAMEID_LENGTH+1, posUser->GetGameID(), MAX_GAMEID_LENGTH);

				listUser.push_back(UserInfo);
			}
		}
	m_mapUserTotal.Unlock();

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	
	list<_UserInfoForPaper>::iterator iter = listUser.begin();
	list<_UserInfoForPaper>::iterator endIter = listUser.end();
	for (; iter != endIter; ++iter)
	{
		int index = 100;
		int flag = 0;
		if(pODBC->spAddPaper((*iter).iGameIDIndex, index, iSendGameIDIndex, szTitle, szText, flag) == 0)
		{
			if(pCenter) 
			{
				pCenter->SendUserNotify((*iter).szGameID, index, MAIL_NOTE);				
			}
		}
		else
		{
			res = FALSE;

			WRITE_LOG_NEW(LOG_TYPE_PAPER, DB_DATA_UPDATE, FAIL, "AllUserSendPaper - GameIDIndex:11866902", (*iter).iGameIDIndex);
}
	return res;
}

int CFSGameServer::CheckShoutTime(int iShoutItemMode)
{
	if( GetShout() == FALSE )
	{
		return 0;
	}
	
	int iLastTime = 0;
	int iEnableTime = 0;
	DWORD dwTick = GetTickCount(); 
	
	DWORD dwRemainTime = dwTick - m_dwShoutTime  ;

	if (SHOUT_MODE_BASIC == iShoutItemMode)
	{
		iLastTime = SHOUT_ENABLE_TIME_NOMAL - (int)dwRemainTime;
	}else if (SHOUT_MODE_RED == iShoutItemMode || SHOUT_MODE_GOLD == iShoutItemMode)
	{
		iLastTime = SHOUT_ENABLE_TIME_VIPANDMYSTICAL - (int)dwRemainTime;
	}else if (SHOUT_MODE_VIP_2 == iShoutItemMode || SHOUT_MODE_VIP_3 == iShoutItemMode)
	{
		iLastTime = SHOUT_ENABLE_TIME_COLORANDPLATINUM - (int)dwRemainTime;
	}else
	{
		iLastTime = SHOUT_ENABLE_TIME_SPECIAL - (int)dwRemainTime;
	}
	
	if( iLastTime < 0 )
	{
		iLastTime = 0;
	}
	else
	{
		iLastTime /= 1000;
	}
	
	TRACE( " 외치기 시간 : dwTick - 11866902, m_dwShoutTime - 0, iLastTime - 18227200 \n", dwTick, m_dwShoutTime, iLastTime );
iLastTime;
}

void CFSGameServer::SetCodePage()
{
	m_GameIDStringChecker.RegisterCodePage(949);
}

void CFSGameServer::BroadCastSendGMMsg( int iMsgType, LPCTSTR szMsg )
{
	LockUserManager(); 
	
	CPacketComposer PacketComposer(S2C_REALTIME_MESSAGE_RES);
	PacketComposer.Add(&iMsgType);
	PacketComposer.Add((PBYTE)szMsg, MAX_GM_MSG_LENGTH + 1 );
	
	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->Send(&PacketComposer); 
		}
		
	}
	
	UnlockUserManager();
}

void CFSGameServer::SetClubCheerLeaderCode(int iClubSN, int iChangedCheerLeaderCode, BYTE btTypeNum)
{
	LOBBYCLUBUSER.SetClubCheerLeaderCode(iClubSN, iChangedCheerLeaderCode, btTypeNum);
}

void CFSGameServer::SetClubCheerLeaderCode(int iClubSN, int iExpiredCheerLeaderCode, int iChangedCheerLeaderCode)
{
	LOBBYCLUBUSER.SetClubCheerLeaderCode(iClubSN, iExpiredCheerLeaderCode, iChangedCheerLeaderCode);
}

BOOL CFSGameServer::IsNoWithdrawChannel()
{
	if( m_bNoWithdraw == TRUE && m_ContentsConfig.iNoWithdraw == 1 )
	{
		return TRUE;
	}
	
	return FALSE;
}

// 20090917 Leveling
BOOL CFSGameServer::LoadLevelingCurve()
{
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_BOOL( pNexusODBC );

	m_vecLevelingCurve.clear();
	if( ODBC_RETURN_FAIL == ((CFSGameODBC *)pNexusODBC)->CONFIG_GetConfigLv(m_vecLevelingCurve))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, FAIL, "CONFIG_GetConfigLv");
		return FALSE;
	}	

	if (m_vecLevelingCurve.size() != GAME_LVUP_MAX)
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, CHECK_FAIL, "LoadLevelingCurve");
		return FALSE;
	}

	WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, SUCCESS, "LoadLevelingCurve");
	return TRUE;
}
// End

BOOL CFSGameServer::LoadMaxLevelPlayCountData()
{
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_BOOL( pNexusODBC );

	m_vecMaxLevelStepLv.clear();

	if( ODBC_RETURN_FAIL == ((CFSGameODBC *)pNexusODBC)->CONFIG_GetConfigMaxLevelStep(m_vecMaxLevelStepLv))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, FAIL, "CONFIG_GetConfigMaxLevelStep");
		return FALSE;
	}	

	return TRUE;
}

// 20090917 Leveling
int  CFSGameServer::GetLvUpExp(int iLv)
{
	if (iLv < 1)
	{
		return -1;
	}
	if (iLv > GAME_LVUP_MAX)
	{
		return GAME_EXP_MAX;
	}
	
	return m_vecLevelingCurve[iLv-1];
}
// End

BOOL CFSGameServer::GetMaxLevelStepUpLv(int iMaxLevelStep, int& iRequireLv)
{
	if(iMaxLevelStep < 1 || m_vecMaxLevelStepLv.size() < iMaxLevelStep )
	{
		return  FALSE;
	}

	iRequireLv = m_vecMaxLevelStepLv[iMaxLevelStep-1].iLv;
	return TRUE;
}

int CFSGameServer::GetMaxLevelStepUpRewardIndex(int iMaxLevelStep)
{
	if(iMaxLevelStep < 1 || m_vecMaxLevelStepLv.size() < iMaxLevelStep )
	{
		return  0;
	}

	return m_vecMaxLevelStepLv[iMaxLevelStep-1].iRewardIndex;
}

int CFSGameServer::GetMaxLevelStepListSize()
{
	return m_vecMaxLevelStepLv.size();
}

void CFSGameServer::SetConfig()
{
	SetOptionFlag();
	SetStatusUpTable();
	SetLankUpManagerConfig();
}

void CFSGameServer::SetOptionFlag()
{
	float fEnable = 0;

	// 스탯보정 Config 설정
	if(TRUE == CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_STAT_MOD, CONTENTS_TYPE_STAT_MOD_ENABLE, fEnable))
	{
		SetOptionStatusMod((BOOL)fEnable);
	}
}

void CFSGameServer::SetOptionStatusMod(BOOL bFlag)
{
	m_bOptionFlagStatusMod = bFlag;
}

BOOL CFSGameServer::GetOptionStatusMod()
{
	return m_bOptionFlagStatusMod;
}

void CFSGameServer::SetStatusUpTable()
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	if(TRUE == GetOptionStatusMod())
	{
		pGameODBC->AVATAR_GetUserStatConfig(m_StatusUpTable);
	}
}

void CFSGameServer::GetStatusUp(int iPosition, int iLv, int* iStatus, int iNum)
{
	m_StatusUpTable.GetStatusUp(iPosition, iLv, iStatus, iNum);
}

void CFSGameServer::GetStatusUp(int iPosition, int iStartLv, int iEndLv, int* iStatus, int iNum)
{
	m_StatusUpTable.GetStatusUp(iPosition, iStartLv, iEndLv, iStatus, iNum);
}
// End

void CFSGameServer::SetLankUpManagerConfig()
{
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );

	m_LankUpManager.ResetLankUpRequire();

	if(ODBC_RETURN_FAIL == ((CFSGameODBC*)pNexusODBC)->CONFIG_GetLankUp(&m_LankUpManager))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANK, DB_DATA_LOAD, FAIL, "CONFIG_GetLankUp");
		return;
	}	

	if(FALSE == m_LankUpManager.CheckLankUpSetting())
	{
		WRITE_LOG_NEW(LOG_TYPE_RANK, DB_DATA_LOAD, CHECK_FAIL, "SetLankUpManagerConfig");
		return;
	}

	WRITE_LOG_NEW(LOG_TYPE_RANK, DB_DATA_LOAD, SUCCESS, "SetLankUpManagerConfig");
}

int CFSGameServer::GetFameLankUpRequire(int iFameLevel)
{
	return m_LankUpManager.GetLankUpRequire(LANKUP_FAME, iFameLevel);
}

void CFSGameServer::BroadCastMatchServerConnect(CMatchSvrProxy* pProxy)
{
	CHECK_NULL_POINTER_VOID(pProxy);

	SS2C_MATCH_SERVER_CONNECT info;
	info.btMatchType = pProxy->GetMatchType();

	m_mapUserTotal.Lock(); 
	for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
	{
		CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			if (MATCH_TYPE_RATING != pProxy->GetMatchType() ||
					(MATCH_TYPE_RATING == pProxy->GetMatchType() &&
					pProxy->IsValidLv(pUser->GetCurUsedAvtarLv()))
				)
			{
				info.btAutoTeam = pProxy->IsAutoTeam();
				pUser->CheckAndSend(S2C_MATCH_SERVER_CONNECT, &info, sizeof(SS2C_MATCH_SERVER_CONNECT));
			}
		}
	}
	m_mapUserTotal.Unlock();
}

void CFSGameServer::BroadCastMatchServerLost(CMatchSvrProxy* pProxy)
{
	CHECK_NULL_POINTER_VOID(pProxy);

	SS2C_MATCH_SERVER_LOST info;
	info.btMatchType = pProxy->GetMatchType();

	m_mapUserTotal.Lock(); 
	for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
	{
		CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			if (MATCH_TYPE_RATING != pProxy->GetMatchType() ||
					(MATCH_TYPE_RATING == pProxy->GetMatchType() &&
					pProxy->IsValidLv(pUser->GetCurUsedAvtarLv()))
				)
			{
				if (pProxy->GetProcessID() == pUser->GetMatchLocation())
				{
					pUser->GetClient()->SetState(NEXUS_LOBBY);

					info.btGoToLobby = TRUE;
				}
				else
				{
					info.btGoToLobby = FALSE;
				}

				pUser->Send(S2C_MATCH_SERVER_LOST, &info, sizeof(SS2C_MATCH_SERVER_LOST));
			}
		}
	}
	m_mapUserTotal.Unlock();
}

void CFSGameServer::BroadCastChatServerStatus(int iServerStatus)
{
	CPacketComposer PacketComposer(S2C_CHAT_SERVER_STATUS_NOT);
	PacketComposer.Add((BYTE)iServerStatus);

	m_mapUserTotal.Lock(); 
	for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
	{
		CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->Send(&PacketComposer);
		}
	}
	m_mapUserTotal.Unlock();
}

void CFSGameServer::UpdateRecordBoard()
{
	CheckAndResetRecordBoard();
	CheckAndUpdateRecordBoardRanking();
}

void CFSGameServer::CheckAndResetRecordBoard()
{
	CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC( ODBC_RANK );
	CHECK_NULL_POINTER_VOID( pRankODBC );

	int iStatusFlag = 0;
	int iProcessID = GetProcessID();

	if(ODBC_RETURN_SUCCESS == pRankODBC->spFSCheckFlagStatus(STATUSFLAG_RECORDBOARD_RESET, iStatusFlag, iProcessID))
	{
		if(iStatusFlag == 1)
		{
			m_mapUserTotal.Lock(); 
			for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
			{
				CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
				if(NULL != pUser)
				{
					pUser->GetUserRecordBoard()->ResetRecordBoard();
				}
			}
			m_mapUserTotal.Unlock();

			RECORDBOARDMANAGER.RemoveRanking();
			RECORDBOARDMANAGER.LoadSeasonSchedule();

			pRankODBC->spFSSetFlagStatus(STATUSFLAG_RECORDBOARD_RESET, 0, iProcessID);
		}
	}
}

void CFSGameServer::BroadCastLeagueServerConnect( CLeagueSvrProxy* pProxy )
{
	CHECK_NULL_POINTER_VOID(pProxy);

	SS2C_MATCH_SERVER_CONNECT info;
	info.btMatchType = (BYTE)MATCH_TYPE_RANKMATCH;
	info.btAutoTeam = pProxy->IsAutoTeam();

	m_mapUserTotal.Lock(); 
	for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
	{
		CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->CheckAndSend(S2C_MATCH_SERVER_CONNECT, &info, sizeof(SS2C_MATCH_SERVER_CONNECT));
		}
	}
	m_mapUserTotal.Unlock();
}

void CFSGameServer::BroadCastLeagueServerLost( CLeagueSvrProxy* pProxy )
{
	CHECK_NULL_POINTER_VOID(pProxy);

	SS2C_MATCH_SERVER_LOST info;
	info.btMatchType = MATCH_TYPE_RANKMATCH;

	m_mapUserTotal.Lock(); 
	for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
	{
		CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			if (pProxy->GetProcessID() == pUser->GetMatchLocation())
			{
				pUser->GetClient()->SetState(NEXUS_LOBBY);
				pUser->SetMatchLocation(-1);
				info.btGoToLobby = TRUE;
			}
			else
			{
				info.btGoToLobby = FALSE;
			}

			pUser->Send(S2C_MATCH_SERVER_LOST, &info, sizeof(SS2C_MATCH_SERVER_LOST));
		}
	}
	m_mapUserTotal.Unlock();
}

void CFSGameServer::CheckAndUpdateRecordBoardRanking()
{
	CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC( ODBC_RANK );
	CHECK_NULL_POINTER_VOID( pRankODBC );

	int iStatusFlag = 0;
	int iProcessID = GetProcessID();

	if(ODBC_RETURN_SUCCESS == pRankODBC->spFSCheckFlagStatus(STATUSFLAG_RECORDBOARD_RANKING, iStatusFlag, iProcessID))
	{
		if(iStatusFlag == 1)
		{
			RECORDBOARDMANAGER.RemoveRanking();
			if(RECORDBOARDMANAGER.LoadRecordBoardRanking() == TRUE)
			{
				pRankODBC->spFSSetFlagStatus(STATUSFLAG_RECORDBOARD_RANKING, 0, iProcessID);
			}
		}
	}
}

void CFSGameServer::CheckAndLoadRankList()
{
	CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_NULL_POINTER_VOID( pRankODBC );

	int iStatusFlag = 1;
	int iProcessID = GetProcessID();

	if(pRankODBC != NULL && ODBC_RETURN_SUCCESS == pRankODBC->spFSCheckFlagStatus(0, iStatusFlag, iProcessID))
	{
		if(iStatusFlag == 1)
		{
			SYSTEMTIME SystemTime;
			::GetLocalTime(&SystemTime);

			if( 1 == SystemTime.wDay )
			{
				if( TRUE == RANK.RecallLastMonthRank() )
				{
					ProcessSeasonRankAchievement();
				}
			}

			if(TRUE == RANK.LoadRank(pRankODBC))
			{
				RANK.RemoveAllAvatarFeature();
				pRankODBC->spFSSetFlagStatus(0, 0, iProcessID);

				m_mapUserTotal.Lock(); 
				for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
				{
					CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
					if(NULL != pUser)
					{
						pUser->InitTopRatingCharacter();
					}
				}
				m_mapUserTotal.Unlock();
			}
		}
	}
}

void CFSGameServer::ProcessSeasonRankAchievement()
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC);

	typedef multimap<int,int> GameIDMap;
	typedef GameIDMap::iterator mPosition;
	int iGameIDIndex;

	GameIDMap mapTopRankerGameID;
	RANK.GetTopRankAvatarEveryCategory(mapTopRankerGameID, AVATAR_LV_GRADE_PRO);

	for (int iLoopIndex = SEASON_RANK_POINT; iLoopIndex < MAX_SEASON_RANK_ACHIEVEMENT_COUNT; iLoopIndex++)
	{
		GameIDMap::iterator itrFind = mapTopRankerGameID.find(iLoopIndex);

		if (itrFind != mapTopRankerGameID.end())
		{
			iGameIDIndex = itrFind->second;

			CScopedRefGameUser pUser(iGameIDIndex);
			if (pUser != NULL)
			{
				vector<int> vNewlyReachedAchievementIndex;
				vector<int> vAnnounceAchievementIndex;

				vNewlyReachedAchievementIndex.push_back(GetAchievementIndexWithSeasonRankIndex(iLoopIndex)); 

				if (TRUE == pUser->ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex))
				{
					SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
					info.iGameIDIndex = pUser->GetGameIDIndex();

					strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), _countof(info.szGameID)-1);
					info.iAchievementCount = vAnnounceAchievementIndex.size();

					for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
					{
						info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
					}

					CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
					PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

					CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
					if (pMatch != NULL)
					{
						pMatch->Send(&PacketComposer);
					}
				}
			}
			else
			{
				SAchievementLog sAchievementLog;
				sAchievementLog.iAchievementIndex = GetAchievementIndexWithSeasonRankIndex(iLoopIndex);
				sAchievementLog.iAchievementLogGroupIndex = ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO;

				BYTE btStorageType =ACHIEVEMENTMANAGER.GetAchievementStorageType(sAchievementLog.iAchievementIndex);

				pGameODBC->ACHIEVEMENT_InsertCompletedAchievement(-1, iGameIDIndex, sAchievementLog, btStorageType);
			}
		}
	}

	vector<int> vTopRankerUserID;
	vector<int> vTopRankerGameID;
	int iUserIDIndex;
	for(int iCategory = SEASON_RANK_USER_RATING_GRADE; iCategory <= SEASON_RANK_SW_RATING_GRADE; ++iCategory)
	{
		for(int iRank = USER_TOP_RATING_RANK_1; iRank <= USER_TOP_RATING_RANK_4; ++iRank)
		{
			int iMinRank = iRank;
			int iMaxRank = iRank;
			if(USER_TOP_RATING_RANK_2 == iMinRank) iMaxRank = USER_TOP_RATING_RANK_3;
			else if(USER_TOP_RATING_RANK_3 == iMinRank) continue;
			else if(USER_TOP_RATING_RANK_4 == iMinRank) iMaxRank = USER_TOP_RATING_RANK_10;

			vTopRankerUserID.clear();
			vTopRankerGameID.clear();
			RANK.GetTopRatingRankCharacter(iCategory, iMinRank, iMaxRank, vTopRankerUserID, vTopRankerGameID);

			for(int i = 0; i < vTopRankerGameID.size(); ++i)
			{
				iUserIDIndex = vTopRankerUserID[i];
				iGameIDIndex = vTopRankerGameID[i];

				CScopedRefGameUser pUser(iGameIDIndex);
				if (pUser != NULL)
				{
					vector<int> vNewlyReachedAchievementIndex;
					vector<int> vAnnounceAchievementIndex;

					vNewlyReachedAchievementIndex.push_back(GetAchievementIndexWithSeasonRankIndex(iCategory, iRank)); 

					if (TRUE == pUser->ProcessCompletedAchievements(ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex))
					{
						SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
						info.iGameIDIndex = pUser->GetGameIDIndex();

						strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), _countof(info.szGameID)-1);
						info.iAchievementCount = vAnnounceAchievementIndex.size();

						for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
						{
							info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
						}

						CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
						PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

						CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
						if (pMatch != NULL)
						{
							pMatch->Send(&PacketComposer);
						}
					}
				}
				else
				{
					if(SEASON_RANK_USER_RATING_GRADE == iCategory)
					{
						SG2S_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ rq;
						rq.iUserIDIndex = iUserIDIndex;
						rq.iAchievementIndex = GetAchievementIndexWithSeasonRankIndex(iCategory, iRank);

						CPacketComposer Packet(G2S_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
						Packet.Add((BYTE*)&rq, sizeof(SG2S_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
						if(pCenter)
						{
							pCenter->Send(&Packet);
						}
						else
						{
							SAchievementLog sAchievementLog;
							sAchievementLog.iAchievementIndex = GetAchievementIndexWithSeasonRankIndex(iCategory, iRank);
							sAchievementLog.iAchievementLogGroupIndex = ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO;

							BYTE btStorageType =ACHIEVEMENTMANAGER.GetAchievementStorageType(sAchievementLog.iAchievementIndex);

							pGameODBC->ACHIEVEMENT_InsertCompletedAchievement(iUserIDIndex, iGameIDIndex, sAchievementLog, btStorageType);
						}
					}
					else
					{
						SAchievementLog sAchievementLog;
						sAchievementLog.iAchievementIndex = GetAchievementIndexWithSeasonRankIndex(iCategory, iRank);
						sAchievementLog.iAchievementLogGroupIndex = ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO;

						BYTE btStorageType = ACHIEVEMENTMANAGER.GetAchievementStorageType(sAchievementLog.iAchievementIndex);

						pGameODBC->ACHIEVEMENT_InsertCompletedAchievement(iUserIDIndex, iGameIDIndex, sAchievementLog, btStorageType);
					}					
				}
			}
		}
	}
}

void CFSGameServer::CheckAndSetOptionWriteUserLogToDB()
{
	CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC( ODBC_RANK );
	CHECK_NULL_POINTER_VOID( pRankODBC );

	int iStatusFlag = 1;
	int iProcessID = GetProcessID();

	if (pRankODBC != NULL && ODBC_RETURN_SUCCESS == pRankODBC->spFSCheckFlagStatus(2, iStatusFlag, GetProcessID()))
	{
		if (iStatusFlag == 1)
		{
			m_bSetUserLog = TRUE;
		}
		else if (iStatusFlag == 0)
		{
			m_bSetUserLog = FALSE;
		}
	}
}

BOOL CFSGameServer::SetUserSingleContent( SAvatarInfo* pAvatarInfo, BYTE &byCount )
{
	if( NULL == pAvatarInfo ) return FALSE;

	byCount = 0;
	//	int iCount = 0;
	BYTE byPosition = (BYTE)pAvatarInfo->Status.iGamePosition;

	switch( byPosition )
	{
	case POSITION_CODE_CENTER: break;
	case POSITION_CODE_FOWARD:
	case POSITION_CODE_POWER_FOWARD:
	case POSITION_CODE_SMALL_FOWARD:	byPosition = 1; break;
	case POSITION_CODE_GUARD:
	case POSITION_CODE_POINT_GUARD:
	case POSITION_CODE_SHOOT_GUARD:	byPosition = 2; break;
	case POSITION_CODE_SWING_MAN : byPosition = 7; break;
	default:
		return FALSE;
	}	

	m_SingleContentsList.Lock();

	CTypeList<SSingleContent>::POSITION pEnd  = m_SingleContentsList.GetEndPosition();
	CTypeList<SSingleContent>::POSITION pos  = m_SingleContentsList.GetHeadPosition();
	while ( pos != pEnd) 
	{
		//		SSingleContent Test = (*pos);
		//memcpy(&Test,&(*pos),sizeof(SSingleContents));
		if( byPosition == (*pos).byPosition )
		{
			pAvatarInfo->UserSingleContents[(*pos).byTrainingNum].byPosition = byPosition;
			pAvatarInfo->UserSingleContents[(*pos).byTrainingNum].byTotalStageNum = (*pos).byTotalStageNum;
			pAvatarInfo->UserSingleContents[(*pos).byTrainingNum].byTrainingKind = (*pos).byTrainingKind;			// 테스트 코드 다되면 바꿔줘야쥐
			// 20080908 신규 유저 캐릭터슬롯 선물
			pAvatarInfo->UserSingleContents[(*pos).byTrainingNum].byArrow = (*pos).byArrow;			// 테스트 코드 다되면 바꿔줘야쥐
			// End
			byCount++;
		}

		pos = m_SingleContentsList.GetNextPosition(pos);
	}

	if( byCount > MAX_MINIGAME_NUM )
	{
		m_SingleContentsList.Unlock();
		return FALSE;
	}
	m_SingleContentsList.Unlock();

	return TRUE;
}

BOOL CFSGameServer::GetSingleContentBonus( SAvatarInfo* pAvatarInfo, int iUserStage, BYTE byTrainingKind, BYTE byStageIndex , int& iExp, int& iPoint )
{
// 20070911 Add MiniGame Info
	if( NULL == pAvatarInfo ) return 0;

	BYTE byPosition = pAvatarInfo->Status.iGamePosition;
	switch( byPosition )
	{
	case POSITION_CODE_CENTER: break;
	case POSITION_CODE_FOWARD:
	case POSITION_CODE_POWER_FOWARD:
	case POSITION_CODE_SMALL_FOWARD:	byPosition = 1; break;
	case POSITION_CODE_GUARD:
	case POSITION_CODE_POINT_GUARD:
	case POSITION_CODE_SHOOT_GUARD:	byPosition = 2; break;
	case POSITION_CODE_SWING_MAN : byPosition = 7; break;
	default:
		return FALSE;
	}	

// 20080522 Send Point at last stage
	bool bResult = false, bIsEndStage = false, bFirst =false;
	BYTE byPrevTraininingKind = 0;
	int iStageNum;
// End
	m_SingleContentBonusList.Lock();
	
	CTypeList< SSingleContentBonus >::POSITION pEnd  = m_SingleContentBonusList.GetEndPosition();
	CTypeList< SSingleContentBonus >::POSITION pos  = m_SingleContentBonusList.GetHeadPosition();
	
	while ( pos != pEnd) 
	{
		SSingleContentBonus Test = (*pos);
		if( byTrainingKind == (*pos).byTraininingKind && byStageIndex == (*pos).byStageIndex )
		{
			// 2010.01.11
			// Modify for Migration 2005
			// ktKim
			iStageNum = pow(2.f, byStageIndex);
			// End
			bFirst = CheckFirstMiniGame(iUserStage, iStageNum);
			if( bFirst == true )
			{
				iExp = (*pos).iExp[0];
				iPoint = (*pos).iPoint[0];
			}
			else
			{
				iExp = (*pos).iExp[1];
				iPoint = (*pos).iPoint[1];
			}
// 20080522 Send Point at last stage
			bResult = TRUE;
			
		}
// End
		pos = m_SingleContentBonusList.GetNextPosition(pos);
	}
// 20080522 Send Point at last stage
	m_SingleContentBonusList.Unlock();

	if( TRUE == m_bUseSingleTrainingLastStageBonus )
	{
		m_SingleContentsList.Lock();
		CTypeList< SSingleContent >::POSITION ContentsListEndPos  = m_SingleContentsList.GetEndPosition();
		CTypeList< SSingleContent >::POSITION ContentsListPos  = m_SingleContentsList.GetHeadPosition();
		while ( ContentsListPos != ContentsListEndPos) 
		{
			if(bFirst == true && byTrainingKind == (*ContentsListPos).byTrainingKind && byPosition == (*ContentsListPos).byPosition)
			{
				if( CheckLastMiniGame(iUserStage, iStageNum, (*ContentsListPos).byTotalStageNum) == true)
				{
					iExp += (*ContentsListPos).iBonusExp;
					iPoint += (*ContentsListPos).iBonusPoint;
					bResult = true;
				}
			}

			ContentsListPos = m_SingleContentsList.GetNextPosition(ContentsListPos);
		}
		m_SingleContentsList.Unlock(); 
	}
	
	return bResult;
// End
// End
}

bool CFSGameServer::CheckFirstMiniGame( int iUserStage, int iStageNum )
{
		return iUserStage & iStageNum ? false : true ;
}

bool CFSGameServer::CheckLastMiniGame( int iUserStage, int iStageNum ,int iTotalStageNum )
{
	// 2010.01.11
	// Modify for Migration 2005
	// ktKim
	if( (iUserStage + iStageNum) == ( pow(2.f,iTotalStageNum) - 1 ) )
	{
		return true;
	}
	else
	{
		return false;
	}
	// End
}

void CFSGameServer::SetUserBaseEpisodeInfo( CUser* pUser )
{
	for(int iEpisodeGroupIndex = 0; iEpisodeGroupIndex < MAX_EPISODE_GROUP_COUNT; iEpisodeGroupIndex++)
	{
		m_paEpisodeList[iEpisodeGroupIndex]->Lock();

		CTypeList< SEpisode >::POSITION pEnd	= m_paEpisodeList[iEpisodeGroupIndex]->GetEndPosition();
		CTypeList< SEpisode >::POSITION pos		= m_paEpisodeList[iEpisodeGroupIndex]->GetHeadPosition();
		while ( pos != pEnd) 
		{
			SEpisode sBaseInfo = (*pos);
			pUser->SetBaseEpisodeInfo( sBaseInfo );
			pos = m_paEpisodeList[iEpisodeGroupIndex]->GetNextPosition(pos);
		}

		m_paEpisodeList[iEpisodeGroupIndex]->Unlock();
	}
}

// 20070823 Add Story Mode
int CFSGameServer::GetEpisodeMaxStage( int iEpisodeGroupIndex, int iEpisodeNum )
{
	int iMaxStageNum = -1;
	m_paEpisodeList[iEpisodeGroupIndex]->Lock();

	CTypeList< SEpisode >::POSITION pEnd	= m_paEpisodeList[iEpisodeGroupIndex]->GetEndPosition();
	CTypeList< SEpisode >::POSITION pos		= m_paEpisodeList[iEpisodeGroupIndex]->GetHeadPosition();
	while ( pos != pEnd) 
	{
		if( iEpisodeNum == (*pos).iEpisodeIndex )
		{
			iMaxStageNum = (*pos).iMaxStageNum;
			m_paEpisodeList[iEpisodeGroupIndex]->Unlock();
			return iMaxStageNum;
		}

		pos = m_paEpisodeList[iEpisodeGroupIndex]->GetNextPosition(pos);
	}

	m_paEpisodeList[iEpisodeGroupIndex]->Unlock();

	return iMaxStageNum;

}
// End

void CFSGameServer::Process_CompleteAchievement(SM2G_ACHIEVEMENT_COMPLETE rs)
{
	CScopedRefGameUser pUser(rs.iGameIDIndex);
	if (pUser != NULL)
	{
		if (pUser->PreCheckAchievementAlreadyComplete(rs.iAchievementIndex) == TRUE)
		{
			return;
		}

		vector<int> vNewlyReachedAchievementIndex;
		vNewlyReachedAchievementIndex.push_back(rs.iAchievementIndex);
		vector<int> vAnnounceAchievementIndex;
		
		int iGameIDIndex = pUser->GetGameIDIndex();
		if(rs.iAchievementIndex == ACHIEVEMENT_CODE_POINT_1000000)
			iGameIDIndex = -1;

		if (TRUE == pUser->ProcessCompletedAchievements(rs.iAchievementLogGroupIndex, vNewlyReachedAchievementIndex, vAnnounceAchievementIndex) &&
			FALSE == vAnnounceAchievementIndex.empty())
		{
			SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ info;
			info.iGameIDIndex = pUser->GetGameIDIndex();

			strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), _countof(info.szGameID)-1);
			info.iAchievementCount = vAnnounceAchievementIndex.size();

			for (int iLoopIndex = 0; iLoopIndex < vAnnounceAchievementIndex.size() && iLoopIndex < ACHIEVEMENT_MAX_COMPLETE_NUM; iLoopIndex++)
			{
				info.aiAchievementIndex[iLoopIndex] = vAnnounceAchievementIndex[iLoopIndex];
			}

			CPacketComposer PacketComposer(G2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
			PacketComposer.Add((BYTE*)&info, sizeof(SG2M_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ));

			CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
			if (pMatch != NULL)
			{
				pMatch->Send(&PacketComposer);
			}	
		}
	}
}

int	CFSGameServer::GetDiscountRate(int iType)
{
	FLOAT fEnable = 0;
	if(FALSE == CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_DISCOUNT_RATE, iType, fEnable))
	{
		return 0;
	}

	return (int)fEnable;
}

int CFSGameServer::GetShopItemSexCondition(int iItemCode)
{
	SShopItemInfo ItemInfo;
	if (FALSE == m_pItemShop->GetItem(iItemCode, ItemInfo))
		return 0;

	return ItemInfo.iSexCondition;
}

void CFSGameServer::BroadCastWordPuzzlesEventStatus(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastWordPuzzlesEventStatus(eStatus);
}

void CFSGameServer::BroadCast_RandomBoxStatus(OPEN_STATUS eStatus)
{
	LockUserManager(); 

	SS2C_RANDOMBOX_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser &&
			(pUser->GetLocation() == U_LOBBY ||
			pUser->GetLocation() == U_TEAM))
		{
			if(pUser->GetLocation() == U_TEAM)
			{
				CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
				if(NULL == pMatch)
					continue;

				if(MATCH_TYPE_RATING != pMatch->GetMatchType() && MATCH_TYPE_CLUB_LEAGUE != pMatch->GetMatchType())
					continue;
			}

			if (OPEN == eStatus && NULL != pUser->GetUserHighFrequencyItem() )
			{
				info.iBoxItemCount = (pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GOLD_BAGS ) +
					pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_RED_BAGS ) +
					pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_GREEN_BAGS ) +
					pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_BLUE_BAGS ));
			}
			else
				info.iBoxItemCount = 0;

			if(pUser->GetLocation() == U_LOBBY)
				info.btUserState = RANDOMBOX_USER_STATUS_LOBBY;
			else
				info.btUserState = RANDOMBOX_USER_STATUS_TEAM;

			pUser->Send(S2C_RANDOMBOX_OPEN_NOT, &info, sizeof(SS2C_RANDOMBOX_OPEN_NOT));
		}
	}

	UnlockUserManager();
}

void CFSGameServer::BroadCast_LvUpEventStatus(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastLvUpEventStatus(eStatus);
}

void CFSGameServer::BoradCast_RandomItemBoardEventStatus(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastRandomItemBoardEventStatus(eStatus);
}

void CFSGameServer::SetClubPennantCode(int iClubSN, short stSlotIndex, int iPennantCode)
{
	LOBBYCLUBUSER.SetClubPennantCode(iClubSN, stSlotIndex, iPennantCode);
}

void CFSGameServer::GiveClubClothes(int iClubSN, int iItemCode)
{
	vector<int> vGameIDIndex;
	LOBBYCLUBUSER.GiveClubClothes(iClubSN, vGameIDIndex);

	for(int i = 0; i < vGameIDIndex.size(); ++i)
	{
		CScopedRefGameUser pUser(vGameIDIndex[i]);
		if (pUser != NULL)
		{
			pUser->GiveClubClothes(iItemCode);
		}
	}

	vGameIDIndex.clear();
}

void CFSGameServer::UpdateShoutTime() 
{
	m_bShout = TRUE; 
	m_dwShoutTime = GetTickCount(); 
}

void CFSGameServer::ResetIntensivePracticOnedayRecord()
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iStatusFlag = 0;
	int iProcessID = GetProcessID();

	if(NULL != pODBC)
	{
		if(pODBC != NULL && ODBC_RETURN_SUCCESS == pODBC->spFSCheckFlagStatus(STATUSFLAG_RESET_INTENSIVEPRACTICE_ONEDAY_RECORD, iStatusFlag, iProcessID))
		{
			if(iStatusFlag == 1)
			{
				LockUserManager(); 

				mPosition pEnd = m_mapUserTotal.GetEnd(); 
				for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
				{
					CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
					if(NULL != pUser)
					{
						pUser->GetUserIntensivePractice()->ResetOneDayRecord();
					}
				}

				UnlockUserManager();

				pODBC->spFSSetFlagStatus(STATUSFLAG_RESET_INTENSIVEPRACTICE_ONEDAY_RECORD, 0, iProcessID);
			}
		}
	}	
}

void CFSGameServer::AddMatchingPoolCareTimeCount()
{
	if(m_iMatchingPoolCareTimeCount < TIME_COUNT_MINUTE_180)
		m_iMatchingPoolCareTimeCount++;
}

BOOL CFSGameServer::CheckMatchingPoolCareTime()
{
	if(m_iMatchingPoolCareTimeCount >= TIME_COUNT_MINUTE_180)
		return TRUE;

	return FALSE;
}

void CFSGameServer::FactionTimer()
{
	//동기화는 센터서버랑 하는데 만약 센터가 비활성되어 있다면
	CHECK_CONDITION_RETURN_VOID(NULL != GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));

	FACTION.LoadFactionUserCount();

	map<int/*DistrictIndex*/, int/*FactionIndex*/> mapChampChangedDistrict;
	FACTION.LoadDistrictFactionScore(mapChampChangedDistrict);
}

void CFSGameServer::CheckAndLoadIntensivePracticeRanking()
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iStatusFlag = 0;
	int iProcessID = GetProcessID();

	if(ODBC_RETURN_SUCCESS == pODBC->spFSCheckFlagStatus(STATUSFLAG_RELOAD_INTENSIVEPRACTICE_RANKING, iStatusFlag, iProcessID))
	{
		if(iStatusFlag == 1)
		{
			INTENSIVEPRACTICEMANAGER.LoadRankList(INTENSIVEPRACTICE_RANK_TYPE_ALL);
			INTENSIVEPRACTICEMANAGER.LoadIntensivePracticeUserSeasonRecord();

			m_mapUserTotal.Lock(); 
			for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
			{
				CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
				if(NULL != pUser)
				{
					pUser->GetUserIntensivePractice()->InitializeBestPoint();
				}
			}
			m_mapUserTotal.Unlock();

			CClubSvrProxy* pClub = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
			if(pClub)
			{
				SG2CL_DEFAULT rq;
				pClub->SendPacket(G2CL_INTENSIVEPRACTICE_RANK_RELOAD_REQ, &rq, sizeof(SG2CL_DEFAULT));
			}

			pODBC->spFSSetFlagStatus(STATUSFLAG_RELOAD_INTENSIVEPRACTICE_RANKING, 0, iProcessID);
		}
	}
}

void CFSGameServer::BroadCast_PuzzlesStatus(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastPuzzlesStatus(eStatus);
}

void CFSGameServer::BroadCast_ThreeKingdomsEventStatus(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastThreeKingdomsEventStatus(eStatus);
}

void CFSGameServer::BroadCast_MissionShopEventStatus()
{
	FSLOBBY.BroadCastMissionShopEventStatus();
}

void CFSGameServer::BroadCastHotGirlMissionEventStatus(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastHotGirlMissionEventStatus(eStatus);
}

void CFSGameServer::BroadCast_RandomCardEventStatus(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastRandomCardEventStatus(eStatus);
}

void CFSGameServer::BroadCastShoppingFestivalEventStatus(BYTE btCurrentEventType, BYTE btOpenStatus)
{
	// FSLOBBY.BroadCastShoppingFestivalEventStatus(btCurrentEventType, btOpenStatus);

	SS2C_EVENT_SHOPPING_FESTIVAL_NOT info;
	info.btEventType = btCurrentEventType;
	info.btOpenStatus = btOpenStatus;
	info.iCouponCnt = 0;

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			if(NULL != pUser &&
				(pUser->GetLocation() == U_LOBBY ||
				pUser->GetLocation() == U_TEAM))
			{
				pUser->Send(S2C_EVENT_SHOPPING_FESTIVAL_NOT, &info, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_NOT));
			}
		}
	}
	m_mapUserTotal.Unlock(); 
}

void CFSGameServer::BroadCastShoppingFestivalEventInfo(BYTE btEventType)
{
	FSLOBBY.BroadCastShoppingFestivalEventInfo(btEventType);
}

void CFSGameServer::GiveFactionRaceRankReward(SS2G_FACTION_GIVE_REWARD_REQ req, CReceivePacketBuffer* rp)
{
	BOOL bClearSeason = FALSE;
	if(req.iSeasonIndex !=
		FACTION.GetCurrentSeasonIndex())
		bClearSeason = TRUE;
			
	for (int iRankCount = 0; iRankCount < req.iRankCount; ++iRankCount)
	{
		SS2G_FACTION_GIVE_REWARD_INFO info;
		rp->Read((PBYTE)&info, sizeof(SS2G_FACTION_GIVE_REWARD_INFO));
		m_mapUserTotal.Lock(); 
		for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
		{
			CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
			if(NULL != pUser)
			{
				SUserFactionInfo* pUserFaction = pUser->GetUserFaction();
				if(NULL != pUserFaction &&
					pUserFaction->_iFactionIndex == info.iFactionIndex)
				{
					if(TRUE == FACTION.GiveFactionRaceRankReward(req.iSeasonIndex, req.iDateKey, req.iTimeIndex, req.btFactionRewardType, info.iRank, pUser))
						pUser->UpdateFactionLastPlayTime(bClearSeason);		
				}
			}
		}
		m_mapUserTotal.Unlock();
	}
}

void CFSGameServer::BroadCast_FactionStatus(FACTION_RACE_STATUS eStatus)
{
	LockUserManager(); 

	SS2C_FACTION_RACE_STATUS_NOT not;
	not.btFactionRaceStatus = eStatus;

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser &&
			(pUser->GetLocation() == U_LOBBY ||
			pUser->GetLocation() == U_TEAM))
		{
			if(pUser->GetLocation() == U_TEAM)
			{
				CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
				if(NULL == pMatch)
					continue;

				if(MATCH_TYPE_RATING != pMatch->GetMatchType() && MATCH_TYPE_CLUB_LEAGUE != pMatch->GetMatchType())
					continue;
			}

			SUserFactionInfo* pUserFaction = pUser->GetUserFaction();
			if(NULL == pUserFaction)
				continue;

			not.btMyFactionIndex = pUserFaction->_iFactionIndex;
			pUser->Send(S2C_FACTION_RACE_STATUS_NOT, &not, sizeof(SS2C_FACTION_RACE_STATUS_NOT));
		}
	}

	UnlockUserManager();
}

void CFSGameServer::ClearUserFactionInfo()
{
	LockUserManager(); 

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser &&
			(pUser->GetLocation() == U_LOBBY ||
			pUser->GetLocation() == U_TEAM))
		{
			if(pUser->GetLocation() == U_TEAM)
			{
				CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
				if(NULL == pMatch)
					continue;

				if(MATCH_TYPE_RATING != pMatch->GetMatchType() && MATCH_TYPE_CLUB_LEAGUE != pMatch->GetMatchType())
					continue;
			}

			pUser->ClearUserFactionInfo();
		}
	}

	UnlockUserManager();
}

void CFSGameServer::CheckHotGirlTime()
{
	if( TRUE == HOTGIRLTIMEManager.CheckSchedule())
	{
		SHotGirlTimeConfig sConfig = HOTGIRLTIMEManager.GetHotGirlTimeConfig();	
		SS2C_HOTGIRLTIME_OPEN_NOT sNot;

		sNot.iStartTime = sConfig._iStartTime;
		sNot.iEndTime = sConfig._iEndTime;
		sNot.btStatus = (BYTE)HOTGIRLTIMEManager.IsOpen();

		CPacketComposer	Packet(S2C_HOTGIRLTIME_OPEN_NOT);
		Packet.Add((PBYTE)&sNot, sizeof(SS2C_HOTGIRLTIME_OPEN_NOT));

		BroadCast(Packet, FALSE);
	}
}

void CFSGameServer::CheckCoachCardStatus()
{
	if( TRUE == COACHCARD.CheckSchedule() )
	{
		CPacketComposer	Packet(S2C_POTENCARD_EVENT_OPEN_NOT);
		COACHCARD.MakePacketOpenPotenCardEventList(Packet);

		BroadCast(Packet, FALSE);
	}
}

void CFSGameServer::CheckEventGoldenCrushStatus()
{
	if( TRUE == GOLDENCRUSH.CheckSchedule() )	
	{
		SS2C_EVENT_GOLDENCRUSH_EVENT_NOT	not;

		not.btIsOpen = static_cast<BYTE>(GOLDENCRUSH.IsOpen());

		CPacketComposer	Packet(S2C_EVENT_GOLDENCRUSH_EVENT_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_EVENT_GOLDENCRUSH_EVENT_NOT));

		BroadCast(Packet, FALSE);
	}
}

void CFSGameServer::CheckEventMatchingCardStatus()
{
	if( TRUE == MATCHINGCARD.CheckSchedule() )
	{
		SS2C_EVENT_MATCHINGCARD_STATUS_NOT	not;
		not.btIsOpen = static_cast<BYTE>(MATCHINGCARD.IsOpen());
		not.iCashPrize = MATCHINGCARD.GetCumulativeCash();

		CPacketComposer Packet(S2C_EVENT_MATCHINGCARD_STATUS_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_EVENT_MATCHINGCARD_STATUS_NOT));

		BroadCast(Packet, FALSE);
	}
}

BOOL CFSGameServer::UpdateCurrentDBDate()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	if( ODBC_RETURN_SUCCESS != pODBCBase->SP_GetDBTime(m_tCurrentDBDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, CALL_SP, FAIL, "Fail - SP_GetDBTime" );
		++m_tCurrentDBDate;
		return FALSE;
	}

	return TRUE;
}

void CFSGameServer::CheckEventAttendanceItemShopStatus()
{
	OPEN_STATUS eIsOldOpen = ATTENDANCEITEMSHOP.GetOpen();
	OPEN_STATUS eIsOpen = ATTENDANCEITEMSHOP.IsOpen();
	if( eIsOldOpen != eIsOpen )
	{
		SS2C_EVENT_ATTENDANCE_ITEMSHOP_LOBBY_STATUS_NOT not;

		not.btIsOpen = ATTENDANCEITEMSHOP.IsOpen();
		not.btCometoShop = (BYTE)( OPEN == ATTENDANCEITEMSHOP.IsOpen() ? TRUE : FALSE );

		CPacketComposer	Packet(S2C_EVENT_ATTENDANCE_ITEMSHOP_LOBBY_STATUS_NOT);
		Packet.Add((BYTE*)&not, sizeof(SS2C_EVENT_ATTENDANCE_ITEMSHOP_LOBBY_STATUS_NOT));

		BroadCast(Packet, FALSE);

		if( CLOSED == not.btIsOpen )
		{
			CPacketComposer PacketReset(S2C_EVENT_ATTENDANCE_ITEMSHOP_SHOP_REFRESH_NOT);
			BroadCast(PacketReset, FALSE);
		}
	}
}

void CFSGameServer::CheckEventAttendanceTodayItemReset()
{
	if( TRUE == ATTENDANCEITEMSHOP.CheckResetEventItem(m_tCurrentDBDate) )
	{
		CPacketComposer	Packet(S2C_EVENT_ATTENDANCE_ITEMSHOP_SHOP_REFRESH_NOT);

		BroadCast(Packet, FALSE);
	}
}

int CFSGameServer::GetAchievementIndexWithSeasonRankIndex(int iSeasonRankIndex, int iMinRank)
{
	int iReturnCode = 0;

	switch( iSeasonRankIndex )
	{
	case SEASON_RANK_POINT			:	iReturnCode = ACHIEVEMENT_CODE_RANK_POINT;		break;
	case SEASON_RANK_POINT_AVR		:	iReturnCode = ACHIEVEMENT_CODE_RANK_POINTAVR;	break;
	case SEASON_RANK_3POINT			:	iReturnCode = ACHIEVEMENT_CODE_RANK_3POINT;		break;
	case SEASON_RANK_2POINT_RATE	:	iReturnCode = ACHIEVEMENT_CODE_RANK_2POINTREAT;	break;
	case SEASON_RANK_3POINT_RATE	:	iReturnCode = ACHIEVEMENT_CODE_RANK_3POINTRATE;	break;
	case SEASON_RANK_ASSIST			:	iReturnCode = ACHIEVEMENT_CODE_RANK_ASIST;		break;
	case SEASON_RANK_ASSIST_AVR		:	iReturnCode = ACHIEVEMENT_CODE_RANK_ASISTAVR;	break;
	case SEASON_RANK_REBOUND		:	iReturnCode = ACHIEVEMENT_CODE_RANK_REBOUND;	break;
	case SEASON_RANK_REBOUND_AVR	:	iReturnCode = ACHIEVEMENT_CODE_RANK_REBOUNDAVR;	break;
	case SEASON_RANK_BLOCK			:	iReturnCode = ACHIEVEMENT_CODE_RANK_BLOCK;		break;
	case SEASON_RANK_BLOCK_AVR		:	iReturnCode = ACHIEVEMENT_CODE_RANK_BLOCKAVR;	break;
	case SEASON_RANK_STEAL			:	iReturnCode = ACHIEVEMENT_CODE_RANK_STEAL;		break;
	case SEASON_RANK_STEAL_AVR		:	iReturnCode = ACHIEVEMENT_CODE_RANK_STEALAVR;	break;
	case SEASON_RANK_LOOSEBALL		:	iReturnCode = ACHIEVEMENT_CODE_RANK_LOOSEBALL;	break;
	case SEASON_RANK_LOOSEBALL_AVR	:	iReturnCode = ACHIEVEMENT_CODE_RANK_LOOSEBALLAVG;	break;
	case SEASON_RANK_WINPOINT		:	iReturnCode = ACHIEVEMENT_CODE_RANK_WINPOINT;	break;
	case SEASON_RANK_WINPOINT1ON1	:	iReturnCode = ACHIEVEMENT_CODE_RANK_WINPOINT1ON1;		break;
	case SEASON_RANK_PLAY_TIME		:	iReturnCode = ACHIEVEMENT_CODE_RANK_PLAYTIME;	break;
	case SEASON_RANK_POG_NUM		:	iReturnCode = ACHIEVEMENT_CODE_RANK_POGNUM;	break;
	case SEASON_RANK_USER_RATING_GRADE:	
		{
			if(USER_TOP_RATING_RANK_1 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_USER_TOP_RATING_1;
			else if(USER_TOP_RATING_RANK_2 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_USER_TOP_RATING_2;
			else if(USER_TOP_RATING_RANK_4 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_USER_TOP_RATING_3;
		}
		break;
	case SEASON_RANK_CENTER_RATING_GRADE:
		{
			if(USER_TOP_RATING_RANK_1 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_C_TOP_RATING_1;
			else if(USER_TOP_RATING_RANK_2 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_C_TOP_RATING_2;
			else if(USER_TOP_RATING_RANK_4 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_C_TOP_RATING_3;
		}
		break;
	case SEASON_RANK_PF_RATING_GRADE:
		{
			if(USER_TOP_RATING_RANK_1 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_PF_TOP_RATING_1;
			else if(USER_TOP_RATING_RANK_2 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_PF_TOP_RATING_2;
			else if(USER_TOP_RATING_RANK_4 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_PF_TOP_RATING_3;
		}
		break;
	case SEASON_RANK_SF_RATING_GRADE:
		{
			if(USER_TOP_RATING_RANK_1 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SF_TOP_RATING_1;
			else if(USER_TOP_RATING_RANK_2 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SF_TOP_RATING_2;
			else if(USER_TOP_RATING_RANK_4 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SF_TOP_RATING_3;
		}
		break;
	case SEASON_RANK_PG_RATING_GRADE:
		{
			if(USER_TOP_RATING_RANK_1 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_PG_TOP_RATING_1;
			else if(USER_TOP_RATING_RANK_2 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_PG_TOP_RATING_2;
			else if(USER_TOP_RATING_RANK_4 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_PG_TOP_RATING_3;
		}
		break;
	case SEASON_RANK_SG_RATING_GRADE:
		{
			if(USER_TOP_RATING_RANK_1 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SG_TOP_RATING_1;
			else if(USER_TOP_RATING_RANK_2 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SG_TOP_RATING_2;
			else if(USER_TOP_RATING_RANK_4 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SG_TOP_RATING_3;
		}
		break;
	case SEASON_RANK_SW_RATING_GRADE:
		{
			if(USER_TOP_RATING_RANK_1 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SW_TOP_RATING_1;
			else if(USER_TOP_RATING_RANK_2 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SW_TOP_RATING_2;
			else if(USER_TOP_RATING_RANK_4 == iMinRank)
				iReturnCode = ACHIEVEMENT_CODE_RANK_SW_TOP_RATING_3;
		}
		break;
	}
	return iReturnCode;
}

void CFSGameServer::BroadCast_EventStatus()
{
	FSLOBBY.BroadCastEventStatus();
}

void CFSGameServer::CheckAndLoadUserAppraisal()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == USERAPPRAISAL.IsOpen());

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iStatusFlag = 0;
	int iProcessID = GetProcessID();

	if(ODBC_RETURN_SUCCESS == pODBC->spFSCheckFlagStatus(STATUSFLAG_RELOAD_USER_APPRAISAL, iStatusFlag, iProcessID))
	{
		if(iStatusFlag == 1)
		{
			m_mapUserTotal.Lock(); 
			for(mPosition pos = m_mapUserTotal.GetHead(); pos != m_mapUserTotal.GetEnd(); pos = m_mapUserTotal.GetNextPosition(pos))
			{
				CFSGameUser* pUser = (CFSGameUser*)m_mapUserTotal.GetUser(pos);
				if(NULL != pUser)
				{
					pUser->GetUserAppraisal()->Load();
					pUser->GetUserBasketBall()->CheckAndRemoveBasketBall();
				}
			}
			m_mapUserTotal.Unlock();

			pODBC->spFSSetFlagStatus(STATUSFLAG_RELOAD_USER_APPRAISAL, 0, iProcessID);
		}
	}
}

void CFSGameServer::SendToChatChangePenaltyStatus(int iGameIDIndex, BOOL bPenaltyUser)
{
	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	if( pChat )
	{
		SS2T_CHANGE_PENALTY_STATUS info;
		info.iGameIDIndex = iGameIDIndex;
		info.bEnablePenalty = bPenaltyUser;

		CPacketComposer Packet(S2T_CHANGE_PENALTY_STATUS); 
		Packet.Add((PBYTE)&info, sizeof(SS2T_CHANGE_PENALTY_STATUS));
		pChat->Send(&Packet);
	}
}

BOOL CFSGameServer::GetRandomGameUser( int iUserCount, vector<char*>& vGameUser )
{
	const int cnMaxGivePresnetSize_LuckStar = 5;
	vector<char*> vGameUserInGame;
	vector<char*> vGameUserNotInGame;
	m_mapUserTotal.Lock(); 
	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			if(pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= (MAX_PRESENT_LIST-cnMaxGivePresnetSize_LuckStar))
				continue; //  편지함 꽉찼으면 보상 못줌 대상제외

			if ( pUser->IsPlaying() )
			{
				vGameUserInGame.push_back( pUser->GetGameID() );
			}
			else
			{
				vGameUserNotInGame.push_back( pUser->GetGameID() );
			}
			if ( vGameUser.size() < iUserCount )
			{
				vGameUser.push_back( pUser->GetGameID() );
			}
		}

	}
	m_mapUserTotal.Unlock(); 

	int iInGameUserPerCount = vGameUserInGame.size() / ( iUserCount - 1 );
	int iNotInGameUserCount = vGameUserNotInGame.size();
	if ( iInGameUserPerCount == 0 || vGameUser.size() < iUserCount )
	{
		for ( int i = vGameUser.size(); i < iUserCount; ++i)
		{
			vGameUser.push_back("NULL");
		}
		return TRUE;
	}
	int iPos = 0;
	for ( int i = 0; i < iUserCount - 1; i++)
	{
		iPos = rand()  11866902InGameUserPerCount;
eUser[i] = vGameUserInGame[ i * iInGameUserPerCount + iPos ];
	}

	if ( iNotInGameUserCount > 0 )
	{
		vGameUser[ iUserCount - 1 ] = vGameUserNotInGame[ rand()  11866902NotInGameUserCount];
eturn TRUE;
}

void CFSGameServer::BroadCast_UserChoiceMissionEventStatus(OPEN_STATUS eStatus)
{
	LockUserManager(); 

	SS2C_USER_CHOICE_MISSION_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser &&
			(pUser->GetLocation() == U_LOBBY ||
			pUser->GetLocation() == U_TEAM))
		{
			if(pUser->GetLocation() == U_TEAM)
			{
				CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
				if(NULL == pMatch)
					continue;

				if(MATCH_TYPE_RATING != pMatch->GetMatchType() && MATCH_TYPE_CLUB_LEAGUE != pMatch->GetMatchType())
					continue;
			}

			info.btMissionStatus = pUser->GetUserChoiceMissionEvent()->GetNoticeMissionStatus();
			pUser->Send(S2C_USER_CHOICE_MISSION_OPEN_NOT, &info, sizeof(SS2C_USER_CHOICE_MISSION_OPEN_NOT));
		}
	}

	UnlockUserManager();
}

void CFSGameServer::BroadCast_EventStatus(int iEventKind, OPEN_STATUS eStatus, BYTE btSetUpType, BYTE btClickCloseButtonNotice)
{
	LockUserManager(); 

	SS2C_EVENT_STATUS_NOT info;
	info.iEventKind = iEventKind;
	info.btOpenStatus = (BYTE)eStatus;
	info.btSetUpType = btSetUpType;
	info.btClickCloseButtonNotice = btClickCloseButtonNotice;

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->Send(S2C_EVENT_STATUS_NOT, &info, sizeof(SS2C_EVENT_STATUS_NOT));
		}
	}

	UnlockUserManager();
}

void CFSGameServer::BroadCast_UserRandomItemTreeEvent(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastUserRandomItemTreeEvent(eStatus);
}

void CFSGameServer::BroadCast_UserExpBoxItemEvent(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastUserExpBoxItemEvent(eStatus);
}

void CFSGameServer::CheckContentsDefaultSetting()
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	float fValue = 0.0f;
	if(ODBC_RETURN_SUCCESS == pODBC->CONTENTS_GetDefaultSettingInfo(CONTENTS_INDEX_CLIENT_PACKET_TICKCOUNT_LOG, (int)1, fValue))
	{
		CONTENTSMANAGER.Process_UpdateContentsSetting(CONTENTS_INDEX_CLIENT_PACKET_TICKCOUNT_LOG, (int)1, fValue);
	}
}

void CFSGameServer::BroadCast_UserRandomItemGroupEvent(OPEN_STATUS eStatus)
{
	FSLOBBY.BroadCastUserRandomItemGroupEvent(eStatus);
}

BOOL CFSGameServer::CheckItemShopEventButtonRender()
{
	return m_bItemShopEventButton;
}

BOOL CFSGameServer::CheckItemShopEventButtonSchedule()
{
	CHECK_NULL_POINTER_BOOL(m_pItemShop);

	if( TRUE == CFSGameServer::GetInstance()->CheckItemShopEventButtonRender() ) 
	{
		BOOL bOldEventButtonEnable = FSLOBBY.GetItemShopeventButton();
		BOOL bActiveSaleItem = m_pItemShop->IsEmptyActiveSaleItem();

		if(FALSE == bActiveSaleItem)
		{
			bActiveSaleItem = EVENTDATEMANAGER.IsView(EVENT_KIND_ITEMSHOP_PACKAGEITEM);
		}

		SS2C_ITEMSHOP_EVENTBUTTON_RENDER_NOT not;

		FSLOBBY.SetItemShopEventButton(bActiveSaleItem);

		if( bOldEventButtonEnable != bActiveSaleItem )
		{
			not.btOpenStatus = (BYTE)(bActiveSaleItem == FALSE ? OPEN : CLOSED);

			CPacketComposer Packet(S2C_ITEMSHOP_EVENTBUTTON_RENDER_NOT);
			Packet.Add((PBYTE)&not, sizeof(SS2C_ITEMSHOP_EVENTBUTTON_RENDER_NOT));
			FSLOBBY.BroadCast(&Packet,NEXUS_LOBBY);

			return TRUE;
		}
	}

	return FALSE;
}

void CFSGameServer::CheckBasketBallEventButtonSchedule()
{
	if( TRUE == m_BasketBallManager.CheckSchedule() )
	{
		SS2C_BASKETBALL_EVENTBUTTON_RENDER_NOT not;
		not.btOpenStatus = (BYTE)m_BasketBallManager.IsEventButtonOpen();

		CPacketComposer Packet(S2C_BASKETBALL_EVENTBUTTON_RENDER_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_BASKETBALL_EVENTBUTTON_RENDER_NOT));
		FSLOBBY.BroadCast(&Packet,NEXUS_LOBBY);
	}
}

void CFSGameServer::CheckLeagueModeStatus()
{
	if( TRUE == RANKMATCH.CheckSchedule() )
	{
		SS2C_RANKMATCH_STATUS_NOT	not;
		not.btOpenStatus = static_cast<BYTE>(RANKMATCH.IsOpen());
		RANKMATCH.GetSeasonDate(not.tSeasonStartTime, not.tSeasonEndTime);
		not.iSeasonIndex = RANKMATCH.GetSeasonIndex();

		CPacketComposer Packet(S2C_RANKMATCH_STATUS_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_RANKMATCH_STATUS_NOT));

		BroadCast(Packet, FALSE);
	}
}

BYTE CFSGameServer::GetServerIndex( void )
{
	//just kor
	return 0;
}

SYSTEMTIME CFSGameServer::GetCurrentDBDateToSystemTime( void )
{
	SYSTEMTIME	sSystemTime;
	ZeroMemory(&sSystemTime, sizeof(SYSTEMTIME));
	TimetToSystemTime( m_tCurrentDBDate , sSystemTime);
	return sSystemTime;
}

void CFSGameServer::GivePCRoomUserLoginTimeReward()
{
	LockUserManager(); 

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->GetUserPCRoomEvent()->GiveLoginTimeReward();
		}
	}

	UnlockUserManager();
}

void CFSGameServer::CheckLeagueModeMatchOpenTime()
{
	if( TRUE == RANKMATCH.CheckMatchOpenTime(_GetCurrentDBDate) )
	{
		SS2C_RANKMATCH_MATCHOPEN_NOT	not;
		not.btOpen = (BYTE)RANKMATCH.GetMatchOpenTime();

		CPacketComposer	Packet(S2C_RANKMATCH_MATCHOPEN_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_RANKMATCH_MATCHOPEN_NOT));

		BroadCast(Packet);
	}
}

void CFSGameServer::UpdateLeagueModeSwitch()
{ // 한국만.
	CFSLeagueODBC*	pLeagueODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
	CHECK_NULL_POINTER_VOID(pLeagueODBC);

	int iEnable , iValue;
	iEnable = iValue = 0;
	if( ODBC_RETURN_SUCCESS != pLeagueODBC->LEAGUE_GetConfigValue(LEAGUE_CONFIG_RealTimeSwitch_Off, iEnable, iValue))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetConfigValue");
		return;
	}
	BOOL bSwitch = RANKMATCH.GetLeagueModeSwitchOff();
	
	if( 0 == iValue && TRUE == bSwitch )
	{
		RANKMATCH.UnsetLeagueModeSwitch();
	} 
	else if( 1 == iValue && FALSE == bSwitch)
	{
		RANKMATCH.SetLeagueModeSwitch();
	}
}

void CFSGameServer::AddRankMatchLimitUser(CFSGameUser* pUser)
{
	CHECK_NULL_POINTER_VOID(pUser);

	BEGIN_SCOPE_LOCK(m_csRankMatchLimitUser)
		m_mapRankMatchLimitUser.insert(MAP_MATCHING_LIMIT_USER::value_type(pUser->GetGameIDIndex(), pUser));
	END_SCOPE_LOCK
}

void CFSGameServer::RemoveRankMatchLimitUser(int iGameIDIndex)
{
	BEGIN_SCOPE_LOCK(m_csRankMatchLimitUser)
		MAP_MATCHING_LIMIT_USER::iterator iter = m_mapRankMatchLimitUser.find(iGameIDIndex);
	CHECK_CONDITION_RETURN_VOID(iter == m_mapRankMatchLimitUser.end());

	m_mapRankMatchLimitUser.erase(iter);
	END_SCOPE_LOCK
}

void CFSGameServer::DecreaseRankMatchLimitTime()
{
	BEGIN_SCOPE_LOCK(m_csRankMatchLimitUser)

		int iGameIdIndex = -1;
	vector<int> vecLimitMatchingUserList;
	MAP_MATCHING_LIMIT_USER::iterator iter = m_mapRankMatchLimitUser.begin();
	for( ; iter != m_mapRankMatchLimitUser.end(); ++iter)
	{
		if(NULL != iter->second)
		{
			iGameIdIndex = iter->second->GetUserRankMatch()->DecreaseLimitMatchingTime();
			if ( -1 != iGameIdIndex )
			{
				vecLimitMatchingUserList.push_back( iGameIdIndex );
			}
		}
	}

	for ( int i = 0; i < vecLimitMatchingUserList.size(); ++i )
	{
		RemoveRankMatchLimitUser(vecLimitMatchingUserList[i]);
	}
	END_SCOPE_LOCK
}


void CFSGameServer::BroadCast_LoginPayback_ShopPopupStatus( OPEN_STATUS eStatus )
{
	CPacketComposer Packet(S2C_EVENT_LOGINPAYBACK_SHOP_POPUP_NOT);
	SS2C_EVENT_LOGINPAYBACK_SHOP_POPUP_NOT not;

	not.btEnable = static_cast<BYTE>((OPEN == eStatus ? TRUE : FALSE));

	Packet.Add((BYTE*)&not, sizeof(SS2C_EVENT_LOGINPAYBACK_SHOP_POPUP_NOT));

	BroadCast(Packet);
}

void CFSGameServer::CheckAndLoadGameOfDiceRankList()
{
	if((OPEN == GAMEOFDICEEVENT.IsOpen() || OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_GAMEOFDICE_RANK)) &&
		FALSE == GAMEOFDICEEVENT.IsLoadRank())
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			CPacketComposer	Packet(S2O_EVENT_GAMEOFDICE_RANK_LIST_REQ);
			pODBCSvr->Send(&Packet);
		}
	}
}

void CFSGameServer::CheckAndLoadMiniGameZoneRankList()
{
	if(OPEN == MINIGAMEZONE.IsOpen() && FALSE == MINIGAMEZONE.IsRankLoad())
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			CPacketComposer	Packet(S2O_EVENT_MINIGAMEZONE_RANK_LIST_REQ);
			pODBCSvr->Send(&Packet);
		}
	}
}

void CFSGameServer::UpdateDiscountItemEventItemPrice()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == DISCOUNTITEMEVENT.IsOpen());

	m_mapUserTotal.Lock(); 

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->GetUserDiscountItemEvent()->UpdateItemPrice();
		}
	}

	m_mapUserTotal.Unlock(); 
}

void CFSGameServer::UpdateShoppingEventLoginTimeMission()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPING.IsOpen());

	m_mapUserTotal.Lock(); 

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->GetUserShoppingEvent()->UpdateLoginTimeMission();
		}
	}

	m_mapUserTotal.Unlock(); 
}

void CFSGameServer::CheckAndSendFriendInviteKingRankList()
{
	if( TRUE == FRIENDINVITE.CheckRankList())
	{
		CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		CHECK_NULL_POINTER_VOID(pProxy);

		CPacketComposer Packet(S2O_EVENT_FRIENDINVITE_RANKLIST_REQ);
		pProxy->Send(&Packet);
	}
}

void CFSGameServer::UpdatePotionMakingEventLoginTimeMission()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == POTIONMAKEEVENT.IsOpen());

	m_mapUserTotal.Lock(); 

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->GetUserPotionMakingEvent()->UpdateLoginTimeMission();
		}
	}

	m_mapUserTotal.Unlock(); 
}

void CFSGameServer::CheckAndLoadHippocampusRankList()
{
	if((OPEN == HIPPOCAMPUS.IsOpen() || OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_HIPPOCAMPUS_RANK)) &&
		FALSE == HIPPOCAMPUS.IsLoadRank())
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			CPacketComposer	Packet(S2O_EVENT_HIPPOCAMPUS_RANK_LIST_REQ);
			pODBCSvr->Send(&Packet);
		}
	}
}

void CFSGameServer::CheckComebackBenefitAttendance()
{
	if(TRUE == COMEBACK.CheckAttendanceUpdate(_GetCurrentDBDate))
	{
		CFSODBCBase* pODBCBase = static_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));

		m_mapUserTotal.Lock(); 
		mPosition pEnd = m_mapUserTotal.GetEnd(); 
		for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
		{
			CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
			if(NULL != pUser)
			{
				pUser->GetUserComebackBenefit()->CheckAttendance(FALSE);
			}
		}
		m_mapUserTotal.Unlock();
	}
}

void CFSGameServer::BroadCast_SteelBagMissionEventStatus(OPEN_STATUS eStatus)
{
	CPacketComposer Packet(S2C_EVENT_STEELBAG_STATUS_NOT);
	SS2C_EVENT_STEELBAG_STATUS_NOT not;

	not.btStatus = EVENT_STEELBAG_STATUS_NONE;
	if(OPEN == eStatus)
		not.btStatus = EVENT_STEELBAG_STATUS_NOT_COMPLETED_MISSION;

	Packet.Add((BYTE*)&not, sizeof(SS2C_EVENT_STEELBAG_STATUS_NOT));

	BroadCast(Packet);
}

void CFSGameServer::ResetSteelBagMission()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == STEELBAGMISSION.IsOpen());

	m_mapUserTotal.Lock(); 

	mPosition pEnd = m_mapUserTotal.GetEnd(); 
	for (mPosition pos=m_mapUserTotal.GetHead(); pos!=pEnd; pos++)
	{
		CFSGameUser* pUser=(CFSGameUser*)m_mapUserTotal.GetUser(pos);
		if(NULL != pUser)
		{
			pUser->GetUserSteelBagMissionEvent()->ResetMission();
		}
	}

	m_mapUserTotal.Unlock(); 
}

void CFSGameServer::GetLoseLoadConfigInfo( SLOSE_LEAD_CONFIG& info )
{
	info.bLoad = m_sLoseLeadConfig.bLoad;
	info.iLeadRating = m_sLoseLeadConfig.iLeadRating;
	info.iLeadTime = m_sLoseLeadConfig.iLeadTime;
	info.iRewardIndex = m_sLoseLeadConfig.iRewardIndex;
	info.iRatingRate = m_sLoseLeadConfig.iRatingRate;
}

void CFSGameServer::GiveClubMissionReward(vector<SCLUB_MISSION_GIVE_REWARD_INFO>& vPresent)
{
	for(int i = 0; i < vPresent.size(); ++i)
	{
		CScopedRefGameUser pUser(vPresent[i].iGameIDIndex);
		if (pUser != NULL)
		{
			pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, vPresent[i].iPresentIndex);
		}
	}
}

void CFSGameServer::CheckAndUpdateUserAppraisalRanking()
{
	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iStatusFlag = 0;
	int iProcessID = GetProcessID();

	if(ODBC_RETURN_SUCCESS == pODBC->spFSCheckFlagStatus(STATUSFLAG_USERAPPRAISAL_RANKING, iStatusFlag, iProcessID))
	{
		if(iStatusFlag == 1)
		{
			if(USERAPPRAISAL.LoadRank() == TRUE)
			{
				pODBC->spFSSetFlagStatus(STATUSFLAG_USERAPPRAISAL_RANKING, 0, iProcessID);

				vector<int> vecAnnounce;
				int iToDay = GetYYYYMMDD() ;
00;
				if(1 == iToDay)
				{
					int iGameIDIndex = USERAPPRAISAL.GetGameIDIndex_TopRank(APPRAISAL_RANK_SEASON_TYPE_PRESEASON);
					CScopedRefGameUser pUser(iGameIDIndex);
					if(pUser != NULL)
					{
						pUser->UpdateAchievement_Memory(ACHIEVEMENT_LOG_GROUP_RANKINGANDINFO, ACHIEVEMENT_CODE_EVENT_APPRAISAL_TOPRANK, vecAnnounce);
					}
				}
			}
		}
	}
}
