qpragma once

class CFSGameUser;
class CFSODBCBase;

typedef map<BYTE/*_btMaterialType*/, int/*_iMaterialCount*/> SHOPPING_USER_MATERIAL_MAP;

class CFSGameUserShoppingEvent
{
public:
	CFSGameUserShoppingEvent(CFSGameUser* pUser);
	~CFSGameUserShoppingEvent(void);

	BOOL				Load();

	int					GetCurrentCoin() { return m_sUserInfo.iCurrentCoin; }
	void				SendShoppingEventInfo();
	void				SendShoppingEventRewardInfo();
	BOOL				CheckAndUpdateUserMission(BYTE btMissionType, BYTE& btMaterialType, int& iAddMaterialCount, time_t tCurrentTime = _time64(NULL));
	eSHOPPING_SELL_PRODUCT_RESULT SellProduct(int	iProductIndex, SS2C_SHOPPING_SELL_PRODUCT_RES& rs);
	eSHOPPING_USE_COIN_RESULT UseCoin(int iExchangeProductIndex);
	void				SaveUserShoppingInfo();
	void				UpdateLoginTimeMission();

private:
	BOOL CheckResetTime(time_t tCurrentTime = _time64(NULL));
	BOOL ResetUserShoppingInfo();

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;
	SShoppingUserInfo	m_sUserInfo;
	int m_iaMaterialCount[MAX_SHOPPING_MISSION_MATERIAL_TYPE];
	time_t m_tMissionCheckTime;
};

