qpragma once
qinclude "Lock.h"

class CUser;
class CFSGameUserOpenBoxEvent
{
public:

	CFSGameUserOpenBoxEvent(CUser* pUser);
	~CFSGameUserOpenBoxEvent();

	BOOL Load();
	void SendEventInfo();
	RESULT_EVENT_OPEN_BOX_UPDATE UpdateOpenBoxReq(BYTE btType, SS2C_EVENT_OPEN_BOX_UPDATE_RES& rs);
	BOOL GiveKey();
	BYTE GetBoxType();
	int GetKeyCount();

private:
	CUser* m_pUser;
	BOOL m_bDataLoaded;

	BYTE m_btBoxType;
};