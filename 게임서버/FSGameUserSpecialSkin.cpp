qinclude "stdafx.h"
qinclude "FSGameUserSpecialSkin.h"
qinclude "CFSGameUser.h"
qinclude "ThreadODBCManager.h"
qinclude "SpecialSkinManager.h"
qinclude "CFSItemList.h"
qinclude "CBillingManagerGame.h"

CFSGameUserSpecialSkin::CFSGameUserSpecialSkin(CFSGameUser* pUser)
:m_pUser (pUser)
{
}


CFSGameUserSpecialSkin::~CFSGameUserSpecialSkin(void)
{
}

BOOL CFSGameUserSpecialSkin::Load( void )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	vector<SUSERSPECIALSKIN>	vecUserSpecialSkin;
	if( ODBC_RETURN_SUCCESS != pODBCBase->SPECIALSKIN_GetUserInventory(m_pUser->GetGameIDIndex(), vecUserSpecialSkin) )
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, INITIALIZE_DATA, FAIL, "Load-SPECIALSKIN_GetUserInventory\tGameIDIndex:10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}
	SpecialSkinUserDataSet( vecUserSpecialSkin );
	//CheckExpireSpecialSkin();
	return TRUE;
}

void CFSGameUserSpecialSkin::SpecialSkinUserDataSet( vector<SUSERSPECIALSKIN> vecSkinInfo )
{
	CLock lock(&m_SyncmapSpecialSkin);
	for( size_t iIndex = 0 ; iIndex < vecSkinInfo.size() ; ++iIndex )
	{
		SPECIALSKINManager.ConvertToSkillNoAndKind(	vecSkinInfo[iIndex].iSkinCode, vecSkinInfo[iIndex].iSkillNo, vecSkinInfo[iIndex].iKind);
		m_mapSpecialSkin.insert(MAP_SPECIALSKIN::value_type(vecSkinInfo[iIndex].iItemIndex, vecSkinInfo[iIndex]));
	}
}

void CFSGameUserSpecialSkin::EquipSpecailSkin( CPacketComposer& Packet, int iSpecialSkinCode, int iItemIndex)
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);
	SAvatarInfo* pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);
	SUserSpecialSkin sInfo;
	MAP_SPECIALSKIN::iterator	iter;
	BEGIN_SCOPE_LOCK(m_SyncmapSpecialSkin)
		iter = m_mapSpecialSkin.find( iItemIndex );
		CHECK_CONDITION_RETURN_VOID( iter == m_mapSpecialSkin.end() );

		sInfo = (iter->second);
	END_SCOPE_LOCK
	 
	int iSkillNo, btKind;
	iSkillNo = btKind = sInfo.iSkinCode;
	SPECIALSKIN_SKILLNO_CONVERSION(iSkillNo);
	SPECIALSKIN_KIND_CONVERSION(btKind);

	SS2C_SPECIALSKIN_EQUIP_RES rs;

	if(FALSE == SPECIALSKINManager.IsTransformSkinBySkillNo(iSkillNo) &&
		FALSE == pAvatarInfo->Skill.IsOwned(iSkillNo, (int)btKind) ) // 통일되었음.
	{
		if( SKILL_TYPE_SKILL == (SkillType)btKind )
			rs.btErrorCode = static_cast<BYTE>(RESULT_SPECIALSKIN_EQUIP_ERROR_DOESNOTOWN_SKILL);
		else if( SKILL_TYPE_FREESTYLE == (SkillType)btKind )
			rs.btErrorCode = static_cast<BYTE>(RESULT_SPECIALSKIN_EQUIP_ERROR_DOESNOTOWN_FREESTYLE);
		else 
			rs.btErrorCode = RESULT_SPECIALSKIN_EQUIP_ERROR_DB;

		Packet.Add((PBYTE)&rs, sizeof(rs));
		m_pUser->Send(&Packet);
		return;
	}
	
	int iGamePosition = m_pUser->GetCurUsedAvatarPosition();
	CHECK_CONDITION_RETURN_VOID( FALSE == SPECIALSKINManager.CheckMyPositionSkillInfo(iSkillNo, (int)btKind, iGamePosition ))

	int iUnEquipIndex = CheckSpecialSkin_OverLapAndGetInventoryIndex(iSkillNo, (int)btKind);

	if( ODBC_RETURN_SUCCESS != pODBCBase->SPECIALSKIN_UpdateUserInventory( m_pUser->GetGameIDIndex(), iItemIndex, SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP, iUnEquipIndex)) 
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, EXEC_SP, FAIL, "SPECIALSKIN_UpdateUserInventory - Equip \tGameIDIndex:10752790\tInventoryIndex = 0"
m_pUser->GetGameIDIndex(), iItemIndex);

		rs.btErrorCode = static_cast<BYTE>(RESULT_SPECIALSKIN_EQUIP_ERROR_DB);
		Packet.Add((PBYTE)&rs, sizeof(rs));
		m_pUser->Send(&Packet);
		return;
	}
	
	UpdateSpecialSkinStatus((BYTE)SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP, iItemIndex,iSpecialSkinCode , iUnEquipIndex);

	SendUserSpecialSkinEquipList(iGamePosition, m_pUser->GetCurUsedAvatarSpecialCharacterIndex());
	SendUserSpecialSkinInventoryList(iGamePosition);
}

void CFSGameUserSpecialSkin::Un_EquipSpecailSkin( CPacketComposer& Packet, int iSpecialSkinCode, int iInventoryIndex )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	MAP_SPECIALSKIN::iterator	iter;
	SUserSpecialSkin sInfo;
	BEGIN_SCOPE_LOCK(m_SyncmapSpecialSkin)
		iter = m_mapSpecialSkin.find( iInventoryIndex );
		CHECK_CONDITION_RETURN_VOID( iter == m_mapSpecialSkin.end() );

		sInfo = (iter->second);
	END_SCOPE_LOCK

	int iSkillNo, btKind;
	iSkillNo = btKind = sInfo.iSkinCode;
	SPECIALSKIN_SKILLNO_CONVERSION(iSkillNo);
	SPECIALSKIN_KIND_CONVERSION(btKind);
	int iGamePosition = m_pUser->GetCurUsedAvatarPosition();
	CHECK_CONDITION_RETURN_VOID( FALSE == SPECIALSKINManager.CheckMyPositionSkillInfo(iSkillNo, (int)btKind,iGamePosition ));
		
	SS2C_SPECIALSKIN_UNEQUIP_RES rs;
	if( ODBC_RETURN_SUCCESS != pODBCBase->SPECIALSKIN_UpdateUserInventory( m_pUser->GetGameIDIndex(), iInventoryIndex, SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY, -1)) 
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, EXEC_SP, FAIL, "SPECIALSKIN_UpdateUserInventory - UnEquip \tGameIDIndex:10752790\tInventoryIndex = 0"
m_pUser->GetGameIDIndex(), iInventoryIndex);

		rs.btErrorCode = static_cast<BYTE>(RESULT_SPECIALSKIN_UNEQUIP_ERROR_DB);
		Packet.Add((PBYTE)&rs, sizeof(rs));
		m_pUser->Send(&Packet);
		return;
	}
	UpdateSpecialSkinStatus((BYTE)SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY, iInventoryIndex,iSpecialSkinCode, -1 );

	rs.btErrorCode = static_cast<BYTE>(RESULT_SPECIALSKIN_UNEQUIP_SUCCESS);

	SendUserSpecialSkinEquipList(iGamePosition, m_pUser->GetCurUsedAvatarSpecialCharacterIndex());
	SendUserSpecialSkinInventoryList(iGamePosition);
}

void CFSGameUserSpecialSkin::Un_EquipSpecailSkin( int iInventoryIndex )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID( pODBCBase );

	if( ODBC_RETURN_SUCCESS != pODBCBase->SPECIALSKIN_UpdateUserInventory( m_pUser->GetGameIDIndex(), iInventoryIndex, SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY, -1)) 
	{
		WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, EXEC_SP, FAIL, "SPECIALSKIN_UpdateUserInventory - UnEquip2 \tGameIDIndex:10752790\tInventoryIndex = 0"
m_pUser->GetGameIDIndex(), iInventoryIndex);
		return;
	}
	UpdateSpecialSkinStatus((BYTE)SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY, iInventoryIndex,-1, -1 );
}

void CFSGameUserSpecialSkin::MakePacketUser_EquipSpecialSKinList( CPacketComposer& Packet )
{
	SUserSpecialSkin sInfo;
	SS2C_SPECIALSKIN_MY_EQUIP_LIST_RES rs;
	//CheckExpireSpecialSkin();	// in lock

	CLock	lock(&m_SyncmapSpecialSkin);

	BYTE btCount = 0;
	PBYTE pbtCountLocation = Packet.GetTail();
	Packet.Add(btCount);

	MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin();

	while( iter != m_mapSpecialSkin.end() )
	{
		sInfo = (iter->second);
		if( SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == sInfo.btStatus )
		{
			rs.iSpecialSkinCode = sInfo.iSkinCode;
			rs.iComponentIndex = sInfo.iComponentIndex;
			rs.iRelateIndex = sInfo.iRelateIndex;
			Packet.Add((BYTE*)&rs, sizeof(rs));
		}
		++iter, ++btCount;
	}

	memcpy(pbtCountLocation, &btCount, sizeof(btCount));
}

void CFSGameUserSpecialSkin::UpdateSpecialSkinStatus( BYTE btStatus , int iItemIndex, int iSpecialSkinCode, int iUnEquipIndex)
{
	CLock	lock(&m_SyncmapSpecialSkin);

	MAP_SPECIALSKIN::iterator iter =  m_mapSpecialSkin.find( iItemIndex );

	if ( iter != m_mapSpecialSkin.end() )
	{
		iter->second.btStatus = btStatus;
	}

	if( iUnEquipIndex > -1 )
	{
		iter = m_mapSpecialSkin.find( iUnEquipIndex );
		if( iter != m_mapSpecialSkin.end() )
		{
			iter->second.btStatus = SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY;
		}
	}
}

BOOL CFSGameUserSpecialSkin::BuySpecialSkinCard( int& iErrorCode, SC2S_SPECIALSKIN_BUY_REQ sBuyInfo, CFSItemShop* pItemShop )
{
	CHECK_NULL_POINTER_BOOL( pItemShop );
	const SSPECIALSKINCARD_SHOPINFO* pSkinCardInfo =  SPECIALSKINManager.GetSpecialSkinCardInfo(sBuyInfo.iSkinCardIndex);
	CHECK_NULL_POINTER_BOOL( pSkinCardInfo );

	CHECK_CONDITION_RETURN(FALSE == SPECIALSKINManager.CheckBuySpecialSkinCardCondition(iErrorCode,pSkinCardInfo->_iSkinCardIndex 
		, m_pUser->GetCurUsedAvtarLv(), m_pUser->GetCurUsedAvatarPosition(), m_pUser->GetCoin() + m_pUser->GetEventCoin(), sBuyInfo.btColoNum), FALSE);

	if( m_pUser->GetPresentList(MAIL_PRESENT_ON_AVATAR)->GetSize() >= MAX_PRESENT_LIST )
	{
		iErrorCode = RESULT_SPECIALSKIN_BUY_MAILBOXISFULL;
		return FALSE;
	}

	if( TRUE == CheckOwnedSpecialSkin(pSkinCardInfo->_iShowSkinCode, pSkinCardInfo->_iSkinCardIndex, sBuyInfo.btColoNum, 
			sBuyInfo.iComponentIndex , sBuyInfo.iRelateIndex) )
	{
		iErrorCode = RESULT_SPECIALSKIN_BUY_ALREADY_OWNED;
		return FALSE;
	}
	
	if( TRUE == SPECIALSKINManager.CheckAdditionalEffects(pSkinCardInfo->_iShowSkinCode) && 
		FALSE == SPECIALSKINManager.CheckSpecialSkinAdditionalEffects(pSkinCardInfo->_iShowSkinCode, sBuyInfo.iComponentIndex, sBuyInfo.iRelateIndex ) )
	{
		iErrorCode = RESULT_SPECIALSKIN_BUY_NO_COMPONENT;
		return FALSE;
	}

	SSPECIALSKINCARD_ITEM sItemInfo;
	CHECK_CONDITION_RETURN(FALSE == SPECIALSKINManager.CheckAndGetRandomResult_SpecialSkinInfo( iErrorCode , pSkinCardInfo->_iSkinCardIndex, sItemInfo), FALSE);
		
	if( SPECIALSKIN_REWARD_TYPE_SPECIALSKIN == sItemInfo._btRewardType )
	{
		SPECIALSKINManager.CheckSpecialSkinAdditionalEffects(pSkinCardInfo->_iShowSkinCode, sBuyInfo.iComponentIndex, sBuyInfo.iRelateIndex );
		SPECIALSKINManager.ConvertSpecialSkinCodeColor( sItemInfo._iItemCode, sBuyInfo.btColoNum );
	}

	//////////////// 구매부분 ///////////////////

	SBillingInfo BillingInfo;

	BillingInfo.iEventCode = EVENT_BUY_SPECIALSKIN_ITEM;
	BillingInfo.pUser = m_pUser;
	memcpy(BillingInfo.szUserID, m_pUser->GetUserID(), MAX_USERID_LENGTH+1);
	BillingInfo.iSerialNum = m_pUser->GetUserIDIndex();
	memcpy(BillingInfo.szGameID, m_pUser->GetGameID(), MAX_GAMEID_LENGTH + 1);	
	memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1);
	memcpy(BillingInfo.szIPAddress, m_pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;

	BillingInfo.iItemCode = sItemInfo._iItemCode;


	if( SPECIALSKIN_REWARD_TYPE_SPECIALSKIN == sItemInfo._btRewardType )
	{
		int iSkillNo = 0, iKind = 0; 
		SPECIALSKINManager.ConvertToSkillNoAndKind(sItemInfo._iItemCode, iSkillNo, iKind);

		char sztemp[MAX_ITEMNAME_LENGTH+1] = {NULL,};

		sprintf(sztemp, "SpecialSkin: ");
		strcat(sztemp , pItemShop->GetItemTextCheckAndReturnEventCodeDescription( iSkillNo + (iKind * 1000), EVENT_BUY_SPECIALSKIN_ITEM ));
		memcpy(BillingInfo.szItemName, sztemp , MAX_ITEMNAME_LENGTH+1);
	}
	else
	{
		char* szItemName = NULL;

		szItemName = pItemShop->GetItemTextCheckAndReturnEventCodeDescription( sItemInfo._iItemCode ,EVENT_BUY_SPECIALSKIN_ITEM );
		memcpy(BillingInfo.szItemName, szItemName , MAX_ITEMNAME_LENGTH+1);
	}


	BillingInfo.iSellType = static_cast<int>(pSkinCardInfo->_btSellType);
 	BillingInfo.iCurrentCash = m_pUser->GetCoin();
 	BillingInfo.iCashChange = pSkinCardInfo->_iPrice;
 	const int iPublisherIDIndex = m_pUser->GetPublisherIDIndex();
	BillingInfo.iParam0 = sItemInfo._iValue;
	BillingInfo.iParam1 = sBuyInfo.iComponentIndex;
	BillingInfo.iParam2 = sBuyInfo.iRelateIndex;
	BillingInfo.iParam3 = static_cast<int>(sItemInfo._btRewardType);
	BillingInfo.iParam4 = sItemInfo._iPropertyType;
	BillingInfo.iParam5 = pSkinCardInfo->_iSkinCardIndex;
	BillingInfo.iParam6 = pSkinCardInfo->_iShowSkinCode;
	BillingInfo.iItemPrice = pSkinCardInfo->_iPrice;

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		iErrorCode = RESULT_SPECIALSKIN_BUY_BILLING_FAIL;
		return FALSE;
	}
	
	iErrorCode = RESULT_SPECIALSKIN_BUY_SUCCESS;

	return TRUE;
}

BOOL CFSGameUserSpecialSkin::BuySpecialSkinCard_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuySpecialSkinItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}
	
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL( pODBCBase );
	
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int iSellType = pBillingInfo->iSellType;
	BYTE btRewardType = static_cast<BYTE>(pBillingInfo->iParam3);
	int iSkinCardIndex = pBillingInfo->iParam5;
	SS2C_SPECIALSKIN_BUY_RES rs;
	ZeroMemory( &rs, sizeof(SS2C_SPECIALSKIN_BUY_RES));
	rs.btErrorCode = RESULT_SPECIALSKIN_BUY_SUCCESS;
	rs.iEventBonusPoint = 0;
	rs.iEventBonusCash = 0;

	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			int iItemIndex = 0, iPresentIndex = 0;
			if( ODBC_RETURN_SUCCESS == pODBCBase->ITEM_BuySpecialSkinCard( pBillingInfo->iSerialNum, m_pUser->GetGameIDIndex(), 
				pBillingInfo->iItemCode, btRewardType , pBillingInfo->iParam0 /* iValue */, iSellType, pBillingInfo->iItemPrice, 
				pBillingInfo->iParam1 /* ComponentIndex */ , pBillingInfo->iParam2 /* RelateIndex */,iPrevCash , iPostCash ,iSkinCardIndex
				, iItemIndex, iPresentIndex))
			{
				int iPreSkillPoint = m_pUser->GetSkillPoint();
				int iPreEventCoin = m_pUser->GetEventCoin();

				m_pUser->SetPayCash(pBillingInfo->iCashChange);
				if(pBillingInfo->bUseEventCoin == TRUE)
					m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

				m_pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pODBCBase);

				rs.iEventBonusPoint = m_pUser->GetSkillPoint() - iPreSkillPoint;
				rs.iEventBonusCash = m_pUser->GetEventCoin() - iPreEventCoin;
				if(rs.iEventBonusPoint > 0 || rs.iEventBonusCash > 0)
					rs.btErrorCode = RESULT_SPECIALSKIN_BUY_SUCCESS_WITH_BONUSPOINT;

				if( SPECIALSKIN_REWARD_TYPE_POINT == btRewardType )
					m_pUser->SetSkillPoint(m_pUser->GetSkillPoint() + pBillingInfo->iParam0);		
				
				m_pUser->SetUserBillResultAtMem(pBillingInfo->iSellType, pBillingInfo->iCurrentCash-pBillingInfo->iCashChange, 0, 0, pBillingInfo->iBonusCoin );
				m_pUser->SendUserGold();

				rs.btRewardType = btRewardType;
				rs.iItemCode = pBillingInfo->iItemCode;
				rs.iPropertyType = pBillingInfo->iParam4;
				rs.iValue = pBillingInfo->iParam0;

				if( SPECIALSKIN_REWARD_TYPE_POINT != btRewardType )
					SpecialSkinCard_GiveItem( btRewardType, rs.iItemCode, iItemIndex, pBillingInfo->iParam1, pBillingInfo->iParam2, iPresentIndex);

				CPacketComposer	Packet(S2C_SPECIALSKIN_BUY_RES);
				Packet.Add((PBYTE)&rs, sizeof(SS2C_SPECIALSKIN_BUY_RES));
				m_pUser->Send(&Packet);
			}
		}
		break;
	default:
		{
			rs.btErrorCode = RESULT_SPECIALSKIN_BUY_CASH_SHORTAGE;
		}
		break; 
	}

	return (RESULT_SPECIALSKIN_BUY_SUCCESS == rs.btErrorCode || RESULT_SPECIALSKIN_BUY_SUCCESS_WITH_BONUSPOINT == rs.btErrorCode);
}

void CFSGameUserSpecialSkin::SpecialSkinCard_GiveItem( BYTE btRewardType, int iItemCode , int iItemIndex , int iComponentIndex, int iRelateIndex, int iPresentIndex)
{
	if( SPECIALSKIN_REWARD_TYPE_SPECIALSKIN == btRewardType )
	{
		SUserSpecialSkin	sInfo; 
		sInfo.iSkillNo = sInfo.iKind = sInfo.iSkinCode = iItemCode;
		SPECIALSKIN_SKILLNO_CONVERSION(sInfo.iSkillNo);
		SPECIALSKIN_KIND_CONVERSION(sInfo.iKind);
		sInfo.iComponentIndex = iComponentIndex;
		sInfo.iRelateIndex = iRelateIndex;
		sInfo.iItemIndex = iItemIndex;
		sInfo.btStatus = SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY;

		BEGIN_LOCK
			CLock lock(&m_SyncmapSpecialSkin);
			m_mapSpecialSkin.insert(MAP_SPECIALSKIN::value_type(iItemIndex, sInfo ));
		END_LOCK

		SendUserSpecialSkinEquipList(m_pUser->GetCurUsedAvatarPosition(), m_pUser->GetCurUsedAvatarSpecialCharacterIndex());
		SendUserSpecialSkinInventoryList(m_pUser->GetCurUsedAvatarPosition());
	}
	else if( SPECIALSKIN_REWARD_TYPE_ITEM == btRewardType )
	{
		m_pUser->RecvPresent(MAIL_PRESENT_ON_AVATAR,iPresentIndex);
	}
}

void CFSGameUserSpecialSkin::CheckExpireSpecialSkin()
{
	CFSODBCBase*	pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	CLock	lock(&m_SyncmapSpecialSkin);
	time_t tCurrentTime = _time64(NULL);

	for( MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin() ; 
		iter != m_mapSpecialSkin.end() ; )
	{
		SUserSpecialSkin sInfo = (iter->second);
		double dDiffSeconds = difftime(tCurrentTime, sInfo.tExpireDate);
		if( dDiffSeconds > 0 )
		{
			if( ODBC_RETURN_SUCCESS == pODBCBase->SPECIALSKIN_UpdateUserInventory( m_pUser->GetGameIDIndex(), sInfo.iItemIndex ,SPECIALSKIN_STATUS_INVENTORY_FLAG_EXPIRED, -1))
				m_mapSpecialSkin.erase(iter++);
			else
				++iter;
		}
		else
			++iter;
	}
}

BOOL CFSGameUserSpecialSkin::CheckAndGetOwnedSpecialSkin( int iSkinCode, int iSkinIndex, SUserSpecialSkin& sInfo )
{
	ZeroMemory(&sInfo, sizeof(SUserSpecialSkin));

	BOOL bResultOwnedSkin = FALSE;

	CLock	lock(&m_SyncmapSpecialSkin);

	int iSkillNo[MaxCheckSkin], iKind[MaxCheckSkin], iColor[MaxCheckSkin], iEffectType[MaxCheckSkin];
	for( MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin() ; 
		iter != m_mapSpecialSkin.end() ; ++iter )
	{
		int iUserSkinCode = (iter->second).iSkinCode;
		int iColorMaxNum = SPECIALSKINManager.GetMaxColorCode(iSkinIndex);

		SPECIALSKINManager.ConvertToSkillNoKindColorEffectType(iUserSkinCode, iSkillNo[Mine], iKind[Mine], iColor[Mine], iEffectType[Mine]);
		SPECIALSKINManager.ConvertToSkillNoKindColorEffectType(iSkinCode, iSkillNo[Target], iKind[Target], iColor[Target], iEffectType[Target]);
		if( iSkillNo[Mine] == iSkillNo[Target] &&
			iKind[Mine] == iKind[Target] &&
			iColor[Mine] == iColor[Target] 
			/*iEffectType[Mine] == iEffectType[Target]*/ )
		{
			if( SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == (iter->second).btStatus )
			{
				sInfo = (iter->second);
				return TRUE;
			}
			bResultOwnedSkin = TRUE;
		}
	}
	return bResultOwnedSkin;
}

void CFSGameUserSpecialSkin::SendUserSpecialSkinInventoryList( int iGamePosition )
{
	CPacketComposer	Packet(S2C_SPECIALSKIN_INVENTORYLIST_RES);

	SS2C_SPECIALSKIN_INVENTORYLIST_RES rs;

	CLock	lock(&m_SyncmapSpecialSkin);

	time_t tCurrentTime = _time64(NULL);
	BYTE btCount = 0;
	PBYTE pbtCountLocation = Packet.GetTail();
	Packet.Add(btCount);

	MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin();

	while( iter != m_mapSpecialSkin.end() )
	{
		SUserSpecialSkin sInfo = (iter->second);
		if( SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY == sInfo.btStatus )
		{
			rs.iSpecialSkinCode = sInfo.iSkinCode;
			rs.iComponentIndex = sInfo.iComponentIndex;
			rs.iRelateIndex = sInfo.iRelateIndex;
			rs.btStatus	= sInfo.btStatus;

			int iDiffSecond = difftime(sInfo.tExpireDate, tCurrentTime);
			rs.iRemainHours = (iDiffSecond) / 3600;
			
			rs.iInventoryIndex = sInfo.iItemIndex;

			rs.bTransformSkin = SPECIALSKINManager.IsTransformSkinBySkinCode(rs.iSpecialSkinCode);

			Packet.Add((BYTE*)&rs, sizeof(rs));
			++btCount;
		}
		++iter;
	}

	memcpy(pbtCountLocation, &btCount, sizeof(btCount));

	m_pUser->Send(&Packet);
}

void CFSGameUserSpecialSkin::SendUserSpecialSkinEquipList( int iGamePosition, int iSpecialCharacterIndex )
{
	CPacketComposer	Packet(S2C_SPECIALSKIN_EQUIPSLOTLIST_RES);
	const LIST_SPECIALSKINANDSKILLINFO* listSpecialSkinPosition = SPECIALSKINManager.GetSpecialSkinPositionList( iGamePosition );
	SAvatarInfo * pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);
	time_t tCurrentTime = _time64(NULL);

	//CheckExpireSpecialSkin();	// in lock
	if( NULL != listSpecialSkinPosition )
	{
		BYTE btCount = 0;
		PBYTE pbtCountLocation = Packet.GetTail();
		Packet.Add(btCount);

		for(LIST_SPECIALSKINANDSKILLINFO::const_iterator	c_iter = listSpecialSkinPosition[iGamePosition].begin() ;
			c_iter != listSpecialSkinPosition[iGamePosition].end() ; ++c_iter)
		{
			S_ODBC_SPECIALSKINANDSKILLINFO sInfo = (*c_iter);
			if( FALSE == SPECIALSKINManager.CheckEnableSkinCard(sInfo._iSkinCardIndex, sInfo._iGamePosition, iSpecialCharacterIndex))
				continue;

			SS2C_SPECIALSKIN_EQUIPSLOTLIST_RES rs;
			SUserSpecialSkin sUserSkinInfo;
			BYTE btPreviewState = SPECIALSKIN_PREVIEW_CONDITION_NOTWEAR;
			BOOL bIsOwnedSkill = pAvatarInfo->Skill.IsOwned(sInfo._iSkillNo, (int)sInfo._btKind);
			BOOL bIsOwnedSpecialSkin = CheckAndGetOwnedSpecialSkin(sInfo._iItemCode, sInfo._iSkinCardIndex, sUserSkinInfo);	// in lock
			BOOL bIsUsedSkill = pAvatarInfo->Skill.IsUsedSkill(sInfo._iSkillNo, (int)sInfo._btKind);
			
			if ( FALSE == bIsOwnedSkill && FALSE == bIsOwnedSpecialSkin && SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == sUserSkinInfo.btStatus)
			{
				sUserSkinInfo.btStatus = SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY;
				Un_EquipSpecailSkin(sUserSkinInfo.iItemIndex);
			}
			

			if( SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == sUserSkinInfo.btStatus )
				btPreviewState = SPECIALSKIN_PREVIEW_CONDITION_OFWEAR;
			else if( TRUE == bIsOwnedSkill && TRUE == bIsOwnedSpecialSkin)
				btPreviewState = SPECIALSKIN_PREVIEW_CONDITION_CANBEWORN;
			else if( SKILL_TYPE_FREESTYLE == sInfo._btKind && FALSE == bIsOwnedSkill && TRUE == bIsOwnedSpecialSkin )
				btPreviewState = SPECIALSKIN_PREVIEW_CONDITION_PURCHASE_REQUIRED_FREESTYLE;
			else if( SKILL_TYPE_SKILL == sInfo._btKind && FALSE == bIsOwnedSkill && TRUE == bIsOwnedSpecialSkin)
				btPreviewState = SPECIALSKIN_PREVIEW_CONDITION_PURCHASE_REQUIRED_SKILL;
			else
				btPreviewState = SPECIALSKIN_PREVIEW_CONDITION_NOTWEAR;

			if( SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP != sUserSkinInfo.btStatus )
				ZeroMemory(&sUserSkinInfo, sizeof(SUserSpecialSkin));
			
			rs.iTargetSkillNo = sInfo._iSkillNo;
			rs.iTargetKind = (int)sInfo._btKind;
			rs.btPreviewState = btPreviewState;
			rs.iSpecialSkinCode = sUserSkinInfo.iSkinCode;
			rs.iInventoryIndex = sUserSkinInfo.iItemIndex;
			rs.iComponentIndex = sUserSkinInfo.iComponentIndex;
			rs.iRelateIndex = sUserSkinInfo.iRelateIndex;
			
			if( 0 < sUserSkinInfo.iSkinCode )
			{
				int iDiffSecond = difftime(sUserSkinInfo.tExpireDate, tCurrentTime);	
				rs.iRemainHours = (iDiffSecond) / 3600;
			}
			else
				rs.iRemainHours = 0;

			rs.btInventoryStatus = sUserSkinInfo.btStatus;  //enum eSPECIALSKIN_STATUS_INVENTORY_FLAG
			rs.bTransformSkin = SPECIALSKINManager.IsTransformSkinBySkillNo(rs.iTargetSkillNo);
			if(TRUE == rs.bTransformSkin)
			{
				if(SPECIALSKIN_PREVIEW_CONDITION_PURCHASE_REQUIRED_SKILL == rs.btPreviewState ||
					SPECIALSKIN_PREVIEW_CONDITION_PURCHASE_REQUIRED_FREESTYLE == rs.btPreviewState)
					rs.btPreviewState = SPECIALSKIN_PREVIEW_CONDITION_CANBEWORN;
			}

			Packet.Add((PBYTE)&rs, sizeof( SS2C_SPECIALSKIN_EQUIPSLOTLIST_RES ));
			++btCount;
		}
		memcpy(pbtCountLocation, &btCount, sizeof(btCount));

		m_pUser->Send(&Packet);
	}
}

int CFSGameUserSpecialSkin::CheckSpecialSkin_OverLapAndGetInventoryIndex( int iSkillNo, int iKind )
{
	CLock	lock(&m_SyncmapSpecialSkin);
	for( MAP_SPECIALSKIN::iterator	 iter = m_mapSpecialSkin.begin()
		; iter != m_mapSpecialSkin.end() ; ++iter )
	{
		SUserSpecialSkin sInfo = (iter->second);
		if( iSkillNo == sInfo.iSkillNo && iKind == sInfo.iKind &&	
			SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == sInfo.btStatus )
		{
			return (iter->second).iItemIndex;
		}
	}

	return -1;
}

void CFSGameUserSpecialSkin::GetUsedSpecialSkinAdditionalEffects( vector<SSpecialSkinAdditionaleffectsInfo>& vec )
{
	SAvatarInfo* pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);
	
	CLock	lock(&m_SyncmapSpecialSkin);
	MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin();
	for( ; iter != m_mapSpecialSkin.end() ; ++iter )
	{
		SUserSpecialSkin	sUserSkinInfo = (iter->second);

		
		if(SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP != sUserSkinInfo.btStatus || SPECIALSKIN_COMPONENT_FREESTYLE != sUserSkinInfo.iComponentIndex)
			continue;

		if(FALSE == pAvatarInfo->Skill.IsOwned(sUserSkinInfo.iSkillNo, sUserSkinInfo.iKind) &&
			FALSE == SPECIALSKINManager.IsTransformSkinBySkinCode(sUserSkinInfo.iSkinCode))
			continue;

		SSpecialSkinAdditionaleffectsInfo	sInfo;
		sInfo.iSkillNo = sUserSkinInfo.iSkillNo;
		sInfo.iKind = sUserSkinInfo.iKind;
		sInfo.iComponentIndex = sUserSkinInfo.iComponentIndex;
		sInfo.iRelateIndex = sUserSkinInfo.iRelateIndex;
		sInfo.iSkinCode = sUserSkinInfo.iSkinCode;
		sInfo.fValue = SPECIALSKINManager.GetAdditionalEffectsValue( sInfo.iComponentIndex, sInfo.iRelateIndex /*&*/, sInfo.iSkillNo, sInfo.iKind );

		vec.push_back(sInfo);
	}
}

BOOL CFSGameUserSpecialSkin::CheckOwnedSpecialSkin( int iSkinCode, int iSkinCardIndex , BYTE btSelectColor, int iComponentIndex, int iRelateIndex)
{
	int iSkillNo[MaxCheckSkin], iKind[MaxCheckSkin], iColor[MaxCheckSkin], iEffectType[MaxCheckSkin];
	SPECIALSKINManager.ConvertToSkillNoKindColorEffectType(iSkinCode, iSkillNo[Mine], iKind[Mine], iColor[Mine], iEffectType[Mine]);

	CLock	lock(&m_SyncmapSpecialSkin);
	MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin();

	for( ; iter != m_mapSpecialSkin.end() ; ++iter )
	{
		LPSUSERSPECIALSKIN	pInfo = &(iter->second);
		SPECIALSKINManager.ConvertToSkillNoKindColorEffectType(pInfo->iSkinCode, iSkillNo[Target], iKind[Target], iColor[Target], iEffectType[Target]);

		if( iSkillNo[Mine] == iSkillNo[Target] && iKind[Mine] == iKind[Target] && btSelectColor == iColor[Target] && 
			iEffectType[Mine] == iEffectType[Target] &&
			pInfo->iComponentIndex == iComponentIndex && pInfo->iRelateIndex == iRelateIndex)
		{
			return TRUE;
		}
	}

	return FALSE;
}

// 스킬이나 프리스타일을 뺄경우 스페셜스킨도 제거해줍니다. 
void CFSGameUserSpecialSkin::CheckAndUnEquipSpecialSKin( int iSkillNo, int iKind ) 
{
	CLock lock(&m_SyncmapSpecialSkin);

	for( MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin(); iter != m_mapSpecialSkin.end() ; ++iter )
	{
		LPSUSERSPECIALSKIN	pInfo = &(iter->second);
		
		if( iSkillNo == pInfo->iSkillNo && iKind == pInfo->iKind && 
			SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == pInfo->btStatus )
		{
			CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID( pODBCBase );

			if( ODBC_RETURN_SUCCESS != pODBCBase->SPECIALSKIN_UpdateUserInventory( m_pUser->GetGameIDIndex(), pInfo->iItemIndex, SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY, -1)) 
			{
				WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, EXEC_SP, FAIL, "SPECIALSKIN_UpdateUserInventory - CheckAndUnEquipSpecialSKin \tGameIDIndex:10752790\tInventoryIndex = 0"
, m_pUser->GetGameIDIndex(), pInfo->iItemIndex);
				return;
			}

			(iter->second).btStatus = SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY;
			break;
		}
	}
}

void CFSGameUserSpecialSkin::Un_EquipSpecialSkin_Kind_All( int iKind )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID( pODBCBase );

	BEGIN_LOCK
		CLock lock(&m_SyncmapSpecialSkin);

		for( MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin(); iter != m_mapSpecialSkin.end() ; ++iter )
		{
			LPSUSERSPECIALSKIN	pInfo = &(iter->second);

			if( iKind == pInfo->iKind && SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == pInfo->btStatus )
			{
				if( ODBC_RETURN_SUCCESS != pODBCBase->SPECIALSKIN_UpdateUserInventory( m_pUser->GetGameIDIndex(), pInfo->iItemIndex, SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY, -1)) 
				{
					WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, EXEC_SP, FAIL, "SPECIALSKIN_UpdateUserInventory - Un_EquipSpecialSkin_Kind_All \tGameIDIndex:10752790\tInventoryIndex = 0"
	, m_pUser->GetGameIDIndex(), pInfo->iItemIndex);
					return;
				}

				(iter->second).btStatus = SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY;
				break;
			}
		}
	END_LOCK
}

void CFSGameUserSpecialSkin::CheckUnEquip_AfterSkillUnEquip_All()
{
	SAvatarInfo* pAvatarInfo =  m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	BEGIN_LOCK
		CLock lock(&m_SyncmapSpecialSkin);
		MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin();
		for( ; iter != m_mapSpecialSkin.end() ; ++iter )
		{
			SUserSpecialSkin* pInfo = &(iter->second);

			if( SPECIALSKIN_STATUS_INVENTORY_FLAG_EQUIP == pInfo->btStatus &&
				FALSE == pAvatarInfo->Skill.IsUsedSkill(pInfo->iSkillNo, pInfo->iKind) )
			{
				if( ODBC_RETURN_SUCCESS != pODBCBase->SPECIALSKIN_UpdateUserInventory( m_pUser->GetGameIDIndex(), pInfo->iItemIndex, SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY, -1)) 
				{
					WRITE_LOG_NEW(LOG_TYPE_SPECIALSKIN, EXEC_SP, FAIL, "SPECIALSKIN_UpdateUserInventory - CheckUnEquip_AfterSkillUnEquip_All \tGameIDIndex:10752790\tInventoryIndex = 0"
	, m_pUser->GetGameIDIndex(), pInfo->iItemIndex);
					return;
				}

				(iter->second).btStatus = SPECIALSKIN_STATUS_INVENTORY_FLAG_INVENTORY;
			}
		}
	END_LOCK
}

BOOL CFSGameUserSpecialSkin::btRetentionOfSkinCard( int iSkinCode )
{
	int iSkillNo[MaxCheckSkin], iKind[MaxCheckSkin], iColor[MaxCheckSkin], iEffectType[MaxCheckSkin];
	SPECIALSKINManager.ConvertToSkillNoKindColorEffectType(iSkinCode, iSkillNo[Mine], iKind[Mine], iColor[Mine], iEffectType[Mine]);

	CLock	lock(&m_SyncmapSpecialSkin);
	MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin();

	for( ; iter != m_mapSpecialSkin.end() ; ++iter )
	{
		LPSUSERSPECIALSKIN	pInfo = &(iter->second);
		SPECIALSKINManager.ConvertToSkillNoKindColorEffectType(pInfo->iSkinCode, iSkillNo[Target], iKind[Target], iColor[Target], iEffectType[Target]);

		if( iSkillNo[Mine] == iSkillNo[Target] && 
			iKind[Mine] == iKind[Target] && 
			//iColor[Mine] == iColor[Target] &&
			iEffectType[Mine] == iEffectType[Target] )
			return TRUE;
	}

	return FALSE;
}

void CFSGameUserSpecialSkin::GetUserRetentionOfSkilCode(list<int>& listRetentionSkinCode)
{
	for( MAP_SPECIALSKIN::iterator	iter = m_mapSpecialSkin.begin() ; iter != m_mapSpecialSkin.end() ; ++iter )
	{
		listRetentionSkinCode.push_back((iter->second).iSkinCode);
	}
}
