qinclude "stdafx.h"
qinclude "FSGameUserRankMatch.h"
qinclude "RankMatchManager.h"
qinclude "CFSGameUser.h"
qinclude "ThreadODBCManager.h"
qinclude "CFSGameServer.h"
qinclude "ODBCSvrProxy.h"
qinclude "GameProxyManager.h"
qinclude "FSLeagueCommon.h"
qinclude "LeagueSvrProxy.h"
qinclude "LogSvrProxy.h"
qinclude "RewardManager.h"

CFSGameUserRankMatch::CFSGameUserRankMatch(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_fLeagueRatingPoint(0)
	, m_dwLastRankPageView(0)
	, m_btBasketBallSkinRewardTier(0)
	, m_iMatchingRefusalCount(0)
	, m_tMatchingLimitRemainTime(0)
{
	m_sLeagueAvatarMyInfo._btPreTier = LEAGUE_MAIN_CATEGORY_NONE;
}


CFSGameUserRankMatch::~CFSGameUserRankMatch(void)
{
}

BOOL CFSGameUserRankMatch::SendPacketAvoidTeamInfo()
{
	CHECK_CONDITION_RETURN(FALSE == LoadAvatarRankMatchPositionInfo(),FALSE);

	list<BYTE> listTemp;
	SS2C_RANKMATCH_AVOID_TEAM_INFO_RES	res;
	BYTE btMyPosition = (BYTE)m_pUser->GetCurUsedAvatarPosition();
	CPacketComposer	Packet(S2C_RANKMATCH_AVOID_TEAM_INFO_RES);
	res.btMyPosition = static_cast<BYTE>(m_pUser->GetCurUsedAvatarPosition());
	res.btAvoidTeamCount = (BYTE)m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.size();
	int iMaxCheckCount = RANKMATCH.GetAvoidMaxCheckCount(btMyPosition) - m_sLeagueAvatarTeamPosition.GetCheckCount();

	if( 0 > iMaxCheckCount )
		iMaxCheckCount = 0;

	res.btMaxCheckCount = (BYTE)iMaxCheckCount ;

	Packet.Add((BYTE*)&res, sizeof(SS2C_RANKMATCH_AVOID_TEAM_INFO_RES));

	MAP_LEAGUETeamPosition::iterator iter = m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.begin();
	for( ; iter != m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.end() ; ++iter )
	{
		SLeagueTeamPosition* pInfo = &(iter->second);

		SRANKMATCH_AVOID_TEAM_POSITION	avoidInfo;

		avoidInfo.btAvoidIndex = (BYTE)pInfo->iTeamIndex;
		avoidInfo.btIsCheck = pInfo->btCheck;

		int iTemp = (pInfo->iWin + pInfo->iLose);
		if( 0 >= iTemp )
		{
			avoidInfo.btWinRate = 0;
		}
		else
		{
			avoidInfo.btWinRate = (BYTE)(pInfo->iWin * 100 / (pInfo->iWin + pInfo->iLose));
		}

		avoidInfo.iMatchCnt = pInfo->iWin + pInfo->iLose;
		RANKMATCH.GetPossibleTeamPosition(&listTemp, btMyPosition, avoidInfo.btAvoidIndex);
		avoidInfo.btPositionCount = listTemp.size();

		Packet.Add((BYTE*)&avoidInfo, sizeof(SRANKMATCH_AVOID_TEAM_POSITION));

		for( auto it = listTemp.begin() ; it != listTemp.end() ; ++it )
		{
			Packet.Add((*it));
		}

		listTemp.clear();
	}

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL CFSGameUserRankMatch::LoadAvatarRankMatchPositionInfo()
{
	CHECK_CONDITION_RETURN( TRUE == m_sLeagueAvatarTeamPosition._bDBLoad , TRUE );

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBCBase, FALSE);

	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_GetAvatarTeamPositionSet( m_pUser->GetGameIDIndex(), m_sLeagueAvatarTeamPosition))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetAvatarTeamPositionSet GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}

	m_sLeagueAvatarTeamPosition._bDBLoad = TRUE;

	return TRUE;
}

BOOL CFSGameUserRankMatch::ReqAvoidTeamSave(list<SRANKMATCH_AVOID_TEAM>&	listAvoidInfo, BYTE& btResult)
{
	btResult = RESULT_RANKMATCH_AVOID_TEAM_SAVE_FAIL;

	CHECK_CONDITION_RETURN(FALSE == LoadAvatarRankMatchPositionInfo(),FALSE);

	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	btResult = RESULT_RANKMATCH_AVOID_TEAM_SAVE_LIMIT_EXCEEDED;
	CHECK_CONDITION_RETURN( listAvoidInfo.size() > (size_t)RANKMATCH.GetAvoidPositionConfigValue((BYTE)m_pUser->GetCurUsedAvatarPosition()), FALSE);

	list<SRANKMATCH_AVOID_TEAM>::iterator	iter = listAvoidInfo.begin();
	list<SRANKMATCH_AVOID_TEAM>::iterator	iterEnd = listAvoidInfo.end();

	int iCheckAvoidTeamIndex[MAX_LEAGUE_AVATAR_TEAMPOSITION_SET]; 
	memset(iCheckAvoidTeamIndex, -1, sizeof(int) * MAX_LEAGUE_AVATAR_TEAMPOSITION_SET);

	size_t iIndex = 0;
	for( iIndex = 0 
		; iter != iterEnd && iIndex < MAX_LEAGUE_AVATAR_TEAMPOSITION_SET 
		; ++iter , ++iIndex )
	{
		iCheckAvoidTeamIndex[iIndex] = (*iter).btAvoidIndex;
	}

	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_UpdateAvoidCheckAvatarTeamPosition(m_pUser->GetGameIDIndex(), iCheckAvoidTeamIndex) )
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetAvatarTeamPositionSet GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
sult = RESULT_RANKMATCH_AVOID_TEAM_SAVE_FAIL;
		return FALSE;
	}

	m_sLeagueAvatarTeamPosition.InitAllCheckPosition();

	for( int i = 0 ; i < MAX_LEAGUE_AVATAR_TEAMPOSITION_SET ; ++i )
	{
		if( 0 < iCheckAvoidTeamIndex[i] )
		{
			MAP_LEAGUETeamPosition::iterator iter = m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.find(iCheckAvoidTeamIndex[i]);

			if( iter != m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.end() )
			{
				(iter->second).btCheck = TRUE;
			}
		}
	}

	btResult = RESULT_RANKMATCH_AVOID_TEAM_SAVE_SUCCESS;

	return TRUE;
}

BOOL CFSGameUserRankMatch::CheckAvoidTeamLimit( BYTE btCount )
{
	int iLimitValue = RANKMATCH.GetConfigValue(RANKMATCH.GetAvoidGamePositionCode(m_pUser->GetCurUsedAvatarPosition()));
	if( iLimitValue < btCount )
	{
		return FALSE;
	}

	return TRUE;
}

void CFSGameUserRankMatch::GetAvatarAvoidTeamIndex( list<BYTE>& listAVoidTeamIndex )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadAvatarRankMatchPositionInfo());

	for( MAP_LEAGUETeamPosition::iterator iter = m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.begin()
		; iter != m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.end() ; ++iter ) 
	{
		if( TRUE == (iter->second).btCheck )
		{
			BYTE btTeamIndex = (BYTE)(iter->second).iTeamIndex;
			listAVoidTeamIndex.push_back(btTeamIndex);
		}
	}
}

BOOL CFSGameUserRankMatch::CheckRankMatchTeamReg( void )
{
	// 레벨>신고>의상>훈련>시간

	CRankMatchManager*	pManager = &RANKMATCH;
	CHECK_NULL_POINTER_BOOL(pManager);

	SAvatarInfo* pAvatarInfo = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatarInfo)

		do 
		{
			int iTrainingStep2 = 1 << 1;

			if( m_pUser->GetCurUsedAvtarLv() < pManager->GetConfigValue(LEAGUE_CONFIG_RequiredLevel) )
			{
				SendAutoTeamRegFail((BYTE)RESULT_AUTO_TEAM_REG_LEVEL_SHORTAGE);
				return FALSE;
			}

			if( (0 < pManager->GetConfigEnable(LEAGUE_CONFIG_CostumeLimit_Upper) && FALSE == m_pUser->GetAvatarItemList()->CheckBasicItemWithChannel(ITEMCHANNEL_UP) ) ||
				(0 < pManager->GetConfigEnable(LEAGUE_CONFIG_CostumeLimit_Pants) && FALSE == m_pUser->GetAvatarItemList()->CheckBasicItemWithChannel(ITEMCHANNEL_DOWN) ) ||
				(0 < pManager->GetConfigEnable(LEAGUE_CONFIG_CostumeLimit_Footwear) && FALSE == m_pUser->GetAvatarItemList()->CheckBasicItemWithChannel(ITEMCHANNEL_SHOES) ))
			{
				SendAutoTeamRegFail((BYTE)RESULT_AUTO_TEAM_REG_NO_COSTUMES);
				return FALSE;
			}

			if( TRUE == pManager->GetConfigEnable(LEAGUE_CONFIG_TrainingLimit) )
			{
				int iLimitValue = pManager->GetConfigValue(LEAGUE_CONFIG_TrainingLimit);

				if( iLimitValue > PARAMETER_INDEX_TRAINING_STEP4+1 )
					iLimitValue = PARAMETER_INDEX_TRAINING_STEP4+1 ;

				int iTrainingStep = 1;
				int iTrainingStep2 = 1 << 1;
				int iTrainingStep3 = 1 << 2;
				int iTrainingStep4 = 1 << 3;

				for( int iIndex = 0 ; iIndex < iLimitValue ; ++iIndex ) 
				{
					int iTrainingStep = 0;
					switch( iIndex )
					{
					case 0: iTrainingStep = 1;	break;
					case 1:	iTrainingStep = 1 << 1; break;
					case 2:	iTrainingStep = 1 << 2; break;
					case 3: iTrainingStep = 1 << 3; break;
					default:break;
					}

					for (int iLoopIndex = STAT_TRAINING_CATEGOTY_RUN; iLoopIndex <= STAT_TRAINING_CATEGOTY_CLOSE; iLoopIndex++)
					{
						if ( 0 == (pAvatarInfo->Skill.iaTraining[iLoopIndex] & iTrainingStep) )
						{
							SendAutoTeamRegFail((BYTE)RESULT_AUTO_TEAM_REG_LACK_TRAINING);
							return FALSE;
						}
					}
				}
			}

			if( TRUE == pManager->GetConfigEnable(LEAGUE_CONFIG_TimeOut_StartTime) )
			{
				if( TRUE == pManager->CheckTimeOut(_GetCurrentDBDate) )
				{
					SendAutoTeamRegFail((BYTE)RESULT_AUTO_TEAM_REG_BREAK_TIME);
					return FALSE;
				}
			}

			if( FALSE == pManager->CheckSeasonOpen(_GetCurrentDBDate) )
			{
				SendAutoTeamRegFail((BYTE)RESULT_AUTO_TEAM_REG_CLOSED);
				return FALSE;
			}

		} while (FALSE);

		if(TRUE == m_pUser->GetUserAppraisal()->CheckPenaltyUser())
		{
			SendAutoTeamRegFail((BYTE)RESULT_AUTO_TEAM_REG_PENALTY_USER);
			return FALSE;
		}


		return TRUE;
}

BOOL CFSGameUserRankMatch::LoadAvatarRankMatchMyInfo()
{
	CHECK_CONDITION_RETURN( TRUE == m_sLeagueAvatarMyInfo._bDBLoad , TRUE );

	CFSLeagueODBC*	pLeagueODBC = dynamic_cast<CFSLeagueODBC*>(ODBCManager.GetODBC(ODBC_LEAGUE));
	CHECK_NULL_POINTER_BOOL(pLeagueODBC);


	if( ODBC_RETURN_SUCCESS != pLeagueODBC->LEAGUE_GetUserInfo(m_pUser->GetGameIDIndex()
		, CFSGameServer::GetInstance()->GetServerIndex() ,m_sLeagueAvatarMyInfo._btTier
		,m_sLeagueAvatarMyInfo._btRating,m_sLeagueAvatarMyInfo._iGroup, m_sLeagueAvatarMyInfo._iLeaguePoint
		, m_sLeagueAvatarMyInfo._btPreTier , m_sLeagueAvatarMyInfo._btIsMaster))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetUserInfo GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}

	m_sLeagueAvatarMyInfo._bDBLoad = TRUE;
	m_pUser->SetCurrentRankMatchTier(m_sLeagueAvatarMyInfo._btTier);

	UpdateRankMatchInfo(m_sLeagueAvatarMyInfo);

	return TRUE;
}

void CFSGameUserRankMatch::UpdateRankMatchScale( void )
{
	for( BYTE btScale = 0 ; btScale < MAX_MATCH_TEAM_SCALE ; ++btScale )
	{
		if( MATCH_TEAM_SCALE_1ON1 == btScale && TRUE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_TeamScale1on1) ||
			MATCH_TEAM_SCALE_2ON2 == btScale && TRUE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_TeamScale2on2) || 
			MATCH_TEAM_SCALE_3ON3 == btScale && TRUE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_TeamScale3on3) || 
			MATCH_TEAM_SCALE_3ON3CLUB == btScale && TRUE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_TeamScale3on3_Club) ) 
		{	
			//	m_stackRankMatchScale.push(btScale);
		}
	}
}

BOOL CFSGameUserRankMatch::Load()
{
	m_sLeagueAvatarMyInfo._btPreTier = LEAGUE_MAIN_CATEGORY_NONE;
	m_pUser->SetCurrentRankMatchTier(LEAGUE_MAIN_CATEGORY_NONE);

	SendRankMatchOpenStatus();
	SendRankMatch_MatchOpenTime();

	if( m_pUser->GetCurUsedAvtarLv() < RANKMATCH.GetConfigValue(LEAGUE_CONFIG_PageRankPositionLimitLevel) )
		return TRUE;

	LoadAvatarRankMatchPositionInfo();
	LoadAvatarSubTable();
	LoadAvatarLimitMatchingInfo();
	SendAvatarRankMatchMyInfo();

	//m_sLeagueAvatarMyInfo._bDBLoad = TRUE;

	return TRUE;
}

void CFSGameUserRankMatch::UpdateRankMatchInfo( SRankMatchUserInfoBase& base )
{
	memcpy(&m_sLeagueAvatarMyInfo, &base, sizeof(SRankMatchUserInfoBase));

	m_sLeagueAvatarMyInfo._bDBLoad = TRUE;

	if( 0 < m_iLoginPenaltyPoint )
	{
		UpdateLeaguePoint(m_sLeagueAvatarMyInfo._iLeaguePoint - m_iLoginPenaltyPoint);
		m_iLoginPenaltyPoint = 0;
	}

	SendUserRankMatchInfo();
	m_pUser->SetCurrentRankMatchTier(m_sLeagueAvatarMyInfo._btTier);

	// 	if( FALSE == m_sUserNotice.bIsSend )
	// 		LoadUserNotice();
}

BOOL CFSGameUserRankMatch::GetLeagueInfo( BYTE& btTier, BYTE& btRating, int& iGroup )
{
	if( TRUE == CheckPlacementTest() ) // 배치고사중
	{
		return FALSE;
	}

	btTier		= m_sLeagueAvatarMyInfo._btTier;
	btRating	= m_sLeagueAvatarMyInfo._btRating;
	iGroup		= m_sLeagueAvatarMyInfo._iGroup;

	return TRUE;
}

BOOL CFSGameUserRankMatch::CheckPlacementTest()
{
	if( LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST == m_sLeagueAvatarSubTable.iSubIndex )
		return TRUE;

	return FALSE;
}

BOOL CFSGameUserRankMatch::LoadAvatarSubTable()
{
	CHECK_CONDITION_RETURN( TRUE == m_sLeagueAvatarSubTable.bDbLoad, TRUE );

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	time_t tlastlogin;
	m_iLoginPenaltyPoint = 0;
	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_GetAvatarSubTable( m_pUser->GetGameIDIndex(), m_sLeagueAvatarSubTable, tlastlogin))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetAvatarSubTable GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}

	m_sLeagueAvatarSubTable.bDbLoad = TRUE;

	if( LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST == m_sLeagueAvatarSubTable.iSubIndex  )
		m_sLeagueAvatarMyInfo._bPlacementTest = TRUE;
	else
		m_sLeagueAvatarMyInfo._bPlacementTest = FALSE;

	time_t tcurrenttime = _GetCurrentDBDate;

	if( 0 < tlastlogin && TRUE == RANKMATCH.IsSeasonOpen(tcurrenttime) )
	{
		int iDiffSecond = (int)difftime(_GetCurrentDBDate, tlastlogin);

		int iDay = iDiffSecond / 86400;

		const int iLoginPenalty = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Loginpenalty_day);

		if( m_sLeagueAvatarSubTable.iSubIndex == LEAGUE_SUB_TABLE_INDEX_NONE &&
			0 < iLoginPenalty &&  // 배치고사나 승강전중 아니고, 패널티받을 날짜가 지났다면
			abs(iDay) >= iLoginPenalty )
		{
			int iMultiple = abs(iDay) / iLoginPenalty;

			const int iPenaltyPoint = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Loginpenalty_point) * iMultiple;

			if( FALSE == m_sLeagueAvatarMyInfo._bDBLoad ) // 아직 로딩 안했다면.
			{
				m_iLoginPenaltyPoint = iPenaltyPoint;
			}
			else
			{
				m_iLoginPenaltyPoint = 0;

				UpdateLeaguePoint(m_sLeagueAvatarMyInfo._iLeaguePoint - iPenaltyPoint);
			}
		}
	}

	return TRUE;
}

SLeagueSubTable* CFSGameUserRankMatch::GetSubTable() const
{
	return const_cast<SLeagueSubTable*>(&m_sLeagueAvatarSubTable);
}

void CFSGameUserRankMatch::GetLeagueMatchInfo( SLeagueMatchInfo& info )
{
	if( FALSE == m_sLeagueAvatarSubTable.bDbLoad )
	{
		if( FALSE == LoadAvatarSubTable() )
		{
			info.Init();
		}
	}
	info.Init();
	if( LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST == m_sLeagueAvatarSubTable.iSubIndex )
	{
		info.iSubIndex = m_sLeagueAvatarSubTable.iSubIndex;

		info.btRating = 0;
		info.btTier = 0;
		info.iGroup = 0;
		info.iLeaguePoint = 0;

		info.btRemainPlacementTest = m_sLeagueAvatarSubTable.btRemainPlacementTest;
		info.fTestRatingPoint = m_sLeagueAvatarSubTable.fTestRating = m_fLeagueRatingPoint;
		
		for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
			info.btOutCome[i] = m_sLeagueAvatarSubTable.btOutCome[i];
	}
	else
	{
		info.iSubIndex = m_sLeagueAvatarSubTable.iSubIndex;

		for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
			info.btOutCome[i] = m_sLeagueAvatarSubTable.btOutCome[i];

		info.btRating = m_sLeagueAvatarMyInfo._btRating;
		info.btTier = m_sLeagueAvatarMyInfo._btTier;
		info.iGroup = m_sLeagueAvatarMyInfo._iGroup;
		info.iLeaguePoint = m_sLeagueAvatarMyInfo._iLeaguePoint;

		info.btRemainPlacementTest = m_sLeagueAvatarSubTable.btRemainPlacementTest;
		info.fTestRatingPoint = m_sLeagueAvatarSubTable.fTestRating = m_fLeagueRatingPoint;

	}
	info.btPreSeasonTier = m_sLeagueAvatarMyInfo._btPreTier;
}

void CFSGameUserRankMatch::UpdateResult( int& iResultType, int& iPromotion, int &iMyLeague
	, int &iMyRating, int &iLtotalPoint, int& iLaddPoint, float fTestRatingPoint, int iLeaguePopupMode
	, short shIsWin )
{
	SLeagueRatingConfig RatingConfig;
	CRankMatchManager* pManager = &RANKMATCH;
	CHECK_NULL_POINTER_VOID(pManager);

	BYTE btPreTier = m_sLeagueAvatarMyInfo._btTier;
	BYTE btPreRating = m_sLeagueAvatarMyInfo._btRating;

	CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);

	if( iResultType == eRMRT_Promotion ) // 배치고사
	{
		if( 0 == iPromotion )
		{ // 배치고사 끝났다.
			// 셋팅해서 넣어줌.

			m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_NONE;
			m_sLeagueAvatarSubTable.btRemainPlacementTest = 0;
			ZeroMemory(m_sLeagueAvatarSubTable.btOutCome, sizeof(BYTE)*MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT);
			m_sLeagueAvatarSubTable.fTestRating = 0.f;

			if( 0 > fTestRatingPoint )
				fTestRatingPoint = 0.f;

			if( TRUE == pManager->GetLeagueRatingInfo(fTestRatingPoint, RatingConfig))
			{
				m_sLeagueAvatarMyInfo._btTier = RatingConfig.btTier;
				m_sLeagueAvatarMyInfo._btRating = RatingConfig.btRating;
				m_sLeagueAvatarMyInfo._iLeaguePoint = pManager->GetConfigValue(LEAGUE_CONFIG_StartLeaguePoint);
				m_sLeagueAvatarMyInfo._bPlacementTest = FALSE;
				m_sLeagueAvatarMyInfo._bDBLoad = FALSE;
				m_sLeagueAvatarMyInfo._iGroup = 0;

				SendInsertBasic(pProxy);

				// 배치고사 끝났으면 렙업했다고 알려주자.
				iResultType = eRMRT_GradeChange;
				iPromotion = 0; // 배치
				iMyLeague = (int)RatingConfig.btTier;
				iMyRating = (int)RatingConfig.btRating;
				iLtotalPoint = m_sLeagueAvatarMyInfo._iLeaguePoint;

				SetRankMatchMyInfoLoad(FALSE);
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LeagueRatingPoint GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
		}
		else 
		{
			for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
			{
				if( LEAGUE_SUB_TABLE_OUTCOME_NONE == m_sLeagueAvatarSubTable.btOutCome[i] ||
					LEAGUE_SUB_TABLE_OUTCOME_NOTYET == m_sLeagueAvatarSubTable.btOutCome[i])
				{
					m_sLeagueAvatarSubTable.btOutCome[i] = (1 == shIsWin ? LEAGUE_SUB_TABLE_OUTCOME_WIN : LEAGUE_SUB_TABLE_OUTCOME_LOSE); 
					break;
				}
			}

			m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST;
			m_sLeagueAvatarSubTable.fTestRating = 0;
			m_sLeagueAvatarSubTable.btRemainPlacementTest = iPromotion;
		}

		UpdateSubTable();
	}
	else if(iResultType == eRMRT_RatingUp ) // 승강전
	{// 1승급, 0배치고사완료, -1강등, -2승급실패
		m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING;
		m_sLeagueAvatarSubTable.btOutCome[0] = (iPromotion / 10000) ;
0;
		m_sLeagueAvatarSubTable.btOutCome[1] = (iPromotion / 1000)	;
0;
		m_sLeagueAvatarSubTable.btOutCome[2] = (iPromotion / 100)	;
0;
		m_sLeagueAvatarSubTable.btOutCome[3] = LEAGUE_SUB_TABLE_OUTCOME_NONE;
		m_sLeagueAvatarSubTable.btOutCome[4] = LEAGUE_SUB_TABLE_OUTCOME_NONE;

		if( 0 == iLtotalPoint ) // 강등전 중인데 탈출못했다면
		{
			iResultType = eRMRT_Normal;
			iMyLeague = m_sLeagueAvatarMyInfo._btTier;
			iMyRating = m_sLeagueAvatarMyInfo._btRating;
			iLtotalPoint = 0;
			iLaddPoint = 0;
		} 
		UpdateSubTable();
	}
	else if( iResultType == eRMRT_LeagueUp )
	{// 1승급, 0배치고사완료, -1강등, -2승급실패
		m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING;
		m_sLeagueAvatarSubTable.btOutCome[0] = (iPromotion / 10000) ;
0;
		m_sLeagueAvatarSubTable.btOutCome[1] = (iPromotion / 1000)	;
0;
		m_sLeagueAvatarSubTable.btOutCome[2] = (iPromotion / 100)	;
0;
		m_sLeagueAvatarSubTable.btOutCome[3] = (iPromotion / 10)	;
0;
		m_sLeagueAvatarSubTable.btOutCome[4] = (iPromotion)			;
0;

		if( 0 == iLtotalPoint ) // 강등전 중인데 탈출못했다면
		{
			iResultType = eRMRT_Normal;
			iMyLeague = m_sLeagueAvatarMyInfo._btTier;
			iMyRating = m_sLeagueAvatarMyInfo._btRating;
			iLtotalPoint = 0;
			iLaddPoint = 0;
		}

		UpdateSubTable();
	}
	else if ( iResultType == eRMRT_GradeChange) // 뭔가 변경
	{// 1승급, 0배치고사완료, -1강등, -2승급실패

		if( m_sLeagueAvatarMyInfo._btTier != iMyLeague )
		{ // 티어 변경
			/* do noting */	
		}
		else
		{ // 랭크 변경
			if( LEAGUE_SUB_TABLE_OUTCOME_NONE == m_sLeagueAvatarSubTable.btOutCome[3] )
				iPromotion /= 10 * 10; // 3칸짜리만 가운데로 몰아줌 ( client UI )
		}

		for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
		{
			m_sLeagueAvatarSubTable.btOutCome[i] = LEAGUE_SUB_TABLE_INDEX_NONE;
		}

		m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_NONE;

		m_sLeagueAvatarMyInfo._btTier = iMyLeague;
		m_sLeagueAvatarMyInfo._btRating = iMyRating;
		m_sLeagueAvatarMyInfo._iLeaguePoint = iLtotalPoint;

		SendInsertBasic(pProxy);

		UpdateSubTable();
		SendUserRankMatchInfo();
		SetRankMatchMyInfoLoad(FALSE);
	}
	else
	{
		m_sLeagueAvatarMyInfo._iLeaguePoint += iLaddPoint;

		if( LEAGUE_MAIN_CATEGORY_DIAMOND > m_sLeagueAvatarMyInfo._btTier && 
			100 < m_sLeagueAvatarMyInfo._iLeaguePoint )
			m_sLeagueAvatarMyInfo._iLeaguePoint = 100;
		else if( 0 > m_sLeagueAvatarMyInfo._iLeaguePoint )
			m_sLeagueAvatarMyInfo._iLeaguePoint = 0;

		//SendUpdateLeaguePoint();

		if( (LEAGUE_MAIN_CATEGORY_DIAMOND > m_sLeagueAvatarMyInfo._btTier && 100 == m_sLeagueAvatarMyInfo._iLeaguePoint) ||
			0 == m_sLeagueAvatarMyInfo._iLeaguePoint )
		{
			if( RANKMATCH_RANK_RATING_5 == m_sLeagueAvatarMyInfo._btRating && 
				LEAGUE_MAIN_CATEGORY_BRONZE == m_sLeagueAvatarMyInfo._btTier && 
				0 == m_sLeagueAvatarMyInfo._iLeaguePoint)
			{
				return; //심해어
			}

			// 승강전 시작
			m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING;
			ZeroMemory(m_sLeagueAvatarSubTable.btOutCome, MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT);
			UpdateSubTable();
			SendUserRankMatchInfo();
		}
		else
		{
			if( m_sLeagueAvatarSubTable.btOutCome[0] != LEAGUE_SUB_TABLE_INDEX_NONE )
			{
				m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_NONE;
				ZeroMemory(m_sLeagueAvatarSubTable.btOutCome, MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT);
				UpdateSubTable();
			}
		}

		// 강등전 처리
		if( 0 < m_sLeagueAvatarMyInfo._iLeaguePoint && 
			100 > m_sLeagueAvatarMyInfo._iLeaguePoint &&
			m_sLeagueAvatarSubTable.iSubIndex != LEAGUE_SUB_TABLE_INDEX_NONE)
		{
			m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_NONE;
			ZeroMemory(m_sLeagueAvatarSubTable.btOutCome, MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT);

			UpdateSubTable();
		}
	}

	if( btPreTier != m_sLeagueAvatarMyInfo._btTier ||
		btPreRating != m_sLeagueAvatarMyInfo._btRating ||
		m_sLeagueAvatarSubTable.iSubIndex != LEAGUE_SUB_TABLE_INDEX_NONE)
	{
		SendUserRankMatchInfo();
	}
}

void CFSGameUserRankMatch::SendInsertBasic(CODBCSvrProxy* pProxy)
{
	SS2O_RANKMATCH_INSERT_BASIC_REQ req;

	req.iGameIDIndex = m_pUser->GetGameIDIndex();
	req.btServerIndex = _GetServerIndex;
	req.btTier = m_sLeagueAvatarMyInfo._btTier;
	req.btRating = m_sLeagueAvatarMyInfo._btRating;
	req.iGroup = m_sLeagueAvatarMyInfo._iGroup;
	req.iLeaguePoint = m_sLeagueAvatarMyInfo._iLeaguePoint;
	strncpy_s(req.szGameID, _countof(req.szGameID), m_pUser->GetGameID(), MAX_GAMEID_LENGTH);
	req.btGamePosition = m_pUser->GetCurUsedAvatarPosition();

	if( nullptr != pProxy )
	{
		CPacketComposer Packet(S2O_RANKMATCH_INSERT_BASIC_REQ);
		Packet.Add((PBYTE)&req, sizeof(SS2O_RANKMATCH_INSERT_BASIC_REQ));
		pProxy->Send(&Packet);
	}
	else
	{
		CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_InsertBasic(req))
		{
			WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_InsertBasic GameIDIndex = 10752790 , ServerIndex = 0", req.iGameIDIndex, req.btServerIndex);
	
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "SendInsertBasic GameIDIndex = 10752790,  Tier = 0" , m_pUser->GetGameIDIndex(), req.btTier);

m_pUser->SetCurrentRankMatchTier(m_sLeagueAvatarMyInfo._btTier);
}

void CFSGameUserRankMatch::SendUpdateBasic( _in SM2G_GAME_RESULT_RES* prs)
{
	CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);

	CHECK_NULL_POINTER_VOID(prs);

	SS2O_RANKMATCH_UPDATE_BASIC_NOT not;

	not.SetData(m_pUser->GetGameIDIndex(), _GetServerIndex, prs);
	not.btTier = m_sLeagueAvatarMyInfo._btTier;
	not.btRating = m_sLeagueAvatarMyInfo._btRating;
	not.iGroup = m_sLeagueAvatarMyInfo._iGroup;
	not.iLeaguePoint = m_sLeagueAvatarMyInfo._iLeaguePoint;

	if( nullptr != pProxy )
	{

		CPacketComposer Packet(S2O_RANKMATCH_UPDATE_BASIC_NOT);
		Packet.Add((BYTE*)&not, sizeof(SS2O_RANKMATCH_UPDATE_BASIC_NOT));
		pProxy->Send(&Packet);
	}
	else
	{
		CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
		CHECK_NULL_POINTER_VOID(pODBC);

		SODBCLeagueRecordBasic sODBCInfo;

		not.GetData(sODBCInfo);

		if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_UpdateBasic(
			not.iGameIDIndex, not.iServerIndex, sODBCInfo))
		{
			WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "SendUpdateBasic GameIDIndex = 10752790 , ServerIndex = 0", not.iGameIDIndex, not.iServerIndex);
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "SendUpdateBasic ODBCSvrFail GameIDIndex = 10752790 , ServerIndex = 0, Tier = 7106560", not.iGameIDIndex, not.iServerIndex, not.btTier);
oid CFSGameUserRankMatch::UpdateSubTable()
{
	if( FALSE == m_sLeagueAvatarSubTable.bDbLoad )
	{
		CHECK_CONDITION_RETURN_VOID( FALSE == LoadAvatarSubTable());
	}

	RANKMATCH.UpdateAvatarSubTable(m_pUser->GetGameIDIndex(), &m_sLeagueAvatarSubTable);
}

void CFSGameUserRankMatch::SendUpdateLeaguePoint()
{
	SS2O_RANKMATCH_UPDATE_LEAGUEPOINT_NOT not;
	not.iGameIDIndex = m_pUser->GetGameIDIndex();
	not.iServerIndex = _GetServerIndex;
	not.iLeaguePoint = m_sLeagueAvatarMyInfo._iLeaguePoint;
	not.btIsTerminate = FALSE;

	CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
	if( nullptr != pProxy )
	{
		CPacketComposer Packet(S2O_RANKMATCH_UPDATE_LEAGUEPOINT_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2O_RANKMATCH_UPDATE_LEAGUEPOINT_NOT));

		pProxy->Send(&Packet);
	}
	else
	{
		CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_UpdateLeaguePoint(
			not.iGameIDIndex, not.iServerIndex, not.iLeaguePoint, not.btIsTerminate ))
		{
			WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_UpdateLeaguePoint GameIDIndex = 10752790 , ServerIndex = 0 , Tier = 7106560", not.iGameIDIndex, not.iServerIndex, m_sLeagueAvatarMyInfo._btTier);
TE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "SendUpdateLeaguePoint GameIDIndex = 10752790 , ServerIndex = 0 , Tier = 7106560", not.iGameIDIndex, not.iServerIndex, m_sLeagueAvatarMyInfo._btTier);
oid CFSGameUserRankMatch::SetRankMatchMyInfoLoad( BOOL bLoad )
{
	m_sLeagueAvatarMyInfo._bDBLoad = bLoad;
}

BOOL CFSGameUserRankMatch::SendAvatarRankMatchMyInfo()
{
	if( m_pUser->GetCurUsedAvtarLv() < RANKMATCH.GetConfigValue(LEAGUE_CONFIG_PageRankPositionLimitLevel) )
		return TRUE;

	CHECK_CONDITION_RETURN( TRUE == m_sLeagueAvatarMyInfo._bDBLoad , TRUE );

// 	CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
// 	if( NULL == pProxy )
// 	{
	// 한국만 그냥 로딩
		LoadAvatarRankMatchMyInfo(); // 실패했다고 접속실패시키진 말자.
// 	}
// 	else
// 	{
// 		SS2O_RANKMATCH_USER_INFO_REQ req;
// 		req.iGameIDIndex = m_pUser->GetGameIDIndex();
// 		req.btServerIndex = CFSGameServer::GetInstance()->GetServerIndex();
// 
// 		CPacketComposer	PacketComposer(S2O_RANKMATCH_USER_INFO_REQ);
// 		PacketComposer.Add((PBYTE)&req,sizeof(SS2O_RANKMATCH_USER_INFO_REQ));
// 		pProxy->Send(&PacketComposer);
// 	}

	return TRUE;
}

BOOL CFSGameUserRankMatch::LoadUserNotice()
{
	if( m_pUser->GetCurUsedAvtarLv() < RANKMATCH.GetConfigValue(LEAGUE_CONFIG_PageRankPositionLimitLevel) )
		return TRUE; // 랭킹모드 참여레벨이 안되면 보내지 않습니다.

	if( TRUE == m_sUserNotice.bIsSend )
		return TRUE;

	if( FALSE == m_sLeagueAvatarMyInfo._bDBLoad )
		return TRUE;

	if( TRUE == RANKMATCH.GetLeagueModeSwitchOff())
		return TRUE;

	m_sUserNotice.iGameIDIndex = m_pUser->GetGameIDIndex();

	int iCurrentSeasonIndex = (OPEN == RANKMATCH.IsOpen() ? RANKMATCH.GetSeasonIndex() : RANKMATCH.GetLastSeasonIndex());

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	// 현재시즌 정보를 가져온다.
	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_GetUserNotice(m_sUserNotice.iGameIDIndex, m_sUserNotice.btIsNotice, m_sUserNotice.iSeasonIndex, _in iCurrentSeasonIndex))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetUserNotice gameidindex = 10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}

	m_sUserNotice.bIsSend = TRUE;
	BYTE btPreNotice = 0;
	int iPreSeasonIndex = 0;

	if( LEAGUE_USER_NOTICE_NONE == m_sUserNotice.btIsNotice && 
		TRUE == RANKMATCH.IsOpen())
	{
		SS2C_RANKMATCH_SEASON_OPEN_NOT not;
		not.iSeasonIndex = iCurrentSeasonIndex;
		time_t tDummy;
		RANKMATCH.GetSeasonDate(tDummy, not.tSeasonEndTime);

		m_pUser->Send(S2C_RANKMATCH_SEASON_OPEN_NOT, &not, sizeof(SS2C_RANKMATCH_SEASON_OPEN_NOT));

		if( 0 < iCurrentSeasonIndex )
		{
			int iTempIndex = iCurrentSeasonIndex - 1;
			pODBCBase->LEAGUE_GetUserNotice(m_sUserNotice.iGameIDIndex, btPreNotice, iPreSeasonIndex, _in iTempIndex);
		}

		pODBCBase->LEAGUE_UpdateUserNotice(m_pUser->GetGameIDIndex(), (BYTE)LEAGUE_USER_NOTICE_SEASON_OPEN, iCurrentSeasonIndex);		
	}

	if( 0 < m_sLeagueAvatarMyInfo._btPreTier || 0 < m_sLeagueAvatarMyInfo._btTier)
	{
		// 시즌이 끝났는지도 여기서 확인. 
		if( LEAGUE_USER_NOTICE_SEASON_OPEN == m_sUserNotice.btIsNotice || LEAGUE_USER_NOTICE_SEASON_OPEN == btPreNotice ) // 시즌오픈때 참여했다면
		{// 지난시즌 오픈때 참여한 유저라면
			time_t tOpen, tEnd;
			RANKMATCH.GetSeasonDate(tOpen, tEnd);
			time_t tCurrentTime = _time64(NULL);

			if( (tEnd < tCurrentTime && FALSE == RANKMATCH.IsOpen() && LEAGUE_MAIN_CATEGORY_NONE < m_sLeagueAvatarMyInfo._btTier) 
				|| // 시즌이 끝나서 종료되었거나, 다음시즌이 시작되었을경우.
				(tEnd < tCurrentTime && FALSE == RANKMATCH.IsOpen() && LEAGUE_MAIN_CATEGORY_NONE < m_sLeagueAvatarMyInfo._btPreTier)
				||
				(TRUE == RANKMATCH.IsOpen() && m_sUserNotice.iSeasonIndex < iCurrentSeasonIndex) 
				||
				LEAGUE_USER_NOTICE_SEASON_OPEN == btPreNotice) // 지난시즌 참여했으면 보상줘야함.
			{
				// 시즌이 종료중인데 아직 티어없으면 대상이 아님.
				if( FALSE == RANKMATCH.IsOpen() && LEAGUE_MAIN_CATEGORY_NONE == m_sLeagueAvatarMyInfo._btTier)
					return FALSE; 

				SS2C_RANKMATCH_SEASON_END_NOT not;
				not.iCompleteCategory = m_sLeagueAvatarMyInfo._btPreTier;

				if( OPEN == RANKMATCH.IsOpen() && 0 < m_sLeagueAvatarMyInfo._btPreTier )
				{
					not.iCompleteCategory = m_sLeagueAvatarMyInfo._btPreTier;
					SendPreTier(not.iCompleteCategory);
				}
				else if( CLOSED == RANKMATCH.IsOpen() && 0 < m_sLeagueAvatarMyInfo._btTier )
				{
					not.iCompleteCategory = (TRUE == m_sLeagueAvatarMyInfo._btIsMaster ? LEAGUE_MAIN_CATEGORY_MASTER : m_sLeagueAvatarMyInfo._btTier);
					SendPreTier(not.iCompleteCategory);
				}
				else
					return FALSE;

				not.iSeasonIndex = m_sUserNotice.iSeasonIndex;
				m_pUser->Send(S2C_RANKMATCH_SEASON_END_NOT, &not, sizeof(SS2C_RANKMATCH_SEASON_END_NOT));

				if( LEAGUE_USER_NOTICE_SEASON_OPEN != btPreNotice )
				{ // 지난시즌 보상일때는 이번시즌 종료가 아님.
					pODBCBase->LEAGUE_UpdateUserNotice(m_pUser->GetGameIDIndex(), (BYTE)LEAGUE_USER_NOTICE_SEASON_CLOSE, iCurrentSeasonIndex);
				}

				SLeagueRewardInfo sRewardInfo;
				if( TRUE == RANKMATCH.GetSeasonReward(not.iCompleteCategory, sRewardInfo, m_sUserNotice.iSeasonIndex))
				{
					m_pUser->GiveReward_Achievement(sRewardInfo.iAchievementIndex, FALSE);

					SetPreRewardTier((BYTE)not.iCompleteCategory);
					m_sLeagueAvatarMyInfo._btPreTier = not.iCompleteCategory; 

					if( CLOSED == RANKMATCH.IsOpen() && 0 < m_sUserNotice.iSeasonIndex && FALSE == RANKMATCH.GetFreeSeason(m_sUserNotice.iSeasonIndex) )
					{
						m_pUser->GetUserBasketBall()->CheckAndGiveConditionBallSkin(BASKETBALL_BALLPART_PATTERN);
					}
					else if( 0 < iCurrentSeasonIndex )
					{
						int iPreSeason = iCurrentSeasonIndex -1;
						if( FALSE == RANKMATCH.GetFreeSeason(iPreSeason) )
							m_pUser->GetUserBasketBall()->CheckAndGiveConditionBallSkin(BASKETBALL_BALLPART_PATTERN);
					}

					if( 0 < iCurrentSeasonIndex && LEAGUE_MAIN_CATEGORY_DIAMOND <= not.iCompleteCategory ) // 다이아 이상이라면 농구공을 지급해야한다고 함... 
					{
						if( TRUE == RANKMATCH.GetSeasonReward( LEAGUE_MAIN_CATEGORY_DIAMOND , sRewardInfo, 0) ) // default 
						{
							SetPreRewardTier(LEAGUE_MAIN_CATEGORY_DIAMOND);
							m_pUser->GetUserBasketBall()->CheckAndGiveConditionBallSkin(BASKETBALL_BALLPART_BACKGROUND);
						}
					}

					SetPreRewardTier(LEAGUE_MAIN_CATEGORY_NONE);
				}
				m_sUserNotice.iSeasonIndex = iCurrentSeasonIndex;

				SendUserRankMatchInfo();
			}	
		}
	}

	return TRUE;
}

void CFSGameUserRankMatch::UpdatePositionSet( int iRankMatchTeamIndex, BOOL bIsWin )
{
	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBCBase);

	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_UpdateAvatarTeamPositionSet(m_pUser->GetGameIDIndex(), iRankMatchTeamIndex, (BYTE)bIsWin))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_UpdateAvatarTeamPositionSet GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
	auto iter = m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.find(iRankMatchTeamIndex);

	if( iter != m_sLeagueAvatarTeamPosition._mapLeagueTeamPosition.end() )
	{
		if( TRUE == bIsWin )
			(iter->second).iWin += 1;
		else
			(iter->second).iLose += 1;
	}
}

void CFSGameUserRankMatch::UpdateMatchInfo( SLeagueMatchInfo& sInfo )
{
	for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
	{
		m_sLeagueAvatarSubTable.btOutCome[i] = sInfo.btOutCome[i];
	}
	m_sLeagueAvatarMyInfo._btRating = sInfo.btRating;
	m_sLeagueAvatarMyInfo._btTier = sInfo.btTier;
	m_sLeagueAvatarMyInfo._iGroup = sInfo.iGroup;
	m_sLeagueAvatarMyInfo._iLeaguePoint = sInfo.iLeaguePoint;

	m_sLeagueAvatarSubTable.iSubIndex = sInfo.iSubIndex;
	m_sLeagueAvatarSubTable.btRemainPlacementTest = sInfo.btRemainPlacementTest;
	m_sLeagueAvatarSubTable.fTestRating = sInfo.fTestRatingPoint;
}

void CFSGameUserRankMatch::UpdateLeaguePoint( int iLeaguePoint )
{
	m_sLeagueAvatarMyInfo._iLeaguePoint = iLeaguePoint;

	if( 0 >= m_sLeagueAvatarMyInfo._iLeaguePoint )
	{
		m_sLeagueAvatarMyInfo._iLeaguePoint = 0;

		// 강등전으로 변경
		if( LEAGUE_MAIN_CATEGORY_BRONZE == m_sLeagueAvatarMyInfo._btTier &&
			RANKMATCH_RANK_RATING_5 == m_sLeagueAvatarMyInfo._btRating )
		{		}
		else
		{
			m_sLeagueAvatarSubTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING;
			for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
				m_sLeagueAvatarSubTable.btOutCome[i] = LEAGUE_SUB_TABLE_OUTCOME_NONE;

			m_sLeagueAvatarSubTable.btRemainPlacementTest = 0;
			m_sLeagueAvatarSubTable.fTestRating = 0.f;

			UpdateSubTable();
		}
	}

	SendUpdateLeaguePoint();
}

void CFSGameUserRankMatch::SendUserRankMatchInfo()
{
	if( TRUE == m_sLeagueAvatarMyInfo._bDBLoad )
	{
		SS2C_RANKMATCH_USER_INFO_NOT not;

		if( LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST == m_sLeagueAvatarSubTable.iSubIndex ||
			CLOSED == RANKMATCH.IsOpen()) // 시즌이 종료되어있다면.
			not.btLeagueCategory = LEAGUE_MAIN_CATEGORY_NONE;
		else
			not.btLeagueCategory = m_sLeagueAvatarMyInfo._btTier;

		not.btLeagueRating = m_sLeagueAvatarMyInfo._btRating;
		not.btPreCategory = m_sLeagueAvatarMyInfo._btPreTier;

		not.btPromotionCount = 0;
		not.iPromotioninfo = 0;

		if( LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING == m_sLeagueAvatarSubTable.iSubIndex )
		{
			if( 100 == m_sLeagueAvatarMyInfo._iLeaguePoint &&
				LEAGUE_MAIN_CATEGORY_DIAMOND > m_sLeagueAvatarMyInfo._btTier) 
			{
				if( RANKMATCH_RANK_RATING_1 == m_sLeagueAvatarMyInfo._btRating )
				{
					// 티어 승급
					not.btPromotionCount = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Advancement_Tier_MatchCount);
				}
				else
				{
					not.btPromotionCount = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Advancement_Rating_MatchCount);
				}
			}
		}
		else if(LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST == m_sLeagueAvatarSubTable.iSubIndex)
		{
			not.btPromotionCount = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_PlacementTest_Count);
		}

		if( 0 < not.btPromotionCount )
		{
			GetPromotionInfo(not.iPromotioninfo,not.btPromotionCount);
		}

		m_pUser->Send(S2C_RANKMATCH_USER_INFO_NOT, &not, sizeof(SS2C_RANKMATCH_USER_INFO_NOT));

		m_pUser->SetCurrentRankMatchTier(not.btLeagueCategory);
	}
}

void CFSGameUserRankMatch::SendRankMatchOpenStatus()
{
	SS2C_RANKMATCH_STATUS_NOT	not;
	not.btOpenStatus = static_cast<BYTE>(RANKMATCH.IsOpen());
	RANKMATCH.GetSeasonDate(not.tSeasonStartTime, not.tSeasonEndTime);
	not.iSeasonIndex = RANKMATCH.GetSeasonIndex();

	CPacketComposer Packet(S2C_RANKMATCH_STATUS_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_RANKMATCH_STATUS_NOT));

	m_pUser->Send(&Packet);

}

BOOL CFSGameUserRankMatch::CheckPing()
{
	CHECK_CONDITION_RETURN(FALSE == CheckAndSendTerminate(), FALSE);
	DWORD dwTick = GetCurrentTime();
	int iCheckTick = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Checkping_s) * 1000;

	CLeagueSvrProxy* pLeague = dynamic_cast<CLeagueSvrProxy*>(GAMEPROXY.GetProxy(FS_LEAGUE_SERVER_PROXY));

	if( nullptr == pLeague )
	{
		SendAutoTeamRegFail(RESULT_AUTO_TEAM_REG_FAILED);
		return FALSE;
	}

	SG2L_BASE	base;
	base.iGameIDIndex = m_pUser->GetGameIDIndex();
	base.btServerIndex = _GetServerIndex;

	CPacketComposer Packet(G2L_HOST_SERVER_INFO_REQ);
	Packet.Add((PBYTE)&base, sizeof(SG2L_BASE));
	pLeague->Send(&Packet);

	return FALSE;
}

void CFSGameUserRankMatch::SendAutoTeamRegFail( BYTE btResult )
{
	SS2C_AUTO_TEAM_REG_RES	res;
	res.btRegMode = (BYTE)m_AutoTeamRegInfo.btRegMode;
	res.btResult = btResult;
	res.iNeedClubContributionPoint = 0;

	if ( RESULT_AUTO_TEAM_REG_PENALTY_USER == res.btResult )
		res.tPenaltyRemainTime = m_pUser->GetUserAppraisal()->GetPenaltyRemainTime();
	else if ( RESULT_AUTO_TEAM_REG_LIMIT_MATCHING_USER == res.btResult )
		res.tPenaltyRemainTime = m_tMatchingLimitRemainTime;

	m_pUser->Send(S2C_AUTO_TEAM_REG_RES, (PBYTE)&res, sizeof(SS2C_AUTO_TEAM_REG_RES));
}


void CFSGameUserRankMatch::SendAutoTeamReg(CLeagueSvrProxy* pLeagueProxy/*=nullptr*/)
{
	if( nullptr == pLeagueProxy )
	{
		pLeagueProxy = dynamic_cast<CLeagueSvrProxy*>(GAMEPROXY.GetProxy(FS_LEAGUE_SERVER_PROXY));
		CHECK_NULL_POINTER_VOID(pLeagueProxy);
	}

	if( FALSE == m_sLeagueAvatarMyInfo._bDBLoad )
	{
		if( FALSE == LoadAvatarRankMatchMyInfo())
		{
			SendAutoTeamRegFail(RESULT_AUTO_TEAM_REG_FAILED);
			return;
		}
	}

	list<BYTE>	listRankMatchTeamIndex;

	SG2L_AUTO_TEAM_REG_REQ	Leagueinfo;
	Leagueinfo.regInfo = m_AutoTeamRegInfo;
	
	SUserPingInfo* pUserPingInfo = m_pUser->GetUserPingInfo();
	CHECK_NULL_POINTER_VOID(pUserPingInfo);

	if( 0 == m_fLeagueRatingPoint )
	{
		Leagueinfo.fRatingPoint = m_fLeagueRatingPoint = LoadLeagueRatingPoint();

		if( 0.f == Leagueinfo.fRatingPoint )
		{
			SendAutoTeamRegFail(RESULT_AUTO_TEAM_REG_FAILED);
			return;
		}
	}
	else 
	{
		Leagueinfo.fRatingPoint = m_fLeagueRatingPoint;
	}

	Leagueinfo.iRemainUserExamCourt = 0;

	Leagueinfo.iGameIDIndex = Leagueinfo.AvatarInfo.iGameIDIndex = m_pUser->GetGameIDIndex();
	Leagueinfo.btServerIndex = _GetServerIndex;
	m_pUser->GetMatchLoginUserInfo(Leagueinfo, MATCH_TYPE_RANKMATCH);
	GetAvatarAvoidTeamIndex(listRankMatchTeamIndex);
	Leagueinfo.btRankMatchAvoidTeamCount = (BYTE)listRankMatchTeamIndex.size();

	GetLeagueMatchInfo(Leagueinfo.leagueMatchInfo); 

	if( 0 != pUserPingInfo->iRecvedPingCnt)
		Leagueinfo.dwResponseTime = pUserPingInfo->iSumPing / pUserPingInfo->iRecvedPingCnt;
	else
		Leagueinfo.dwResponseTime = 300;

	Leagueinfo.iUpbandwidthBPS = pUserPingInfo->iUpbandwidthBPS;

	CPacketComposer	Packet2(G2L_AUTO_TEAM_REG_REQ);

	Packet2.Add((PBYTE)&Leagueinfo, sizeof(SG2L_AUTO_TEAM_REG_REQ));

	SAUTO_TEAM_REG_RANKMATCH_AVOID_TEAM	AVoidTeam;
	for( list<BYTE>::iterator iter = listRankMatchTeamIndex.begin() ; iter != listRankMatchTeamIndex.end() ; ++iter )
	{
		AVoidTeam.btTeamIndex = (*iter);
		Packet2.Add((PBYTE)&AVoidTeam.btTeamIndex, sizeof(SAUTO_TEAM_REG_RANKMATCH_AVOID_TEAM));
	}

	pLeagueProxy->Send(&Packet2);
}

void CFSGameUserRankMatch::SendPingLog( SUserPingInfo* pInfo )
{
	CHECK_NULL_POINTER_VOID(pInfo);

	CLogSvrProxy* pLogProxy = (CLogSvrProxy*) GAMEPROXY.GetProxy(FS_LOG_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pLogProxy);

	CPacketComposer	Packet(G2L_PING_LOG_NOT);
	SG2L_PING_LOG_NOT	not;

	not.iGameIDIndex = m_pUser->GetGameIDIndex();
	not.btServerIndex = (BYTE)0;

	if( 0 != pInfo->iSumPing && 0 != pInfo->iRecvedPingCnt)
		not.dwResponseTime = pInfo->iSumPing / pInfo->iRecvedPingCnt;
	else
		not.dwResponseTime = 0;

	not.dwThreadAliveTime = pInfo->dwThreadAliveTime;
	not.iFPS = pInfo->iFPS;
	not.iUpbandwidthBPS = pInfo->iUpbandwidthBPS;

	Packet.Add((PBYTE)&not, sizeof(SG2L_PING_LOG_NOT));

	pLogProxy->Send(&Packet);
}


void CFSGameUserRankMatch::CheckAndSendNotice()
{
	if( m_pUser->GetCurUsedAvtarLv() < RANKMATCH.GetConfigValue(LEAGUE_CONFIG_PageRankPositionLimitLevel) )
		return;

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	int iDownLeaguePoint =0;
	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_GetAvatarTerminate(m_pUser->GetGameIDIndex(), iDownLeaguePoint, 
		m_sTerminateInfo.iTerminateContinue, m_sTerminateInfo.tLastTerminateDate))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetAvatarTerminate GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
rn;
	}

	CPacketComposer Packet(S2C_NOTICE); 

	int iTercondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Terminate_penalty_Continue_Condition);
	if( iTercondition <= m_sTerminateInfo.iTerminateContinue )
	{
		int iDiffSecond = difftime(m_sTerminateInfo.tLastTerminateDate , _GetCurrentDBDate );
		int iMin = (iDiffSecond ) / 60;
 60;

		int imincondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Terminate_penalty_min);

		if( iMin < imincondition )
		{
			int iCode, iType;
			iCode = 32; //leaguemode
			iType = 0;

			Packet.Initialize(S2C_NOTICE);
			Packet.Add(iCode);
			Packet.Add(iType);
			Packet.Add(iMin);

			m_pUser->Send(&Packet);
		}
	}

	if( 0 < iDownLeaguePoint )
	{
		int iCode, iType;
		iCode = 31; //leaguemode
		iType = 0;

		Packet.Initialize(S2C_NOTICE);
		Packet.Add(iCode);
		Packet.Add(iType);
		Packet.Add(iDownLeaguePoint);

		m_pUser->Send(&Packet);
	}
}

float CFSGameUserRankMatch::LoadLeagueRatingPoint()
{
	CFSODBCBase* pGameODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN( nullptr == pGameODBC, 0.f);

	if( ODBC_RETURN_SUCCESS != pGameODBC->LEAGUE_GetAvatarRatingInfo(m_pUser->GetGameIDIndex(), m_fLeagueRatingPoint) )
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCH, DB_DATA_LOAD, FAIL, "LEAGUE_GetAvatarRatingInfo - GameIDIndex:10752790", m_pUser->GetGameIDIndex());
rn 0.f;
	}

	if( 0 == m_fLeagueRatingPoint ) // 없으니 연산해서 새로 넣어주자........
	{
		float fRatingPoint = 0.f;
		int iRemainUserExamCourt = 0;
		if(ODBC_RETURN_SUCCESS != pGameODBC->MATCHMAKING_GetRatingInfo(
			m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), RATING_CODE_NORMAL_3ON3, fRatingPoint, iRemainUserExamCourt))
		{
			WRITE_LOG_NEW(LOG_TYPE_MATCH, DB_DATA_LOAD, FAIL, "LeagueMode MATCHMAKING_GetRatingInfo - GameIDIndex:10752790", m_pUser->GetGameIDIndex());
RatingPoint = RATING_DEFAULT;
			iRemainUserExamCourt = DEFAULT_RATING_EXAM_GAME_COUNT;
		}

		m_pUser->SetRatingPoint(fRatingPoint);

		SLeagueRatingConfig RatingConfig;
		// 첫겜이라면 
		CHECK_CONDITION_RETURN(FALSE == RANKMATCH.GetLeagueRatingInfo(m_pUser->GetRatingPoint(), RatingConfig), 0.f);

		// 시작 레이팅을 가져오고 셋팅해줍니다.
		m_fLeagueRatingPoint = RatingConfig.fStartPoint;
		RANKMATCH.UpdateAvatarRatingInfo(m_pUser->GetGameIDIndex(), m_fLeagueRatingPoint);
	}

	return m_fLeagueRatingPoint; 
}

void CFSGameUserRankMatch::SetLeagueRatingPoint( float fRatingPoint )
{
	m_fLeagueRatingPoint = fRatingPoint;
}

void CFSGameUserRankMatch::SetAutoTeamRegInfo( SG2M_AUTO_TEAM_REG_REQ& req )
{
	memcpy_s(&m_AutoTeamRegInfo, sizeof(m_AutoTeamRegInfo), &req.regInfo, sizeof(m_AutoTeamRegInfo));
}

void CFSGameUserRankMatch::SetPreRewardTier( BYTE btTier )
{
	m_btBasketBallSkinRewardTier = btTier;
}

void CFSGameUserRankMatch::SendPreTier(BYTE btPreTier)
{
	CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
	SS2O_RANKMATCH_UPDATE_PRETIER_NOT not;

	not.btPreTier = btPreTier;
	not.iGameIDIndex = m_pUser->GetGameIDIndex();
	not.btServerIndex = _GetServerIndex;

	if( nullptr != pProxy )
	{

		CPacketComposer Packet(S2O_RANKMATCH_UPDATE_PRETIER_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2O_RANKMATCH_UPDATE_PRETIER_NOT));
		pProxy->Send(&Packet);
	}
	else
	{
		CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_UpdatePreTier(not.iGameIDIndex, not.btServerIndex, not.btPreTier))
		{
			WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_UpdatePreTier GameIDIndex = 10752790 , ServerIndex = 0", not.iGameIDIndex, not.btServerIndex);

		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "SendPreTier GameIDIndex = 10752790 , ServerIndex = 0 , PreTier = 7106560", not.iGameIDIndex, not.btServerIndex, not.btPreTier);
OOL CFSGameUserRankMatch::CheckAndSendPingCheck()
{
	//	CHECK_CONDITION_RETURN(FALSE == CheckRankMatchTeamReg(), FALSE);
	CHECK_CONDITION_RETURN(FALSE == CheckPing(), FALSE);

	return TRUE;
}

BOOL CFSGameUserRankMatch::CheckAndSendTerminate()
{
	BOOL bErrorCheck = FALSE;
	int iTercondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Terminate_penalty_Continue_Condition);

	SS2C_USER_PING_CHECK_REQ	info;
	ZeroMemory(&info, sizeof(SS2C_USER_PING_CHECK_REQ));

	CLeagueSvrProxy* pLeague = (CLeagueSvrProxy*)GAMEPROXY.GetProxy(FS_LEAGUE_SERVER_PROXY);

	if( iTercondition <= m_sTerminateInfo.iTerminateContinue )
	{
		int iDiffSecond = difftime(m_sTerminateInfo.tLastTerminateDate , _GetCurrentDBDate );
		int iMin = (iDiffSecond ) / 60;
 60;

		int imincondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Terminate_penalty_min);

		if( iMin < imincondition )
		{// 아직..
			bErrorCheck = TRUE;

			info.btResult = RESULT_PING_CHECK_PENALTY_TERMINATED;
			info.iRemainMin = iMin;
		}
	}
	else if( nullptr == pLeague )
	{// 서버떨어져있으면
		bErrorCheck = TRUE;

		info.btResult = RETURN_PING_CHECK_SERVER_CLOSE;
	}

	if( TRUE == bErrorCheck )
	{
		m_pUser->Send(S2C_USER_PING_CHECK_REQ, &info, sizeof(SS2C_USER_PING_CHECK_REQ));

		return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUserRankMatch::CheckUserPingVal( BYTE btServerIndex )
{
	SUserPingInfo* pInfo = m_pUser->GetUserPingInfo();
	if( nullptr == pInfo )
	{
		SendAutoTeamRegFail(RESULT_AUTO_TEAM_REG_BAD_PING);
		return FALSE;
	}

	if( 0 < pInfo->iRecvedPingCnt )
	{
		int iLimitPing = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_ChecklimitPing);

		if( 0 < iLimitPing )
		{
			if( 0 < pInfo->iRecvedPingCnt && 
				iLimitPing >= pInfo->iSumPing / pInfo->iRecvedPingCnt )
			{
				return TRUE;
			}
		}
		else
		{
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CFSGameUserRankMatch::CheckRankMatchPageView( void )
{
	DWORD dwTick = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_RankPageView_fallbacktime) * 1000;
	DWORD dwCurrentTick = GetCurrentTime();

	if( dwTick > 1000 )
	{
		dwTick -= 1000; 
	}

	if( dwCurrentTick > dwTick + m_dwLastRankPageView )
	{
		m_dwLastRankPageView = dwCurrentTick;

		return TRUE;
	}

	return FALSE;
}

BOOL CFSGameUserRankMatch::CheckUpdateGameIDInRank()
{
	if( m_pUser->GetCurUsedAvtarLv() < RANKMATCH.GetConfigValue(LEAGUE_CONFIG_RequiredLevel) )
		return FALSE;

	return TRUE;
}

void CFSGameUserRankMatch::SendUpdateGameIDInRankPage( LPCTSTR szGameID )
{
	CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);

	SS2O_RANKMATCH_UPDATE_GAMEID_NOT	not;
	not.btServerIndex = _GetServerIndex;
	not.iGameIDIndex = m_pUser->GetGameIDIndex();
	strncpy_s(not.szNewGameID, _countof(not.szNewGameID), szGameID, _countof(not.szNewGameID)-1);

	if( nullptr != pODBCSvr )
	{
		CPacketComposer	Packet(S2O_RANKMATCH_UPDATE_GAMEID_NOT);
		Packet.Add((PBYTE)&not,sizeof(SS2O_RANKMATCH_UPDATE_GAMEID_NOT));

		pODBCSvr->Send(&Packet);
	}
	else
	{
		CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
		CHECK_NULL_POINTER_VOID(pODBC);


		if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_UpdateGameID(not))
		{
			WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_UPDATE, FAIL, "LEAGUE_Record_UpdatePreTier GameIDIndex = 10752790 , ServerIndex = 0", not.iGameIDIndex, not.btServerIndex);
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_UPDATE, FAIL, "SendUpdateGameIDInRankPage GameIDIndex = 10752790 , ServerIndex = 0", not.iGameIDIndex, not.btServerIndex);


void CFSGameUserRankMatch::SendUpdateGameIDInRankPage( SS2O_RANKMATCH_UPDATE_GAMEID_NOT& info )
{
	CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
	if( nullptr != pODBCSvr )
	{
		CPacketComposer	Packet(S2O_RANKMATCH_UPDATE_GAMEID_NOT);
		Packet.Add((PBYTE)&info,sizeof(SS2O_RANKMATCH_UPDATE_GAMEID_NOT));

		pODBCSvr->Send(&Packet);
	}
	else
	{
		CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_UpdateGameID(info))
		{
			WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_UPDATE, FAIL, "LEAGUE_Record_UpdatePreTier GameIDIndex = 10752790 , ServerIndex = 0", info.iGameIDIndex, info.btServerIndex);
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_UPDATE, FAIL, "SendUpdateGameIDInRankPage GameIDIndex = 10752790 , ServerIndex = 0", info.iGameIDIndex, info.btServerIndex);



void CFSGameUserRankMatch::SendRankMatch_MatchOpenTime()
{
	SS2C_RANKMATCH_MATCHOPEN_NOT not;
	not.btOpen = (BYTE)RANKMATCH.GetMatchOpenTime();

	CPacketComposer Packet(S2C_RANKMATCH_MATCHOPEN_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_RANKMATCH_MATCHOPEN_NOT));

	m_pUser->Send(&Packet);
}

void CFSGameUserRankMatch::GetPromotionInfo( int& iPromotion , int iPromotionCount)
{
	int iMultiple = 10000;

	for( int i = 0 ; i < iPromotionCount && i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
	{
		int iOutcome = 0;
		if( LEAGUE_SUB_TABLE_OUTCOME_NONE == m_sLeagueAvatarSubTable.btOutCome[i] || 
			LEAGUE_SUB_TABLE_OUTCOME_NOTYET == m_sLeagueAvatarSubTable.btOutCome[i] )
		{
			iOutcome = LEAGUE_SUB_TABLE_OUTCOME_NOTYET;
		}
		else
		{
			iOutcome = m_sLeagueAvatarSubTable.btOutCome[i];
		}

		if( 0 < iMultiple )
			iPromotion += iOutcome * iMultiple;
		else 
			iPromotion += iOutcome;

		iMultiple /=  10;
	}

	if( 3 == iPromotionCount ) // rating up 
	{
		iPromotion /= 10;
	}
}

BOOL CFSGameUserRankMatch::LoadAvatarLimitMatchingInfo()
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBCBase, FALSE);

	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_GetAvatarLimitMatchingInfo(m_pUser->GetGameIDIndex(), m_iMatchingRefusalCount, m_tMatchingLimitRemainTime))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_GetAvatarLimitMatchingInfo GameIDIndex = 10752790", m_pUser->GetGameIDIndex());
rn FALSE;
	}

	if(m_tMatchingLimitRemainTime > 0)
		CFSGameServer::GetInstance()->AddRankMatchLimitUser(m_pUser);

	return TRUE;
}

BOOL CFSGameUserRankMatch::CheckLimitMatchingUser()
{
	CHECK_CONDITION_RETURN(m_tMatchingLimitRemainTime == 0, FALSE);

	return TRUE;
}

void CFSGameUserRankMatch::UpdateLimitMatchingInfo()
{
	if(m_iMatchingRefusalCount < MAX_MATCHING_REFUSAL_COUNT)
	{
		m_iMatchingRefusalCount += 1;
	}

	if(m_iMatchingRefusalCount >= MAX_MATCHING_REFUSAL_COUNT)
	{
		m_tMatchingLimitRemainTime = MATCHING_LIMIT_TIME;

		CFSGameServer::GetInstance()->AddRankMatchLimitUser(m_pUser);
	}
}

void CFSGameUserRankMatch::ClearLimitMatchingInfo()
{
	CHECK_CONDITION_RETURN_VOID(m_iMatchingRefusalCount == 0);

	if(m_tMatchingLimitRemainTime > 0)
		CFSGameServer::GetInstance()->RemoveRankMatchLimitUser(m_pUser->GetGameIDIndex());

	m_iMatchingRefusalCount = 0;
	m_tMatchingLimitRemainTime = 0;
}

int CFSGameUserRankMatch::DecreaseLimitMatchingTime()
{
	CHECK_CONDITION_RETURN(m_tMatchingLimitRemainTime <= 0, -1);

	int iGameIdIndex = -1;
	m_tMatchingLimitRemainTime--;
	if(m_tMatchingLimitRemainTime == 0)
		iGameIdIndex = m_pUser->GetGameIDIndex();

	return iGameIdIndex;
}

void CFSGameUserRankMatch::SaveLimitMatchingInfo()
{
	CFSGameServer::GetInstance()->RemoveRankMatchLimitUser(m_pUser->GetGameIDIndex());

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_UpdateAvatarLimitMatchingInfo(m_pUser->GetGameIDIndex(), m_iMatchingRefusalCount, m_tMatchingLimitRemainTime))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_UpdateAvatarLimitMatchingInfo GameIDIndex = 10752790", m_pUser->GetGameIDIndex());

