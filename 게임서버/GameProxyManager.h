qpragma once
qinclude "ProxyManager.h"
qinclude "Singleton.h"

class CSocketServer;
class CFSSvrList;
class CFSGameUser;

class CGameProxyManager : public CProxyManager, public Singleton<CGameProxyManager>
{
public:	
	CGameProxyManager();
	virtual ~CGameProxyManager();

	BOOL Initialize(CSocketServer* server, CFSSvrList* pSvrList);
	virtual void Disconnect(CSvrProxy* pProxy);

	void SendToAllMatchProxy(int iCommand, PVOID info, int iSize);
	void SendMatchServerStatus(CFSGameUser* pUser);

protected:

};

#define GAMEPROXY CGameProxyManager::GetInstance()