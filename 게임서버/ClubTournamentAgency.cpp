qinclude "stdafx.h"
qinclude "ClubTournamentAgency.h"
qinclude "CFSGameUser.h"
qinclude "CFSLobby.h"
qinclude "CFSGameServer.h"

CClubTournamentAgency::CClubTournamentAgency(void)
{
	m_btCurrentStatus = CLUBTOURNAMENT_STATUS_NONE;
	m_iCurrentMonth = JANUARY;
}

CClubTournamentAgency::~CClubTournamentAgency(void)
{
}

void CClubTournamentAgency::Update(SCL2G_UPDATE_CLUBTOURNAMENT_AGENCY_NOT& info)
{
	CPacketComposer Packet(S2C_CLUBTOURNAMENT_STATUS_NOT);
	SS2C_CLUBTOURNAMENT_STATUS_NOT rs;
	rs.btStatus = info.btCurrentStatus;
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CLUBTOURNAMENT_STATUS_NOT));
	FSLOBBY.BroadCastToClubTournament(&Packet);

	CHECK_CONDITION_RETURN_VOID(info.iCurrentMonth >= MAX_MONTH_COUNT);

	if(m_Schedule[info.iCurrentMonth].iYear != info.scheduleInfo.iYear ||
		m_Schedule[info.iCurrentMonth].iMonth != info.scheduleInfo.iMonth)
	{
		Packet.Initialize(S2C_CLUBTOURNAMENT_SCHEDULE_NOT);
		SS2C_CLUBTOURNAMENT_SCHEDULE_NOT scheduleinfo;
		GetClubTournamentScheduleInfo(scheduleinfo.scheduleInfo);
		Packet.Add((PBYTE)&scheduleinfo, sizeof(SS2C_CLUBTOURNAMENT_SCHEDULE_NOT));
		FSLOBBY.BroadCastToClubTournament(&Packet);
	}

	if(m_btCurrentStatus != info.btCurrentStatus &&
		(info.btCurrentStatus <= CLUBTOURNAMENT_STATUS_READY_16ROUND ||
		info.btCurrentStatus >= CLUBTOURNAMENT_STATUS_PLAYING_2ROUND))
	{
		CPacketComposer Packet(S2C_CLUBTOURNAMENT_INFO_NOT);
		SS2C_CLUBTOURNAMENT_INFO_NOT nofity;
		MakeClubTournamentStatus(info.btCurrentStatus, nofity);
		Packet.Add((PBYTE)&nofity, sizeof(SS2C_CLUBTOURNAMENT_INFO_NOT));
		CFSGameServer::GetInstance()->BroadCast(Packet);
	}

	m_btCurrentStatus = info.btCurrentStatus;
	m_iCurrentMonth = info.iCurrentMonth;

	SetClubTournamentScheduleInfo(info.scheduleInfo);
}

void CClubTournamentAgency::MakeClubTournamentStatus(int iCurrentStatus, SS2C_CLUBTOURNAMENT_INFO_NOT& info)
{
	info.btButtonStatus = CLUBTOURNAMENT_BUTTON_STATUS_INACTIVATION;
	GetClubTournamentScheduleInfo(info.scheduleInfo);

	if(iCurrentStatus <= CLUBTOURNAMENT_STATUS_READY)
		info.btButtonStatus = CLUBTOURNAMENT_BUTTON_STATUS_INACTIVATION;
	else if(iCurrentStatus >= CLUBTOURNAMENT_STATUS_END_2ROUND)
	{
		SYSTEMTIME systemTime;
		::GetLocalTime(&systemTime);

		if(systemTime.wYear == info.scheduleInfo.iYear &&
			systemTime.wMonth == info.scheduleInfo.iMonth &&
			systemTime.wDay >= info.scheduleInfo.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iDay &&
			systemTime.wDay <= info.scheduleInfo.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iDay)
			info.btButtonStatus = CLUBTOURNAMENT_BUTTON_STATUS_ACTIVATION;
	}
	else
		info.btButtonStatus = CLUBTOURNAMENT_BUTTON_STATUS_ACTIVATION;

	info.btIsClubTournamnetEnter = 0;
	if(iCurrentStatus != CLUBTOURNAMENT_STATUS_NONE)
		info.btIsClubTournamnetEnter = 1;
}

void CClubTournamentAgency::SendClubTournamentStatus(CUser* pUser)
{
	CHECK_CONDITION_RETURN_VOID(pUser == NULL);

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_INFO_NOT);
	SS2C_CLUBTOURNAMENT_INFO_NOT info;
	MakeClubTournamentStatus(m_btCurrentStatus, info);
	Packet.Add((PBYTE)&info, sizeof(SS2C_CLUBTOURNAMENT_INFO_NOT));
	pUser->Send(&Packet);
}

void CClubTournamentAgency::SetClubTournamentScheduleInfo(CLUBTOURNAMENT_SCHEDULEINFO& info)
{
	CHECK_CONDITION_RETURN_VOID(m_iCurrentMonth < JANUARY || m_iCurrentMonth >= MAX_MONTH_COUNT);

	m_Schedule[m_iCurrentMonth].iYear = info.iYear;
	m_Schedule[m_iCurrentMonth].iMonth = info.iMonth;
	m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iDay = info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iDay;
	m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iFirstRoundHour = info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iFirstRoundHour;
	m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iSecondRoundHour = info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iSecondRoundHour;
	m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iDay = info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iDay;
	m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iFirstRoundHour = info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iFirstRoundHour;
	m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iSecondRoundHour = info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iSecondRoundHour;
}

void CClubTournamentAgency::GetClubTournamentScheduleInfo(CLUBTOURNAMENT_SCHEDULEINFO& info)
{
	CHECK_CONDITION_RETURN_VOID(m_iCurrentMonth < JANUARY || m_iCurrentMonth >= MAX_MONTH_COUNT);

	info.iYear	= m_Schedule[m_iCurrentMonth].iYear;
	info.iMonth = m_Schedule[m_iCurrentMonth].iMonth;
	info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iDay = m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iDay;
	info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iFirstRoundHour = m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iFirstRoundHour;
	info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iSecondRoundHour = m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_1DAY].iSecondRoundHour;
	info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iDay = m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iDay;
	info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iFirstRoundHour = m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iFirstRoundHour;
	info.scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iSecondRoundHour = m_Schedule[m_iCurrentMonth].scheduleTimeInfo[CLUBTOURNAMENT_MATCH_DAY_TYPE_2DAY].iSecondRoundHour;
}

