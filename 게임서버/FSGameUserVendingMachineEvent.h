qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserVendingMachineEvent
{
public:
	CFSGameUserVendingMachineEvent(CFSGameUser* pUser);
	~CFSGameUserVendingMachineEvent(void);

	BOOL				Load();
	BOOL				CheckAndGiveMoney(time_t tCurrentTime = _time64(NULL));
	void				SendEventInfo();
	void				SendUserInfo();
	BOOL				InputMoney();
	BOOL				UseMoney(BYTE btProductIndex);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;
	int					m_iGiveMoney;
	int					m_iInputMoney;
	int					m_iMyMoney;
	time_t				m_tRecentResetTime;
};

