qpragma once
qinclude "Lock.h"
qinclude "CIntensivePracticeRank.h"

class CFSGameUser;
class CFSODBCBase;

typedef map<int/*iPracticeType*/, SIntensivePracticeUserBestRecord> INTENSIVEPRACTICE_USER_BEST_RECORD_MAP;

#define RANKING_MODE_ADD_TIME_VALUE_STEP_2SHOOT		5
#define RANKING_MODE_ADD_TIME_VALUE_NICEPASS_DUNK	7
#define RANKING_MODE_ADD_TIME_VALUE_NICEPASS_LAYUP	7

class CFSGameUserIntensivePractice : public CIntensivePracticeRank
{
public:
	CFSGameUserIntensivePractice(CFSGameUser* pUser);
	~CFSGameUserIntensivePractice(void);
	
	BOOL Load();
	BOOL CheckNoticeLog();

	int GetPracticeType()	{ return m_UserCurrentRecord.iPracticeType; }
	int GetRoomMode() { return m_UserCurrentRecord.iRoomMode; }
	int GetScore() { return m_UserCurrentRecord.iScore; }
	int GetUpdateExp();
	int GetUpdatePoint();
	int GetBestPoint();
	int GetBestPoint(int iPracticeType);

	void SetGameStartInfo(SS2C_START_INTENSIVEPRACTICE_RES rs);
	void SetRoomMode(int iRoomMode, int& iAddGameTime);
	void SetLevel(int iLevel);

	void InitializeBestPoint();
	void InitializeGame();
	void AddScore(int* iScoreType, int& iAddGameTime);
	void AddTryCount();

	void EndGame();
	void CheckGameTime();
	void ResetOneDayRecord();
	void UpdateReadyGiveReward(int iLoseCount, int iScore);
	void CheckOneDayPopUpNotice();
	void GiveAchievement();
	void CheckUpdateRank();

	void MakeGameStartInfo(CPacketComposer& Packet, SS2C_START_INTENSIVEPRACTICE_RES& rs);

private:
	CFSGameUser* m_pUser;
	BOOL m_bDataLoaded;

	INTENSIVEPRACTICE_USER_BEST_RECORD_MAP	m_mapUserBestRecord;
	SIntensivePracticeUserCurrentRecord		m_UserCurrentRecord;
	SIntensivePracticeUserOnedayRecord		m_UserOneDayRecord;
	BOOL m_bOneDayPopUpNotice;
	BOOL m_bReadyGiveReward;	// 5연패 or 20점차 이상으로 패배했을 경우
	BOOL m_bGiveReward_Completion; // 보상을 지급 하였는지
};

