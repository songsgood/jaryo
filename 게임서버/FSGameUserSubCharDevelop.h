qpragma once

class CFSGameUser;
class CFSGameUserSubCharDevelop
{
private:
	CFSGameUser* m_pUser;
	MAP_SUB_CHAR_DEVELOP_EXP_USER_INFO m_mapExpUserInfo;

private:
	int ConvertPositionCode(BYTE btSubCharPositionType);
	BYTE ConvertPositionType(int iPositionCode);

	BOOL GetMaxExpCharacter(MY_AVATAR_LIST* pAvatarList, int iPositionCode, SMyAvatar* pMaxAvatar);

public:
	BOOL Load(void);
	void SendExpUserInfo(void);
	void SendAvatarInfo(BYTE btPositionType);

	int AddExp(int iLevel, int iPositionCode, int iAddExp, short shEstEFF);
	BYTE AcceptExpReq(int iGameIDIndex, int iAcceptExp, SS2C_SUB_CHAR_DEVELOP_EXP_ACCEPT_RES& rs);

public:
	CFSGameUserSubCharDevelop(CFSGameUser* pUser);
	~CFSGameUserSubCharDevelop(void);
};
