qinclude "stdafx.h"
qinclude "GameProxyManager.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "ChatSvrProxy.h"
qinclude "GMSvrProxy.h"
qinclude "LogSvrProxy.h"
qinclude "CFSSvrList.h"
qinclude "CFSGameServer.h"
qinclude "LobbyChatUserManager.h"
qinclude "LeagueSvrProxy.h"
qinclude "ODBCSvrProxy.h"

CGameProxyManager::CGameProxyManager()
{
}

CGameProxyManager::~CGameProxyManager()
{
}

BOOL CGameProxyManager::Initialize(CSocketServer* server, CFSSvrList* pSvrList)
{
	CProxyManager::Initialize(server);

	//Center
	SCommonProcessInfo*	pInfo = pSvrList->GetProcessInfo(SERVER_TYPE_CENTER);
	if (NULL != pInfo)
	{
		CSvrProxy* pProxy = new CCenterSvrProxy();
		pProxy->Initialize(pInfo->iProcessID, pInfo->szIP, pInfo->iPort);
		AddProxy(pProxy);
	}

	//Club
	pInfo = pSvrList->GetProcessInfo(SERVER_TYPE_CLUB);
	if (NULL != pInfo)
	{
		CSvrProxy* pProxy = new CClubSvrProxy();
		pProxy->Initialize(pInfo->iProcessID, pInfo->szIP, pInfo->iPort);
		AddProxy(pProxy);
	}

	//Chat
	pInfo = pSvrList->GetProcessInfo(SERVER_TYPE_CHAT);
	if (NULL != pInfo)
	{
		CSvrProxy* pProxy = new CChatSvrProxy();
		pProxy->Initialize(pInfo->iProcessID, pInfo->szIP, pInfo->iPort);
		AddProxy(pProxy);
	}

	//Log
	pInfo = pSvrList->GetProcessInfo(SERVER_TYPE_LOG);
	if (NULL != pInfo)
	{
		CSvrProxy* pProxy = new CLogSvrProxy();
		pProxy->Initialize(pInfo->iProcessID, pInfo->szIP, pInfo->iPort);
		AddProxy(pProxy);
	}

	//GM
	pInfo = pSvrList->GetProcessInfo(SERVER_TYPE_GM);
	if (NULL != pInfo)
	{
		CSvrProxy* pProxy = new CGMSvrProxy();
		pProxy->Initialize(pInfo->iProcessID, pInfo->szIP, pInfo->iPort);
		AddProxy(pProxy);
		pProxy->SetFirstPacketRecv(TRUE);
	}

	//Match
	MatchServerInfoMap* pMatchServerMap = pSvrList->GetMatchServerInfoMap();
	CHECK_NULL_POINTER_BOOL(pMatchServerMap);

	MatchServerInfoMap::iterator iter = pMatchServerMap->begin();
	for (; pMatchServerMap->end() != iter; ++iter)
	{
		SMatchServerInfo* pMatchInfo = iter->second;

		CMatchSvrProxy* pProxy = new CMatchSvrProxy();
		pProxy->Initialize(pMatchInfo->iProcessID, pMatchInfo->szIP, pMatchInfo->iPort);
		pProxy->SetLvSection(pMatchInfo->iEnteranceLvGrade);
		pProxy->SetMatchType(pMatchInfo->iMatchType);
		AddProxy(pProxy);
	}

	//League
	pInfo = pSvrList->GetProcessInfo(SERVER_TYPE_LEAGUE);
	if (NULL != pInfo)
	{
		CSvrProxy* pProxy = new CLeagueSvrProxy();
		pProxy->Initialize(pInfo->iProcessID, pInfo->szIP, pInfo->iPort);
		pProxy->SetMatchType(MATCH_TYPE_RANKMATCH);
		AddProxy(pProxy);
	}

	//odbc
	pInfo = pSvrList->GetProcessInfo(SERVER_TYPE_ODBC);
	if (NULL != pInfo)
	{
		CSvrProxy* pProxy = new CODBCSvrProxy();
		pProxy->Initialize(pInfo->iProcessID, pInfo->szIP, pInfo->iPort);
		AddProxy(pProxy);
	}
	
	return TRUE;
}

void CGameProxyManager::Disconnect(CSvrProxy* pProxy)
{
	switch (pProxy->GetState())
	{
	case FS_MATCH_SERVER_PROXY:
		CFSGameServer::GetInstance()->BroadCastMatchServerLost((CMatchSvrProxy*)pProxy);
		break;
	case FS_CHAT_SERVER_PROXY:
		LOBBYCHAT.ChatServerDisconnected();
		CFSGameServer::GetInstance()->BroadCastChatServerStatus(SERVER_OFF);
	case FS_LEAGUE_SERVER_PROXY:
		CFSGameServer::GetInstance()->BroadCastLeagueServerLost((CLeagueSvrProxy*)pProxy);
		break;
	}

	CProxyManager::Disconnect(pProxy);
}

void CGameProxyManager::SendToAllMatchProxy(int iCommand, PVOID info, int iSize)
{
	CPacketComposer Packet(iCommand);
	Packet.Add((PBYTE)info, iSize);

	SVRPROXYMAP::iterator iter = m_mapRatingMatchSvrProxy.begin();
	for (; m_mapRatingMatchSvrProxy.end() != iter; ++iter)
	{
		if (TRUE == iter->second->IsConnected())
		{
			iter->second->Send(&Packet);
		}
	}

	for (int i = 0; i < MAX_MATCH_TYPE_COUNT; ++i)
	{
		if (NULL != m_paMatchProxy[i] &&
			TRUE == m_paMatchProxy[i]->IsConnected())
		{
			m_paMatchProxy[i]->Send(&Packet);
		}
	}
}

void CGameProxyManager::SendMatchServerStatus(CFSGameUser* pUser)
{
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_MATCH_SERVER_CONNECT info;

	SVRPROXYMAP::iterator iter = m_mapRatingMatchSvrProxy.begin();
	for (; m_mapRatingMatchSvrProxy.end() != iter; ++iter)
	{
		CMatchSvrProxy* pProxy = (CMatchSvrProxy*)iter->second;
		if (TRUE == pProxy->IsConnected() &&
			TRUE == pProxy->IsValidLv(pUser->GetCurUsedAvtarLv()))
		{
			info.btMatchType = pProxy->GetMatchType();
			info.btAutoTeam = pProxy->IsAutoTeam();
			pUser->Send(S2C_MATCH_SERVER_CONNECT, &info, sizeof(SS2C_MATCH_SERVER_CONNECT), FALSE);
		}
	}

	for (int i = 0; i < MAX_MATCH_TYPE_COUNT; ++i)
	{
		CMatchSvrProxy* pProxy = (CMatchSvrProxy*)m_paMatchProxy[i];

		if (NULL != pProxy &&
			TRUE == pProxy->IsConnected())
		{
			info.btMatchType = pProxy->GetMatchType();
			info.btAutoTeam = pProxy->IsAutoTeam();
			pUser->Send(S2C_MATCH_SERVER_CONNECT, &info, sizeof(SS2C_MATCH_SERVER_CONNECT), FALSE);
		}
	}

	CLeagueSvrProxy* pLeague = (CLeagueSvrProxy*)GetProxy(FS_LEAGUE_SERVER_PROXY);
	if( NULL != pLeague )
	{
		info.btMatchType = MATCH_TYPE_RANKMATCH;
		info.btAutoTeam = (BYTE)pLeague->IsAutoTeam();

		pUser->Send(S2C_MATCH_SERVER_CONNECT, &info, sizeof(SS2C_MATCH_SERVER_CONNECT), FALSE );
	}

	pUser->Send();
}