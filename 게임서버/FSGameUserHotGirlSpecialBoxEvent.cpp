qinclude "stdafx.h"
qinclude "FSGameUserHotGirlSpecialBoxEvent.h"
qinclude "ThreadODBCManager.h"
qinclude "CFSGameUser.h"
qinclude "HotGirlSpecialBoxEventManager.h"
qinclude "RewardManager.h"

CFSGameUserHotGirlSpecialBoxEvent::CFSGameUserHotGirlSpecialBoxEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)	
	, m_iSpecialBoxCount(0)
	, m_btNormalPotion(0)
	, m_btSpecialPotion(0)
	, m_bIsGetMainReward(FALSE)
	, m_bIsFirstLogin(FALSE)
{
}


CFSGameUserHotGirlSpecialBoxEvent::~CFSGameUserHotGirlSpecialBoxEvent(void)
{
}

BOOL	CFSGameUserHotGirlSpecialBoxEvent::Load()
{
	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	BYTE btCurrentSeason = HOTGIRLSPECIALBOX.GetCurrentSeason();

	CHECK_CONDITION_RETURN(SEASON_NONE == btCurrentSeason, TRUE);

	ODBCRETURN iRet = pBaseODBC->EVENT_HOTGIRLSPECIALBOX_GetUserData(m_pUser->GetUserIDIndex(), btCurrentSeason, m_iSpecialBoxCount, m_btNormalPotion, m_btSpecialPotion, m_bIsGetMainReward);
	if(1 == iRet)	// 각 이벤트 시즌 중 최초 로그인일 경우
	{
		m_bIsFirstLogin = TRUE;
	}
	else if(ODBC_RETURN_SUCCESS != iRet)
	{
		WRITE_LOG_NEW(LOG_TYPE_HOTGIRLSPECIALBOX, DB_DATA_LOAD, FAIL, "EVENT_HOTGIRLSPECIALBOX_GetUserData failed. UserIDIndex:10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}	

	m_bDataLoaded = TRUE;

	return TRUE;
}

void	CFSGameUserHotGirlSpecialBoxEvent::SendHotGirlSpecialBoxEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	BYTE btCurrentSeason = HOTGIRLSPECIALBOX.GetCurrentSeason();

	CHECK_CONDITION_RETURN_VOID(SEASON_NONE == btCurrentSeason);

	SS2C_HOTGIRLSPECIALBOX_EVENT_INFO_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_EVENT_INFO_NOT));

	vector<SHOTGIRLSPECIALBOX_REWARD_INFO>	vecInfo;
	HOTGIRLSPECIALBOX.GetRewardInfo(vecInfo);

	ss.btCurrentSeason = btCurrentSeason;
	ss.bIsFirstLogin = m_bIsFirstLogin;
	ss.iRewardListCount = vecInfo.size();
	
	m_bIsFirstLogin = FALSE;

	CPacketComposer Packet(S2C_HOTGIRLSPECIALBOX_EVENT_INFO_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_EVENT_INFO_NOT));

	for(int i = 0; i < ss.iRewardListCount; ++i)
	{
		Packet.Add((PBYTE)&vecInfo[i], sizeof(SHOTGIRLSPECIALBOX_REWARD_INFO));
	}

	m_pUser->Send(&Packet);
}

void	CFSGameUserHotGirlSpecialBoxEvent::SendHotGirlSpecialBoxUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	BYTE btCurrentSeason = HOTGIRLSPECIALBOX.GetCurrentSeason();

	CHECK_CONDITION_RETURN_VOID(SEASON_NONE == btCurrentSeason);

	SS2C_HOTGIRLSPECIALBOX_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_USER_INFO_RES));

	ss.iSpecialBoxCount		= m_iSpecialBoxCount;
	ss.btCurrentSeason		= btCurrentSeason;
	ss.bIsGetMainReward		= m_bIsGetMainReward;

	// 일반물약 설정
	for(BYTE btPotionType = POTION_NORMAL_1; btPotionType <= POTION_NORMAL_3; ++btPotionType)
	{
		if(m_btNormalPotion > btPotionType)
		{
			ss.bPotion[btPotionType] = TRUE;
		}
	}

	// 특수물약 설정
	if(m_btSpecialPotion == SPECIAL_POTION_GET_1)
	{
		if(m_pUser->GetUserIDIndex()  == 0)		// UserIDIndex 가 짝수인 유저는 상의 물약 먼저 나옴.
.
			ss.bPotion[POTION_SPECIAL_1] = TRUE;
		else										// UserIDIndex 가 홀수인 유저는 하의 물약 먼저 나옴.
			ss.bPotion[POTION_SPECIAL_2] = TRUE;
	}
	else if(m_btSpecialPotion == SPECIAL_POTION_GET_2)
	{
		ss.bPotion[POTION_SPECIAL_1] = TRUE;
		ss.bPotion[POTION_SPECIAL_2] = TRUE;
	}

	CPacketComposer Packet(S2C_HOTGIRLSPECIALBOX_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_USER_INFO_RES));

	m_pUser->Send(&Packet);
}

BOOL	CFSGameUserHotGirlSpecialBoxEvent::BuySpecialBox_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_HOTGIRLSPECIALBOX, INVALED_DATA, CHECK_FAIL, "BuySpecialBox_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int	iBuyBoxCount = pBillingInfo->iParam1;
	int	iSumBoxCount = 0;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	SS2C_HOTGIRLSPECIALBOX_BUY_RESULT_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_BUY_RESULT_NOT));
	
	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_HOTGIRLSPECIALBOX_BuyBox(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, iBuyBoxCount, iPrevCash, iPostCash, pBillingInfo->iItemPrice, iSumBoxCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_HOTGIRLSPECIALBOX, LA_DEFAULT, NONE, "EVENT_HOTGIRLSPECIALBOX_BuyBox Fail, User:10752790, GameID:(null)", m_pUser->GetUserIDIndex(), m_pUser->GetGameID());
ult = RESULT_HOTGIRLSPECIALBOX_BUY_FAIL;
	}
	else
	{
		m_iSpecialBoxCount = iSumBoxCount;
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_BUY_SUCCESS;
		ss.iSpecialBoxCount = m_iSpecialBoxCount;

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);

	// 특급상자 구매 결과 알림 패킷 보냄.
	CPacketComposer	Packet(S2C_HOTGIRLSPECIALBOX_BUY_RESULT_NOT);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_BUY_RESULT_NOT));

	m_pUser->Send(&Packet);

	int iCostType = pBillingInfo->iSellType;
	m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin );
	m_pUser->SendUserGold();

	return (ss.btResult == RESULT_HOTGIRLSPECIALBOX_BUY_SUCCESS);
}

BOOL	CFSGameUserHotGirlSpecialBoxEvent::OpenSpecialBox(BYTE btUserCurrentSeason)
{
	BYTE btCurrentSeason = HOTGIRLSPECIALBOX.GetCurrentSeason();

	CHECK_CONDITION_RETURN(SEASON_NONE == btCurrentSeason, FALSE);

	SS2C_HOTGIRLSPECIALBOX_OPEN_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_OPEN_RES));

	if(m_iSpecialBoxCount <= 0)
	{
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_OPEN_LACK_BOX_FAIL;
	}
	else if(btUserCurrentSeason != btCurrentSeason)
	{
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_OPEN_WRONG_SEASON_FAIL;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_OPEN_FULL_MAILBOX_FAIL;
	}
	else
	{
		ss.iRewardIndex = HOTGIRLSPECIALBOX.GetRewardRandom(m_btSpecialPotion);

		ss.btResult = RESULT_HOTGIRLSPECIALBOX_OPEN_GIVE_REWARD_FAIL;

		SRewardConfig* pReward = REWARDMANAGER.GetReward(ss.iRewardIndex);

		if(pReward)
		{
			BOOL bResult = FALSE;

			if(REWARD_TYPE_SPECIAL_POTION == pReward->iRewardType)
			{
				// 특수물약 설정
				if(m_btSpecialPotion == SPECIAL_POTION_GET_0)		// 첫번째 얻는 특수물약일때,
				{ 
					ss.bGetPotionType = (m_pUser->GetUserIDIndex()  == 0) ? POTION_SPECIAL_1 : POTION_SPECIAL_2;
;
				}
				else if(m_btSpecialPotion == SPECIAL_POTION_GET_1)	// 두번째 얻는 특수물약일때,
				{
					ss.bGetPotionType = (m_pUser->GetUserIDIndex()  == 0) ? POTION_SPECIAL_2 : POTION_SPECIAL_1;
;
				}

				bResult = GivePotion(REWARD_TYPE_SPECIAL_POTION, btCurrentSeason);
			}
			else
			{
				pReward = REWARDMANAGER.GiveReward(m_pUser, pReward);
				bResult = (pReward != NULL);
			}

			if(bResult)
			{
				CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
				CHECK_NULL_POINTER_BOOL(pBaseODBC);

				int iResultBoxCount = 0;

				if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_HOTGIRLSPECIALBOX_OpenBox(m_pUser->GetUserIDIndex(), btCurrentSeason, ss.iRewardIndex, iResultBoxCount))
				{
					ss.btResult = RESULT_HOTGIRLSPECIALBOX_OPEN_SUCCESS;
					m_iSpecialBoxCount = iResultBoxCount;
					ss.iSpecialBoxCount = m_iSpecialBoxCount;

					m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_OPEN_HOTGIRLSPECIALBOX);
				}
			}
		}		
	}

	CPacketComposer	Packet(S2C_HOTGIRLSPECIALBOX_OPEN_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_OPEN_RES));

	m_pUser->Send(&Packet);

	return TRUE;
}

BOOL	CFSGameUserHotGirlSpecialBoxEvent::GivePotion(BYTE btPotionRewardType, BYTE btCurrentSeason)
{
	switch(btPotionRewardType)
	{
	case REWARD_TYPE_NORMAL_POTION:
		{
			btCurrentSeason = HOTGIRLSPECIALBOX.GetCurrentSeason();
			CHECK_CONDITION_RETURN(SEASON_NONE == btCurrentSeason, FALSE);	
			CHECK_CONDITION_RETURN(m_btNormalPotion >= NORMAR_POTION_MAX_COUNT, FALSE);
		}
		break;

	case REWARD_TYPE_SPECIAL_POTION:
		{
			CHECK_CONDITION_RETURN(m_btSpecialPotion >= SPECIAL_POTION_MAX_COUNT, FALSE);
		}
		break;

	default: 
		return FALSE;
	}

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	BYTE btResultPotionCount = 0;

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_HOTGIRLSPECIALBOX_GivePotion(m_pUser->GetUserIDIndex(), btCurrentSeason, btPotionRewardType, btResultPotionCount))
	{
		return FALSE;
	}

	if(REWARD_TYPE_NORMAL_POTION == btPotionRewardType)
	{
		m_btNormalPotion = btResultPotionCount;
	}
	else if(REWARD_TYPE_SPECIAL_POTION == btPotionRewardType)
	{
		m_btSpecialPotion = btResultPotionCount;
	}

	return TRUE;
}

BOOL	CFSGameUserHotGirlSpecialBoxEvent::GiveMainReward(BYTE btUserCurrentSeason)
{
	BYTE btCurrentSeason = HOTGIRLSPECIALBOX.GetCurrentSeason();

	CHECK_CONDITION_RETURN(SEASON_NONE == btCurrentSeason, FALSE);

	SS2C_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_RES));

	if(btUserCurrentSeason != btCurrentSeason)
	{
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_WRONG_SEASON_FAIL;
	}
	else if(m_bIsGetMainReward)
	{
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_ALREADY_GET_FAIL;
	}
	else if(m_btNormalPotion != NORMAR_POTION_MAX_COUNT ||
			m_btSpecialPotion != SPECIAL_POTION_MAX_COUNT)
	{
		ss.btResult = RESULT_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_LACK_POTION_FAIL;
	}
	else
	{
		int iMainRewardIndex = HOTGIRLSPECIALBOX.GetMainRewardInfo(btCurrentSeason);
		SRewardConfig* pReward = REWARDMANAGER.GetReward(iMainRewardIndex);
		CHECK_NULL_POINTER_BOOL(pReward);

		SRewardConfigItem* pRewardItem = dynamic_cast<SRewardConfigItem*>(pReward);
		CHECK_NULL_POINTER_BOOL(pRewardItem);

		if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + pRewardItem->GetItemCount() <= MAX_PRESENT_LIST)
		{
			CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
			CHECK_NULL_POINTER_BOOL(pBaseODBC);

			if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_HOTGIRLSPECIALBOX_GiveMainReward(m_pUser->GetUserIDIndex(), btCurrentSeason))
			{
				ss.btResult = RESULT_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_SUCCESS;
				m_bIsGetMainReward = TRUE;

				pReward = REWARDMANAGER.GiveReward(m_pUser, pReward);

				if(NULL == pReward)
				{
					WRITE_LOG_NEW(LOG_TYPE_HOTGIRLSPECIALBOX, LA_DEFAULT, NONE, "REWARDMANAGER.GiveReward Fail, User:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), iMainRewardIndex);

			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_HOTGIRLSPECIALBOX, LA_DEFAULT, NONE, "EVENT_HOTGIRLSPECIALBOX_GiveMainReward Fail, User:10752790", m_pUser->GetUserIDIndex());
		}
		else
		{
			ss.btResult = RESULT_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_FULL_MAILBOX_FAIL;
		}
	}

	CPacketComposer	Packet(S2C_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_RES));

	m_pUser->Send(&Packet);

	return TRUE;
}

void	CFSGameUserHotGirlSpecialBoxEvent::SetSpecialBoxCount(int iCount)
{
	CHECK_CONDITION_RETURN_VOID(SEASON_NONE == HOTGIRLSPECIALBOX.GetCurrentSeason());
	
	m_iSpecialBoxCount = iCount;
}