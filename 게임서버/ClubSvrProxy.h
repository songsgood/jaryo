#if !defined(AFX_CLUBSVRPROXY_H__F5081D19_108D_416B_9828_B5BD6AAF3966__INCLUDED_)
#define AFX_CLUBSVRPROXY_H__F5081D19_108D_416B_9828_B5BD6AAF3966__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "SvrProxy.h"
qinclude "TypeList.h"
qinclude "ThreadODBCManager.h"

#define DECLARE_CLUBPROXY_PROC_FUNC(x) void Proc_##x(CReceivePacketBuffer*);

class CClubSvrProxy : public CSvrProxy
{
public:
	CClubSvrProxy();
	virtual ~CClubSvrProxy();

	void			SendFirstPacket();
	virtual void	ProcessPacket(CReceivePacketBuffer* recvPacket);

	void			SendGameIDChangeReq(SG2CL_CHANGE_MEMBERID_REQ info);
	void			SendClubChatReq(SG2CL_CLUB_CHAT_REQ info);
	void			SendClubMemberLogin(SG2CL_CLUB_MEMBER_LOGIN	info);
	void			SendClubMemberLogout(SG2CL_DEFAULT info);
	void			SendClubInfoReq(SG2CL_CLUB_INFO_REQ info);
	void			SendClubLobbyMemberListReq(SG2CL_DEFAULT info);
	void			SendClubMemberListReq(SG2CL_DEFAULT info);
	void			SendClubListReq(SG2CL_CLUB_LIST_REQ info);
	void			SendClubMarkChangeReq(SG2CL_CLUB_MARK_CHANGE_REQ info);
	void			SendClubGradeUpReq(SG2CL_DEFAULT info);
	void			SendClubNoticeChangeReq(SG2CL_CLUB_NOTICE_CHANGE_REQ info);
	void			SendClubInviteReq(SG2CL_CLUB_INVITE_REQ info);
	void			SendClubJoinReq(SG2CL_CLUB_JOIN_REQ info);
	void			SendClubJoinRequestReq(SG2CL_CLUB_JOIN_REQUEST_REQ info);
	void			SendClubRemoveMemberReq(SG2CL_CLUB_REMOVE_MEMBER_REQ info);
	void			SendClubWithDrawReq(SG2CL_CLUB_WITHDRAW_REQ info);
	void			SendClubAdminMemberListReq(SG2CL_DEFAULT info);
	void			SendClubAddAdminMemberReq(SG2CL_CLUB_ADD_ADMIN_MEMBER_REQ info);
	void			SendClubRemoveAdminMemberReq(SG2CL_CLUB_REMOVE_ADMIN_MEMBER_REQ info);
	void			SendClubAddKeyPlayerReq(SG2CL_CLUB_ADD_KEY_PLAYER_REQ info);
	void			SendClubRemoveKeyPlayerReq(SG2CL_CLUB_REMOVE_KEY_PLAYER_REQ info);
	void			SendClubCLReq(SG2CL_CLUB_CL_REQ info);
	void			SendClubExtendKeyPlayerReq(SG2CL_DEFAULT info);
	void			SendClubMemberPRChangeReq(SG2CL_CLUB_MEMBER_PR_CHANGE_REQ info);
	void			SendClubCheckClubNameReq(SG2CL_CLUB_CHECK_CLUBNAME_REQ info);
	void			SendClubSetUpReq(SG2CL_CLUB_SETUP_REQ info);
	void			SendClubTransferReq(SG2CL_CLUB_TRANSFER_REQ info);
	void			SendClubDonationInfoReq(SG2CL_DEFAULT info);
	void			SendClubDonationReq(SG2CL_CLUB_DONATION_REQ info);
	void			SendClubCreateReq(SG2CL_CLUB_CREATE_REQ info);
	void			SendClubMemberRecordListReq(SG2CL_DEFAULT info);
	void			SendClubRecentMatchRecordListReq(SG2CL_DEFAULT info);
	void			SendClubPRListReq(SG2CL_CLUB_PR_LIST_REQ info);
	void			SendClubPRChangeReq(SG2CL_CLUB_PR_CHANGE_REQ info);
	void			SendMyClubRankReq(SG2CL_MY_CLUB_RANK_REQ info);
	void			SendClubRankListReq(SG2CL_CLUB_RANK_LIST_REQ info);
	void			SendClubCheerLeaderInfoReq(SG2CL_DEFAULT info);
	void			SendClubBuyCheerLeaderInfoReq(SG2CL_CLUB_BUY_CHEERLEADER_INFO_REQ info);
	void			SendClubBuyCheerLeaderReq(SG2CL_CLUB_BUY_CHEERLEADER_REQ info);
	void			SendClubCheerLeaderListReq(SG2CL_DEFAULT info);
	void			SendClubCheerLeaderChangeReq(SG2CL_CLUB_CHEERLEADER_CHANGE_REQ info);
	void			SendClubMyCheerLeaderChangeReq(SG2CL_CLUB_MY_CHEERLEADER_CHANGE_REQ info);
	void			SendClubStatusChangeReq(SG2CL_CLUB_STATUS_CHANGE_REQ info);
	void			SendMyClubPRReq(SG2CL_DEFAULT info);

	void			Process_ClubChatRes(CReceivePacketBuffer* rp);
	void			Process_ClubMemberStatusUpdateNoticeRes(CReceivePacketBuffer* rp);
	void			Process_ClubInfoRes(CReceivePacketBuffer* rp);
	void			Process_ClubLobbyMemberListRes(CReceivePacketBuffer* rp);
	void			Process_ClubMemberListRes(CReceivePacketBuffer* rp);
	void			Process_ClubListRes(CReceivePacketBuffer* rp);
	void			Process_ClubMarkListRes(CReceivePacketBuffer* rp);
	void			Process_ClubMarkChangeRes(CReceivePacketBuffer* rp);
	void			Process_ClubGradeUpInfoRes(CReceivePacketBuffer* rp);
	void			Process_ClubGradeUpRes(CReceivePacketBuffer* rp);
	void			Process_ClubNoticeChangeRes(CReceivePacketBuffer* rp);
	void			Process_ClubInviteRes(CReceivePacketBuffer* rp);
	void			Process_ClubJoinRes(CReceivePacketBuffer* rp);
	void			Process_ClubJoinRequestRes(CReceivePacketBuffer* rp);
	void			Process_ClubRemoveMemberRes(CReceivePacketBuffer* rp);
	void			Process_ClubWithDrawRes(CReceivePacketBuffer* rp);
	void			Process_ClubAdminMemberListRes(CReceivePacketBuffer* rp);
	void			Process_ClubAddAdminMemberRes(CReceivePacketBuffer* rp);
	void			Process_ClubRemoveAdminMemberRes(CReceivePacketBuffer* rp);
	void			Process_ClubAddKeyPlayerRes(CReceivePacketBuffer* rp);
	void			Process_ClubRemoveKeyPlayerRes(CReceivePacketBuffer* rp);
	void			Process_ClubCLRes(CReceivePacketBuffer* rp);
	void			Process_ClubDeleteBoardMsgRes(CReceivePacketBuffer* rp);
	void			Process_ClubBoardMsgRes(CReceivePacketBuffer* rp);
	void			Process_ClubAddBoardMsgRes(CReceivePacketBuffer* rp);
	void			Process_ClubExtendKeyPlayerRes(CReceivePacketBuffer* rp);
	void			Process_ClubMemberPRChangeRes(CReceivePacketBuffer* rp);
	void			Process_ClubCheckClubNameRes(CReceivePacketBuffer* rp);
	void			Process_ClubSetUpRes(CReceivePacketBuffer* rp);
	void			Process_ClubTransferRes(CReceivePacketBuffer* rp);
	void			Process_ClubDonationInfoRes(CReceivePacketBuffer* rp);
	void			Process_ClubDonationRes(CReceivePacketBuffer* rp);
	void			Process_ClubCreateRes(CReceivePacketBuffer* rp);
	void			Process_ClubMemberRecordListRes(CReceivePacketBuffer* rp);
	void			Process_ClubRecentMatchRecordListRes(CReceivePacketBuffer* rp);
	void			Process_ClubPRListRes(CReceivePacketBuffer* rp);
	void			Process_ClubPRChangeRes(CReceivePacketBuffer* rp);
	void			Process_MyClubRankRes(CReceivePacketBuffer* rp);
	void			Process_ClubRankListRes(CReceivePacketBuffer* rp);
	void			Process_ClubCheerLeaderInfoRes(CReceivePacketBuffer* rp);
	void            Process_ClubBuyCheerLeaderInfoRes(CReceivePacketBuffer* rp);
	void			Process_ClubBuyCheerLeaderRes(CReceivePacketBuffer* rp);
	void			Process_ClubCheerLeaderListRes(CReceivePacketBuffer* rp);
	void			Process_ClubCheerLeaderChangeRes(CReceivePacketBuffer* rp);
	void			Process_ClubCheerLeaderChangedNotice(CReceivePacketBuffer* rp);
	void			Process_ClubCheerLeaderExpiredNotice(CReceivePacketBuffer* rp);
	void			Process_ClubMyCheerLeaderChangeRes(CReceivePacketBuffer* rp);
	void			Process_ClubStatusChangeRes(CReceivePacketBuffer* rp);
	void			Process_MyClubPRRes(CReceivePacketBuffer* rp);
	void			Process_UpdateClubNotice(CReceivePacketBuffer* rp);

	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_ANNOUNCE_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_GET_BENEFIT_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_COMPLETE_NOTICE);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_PENNANT_SLOT_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_PENNANT_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_USE_PENNANT_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_USE_PENNANT_NOTICE);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_PENNANT_SLOT_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_CLOTHES_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_CLOTHES_NOTICE);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_CLUBMARK_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MEMBER_LOGIN_NOTICE);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_UPDATE_CLUBTOURNAMENT_AGENCY_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_MATCH_TBALE_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_KEY_PLAYER_LIST_RES)
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_LIST_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_ROOM_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_COUNT_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_REMAIN_TIME_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_MATCH_RESULT_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GET_BENEFIT_ITEM_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GAME_START_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_CURRENT_ROUND_CLUB_STATUS_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GAME_END_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_WINTEAM_GAME_END_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GIVE_BENEFIT_CLUBITEM_NOTICE);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GIVE_BENEFIT_ITEM_NOTICE);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_TIME_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_INTENSIVEPRACTICE_RANK_LIST_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_LEAGUE_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_RANKING_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_RANKING_LIST_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_MEMBER_RANKING_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_MEMBER_LEAGUE_RANKING_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_RANKING_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_LEAGUE_RANKING_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_TOURNAMENT_RANKING_INFO_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_TOURNAMENT_RANKING_LIST_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_USER_UPDATE_GAMERESULT_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_GIVE_REWARD_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_COLLECTOR_NOT);	
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_INFO_RES);	
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES);	
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_NAME_CHANGE_NOT);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_NOTICE_RES);
	DECLARE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_NOTICE_NOT);
};

#endif // !defined(AFX_CLUBSVRPROXY_H__F5081D19_108D_416B_9828_B5BD6AAF3966__INCLUDED_)

