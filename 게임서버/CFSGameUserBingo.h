qpragma once

class CFSGameUser;
class CFSODBCBase;

#define MIN_CONTINUOUS_SPEED 1000 // 1sec

class CFSGameUserBingo
{
public:
	CFSGameUserBingo(CFSGameUser* pUser);
	~CFSGameUserBingo(void);

	BOOL Load();

	BOOL CheckBingoCompleted();
	BOOL IsThereBingoLine();
	RESULT_BINGO_INITIALIZE Initialize(BYTE& btCompleteRewardType);
	RESULT_USE_BINGO_TATOO PickNumber(BYTE& btNumber, int& iUpdatedTatooCount, int& iMarkedSlotIndex);
	void CheckLineMade(int iMarkedSlotIndex = -1);
	RESULT_BINGO_SHUFFLE Shuffle();
	BOOL ShuffleSlot_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	RESULT_BINGO_INITIALIZE_REWARD InitializeReward();

	int GetShuffleCount()	{	return m_BingoBoard.iShuffledCount; }

	void SendBingoBoardInfo();
	void SendBingoSlotList();
	void SendBingoLineList();
	void SendBingoPremiumRewardInfo();
	void SendBingoRewardList();
	void SendBingoMainRewardList();
	BOOL CheckContinuousCount();
	void SendBingoTokenInfo();

private:
	BOOL LoadLine(CFSODBCBase* pODBC);
	BOOL LoadSlot(CFSODBCBase* pODBC);
	BOOL LoadBoard(CFSODBCBase* pODBC);

	BINGO_BOARD_STATUS GetBoardStatus();

	SAvatarBingoSlot* GetSlotWithNumber(int iNumber);
	SAvatarBingoLine* GetLine(int iLineIndex);
	SAvatarBingoSlot* GetSlot(int iSlotIndex);

private:
	CFSGameUser* m_pUser;
	BOOL		m_bDataLoaded;

	SAvatarBingoBoard m_BingoBoard;
	AVATAR_BINGO_LINE_MAP	m_mapBingoLine;
	AVATAR_BINGO_SLOT_MAP	m_mapBingoSlot;

	DWORD	m_dwPickCount;
};

