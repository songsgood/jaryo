qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserGoldenSafeEvent
{
public:
	CFSGameUserGoldenSafeEvent(CFSGameUser* pUser);
	~CFSGameUserGoldenSafeEvent(void);

	BOOL			Load();
	BOOL			CheckResetDate(time_t tCurrentTime = _time64(NULL));
	BOOL			IsGoldenSafeEventButtonOpen();
	BOOL			SendEventInfo();
	void			SendUserEventData();
	void			GiveGoldenSafeReward();

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;
	int				m_iKeepStepFlag;
	BYTE			m_btCurrentStep;
	BYTE			m_btCurrentStepStatus;
	time_t			m_tRecentCheckDate;
	BOOL			m_bIsGetKeepReward;
};

