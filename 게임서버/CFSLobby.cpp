qinclude "stdafx.h"
qinclude "CFSLobby.h"
qinclude "CFSGameUser.h"
qinclude "CFSGameClient.h"
qinclude "BingoManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "LuckyBoxManager.h"
qinclude "WordPuzzlesManager.h"
qinclude "PuzzlesManager.h"
qinclude "MissionShopManager.h"
qinclude "CheerLeaderEventManager.h"
qinclude "EventDateManager.h"

CFSLobby::CFSLobby()
	:m_bItemShopEventButton(FALSE)
{
	
}

CFSLobby::~CFSLobby()
{
	
}

BOOL CFSLobby::AddUser(CFSGameUser* pUser, NEXUS_CLIENT_STATE eState)
{
	CHECK_NULL_POINTER_BOOL(pUser);

	CLock lock(&m_csUserMap[eState]);

	USERMAP::iterator iter = m_mapUser[eState].find(pUser->GetUserIDIndex());
	if (m_mapUser[eState].end() != iter)
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, USER_LOBBY_ENTER, ADD_FAIL, "CFSLobby::AddUser");
		return FALSE;
	}

	m_mapUser[eState][pUser->GetUserIDIndex()] = pUser;

	return TRUE;
}

BOOL CFSLobby::RemoveUser(CFSGameUser* pUser, NEXUS_CLIENT_STATE eState)
{
	CHECK_NULL_POINTER_BOOL(pUser);

	CLock lock(&m_csUserMap[eState]);

	USERMAP::iterator iter = m_mapUser[eState].find(pUser->GetUserIDIndex());
	if (m_mapUser[eState].end() == iter)
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, USER_LOBBY_EXIT, REMOVE_FAIL, "CFSLobby::RemoveUser");
		return FALSE;
	}

	m_mapUser[eState].erase(pUser->GetUserIDIndex());
	
	return TRUE;
}

void CFSLobby::BroadCast(CPacketComposer* pComposer, NEXUS_CLIENT_STATE eState)
{
	CHECK_CONDITION_RETURN_VOID(eState <= START_STATE_RELATED_NOTICE || END_STATE_RELATED_NOTICE <= eState);

	CLock lock(&m_csUserMap[eState]);

	USERMAP::iterator iter = m_mapUser[eState].begin();
	USERMAP::iterator endIter = m_mapUser[eState].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;
			
		if(NULL != pUser)
		{
			pUser->Send(pComposer);
		}
	}
}

void CFSLobby::BroadCastNotTournamentUser(CPacketComposer* pComposer)
{
	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CUser* pUser = iter->second;
		if(NULL != pUser && FALSE == pUser->IsTournamentUser())
		{
			pUser->Send(pComposer);
		}
	}
}

void CFSLobby::BroadCastToClubTournament(CPacketComposer* pComposer)
{
	CLock lock(&m_csUserMap[FS_CLUB]);

	USERMAP::iterator iter = m_mapUser[FS_CLUB].begin();
	USERMAP::iterator endIter = m_mapUser[FS_CLUB].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if(NULL != pUser && (USER_SUB_PAGE_STATE_CLUBTOURNAMENT == pUser->GetUserSubPageState() ||
			USER_SUB_PAGE_STATE_CLUBTOURNAMENT_VOTE_ROOM == pUser->GetUserSubPageState()))
		{
			pUser->Send(pComposer);
		}
	}
}

void CFSLobby::BroadCastToClubTournamentVoteRoom(CPacketComposer* pComposer, BYTE btRoundGroupIndex)
{
	CLock lock(&m_csUserMap[FS_CLUB]);

	USERMAP::iterator iter = m_mapUser[FS_CLUB].begin();
	USERMAP::iterator endIter = m_mapUser[FS_CLUB].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if(NULL != pUser && USER_SUB_PAGE_STATE_CLUBTOURNAMENT_VOTE_ROOM == pUser->GetUserSubPageState() &&
			(btRoundGroupIndex == pUser->GetRoundGroupIndex() || btRoundGroupIndex == MAX_CLUBTOURNAMENT_ROUND_GROUP_INDEX_COUNT))
		{
			pUser->Send(pComposer);
		}
	}
}

BOOL CFSLobby::UpdateState(CFSGameClient* pClient, NEXUS_CLIENT_STATE eState)
{
	CHECK_NULL_POINTER_BOOL(pClient);

	NEXUS_CLIENT_STATE eCurrentState = pClient->GetState();
	CHECK_CONDITION_RETURN(eCurrentState == eState, TRUE);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_BOOL(pUser);

	if (START_STATE_RELATED_NOTICE < eCurrentState &&
		eCurrentState < END_STATE_RELATED_NOTICE)
	{
		if (FALSE == RemoveUser(pUser, eCurrentState))
		{
			return FALSE;
		}
	}

	if (START_STATE_RELATED_NOTICE < eState &&
		eState < END_STATE_RELATED_NOTICE)
	{
		if (FALSE == AddUser(pUser, eState))
		{
			return FALSE;
		}
	}

	pClient->CBaseClient::SetState(eState);
	return TRUE;
}

void CFSLobby::BroadCastTeamInfo(CPacketComposer* pComposer, int iMatchSvrID, BYTE btMatchTeamScale, int iPage, int iTeamIdx, int iClubSN , int iOp, BOOL bSend/*=TRUE*/)
{
	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if(NULL != pUser && TRUE == pUser->EnableBroadcastTeamInfo(iMatchSvrID, btMatchTeamScale, iPage, iTeamIdx, iClubSN, iOp))
		{
			pUser->Send(pComposer, bSend);
		}
	}
}

void CFSLobby::BroadCastFreeRoomInfo(CPacketComposer* pComposer, int iMatchSvrID, int iPage, int iRoomIdx, int iOp, BOOL bSend/*=TRUE*/)
{
	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if(NULL != pUser && TRUE == pUser->EnableBroadcastFreeRoomInfo(iMatchSvrID, iPage, iRoomIdx, iOp))
		{
			pUser->Send(pComposer, bSend);
		}
	}
}

void CFSLobby::CheckIntensivePracticeGameTime()
{
	CLock lock(&m_csUserMap[FS_INTENSIVEPRACTICE]);

	USERMAP::iterator iter = m_mapUser[FS_INTENSIVEPRACTICE].begin();
	USERMAP::iterator endIter = m_mapUser[FS_INTENSIVEPRACTICE].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;
		if(NULL != pUser)
			pUser->GetUserIntensivePractice()->CheckGameTime();
	}
}

void CFSLobby::BroadCastWordPuzzlesEventStatus(OPEN_STATUS eStatus)
{
	SS2C_WORDPUZZLES_EVENT_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_WORDPUZZLES_EVENT_OPEN_NOT, &info, sizeof(SS2C_WORDPUZZLES_EVENT_OPEN_NOT));
		}
	}
}

void CFSLobby::BroadCastPuzzlesStatus(OPEN_STATUS eStatus)
{
	SS2C_PUZZLES_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_PUZZLES_OPEN_NOT, &info, sizeof(SS2C_PUZZLES_OPEN_NOT));
		}
	}
}

void CFSLobby::BroadCastThreeKingdomsEventStatus(OPEN_STATUS eStatus)
{
	SS2C_EVENT_THREE_KINGDOMS_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_EVENT_THREE_KINGDOMS_OPEN_NOT, &info, sizeof(SS2C_EVENT_THREE_KINGDOMS_OPEN_NOT));
		}
	}
}

void CFSLobby::BroadCastMissionShopEventStatus()
{
	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->SendMissionShopEventStatus();
		}
	}
}

void CFSLobby::BroadCastHotGirlMissionEventStatus(OPEN_STATUS eStatus)
{
	SS2C_EVENT_HOTGIRL_MISSION_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			info.btMissionStatus = pUser->GetUserHotGirlMissionEvent()->GetMissionStatus();
			pUser->Send(S2C_EVENT_HOTGIRL_MISSION_OPEN_NOT, &info, sizeof(SS2C_EVENT_HOTGIRL_MISSION_OPEN_NOT));
		}
	}
}

void CFSLobby::BroadCastCheerLeaderEventStatus(OPEN_STATUS eStatus)
{
	SS2C_CHEERLEADER_EVENT_NOT info;
	info.btOpenStatus = (BYTE)eStatus;
	info.tRemainTime = CHEERLEADEREVENT.GetRemainTime();

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_CHEERLEADER_EVENT_NOT, &info, sizeof(SS2C_CHEERLEADER_EVENT_NOT));
		}
	}
}

void CFSLobby::BroadCastLvUpEventStatus(OPEN_STATUS eStatus)
{
	SS2C_LVUPEVENT_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_LVUPEVENT_OPEN_NOT, &info, sizeof(SS2C_LVUPEVENT_OPEN_NOT));
		}
	}
}

void CFSLobby::BroadCastRandomItemBoardEventStatus(OPEN_STATUS eStatus)
{
	SS2C_RANDOMITEMBOARD_EVENT_OPEN_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_RANDOMITEMBOARD_EVENT_OPEN_NOT, &info, sizeof(SS2C_RANDOMITEMBOARD_EVENT_OPEN_NOT));
		}
	}
}

void CFSLobby::BroadCastEventStatus()
{
	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			EVENTDATEMANAGER.SendOpenEventList(pUser, TRUE);
		}
	}
}

void CFSLobby::BroadCastUserRandomItemTreeEvent(OPEN_STATUS eStatus)
{
	SS2C_RANDOMITEM_TREE_EVENT_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_RANDOMITEM_TREE_EVENT_NOT, &info, sizeof(SS2C_RANDOMITEM_TREE_EVENT_NOT));
		}
	}
}

void CFSLobby::BroadCastUserExpBoxItemEvent(OPEN_STATUS eStatus)
{
	SS2C_USER_EXPBOXITEM_EVENT_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->GetUserExpBoxItemEvent()->MakeCurrentSlotBoxInfo(info.boxInfo);
			pUser->Send(S2C_USER_EXPBOXITEM_EVENT_NOT, &info, sizeof(SS2C_USER_EXPBOXITEM_EVENT_NOT));
		}
	}
}

void CFSLobby::BroadCastUserRandomItemGroupEvent(OPEN_STATUS eStatus)
{
	SS2C_RANDOMITEM_GROUP_EVENT_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_RANDOMITEM_GROUP_EVENT_NOT, &info, sizeof(SS2C_RANDOMITEM_GROUP_EVENT_NOT));
		}
	}
}

void CFSLobby::BroadCastRandomCardEventStatus(OPEN_STATUS eStatus)
{
	SS2C_EVENT_RANDOMCARD_NOT info;
	info.btOpenStatus = (BYTE)eStatus;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_EVENT_RANDOMCARD_NOT, &info, sizeof(SS2C_EVENT_RANDOMCARD_NOT));
		}
	}
}

void CFSLobby::SetItemShopEventButton( BOOL b )
{
	m_bItemShopEventButton = b;
}

BOOL CFSLobby::GetItemShopeventButton( void )
{
	return m_bItemShopEventButton;
}

void CFSLobby::BroadCastShoppingFestivalEventStatus(BYTE btCurrentEventType, BYTE btOpenStatus)
{
	SS2C_EVENT_SHOPPING_FESTIVAL_NOT info;
	info.btEventType = btCurrentEventType;
	info.btOpenStatus = btOpenStatus;
	info.iCouponCnt = 0;

	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->Send(S2C_EVENT_SHOPPING_FESTIVAL_NOT, &info, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_NOT));
		}
	}
}

void CFSLobby::BroadCastShoppingFestivalEventInfo(BYTE btEventType)
{
	CLock lock(&m_csUserMap[NEXUS_LOBBY]);

	USERMAP::iterator iter = m_mapUser[NEXUS_LOBBY].begin();
	USERMAP::iterator endIter = m_mapUser[NEXUS_LOBBY].end();
	for(; iter != endIter; ++iter)
	{
		CFSGameUser* pUser = iter->second;

		if (NULL != pUser)
		{
			pUser->GetUserShoppingFestivalEvent()->SendEventInfo(btEventType);
		}
	}
}
