qpragma once

class CUser;
class CFSODBCBase;

struct SUserDiscountItemEvent
{
	int _iCurrentStep;
	int _iDiscountRate;
	int _iRemainSecond;

	SUserDiscountItemEvent()
	{
		ZeroMemory(this, sizeof(SUserDiscountItemEvent));
	}
};

typedef map<int/*_iStep*/,SUserDiscountItemEventBuyLog> EVENT_DISCOUNT_ITEM_BUY_LOG_MAP;

class CFSGameUserDiscountItemEvent
{
public:
	CFSGameUserDiscountItemEvent(CUser* pUser);
	~CFSGameUserDiscountItemEvent(void);

	BOOL Load();
	void UpdateItemPrice(BOOL bLogout = FALSE);
	BOOL CheckEventUser();
	void SendEventInfo();
	RESULT_EVENT_DISCOUNT_ITEM_BUY CheckBuyReq(int iStep, SRewardInfo& sReward, int& iPrice, int& iDiscountPrice);
	BOOL BuyDiscountItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);

private:
	CUser* m_pUser;
	BOOL m_bDataLoaded;
	int m_tMissionCheckTime;

	SUserDiscountItemEvent m_UserEvent;
	EVENT_DISCOUNT_ITEM_BUY_LOG_MAP m_mapUserBuyLog;
	BOOL m_bSave;
};

