qinclude "stdafx.h"
qinclude "FSGameUserFriendInviteList.h"
qinclude "CFSGameUser.h"
qinclude "FriendInviteManager.h"
qinclude "CFSGameServer.h"
qinclude "RewardManager.h"
qinclude "CenterSvrProxy.h"
qinclude "GameProxyManager.h"
qinclude "EventDateManager.h"
qinclude "Util.h"
qinclude "ODBCSvrProxy.h"
qinclude "MatchSvrProxy.h"

CFSGameUserFriendInviteList::CFSGameUserFriendInviteList(CFSGameUser* pUser)
	:m_pUser(pUser)
	,m_EventInviteKingInfo(pUser)
{
	for( int i = 0 ; i < MAX_FRIEND_INVITE_UPDATE ; ++i )
		m_lBool[i] = FALSE;

	m_lMissionSave = 0;
	m_lMissionUpdateCount = 0;
}


CFSGameUserFriendInviteList::~CFSGameUserFriendInviteList(void)
{
}

BOOL CFSGameUserFriendInviteList::LoadFriendInviteMyInfo(BOOL bLastConnectTimeNotice /*= FALSE*/)
{
	CHECK_CONDITION_RETURN(TRUE == GetUserUpdate(FRIEND_INVITE_UPDATE_MYINFO), TRUE);

	CHECK_NULL_POINTER_BOOL(m_pUser);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GetMyInfo(m_pUser->GetUserIDIndex(), m_myInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "FRIEND_INVITE_GetMyInfo UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	// update invite code
	if( 0 == strlen(m_myInfo.szInviteCode))
	{
		SFRIENDINVITE_UpdateInviteCode code;
		code.iUserIDIndex = m_pUser->GetUserIDIndex();
		if(SUCCEEDED(FRIENDINVITE.ConvertFriendInviteCode(code.szInviteCode,MAX_FRIEND_INVITE_CODE_LENGTH,code.iUserIDIndex, _GetServerIndex)))
		{
			if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateInviteCode(code))
			{
				WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "ConvertFriendInviteCode UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rintf_s(m_myInfo.szInviteCode, _countof(m_myInfo.szInviteCode), "");
			}
			else
				strncpy_s(m_myInfo.szInviteCode, _countof(m_myInfo.szInviteCode), code.szInviteCode, MAX_FRIEND_INVITE_CODE_LENGTH);
		}
	}

	if( FALSE == GetEventInviteKing()->LoadFriendInviteKingInfo() )
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "LoadFriendInviteKingInfo UserIDIndex : 10752790", m_pUser->GetUserIDIndex());

SetUserUpdate(FRIEND_INVITE_UPDATE_MYINFO);

	if( TRUE == bLastConnectTimeNotice )
		SendCenterSvrLastConnectTimeNoticeForFriend(TRUE);
	
	if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GetUserBenefit(m_pUser->GetUserIDIndex(), m_listBenefit))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_GetUserBenefit UserIDIndex : 10752790", m_pUser->GetUserIDIndex());

	return TRUE;
}

void CFSGameUserFriendInviteList::SendFriendInviteRegisterButtonNot()
{	
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteMyInfo());

	CheckAndGiveBenefit();
	CheckAndSendDeleteStatus();

	time_t tCreateAccount = m_pUser->GetAccountCreateTime();
	time_t tCurrentDate = _GetCurrentDBDate;
	int iCreateConditionValue = FRIENDINVITE.GetConfigValue(FIC_INVITE_NEWACCOUNT_CONDITION_DD, FP0);

	int iDiffDate = abs(difftime(tCreateAccount, tCurrentDate) / 86400)/*(24*60*60)*/;
	if(iCreateConditionValue < iDiffDate )
		return;

	CPacketComposer Packet(S2C_FRIEND_INVITE_CODE_REGISTER_BUTTON_NOT);

	SS2C_FRIEND_INVITE_CODE_REGISTER_BUTTON_NOT not;
	not.btEnable = (0 < m_myInfo.iFriendUserIDIndex) ? FALSE : TRUE;

	Packet.Add((PBYTE)&not, sizeof(SS2C_FRIEND_INVITE_CODE_REGISTER_BUTTON_NOT));

	m_pUser->Send(&Packet);
}

void CFSGameUserFriendInviteList::SendFriendInviteMyInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendList());

	CPacketComposer Packet(S2C_FRIEND_INVITE_INFO_RES);
	SS2C_FRIEND_INVITE_INFO_RES info;

	strncpy_s( info.szInviteCode, _countof(info.szInviteCode), m_myInfo.szInviteCode, MAX_FRIEND_INVITE_CODE_LENGTH+1 );
	info.btMyMissionInfoEnable = static_cast<BYTE>((0 < m_myInfo.iFriendUserIDIndex) ? TRUE : FALSE);
	info.btMaxFriendListCount = FRIENDINVITE.GetConfigValue(FIC_MAX_FRIEND_CNT, FP1);
	info.btMaxWaitListCount = FRIENDINVITE.GetConfigValue(FIC_MAX_FRIEND_CNT, FP2);

	list<SFRIENDINVITE_Friend> listFriend;
	BEGIN_SCOPE_LOCK(m_csFriendlist)

		info.btFriendListCount = m_mapFriend.size();

		for( auto iter = m_mapFriend.begin() ; iter != m_mapFriend.end() ; ++iter )
		{
			listFriend.push_back((iter->second));
		}

		listFriend.sort([](const SFRIENDINVITE_Friend& obj1 ,const SFRIENDINVITE_Friend& obj2)->bool
		{
			if( obj1.btStatus == FRIEND_INVITE_FRIEND_STATUS_SUCCESS_MISSION)
				return true;

			return false;
		});
	END_SCOPE_LOCK

	Packet.Add((PBYTE)&info, sizeof(SS2C_FRIEND_INVITE_INFO_RES));

	for( auto iter = listFriend.begin() ; iter != listFriend.end() ; ++iter )
	{
		SFRIENDINVITE_Friend* pinfo = &(*iter);
		SFRIEND_INVITE_LIST_INFO addinfo;
		addinfo.btButtonType = FRIEND_INVITE_BUTTON_TYPE_DEL;
		addinfo.btIndex = pinfo->btIndex;

		if(FRIEND_INVITE_FRIEND_STATUS_FRIEND == pinfo->btStatus || FRIEND_INVITE_FRIEND_STATUS_REWARD_FINISH == pinfo->btStatus)
			addinfo.btButtonType = FRIEND_INVITE_BUTTON_TYPE_DEL;
		else if(FRIEND_INVITE_FRIEND_STATUS_WAIT == pinfo->btStatus)
			addinfo.btButtonType = FRIEND_INVITE_BUTTON_TYPE_ACCEPT_REFUSAL;
		else if(FRIEND_INVITE_FRIEND_STATUS_SUCCESS_MISSION == pinfo->btStatus)
			addinfo.btButtonType = FRIEND_INVITE_BUTTON_TYPE_DEL_REWARD;

		addinfo.btOpCode = (FRIEND_INVITE_FRIEND_STATUS_WAIT == pinfo->btStatus) ? FRIEND_INVITE_OPCODE_WAITLIST : FRIEND_INVITE_OPCODE_FRIENDLIST;
		strncpy_s(addinfo.szComment,_countof(addinfo.szComment), pinfo->szComment,MAX_FRIEND_INVITE_COMMENT_LENGTH);
		addinfo.szComment[MAX_FRIEND_INVITE_COMMENT_LENGTH] = '\0';

		strncpy_s(addinfo.szGameID,_countof(addinfo.szGameID), pinfo->szGameID,MAX_GAMEID_LENGTH+1);
		addinfo.tInviteDate = pinfo->tInviteDate;
		if(0 >= pinfo->tLastConnectTime)// 접속중
			addinfo.tLastConnectTime = 0;
		else
			addinfo.tLastConnectTime = difftime(_GetCurrentDBDate, pinfo->tLastConnectTime);

		Packet.Add((PBYTE)&addinfo, sizeof(SFRIEND_INVITE_LIST_INFO));
	}

	m_pUser->Send(&Packet);
}

BOOL CFSGameUserFriendInviteList::LoadFriendList()
{
	CHECK_CONDITION_RETURN(TRUE == GetUserUpdate(FRIEND_INVITE_UPDATE_FRIENDLIST), TRUE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	
	list<SFRIENDINVITE_Friend> listFriend;
	if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GetFriendList(m_pUser->GetUserIDIndex(), listFriend))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "FRIEND_INVITE_GetFriendList UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	SetUserUpdate(FRIEND_INVITE_UPDATE_FRIENDLIST);

	listFriend.sort([](const SFRIENDINVITE_Friend& obj1, const SFRIENDINVITE_Friend& obj2)->bool
	{
		if( obj1.tInviteDate == obj2.tInviteDate)
			return obj1.iFriendUserIDIndex < obj2.iFriendUserIDIndex;

		return obj1.tInviteDate < obj2.tInviteDate;
	});

	BYTE btIndex = 0;
	for( auto iter = listFriend.begin() ; iter != listFriend.end() ; ++iter) 
		(*iter).btIndex = btIndex++;

	BEGIN_SCOPE_LOCK(m_csFriendlist)
		m_mapFriend.clear();

		for( auto iter = listFriend.begin() ; iter != listFriend.end() ; ++iter )
		{
			m_mapFriend[(*iter).btIndex] = (*iter);
	 	}
	END_SCOPE_LOCK

	return TRUE;
}

void CFSGameUserFriendInviteList::SendFriendMissionInfo( const SC2S_FRIEND_INVITE_MISSION_INFO_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendMissionList());

	BOOL bSuccess = FALSE;
	SFRIENDINVITE_Friend info;
	BEGIN_SCOPE_LOCK(m_csFriendlist)
		auto iter = m_mapFriend.find(rq.btIndex);
		if( iter != m_mapFriend.end() )
		{
			info = iter->second;
			bSuccess = TRUE;
		}	
	END_SCOPE_LOCK

	if( TRUE == bSuccess )
	{
		int iMaxMissionValue[MAX_FRIEND_INVITE_MISSION_INDEX] = {0,};
		FRIENDINVITE.GetMaxMissionValue(iMaxMissionValue);

		CPacketComposer Packet(S2C_FRIEND_INVITE_MISSION_INFO_RES);
		SS2C_FRIEND_INVITE_MISSION_INFO_RES MissionInfo;
		ZeroMemory(&MissionInfo, sizeof(SS2C_FRIEND_INVITE_MISSION_INFO_RES));

		BEGIN_SCOPE_LOCK(m_csFriendMission)
			auto iter = m_mapFriendMission.find(info.iFriendUserIDIndex);

			if( iter != m_mapFriendMission.end() )
			{
				for( int i = 0 ; i < MAX_FRIEND_INVITE_MISSION_INDEX; ++i )
				{
					MissionInfo.iMaxMissionValue[i] = iMaxMissionValue[i];
					MissionInfo.iMissionIndex[i] = iter->second.iMissionIndex[i];
					MissionInfo.iMissionValue[i] = iter->second.iMissionValue[i];
				}
			}
		END_SCOPE_LOCK

		Packet.Add((PBYTE)&MissionInfo, sizeof(SS2C_FRIEND_INVITE_MISSION_INFO_RES));
		m_pUser->Send(&Packet);
	}
}

BOOL CFSGameUserFriendInviteList::LoadFriendMissionList()
{
	CHECK_CONDITION_RETURN(TRUE == GetUserUpdate(FRIEND_INVITE_UPDATE_MISSIONLIST), TRUE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	list<SFRIENDINVITE_Mission> listMission;
	if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GetFriendMissionList(m_pUser->GetUserIDIndex(), listMission))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "FRIEND_INVITE_GetFriendMissionList UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	SetUserUpdate(FRIEND_INVITE_UPDATE_MISSIONLIST);

	BEGIN_SCOPE_LOCK(m_csFriendMission)
		m_mapFriendMission.clear();

		for( auto iter = listMission.begin() ; iter != listMission.end() ; ++iter )
		{
			m_mapFriendMission[(*iter).iUserIDIndex] = (*iter);
		}	
	END_SCOPE_LOCK

	return TRUE;
}

void CFSGameUserFriendInviteList::UpdateFriendComment( const SC2S_FRIEND_INVITE_UPDATE_COMMENT_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendMissionList());

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	SFRIENDINVITE_UpdateComment info;

	info.iUserIDIndex = m_pUser->GetUserIDIndex();
	strncpy_s(info.szComment, _countof(info.szComment), rq.szComment , MAX_FRIEND_INVITE_COMMENT_LENGTH);
	info.szComment[MAX_FRIEND_INVITE_COMMENT_LENGTH] = '\0';

	BEGIN_SCOPE_LOCK(m_csFriendlist)
		map<BYTE, SFRIENDINVITE_Friend>::iterator iter = m_mapFriend.find(rq.btIndex);
		CHECK_CONDITION_RETURN_VOID(iter == m_mapFriend.end());
		CHECK_CONDITION_RETURN_VOID(iter->second.btStatus == FRIEND_INVITE_FRIEND_STATUS_WAIT);
		info.iFriendUserIDIndex = iter->second.iFriendUserIDIndex;
	END_SCOPE_LOCK
	
	if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateComment(info))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "FRIEND_INVITE_UpdateComment UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rn;
	}

	BEGIN_SCOPE_LOCK(m_csFriendlist)
		map<BYTE, SFRIENDINVITE_Friend>::iterator iter = m_mapFriend.find(rq.btIndex);
		if( iter != m_mapFriend.end())
		{
			strncpy_s(iter->second.szComment, _countof(iter->second.szComment), rq.szComment , MAX_FRIEND_INVITE_COMMENT_LENGTH);
			iter->second.szComment[MAX_FRIEND_INVITE_COMMENT_LENGTH] = '\0';
		}
	END_SCOPE_LOCK

	SS2C_FRIEND_INVITE_UPDATE_COMMENT_RES rs;
	rs.btResult = TRUE;
	CPacketComposer Packet(S2C_FRIEND_INVITE_UPDATE_COMMENT_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_FRIEND_INVITE_UPDATE_COMMENT_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserFriendInviteList::SendFriendInviteRewardList( void )
{
	CPacketComposer Packet(S2C_FRIEND_INVITE_REWARD_INFO_RES);
	if( FAILED( FRIENDINVITE.MakePacketRewardList(Packet)))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, GET_DATA, FAIL, "FRIEND_INVITE_UpdateComment UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rn;
	}

	m_pUser->Send(&Packet);
}

void CFSGameUserFriendInviteList::SendMyMissionInfo( void )
{
	CHECK_CONDITION_RETURN_VOID(0 >= m_myInfo.iFriendUserIDIndex)
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadMyMissionList());
	
	SS2C_FRIEND_INVITE_MYMISSION_INFO_RES info;
				
	for( int i = 0 ; i < MAX_FRIEND_INVITE_MISSION_INDEX ; ++i )
	{
		info.iMissionIndex[i] = m_myMission.iMissionIndex[i];
		info.iMissionValue[i] = m_myMission.iMissionValue[i];
	}
	FRIENDINVITE.GetMaxMissionValue(info.iMaxMissionValue);

	info.btRewardButtonEnable = static_cast<BYTE>(FRIENDINVITE.CheckMissionAllSuccess(m_myMission.iMissionIndex, m_myMission.iMissionValue));
	if(TRUE == m_myInfo.iIsGiveReward)
		info.btRewardButtonEnable = FALSE;

	strncpy_s( info.szInviteGameID, _countof(info.szInviteGameID), m_myInfo.szInviteGameID , MAX_GAMEID_LENGTH+1 );
	info.tInviteDate = m_myInfo.tInviteTime;
	info.tLastConnectTime = difftime(_GetCurrentDBDate, m_myInfo.tLastConnectTime);
		
	CPacketComposer Packet(S2C_FRIEND_INVITE_MYMISSION_INFO_RES);
	Packet.Add((PBYTE)&info, sizeof(SS2C_FRIEND_INVITE_MYMISSION_INFO_RES));
	m_pUser->Send(&Packet);
}

BOOL CFSGameUserFriendInviteList::LoadMyMissionList()
{
	CHECK_CONDITION_RETURN(TRUE == GetUserUpdate(FRIEND_INVITE_UPDATE_MYMISSION), TRUE);
	
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	
	m_myMission.iUserIDIndex = m_pUser->GetUserIDIndex();
	if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GetMyMission(m_myMission))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, DB_DATA_LOAD, FAIL, "FRIEND_INVITE_GetMyMission UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	SetUserUpdate(FRIEND_INVITE_UPDATE_MYMISSION);

	UpdateFriendInviteMission(FRIEND_INVITE_MISSION_INDEX_LV_ATTAINMENT, m_pUser->GetMaxAvatarLevel());
	UpdateFriendInviteMission(FRIEND_INVITE_MISSION_INDEX_END_OF_TRAINING);

	return TRUE;
}

void CFSGameUserFriendInviteList::GiveFriendInviteReward( const SC2S_FRIEND_INVITE_REWARD_RECEIVING_REQ& rq )
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	SFRIENDINVITE_GiveReward give;
	give.iUserIDIndex = m_pUser->GetUserIDIndex();
	give.iGameIDIndex = m_pUser->GetGameIDIndex();
	give.iFriendUserIDIndex = m_myInfo.iFriendUserIDIndex;
	give.iRewardIndex = -1;
	give.iBenefitStatus = -1;
	give.iPresentIndex = 0;
	SS2C_FRIEND_INVITE_REWARD_RECEIVING_RES rs;
	rs.btResult = FRIEND_INVITE_REWARD_RECEVING_RESULT_FAIL;

	if( -1 == rq.iFriendUserIDIndex && TRUE == LoadMyMissionList())
	{// 내가 완료한 미션
		if( FALSE == m_myInfo.iIsGiveReward &&
			TRUE == FRIENDINVITE.CheckMissionAllSuccess(m_myMission.iMissionIndex, m_myMission.iMissionValue))
		{	// rewardmanager 의 item 과 같은 주소
			SRewardConfigItem* pRewardItem = FRIENDINVITE.GetFriendInviteRewardConfigItem(rq.iRewardIndex);
			if( nullptr != pRewardItem )
			{
				give.iFlag = FRIEND_INVITE_GIVE_FLAG_MY_REWARD;
				give.iRewardIndex = pRewardItem->iRewardIndex;	
			}
		}
	}
	else if (TRUE == LoadFriendMissionList())
	{// 나랑 친구맺은 사람이 완료한 미션
		SFRIENDINVITE_Mission missioninfo;
		missioninfo.Reset();
		BOOL bStatusCheck = FALSE;

		BEGIN_SCOPE_LOCK(m_csFriendlist)
			auto iter = m_mapFriend.find(rq.iFriendUserIDIndex);
		if( iter != m_mapFriend.end() )
		{
			if( FRIEND_INVITE_FRIEND_STATUS_FRIEND == iter->second.btStatus || 
				FRIEND_INVITE_FRIEND_STATUS_SUCCESS_MISSION == iter->second.btStatus)
			{
				give.iFriendUserIDIndex = iter->second.iFriendUserIDIndex;
				bStatusCheck = TRUE;
			}
		}
		END_SCOPE_LOCK
		
		if(TRUE == bStatusCheck)
		{
			BEGIN_SCOPE_LOCK(m_csFriendMission)
				auto iter = m_mapFriendMission.find(give.iFriendUserIDIndex);
			if( iter != m_mapFriendMission.end() )
			{
				missioninfo = iter->second;
			}
			END_SCOPE_LOCK

			if( 0 < missioninfo.iUserIDIndex &&
				TRUE == FRIENDINVITE.CheckMissionAllSuccess(missioninfo.iMissionIndex, missioninfo.iMissionValue))
			{
				SRewardConfigItem* pRewardItem = FRIENDINVITE.GetFriendInviteRewardConfigItem(rq.iRewardIndex);
				if( nullptr != pRewardItem )
				{
					give.iFlag = FRIEND_INVITE_GIVE_FLAG_FRIEND_REWARD;
					give.iRewardIndex = pRewardItem->iRewardIndex;	
				}
			}
		}
	}

	if( -1 < give.iRewardIndex )
	{
		if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
			rs.btResult = FRIEND_INVITE_REWARD_RECEVING_RESULT_MAILBOX_FULL;
		else if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GiveReward(give))
			WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_GiveReward UserIDIndex : 10752790", m_pUser->GetUserIDIndex());

			rs.btResult = FRIEND_INVITE_REWARD_RECEVING_RESULT_SUCCESS;
	}

	if(FRIEND_INVITE_REWARD_RECEVING_RESULT_SUCCESS == rs.btResult)
	{
		UpdateGaveReward(give, rq.iFriendUserIDIndex);

		if( 0 <= give.iPresentIndex )
			m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, give.iPresentIndex);
	}
	CPacketComposer Packet(S2C_FRIEND_INVITE_REWARD_RECEIVING_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_FRIEND_INVITE_REWARD_RECEIVING_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateGaveReward(const SFRIENDINVITE_GiveReward& give, IN const BYTE& btFriendIndex)
{
	if( FRIEND_INVITE_GIVE_FLAG_MY_REWARD == give.iFlag )
	{
		m_myInfo.iIsGiveReward = TRUE; 
	}
	else if( FRIEND_INVITE_GIVE_FLAG_FRIEND_REWARD == give.iFlag )
	{
		BEGIN_SCOPE_LOCK(m_csFriendlist)
			auto iter = m_mapFriend.find(btFriendIndex);

			if(iter != m_mapFriend.end())
			{
				iter->second.btStatus = FRIEND_INVITE_FRIEND_STATUS_REWARD_FINISH;
			}
		END_SCOPE_LOCK
	}
}

void CFSGameUserFriendInviteList::UpdateFriendStatus( const SC2S_FRIEND_INVITE_UPDATE_FRIEND_STATUS_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendList());
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);
	
	CPacketComposer Packet(S2C_FRIEND_INVITE_UPDATE_FRIEND_STATUS_RES);
	SS2C_FRIEND_INVITE_UPDATE_FRIEND_STATUS_RES rs;
	rs.btRequest = rq.btRequest;
	rs.btResult = FRIEND_INVITE_UPDATE_FAIL;

	SFRIENDINVITE_UpdateFriendStatus sUpdate;
	sUpdate.iUserIDIndex = m_pUser->GetUserIDIndex();
	sUpdate.iFriendUserIDIndex = -1;
	sUpdate.iStatus = static_cast<int>(rq.btRequest);
	int iPrevStatus = -1;

	const int iMaxFriendListCount = FRIENDINVITE.GetConfigValue(FIC_MAX_FRIEND_CNT, FP1);
	const int iMaxWaitListCount = FRIENDINVITE.GetConfigValue(FIC_MAX_FRIEND_CNT, FP2);

	if(FRIEND_UPDATE_ACCEPT == rq.btRequest)
	{
		BEGIN_SCOPE_LOCK(m_csFriendlist)
			if( iMaxFriendListCount > GetFriendCount(static_cast<BYTE>(FRIEND_INVITE_FRIEND_STATUS_WAIT), FIFI_NOT_IN))
			{
				auto iter = m_mapFriend.find(rq.btIndex);
				if( iter != m_mapFriend.end() )
				{
					if(FRIEND_INVITE_FRIEND_STATUS_WAIT == iter->second.btStatus) // 대기중인 친구만 친구가 될 수 있다.
					{
						sUpdate.iFriendUserIDIndex = iter->second.iFriendUserIDIndex;
						iPrevStatus = iter->second.btStatus;
						iter->second.btStatus = FRIEND_INVITE_FRIEND_STATUS_FRIEND;
					}
				}
			}
			else
			{
				rs.btResult = FRIEND_INVITE_UPDATE_FRIENDLIST_IS_FULL;
			}
		END_SCOPE_LOCK
	}
	else if(FRIEND_UPDATE_REFUSAL == rq.btRequest)
	{
		BEGIN_SCOPE_LOCK(m_csFriendlist)
			auto iter = m_mapFriend.find(rq.btIndex);
		if( iter != m_mapFriend.end() )
		{
			if(FRIEND_INVITE_FRIEND_STATUS_WAIT == iter->second.btStatus) // 대기중인 친구만 거절 가능
			{
				sUpdate.iFriendUserIDIndex = iter->second.iFriendUserIDIndex;
				iPrevStatus = iter->second.btStatus;
				iter->second.btStatus = FRIEND_INVITE_FRIEND_STATUS_REFUSAL;
			}
		}
		END_SCOPE_LOCK
	}
	else if(FRIEND_UPDATE_DELETE == rq.btRequest)
	{
		int iDeleteUserIDIndex = -1;
		BOOL bDelete = FALSE;
		BOOL bExpire = FALSE;
		BEGIN_SCOPE_LOCK(m_csFriendlist)
			auto iter = m_mapFriend.find(rq.btIndex);
			if( iter != m_mapFriend.end() )
			{
				iDeleteUserIDIndex = iter->second.iFriendUserIDIndex;

				bExpire = FRIENDINVITE.CheckDeleteExpireDate(iter->second.tLastConnectTime);
				if(FRIEND_INVITE_FRIEND_STATUS_FRIEND == iter->second.btStatus &&
					TRUE == bExpire) // 친구상태이고 // 현재보다 n지났을때
				{
					sUpdate.iFriendUserIDIndex = iter->second.iFriendUserIDIndex;
					iPrevStatus = iter->second.btStatus;
					bDelete = TRUE; 
				}
			}
		END_SCOPE_LOCK

		if(-1 < iDeleteUserIDIndex && FALSE == bDelete) // 친구가 1년이 안되었으면 미션완료 인지 체크
		{
			BEGIN_SCOPE_LOCK(m_csFriendMission)
				auto iter = m_mapFriendMission.find(iDeleteUserIDIndex);
				if( iter != m_mapFriendMission.end() )
				{
					if( FRIENDINVITE.CheckMissionAllSuccess(iter->second.iMissionIndex, iter->second.iMissionValue))
					{
						bDelete = TRUE;
					}
				}
			END_SCOPE_LOCK
		}

		if(bDelete)
		{
			sUpdate.iFriendUserIDIndex = iDeleteUserIDIndex;
			rs.btResult = DeleteFriend(sUpdate, rq.btIndex);
		}
		else
		{
			rs.btResult = FRIEND_INVITE_UPDATE_DELETE_FAIL;
		}
	}

	if( -1 < sUpdate.iFriendUserIDIndex && (FRIEND_UPDATE_ACCEPT == rq.btRequest || FRIEND_UPDATE_REFUSAL == rq.btRequest) )
	{
		if(ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateFriendStatus(sUpdate))
		{
			WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateFriendStatus (1) UserIDIndex : 10752790, Status : 0"
 m_pUser->GetUserIDIndex(), sUpdate.iStatus);

			UpdateFriendStatus(rq.btIndex, iPrevStatus); // rollback
		}
		else
		{
			SendCenterSvrUpdateInviteStatus(sUpdate);
			rs.btResult = FRIEND_INVITE_UPDATE_SUCCESS;

			if(FRIEND_UPDATE_ACCEPT == rq.btRequest)
			{
				UnSetUserUpdate(FRIEND_INVITE_UPDATE_MISSIONLIST);
			}
			else if(FRIEND_UPDATE_REFUSAL == rq.btRequest)
			{
				BEGIN_SCOPE_LOCK(m_csFriendlist)
					m_mapFriend.erase(rq.btIndex);
				END_SCOPE_LOCK
			}
		}
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_FRIEND_INVITE_UPDATE_FRIEND_STATUS_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateFriendStatus( BYTE btIndex, int iStatus )
{
	CLock lock(&m_csFriendlist);

	auto iter = m_mapFriend.find(btIndex);
	if(iter != m_mapFriend.end())
	{
		iter->second.btStatus = iStatus;
	}
}

void CFSGameUserFriendInviteList::SendCenterSvrUpdateInviteStatus(const SFRIENDINVITE_UpdateFriendStatus& sUpdate)
{
	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	CPacketComposer Packet(G2S_FRIEND_INVITE_UPDATE_STATUS_FRIEND_REQ);
	SG2S_FRIEND_INVITE_UPDATE_STATUS_FRIEND_REQ info;
	
	info.iUserIDIndex = sUpdate.iUserIDIndex;
	info.iFriendUserIDIndex = sUpdate.iFriendUserIDIndex;
	info.iStatus = sUpdate.iStatus;

	const SREPRESENT_INFO* pRePresent = m_pUser->GetRepresentInfo();
	if( pRePresent != nullptr )
		strncpy_s(info.szGameID, _countof(info.szGameID), pRePresent->szGameID, MAX_GAMEID_LENGTH+1);
	else
		ZeroMemory(info.szGameID, _countof(info.szGameID));

	Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_INVITE_UPDATE_STATUS_FRIEND_REQ));

	pProxy->Send(&Packet);
}

int CFSGameUserFriendInviteList::DeleteFriend( const SFRIENDINVITE_UpdateFriendStatus& sUpdate , IN const BYTE& btIndex)
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(nullptr == pODBCBase,FRIEND_INVITE_UPDATE_FAIL);
	
	SFRIENDINVITE_DelFriend sDelete;
	sDelete.iUserIDIndex = m_pUser->GetUserIDIndex();
	sDelete.iFriendUserIDIndex = sUpdate.iFriendUserIDIndex;

	if(ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_DelFriend(sDelete))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_DelFriend UserIDIndex : 10752790, FriendUserIDIndex : 0", m_pUser->GetUserIDIndex(), sDelete.iFriendUserIDIndex);
urn FRIEND_INVITE_UPDATE_FAIL;
	}

	BEGIN_SCOPE_LOCK(m_csFriendlist)
		m_mapFriend.erase(btIndex);
	END_SCOPE_LOCK

	BEGIN_SCOPE_LOCK(m_csFriendlist)
		m_mapFriendMission.erase(sUpdate.iFriendUserIDIndex);
	END_SCOPE_LOCK

	SendCenterSvrUpdateInviteStatus(sUpdate);
	
	return FRIEND_INVITE_UPDATE_SUCCESS;
}

void CFSGameUserFriendInviteList::UpdateInviteStatus( IN int iUserIDIndex, IN int iFriendUserIDIndex, IN int iStatus , IN char* szGameID)
{
	// useridindex= 나를 초대한 사람
	// frienduseridindex = 나
	if( FRIEND_UPDATE_ACCEPT == iStatus )
	{
		//m_myInfo.iFriendUserIDIndex = iUserIDIndex;
		//m_myInfo.tInviteTime = _GetCurrentDBDate;
		UnSetUserUpdate(FRIEND_INVITE_UPDATE_MYINFO);
		UnSetUserUpdate(FRIEND_INVITE_UPDATE_MYMISSION);

		SendFriendInviteMessageNot(FRIEND_INVITE_MESSAGE_INDEX_INVITE_SUCCESS, szGameID);

		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);

		SFRIENDINVITE_UpdateMyInfoStatus updatestatus;
		updatestatus.iUserIDIndex = m_pUser->GetUserIDIndex();
		m_myInfo.iIsDeleteFriend = FRIEND_INVITE_DELETEFRIEND_STATUS_NONE;
		updatestatus.iDeleteFriend = FRIEND_INVITE_DELETEFRIEND_STATUS_NONE;
		updatestatus.iBenefitStatus = -1;

		if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateMyInfoStatus(updatestatus))
		{
			WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateMyInfoStatus UserIDIndex : 10752790", m_pUser->GetUserIDIndex());

	SendFriendInviteRegisterButtonNot();
	}
	else if( FRIEND_UPDATE_REFUSAL == iStatus )
	{
		SendFriendInviteMessageNot(FRIEND_INVITE_MESSAGE_INDEX_INVITE_REFUSAL);

		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);

		SFRIENDINVITE_UpdateMyInfoStatus updatestatus;
		updatestatus.iUserIDIndex = m_pUser->GetUserIDIndex();
		m_myInfo.iIsDeleteFriend = FRIEND_INVITE_DELETEFRIEND_STATUS_NONE;
		updatestatus.iDeleteFriend = FRIEND_INVITE_DELETEFRIEND_STATUS_NONE;
		updatestatus.iBenefitStatus = -1;

		if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateMyInfoStatus(updatestatus))
		{
			WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateMyInfoStatus UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
}
	else if( FRIEND_UPDATE_DELETE == iStatus )
	{
		m_myInfo.iFriendUserIDIndex = -1;

		SetUserUpdate(FRIEND_INVITE_UPDATE_MYINFO);
		SetUserUpdate(FRIEND_INVITE_UPDATE_MYMISSION);

		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(m_pUser->GetMatchLocation());
		if(nullptr != pMatch)
		{
			SG2M_FRIENDINVITE_DELETE_FRIEND_NOT not;
			CPacketComposer Packet(G2M_FRIENDINVITE_DELETE_FRIEND_NOT);
			not.iGameIDIndex = m_pUser->GetGameIDIndex();
			not.btServerIndex = _GetServerIndex;
			Packet.Add((PBYTE)&not,sizeof(SG2M_FRIENDINVITE_DELETE_FRIEND_NOT));
			pMatch->Send(&Packet);
		}
	}
}

void CFSGameUserFriendInviteList::SendFriendInviteMessageNot( const SS2C_FRIEND_INVITE_MESSAGE_NOT& not)
{
	if(m_pUser->IsPlaying())
	{
		CPacketComposer* sp = new CPacketComposer(S2C_FRIEND_INVITE_MESSAGE_NOT);
		sp->Add((PBYTE)&not, sizeof(SS2C_FRIEND_INVITE_MESSAGE_NOT));
		m_pUser->PushPacketQueue(sp);
	}
	else
	{
		CPacketComposer Packet(S2C_FRIEND_INVITE_MESSAGE_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2C_FRIEND_INVITE_MESSAGE_NOT));
		m_pUser->Send(&Packet);
	}
}

void CFSGameUserFriendInviteList::SendFriendInviteMessageNot( FRIEND_INVITE_MESSAGE_INDEX index , char* szGameID /*= nullptr*/)
{
	SS2C_FRIEND_INVITE_MESSAGE_NOT not;
	not.btnotIndex = static_cast<BYTE>(index);
	if( nullptr == szGameID )
		ZeroMemory(not.szInviteGameID, _countof(not.szInviteGameID));
	else
		strncpy_s(not.szInviteGameID, _countof(not.szInviteGameID), szGameID, MAX_GAMEID_LENGTH);

	SendFriendInviteMessageNot(not);
}

void CFSGameUserFriendInviteList::AddInviteFriend( const SC2S_FRIEND_INVITE_ADD_FRIEND_REQ& rq )
{
	CHECK_NULL_POINTER_VOID(rq.szInviteCode);
	CHECK_CONDITION_RETURN_VOID(0 >= strlen(rq.szInviteCode));
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteMyInfo());

	time_t tCreateAccount = m_pUser->GetAccountCreateTime();
	time_t tCurrentDate = _GetCurrentDBDate;
	int iCreateConditionValue = FRIENDINVITE.GetConfigValue(FIC_INVITE_NEWACCOUNT_CONDITION_DD, FP0);
	int iDiffDate = abs(difftime(tCreateAccount, tCurrentDate) / 86400)/*(24*60*60)*/;
	CHECK_CONDITION_RETURN_VOID(iCreateConditionValue < iDiffDate);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	SFRIENDINVITE_AddFriend sAdd;
	sAdd.iUserIDIndex = m_pUser->GetUserIDIndex();
	sprintf_s(sAdd.szInviteCode, _countof(sAdd.szInviteCode), rq.szInviteCode, MAX_FRIEND_INVITE_CODE_LENGTH+1);
	sAdd.iFriendUserIDIndex = -1;
	sAdd.iDuplicateReg = (TRUE == rq.btUpdate ? TRUE : FALSE);
	sAdd.iRefusalUserIDIndex = 0;
	SS2C_FRIEND_INVITE_ADD_FRIEND_RES rs;
	sprintf_s(rs.szGameID, "");

	if( 0 == ::strcmp(rq.szInviteCode,m_myInfo.szInviteCode))
	{
		rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_FAIL;
	}
	else
	{
		int iResult = pODBCBase->FRIEND_INVITE_AddFriend(sAdd);
		rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_FAIL;
		ZeroMemory(rs.szGameID, _countof(rs.szGameID));

		if(ODBC_RETURN_SUCCESS == iResult)
			rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_SUCCESS;
		else if(FRIEND_INVITE_ADD_ERROR_INDEX_CODE_FAIL == iResult) // 초대코드이상
			rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_FAIL;
		else if(FRIEND_INVITE_ADD_ERROR_INDEX_DUPLICATE_REG == iResult) //다른곳에 등록되어있음
		{
			rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_DUPLICATE_REG_UPDATE;

			if( nullptr != sAdd.szErrorGameID )
				strncpy_s(rs.szGameID,_countof(rs.szGameID), sAdd.szErrorGameID, MAX_GAMEID_LENGTH+1);
		}
		else if(FRIEND_INVITE_ADD_ERROR_INDEX_FULL_FRIEND_LIST == iResult ||
			FRIEND_INVITE_ADD_ERROR_INDEX_FULL_WAIT_LIST == iResult) // 친구 리스트 꽉 참
			rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_FULL_FRIENDLIST;
		else if(FRIEND_INVITE_ADD_ERROR_INDEX_DEADLOCK == iResult)
			rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_DEADLOCK;
		else if(FRIEND_INVITE_ADD_ERROR_INDEX_PERMISSION == iResult)
			rs.btResult = FRIEND_INVITE_ADD_FRIEND_RESULT_PERMISSION;
	}

	CPacketComposer Packet(S2C_FRIEND_INVITE_ADD_FRIEND_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_FRIEND_INVITE_ADD_FRIEND_RES));
	m_pUser->Send(&Packet);

	//SendCetnerSvrAddFriendAndCheck(rq);

	/*if( FRIEND_INVITE_ADD_FRIEND_RESULT_SUCCESS == rs.btResult && 0 < sAdd.iRefusalUserIDIndex)
	{
		SendCenterSvrRefusal(sAdd.iUserIDIndex, sAdd.iRefusalUserIDIndex);
	}
	else*/ if( FRIEND_INVITE_ADD_FRIEND_RESULT_SUCCESS == rs.btResult )
	{
		SendCenterSvrAddFriend(sAdd);

		if( TRUE == sAdd.iDuplicateReg )
			SendCenterSvrRemoveFriendList(sAdd); //기존의 유저를 삭제해줘야 한다.
	}
}

void CFSGameUserFriendInviteList::CheckAndSendFriendInviteCode( const SC2S_FRIEND_INVITE_CHECK_INVITE_CODE_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteMyInfo());
	CHECK_NULL_POINTER_VOID(rq.szInviteCode);
	CHECK_CONDITION_RETURN_VOID(0 >= strlen(rq.szInviteCode));

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	SFRIENDINVITE_CheckCode check;
	check.iUserIDIndex = m_pUser->GetUserIDIndex();
	sprintf_s(check.szGameID, _countof(check.szGameID), "");
	sprintf_s(check.szInviteCode, _countof(check.szInviteCode), rq.szInviteCode, MAX_FRIEND_INVITE_CODE_LENGTH+1);

	CPacketComposer Packet(S2C_FRIEND_INVITE_CHECK_INVITE_CODE_RES);
	SS2C_FRIEND_INVITE_CHECK_INVITE_CODE_RES rs;
	rs.btResult = FRIEND_INVITE_CHECK_CODE_FAIL;

	if( 0 < m_myInfo.iFriendUserIDIndex )
	{
		rs.btResult = FRIEND_INVITE_CHECK_CODE_FAIL;
	}
	else if(0 == ::strcmp(check.szInviteCode, m_myInfo.szInviteCode))
	{
		rs.btResult = FRIEND_INVITE_CHECK_CODE_COMPARE_MYCODE;
	}
	else if(ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_CheckInviteCode(check))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_DelFriend UserIDIndex : 10752790, inviteCode : (null)", m_pUser->GetUserIDIndex(), check.szInviteCode);

}

	if( nullptr != check.szGameID && 0 < strlen(check.szGameID) )
	{
		rs.btResult = FRIEND_INVITE_CHECK_CODE_SUCCESS;
		sprintf_s(rs.szGameID, _countof(rs.szGameID), check.szGameID, MAX_GAMEID_LENGTH+1);
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_FRIEND_INVITE_CHECK_INVITE_CODE_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserFriendInviteList::SendCenterSvrRefusal( int iUserIDIndex, int iRefusalUserIDIndex )
{
	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	SG2S_FRIEND_INVITE_REFUSAL_REQ info;

	info.iUserIDIndex = iUserIDIndex;
	info.iRefusalUserIDIndex = iRefusalUserIDIndex;

	CPacketComposer Packet(G2S_FRIEND_INVITE_REFUSAL_REQ);
	Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_INVITE_REFUSAL_REQ));

	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::RefusalUser( IN int iRefusalUserIDIndex,IN int iUserIDIndex )
{
	BEGIN_SCOPE_LOCK(m_csFriendlist)
		auto iter = m_mapFriend.begin();
		for( ; iter != m_mapFriend.end() ; ++iter )
		{
			if( iter->second.iFriendUserIDIndex == iUserIDIndex )
			{
				break;
			}
		}

		if( iter != m_mapFriend.end())
		{
			m_mapFriend.erase(iter);
		}
	END_SCOPE_LOCK
}

int CFSGameUserFriendInviteList::GetFriendUserIDIndex( void )
{
	if(FALSE == LoadFriendInviteMyInfo())
		return -1;

	return m_myInfo.iFriendUserIDIndex;
}

void CFSGameUserFriendInviteList::CheckAndGiveFriendBenefit( IN int iType )
{
	if(FRIEND_INVITE_BENEFIT_INDEX_VVIP == iType)
	{
		if( 0 < m_myInfo.iFriendUserIDIndex && 
			(m_myInfo.btBenefitStatus == FRIEND_INVITE_BENEFIT_STATUS_NONE || 
			m_myInfo.btBenefitStatus == FRIEND_INVITE_BENEFIT_STATUS_POINT_ONLY) )
		{
			CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteMyInfo());

			CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID(pODBCBase);
			
			SFRIENDINVITE_GiveReward give;
			// ***** 초대받은 친구가 받는것임. *****
			give.iUserIDIndex = m_myInfo.iFriendUserIDIndex;
			give.iFriendUserIDIndex = m_pUser->GetUserIDIndex();

			give.iFlag = FRIEND_INVITE_GIVE_FLAG_BENEFIT;
			give.iGameIDIndex = m_pUser->GetUserIDIndex();
			give.iRewardIndex = FRIENDINVITE.GetConfigValue(FIC_BONUS_VVIP, FP1);
			
			if( FRIEND_INVITE_BENEFIT_STATUS_POINT_ONLY == m_myInfo.btBenefitStatus )
				give.iBenefitStatus = FRIEND_INVITE_BENEFIT_STATUS_ALL;
			else
				give.iBenefitStatus = FRIEND_INVITE_BENEFIT_STATUS_VVIP_ONLY;

			give.iPresentIndex = 0;

			SFRIEND_INVITE_UpdateUserBenefit benefit;
			benefit.iBenefitStatus = give.iBenefitStatus;
			benefit.iFriendUserIDIndex = m_myInfo.iFriendUserIDIndex;
			benefit.iRewardIndex = give.iRewardIndex;
			benefit.iType = FRIEND_INVITE_UPDATE_USER_BENEFIT_DB_TYPE_UPDATE;
			benefit.iUserIDIndex = m_pUser->GetUserIDIndex();

			// 한국은 예약발송
			if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateUserBenefit(benefit))
			{
				WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateUserBenefit UserIDIndex : 10752790, benefitstatus :0", m_pUser->GetUserIDIndex(), give.iBenefitStatus);
eturn;
			}

			m_myInfo.btBenefitStatus = give.iBenefitStatus;

// 			if(ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GiveReward(give))
// 			{
// 				WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_GiveReward UserIDIndex : 10752790, benefitstatus :0", m_pUser->GetUserIDIndex(), give.iBenefitStatus);
		return;
// 			}
// 
// 			if( 0 <= give.iPresentIndex )
// 				SendCenterSvrGaveBenefitReward(iType, give.iPresentIndex, give.iUserIDIndex);
		}
	}
	else if(FRIEND_INVITE_BENEFIT_INDEX_POINT == iType)
	{
		if( 0 < m_myInfo.iFriendUserIDIndex && 
			(m_myInfo.btBenefitStatus == FRIEND_INVITE_BENEFIT_STATUS_NONE || 
			m_myInfo.btBenefitStatus == FRIEND_INVITE_BENEFIT_STATUS_VVIP_ONLY) )
		{
			CHECK_CONDITION_RETURN_VOID(FALSE == LoadMyMissionList());

			int iConditionValue = FRIENDINVITE.GetConfigValue(FIC_BONUSPOINT_PLAYCOUNT, FP0);
			CHECK_CONDITION_RETURN_VOID(m_myMission.iMissionValue[FRIEND_INVITE_MISSION_INDEX_GAMEPLAY_RACE] < iConditionValue)
			
			CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID(pODBCBase);
						
			SFRIENDINVITE_GiveReward give;
			// ***** 초대받은 친구가 받는것임. *****
			give.iUserIDIndex = m_pUser->GetUserIDIndex();
			give.iFriendUserIDIndex = m_myInfo.iFriendUserIDIndex;

			give.iFlag = FRIEND_INVITE_GIVE_FLAG_BENEFIT;
			give.iGameIDIndex = m_pUser->GetUserIDIndex();
			give.iRewardIndex = FRIENDINVITE.GetConfigValue(FIC_BONUSPOINT_PLAYCOUNT, FP1);

			if( FRIEND_INVITE_BENEFIT_STATUS_VVIP_ONLY == m_myInfo.btBenefitStatus )
				give.iBenefitStatus = FRIEND_INVITE_BENEFIT_STATUS_ALL;
			else
				give.iBenefitStatus = FRIEND_INVITE_BENEFIT_STATUS_POINT_ONLY;

			give.iPresentIndex = 0;

			// 둘다 줘야함
			for( int i = 0 ; i < 2 ; ++i )
			{
				if( i == 1 )
				{
					give.iUserIDIndex = m_myInfo.iFriendUserIDIndex;
					give.iFriendUserIDIndex = m_pUser->GetUserIDIndex();
				}
				if(ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_GiveReward(give))
				{
					WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_GiveReward (point) UserIDIndex : 10752790, benefitstatus :0, index:7106560", m_pUser->GetUserIDIndex(), give.iBenefitStatus, i);
n;
				}

				if( 0 == i && 0 <= give.iPresentIndex )
					m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, give.iPresentIndex);
			}
			// 상대것			
			if( 0 <= give.iPresentIndex )
				SendCenterSvrGaveBenefitReward(iType, give.iPresentIndex, give.iUserIDIndex);

			m_myInfo.btBenefitStatus = give.iBenefitStatus;
		}
	}
}

void CFSGameUserFriendInviteList::UpdateFriendInviteMission( IN int iMissionIndex, int iValue /*= 1*/ )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteMyInfo());
	CHECK_CONDITION_RETURN_VOID(1 > m_myInfo.iFriendUserIDIndex);
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadMyMissionList());

	int iPrevMissionValue[MAX_FRIEND_INVITE_MISSION_INDEX];
	memcpy_s(iPrevMissionValue, sizeof(int)*MAX_FRIEND_INVITE_MISSION_INDEX, m_myMission.iMissionValue, sizeof(int)*MAX_FRIEND_INVITE_MISSION_INDEX);
	
	int iMaxMissionValue[MAX_FRIEND_INVITE_MISSION_INDEX] = {0};
	FRIENDINVITE.GetMaxMissionValue(iMaxMissionValue);

	if( FRIEND_INVITE_MISSION_INDEX_WITH_FRIEND_GAMEPLAY == iMissionIndex || 
		FRIEND_INVITE_MISSION_INDEX_GAMEPLAY_RACE == iMissionIndex || 
		FRIEND_INVITE_MISSION_INDEX_PLAYTIME == iMissionIndex )
	{
		if( m_myMission.iMissionValue[iMissionIndex] < iMaxMissionValue[iMissionIndex] )
			m_myMission.iMissionValue[iMissionIndex] += iValue;
		
		if(m_myMission.iMissionValue[iMissionIndex] > iMaxMissionValue[iMissionIndex])
			m_myMission.iMissionValue[iMissionIndex] = iMaxMissionValue[iMissionIndex];
	}
	else if( FRIEND_INVITE_MISSION_INDEX_LV_ATTAINMENT == iMissionIndex )
	{
		int iUserMaxLv = m_pUser->GetMaxAvatarLevel();
		if( m_myMission.iMissionValue[iMissionIndex] < iMaxMissionValue[iMissionIndex] )
			m_myMission.iMissionValue[iMissionIndex] = iUserMaxLv;
		
		if(m_myMission.iMissionValue[iMissionIndex] > iMaxMissionValue[iMissionIndex])
			m_myMission.iMissionValue[iMissionIndex] = iMaxMissionValue[iMissionIndex];
	}
	else if( FRIEND_INVITE_MISSION_INDEX_END_OF_TRAINING == iMissionIndex)
	{ // 왜 이것만 캐릭터 단위 미션이지?
		CHECK_CONDITION_RETURN_VOID(m_myMission.iMissionValue[iMissionIndex] == iMaxMissionValue[iMissionIndex]);

		SAvatarInfo* pAvatarInfo = m_pUser->GetCurUsedAvatar();
		CHECK_NULL_POINTER_VOID(pAvatarInfo);

		BOOL bFinish = TRUE;
		int iShift = FRIENDINVITE.GetConfigValue(FIC_TRAINING_FINISH_LV,FP0) -1;
		if( 0 > iShift ) iShift  = 0;
		int iTrainingStep = 1 << iShift;
		for (int iLoopIndex = STAT_TRAINING_CATEGOTY_RUN; iLoopIndex <= STAT_TRAINING_CATEGOTY_CLOSE; iLoopIndex++)
		{
			if ( 0 == (pAvatarInfo->Skill.iaTraining[iLoopIndex] & iTrainingStep) )
			{
				bFinish = FALSE;
				break;
			}
		}

		if( TRUE == bFinish )
		{
			m_myMission.iMissionValue[iMissionIndex] = iMaxMissionValue[iMissionIndex]; // 1이 success
		}
	}
	
	BOOL bUpdate = FALSE;
	for( int i = 0 ; i < MAX_FRIEND_INVITE_MISSION_INDEX ; ++i )
 	{
 		if( iPrevMissionValue[i] != m_myMission.iMissionValue[i] )
 		{
			bUpdate = TRUE;
			IncreaseMyMissionUpdateCount();
			break;
 		}
 	}

	if( TRUE ==bUpdate )
	{
		const int iUpdateMissionValue = FRIENDINVITE.GetConfigValue(FIC_UPDATE_MISSION_COUNT, FP0);
		const int iRealTime = FRIENDINVITE.GetConfigValue(FIC_REALTIME_UPDATE_MISSION, FP0);

		SetMissionSave();
		if(TRUE == iRealTime || iUpdateMissionValue == GetMyMissionUpdateCount())
		{
			UpdateAllMissionUpdate(dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE)));
		}

		SendCenterSvrFriendMission();

		if(TRUE == FRIENDINVITE.CheckMissionAllSuccess(m_myMission.iMissionIndex, m_myMission.iMissionValue))
		{
			CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID(pODBCBase);

			SFRIENDINVITE_UpdateFriendStatus updatestatus;
			updatestatus.iStatus = FRIEND_INVITE_FRIEND_STATUS_SUCCESS_MISSION;
			updatestatus.iFriendUserIDIndex = m_pUser->GetUserIDIndex();
			updatestatus.iUserIDIndex = m_myInfo.iFriendUserIDIndex;

			if(ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateFriendStatus(updatestatus)) // 미션 전부 성공시 친구리스트의 정보를 업데이트 시킨다.
			{
				WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateFriendStatus (mission success) UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
turn;
			}

			SendCenterSvrUpdateFriendListStatus(updatestatus);
			SendCenterSvrMissionSuccessFriend();
		}
	}
}

void CFSGameUserFriendInviteList::UpdateAllMissionUpdate(CFSODBCBase* pODBCBase /*= nullptr*/)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == GetMissionSave());

	if( nullptr == pODBCBase )
		pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));

	CHECK_NULL_POINTER_VOID(pODBCBase);

	SFRIENDINVITE_UpdateFriendMissionList missionlist;

	missionlist.iUserIDIndex = m_pUser->GetUserIDIndex();
	missionlist.iMissionStatus = FRIEND_INVITE_MISSION_STATUS_UPDATE;
	ZeroMemory(missionlist.szMissionData, MAX_FRIEND_INVITE_MISSION_DATA_LENGTH+1);
		
	if( TRUE == ConvertArrayIndexStr(missionlist.szMissionData, m_myMission.iMissionValue,MAX_FRIEND_INVITE_MISSION_INDEX, MAX_FRIEND_INVITE_MISSION_DATA_LENGTH))
	{
		if(ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateFriendMissionList(missionlist))
		{
			WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateFriendMissionList UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
urn;
		}
	}
	UnSetMissionSave();
}

void CFSGameUserFriendInviteList::SendCenterSvrFriendMission()
{
	CHECK_CONDITION_RETURN_VOID( 0 > m_myInfo.iFriendUserIDIndex );
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadMyMissionList());

	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	SG2S_FRIEND_INVITE_UPDATE_FRIEND_MISSION_REQ info;

	info.iUserIDIndex = m_myInfo.iFriendUserIDIndex;
	info.iUpdateTargetUserIDIndex = m_pUser->GetUserIDIndex();
	for( int i = 0 ; i < MAX_FRIEND_INVITE_MISSION_INDEX ; ++i )
		info.iMIssionValue[i] = m_myMission.iMissionValue[i]; 
	
	CPacketComposer Packet(G2S_FRIEND_INVITE_UPDATE_FRIEND_MISSION_REQ);
	Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_INVITE_UPDATE_FRIEND_MISSION_REQ));

	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateFriendMission( IN const SS2G_FRIEND_INVITE_UPDATE_FRIEND_MISSION_RES& rs )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendMissionList());

	BEGIN_SCOPE_LOCK(m_csFriendMission)
		auto iter = m_mapFriendMission.find(rs.iUpdateTargetUserIDIndex);
		if( iter != m_mapFriendMission.end() )
		{
			for( int i = 0 ; i < MAX_FRIEND_INVITE_MISSION_INDEX; ++i )
			{
				iter->second.iMissionValue[i] = rs.iMIssionValue[i];
			}
		}
	END_SCOPE_LOCK		
}

void CFSGameUserFriendInviteList::UpdateFriendInviteKingExp( int iExp )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_FRIENDINVITE_RANKING));
	CHECK_CONDITION_RETURN_VOID(TRUE == EVENTDATEMANAGER.IsView(EVENT_KIND_FRIENDINVITE_RANKING)); // 보여지는 기간이면 합산하지 않음
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteMyInfo());
	CHECK_CONDITION_RETURN_VOID(1 > m_myInfo.iFriendUserIDIndex);
	CHECK_CONDITION_RETURN_VOID(0 > iExp);

	GetEventInviteKing()->UpdateFriendInviteKingExp(m_myInfo.iFriendUserIDIndex, iExp, m_myInfo.szInviteGameID);
}

void CFSGameUserFriendInviteList::UpdateFriendInviteKingExp( IN const SS2G_FRIEND_KING_EVENT_UPDATE_EXP_RES& rs )
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_FRIENDINVITE_RANKING));
	CHECK_CONDITION_RETURN_VOID(TRUE == EVENTDATEMANAGER.IsView(EVENT_KIND_FRIENDINVITE_RANKING)); // 보여지는 기간이면 합산하지 않음
	CHECK_CONDITION_RETURN_VOID(0 > rs.iExp);
	CHECK_CONDITION_RETURN_VOID(m_pUser->GetUserIDIndex() != rs.iUserIDIndex);
	
	GetEventInviteKing()->UpdateFriendInviteKingExp(rs.iExp);
}

void CFSGameUserFriendInviteList::SendCenterSvrUpdateFriendListStatus( const SFRIENDINVITE_UpdateFriendStatus& sUpdate )
{
	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	CPacketComposer Packet(G2S_FRIEND_INVITE_UPDATE_STATUS_FRIEND_REQ);
	SG2S_FRIEND_INVITE_UPDATE_STATUS_FRIEND_REQ info;

	info.iUserIDIndex = sUpdate.iUserIDIndex;
	info.iFriendUserIDIndex = sUpdate.iFriendUserIDIndex;
	info.iStatus = sUpdate.iStatus;

	const SREPRESENT_INFO* pRePresent = m_pUser->GetRepresentInfo();
	if( pRePresent != nullptr )
		strncpy_s(info.szGameID, _countof(info.szGameID), pRePresent->szGameID, MAX_GAMEID_LENGTH+1);
	else
		ZeroMemory(info.szGameID, _countof(info.szGameID));

	Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_INVITE_UPDATE_STATUS_FRIEND_REQ));

	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateFriendListStatus( IN const SS2G_FRIEND_INVITE_UPDATE_FRIENDLIST_STATUS_RES& rs )
{
	CHECK_CONDITION_RETURN_VOID(m_pUser->GetUserIDIndex() != rs.iFriendUserIDIndex );

	BEGIN_SCOPE_LOCK(m_csFriendlist)
		for( auto iter = m_mapFriend.begin() ; iter != m_mapFriend.end() ; ++iter )
		{
			if( iter->second.iFriendUserIDIndex == rs.iUserIDIndex )
			{
				iter->second.btStatus = rs.iStatus;
				break;
			}
		}
	END_SCOPE_LOCK
}

void CFSGameUserFriendInviteList::SendCenterSvrGaveBenefitReward(int iType, int iPresentIndex, int iUserIDIndex)
{
	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	CPacketComposer Packet(G2S_FRIEND_INVITE_GIVE_BENEFIT_REQ);
	SG2S_FRIEND_INVITE_GIVE_BENEFIT_REQ info;

	info.iUserIDIndex = iUserIDIndex;
	info.iType = iType;
	info.iPresentIndex = iPresentIndex;

	Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_INVITE_GIVE_BENEFIT_REQ));
	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateGaveBenefitReward(IN const SS2G_FRIEND_INVITE_GIVE_BENEFIT_RES& rs)
{
	if(FRIEND_INVITE_BENEFIT_INDEX_VVIP == rs.iType || 
		FRIEND_INVITE_BENEFIT_INDEX_POINT == rs.iType)
	{
		m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, rs.iPresentIndex);
	}
}

void CFSGameUserFriendInviteList::SendCenterSvrAddFriend( const SFRIENDINVITE_AddFriend& sAdd )
{
	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	CPacketComposer Packet(G2S_FRIEND_INVITE_ADD_FRIEND_REQ);

	SG2S_FRIEND_INVITE_ADD_FRIEND_REQ info;
	info.iUserIDIndex = sAdd.iFriendUserIDIndex;
	info.iUpdateTargetUserIDIndex = sAdd.iUserIDIndex;

	Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_INVITE_ADD_FRIEND_REQ));
	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateFriendInviteAddFriend( IN const SS2G_FRIEND_INVITE_ADD_FRIEND_RES& rs )
{
	// reload friendlist
	UnSetUserUpdate(FRIEND_INVITE_UPDATE_FRIENDLIST);
}

void CFSGameUserFriendInviteList::CheckAndSendDeleteStatus()
{
	if( FRIEND_INVITE_DELETEFRIEND_STATUS_REFUSAL == m_myInfo.iIsDeleteFriend) // 거절당함
	{
		SendFriendInviteMessageNot(FRIEND_INVITE_MESSAGE_INDEX_INVITE_REFUSAL);
	}
	else if(FRIEND_INVITE_DELETEFRIEND_STATUS_ADDFRIEND == m_myInfo.iIsDeleteFriend)
	{
		SendFriendInviteMessageNot(FRIEND_INVITE_MESSAGE_INDEX_INVITE_SUCCESS, m_myInfo.szInviteGameID);
	}
	else {
		return; 
	}

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	SFRIENDINVITE_UpdateMyInfoStatus updatestatus;
	updatestatus.iUserIDIndex = m_pUser->GetUserIDIndex();
	m_myInfo.iIsDeleteFriend = FRIEND_INVITE_DELETEFRIEND_STATUS_NONE;
	updatestatus.iDeleteFriend = FRIEND_INVITE_DELETEFRIEND_STATUS_NONE;
	updatestatus.iBenefitStatus = -1;

	if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateMyInfoStatus(updatestatus))
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateMyInfoStatus UserIDIndex : 10752790", m_pUser->GetUserIDIndex());


oid CFSGameUserFriendInviteList::SendCenterSvrRemoveFriendList( const SFRIENDINVITE_AddFriend& sAdd )
{
	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	SG2S_FRIEND_INVITE_REMOVE_FRIENDLIST_REQ info;

	info.iUserIDIndex = sAdd.iRefusalUserIDIndex;
	info.iRemoveTargetUserIDIndex = sAdd.iUserIDIndex;

	CPacketComposer Packet(G2S_FRIEND_INVITE_REMOVE_FRIENDLIST_REQ);
	Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_INVITE_REMOVE_FRIENDLIST_REQ));
	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateFriendInvteRemoveFriendList( IN const SS2G_FRIEND_INVITE_REMOVE_FRIENDLIST_RES& rs )
{
	BEGIN_SCOPE_LOCK(m_csFriendlist)
		for( auto iter = m_mapFriend.begin() ; iter != m_mapFriend.end() ; ++iter ) 
		{
			if( iter->second.iFriendUserIDIndex == rs.iRemoveTargetUserIDIndex )
			{
				m_mapFriend.erase(iter);
				break;
			}
		}
		END_SCOPE_LOCK
}

void CFSGameUserFriendInviteList::SendCenterSvrLastConnectTimeNoticeForFriend(BOOL bLogin /*= FALSE*/)
{
	CHECK_CONDITION_RETURN_VOID(0 >= m_myInfo.iFriendUserIDIndex);

	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	SG2S_FRIEND_INVITE_LASTCONNECT_REQ rq;
	rq.iUserIDIndex = m_myInfo.iFriendUserIDIndex;
	rq.iFriendUserIDIndex = m_pUser->GetUserIDIndex();
	if( TRUE == bLogin )
	{
		rq.tLastConnectTime = 0; // 접속중
	}
	else
	{
		rq.tLastConnectTime = _GetCurrentDBDate;
	}

	CPacketComposer Packet(G2S_FRIEND_INVITE_LASTCONNECT_REQ);
	Packet.Add((PBYTE)&rq,sizeof(SG2S_FRIEND_INVITE_LASTCONNECT_REQ));
	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateLastConnectTimeFriend( IN const SS2G_FRIEND_INVITE_LASTCONNECT_RES& rs )
{
	BEGIN_SCOPE_LOCK(m_csFriendlist)
		for( auto iter = m_mapFriend.begin() ; iter != m_mapFriend.end() ; ++iter ) 
		{
			if( iter->second.iFriendUserIDIndex == rs.iFriendUserIDIndex )
			{
				iter->second.tLastConnectTime = rs.tLastConnectTime;
				break;
			}
		}
		END_SCOPE_LOCK
}

void CFSGameUserFriendInviteList::SendCenterSvrMissionSuccessFriend()
{
	CHECK_CONDITION_RETURN_VOID(0 >= m_myInfo.iFriendUserIDIndex);
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadMyMissionList());
	CHECK_CONDITION_RETURN_VOID(FALSE == FRIENDINVITE.CheckMissionAllSuccess(m_myMission.iMissionIndex, m_myMission.iMissionValue));

	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	SG2S_FRIEND_INVITE_MISSION_SUCCESS_REQ rq;
	rq.iUserIDIndex = m_myInfo.iFriendUserIDIndex;
	rq.iFriendUserIDIndex = m_pUser->GetUserIDIndex();

	CPacketComposer Packet(G2S_FRIEND_INVITE_MISSION_SUCCESS_REQ);
	Packet.Add((PBYTE)&rq,sizeof(SG2S_FRIEND_INVITE_MISSION_SUCCESS_REQ));
	pProxy->Send(&Packet);
}

void CFSGameUserFriendInviteList::UpdateMissionSuccessFriend( IN const SS2G_FRIEND_INVITE_MISSION_SUCCESS_RES& rs )
{
	BEGIN_SCOPE_LOCK(m_csFriendlist)
		for( auto iter = m_mapFriend.begin() ; iter != m_mapFriend.end() ; ++iter ) 
		{
			if( iter->second.iFriendUserIDIndex == rs.iFriendUserIDIndex )
			{
				iter->second.btStatus = FRIEND_INVITE_FRIEND_STATUS_SUCCESS_MISSION;
				break;
			}
		}
		END_SCOPE_LOCK
}

void CFSGameUserFriendInviteList::SendCenterSvrMyFriendListMissionReq()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendList());

	CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
	CHECK_NULL_POINTER_VOID(pProxy);

	SG2S_FRIEND_INVITE_FRIEND_MISSION_LIST_REQ rq;
	rq.iCount = GetFriendCount(static_cast<BYTE>(FRIEND_INVITE_FRIEND_STATUS_WAIT), FIFI_NOT_IN);

	vector<SFRIEND_INVITE_USER_ENTITY> vecFriend;
	BEGIN_SCOPE_LOCK(m_csFriendlist)
		SFRIEND_INVITE_USER_ENTITY info;
	info.iFriendUserIDIndex = m_pUser->GetUserIDIndex();
	for( auto iter = m_mapFriend.begin() ; iter != m_mapFriend.end() ; ++iter )
	{
		if(FRIEND_INVITE_FRIEND_STATUS_WAIT != iter->second.btStatus)
		{
			info.iUserIDIndex = iter->second.iFriendUserIDIndex;
			vecFriend.push_back(info);
		}
	}
	END_SCOPE_LOCK

	if( static_cast<size_t>(rq.iCount) == vecFriend.size())
	{
		CPacketComposer Packet(G2S_FRIEND_INVITE_FRIEND_MISSION_LIST_REQ);
		Packet.Add((PBYTE)&rq, sizeof(SG2S_FRIEND_INVITE_FRIEND_MISSION_LIST_REQ));

		for(size_t i = 0 ; i < vecFriend.size() ; ++i)
		{
			Packet.Add((PBYTE)&vecFriend[i], sizeof(SFRIEND_INVITE_USER_ENTITY));
		}

		pProxy->Send(&Packet);
	}
}

void CFSGameUserFriendInviteList::CheckAndGiveBenefit()
{
	CHECK_CONDITION_RETURN_VOID(true == m_listBenefit.empty());
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	time_t tConditionTime = _GetCurrentDBDate - (FRIENDINVITE.GetConfigValue(FIC_BENEFIT_VIP_CONDITION, FP0)*24*60*60);
	for( auto iter = m_listBenefit.begin() ; iter != m_listBenefit.end() ; ) 
	{
		if( (*iter).tBuyDate < tConditionTime )
		{
			SFRIEND_INVITE_UpdateUserBenefit benefit;
			benefit.iBenefitStatus = -1;
			benefit.iFriendUserIDIndex = (*iter).iFriendUserIDIndex;
			benefit.iRewardIndex = -1;
			benefit.iType = FRIEND_INVITE_UPDATE_USER_BENEFIT_DB_TYPE_DELETE;
			benefit.iUserIDIndex = m_pUser->GetUserIDIndex();

			if( ODBC_RETURN_SUCCESS != pODBCBase->FRIEND_INVITE_UpdateUserBenefit(benefit))
			{
				WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "FRIEND_INVITE_UpdateUserBenefit UserIDIndex : 10752790, RewardIndex : 0", m_pUser->GetUserIDIndex(), (*iter).iRewardIndex);

		else
			{
				if( nullptr == REWARDMANAGER.GiveReward((CUser*)m_pUser,(*iter).iRewardIndex))
				{
					WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "REWARDMANAGER.GiveReward((*iter).iRewardIndex) UserIDIndex : 10752790, RewardIndex : 0", m_pUser->GetUserIDIndex(), (*iter).iRewardIndex);

			}

			iter = m_listBenefit.erase(iter);
		}
		else
		{
			++iter;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
////////////	user		//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

CEVENT_FriendInviteKingUserInfo::CEVENT_FriendInviteKingUserInfo( CFSGameUser* pUser )
	:m_bLoad(FALSE)
	,m_pUser(pUser)
{
	m_MyInfo.Init();
}

CEVENT_FriendInviteKingUserInfo::~CEVENT_FriendInviteKingUserInfo( void )
{

}

BOOL CEVENT_FriendInviteKingUserInfo::LoadFriendInviteKingInfo()
{
	CHECK_CONDITION_RETURN(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_FRIENDINVITE_RANKING), TRUE );
	CHECK_NULL_POINTER_BOOL(m_pUser);
	CHECK_CONDITION_RETURN(TRUE == m_bLoad, TRUE);
	CHECK_CONDITION_RETURN(FALSE == m_pUser->LoadRepresentInfo(), FALSE);

	const SREPRESENT_INFO* pRepresent = m_pUser->GetRepresentInfo();
	CHECK_NULL_POINTER_BOOL(pRepresent);

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_NULL_POINTER_BOOL(pEventODBC);
	
	m_MyInfo.btServerIndex = _GetServerIndex;
	m_MyInfo.iRank = -1;
	m_MyInfo.iUserIDIndex = m_pUser->GetUserIDIndex();
	strncpy_s(m_MyInfo.szGameID,_countof(m_MyInfo.szGameID), pRepresent->szGameID,MAX_GAMEID_LENGTH+1);
	m_MyInfo.iTotalExp = 0;

	if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_FRIEND_KING_GetUserInfo(m_pUser->GetUserIDIndex(), m_MyInfo.iTotalExp)) // 불러올땐 내정보
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "EVENT_FRIEND_KING_GetUserInfo UserIDIndex : 10752790", m_pUser->GetUserIDIndex());
rn FALSE;
	}
	else
		m_bLoad = TRUE;

	
	return TRUE;
}

void CEVENT_FriendInviteKingUserInfo::UpdateFriendInviteKingExp( IN int iFriendUserIDIndex, IN int iExp, IN char* szGameID )
{
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteKingInfo());
	CHECK_CONDITION_RETURN_VOID(1 >= iFriendUserIDIndex || 0 > iExp || nullptr == szGameID);

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_NULL_POINTER_VOID(pEventODBC);
		
	int iTotalExp = 0;
	if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_FRIEND_KING_UpdateExp(iFriendUserIDIndex, iExp, iTotalExp)) // 저장할땐 내가 초대한 친구의 경험치 합산이 나옴.
	{
		WRITE_LOG_NEW(LOG_TYPE_FRIEND_INVITE, CALL_SP, FAIL, "EVENT_FRIEND_KING_UpdateExp UserIDIndex : 10752790, Exp: 0", m_pUser->GetUserIDIndex(), iExp);
urn;
	}

	//FRIENDINVITEKING.UpdateInviteKingExp(iTotalExp);

	{
		CODBCSvrProxy* pProxy = dynamic_cast<CODBCSvrProxy*>(GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY));
		CHECK_NULL_POINTER_VOID(pProxy);

		SS2O_EVENT_FRIENDINVITE_UPDATE_EXP_NOT not;
		not.btServerIndex = _GetServerIndex;
		not.iTotalExp = iTotalExp;
		not.iUserIDIndex = iFriendUserIDIndex;
		strncpy_s(not.szGameID, _countof(not.szGameID), szGameID, MAX_GAMEID_LENGTH);
		not.szGameID[MAX_GAMEID_LENGTH] = '\0';
	
		CPacketComposer Packet(S2O_EVENT_FRIENDINVITE_UPDATE_EXP_NOT);
		Packet.Add((PBYTE)&not, sizeof(SS2O_EVENT_FRIENDINVITE_UPDATE_EXP_NOT));

		pProxy->Send(&Packet);
	}

	{
		CCenterSvrProxy* pProxy = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
		CHECK_NULL_POINTER_VOID(pProxy);

		CPacketComposer Packet(G2S_FRIEND_KING_EVENT_UPDATE_EXP_REQ);
		SG2S_FRIEND_KING_EVENT_UPDATE_EXP_REQ info;
		
		info.iUserIDIndex = iFriendUserIDIndex;
		info.iExp = iTotalExp;
		
		Packet.Add((PBYTE)&info, sizeof(SG2S_FRIEND_KING_EVENT_UPDATE_EXP_REQ));
		pProxy->Send(&Packet);
	}
}

void CEVENT_FriendInviteKingUserInfo::UpdateFriendInviteKingExp( IN int iExp )
{
	// no thread safe.
	CHECK_CONDITION_RETURN_VOID(FALSE == LoadFriendInviteKingInfo());
	m_MyInfo.iTotalExp = iExp;
}
