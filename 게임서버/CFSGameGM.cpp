// CFSGameGM.cpp: implementation of the CFSGameThread class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "TTFSGameSvr.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameODBC.h"
qinclude "CFSGameClient.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "CFSRankManager.h"
qinclude "CFSAvatarFeature.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSTrainingShop.h"
qinclude "CFSSkillShop.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameDBCfg.h"
qinclude "CFSFileLog.h"
qinclude "baseMailList.h"
qinclude "CFSGameGM.h"

qinclude "centersvrproxy.h"	
qinclude "MatchSvrProxy.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


extern CLogManager g_LogManager;
extern CFSGameDBCfg	* g_pFSDBCfg; 
extern CFSFileLog   g_FSFileLog;

BOOL CFSGameThread::ProcessGMPacket( CFSGameClient * pFSClient )
{
	switch( pFSClient->GetState() )
	{
	case NEXUS_INITIAL:
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case GM_G2S_LOGIN_REQ : 
				Process_GM_G2S_LOGIN_REQ(pFSClient);
				return TRUE;
			}
		}
		break;
	case NEXUS_LOGINSUCCESS :
		{
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case GM_G2S_ROOMLIST_REQ : 
				Process_GM_G2S_ROOMLIST_REQ(pFSClient);
				return TRUE;
			case GM_G2S_ROOMINFO_REQ : 
				Process_GM_G2S_ROOMINFO_REQ(pFSClient);
				return TRUE;
			case GM_G2S_CHAT_REQ : 
				Process_GM_G2S_CHAT_REQ(pFSClient);
				return TRUE;
			case GM_G2S_KICKOUT_REQ : 
				Process_GM_G2S_KICKOUT_REQ(pFSClient);
				return TRUE;
			case GM_G2S_FINDUSER_REQ : 
				Process_GM_G2S_FINDUSER_REQ(pFSClient);
				return TRUE;
			case GM_G2S_CLOSEROOM_REQ : 
				Process_GM_G2S_CLOSEROOM_REQ(pFSClient);
				return TRUE;
			}
		}
	default :
		{
			// 20090401 GM Kick기능
			switch(m_ReceivePacketBuffer.GetCommand())
			{
			case C2S_MANAGER_USER_KICK_REQ :
				Process_GM_C2S_KICKOUT_REQ(pFSClient);
				return TRUE;
			case C2S_GM_ALLUSER_KICKOUT_REQ:
				Process_GM_ALLUSER_KICKOUT_REQ(pFSClient);
				return TRUE;
			case C2S_GM_ALLUSER_SENDPAPER_REQ:
				Process_GM_ALLUSER_SENDPAPER_REQ(pFSClient);
				return TRUE;
			case GM_G2S_FORBID_USER_REQ :
				Process_GM_G2S_FORBID_USER_REQ(pFSClient);
				return TRUE;
			case GM_G2S_CHANGE_HOST_REQ :
				Process_GM_G2S_CHANGE_HOST_REQ(pFSClient);
				return TRUE;
			case GM_G2S_FORBID_MULITUSER_REQ:
				Process_GM_G2S_FORBID_MULITUSER_REQ(pFSClient);
				return TRUE;
			}
			// End
		}
		break;
	}
	
	return FALSE;
}

void CFSGameThread::Process_GM_G2S_LOGIN_REQ( CFSGameClient * pFSClient )
{
	char szUserID[MAX_USERID_LENGTH + 1];	
	char szPasswd[MAX_PASSWORD_LENGTH + 1];
	
	m_ReceivePacketBuffer.ReadNTString(szUserID, MAX_USERID_LENGTH + 1);	
	m_ReceivePacketBuffer.ReadNTString(szPasswd, MAX_PASSWORD_LENGTH + 1);
	
	pFSClient->SetState(NEXUS_LOGINSUCCESS);
	
	int nResult = 0;
	CPacketComposer packet(GM_S2G_LOGIN_RES);
	packet.Add(nResult);
	pFSClient->Send(&packet);
}

void CFSGameThread::Process_GM_G2S_ROOMLIST_REQ( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	//TD - GM 룸리스트
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	pMatch->SendGMRoomListReq(info);
	//}
}

void CFSGameThread::Process_GM_G2S_ROOMINFO_REQ( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_GM_ROOMINFO_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iRoomNum);

	//TD - GM 정보
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	pMatch->SendGMRoomInfoReq(info);
	//}
}

void CFSGameThread::Process_GM_G2S_CHAT_REQ( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_GM_CHAT_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iRoomNum);
	m_ReceivePacketBuffer.ReadNTString(info.szMessage, MAX_CHATBUFF_LENGTH+1);
	m_ReceivePacketBuffer.Read(&info.iType);

	if(3 == info.iType)
	{
		m_ReceivePacketBuffer.ReadNTString(info.szGameID, MAX_GAMEID_LENGTH+1);
	}

	//TD - GM 채팅
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	pMatch->SendGMChatReq(info);
	//}
}

void CFSGameThread::Process_GM_G2S_KICKOUT_REQ( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_GM_KICKOUT_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iRoomNum);
	m_ReceivePacketBuffer.Read(&info.iType);
	m_ReceivePacketBuffer.ReadNTString(info.szGameID, MAX_GAMEID_LENGTH+1);
	
	//TD - GM 킥
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	pMatch->SendGMKickOutReq(info);
	//}
}

void CFSGameThread::Process_GM_G2S_FINDUSER_REQ( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_GM_FINDUSER_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.ReadNTString(info.szGameID, MAX_GAMEID_LENGTH+1);

	//TD - GM 유저찾기
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	pMatch->SendGMFindUserReq(info);
	//}
}

void CFSGameThread::Process_GM_G2S_CLOSEROOM_REQ( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_GM_CLOSEROOM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iRoomNum);

	//TD - GM 방폭
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	pMatch->SendGMCloseRoomReq(info);
	//}
}

void CFSGameThread::Process_GM_G2S_FORBID_USER_REQ(CFSGameClient* pFSClient)
{
	CFSLoginGlobalODBC* pLoginGlobalODBC = (CFSLoginGlobalODBC*)ODBCManager.GetODBC( ODBC_LOGINGLOBAL );
	CHECK_NULL_POINTER_VOID(pLoginGlobalODBC);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pCenter);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iForbidUserIDIndex = -1, iForbidTime = 0, iResult = -1;
	DECLARE_INIT_TCHAR_ARRAY(szForbidGameID, MAX_GAMEID_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szForbidReason, MAX_FORBID_REASON_LENGTH+1);

	m_ReceivePacketBuffer.Read((PBYTE)szForbidGameID, MAX_GAMEID_LENGTH+1);
	m_ReceivePacketBuffer.Read(&iForbidTime );
	m_ReceivePacketBuffer.Read((PBYTE)szForbidReason, MAX_FORBID_REASON_LENGTH+1);

	if(ODBC_RETURN_SUCCESS == pGameODBC->spGetUserIDIndexFromGameIDOrIndex(szForbidGameID, 0, iForbidUserIDIndex))
	{
		if(ODBC_RETURN_SUCCESS == pLoginGlobalODBC->GMForbidUser(pUser->GetUserID(), iForbidUserIDIndex, iForbidTime, szForbidReason, iResult))
		{
			pCenter->SendRequestKickOut((char*)szForbidGameID);
		}
	}
	else
	{
		iResult = -2;
	}

	CPacketComposer packet(GM_S2G_FORBID_USER_RES);
	packet.Add((BYTE*)szForbidGameID, MAX_GAMEID_LENGTH+1);
	packet.Add(iResult);
	
	pUser->Send(&packet);
}

void CFSGameThread::Process_GM_G2S_FORBID_MULITUSER_REQ(CFSGameClient* pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_GM_FORBID_MULITUSER_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.byType);
	m_ReceivePacketBuffer.Read(&info.iRoomIdx);
	m_ReceivePacketBuffer.Read(&info.iForbiddenTime);
	m_ReceivePacketBuffer.Read((PBYTE)info.szKickReason, MAX_FORBID_REASON_LENGTH+1 );

	//TD - GM 유저 블락
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	CPacketComposer PacketComposer(G2M_GM_FORBID_MULITUSER_REQ);
	//	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_GM_FORBID_MULITUSER_REQ));

	//	pMatch->Send(&PacketComposer);
	//}
}

void CFSGameThread::Process_GM_G2S_CHANGE_HOST_REQ(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_GM_CHANGE_HOST_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iOpen);

	//TD - GM 호스트변경
	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)CFSGameServer::GetInstance()->GetProxy(CFSGameServer::GetInstance()->GetMatchSvrProcessID());
	//if(pMatch) 
	//{
	//	CPacketComposer PacketComposer(G2M_GM_CHANGE_HOST_REQ);
	//	PacketComposer.Add((BYTE*)&info, sizeof(SG2M_GM_CHANGE_HOST_REQ));

	//	pMatch->Send(&PacketComposer);
	//}
}

// 20090401 GM Kick기능 - 20090520 expand this function to all channels.
void CFSGameThread::Process_GM_C2S_KICKOUT_REQ( CFSGameClient * pFSClient )
{
	CFSGameServer* pServer = (CFSGameServer *)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID( pServer )

	CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID( pCenter )

	DECLARE_INIT_TCHAR_ARRAY( szGameID, MAX_GAMEID_LENGTH+1 );	
	
	m_ReceivePacketBuffer.Read( (PBYTE) szGameID, MAX_GAMEID_LENGTH + 1 );

	pCenter->SendRequestKickOut( (char*) szGameID );
}
// End
void CFSGameThread::Process_GM_ALLUSER_KICKOUT_REQ(CFSGameClient * pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer *)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID( pServer )

	CPacketComposer packet(S2C_GM_ALLUSER_KICKOUT_RES);

	if(pServer->AllUserForceOutExceptMe((CFSGameUser*)pFSClient->GetUser()) == TRUE)
	{
		packet.Add(1);
	}
	else
	{
		packet.Add(0);
	}
	pFSClient->Send(&packet);
}

void CFSGameThread::Process_GM_ALLUSER_SENDPAPER_REQ(CFSGameClient * pFSClient)
{
	DECLARE_INIT_TCHAR_ARRAY( szTitle, 20+1 );
	DECLARE_INIT_TCHAR_ARRAY( szText, 256+1 );

	CFSGameServer* pServer = (CFSGameServer *)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID( pServer )

	CPacketComposer packet(S2C_GM_ALLUSER_SENDPAPER_RES);

	short TextLength = 0;

	m_ReceivePacketBuffer.Read( (PBYTE) szTitle, 20 + 1 );
	m_ReceivePacketBuffer.Read(&TextLength);
	m_ReceivePacketBuffer.Read( (PBYTE) szText, TextLength + 1 );

	if(pServer->AllUserSendPaper(((CFSGameUser*)(pFSClient->GetUser()))->GetGameIDIndex(), szTitle, szText) == TRUE)
	{
		packet.Add(1);
	}
	else
	{
		packet.Add(0);
	}
	pFSClient->Send(&packet);
}