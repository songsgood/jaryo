qinclude "stdafx.h"
qinclude "CFSGameUser.h"
qinclude "FSGameUserCongratulationEvent.h"
qinclude "CongratulationEventManager.h"
qinclude "ThreadODBCManager.h"
qinclude "CFSGameServer.h"
qinclude "RewardManager.h"
qinclude "EventDateManager.h"

CFSGameUserCongratulationEvent::CFSGameUserCongratulationEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_tLastResetTime(-1)
	, m_tStartConnectTime(-1)
	, m_iConnectSecond(0)
	, m_btBalloonStatus(CongratulationEvent::BALLOON_STATUS_BUTTON_OFF)
	, m_iBalloonGiveCnt(-1)
	, m_iMyMultipleCoin(0)
	, m_iTodayGetCoin(0)
	, m_btExpGamePlayCnt(0)
	, m_btExpGamePlayCnt_Send(0)
	, m_iTotalExp(0)
{
	ZeroMemory(m_iMissionValue, sizeof(int) * CongratulationEvent::MAX_CNT_MISSION_CONDITION_TYPE);
	ZeroMemory(m_iMissionBoxCnt, sizeof(int) * CongratulationEvent::BALLOON_MISSION_CNT);	
}


CFSGameUserCongratulationEvent::~CFSGameUserCongratulationEvent(void)
{
}


BOOL			CFSGameUserCongratulationEvent::Load()
{
	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(CLOSED == CONGRATULATION.IsOpen(), TRUE);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_CONGRATULATION_GetUserData(
		  m_pUser->GetUserIDIndex()
		, m_tLastResetTime
		, m_iConnectSecond
		, m_btBalloonStatus
		, m_iMyMultipleCoin
		, m_iTodayGetCoin
		, m_iTotalExp
		, m_btExpGamePlayCnt
		, m_iMissionValue
		, m_iMissionBoxCnt))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_CONGRATULATION_GetUserData failed. UserIDIndex:10752790", m_pUser->GetUserIDIndex());
rn FALSE;	
	}

	m_bDataLoaded = TRUE;
	m_tStartConnectTime = _time64(NULL);

	CheckResetDate(m_tStartConnectTime, TRUE);

	return TRUE;
}

BOOL			CFSGameUserCongratulationEvent::LoadBalloonGiveCnt()
{
	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	return (ODBC_RETURN_SUCCESS == pODBC->EVENT_CONGRATULATION_GetUserBalloonGiveCnt(m_pUser->GetUserIDIndex(), m_iBalloonGiveCnt));
}

BOOL			CFSGameUserCongratulationEvent::CheckResetDate(time_t tCurrentTime/* = _time64(NULL)*/, BOOL bIsLogin/* = FALSE*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	BOOL	bResult = FALSE;

	if(-1 == m_tLastResetTime)
	{
		bResult = TRUE;
	}
	else
	{
		int iCurrentDate_Reset = TimetToYYYYMMDD(tCurrentTime - (EVENT_RESET_HOUR*60*60));
		int iLastResetDate_Reset = TimetToYYYYMMDD(m_tLastResetTime - (EVENT_RESET_HOUR*60*60));

		if(iCurrentDate_Reset > iLastResetDate_Reset)
		{
			bResult = TRUE;
		}
		else if(iCurrentDate_Reset < iLastResetDate_Reset)
		{
			WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "Check the server machine time, please. (CurrentDate:10752790, LastResetDate:0)", iCurrentDate_Reset, iLastResetDate_Reset);
	}	

	if(bResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_CONGRATULATION_ResetUserData(m_pUser->GetUserIDIndex(), tCurrentTime, m_iMissionBoxCnt))
		{			
			m_iConnectSecond = 0;
			m_tLastResetTime = tCurrentTime;
			m_tStartConnectTime = tCurrentTime; 

			m_btBalloonStatus = CongratulationEvent::BALLOON_STATUS_BUTTON_ON;
			ZeroMemory(m_iMissionValue, sizeof(int) * CongratulationEvent::MAX_CNT_MISSION_CONDITION_TYPE);
			m_iTodayGetCoin = 0;
			m_btExpGamePlayCnt = 0;
			m_btExpGamePlayCnt_Send = 0;

			if(FALSE == bIsLogin)	// 접속하고 있다가 리셋이 된 경우, 접속 시작시각을 그 날 리셋시간으로 설정.
			{
				TIMESTAMP_STRUCT	RecentResetHourDate;
				TimetToTimeStruct(m_tLastResetTime, RecentResetHourDate);
				RecentResetHourDate.hour = EVENT_RESET_HOUR;
				RecentResetHourDate.minute = 0;
				RecentResetHourDate.second = 0;

				m_tStartConnectTime = TimeStructToTimet(RecentResetHourDate);
			}
		}
	}

	return bResult;
}

void			CFSGameUserCongratulationEvent::SendEventInfo(BYTE btEventSubKind)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == CONGRATULATION.IsOpen());

	switch(btEventSubKind)
	{
	case CongratulationEvent::BALLOON_MISSION_EVENT:
		{
			m_pUser->Send(CONGRATULATION.GetPacketEventInfo_BalloonMission());
		}
		break;

	case CongratulationEvent::MULTIPLE_GIFTSHOP_EVENT:
		{
			m_pUser->Send(CONGRATULATION.GetPacketEventInfo_GiftShop());
		}
		break;

	case CongratulationEvent::MAX_CNT_CONGRATULATION_SUB_EVENT_KIND:
		{
			CPacketComposer Packet(S2C_CONGRATULATION_MAIN_EVENT_INFO_RES);
			m_pUser->Send(&Packet);
		}
		break;
	}
}

void			CFSGameUserCongratulationEvent::SendUserInfo(BYTE btEventSubKind)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == CONGRATULATION.IsOpen());

	time_t tCurrentTime = _time64(NULL);
	CheckAndUpdateMission(CongratulationEvent::LOGIN_TIME_MIN, 0, tCurrentTime);

	switch(btEventSubKind)
	{
	case CongratulationEvent::BALLOON_MISSION_EVENT:
		{
			SS2C_CONGRATULATION_BALLOON_MISSION_USER_INFO_RES	ss;
			ZeroMemory(&ss, sizeof(SS2C_CONGRATULATION_BALLOON_MISSION_USER_INFO_RES));

			ss.btBalloonStatus = m_btBalloonStatus;
			for(BYTE i = 0; i < CongratulationEvent::BALLOON_MISSION_CNT; ++i)
			{
				SBalloonMissionInfo sMission;
				if(CONGRATULATION.GetMissionInfo(i, sMission) && 
					sMission.btMissionConditionType < CongratulationEvent::MAX_CNT_MISSION_CONDITION_TYPE)
				{
					ss.sUser[i].iCurrentValue = MIN(m_iMissionValue[sMission.btMissionConditionType], sMission.iClearValue);
				}
				ss.sUser[i].iMissionBoxCnt = m_iMissionBoxCnt[i];
			}

			m_pUser->Send(S2C_CONGRATULATION_BALLOON_MISSION_USER_INFO_RES, &ss, sizeof(SS2C_CONGRATULATION_BALLOON_MISSION_USER_INFO_RES));
		}
		break;

	case CongratulationEvent::MULTIPLE_GIFTSHOP_EVENT:
		{
			SS2C_CONGRATULATION_MULTIPLE_GIFTSHOP_USER_INFO_RES	ss;

			ss.iMyCoin = m_iMyMultipleCoin;
			ss.iTodayGetCoin = m_iTodayGetCoin;
			ss.iMissionCurrentValue = m_iMissionValue[CongratulationEvent::GAME_PLAY];

			m_pUser->Send(S2C_CONGRATULATION_MULTIPLE_GIFTSHOP_USER_INFO_RES, &ss, sizeof(SS2C_CONGRATULATION_MULTIPLE_GIFTSHOP_USER_INFO_RES));
		}
		break;

	case CongratulationEvent::SPECIAL_PEAKTIME_EVENT:
		{
			SS2C_CONGRATULATION_SPECIAL_PEAKTIME_USER_INFO_RES	ss;
			ss.iTotalExp = m_iTotalExp;

			m_pUser->Send(S2C_CONGRATULATION_SPECIAL_PEAKTIME_USER_INFO_RES, &ss, sizeof(SS2C_CONGRATULATION_SPECIAL_PEAKTIME_USER_INFO_RES));
		}
		break;
	}
}

void			CFSGameUserCongratulationEvent::GiveBalloon(BYTE btProperty)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == CONGRATULATION.IsOpen());

	CheckResetDate();

	SS2C_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_RES	ss;

	if(btProperty >= MAX_STAT_TYPE_COUNT)
	{
		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_GET_BALLOON_FAIL_WRONG_INFO;
	}
	else if(CongratulationEvent::BALLOON_STATUS_BUTTON_OFF == m_btBalloonStatus)
	{
		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_GET_BALLOON_FAIL_ALREADY_GET;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_GET_BALLOON_FAIL_FULL_MAILBOX;
	}
	else
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);
		
		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_GET_BALLOON_FAIL_WRONG_INFO;

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_CONGRATULATION_GiveBalloon(m_pUser->GetUserIDIndex(), btProperty, m_iBalloonGiveCnt))
		{
			m_btBalloonStatus = CongratulationEvent::BALLOON_STATUS_BUTTON_OFF;
						
			vector<int> vParameters;
			vParameters.push_back(m_iBalloonGiveCnt);
			m_pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_CONGRATULATION_EVENT_GIVE_BALLOON_CNT, ACHIEVEMENT_LOG_GROUP_EVENT, vParameters);
			
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, CONGRATULATION.GetBalloonRewardIndex(btProperty));
			if(pReward)
			{
				ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_GET_BALLOON_SUCCESS;
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "CFSGameUserCongratulationEvent::GiveBalloon REWARDMANAGER.GiveReward Fail. UserIDIndex:10752790, Property:0", m_pUser->GetUserIDIndex(), btProperty);

	}
	}

	m_pUser->Send(S2C_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_RES, &ss, sizeof(SS2C_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_RES));
}

void			CFSGameUserCongratulationEvent::OpenBox(BYTE btMissionIndex)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == CONGRATULATION.IsOpen());

	CheckResetDate();

	SS2C_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_RES));

	if(btMissionIndex >= CongratulationEvent::BALLOON_MISSION_CNT)
	{
		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_OPEN_BOX_FAIL_WRONG_INFO;
	}
	else if(m_iMissionBoxCnt[btMissionIndex] <= 0)
	{
		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_OPEN_BOX_FAIL_LACK_BOX;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_OPEN_BOX_FAIL_FULL_MAILBOX;
	}
	else
	{		
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);

		ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_OPEN_BOX_FAIL_WRONG_INFO;

		int iRewardIndex = CONGRATULATION.GetBoxRandomReward(btMissionIndex);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_CONGRATULATION_OpenBox(m_pUser->GetUserIDIndex(), btMissionIndex, iRewardIndex))
		{
			--m_iMissionBoxCnt[btMissionIndex];

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(pReward)
			{
				ss.btResult = CongratulationEvent::RESULT_BALLOON_MISSION_OPEN_BOX_SUCCESS;

				ss.sReward.btRewardType = pReward->GetRewardType();

				if(REWARD_TYPE_ITEM == ss.sReward.btRewardType ||
					REWARD_TYPE_EXHAUST_ITEM_TO_INVEN == ss.sReward.btRewardType || 
					REWARD_TYPE_COACH_CARD == ss.sReward.btRewardType)
				{
					SRewardConfigItem* pRewardItem = dynamic_cast<SRewardConfigItem*>(pReward);
					if(pRewardItem)
					{
						if(REWARD_TYPE_COACH_CARD == ss.sReward.btRewardType)
							ss.sReward.iPropertyType = ITEM_PROPERTY_EXHAUST;
						else
							ss.sReward.iPropertyType = pRewardItem->iPropertyType;

						ss.sReward.iItemCode = pReward->GetRewardValue();
						ss.sReward.iPropertyValue = pReward->GetPropertyValue();
					}
				}
				else
				{
					ss.sReward.iPropertyValue = pReward->GetRewardValue();
				}
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "CFSGameUserCongratulationEvent::OpenBox REWARDMANAGER.GiveReward Fail. UserIDIndex:10752790, MissionIndex:0", m_pUser->GetUserIDIndex(), btMissionIndex);

	}
	}

	m_pUser->Send(S2C_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_RES, &ss, sizeof(SS2C_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_RES));
}
	
void			CFSGameUserCongratulationEvent::BuyMultipleGift(BYTE btGiftIndex)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == CONGRATULATION.IsOpen());

	CheckResetDate();

	SS2C_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_RES	ss;
	SMultipleGiftInfo sGift;
	if(FALSE == CONGRATULATION.GetMultipleGiftInfo(btGiftIndex, sGift, ss.btMultipleNum))
	{
		ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_BUY_GIFT_FAIL_WRONG_INFO;
	}
	else if(m_iMyMultipleCoin < sGift.iPrice)
	{
		ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_BUY_GIFT_FAIL_LACK_COIN;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_BUY_GIFT_FAIL_FULL_MAILBOX;
	}
	else
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);

		ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_BUY_GIFT_FAIL_WRONG_INFO;

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_CONGRATULATION_BuyGift(m_pUser->GetUserIDIndex(), btGiftIndex, sGift.iPrice, ss.btMultipleNum))
		{
			m_iMyMultipleCoin -= sGift.iPrice;

			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, sGift.sReward.iRewardIndex, static_cast<int>(ss.btMultipleNum));
			if(pReward)
			{
				ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_BUY_GIFT_SUCCESS;
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "CFSGameUserCongratulationEvent::BuyMultipleGift REWARDMANAGER.GiveReward Fail. UserIDIndex:10752790, RewardIndex:0, MultipleNum:7106560", m_pUser->GetUserIDIndex(), sGift.sReward.iRewardIndex, ss.btMultipleNum);

}

	m_pUser->Send(S2C_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_RES, &ss, sizeof(SS2C_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_RES));
}

void			CFSGameUserCongratulationEvent::UseTotalExp(char* szGameID)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == CONGRATULATION.IsOpen());
	
	if(nullptr == szGameID)
	{
		return;
	}

	if(m_iTotalExp <= 0)
	{
		return;
	}

	SS2C_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_RES));

	SMyAvatar sAvatar;
	if(FALSE == m_pUser->GetMyAvatar(szGameID, sAvatar))
	{
		ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_GET_EXP_FAIL;
	}
	else if(GAME_LVUP_MAX == sAvatar.iLv)
	{
		ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_GET_EXP_FAIL;
	}
	else
	{
		int iUpdateLv = sAvatar.iLv;
		int iUpdateExp = sAvatar.iExp + m_iTotalExp;
		int iLevelUpCount = 0;
		int iPrevLv = iUpdateLv;

		CFSGameServer* pServer = CFSGameServer::GetInstance();
		CHECK_NULL_POINTER_VOID(pServer);

		while(GAME_LVUP_MAX > iUpdateLv && iUpdateExp >= pServer->GetLvUpExp(iUpdateLv+1))
		{
			iUpdateLv++;
			iLevelUpCount++;
		}

		if(iUpdateLv == GAME_LVUP_MAX)
		{
			iUpdateExp = pServer->GetLvUpExp(iUpdateLv);
		}

		CFSEventODBC* pEventODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pEventODBC);

		ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_GET_EXP_FAIL;

		if(ODBC_RETURN_SUCCESS == pEventODBC->EVENT_CONGRATULATION_UseTotalExp(m_pUser->GetUserIDIndex(), sAvatar.iGameIDIndex))
		{
			CFSODBCBase* pBaseODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
			CHECK_NULL_POINTER_VOID(pBaseODBC);
			
			if(ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_CONGRATULATION_UpdateUserLvExp(sAvatar.iGameIDIndex, iUpdateLv, iUpdateExp))
			{
				if(iLevelUpCount > 0)
				{
					m_pUser->ProcessLevelUp(sAvatar.iGameIDIndex, sAvatar.iGamePosition, iPrevLv, iUpdateLv, iUpdateExp);
				}

				ss.btResult = CongratulationEvent::RESULT_MULTIPLE_GIFTSHOP_GET_EXP_SUCCESS;
				
				if(m_pUser->GetMyAvatar(sAvatar.iGameIDIndex, sAvatar))
				{
					strncpy_s(ss.sAvatar.szGameID, _countof(ss.sAvatar.szGameID), sAvatar.szGameID, MAX_GAMEID_LENGTH);
					ss.sAvatar.iGamePosition = sAvatar.iGamePosition;
					ss.sAvatar.btSex = sAvatar.btSex;
					ss.sAvatar.iLv = sAvatar.iLv;
					ss.sAvatar.iMinExp = sAvatar.iMinExp;
					ss.sAvatar.iMaxExp = sAvatar.iMaxExp;
					ss.sAvatar.iExp = sAvatar.iExp;
				}					
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, FAIL, "EVENT_CONGRATULATION_UpdateUserLvExp Fail. GameIDIndex:10752790, TotalExp:0", sAvatar.iGameIDIndex, m_iTotalExp);


		m_iTotalExp = 0;			
		}		
	}

	m_pUser->Send(S2C_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_RES, &ss, sizeof(SS2C_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_RES));
}

BOOL			CFSGameUserCongratulationEvent::CheckAndUpdateMission(const BYTE btMissionConditionType, const int iGiveExp, const time_t tCurrentTime/* = _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == CONGRATULATION.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(btMissionConditionType >= CongratulationEvent::MAX_CNT_MISSION_CONDITION_TYPE, FALSE);
	CheckResetDate(tCurrentTime);

	int iMaxClearValue = CONGRATULATION.GetMaxClearValue(btMissionConditionType);
	BOOL bMaxValue = FALSE;
	if(m_iMissionValue[btMissionConditionType] >= iMaxClearValue)
	{
		bMaxValue = TRUE;
	}
	
	int iConnectSecond = m_iConnectSecond;
	int iGamePlayGiftCoin = 0;
	time_t tStartConnectTime = m_tStartConnectTime;

	int iAddValue = 0;
	
	switch(btMissionConditionType)
	{
	case CongratulationEvent::GAME_PLAY:
		{
			iAddValue = 1;

			if(bMaxValue)
			{
				if(m_iTodayGetCoin >= CONGRATULATION.GetGiftShopOneDayMaxCoin() &&
					iGiveExp <= 0)
				{
					return FALSE;
				}

				iAddValue = 0;
			}

			iGamePlayGiftCoin = CONGRATULATION.GetGiftShopGamePlayRewardCoin();
		}
		break;

	case CongratulationEvent::LOGIN_TIME_MIN:
		{
			if(bMaxValue)
			{
				return FALSE;
			}
			
			iAddValue = (tCurrentTime - m_tStartConnectTime + m_iConnectSecond) / 60;

			CHECK_CONDITION_RETURN(iAddValue <= 0, FALSE);

			if(m_iMissionValue[btMissionConditionType] + iAddValue > iMaxClearValue)
			{
				iAddValue = iMaxClearValue - m_iMissionValue[btMissionConditionType];
			}

			iConnectSecond = (tCurrentTime - m_tStartConnectTime + m_iConnectSecond) - (iAddValue * 60);
			tStartConnectTime = tCurrentTime;
		}
		break;

	default:
		return FALSE;
	}

	if(btMissionConditionType == CongratulationEvent::GAME_PLAY &&
		FALSE == bMaxValue &&
		m_iMissionValue[btMissionConditionType] + iAddValue == iMaxClearValue)
	{
		//최종완료 시 추가 코인 지급
		iGamePlayGiftCoin += CONGRATULATION.GetGiftShopGamePlayRewardCoinAllClear();
	}

	iGamePlayGiftCoin = MIN(iGamePlayGiftCoin, CONGRATULATION.GetGiftShopOneDayMaxCoin() - m_iTodayGetCoin);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_CONGRATULATION_UpdateMissionValue(m_pUser->GetUserIDIndex(), btMissionConditionType, iAddValue, iGamePlayGiftCoin, iGiveExp), FALSE);

	m_iMissionValue[btMissionConditionType] += iAddValue;
	m_iConnectSecond = iConnectSecond;
	m_tStartConnectTime = tStartConnectTime;
	m_iMyMultipleCoin += iGamePlayGiftCoin;
	m_iTodayGetCoin += iGamePlayGiftCoin;

	if(iGiveExp > 0)
	{
		m_iTotalExp += iGiveExp;
		++ m_btExpGamePlayCnt;
	}

	return TRUE;
}

BOOL			CFSGameUserCongratulationEvent::CheckGamePlayMission(BOOL bIsDisconnectedMatch, BOOL bRunAway, time_t tGameStartTime, int iBaseExp)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == CONGRATULATION.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(TRUE == bIsDisconnectedMatch || TRUE == bRunAway, FALSE);

	int iGiveExp = 0;
	time_t tEventStartDate, tEventEndDate;
	EVENTDATEMANAGER.GetEventDate(EVENT_KIND_CONGRATULATION, tEventStartDate, tEventEndDate);
	if(tEventStartDate <= tGameStartTime && tGameStartTime <= tEventEndDate)
	{
		SYSTEMTIME GameStartTime;
		TimetToSystemTime(tGameStartTime, GameStartTime);
		
		if(CONGRATULATION.IsExpBuffGamePlayTime((GameStartTime.wHour * 100) + GameStartTime.wMinute))
		{
			m_btExpGamePlayCnt_Send = MIN(m_btExpGamePlayCnt + 1, CONGRATULATION.GetGamePlayExpBuffMaxCnt());
			iGiveExp = iBaseExp * m_btExpGamePlayCnt_Send;
		}
	}

	return CheckAndUpdateMission(CongratulationEvent::GAME_PLAY, iGiveExp);
}

BOOL			CFSGameUserCongratulationEvent::SaveStartConnectTime()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == CONGRATULATION.IsOpen(), FALSE);

	time_t tCurrentTime = _time64(NULL);
	CHECK_CONDITION_RETURN(TRUE == CheckAndUpdateMission(CongratulationEvent::LOGIN_TIME_MIN, 0, tCurrentTime), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_CONGRATULATION_SaveStartConnectTime(m_pUser->GetUserIDIndex(), tCurrentTime - m_tStartConnectTime + m_iConnectSecond), FALSE);

	return TRUE;
}

int				CFSGameUserCongratulationEvent::GetBalloonGiveCnt()
{
	return (m_iBalloonGiveCnt >= 0 || TRUE == LoadBalloonGiveCnt()) ? m_iBalloonGiveCnt : 0;		
}

void CFSGameUserCongratulationEvent::CheckAndSendBalloonMissionNoClearNot()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == CONGRATULATION.IsOpen());

	BOOL bNotice = FALSE;

	if(TRUE == CheckResetDate())
	{
		bNotice = TRUE;
	}
	else if(CongratulationEvent::BALLOON_STATUS_BUTTON_ON == m_btBalloonStatus)
	{
		bNotice = TRUE;
	}
	else
	{
		for(BYTE i = 0; i < CongratulationEvent::MAX_CNT_MISSION_CONDITION_TYPE; ++i)
		{
			if(m_iMissionValue[i] < CONGRATULATION.GetMaxClearValue(i))
			{
				bNotice = TRUE;
				break;
			}
		}
	}	

	if(bNotice)
	{
		CPacketComposer Packet(S2C_CONGRATULATION_BALLOON_MISSION_NO_CLEAR_NOT);
		m_pUser->Send(&Packet);
	}
}

BYTE CFSGameUserCongratulationEvent::GetExpGamePlayCnt()
{
	BYTE btCnt = m_btExpGamePlayCnt_Send;
	m_btExpGamePlayCnt_Send = 0;
	return btCnt;
}
