qpragma once
qinclude "Lock.h"
qinclude "MatchingCardManager.h"

typedef map<BYTE, SMATCHINGCARD_REWARD_ITEM_INFO> MAP_USERREWARDITEM;

class CFSGameUser;
class CFSGameUserMatchingCard
{
public:
	BOOL Load(void);
	void SendMatchingCardInfo(void);
	void SendMatchingCardReady(void);
	void SendMatchingCardRewardInfo(void);
	void FlipCard( BYTE btIndex );
	void CheckAndAddPlayCount(void);
	void CheckAndSendTimeCheck(void);
	void CheckAndGivePlayCountWithPcRoom();

public:
	int GetGameLogIndex(void);

private:
	BOOL InitializeMatchingCard(void);
	BOOL CollisionFirstFlipCard(BYTE btIndex);
	BOOL GiveRewardMatchingCardItem(BYTE btIndex);
	void CopyMatchingCardPacketItemInfo(SMATCHINGCARD_REWARD_ITEM_INFO& sLeft, SMATCHINGCARD_REWARD_ITEM_INFO sRight );
	void CopyMatchingCardPacketItemInfo(SMATCHINGCARD_REWARD_ITEM_INFO& sLeft, SMATCHINGCARDITEM sRight );
	void MakePacketDataMatchingCardInfo(SMATCHINGCARD_INFO* pCardInfo);
	int GetOpenCardCount(void);
	BOOL CheckAllClear(void);
	BOOL CheckPlayCountAndDecreaseLife(void);
	void RemoveGame(void);
	void CheckAndGivePlayCount(time_t tOpDate);
	void ReLoadUserData(void);
	void SendCumulativeCashToCenterSvr(void);
	RESULT_EVENT_MATCHINGCARD_FLIPCARD CheckFlipCard(SMATCHINGCARDCONFIG sConfig);
	void SendFlipCard(SS2C_EVENT_MATCHINGCARD_FLIPCARD_RES& rs);
	

private:
	CFSGameUser* m_pUser;
	int		m_iGameLogIndex;
	DWORD	m_dwStartTick;
	BYTE	m_btFirstCardIndex, m_btSecondCardIndex;
	BOOL	m_bInitDBdata;

	SUSERMATCHINGCARD m_sUserMatchingCardInfo;
	SMATCHINGCARD_INFO m_sUserMatchingCard[MAX_MATCHINGCARD_INDEX];
	MAP_USERREWARDITEM m_mapUserRewardItem;
	int m_iTodayCumulativeCoin;
	time_t m_tOpdate;

public:
	CFSGameUserMatchingCard(CFSGameUser* pUser);
	virtual ~CFSGameUserMatchingCard(void);
};

