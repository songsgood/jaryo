qinclude "stdafx.h"
qinclude "FSGameUserTransferJoycityShopEvent.h"
qinclude "TransferJoycityShopEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"
qinclude "CFSGameServer.h"
qinclude "UserHighFrequencyItem.h"

CFSGameUserTransferJoycityShopEvent::CFSGameUserTransferJoycityShopEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bIsGetDailyCoin(FALSE)
	, m_tGetCoinTime(-1)
	, m_iTodayPlayCnt(0)
	, m_tLastPlayTime(-1)
{
}

CFSGameUserTransferJoycityShopEvent::~CFSGameUserTransferJoycityShopEvent(void)
{
}

BOOL CFSGameUserTransferJoycityShopEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == TRANSFERJOYCITYSHOP.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_TRANSFERJOYCITY_SHOP_GetUserData(m_pUser->GetUserIDIndex(), m_bIsGetDailyCoin, m_tGetCoinTime, m_iTodayPlayCnt, m_tLastPlayTime))
	{
		WRITE_LOG_NEW(LOG_TYPE_TRANSFERJOYCITYSHOP, INITIALIZE_DATA, FAIL, "EVENT_TRANSFERJOYCITY_SHOP_GetUserData error");
		return FALSE;
	}

	UpdateStatus();

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserTransferJoycityShopEvent::UpdateStatus()
{
	if(TRUE == m_bIsGetDailyCoin &&
		FALSE == TodayCheck(EVENT_RESET_HOUR, m_tGetCoinTime, _GetCurrentDBDate))
	{
		m_bIsGetDailyCoin = FALSE;
	}
	
	if(m_iTodayPlayCnt > 0 &&
		FALSE == TodayCheck(EVENT_RESET_HOUR, m_tLastPlayTime, _GetCurrentDBDate))
	{
		m_iTodayPlayCnt = 0;
	}
}

void CFSGameUserTransferJoycityShopEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == TRANSFERJOYCITYSHOP.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	UpdateStatus();

	SS2C_EVENT_TRANSFERJOYCITY_SHOP_INFO_RES rs;
	rs.iTodayCnt = m_iTodayPlayCnt;
	rs.iTodayCntMax = TRANSFERJOYCITYSHOP.GetDailyMaxPlayCnt();

	rs.iHaveCoin = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_SHOP_COIN);
	if(NULL != pItem)
		rs.iHaveCoin = pItem->iPropertyTypeValue;

	rs.bIsGetDailyCoin = m_bIsGetDailyCoin;

	m_pUser->Send(S2C_EVENT_TRANSFERJOYCITY_SHOP_INFO_RES, &rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_SHOP_INFO_RES));
}

void CFSGameUserTransferJoycityShopEvent::SendEventShopItemInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == TRANSFERJOYCITYSHOP.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CPacketComposer Packet(S2C_EVENT_TRANSFERJOYCITY_SHOP_SLOT_INFO_RES);
	TRANSFERJOYCITYSHOP.MakeShopItemInfo(Packet);
	m_pUser->Send(&Packet);
}

RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD CFSGameUserTransferJoycityShopEvent::GetRewardReq(int iSlotIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == TRANSFERJOYCITYSHOP.IsOpen(), RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_FAIL);
	
	UpdateStatus();

	SRewardInfo sReward; 
	int iPrice;
	CHECK_CONDITION_RETURN(FALSE == TRANSFERJOYCITYSHOP.GeShopItemInfo(iSlotIndex, sReward, iPrice), RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_FAIL);

	int iHaveCoin = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_SHOP_COIN);
	if(NULL != pItem)
		iHaveCoin = pItem->iPropertyTypeValue;

	CHECK_CONDITION_RETURN(iHaveCoin < iPrice, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_ENOUGH_COIN);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_FAIL);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_FAIL);

	int iUpdated_Coin = 0;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_TRANSFERJOYCITY_SHOP_BuyItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iSlotIndex, sReward.iRewardIndex, iPrice, iUpdated_Coin), RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_FAIL);
	
	if(0 >= iUpdated_Coin)
		m_pUser->GetUserHighFrequencyItem()->DeleteUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_SHOP_COIN);
	else
		m_pUser->GetUserHighFrequencyItem()->UpdateUserHighFrequencyItemCount(ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_SHOP_COIN, iUpdated_Coin);

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, sReward.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_FAIL);

	return RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_SUCCESS;
}

RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN CFSGameUserTransferJoycityShopEvent::GetDailyCoinReq()
{
	CHECK_CONDITION_RETURN(CLOSED == TRANSFERJOYCITYSHOP.IsOpen(), RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_FAIL);

	UpdateStatus();

	CHECK_CONDITION_RETURN(TRUE == m_bIsGetDailyCoin, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_FAIL);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_FAIL);

	int iAddCoin = 1;
	int iUpdated_Coin = 0;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_TRANSFERJOYCITY_SHOP_UpdateCoin(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), EVENT_TRANSFERJOYCITY_SHOP_LOG_GET_COIN_TYPE_LOGIN, iAddCoin, m_iTodayPlayCnt, iUpdated_Coin), RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_FAIL);

	CUserHighFrequencyItem* pHighFrequencyItem = m_pUser->GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_EVENT_TRANSFERJOYCITY_SHOP_COIN;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdated_Coin;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	m_bIsGetDailyCoin = 1;
	m_tGetCoinTime = _GetCurrentDBDate;

	return RESULT_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_SUCCESS;
}

BOOL CFSGameUserTransferJoycityShopEvent::CheckGameEndGiveCoin()
{
	CHECK_CONDITION_RETURN(CLOSED == TRANSFERJOYCITYSHOP.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	UpdateStatus();

	CHECK_CONDITION_RETURN(m_iTodayPlayCnt >= TRANSFERJOYCITYSHOP.GetDailyMaxPlayCnt(), FALSE);

	return TRUE;
}


