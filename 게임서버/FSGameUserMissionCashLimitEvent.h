qpragma once

using namespace MissionCashLimitEvent;

typedef set<BYTE/*MissionIndex*/>		SET_MISSIONCASHLIMIT_CLEARMISSION;

class CFSGameUser;
class CFSGameUserMissionCashLimitEvent
{
public:
	CFSGameUserMissionCashLimitEvent(CFSGameUser* pUser);
	~CFSGameUserMissionCashLimitEvent(void);

	BOOL			Load();
	void			SendUserInfo(int iServerIndex = -1);
	BYTE			GiveCashReq(BYTE btMissionIndex);
	void			GiveCashRes(BYTE btResult, BYTE btMissionIndex, int iRewardIndex);
	void			CheckAndUpdateMission(bool bWin, BOOL bIsDisconnectedMatch, BOOL bRunAway);

private:
	CFSGameUser*						m_pUser;
	BOOL								m_bDataLoaded;
	int									m_iCurrentValue[MissionCashLimitEvent::MAX_MISSION_CONDITION_TYPE];
	SET_MISSIONCASHLIMIT_CLEARMISSION	m_setClearMission;
};

