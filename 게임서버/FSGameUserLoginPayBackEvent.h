qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserLoginPayBackEvent
{
public:
	CFSGameUserLoginPayBackEvent(CFSGameUser* pUser);
	~CFSGameUserLoginPayBackEvent(void);

	BOOL				Load();
	BOOL				CheckAndPayBackCash(time_t tCurrentTime = _time64(NULL));
	void				SendLoginPayBackUserInfo();
	void				RequestPayBack();
	void				CheckUseCash(int iUseCash);
	BOOL				CheckPayBackTarget(int iCurrentDate = GetYYYYMMDD());
	int					GetPayBackCash(int iUseCash);

private:
	CFSGameUser*				m_pUser;
	BOOL						m_bDataLoaded;
	USER_LOGINPAYBACK_INFO_MAP	m_mapUserCashInfo;
};

