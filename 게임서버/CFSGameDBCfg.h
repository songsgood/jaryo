// CFSGameDBCfg.h: interface for the CFSGameDBCfg class.
//
// ���� config setting ���� (DB)
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CFSDBCFG_H__6553E849_5EEF_45DA_8D73_5E26D7A4399E__INCLUDED_)
#define AFX_CFSDBCFG_H__6553E849_5EEF_45DA_8D73_5E26D7A4399E__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "CFSDBCfgBase.h"

class CFSGameODBC;

class CFSGameDBCfg : public CFSDBCfgBase
{
public:
	CFSGameDBCfg();
	~CFSGameDBCfg();

};

#endif // !defined(AFX_CFSDBCFG_H__6553E849_5EEF_45DA_8D73_5E26D7A4399E__INCLUDED_)

