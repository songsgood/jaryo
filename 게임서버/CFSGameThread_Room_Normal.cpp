qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CFSGameUserSkill.h"
qinclude "CFSGameUserItem.h"
qinclude "MatchSvrProxy.h"
qinclude "CenterSvrProxy.h"
qinclude "ChatSvrProxy.h"
qinclude "LobbyChatUserManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CFSGameThread::Process_FSRGetMyItemBag(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	if( NULL == pServer ) return;
	
	CFSItemShop* pItemShop = pServer->GetItemShop();
	if( NULL == pItemShop ) return;
	
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	pUser->SendMyItemBag(pItemShop);
}

// Modify for Match Server
void CFSGameThread::Process_FSApplyBagItem(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItem = (CFSGameUserItem*)pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItem);

	SAvatarInfo* pAvatar = (SAvatarInfo*)pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC)

	BYTE byItemNum = 0;
	short naItemIdx[MAX_ITEMCHANNEL_NUM] = {-1};
	int naPCRoomItemPropertyIdx1[MAX_ITEMCHANNEL_NUM] = {-1};
	int naPCRoomItemPropertyIdx2[MAX_ITEMCHANNEL_NUM] = {-1};

	memset(naItemIdx, -1, sizeof(naItemIdx));

	m_ReceivePacketBuffer.Read(&byItemNum);
	CHECK_CONDITION_RETURN_VOID(MAX_ITEMCHANNEL_NUM < byItemNum);

	for(int i=0; i<byItemNum; i++)
	{
		m_ReceivePacketBuffer.Read(&naItemIdx[i]);
		m_ReceivePacketBuffer.Read(&naPCRoomItemPropertyIdx1[i]);
		m_ReceivePacketBuffer.Read(&naPCRoomItemPropertyIdx2[i]);
	}

	BYTE btApplyAccItemCnt = 0;
	int iApplyAccItemIdx[MAX_USER_APPLY_ACCITEM_PROPERTY] = {-1,-1,-1};

	m_ReceivePacketBuffer.Read(&btApplyAccItemCnt);
	for(int i=0; i<btApplyAccItemCnt; i++)
	{
		m_ReceivePacketBuffer.Read(&iApplyAccItemIdx[i]);
	}

	int iOriginalStyleIndex = pUser->GetUseStyleIndex();
	int iUseStyleIndex = 1;
	m_ReceivePacketBuffer.Read(&iUseStyleIndex);

	if( Clone_Index_Clone == pUser->CheckCloneCharacter_Index() )
	{
		iUseStyleIndex += (CLONE_CHARACTER_DEFULAT_STYLE_INDEX-1);
	}

	if(iUseStyleIndex > 0 && iUseStyleIndex <= MAX_STYLE_COUNT)
	{
		if(ODBC_RETURN_SUCCESS == pGameODBC->AVATAR_ChangeUseStyleIndex(pUser->GetGameIDIndex(), iUseStyleIndex))
		{
			pUser->SetUseStyleIndex(iUseStyleIndex, FALSE);
		}
	}

	int iSpecialPartsNum = 0;
	int	iSpecialPartsIndex = -1;
	int iStatNum = 0;
	int iProperty = PROPERTY_NONE;
	int iPropertyCheck[MAX_STAT_TYPE_COUNT] = {0};
	vector<int/*iProperty*/> vProperty;
	map<int/*iSpecialPartsIndex*/, vector<int/*iProperty*/>> mapProperty;

	for(int i = 0; i < MAX_SPECIAL_PARTS_USE_PROPERTY_INDEX_COUNT; ++i)
	{
		iSpecialPartsIndex = -1;
		iStatNum = 0;
		iProperty = PROPERTY_NONE;
		ZeroMemory(iPropertyCheck, sizeof(int)*MAX_STAT_TYPE_COUNT);
		vProperty.clear();

		m_ReceivePacketBuffer.Read(&iSpecialPartsIndex);

		for(int j = 0; j < MAX_SPECIAL_PARTS_PROPERTYINDEX_COUNT; ++j)
		{
			m_ReceivePacketBuffer.Read(&iProperty);

			if(iSpecialPartsIndex > -1 && iProperty > -1 && iProperty < MAX_STAT_TYPE_COUNT)
			{
				++iPropertyCheck[iProperty];
			}
			else if(iSpecialPartsIndex > -1 && PROPERTY_ALL_STAT == iProperty)	// 모든능력치
			{
				for(int i = 0; i < MAX_STAT_TYPE_COUNT ; i++)
				{
					++iPropertyCheck[i];
				}
			}
			else
			{
				continue;
			}
		
			map<int/*iSpecialPartsIndex*/, vector<int/*iProperty*/>>::iterator iter = mapProperty.find(iSpecialPartsIndex);
			if(iter != mapProperty.end())
			{
				 vector<int/*iProperty*/> vProperty = iter->second;
				 vProperty.push_back(iProperty);
				 mapProperty[iSpecialPartsIndex] = vProperty;
			}
			else
			{
				vProperty.push_back(iProperty);
				mapProperty.insert(map<int/*iSpecialPartsIndex*/, vector<int/*iProperty*/>>::value_type(iSpecialPartsIndex, vProperty));
			}

			for(int j = 0; j < MAX_STAT_TYPE_COUNT; ++j)
			{
				if(iPropertyCheck[j] > 1)
				{
					return; // 중복 능력치 설정 시도
				}
			}
		}
	}

	if(FALSE == pUserItem->ApplyBagItem(pAvatar, naItemIdx, byItemNum, iApplyAccItemIdx, naPCRoomItemPropertyIdx1, naPCRoomItemPropertyIdx2, mapProperty)) 
	{
		if(ODBC_RETURN_SUCCESS == pGameODBC->AVATAR_ChangeUseStyleIndex(pUser->GetGameIDIndex(), iOriginalStyleIndex))
		{
			pUser->SetUseStyleIndex(iOriginalStyleIndex);
		}
		return;
	}

	pUser->SendAvatarInfoUpdateToMatch();
}

void CFSGameThread::Process_FSRGetMySkillBag(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	if( NULL == pServer ) return;
	
	CFSSkillShop *pSkillShop = pServer->GetSkillShop();
	if( NULL == pSkillShop ) return;
	
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	pUser->SendMySkillBag(pSkillShop);
}

// Modify for Match Server
void CFSGameThread::Process_FSRMoveChief(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHIEF_ASSIGN_MOVE_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szName, MAX_GAMEID_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChiefAssignMoveReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSRCheckChiefAssign(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHIEF_ASSIGN_CHECK_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szName, MAX_GAMEID_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChiefAssignCheckReq(info);
	}
}
// Modify for Match Server
void CFSGameThread::Process_FSRExitRoom(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_EXIT_ROOM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iRoomType);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendExitRoomReq(info);
	}	
}

// Modify for Match Server
void CFSGameThread::Process_FSRForceOutUser(CFSGameClient* pClient)
{
	if(TRUE == CFSGameServer::GetInstance()->IsNoWithdrawChannel())
	{
		int iErrorMsg = ERROR_MSG_TYPE_EXIT_BEGINNER;
		CPacketComposer PacketComposer(S2C_ERROR_MSG_RES);
		PacketComposer.Add(iErrorMsg);
		pClient->Send(&PacketComposer);

		return;
	}

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_FORCEOUT_USER_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szBannedGameID, MAX_GAMEID_LENGTH+1);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendForceOutUserReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSRRequestTeamMemberList(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC( ODBC_BASE );
		CHECK_NULL_POINTER_VOID(pODBCBase);

		SG2M_ACTION_SLOT_UPDATE info;
		SG2M_BASE infobase;

		infobase.iGameIDIndex = info.iGameIDIndex = pUser->GetGameIDIndex();
		infobase.btServerIndex = info.btServerIndex = _GetServerIndex;

		pUser->CheckExpireAction(pUser->GetGameIDIndex(), pServer->GetActionShop());
		pUser->CopyActionSlotPacket(pODBCBase, pServer->GetActionShop(), info);

		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ACTION_SLOT, &info, sizeof(SG2M_ACTION_SLOT_UPDATE));
		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TEAM_MEMBER_LIST_REQ, &infobase, sizeof(SG2M_BASE));		
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSRRequestTeamChange(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TEAM_CHANGE_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read(&info.iDestTeamIdx);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendTeamChangeReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSRChangeRoomPassword(CFSGameClient *pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_TEAM_PASSWORD_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((BYTE*)info.szPassword, MAX_TEAM_PASS_LENGTH+1);
	m_ReceivePacketBuffer.Read(&info.iRoomType);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeTeamPasswordReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSChangeGameRoomName(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_CHANGE_GAMEROOM_NAME_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read((BYTE*)info.szRoomName, sizeof(char)*(MAX_ROOM_NAME_LENGTH+1));

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendChangeGameRoomNameReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::ProcessFSRTeamEstimate(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	if(pMatch) 
	{
		pMatch->SendTeamEstimateReq(info);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CHAT_ROOM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2T_BASE	info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_EXIT_LOBBY_CHAT_ROOM_REQ, &info, sizeof(SS2T_BASE));

	pUser->ExitChatChannel();
}