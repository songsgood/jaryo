qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserRandomArrowEvent
{
public:
	CFSGameUserRandomArrowEvent(CFSGameUser* pUser);
	~CFSGameUserRandomArrowEvent(void);

	BOOL				Load();
	void				SendEventInfo_Product();
	void				SendEventInfo_Pot();
	void				SendUserInfo();
	void				SetArrowCount(int iCount);
	BOOL				BuyRandomArrow_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL				UseArrow(SC2S_RANDOMARROW_USE_ARROW_REQ& rs);
	BOOL				UsePot(SC2S_RANDOMARROW_USE_POT_REQ& rs);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;
	BOOL				m_bIsFirstLogin;
	int					m_iMyArrowCnt;
	int					m_iMyPotCnt;
};

