qpragma once

class CUser;
class CFSODBCBase;

typedef set<int/*iRewardIndex*/> UNLIMITED_TATOO_REWARD_LOG_SET;

class CFSGameUserUnlimitedTatooEvent
{
public:
	CFSGameUserUnlimitedTatooEvent(CUser* pUser);
	~CFSGameUserUnlimitedTatooEvent(void);

	BOOL Load();

	void SendEventInfo();
	RESULT_EVENT_UNLIMITED_TATOO_GET_REWARD GetRewardReq(int iRewardIndex);

private:
	CUser* m_pUser;
	BOOL m_bDataLoaded;

	UNLIMITED_TATOO_REWARD_LOG_SET m_setRewardLog;
};

