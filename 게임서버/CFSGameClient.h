// CFSClient.h: interface for the CFSClient class.
//
// 통신 소켓 클라이언트
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CFSCLIENT_H__6ECD4C61_98E4_4834_90FA_2C555C3443B0__INCLUDED_)
#define AFX_CFSCLIENT_H__6ECD4C61_98E4_4834_90FA_2C555C3443B0__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "BaseClient.h"
qinclude "Lock.h"

class CFSGameClient : public CBaseClient  
{
public:
	CFSGameClient();
	virtual ~CFSGameClient();

	void					CleanupInstance();
	void					InitializeInstance();
	CUser*					CreateUser();
	
	BOOL					CheckTimeOut(DWORD dwTick);
	void					DisconnectClient();

	void					SendKey();	

	void					SetCheatCode(int iCode, int Request1, int Request2 );	
	void					GetCheatCode(int &iCode, int &Request1, int &Request2 );	

	void					SendEnterLobbyRes(BOOL bResult, int iErrorCode);

	void					SendPacket(WORD wCommand, PVOID info, int iSize, BOOL IsSend = TRUE); 

	static void				InitializeAvailableUserClientState();
	virtual BOOL			SetState(NEXUS_CLIENT_STATE	eState);

	void					SendBuyItemRes(BOOL bResult, int iErrorCode, int iItemIDNumber);

private:
	INT						m_aCheatCode[3];
	char					m_szCheatUserName[10+1];

	LOCK_RESOURCE(m_csClientState);	//CBaseClient::m_ClientState 동기화를 위함

	static BOOL AVAILABLE_USER_CLIENT_STATE[MAX_USER_CLIENT_STATE][MAX_USER_CLIENT_STATE];
};

#endif // !defined(AFX_CFSCLIENT_H__6ECD4C61_98E4_4834_90FA_2C555C3443B0__INCLUDED_)
