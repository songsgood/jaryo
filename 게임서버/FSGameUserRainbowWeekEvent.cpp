qinclude "stdafx.h"
qinclude "FSGameUserRainbowWeekEvent.h"
qinclude "RainbowWeekEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserRainbowWeekEvent::CFSGameUserRainbowWeekEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iRecvRewardIndex(0)
	, m_iUpdateDate(0)
	, m_iUpdateTime(0)
	, m_btCurrentRewardStatus(EVENT_RAINBOW_WEEK_REWARD_STATUS_NONE)
{
}

CFSGameUserRainbowWeekEvent::~CFSGameUserRainbowWeekEvent(void)
{
}

BOOL CFSGameUserRainbowWeekEvent::Load()
{
	CFSEventODBC* pODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_RAINBOW_WEEK_GetUserData(m_pUser->GetUserIDIndex(), m_iRecvRewardIndex, m_iUpdateDate, m_iUpdateTime))
	{
		WRITE_LOG_NEW(LOG_TYPE_RAINBOW_WEEK_EVENT, INITIALIZE_DATA, FAIL, "EVENT_RAINBOW_WEEK_CheckRewardLog error");
		return FALSE;
	}

	UpdateUserEventStatus();

	m_bDataLoaded = TRUE;

	return TRUE;
}

BOOL CFSGameUserRainbowWeekEvent::CheckUpdateTime()
{
	CHECK_CONDITION_RETURN(m_iRecvRewardIndex == 0, FALSE);

	time_t tCurrentTime = _time64(NULL);
	time_t tYesterDate = tCurrentTime - (60*60*24);

	TIMESTAMP_STRUCT CurrentDate;
	TimetToTimeStruct(tCurrentTime, CurrentDate);

	const int iResetHour = EVENT_RESET_HOUR;

	int iYesterYYMMDD = TimetToYYYYMMDD(tYesterDate);
	int iHour = CurrentDate.hour;

	int iYYMMDD = TimetToYYYYMMDD(tCurrentTime);
	CHECK_CONDITION_RETURN(iHour < iResetHour && ((m_iUpdateDate == iYesterYYMMDD && m_iUpdateTime >= iResetHour) || m_iUpdateDate >= iYYMMDD), FALSE);
	CHECK_CONDITION_RETURN(iHour >= iResetHour && ((m_iUpdateDate == iYYMMDD && m_iUpdateTime >= iResetHour) || m_iUpdateDate > iYYMMDD), FALSE);

	return TRUE;
}

void CFSGameUserRainbowWeekEvent::UpdateUserEventStatus()
{
	if(m_iRecvRewardIndex > 0 &&
		FALSE == CheckUpdateTime())
	{
		m_btCurrentRewardStatus = EVENT_RAINBOW_WEEK_REWARD_STATUS_COMPLETED;
	}
}

int CFSGameUserRainbowWeekEvent::GetCurrentEventWeek()
{
	SYSTEMTIME CurrentDate;
	::GetLocalTime(&CurrentDate);

	time_t tCurrentTime = SystemTimeToTimet(&CurrentDate);

	const int iResetHour = EVENT_RESET_HOUR;
	int iHour = CurrentDate.wHour;

	if(iHour < iResetHour)
	{
		int iYesterWeek = CurrentDate.wDayOfWeek - 1;
		if(CurrentDate.wDayOfWeek == 0)
			iYesterWeek = 6;

		return iYesterWeek;
	}

	return CurrentDate.wDayOfWeek;
}

void CFSGameUserRainbowWeekEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RAINBOWWEEKEVENT.IsOpen());

	UpdateUserEventStatus();

	SS2C_EVENT_RAINBOW_WEEK_INFO_RES rs;
	rs.btRewardStatus = m_btCurrentRewardStatus;
	RAINBOWWEEKEVENT.MakeRewardInfo(GetCurrentEventWeek(), rs);
	m_pUser->Send(S2C_EVENT_RAINBOW_WEEK_INFO_RES, &rs, sizeof(SS2C_EVENT_RAINBOW_WEEK_INFO_RES));
}

RESULT_EVENT_RAINBOW_GET_REWARD CFSGameUserRainbowWeekEvent::GetRewardReq(int iRewardIndex)
{
	CHECK_CONDITION_RETURN(CLOSED == RAINBOWWEEKEVENT.IsOpen(), RESULT_EVENT_RAINBOW_GET_REWARD_FAIL);
	
	UpdateUserEventStatus();
	CHECK_CONDITION_RETURN(iRewardIndex != RAINBOWWEEKEVENT.GetRewardIndex(GetCurrentEventWeek()), RESULT_EVENT_RAINBOW_GET_REWARD_FAIL_CHANGED_DAYOFWEEK);
	CHECK_CONDITION_RETURN(m_btCurrentRewardStatus != EVENT_RAINBOW_WEEK_REWARD_STATUS_NONE, RESULT_EVENT_RAINBOW_GET_REWARD_FAIL);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_RAINBOW_GET_REWARD_FAIL_FULL_MAILBOX);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_RAINBOW_GET_REWARD_FAIL);

	CFSEventODBC* pODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_RAINBOW_GET_REWARD_FAIL);

	time_t tCurrentTime = _time64(NULL);

	SYSTEMTIME CurrentDate;
	TimetToSystemTime(tCurrentTime, CurrentDate);

	int iUpdateDate = TimetToYYYYMMDD(tCurrentTime);
	int iUpdateTime = CurrentDate.wHour;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_RAINBOW_WEEK_SetUserData(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex, iUpdateDate, iUpdateTime))
	{
		WRITE_LOG_NEW(LOG_TYPE_RAINBOW_WEEK_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_RAINBOW_WEEK_InsertRewardLog error");
		return RESULT_EVENT_RAINBOW_GET_REWARD_FAIL;
	}

	pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_RAINBOW_GET_REWARD_FAIL);

	m_iRecvRewardIndex = iRewardIndex;
	m_iUpdateDate = iUpdateDate;
	m_iUpdateTime = iUpdateTime;
	m_btCurrentRewardStatus = EVENT_RAINBOW_WEEK_REWARD_STATUS_COMPLETED;

	return RESULT_EVENT_RAINBOW_GET_REWARD_SUCCESS;
}
