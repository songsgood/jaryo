qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "MatchSvrProxy.h"
qinclude "RewardCardManager.h"
qinclude "RewardManager.h"
qinclude "MatchingPoolCareManager.h"
qinclude "CenterSvrProxy.h"
qinclude "HackingManager.h"
qinclude "EventDateManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

///////////////////////////////// Process Game /////////////////////////////

// Modify for Match Server
void CFSGameThread::Process_FSGInitialGame(CFSGameClient *pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
		
	SG2M_INITIALGAME_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.iSuccess);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_INITIALGAME_RES, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGPlayReadyOk(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if(NULL == pUser)
	{
		WRITE_LOG_NEW(LOG_TYPE_MATCH, INVALED_DATA, CHECK_FAIL, "Process_FSGPlayReadyOk:pUser");
		return;
	}

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	pUser->GetUserCheerLeader()->CheckExpiredCheerLeader();

	SG2M_ADD_STAT_UPDATE update;
	ZeroMemory(&update, sizeof(SG2M_ADD_STAT_UPDATE));
	update.iGameIDIndex = pUser->GetGameIDIndex();
	update.btServerIndex = pServer->GetServerIndex();
	update.bWearPropertyItem = FALSE;
	memcpy(&update.AvatarInfo, pUser->GetCurUsedAvatar(), sizeof(SAvatarInfo));
	pUser->GetAvatarItemStatus(update.iAvatarAddStat, update.bWearPropertyItem);
	pUser->SetAvatarFameSkillStatus(update.iAvatarAddStat);
	pUser->GetAvatarLinkItemStatus(update.iLinkItemAddStat);	
	update.bySecondBoostItemCount = (BYTE)pUser->IsSecondBoostItemEquipped(update.byAbilityType);
	pUser->CheckExpPointBonusItem(update.iExpItemBonusRate, update.iSkillItemBonusRate, update.iFameItemBonusRate);
	pUser->CheckExpPointBonusWinItem(update.iExpWinItemBonusRate, update.iSkillWinItemBonusRate);
	pUser->CheckPremiumItemExpPointBonus(update.iExpPremiumItemBonusRate, update.iSkillPremiumItemBonusRate);
	pUser->CheckMatchingPoolCareExpPointBonus(update.iExpMatchingPoolCareBonusRate, update.iSkillMatchingPoolCareBonusRate);

	if(TRUE == pUser->CheckMatchingPoolCareUser() &&
		TRUE == MATCHINGPOOLCAREMANAGER.CheckTimeEndTarget(pUser->GetGameIDIndex(), pUser->GetCurUsedAvtarLvGrade()))
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if(pCenter != NULL) 
		{
			SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ ss;
			ss.iGameIDIndex = pUser->GetGameIDIndex();
			strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
			ss.btUpdateType = MATCHINGPOOLCARE_UPDATE_TARGETUSER_TYPE_REMOVE;
			ss.btLvGrade = pUser->GetCurUsedAvtarLvGrade();
			pCenter->SendPacket(G2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ, &ss, sizeof(SG2S_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ));
		}
		update.iExpMatchingPoolCareBonusRate	= 0;
		update.iSkillMatchingPoolCareBonusRate	= 0;
	}
	update.bFinishedWordPuzzle = pUser->GetUserWordPuzzle()->CheckFinished();
	update.iPuzzleNextRewardIndex = pUser->GetUserPuzzle()->GetNextGameResultRewardIndex();

	pUser->GetUserCharacterCollection()->GetSentenceStatus(update);
	pUser->GetSpecialPartsOptionProperty(update);
	update.bEnableLoadingPattern = pUser->GetUserAppraisal()->CheckLoadingPatternGoodPoint(pMatch->GetMatchType());
	pUser->GetUserPowerupCapsule()->GetPowerupCapsuleStatus(update.iAvatarAddStat);
	pUser->GetCheerLeaderStatus(update.iAvatarAddStat);
	pUser->GetLvIntervalBuffItemStatus(update.iAvatarAddStat);
	pUser->GetAvatarSpecialPieceProperty(update.iAvatarAddStat);
	pUser->GetDragonTigerCloneInfo(update.AvatarInfo, update.iaDragonTigerCloneStat);		
	update.bGiveEventSummerCandy = pUser->GetUserSummerCandyEvent()->CheckGiveRewardCandy();
	update.bGiveEventTransferJoyCityCoin = pUser->GetUserTransferJoycityShopEvent()->CheckGameEndGiveCoin();
	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ADD_STAT_UPDATE, &update, sizeof(update));

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();
	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_PLAY_READY_OK_REQ, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGPlayStartOK(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);
	
	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_GAME_START_OK_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.iShootSeed);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_GAME_START_OK_REQ, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGRequestScoreChange(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_SCORE_ADD_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.dwUserID);
	m_ReceivePacketBuffer.Read(&info.dwDeltaScore);
	m_ReceivePacketBuffer.Read(&info.dwAddPoint);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_SCORE_ADD_REQ, &info, sizeof(info));	
}

// Modify for Match Server
void CFSGameThread::Process_FSGQuarterEndAck(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_QUATER_END_ACK, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGShootTry(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	SG2M_SHOOT_TRY info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.iShootPoint);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_SHOOT_TRY, &info, sizeof(info));
}

void CFSGameThread::Process_FSGShootSuccessorFailure(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_SHOOT_SUCCESS_OR_FAILURE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.iShootSuccess);
	m_ReceivePacketBuffer.Read(&info.iShootPoint);
	m_ReceivePacketBuffer.Read(&info.iShootType);
	m_ReceivePacketBuffer.Read(&info.dwCharID);
	m_ReceivePacketBuffer.Read(&info.btDunkType);	// 0:举府矿耽农酒丛, 1:举府矿耽农

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_SHOOT_SUCCESS_OR_FAILURE, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGRebound(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DWORD dwIsOffence = 0;
	m_ReceivePacketBuffer.Read(&dwIsOffence);

	SG2M_REBOUND info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();
	info.dwIsOffence = dwIsOffence;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_REBOUND, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGAsist(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);
	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_ASSIST info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.iUserID);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_ASSIST, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGSteal(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_STEAL, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGBlock(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BLOCK_SUCCESS info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.sBlockType);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_BLOCK, &info, sizeof(info));
}

void CFSGameThread::Process_FSGJumpBall(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_JUMPBALL, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGSendGameResult(CFSGameClient* pClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_GAME_RESULT_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.shMode);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_GAME_RESULT_REQ, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGDisconnectedGame(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);
	
	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_PEER_DISCONNECTED_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	if(U_GAME == pUser->GetLocation())
	{
		m_ReceivePacketBuffer.Read(&info.iReason);
		m_ReceivePacketBuffer.Read(&info.dwTime);
	}

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_PEER_DISCONNECTED_REQ, &info, sizeof(info));
}

// Modify for Match server
void CFSGameThread::Process_FSGTerminateGame(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_GAME_RESULT_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.shMode);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_GAME_TERMINATED_RES, &info, sizeof(info));	
}

// Modify for Match server
void CFSGameThread::Process_FSGChangeGameStatus(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_EXTRA_QUARTER_START, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGSetGameTime(CFSGameClient * pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_GAME_TIME_RES info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	m_ReceivePacketBuffer.Read(&info.iPlayTime);

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_GAME_TIME_RES, &info, sizeof(info));
}

// Modify for Match Server
void CFSGameThread::Process_FSGamePause(CFSGameClient *pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	pMatch->SendGamePauseReq(info);
}

void CFSGameThread::Process_FSGUseHackNotifyRes(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

 	SG2M_FSG_USEHACK_NOTIRYRES info;
 	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	m_ReceivePacketBuffer.Read(&info.iErrorCode);
	m_ReceivePacketBuffer.Read((PBYTE)info.szHostGameID, sizeof(info.szHostGameID));
 
	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_FSG_USEHACK_NOTIRYRES, &info, sizeof(info));
}

void CFSGameThread::Process_FSRExposeReq( CFSGameClient* pClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	const int iTypeNameLen = 32+1, iCommentLen = 128+1;
	const int iChannel = CFSGameServer::GetInstance()->GetProcessID();
	const int iUserIDIndex = pUser->GetUserIDIndex(), iGameIDIndex = pUser->GetGameIDIndex(); 

	DECLARE_INIT_TCHAR_ARRAY( szBeExposeGameID, MAX_GAMEID_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY( szExposeTypeName, iTypeNameLen );
	DECLARE_INIT_TCHAR_ARRAY( szExposeComment, iCommentLen );

	SG2M_EXPOSE_REQ info;

	m_ReceivePacketBuffer.Read((PBYTE)info.szBeExposeGameID, MAX_GAMEID_LENGTH+1);
	m_ReceivePacketBuffer.Read((PBYTE)info.szExposeTypeName, iTypeNameLen );
	m_ReceivePacketBuffer.Read((PBYTE)info.szExposeComment, iCommentLen );

	CScopedRefGameUser pBeExposeUser(szBeExposeGameID);
	CHECK_NULL_POINTER_VOID(pBeExposeUser);

	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.iBeExposeUserIDIndex = pBeExposeUser->GetUserIDIndex();
	info.iBeExposeGameIDIndex = pBeExposeUser->GetGameIDIndex();

	pMatch->SendPacket(G2M_EXPOSE_REQ, &info, sizeof(SG2M_EXPOSE_REQ));
}

void CFSGameThread::Process_OpenPointRewardCardReq(CFSGameClient* pFSClient)
{
	// delete
	return;

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CHECK_CONDITION_RETURN_VOID(FALSE == pUser->IsAvailablePointRewardCard());
	pUser->SetAvailablePointRewardCard(FALSE);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CPacketComposer Packet(S2C_OPEN_POINT_REWARD_CARD_RES);
	int iOpenCost = REWARDCARDMANAGER.GetPointCardCost();
	OPEN_STATUS bPointCardFreeEvent = EVENTDATEMANAGER.IsOpen(EVENT_KIND_POINTCARD);
	if((pMatch->GetMatchType() == MATCH_TYPE_RATING || pMatch->GetMatchType() == MATCH_TYPE_RANKMATCH || pMatch->GetMatchType() == MATCH_TYPE_CLUB_LEAGUE) &&
		bPointCardFreeEvent == OPEN)
	{
		iOpenCost = 0;
	}
	else 
	{
		if(pUser->GetSkillPoint() < iOpenCost)
		{
			Packet.Add(RESULT_NOT_ENOUGH_POINT);
			pFSClient->Send(&Packet);
			return;
		}		
	}


	int iMatchType = pMatch->GetMatchType();
	if( MATCH_TYPE_RANKMATCH == iMatchType || MATCH_TYPE_CLUB_LEAGUE == iMatchType || MATCH_TYPE_PVE == iMatchType || MATCH_TYPE_AIPVP == iMatchType || MATCH_TYPE_PVE_3CENTER == iMatchType || MATCH_TYPE_PVE_CHANGE_GRADE == iMatchType || MATCH_TYPE_PVE_AFOOTBALL == iMatchType)
		iMatchType = MATCH_TYPE_RATING;

	int iRewardIndex = REWARDCARDMANAGER.GetRandomRewardIndex(iMatchType, REWARD_CARD_TYPE_POINT, REWARD_CARD_GROUP_TYPE_NORMAL, pUser->IsLastMatchDisconnected(), FALSE, pUser);
	if (0 == iRewardIndex)
	{
		WRITE_LOG_NEW(LOG_TYPE_REWARDCARD, GET_DATA, FAIL, "Process_OpenPointRewardCardReq::GetRandomRewardIndex -  GameIDIndex:11866902", pUser->GetGameIDIndex());
et.Add((int)RESULT_COMMON_ERROR);
		pFSClient->Send(&Packet);
		return;
	}

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(pUser, iRewardIndex);
	if (NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_REWARDCARD, EXEC_FUNCTION, FAIL, "Process_OpenPointRewardCardReq::GiveReward -  GameIDIndex:11866902", pUser->GetGameIDIndex());
et.Add((int)RESULT_COMMON_ERROR);
		pFSClient->Send(&Packet);
		return;
	}

	BYTE btRewardType = pReward->iRewardType;
	int iRewardValue = pReward->GetRewardValue();
	int iItemCount = 0;
	if(REWARD_TYPE_ITEM == btRewardType ||
		REWARD_TYPE_EXHAUST_ITEM_TO_INVEN == btRewardType || 
		REWARD_TYPE_COACH_CARD == btRewardType)
	{
		SRewardConfigItem* pRewardItem = dynamic_cast<SRewardConfigItem*>(pReward);
		if(pRewardItem)
		{
			iItemCount = pRewardItem->GetItemCount();
		}
	}

	if (iOpenCost > 0)
	{
		int iSPReturn;
		if (ODBC_RETURN_SUCCESS != (iSPReturn = pGameODBC->REWARD_CARD_ConsumePoint(pUser->GetUserIDIndex(), iOpenCost)))
		{
			Packet.Add((int)iSPReturn);
			pFSClient->Send(&Packet);
			return;
		}

		pUser->SetSkillPoint(pUser->GetSkillPoint() - iOpenCost);
		pUser->SendUserGold();
		pUser->AddPointAchievements(); 
		pUser->GetUserMissionShopEvent()->CheckMissionValue( CONDITION_TYPE_POINT_USE, iOpenCost );
		pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_POINT, iOpenCost);
		pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_POINT, iOpenCost);
		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POINT_ITEM);
		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_USEPOINT, iOpenCost);
		pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_POINT, iOpenCost);
		pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USE_POINT, iOpenCost);
	}

	Packet.Add((int)RESULT_COMMON_SUCCESS);
	Packet.Add(&btRewardType);
	Packet.Add(&iRewardValue);
	Packet.Add(&iItemCount);
	pFSClient->Send(&Packet);


 	SG2M_OPEN_POINT_REWARD_CARD_NOT info;
 	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btRewardType = btRewardType;
	info.iRewardValue = iRewardValue;
	info.iItemCount = iItemCount;

	CPacketComposer PacketComposer( G2M_OPEN_POINT_REWARD_CARD_NOT );
	PacketComposer.Add((PBYTE)&info, sizeof(SG2M_OPEN_POINT_REWARD_CARD_NOT) );
	pMatch->Send(&PacketComposer);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LEFT_SEAT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_LEFT_SEAT_REQ rq;
	m_ReceivePacketBuffer.Read(&rq.info.btReqType);

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pMatch->SendPacket(G2M_LEFT_SEAT_REQ, &rq, sizeof(SG2M_LEFT_SEAT_REQ));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTOR_STAT_MODULATION_CHECK_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if( FS_INTENSIVEPRACTICE == pFSClient->GetState() )
	{
		CHECK_CONDITION_RETURN_VOID(FALSE == HACKINGMANAGER.IsEnable(HACK_TYPE_SOME_ACTION_CHANGE_STATUS_INTENSIVEPRACTICE));
		pUser->ProcessCheckAvatarSomeAction_ChangeStatus(m_ReceivePacketBuffer.GetBuffer() , m_ReceivePacketBuffer.GetDataSize()
			,HACK_TYPE_SOME_ACTION_CHANGE_STATUS_INTENSIVEPRACTICE );
	}
	else
	{
		CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		CHECK_NULL_POINTER_VOID(pMatch);

		CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
		CHECK_NULL_POINTER_VOID(pServer);

		CPacketComposer Packet(G2M_CHARACTOR_STAT_MODULATION_CHECK_REQ);

		SG2M_BASE info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.btServerIndex = pServer->GetServerIndex();

		int iDataSize = m_ReceivePacketBuffer.GetRemainedDataSize();
		Packet.Add((BYTE*)&info, sizeof(SG2M_BASE));
		Packet.Add(iDataSize);
		Packet.Add(m_ReceivePacketBuffer.GetRemainedData(), m_ReceivePacketBuffer.GetRemainedDataSize());

		pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_VARIABLE_FUNC(G2M_CHARACTOR_STAT_MODULATION_CHECK_REQ, &Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOSEBALL_CATCH)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = pServer->GetServerIndex();

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_LOOSEBALL, &info, sizeof(info));
}


DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_GAME_EXIT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2L_RANKMATCH_GAME_EXIT_REQ, &info, sizeof(info));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_TRANSFORM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SC2S_TRANSFORM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_TRANSFORM_REQ));

	SG2M_TRANSFORM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;
	info.info.bUseMotion = rq.bUseMotion;
	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TRANSFORM_REQ, &info, sizeof(info));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_TRANSFORM_DISABLE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TRANSFORM_DISABLE_REQ, &info, sizeof(info));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_TRANSFORM_STATUS_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = _GetServerIndex;

	pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_TRANSFORM_STATUS_REQ, &info, sizeof(info));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_INGAME_SCORE_ADD_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SC2S_PVE_INGAME_SCORE_ADD_NOT rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs,sizeof(SC2S_PVE_INGAME_SCORE_ADD_NOT));

	SG2M_PVE_INGAME_SCORE_ADD_NOT not;

	not.iGameIDIndex = pUser->GetGameIDIndex();
	not.btServerIndex = _GetServerIndex;
	not.info.dwCharID = rs.dwCharID;
	not.info.iRecordType = rs.iRecordType;
	not.info.btBlockType = rs.btBlockType;
	not.info.dwDiffCharID = rs.dwDiffCharID;

	CPacketComposer Packet(G2M_PVE_INGAME_SCORE_ADD_NOT);
	Packet.Add((PBYTE)&not, sizeof(SG2M_PVE_INGAME_SCORE_ADD_NOT));
	pMatch->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_FALL_DOWN_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_FALL_DOWN_NOT rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_PVE_FALL_DOWN_NOT));

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_PVE_FALL_DOWN_NOT not;
	not.info.dwDiffCharID = rs.dwDiffCharID;
	not.iGameIDIndex = pUser->GetGameIDIndex();
	not.btServerIndex = _GetServerIndex;

	CPacketComposer Packet(G2M_PVE_FALL_DOWN_NOT);
	Packet.Add((PBYTE)&not, sizeof(SG2M_BASE));
	pMatch->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_DROP_BALL_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SG2M_BASE not;
	not.iGameIDIndex = pUser->GetGameIDIndex();
	not.btServerIndex = _GetServerIndex;

	CPacketComposer Packet(G2M_PVE_DROP_BALL_NOT);
	Packet.Add((PBYTE)&not, sizeof(SG2M_BASE));
	pMatch->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_CHEAP_OUT_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SC2S_PVE_CHEAP_OUT_NOT rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_CHEAP_OUT_NOT));

	SG2M_PVE_CHEAP_OUT_NOT not;
	not.iGameIDIndex = pUser->GetGameIDIndex();
	not.btServerIndex = _GetServerIndex;
	not.btResult = rq.btResult;

	CPacketComposer Packet(G2M_PVE_CHEAP_OUT_NOT);
	Packet.Add((PBYTE)&not, sizeof(SG2M_PVE_CHEAP_OUT_NOT));
	pMatch->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SC2S_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT info;
	m_ReceivePacketBuffer.Read((PBYTE)&info , sizeof(SC2S_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT));

	CPacketComposer Packet(G2M_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT);
	SG2M_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT not;

	not.dwVoteID = info.dwVoteID;
	not.iGameIDIndex = pUser->GetGameIDIndex();
	not.btServerIndex = _GetServerIndex;
	Packet.Add((PBYTE)&not,sizeof(SG2M_GAME_RESULT_BEST_PLAYER_APPRAISAL_VOTE_NOT));

	pMatch->Send(&Packet);
}