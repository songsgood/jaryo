// CFSGameService.h: interface for the CFSGameService class.
//
// 서비스 가동 부분
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CFSSERVICE_H__C360ED3E_EE41_4337_8530_1D9191963BA3__INCLUDED_)
#define AFX_CFSSERVICE_H__C360ED3E_EE41_4337_8530_1D9191963BA3__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "CIOCPService.h"

class CFSGameService : public CIOCPService  
{
public:
	CFSGameService(CIOCP* pIOCP);
	~CFSGameService();

protected:
	CServer* CreateServer(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv);
};

#endif // !defined(AFX_CFSSERVICE_H__C360ED3E_EE41_4337_8530_1D9191963BA3__INCLUDED_)
