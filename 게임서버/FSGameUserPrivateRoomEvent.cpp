qinclude "stdafx.h"
qinclude "PrivateRoomEventManager.h"
qinclude "FSGameUserPrivateRoomEvent.h"
qinclude "CFSGameUser.h"
qinclude "FSServer.h"
qinclude "RewardManager.h"

CFSGameUserPrivateRoomEvent::CFSGameUserPrivateRoomEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iTicketCount(0)
	, m_iTodayTicketCount(0)
	, m_iUpdateDate(0)
	, m_iUpdateHour(0)
{

}

CFSGameUserPrivateRoomEvent::~CFSGameUserPrivateRoomEvent(void)
{
}

BOOL CFSGameUserPrivateRoomEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == PRIVATEROOM.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<SEventPrivateRoomUserMission> vUserMission;
	vector<int> vProductIndex;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_PRIVATEROOM_GetUserTicket(m_pUser->GetUserIDIndex(), m_iTicketCount) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_PRIVATEROOM_GetUserMission(m_pUser->GetUserIDIndex(), vUserMission) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_PRIVATEROOM_GetUserProductGetLog(m_pUser->GetUserIDIndex(), vProductIndex) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_PRIVATEROOM_GetUserUpdateTime(m_pUser->GetUserIDIndex(), m_iTodayTicketCount, m_iUpdateDate, m_iUpdateHour))
	{
		WRITE_LOG_NEW(LOG_TYPE_PRIVATEROOM_EVENT, INITIALIZE_DATA, FAIL, "EVENT_PRIVATEROOM_GetUserTicket error");
		return FALSE;
	}

	SetUserMission(vUserMission);
	SetUserProductGetLog(vProductIndex);

	ResetMission();

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserPrivateRoomEvent::SetUserMission(vector<SEventPrivateRoomUserMission>& vUserMission)
{
	for(int i = 0; i < vUserMission.size(); ++i)
	{
		m_mapUserMission[vUserMission[i]._btMissionType] = vUserMission[i];
	}
}

void CFSGameUserPrivateRoomEvent::SetUserProductGetLog(vector<int>& vProductIndex)
{
	for(int i = 0; i < vProductIndex.size(); ++i)
	{
		m_setUserProductGetLog.insert(vProductIndex[i]);
	}
}

void CFSGameUserPrivateRoomEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PRIVATEROOM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	ResetMission();

	SS2C_EVENT_PRIVATEROOM_INFO_RES rs;
	rs.iCurrentTicktCount = m_iTicketCount;
	rs.iMaxTodayTicketCount = EVENT_PRIVATEROOM_TODAY_MAX_TICKET_COUNT;
	rs.iTodayTicketCount = m_iTodayTicketCount;
	rs.iProductCount = 0;
	rs.iMissionCount = 0;

	CPacketComposer Packet(S2C_EVENT_PRIVATEROOM_INFO_RES);
	PRIVATEROOM.MakeProductandMissionList(Packet, rs, m_setUserProductGetLog, m_mapUserMission);
	m_pUser->Send(&Packet);
}

void CFSGameUserPrivateRoomEvent::SendExchangeItemInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PRIVATEROOM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_EVENT_PRIVATEROOM_EXCHANGEITEM_INFO_RES rs;
	rs.iCurrentTicktCount = m_iTicketCount;
	rs.iExchangeItemCount = 0;

	CPacketComposer Packet(S2C_EVENT_PRIVATEROOM_EXCHANGEITEM_INFO_RES);
	PRIVATEROOM.MakeExchangeItemList(Packet, rs);
	m_pUser->Send(&Packet);
}

RESULT_EVENT_PRIVATEROOM_GET_PRODUCT CFSGameUserPrivateRoomEvent::GetProductReq(int iProductIndex, int iPropertyValue)
{
	CHECK_CONDITION_RETURN(CLOSED == PRIVATEROOM.IsOpen(), RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_FAIL);

	PRIVATEROOM_USER_PRODUCT_GET_LOG_SET::iterator iter = m_setUserProductGetLog.find(iProductIndex);
	CHECK_CONDITION_RETURN(iter != m_setUserProductGetLog.end(), RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_ALREADY_GET_REWARD);

	int iNeedTicketCount = PRIVATEROOM.GetProductNeedTicketCount(iProductIndex, iPropertyValue);
	CHECK_CONDITION_RETURN(0 == iNeedTicketCount, RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_FAIL);
	CHECK_CONDITION_RETURN(iNeedTicketCount > m_iTicketCount, RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_NOT_EXIST_TICKET);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_MAX_MAILBOX);

	CFSEventODBC* pEventODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pEventODBC, RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_FAIL);

	int iUpdatedTicketCount = m_iTicketCount - iNeedTicketCount;
	if(iUpdatedTicketCount < 0)
		iUpdatedTicketCount = 0;

	int iRewardIndex = PRIVATEROOM.GetProductRewardIndex(iProductIndex, iPropertyValue, m_pUser->GetCurUsedAvatarSex());
	CHECK_CONDITION_RETURN(0 == iRewardIndex, RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_FAIL);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_PRIVATEROOM_UpdateUserTicket(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iUpdatedTicketCount, 0, iProductIndex, EVENT_PRIVATEROOM_LOGTYPE_USE_TICKET, 0, 0, iRewardIndex, 0), RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_FAIL);

	m_iTicketCount = iUpdatedTicketCount;
	m_setUserProductGetLog.insert(iProductIndex);

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_FAIL);

	return RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_SUCCESS;
}

RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM CFSGameUserPrivateRoomEvent::GetExchangeItemReq(int iRewardIndex, int& iUpdatedTicketCount)
{
	CHECK_CONDITION_RETURN(CLOSED == PRIVATEROOM.IsOpen(), RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_FAIL);

	int iNeedTicketCount = PRIVATEROOM.GetExchangeProductNeedTicketCount(iRewardIndex);
	CHECK_CONDITION_RETURN(0 == iNeedTicketCount, RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_FAIL);
	CHECK_CONDITION_RETURN(iNeedTicketCount > m_iTicketCount, RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_NOT_EXIST_TICKET);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_MAX_MAILBOX);

	CFSEventODBC* pEventODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_CONDITION_RETURN(NULL == pEventODBC, RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_FAIL);

	iUpdatedTicketCount = m_iTicketCount - iNeedTicketCount;
	if(iUpdatedTicketCount < 0)
		iUpdatedTicketCount = 0;

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_PRIVATEROOM_UpdateUserTicket(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iUpdatedTicketCount, 0, 0, EVENT_PRIVATEROOM_LOGTYPE_USE_TICKET, 0, 0, iRewardIndex, 0), RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_FAIL);

	m_iTicketCount = iUpdatedTicketCount;

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_FAIL);

	return RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_SUCCESS;
}

void CFSGameUserPrivateRoomEvent::ResetMission()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PRIVATEROOM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	time_t tCurrentDate = _time64(NULL);
	TIMESTAMP_STRUCT CurrentDate;
	TimetToTimeStruct(tCurrentDate, CurrentDate);

	time_t tYesterDate = tCurrentDate - (60*60*24);

	int iYesterYYMMDD = TimetToYYYYMMDD(tYesterDate);
	int iYYMMDD = TimetToYYYYMMDD(tCurrentDate);

	int iHour = CurrentDate.hour;
	const int iResetHour = 7;

	if(0 < m_iUpdateDate)
	{
		CHECK_CONDITION_RETURN_VOID(iHour < iResetHour && ((m_iUpdateDate == iYesterYYMMDD && m_iUpdateHour >= iResetHour) || m_iUpdateDate >= iYYMMDD));
		CHECK_CONDITION_RETURN_VOID(iHour >= iResetHour && ((m_iUpdateDate == iYYMMDD && m_iUpdateHour >= iResetHour) || m_iUpdateDate > iYYMMDD));
	}

	CFSEventODBC* pEventODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_VOID(pEventODBC);

	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_PRIVATEROOM_ResetUserMission(m_pUser->GetUserIDIndex(), iYYMMDD, iHour));
	
	m_iTodayTicketCount = 0;
	m_iUpdateDate = iYYMMDD;
	m_iUpdateHour = iHour;

	m_mapUserMission.clear();
	
	CheckMission(EVENT_PRIVATEROOM_MISSION_TYPE_LOGIN_COUNT);
}

void CFSGameUserPrivateRoomEvent::CheckMission(BYTE btMissionType)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PRIVATEROOM.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(m_iTodayTicketCount >= EVENT_PRIVATEROOM_TODAY_MAX_TICKET_COUNT);

	ResetMission();

	int iValue, iGiveTicketCount;
	CHECK_CONDITION_RETURN_VOID(FALSE == PRIVATEROOM.GetMissionConfig(btMissionType, iValue, iGiveTicketCount));

	if((m_iTodayTicketCount+iGiveTicketCount) > 
		EVENT_PRIVATEROOM_TODAY_MAX_TICKET_COUNT)
		iGiveTicketCount = EVENT_PRIVATEROOM_TODAY_MAX_TICKET_COUNT - m_iTodayTicketCount;

	int iUpdatedValue = 1;
	int iUpdatedGetTicketCount = 0;
	int iUpdatedTicketCount = m_iTicketCount;
	int iUpdatedTodayGetTicketCount = m_iTodayTicketCount;
	PRIVATEROOM_USER_MISSION_MAP::iterator iter = m_mapUserMission.find(btMissionType);
	if(iter != m_mapUserMission.end())
	{
		iUpdatedValue = iter->second._iValue+1;
	}

	if(iUpdatedValue >= iValue)
	{
		iUpdatedTicketCount = m_iTicketCount+iGiveTicketCount;
		iUpdatedTodayGetTicketCount = m_iTodayTicketCount+iGiveTicketCount;

		if(EVENT_PRIVATEROOM_MISSION_TYPE_LOGIN_COUNT != btMissionType)
			iUpdatedValue = 0;
	}
	
	CFSEventODBC* pEventODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_VOID(pEventODBC);

	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_PRIVATEROOM_UpdateUserTicket(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iUpdatedTicketCount, iUpdatedGetTicketCount, 0, EVENT_PRIVATEROOM_LOGTYPE_GET_TICKET, btMissionType, iUpdatedValue, 0, iUpdatedTodayGetTicketCount));

	if(iter != m_mapUserMission.end())
	{
		iter->second._iValue = iUpdatedValue;
		iter->second._iGetTicketCount = iUpdatedGetTicketCount;
	}
	else
	{
		SEventPrivateRoomUserMission Mission;
		Mission._btMissionType = btMissionType;
		Mission._iValue = iUpdatedValue;
		Mission._iGetTicketCount = iUpdatedGetTicketCount;
		m_mapUserMission[btMissionType] = Mission;
	}

	m_iTicketCount = iUpdatedTicketCount;
	m_iTodayTicketCount = iUpdatedTodayGetTicketCount;
}
