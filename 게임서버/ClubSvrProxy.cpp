// ClubSvrProxy.cpp: implementation of the CClubSvrProxy class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "ClubSvrProxy.h"
qinclude "CFSGameServer.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameUserItem.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubConfigManager.h"
qinclude "MatchSvrProxy.h"
qinclude "CFSLobby.h"
qinclude "ClubTournamentAgency.h"
qinclude "LobbyClubUserManager.h"
qinclude "CheerLeaderEventManager.h"
qinclude "CFSGameUserIntensivePractice.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define DEFINE_CLUBPROXY_PROC_FUNC(x)	void CClubSvrProxy::Proc_##x(CReceivePacketBuffer* rp)

CClubSvrProxy::CClubSvrProxy()
{
	SetState(FS_CLUB_SERVER_PROXY);
}

CClubSvrProxy::~CClubSvrProxy()
{
}

void CClubSvrProxy::SendFirstPacket()
{
	CPacketComposer PacketComposer(G2S_CLIENT_INFO);
	SG2S_CLIENT_INFO info;
	
	info.ProcessID = CFSGameServer::GetInstance()->GetProcessID();	
	info.iServerType = SERVER_TYPE_NORMAL;
	strncpy_s(info.ProcessName, _countof(info.ProcessName), CFSGameServer::GetInstance()->GetProcessInfo()->szServerName, MAX_PROCESS_NAME_LENGTH);
	
	PacketComposer.Add((BYTE*)&info, sizeof(SG2S_CLIENT_INFO));
	
	Send(&PacketComposer);
}

void CClubSvrProxy::ProcessPacket(CReceivePacketBuffer* recvPacket)
{
#define CASE_CLUBPROXY_PACKET_PROC_FUNC(x)	case x:	Proc_##x(recvPacket);	break;

	switch(recvPacket->GetCommand())
	{
	case CL2G_CLUB_CHAT_RES:							Process_ClubChatRes(recvPacket);					break;
	case CL2G_CLUB_MEMBER_STATUS_UPDATE_NOTICE:			Process_ClubMemberStatusUpdateNoticeRes(recvPacket);break;
	case CL2G_CLUB_INFO_RES:							Process_ClubInfoRes(recvPacket);					break;
	case CL2G_CLUB_LOBBY_MEMBER_LIST_RES:				Process_ClubLobbyMemberListRes(recvPacket);			break;
	case CL2G_CLUB_MEMBER_LIST_RES:						Process_ClubMemberListRes(recvPacket);				break;
	case CL2G_CLUB_LIST_RES:							Process_ClubListRes(recvPacket);					break;
	case CL2G_CLUB_MARK_LIST_RES:						Process_ClubMarkListRes(recvPacket);				break;
	case CL2G_CLUB_MARK_CHANGE_RES:						Process_ClubMarkChangeRes(recvPacket);				break;
	case CL2G_CLUB_GRADE_UP_RES:						Process_ClubGradeUpRes(recvPacket);					break;
	case CL2G_CLUB_NOTICE_CHANGE_RES:					Process_ClubNoticeChangeRes(recvPacket);			break;
	case CL2G_CLUB_INVITE_RES:							Process_ClubInviteRes(recvPacket);					break;
	case CL2G_CLUB_JOIN_RES:							Process_ClubJoinRes(recvPacket);					break;
	case CL2G_CLUB_JOIN_REQUEST_RES:					Process_ClubJoinRequestRes(recvPacket);				break;
	case CL2G_CLUB_REMOVE_MEMBER_RES:					Process_ClubRemoveMemberRes(recvPacket);			break;
	case CL2G_CLUB_WITHDRAW_RES:						Process_ClubWithDrawRes(recvPacket);				break;
	case CL2G_CLUB_ADMIN_MEMBER_LIST_RES:				Process_ClubAdminMemberListRes(recvPacket);			break;
	case CL2G_CLUB_ADD_ADMIN_MEMBER_RES:				Process_ClubAddAdminMemberRes(recvPacket);			break;
	case CL2G_CLUB_REMOVE_ADMIN_MEMBER_RES:				Process_ClubRemoveAdminMemberRes(recvPacket);		break;
	case CL2G_CLUB_ADD_KEY_PLAYER_RES:					Process_ClubAddKeyPlayerRes(recvPacket);			break;
	case CL2G_CLUB_REMOVE_KEY_PLAYER_RES:				Process_ClubRemoveKeyPlayerRes(recvPacket);			break;
	case CL2G_CLUB_CL_RES:								Process_ClubCLRes(recvPacket);						break;
	case CL2G_DELETE_BOARD_MSG_RES:						Process_ClubDeleteBoardMsgRes(recvPacket);			break;
	case CL2G_CLUB_BOARD_MSG_RES:						Process_ClubBoardMsgRes(recvPacket);				break;
	case CL2G_CLUB_ADD_BOARD_MSG_RES:					Process_ClubAddBoardMsgRes(recvPacket);				break;
	case CL2G_CLUB_EXTEND_KEY_PLAYER_RES:				Process_ClubExtendKeyPlayerRes(recvPacket);			break;
	case CL2G_CLUB_MEMBER_PR_CHANGE_RES:				Process_ClubMemberPRChangeRes(recvPacket);			break;
	case CL2G_CLUB_CHECK_CLUBNAME_RES:					Process_ClubCheckClubNameRes(recvPacket);			break;
	case CL2G_CLUB_SETUP_RES:							Process_ClubSetUpRes(recvPacket);					break;
	case CL2G_CLUB_TRANSFER_RES:						Process_ClubTransferRes(recvPacket);				break;
	case CL2G_CLUB_DONATION_INFO_RES:					Process_ClubDonationInfoRes(recvPacket);			break;
	case CL2G_CLUB_DONATION_RES:						Process_ClubDonationRes(recvPacket);				break;
	case CL2G_CLUB_CREATE_RES:							Process_ClubCreateRes(recvPacket);					break;
	case CL2G_CLUB_MEMBER_RECORD_LIST_RES:				Process_ClubMemberRecordListRes(recvPacket);		break;
	case CL2G_CLUB_RECENT_MATCH_RECORD_LIST_RES:		Process_ClubRecentMatchRecordListRes(recvPacket);	break;
	case CL2G_CLUB_PR_LIST_RES:							Process_ClubPRListRes(recvPacket);					break;
	case CL2G_CLUB_PR_CHANGE_RES:						Process_ClubPRChangeRes(recvPacket);				break;
	case CL2G_MY_CLUB_RANK_RES:							Process_MyClubRankRes(recvPacket);					break;
	case CL2G_CLUB_RANK_LIST_RES:						Process_ClubRankListRes(recvPacket);				break;
	case CL2G_CLUB_CHEERLEADER_INFO_RES:				Process_ClubCheerLeaderInfoRes(recvPacket);			break;
	case CL2G_CLUB_BUY_CHEERLEADER_INFO_RES:			Process_ClubBuyCheerLeaderInfoRes(recvPacket);		break;
	case CL2G_CLUB_BUY_CHEERLEADER_RES:					Process_ClubBuyCheerLeaderRes(recvPacket);			break;
	case CL2G_CLUB_CHEERLEADER_LIST_RES:				Process_ClubCheerLeaderListRes(recvPacket);			break;
	case CL2G_CLUB_CHEERLEADER_CHANGE_RES:				Process_ClubCheerLeaderChangeRes(recvPacket);		break;
	case CL2G_CLUB_CHEERLEADER_CHANGED_NOTICE:			Process_ClubCheerLeaderChangedNotice(recvPacket);	break;
	case CL2G_CLUB_CHEERLEADER_EXPIRED_NOTICE:			Process_ClubCheerLeaderExpiredNotice(recvPacket);	break;
	case CL2G_CLUB_MY_CHEERLEADER_CHANGE_RES:			Process_ClubMyCheerLeaderChangeRes(recvPacket);		break;
	case CL2G_CLUB_STATUS_CHANGE_RES:					Process_ClubStatusChangeRes(recvPacket);			break;
	case CL2G_MY_CLUB_PR_RES:							Process_MyClubPRRes(recvPacket);					break;
	case CL2G_UPDATE_CLUB_NOTICE:						Process_UpdateClubNotice(recvPacket);				break;

		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_ANNOUNCE_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_MISSION_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_MISSION_GET_BENEFIT_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_MISSION_COMPLETE_NOTICE)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_PENNANT_SLOT_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_PENNANT_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_USE_PENNANT_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_USE_PENNANT_NOTICE)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_BUY_PENNANT_SLOT_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_BUY_CLOTHES_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_BUY_CLOTHES_NOTICE)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_BUY_CLUBMARK_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_MEMBER_LOGIN_NOTICE);

		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_UPDATE_CLUBTOURNAMENT_AGENCY_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_MATCH_TBALE_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_KEY_PLAYER_LIST_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_LIST_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_ROOM_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_COUNT_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_REMAIN_TIME_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_MATCH_RESULT_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_GET_BENEFIT_ITEM_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_GAME_START_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_CURRENT_ROUND_CLUB_STATUS_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_GAME_END_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_WINTEAM_GAME_END_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_NOT)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_GIVE_BENEFIT_CLUBITEM_NOTICE)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_GIVE_BENEFIT_ITEM_NOTICE)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUBTOURNAMENT_TIME_INFO_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_INTENSIVEPRACTICE_RANK_LIST_RES)
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_LEAGUE_INFO_RES);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_RANKING_INFO_RES);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_RANKING_LIST_RES);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_UPDATE_MEMBER_RANKING_NOT);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_UPDATE_MEMBER_LEAGUE_RANKING_NOT);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_UPDATE_RANKING_NOT);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_UPDATE_LEAGUE_RANKING_NOT);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_TOURNAMENT_RANKING_INFO_RES);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_TOURNAMENT_RANKING_LIST_RES);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_USER_UPDATE_GAMERESULT_NOT);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_MISSION_GIVE_REWARD_NOT);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_COLLECTOR_NOT);	
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_INFO_RES);	
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES);	
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_NAME_CHANGE_NOT);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_NOTICE_RES);
		CASE_CLUBPROXY_PACKET_PROC_FUNC(CL2G_CLUB_NOTICE_NOT);
	}
}

void CClubSvrProxy::SendGameIDChangeReq(SG2CL_CHANGE_MEMBERID_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CHANGE_CLUB_MEMBERID_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CHANGE_MEMBERID_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::SendClubChatReq(SG2CL_CLUB_CHAT_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_CHAT_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_CHAT_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubChatRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CHAT_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CHAT_RES));

	if(rs.iMsgLen < 1 || rs.iMsgLen > 256) 
		return;

	BYTE bIsGM = FALSE;
	CPacketComposer	PacketComposer(S2C_CHAT);
	PacketComposer.Add((int)CHAT_TYPE_CLUB);
	PacketComposer.Add(bIsGM);
	PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);	
	PacketComposer.Add(rs.iMsgLen-1);
	PacketComposer.Add((PBYTE)rs.szMsg, rs.iMsgLen);

	LOBBYCLUBUSER.Broadcast(rs.iClubSN, PacketComposer);
}

void CClubSvrProxy::SendClubMemberLogin(SG2CL_CLUB_MEMBER_LOGIN	info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_MEMBER_LOGIN_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_MEMBER_LOGIN));
	Send(&PacketComposer);
}

void CClubSvrProxy::SendClubMemberLogout(SG2CL_DEFAULT info)	
{
	CPacketComposer PacketComposer(G2CL_CLUB_MEMBER_LOGOUT_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubMemberStatusUpdateNoticeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_MEMBER_STATUS_UPDATE_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MEMBER_STATUS_UPDATE_NOTICE));

	CPacketComposer	PacketComposer(S2C_CLUB_MEMBER_STATUS_UPDATE_NOTICE);
	PacketComposer.Add(rs.btKind);	
	PacketComposer.Add(rs.iClubSN);	
	PacketComposer.Add(rs.iClubMarkCode);	

	if(rs.btKind == CLUB_MEMBER_STATUS_UPDATE_LOGIN)
	{
		rs.btStatus = CLUB_MEMBER_LOCATION_DIFF;
		if(rs.iChannelNum == CFSGameServer::GetInstance()->GetProcessID())
		{
			rs.btStatus = CLUB_MEMBER_LOCATION_EQUAL;
		}
	}
	else if(rs.btKind == CLUB_MEMBER_STATUS_UPDATE_CHANGE_GAMEID)
	{
		if(rs.btStatus == CLUB_MEMBER_LOCATION_DIFF)
		{
			if(rs.iChannelNum == CFSGameServer::GetInstance()->GetProcessID())
			{
				rs.btStatus = CLUB_MEMBER_LOCATION_EQUAL;
			}
		}
		else
		{
			rs.btStatus = CLUB_MEMBER_OFFLINE;
		}
	}
	else
	{
		rs.btStatus = CLUB_MEMBER_OFFLINE;
	}

	PacketComposer.Add(rs.btStatus);	
	PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);	
	PacketComposer.Add(rs.btUnifromNum);
	PacketComposer.Add(rs.btPosition);
	PacketComposer.Add(rs.btClubPosition);
	PacketComposer.Add((int)-1);	//통합 삭제대기 vip
	PacketComposer.Add((int)-1);	//통합 삭제대기 결혼
	PacketComposer.Add((int)-1);	//통합 삭제대기 powerup
	PacketComposer.Add(rs.btLv);
	PacketComposer.Add(rs.iFameLevel);
	PacketComposer.Add((PBYTE)rs.szPR, MAX_CLUB_MEMBER_PR_LENGTH+1);	
	PacketComposer.Add((PBYTE)rs.szOldGameID, MAX_GAMEID_LENGTH+1);	

	LOBBYCLUBUSER.Broadcast(rs.iClubSN, PacketComposer, TRUE);
}

void CClubSvrProxy::SendClubInfoReq(SG2CL_CLUB_INFO_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_INFO_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubInfoRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_INFO_RES));

	CPacketComposer	PacketComposer(S2C_CLUB_INFO_NOTICE);
	if(rs.btKind == CLUB_INFO_REQUEST_KIND_CLUBLIST_PAGE || rs.btKind == CLUB_INFO_REQUEST_KIND_CLUBPAGE)
	{
		PacketComposer.Initialize(S2C_CLUB_INFO_RES);
		PacketComposer.Add(rs.btKind);	
	}

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{	
		if(pUser->GetClubSN() == rs.iClubSN)
		{
			pUser->SetClub(rs.btKind, rs.sInfo, rs.iCheerLeaderIndex);
			if ( rs.btKind == CLUB_INFO_REQUEST_KIND_LOGIN )
			{
				pUser->ClubInfoUpdateToCenterSvr();
			}
		}
		PacketComposer.Add(rs.sInfo.iClubSN);	
		PacketComposer.Add((PBYTE)rs.sInfo.szClubName, MAX_CLUB_NAME_LENGTH+1);
		PacketComposer.Add(rs.sInfo.iClubMarkCode);
		PacketComposer.Add((PBYTE)rs.sInfo.szMasterName, MAX_GAMEID_LENGTH+1);
		PacketComposer.Add(rs.sInfo.btClubPosition);	
		PacketComposer.Add(rs.sInfo.btLv);	
		PacketComposer.Add(rs.sInfo.btExp);	
		PacketComposer.Add((BYTE)MAX_CLUB_GRADE_BY_GRADEUP);	
		PacketComposer.Add(rs.sInfo.btGrade);	
		PacketComposer.Add(rs.sInfo.iMemberNum);	
		PacketComposer.Add(rs.sInfo.iMaxMemberNum);	

		if(rs.btKind == CLUB_INFO_REQUEST_KIND_LOGIN || rs.btKind == CLUB_INFO_REQUEST_KIND_CLUBPAGE)
		{
			PacketComposer.Add((PBYTE)rs.sInfo.szNotice, MAX_CLUB_NOTICE_LENGTH+1);
			PacketComposer.Add(rs.sInfo.iClubCL);	
			PacketComposer.Add(rs.sInfo.sCheerLeaderInfo.iCheerLeaderCode);	
			PacketComposer.Add(rs.sInfo.sCheerLeaderInfo.btTypeNum);
			PacketComposer.Add(rs.sInfo.sCheerLeaderInfo.iCheerLeaderTime);	
		}
		else
		{
			PacketComposer.Add((PBYTE)rs.sInfo.szPRContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);
			PacketComposer.Add(rs.sInfo.sCheerLeaderInfo.iCheerLeaderCode);
			PacketComposer.Add(rs.sInfo.sCheerLeaderInfo.btTypeNum);	
		}
		PacketComposer.Add(rs.sInfo.btStatus);	

		if(rs.btKind == CLUB_INFO_REQUEST_KIND_CLUBLIST_PAGE || rs.btKind == CLUB_INFO_REQUEST_KIND_CLUBPAGE)
		{
			for(int i = 0; i < MAX_CLUB_PENNANT_SLOT_COUNT; ++i)
			{
				PacketComposer.Add(rs.sInfo.iPennantCode[i]);
				PacketComposer.Add(rs.sInfo.btPennantGrade[i]);
				PacketComposer.Add(rs.sInfo.btConditionType[i]);
				PacketComposer.Add(rs.sInfo.iConditionValue[i]);
			}
		}

		PacketComposer.Add(rs.sInfo.iContributionPointRank);
		PacketComposer.Add(rs.sInfo.iCurSeasonContributionPoint);
		PacketComposer.Add(rs.sInfo.iLeagueRank);
		PacketComposer.Add(rs.sInfo.btLeagueGrade);
		PacketComposer.Add(rs.sInfo.iLeagueLogoIndex);
		PacketComposer.Add(rs.sInfo.iLeaguePoint);

		PacketComposer.Add(rs.sInfo.btClubTournamentCupType);
		PacketComposer.Add(rs.sInfo.iClubMemberCount);
		PacketComposer.Add(rs.sInfo.iMaxClubMemberCount);

		if(rs.btKind == CLUB_INFO_REQUEST_KIND_CLUBPAGE)
		{
			PacketComposer.Add(rs.sInfo.iUserContributionPointRank);
			PacketComposer.Add(rs.sInfo.iUserContributionPoint);
			PacketComposer.Add(rs.sInfo.iUserLeagueRank);
			PacketComposer.Add(rs.sInfo.iUserLeaguePoint);

			PacketComposer.Add(pUser->GetUserClubCoin());

			// 실제 초기화는 클럽리그 매치서버에서 경기시작 전에 한다. 여기서는 내클럽 페이지에서 표기하는 부분만 동기화
			time_t tCurrentTime = _time64(NULL);
			time_t tPlayCountUpdateTime = pUser->GetUserClubPlayCountUpdateDate();
			int iClubPlayCount = pUser->GetUserClubPlayCount();
			if(FALSE == TodayCheck(EVENT_RESET_HOUR, tPlayCountUpdateTime, tCurrentTime))
			{
				iClubPlayCount = 0;
			}	

			PacketComposer.Add(iClubPlayCount);
			PacketComposer.Add((int)MAX_DAILY_MISSION_PLAY_COUNT);
		}

		pUser->Send(&PacketComposer);	
		pUser->SendUserGold();
	}
}

void CClubSvrProxy::SendClubLobbyMemberListReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_LOBBY_MEMBER_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubLobbyMemberListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_LOBBY_MEMBER_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_LOBBY_MEMBER_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_LOBBY_MEMBER_LIST_RES);
		PacketComposer.Add((PBYTE)&rs.bIsFirst, sizeof(bool));
		PacketComposer.Add((PBYTE)&rs.bIsLast, sizeof(bool));
		PacketComposer.Add(rs.iClubSN);
		PacketComposer.Add(rs.iClubMarkCode);
		PacketComposer.Add(rs.btMemberCnt);

		SCL2G_CLUB_LOBBY_MEMBER_INFO info;
		for(int i = 0; i < rs.btMemberCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_LOBBY_MEMBER_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_LOBBY_MEMBER_INFO));	
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubMemberListReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_MEMBER_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubMemberListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_MEMBER_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MEMBER_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_MEMBER_LIST_RES);
		PacketComposer.Add(rs.iClubSN);
		PacketComposer.Add(rs.btPageNum);
		PacketComposer.Add(rs.btMaxPageNum);
		PacketComposer.Add(rs.btWaitKeyPlayerCnt);
		PacketComposer.Add(rs.btExtendKeyPlayerCnt);
		PacketComposer.Add(rs.btMemberCnt);

		SCL2G_CLUB_MEMBER_INFO info;
		for(int i = 0; i < rs.btMemberCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_MEMBER_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_MEMBER_INFO));	
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubListReq(SG2CL_CLUB_LIST_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_LIST_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_LIST_RES);
		PacketComposer.Add(rs.btCnt);

		SCL2G_CLUB_INFO info;
		for(int i = 0; i < rs.btCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_INFO));	
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::Process_ClubMarkListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_MARK_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MARK_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer PacketComposer(S2C_CLUB_MARK_LIST_RES);
		PacketComposer.Add(pUser->GetClubMarkCode());
		PacketComposer.Add(rs.btCnt);
		PacketComposer.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubMarkChangeReq(SG2CL_CLUB_MARK_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_MARK_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_MARK_CHANGE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubMarkChangeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_MARK_CHANGE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MARK_CHANGE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if(rs.btResult == CLUB_MARK_CHANGE_RESULT_SUCCESS)
		{
			pUser->SetClubMarkCode(rs.iClubMarkCode);
			pUser->ClubInfoUpdateToCenterSvr();
		}

		CPacketComposer	PacketComposer(S2C_CLUB_MARK_CHANGE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.iClubMarkCode);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubGradeUpReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_GRADE_UP_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubGradeUpRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_GRADE_UP_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_GRADE_UP_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if ( rs.btResult == CLUB_GRADE_UP_RESULT_SUCCESS )
		{
			pUser->SetClubGrade( rs.btGrade );
		}

		CPacketComposer	PacketComposer(S2C_CLUB_GRADE_UP_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.btGrade);
		PacketComposer.Add(rs.btUpdatedMaxMemberCount);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubNoticeChangeReq(SG2CL_CLUB_NOTICE_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_NOTICE_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_NOTICE_CHANGE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubNoticeChangeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_NOTICE_CHANGE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_NOTICE_CHANGE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_NOTICE_CHANGE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szNotice, MAX_CLUB_NOTICE_LENGTH+1);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubInviteReq(SG2CL_CLUB_INVITE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_INVITE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_INVITE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubInviteRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_INVITE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_INVITE_RES));

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_VOID( pClubBaseODBC );

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if(rs.btResult == CLUB_INVITE_RESULT_SUCCESS)
		{
			int iPaperIndex = MAX_CLUB_INVITION_COUNT, iResult = 0;
			if(ODBC_RETURN_SUCCESS == pClubBaseODBC->SP_Club_InviteMember(pUser->GetGameIDIndex(), pUser->GetClubSN(), rs.szCharName, iPaperIndex, iResult) && iResult == CLUB_INVITE_RESULT_SUCCESS )
			{
				CScopedRefGameUser pRecvUser(rs.szCharName);
				if(pRecvUser == NULL) 
				{
					CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
					if(pCenter) 
					{
						pCenter->SendUserNotify(rs.szCharName, iPaperIndex, MAIL_CLUB_INVITE);				
					}
				}
				else
				{
					pRecvUser->RecvClubInvitation(iPaperIndex, (CFSClubBaseODBC*)pClubBaseODBC);
				}
			}
			rs.btResult = iResult;
		}

		CPacketComposer	PacketComposer(S2C_CLUB_INVITE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szCharName, MAX_GAMEID_LENGTH+1);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubJoinRequestReq(SG2CL_CLUB_JOIN_REQUEST_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_JOIN_REQUEST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_JOIN_REQUEST_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubJoinRequestRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_JOIN_REQUEST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_JOIN_REQUEST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_JOIN_REQUEST_RES);
		PacketComposer.Add(rs.btResult);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubJoinReq(SG2CL_CLUB_JOIN_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_JOIN_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_JOIN_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubJoinRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_JOIN_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_JOIN_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if(rs.btResult == CLUB_JOIN_RESULT_SUCCESS)
		{
			CScopedRefGameUser pRecvUser(rs.szCharName);
			if(pRecvUser == NULL) 
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
				if(pCenter) 
				{
					pCenter->SendClubJoinReq(rs.iClubSN, rs.btKind, rs.szCharName);				
				}
			}
			else
			{
				pRecvUser->SetClubSN(rs.iClubSN);
				pRecvUser->AddLobbyClubUser();

				SG2CL_CLUB_INFO_REQ info;
				info.iClubSN = rs.iClubSN;
				strncpy_s(info.szGameID, _countof(info.szGameID), pRecvUser->GetGameID(), MAX_GAMEID_LENGTH);	
				info.btKind	= CLUB_INFO_REQUEST_KIND_LOGIN;
				info.iCheerLeaderIndex = pRecvUser->GetCheerLeaderCode();
				SendClubInfoReq(info);

				if( rs.btKind == CLUB_JOIN_KIND_APPLICATION_FORM || rs.btKind == CLUB_JOIN_KIND_INVITE )
				{
					SAvatarInfo* pInfo = pRecvUser->GetCurUsedAvatar();
					BYTE btPosition = 0, btLv = 0, btSex = 0;
					if(pInfo)
					{
						btPosition	= pInfo->Status.iGamePosition;
						btLv		= pInfo->iLv;
						btSex		= pInfo->iSex;
					}

					SG2CL_CLUB_MEMBER_LOGIN	loginInfo;
					loginInfo.iClubSN = rs.iClubSN;
					strncpy_s(loginInfo.szGameID, _countof(loginInfo.szGameID), pRecvUser->GetGameID(), MAX_GAMEID_LENGTH);	
					loginInfo.btPosition = btPosition;
					loginInfo.btLv = btLv;
					loginInfo.btSex = btSex;
					SendClubMemberLogin(loginInfo);

					pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_JOIN_CLUB);
				}
			}
		}
		CPacketComposer	PacketComposer(S2C_CLUB_JOIN_RES);
		if(rs.btKind == CLUB_JOIN_KIND_INVITE)
		{
			PacketComposer.Add(rs.btResult);
			PacketComposer.Add(rs.iClubSN);
		}
		else
		{
			PacketComposer.Initialize(S2C_CLUB_JOIN_REQUEST_ANSWER_RES);
			PacketComposer.Add(rs.btResult);
			
			BYTE btKind = CLUB_JOIN_REQUEST_ANSWER_KIND_ACCEPT;
			PacketComposer.Add(btKind);
			PacketComposer.Add((PBYTE)rs.szCharName, MAX_GAMEID_LENGTH+1);
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubRemoveMemberReq(SG2CL_CLUB_REMOVE_MEMBER_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_REMOVE_MEMBER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_REMOVE_MEMBER_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubRemoveMemberRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_REMOVE_MEMBER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_REMOVE_MEMBER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if(rs.btResult == CLUB_REMOVE_MEMBER_RESULT_SUCCESS)
		{
			CScopedRefGameUser pRecvUser(rs.szCharName);
			if(pRecvUser == NULL) 
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
				if(pCenter) 
				{
					pCenter->SendClubWithDrawReq(rs.iClubSN, rs.szCharName);				
				}
			}
			else
			{
				pRecvUser->InitClub();
				pRecvUser->ClubInfoUpdateToCenterSvr();
				pRecvUser->RemoveLobbyClubUser();

				CPacketComposer	PacketComposer(S2C_CLUB_WITHDRAW_RES);
				PacketComposer.Add((BYTE)CLUB_WITHDRAW_RESULT_SUCCESS);
				pRecvUser->Send(&PacketComposer);
			}
		}
		CPacketComposer	PacketComposer(S2C_CLUB_REMOVE_MEMBER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szCharName, MAX_GAMEID_LENGTH+1);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubWithDrawReq(SG2CL_CLUB_WITHDRAW_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_WITHDRAW_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_WITHDRAW_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubWithDrawRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_WITHDRAW_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_WITHDRAW_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if(rs.btResult == CLUB_REMOVE_MEMBER_RESULT_SUCCESS || rs.btResult == CLUB_WITHDRAW_RESULT_SUCCESS_CLUB_REMOVE)
		{
			pUser->InitClub();
			pUser->ClubInfoUpdateToCenterSvr();
			pUser->RemoveLobbyClubUser();
		}

		CPacketComposer	PacketComposer(S2C_CLUB_WITHDRAW_RES);
		PacketComposer.Add(rs.btResult);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubAdminMemberListReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_ADMIN_MEMBER_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubAdminMemberListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_ADMIN_MEMBER_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_ADMIN_MEMBER_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_ADMIN_MEMBER_LIST_RES);
		PacketComposer.Add(rs.btCnt);

		SCL2G_CLUB_ADMIN_MEMBER_INFO info;
		for(int i = 0; i < rs.btCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_ADMIN_MEMBER_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_ADMIN_MEMBER_INFO));
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubAddAdminMemberReq(SG2CL_CLUB_ADD_ADMIN_MEMBER_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_ADD_ADMIN_MEMBER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_ADD_ADMIN_MEMBER_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubAddAdminMemberRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_ADD_ADMIN_MEMBER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_ADD_ADMIN_MEMBER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_ADD_ADMIN_MEMBER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.btClubPosition);
		PacketComposer.Add((PBYTE)&rs.szCharName, MAX_GAMEID_LENGTH+1);
		pUser->Send(&PacketComposer);

		if(rs.btResult == CLUB_ADD_ADMIN_MEMBER_RESULT_SUCCESS)
		{
			CScopedRefGameUser pRecvUser(rs.szCharName);
			if(pRecvUser == NULL) 
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
				if(pCenter) 
				{
					pCenter->SendClubUpdateAdminMemberReq(rs.szCharName, rs.btClubPosition);				
				}
			}
			else
			{
				pRecvUser->SetClubPosition(rs.btClubPosition);
			}
		}
	}
}

void CClubSvrProxy::SendClubRemoveAdminMemberReq(SG2CL_CLUB_REMOVE_ADMIN_MEMBER_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_REMOVE_ADMIN_MEMBER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_REMOVE_ADMIN_MEMBER_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubRemoveAdminMemberRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_REMOVE_ADMIN_MEMBER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_REMOVE_ADMIN_MEMBER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_REMOVE_ADMIN_MEMBER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szCharName, MAX_GAMEID_LENGTH+1);
		pUser->Send(&PacketComposer);

		if(rs.btResult == CLUB_ADD_ADMIN_MEMBER_RESULT_SUCCESS)
		{
			CScopedRefGameUser pRecvUser(rs.szCharName);
			if(pRecvUser == NULL) 
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
				if(pCenter) 
				{
					pCenter->SendClubUpdateAdminMemberReq(rs.szCharName, CLUB_POSITION_NORMAL);				
				}
			}
			else
			{
				pRecvUser->SetClubPosition(CLUB_POSITION_NORMAL);
			}
		}
	}
}

void CClubSvrProxy::SendClubAddKeyPlayerReq(SG2CL_CLUB_ADD_KEY_PLAYER_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_ADD_KEY_PLAYER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_ADD_KEY_PLAYER_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubAddKeyPlayerRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_ADD_KEY_PLAYER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_ADD_KEY_PLAYER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_ADD_KEY_PLAYER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szCharName, MAX_GAMEID_LENGTH+1);
		PacketComposer.Add(rs.btUniformNum);
		pUser->Send(&PacketComposer);

		if(rs.btResult == CLUB_ADD_KEY_PLAYER_RESULT_SUCCESS)
		{
			CScopedRefGameUser pRecvUser(rs.szCharName);
			if(pRecvUser == NULL) 
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
				if(pCenter) 
				{
					pCenter->SendClubUpdateKeyPlayerReq(rs.szCharName, rs.btUniformNum);				
				}
			}
			else
			{
				pRecvUser->SetUnifromNum(rs.btUniformNum);
			}
		}
	}
}

void CClubSvrProxy::SendClubRemoveKeyPlayerReq(SG2CL_CLUB_REMOVE_KEY_PLAYER_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_REMOVE_KEY_PLAYER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_REMOVE_KEY_PLAYER_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubRemoveKeyPlayerRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_REMOVE_KEY_PLAYER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_REMOVE_KEY_PLAYER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_REMOVE_KEY_PLAYER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szCharName, MAX_GAMEID_LENGTH+1);
		pUser->Send(&PacketComposer);

		if(rs.btResult == CLUB_REMOVE_KEY_PLAYER_RESULT_SUCCESS)
		{
			CScopedRefGameUser pRecvUser(rs.szCharName);
			if(pRecvUser == NULL) 
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
				if(pCenter) 
				{
					pCenter->SendClubUpdateKeyPlayerReq(rs.szCharName, 0);				
				}
			}
			else
			{
				pRecvUser->SetUnifromNum(0);
			}
		}
	}
}

void CClubSvrProxy::SendClubCLReq(SG2CL_CLUB_CL_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_CL_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_CL_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubCLRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CL_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CL_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_MY_CLUB_CL_RES);

		int iCL = 0;
		if(rs.btKind == CLUB_CL_KIND_CLUB_EXTEND_KEY_PLAYER)
		{
			PacketComposer.Initialize(S2C_CLUB_EXTEND_KEY_PLAYER_POPUP_RES);			
			BYTE btSlotCount = rs.iValue;
			if(btSlotCount+1 < MIN_PREMIUM_KEY_PLAYER_SLOT_COUNT)
				iCL = CLUBCONFIGMANAGER.GetKeyPlayerSlotCost();
			else
				iCL = CLUBCONFIGMANAGER.GetKeyPlayerPremiumSlotCost();

			PacketComposer.Add(rs.iClubCL);
			PacketComposer.Add(iCL);
		}
		if(rs.btKind == CLUB_CL_KIND_CLUB_SETUP)
		{
			PacketComposer.Initialize(S2C_CLUB_SETUP_POPUP_RES);
			iCL = CLUBCONFIGMANAGER.GetChangingClubInfoCost();

			PacketComposer.Add(rs.iClubCL);
			PacketComposer.Add(iCL);
		}
		if(rs.btKind == CLUB_CL_KIND_CLUB_BUY_PENNANT_SLOT)
		{
			PacketComposer.Initialize(S2C_CLUB_BUY_PENNANT_SLOT_POPUP_RES);
			iCL = CLUBCONFIGMANAGER.GetPennantSlotCost();
			short stSlotIndex = rs.iValue;
			PacketComposer.Add(stSlotIndex);
			PacketComposer.Add(rs.iClubCL);
			PacketComposer.Add(iCL);
		}
		if(rs.btKind == CLUB_CL_KIND_CLUB_BUY_CLOTHES)
		{
			PacketComposer.Initialize(S2C_CLUB_BUY_CLOTHES_POPUP_RES);
			int iItemCode = rs.iValue;
			PacketComposer.Add(iItemCode);
			PacketComposer.Add(rs.iClubCL);
			CLUBCONFIGMANAGER.MakeClubClothesShopPopUpInfo(iItemCode, PacketComposer);

		}
		if(rs.btKind == CLUB_CL_KIND_CLUB_BUY_CLUBMARK)
		{
			PacketComposer.Initialize(S2C_CLUB_BUY_CLUBMARK_POPUP_RES);
			int iClubMarkCode = rs.iValue;
			PacketComposer.Add(iClubMarkCode);
			PacketComposer.Add(rs.iClubCL);
			CLUBCONFIGMANAGER.MakeClubMarkShopPopUpInfo(iClubMarkCode, PacketComposer);
		}
		else
		{
			PacketComposer.Add(rs.iClubCL);
		}
		
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::Process_ClubDeleteBoardMsgRes(CReceivePacketBuffer* rp)
{
	SCL2G_DELETE_BOARD_MSG_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_DELETE_BOARD_MSG_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	Packet(S2C_DELETE_BOARD_MSG_RES);
		Packet.Add(rs.btResult);
		Packet.Add(rs.iMsgIndex);

		pUser->Send(&Packet);
	}
}

void CClubSvrProxy::Process_ClubBoardMsgRes(CReceivePacketBuffer* rp)
{
	SCL2G_DEFAULT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_DEFAULT));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	Packet(S2C_CLUB_LINE_BOARD_LIST_RES);
		Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());

		pUser->Send(&Packet);
	}
}

void CClubSvrProxy::Process_ClubAddBoardMsgRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_ADD_BOARD_MSG_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_ADD_BOARD_MSG_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	Packet(S2C_CLUB_ADD_LINE_BOARD_RES);
		Packet.Add(rs.btResult);

		pUser->Send(&Packet);
	}
}	

void CClubSvrProxy::SendClubExtendKeyPlayerReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_EXTEND_KEY_PLAYER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubExtendKeyPlayerRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_EXTEND_KEY_PLAYER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_EXTEND_KEY_PLAYER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_EXTEND_KEY_PLAYER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.iClubCL);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubMemberPRChangeReq(SG2CL_CLUB_MEMBER_PR_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_MEMBER_PR_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_MEMBER_PR_CHANGE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubMemberPRChangeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_MEMBER_PR_CHANGE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MEMBER_PR_CHANGE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_MEMBER_PR_CHANGE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szPR, MAX_CLUB_MEMBER_PR_LENGTH+1);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubCheckClubNameReq(SG2CL_CLUB_CHECK_CLUBNAME_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_CHECK_CLUBNAME_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_CHECK_CLUBNAME_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubCheckClubNameRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CHECK_CLUBNAME_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CHECK_CLUBNAME_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if(rs.btType == 0)
		{
			CPacketComposer	PacketComposer(S2C_CLUB_CHECK_CLUBNAME_RES);
			PacketComposer.Add(rs.btResult);
			PacketComposer.Add((PBYTE)&rs.szClubName, MAX_CLUB_NAME_LENGTH+1);
			pUser->Send(&PacketComposer);
			return;
		}

		SBillingInfo BillingInfo;
		BillingInfo.iEventCode = EVENT_BUY_CLUB_SETUP;
		pUser->SetBillingInfo(BillingInfo, SELL_TYPE_CASH, CLUB_CHANGE_NAME_COST);
		strcpy_s(BillingInfo.szItemName, "Change ClubName");
		strncpy_s(BillingInfo.szClubName, MAX_CLUB_NAME_LENGTH+1, rs.szClubName, MAX_CLUB_NAME_LENGTH);
		BillingInfo.iParam0 = rs.iClubAreaIndex;
		if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
		{
			CPacketComposer Packet(S2C_CLUB_SETUP_RES);
			Packet.Add((BYTE)CLUB_SETUP_RESULT_FAIL_SUPPLY_CASH);
			Packet.Add((PBYTE)&rs.szClubName, MAX_CLUB_NAME_LENGTH+1);
			Packet.Add(rs.iClubAreaIndex);
			pUser->Send(&Packet);
		}
	}
}

void CClubSvrProxy::SendClubSetUpReq(SG2CL_CLUB_SETUP_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_SETUP_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_SETUP_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubSetUpRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_SETUP_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_SETUP_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_SETUP_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szClubName, MAX_CLUB_NAME_LENGTH+1);
		PacketComposer.Add(rs.iClubAreaIndex);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubTransferReq(SG2CL_CLUB_TRANSFER_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_TRANSFER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_TRANSFER_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubTransferRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_TRANSFER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_TRANSFER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_TRANSFER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add((PBYTE)&rs.szCharName, MAX_GAMEID_LENGTH+1);
		pUser->Send(&PacketComposer);

		if(rs.btResult == CLUB_REMOVE_KEY_PLAYER_RESULT_SUCCESS)
		{
			pUser->SetClubPosition(CLUB_POSITION_NORMAL);

			CScopedRefGameUser pRecvUser(rs.szCharName);
			if(pRecvUser == NULL) 
			{
				CCenterSvrProxy* pCenter = (CCenterSvrProxy *)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);;
				if(pCenter) 
				{
					pCenter->SendClubTransferReq(rs.szCharName);				
				}
			}
			else
			{
				pRecvUser->SetClubPosition(CLUB_POSITION_MASTER);

				SG2CL_CLUB_INFO_REQ info;
				info.iClubSN = pRecvUser->GetClubSN();
				strncpy_s(info.szGameID, _countof(info.szGameID), pRecvUser->GetGameID(), MAX_GAMEID_LENGTH);	
				info.btKind	= CLUB_INFO_REQUEST_KIND_LOGIN;
				info.iCheerLeaderIndex = pRecvUser->GetCheerLeaderCode();
				SendClubInfoReq(info);
			}
		}
	}
}

void CClubSvrProxy::SendClubDonationInfoReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_DONATION_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubDonationInfoRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_DONATION_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_DONATION_INFO_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_DONATION_POPUP_RES);
		PacketComposer.Add(rs.btMyRank);
		PacketComposer.Add(rs.iMyDonation);
		PacketComposer.Add(rs.btRankCnt);
		
		SCL2G_CLUB_DONATION_RANK_INFO rankInfo;
		for(int i = 0; i < rs.btRankCnt; i++)
		{
			rp->Read((BYTE*)&rankInfo, sizeof(SCL2G_CLUB_DONATION_RANK_INFO));
			PacketComposer.Add((PBYTE)&rankInfo, sizeof(SCL2G_CLUB_DONATION_RANK_INFO));
		}
		PacketComposer.Add(rs.btLatelyCnt);

		SCL2G_CLUB_LATELY_DONATION_INFO latelyInfo;
		for(int i = 0; i < rs.btLatelyCnt; i++)
		{
			rp->Read((BYTE*)&latelyInfo, sizeof(SCL2G_CLUB_LATELY_DONATION_INFO));
			PacketComposer.Add((PBYTE)&latelyInfo, sizeof(SCL2G_CLUB_LATELY_DONATION_INFO));
		}

		CLUBCONFIGMANAGER.MakeClubCashShopFirstDataInfo(PacketComposer);
		
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubDonationReq(SG2CL_CLUB_DONATION_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_DONATION_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_DONATION_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubDonationRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_DONATION_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_DONATION_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_DONATION_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.iClubCL);
		pUser->Send(&PacketComposer);
		pUser->SendUserGold();
	}
}

void CClubSvrProxy::SendClubCreateReq(SG2CL_CLUB_CREATE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_CREATE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_CREATE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubCreateRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CREATE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CREATE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		SClubInfo sClubInfo;
		sClubInfo.iClubSN			= rs.iClubSN;
		memcpy(sClubInfo.szClubName, rs.szClubName, MAX_CLUB_NAME_LENGTH+1);
		sClubInfo.iClubMarkCode		= rs.iClubMarkCode;
		sClubInfo.btGrade			= rs.btGrade;
		sClubInfo.btClubPosition	= rs.btClubPosition;
		if(rs.btResult == CLUB_CREATE_RESULT_SUCCESS)
		{
			pUser->SetClub(CLUB_INFO_REQUEST_KIND_CLUBPAGE, sClubInfo, 0);
			pUser->AddLobbyClubUser();
		}
		
		CPacketComposer	PacketComposer(S2C_CLUB_CREATE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.iClubSN);	
		pUser->Send(&PacketComposer);
		pUser->SendUserGold();

		SG2CL_CLUB_INFO_REQ info;
		info.iClubSN = rs.iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.btKind	= CLUB_INFO_REQUEST_KIND_LOGIN;
		info.iCheerLeaderIndex = pUser->GetCheerLeaderCode();
		SendClubInfoReq(info);

		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_JOIN_CLUB);
	}
}

void CClubSvrProxy::SendClubMemberRecordListReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_MEMBER_RECORD_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubMemberRecordListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_MEMBER_RECORD_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MEMBER_RECORD_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_MEMBER_RECORD_LIST_RES);
		PacketComposer.Add((PBYTE)&rs.bIsLast, sizeof(bool));
		PacketComposer.Add(rs.btMemberCnt);

		SCL2G_CLUB_MEMBER_RECORD_INFO info;
		for(int i = 0; i < rs.btMemberCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_MEMBER_RECORD_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_MEMBER_RECORD_INFO));
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubRecentMatchRecordListReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_RECENT_MATCH_RECORD_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubRecentMatchRecordListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_RECENT_MATCH_RECORD_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_RECENT_MATCH_RECORD_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_RECENT_MATCH_RECORD_LIST_RES);
		PacketComposer.Add(rs.btCnt);

		SCL2G_CLUB_RECENT_MATCH_RECORD_INFO info;
		for(int i = 0; i < rs.btCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_RECENT_MATCH_RECORD_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_RECENT_MATCH_RECORD_INFO));
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubPRListReq(SG2CL_CLUB_PR_LIST_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_PR_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_PR_LIST_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubPRListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_PR_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_PR_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_PR_LIST_RES);
		PacketComposer.Add(rs.btCnt);

		SCL2G_CLUB_PR_INFO info;
		for(int i = 0; i < rs.btCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_PR_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_PR_INFO));
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubPRChangeReq(SG2CL_CLUB_PR_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_PR_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_PR_CHANGE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubPRChangeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_PR_CHANGE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_PR_CHANGE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_PR_CHANGE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.btKind);
		PacketComposer.Add((PBYTE)&rs.szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1);
		PacketComposer.Add((PBYTE)&rs.szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);
		PacketComposer.Add(rs.iRemaintime);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendMyClubRankReq(SG2CL_MY_CLUB_RANK_REQ info)
{
	CPacketComposer PacketComposer(G2CL_MY_CLUB_RANK_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_MY_CLUB_RANK_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_MyClubRankRes(CReceivePacketBuffer* rp)
{
	SCL2G_MY_CLUB_RANK_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_MY_CLUB_RANK_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_MY_CLUB_RANK_RES);
		PacketComposer.Add(rs.iRank);
		PacketComposer.Add(rs.iClubSN);
		PacketComposer.Add((PBYTE)rs.szClubName, MAX_CLUB_NAME_LENGTH+1);
		PacketComposer.Add(rs.iClubMarkCode);
		PacketComposer.Add(rs.btLv);
		PacketComposer.Add(rs.iExp);
		PacketComposer.Add(rs.btGrade);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubRankListReq(SG2CL_CLUB_RANK_LIST_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_RANK_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_RANK_LIST_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubRankListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_RANK_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_RANK_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_RANK_LIST_RES);
		PacketComposer.Add(rs.btKind);
		PacketComposer.Add(rs.btSeasonType);
		PacketComposer.Add(rs.btClubCnt);

		SCL2G_CLUB_RANK_INFO info;
		for(int i = 0; i < rs.btClubCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_RANK_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_RANK_INFO));
		}
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubCheerLeaderInfoReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_CHEERLEADER_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubCheerLeaderInfoRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CHEERLEADER_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CHEERLEADER_INFO_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_CHEERLEADER_INFO_RES);
		PacketComposer.Add(rs.iClubSN);
		PacketComposer.Add(rs.iCheerLeaderCode);
		PacketComposer.Add(rs.btClubStatus);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubBuyCheerLeaderInfoReq(SG2CL_CLUB_BUY_CHEERLEADER_INFO_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_BUY_CHEERLEADER_INFO_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_BUY_CHEERLEADER_INFO_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubBuyCheerLeaderInfoRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_BUY_CHEERLEADER_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_BUY_CHEERLEADER_INFO_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_BUY_CHEERLEADER_POPUP_RES);
		PacketComposer.Add(rs.iCheerLeaderCode);

		PacketComposer.Add(rs.btPriceCount);
		PacketComposer.Add((PBYTE)rs.Price, sizeof(SCheerLeaderPrice)*rs.btPriceCount);

		PacketComposer.Add(rs.iClubCL);
		PacketComposer.Add(rs.btGrade);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubBuyCheerLeaderReq(SG2CL_CLUB_BUY_CHEERLEADER_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_BUY_CHEERLEADER_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_BUY_CHEERLEADER_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubBuyCheerLeaderRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_BUY_CHEERLEADER_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_BUY_CHEERLEADER_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_BUY_CHEERLEADER_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.iClubCL);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubCheerLeaderListReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_CHEERLEADER_LIST_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubCheerLeaderListRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CHEERLEADER_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CHEERLEADER_LIST_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_CHEERLEADER_LIST_RES);

		int iCheerLeaderCode = 0;
		BYTE btTypeNum = 0;

		if( FS_CLUB == pUser->GetClient()->GetState() )
		{
			iCheerLeaderCode = rs.sCheerLeaderInfo.iCheerLeaderCode;
			btTypeNum = rs.sCheerLeaderInfo.btTypeNum;
		}
		else if(0 < pUser->GetCheerLeaderCode())
		{
			iCheerLeaderCode = pUser->GetCheerLeaderCode();
			btTypeNum = pUser->GetCheerLeaderTypeNum();
		}
		else
		{
			iCheerLeaderCode = rs.sCheerLeaderInfo.iCheerLeaderCode;
			btTypeNum = rs.sCheerLeaderInfo.btTypeNum;
		}

		PacketComposer.Add(iCheerLeaderCode);		
		PacketComposer.Add(btTypeNum);

		int iCheerLeaderTime = pUser->GetUserCheerLeader()->GetRemainTime(iCheerLeaderCode);
		int iRemainEffectCnt = pUser->GetUserCheerLeader()->GetCheerLeaderEffectRemainCnt(iCheerLeaderCode, btTypeNum);

		PacketComposer.Add(iCheerLeaderTime);
		PacketComposer.Add(iRemainEffectCnt);

		BYTE btCnt = rs.btCnt;
		if(FS_CLUB != pUser->GetClient()->GetState())
		{
			pUser->GetUserCheerLeader()->MakeCheerLeaderList(PacketComposer, btCnt);
		}
		else
		{
			PacketComposer.Add(btCnt);
		}
		
		SCL2G_CLUB_CHEERLEADER_INFO info;
		for(int i = 0; i < btCnt; i++)
		{
			rp->Read((BYTE*)&info, sizeof(SCL2G_CLUB_CHEERLEADER_INFO));
			PacketComposer.Add((PBYTE)&info, sizeof(SCL2G_CLUB_CHEERLEADER_INFO));
		}
		
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubCheerLeaderChangeReq(SG2CL_CLUB_CHEERLEADER_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_CHEERLEADER_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_CHEERLEADER_CHANGE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubCheerLeaderChangeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CHEERLEADER_CHANGE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CHEERLEADER_CHANGE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_CHEERLEADER_CHANGE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.iCheerLeaderCode);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::Process_ClubCheerLeaderChangedNotice(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CHEERLEADER_CHANGED_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CHEERLEADER_CHANGED_NOTICE));

	CFSGameServer::GetInstance()->SetClubCheerLeaderCode(rs.iClubSN, rs.iChangedCheerLeaderCode, rs.btChangedCheerLeaderTypeNum);
}

void CClubSvrProxy::Process_ClubCheerLeaderExpiredNotice(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_CHEERLEADER_EXPIRED_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_CHEERLEADER_EXPIRED_NOTICE));

	CFSGameServer::GetInstance()->SetClubCheerLeaderCode(rs.iClubSN, rs.iExpiredCheerLeaderCode, rs.iChangedCheerLeaderCode);
}

void CClubSvrProxy::SendClubMyCheerLeaderChangeReq(SG2CL_CLUB_MY_CHEERLEADER_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_MY_CHEERLEADER_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_MY_CHEERLEADER_CHANGE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubMyCheerLeaderChangeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_MY_CHEERLEADER_CHANGE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MY_CHEERLEADER_CHANGE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		if(rs.btResult == CLUB_MY_CHEERLEADER_CHANGE_RESULT_SUCCESS)
		{
			CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
			CHECK_NULL_POINTER_VOID(pODBC);

			if (ODBC_RETURN_SUCCESS != pODBC->AVATAR_ChangeCheerLeader(pUser->GetGameIDIndex(), rs.iCheerLeaderCode))
			{
				WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, DB_DATA_UPDATE, FAIL, "AVATAR_ChangeCheerLeader - GameIDIndex:10752790", pUser->GetGameIDIndex());	
turn;
			}
			pUser->SetCheerLeaderCode(rs.iCheerLeaderCode);

			CMatchSvrProxy* pMatchSvrProxy = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
			if(pMatchSvrProxy)
			{
				SG2M_CHEERLEADER_CHANGE_REQ info;
				info.iGameIDIndex = pUser->GetGameIDIndex();
				info.iCheerLeaderCode = rs.iCheerLeaderCode;
				pMatchSvrProxy->SendCheerLeaderChangeReq(info);
			}
		}

		CPacketComposer	PacketComposer(S2C_CLUB_MY_CHEERLEADER_CHANGE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.iCheerLeaderCode);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendClubStatusChangeReq(SG2CL_CLUB_STATUS_CHANGE_REQ info)
{
	CPacketComposer PacketComposer(G2CL_CLUB_STATUS_CHANGE_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_CLUB_STATUS_CHANGE_REQ));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_ClubStatusChangeRes(CReceivePacketBuffer* rp)
{
	SCL2G_CLUB_STATUS_CHANGE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_STATUS_CHANGE_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_STATUS_CHANGE_RES);
		PacketComposer.Add(rs.btResult);
		PacketComposer.Add(rs.btStatus);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::SendMyClubPRReq(SG2CL_DEFAULT info)
{
	CPacketComposer PacketComposer(G2CL_MY_CLUB_PR_REQ);
	PacketComposer.Add((BYTE*)&info, sizeof(SG2CL_DEFAULT));
	Send(&PacketComposer);
}

void CClubSvrProxy::Process_MyClubPRRes(CReceivePacketBuffer* rp)
{
	SCL2G_MY_CLUB_PR_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_MY_CLUB_PR_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	if(pUser != NULL)
	{
		CPacketComposer	PacketComposer(S2C_MY_CLUB_PR_RES);
		PacketComposer.Add((PBYTE)&rs.szClubName, MAX_CLUB_NAME_LENGTH+1);
		PacketComposer.Add(&rs.btGrade);
		PacketComposer.Add(&rs.iClubMarkCode);
		PacketComposer.Add((PBYTE)&rs.szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1);
		PacketComposer.Add((PBYTE)&rs.szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);
		pUser->Send(&PacketComposer);
	}
}

void CClubSvrProxy::Process_UpdateClubNotice(CReceivePacketBuffer* rp)
{
	SCL2G_UPDATE_CLUB_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_UPDATE_CLUB_NOTICE));
	
	CPacketComposer	PacketComposer(S2C_UPDATE_CLUB_NOTICE);
	PacketComposer.Add(rs.btClubUpdateKind);
	PacketComposer.Add(rs.iUpdatedValue);

	LOBBYCLUBUSER.Broadcast(rs.iClubSN, PacketComposer);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_ANNOUNCE_NOT)
{
	SCL2G_ANNOUNCE_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_ANNOUNCE_NOT));

	int iMessageLength = strlen(info.szAnnounce);

	CFSGameServer::GetInstance()->BroadCastNotifyMsg(ANNOUNCE_OUTPUT_IN_TOP , info.szAnnounce, iMessageLength);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_INFO_RES)
{
	SCL2G_CLUB_MISSION_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MISSION_INFO_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_MISSION_INFO_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_MISSION_INFO_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_GET_BENEFIT_RES)
{
	SCL2G_CLUB_MISSION_GET_BENEFIT_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MISSION_GET_BENEFIT_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUB_MISSION_GET_BENEFIT_RES, &rs.info, sizeof(SS2C_CLUB_MISSION_GET_BENEFIT_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_COMPLETE_NOTICE)
{
	SCL2G_CLUB_MISSION_COMPLETE_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MISSION_COMPLETE_NOTICE));

	CPacketComposer	Packet(S2C_CLUB_MISSION_COMPLETE_NOTICE);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_MISSION_COMPLETE_NOTICE));
	
	LOBBYCLUBUSER.Broadcast(rs.iClubSN, Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_PENNANT_SLOT_INFO_RES)
{
	SCL2G_CLUB_PENNANT_SLOT_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_PENNANT_SLOT_INFO_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_PENNANT_SLOT_INFO_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_PENNANT_SLOT_INFO_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_PENNANT_INFO_RES)
{
	SCL2G_CLUB_PENNANT_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_PENNANT_INFO_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_PENNANT_INFO_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_PENNANT_INFO_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_USE_PENNANT_RES)
{
	SCL2G_CLUB_USE_PENNANT_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_USE_PENNANT_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUB_USE_PENNANT_RES, &rs.info, sizeof(SS2C_CLUB_USE_PENNANT_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_USE_PENNANT_NOTICE)
{
	SCL2G_CLUB_USE_PENNANT_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_USE_PENNANT_NOTICE));

	CFSGameServer::GetInstance()->SetClubPennantCode(rs.iClubSN, rs.stSlotIndex, rs.iPennantIndex);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_PENNANT_SLOT_RES)
{
	SCL2G_CLUB_BUY_PENNANT_SLOT_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_BUY_PENNANT_SLOT_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUB_BUY_PENNANT_SLOT_RES, &rs.info, sizeof(SS2C_CLUB_BUY_PENNANT_SLOT_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_CLOTHES_RES)
{
	SCL2G_CLUB_BUY_CLOTHES_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_BUY_CLOTHES_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUB_BUY_CLOTHES_RES, &rs.info, sizeof(SS2C_CLUB_BUY_CLOTHES_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_CLOTHES_NOTICE)
{
	SCL2G_CLUB_BUY_CLOTHES_NOTICE rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_BUY_CLOTHES_NOTICE));

	CFSGameServer::GetInstance()->GiveClubClothes(rs.iClubSN, rs.info.iItemCode);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_BUY_CLUBMARK_RES)
{
	SCL2G_CLUB_BUY_CLUBMARK_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_BUY_CLUBMARK_RES));

	CScopedRefGameUser pUser(rs.szGameID);
	CHECK_NULL_POINTER_VOID(pUser);

	if(rs.bGive || RESULT_CLUB_BUY_CLUBMARK_SUCCESS != rs.info.btResult)
	{
		pUser->Send(S2C_CLUB_BUY_CLUBMARK_RES, &rs.info, sizeof(SS2C_CLUB_BUY_CLUBMARK_RES));
		return;
	}
	else
	{
		SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
		CHECK_NULL_POINTER_VOID(pAvatar);

		BYTE btSellType = CLUBCONFIGMANAGER.GetClubMarkSellType(rs.info.iClubMarkCode);
		CHECK_CONDITION_RETURN_VOID(SELL_TYPE_CASH != btSellType);

		int iCost = CLUBCONFIGMANAGER.GetClubMarkPrice(rs.info.iClubMarkCode, rs.info.iPeriod);
		iCost = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, iCost);

		if(0 == iCost ||
			pUser->GetCoin() + pUser->GetEventCoin() < iCost)
		{
			rs.info.btResult = RESULT_CLUB_BUY_CLUBMARK_ENOUGH_CASH;
			pUser->Send(S2C_CLUB_BUY_CLUBMARK_RES, &rs.info, sizeof(SS2C_CLUB_BUY_CLUBMARK_RES));
			return;
		}

		SBillingInfo BillingInfo;
		BillingInfo.iEventCode = EVENT_BUY_CLUBMARK;
		pUser->SetBillingInfo(BillingInfo, btSellType, iCost);
		strcpy_s(BillingInfo.szItemName, "Buy ClubMark");
		BillingInfo.iParam0 = rs.info.iClubMarkCode;
		BillingInfo.iParam1 = rs.info.iPeriod;

		if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
		{
			rs.info.btResult = RESULT_CLUB_BUY_CLUBMARK_FAILED;
			pUser->Send(S2C_CLUB_BUY_CLUBMARK_RES, &rs.info, sizeof(SS2C_CLUB_BUY_CLUBMARK_RES));
			return;
		}
	}
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MEMBER_LOGIN_NOTICE)
{
	SG2CL_DEFAULT	rs;
	rp->Read((PBYTE)&rs, sizeof(SG2CL_DEFAULT));

	CPacketComposer	PacketComposer(S2C_CLUB_MEMBER_LOGIN_NOTICE);
	PacketComposer.Add((PBYTE)rs.szGameID, MAX_GAMEID_LENGTH+1);

	LOBBYCLUBUSER.Broadcast(rs.iClubSN, PacketComposer);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_INFO_RES)
{
	SCL2G_CLUBTOURNAMENT_INFO_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUBTOURNAMENT_INFO_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_INFO_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_UPDATE_CLUBTOURNAMENT_AGENCY_NOT)
{
	SCL2G_UPDATE_CLUBTOURNAMENT_AGENCY_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_UPDATE_CLUBTOURNAMENT_AGENCY_NOT));

	CLUBTOURNAMENTAGENCY.Update(info);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_MATCH_TBALE_INFO_RES)
{
	SCL2G_CLUBTOURNAMENT_MATCH_TBALE_INFO_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_MATCH_TBALE_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_MATCH_TBALE_INFO_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUBTOURNAMENT_MATCH_TBALE_INFO_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_KEY_PLAYER_LIST_RES)
{
	SCL2G_CLUBTOURNAMENT_KEY_PLAYER_LIST_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_KEY_PLAYER_LIST_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_KEY_PLAYER_LIST_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUBTOURNAMENT_KEY_PLAYER_LIST_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_RES)
{
	SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_RES)
{
	SCL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_RES)
{
	SCL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	if(rs.bIsEnter == TRUE)
	{
		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_CLUBTOURNAMENT);
		CHECK_NULL_POINTER_VOID(pMatch);

		SG2M_CLUBTOURNAMENT_ENTER_GAMEROOM_REQ rq;
		rq.info.btRoundGroupIndex = rs.btRoundGroupIndex;

		User->GetMatchLoginUserInfo(rq, pMatch->GetMatchType());

		pMatch->SendPacket(G2M_CLUBTOURNAMENT_ENTER_GAMEROOM_REQ, &rq, sizeof(SG2M_CLUBTOURNAMENT_ENTER_GAMEROOM_REQ)); 
	}
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_LIST_RES)
{
	SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_LIST_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_LIST_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_JOIN_PLAYER_LIST_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUBTOURNAMENT_JOIN_PLAYER_LIST_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_RES)
{
	SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_ROOM_INFO_RES)
{
	SCL2G_CLUBTOURNAMENT_VOTE_ROOM_INFO_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_VOTE_ROOM_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_VOTE_ROOM_INFO_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUBTOURNAMENT_VOTE_ROOM_INFO_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_RES)
{
	SCL2G_CLUBTOURNAMENT_VOTE_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_VOTE_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUBTOURNAMENT_VOTE_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_VOTE_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_VOTE_COUNT_NOT)
{
	SCL2G_CLUBTOURNAMENT_VOTE_COUNT_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_CLUBTOURNAMENT_VOTE_COUNT_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_VOTE_COUNT_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_VOTE_COUNT_NOT));
	FSLOBBY.BroadCastToClubTournament(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_REMAIN_TIME_NOT)
{
	SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_REMAIN_TIME_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_REMAIN_TIME_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_REMAIN_TIME_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_REMAIN_TIME_NOT));

	LOBBYCLUBUSER.Broadcast(info.iClubSN, Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_MATCH_RESULT_RES)
{
	SCL2G_CLUBTOURNAMENT_MATCH_RESULT_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_MATCH_RESULT_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUBTOURNAMENT_MATCH_RESULT_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_MATCH_RESULT_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GET_BENEFIT_ITEM_RES)
{
	SCL2G_CLUBTOURNAMENT_GET_BENEFIT_ITEM_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_GET_BENEFIT_ITEM_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	if(rs.info.btResult == RESULT_CLUBTOURNAMENT_GET_BENEFIT_ITEM_SUCCESS)
	{
		if(FALSE == User->GiveClubTournamentVoteUserBenefitItem(rs.info.iItemCode, rs.iItemIdx, rs.iUpdatedPropertyValue))
		{
			WRITE_LOG_NEW(LOG_TYPE_CLUBTOURNAMENT, SET_DATA, FAIL, "GiveClubTournamentVoteUserBenefitItem - GameIDIndex:10752790, ItemCode:0, ItemIdx:7106560", rs.iGameIDIndex, rs.info.iItemCode, rs.iItemIdx);

User->Send(S2C_CLUBTOURNAMENT_GET_BENEFIT_ITEM_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_GET_BENEFIT_ITEM_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GAME_START_INFO_RES)
{
	SCL2G_CLUBTOURNAMENT_GAME_START_INFO_RES rs;
	rp->Read((PBYTE)&rs, sizeof(SCL2G_CLUBTOURNAMENT_GAME_START_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUBTOURNAMENT_GAME_START_INFO_RES, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_GAME_START_INFO_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_CURRENT_ROUND_CLUB_STATUS_NOT)
{
	SCL2G_CLUBTOURNAMENT_CURRENT_ROUND_CLUB_STATUS_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_CLUBTOURNAMENT_CURRENT_ROUND_CLUB_STATUS_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_CURRENT_ROUND_CLUB_STATUS_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_CURRENT_ROUND_CLUB_STATUS_NOT));
	FSLOBBY.BroadCastToClubTournament(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GAME_END_NOT)
{
	SCL2G_CLUBTOURNAMENT_GAME_END_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_CLUBTOURNAMENT_GAME_END_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_GAME_END_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_GAME_END_NOT));
	FSLOBBY.BroadCastToClubTournament(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_WINTEAM_GAME_END_NOT)
{
	SCL2G_CLUBTOURNAMENT_WINTEAM_GAME_END_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_CLUBTOURNAMENT_WINTEAM_GAME_END_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_WINTEAM_GAME_END_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_WINTEAM_GAME_END_NOT));

	LOBBYCLUBUSER.Broadcast(info.iClubSN, Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_NOT)
{
	SCL2G_CLUBTOURNAMENT_NOT info;
	rp->Read((PBYTE)&info, sizeof(SCL2G_CLUBTOURNAMENT_NOT));

	CPacketComposer Packet(S2C_CLUBTOURNAMENT_NOT);
	Packet.Add((PBYTE)&info.info, sizeof(SS2C_CLUBTOURNAMENT_NOT));
	CFSGameServer::GetInstance()->BroadCast(Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GIVE_BENEFIT_CLUBITEM_NOTICE)
{
	SCL2G_CLUBTOURNAMENT_GIVE_BENEFIT_CLUBITEM_NOTICE info;
	rp->Read((BYTE*)&info, sizeof(SCL2G_CLUBTOURNAMENT_GIVE_BENEFIT_CLUBITEM_NOTICE));

	CFSGameServer::GetInstance()->GiveClubClothes(info.iClubSN, info.iItemCode);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_GIVE_BENEFIT_ITEM_NOTICE)
{
	SCL2G_CLUBTOURNAMENT_GIVE_BENEFIT_ITEM_NOTICE info;
	rp->Read((BYTE*)&info, sizeof(SCL2G_CLUBTOURNAMENT_GIVE_BENEFIT_ITEM_NOTICE));

	CScopedRefGameUser User(info.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->RecvPresent(MAIL_PRESENT_ON_AVATAR, info.iPresentIndex);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUBTOURNAMENT_TIME_INFO_RES)
{
	SCL2G_CLUBTOURNAMENT_TIME_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUBTOURNAMENT_TIME_INFO_RES));

	if(rs.iGameIDIndex > 0)
	{
		CScopedRefGameUser User(rs.iGameIDIndex);
		CHECK_NULL_POINTER_VOID(User);

		User->Send(S2C_CLUBTOURNAMENT_GAMEROOM_TIME_INFO_NOT, &rs.info, sizeof(SS2C_CLUBTOURNAMENT_GAMEROOM_TIME_INFO_NOT));
	}
	else
	{
		CPacketComposer Packet(S2C_CLUBTOURNAMENT_GAMEROOM_TIME_INFO_NOT);
		Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUBTOURNAMENT_GAMEROOM_TIME_INFO_NOT));
		FSLOBBY.BroadCastToClubTournamentVoteRoom(&Packet, MAX_CLUBTOURNAMENT_ROUND_GROUP_INDEX_COUNT);
	}
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_INTENSIVEPRACTICE_RANK_LIST_RES)
{
	SCL2G_INTENSIVEPRACTICE_RANK_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_INTENSIVEPRACTICE_RANK_LIST_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	rs.info.iMyBestPoint = User->GetUserIntensivePractice()->GetBestPoint(rs.info.iPracticeType);

	CPacketComposer Packet(S2C_INTENSIVEPRACTICE_RANK_LIST_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_INTENSIVEPRACTICE_RANK_LIST_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_LEAGUE_INFO_RES)
{
	SCL2G_CLUB_LEAGUE_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_LEAGUE_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_LEAGUE_INFO_RES);
	rs.info.btServerIndex = _GetServerIndex;
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_LEAGUE_INFO_RES));
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_RANKING_INFO_RES)
{
	SCL2G_CLUB_RANKING_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_RANKING_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_RANKING_INFO_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_RANKING_INFO_RES));
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_RANKING_LIST_RES)
{
	SCL2G_CLUB_RANKING_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_RANKING_LIST_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_RANKING_LIST_RES);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_RANKING_LIST_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_RANKING_NOT)
{
	SCL2G_CLUB_UPDATE_RANKING_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_UPDATE_RANKING_NOT));

	LOBBYCLUBUSER.UpdateClubContributionPointRankSeasonIndex(rs.iSeasonIndex);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_LEAGUE_RANKING_NOT)
{
	SCL2G_CLUB_UPDATE_LEAGUE_RANKING_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_UPDATE_LEAGUE_RANKING_NOT));

	LOBBYCLUBUSER.UpdateClubLeagueRankSeasonIndex(rs.iSeasonIndex);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_MEMBER_RANKING_NOT)
{
	SCL2G_CLUB_UPDATE_MEMBER_RANKING_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_UPDATE_MEMBER_RANKING_NOT));

	vector<SCLUB_UPDATE_MEMBER_RANKING> vRank;
	SCLUB_UPDATE_MEMBER_RANKING rank;
	for(int i = 0; i < rs.iMemberCnt; ++i)
	{
		rp->Read((BYTE*)&rank, sizeof(SCLUB_UPDATE_MEMBER_RANKING));
		vRank.push_back(rank);
	}

	LOBBYCLUBUSER.UpdateUserClubContributionPointRank(rs.iSeasonIndex, vRank);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_UPDATE_MEMBER_LEAGUE_RANKING_NOT)
{
	SCL2G_CLUB_UPDATE_MEMBER_LEAGUE_RANKING_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_UPDATE_MEMBER_LEAGUE_RANKING_NOT));

	vector<SCLUB_UPDATE_MEMBER_RANKING> vRank;
	SCLUB_UPDATE_MEMBER_RANKING rank;
	for(int i = 0; i < rs.iMemberCnt; ++i)
	{
		rp->Read((BYTE*)&rank, sizeof(SCLUB_UPDATE_MEMBER_RANKING));
		vRank.push_back(rank);
	}

	LOBBYCLUBUSER.UpdateUserClubLeagueRank(rs.iSeasonIndex, vRank);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_TOURNAMENT_RANKING_INFO_RES)
{
	SCL2G_CLUB_TOURNAMENT_RANKING_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_TOURNAMENT_RANKING_INFO_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUB_TOURNAMENT_RANKING_INFO_RES, &rs.info, sizeof(SS2C_CLUB_TOURNAMENT_RANKING_INFO_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_TOURNAMENT_RANKING_LIST_RES)
{
	SCL2G_CLUB_TOURNAMENT_RANKING_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_TOURNAMENT_RANKING_LIST_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_TOURNAMENT_RANKING_LIST_RES);
	SS2C_CLUB_TOURNAMENT_RANKING_LIST_RES rankCnt;
	rankCnt.iRankCount = rs.info.iRankCount;
	Packet.Add((PBYTE)&rankCnt, sizeof(SS2C_CLUB_TOURNAMENT_RANKING_LIST_RES));

	SCLUB_TOURNAMENT_RANKING info;
	for(int i = 0; i < rs.info.iRankCount; ++i)
	{
		rp->Read((BYTE*)&info, sizeof(SCLUB_TOURNAMENT_RANKING));
		Packet.Add((PBYTE)&info, sizeof(SCLUB_TOURNAMENT_RANKING));
	}
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES)
{
	SCL2G_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer Packet(S2C_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES);
	SS2C_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES rankCnt;
	rankCnt.bLastList = rs.info.bLastList;
	rankCnt.btListIndex = rs.info.btListIndex;
	rankCnt.iRankCount = rs.info.iRankCount;
	Packet.Add((PBYTE)&rankCnt, sizeof(SS2C_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_RES));

	SCLUB_TOURNAMENT_CLUBCUP_RANKING info;
	for(int i = 0; i < rs.info.iRankCount; ++i)
	{
		rp->Read((BYTE*)&info, sizeof(SCLUB_TOURNAMENT_CLUBCUP_RANKING));
		Packet.Add((PBYTE)&info, sizeof(SCLUB_TOURNAMENT_CLUBCUP_RANKING));
	}
	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_USER_UPDATE_GAMERESULT_NOT)
{
	SCL2G_CLUB_USER_UPDATE_GAMERESULT_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_USER_UPDATE_GAMERESULT_NOT));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->UpdateGameReultClubInfo(rs);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_MISSION_GIVE_REWARD_NOT)
{
	SCL2G_CLUB_MISSION_GIVE_REWARD_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_MISSION_GIVE_REWARD_NOT));

	vector<SCLUB_MISSION_GIVE_REWARD_INFO> vPresent;
	SCLUB_MISSION_GIVE_REWARD_INFO info;
	for(int i = 0; i < rs.iMemberCnt; ++i)
	{
		rp->Read((BYTE*)&info, sizeof(SCLUB_MISSION_GIVE_REWARD_INFO));
		vPresent.push_back(info);
	}

	CFSGameServer::GetInstance()->GiveClubMissionReward(vPresent);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_COLLECTOR_NOT)
{
	SCL2G_CLUB_COLLECT_LETTER_EVENT_COLLECTOR_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_COLLECT_LETTER_EVENT_COLLECTOR_NOT));

	CPacketComposer	Packet(S2C_CLUB_COLLECT_LETTER_EVENT_COLLECTOR_NOT);
	Packet.Add((PBYTE)&rs.sData, sizeof(SS2C_CLUB_COLLECT_LETTER_EVENT_COLLECTOR_NOT));

	LOBBYCLUBUSER.Broadcast(rs.iClubSN, Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_INFO_RES)
{
	SCL2G_CLUB_COLLECT_LETTER_EVENT_INFO_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_COLLECT_LETTER_EVENT_INFO_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	CPacketComposer	Packet(S2C_CLUB_COLLECT_LETTER_EVENT_INFO_RES);
	Packet.Add((PBYTE)&rs.sInfo, sizeof(SS2C_CLUB_COLLECT_LETTER_EVENT_INFO_RES));
	Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());

	User->Send(&Packet);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES)
{
	SCL2G_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES));

	CScopedRefGameUser User(rs.szGameID);
	CHECK_NULL_POINTER_VOID(User);

	SS2C_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES	ss;
	ss.eResult = rs.eResult;

	User->Send(S2C_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES, &ss, sizeof(SS2C_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_NAME_CHANGE_NOT)
{
	SCL2G_CLUB_NAME_CHANGE_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_NAME_CHANGE_NOT));

	LOBBYCLUBUSER.SetClubName(rs.iClubIndex, rs.szClubName);
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_NOTICE_RES)
{
	SCL2G_CLUB_NOTICE_RES rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_NOTICE_RES));

	CScopedRefGameUser User(rs.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	User->Send(S2C_CLUB_NOTICE_RES, &rs.info, sizeof(SS2C_CLUB_NOTICE_RES));
}

DEFINE_CLUBPROXY_PROC_FUNC(CL2G_CLUB_NOTICE_NOT)
{
	SCL2G_CLUB_NOTICE_NOT rs;
	rp->Read((BYTE*)&rs, sizeof(SCL2G_CLUB_NOTICE_NOT));

	CPacketComposer Packet(S2C_CLUB_NOTICE_NOT);
	Packet.Add((PBYTE)&rs.info, sizeof(SS2C_CLUB_NOTICE_NOT));
	LOBBYCLUBUSER.Broadcast(rs.iClubIndex, Packet, FALSE);
}
