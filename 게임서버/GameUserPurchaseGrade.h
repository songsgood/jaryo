qpragma once

class CFSGameUser;

struct SCurrentGradeInfo
{
	int iCurrentMonth;		// yyyymm
	BYTE btCurrentGrade;	// ePURCHASE_GRADE_TYPE

	SCurrentGradeInfo(int _iCurrentMonth, BYTE _btCurrentGrade)
		: iCurrentMonth(_iCurrentMonth)
		, btCurrentGrade(_btCurrentGrade)
	{	}
};

class CGameUserPurchaseGrade
{
public:
	CGameUserPurchaseGrade(CFSGameUser* pUser);
	~CGameUserPurchaseGrade(void);

	BOOL				Load();
	BOOL				LoadUserInfo(int iCurrentMonth = GetYYYYMMDD() / 100);
	BOOL				CheckResetMonth(int iCurrentMonth = GetYYYYMMDD() / 100);
	void				SendCurrentGradeButton();
	void				SendUserInfo();
	void				SendUserGradeCardCombineRateUp();
	BOOL				GiveRewardEventCash();
	BOOL				GiveCardReward(int iItemCode, int iPropertyKind);
	BOOL				UseCash(int iUseCash);
	float				GetCardCombineRateUpValue();
	int					GetCurrentGradeCount(BYTE btGrade);
	BOOL				CheckAndGiveAchievement();

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	SCurrentGradeInfo	m_sCurrentGradeInfo;
	int					m_iSumUseCash_Current;
	int					m_iSumUseCash_Next;
	int					m_iRecentCardOpenDate;	// yyyymmdd
	BOOL				m_bIsGetRewardEventCash;
	int					m_iaGradeCount[MAX_PURCHASE_GRADE_TYPE];
};

