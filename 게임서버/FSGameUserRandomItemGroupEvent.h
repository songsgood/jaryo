qpragma once

class CFSGameUser;
class CFSODBCBase;

#define RANDOMITEM_GROUP_NORMAL_COIN_ITEMCODE	52113421
#define RANDOMITEM_GROUP_PREMIUM_COIN_ITEMCODE	52113431

class CFSGameUserRandomItemGroupEvent
{
public:
	CFSGameUserRandomItemGroupEvent(CFSGameUser* pUser);
	~CFSGameUserRandomItemGroupEvent(void);

	BOOL Load();

	void SendEventOpenStatus();
	void SendEventInfo();
	void SendRewardList(BYTE btGroupType);
	void SendExchangeProductList();
	RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN UseCoinReq(BYTE btGroupType, SS2C_RANDOMITEM_GROUP_EVENT_USE_COIN_RES& rs);
	RESULT_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT ExchangeProductReq(int iProductIndex, SS2C_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_RES& rs);

private:
	CFSGameUser* m_pUser;
	BOOL m_bDataLoaded;

	int m_iaDrawCount[MAX_EVENT_RANDOMITEM_GROUP_TYPE_COUNT];
	int m_iaCardCount[MAX_EVENT_RANDOMITEM_CARD_TYPE_COUNT];
};

