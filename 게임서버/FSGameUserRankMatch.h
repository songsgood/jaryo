qpragma once
qinclude "Lock.h"
qinclude "FSMatchCommon.h"
qinclude "FSCommon.h"
qinclude "FSODBCCommon.h"

struct SLeague_TerminateInfo
{
	int iTerminateContinue;
	time_t tLastTerminateDate;

	SLeague_TerminateInfo()
		:iTerminateContinue(0)
		,tLastTerminateDate(0)
	{

	}
};

class CODBCSvrProxy;
class CFSGameUser;
class CRankMatchManager;
class CLeagueSvrProxy;
class CFSGameUserRankMatch
{

public:
	CFSGameUserRankMatch(CFSGameUser* pUser);
	virtual ~CFSGameUserRankMatch(void);

public:
	BOOL Load();
	BOOL LoadAvatarRankMatchPositionInfo();
	BOOL LoadAvatarRankMatchMyInfo();
	BOOL LoadAvatarSubTable();
	BOOL LoadUserNotice();
	BOOL LoadAvatarLimitMatchingInfo();
	BOOL SendPacketAvoidTeamInfo();
	BOOL ReqAvoidTeamSave(list<SRANKMATCH_AVOID_TEAM>&	listAvoidInfo, BYTE& btResult);
	BOOL CheckAvoidTeamLimit( BYTE btCount );
	BOOL CheckRankMatchTeamReg(void);
	BOOL SendAvatarRankMatchMyInfo();
	BOOL CheckPing();
	BOOL CheckAndSendPingCheck();
	BOOL CheckAndSendTerminate();
	BOOL CheckUserPingVal(BYTE btServerIndex);
	BOOL CheckUpdateGameIDInRank();	

public:
	void GetAvatarAvoidTeamIndex(list<BYTE>& listAVoidTeamIndex);
	void UpdateRankMatchScale(void);
	void UpdateRankMatchInfo(SRankMatchUserInfoBase& base);
	void UpdateResult(int& iResultType, int& iPromotion, int &iMyLeague,
		int &iMyRating, int &iLtotalPoint, int& iLaddPoint, float fTestRatingPoint, int iLeaguePopupMode
		,short shIsWin);
	BOOL GetLeagueInfo(BYTE& btTier, BYTE& btRating, int& iGroup);
	BOOL CheckPlacementTest();
	SLeagueSubTable* GetSubTable()	const;

	void GetLeagueMatchInfo(SLeagueMatchInfo& info);
	void SendInsertBasic(CODBCSvrProxy* pProxy);
	void SendUpdateBasic(SM2G_GAME_RESULT_RES* prs);
	void SendUpdateLeaguePoint();
	void SendUserRankMatchInfo();
	void SendRankMatchOpenStatus();
	void SendRankMatch_MatchOpenTime();
	void SendAutoTeamRegFail(BYTE btResult);
	void SendAutoTeamReg(CLeagueSvrProxy* pLeagueProxy = nullptr);
	void SendPingLog(SUserPingInfo* pInfo);
	void SendPreTier(BYTE btPreTier);
	void SendUpdateGameIDInRankPage(LPCTSTR szGameID);
	void SendUpdateGameIDInRankPage(SS2O_RANKMATCH_UPDATE_GAMEID_NOT& info);

	void UpdateSubTable();
	void SetRankMatchMyInfoLoad(BOOL bLoad);
	void UpdatePositionSet(int iRankMatchTeamIndex, BOOL bIsWin);
	void UpdateMatchInfo( SLeagueMatchInfo& sInfo);
	void UpdateLeaguePoint(int iLeaguePoint);
	void CheckAndSendNotice();
	float LoadLeagueRatingPoint();
	void SetAutoTeamRegInfo(SG2M_AUTO_TEAM_REG_REQ& req);
	void SetPreRewardTier(BYTE btTier);
	BOOL CheckRankMatchPageView(void);
	void GetPromotionInfo(int& iPromotion, int iPromotionCount);

	BOOL CheckLimitMatchingUser();
	void UpdateLimitMatchingInfo();
	void ClearLimitMatchingInfo();
	int DecreaseLimitMatchingTime();
	void SaveLimitMatchingInfo();

public:
	inline float GetLeagueRatingPoint(void)	
	{
		return m_fLeagueRatingPoint;
	}
	void SetLeagueRatingPoint(float fRatingPoint);

	inline BYTE GetPreRewardTier(void)
	{
		return m_btBasketBallSkinRewardTier;
	}

private:

	SLesagueAvatarTeamPosition	m_sLeagueAvatarTeamPosition;
	SRankMatchUserInfoBase		m_sLeagueAvatarMyInfo;
	CFSGameUser*				m_pUser;
	SLeagueSubTable				m_sLeagueAvatarSubTable;
	SUserNotice					m_sUserNotice;
	int							m_iLoginPenaltyPoint;

	SC2S_AUTO_TEAM_REG_REQ		m_AutoTeamRegInfo;
	//stack			m_stackRankMatchScale;

	float						m_fLeagueRatingPoint; // just league
	SLeague_TerminateInfo		m_sTerminateInfo;
	DWORD						m_dwLastRankPageView;
	BYTE						m_btBasketBallSkinRewardTier;

	int							m_iMatchingRefusalCount;	// ��Ī ���� Ƚ��
	time_t						m_tMatchingLimitRemainTime;	// ��Ī �̿����� �����ð�
};

