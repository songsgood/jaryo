qpragma once
qinclude "CBillingManager.h"
qinclude "CNtreevBillManagerGame.h"

class CFSGameUser;

class CBillingManagerGame : public CBillingManager, public Singleton<CBillingManagerGame>
{
public:
	CBillingManagerGame();
	virtual ~CBillingManagerGame();
	
public:
	virtual int		GetCashBalance(CUser* pUser);
	virtual BOOL	RequestPurchaseItem(SBillingInfo &BillingInfo);
	virtual BOOL	BuyContents(SBillingInfo* pBillingInfo, int iCode);

protected:
	virtual CNtreevBillManager* CreateNtreevBillManager()	{	return new CNtreevBillManagerGame(); }

private:


};

#define BILLING_GAME CBillingManagerGame::GetInstance()