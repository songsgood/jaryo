qpragma once

class CFSGameUser;
class CFSODBCBase;

enum EVENT_PRIVATEROOM_LOGTYPE
{
	EVENT_PRIVATEROOM_LOGTYPE_GET_TICKET,
	EVENT_PRIVATEROOM_LOGTYPE_USE_TICKET
};

#define EVENT_PRIVATEROOM_TODAY_MAX_TICKET_COUNT	70

typedef map<int/*_btMissionType*/, SEventPrivateRoomUserMission> PRIVATEROOM_USER_MISSION_MAP;
typedef set<int/*_iProductIndex*/> PRIVATEROOM_USER_PRODUCT_GET_LOG_SET;

class CFSGameUserPrivateRoomEvent
{
public:
	CFSGameUserPrivateRoomEvent(CFSGameUser* pUser);
	~CFSGameUserPrivateRoomEvent(void);

	BOOL Load();

	void SetUserMission(vector<SEventPrivateRoomUserMission>& vUserMission);
	void SetUserProductGetLog(vector<int>& vProductIndex);

	void SendEventInfo();
	void SendExchangeItemInfo();
	RESULT_EVENT_PRIVATEROOM_GET_PRODUCT GetProductReq(int iProductIndex, int iPropertyValue);
	RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM GetExchangeItemReq(int iRewardIndex, int& iUpdatedTicketCount);
	void CheckMission(BYTE btMissionType);

private:
	void ResetMission();

private:
	PRIVATEROOM_USER_MISSION_MAP m_mapUserMission;
	PRIVATEROOM_USER_PRODUCT_GET_LOG_SET m_setUserProductGetLog;
	int m_iTicketCount;
	int m_iTodayTicketCount;
	int m_iUpdateDate;
	int m_iUpdateHour;

	CFSGameUser* m_pUser;
	BOOL m_bDataLoaded;

};

