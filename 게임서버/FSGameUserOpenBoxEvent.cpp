qinclude "stdafx.h"
qinclude "OpenBoxEventManager.h"
qinclude "FSGameUserOpenBoxEvent.h"
qinclude "TUser.h"
qinclude "FSServer.h"
qinclude "RewardManager.h"
qinclude "UserHighFrequencyItem.h"

CFSGameUserOpenBoxEvent::CFSGameUserOpenBoxEvent(CUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_btBoxType(SEVENT_OPEN_BOX_TYPE_NONE)
{
}

CFSGameUserOpenBoxEvent::~CFSGameUserOpenBoxEvent(void)
{
}

BOOL CFSGameUserOpenBoxEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == OPENBOXEVENT.IsOpen(), TRUE);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_OPEN_BOX_GetUserData(m_pUser->GetUserIDIndex(), m_btBoxType))
	{
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserOpenBoxEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == OPENBOXEVENT.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	SS2C_EVENT_OPEN_BOX_INFO_RES rs;
	rs.btCurrentBoxType = m_btBoxType;
	rs.iKeyCount = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_OPEN_BOX_KEY);
	if(NULL != pItem)
		rs.iKeyCount = pItem->iPropertyTypeValue;

	m_pUser->Send(S2C_EVENT_OPEN_BOX_INFO_RES, &rs, sizeof(SS2C_EVENT_OPEN_BOX_INFO_RES));
}

RESULT_EVENT_OPEN_BOX_UPDATE CFSGameUserOpenBoxEvent::UpdateOpenBoxReq(BYTE btType, SS2C_EVENT_OPEN_BOX_UPDATE_RES& rs)
{
	rs.iUpdatedBoxType = m_btBoxType;
	rs.iUpdatedKeyCount = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_OPEN_BOX_KEY);
	if(NULL != pItem)
		rs.iUpdatedKeyCount = pItem->iPropertyTypeValue;

	ZeroMemory(&rs.sReward, sizeof(SREWARD_INFO));

	CHECK_CONDITION_RETURN(CLOSED == OPENBOXEVENT.IsOpen(), RESULT_EVENT_OPEN_BOX_UPDATE_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_OPEN_BOX_UPDATE_FAIL);
	CHECK_CONDITION_RETURN(btType >= MAX_EVENT_OPEN_BOX_UPDATE_TYPE_COUNT, RESULT_EVENT_OPEN_BOX_UPDATE_FAIL);
	CHECK_CONDITION_RETURN(0 >= rs.iUpdatedKeyCount, RESULT_EVENT_OPEN_BOX_UPDATE_NOT_EXIST_KEY);

	int iUpdatedBoxType = m_btBoxType;
	int iRewardIndex = 0;
	if(SEVENT_OPEN_BOX_UPDATE_TYPE_OPEN == btType)
	{
		CHECK_CONDITION_RETURN(SEVENT_OPEN_BOX_TYPE_NONE >= m_btBoxType || MAX_EVENT_OPEN_BOX_TYPE_COUNT <= m_btBoxType, RESULT_EVENT_OPEN_BOX_UPDATE_FAIL);
		CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_OPEN_BOX_UPDATE_FULL_MAILBOX);

		SRewardInfo sReward;
		ZeroMemory(&sReward, sizeof(sReward));
		OPENBOXEVENT.GetRandomReward(m_btBoxType, sReward);
		CHECK_CONDITION_RETURN(0 == sReward.iRewardIndex, RESULT_EVENT_OPEN_BOX_UPDATE_FAIL);

		rs.sReward.btRewardType = sReward.btRewardType;
		rs.sReward.iItemCode = sReward.iItemCode;
		rs.sReward.iPropertyType = sReward.iPropertyType;
		rs.sReward.iPropertyValue = sReward.iPropertyValue;

		iUpdatedBoxType = SEVENT_OPEN_BOX_TYPE_D;
		iRewardIndex = sReward.iRewardIndex;
	}
	else if(SEVENT_OPEN_BOX_UPDATE_TYPE_UPGRADE == btType)
	{
		if(0 == rand() )
)
			iUpdatedBoxType++;

		CHECK_CONDITION_RETURN(SEVENT_OPEN_BOX_TYPE_NONE >= iUpdatedBoxType || MAX_EVENT_OPEN_BOX_TYPE_COUNT <= iUpdatedBoxType, RESULT_EVENT_OPEN_BOX_UPDATE_FAIL);
	}

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_OPEN_BOX_UPDATE_FAIL);

	int iUpdated_KeyCount = 0;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_OPEN_BOX_UpdateUserData(m_pUser->GetUserIDIndex(), btType, m_btBoxType, iUpdatedBoxType, iRewardIndex, iUpdated_KeyCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_OPENBOX_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_OPEN_BOX_UpdateUserData - UserIDIndex:10752790, btType:0", m_pUser->GetUserIDIndex(), btType);
urn RESULT_EVENT_OPEN_BOX_UPDATE_FAIL;
	}

	int iPreBoxType = m_btBoxType;
	m_btBoxType = iUpdatedBoxType;

	if(0 >= iUpdated_KeyCount)
		m_pUser->GetUserHighFrequencyItem()->DeleteUserHighFrequencyItem(ITEM_PROPERTY_KIND_OPEN_BOX_KEY);
	else
		m_pUser->GetUserHighFrequencyItem()->UpdateUserHighFrequencyItemCount(ITEM_PROPERTY_KIND_OPEN_BOX_KEY, iUpdated_KeyCount);

	rs.iUpdatedBoxType = m_btBoxType;
	rs.iUpdatedKeyCount = iUpdated_KeyCount;

	if(SEVENT_OPEN_BOX_UPDATE_TYPE_OPEN == btType)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
		CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_OPEN_BOX_UPDATE_FAIL);
	}

	if(SEVENT_OPEN_BOX_UPDATE_TYPE_UPGRADE == btType &&
		iPreBoxType == iUpdatedBoxType)
		return RESULT_EVENT_OPEN_BOX_UPDATE_FAIL;

	return RESULT_EVENT_OPEN_BOX_UPDATE_SUCCESS;
}

BOOL CFSGameUserOpenBoxEvent::GiveKey()
{
	CHECK_CONDITION_RETURN(CLOSED == OPENBOXEVENT.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBC, FALSE);

	int iUpdated_KeyCount = 0;
	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_OPEN_BOX_GiveKey(m_pUser->GetUserIDIndex(), iUpdated_KeyCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_OPENBOX_EVENT, DB_DATA_UPDATE, FAIL, "EVENT_OPEN_BOX_GiveKey - UserIDIndex:10752790, KeyCount:0", m_pUser->GetUserIDIndex(), iUpdated_KeyCount);
urn FALSE;
	}

	CUserHighFrequencyItem* pHighFrequencyItem = m_pUser->GetUserHighFrequencyItem();
	if(pHighFrequencyItem != NULL)
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_OPEN_BOX_KEY;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdated_KeyCount;
		pHighFrequencyItem->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}

	return TRUE;
}

BYTE CFSGameUserOpenBoxEvent::GetBoxType()
{
	CHECK_CONDITION_RETURN(CLOSED == OPENBOXEVENT.IsOpen(), SEVENT_OPEN_BOX_TYPE_NONE);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, SEVENT_OPEN_BOX_TYPE_NONE);

	return m_btBoxType;
}

int CFSGameUserOpenBoxEvent::GetKeyCount()
{
	CHECK_CONDITION_RETURN(CLOSED == OPENBOXEVENT.IsOpen(), 0);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, 0);

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_OPEN_BOX_KEY);
	if(NULL != pItem)
		return pItem->iPropertyTypeValue;

	return 0;
}

