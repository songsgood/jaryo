qpragma once
qinclude "CNtreevBillManager.h"

class CNtreevBillManagerGame : public CNtreevBillManager
{
public:
	CNtreevBillManagerGame() {};
	virtual ~CNtreevBillManagerGame() {};

protected:
	virtual bool ProgressCharge();
	virtual bool ProgressTotal();


};
