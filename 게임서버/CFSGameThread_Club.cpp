qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CFSRankManager.h"
qinclude "ClubSvrProxy.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubConfigManager.h"
qinclude "CFSGameUserItem.h"
qinclude "ClubTournamentAgency.h"
qinclude "MatchSvrProxy.h"
qinclude "CheerLeaderEventManager.h"
qinclude "CUserAction.h"
qinclude "CActionShop.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CFSGameThread::Process_FSEnterClubReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iSuccess = -1;

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy == NULL)
	{
		CPacketComposer PacketComposer(S2C_ENTER_CLUB_RES);
		PacketComposer.Add(iSuccess);
		pFSClient->Send(&PacketComposer);
		return;
	}
	
	if(TRUE == pFSClient->SetState(FS_CLUB))
	{
		iSuccess = 0;

		//pUser->GetUserItemList()->BackUpUseFeatureInfo();
		pUser->GetUserItemList()->BackUpUseTryFeatureInfo();
		pUser->GetUserItemList()->SetMode(ITEM_PAGE_MODE_CLUBSHOP);
	}

	CPacketComposer PacketComposer(S2C_ENTER_CLUB_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);

	SS2C_CLUB_CASH_DISCOUNT_NOT info;
	info.btDiscount = CLUBCONFIGMANAGER.IsClubCashDiscount();
	PacketComposer.Initialize(S2C_CLUB_CASH_DISCOUNT_NOT);
	PacketComposer.Add((PBYTE)&info, sizeof(SS2C_CLUB_CASH_DISCOUNT_NOT));
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSExitClubReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iSuccess = -1;
	if(TRUE == pFSClient->SetState(NEXUS_LOBBY))
	{
		iSuccess = 0;
		pUser->GetUserItemList()->SetMode(ITEM_PAGE_MODE_NONE);
	}
	
	CPacketComposer PacketComposer(S2C_EXIT_CLUB_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSClubInfoReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btKind = 0;
	int iClubSN = 0;
	m_ReceivePacketBuffer.Read(&btKind);
	m_ReceivePacketBuffer.Read(&iClubSN);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{
		SG2CL_CLUB_INFO_REQ info;
		info.btKind	= btKind;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.iCheerLeaderIndex = pUser->GetCheerLeaderCode();
		pClubProxy->SendClubInfoReq(info);
	}
}

void CFSGameThread::Process_FSClubLobbyMemberListReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{
		SG2CL_DEFAULT info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubLobbyMemberListReq(info);
	}
}

void CFSGameThread::Process_FSClubMemberListReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	m_ReceivePacketBuffer.Read(&iClubSN);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{
		SG2CL_DEFAULT info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubMemberListReq(info);
	}
}

void CFSGameThread::Process_FSClubListReq(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btPageNum = 0, btGrade = 0, btClubNameLen = 0;
	int iClubHighAreaIndex = -1, iClubAreaIndex = -1;
	DECLARE_INIT_TCHAR_ARRAY(szClubName, MAX_CLUB_NAME_LENGTH+1);

	m_ReceivePacketBuffer.Read(&btPageNum);
	m_ReceivePacketBuffer.Read(&btGrade);
	m_ReceivePacketBuffer.Read(&iClubHighAreaIndex);
	m_ReceivePacketBuffer.Read(&iClubAreaIndex);
	m_ReceivePacketBuffer.Read(&btClubNameLen);

	if(btClubNameLen < 1 || btClubNameLen > MAX_CLUB_NAME_LENGTH+1) 
	{
		btClubNameLen = 0;
	}

	if(btClubNameLen > 0)	
	{
		m_ReceivePacketBuffer.Read((BYTE*)szClubName, btClubNameLen);	
		szClubName[MAX_CLUB_NAME_LENGTH] = '\0';
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_LIST_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.btPageNum = btPageNum;
		info.btGrade = btGrade;
		info.iClubHighAreaIndex = iClubHighAreaIndex;
		info.iClubAreaIndex = iClubAreaIndex;
		info.btClubNameLen = btClubNameLen;
		strncpy_s(info.szClubName, _countof(info.szClubName), szClubName, MAX_CLUB_NAME_LENGTH);	
		pClubProxy->SendClubListReq(info); 
	}
}

void CFSGameThread::Process_FSClubMarkListReq(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	

		CPacketComposer Packet(G2CL_CLUB_MARK_LIST_REQ);
		Packet.Add((PBYTE)&info, sizeof(SG2CL_DEFAULT));
		pClubProxy->Send(&Packet);
	}
}

void CFSGameThread::Process_FSClubMarkChangeReq(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubMarkCode = 0;
	m_ReceivePacketBuffer.Read(&iClubMarkCode);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_MARK_CHANGE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.iClubMarkCode = iClubMarkCode;
		pClubProxy->SendClubMarkChangeReq(info); 
	}
}

void CFSGameThread::Process_FSClubGradeUpInfoReq(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SClubConfig* pClubConfig = CLUBCONFIGMANAGER.GetClubConfig(pUser->GetClubGrade());
	CHECK_NULL_POINTER_VOID(pClubConfig);

	SClubConfig* pNextGradeClubConfig  = CLUBCONFIGMANAGER.GetNextGradeClubConfig(pUser->GetClubGrade());
	CHECK_NULL_POINTER_VOID(pNextGradeClubConfig);

	CPacketComposer PacketComposer(S2C_CLUB_GRADE_UP_INFO_RES);
	PacketComposer.Add((BYTE)pNextGradeClubConfig->iClubGrade);
	PacketComposer.Add((BYTE)pNextGradeClubConfig->iClubLvCondition);
	PacketComposer.Add((BYTE)pNextGradeClubConfig->iMemberCountCondition);
	PacketComposer.Add((BYTE)pClubConfig->iMemberCount);
	PacketComposer.Add((BYTE)pNextGradeClubConfig->iMemberCount);
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_FSClubGradeUpReq(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubGradeUpReq(info); 
	}
}

void CFSGameThread::Process_FSClubNoticeChangeReq(CFSGameClient* pFSClient)	
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szNotice, MAX_CLUB_NOTICE_LENGTH+1);

	m_ReceivePacketBuffer.Read((PBYTE)szNotice, MAX_CLUB_NOTICE_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_NOTICE_CHANGE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szNotice, _countof(info.szNotice), szNotice, MAX_CLUB_NOTICE_LENGTH);	
		pClubProxy->SendClubNoticeChangeReq(info); 
	}
}

void CFSGameThread::Process_FSClubInviteReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);

	if(!strcmp(pUser->GetGameID(),szCharName))
	{ 
		CPacketComposer PacketComposer(S2C_CLUB_INVITE_RES);
		BYTE btResult = CLUB_INVITE_RESULT_FAIL_SELF_GAMEID;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);

		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_INVITE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubInviteReq(info); 
	}
}

void CFSGameThread::Process_FSClubInviteDataReq(CFSGameClient *pClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	BYTE btPaperIndex = 0;
	m_ReceivePacketBuffer.Read(&btPaperIndex);

	pUser->SendClubInvitation(btPaperIndex);
}

void CFSGameThread::Process_FSClubJoinReq(CFSGameClient* pFSClient)	
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	BYTE btPaperIndex = MAX_CLUB_INVITION_COUNT;

	m_ReceivePacketBuffer.Read(&iClubSN);
	m_ReceivePacketBuffer.Read(&btPaperIndex);

	if(0 < pUser->GetClubSN())
	{
		return;
	}

	if(pUser->CheckPaperExist(btPaperIndex) == FALSE)
	{
		return;
	}
	
	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_JOIN_REQ info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.btKind = CLUB_JOIN_KIND_INVITE;
		strncpy_s(info.szCharName, _countof(info.szCharName), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.btPosition = pUser->GetCurUsedAvatarPosition();
		info.btLv = pUser->GetCurUsedAvtarLv();
		info.btPaperIndex = btPaperIndex;
		pClubProxy->SendClubJoinReq(info); 
	}
}

void CFSGameThread::Process_FSClubJoinRequestReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	DECLARE_INIT_TCHAR_ARRAY(szMainPosition, MAX_MAIN_POSITION_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szPlayStyle, MAX_PLAY_STYLE_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szSelfIntroduction, MAX_SELFINTRODUCTION_LENGTH+1);

	m_ReceivePacketBuffer.Read(&iClubSN);
	m_ReceivePacketBuffer.Read((BYTE*)szMainPosition, MAX_MAIN_POSITION_LENGTH+1);
	m_ReceivePacketBuffer.Read((BYTE*)szPlayStyle, MAX_PLAY_STYLE_LENGTH+1);
	m_ReceivePacketBuffer.Read((BYTE*)szSelfIntroduction, MAX_SELFINTRODUCTION_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_JOIN_REQUEST_REQ info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.iGameIDIndex = pUser->GetGameIDIndex();
		strncpy_s(info.szMainPosition, _countof(info.szMainPosition), szMainPosition, MAX_MAIN_POSITION_LENGTH);	
		strncpy_s(info.szPlayStyle, _countof(info.szPlayStyle), szPlayStyle, MAX_PLAY_STYLE_LENGTH);	
		strncpy_s(info.szSelfIntroduction, _countof(info.szSelfIntroduction), szSelfIntroduction, MAX_SELFINTRODUCTION_LENGTH);	
		pClubProxy->SendClubJoinRequestReq(info); 
	}
}

void CFSGameThread::Process_FSClubJoinRequestListReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC( ODBC_CLUB );
	CHECK_NULL_POINTER_VOID( pClubBaseODBC );

	vector<SDBJoinRequestList> vecOutput;
	if(ODBC_RETURN_SUCCESS == pClubBaseODBC->CLUB_GetJoinRequestList(pUser->GetClubSN(), vecOutput))
	{
		CPacketComposer PacketComposer(S2C_CLUB_JOIN_REQUEST_LIST_RES);
		PacketComposer.Add((BYTE)vecOutput.size());

		vector<SDBJoinRequestList>::iterator itr = vecOutput.begin();
		vector<SDBJoinRequestList>::iterator endItr = vecOutput.end();
		for( ; itr != endItr; ++itr )
		{
			SDBJoinRequestList sInfo = *itr;
			PacketComposer.Add((BYTE)sInfo.iGamePosition);
			PacketComposer.Add((PBYTE)sInfo.szGameID, MAX_GAMEID_LENGTH+1);
			PacketComposer.Add((PBYTE)sInfo.szSelfIntroduction, MAX_SELFINTRODUCTION_LENGTH+1);
		}
		pUser->Send(&PacketComposer);
	}	
}

void CFSGameThread::Process_FSClubJoinRequestInfoReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC(ODBC_CLUB);
	CHECK_NULL_POINTER_VOID(pClubBaseODBC);

	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);

	int iGamePosition = 0, iLv = 0;
	char szMainPosition[MAX_MAIN_POSITION_LENGTH+1]; 
	char szPlayStyle[MAX_PLAY_STYLE_LENGTH+1];  
	char szSelfInstruction[MAX_SELFINTRODUCTION_LENGTH+1];
	memset(szMainPosition, 0, MAX_MAIN_POSITION_LENGTH+1);
	memset(szPlayStyle, 0, MAX_PLAY_STYLE_LENGTH+1);
	memset(szSelfInstruction, 0, MAX_SELFINTRODUCTION_LENGTH+1);

	CPacketComposer PacketComposer(S2C_CLUB_JOIN_REQUEST_INFO_RES);
	BYTE btResult = CLUB_JOIN_REQUEST_INFO_RESULT_SUCCESS;

	if(pUser->GetClubSN() > 0 &&  pUser->GetClubPosition() < CLUB_POSITION_MANAGER)
	{
		btResult = CLUB_JOIN_REQUEST_INFO_RESULT_FAIL_NOT_ADMINMEMBER;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}

	if(ODBC_RETURN_SUCCESS != pClubBaseODBC->CLUB_GetJoinRequest(pUser->GetClubSN(), szCharName, iGamePosition, iLv, szMainPosition, szPlayStyle, szSelfInstruction))
	{
		btResult = CLUB_JOIN_REQUEST_INFO_RESULT_FAIL_NOT_ADMINMEMBER;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}

	PacketComposer.Add(btResult);
	PacketComposer.Add((PBYTE)szCharName, MAX_GAMEID_LENGTH+1);
	PacketComposer.Add((BYTE)iGamePosition);
	PacketComposer.Add((BYTE)iLv);
	PacketComposer.Add((PBYTE)szMainPosition, MAX_MAIN_POSITION_LENGTH+1);
	PacketComposer.Add((PBYTE)szPlayStyle, MAX_PLAY_STYLE_LENGTH+1);
	PacketComposer.Add((PBYTE)szSelfInstruction, MAX_SELFINTRODUCTION_LENGTH+1);
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_FSClubRequestAnswerReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC( ODBC_CLUB );
	CHECK_NULL_POINTER_VOID(pClubBaseODBC);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pGameODBC);

	BYTE btKind = 0;
	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read(&btKind);
	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);
	szCharName[MAX_GAMEID_LENGTH] = '\0';

	if(btKind == CLUB_JOIN_REQUEST_ANSWER_KIND_REJECT)
	{
		CPacketComposer	PacketComposer(S2C_CLUB_JOIN_REQUEST_ANSWER_RES);
		BYTE btResult = CLUB_JOIN_RESULT_ERROR;
		if(ODBC_RETURN_SUCCESS == pClubBaseODBC->CLUB_RejectJoinRequest(pUser->GetClubSN(), szCharName))
		{
			int iIndex = 0;
			pGameODBC->CLUB_AddRejectJoinPaper(pUser->GetClubSN(), szCharName, iIndex);
			{
				CScopedRefGameUser pRecvChar(szCharName);
				if(pRecvChar == NULL) 
				{
					CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
					if(pCenter) 
						pCenter->SendUserNotify(szCharName, iIndex, MAIL_NOTE);				
				}
				else
				{
					pRecvChar->RecvPaper(iIndex);
				}		
			}

			btResult = CLUB_JOIN_RESULT_SUCCESS;
			PacketComposer.Add(btResult);
			PacketComposer.Add(btKind);
			PacketComposer.Add((PBYTE)szCharName, MAX_GAMEID_LENGTH+1);
		}
		else
		{
			PacketComposer.Add(btResult);
		}

		pFSClient->Send(&PacketComposer);
		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_JOIN_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.btKind = CLUB_JOIN_KIND_APPLICATION_FORM;
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	

		pClubProxy->SendClubJoinReq(info); 
	}
}

void CFSGameThread::Process_FSClubRemoveMemberReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_REMOVE_MEMBER_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubRemoveMemberReq(info); 
	}
}

void CFSGameThread::Process_FSClubWithDrawReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_WITHDRAW_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubWithDrawReq(info); 
	}
}

void CFSGameThread::Process_FSClubAdminMemberListReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	m_ReceivePacketBuffer.Read(&iClubSN);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubAdminMemberListReq(info); 
	}
}

void CFSGameThread::Process_FSClubAddAdminMemberReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btClubPosition = 0;
	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read(&btClubPosition);
	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_ADD_ADMIN_MEMBER_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.btClubPosition = btClubPosition;
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubAddAdminMemberReq(info); 
	}
}

void CFSGameThread::Process_FSClubRemoveAdminMemberReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_REMOVE_ADMIN_MEMBER_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubRemoveAdminMemberReq(info); 
	}
}

void CFSGameThread::Process_FSClubAddKeyPlayerReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);
	BYTE btUniformNum = 0;

	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);
	m_ReceivePacketBuffer.Read(&btUniformNum);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_ADD_KEY_PLAYER_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	
		info.btUniformNum = btUniformNum;
		pClubProxy->SendClubAddKeyPlayerReq(info); 
	}
}

void CFSGameThread::Process_FSClubRemoveKeyPlayerReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_REMOVE_KEY_PLAYER_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubRemoveKeyPlayerReq(info); 
	}
}

void CFSGameThread::Process_FSClubExtendKeyPlayerPopUpReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_CL_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.btKind = CLUB_CL_KIND_CLUB_EXTEND_KEY_PLAYER;
		pClubProxy->SendClubCLReq(info); 
	}
}

void CFSGameThread::Process_FSClubExtendKeyPlayerReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubExtendKeyPlayerReq(info); 
	}
}

void CFSGameThread::Process_FSClubLineBoardListReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	int iLastUpdatedMsgIndex;
	m_ReceivePacketBuffer.Read(&iClubSN);
	m_ReceivePacketBuffer.Read(&iLastUpdatedMsgIndex);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_BOARD_MSG_REQ info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, MAX_GAMEID_LENGTH+1, pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.iLastUpdatedMsgIndex = iLastUpdatedMsgIndex;
		pClubProxy->SendPacket(G2CL_CLUB_BOARD_MSG_REQ, &info, sizeof(SG2CL_CLUB_BOARD_MSG_REQ)); 
	}
}

void CFSGameThread::Process_FSClubAddLineBoardReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	DECLARE_INIT_TCHAR_ARRAY(szMessage, MAX_CLUB_LINE_BOARD_LENGTH+1);

	m_ReceivePacketBuffer.Read(&iClubSN);
	m_ReceivePacketBuffer.Read((PBYTE)szMessage, MAX_CLUB_LINE_BOARD_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_ADD_BOARD_MSG_REQ info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, MAX_GAMEID_LENGTH+1, pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iGamePosition = pUser->GetCurUsedAvatarPosition();
		strncpy_s(info.szMsg, MAX_CLUB_LINE_BOARD_LENGTH+1, szMessage, MAX_CLUB_LINE_BOARD_LENGTH);
		pClubProxy->SendPacket(G2CL_CLUB_ADD_BOARD_MSG_REQ, &info, sizeof(SG2CL_CLUB_ADD_BOARD_MSG_REQ)); 
	}
}

void CFSGameThread::Process_FSClubMemberPRChangeReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szPR, MAX_CLUB_MEMBER_PR_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szPR, MAX_CLUB_MEMBER_PR_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_MEMBER_PR_CHANGE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szPR, _countof(info.szPR), szPR, MAX_CLUB_MEMBER_PR_LENGTH);	
		pClubProxy->SendClubMemberPRChangeReq(info); 
	}
}

void CFSGameThread::Process_FSClubSetUpPopUpReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CPacketComposer Packet(S2C_CLUB_SETUP_POPUP_RES);
	int iNeedCash = CLUB_CHANGE_NAME_COST;
	Packet.Add(iNeedCash);
	Packet.Add(pUser->GetCoin());
	pUser->Send(&Packet);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//if(pClubProxy != NULL)
	//{ 
	//	SG2CL_CLUB_CL_REQ info;
	//	info.iClubSN = pUser->GetClubSN();
	//	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
	//	info.btKind = CLUB_CL_KIND_CLUB_SETUP;
	//	pClubProxy->SendClubCLReq(info); 
	//}
}

void CFSGameThread::Process_FSClubCheckClubNameReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szClubName, MAX_CLUB_NAME_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szClubName, MAX_CLUB_NAME_LENGTH+1);

	if ( lstrlen(szClubName) < 1 )
	{
		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_CHECK_CLUBNAME_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szClubName, _countof(info.szClubName), szClubName, MAX_CLUB_NAME_LENGTH);	
		info.btType = 0;
		info.iClubAreaIndex = 0;
		pClubProxy->SendClubCheckClubNameReq(info); 
	}
}

void CFSGameThread::Process_FSClubSetUpReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szClubName, MAX_CLUB_NAME_LENGTH+1);
	int iClubAreaIndex = 0;

	m_ReceivePacketBuffer.Read((BYTE*)szClubName, MAX_CLUB_NAME_LENGTH+1);
	m_ReceivePacketBuffer.Read(&iClubAreaIndex);

	CPacketComposer Packet(S2C_CLUB_SETUP_RES);
	BYTE btResult = CLUB_SETUP_RESULT_SUCCESS;

	if(CLUB_POSITION_MASTER != pUser->GetClubPosition())
	{
		btResult = CLUB_SETUP_RESULT_FAIL_NOT_CLUBMASTER;
		Packet.Add(btResult);
		pUser->Send(&Packet);
		return;
	}

	int iCost = CLUB_CHANGE_NAME_COST;
	if(pUser->GetCoin()+pUser->GetEventCoin() < iCost)
	{
		btResult = CLUB_SETUP_RESULT_FAIL_SUPPLY_CASH;
		Packet.Add(btResult);
		pUser->Send(&Packet);
		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_CHECK_CLUBNAME_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		info.btType = 1;
		info.iClubAreaIndex = iClubAreaIndex;
		strncpy_s(info.szClubName, _countof(info.szClubName), szClubName, MAX_CLUB_NAME_LENGTH);	
		pClubProxy->SendClubCheckClubNameReq(info); 
	}
}

void CFSGameThread::Process_FSClubTransferReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	DECLARE_INIT_TCHAR_ARRAY(szCharName, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read((BYTE*)szCharName, MAX_GAMEID_LENGTH+1);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_TRANSFER_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		strncpy_s(info.szCharName, _countof(info.szCharName), szCharName, MAX_GAMEID_LENGTH);	
		pClubProxy->SendClubTransferReq(info); 
	}
}

void CFSGameThread::Process_FSClubDonationPopUpReq(CFSGameClient* pFSClient)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	//CHECK_NULL_POINTER_VOID(pServer);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//if(pClubProxy != NULL)
	//{ 
	//	SG2CL_DEFAULT info;
	//	info.iClubSN = pUser->GetClubSN();
	//	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
	//	pClubProxy->SendClubDonationInfoReq(info); 
	//}
}

void CFSGameThread::Process_FSClubDonationReq(CFSGameClient* pFSClient)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	//CHECK_NULL_POINTER_VOID(pItemShop);

	//int iCL = 0;
	//m_ReceivePacketBuffer.Read(&iCL);

	//CPacketComposer PacketComposer(S2C_CLUB_DONATION_RES);
	//BYTE btResult = CLUB_DONATION_RESULT_SUCCESS;

	//if(pUser->GetClubSN() <= 0)
	//{
	//	btResult = CLUB_DONATION_RESULT_FAIL_NOT_CLUBNMEMBER;
	//	PacketComposer.Add(btResult);
	//	pUser->Send(&PacketComposer);
	//	return;
	//}

	//int iCost = CLUBCONFIGMANAGER.GetCashCostForBuyingClubCash(iCL);
	//if(iCost == -1)
	//{
	//	btResult = CLUB_DONATION_RESULT_FAIL_SUPPLY_CASH;
	//	PacketComposer.Add(btResult);
	//	pUser->Send(&PacketComposer);
	//	return;
	//}

	//if( pUser->GetBonusCoin() > 0 &&
	//	pUser->CheckGMUser() == FALSE &&
	//	pUser->CheckUseableBonusCoinUser() == FALSE)
	//{
	//	btResult = CLUB_DONATION_RESULT_FAIL_SUPPLY_CASH;
	//	PacketComposer.Add(btResult);
	//	pUser->Send(&PacketComposer);
	//	return;
	//}

	//if( pUser->GetCoin() < iCost )
	//{
	//	btResult = CLUB_DONATION_RESULT_FAIL_SUPPLY_CASH;
	//	PacketComposer.Add(btResult);
	//	pUser->Send(&PacketComposer);
	//	return;
	//}

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//if(pClubProxy == NULL)
	//{ 
	//	btResult = CLUB_DONATION_RESULT_FAIL_SUPPLY_CASH;
	//	PacketComposer.Add(btResult);
	//	pUser->Send(&PacketComposer);
	//	return;
	//}

	//SBillingInfo BillingInfo;
	//BillingInfo.iEventCode		= EVENT_DONATION_CLUB;	
	//BillingInfo.pUser			= pUser;
	//memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
	//BillingInfo.iSerialNum		= pUser->GetUserIDIndex();
	//memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH+1);	
	//memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH+1);
	//memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	//BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	//BillingInfo.iSellType		= SELL_TYPE_CASH;
	//BillingInfo.iCurrentCash	= pUser->GetCoin();
	//BillingInfo.iCashChange		= iCost;
	//BillingInfo.iParam0			= pUser->GetGameIDIndex();
	//BillingInfo.iParam1			= iCL;

	//char* szItemName = pItemShop->GetEventCode_Drscription( BillingInfo.iEventCode );
	//if( NULL == szItemName )
	//	printf(	BillingInfo.szItemName, "Donation_Club");
	//else
	//	memcpy( BillingInfo.szItemName, szItemName, MAX_ITEMNAME_LENGTH + 1 );	

	//if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	//{
	//	PacketComposer.Add(btResult);
	//	pUser->Send(&PacketComposer);
	//	return;
	//}
}

void CFSGameThread::Process_FSClubCreatePopUpReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_CLUB_CREATE_POPUP_RES);
	PacketComposer.Add(CLUBCONFIGMANAGER.GetCreatingClubCost());
	PacketComposer.Add(pUser->GetCoin());
	PacketComposer.Add(pUser->GetEventCoin());
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_FSClubCreateReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	DECLARE_INIT_TCHAR_ARRAY(szClubName, MAX_CLUB_NAME_LENGTH+1);
	int iClubAreaIndex = 0;

	CFSClubBaseODBC* pClubBaseODBC = (CFSClubBaseODBC*)ODBCManager.GetODBC( ODBC_CLUB );
	CHECK_NULL_POINTER_VOID(pClubBaseODBC);

	m_ReceivePacketBuffer.Read((BYTE*)szClubName, MAX_CLUB_NAME_LENGTH+1);
	m_ReceivePacketBuffer.Read(&iClubAreaIndex);

	CPacketComposer PacketComposer(S2C_CLUB_CREATE_RES);
	BYTE btResult = CLUB_CREATE_RESULT_SUCCESS;
	if( pUser->GetCoin() + pUser->GetEventCoin() < CLUBCONFIGMANAGER.GetCreatingClubCost() )
	{
		btResult = CLUB_CREATE_RESULT_FAIL_SUPPLY_CASH;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}

	if(FALSE == CLUBCONFIGMANAGER.IsValidClubName(szClubName) ||
		strlen(szClubName) < MIN_CLUBNAME_LENGTH)
	{
		btResult = CLUB_CREATE_RESULT_FAIL_ERROR_CLUBNAME;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}
	if(ODBC_RETURN_SUCCESS != pClubBaseODBC->CLUB_CheckClubName(szClubName))
	{
		btResult = CLUB_CREATE_RESULT_FAIL_EXIST_CLUBNAME;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}
	if(pUser->GetClubSN() > 0)
	{
		btResult = CLUB_CREATE_RESULT_FAIL_ALREADY_CLUBNMEMBER;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy == NULL)
	{ 
		btResult = CLUB_CREATE_RESULT_FAIL_SUPPLY_CASH;
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}

	SBillingInfo BillingInfo;
	BillingInfo.iEventCode		= EVENT_CREATE_CLUB;	
	BillingInfo.pUser			= pUser;
	memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
	BillingInfo.iSerialNum		= pUser->GetUserIDIndex();
	memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH+1);	
	memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH+1);
	memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	memcpy(BillingInfo.szItemName, szClubName, MAX_CLUB_NAME_LENGTH+1);
	BillingInfo.iSellType       = SELL_TYPE_CASH;
	BillingInfo.iCurrentCash	= pUser->GetCoin();
	BillingInfo.iCashChange		= CLUBCONFIGMANAGER.GetCreatingClubCost();
	BillingInfo.iParam0			= pUser->GetGameIDIndex();
	BillingInfo.iParam1			= iClubAreaIndex;

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		/*PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;*/
	}
}

void CFSGameThread::Process_FSClubMemberRecordListReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	m_ReceivePacketBuffer.Read(&iClubSN);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		pClubProxy->SendClubMemberRecordListReq(info); 
	}
}

void CFSGameThread::Process_FSClubRecentMatchRecordListReq(CFSGameClient* pFSClient)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	//CHECK_NULL_POINTER_VOID(pServer);

	//int iClubSN = 0;
	//m_ReceivePacketBuffer.Read(&iClubSN);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//if(pClubProxy != NULL)
	//{ 
	//	SG2CL_DEFAULT info;
	//	info.iClubSN = iClubSN;
	//	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//	pClubProxy->SendClubRecentMatchRecordListReq(info); 
	//}
}

void CFSGameThread::Process_FSClubPRListReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btKind = 0, btPageNum = 0, btGrade = 0, btClubNameLen = 0;
	int iClubHighAreaIndex = -1, iClubAreaIndex = -1;
	DECLARE_INIT_TCHAR_ARRAY(szClubName, MAX_CLUB_NAME_LENGTH+1);

	m_ReceivePacketBuffer.Read(&btPageNum);
	m_ReceivePacketBuffer.Read(&btGrade);
	m_ReceivePacketBuffer.Read(&iClubHighAreaIndex);
	m_ReceivePacketBuffer.Read(&iClubAreaIndex);
	m_ReceivePacketBuffer.Read(&btClubNameLen);

	if(btClubNameLen < 1 || btClubNameLen > MAX_CLUB_NAME_LENGTH+1) 
	{
		btClubNameLen = 0;
	}

	if(btClubNameLen > 0)	
	{
		m_ReceivePacketBuffer.Read((BYTE*)szClubName, btClubNameLen);	
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_PR_LIST_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.btPageNum = btPageNum;
		info.btGrade = btGrade;
		info.iClubHighAreaIndex = iClubHighAreaIndex;
		info.iClubAreaIndex = iClubAreaIndex;
		info.btClubNameLen = btClubNameLen;
		strncpy_s(info.szClubName, _countof(info.szClubName), szClubName, MAX_CLUB_NAME_LENGTH);
		pClubProxy->SendClubPRListReq(info); 
	}
}

void CFSGameThread::Process_FSClubPRChangeReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btKind = 0;
	DECLARE_INIT_TCHAR_ARRAY(szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);

	m_ReceivePacketBuffer.Read(&btKind);

	if(btKind == CLUB_PR_CHANGE_KIND_CHANGE || btKind == CLUB_PR_CHANGE_KIND_PREMIUM_CHANGE)
	{
		m_ReceivePacketBuffer.Read((BYTE*)szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1);	
		m_ReceivePacketBuffer.Read((BYTE*)szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);	
	}
	
	CPacketComposer	Packet(S2C_CLUB_PR_CHANGE_RES);

	BYTE btResult = CLUB_PR_CHANGE_RESULT_SUCCESS;
	if(CLUB_POSITION_SECOND_MASTER > pUser->GetClubPosition())
	{
		btResult = CLUB_SETUP_RESULT_FAIL_NOT_CLUBMASTER;
		Packet.Add(btResult);
		Packet.Add(btKind);
		Packet.Add((PBYTE)&szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1);
		Packet.Add((PBYTE)&szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);
		Packet.Add((int)0);
		pUser->Send(&Packet);
		return;
	}

	if(CLUB_PR_CHANGE_KIND_PREMIUM_CHANGE == btKind)
	{
		int iCost = CLUB_PR_CHANGE_COST;
		if(pUser->GetCoin()+pUser->GetEventCoin() < iCost)
		{
			btResult = CLUB_PR_CHANGE_RESULT_FAIL_SUPPLY_CASH;
			Packet.Add(btResult);
			Packet.Add(btKind);
			Packet.Add((PBYTE)&szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1);
			Packet.Add((PBYTE)&szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);
			Packet.Add((int)0);
			pUser->Send(&Packet);
			return;
		}

		SBillingInfo BillingInfo;
		BillingInfo.iEventCode = EVENT_BUY_CHANGE_CLUB_PR;
		pUser->SetBillingInfo(BillingInfo, SELL_TYPE_CASH, CLUB_PR_CHANGE_COST);
		BillingInfo.iParam0 = btKind;
		strcpy_s(BillingInfo.szItemName, "Change Club PR");
		strncpy_s(BillingInfo.szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1, szPRTitle, MAX_CLUB_PR_TITLE_LENGTH);
		strncpy_s(BillingInfo.szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1, szContents, MAX_CLUB_PR_CONTENTS_LENGTH);
		if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
		{
			CPacketComposer Packet(S2C_CLUB_PR_CHANGE_RES);
			Packet.Add((BYTE)CLUB_PR_CHANGE_RESULT_FAIL_SUPPLY_CASH);
			Packet.Add(btKind);
			Packet.Add((PBYTE)&szPRTitle, MAX_CLUB_PR_TITLE_LENGTH+1);
			Packet.Add((PBYTE)&szContents, MAX_CLUB_PR_CONTENTS_LENGTH+1);
			Packet.Add((int)0);
			pUser->Send(&Packet);
		}

		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_PR_CHANGE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.btKind = btKind;
		strncpy_s(info.szPRTitle, _countof(info.szPRTitle), szPRTitle, MAX_CLUB_PR_TITLE_LENGTH);
		strncpy_s(info.szContents, _countof(info.szContents), szContents, MAX_CLUB_PR_CONTENTS_LENGTH);
		info.iUserIDIndex = pUser->GetUserIDIndex();
		info.iGameIDIndex = pUser->GetGameIDIndex();
		pClubProxy->SendClubPRChangeReq(info); 
	}
}

void CFSGameThread::Process_FSMyClubRankReq(CFSGameClient* pFSClient)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	//CHECK_NULL_POINTER_VOID(pServer);

	//BYTE btKind = 0;
	//m_ReceivePacketBuffer.Read(&btKind);

	//if(pUser->GetClubSN() > 0)
	//{
	//	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//	if(pClubProxy != NULL)
	//	{ 
	//		SG2CL_MY_CLUB_RANK_REQ info;
	//		info.iClubSN = pUser->GetClubSN();
	//		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//		info.btKind = btKind;
	//		pClubProxy->SendMyClubRankReq(info); 
	//	}
	//}
}

void CFSGameThread::Process_FSClubRankListReq(CFSGameClient* pFSClient)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	//CHECK_NULL_POINTER_VOID(pServer);

	//BYTE btKind = 0, btSeasonType = 0, btPageNum = 0;
	//m_ReceivePacketBuffer.Read(&btKind);
	//m_ReceivePacketBuffer.Read(&btSeasonType);
	//m_ReceivePacketBuffer.Read(&btPageNum);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//if(pClubProxy != NULL)
	//{ 
	//	SG2CL_CLUB_RANK_LIST_REQ info;
	//	info.iClubSN = pUser->GetClubSN();
	//	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//	info.btKind = btKind;
	//	info.btSeasonType = btSeasonType;
	//	info.btPageNum = btPageNum;
	//	pClubProxy->SendClubRankListReq(info); 
	//}
}

void CFSGameThread::Process_FSClubCheerLeaderShopListReq(CFSGameClient* pFSClient)
{
	CPacketComposer PacketComposer(S2C_CLUB_CHEERLEADERSHOP_LIST_RES);
	CLUBCONFIGMANAGER.MakeCheerLeaderShopList(PacketComposer);
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSClubCheerLeaderInfoReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iClubSN = 0;
	m_ReceivePacketBuffer.Read(&iClubSN);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		pClubProxy->SendClubCheerLeaderInfoReq(info); 
	}
}

void CFSGameThread::Process_FSClubBuyCheerLeaderPopUpReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iCheerLeaderCode = 0;
	m_ReceivePacketBuffer.Read(&iCheerLeaderCode);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_BUY_CHEERLEADER_INFO_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iCheerLeaderCode = iCheerLeaderCode;
		pClubProxy->SendClubBuyCheerLeaderInfoReq(info); 
	}
}

void CFSGameThread::Process_FSClubBuyCheerLeaderReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iCheerLeaderCode = 0, iPeriod = 0;
	BYTE btTypeNum = 0;
	m_ReceivePacketBuffer.Read(&iCheerLeaderCode);
	m_ReceivePacketBuffer.Read(&iPeriod);
	m_ReceivePacketBuffer.Read(&btTypeNum);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_BUY_CHEERLEADER_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iCheerLeaderCode = iCheerLeaderCode;
		info.iPeriod = iPeriod;
		info.btTypeNum = btTypeNum;
		pClubProxy->SendClubBuyCheerLeaderReq(info); 
	}
}

void CFSGameThread::Process_FSClubCheerLeaderListReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btKind = 0;
	int iClubSN = 0;
	m_ReceivePacketBuffer.Read(&btKind);

	if(btKind == CLUB_CHEERLEADER_LIST_KIND_PUBLIC_CLUB)
		m_ReceivePacketBuffer.Read(&iClubSN);
	else
		iClubSN = pUser->GetClubSN();

	pUser->GetUserCheerLeader()->CheckExpiredCheerLeader();

	if(iClubSN == 0)
	{
		CPacketComposer	Packet(S2C_CLUB_CHEERLEADER_LIST_RES);

		int iCheerLeaderCode = pUser->GetCheerLeaderCode();
		BYTE btTypeNum =pUser->GetCheerLeaderTypeNum();
		Packet.Add(iCheerLeaderCode);
		Packet.Add(btTypeNum);

		int iCheerLeaderTime = pUser->GetUserCheerLeader()->GetRemainTime(iCheerLeaderCode);
		int iRemainEffectCnt = pUser->GetUserCheerLeader()->GetCheerLeaderEffectRemainCnt(iCheerLeaderCode, btTypeNum);

		Packet.Add(iCheerLeaderTime);
		Packet.Add(iRemainEffectCnt);

		BYTE btCnt = 0;
		pUser->GetUserCheerLeader()->MakeCheerLeaderList(Packet, btCnt);
		pUser->Send(&Packet);	

		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		pClubProxy->SendClubCheerLeaderListReq(info); 
	}
}

void CFSGameThread::Process_FSClubCheerLeaderChangeReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iCheerLeaderCode = 0;
	m_ReceivePacketBuffer.Read(&iCheerLeaderCode);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_CHEERLEADER_CHANGE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iCheerLeaderCode = iCheerLeaderCode;
		pClubProxy->SendClubCheerLeaderChangeReq(info); 
	}
}

void CFSGameThread::Process_FSClubMyCheerLeaderChangeReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iCheerLeaderCode = 0;
	BYTE btTypeNum = 0;
	m_ReceivePacketBuffer.Read(&iCheerLeaderCode);

	if(TRUE == pUser->GetUserCheerLeader()->CheckCheerLeader(iCheerLeaderCode, btTypeNum))
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBC);

		if (ODBC_RETURN_SUCCESS != pODBC->AVATAR_ChangeCheerLeader(pUser->GetGameIDIndex(), iCheerLeaderCode))
		{
			WRITE_LOG_NEW(LOG_TYPE_CHEERLEADER, DB_DATA_UPDATE, FAIL, "AVATAR_ChangeCheerLeader - GameIDIndex:11866902", pUser->GetGameIDIndex());	
urn;
		}
		pUser->SetCheerLeaderCode(iCheerLeaderCode);
		pUser->SetCheerLeaderTypeNum(btTypeNum);

		CMatchSvrProxy* pMatchSvrProxy = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		if(pMatchSvrProxy)
		{
			SG2M_CHEERLEADER_CHANGE_REQ info;
			info.iGameIDIndex = pUser->GetGameIDIndex();
			info.iCheerLeaderCode = iCheerLeaderCode;
			info.btTypeNum = btTypeNum;
			pMatchSvrProxy->SendCheerLeaderChangeReq(info);
		}

		CPacketComposer	PacketComposer(S2C_CLUB_MY_CHEERLEADER_CHANGE_RES);
		PacketComposer.Add((BYTE)CLUB_MY_CHEERLEADER_CHANGE_RESULT_SUCCESS);
		PacketComposer.Add(iCheerLeaderCode);
		pUser->Send(&PacketComposer);

		return;
	}
	
	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_MY_CHEERLEADER_CHANGE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iCheerLeaderCode = iCheerLeaderCode;
		pClubProxy->SendClubMyCheerLeaderChangeReq(info); 
	}
}

void CFSGameThread::Process_FSClubAreaListReq(CFSGameClient* pFSClient)
{
	CLUBCONFIGMANAGER.SendClubAreaList(pFSClient);
}

void CFSGameThread::Process_FSClubBuyCLPopupReq(CFSGameClient* pFSClient)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CPacketComposer PacketComposer(S2C_CLUB_BUY_CL_POPUP_RES);
	//PacketComposer.Add(pUser->GetCoin());
	//CLUBCONFIGMANAGER.MakeClubCashShopList(PacketComposer);
	//pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_FSClubStatusChangeReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	BYTE btStatus = 0;
	m_ReceivePacketBuffer.Read(&btStatus);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_CLUB_STATUS_CHANGE_REQ info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.btStatus = btStatus;
		pClubProxy->SendClubStatusChangeReq(info); 
	}
}

void CFSGameThread::Process_FSMyClubPRReq(CFSGameClient* pFSClient)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DEFAULT info;
		info.iClubSN = pUser->GetClubSN();
		strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
		pClubProxy->SendMyClubPRReq(info); 
	}
}

void CFSGameThread::Process_FSMyClubCLReq(CFSGameClient* pFSClient)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	//CHECK_NULL_POINTER_VOID(pServer);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//if(pClubProxy != NULL)
	//{ 
	//	SG2CL_CLUB_CL_REQ info;
	//	info.iClubSN = pUser->GetClubSN();
	//	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
	//	info.btKind = CLUB_CL_KIND_NORMAL;
	//	pClubProxy->SendClubCLReq(info); 
	//}
}

void CFSGameThread::Process_FSDeleteBoardMsgReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iClubSN;
	int iMsgIndex;
	m_ReceivePacketBuffer.Read(&iClubSN);
	m_ReceivePacketBuffer.Read(&iMsgIndex);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	if(pClubProxy != NULL)
	{ 
		SG2CL_DELETE_BOARD_MSG_REQ info;
		info.iClubSN = iClubSN;
		strncpy_s(info.szGameID, MAX_GAMEID_LENGTH+1, pUser->GetGameID(), MAX_GAMEID_LENGTH);
		info.iMsgIndex = iMsgIndex;
		pClubProxy->SendPacket(G2CL_DELETE_BOARD_MSG_REQ, &info, sizeof(SG2CL_DELETE_BOARD_MSG_REQ)); 
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_MISSION_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_MISSION_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iClubSN = pUser->GetClubSN();
	strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	pClubProxy->SendPacket(G2CL_CLUB_MISSION_INFO_REQ, &rq, sizeof(SG2CL_CLUB_MISSION_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_MISSION_GET_BENEFIT_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_MISSION_GET_BENEFIT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	if(pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
	{
		SS2C_CLUB_MISSION_GET_BENEFIT_RES rs;
		rs.btResult = RESULT_CLUB_MISSION_GET_BENEFIT_FAILED;
		pUser->Send(S2C_CLUB_MISSION_GET_BENEFIT_RES, &rs, sizeof(SS2C_CLUB_MISSION_GET_BENEFIT_RES));
		return;
	}

	rq.iClubSN = pUser->GetClubSN();
	strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	pClubProxy->SendPacket(G2CL_CLUB_MISSION_GET_BENEFIT_REQ, &rq, sizeof(SG2CL_CLUB_MISSION_GET_BENEFIT_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_PENNANT_SLOT_INFO_REQ)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//CHECK_NULL_POINTER_VOID(pClubProxy);

	//SG2CL_CLUB_PENNANT_SLOT_INFO_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	//rq.iClubSN = pUser->GetClubSN();
	//strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//pClubProxy->SendPacket(G2CL_CLUB_PENNANT_SLOT_INFO_REQ, &rq, sizeof(SG2CL_CLUB_PENNANT_SLOT_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_PENNANT_INFO_REQ)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//CHECK_NULL_POINTER_VOID(pClubProxy);

	//SG2CL_CLUB_PENNANT_INFO_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	//rq.iClubSN = pUser->GetClubSN();
	//strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//pClubProxy->SendPacket(G2CL_CLUB_PENNANT_INFO_REQ, &rq, sizeof(SG2CL_CLUB_PENNANT_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_USE_PENNANT_REQ)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//CHECK_NULL_POINTER_VOID(pClubProxy);

	//SG2CL_CLUB_USE_PENNANT_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	//rq.iClubSN = pUser->GetClubSN();
	//strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//pClubProxy->SendPacket(G2CL_CLUB_USE_PENNANT_REQ, &rq, sizeof(SG2CL_CLUB_USE_PENNANT_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_PENNANT_SLOT_POPUP_REQ)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//CHECK_NULL_POINTER_VOID(pClubProxy);

	//SC2S_CLUB_BUY_PENNANT_SLOT_POPUP_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(rq));

	//SG2CL_CLUB_CL_REQ info;
	//info.iClubSN = pUser->GetClubSN();
	//strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//info.btKind = CLUB_CL_KIND_CLUB_BUY_PENNANT_SLOT;
	//info.iValue = rq.stSlotIndex;
	//pClubProxy->SendClubCLReq(info); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_PENNANT_SLOT_REQ)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//CHECK_NULL_POINTER_VOID(pClubProxy);

	//SG2CL_CLUB_BUY_PENNANT_SLOT_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	//rq.iClubSN = pUser->GetClubSN();
	//strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//pClubProxy->SendPacket(G2CL_CLUB_BUY_PENNANT_SLOT_REQ, &rq, sizeof(SG2CL_CLUB_BUY_PENNANT_SLOT_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_CLOTHES_SHOP_LIST_REQ)
{
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CLUBCONFIGMANAGER.SendClubClothesShopList(pItemShop, pFSClient);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLOTHES_POPUP_REQ)
{
	//CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	//CHECK_NULL_POINTER_VOID(pItemShop);

	//CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	//CHECK_NULL_POINTER_VOID(pClubProxy);

	//SC2S_CLUB_BUY_CLOTHES_POPUP_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(rq));

	//SG2CL_CLUB_CL_REQ info;
	//info.iClubSN = pUser->GetClubSN();
	//strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	//info.btKind = CLUB_CL_KIND_CLUB_BUY_CLOTHES;
	//info.iValue = rq.iItemCode;
	//pClubProxy->SendClubCLReq(info); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLOTHES_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_BUY_CLOTHES_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iClubSN = pUser->GetClubSN();
	strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	pClubProxy->SendPacket(G2CL_CLUB_BUY_CLOTHES_REQ, &rq, sizeof(SG2CL_CLUB_BUY_CLOTHES_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_CLUBMARK_SHOP_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CPacketComposer Packet(S2C_CLUB_CLUBMARK_SHOP_LIST_RES);
	CLUBCONFIGMANAGER.MakeClubMarkShopList(pAvatar->iClubLeagueSeasonRank, Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLUBMARK_POPUP_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	SC2S_CLUB_BUY_CLUBMARK_POPUP_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(rq));

	vector<SClubMarkPrice> vPrice;
	CLUBCONFIGMANAGER.GetClubMarkPriceList(rq.iClubMarkCode, vPrice);
	CHECK_CONDITION_RETURN_VOID(vPrice.empty());

	SS2C_CLUB_BUY_CLUBMARK_POPUP_RES rs;
	rs.iClubMarkCode = rq.iClubMarkCode;
	rs.btPriceCount = vPrice.size();

	CPacketComposer Packet(S2C_CLUB_BUY_CLUBMARK_POPUP_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CLUB_BUY_CLUBMARK_POPUP_RES));

	for(int i = 0; i < vPrice.size(); ++i)
	{
		SS2C_CLUB_BUY_CLUBMARK_PRICE_INFO price;
		price.iPeriod = vPrice[i].iPeriod;
		price.iCost = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, vPrice[i].iCost);
		Packet.Add((PBYTE)&price, sizeof(SS2C_CLUB_BUY_CLUBMARK_PRICE_INFO));
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLUBMARK_REQ)
{
	SG2CL_CLUB_BUY_CLUBMARK_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_CLUB_BUY_CLUBMARK_RES rs;
	rs.iClubMarkCode = rq.info.iClubMarkCode;
	rs.iPeriod = rq.info.iPeriod;

	if(CLUB_POSITION_MASTER != pUser->GetClubPosition())
	{
		rs.btResult = RESULT_CLUB_BUY_CLUBMARK_CLUBMASTER;
		pUser->Send(S2C_CLUB_BUY_CLUBMARK_RES, &rs, sizeof(SS2C_CLUB_BUY_CLUBMARK_RES));
		return;
	}

	rs.btResult = CLUBCONFIGMANAGER.CanBuyClubMark(rq.info.iClubMarkCode, pUser->GetClubGrade(), pUser->GetClubContributionPoint());
	if(RESULT_CLUB_BUY_CLUBMARK_SUCCESS != rs.btResult)
	{		
		pUser->Send(S2C_CLUB_BUY_CLUBMARK_RES, &rs, sizeof(SS2C_CLUB_BUY_CLUBMARK_RES));
		return;
	}	

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	rq.iClubSN = pUser->GetClubSN();
	strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
	rq.bGive = false;
	pClubProxy->SendPacket(G2CL_CLUB_BUY_CLUBMARK_REQ, &rq, sizeof(SG2CL_CLUB_BUY_CLUBMARK_REQ));	
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_CLUBTOURNAMENT_PAGE_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

	pUser->SetUserSubPageState(USER_SUB_PAGE_STATE_CLUBTOURNAMENT);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CLUBTOURNAMENT_PAGE_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

	pUser->SetUserSubPageState(USER_SUB_PAGE_STATE_NONE);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL rq;
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_INFO_REQ, &rq, sizeof(SG2CL_DEFAULT)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_MATCH_TBALE_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_MATCH_TBALE_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_MATCH_TBALE_INFO_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_MATCH_TBALE_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_KEY_PLAYER_LIST_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_KEY_PLAYER_LIST_REQ rq;
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	rq.iClubIndex = pUser->GetClubSN();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_KEY_PLAYER_LIST_REQ, &rq, sizeof(SG2CL_DEFAULT)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	rq.iClubIndex = pUser->GetClubSN();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_JOIN_PLAYER_REGISTER_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_REQ rq;
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	rq.iClubIndex = pUser->GetClubSN();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_ENTER_GAMEROOM_POPUP_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_ENTER_GAMEROOM_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2CL_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.btRoundGroupIndex, sizeof(rq.btRoundGroupIndex));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	rq.iClubIndex = pUser->GetClubSN();

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_ENTER_GAMEROOM_CHECK_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_LIST_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_JOIN_PLAYER_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_JOIN_PLAYER_LIST_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_JOIN_PLAYER_LIST_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_JOIN_PLAYER_FEATURE_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_VOTE_ROOM_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_VOTE_ROOM_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_VOTE_ROOM_INFO_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_VOTE_ROOM_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_CLUBTOURNAMENT_VOTE_PAGE_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

	SC2S_ENTER_CLUBTOURNAMENT_VOTE_PAGE_NOT info;
	m_ReceivePacketBuffer.Read((PBYTE)&info.btRoundGroupIndex, sizeof(info.btRoundGroupIndex));

	pUser->SetUserSubPageState(USER_SUB_PAGE_STATE_CLUBTOURNAMENT_VOTE_ROOM);
	pUser->SetRoundGroupIndex(info.btRoundGroupIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CLUBTOURNAMENT_VOTE_PAGE_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

	pUser->SetUserSubPageState(USER_SUB_PAGE_STATE_CLUBTOURNAMENT);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_VOTE_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_VOTE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	if(pUser->GetCurUsedAvtarLv() < GAME_LVUP_BOUND2)
	{
		SS2C_CLUBTOURNAMENT_VOTE_RES rs;
		rs.btResult = RESULT_CLUBTOURNAMENT_VOTE_ENOUGH_LV;
		rs.btRoundGroupIndex = rq.info.btRoundGroupIndex;
		pUser->Send(S2C_CLUBTOURNAMENT_VOTE_RES, &rs, sizeof(SS2C_CLUBTOURNAMENT_VOTE_RES));
		return;
	}

	rq.iUserIDIndex = pUser->GetUserIDIndex();
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_VOTE_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_VOTE_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_MATCH_RESULT_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_MATCH_RESULT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_MATCH_RESULT_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_MATCH_RESULT_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_BENEFIT_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(CLUBTOURNAMENTAGENCY.GetCurrentStatus() == CLUBTOURNAMENT_STATUS_NONE);

	CLUBCONFIGMANAGER.SendClubTournamentBenefitList(pFSClient, CLUBTOURNAMENTAGENCY.GetCurrentMonth());
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_GET_BENEFIT_ITEM_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_GET_BENEFIT_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_GET_BENEFIT_ITEM_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_GET_BENEFIT_ITEM_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBTOURNAMENT_GAME_START_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUBTOURNAMENT_GAME_START_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUBTOURNAMENT_GAME_START_INFO_REQ, &rq, sizeof(SG2CL_CLUBTOURNAMENT_GAME_START_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_TEST_CLUBTOURNAMENT_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_TEST_CLUBTOURNAMENT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	pClubProxy->SendPacket(G2CL_TEST_CLUBTOURNAMENT_REQ, &rq, sizeof(SG2CL_TEST_CLUBTOURNAMENT_REQ)); 
}


DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_LEAGUE_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if(0 >= pUser->GetClubSN())
	{
		SS2C_CLUB_LEAGUE_INFO_RES rs;
		ZeroMemory(&rs, sizeof(SS2C_CLUB_LEAGUE_INFO_RES));
		pUser->Send(S2C_CLUB_LEAGUE_INFO_RES, &rs, sizeof(SS2C_CLUB_LEAGUE_INFO_RES));
		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_LEAGUE_INFO_REQ rq;
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	rq.iClubIndex = pUser->GetClubSN();
	pClubProxy->SendPacket(G2CL_CLUB_LEAGUE_INFO_REQ, &rq, sizeof(SG2CL_CLUB_LEAGUE_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_RANKING_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2CL_CLUB_RANKING_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	if(0 >= pUser->GetClubSN())
	{
		SS2C_CLUB_RANKING_INFO_RES rs;
		ZeroMemory(&rs, sizeof(SS2C_CLUB_RANKING_INFO_RES));
		rs.btType = rq.info.btType;
		pUser->Send(S2C_CLUB_RANKING_INFO_RES, &rs, sizeof(SS2C_CLUB_RANKING_INFO_RES));
		return;
	}

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	rq.iClubIndex = pUser->GetClubSN();
	pClubProxy->SendPacket(G2CL_CLUB_RANKING_INFO_REQ, &rq, sizeof(SG2CL_CLUB_RANKING_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_RANKING_LIST_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_RANKING_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq.info, sizeof(rq.info));

	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUB_RANKING_LIST_REQ, &rq, sizeof(SG2CL_CLUB_RANKING_LIST_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_RANKING_REWARD_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	CPacketComposer Packet(S2C_CLUB_RANKING_REWARD_RES);
	CLUBCONFIGMANAGER.MakeRewardListForSeasonRank(Packet);

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_TOURNAMENT_RANKING_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_TOURNAMENT_RANKING_INFO_REQ rq;
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	rq.iClubIndex = pUser->GetClubSN();
	pClubProxy->SendPacket(G2CL_CLUB_TOURNAMENT_RANKING_INFO_REQ, &rq, sizeof(SG2CL_CLUB_TOURNAMENT_RANKING_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_TOURNAMENT_RANKING_LIST_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL rq;
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUB_TOURNAMENT_RANKING_LIST_REQ, &rq, sizeof(SG2CL)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL rq;
	rq.iGameIDIndex = pUser->GetGameIDIndex();
	pClubProxy->SendPacket(G2CL_CLUB_TOURNAMENT_CLUBCUP_RANKING_LIST_REQ, &rq, sizeof(SG2CL)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_SHOP_BUY_CONDITION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_CLUB_SHOP_BUY_CONDITION_INFO_RES);
	CLUBCONFIGMANAGER.MakeClubShopBuyConditionList(Packet);

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_SHOP_USER_CASH_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendClubShopUserCashInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_SHOP_BUY_ITEM_POPUP_REQ)
{
	//CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	//CHECK_NULL_POINTER_VOID(pAvatar);

	//CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	//CHECK_NULL_POINTER_VOID(pItemShop);

	//SC2S_CLUB_SHOP_BUY_ITEM_POPUP_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_CLUB_SHOP_BUY_ITEM_POPUP_REQ));

	//SS2C_CLUB_SHOP_BUY_ITEM_POPUP_RES rs;
	//rs.btPopupType = rq.btPopupType;
	//rs.iItemCode = rq.iItemCode;
	//rs.iUserCash = pUser->GetUserCoin() + pUser->GetEventCoin();
	//rs.iUserPoint = pUser->GetSkillPoint();
	//rs.iUserClubCoin = pUser->GetUserClubCoin();

	//CPacketComposer Packet(S2C_CLUB_SHOP_BUY_ITEM_POPUP_RES);

	//if(CLUB_SHOP_BUY_ITEM_POPUP_TYPE_CHEERLEADER == rq.btPopupType ||
	//	CLUB_SHOP_BUY_ITEM_POPUP_TYPE_EMBLEM == rq.btPopupType ||
	//	CLUB_SHOP_BUY_ITEM_POPUP_TYPE_PRIMIUM_ETC == rq.btPopupType ||
	//	CLUB_SHOP_BUY_ITEM_POPUP_TYPE_CLOTHES == rq.btPopupType ||
	//	CLUB_SHOP_BUY_ITEM_POPUP_TYPE_ETC == rq.btPopupType)
	//{
	//	SShopItemInfo ShopItemInfo;
	//	CHECK_CONDITION_RETURN_VOID(FALSE == pItemShop->GetItem(rq.iItemCode, ShopItemInfo));
	//	CHECK_CONDITION_RETURN_VOID(ITEM_CATEGORY_CLUB != ShopItemInfo.iCategory);

	//	if(CLUB_SHOP_BUY_ITEM_POPUP_TYPE_CHEERLEADER == rq.btPopupType)
	//	{
	//		SCheerLeaderConfig* pConfig = CLUBCONFIGMANAGER.GetCheerLeaderConfig(ShopItemInfo.iCheerLeaderIndex);
	//		CHECK_NULL_POINTER_VOID(pConfig);
	//	}

	//	CItemPriceList* pItemPriceList = pItemShop->GetItemPriceList(ShopItemInfo.iItemCode0);
	//	CHECK_NULL_POINTER_VOID(pItemPriceList);

	//	vector< SItemPrice > vItemPrice;
	//	rs.btPriceCount = pItemPriceList->GetItemPriceVector(ShopItemInfo.iSellType, vItemPrice); 
	//	CHECK_CONDITION_RETURN_VOID(0 >= rs.btPriceCount);

	//	rs.btSellType = ShopItemInfo.iSellType-1;
	//	rs.iPropertyType = ShopItemInfo.iPropertyType;
	//	Packet.Add((PBYTE)&rs, sizeof(SS2C_CLUB_SHOP_BUY_ITEM_POPUP_RES));

	//	for(int i = 0; i < rs.btPriceCount; ++i)
	//	{
	//		SCLUB_SHOP_BUY_ITEM_PRICE price;
	//		price.iPropertyValue = vItemPrice[i].iPropertyValue;
	//		price.iPrice = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, vItemPrice[i].GetOriginalPrice());
	//		Packet.Add((PBYTE)&price, sizeof(SCLUB_SHOP_BUY_ITEM_PRICE));
	//	}
	//}
	//else if(CLUB_SHOP_BUY_ITEM_POPUP_TYPE_MOTION == rq.btPopupType)
	//{
	//	vector<SClubMotionPrice> vPrice;
	//	CLUBCONFIGMANAGER.GetClubMotionPriceList(rq.iItemCode, vPrice);
	//	CHECK_CONDITION_RETURN_VOID(vPrice.empty());

	//	rs.btPriceCount = vPrice.size();
	//	rs.btSellType = CLUBCONFIGMANAGER.GetClubMotionSellType(rq.iItemCode)-1;
	//	rs.iPropertyType = ITEM_PROPERTY_TIME;
	//	Packet.Add((PBYTE)&rs, sizeof(SS2C_CLUB_SHOP_BUY_ITEM_POPUP_RES));

	//	for(int i = 0; i < vPrice.size(); ++i)
	//	{
	//		SCLUB_SHOP_BUY_ITEM_PRICE price;
	//		price.iPropertyValue = vPrice[i].iPeriod;
	//		price.iPrice = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, vPrice[i].iCost);
	//		Packet.Add((PBYTE)&price, sizeof(SCLUB_SHOP_BUY_ITEM_PRICE));
	//	}
	//}
	//else if(CLUB_SHOP_BUY_ITEM_POPUP_TYPE_BASKETBALL == rq.btPopupType)
	//{
	//	CFSGameServer* pServer = CFSGameServer::GetInstance();
	//	CHECK_NULL_POINTER_VOID(pServer);

	//	CBasketBallManager* pBasketBallManager = pServer->GetBasketBallManager();
	//	CHECK_NULL_POINTER_VOID(pBasketBallManager);

	//	vector<SBasketBallShopPriceInfo> vPrice;
	//	CHECK_CONDITION_RETURN_VOID(FALSE == pBasketBallManager->GetBasketBallSkinPriceInfo(BASKETBALL_BALLPART_PATTERN, rq.iItemCode, vPrice));
	//	CHECK_CONDITION_RETURN_VOID(vPrice.empty());

	//	rs.btPriceCount = vPrice.size();
	//	rs.btSellType = vPrice[0].iSellType-1;
	//	rs.iPropertyType = ITEM_PROPERTY_NORMAL;
	//	Packet.Add((PBYTE)&rs, sizeof(SS2C_CLUB_SHOP_BUY_ITEM_POPUP_RES));

	//	for(int i = 0; i < vPrice.size(); ++i)
	//	{
	//		SCLUB_SHOP_BUY_ITEM_PRICE price;
	//		price.iPropertyValue = vPrice[i].iPropertyValue;
	//		price.iPrice = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, vPrice[i].iPrice);
	//		Packet.Add((PBYTE)&price, sizeof(SCLUB_SHOP_BUY_ITEM_PRICE));
	//	}
	//}
	//else if(CLUB_SHOP_BUY_ITEM_POPUP_TYPE_CLUBMARK == rq.btPopupType)
	//{
	//	vector<SClubMarkPrice> vPrice;
	//	CLUBCONFIGMANAGER.GetClubMarkPriceList(rq.iItemCode, vPrice);
	//	CHECK_CONDITION_RETURN_VOID(vPrice.empty());

	//	rs.btPriceCount = vPrice.size();
	//	rs.btSellType = CLUBCONFIGMANAGER.GetClubMarkSellType(rq.iItemCode)-1;
	//	rs.iPropertyType = ITEM_PROPERTY_NORMAL;
	//	Packet.Add((PBYTE)&rs, sizeof(SS2C_CLUB_SHOP_BUY_ITEM_POPUP_RES));

	//	for(int i = 0; i < vPrice.size(); ++i)
	//	{
	//		SCLUB_SHOP_BUY_ITEM_PRICE price;
	//		price.iPropertyValue = vPrice[i].iPeriod;
	//		price.iPrice = CLUBCONFIGMANAGER.GetShopDiscountPrice(pAvatar->iClubLeagueSeasonRank, vPrice[i].iCost);
	//		Packet.Add((PBYTE)&price, sizeof(SCLUB_SHOP_BUY_ITEM_PRICE));
	//	}
	//}

	//pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUBMOTION_SHOP_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CPacketComposer Packet(S2C_CLUBMOTION_SHOP_LIST_RES);
	CLUBCONFIGMANAGER.MakeClubMotionShopList(pAvatar->iClubLeagueSeasonRank, Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_BUY_CLUBMOTION_REQ)
{
//	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
//	CHECK_NULL_POINTER_VOID(pUser);
//
//	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
//	CHECK_NULL_POINTER_VOID(pServer);
//
//	CActionShop* pActionShop = pServer->GetActionShop();
//	CHECK_NULL_POINTER_VOID(pActionShop);
//
//	CUserAction* pUserAction = pUser->GetUserAction();
//	CHECK_NULL_POINTER_VOID(pUserAction);
//
//	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
//	CHECK_NULL_POINTER_VOID(pGameODBC);
//
//	SC2S_CLUB_BUY_CLUBMOTION_REQ rq;
//	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_CLUB_BUY_CLUBMOTION_REQ));
//
//	BYTE btSellType = CLUBCONFIGMANAGER.GetClubMotionSellType(rq.iMotionIndex);
//	CHECK_CONDITION_RETURN_VOID(SELL_TYPE_CASH != btSellType);
//
//	SS2C_CLUB_BUY_CLUBMOTION_RES rs;
//	rs.iMotionIndex = rq.iMotionIndex;
//	rs.iPeriod = rq.iPeriod;
//
//	rs.btResult = CLUBCONFIGMANAGER.CanBuyClubMotion(rq.iMotionIndex, pUser->GetClubGrade(), pUser->GetClubContributionPoint());
//	if(RESULT_CLUB_BUY_CLUBMOTION_SUCCESS != rs.btResult)
//	{		
//		pUser->Send(S2C_CLUB_BUY_CLUBMOTION_RES, &rs, sizeof(SS2C_CLUB_BUY_CLUBMOTION_RES));
//		return;
//	}
//
//	int iCost = CLUBCONFIGMANAGER.GetClubMotionPrice(rq.iMotionIndex, rq.iPeriod);
//	if(0 == iCost ||
//		pUser->GetCoin()+pUser->GetEventCoin() < iCost)
//	{
//		rs.btResult = RESULT_CLUB_BUY_CLUBMOTION_ENOUGH_CASH;
//		pUser->Send(S2C_CLUB_BUY_CLUBMOTION_RES, &rs, sizeof(SS2C_CLUB_BUY_CLUBMOTION_RES));
//		return;
//	}
//
//	if( FALSE == pUserAction->CheckOwnUnLimitAction(rq.iMotionIndex, rq.iPeriod) )
//	{
//		rs.btResult = RESULT_CLUB_BUY_CLUBMOTION_FAILED;
//		pUser->Send(S2C_CLUB_BUY_CLUBMOTION_RES, &rs, sizeof(SS2C_CLUB_BUY_CLUBMOTION_RES));
//		return;
//	}
//
//	SBillingInfo BillingInfo;
//	BillingInfo.iEventCode = EVENT_BUY_ACTION;	
//	pUser->SetBillingInfo(BillingInfo, btSellType, iCost);
//	BillingInfo.iItemCode = rq.iMotionIndex;
//
//	char* szActionName = pActionShop->GetActionName(rq.iMotionIndex);
//	memcpy(BillingInfo.szItemName, szActionName, MAX_ACTION_NAME+1);
//
//	BillingInfo.iParam0 = pUser->GetGameIDIndex();
//	BillingInfo.iParam1 = pActionShop->GetActionCategory(rq.iMotionIndex);
//	BillingInfo.iParam2 = pActionShop->GetActionKind(rq.iMotionIndex);
//	BillingInfo.iParam3 = rq.iPeriod;
////	BillingInfo.pGameODBC = pGameODBC;
//	BillingInfo.iCashChange = iCost;
//
//	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
//	{
//		rs.btResult = RESULT_CLUB_BUY_CLUBMOTION_FAILED;
//		pUser->Send(S2C_CLUB_BUY_CLUBMOTION_RES, &rs, sizeof(SS2C_CLUB_BUY_CLUBMOTION_RES));
//		return;
//	}
//
//	pUser->SendClubShopUserCashInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_COLLECT_LETTER_EVENT_INFO_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_COLLECT_LETTER_EVENT_INFO_REQ ss;
	ZeroMemory(&ss, sizeof(SG2CL_CLUB_COLLECT_LETTER_EVENT_INFO_REQ));

	ss.iClubSN = pUser->GetClubSN();
	strncpy_s(ss.szGameID, _countof(ss.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	

	pClubProxy->SendPacket(G2CL_CLUB_COLLECT_LETTER_EVENT_INFO_REQ, &ss, sizeof(SG2CL_CLUB_COLLECT_LETTER_EVENT_INFO_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SG2CL_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_REQ ss;
	ZeroMemory(&ss, sizeof(SG2CL_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_REQ));

	ss.iClubSN = pUser->GetClubSN();
	strncpy_s(ss.szGameID, _countof(ss.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	

	pClubProxy->SendPacket(G2CL_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_REQ, &ss, sizeof(SG2CL_CLUB_COLLECT_LETTER_EVENT_GET_REWARD_REQ)); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_NAME_REQ)
{
	CFSGameUser *pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_CLUB_NAME_RES rs;
	strcpy_s(rs.szClubName, sizeof(rs.szClubName), "");

	if(0 < pUser->GetClubSN())
		strncpy_s(rs.szClubName, _countof(rs.szClubName), pUser->GetClubName(), MAX_CLUB_NAME_LENGTH+1);	
	else
		strncpy_s(rs.szClubName, _countof(rs.szClubName), pUser->GetGameID(), MAX_GAMEID_LENGTH+1);	

	pUser->Send(S2C_CLUB_NAME_RES, &rs, sizeof(SS2C_CLUB_NAME_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLUB_NOTICE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser *)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CClubSvrProxy* pClubProxy = (CClubSvrProxy*) GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pClubProxy);

	SC2S_CLUB_NOTICE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_CLUB_NOTICE_REQ));

	SS2C_CLUB_NOTICE_RES rs;
	if(0 >= pUser->GetClubSN())
	{
		rs.btResult = CLUB_NOTICE_RESULT_FAIL_NOT_ADMINMEMBER;
		pUser->Send(S2C_CLUB_NOTICE_RES, &rs, sizeof(SS2C_CLUB_NOTICE_RES));
		return;
	}

	if(CLUB_POSITION_MANAGER > pUser->GetClubPosition())
	{
		rs.btResult = CLUB_NOTICE_RESULT_FAIL_NOT_ADMINMEMBER;
		pUser->Send(S2C_CLUB_NOTICE_RES, &rs, sizeof(SS2C_CLUB_NOTICE_RES));
		return;
	}

	if(MAX_SCLUB_NOTICE_OPTION_COUNT <= rq.btOption)
	{
		rs.btResult = CLUB_NOTICE_RESULT_FAIL;
		pUser->Send(S2C_CLUB_NOTICE_RES, &rs, sizeof(SS2C_CLUB_NOTICE_RES));
		return;
	}

	SG2CL_CLUB_NOTICE_REQ info;
	info.iClubIndex = pUser->GetClubSN();
	info.iGameIDIndex = pUser->GetGameIDIndex();
	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH+1);	
	info.info.btOption = rq.btOption;
	strncpy_s(info.info.szNotice, _countof(info.info.szNotice), rq.szNotice, MAX_CLUB_NOTICE_CHAT_LENGTH+1);	

	pClubProxy->SendPacket(G2CL_CLUB_NOTICE_REQ, &info, sizeof(SG2CL_CLUB_NOTICE_REQ)); 
}
