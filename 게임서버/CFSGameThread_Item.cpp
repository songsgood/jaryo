qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUserItem.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "FSGameCommon.h"
qinclude "CFSGameClient.h"
qinclude "HackingManager.h"
qinclude "CContentsManager.h"
qinclude "CChannelBuffManager.h"
qinclude "CUserAction.h"
qinclude "CFSGameDBCfg.h"
qinclude "ChatSvrProxy.h"
qinclude "RandomCardShopManager.h"
qinclude "ClubConfigManager.h"
qinclude "ClubTournamentAgency.h"
qinclude "UserHighFrequencyItem.h"
qinclude "SecretStoreManager.h"
qinclude "RewardManager.h"
qinclude "SpecialSkinManager.h"
qinclude "CustomizeManager.h"
qinclude "AttendanceItemShop.h"
qinclude "TodayHotDeal.h"
qinclude "CAvatarCreateManager.h"
qinclude "EventDateManager.h"
qinclude "SaleRandomItemEventManager.h"
qinclude "CFSRankManager.h"
qinclude "CoachCardShop.h"
qinclude "ODBCSvrProxy.h"
qinclude "MiniGameZoneEventManager.h"
qinclude "HippocampusEventManager.h"
qinclude "GameOfDiceEventManager.h"
qinclude "TransferJoycity100DreamEventManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CFSGameDBCfg*	g_pFSDBCfg;

void CFSGameThread::Process_FSIEnterItemPage(CFSGameClient* pFSClient)
{
	CFSGameUser  * pUser = (CFSGameUser *) pFSClient->GetUser();	
	if( pUser == NULL ) return;

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	pUser->CheckExpireItem();

	enum PAGE_CODE {PAGE_CODE_SHOP, PAGE_CODE_MYITEM};
	int iSuccess = 0;
	int iCode = 0; //PAGE_CODE
	m_ReceivePacketBuffer.Read(&iCode);
	
	pUser->SendUserStat();

	const NEXUS_CLIENT_STATE eOldState = pFSClient->GetState();
	if (FALSE == pFSClient->SetState(FS_ITEM))
	{
		iSuccess = -1;
		CPacketComposer PacketComposer(S2C_ENTER_ITEMPAGE_RES);
		PacketComposer.Add(iSuccess);
		pFSClient->Send(&PacketComposer);
		return;
	}

	if( eOldState == FS_ITEM)
	{
		if( pUser->GetUserItemList()->GetMode() == ITEM_PAGE_MODE_MYITEM )  // ???�벤 ?�면 
		{
			pUser->GetUserItemList()->SetMode(ITEM_PAGE_MODE_SHOP); //shop
		}
		else	// ???�면 
		{
			pUser->GetUserItemList()->SetMode(ITEM_PAGE_MODE_MYITEM); // ???�벤 
		}
	}
	else if( eOldState == NEXUS_LOBBY ||
		eOldState == FS_SKILL || 
		eOldState == FS_RANKING || 
		eOldState == FS_CLUB ||
		eOldState == FS_COACH_CARD ||
		eOldState == FS_CHARACTER_COLLECTION ||
		eOldState == FS_BASKETBALL ||
		eOldState == FS_PVE_RANK ||
		eOldState == FS_SPECIAL_PIECE )
	{
		if( iCode == PAGE_CODE_SHOP ) 
			pUser->GetUserItemList()->SetMode(ITEM_PAGE_MODE_SHOP); //shop
		else	
			pUser->GetUserItemList()->SetMode(ITEM_PAGE_MODE_MYITEM); 
	}

	CPacketComposer PacketComposer(S2C_ENTER_ITEMPAGE_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);
			
	//pUser->GetUserItemList()->BackUpUseFeatureInfo();
	pUser->GetUserItemList()->BackUpUseTryFeatureInfo();
			
	pUser->SendUserGold();
	pUser->SendAvatarInfo();
	pUser->SendChangeFaceNotice();

	if(iCode == PAGE_CODE_SHOP)
	{
		pUser->SendFirstCashBackEventStatus();
		pUser->SendRandomCardEventStatus();
		pUser->GetUserPurchaseGrade()->SendCurrentGradeButton();
		pUser->GetUserCashItemRewardEvent()->SendEventStatus();
		pUser->SendSecretStoreOpen();
	}
}

void CFSGameThread::Process_FSIExitItemPage(CFSGameClient* pFSClient)
{
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iSuccess = -1;
	
	if( TRUE == pFSClient->SetState(NEXUS_LOBBY) )
	{
		iSuccess = 0;
	}
	
	CPacketComposer PacketComposer(S2C_EXIT_ITEMPAGE_RES);
	PacketComposer.Add(iSuccess);
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSIUserItemList(CFSGameClient* pFSClient)
{
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	int iBigKind = 0, iSmallKind = 0, iPage = 0, iOp = 0;
	int iInventorytKind = INVENTORY_KIND_NORMAL;

	m_ReceivePacketBuffer.Read(&iInventorytKind);
	m_ReceivePacketBuffer.Read(&iBigKind);
	m_ReceivePacketBuffer.Read(&iSmallKind);
	m_ReceivePacketBuffer.Read(&iPage);
	
	if( iPage < 0 ) return;
	
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	pUser->CheckExpireItem();

	pUser->SendUserItemList(iInventorytKind, iBigKind, iSmallKind, iPage);
	
	pUser->SendUserGold();
}

void CFSGameThread::Process_FSIShopItemList(CFSGameClient* pFSClient)
{
	CFSGameServer *pServer = (CFSGameServer *) pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSGameUser * pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSItemShop *pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	int iSex = 0;	
	m_ReceivePacketBuffer.Read(&iSex);
	
	if( ITEM_PAGE_MODE_MYITEM == pUser->GetUserItemList()->GetMode() )
	{
		pUser->GetUserItemList()->SetMode(ITEM_PAGE_MODE_SHOP);
		pUser->GetUserItemList()->BackUpUseTryFeatureInfo();
	}

	pUser->SendAllItemInfo( iSex , pUser->GetCloneCharacter());

	pUser->CheckFirstTimeTutorial();
}

// Modify for Match Server
void CFSGameThread::Process_FSIItemSelect(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
		
	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
		
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
		
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );
		
	int iOp, iItemIndex;
	int iFlagFeature = -1, iFlagPoint = -1;
	int iSuccess = -1;
	
	char 	recvID[MAX_GAMEID_LENGTH+1];	
	char 	title[MAX_PRESENT_TITLE_LENGTH+1];
	short 	textLen;
	char 	text[MAX_PRESENT_TEXT_LENGTH+1];
	int		res = 0;
	int		iBuyCount = 0;
	int		iTotalCash = 0;
	int		iTotalPoint = 0;
	int		iTotalTrophy = 0;
	int		iEventPoint = 0;
	int		iEventCash = 0;
	
	int iPropertyValue	= -1;

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	m_ReceivePacketBuffer.Read(&iOp);
	m_ReceivePacketBuffer.Read(&iItemIndex);
		
	TRACE( " Choose Item Num  ------->  11866902\n", iItemIndex );
itch(iOp)
	{
	case FS_ITEM_OP_USE: 
		{
			// 20090326 ?�리미엄 ?�시�??�이??추�?
			int iOptionPropertyIndex1;
			int iOptionPropertyIndex2;
			m_ReceivePacketBuffer.Read(&iOptionPropertyIndex1);
			m_ReceivePacketBuffer.Read(&iOptionPropertyIndex2);
			// End
			int iaRemoveItem[MAX_ITEMCHANNEL_NUM];
			memset( iaRemoveItem , -1 , sizeof(int) * MAX_ITEMCHANNEL_NUM);
			if(	pUser->GetUserItemList()->UseItem( pItemShop , iItemIndex , iaRemoveItem, iSuccess, iOptionPropertyIndex1, iOptionPropertyIndex2 )  )
			{
				iSuccess = 0;
				for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
				{
					int iOp2 = FS_ITEM_OP_REMOVE;
					if( -1 != iaRemoveItem[i] )
					{
						PacketComposer.Add(iOp2);
						PacketComposer.Add(iSuccess);
						PacketComposer.Add(iaRemoveItem[i]);

						CAvatarItemList* pAvatarItemList = pUser->GetUserItemList()->GetCurAvatarItemList();
						if(pAvatarItemList != NULL)
						{
							SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iaRemoveItem[i]);
							if(pItem != NULL)
							{
								PacketComposer.Add(pUser->GetUserItemList()->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
								PacketComposer.Add(pUser->GetUserItemList()->GetDifferentUserPropertyAccItemStatus());
								if( pItem->iPropertyKind >= MIN_BUFF_ITEM_KIND_NUM && pItem->iPropertyKind <= MAX_BUFF_ITEM_KIND_NUM )
								{
									pAvatarItemList->RemoveItem(iaRemoveItem[i]);
									pAvatarItemList->RemoveItemProperty(iaRemoveItem[i]);	
								}
							}
							else
							{
								PacketComposer.Add((BYTE)ACCITEM_STATUS_NOT_CHECKBOX);
								PacketComposer.Add((BYTE)ACCITEM_STATUS_DISABLE_CHECKBOX);
							}
						}
					}
				}
			}
			
			PacketComposer.Add(iOp);
			PacketComposer.Add(iSuccess);
			PacketComposer.Add(iItemIndex);

			CAvatarItemList* pAvatarItemList = pUser->GetUserItemList()->GetCurAvatarItemList();
			if( iSuccess == 0 && pAvatarItemList != NULL )
			{
				SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIndex);
				if(pItem != NULL && ( pItem->iPropertyKind >= MIN_BUFF_ITEM_KIND_NUM && pItem->iPropertyKind <= MAX_BUFF_ITEM_KIND_NUM ))
				{
					Process_FSExposeUserItem(pFSClient);
				}

				pUser->CheckSpecialPartsUserProperty();
			}

			if(pAvatarItemList != NULL)
			{
				SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIndex);
				if(pItem != NULL)
				{
					PacketComposer.Add(pUser->GetUserItemList()->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
					PacketComposer.Add(pUser->GetUserItemList()->GetDifferentUserPropertyAccItemStatus());
				}
				else
				{
					PacketComposer.Add((BYTE)ACCITEM_STATUS_NOT_CHECKBOX);
					PacketComposer.Add((BYTE)ACCITEM_STATUS_DISABLE_CHECKBOX);
				}
			}
			
			iFlagFeature = iOp;
			
			break;
		}
	case FS_ITEM_OP_REMOVE:
		{
			if(	pUser->GetUserItemList()->RemoveItemWithItemIdx( iItemIndex ))
			{
				iSuccess = 0;
			}
			else iSuccess = -1;
			
			PacketComposer.Add(iOp);
			PacketComposer.Add(iSuccess);
			PacketComposer.Add(iItemIndex);

			CAvatarItemList* pAvatarItemList = pUser->GetUserItemList()->GetCurAvatarItemList();
			if(pAvatarItemList != NULL)
			{
				SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIndex);
				if(pItem != NULL)
				{
					PacketComposer.Add(pUser->GetUserItemList()->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
					PacketComposer.Add(pUser->GetUserItemList()->GetDifferentUserPropertyAccItemStatus());
				}
				else
				{
					PacketComposer.Add((BYTE)ACCITEM_STATUS_NOT_CHECKBOX);
					PacketComposer.Add((BYTE)ACCITEM_STATUS_DISABLE_CHECKBOX);
				}
			}
			
			iFlagFeature = iOp;
			
			break;
		}
	case FS_ITEM_OP_USE_TRY:
		{
			if(	pUser->GetUserItemList()->UseTryItem(  iItemIndex , pItemShop, iSuccess))
			{
				iSuccess = 0;
			}
			
			PacketComposer.Add(iOp);
			PacketComposer.Add(iSuccess);
			PacketComposer.Add(iItemIndex);
			
			iFlagFeature = iOp;
			break;
		}
		
	case FS_ITEM_OP_UNDRESS:
		{
			int iChannel = -1;
			m_ReceivePacketBuffer.Read(&iChannel);

			if(pUser->GetUserItemList()->UndressItem(iItemIndex, iChannel, pItemShop, iSuccess))
			{
				iSuccess = 0;
			}

			PacketComposer.Add(iOp);
			PacketComposer.Add(iSuccess);
			PacketComposer.Add(iItemIndex);

			iFlagFeature = iOp;
			break;
		}
	case FS_ITEM_OP_UNIFORM_USE_TRY:
		{
			if(	pUser->GetUserItemList()->UseTryUniform(  iItemIndex , pItemShop, iSuccess))
			{
				iSuccess = 0;
				
			}
			
			PacketComposer.Add(iOp);
			PacketComposer.Add(iSuccess);
			PacketComposer.Add(iItemIndex);
			
			iFlagFeature = iOp;
			break;
		}
		
	case FS_ITEM_OP_BUY:
	case FS_ITEM_OP_BUY_SECRETSTOREITEM:
	case FS_ITEM_OP_BUY_TODAYHOTDEAL:
	case FS_ITEM_OP_BUY_SALE_RANDOMITEM:
	case FS_ITEM_OP_BUY_EVENT_SALEPLUS:
	case FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE:
		{
			BYTE btUseDressItemToken;
			int iPropertyValue	= -1;
			BYTE btPropertyAssignType;
			int iPropertyIndexCount;
			int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
			int iUseCouponPropertyKind = 0;

			m_ReceivePacketBuffer.Read(&btUseDressItemToken);
			m_ReceivePacketBuffer.Read(&iPropertyValue);
			m_ReceivePacketBuffer.Read(&btPropertyAssignType);
			m_ReceivePacketBuffer.Read(&iPropertyIndexCount);

			if (0 > iPropertyIndexCount || iPropertyIndexCount > MAX_ITEM_PROPERTYINDEX_COUNT)
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyItem - PropertyIndexCount:11866902, UserIndex:0", iPropertyIndexCount, pUser->GetUserIDIndex());
eturn;
			}

			for (int j = 0 ; j < iPropertyIndexCount ; j++)
			{
				m_ReceivePacketBuffer.Read(&aiPropertyIndex[j]);
			}
			pUser->SetUseCashStatus(TRUE);

			if ( FS_ITEM_OP_BUY == iOp )
			{
				m_ReceivePacketBuffer.Read(&iUseCouponPropertyKind);
			}

			if( OPEN == ATTENDANCEITEMSHOP.IsOpen() &&
				TRUE == ATTENDANCEITEMSHOP.CheckAttendanceEventItemCode(iItemIndex) )
			{
				int iErrorCode = 0;
				if( FALSE == pUser->BuyAttendanceItemShop(iItemIndex, iErrorCode, iOp) )
				{
					PacketComposer.Add((int)iOp);
					PacketComposer.Add(iErrorCode);				
					PacketComposer.Add(iItemIndex);							
					PacketComposer.Add((int)BUY_ITEM_END_OPERATION);								
					pFSClient->Send(&PacketComposer);	
				}
				return;
			}
			
			if(pUser->GetUserItemList()->BuyItem(pUser->GetIsLocalPubliser(), pItemShop, iItemIndex, 0, iPropertyValue, (int)btPropertyAssignType, aiPropertyIndex, iUseCouponPropertyKind, iTotalCash, iTotalPoint, iTotalTrophy, iSuccess, iOp, btUseDressItemToken) )
			{		
				pUser->SendUserGold();
				TRACE( " ERROR Return  ---------------> 11866902\n",iSuccess );
			else if (BUY_ITEM_ERROR_DO_NOT_RESPONSE != iSuccess)
			{
				// 모든 ?�라가 모두 구입 기능???�용?��? ?�기?�문??그냥 return
				PacketComposer.Add((int)iOp);
				PacketComposer.Add(iSuccess);	
				PacketComposer.Add(iItemIndex);							
				PacketComposer.Add((int)BUY_ITEM_END_OPERATION);		

				if(BUY_ITEM_ERROR_MISMATCH_LEVEL == iSuccess)
				{
					if(pItemShop)
					{
						SShopItemInfo ShopItemInfo;
						pItemShop->GetItem( iItemIndex, ShopItemInfo );

						PacketComposer.Add(ShopItemInfo.iLvCondition);
						PacketComposer.Add(ShopItemInfo.iMaxLvCondition);
					}
					else
					{
						PacketComposer.Add((int)0);
						PacketComposer.Add((int)0);
					}		
				}							
				pFSClient->Send(&PacketComposer);	
			}	
			pUser->SetUseCashStatus(FALSE);
			return;
		}
		break;
	case FS_ITEM_OP_SEND:
		{
			int iPropertyValue	= -1;
			BYTE btPropertyAssignType;
			int iPropertyIndexCount;
			int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
			
			m_ReceivePacketBuffer.Read(&iPropertyValue);
			m_ReceivePacketBuffer.Read(&btPropertyAssignType);
			m_ReceivePacketBuffer.Read(&iPropertyIndexCount);

			if (0 > iPropertyIndexCount || iPropertyIndexCount > MAX_ITEM_PROPERTYINDEX_COUNT)
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "SendItem - PropertyIndexCount:11866902, UserIndex:0", iPropertyIndexCount, pUser->GetUserIDIndex());
eturn;
			}

			for (int j = 0 ; j < iPropertyIndexCount ; j++)
			{
				m_ReceivePacketBuffer.Read(&aiPropertyIndex[j]);
			}

			m_ReceivePacketBuffer.Read((BYTE*)recvID, MAX_GAMEID_LENGTH+1);	
			recvID[MAX_GAMEID_LENGTH] = '\0';	FSStrTrim(recvID);
			
			m_ReceivePacketBuffer.Read((BYTE*)title, MAX_PRESENT_TITLE_LENGTH+1);	
			
			title[MAX_PRESENT_TITLE_LENGTH] = '\0';
			
			m_ReceivePacketBuffer.Read(&textLen);	
			if(textLen <= 0 || textLen >= MAX_PRESENT_TEXT_LENGTH)
			{
				textLen = 0;
				text[textLen] = '\0';
			}
			else
			{
				m_ReceivePacketBuffer.Read((BYTE*)text, textLen);		
				text[textLen] = '\0';
			}
							
			int index = -1;
			res = 0;
			
			DECLARE_INIT_TCHAR_ARRAY(szSSN2, MAX_SSN2_LENGTH+1);	
			DECLARE_INIT_TCHAR_ARRAY(szCertification, MAX_CERTIFICATION_LENGTH+1);	

			if( pUser->GetUserType() LIENT_PLUS_TYPE_NEW != CLIENT_ACCOUNT_TYPE_NORMAL )
)
			{
				res = 19;
			}
			
			if(strlen(recvID) < 1)
			{
				res = 1;
			}
			
			if(res == 0)
			{
				// recvID check
				pUser->SetUseCashStatus(TRUE);
				if(pUser->GetUserItemList()->BuyItem( pUser->GetIsLocalPubliser(), pItemShop , iItemIndex, 0, iPropertyValue , (int)btPropertyAssignType, aiPropertyIndex, 0, iTotalCash, iTotalPoint, iTotalTrophy, iSuccess, FS_ITEM_OP_SEND, FALSE/*UseDressItemToken*/, 0, recvID, title, text, &res, &index))
				{	// res 0: ?�공, 1: ?�는 ?��?, 2: ?�물??꽉참, 3: ?�인?��?�? 4:캐쉬부�? 5:?�착못함, 6:�?밖에 ?�러, 7: 주�??�록 번호 ?�류
					pUser->SendUserGold();
					pUser->SetUseCashStatus(FALSE);
					return;
				}
				else if (BUY_ITEM_ERROR_DO_NOT_RESPONSE != iSuccess)
				{	
					pUser->SetUseCashStatus(FALSE);

					if( iSuccess == BUY_ITEM_ERROR_SPECIALCHARACTER_IMPOSSIBLE )
					{
						int iResult = -2;
						CPacketComposer PacketError(S2C_SEND_GIFT_GAME_ID_RES);
						PacketError.Add(&iResult);
						PacketError.Add((PBYTE)recvID, MAX_GAMEID_LENGTH + 1);						
						pFSClient->Send(&PacketError);
						return;
					}
					else if( iSuccess == BUY_ITEM_ERROR_REFER_SEND_ERROR || BUY_ITEM_ERROR_GSGIRL_ITEM == iSuccess ||
						BUY_ITEM_ERROR_TODAYHOTDEAL_TIMEOVER == iSuccess || BUY_ITEM_ERROR_TODAYHOTDEAL_LIMITOVER == iSuccess|| 
						BUY_ITEM_ERROR_TODAYHOTDEAL_SALEITEM_COMINGSOON == iSuccess ||
						BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR == iSuccess)
					{
						PacketComposer.Add((int)FS_ITEM_OP_SEND);
						PacketComposer.Add(iSuccess);
						PacketComposer.Add(iItemIndex);							
						PacketComposer.Add((int)BUY_ITEM_END_OPERATION);
						PacketComposer.Add((BYTE)res);
						PacketComposer.Add((PBYTE)recvID, MAX_GAMEID_LENGTH + 1);								
						pFSClient->Send(&PacketComposer);
						return;
					}
					else if (iSuccess == SEND_ITEM_ERROR_MISMATCH_LEVEL)
					{
						PacketComposer.Add((int)FS_ITEM_OP_SEND);
						PacketComposer.Add(iSuccess);
						PacketComposer.Add(iItemIndex);							
						PacketComposer.Add((int)BUY_ITEM_END_OPERATION);
						PacketComposer.Add((BYTE)res);
						PacketComposer.Add((PBYTE)recvID, MAX_GAMEID_LENGTH + 1);
						pFSClient->Send(&PacketComposer);
						return;
					}
					else if(iSuccess == BUY_ITEM_ERROR_NOT_SELL) // fot event
					{
						PacketComposer.Add((int)FS_ITEM_OP_BUY);
						PacketComposer.Add(iSuccess);				
						PacketComposer.Add(iItemIndex);							
						PacketComposer.Add((int)BUY_ITEM_END_OPERATION);								
						pFSClient->Send(&PacketComposer);	
						return;
					}
				}					
			}
			else
			{
				pUser->SetUseCashStatus(FALSE);

				PacketComposer.Add((int)FS_ITEM_OP_SEND);
				PacketComposer.Add(iSuccess);
				PacketComposer.Add(iItemIndex);							
				PacketComposer.Add((int)BUY_ITEM_END_OPERATION);
				PacketComposer.Add((BYTE)res);
				PacketComposer.Add((PBYTE)recvID, MAX_GAMEID_LENGTH + 1);								
				pFSClient->Send(&PacketComposer);
			}
			
			iFlagPoint = iOp;
		}
		break;
		
	case FS_ITEM_OP_SELL: 
		{
			int iRemove = 0; //?�고?�던것에???�매가 ?�루?�졌?��?
			if(pUser->GetUserItemList()->SellItem(pItemShop , iItemIndex ,iRemove, iSuccess))
			{
				iSuccess =0;
			}
			else iSuccess = -1;
			
			PacketComposer.Add(iOp);
			PacketComposer.Add(iSuccess);
			PacketComposer.Add(iItemIndex);
			
			iFlagPoint = iOp;
			
			if(iRemove)	iFlagFeature = iOp;
			
			break;
		}	
	case FS_ITEM_OP_RENEW:
		{
			int iItemNum;
			int iPropertyValue	= -1;
			BYTE btPropertyAssignType;
			int iPropertyIndexCount;
			int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
			m_ReceivePacketBuffer.Read(&iItemNum);
			m_ReceivePacketBuffer.Read(&iPropertyValue);
			m_ReceivePacketBuffer.Read(&btPropertyAssignType);
			m_ReceivePacketBuffer.Read(&iPropertyIndexCount);

			if (0 > iPropertyIndexCount || iPropertyIndexCount > MAX_ITEM_PROPERTYINDEX_COUNT)
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "Renew - PropertyIndexCount:11866902, UserIndex:0", iPropertyIndexCount, pUser->GetUserIDIndex());
eturn;
			}

			for (int j = 0 ; j < iPropertyIndexCount ; j++)
			{
				m_ReceivePacketBuffer.Read(&aiPropertyIndex[j]);
			}

			if(pUser->GetUserItemList()->BuyItem( pUser->GetIsLocalPubliser(), pItemShop, iItemIndex, 0, iPropertyValue, (int)btPropertyAssignType, aiPropertyIndex, 0, iTotalCash, iTotalPoint, iTotalTrophy, iSuccess, FS_ITEM_OP_RENEW, FALSE/*UseDressItemToken*/, iItemNum ) )
			{		
				pUser->SendUserGold();
				TRACE( " ERROR Return  ---------------> 11866902\n",iSuccess );
			else if (BUY_ITEM_ERROR_DO_NOT_RESPONSE != iSuccess)
			{
				// 모든 ?�라가 모두 구입 기능???�용?��? ?�기?�문??그냥 return
				PacketComposer.Add((int)FS_ITEM_OP_RENEW);
				PacketComposer.Add(iSuccess);				
				PacketComposer.Add(iItemIndex);							
				PacketComposer.Add((int)BUY_ITEM_END_OPERATION);								
				pFSClient->Send(&PacketComposer);	
			}	
			return;
		}
	case FS_ITEM_OP_BUY_FACTIONSHOP:
		{
			iSuccess = pUser->GetUserItemList()->BuyFactionShopItem(iItemIndex);
			if (BUY_ITEM_ERROR_DO_NOT_RESPONSE != iSuccess)
			{		
				PacketComposer.Add(iOp);
				PacketComposer.Add(iSuccess);				
				PacketComposer.Add(iItemIndex);							
				PacketComposer.Add((int)BUY_ITEM_END_OPERATION);								
				pFSClient->Send(&PacketComposer);	
			}
			return;
		} break;
	}
	
	if( iOp != FS_ITEM_OP_BUY )
	{
		int iEndOp = -1;
		PacketComposer.Add(iEndOp);
	}
	
	int iTotalResult = -1;
	
	pFSClient->Send(&PacketComposer);
	
	if( -1 != iFlagFeature ) //?�거??벗을?�만
	{
		pUser->SendAvatarInfo();
	}
	
	if( -1 == iFlagFeature && iItemIndex > BASE_FACE_OFF_NUM )
	{
		pUser->SendAvatarInfo();
	}
	
	if( -1 != iFlagPoint)
	{
		int iSkillPoint = 0;
		if( ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->spUserGetUserPointInfo( pUser->GetUserIDIndex(), iSkillPoint ))
		{	
			pUser->SetSkillPoint( iSkillPoint );
			pUser->AddPointAchievements(); 
		}
		
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();
	}
	
	if( iOp == FS_ITEM_OP_USE || iOp == FS_ITEM_OP_REMOVE || iOp == FS_ITEM_OP_BUY )
	{
		pUser->SendUserStat();		
	}

	if( iOp == FS_ITEM_OP_USE )
	{
		map<int, BYTE> mapEquipCount;
		map<int, vector<int>> mapProperty;
		pUser->SendSpecialPartsInfo(mapEquipCount, mapProperty);
	}
	
	pUser->SendAvatarInfoUpdateToMatch();
}

void CFSGameThread::Process_FSIShopItemSelect(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iOp;
	m_ReceivePacketBuffer.Read(&iOp);
	
	CPacketComposer PacketComposer(S2C_SHOPITEM_SELECT_RES);

	if( FS_ITEM_OP_BUY == iOp || 
		FS_ITEM_OP_SEND == iOp || 
		FS_ITEM_OP_UNIFORM_BUY == iOp || 
		FS_ITEM_OP_RENEW == iOp || 
		FS_ITEM_OP_BUY_SECRETSTOREITEM == iOp ||
		FS_ITEM_OP_BUY_TODAYHOTDEAL == iOp || 
		FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp ||
		FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOp || 
		FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE == iOp)
	{
		int iItemCode;
		m_ReceivePacketBuffer.Read(&iItemCode);
		
		int iRenewItemIdx = 0;
		if ( FS_ITEM_OP_RENEW == iOp )
			m_ReceivePacketBuffer.Read(&iRenewItemIdx);

		int iRecvUserLv = 0;
		if( FS_ITEM_OP_SEND == iOp )
			m_ReceivePacketBuffer.Read(&iRecvUserLv);

		int iSecretItemStoreIndex = 0;
		if( FS_ITEM_OP_BUY_SECRETSTOREITEM == iOp ||
			FS_ITEM_OP_BUY_TODAYHOTDEAL == iOp ||
			FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOp || 
			FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOp ||
			FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE == iOp) 
			m_ReceivePacketBuffer.Read(&iSecretItemStoreIndex);

		int iUserLv = -1;
		if (TRUE == pUser->GetShopItemSelectInfo(PacketComposer, iOp, iItemCode, iRenewItemIdx, iRecvUserLv, iSecretItemStoreIndex))
		{
			pFSClient->Send(&PacketComposer);
		}
	}
}

void CFSGameThread::Process_FSIResetFeature(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	if( NULL == pUser ) return;

	if( ITEM_PAGE_MODE_MYITEM == pUser->GetUserItemList()->GetMode() ) return;
	
	pUser->GetUserItemList()->RestoreFeatureInfo();
	
	pUser->SendAvatarInfo();
	pUser->SendUserStat();
}

void CFSGameThread::Process_FSIAvatarSelectInItem(CFSGameClient* pFSClient)
{
	//TODO - Delete Process_FSIAvatarSelectInItem
}

void CFSGameThread::Process_FSIChangeHeight(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser )	return;
	
	SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo )	return;
	
	int iOutMinHeight = 0, iOutMaxHeight = 0;
	int iHeight = 0, iWeight = 0;

	if (pAvatarInfo->iEnableChangeHeight == 0) // SPECIAL_AvatarConfig - IsSelectHeight 
	{
		return;
	}
	
	m_ReceivePacketBuffer.Read(&iHeight);
	
	int iaChangeStat[MAX_STAT_NUM];
	memset( iaChangeStat, 0, sizeof(int)*MAX_STAT_NUM);
	
	if( FALSE == pUser->GetAndSetHeightStat( iHeight, iWeight, iOutMinHeight, iOutMaxHeight, iaChangeStat ) )	return;
	
	int iaStat[MAX_STAT_NUM];
	memset( iaStat, 0, sizeof(int)*MAX_STAT_NUM);
	
	iaStat[0] = pAvatarInfo->Status.iStatRun		+ iaChangeStat[0];
	iaStat[1] = pAvatarInfo->Status.iStatJump		+ iaChangeStat[1];
	iaStat[2] = pAvatarInfo->Status.iStatStr		+ iaChangeStat[2];
	iaStat[3] = pAvatarInfo->Status.iStatPass		+ iaChangeStat[3];
	iaStat[4] = pAvatarInfo->Status.iStatDribble	+ iaChangeStat[4];
	iaStat[5] = pAvatarInfo->Status.iStatRebound	+ iaChangeStat[5];
	iaStat[6] = pAvatarInfo->Status.iStatBlock		+ iaChangeStat[6];
	iaStat[7] = pAvatarInfo->Status.iStatSteal		+ iaChangeStat[7];
	iaStat[8] = pAvatarInfo->Status.iStat2Point		+ iaChangeStat[8];
	iaStat[9] = pAvatarInfo->Status.iStat3Point		+ iaChangeStat[9];
	iaStat[10] = pAvatarInfo->Status.iStatDrive		+ iaChangeStat[10];
	iaStat[11] = pAvatarInfo->Status.iStatClose		+ iaChangeStat[11];
	iaStat[12] = pAvatarInfo->Status.sStatStam		+ iaChangeStat[12];
	iaStat[13] = pAvatarInfo->Status.sStatRec		+ iaChangeStat[13];
	
	int iaStatAdd[MAX_STAT_NUM];
	memset( iaStatAdd, 0, sizeof(int)*MAX_STAT_NUM);
	
	pUser->GetAvatarItemStatus( iaStatAdd );
	pUser->GetUserCharacterCollection()->GetSentenceStatus(iaStatAdd);
	pUser->GetUserPowerupCapsule()->GetPowerupCapsuleStatus(iaStatAdd);
	pUser->GetCheerLeaderStatus(iaStatAdd);
	pUser->GetLvIntervalBuffItemStatus(iaStatAdd);
	pUser->GetAvatarSpecialPieceProperty(iaStatAdd);

	int iaLinkStatAdd[MAX_STAT_NUM];
	memset( iaLinkStatAdd, 0, sizeof(int)*MAX_STAT_NUM );
	//��ȡħ�ֽǺͿ���ͷ������ֵ
	pUser->GetAvatarLinkItemStatus( iaLinkStatAdd );

	CPacketComposer PacketComposer(S2C_CHANGE_HEIGHT_RES);
	PacketComposer.Add(iHeight);
	PacketComposer.Add(iOutMinHeight);
	PacketComposer.Add(iOutMaxHeight);
	
	int i;
	for( i=0; i<MAX_STAT_NUM; ++i )
	{
		PacketComposer.Add((short)iaStat[i]);
	}
	for( i=0; i<MAX_STAT_NUM; ++i )
	{
		PacketComposer.Add((short)( iaStatAdd[i] + iaLinkStatAdd[i] ) );
	}
	
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSISetupHeight(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );

	if (pAvatarInfo->iEnableChangeHeight == 0) // 기본 캐릭?��? ?�니�?
	{
		return;
	}
	
	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	if( NULL == pUserItemList ) return;
	
	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;
	
	int iItemIdx = -1;
	int iOutMinHeight = 0, iOutMaxHeight = 0;
	int iHeight = 0, iWeight = 0;
	
	m_ReceivePacketBuffer.Read(&iItemIdx);
	m_ReceivePacketBuffer.Read(&iHeight);
	
	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if( NULL == pItem ) return;
	if( pItem->iPropertyKind != CHANGE_HEIGHT_KIND_NUM ) return;
	
	int iaChangeStat[MAX_STAT_NUM];
	memset( iaChangeStat, 0, sizeof(int)*MAX_STAT_NUM );
	
	if( FALSE == pUser->GetAndSetHeightStat( iHeight, iWeight, iOutMinHeight, iOutMaxHeight, iaChangeStat ) )	return;

	BOOL bIsClone = FALSE;
	
	if( TRUE == pUser->GetCloneCharacter())
	{
		int iUseStyle = pUser->GetUseStyleIndex();
		Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > iUseStyle ? Clone_Index_Mine : Clone_Index_Clone;

		if(eIndex == Clone_Index_Mine)
			bIsClone = FALSE;
		else 
			bIsClone = TRUE;
	}

	int iaBaseStat[MAX_STAT_NUM];
	memset( iaBaseStat, 0, sizeof(int)*MAX_STAT_NUM );

	pUser->SetCloneStatToArray(iaBaseStat , bIsClone);
		
	if( ODBC_RETURN_SUCCESS == ((CFSGameODBC *)pNexusODBC)->spChangeHeight( pAvatarInfo, iHeight, iWeight, iaChangeStat, iItemIdx, iaBaseStat ) )
	{
		if( FALSE == pAvatarItemList->RemoveItem(iItemIdx) )
		{
			WRITE_LOG_NEW(LOG_TYPE_AVATAR, EXEC_FUNCTION, FAIL, "Process_FSISetupHeight::RemoveItem - GameID:�4", pAvatarInfo->szGameID);
	}
		
		pAvatarInfo->Status.iHeight = iHeight;
		pAvatarInfo->Status.iWeight = iWeight;
		
		pAvatarInfo->Status.iStatRun		+= iaChangeStat[0];
		pAvatarInfo->Status.iStatJump		+= iaChangeStat[1];
		pAvatarInfo->Status.iStatStr		+= iaChangeStat[2];
		pAvatarInfo->Status.iStatPass		+= iaChangeStat[3];
		pAvatarInfo->Status.iStatDribble	+= iaChangeStat[4];
		pAvatarInfo->Status.iStatRebound	+= iaChangeStat[5];
		pAvatarInfo->Status.iStatBlock		+= iaChangeStat[6];
		pAvatarInfo->Status.iStatSteal		+= iaChangeStat[7];
		pAvatarInfo->Status.iStat2Point		+= iaChangeStat[8];
		pAvatarInfo->Status.iStat3Point		+= iaChangeStat[9];
		pAvatarInfo->Status.iStatDrive		+= iaChangeStat[10];
		pAvatarInfo->Status.iStatClose		+= iaChangeStat[11];
		pAvatarInfo->Status.sStatStam		+= iaChangeStat[12];
		pAvatarInfo->Status.sStatRec		+= iaChangeStat[13];
		
		if( TRUE == pUser->GetCloneCharacter() )
		{
			int iUseStyle = pUser->GetUseStyleIndex();
			Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > iUseStyle ? Clone_Index_Mine : Clone_Index_Clone;

			pUser->UpdateHeightAndStatus(eIndex);
		}

		CPacketComposer PacketComposer(S2C_HEIGHT_SETUP_RES);
		PacketComposer.Add(iHeight);
		pFSClient->Send(&PacketComposer);
	}
	
	pUser->SendUserStat();
}

void CFSGameThread::Process_FSIChangeName(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser )	return;
	
	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	if( NULL == pUserItemList ) return;
	
	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID( pGameODBC );
	
	DECLARE_INIT_TCHAR_ARRAY(szNewGameID, MAX_GAMEID_LENGTH+1);	
	int iItemIdx = -1;
	int iResult = 0;
	
	m_ReceivePacketBuffer.Read(&iItemIdx);
	m_ReceivePacketBuffer.Read((PBYTE)szNewGameID, MAX_GAMEID_LENGTH+1);	
	szNewGameID[MAX_GAMEID_LENGTH] = 0;
	
	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if( NULL == pItem ) return;

	int iPropertyKind = pItem->iPropertyKind ;
	if( iPropertyKind != CHANGE_NAME_KIND_NUM && 
		iPropertyKind != CHANGE_NAME_SPECIAL_KIND_NUM ) 
		return;

	DECLARE_INIT_TCHAR_ARRAY(szOldGameID, MAX_GAMEID_LENGTH+1);	
	
	char* pGameID = pUser->GetGameID();
	if( NULL == pGameID ) return;
	
	strncpy_s(szOldGameID, _countof(szOldGameID), pGameID, MAX_GAMEID_LENGTH);	
	szOldGameID[MAX_GAMEID_LENGTH] = 0;
	
	if( TRUE == pUser->SetNewCharacterName( szNewGameID, iItemIdx, iPropertyKind) )
	{
		if( FALSE == pAvatarItemList->RemoveItem(iItemIdx) )
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "Process_FSIChangeName::RemoveItem - GameIDIndex:11866902", pUser->GetGameIDIndex());

	iResult = 1;
	}
	
	CPacketComposer PacketComposer(S2C_CHANGE_NAME_RES);
	PacketComposer.Add(iResult);
	pFSClient->Send(&PacketComposer);
	
	
	if( iResult == 1 )
	{
		CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
		if( NULL == pServer )	return;
		
		pServer->LockUserManager();
		pServer->RemoveUser(pUser, FALSE);

		pUser->SetGameID(szNewGameID);
		if (!pServer->AddUser((CUser *)pUser)) 
		{
			WRITE_LOG_NEW(LOG_TYPE_AVATAR, EXEC_FUNCTION, FAIL, "Process_FSIChangeName::AddUser - NewGameID:�4", szNewGameID);
	}
		pServer->UnlockUserManager();
		
		pUser->UpdateExchangeNameItem(szNewGameID);
		pUser->UpdateEventRankMyAvatarName(szNewGameID);
		pUser->GetUserPVEMission()->UpdateRankGameID(pUser->GetCurUsedAvatarGameIDIndex(), szNewGameID);

		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
		
		if(pClubProxy)
		{ 
			SG2CL_CHANGE_MEMBERID_REQ info;
			info.iClubSN = pUser->GetClubSN();
			strncpy_s(info.szGameID, _countof(info.szGameID), szOldGameID, MAX_GAMEID_LENGTH);
			info.iGameIDIndex = pUser->GetGameIDIndex();
			strncpy_s(info.szGameIDNew, _countof(info.szGameIDNew), szNewGameID, MAX_GAMEID_LENGTH);
			info.btStatus = CLUB_MEMBER_LOCATION_DIFF;
			info.bExchangeGameID = FALSE;
			pClubProxy->SendGameIDChangeReq(info); 
		}
		
		if(pCenter)
		{
			pCenter->SendRequestGameIDChange(pUser->GetUserIDIndex(),pUser->GetGameIDIndex(),(LPSTR)szOldGameID,(LPSTR)szNewGameID,pUser->GetFactionIndex());
		}

		CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		if(pMatch) 
		{
			SG2M_CHANGE_GAMEID_REQ Info;
			Info.iGameIDIndex = pUser->GetGameIDIndex();
			Info.btServerIndex = _GetServerIndex;
			strncpy_s(Info.szGameIDNew, _countof(Info.szGameIDNew), pUser->GetGameID(), _countof(Info.szGameIDNew)-1);

			pMatch->CALL_MATCHBASEPROXY_PACKET_SEND_FUNC(G2M_CHANGE_GAMEID_REQ, &Info, sizeof(SG2M_CHANGE_GAMEID_REQ));			

		}

		CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
		if( pChat )
		{
			SS2T_CHANGENAME info;
			info.iGameIDIndex = pUser->GetGameIDIndex();
			strcpy_s(info.szGameID, pUser->GetGameID());
			CPacketComposer Packet(S2T_CHANGENAME); 
			Packet.Add((PBYTE)&info, sizeof(SS2T_CHANGENAME));
			pChat->Send(&Packet);
		}

		if( TRUE == pUser->GetUserRankMatch()->CheckUpdateGameIDInRank())
		{
			pUser->GetUserRankMatch()->SendUpdateGameIDInRankPage(pUser->GetGameID());
		}

		CODBCSvrProxy* pODBCSvr = dynamic_cast<CODBCSvrProxy*>(GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY));
		if( NULL != pODBCSvr &&
			OPEN == MINIGAMEZONE.IsOpen() && 
			pUser->GetGameIDIndex() == pUser->GetUserMiniGameZoneEvent()->GetMainGameIDIndex())
		{
			CPacketComposer Packet(S2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT);

			SS2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT not;
			not.iUserIDIndex = pUser->GetUserIDIndex();
			not.btServerIndex = _GetServerIndex;
			not.iSeason = MINIGAMEZONE.GetCurrentSeason();
			strncpy_s(not.szGameID, _countof(not.szGameID), szNewGameID, MAX_GAMEID_LENGTH);	
			Packet.Add((BYTE*)&not, sizeof(SS2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT));

			pODBCSvr->Send(&Packet);
		}

		if(OPEN == HIPPOCAMPUS.IsOpen())
		{
			CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
			if(NULL != pODBCSvr)
			{
				pUser->LoadRepresentInfo(TRUE);

				SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT not;
				not.btServerIndex = _GetServerIndex;
				pUser->GetUserHippocampusEvent()->MakeIntegrateDB_UpdateUserLog(not);

				CPacketComposer	Packet(S2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT);
				Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT));

				pODBCSvr->Send(&Packet);
			}		
		}
		
		if(OPEN == GAMEOFDICEEVENT.IsOpen())
		{
			pUser->LoadRepresentInfo(TRUE);
			pUser->GetUserGameOfDiceEvent()->CheckAndUpdateRankGameID();
		}

		if(OPEN == TRANSFERJOYCITY100DREAM.IsOpen())
		{
			pUser->LoadRepresentInfo(TRUE);
			pUser->GetUserTransferJoycity100DreamEvent()->CheckAndUpdateRankGameID();
		}
	}
}

void CFSGameThread::Process_FSICheckName(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser )	return;
	
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );
	
	CAvatarItemList* pAvatarItemList = pUser->GetUserItemList()->GetCurAvatarItemList();	
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	int iResult = 0;

	int iItemIdx = -1;
	m_ReceivePacketBuffer.Read(&iItemIdx);

	DECLARE_INIT_TCHAR_ARRAY(szGameID_After, MAX_GAMEID_LENGTH+1);	
	m_ReceivePacketBuffer.Read((PBYTE)szGameID_After, MAX_GAMEID_LENGTH+1);		
	
	BOOL bIsValidGameID = FALSE;
	CFSGameServer* pServer = (CFSGameServer*) pFSClient->GetServer();
	if(pServer == NULL)
		return;

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if( NULL == pItem ) return;

	if( pItem->iPropertyKind != CHANGE_NAME_KIND_NUM && 
		pItem->iPropertyKind != CHANGE_NAME_SPECIAL_KIND_NUM ) 
		return;
	
	bIsValidGameID = pServer->GetGameIDStringChecker()->IsValidGameIDCharacter(szGameID_After, pItem->iPropertyKind);
	
	if(bIsValidGameID == TRUE)
	{
		if( ((CFSGameODBC *)pNexusODBC)->spCheckCharacterName(szGameID_After,iResult) != ODBC_RETURN_SUCCESS )
			iResult = -1;
		else
		{
			if( iResult == 0 )
				iResult = 1;
			else if( iResult == -2 )
				iResult = -1;
			else
				iResult = 0;
		}
	}
	else
	{
		iResult = -1;
	}
	
	CPacketComposer PacketComposer(S2C_CHECK_NAME_RES);
	PacketComposer.Add(iResult);
	PacketComposer.Add(pItem->iPropertyKind);
	
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSIOpenTreasureBox(CFSGameClient *pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSGameUserItem* pUserItem = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItem);
	
	CAvatarItemList *pAvatarItemList = pUser->GetCurAvatarItemListWithGameID(pUser->GetGameID());
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	
	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );

	int iItemIndex = -1;
	int iItemNum = -1;
	int iResult = -1;
	
	int iBigKind = 0, iSmallKind = 0, iPage = 0;
	
	int iPropertyKind = -1;
	int iPoint = 0;
	int iTrophy = 0;
	int iCash = 0;
	int iNewItemCode = -1;
	int iMailType = MAIL_PRESENT_ON_AVATAR;
	int iIndex = -1;
	int iItemType = 0;
	int iPropertyType = 0;
	int iMyPropertyKind = -1;
	int iPropertyTypeValue = -100;
	time_t ExpireTime = -1;
	int iSellPrice = 0;
	
	m_ReceivePacketBuffer.Read(&iBigKind);
	m_ReceivePacketBuffer.Read(&iSmallKind);
	m_ReceivePacketBuffer.Read(&iPage);
	m_ReceivePacketBuffer.Read(&iItemIndex);
	m_ReceivePacketBuffer.Read(&iItemNum);
	
	SUserItemInfo aItemList[MAX_USERITEMLIST_PER_PAGE];
	int iItemNumCount = 0;
	pUser->GetUserItemList()->GetItemListPage(INVENTORY_KIND_NORMAL, iPage, iBigKind, iSmallKind, aItemList, iItemNumCount);
	
	for(int i=0 ; i<iItemNumCount ;i++)
	{
		if( aItemList[i].iItemCode == iItemNum && aItemList[i].iItemIdx == iItemIndex )
		{
			iPropertyKind = aItemList[i].iPropertyKind;
			break;
		}
	}
	
	if( iPropertyKind == -1 ) return;
	
	int iPropertyIndex1 = -1;
	int iPropertyIndex2 = -1;
	int iPropertyIndex3 = -1;
	int iEventCoin = -1;
	srand( (unsigned)time( NULL ) + (unsigned)&iSmallKind );
	int iPercent = (((long)rand() << 15) | rand() )TREASURE_BOX_PERCENT;	// 20071120 Update Treasure Box Percent

	TRACE("?�센??- 11866902\n",iPercent);
 ( NEW_PLAYER_GIFT_KIND_NUM == iPropertyKind  )
	{
		pUser->ProcessOpenNewPlayerGift( iItemIndex );
		return;
	}
	else if ( ITEM_PROPERTY_KIND_SPECIAL_LEVELUP_PACKAGE == iPropertyKind  )
	{
		pUser->ProcessOpenCrazyLevelUpEvent( iItemIndex );
		return;
	}
	else if( ITEM_PROPERTY_KIND_KYOUNGLEE_PRE_PURCHASE == iPropertyKind)
	{
		pUser->ProcessKyoungLeeCharacter(iItemIndex);
		return;
	}
	
	// 20080812 ?�물?�자
	if( iPropertyKind >= MIN_TREASURE_KIN_NUM && iPropertyKind <= MAX_TREASURE_KIN_NUM)		
	{
		int iUpdatedPropertyTypeValue = 0;
		// iPropertyKind 800 - ?? 801 - ?�랏, 802 - 문신

		int iRarityLevel = RARITY_LEVEL_NONE;
		int iNewProductIndex = INVALID_IDINDEX;
		int iNewInventoryIndex = INVALID_IDINDEX;
		int iNewProductCount = 0;
		int iItemIDNumber = 0;
		SDateInfo sProductionDate;

		int iSpecialCardIndex = 0;

		if( ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->MAGICBOX_OpenMagicBox( pUser->GetGameIDIndex(), iItemIndex, iPropertyKind, iPercent, iPoint, iTrophy, iNewItemCode, iMailType, iIndex, 
			iItemType, iPropertyType, iPropertyTypeValue, iMyPropertyKind, iPropertyIndex1, iPropertyIndex2, iPropertyIndex3,
			ExpireTime, iSellPrice,iEventCoin, iUpdatedPropertyTypeValue, iResult , iRarityLevel, iNewProductIndex, iNewInventoryIndex, 
			iNewProductCount, sProductionDate, iSpecialCardIndex, iItemIDNumber) )
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "MAGICBOX_OpenMagicBox - GameID:�4 ",pUser->GetGameID());
		return;
		}

		if ( iResult ==0 && iItemType == 5 )
		{
			pUser->ProcessOpenNewPlayerGift( iItemIndex );
		}
		//if( iResult == 0  && iItemType == 4 /*&& iPropertyKind == 605*/ )
		/*{
			if( iEventCoin == 0  )
			{   //���ڴ�
				CPacketComposer PacketComposer(S2C_EVENT_COIN);
				PacketComposer.Add(iEventCoin);
				pUser->Send(&PacketComposer);
				pAvatarItemList->RemoveItem(iItemIndex);
			}
			else if( iEventCoin < 0  )
			{   //δ���� ��������
				CPacketComposer PacketComposer(S2C_EVENT_COIN);
				PacketComposer.Add(iEventCoin);
				pUser->Send(&PacketComposer);
			}
			else  if( iEventCoin > 0 && iEventCoin > pUser->GetEventCoin()  )
			{   //������
				int iAddEvent = iEventCoin - pUser->GetEventCoin();
				pUser->SetEventCoin(iEventCoin);
				CPacketComposer PacketComposer1(S2C_EVENT_COIN);
				PacketComposer1.Add(iAddEvent);
				pUser->Send(&PacketComposer1);
				pAvatarItemList->RemoveItem(iItemIndex);

				pUser->SendUserGold();
			}
			return;
		}*/

		if( iResult == 0 && (iPoint == -1 && iTrophy == -1))
		{
			iResult = 0;
		}
		else if( iResult == 0 && iPoint > -1 && (1 != iItemType && 5 != iItemType) )
		{
			iResult = 1;
		}
		else if( ( iResult == 0 && iTrophy > -1 ) )
		{
			iResult = 6;
		}
		
		if( iResult >= 0 )
		{
			if ( 5 != iItemType )
			{
				if(iUpdatedPropertyTypeValue > 0)
					pAvatarItemList->UpdateItemCount(iItemIndex, iUpdatedPropertyTypeValue);
				else
					pAvatarItemList->RemoveItem(iItemIndex);
			}
			
			if( iResult == 1 || iResult == 2 )
			{
				pUser->SetSkillPoint( pUser->GetSkillPoint() + iPoint );
				pUser->SendUserGold();
				pUser->AddPointAchievements(); 
			}			
			else if(iResult == 6)
			{
				pUser->SetUserTrophy(pUser->GetUserTrophy() + iTrophy);
				pUser->SendCurAvatarTrophy();
			}

			CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
			PacketComposer.Add(iResult);
			PacketComposer.Add(iNewItemCode);
			
			if( iResult == 5) //vip?�이?�이�??�용?�수�?보낸??
			{
				PacketComposer.Add(iPropertyTypeValue);
			}
			else if( iResult == 1 )
			{
				PacketComposer.Add(iPoint);				
			}
			else if( iResult == 6 )
			{
				PacketComposer.Add(iTrophy);
			}
			pUser->Send(&PacketComposer);
			
		}	
	
		if( 0 == iResult && (iItemType != 3 && iItemType != 6) || 
			1 == iResult && (1 == iItemType || 5 == iItemType) ) // ?�벤?�리??받는 ?�이??
		{
			SUserItemInfo UserItemInfo;
			SShopItemInfo ShopItemInfo;
			
			int iErrorCode = 0;
			
			if( FALSE == pItemShop->GetItem(iNewItemCode , ShopItemInfo) )  
			{
				iErrorCode = -5;
			}
			else if( 7 == iItemType && 0 < iEventCoin ) // eventcoin
			{
				pUser->SetEventCoin(pUser->GetEventCoin() + iEventCoin);
				pUser->SendUserGold();				
			}
			else if(ITEM_PROPERTY_EXHAUST == iPropertyType)
			{
				SUserItemInfo ItemInfo;
				
				((CFSODBCBase *)pNexusODBC)->FSGetAvatarItem(ItemInfo, pUser->GetGameIDIndex(), iIndex);
				((CFSODBCBase *)pNexusODBC)->FSGetAvatarItemProperty(pAvatarItemList, pUser->GetGameIDIndex(), iIndex);
				pAvatarItemList->UpdateExhaustItemPropertyValue(ItemInfo);
			}
			else
			{
				UserItemInfo.iItemIdx = iIndex;
				UserItemInfo.iChannel = ShopItemInfo.iChannel;
				UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;
				UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
				UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
				UserItemInfo.iItemCode = iNewItemCode;
				UserItemInfo.iSellType = SELL_TYPE_POINT;
				UserItemInfo.iSellPrice	= iSellPrice;
				UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;
				UserItemInfo.iPropertyType = iPropertyType;
				UserItemInfo.iPropertyTypeValue = iPropertyTypeValue;	
				UserItemInfo.iPropertyKind =  iMyPropertyKind;
				UserItemInfo.ExpireDate = ExpireTime;
				
				int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {iPropertyIndex1,iPropertyIndex2,iPropertyIndex3,-1,-1,-1};
				ITEM_PROPERTY_VECTOR vecItemProperty;
				pItemShop->GetItemPropertyList(iaPropertyIndex, vecItemProperty);

				pUserItem->InsertUserItemProperty( pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, pAvatarItemList, vecItemProperty);
				UserItemInfo.iPropertyNum = vecItemProperty.size();
					
				int res = 0;
				iErrorCode = 0;
				
				SUserItemInfo* pItem  = pAvatarItemList->GetItemWithItemIdx(UserItemInfo.iItemIdx);
				
				if( NULL != pItem && pItem->iStatus == 0 )
				{
					
					pItem->iItemCode = UserItemInfo.iItemCode;
					pItem->iBigKind = UserItemInfo.iBigKind;
					pItem->iSmallKind = UserItemInfo.iSmallKind;

					pItem->iSellType = UserItemInfo.iSellType;
					pItem->iChannel = ShopItemInfo.iChannel;
					pItem->iSexCondition = UserItemInfo.iSexCondition;
					pItem->iSellPrice = UserItemInfo.iSellPrice;
					pItem->iStatus = 1;
					
					pItem->iPropertyType = UserItemInfo.iPropertyType;
					pItem->iPropertyTypeValue = UserItemInfo.iPropertyTypeValue;		
					pItem->iPropertyKind = UserItemInfo.iPropertyKind;	//// 20060525 1182 Item Update Bug Fix
					pItem->iPropertyNum = UserItemInfo.iPropertyNum;
					pItem->ExpireDate = UserItemInfo.ExpireDate;
				}
				else
				{
					pAvatarItemList->AddItem(UserItemInfo);
				}
			}

			if(0 < iPoint)
			{
				pUser->SetSkillPoint( pUser->GetSkillPoint() + iPoint );
				pUser->SendUserGold();
				pUser->AddPointAchievements(); 
			}
		}
		else if( iResult == 0 && iItemType == 3 ) // ?�물?�으�?받는 ?�이??
		{
			if( iIndex != -1 )
			{
				pUser->RecvPresent(iMailType, iIndex);
			}
		}
		else if( iResult < 0 )
		{
			CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
			PacketComposer.Add(iResult);
			
			pUser->Send(&PacketComposer);
		}
		else if ( iResult == 0 && iItemType == 6 )
		{
			VectorRandomItemConstraintConfig vecConfig;
			COACHCARD.GetItemConstraintConfig(iItemIDNumber, vecConfig);
			
			SProductData ProductData;

			if( 0 < vecConfig.size())
			{
				ProductData.iGradeLevel = vecConfig[0].iGradeLevel;
				ProductData.iTendencyType = vecConfig[0].iTendencyType;
			}
			else
			{
				ProductData.iGradeLevel = RANDOMCARD_GRADE_LEVEL_ALL;
				ProductData.iTendencyType = TENDENCY_TYPE_ALL;
			}

			
			ProductData.iInventoryIndex = iNewInventoryIndex;
			ProductData.iProductIndex = iNewProductIndex;
			ProductData.iItemIDNumber =iItemIDNumber;
			ProductData.iItemType = COACHCARD.GetItemType(iItemIDNumber);
			if( -1 == ProductData.iItemType )  ProductData.iItemType = 1;
			
			ProductData.sProductionDate = sProductionDate;

			SYSTEMTIME systemExpireTime;
			TimetToSystemTime(ExpireTime, systemExpireTime);
			ProductData.sExpireDate.iYear	= systemExpireTime.wYear;
			ProductData.sExpireDate.iMonth	= systemExpireTime.wMonth;
			ProductData.sExpireDate.iDay	= systemExpireTime.wDay;
			ProductData.sExpireDate.iHour	= systemExpireTime.wHour;
			ProductData.sExpireDate.iMin	= systemExpireTime.wMinute;

			ProductData.iProductCount = iNewProductCount;
			ProductData.iSpecialCardIndex = iSpecialCardIndex;
			ProductData.iRarityLevel = iRarityLevel;

			pUser->AddProductToInventory(ProductData);
		}
	}
	else if( iPropertyKind >= MIN_STEP_BOX_KIN_NUM && iPropertyKind <= MAX_STEP_BOX_KIN_NUM)
	{
		// iPropertyKind 800 - ?? 801 - ?�랏, 802 - 문신
		if( ODBC_RETURN_SUCCESS != ((CFSGameODBC *)pNexusODBC)->BONUSBOX_OpenBonusBox( pUser->GetGameIDIndex(), pUser->GetPublisherIDIndex(), iItemIndex, iPropertyKind, iPercent, iCash, iPoint, iNewItemCode, iMailType, iIndex, 
			iPropertyType, iPropertyTypeValue, iMyPropertyKind, iPropertyIndex1, iPropertyIndex2, iPropertyIndex3,
			ExpireTime, iSellPrice, iResult ) )
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "BONUSBOX_OpenBonusBox - GameID:�4 ",pUser->GetGameID());
		return;
		}
				
		/* 2010.07.21 F(x) 캐릭??구매 ?�도 ?�벤??*/		
		if( iResult == EVENT_GIFT_POINT_NUM || iResult == EVENT_GIFT_CASH_NUM )
		{
			int iResultChange;
			pAvatarItemList->RemoveItem(iItemIndex);
			
			pUser->SetCoin( pUser->GetCoin() + iCash );
			pUser->SetSkillPoint( pUser->GetSkillPoint() + iPoint );			
			pUser->SendUserGold();
			pUser->AddPointAchievements(); 
			
			CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
			if (iResult == EVENT_GIFT_CASH_NUM)
			{
				iResultChange = 3;
				PacketComposer.Add(iResultChange);
				PacketComposer.Add(iNewItemCode);
				PacketComposer.Add(iCash);
			}
			else
			{
				iResultChange = 4;
				PacketComposer.Add(iResultChange);
				PacketComposer.Add(iNewItemCode);
				PacketComposer.Add(iPoint);
			}			
			pUser->Send(&PacketComposer);
		}
		
		if( iResult == EVENT_GIFT_NORMAL_ITEM_NUM || iResult == EVENT_GIFT_OFFLINE_ITEM_NUM )
		{
			int iResultChange = 0;
			iPoint = 0;
			
			pAvatarItemList->RemoveItem(iItemIndex);
			
			CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
			PacketComposer.Add(iResultChange);
			PacketComposer.Add(iNewItemCode);
			PacketComposer.Add(iPoint);
			
			pUser->Send(&PacketComposer);
		}
		
		if( ( iResult == EVENT_GIFT_NORMAL_ITEM_NUM && iNewItemCode != SKILL_SLOT_ITEMCODE ) || ( iResult == EVENT_GIFT_OFFLINE_ITEM_NUM && iNewItemCode != SKILL_SLOT_ITEMCODE ) )
		{
			SUserItemInfo UserItemInfo;
			SShopItemInfo ShopItemInfo;
			
			int iErrorCode = 0;
			
			if( FALSE == pItemShop->GetItem(iNewItemCode , ShopItemInfo) )  
			{
				iErrorCode = -5;
			}
			else
			{
				UserItemInfo.iItemIdx = iItemIndex;
				UserItemInfo.iChannel = ShopItemInfo.iChannel;
				UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;
				UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
				UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
				UserItemInfo.iItemCode = iNewItemCode;
				UserItemInfo.iSellType = SELL_TYPE_POINT; 
				UserItemInfo.iSellPrice	= iSellPrice;
				UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;
				UserItemInfo.iPropertyType = iPropertyType;
				UserItemInfo.iPropertyTypeValue = iPropertyTypeValue;
				UserItemInfo.iPropertyKind =  iMyPropertyKind;
				UserItemInfo.ExpireDate = ExpireTime;
				
				int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {iPropertyIndex1,iPropertyIndex2,iPropertyIndex3,-1,-1,-1};
				ITEM_PROPERTY_VECTOR vecItemProperty;
				pItemShop->GetItemPropertyList(iaPropertyIndex, vecItemProperty);

				pUserItem->InsertUserItemProperty( pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, pAvatarItemList, vecItemProperty);
				UserItemInfo.iPropertyNum = vecItemProperty.size();
				
				int res = 0;
				iErrorCode = 0;
				
				SUserItemInfo* pItem  = pAvatarItemList->GetItemWithItemIdx(UserItemInfo.iItemIdx);
				
				if( NULL != pItem && pItem->iStatus == 0 )
				{
					
					pItem->iItemCode = UserItemInfo.iItemCode;
					pItem->iBigKind = UserItemInfo.iBigKind;
					pItem->iSmallKind = UserItemInfo.iSmallKind;

					pItem->iSellType = UserItemInfo.iSellType;
					pItem->iChannel = ShopItemInfo.iChannel;
					pItem->iSexCondition = UserItemInfo.iSexCondition;
					pItem->iSellPrice = UserItemInfo.iSellPrice;
					pItem->iStatus = 1;
					
					pItem->iPropertyType = UserItemInfo.iPropertyType;
					pItem->iPropertyTypeValue = UserItemInfo.iPropertyTypeValue;		
					pItem->iPropertyKind = UserItemInfo.iPropertyKind;	//// 20060525 1182 Item Update Bug Fix
					pItem->iPropertyNum = UserItemInfo.iPropertyNum;
					pItem->ExpireDate = UserItemInfo.ExpireDate;
				}
				else
				{
					pAvatarItemList->AddItem(UserItemInfo);
				}
			}
		}
		else if( ( iResult == EVENT_GIFT_NORMAL_ITEM_NUM && iNewItemCode == SKILL_SLOT_ITEMCODE ) || ( iResult == EVENT_GIFT_OFFLINE_ITEM_NUM && iNewItemCode == SKILL_SLOT_ITEMCODE ) )
		{
			if( iIndex != -1 )
			{
				pUser->RecvPresent(iMailType, iIndex);
			}
		}
		else if( iResult < EVENT_GIFT_NORMAL_ITEM_NUM )
		{
			CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
			PacketComposer.Add(iResult);
			
			pUser->Send(&PacketComposer);
		}
		
		if( iResult == EVENT_GIFT_OFFLINE_ITEM_NUM )
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
			
			if(pCenter)
			{
				pCenter->SendEventShoutMsgReq( pUser->GetGameIDIndex(), pUser->GetUserID(), iNewItemCode );
			}
		}
	}

	if (iPropertyKind == SPECIAL_CHARACTER_SLOT_KIND_NUM || iPropertyKind == SPECIAL_CHARACTER_SLOT_KIND_NUM_EXPERT ||
		iPropertyKind == APINK_FIRST_LEVEL_CHAR_SLOT || iPropertyKind == APINK_SECOND_LEVEL_CHAR_SLOT ||
		iPropertyKind == APINK_THIRD_LEVEL_CHAR_SLOT || iPropertyKind == APINK_FOURTH_LEVEL_CHAR_SLOT ||
		(iPropertyKind >= THREE_KINGDOMS_CHARACTER_SLOT_KIND_MIN && iPropertyKind <= THREE_KINGDOMS_CHARACTER_SLOT_KIND_MAX) ||
		(iPropertyKind >= MADMASTERS_CHARACTER_SLOT_KIND_MIN && iPropertyKind <= MADMASTERS_CHARACTER_SLOT_KIND_MAX))
	{
		int iItemFace = 0;					
		if (iPropertyKind == WONDER_GIRLS_CHARACTER_SLOT_KIND_NUM || iPropertyKind == WONDER_GIRLS_CHARACTER_SLOT_KIND_NUM_EXPERT)
			iItemFace = 9;	

		CPacketComposer PacketComposer(S2C_CHOOSE_SPECIAL_CHARACTER_FROM_INVENTORY);							
		PacketComposer.Add(iItemFace);
		PacketComposer.Add(iPropertyKind);			
		PacketComposer.Add(iItemIndex);
		PacketComposer.Add(pUser->GetGameIDIndex());
		
		short sGroupIndex = AVATARCREATEMANAGER.GetGroupIndexByPropertyKind(iPropertyKind);
		int iCreateLv = AVATARCREATEMANAGER.GetSlotCreateLevel(sGroupIndex, iPropertyKind);
		PacketComposer.Add(sGroupIndex);
		PacketComposer.Add(iCreateLv);

		pUser->Send(&PacketComposer);																	
	}
	else if(ITEM_PROPERTY_KIND_PURCHASE_GRADE_MIN <= iPropertyKind && iPropertyKind <= ITEM_PROPERTY_KIND_PURCHASE_GRADE_MAX)
	{
		pUser->GetUserPurchaseGrade()->GiveCardReward(iItemNum, iPropertyKind);
	}
	else if(RESET_SEASON_RECORD_ITEM_KIND_NUM == iPropertyKind || RESET_TOTAL_RECORD_ITEM_KIND_NUM == iPropertyKind)
	{		
		int iRet = ((CFSGameODBC *)pNexusODBC)->RESET_UserMatchRecordInventory(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), iItemIndex, iPropertyKind);
		if(ODBC_RETURN_SUCCESS == iRet)
		{
			SUserItemInfo* pItem  = pAvatarItemList->GetInventoryItemWithPropertyKind(iPropertyKind);
			if( pItem != NULL && pItem->iPropertyType == ITEM_PROPERTY_TIME )
			{
				pAvatarItemList->RemoveItem(pItem->iItemIdx);
			}

			SAvatarInfo AvatarInfo;
			if( TRUE == pUser->CurUsedAvatarSnapShot( AvatarInfo ) )
			{
				BYTE btAvatarLvGrade = pUser->GetCurUsedAvtarLvGrade();
				int iRankType = pUserItem->GetRankTypeByPropertyKind(iPropertyKind);
				
				CFSGameServer* pServer = CFSGameServer::GetInstance();
				CHECK_CONDITION_RETURN_VOID(NULL == pServer);

				RANK.ResetAvatarRankInTopRank(AvatarInfo.iGameIDIndex, iRankType, btAvatarLvGrade);

				pUser->ResetRecordAndRank(iRankType, GetCurrentSeasonIndex());

				CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
				PacketComposer.Add(OPEN_T_BOX_RESET_RECORD_RES_CODE);
				pUser->Send(&PacketComposer);
			}			
		}
		else if(-2 == iRet)
		{
			CPacketComposer PacketComposer(S2C_OPEN_T_BOX_RESULT);
			PacketComposer.Add(OPEN_T_BOX_RESET_RECORD_RES_CODE_FAIL);
			pUser->Send(&PacketComposer);
		}
	}

	vector<int> vInventoryItemCounts;
	pAvatarItemList->GetInventoryItemCountforAchievement(vInventoryItemCounts);
	pUser->ProcessAchievementbyGroupAndComplete( ACHIEVEMENT_CONDITION_GROUP_INVENTORY, ACHIEVEMENT_LOG_GROUP_SHOP, vInventoryItemCounts);
}

void CFSGameThread::ProcessMyOwnItemList(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser ) return;
	
	pUser->SendAllMyOwnItemInfo();
}

void CFSGameThread::Process_FSIUserItemLock(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	if( NULL == pUser )	return;
	
	int iItemIdx = 0;
	int iItemCode = 0;
	int iRet = -1;
	
	m_ReceivePacketBuffer.Read(&iItemIdx);
	m_ReceivePacketBuffer.Read(&iItemCode);
	
	
	CFSGameUserItem* pUserItem = pUser->GetUserItemList();
	
	if( NULL == pUserItem )
	{
		CPacketComposer PacketComposer(S2C_USER_ITEM_LOCK_RES);
		PacketComposer.Add(iRet);
		pUser->Send(&PacketComposer);
		return;
	}
	
	pUserItem->ChangeItemLockStatus( iItemIdx, iItemCode, iRet );
	
	CPacketComposer PacketComposer(S2C_USER_ITEM_LOCK_RES);
	PacketComposer.Add(iRet);
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_FSExposeUserItem(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );
	
	pUser->SendExposeUserItem();
	pUser->SendUserStat();
}

void CFSGameThread::Process_FSDiscountItemInfoReq(CFSGameClient* pClient)
{
	CHECK_NULL_POINTER_VOID(pClient);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	time_t CurrentTime = _time64(NULL);

	pUser->SetBuyState(FS_ITEM_OP_LIST);
	int iCategory = (pClient->GetState() == FS_CLUB) ? ITEM_CATEGORY_CLUB : ITEM_CATEGORY_NORMAL;
	int iCashDiscountRate = pUser->GetDiscountRate(SELL_TYPE_CASH, iCategory);
	int iPointDiscountRate = pUser->GetDiscountRate(SELL_TYPE_POINT, iCategory);

	list<SItemPrice> listItemPrice;
	pItemShop->GetSaleItemInfo(listItemPrice, iCashDiscountRate, iPointDiscountRate);

	CPacketComposer PacketComposer(S2C_DISCOUNTITEMINFO_RES);
	PacketComposer.Add((int)OP0_CLEAN);
	PacketComposer.Add(iCashDiscountRate);
	PacketComposer.Add(iPointDiscountRate);
	pClient->Send(&PacketComposer, FALSE);
	
	list<SItemPrice>::iterator iter = listItemPrice.begin();
	list<SItemPrice>::iterator endIter = listItemPrice.end();
	for(; iter != endIter; ++iter)
	{
		PacketComposer.Initialize(S2C_DISCOUNTITEMINFO_RES);
		PacketComposer.Add((int)OP1_CREATE);
		PacketComposer.Add((*iter).iItemCode);
		PacketComposer.Add((*iter).GetPrice(CurrentTime));
		PacketComposer.Add((*iter).GetDisCountRate());
		PacketComposer.Add((*iter).GetSaleRemainTime(CurrentTime));

		pClient->Send(&PacketComposer, FALSE);
	}

	CLUBCONFIGMANAGER.SendClubShopBuyConditionList(pAvatar->iClubLeagueSeasonRank, pItemShop, pUser);
	
	PacketComposer.Initialize(S2C_DISCOUNTITEMINFO_RES);
	PacketComposer.Add((int)OP4_END);
	pClient->Send(&PacketComposer);
}

void CFSGameThread::Process_AccItemSelect_Req(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	CNexusODBC* pNexusODBC = (CNexusODBC*)ODBCManager.GetODBC( ODBC_NEXUS );
	CHECK_NULL_POINTER_VOID( pNexusODBC );

	int iItemIdx	= -1;
	m_ReceivePacketBuffer.Read(&iItemIdx);

	CPacketComposer PacketComposer(S2C_ACCITEM_SELECT_RES);
	int iErrorCode = ACCITEM_SELECT_ERROR_NOT_EXIST_ITEM;

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if(pItem == NULL)
	{
		PacketComposer.Add(iErrorCode);	
		PacketComposer.Add(iItemIdx);
		PacketComposer.Add((BYTE)ACCITEM_STATUS_NOT_CHECKBOX);
		pUser->Send(&PacketComposer);

		return;
	}

	vector< SUserItemProperty > vUserItemProperty;
	pAvatarItemList->GetItemProperty(pItem->iItemIdx, vUserItemProperty);

	if(vUserItemProperty.size() == 0)
	{
		iErrorCode = ACCITEM_SELECT_ERROR_NOT_EXIST_PROPERTY;

		PacketComposer.Add(iErrorCode);	
		PacketComposer.Add(iItemIdx);
		PacketComposer.Add(pAvatarItemList->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
		pUser->Send(&PacketComposer);

		return;
	}

	BYTE btKind = ACCITEM_PROPERTY_ADD;
	if(TRUE == pAvatarItemList->CheckUserApplyAccItemProperty(iItemIdx))
	{
		btKind = ACCITEM_PROPERTY_DELETE;
	}

	if(btKind == ACCITEM_PROPERTY_ADD)
	{
		if(pAvatarItemList->IsFullApplyAccItemProperty())
		{
			iErrorCode = ACCITEM_SELECT_ERROR_EXCESS_COUNT;

			PacketComposer.Add(iErrorCode);	
			PacketComposer.Add(iItemIdx);
			PacketComposer.Add(pAvatarItemList->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
			pUser->Send(&PacketComposer);

			return;
		}

		if(TRUE == pAvatarItemList->CheckUserApplyAccItemProperty(iItemIdx))
		{
			iErrorCode = ACCITEM_SELECT_ERROR_ALREADY_SELECT;

			PacketComposer.Add(iErrorCode);	
			PacketComposer.Add(iItemIdx);
			PacketComposer.Add(pAvatarItemList->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
			pUser->Send(&PacketComposer);

			return;
		}

		if( ODBC_RETURN_SUCCESS == ((CFSODBCBase *)pNexusODBC)->ITEM_InsertUserApplyAccItemProperty(pUser->GetGameIDIndex(), iItemIdx))
		{
			iErrorCode = ACCITEM_SELECT_ERROR_SUCCESS;

			pAvatarItemList->AddUserApplyAccItemProperty(iItemIdx);
		}
		pAvatarItemList->Lock();
		pAvatarItemList->ChangeItemPos(pItem, 2, 2);
		pAvatarItemList->Unlock();
	}
	else if(btKind == ACCITEM_PROPERTY_DELETE)
	{
		if(FALSE == pAvatarItemList->CheckUserApplyAccItemProperty(iItemIdx))
		{
			iErrorCode = ACCITEM_SELECT_ERROR_NOT_IN_SELECT_LIST;

			PacketComposer.Add(iErrorCode);	
			PacketComposer.Add(iItemIdx);
			PacketComposer.Add(pAvatarItemList->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
			pUser->Send(&PacketComposer);

			return;
		}

		if( ODBC_RETURN_SUCCESS == ((CFSODBCBase *)pNexusODBC)->ITEM_DeleteUserApplyAccItemProperty(pUser->GetGameIDIndex(), iItemIdx))
		{
			iErrorCode = ACCITEM_SELECT_ERROR_SUCCESS;

			pAvatarItemList->RemoveUserApplyAccItemProperty(iItemIdx);
		}
		pAvatarItemList->Lock();
		pAvatarItemList->ChangeItemPos(pItem, 2, 1);
		pAvatarItemList->Unlock();
	}

	PacketComposer.Add(iErrorCode);	
	PacketComposer.Add(iItemIdx);
	PacketComposer.Add(pAvatarItemList->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind));
	PacketComposer.Add(pAvatarItemList->GetDifferentUserPropertyAccItemStatus());
	pUser->Send(&PacketComposer);

	pUser->SendUserStat();

	pUser->SendAvatarInfoUpdateToMatch();	
}

void CFSGameThread::Process_ChangeItemColorReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();

	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	if( NULL == pUserItemList ) return;

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	int iUseItemIndex = INVALID_IDINDEX;
	int iTargetItemIndex = INVALID_IDINDEX;
	int iChangeItemCode = INVALID_IDINDEX;
	int iTargetItemCode = INVALID_IDINDEX;

	m_ReceivePacketBuffer.Read(&iUseItemIndex);
	m_ReceivePacketBuffer.Read(&iTargetItemIndex);
	m_ReceivePacketBuffer.Read(&iChangeItemCode);

	BOOL bResult = FALSE;

	if( TRUE == pAvatarItemList->CheckTargetColorChange(iUseItemIndex, iTargetItemIndex, iTargetItemCode) )	
	{
		BOOL bSameColorGroup = pItemShop->FindAndCheckSameColorGroup(iTargetItemCode, iChangeItemCode); // �����Ϸ��� �����ۿ� �ش� �������� ���� �������� Ȯ��

		if( TRUE == bSameColorGroup && ODBC_RETURN_SUCCESS == pGameODBC->ITEM_ChangeColor( pUser->GetGameIDIndex(), iUseItemIndex, iTargetItemIndex, iChangeItemCode ) )
		{
			if( FALSE == pAvatarItemList->ProcessAfterChangeItemColor(iUseItemIndex, iTargetItemIndex, iChangeItemCode, bSameColorGroup ) )
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "ProcessAfterChangeItemColor - GameID:�4 ",pUser->GetGameID());
		}

			bResult = TRUE;
		}
		else
		{			
			WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_ChangeColor - UseItemIndex:11866902, TargetItemIndex:0, TargetItemCode:18227200, ChangeItemCode:-858993460, SameColorGroup:-858993460", 
temIndex, iTargetItemCode, iChangeItemCode, bSameColorGroup);
		}
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "CheckTargetColorChange - UseItemIndex:11866902, TargetItemIndex:0, TargetItemCode:18227200", 
Index, iTargetItemIndex, iTargetItemCode);
	}

	if( !bResult )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "Process_ChangeItemColorReq - UseItemIndex:11866902, TargetItemIndex:0, TargetItemCode:18227200, ChangeItemCode:-858993460", 
TargetItemIndex, iTargetItemCode, iChangeItemCode);
	}

	CPacketComposer PacketComposer(S2C_CHANGE_ITEM_COLOR_RES);
	PacketComposer.Add(bResult);
	pFSClient->Send(&PacketComposer);

}

void CFSGameThread::Process_ChannelBuffItemCheckUseReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iItemIndex = INVALID_IDINDEX;
	m_ReceivePacketBuffer.Read(&iItemIndex);

	CPacketComposer PacketComposer(S2C_CHANNEL_BUFF_ITEM_CHECK_USE_RES);
	short stResult = CHANNEL_BUFF_ITEM_CHECK_USE_SUCCESS;

	if(FALSE == CHANNELBUFFMANAGER.CheckMaxUseCount())
	{
		stResult = CHANNEL_BUFF_ITEM_CHECK_USE_FAIL_FULL;
		PacketComposer.Add(stResult);
		pFSClient->Send(&PacketComposer);

		return;
	}

	PacketComposer.Add(stResult);
	PacketComposer.Add(iItemIndex);
	PacketComposer.Add(CHANNELBUFFMANAGER.GetChannelBuffTime());
	PacketComposer.Add(CHANNELBUFFMANAGER.GetPoint());
	PacketComposer.Add(CHANNELBUFFMANAGER.GetExp());
	PacketComposer.Add(CHANNELBUFFMANAGER.GetUseCount());
	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_ChannelBuffItemUseReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	CUserAction* pUserAction = pUser->GetUserAction();
	CHECK_NULL_POINTER_VOID(pUserAction);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	int iItemIndex = INVALID_IDINDEX;
	m_ReceivePacketBuffer.Read(&iItemIndex);

	CPacketComposer PacketComposer(S2C_CHANNEL_BUFF_ITEM_USE_RES);
	short stResult = CHANNEL_BUFF_ITEM_USE_SUCCESS;

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIndex);
	if(pItem == NULL)
	{
		stResult = CHANNEL_BUFF_ITEM_USE_NOT_EXIST_ITEM;
		PacketComposer.Add(stResult);
		pFSClient->Send(&PacketComposer);

		return;
	}
	
	if(FALSE == CHANNELBUFFMANAGER.CheckMaxUseCount())
	{
		stResult = CHANNEL_BUFF_ITEM_USE_FAIL_FULL;
		PacketComposer.Add(stResult);
		pFSClient->Send(&PacketComposer);

		return;
	}

	int iNum = INVALID_IDINDEX;
	if(ODBC_RETURN_SUCCESS != pGameODBC->ITEM_UseChannelBuffItem(pServer->GetProcessID(), pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), pUser->GetCurUsedAvatarSex(), 
		pItem->iItemCode, pItem->iItemIdx, CHANNELBUFFMANAGER.GetChannelBuffTime(), iNum))
	{
		stResult = CHANNEL_BUFF_ITEM_USE_FAIL;
		PacketComposer.Add(stResult);
		pFSClient->Send(&PacketComposer);

		return;
	}

	int iTempItemIdx = -1,iTempItemCode = -1;
	iTempItemIdx = pItem->iItemIdx;
	iTempItemCode = pItem->iItemCode;
	if(FALSE == pAvatarItemList->RemoveItem(pItem->iItemIdx))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHANNELBUFF, EXEC_FUNCTION, FAIL, "Process_ChannelBuffItemUseReq::RemoveItem - GameIDIndex:11866902", pUser->GetGameIDIndex());
	if(FALSE == CHANNELBUFFMANAGER.AddChannelBuffTime(pServer->GetProcessID(), iNum, pUser->GetGameIDIndex(), pUser->GetGameID()))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHANNELBUFF, EXEC_FUNCTION, FAIL, "Process_ChannelBuffItemUseReq::AddChannelBuffTime - GameIDIndex:11866902, Num:0", pUser->GetGameIDIndex(), iNum);
tResult = CHANNEL_BUFF_ITEM_USE_FAIL;
		PacketComposer.Add(stResult);
		pFSClient->Send(&PacketComposer);

		return;
	}

	int iUseCount = CHANNELBUFFMANAGER.GetUseCount();

	PacketComposer.Add(stResult);
	PacketComposer.Add(iUseCount-1);
	pFSClient->Send(&PacketComposer);

	PacketComposer.Initialize(S2C_CHANNEL_BUFF_INFO_NOTIFY);
	CHANNELBUFFMANAGER.MakeChannelBuffTimeInfo(PacketComposer);
	pServer->BroadCast(PacketComposer);

	CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	if(pCenterSvrProxy)
	{
		PacketComposer.Initialize(G2S_USE_CHANNEL_BUFF_ITEM_REQ);
		CHANNELBUFFMANAGER.MakeUserChannelBuffItemInfo(PacketComposer, pServer->GetProcessID(), pUser->GetGameID(), pUser->GetGameIDIndex(), iNum);
		pCenterSvrProxy->Send(&PacketComposer);
	}
}

void CFSGameThread::Process_ChannelBuffRankingReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer PacketComposer(S2C_CHANNEL_BUFF_RANKING_RES);
	CHECK_CONDITION_RETURN_VOID(FALSE == CHANNELBUFFMANAGER.MakeChannelBuffRanking(PacketComposer));
	
	pUser->Send(&PacketComposer);
};

void CFSGameThread::Process_CheckDressItemReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CPacketComposer PacketComposer(S2C_CHECK_CHANGE_DERSS_ITEM_RES);
	pUserItemList->CheckAndSendChangeDressItem(PacketComposer);
}

void CFSGameThread::Process_RegisterDressItemReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	BYTE btIdxCount = 0, btChangeDressItemType[MAX_CHANGE_DRESS_NUM]; 
	int iaChangeItemCodeList[MAX_CHANGE_DRESS_NUM];

	memset(&btChangeDressItemType, -1, sizeof(BYTE)*MAX_CHANGE_DRESS_NUM);
	memset(&iaChangeItemCodeList, -1, sizeof(int)*MAX_CHANGE_DRESS_NUM);

	m_ReceivePacketBuffer.Read(&btIdxCount);
	for(int i = 0; i < btIdxCount; i++)
	{
		m_ReceivePacketBuffer.Read(&btChangeDressItemType[i]);
		m_ReceivePacketBuffer.Read(&iaChangeItemCodeList[i]);
	}

	CPacketComposer PacketComposer(S2C_REGISTER_CHANGE_DERSS_ITEM_RES);
	pUserItemList->ResigterChangeDressItem(pItemShop, btIdxCount, btChangeDressItemType, iaChangeItemCodeList, PacketComposer);
}

void CFSGameThread::Process_PackageItemComponentListReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CShopItemList* pShopItemList = pItemShop->GetShopItemList();
	CHECK_NULL_POINTER_VOID(pShopItemList);

	int iSex = 0;
	m_ReceivePacketBuffer.Read(&iSex);

	CPacketComposer PacketComposer(S2C_PACKAGEITEM_COMPONENT_LIST_RES);
	pShopItemList->MakePackageItemComponentList(iSex, pUser->GetCurUsedAvatarPosition(), PacketComposer);
	pUser->Send(&PacketComposer);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USE_ANNOUNCE_ITEM_REQ)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CAvatarItemList* pAvatarItemList = pUser->GetAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	CCenterSvrProxy* pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pCenterProxy);

	SS2C_USE_ANNOUNCE_ITEM_RES rs;
	SUserItemInfo* pItem = pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_ANNOUNCE);
	if (NULL == pItem || 0 >= pItem->iPropertyTypeValue)
	{
		rs.btResult = RESULT_USE_ANNOUNCE_ITEM_FAILED_NOT_EXIST_ITEM;
		pFSClient->SendPacket(S2C_USE_ANNOUNCE_ITEM_RES, &rs, sizeof(SS2C_USE_ANNOUNCE_ITEM_RES));
		return;
	}

	CFSGameODBC* pODBC = (CFSGameODBC*) ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pODBC);

	int iResult;
	int iUpdatedCount = 0;
	if (ODBC_RETURN_SUCCESS != (iResult = pODBC->ITEM_DecreaseItemCount(pUser->GetGameIDIndex(), pItem->iItemIdx, 1, iUpdatedCount)))
	{
		rs.btResult = (RESULT_USE_ANNOUNCE_ITEM)iResult;
		pFSClient->SendPacket(S2C_USE_ANNOUNCE_ITEM_RES, &rs, sizeof(SS2C_USE_ANNOUNCE_ITEM_RES));
		return;
	}

	rs.iRemainCount = iUpdatedCount;
	if (0 >= iUpdatedCount)
	{
		pAvatarItemList->RemoveItem(pItem->iItemIdx);
		pItem = NULL;
	}
	else
	{
		pItem->iPropertyTypeValue = iUpdatedCount;
	}

	SC2S_USE_ANNOUNCE_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_USE_ANNOUNCE_ITEM_REQ));
	rq.szAnnounce[MAX_ITEM_ANNOUNCE_CONTENTS_LEN] = '\0';

	//���Ͻ� ���� Ȯ������ �Դ� ��������
	pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
	if (NULL != pCenterProxy) 
	{
		int len = strlen(rq.szAnnounce);
		CPacketComposer	Packet(S2G_MANAGER_ANNOUNCE);
		Packet.Add((int)ANNOUNCE_OUTPUT_IN_TOP);
		Packet.Add(len);
		Packet.Add((PBYTE)rq.szAnnounce, len);
		pCenterProxy->Send(&Packet);

		rs.btResult = RESULT_USE_ANNOUNCE_ITEM_SUCCESS;
		pFSClient->SendPacket(S2C_USE_ANNOUNCE_ITEM_RES, &rs, sizeof(SS2C_USE_ANNOUNCE_ITEM_RES));
	}
}

void CFSGameThread::Process_UseSpecialPropertyItemReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	time_t CurrentTime = _time64(NULL);
	int OPCode = 0;
	int Result = 0;
	m_ReceivePacketBuffer.Read(&OPCode);

	CPacketComposer PacketComposer(C2S_USE_SPECIALPROPERTY_ITEM_RES);
	PacketComposer.Add(OPCode);
	if( OPCode == 1 )///�������Ƕ��װ��
	{
		int iItemIdx = -1;

		m_ReceivePacketBuffer.Read(&iItemIdx);
		SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
		if( pItem == NULL   )
		{	
			Result = -1;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;

		}

		if(  pItem->iPropertyKind != ITEM_PROPERTY_KIND_PROPERTY_ADD_ITEM && ( pItem->iPropertyKind < ITEM_PROPERTY_KIND_DATEVALUE_MIN || pItem->iPropertyKind > ITEM_PROPERTY_KIND_DATEVALUE_MAX ) )
		{
			Result = -2;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}

		vector< SUserItemInfo* > vUserItemInfo;
		pAvatarItemList->GetSpecialPropertyItemList( vUserItemInfo, pItem->iPropertyKind );
		int iItemNum = 0;
		iItemNum = vUserItemInfo.size();
		PacketComposer.Add(Result);
		PacketComposer.Add(iItemNum);
		for( int  i = 0; i  < iItemNum; i++)
		{
			PacketComposer.Add(vUserItemInfo[i]->iItemIdx);
			PacketComposer.Add(vUserItemInfo[i]->iItemCode);
			PacketComposer.Add(vUserItemInfo[i]->iPropertyKind);
			PacketComposer.Add(vUserItemInfo[i]->iPropertyNum);
			if(  vUserItemInfo[i]->iPropertyNum > 0 )
			{

				int iDay = -1;
				int iHour = 0;
				int iMin = 0;
				PacketComposer.Add(iDay);
				PacketComposer.Add(iHour);
				PacketComposer.Add(iMin);

				vector< SUserItemProperty > vUserItemProperty;
				pAvatarItemList->GetItemProperty(vUserItemInfo[i]->iItemIdx, vUserItemProperty);
				int iUserItemPropertyNum = vUserItemProperty.size();
				PacketComposer.Add(&iUserItemPropertyNum);
				for (int iPropertyCount = 0; iPropertyCount < iUserItemPropertyNum ; iPropertyCount++ )
				{
					int iProperty	= vUserItemProperty[iPropertyCount].iProperty;
					int iValue		= vUserItemProperty[iPropertyCount].iValue;

					PacketComposer.Add(&iProperty);
					PacketComposer.Add(&iValue);

				}
			}
		}
		pFSClient->Send(&PacketComposer);
	}
	else if( OPCode == 2 )//��Ƕ
	{
		int iItemIdx		 = -1;
		int iSpecialItemIdx  = -1;
		int iPropertyType = -1;
		int iPropertyValue	 = -1;
		m_ReceivePacketBuffer.Read(&iItemIdx);
		m_ReceivePacketBuffer.Read(&iSpecialItemIdx);
		m_ReceivePacketBuffer.Read(&iPropertyType);
		m_ReceivePacketBuffer.Read(&iPropertyValue);
		SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
		SUserItemInfo* pSpecialItem = pAvatarItemList->GetItemWithItemIdx(iSpecialItemIdx);
		if( pItem == NULL  || pSpecialItem == NULL )
		{	
			Result = -3;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;

		}
		if(  pItem->iPropertyKind < ITEM_PROPERTY_KIND_DATEVALUE_MIN || pItem->iPropertyKind > ITEM_PROPERTY_KIND_DATEVALUE_MAX)
		{
			Result = -4;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		if( ( pSpecialItem->iBigKind != ITEM_BIG_KIND_ACC && pSpecialItem->iSmallKind != ITEM_SMALL_KIND_HAIR ) ||  pSpecialItem->iPropertyTypeValue != -1 || pSpecialItem->iStatus != ITEM_STATUS_INVENTORY
			|| pSpecialItem->iPropertyKind != -1 )
		{
			Result = -5;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		CFSGameODBC* pODBC = (CFSGameODBC*) ODBCManager.GetODBC(ODBC_GAME);
		CHECK_NULL_POINTER_VOID(pODBC);
		TIMESTAMP_STRUCT ExpireTime;
		if( ODBC_RETURN_SUCCESS != pODBC->SP_UserSpecialPropertyItem(pUser->GetGameIDIndex(),iItemIdx,iSpecialItemIdx,pItem->iPropertyKind,iPropertyType,iPropertyValue,ExpireTime))
		{

			Result = -6;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		PacketComposer.Add(Result);
		pSpecialItem->PropertyExpireDate = TimeStructToTimet(ExpireTime);
		if( pSpecialItem->iPropertyKind > -1 && pSpecialItem->iPropertyNum > 0 )
		{
			pAvatarItemList->RemoveItemProperty(iSpecialItemIdx);
		}
		pSpecialItem->iPropertyNum = 1;
		pSpecialItem->iPropertyKind = pItem->iPropertyKind;

		SUserItemProperty ItemProperty;
		ItemProperty.iGameIDIndex = pUser->GetGameIDIndex();
		ItemProperty.iItemIdx = iSpecialItemIdx;
		ItemProperty.iProperty = iPropertyType;
		ItemProperty.iValue = iPropertyValue;
		pAvatarItemList->AddUserItemProperty(ItemProperty);
		pAvatarItemList->RemoveItem(iItemIdx);

		pFSClient->Send(&PacketComposer);
	}
	else if( OPCode == 3 )//��Ƕ
	{
		int iItemIdx		 = -1;
		int iAddPropertyItemIdx  = -1;
		int iAddPropertyValue = -1;
		int iRetValue = -1;

		m_ReceivePacketBuffer.Read(&iItemIdx);
		m_ReceivePacketBuffer.Read(&iAddPropertyItemIdx);
		SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
		SUserItemInfo* pAddPropertyItem = pAvatarItemList->GetItemWithItemIdx(iAddPropertyItemIdx);
		if( pItem == NULL  || pAddPropertyItem == NULL )
		{	
			Result = -3;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;

		}
		if(  pItem->iPropertyKind != ITEM_PROPERTY_KIND_PROPERTY_ADD_ITEM )
		{
			Result = -4;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}

		if( pAddPropertyItem->iSmallKind != ITEM_SMALL_KIND_HAIR ||  pAddPropertyItem->iPropertyTypeValue != -1 || pAddPropertyItem->iStatus != ITEM_STATUS_INVENTORY
			|| pAddPropertyItem->iPropertyNum != 1 )
		{
			Result = -5;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		CFSGameODBC* pODBC = (CFSGameODBC*) ODBCManager.GetODBC(ODBC_GAME);
		CHECK_NULL_POINTER_VOID(pODBC);
		if ( ODBC_RETURN_SUCCESS != pODBC->ITEM_UsePropertyAddItem( pUser->GetGameIDIndex(), iItemIdx, pItem->iPropertyKind, iAddPropertyItemIdx, iAddPropertyValue, iRetValue ))
		{
			Result = iRetValue;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}

		PacketComposer.Add(Result);
		PacketComposer.Add(iAddPropertyValue);

		pAvatarItemList->SetItemPropertyValueAll( iAddPropertyItemIdx, iAddPropertyValue );
		pAvatarItemList->RemoveItem(iItemIdx);

		pFSClient->Send(&PacketComposer);
	}
}
void CFSGameThread::Process_ChangeItemReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	time_t CurrentTime = _time64(NULL);
	int OPCode = 0;
	int iItemIdx = -1;
	int iNewItemcode = -1;
	int Result = 0;
	m_ReceivePacketBuffer.Read(&OPCode);
	m_ReceivePacketBuffer.Read(&iItemIdx);
	m_ReceivePacketBuffer.Read(&iNewItemcode);
	CPacketComposer PacketComposer(S2C_USE_CHANGE_ITEM_RES);
	PacketComposer.Add(OPCode);
	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if( pItem == NULL   )
	{	
		Result = -1;
		PacketComposer.Add(Result);
		pFSClient->Send(&PacketComposer);
		return;

	}
	if( pItem->iPropertyTypeValue != -1  )
	{	
		Result = -2;
		PacketComposer.Add(Result);
		pFSClient->Send(&PacketComposer);
		return;

	}
	if( pItem->iStatus != 1 )
	{	
		Result = -3;
		PacketComposer.Add(Result);
		pFSClient->Send(&PacketComposer);
		return;

	}
	SShopItemInfo ShopItemInfo;
	pItemShop->GetItem(pItem->iItemCode, ShopItemInfo);
	if(  ShopItemInfo.iChangeItem <= 0 )
	{
		Result = -4;
		PacketComposer.Add(Result);
		pFSClient->Send(&PacketComposer);
		return;
	}
	SUserItemInfo* pRemoveItem = pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGEITEM);
	if( pRemoveItem == NULL )
	{
		Result = -5;
		PacketComposer.Add(Result);
		pFSClient->Send(&PacketComposer);
		return;
	}

	int iSex = pUser->GetCurUsedAvatarSex();
	int iLv = ShopItemInfo.iLvCondition;
	int iChannel = pItem->iChannel;
		
	if( OPCode == 1 )//�������
	{
		vector< SChangeNewItemInfo* > vItemInfo;
		pItemShop->GetChangeNewItemList(iSex,iLv,iChannel,vItemInfo);
		int iItemNum =  vItemInfo.size();
		PacketComposer.Add(Result);
		PacketComposer.Add(iItemNum);
		for( int  i = 0; i  < iItemNum; i++)
		{
			PacketComposer.Add(vItemInfo[i]->iItemCode);
		}
		pFSClient->Send(&PacketComposer);

	}
	else if( OPCode == 2 )//����
	{
		if( iNewItemcode == pItem->iItemCode )
		{
			Result = -8;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		if( !pItemShop->GetItem(iNewItemcode, ShopItemInfo) )
		{
			Result = -6;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		
		
		if( !pItemShop->IsChangeNewItem(iSex,iLv,iChannel,ShopItemInfo.iItemCode0) )
		{
			Result = -6;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		
		CFSGameODBC* pODBC =(CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
		CHECK_NULL_POINTER_VOID(pODBC);
		if( ODBC_RETURN_SUCCESS != pODBC->SP_UseChangeItem(pUser->GetGameIDIndex(),iItemIdx,iNewItemcode,pRemoveItem->iItemIdx))
		{
			Result = -7;
			PacketComposer.Add(Result);
			pFSClient->Send(&PacketComposer);
			return;
		}
		PacketComposer.Add(Result);
		PacketComposer.Add(iItemIdx);
		PacketComposer.Add(iNewItemcode);
		pItem->iItemCode = iNewItemcode;
		pItem->iChannel = ShopItemInfo.iChannel;
		pAvatarItemList->RemoveItem(pRemoveItem->iItemIdx);
		pFSClient->Send(&PacketComposer);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHAT_ROOM_NAME_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2T_BASE	info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_CHAT_ROOM_NAME_REQ, &info, sizeof(SS2T_BASE));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_VOICE_SETTING_INFO_REQ)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	time_t CurrentTime = _time64(NULL);
	int OPCode = 0;
	int iItemIdx = -1;
	int iResult = RETURN_CODE_VOICE_SETTING_INFO_SUCCESS;
	m_ReceivePacketBuffer.Read(&OPCode);
	m_ReceivePacketBuffer.Read(&iItemIdx);
	CPacketComposer PacketComposer(S2C_VOICE_SETTING_INFO_RES);
	PacketComposer.Add(OPCode);
	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if ( pItem->iPropertyKind != ITEM_PROPERTY_KIND_VOICE_SETTING_ITEM )
	{
		iResult = RETURN_CODE_VOICE_SETTING_INFO_DATE_ERROR;
		PacketComposer.Add( iResult );
		pFSClient->Send( &PacketComposer );
		return;
	}

	if ( OPCode == 1 )
	{
		int iVoiceNum = 0;
		int iUserVoiceNum = 0;
		int iCurrnetVoiceIndex = -1;
		int iCurrnetActionCode = VOICE_ACTION_CODE_DEFAULT;
		vector<SVoiceConfig*> pVoiceConfig;
		vector<SVoiceUserSettingInfo*> pVoiceUserSettingInfo;
		pVoiceConfig.clear();
		pVoiceUserSettingInfo.clear();
		pItemShop->GetVoiceConfig( pItem->iItemCode, -1/*All*/, pUser->GetCurUsedAvatarSex(), pUser->GetCurUsedAvtarLv(), pVoiceConfig );
		pUserItemList->GetVoiceUserSettingInfo( pVoiceUserSettingInfo );

		iVoiceNum = pVoiceConfig.size();
		iUserVoiceNum = pVoiceUserSettingInfo.size();
		if ( iVoiceNum == 0 )
		{
			iResult = RETURN_CODE_VOICE_SETTING_INFO_DATE_ERROR;
			PacketComposer.Add( iResult );
			pFSClient->Send( &PacketComposer );
			return;
		}

		PacketComposer.Add( iResult );
		PacketComposer.Add( iVoiceNum );
		for ( int i = 0; i <iVoiceNum; i++ )
		{
			PacketComposer.Add( pVoiceConfig[i]->iVoiceIndex );
		}

		PacketComposer.Add( iUserVoiceNum );
		for ( int i = 0; i < iUserVoiceNum; i++ )
		{
			PacketComposer.Add( pVoiceUserSettingInfo[i]->iActionCode );
		}

		vector<SVoiceUserSettingInfo*> vCurrentSettingInfo;
		pUserItemList->GetVoiceUserSettingInfo( vCurrentSettingInfo, iItemIdx );
		if ( vCurrentSettingInfo.size() == 1 )
		{
			iCurrnetVoiceIndex = vCurrentSettingInfo[0]->iVoiceIndex;
			iCurrnetActionCode = vCurrentSettingInfo[0]->iActionCode;
		}
		PacketComposer.Add( iCurrnetVoiceIndex );
		PacketComposer.Add( iCurrnetActionCode );

		pFSClient->Send( &PacketComposer );
	} 
	else if ( OPCode == 2 )
	{
		CFSGameODBC* pODBC =(CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
		CHECK_NULL_POINTER_VOID(pODBC);

		int iVoiceIndex = 0;
		int iVoiceActionCode = VOICE_ACTION_CODE_DEFAULT;

		m_ReceivePacketBuffer.Read(&iVoiceIndex);
		m_ReceivePacketBuffer.Read(&iVoiceActionCode);

		if ( iVoiceActionCode < VOICE_ACTION_CODE_DEFAULT || iVoiceActionCode >= VOICE_ACTION_CODE_LENGTH )
		{
			iResult = RETURN_CODE_VOICE_SETTING_INFO_DATE_ERROR;
			PacketComposer.Add( iResult );
			pFSClient->Send( &PacketComposer );
			return;
		}
		vector<SVoiceConfig*> pVoiceConfig;
		vector<SVoiceUserSettingInfo*> pVoiceUserSettingInfo;
		pVoiceConfig.clear();
		pVoiceUserSettingInfo.clear();
		pItemShop->GetVoiceConfig( pItem->iItemCode, iVoiceIndex, pUser->GetCurUsedAvatarSex(), pUser->GetCurUsedAvtarLv(), pVoiceConfig );

		if ( pVoiceConfig.size() != 1 )
		{
			iResult = RETURN_CODE_VOICE_SETTING_INFO_DATE_ERROR;
			PacketComposer.Add( iResult );
			pFSClient->Send( &PacketComposer );
			return;
		}

		if ( ODBC_RETURN_SUCCESS != pODBC->VOICE_UserSetVoiceInfo( pUser->GetGameIDIndex(), pItem->iItemIdx, pItem->iItemCode, iVoiceIndex, iVoiceActionCode, iResult) )
		{
			PacketComposer.Add( iResult );
			pFSClient->Send( &PacketComposer );
			return;
		}

		pUserItemList->SetVoiceInfo( pItem->iItemIdx, pItem->iItemCode, iVoiceIndex, iVoiceActionCode );

		PacketComposer.Add( iResult );
		pFSClient->Send( &PacketComposer );
	}
}


void CFSGameThread::Process_ChangeUseStyleReq(CFSGameClient* pFSClient)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC)

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iUseStyle = 0;
	int iPlace = 0;
	BOOL bResult = FALSE;

	m_ReceivePacketBuffer.Read(&iUseStyle);

	if( Clone_Index_Clone == pUser->CheckCloneCharacter_Index() )
	{
		iUseStyle += (CLONE_CHARACTER_DEFULAT_STYLE_INDEX-1);
	}

	if(iUseStyle > 0 && iUseStyle <= MAX_STYLE_COUNT)
	{
		if(ODBC_RETURN_SUCCESS == pGameODBC->AVATAR_ChangeUseStyleIndex(pUser->GetGameIDIndex(), iUseStyle))
		{
			bResult = pUser->SetUseStyleIndex(iUseStyle);
		}
	}

	CPacketComposer PacketComposer(S2C_CHANGE_USE_STYLE_RES);
	PacketComposer.Add(bResult);

	pUser->Send(&PacketComposer);
	pUser->SendChangeFaceNotice();
}

void CFSGameThread::Process_ChangeStyleNameReq(CFSGameClient* pFSClient)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC)

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iErrorCode = STYLE_NAME_CHANGE_ERROR_SUCCESS;
	char szStyleName[MAX_STYLE_NAME_LENGTH+1] = {0};

	m_ReceivePacketBuffer.Read((BYTE*)&szStyleName, MAX_STYLE_NAME_LENGTH+1);
	szStyleName[MAX_STYLE_NAME_LENGTH] = 0;

	if(FALSE == pUser->CheckGameID(szStyleName))
	{
		iErrorCode = STYLE_NAME_CHANGE_ERROR_NOT_VALID_NAME;
	}
	else
	{
		if(ODBC_RETURN_SUCCESS == pGameODBC->AVATAR_ChangeUseStyleName(pUser->GetGameIDIndex(), szStyleName))
		{
			pUser->ChangeUseStyleName(szStyleName);
			iErrorCode = STYLE_NAME_CHANGE_ERROR_SUCCESS;
		}
		else
		{
			iErrorCode = STYLE_NAME_CHANGE_ERROR_SP_FAIL;
		}
	}

	CPacketComposer PacketComposer(S2C_CHANGE_STYLE_NAME_RES);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add((BYTE*)pUser->GetUseStyleName(), MAX_STYLE_NAME_LENGTH+1);

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_ChangeUseStyleInItemShopReq(CFSGameClient* pFSClient)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC)

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItem = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItem)

	CAvatarItemList *pAvatarItemList = pUserItem->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList)

	SAvatarStyleInfo* pStyleInfo = pUser->GetStyleInfo();
	CHECK_NULL_POINTER_VOID(pStyleInfo)

	int iUseStyle = 0;
	int iPlace = 0;
	BOOL bResult = FALSE;
	int iStyleIndex = 0;

	m_ReceivePacketBuffer.Read(&iUseStyle);

	if( Clone_Index_Clone == pUser->CheckCloneCharacter_Index() )
	{
		iUseStyle += (CLONE_CHARACTER_DEFULAT_STYLE_INDEX-1);
	}

	if(iUseStyle > 0 && iUseStyle <= MAX_STYLE_COUNT)
	{
		if(TRUE == pUser->ChangeUseTryItemFeature(iUseStyle) )
		{
			pUser->SendAvatarInfo();
		}
	}
}

void CFSGameThread::Process_ChangeUseStyleInBagReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop)

	int iUseStyle = 0;
	int iPlace = 0;
	BOOL bResult = FALSE;

	m_ReceivePacketBuffer.Read(&iUseStyle);

	if( Clone_Index_Clone == pUser->CheckCloneCharacter_Index() )
	{
		iUseStyle += (CLONE_CHARACTER_DEFULAT_STYLE_INDEX-1);
	}

	if(iUseStyle > 0 && iUseStyle <= MAX_STYLE_COUNT)
	{
		pUser->SendMyItemBag(pItemShop, iUseStyle);
	}
}

void CFSGameThread::Process_SpecialPartsInfoReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iUseStyle = 0, iSpecialPartsIndex = 0, iStatNum = 0, iSpecialPartsCount = 0, iPropertyIndex = 0; 
	BYTE btEquipCount = 0;
	m_ReceivePacketBuffer.Read(&iUseStyle);

	map<int, BYTE> mapEquipCount;
	for(int i = 0; i < MAX_SPECIAL_PARTS_INDEX_COUNT; ++i)
	{
		m_ReceivePacketBuffer.Read(&iSpecialPartsIndex); // ������ ���������� ����/����
		m_ReceivePacketBuffer.Read(&btEquipCount);

		if(iSpecialPartsIndex > -1)
			mapEquipCount.insert(map<int, BYTE>::value_type(iSpecialPartsIndex, btEquipCount));
	}

	// ������ �����ɷ�ġ ����
	vector<int> vProperty;
	map<int, vector<int>> mapProperty;
	for(int i = 0; i < MAX_SPECIAL_PARTS_USE_PROPERTY_INDEX_COUNT; ++i)
	{
		m_ReceivePacketBuffer.Read(&iSpecialPartsIndex);

		if(iSpecialPartsIndex == -1)
			continue;

		vector<int> vProperty;
		for(int j = 0; j < MAX_SPECIAL_PARTS_PROPERTYINDEX_COUNT; ++j)
		{
			m_ReceivePacketBuffer.Read(&iPropertyIndex);

			if(iPropertyIndex == -1)
				continue;

			map<int, vector<int>>::iterator iter = mapProperty.find(iSpecialPartsIndex);
			if(iter != mapProperty.end())
			{
				vProperty = iter->second;
				vProperty.push_back(iPropertyIndex);
				mapProperty[iSpecialPartsIndex] = vProperty;
			}
			else
			{
				vProperty.push_back(iPropertyIndex);
				mapProperty.insert(map<int, vector<int>>::value_type(iSpecialPartsIndex, vProperty));
			}			
		}
	}

	pUser->SendSpecialPartsInfo(mapEquipCount, mapProperty);
}

void CFSGameThread::Process_SpecialPartsStatApplyReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iSpecialPartsNum = 0;
	int	iSpecialPartsIndex = -1;

	int iPropertyCheck[MAX_STAT_TYPE_COUNT] = {0};

	m_ReceivePacketBuffer.Read(&iSpecialPartsNum);

	for(int i = 0; i < iSpecialPartsNum; ++i)
	{
		iSpecialPartsIndex = -1;
		int iStatNum = 0;
		int iProperty = PROPERTY_NONE;
		vector<int> vecProperty;
		m_ReceivePacketBuffer.Read(&iSpecialPartsIndex);
		m_ReceivePacketBuffer.Read(&iStatNum);

		for(int j = 0; j < iStatNum; ++j)
		{
			m_ReceivePacketBuffer.Read(&iProperty);

			if(iProperty > -1 && iProperty < MAX_STAT_TYPE_COUNT)
			{
				++iPropertyCheck[iProperty];
			}
			else if(PROPERTY_ALL_STAT == iProperty)	// ���ɷ�ġ
			{
				for(int i = 0; i < MAX_STAT_TYPE_COUNT ; i++)
				{
					++iPropertyCheck[i];
				}
			}
			else
			{
				continue;
			}

			vecProperty.push_back(iProperty);
		}

		for(int j = 0; j < MAX_STAT_TYPE_COUNT; ++j)
		{
			if(iPropertyCheck[j] > 1)
			{
				return; // �ߺ� �ɷ�ġ ���� �õ�
			}
		}

		pUser->UpdateSpecialPartsUserProperty(iSpecialPartsIndex, vecProperty);
	}

	CPacketComposer PacketComposer(S2C_SPECIAL_PARTS_STAT_APPLY_RES);
	pUser->Send(&PacketComposer);
	pUser->SendUserStat();
}

void CFSGameThread::Process_FSIPremiumStat(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	
	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);
	
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	int iItemCode, iSendType;
	
	m_ReceivePacketBuffer.Read(&iSendType);
	m_ReceivePacketBuffer.Read(&iItemCode);
	
	SShopItemInfo ShopItem;
	if (FALSE == pItemShop->GetItem(iItemCode, ShopItem))
		return;
	
	CPacketComposer PacketComposer(S2C_ITEM_PREMIUM_INFO_RES);
	
	int iPropertyValue = 0;
	
	PacketComposer.Add(iSendType);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(ShopItem.iSexCondition);
	PacketComposer.Add(ShopItem.iLvCondition);
	PacketComposer.Add(ShopItem.iPropertyType);
	
	int iPropertyKind = ShopItem.iPropertyKind;
	CHECK_CONDITION_RETURN_VOID(iPropertyKind < MIN_PREMIUM_STAT_OPTION_ITEM_NUM || MAX_PREMIUM_STAT_OPTION_ITEM_NUM < iPropertyKind);

	int iUserLv = pUser->GetCurUsedAvtarLv();
			
	CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList( iPropertyKind );
	CHECK_NULL_POINTER_VOID(pItemPropertyBoxList);
			
	int iMaxBoxIdx = pItemPropertyBoxList->GetMaxBoxIdxCount(iUserLv, PROPERTY_ASSIGN_TYPE_NONE );
	int iSendPropertyKind = iMaxBoxIdx;
	PacketComposer.Add(iSendPropertyKind);
	PacketComposer.Add(iMaxBoxIdx);
			
	for( int iBoxIdx = 0; iBoxIdx < iMaxBoxIdx ; iBoxIdx++ )
	{
		int iMaxCategoryCount = pItemPropertyBoxList->GetMaxCategoryCount(iBoxIdx, iUserLv, PROPERTY_ASSIGN_TYPE_NONE );
		PacketComposer.Add(&iMaxCategoryCount);
				
		vector< SItemPropertyBox > vItemPropertyBox;
		short sSmallMaxNum = (short)pItemPropertyBoxList->GetItemPropertyBoxIdxCount(iBoxIdx, iUserLv, PROPERTY_ASSIGN_TYPE_NONE);
		pItemPropertyBoxList->GetItemPropertyBox(iBoxIdx, iUserLv, PROPERTY_ASSIGN_TYPE_NONE, vItemPropertyBox );
				
		sort( vItemPropertyBox.begin(), vItemPropertyBox.end(), PropertyBoxSort() );
				
		sSmallMaxNum++;
		PacketComposer.Add((PBYTE)&sSmallMaxNum, sizeof(short));
				
		short	sPriceType = -1;
		int		iDontChooseNum = 1;
		int		iDontChooseSendPropertyIndex	= -1;
		short	sDontChooseSendCategory			= -1;
		short	sDontChooseSendProperty			= -1;
		int		iDontChooseSendValue			= -1;
		int		iDontChooseSendOrder			= -1;
				
		PacketComposer.Add((PBYTE)&sPriceType, sizeof(short));
		PacketComposer.Add(&iDontChooseNum);
		PacketComposer.Add(&iDontChooseSendPropertyIndex);
		PacketComposer.Add((PBYTE)&sDontChooseSendCategory, sizeof(short));
		PacketComposer.Add((PBYTE)&sDontChooseSendProperty, sizeof(short));
		PacketComposer.Add(&iDontChooseSendValue);
		PacketComposer.Add(&iDontChooseSendOrder);
				
		for( int i = 0; i < vItemPropertyBox.size() ; i++ )
		{
			int iPropertyIndex = vItemPropertyBox[i].iPropertyIndex;
			CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList( iPropertyIndex );
			int iMaxPropertyNum = pItemProperty->GetPropertyMaxCount();
					
			SItemPropertyBox* pItemPropertyBox = pItemPropertyBoxList->GetItemPropertyBox(vItemPropertyBox[i].iCategory, iBoxIdx, iUserLv, PROPERTY_ASSIGN_TYPE_NONE );
			CHECK_NULL_POINTER_VOID(pItemPropertyBox);
					
			short sPriceType			= 2; //(short)( pItemPropertyBox->iPriceType - 1);
			PacketComposer.Add((PBYTE)&sPriceType, sizeof(short));
			PacketComposer.Add(&iMaxPropertyNum);
					
			for( int iPropertyCount = 0; iPropertyCount < iMaxPropertyNum ; iPropertyCount++ )
			{
				vector< SItemProperty > vItemProperty;
				pItemProperty->GetItemProperty( vItemProperty );
				sort( vItemProperty.begin(), vItemProperty.end(), PropertySort() );
						
				int iSendPropertyIndex	= vItemProperty[iPropertyCount].iPropertyIndex;
				short sSendCategory		= (short)vItemPropertyBox[i].iCategory;
				short sSendProperty		= (short)vItemProperty[iPropertyCount].iProperty;
				int iSendValue			= vItemProperty[iPropertyCount].iValue;
				int iSendOrder			= vItemProperty[iPropertyCount].iOrder;
						
				PacketComposer.Add(&iSendPropertyIndex);
				PacketComposer.Add((PBYTE)&sSendCategory, sizeof(short));
				PacketComposer.Add((PBYTE)&sSendProperty, sizeof(short));
				PacketComposer.Add(&iSendValue);
				PacketComposer.Add(&iSendOrder);
			}
		}
	}
	
	pFSClient->Send(&PacketComposer);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EPORTSCARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	int iItemCode = -1;
	m_ReceivePacketBuffer.Read(&iItemCode);

	pUser->CheckExpiredEventCoin();

	CPacketComposer	Packet(S2C_EPORTSCARD_INFO_RES);
	int iErrorCode = GET_RANDOMITEM_SUCESS;

	if( OPEN == ATTENDANCEITEMSHOP.IsOpen() &&
		TRUE == ATTENDANCEITEMSHOP.CheckAttendanceEventItemCode(iItemCode))
	{
		Packet.Add(iErrorCode);
		if(FALSE == RANDOMCARDSHOP.MakeRandomCardInfo(ATTENDANCEITEMSHOP.GetAttenDanceRealItemCode(iItemCode), iItemCode , Packet))
		{
			Packet.Initialize(S2C_EPORTSCARD_INFO_RES);
			iErrorCode = GET_RANDOMITEM_NOT_EXIST_RANDOMITEM;
			Packet.Add(iErrorCode);
			pUser->Send(&Packet);
			return;
		}
		Packet.Add(0); //guarantee
		Packet.Add(0); //guarantee price
		Packet.Add(0); //randomcard ticket
		pUser->Send(&Packet);
		return;
	}

	if(6 == pAvatar->iCharacterType || 
		(403 <= pAvatar->iSpecialCharacterIndex && 406 >= pAvatar->iSpecialCharacterIndex)) 
	{
		if(false == pItemShop->CheckNotNewCharBuyItem(iItemCode))
		{
			iErrorCode = GET_RANDOMITEM_NOT_NEWCHAR_CONDITON;
			Packet.Add(iErrorCode);
			pUser->Send(&Packet);
			return;
		}
	}

	SShopItemInfo ItemInfo;
	if(FALSE == pItemShop->GetItem(iItemCode, ItemInfo))
	{
		iErrorCode = GET_RANDOMITEM_NOT_EXIST_RANDOMITEM;
		Packet.Add(iErrorCode);
		pUser->Send(&Packet);
		return;
	}
	
	if(ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_EVENT == ItemInfo.iSmallKind)
	{ 
		if(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PLUSCARD_EVENT))
		{
			iErrorCode = BUY_RANDOMITEM_EVENT_CLOSED;
			Packet.Add(iErrorCode);
			pUser->Send(&Packet);
			return;
		}
	}

	Packet.Add(iErrorCode);
	if(FALSE == RANDOMCARDSHOP.MakeRandomCardInfo(ItemInfo.iItemCode0, Packet))
	{
		iErrorCode = GET_RANDOMITEM_NOT_EXIST_RANDOMITEM;
		Packet.Add(iErrorCode);
		pUser->Send(&Packet);
		return;
	}

	int iGuaranteeValue = -1;
	SGuaranteeInfo sGuaranteeInfo;
	BYTE btCategory = RANDOMCARDSHOP.GetRandomCardCategory(ItemInfo.iItemCode0);
	if(GUARANTEE_SUCCESS == RANDOMCARDSHOP.GetGuaranteeCardInfo(pUser->GetUserIDIndex(), sGuaranteeInfo))
	{
		pUser->SetGuaranteeCardInfo(sGuaranteeInfo);

		iGuaranteeValue = pUser->GetGuaranteeCardValue(btCategory);
	}
	else 
		iGuaranteeValue = GUARANTEECARD_NOGOT;
	
	Packet.Add(iGuaranteeValue);
	
	int iLimit, iPrice;
	if(TRUE == RANDOMCARDSHOP.GetGuaranteeCardLimitAndPrice(btCategory-1, iLimit, iPrice))
	{
		Packet.Add(iPrice);
	}
	else
	{
		Packet.Add(GUARANTEECARD_NOGOT);
	}

	CFSGameUserItem* pUserItemList = (CFSGameUserItem*)pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	Packet.Add(pUserItemList->GetRandomCardTicketCount((ITEM_SMALL_KIND)ItemInfo.iSmallKind, ItemInfo.iItemCode0));

	pFSClient->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EPORTSCARD_BUY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBCBase);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	int iOp =0;
	m_ReceivePacketBuffer.Read(&iOp);
	
	int iItemCode = -1;
	m_ReceivePacketBuffer.Read(&iItemCode);

	BOOL bUseRandomCardTicket = FALSE;
	m_ReceivePacketBuffer.Read(&bUseRandomCardTicket);

	CPacketComposer Packet(S2C_EPORTSCARD_BUY_RES);

	if( OPEN == ATTENDANCEITEMSHOP.IsOpen() &&
		TRUE == ATTENDANCEITEMSHOP.CheckAttendanceEventItemCode(iItemCode))
	{
		int iErr = 0;
		if( FALSE == pUser->BuyAttendanceRandomItem(iItemCode, iErr)) 
		{
			Packet.Add(iErr);
			pUser->Send(&Packet);
			return;
		}
		return;
	}

	SShopItemInfo ItemInfo;
	CHECK_CONDITION_RETURN_VOID(FALSE == pItemShop->GetItem(iItemCode, ItemInfo));

	int iErrorCode = BUY_RANDOMCARD_SUCCESS;

	CFSGameUserItem* pUserItemList = (CFSGameUserItem*)pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	int iRandomCardTicketItemIndex = pUserItemList->GetRandomCardTicketItemIndex((ITEM_SMALL_KIND)ItemInfo.iSmallKind, iItemCode);
	if(TRUE == bUseRandomCardTicket &&
		-1 == iRandomCardTicketItemIndex)
	{
		iErrorCode = BUY_RANDOMCARD_NOT_EXIST_RANDOMCARD_TICKET;
		Packet.Add(iErrorCode);
		pUser->Send(&Packet);
		return;
	}

	if((pAvatar->iSpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX1 ||
		pAvatar->iSpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX3) &&
		(ItemInfo.iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
	{
		iErrorCode = BUY_RANDOMCARD_MISMATCH_SPECIALCHARACTER_INDEX;
		Packet.Add(iErrorCode);
		pUser->Send(&Packet);
		return;
	}

	if(6 == pAvatar->iCharacterType || 
		(THREEKINGDOM_CHARACTER_SPECIALINDEX4 <= pAvatar->iSpecialCharacterIndex && THREEKINGDOM_CHARACTER_SPECIALINDEX7 >= pAvatar->iSpecialCharacterIndex)) 
	{
		if(false == pItemShop->CheckNotNewCharBuyItem(iItemCode))
		{
			iErrorCode = BUY_RANDOMCARD_MISMATCH_SPECIALCHARACTER_INDEX;
			Packet.Add(iErrorCode);
			pUser->Send(&Packet);
			return;
		}
	}

	int iMailType = MAIL_PRESENT_ON_AVATAR;

	if(ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_EVENT == ItemInfo.iSmallKind)
	{ 
		if(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_PLUSCARD_EVENT))
		{
			iErrorCode = BUY_RANDOMCARD_NOT_SELL_EVENT_CLOSED;
			Packet.Add(iErrorCode);
			pUser->Send(&Packet);
			return;
		}

		iMailType = MAIL_PRESENT_ON_ACCOUNT;
	}

	if(BUY_RANDOMCARD_SUCCESS != (iErrorCode = RANDOMCARDSHOP.CheckBuyRandomCard(pItemShop, ItemInfo.iItemCode0, pUser->GetCurUsedAvatarSex(),
		pUser->GetCurUsedAvtarLv(), pUser->GetCoin()+pUser->GetEventCoin(), pUser->GetSkillPoint(), pUser->GetUserTrophy(), pUser->GetPresentList(iMailType)->GetSize(), bUseRandomCardTicket)))
	{
		Packet.Add(iErrorCode);
		pUser->Send(&Packet);
		return;
	}

	SBillingInfo BillingInfo;
	pUser->SetUseCashStatus(TRUE);
	BillingInfo.iEventCode = EVENT_BUY_RANDOMCARD_ITEM;	
	BillingInfo.pUser = pUser;
	memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
	BillingInfo.iSerialNum = pUser->GetUserIDIndex();
	memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH + 1);	
	memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1);
	memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;

	char* szItemName = pItemShop->GetItemTextCheckAndReturnEventCodeDescription(iItemCode, BillingInfo.iEventCode);
	if(NULL == szItemName)
		printf(BillingInfo.szItemName, "EportsCard_Item");
	else
		memcpy(BillingInfo.szItemName, szItemName, MAX_ITEMNAME_LENGTH + 1);	

	BillingInfo.iItemCode = iItemCode;
	BillingInfo.iSellType = RANDOMCARDSHOP.GetRandomCardSellType(ItemInfo.iItemCode0);
	BillingInfo.iCurrentCash = pUser->GetCoin();
	int iPrice = RANDOMCARDSHOP.GetRandomCardSellPrice(ItemInfo.iItemCode0);
	if(BillingInfo.iSellType == SELL_TYPE_CASH)
		BillingInfo.iCashChange = iPrice;
	else if(BillingInfo.iSellType == SELL_TYPE_POINT)
		BillingInfo.iPointChange= iPrice;

	BillingInfo.iParam0 = pUser->GetGameIDIndex();
	BillingInfo.iParam1 = RANDOMCARDSHOP.GetRandomCardCategory(ItemInfo.iItemCode0);
	BillingInfo.iParam2 = ItemInfo.iLvCondition;
	BillingInfo.iParam3 = ItemInfo.iItemCode0;

	BillingInfo.bUseRandomCardTicket = bUseRandomCardTicket;
	BillingInfo.iParam4 = iRandomCardTicketItemIndex;

	if(FALSE == bUseRandomCardTicket)
		BillingInfo.iItemPrice = iPrice;

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
		Packet.Add(iErrorCode);
		pUser->Send(&Packet);
		return;
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GUARANTEE_BUY_REQ)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC)

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	int iGuaranteeCardType = -1;
	m_ReceivePacketBuffer.Read(&iGuaranteeCardType);

	CPacketComposer PacketComposer(S2C_GUARANTEE_BUY_RES);
	int iErrorCode = BUY_GUARANTEECARD_DB_FAIL;

	SGuaranteeInfo sGuaranteeInfo;
	int iUserIDIndex = pUser->GetUserIDIndex();
	if(GUARANTEE_SUCCESS == RANDOMCARDSHOP.GetGuaranteeCardInfo(iUserIDIndex, sGuaranteeInfo))
	{
		pUser->SetGuaranteeCardInfo(sGuaranteeInfo);

		int iLimit = -1, iPrice = -1;
		if( FALSE == RANDOMCARDSHOP.GetGuaranteeCardLimitAndPrice(iGuaranteeCardType,iLimit,iPrice))
		{
			g_LogManager.WriteLogToFile("Failed GetGuaranteeCardLimitAndPrice UserIDIndex 11866902", iUserIDIndex);
rorCode = BUY_GUARANTEECARD_DB_FAIL;
			PacketComposer.Add(iErrorCode);	
			pUser->Send(&PacketComposer);
			return;
		}
		
		int iHotDealItemCode = 0;
		if(OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_HOTDEAL_EVENT) &&
			NEXUS_LOBBY == pFSClient->GetState()) // �̺�Ʈ �Ⱓ�� �˾��� ���ؼ��� ���� ����.
		{
			int iHotDealSalePrice = 0;
			int iPropertyKind = RANDOMCARDSHOP.GetGuaranteeCardPropertyKind(iGuaranteeCardType);
			
			if(TRUE == TODAYHOTDEALManager.GetSalePriceByPropertyKind(iPropertyKind, iHotDealSalePrice, iHotDealItemCode))
			{
				iPrice = iHotDealSalePrice;
			}
		}

		if(OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_SALE_RANDOMITEM) &&
			NEXUS_LOBBY == pFSClient->GetState()) // �̺�Ʈ �Ⱓ�� �˾��� ���ؼ��� ���� ����.
		{
			if(FALSE == pUser->GetUserSaleRandomItemEvent()->CanBuyStatus())
			{
				iErrorCode = BUY_GUARANTEECARD_ERROR;
				PacketComposer.Add(iErrorCode);	
				pUser->Send(&PacketComposer);
				return;
			}

			int iSalePrice;
			if(0 == (iSalePrice = SALERANDOMITEMEVENT.GetSalePrice(pUser->GetUserSaleRandomItemEvent()->GetCurrentRewardIndex())))
			{
				iErrorCode = BUY_GUARANTEECARD_ERROR;
				PacketComposer.Add(iErrorCode);	
				pUser->Send(&PacketComposer);
				return;
			}

			iPrice = iSalePrice;
		}

		int iCoin = pUser->GetCoin() + pUser->GetEventCoin();
		if(iCoin < iPrice)
		{
			iErrorCode = BUY_GUARANTEECARD_NOT_ENOUGH_CASH;
			PacketComposer.Add(iErrorCode);	
			pUser->Send(&PacketComposer);
			return;
		}

		SBillingInfo BillingInfo;
		pUser->SetUseCashStatus(TRUE);
		BillingInfo.iEventCode = EVENT_BUY_GUARANTEECARD_ITEM;	
		BillingInfo.pUser = pUser;
		memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
		BillingInfo.iSerialNum = pUser->GetUserIDIndex();
		memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH + 1);	
		
		memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1);
		memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
		BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;

		BillingInfo.iItemCode = iGuaranteeCardType;

		const int cnGuaranteeCodeBase = 10000;
		char* szItemName = pItemShop->GetItemTextCheckAndReturnEventCodeDescription(cnGuaranteeCodeBase + iGuaranteeCardType, BillingInfo.iEventCode);
		if( NULL == szItemName )
			printf(BillingInfo.szItemName, "GuaranteeCard");
		else
			memcpy( BillingInfo.szItemName, szItemName, MAX_ITEMNAME_LENGTH + 1 );	

		BillingInfo.iSellType = SELL_TYPE_CASH;
		BillingInfo.iCurrentCash = pUser->GetCoin();
		BillingInfo.iCashChange = iPrice;

		BillingInfo.iParam0 = pUser->GetGameIDIndex();
		BillingInfo.iParam1 = iGuaranteeCardType;
		BillingInfo.iParam2 = iLimit;
		BillingInfo.iParam3 = iHotDealItemCode;
		BillingInfo.iItemPrice = iPrice;

		if(OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_SALE_RANDOMITEM) &&
			NEXUS_LOBBY == pFSClient->GetState()) // �̺�Ʈ �Ⱓ�� �˾��� ���ؼ��� ���� ����.
		{
			BillingInfo.bSaleRandomItemEvent = TRUE;
			BillingInfo.iPropertyValue = 1;
		}

		if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
		{
			int iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			PacketComposer.Add(iErrorCode);
			pUser->Send(&PacketComposer);
		}
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, DB_DATA_LOAD, FAIL, "GuaranteeCardInfo - UserID:�4, GameID:(null), ErrorCode:18227200", 
etUserID(), pUser->GetGameID(), iErrorCode);	

		PacketComposer.Add(iErrorCode);	
		pUser->Send(&PacketComposer);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GUARANTEE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iGuaranteeCardType = -1;
	m_ReceivePacketBuffer.Read(&iGuaranteeCardType);

	int iLimit, iPrice;
	RANDOMCARDSHOP.GetGuaranteeCardLimitAndPrice(iGuaranteeCardType,iLimit,iPrice);

	CPacketComposer	Packet(S2C_GUARANTEE_INFO_RES);
	Packet.Add(iLimit);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_TOKEN_EXCHANGE_ITEM_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iItemIdx = -1;
	m_ReceivePacketBuffer.Read(&iItemIdx);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	CHECK_NULL_POINTER_VOID(pItem);
		
	if( pItem->iPropertyKind == ITEM_PROPERTY_KIND_TOKEN )
		CLUBCONFIGMANAGER.SendTokenExchangeItemList(pFSClient, iItemIdx, pItem->iItemCode);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_TOKEN_EXCHANGE_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	int iExchangeIndex = -1, iItemIdx = -1;
	m_ReceivePacketBuffer.Read(&iExchangeIndex);
	m_ReceivePacketBuffer.Read(&iItemIdx);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	CHECK_NULL_POINTER_VOID(pItem);

	CHECK_CONDITION_RETURN_VOID(pItem->iPropertyKind != ITEM_PROPERTY_KIND_TOKEN);

	CPacketComposer Packet(S2C_TOKEN_EXCHANGE_ITEM_RES);
	SS2C_TOKEN_EXCHANGE_ITEM_RES rs;

	if ( pUser->GetCurUsedAvtarLv() < 16 )
	{
		rs.btResult = RESULT_TOKEN_EXCHANGE_ITEM_ENOUGH_LV;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_TOKEN_EXCHANGE_ITEM_RES));
		pUser->Send(&Packet);
		return;
	}

	int iTokenCount = 0, iItemCode = -1, iValue = -1;
	if(FALSE == CLUBCONFIGMANAGER.GetTokenExchangeItem(pItem->iItemCode, iExchangeIndex, iTokenCount, iItemCode, iValue))
	{
		rs.btResult = RESULT_TOKEN_EXCHANGE_ITEM_FAILED;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_TOKEN_EXCHANGE_ITEM_RES));
		pUser->Send(&Packet);
		return;
	}

	if(pItem->iPropertyTypeValue < iTokenCount)
	{
		rs.btResult = RESULT_TOKEN_EXCHANGE_ITEM_ENOUGH_TOKEN;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_TOKEN_EXCHANGE_ITEM_RES));
		pUser->Send(&Packet);
		return;
	}

	SShopItemInfo ItemInfo;
	if(iItemCode != -1)
	{
		if(FALSE == pItemShop->GetItem(iItemCode, ItemInfo))
		{
			rs.btResult = RESULT_TOKEN_EXCHANGE_ITEM_FAILED;
			Packet.Add((PBYTE)&rs, sizeof(SS2C_TOKEN_EXCHANGE_ITEM_RES));
			pUser->Send(&Packet);
			return;
		}
	}

	CFSGameODBC* pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID(pODBC);

	int iUpdatedPropertyValue = -1;
	int iNewItemIdx = 0;
	time_t tExpireDate = 0;
	int iUpdatedHighFrequencyItemPropertyValue = -1;

	if(ODBC_RETURN_SUCCESS != pODBC->ITEM_TokenItemExchange(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), iItemIdx, iTokenCount, iItemCode, iValue, iUpdatedPropertyValue, iNewItemIdx, tExpireDate, iUpdatedHighFrequencyItemPropertyValue))
	{
		rs.btResult = RESULT_TOKEN_EXCHANGE_ITEM_FAILED;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_TOKEN_EXCHANGE_ITEM_RES));
		pUser->Send(&Packet);
		return;
	}

	if(ItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_TWELVE_PROPERTY_ACC)
	{
		pUser->RecvPresent(MAIL_PRESENT_ON_AVATAR, iNewItemIdx);
	}
	else if(ItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_BINGO_TATOO)
	{
		CUserHighFrequencyItem* pHighFrequencyItem = pUser->GetUserHighFrequencyItem();
		if ( pHighFrequencyItem != NULL )
		{
			SUserHighFrequencyItem sUserHighFrequencyItem;
			sUserHighFrequencyItem.iPropertyKind = ItemInfo.iPropertyKind;
			sUserHighFrequencyItem.iPropertyTypeValue = iUpdatedHighFrequencyItemPropertyValue;
			pHighFrequencyItem->AddUserHighFrequencyItem( sUserHighFrequencyItem );
			pUser->SendHighFrequencyItemCount(ITEM_PROPERTY_KIND_BINGO_TATOO);
		}
	}
	else if(iItemCode == -1)
	{
		pUser->AddSkillPoint(iValue);
	}
	else
	{
		SUserItemInfo UserItemInfo;
		UserItemInfo.SetBaseInfo(ItemInfo);

		UserItemInfo.iItemIdx = iNewItemIdx;
		UserItemInfo.iItemCode = iItemCode;
		UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;
		UserItemInfo.iSellType = REFUND_TYPE_POINT;
		UserItemInfo.iPropertyTypeValue = iValue;
		UserItemInfo.iPropertyNum = 0;
		UserItemInfo.ExpireDate = tExpireDate;

		pAvatarItemList->AddItem(UserItemInfo);
	}

	if(pItem != NULL)
	{
		pItem->iPropertyTypeValue = iUpdatedPropertyValue;
		if(iUpdatedPropertyValue == 0)
			pItem->iStatus = ITEM_STATUS_RESELL;
	}

	rs.btResult = RESULT_TOKEN_EXCHANGE_ITEM_SUCCESS;
	Packet.Add((PBYTE)&rs, sizeof(SS2C_TOKEN_EXCHANGE_ITEM_RES));
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SECRETSTORE_ITEMLIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendSecretStoreInfoRes();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COMPOUNDINGITEM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iCompoundingItemCode = 0, iItemIdx = 0;
	m_ReceivePacketBuffer.Read(&iItemIdx);
	
	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	CHECK_NULL_POINTER_VOID(pItem);	
	
	iCompoundingItemCode = pItem->iItemCode;

	SComboundingItem_Config		sConfig;
	int iMaterialCount[MAX_MATERIALINDEX] = {0,};
	int iDelete_ItemIdx[MAX_MATERIALINDEX] = {0,};

	CHECK_CONDITION_RETURN_VOID( FALSE == pItemShop->GetComboundingItemConfig(iCompoundingItemCode,sConfig) );

	SRewardConfig* pReward = REWARDMANAGER.GetReward(sConfig.iCombineRewardIndex);
	CHECK_CONDITION_RETURN_VOID( NULL == pReward );
	
	SRewardConfigItem* pRewardItem = (SRewardConfigItem*)pReward;

	CPacketComposer	Packet(S2C_COMPOUNDINGITEM_INFO_RES);
	SS2C_COMPOUNDINGITEM_INFO_RES rs1;
	rs1.iCompoundingItemCode = 	pRewardItem->iItemCode;
	rs1.btCompoundingItem_Material_Count = static_cast<BYTE>(sConfig.iMaterialValue);
	rs1.iItemIdx = iItemIdx;
	Packet.Add((PBYTE)&rs1, sizeof(SS2C_COMPOUNDINGITEM_INFO_RES));

	for( int iIndex = 0 ; iIndex < sConfig.iMaterialValue && iIndex < MAX_MATERIALINDEX ; ++iIndex )
	{
		SS2C_COMPOUNDINGITEM_INFO_SECOND_RES	rs2;
		rs2.iItemCode = pItemShop->GetMaterialItemCode(sConfig.iMaterialIndex[iIndex]);
		rs2.btGetItem = (BOOL)pUser->CheckInventory_ComboundingItemWithItemCode(rs2.iItemCode,iMaterialCount[sConfig.iMaterialIndex[iIndex]], iDelete_ItemIdx[iIndex]);

		if( rs2.btGetItem == TRUE )
			iMaterialCount[sConfig.iMaterialIndex[iIndex]]++;
			
		Packet.Add((PBYTE)&rs2, sizeof(SS2C_COMPOUNDINGITEM_INFO_SECOND_RES));
	}
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COMPOUNDINGITEM_COMBINE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CAvatarItemList *pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iCompoundingItemCode = 0, iItemIdx = 0;
	m_ReceivePacketBuffer.Read(&iItemIdx);

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	CHECK_NULL_POINTER_VOID(pItem);	

	iCompoundingItemCode = pItem->iItemCode;
	
	SComboundingItem_Config		sConfig;
	CHECK_CONDITION_RETURN_VOID( FALSE == pItemShop->GetComboundingItemConfig(iCompoundingItemCode,sConfig) );
	CHECK_CONDITION_RETURN_VOID( ITEM_STATUS_INVENTORY != pItem->iStatus );

	SS2C_COMPOUNDINGITEM_COMBINE_RES rs;
	int iResult = pUser->ComboundingItem_Combine(iItemIdx, sConfig, rs);

	rs.btResult = (BYTE)iResult;
	CPacketComposer Packet(S2C_COMPOUNDINGITEM_COMBINE_RES);
	Packet.Add((BYTE*)&rs, sizeof(SS2C_COMPOUNDINGITEM_COMBINE_RES));
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_INFO_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SPECIALSKIN_INFO_REQ	sinfo;
	m_ReceivePacketBuffer.Read((BYTE*)&sinfo, sizeof(SC2S_SPECIALSKIN_INFO_REQ));

	CPacketComposer Packet(S2C_SPECIALSKIN_INFO_RES);
	Packet.Add(sinfo.iSkinCardIndex);
	SPECIALSKINManager.MakePacketForSpecialSkinItem(Packet, sinfo.iSkinCardIndex );

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_BUY_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SC2S_SPECIALSKIN_BUY_REQ	sInfo;
	m_ReceivePacketBuffer.Read((BYTE*)&sInfo, sizeof(SC2S_SPECIALSKIN_BUY_REQ));

	int iErrorCode = 0;
	if( FALSE == pUser->GetUserSpecialSkin()->BuySpecialSkinCard(iErrorCode, sInfo,pItemShop))
	{
		CPacketComposer Packet(S2C_SPECIALSKIN_BUY_RES);
		Packet.Add(static_cast<BYTE>(iErrorCode));
		pUser->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_INVENTORYLIST_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	int iGamePosition = pUser->GetCurUsedAvatarPosition();
	pUser->GetUserSpecialSkin()->SendUserSpecialSkinEquipList(iGamePosition, pUser->GetCurUsedAvatarSpecialCharacterIndex());
	pUser->GetUserSpecialSkin()->SendUserSpecialSkinInventoryList(iGamePosition);	
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_EQUIP_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SPECIALSKIN_EQUIP_REQ	sInfo;
	m_ReceivePacketBuffer.Read((BYTE*)&sInfo, sizeof(SC2S_SPECIALSKIN_EQUIP_REQ));
	
	CPacketComposer	Packet(S2C_SPECIALSKIN_EQUIP_RES);
	pUser->GetUserSpecialSkin()->EquipSpecailSkin(Packet, sInfo.iSpecialSkinCode, sInfo.iInventoryIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_UNEQUIP_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SPECIALSKIN_UNEQUIP_REQ	sInfo;
	m_ReceivePacketBuffer.Read((PBYTE)&sInfo, sizeof( SC2S_SPECIALSKIN_UNEQUIP_REQ ));

	CPacketComposer	Packet(S2C_SPECIALSKIN_UNEQUIP_RES);
	pUser->GetUserSpecialSkin()->Un_EquipSpecailSkin(Packet, sInfo.iSpecialSkinCode, sInfo.iInventoryIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPECIALSKIN_MY_EQUIP_LIST_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_SPECIALSKIN_MY_EQUIP_LIST_RES);
	pUser->GetUserSpecialSkin()->MakePacketUser_EquipSpecialSKinList(Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_LIST_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CUSTOMIZE_ITEM_LIST_REQ sInfo;
	m_ReceivePacketBuffer.Read((PBYTE)&sInfo, sizeof( SC2S_CUSTOMIZE_ITEM_LIST_REQ ));

	CPacketComposer	Packet(S2C_CUSTOMIZE_ITEM_LIST_RES);

	S_ODBC_CustomizeLog	sLogInfo;
	if( TRUE == pUser->GetCustomizeItem_Received(sInfo.iIndex, sLogInfo) ) // �ִ��� üũ
	{
		WRITE_LOG_NEW(LOG_TYPE_CUSTOMIZE, GET_DATA, FAIL, "GetCustomizeItem_Received - GameIDIndex:11866902, Index:0", pUser->GetGameIDIndex(), sInfo.iIndex);
urn;
	}

	SS2C_CUSTOMIZE_ITEM_LIST_RES	rs;
	rs.iListCount = 0;
	PBYTE pbtCountLocation = Packet.GetTail();

	Packet.Add((PBYTE)&rs, sizeof(SS2C_CUSTOMIZE_ITEM_LIST_RES));
	rs.iListCount = CUSTOMIZEManager.MakePacketCustomizeItemList(Packet, pUser->GetCurUsedAvatarSex());

	memcpy(pbtCountLocation, &rs.iListCount, sizeof(rs.iListCount));
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_REQ sInfo;
	m_ReceivePacketBuffer.Read((PBYTE)&sInfo, sizeof( SC2S_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_REQ ));

	CPacketComposer Packet(S2C_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_RES);
	SS2C_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_RES	rs;

	if( FALSE == CUSTOMIZEManager.MakePacketCustomizeItemSelectInfo( rs, Packet, sInfo.iItemCode ) )
	{
		Packet.Initialize(S2C_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_RES);
		rs.iResult = RESULT_CUSTOMIZE_ITEM_MAKE_DB_FAIL;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_CUSTOMIZE_ITEM_MAKE_ITEM_SELECT_RES));
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_MAKE_FINISH_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CUSTOMIZE_ITEM_MAKE_FINISH_REQ sInfo;
	m_ReceivePacketBuffer.Read((PBYTE)&sInfo, sizeof( SC2S_CUSTOMIZE_ITEM_MAKE_FINISH_REQ ));

	SS2C_CUSTOMIZE_ITEM_MAKE_FINISH_RES	rs;
	rs.iResult = pUser->CustomizeItem_InsertLog( sInfo.iItemCode, sInfo.iIndex, sInfo.iPropertyIndex, sInfo.btColorType );

	CPacketComposer	Packet(S2C_CUSTOMIZE_ITEM_MAKE_FINISH_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CUSTOMIZE_ITEM_MAKE_FINISH_RES));
	pUser->Send(&Packet);

	if( RESULT_CUSTOMIZE_ITEM_MAKE_SUCCESS == rs.iResult || 
		RESULT_CUSTOMIZE_ITEM_MAKE_ALERADY_USED == rs.iResult)
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if( NULL != pCenter )
		{
			CPacketComposer CenterPacket(G2S_CUSTOMIZE_MAKE_FINISH_REQ);
			CenterPacket.Add(sInfo.iItemCode);
			CenterPacket.Add(sInfo.btColorType);
			pCenter->Send(&CenterPacket);
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CUSTOMIZE_ITEM_CHECK_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CUSTOMIZE_ITEM_CHECK_REQ sInfo;
	m_ReceivePacketBuffer.Read((PBYTE)&sInfo, sizeof( SC2S_CUSTOMIZE_ITEM_CHECK_REQ ));

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID( pODBCBase );

	CPacketComposer Packet(S2C_CUSTOMIZE_ITEM_CHECK_RES);
	SS2C_CUSTOMIZE_ITEM_CHECK_RES rs;
	S_ODBC_CustomizeLog sLogInfo;

	if( TRUE == pUser->GetCustomizeItem_Received(sInfo.iIndex, sLogInfo))
	{
		rs.iResult = RESULT_CUSTOMIZE_ITEM_CHECK_SUCCESS;
		rs.iItemCode = sLogInfo._iItemCode;
		rs.shColorCount = static_cast<short>(CUSTOMIZEManager.GetCustomItemColorCount(rs.iItemCode));
		for( size_t i = 0 ; i < MAX_CUSTOMIZE_PROPERTYINDEX_NUM ; ++i )
			CUSTOMIZEManager.GetCustomItemPropertyValue(sLogInfo._iPropertyIndex[i], rs.shProperty[i] , rs.shValue[i]);

		Packet.Add((PBYTE)&rs, sizeof(SS2C_CUSTOMIZE_ITEM_CHECK_RES));

		CUSTOMIZEManager.MakePacketCustomizeItemColor_ItemCode( Packet, sLogInfo._iItemCode , sLogInfo._btColor);
	}
	else
	{
		if( ODBC_RETURN_SUCCESS != pODBCBase->CUSTOMIZE_GetUserLog(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), sInfo.iIndex, sLogInfo))
		{
			WRITE_LOG_NEW(LOG_TYPE_CUSTOMIZE, EXEC_FUNCTION, FAIL, "pODBCBase->CUSTOMIZE_GetUserLog - GameIDIndex:11866902, Index:0", pUser->GetGameIDIndex(), sInfo.iIndex);
.iResult = RESULT_CUSTOMIZE_ITEM_CHECK_DB_FAIL;
			Packet.Add((PBYTE)&rs, sizeof(SS2C_CUSTOMIZE_ITEM_CHECK_RES));
		}
		else
		{
			rs.iResult = RESULT_CUSTOMIZE_ITEM_CHECK_SUCCESS;
			rs.iItemCode = sLogInfo._iItemCode;
			rs.shColorCount = static_cast<short>(CUSTOMIZEManager.GetCustomItemColorCount(rs.iItemCode));
			for( size_t i = 0 ; i < MAX_CUSTOMIZE_PROPERTYINDEX_NUM ; ++i )
				CUSTOMIZEManager.GetCustomItemPropertyValue(sLogInfo._iPropertyIndex[i], rs.shProperty[i] , rs.shValue[i]);

			Packet.Add((PBYTE)&rs, sizeof(SS2C_CUSTOMIZE_ITEM_CHECK_RES));

			CUSTOMIZEManager.MakePacketCustomizeItemColor_ItemCode( Packet, sLogInfo._iItemCode, sLogInfo._btColor );
		}
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLONE_CHARACTER_CHANGE_INVENTORY_REQ)
{
	CFSGameUser* pUser = dynamic_cast<CFSGameUser*>(pFSClient->GetUser());
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarStyleInfo* pStyleInfo = pUser->GetStyleInfo();
	CHECK_NULL_POINTER_VOID( pStyleInfo );

	CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID( pODBCBase );

	SC2S_CLONE_CHARACTER_CHANGE_INVENTORY_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_CLONE_CHARACTER_CHANGE_INVENTORY_REQ));

	if( TRUE == pUser->GetCloneCharacter() )
	{
		CHECK_CONDITION_RETURN_VOID( TRUE == pUser->GetChangeofCharacter() );
		pUser->SetChangeofCharacter(TRUE);
		Clone_Index eIndex = CLONE_CHARACTER_DEFULAT_STYLE_INDEX > pStyleInfo->iUseStyle ? Clone_Index_Clone : Clone_Index_Mine ;// �Ųٷ� �ٲ���� ��.
	
		SAvatarInfo* pAvatarInfo = pUser->ChangeCloneAvatar(eIndex);
		if( NULL != pAvatarInfo )
		{		
			if( TRUE == rs.btWantShopItemList )		// 1
				pUser->SendAllItemInfo(pAvatarInfo->iSex, TRUE); // ���������� �ȹ޾Ҵٸ� ���� ������.

			int iUseStyle = pUser->GetCloneCharacterStyleIndex(eIndex);

			pUser->GetUserItemList()->ChangeBasicItem_Sex(pAvatarInfo->iSex);
			pUser->ChangeCloneAvatarDiffSexItem(pAvatarInfo->iSex);
			if(ODBC_RETURN_SUCCESS == pODBCBase->AVATAR_ChangeUseStyleIndex(pUser->GetGameIDIndex(), iUseStyle))
			{
				pUser->SetUseStyleIndex(iUseStyle);
			}
			pUser->RemoveChangeDressItem();

			if( PCROOM_KIND_PREMIUM == pUser->GetPCRoomKind())
				pUser->LoadPCRoomItemList(TRUE);

			pUser->GetUserItemList()->BackUpUseFeatureInfo();
			pUser->SendAvatarInfo();
			pUser->SendUserItemList(rs.iInventoryKind, rs.iBigKind, rs.iSmallKind, rs.iPage);
			pUser->SendUseStyleAndNameInfo();

			map<int, BYTE> mapEquipCount;
			map<int, vector<int>> mapProperty;
			pUser->SendSpecialPartsInfo(mapEquipCount, mapProperty);
			pUser->SendUserStat();
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_AVATAR, INVALED_DATA, CHECK_FAIL, "CloneChange_Inventory GameIDIndex:11866902  StyleIndex:0" , pUser->GetGameIDIndex(), pStyleInfo->iUseStyle);

		pUser->SetChangeofCharacter(FALSE);
	}
	else
	{
		BOOL bResult = FALSE;

		SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
		CHECK_NULL_POINTER_VOID(pAvatarInfo);

		CAvatarCreateManager* pAvatarCreateManager = &AVATARCREATEMANAGER;
		CHECK_NULL_POINTER_VOID(pAvatarCreateManager);

		SSpecialAvatarConfig* pAvatarConfig = pAvatarCreateManager->GetSpecialAvatarConfigByIndex(pAvatarInfo->iSpecialCharacterIndex);
		CHECK_NULL_POINTER_VOID(pAvatarConfig);

		int iStyleCount = pAvatarConfig->GetMultipleCharacterCount();
		CHECK_CONDITION_RETURN_VOID( 2 > iStyleCount ); // �ߴ��� �ƴϿ� 

		int iUseStyle = 1;

		if( /*Clone_Index_Clone*/ Clone_Index_Mine == pUser->CheckCloneCharacter_Index() ) // �ߴ����ϰ�� �ε����� ������ ��� �Ѵ�. �Ͽ� �Ųٷ� ��
		{
			iUseStyle += (CLONE_CHARACTER_DEFULAT_STYLE_INDEX-1);
		}

		if(iUseStyle > 0 && iUseStyle <= MAX_STYLE_COUNT)
		{
			if(ODBC_RETURN_SUCCESS == pODBCBase->AVATAR_ChangeUseStyleIndex(pUser->GetGameIDIndex(), iUseStyle))
			{
				bResult = pUser->SetUseStyleIndex(iUseStyle);
			}
		}

		pUser->SendAvatarInfo();
		pUser->SendUserItemList(rs.iInventoryKind, rs.iBigKind, rs.iSmallKind, rs.iPage);
		pUser->SendUseStyleAndNameInfo();

		map<int, BYTE> mapEquipCount;
		map<int, vector<int>> mapProperty;
		pUser->SendSpecialPartsInfo(mapEquipCount, mapProperty);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_FACE_CHANGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatarInfo);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	SC2S_CHARACTER_FACE_CHANGE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_CHARACTER_FACE_CHANGE_REQ));

	SSpecialMultipleFaceConfig Config;
	if(FALSE == AVATARCREATEMANAGER.GetMultipleCharacterFaceConfig(pAvatarInfo->iSpecialCharacterIndex, rq.btFaceIndex, Config))
	{
		CHECK_CONDITION_RETURN_VOID(rq.btFaceIndex != MULTIPLE_FACE_INDEX_NONE);

		// �⺻ �󱼷� ����
		int iFaceItemIndex = pAvatarInfo->AvatarFeature.GetFaceItemIndex(pAvatarInfo->iSpecialCharacterIndex);
		CHECK_CONDITION_RETURN_VOID(iFaceItemIndex == -1);

		CHECK_CONDITION_RETURN_VOID(FALSE == pUser->GetUserItemList()->RemoveItemWithItemIdx(iFaceItemIndex))
			
		int iFaceItemCode = -1;
		int iFace = AVATARCREATEMANAGER.GetMultipleFaceCharacterFace(pAvatarInfo->iSpecialCharacterIndex, iFaceItemCode);
		CHECK_CONDITION_RETURN_VOID(iFace == -1);

		pAvatarInfo->iFace = iFace;
	}
	else
	{
		CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
		CHECK_NULL_POINTER_VOID(pUserItemList);

		CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
		CHECK_NULL_POINTER_VOID(pAvatarItemList);

		SUserItemInfo* pItem = pAvatarItemList->GetItemWithOnlyItemCode(Config._iFaceItemCode);
		CHECK_NULL_POINTER_VOID(pItem);	

		int iaRemoveItem[MAX_ITEMCHANNEL_NUM];
		memset(iaRemoveItem, -1, sizeof(int) * MAX_ITEMCHANNEL_NUM);
		int iErrorCode;
		CHECK_CONDITION_RETURN_VOID(FALSE == pUser->GetUserItemList()->UseItem(pItemShop, pItem->iItemIdx, iaRemoveItem, iErrorCode, -1, -1));

		int iFaceItemCode = pAvatarInfo->AvatarFeature.GetFaceItemCode(pAvatarInfo->iSpecialCharacterIndex);
		CHECK_CONDITION_RETURN_VOID(iFaceItemCode == -1);

		int iFace = AVATARCREATEMANAGER.GetMultipleFaceCharacterFace(pAvatarInfo->iSpecialCharacterIndex, iFaceItemCode);
		CHECK_CONDITION_RETURN_VOID(iFace == -1);

		pAvatarInfo->iFace = iFace;
	}

	SS2C_CHARACTER_FACE_STATUS_NOT rs;
	rs.btFaceIndex = rq.btFaceIndex;
	pUser->Send(S2C_CHARACTER_FACE_STATUS_NOT, &rs, sizeof(SS2C_CHARACTER_FACE_STATUS_NOT));

	pUser->GetUserItemList()->SetTempFace(pAvatarInfo->iFace);
	pUser->SendAvatarInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXPBOX_ITEM_USE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SC2S_EXPBOX_ITEM_USE_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EXPBOX_ITEM_USE_INFO_REQ));

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(rq.iItemIndex);
	CHECK_NULL_POINTER_VOID(pItem);
	CHECK_CONDITION_RETURN_VOID(pItem->iPropertyKind != ITEM_PROPERTY_KIND_EXP_BOX);

	vector< SUserItemProperty > vUserItemProperty;
	pUser->GetUserItemList()->GetCurAvatarItemList()->GetItemProperty(rq.iItemIndex, vUserItemProperty);

	CHECK_CONDITION_RETURN_VOID(vUserItemProperty.empty());

	SS2C_EXPBOX_ITEM_USE_INFO_RES rs;
	rs.iItemIndex = rq.iItemIndex;
	rs.iCurrentLv = pUser->GetCurUsedAvtarLv();
	rs.iCurrentExp = pUser->GetCurUsedAvtarExp();
	rs.iMinExp = pServer->GetLvUpExp(rs.iCurrentLv);
	rs.iMaxExp = pServer->GetLvUpExp(rs.iCurrentLv+1);
	rs.iAddExp = vUserItemProperty[0].iValue;
	pUser->Send(S2C_EXPBOX_ITEM_USE_INFO_RES, &rs, sizeof(SS2C_EXPBOX_ITEM_USE_INFO_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXPBOX_ITEM_USE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameUserItem* pUserItemList = pUser->GetUserItemList();
	CHECK_NULL_POINTER_VOID(pUserItemList);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SC2S_EXPBOX_ITEM_USE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EXPBOX_ITEM_USE_REQ));

	CPacketComposer Packet(S2C_EXPBOX_ITEM_USE_RES);
	SS2C_EXPBOX_ITEM_USE_RES rs;
	rs.iItemIndex = rq.iItemIndex;

	if(pUser->GetCurUsedAvtarLv() == GAME_LVUP_MAX)
	{
		rs.btResult = RESULT_EXPBOX_ITEM_USE_FAIL_MAX_LV;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_EXPBOX_ITEM_USE_RES));
		pUser->Send(&Packet);
		return;
	}

	if(TRUE == pUser->IsMatching())
	{
		rs.btResult = RESULT_EXPBOX_ITEM_USE_FAIL_MATCHING;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_EXPBOX_ITEM_USE_RES));
		pUser->Send(&Packet);
		return;
	}

	CAvatarItemList* pAvatarItemList = pUserItemList->GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(rq.iItemIndex);
	CHECK_NULL_POINTER_VOID(pItem);
	CHECK_CONDITION_RETURN_VOID(pItem->iPropertyKind != ITEM_PROPERTY_KIND_EXP_BOX);

	vector< SUserItemProperty > vUserItemProperty;
	pUser->GetUserItemList()->GetCurAvatarItemList()->GetItemProperty(rq.iItemIndex, vUserItemProperty);

	CHECK_CONDITION_RETURN_VOID(vUserItemProperty.empty());

	int iPreLv = pUser->GetCurUsedAvtarLv();
	int iUpdateLv = pUser->GetCurUsedAvtarLv();
	int iUpdateExp = pUser->GetCurUsedAvtarExp() + vUserItemProperty[0].iValue;
	while(GAME_LVUP_MAX > iUpdateLv && iUpdateExp >= pServer->GetLvUpExp(iUpdateLv+1))
	{
		iUpdateLv++;
	}

	if(iUpdateLv == GAME_LVUP_MAX)
	{
		iUpdateExp = pServer->GetLvUpExp(iUpdateLv);
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->ITEM_USE_ExpBox(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), rq.iItemIndex, iUpdateLv, iUpdateExp))
	{
		rs.btResult = RESULT_EXPBOX_ITEM_USE_FAIL;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_EXPBOX_ITEM_USE_RES));
		pUser->Send(&Packet);
		return;
	}

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	if(NULL != pAvatar)
	{
		pAvatar->iLv = iUpdateLv;
		pAvatar->iExp = iUpdateExp;
	}

	pUser->GetAvatarItemList()->RemoveItem(rq.iItemIndex);
	pUser->GetAvatarItemList()->RemoveItemProperty(rq.iItemIndex);	

	if ( iPreLv < iUpdateLv )
	{
		pUser->ProcessLevelUp(pUser->GetGameIDIndex(), pUser->GetCurUsedAvatarPosition(), iPreLv, iUpdateLv, iUpdateExp);
	}
	
	rs.btResult = RESULT_EXPBOX_ITEM_USE_SUCCESS;
	rs.iLv = iUpdateLv;
	rs.iExp = iUpdateExp;
	rs.iMinExp = pServer->GetLvUpExp(iUpdateLv);
	rs.iMaxExp = pServer->GetLvUpExp(iUpdateLv+1);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EXPBOX_ITEM_USE_RES));
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXCHANGE_NAME_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendExchangeNameAvatarList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXCHANGE_NAME_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EXCHANGE_NAME_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EXCHANGE_NAME_REQ));

	SS2C_EXCHANGE_NAME_RES rs;
	rs.btResult = pUser->ExchangeNameReq(rq);
	rs.iItemIndex = rq.iItemIndex;
	rs.iGameIDIndex1 = rq.iGameIDIndex1;
	rs.iGameIDIndex2 = rq.iGameIDIndex2;
	pUser->Send(S2C_EXCHANGE_NAME_RES, &rs, sizeof(SS2C_EXCHANGE_NAME_RES));

	if(RESULT_EXCHANGE_NAME_SUCCESS == rs.btResult)
	{
		pUser->UpdateEventRankMyAvatarNameExchange(rq.iGameIDIndex1, rq.iGameIDIndex2);
		
		// MiniGameZone Event
		int iMiniGameZoneMainGameIDIndex = pUser->GetUserMiniGameZoneEvent()->GetMainGameIDIndex();
		CODBCSvrProxy* pODBCSvr = dynamic_cast<CODBCSvrProxy*>(GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY));
		if( NULL != pODBCSvr &&
			OPEN == MINIGAMEZONE.IsOpen() && 
			(rq.iGameIDIndex1 == iMiniGameZoneMainGameIDIndex || rq.iGameIDIndex2 == iMiniGameZoneMainGameIDIndex) )
		{
			CPacketComposer Packet(S2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT);

			SMyAvatar	sAvatar;
			pUser->GetMyAvatarGameID(iMiniGameZoneMainGameIDIndex, sAvatar);

			SS2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT not;
			not.iUserIDIndex = pUser->GetUserIDIndex();
			not.btServerIndex = _GetServerIndex;
			not.iSeason = MINIGAMEZONE.GetCurrentSeason();
			strncpy_s(not.szGameID, _countof(not.szGameID), sAvatar.szGameID, MAX_GAMEID_LENGTH);	
			Packet.Add((BYTE*)&not, sizeof(SS2O_EVENT_MINIGAMEZONE_UPDATE_MAIN_GAMEID_NOT));

			pODBCSvr->Send(&Packet);
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PURCHASE_GRADE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPurchaseGrade()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PURCHASE_GRADE_GIVE_EVENT_COIN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPurchaseGrade()->GiveRewardEventCash();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECEIPT_USER_RECEIPT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserReceiptEvent()->SendReceiptInfoRes();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECEIPT_PAYBACK_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RECEIPT_PAYBACK_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_RECEIPT_PAYBACK_REQ));

	pUser->GetUserReceiptEvent()->PaybackEventCash(rq.iReceiptIdx);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHAGNE_CHAR_STAT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendChangeCharStatInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHAGNE_CHAR_STAT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHAGNE_CHAR_STAT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_CHAGNE_CHAR_STAT_REQ));

	SS2C_CHAGNE_CHAR_STAT_RES rs;
	rs.btResult = pUser->ChangeCharStatReq(rq);
	rs.iItemIndex = rq.iItemIndex;
	pUser->Send(S2C_CHAGNE_CHAR_STAT_RES, &rs, sizeof(SS2C_CHAGNE_CHAR_STAT_RES));
	pUser->SendUserStat();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MOVE_POTENCARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MOVE_POTENCARD_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_MOVE_POTENCARD_INFO_REQ));

	pUser->SendMovePotenCardInfo(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MOVE_POTENCARD_CHARACTER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MOVE_POTENCARD_CHARACTER_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_MOVE_POTENCARD_CHARACTER_INFO_REQ));

	pUser->SendMovePotenCardCharacterInfo(rq.iSelectCharacterPage);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MOVE_POTENCARD_SELECT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MOVE_POTENCARD_SELECT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_MOVE_POTENCARD_SELECT_REQ));

	pUser->UpdateMovePotenCard(rq);
}