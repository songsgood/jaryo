qinclude "stdafx.h"
qinclude "FSGameUserShoppingEvent.h"
qinclude "ShoppingEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserShoppingEvent::CFSGameUserShoppingEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_tMissionCheckTime(-1)
{
}

CFSGameUserShoppingEvent::~CFSGameUserShoppingEvent(void)
{
}

BOOL CFSGameUserShoppingEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == SHOPPING.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iaMaterialCount[MAX_SHOPPING_MISSION_MATERIAL_TYPE];
	ZeroMemory(iaMaterialCount, sizeof(int)*MAX_SHOPPING_MISSION_MATERIAL_TYPE);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_SHOPPING_GetUserData(m_pUser->GetUserIDIndex(), m_sUserInfo) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_SHOPPING_GetUserMaterialData(m_pUser->GetUserIDIndex(), iaMaterialCount))
	{
		WRITE_LOG_NEW(LOG_TYPE_SHOPPINGEVENT, INITIALIZE_DATA, FAIL, "EVENT_SHOPPING_GetUserData error");
		return FALSE;
	}

	time_t tCurrentTime = _time64(NULL);
	if(0 == m_sUserInfo.iUpdateDate)
	{
		m_sUserInfo.iCurrentSellCount = 0;
		m_sUserInfo.btRandomGuestType = SHOPPING.GetRandomGuestType();
		m_sUserInfo.iRemainSecond = SHOPPING.GetRemainSecond();

		SYSTEMTIME CurrentTime;
		TimetToSystemTime(tCurrentTime, CurrentTime);
		m_sUserInfo.iUpdateDate = TimetToYYYYMMDD(tCurrentTime);
		m_sUserInfo.iUpdateTime = CurrentTime.wHour;
	}

	for(int i = 0; i < MAX_SHOPPING_MISSION_MATERIAL_TYPE; ++i)
		m_iaMaterialCount[i] = iaMaterialCount[i];
	
	m_tMissionCheckTime = tCurrentTime;
	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserShoppingEvent::UpdateLoginTimeMission()
{
	BYTE btMaterialType = 0;
	int iAddMaterialCount = 0;
	CheckAndUpdateUserMission(SHOPPING_MISSION_TYPE_LOGIN_TIME, btMaterialType, iAddMaterialCount);
}

void CFSGameUserShoppingEvent::SendShoppingEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPING.IsOpen());

	ResetUserShoppingInfo();
	UpdateLoginTimeMission();

	SS2C_SHOPPING_EVENT_INFO_RES rs;
	for(BYTE btMissionType = SHOPPING_MISSION_TYPE_LOGIN_TIME; btMissionType < MAX_SHOPPING_MISSION_TYPE; ++btMissionType)
	{
		rs.sMissionInfo[btMissionType].iConditionValue = SHOPPING.GetConditionValue(btMissionType);
		rs.sMissionInfo[btMissionType].iCurrentValue = 0;
		if(SHOPPING_MISSION_TYPE_LOGIN_TIME == btMissionType)
		{
			time_t tCurrentTime = _time64(NULL);
			if((tCurrentTime - m_tMissionCheckTime) < m_sUserInfo.iRemainSecond)
				rs.sMissionInfo[btMissionType].iCurrentValue = m_sUserInfo.iRemainSecond - (tCurrentTime - m_tMissionCheckTime);
		}

		rs.sMissionInfo[btMissionType].btMaterialType = SHOPPING.GetMaterialType(btMissionType);
		rs.sMissionInfo[btMissionType].iMaterialCount = m_iaMaterialCount[rs.sMissionInfo[btMissionType].btMaterialType];
	}
	rs.iSellCount = m_sUserInfo.iCurrentSellCount;
	rs.iCurrentCoin = m_sUserInfo.iCurrentCoin;

	CPacketComposer Packet(S2C_SHOPPING_EVENT_INFO_RES);
	SHOPPING.MakePacketShoppingEventGuestInfo(m_sUserInfo.btRandomGuestType, m_sUserInfo.iCurrentSellCount, rs);
	SHOPPING.MakePacketShoppingEventInfo(Packet, rs);
	m_pUser->Send(&Packet);
}

void CFSGameUserShoppingEvent::SendShoppingEventRewardInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPING.IsOpen());

	if(TRUE == ResetUserShoppingInfo())
		SendShoppingEventInfo();

	CPacketComposer Packet(S2C_SHOPPING_EVENT_REWARD_INFO_RES);
	SHOPPING.MakePacketShoppingEventRewardInfo(Packet);
	m_pUser->Send(&Packet);
}

BOOL CFSGameUserShoppingEvent::CheckResetTime(time_t tCurrentTime /*= _time64(NULL)*/)
{
	TIMESTAMP_STRUCT CurrentDate;
	TimetToTimeStruct(tCurrentTime, CurrentDate);

	time_t tYesterTime = tCurrentTime - (60*60*24);

	int iYesterYYMMDD = TimetToYYYYMMDD(tYesterTime);
	int iYYMMDD = TimetToYYYYMMDD(tCurrentTime);

	int iHour = CurrentDate.hour;

	CHECK_CONDITION_RETURN(iHour < EVENT_RESET_HOUR && ((m_sUserInfo.iUpdateDate == iYesterYYMMDD && m_sUserInfo.iUpdateTime >= EVENT_RESET_HOUR) || m_sUserInfo.iUpdateDate >= iYYMMDD), FALSE);
	CHECK_CONDITION_RETURN(iHour >= EVENT_RESET_HOUR && ((m_sUserInfo.iUpdateDate == iYYMMDD && m_sUserInfo.iUpdateTime >= EVENT_RESET_HOUR) || m_sUserInfo.iUpdateDate > iYYMMDD), FALSE);

	return TRUE;
}

BOOL CFSGameUserShoppingEvent::ResetUserShoppingInfo()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == SHOPPING.IsOpen(), FALSE);

	time_t tCurrentTime = _time64(NULL);
	CHECK_CONDITION_RETURN(FALSE == CheckResetTime(tCurrentTime), FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, FALSE);

	BYTE btRandomGuestType = SHOPPING.GetRandomGuestType();
	int iRemainSecond = SHOPPING.GetRemainSecond();
	SYSTEMTIME CurrentTime;
	TimetToSystemTime(tCurrentTime, CurrentTime);
	int iUpdateDate = TimetToYYYYMMDD(tCurrentTime);
	int iUpdateHour = CurrentTime.wHour;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_SHOPPING_ResetUserMission(m_pUser->GetUserIDIndex(), btRandomGuestType, iRemainSecond, iUpdateDate, iUpdateHour), FALSE);
	
	m_sUserInfo.iCurrentSellCount = 0;
	m_sUserInfo.btRandomGuestType = btRandomGuestType;
	m_sUserInfo.iRemainSecond = iRemainSecond;
	m_sUserInfo.iUpdateDate = iUpdateDate;
	m_sUserInfo.iUpdateTime = iUpdateHour;
	ZeroMemory(m_iaMaterialCount, sizeof(int)*MAX_SHOPPING_MISSION_MATERIAL_TYPE);

	m_tMissionCheckTime = tCurrentTime;
	return TRUE;
}

void CFSGameUserShoppingEvent::SaveUserShoppingInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == SHOPPING.IsOpen());
	CHECK_CONDITION_RETURN_VOID(TRUE == ResetUserShoppingInfo());

	BYTE btMaterialType = 0;
	int iAddMaterialCount = 0;
	time_t tCurrentTime = _time64(NULL);
	CHECK_CONDITION_RETURN_VOID(TRUE == CheckAndUpdateUserMission(SHOPPING_MISSION_TYPE_LOGIN_TIME, btMaterialType, iAddMaterialCount, tCurrentTime));

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_VOID(pODBC);

	m_sUserInfo.iRemainSecond = m_sUserInfo.iRemainSecond - (tCurrentTime - m_tMissionCheckTime);
	if(m_sUserInfo.iRemainSecond < 0)
		m_sUserInfo.iRemainSecond = 0;

	pODBC->EVENT_SHOPPING_UpdateUserMission(m_pUser->GetUserIDIndex(), m_sUserInfo.iCurrentSellCount, m_sUserInfo.iCurrentCoin, m_sUserInfo.btRandomGuestType, 
		m_sUserInfo.iRemainSecond, m_sUserInfo.iUpdateDate, m_sUserInfo.iUpdateTime);
}

BOOL CFSGameUserShoppingEvent::CheckAndUpdateUserMission(BYTE btMissionType, BYTE& btMaterialType, int& iAddMaterialCount, time_t tCurrentTime)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == SHOPPING.IsOpen(), FALSE);

	ResetUserShoppingInfo();

	CHECK_CONDITION_RETURN(m_sUserInfo.iCurrentSellCount >= SHOPPING.GetMaxSellCount(), FALSE);

	btMaterialType = SHOPPING.GetMaterialType(btMissionType);
	iAddMaterialCount = SHOPPING.GetMaterialCount(btMissionType);
	CHECK_CONDITION_RETURN(btMaterialType >= MAX_SHOPPING_MISSION_MATERIAL_TYPE, FALSE);

	int iNeedCount = (SHOPPING.GetMaxSellCount() - m_sUserInfo.iCurrentSellCount) * SHOPPING.GetMaxMaterialCount(btMaterialType);
	CHECK_CONDITION_RETURN(m_iaMaterialCount[btMaterialType] >= iNeedCount, FALSE);

	BOOL bCheck = FALSE;
	int iRemainSecond = m_sUserInfo.iRemainSecond;
	if(SHOPPING_MISSION_TYPE_LOGIN_TIME == btMissionType)
	{
		iRemainSecond = m_sUserInfo.iRemainSecond - (tCurrentTime - m_tMissionCheckTime);
		if(iRemainSecond < 0)
			iRemainSecond = 0;

		if(0 == iRemainSecond)
		{
			bCheck = TRUE;
			iRemainSecond = SHOPPING.GetRemainSecond();
			if((m_iaMaterialCount[btMaterialType]+iAddMaterialCount) >= iNeedCount)
				iRemainSecond = 0;

			m_tMissionCheckTime = tCurrentTime;
		}
	}
	else
	{
		bCheck = TRUE;
	}

	CHECK_CONDITION_RETURN(FALSE == bCheck, FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iUpdatedMaterialCount;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_SHOPPING_GiveUserMaterial(m_pUser->GetUserIDIndex(), btMissionType, btMaterialType, iAddMaterialCount, 
		m_sUserInfo.btRandomGuestType, iRemainSecond, m_sUserInfo.iUpdateDate, m_sUserInfo.iUpdateTime, iUpdatedMaterialCount), FALSE);
	
	m_sUserInfo.iRemainSecond = iRemainSecond;
	m_iaMaterialCount[btMaterialType] = iUpdatedMaterialCount;

	return TRUE;
}

eSHOPPING_SELL_PRODUCT_RESULT CFSGameUserShoppingEvent::SellProduct(int	iProductIndex, SS2C_SHOPPING_SELL_PRODUCT_RES& rs)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, SHOPPING_SELL_PRODUCT_FAIL);
	CHECK_CONDITION_RETURN(CLOSED == SHOPPING.IsOpen(), SHOPPING_SELL_PRODUCT_CLOSED_EVENT);

	ResetUserShoppingInfo();

	CHECK_CONDITION_RETURN(m_sUserInfo.iCurrentSellCount >= SHOPPING.GetMaxSellCount(), SHOPPING_SELL_PRODUCT_MAX_SELL_COUNT);

	int iaMaterialCount[MAX_SHOPPING_MISSION_MATERIAL_TYPE];
	SHOPPING.GetProductMaterialInfo(iProductIndex, iaMaterialCount);

	for(BYTE btType = 0; btType < MAX_SHOPPING_MISSION_MATERIAL_TYPE; ++btType)
		CHECK_CONDITION_RETURN(m_iaMaterialCount[btType] < iaMaterialCount[btType], SHOPPING_SELL_PRODUCT_LACK_MATERIAL_FAIL);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, SHOPPING_SELL_PRODUCT_FAIL);

	int iGetCoin = SHOPPING.GetProductPrice(iProductIndex);
	int iUpdatedSellCount = 0, iUpdatedCoin = 0;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_SHOPPING_SellProduct(m_pUser->GetUserIDIndex(), iProductIndex, iGetCoin, m_sUserInfo.iUpdateDate, m_sUserInfo.iUpdateTime, 
		iaMaterialCount, iUpdatedSellCount, iUpdatedCoin), SHOPPING_SELL_PRODUCT_FAIL);

	m_sUserInfo.iCurrentSellCount = iUpdatedSellCount;
	m_sUserInfo.iCurrentCoin = iUpdatedCoin;

	for(BYTE btType = 0; btType < MAX_SHOPPING_MISSION_MATERIAL_TYPE; ++btType)
		m_iaMaterialCount[btType] -= iaMaterialCount[btType];

	rs.iUpdatedCoin = m_sUserInfo.iCurrentCoin;
	rs.iUpdatedGuestIndex = SHOPPING.GetGuestIndex(m_sUserInfo.btRandomGuestType, m_sUserInfo.iCurrentSellCount);
	
	for(BYTE btMissionType = SHOPPING_MISSION_TYPE_LOGIN_TIME; btMissionType < MAX_SHOPPING_MISSION_TYPE; ++btMissionType)
	{
		rs.sMissionInfo[btMissionType].iConditionValue = SHOPPING.GetConditionValue(btMissionType);
		rs.sMissionInfo[btMissionType].iCurrentValue = 0;
		if(SHOPPING_MISSION_TYPE_LOGIN_TIME == btMissionType)
		{
			time_t tCurrentTime = _time64(NULL);
			if((tCurrentTime - m_tMissionCheckTime) < m_sUserInfo.iRemainSecond)
				rs.sMissionInfo[btMissionType].iCurrentValue = m_sUserInfo.iRemainSecond - (tCurrentTime - m_tMissionCheckTime);
		}

		rs.sMissionInfo[btMissionType].btMaterialType = SHOPPING.GetMaterialType(btMissionType);
		rs.sMissionInfo[btMissionType].iMaterialCount = m_iaMaterialCount[rs.sMissionInfo[btMissionType].btMaterialType];
	}

	return SHOPPING_SELL_PRODUCT_SUCCESS;
}

eSHOPPING_USE_COIN_RESULT CFSGameUserShoppingEvent::UseCoin(int iExchangeProductIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, SHOPPING_USE_COIN_GIVE_REWARD_FAIL);
	CHECK_CONDITION_RETURN(CLOSED == SHOPPING.IsOpen(), SHOPPING_USE_COIN_CLOSED_EVENT);

	int iPrice = SHOPPING.GetExchangeProductPrice(iExchangeProductIndex);
	CHECK_CONDITION_RETURN(0 == iPrice, SHOPPING_USE_COIN_GIVE_REWARD_FAIL);
	CHECK_CONDITION_RETURN(m_sUserInfo.iCurrentCoin < iPrice, SHOPPING_USE_COIN_LACK_COIN_FAIL);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, SHOPPING_USE_COIN_FULL_MAILBOX_FAIL);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, SHOPPING_USE_COIN_GIVE_REWARD_FAIL);

	int iRewardIndex = SHOPPING.GetRewardIndex(iExchangeProductIndex);
	int iUpdatedCoin;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_SHOPPING_UseCoin(m_pUser->GetUserIDIndex(), iExchangeProductIndex, iPrice, iRewardIndex, iUpdatedCoin), SHOPPING_USE_COIN_GIVE_REWARD_FAIL);

	m_sUserInfo.iCurrentCoin = iUpdatedCoin;

	SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, SHOPPING_USE_COIN_GIVE_REWARD_FAIL);

	return SHOPPING_USE_COIN_SUCCESS;
}