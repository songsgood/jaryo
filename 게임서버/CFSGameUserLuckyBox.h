qpragma once

class CFSGameUser;
class CFSODBCBase;

enum 
{
	eLUCKYBOX_UPDATE_TYPE_PLAY_MISSION = 0,
	eLUCKYBOX_UPDATE_TYPE_DAILY_RESET,
};

class CFSGameUserLuckyBox
{
public:
	CFSGameUserLuckyBox(CFSGameUser* pUser);
	~CFSGameUserLuckyBox(void);

	BOOL Load();
	BOOL CheckResetDate(time_t tCurrentTime = _time64(NULL));
	void SendEventInfo();
	void SendUserInfo();
	RESULT_USE_LUCKYTICKET UseLuckyTicket(BYTE& btProductIndex, int& iUpdatedLuckyTicketCount);
	void UpdateMission(int iMatchType, BOOL bIsDisconnectedMatch, BOOL bRunAway, BYTE& btGiveLuckyTicketCnt);

private:

private:
	CFSGameUser* m_pUser;
	BOOL		m_bDataLoaded;

	SAvatarLuckyBoxMission m_AvatarLuckyBoxMission;
};