qinclude "StdAfx.h"
qinclude "JoinTeamConfigInfo.h"
qinclude "CFSODBCBase.h"

CJoinTeamConfigInfo::CJoinTeamConfigInfo(void)
	: m_iGameIDIndex(0)
{
}

CJoinTeamConfigInfo::~CJoinTeamConfigInfo(void)
{
}

void CJoinTeamConfigInfo::Load(int iGameIDIndex, CFSODBCBase* pODBC, SS2C_JOIN_TEAM_CONFIG_INFO& info)
{
	m_iGameIDIndex = iGameIDIndex;

	CHECK_NULL_POINTER_VOID(pODBC);

	pODBC->JOIN_TEAM_GetConfigInfo(iGameIDIndex, m_JoinConfig);

	info.btMatchType = m_JoinConfig.btMatchType;
	info.btMatchTeamScale = m_JoinConfig.btMatchTeamScale;
	info.i3on3Position1 = m_JoinConfig.i3on3Position1;
	info.i3on3Position2 = m_JoinConfig.i3on3Position2;
	info.i3on3AvoidTeam = m_JoinConfig.i3on3AvoidTeam;
	info.i2on2Position = m_JoinConfig.i2on2Position;
	memcpy(info.ia5on5Position, m_JoinConfig.ia5on5Position, sizeof(int)*MAX_USER_SLOT_CONFIG_5ON5);
	info.iLOD = m_JoinConfig.iLOD;
	info.iPVEPosition1 = m_JoinConfig.iPVEPosition1;
	info.iPVEPosition2 = m_JoinConfig.iPVEPosition2;
	info.iAIPVPPosition1 = m_JoinConfig.iAIPVPPosition1;
	info.iAIPVPPosition2 = m_JoinConfig.iAIPVPPosition2;
	info.iAIPVPPlayerNum = m_JoinConfig.iAIPVPPlayerNum;
	info.iPVEPlayerNum = m_JoinConfig.iPVEPlayerNum;
}

void CJoinTeamConfigInfo::Save(CFSODBCBase* pODBC)
{
	pODBC->JOIN_TEAM_UpdateConfigInfo(m_iGameIDIndex, m_JoinConfig);
}

void CJoinTeamConfigInfo::SetPositions(int iMatchType, int iTeamMatchScale, int iaPosition[MAX_USER_SLOT_CONFIG_5ON5], int iAvoidTeam/* = -1*/)
{
	if (MAX_MATCH_TYPE_COUNT > iMatchType)
		m_JoinConfig.btMatchType = iMatchType;

	if( MATCH_TYPE_AIPVP == iMatchType)
		m_JoinConfig.btMatchTeamScale = MATCH_TEAM_SCALE_3ON3;
	else if (MAX_MATCH_TEAM_SCALE > iTeamMatchScale)
		m_JoinConfig.btMatchTeamScale = iTeamMatchScale;

	switch (m_JoinConfig.btMatchTeamScale)
	{
	case MATCH_TEAM_SCALE_2ON2:
		m_JoinConfig.i2on2Position = iaPosition[USER_SLOT_CONFIG_IDX_0];
		break;
	case MATCH_TEAM_SCALE_3ON3:
	case MATCH_TEAM_SCALE_3ON3CLUB:
		{
			if(MATCH_TYPE_AIPVP == iMatchType)
			{
				m_JoinConfig.iAIPVPPosition1 = iaPosition[USER_SLOT_CONFIG_IDX_0];
				m_JoinConfig.iAIPVPPosition2 = iaPosition[USER_SLOT_CONFIG_IDX_1];
			}
			else
			{
				m_JoinConfig.i3on3Position1 = iaPosition[USER_SLOT_CONFIG_IDX_0];
				m_JoinConfig.i3on3Position2 = iaPosition[USER_SLOT_CONFIG_IDX_1];
				if (-1 != iAvoidTeam) m_JoinConfig.i3on3AvoidTeam = iAvoidTeam;
			}
		}
		break;
		/*case MATCH_TEAM_SCALE_3ON3CLUB:
		m_JoinConfig.i3on3Position1 = POSITION_ALL;
		m_JoinConfig.i3on3Position2 = POSITION_ALL;
		if (-1 != iAvoidTeam) m_JoinConfig.i3on3AvoidTeam = iAvoidTeam;
		break;*/
	case MATCH_TEAM_SCALE_5ON5:
		memcpy(m_JoinConfig.ia5on5Position, iaPosition, sizeof(int)*MAX_USER_SLOT_CONFIG_5ON5);
		break;
	}
}

void CJoinTeamConfigInfo::SetPVEPosition( int iLOD, int iPosition1, int iPosition2 , int iPlayerNum)
{
	m_JoinConfig.btMatchType = MATCH_TYPE_PVE;

	if(iLOD < MAX_INDEX_LEVEL_OF_DIFFICULTY)
		m_JoinConfig.iLOD = iLOD;

	m_JoinConfig.iPVEPosition1 = iPosition1;
	m_JoinConfig.iPVEPosition2 = iPosition2;

	m_JoinConfig.iPVEPlayerNum = iPlayerNum;
}

int CJoinTeamConfigInfo::GetPVEInfo( SC2S_PVE_CREATE_ROOM_REQ& rq )
{
	rq.iLOD = m_JoinConfig.iLOD;
	rq.shSlotPosition[0] = m_JoinConfig.iPVEPosition1;
	rq.shSlotPosition[1] = m_JoinConfig.iPVEPosition2;

	return m_JoinConfig.iPVEPlayerNum;
}
