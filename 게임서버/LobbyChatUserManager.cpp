qinclude "stdafx.h"
qinclude "LobbyChatUserManager.h"
qinclude "CFSGameUser.h"
qinclude "FactionManager.h"

void CLobbyChatChannel::Add(CFSGameUser* pUser, eCHAT_CHANNEL_TYPE eType)
{
	CHECK_NULL_POINTER_VOID(pUser);

	CLock Lock(&m_csUserMap);

	if(CHAT_CHANNEL == eType)
	{
		CHECK_CONDITION_RETURN_VOID(NULL != pUser->GetChatChannel());
		pUser->SetChatChannel(this);
	}
	else if(CHAT_FACTION_CHANNEL == eType)
	{
		CHECK_CONDITION_RETURN_VOID(NULL != pUser->GetChatFactionChannel());
		pUser->SetChatFactionChannel(this);
	}

	m_mapUser[pUser->GetUserIDIndex()] = pUser;
}

void CLobbyChatChannel::Remove(CFSGameUser* pUser, eCHAT_CHANNEL_TYPE eType)
{
	CHECK_NULL_POINTER_VOID(pUser);

	CLock Lock(&m_csUserMap);

	if(CHAT_CHANNEL == eType)
	{
		pUser->SetChatChannel(NULL);
	}
	else if(CHAT_FACTION_CHANNEL == eType)
	{
		pUser->SetChatFactionChannel(NULL);
	}

	m_mapUser.erase(pUser->GetUserIDIndex());
}

void CLobbyChatChannel::Clear()
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL)
			iter->second->SetChatChannel(NULL);
	}

	m_mapUser.clear();
}

void CLobbyChatChannel::ClearFaction()
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL)
			iter->second->SetChatFactionChannel(NULL);
	}

	m_mapUser.clear();
}

void CLobbyChatChannel::Broadcast(CPacketComposer& Packet)
{
	CLock Lock(&m_csUserMap);

	GAMEUSERMAP::iterator iter = m_mapUser.begin();
	GAMEUSERMAP::iterator endIter = m_mapUser.end();
	for (; iter != endIter; ++iter)
	{
		if(iter->second != NULL)
			iter->second->Send(&Packet);
	}
}

CLobbyChatUserManager::CLobbyChatUserManager(void)
{
}


CLobbyChatUserManager::~CLobbyChatUserManager(void)
{
}

void CLobbyChatUserManager::AddUser(CFSGameUser* pUser, int iRoomType, int iRoomIndex)
{
	CHECK_NULL_POINTER_VOID(pUser);
	
	pUser->ExitChatChannel();

	CHECK_CONDITION_RETURN_VOID(iRoomType <= CHAT_LOBBYROOM_TYPE_NONE || CHAT_LOBBYROOM_TYPE_MAX <= iRoomType);
	CHECK_CONDITION_RETURN_VOID(iRoomIndex <= 0 || MAX_LOBBY_CHAT_ROOM_COUNT < iRoomIndex);

	m_ChatChannel[iRoomType][iRoomIndex].Add(pUser, CHAT_CHANNEL);

	AddFactionUser(pUser);
}

void CLobbyChatUserManager::AddFactionUser(CFSGameUser* pUser)
{
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->ExitChatFactionChannel();

	CHECK_CONDITION_RETURN_VOID(FACTION_RACE_ON_GOING != FACTION.GetRaceStatus() && 
		FACTION_RACE_ON_BATTLE_NOT_PROGRESS_TIME != FACTION.GetRaceStatus());

	CHECK_CONDITION_RETURN_VOID(pUser->GetFactionIndex() <= FACTION_INDEX_NONE || MAX_FACTION_INDEX_COUNT <= pUser->GetFactionIndex());

	m_ChatFactionChannel[pUser->GetFactionIndex()].Add(pUser, CHAT_FACTION_CHANNEL);		
}

void CLobbyChatUserManager::RemoveFactionUser()
{
	for(int iFactionIndex = FACTION_INDEX_1; iFactionIndex < MAX_FACTION_INDEX_COUNT; ++iFactionIndex)
	{
		m_ChatFactionChannel[iFactionIndex].ClearFaction();
	}
}

void CLobbyChatUserManager::ChatServerDisconnected()
{
	for (int iRoomType = 0; iRoomType < CHAT_LOBBYROOM_TYPE_MAX; ++iRoomType)
	{
		for (int iRoomIndex = 0; iRoomIndex <= MAX_LOBBY_CHAT_ROOM_COUNT; ++iRoomIndex)
		{
			m_ChatChannel[iRoomType][iRoomIndex].Clear();
		}
	}

	for(int iFactionIndex = FACTION_INDEX_1; iFactionIndex < MAX_FACTION_INDEX_COUNT; ++iFactionIndex)
	{
		m_ChatFactionChannel[iFactionIndex].ClearFaction();
	}
}

void CLobbyChatUserManager::Broadcast(CPacketComposer& Packet, int iRoomType, int iRoomIndex)
{
	CHECK_CONDITION_RETURN_VOID(iRoomType <= CHAT_LOBBYROOM_TYPE_NONE || CHAT_LOBBYROOM_TYPE_MAX <= iRoomType);
	CHECK_CONDITION_RETURN_VOID(iRoomIndex <= 0 || MAX_LOBBY_CHAT_ROOM_COUNT < iRoomIndex);

	m_ChatChannel[iRoomType][iRoomIndex].Broadcast(Packet);
}

void CLobbyChatUserManager::BroadcastFaction(CPacketComposer& Packet, int iFactionIndex)
{
	CHECK_CONDITION_RETURN_VOID(iFactionIndex <= FACTION_INDEX_NONE || MAX_FACTION_INDEX_COUNT <= iFactionIndex);

	m_ChatFactionChannel[iFactionIndex].Broadcast(Packet);
}