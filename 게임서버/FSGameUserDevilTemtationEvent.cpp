qinclude "stdafx.h"
qinclude "FSGameUserDevilTemtationEvent.h"
qinclude "DevilTemtationEventManager.h"
qinclude "CFSGameUser.h"
qinclude "GameProxyManager.h"
qinclude "CenterSvrProxy.h"

CFSGameUserDevilTemtationEvent::CFSGameUserDevilTemtationEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iSoulStone(0)
	, m_btBonusChanceType(0)
{
	for(int i = NORMAL_TICKET; i < MAX_TICKET_TYPE; ++i)
	{
		m_iDevilTicket[i] = 0;
	}
}


CFSGameUserDevilTemtationEvent::~CFSGameUserDevilTemtationEvent(void)
{
}

BOOL	CFSGameUserDevilTemtationEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == DEVILTEMTATION.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iRet = pODBC->EVENT_DEVILTEMTATION_GetUserData(m_pUser->GetUserIDIndex(), m_iDevilTicket, m_iSoulStone, m_btBonusChanceType);
	if (ODBC_RETURN_SUCCESS != iRet)
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INITIALIZE_DATA, FAIL, "EVENT_DEVILTEMTATION_GetUserData error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void	CFSGameUserDevilTemtationEvent::SendEventProductInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DEVILTEMTATION.IsOpen());

	CPacketComposer* pPacket = DEVILTEMTATION.GetPacketEventProductInfo();
	m_pUser->Send(pPacket);
}

void	CFSGameUserDevilTemtationEvent::SendEventBonusProductInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DEVILTEMTATION.IsOpen());

	CPacketComposer* pPacket = DEVILTEMTATION.GetPacketEventBonusProductInfo();
	m_pUser->Send(pPacket);
}

void	CFSGameUserDevilTemtationEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DEVILTEMTATION.IsOpen());

	SS2C_DEVILTEMTATION_USER_INFO_RES	ss;

	for(int i = NORMAL_TICKET; i < MAX_TICKET_TYPE; ++i)
	{
		ss.iTicketCnt[i] = m_iDevilTicket[i];
	}

	ss.iSoulStone = m_iSoulStone;
	ss.btBallCnt = 0;
	ss.bBonus = false;

	switch(m_btBonusChanceType)
	{
	case BONUS_NONE:
		ss.btBallCnt = 0;
		break;

	case BONUS_1:
		ss.btBallCnt = 1;
		break;

	case BONUS_2:
		ss.btBallCnt = 2;
		break;

	case BONUS_3_CHANCE:
		ss.btBallCnt = 3;
		ss.bBonus = true;
		break;

	case BONUS_3:
		ss.btBallCnt = 3;
		break;

	case BONUS_4:
		ss.btBallCnt = 4;
		break;

	case BONUS_5:
		ss.btBallCnt = 5;
		break;

	case BONUS_6_CHANCE:
		ss.btBallCnt = 6;
		ss.bBonus = true;
		break;

	case BONUS_6:
		ss.btBallCnt = 6;
		break;

	case BONUS_7_CHANCE:
		ss.btBallCnt = 7;
		ss.bBonus = true;
		break;
	}

	m_pUser->Send(S2C_DEVILTEMTATION_USER_INFO_RES, &ss, sizeof(SS2C_DEVILTEMTATION_USER_INFO_RES));
}

BOOL	CFSGameUserDevilTemtationEvent::BuyDevilTemtationTicket_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_LEGENDEVENT, INVALED_DATA, CHECK_FAIL, "BuyLegendKey_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}
	
	int iPropertyKind = pBillingInfo->iParam0;
	BYTE btTicketType = 0;

	if(ITEM_PROPERTY_KIND_DEVILTEMTATION_NORMAL_ITEM == iPropertyKind)
	{
		btTicketType = NORMAL_TICKET;
	}
	else if(ITEM_PROPERTY_KIND_DEVILTEMTATION_PRIMIUM_ITEM == iPropertyKind)
	{
		btTicketType = PRIMIUM_TICKET;
	}
	else
	{
		return FALSE;
	}

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int	iBuyCount = pBillingInfo->iParam1;
	int	iSumCount = 0;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	
	SS2C_DEVILTEMTATION_BUY_TICKET_NOT	ss;
	ZeroMemory(&ss, sizeof(SS2C_DEVILTEMTATION_BUY_TICKET_NOT));

	if(ODBC_RETURN_SUCCESS != pBaseODBC->EVENT_DEVILTEMTATION_BuyDevilTemtation(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, iBuyCount, iPrevCash, iPostCash, pBillingInfo->iItemPrice, 
		iSumCount, btTicketType))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, NONE, "EVENT_DEVILTEMTATION_BuyDevilTemtation Fail, User:10752790, itemCode:0, BuyCount:7106560", m_pUser->GetUserIDIndex(), iItemCode, iBuyCount);
ult = RESULT_BUY_TICKET_FAIL;
	}
	else
	{
		m_iDevilTicket[btTicketType] = iSumCount;
		ss.btResult = RESULT_BUY_TICKET_SUCCESS;
		ss.iBuyCnt = iBuyCount;

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
	}

	// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);
	
	m_pUser->Send(S2C_DEVILTEMTATION_BUY_TICKET_NOT, &ss, sizeof(SS2C_DEVILTEMTATION_BUY_TICKET_NOT));

	int iCostType = pBillingInfo->iSellType;
	m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
	m_pUser->SendUserGold();

	return (ss.btResult == RESULT_BUY_TICKET_SUCCESS);
}

BOOL	CFSGameUserDevilTemtationEvent::UseDevilTemtation(BYTE btTicketType, BYTE btTicketUseType)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	SS2C_DEVILTEMTATION_USE_TICKET_RES	ss;
	ss.btTicketType = btTicketType;
	ss.btTicketUseType = btTicketUseType;

	int iAddSoulStone = 0;
	BOOL bIsShout = FALSE;
	vector<int/*iRewardIndex*/>	vReward;
	BYTE	btResultProductIndex[USE_10_TICKET] = {0,};
	int iRewardCnt = (btTicketUseType == USE_BONUS_CHANCE) ? 1 : static_cast<int>(btTicketUseType);
	BYTE btBonusChanceType = m_btBonusChanceType;
	
	if(CLOSED == DEVILTEMTATION.IsOpen())
	{
		ss.btResult = RESULT_USE_TICKET_FAIL_EVENT_CLOSED;
	}
	else if(btTicketType >= MAX_TICKET_TYPE)
	{
		ss.btResult = RESULT_USE_TICKET_FAIL_WRONG_INFO;
	}
	else if(btTicketUseType != USE_1_TICKET && 
		btTicketUseType != USE_10_TICKET && 
		btTicketUseType != USE_BONUS_CHANCE)
	{
		ss.btResult = RESULT_USE_TICKET_FAIL_WRONG_INFO;
	}
	else if(btTicketUseType != USE_BONUS_CHANCE && 
		m_iDevilTicket[btTicketType] < (int)btTicketUseType)
	{
		ss.btResult = RESULT_USE_TICKET_FAIL_LACK_TICKET;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + iRewardCnt > MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_USE_TICKET_FAIL_FULL_MAILBOX;
	}
	else if(FALSE == DEVILTEMTATION.GetRandomProduct(btTicketType, btTicketUseType, btResultProductIndex, iAddSoulStone, btBonusChanceType, bIsShout, vReward))
	{
		ss.btResult = RESULT_USE_TICKET_FAIL_WRONG_INFO;
	}
	else
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);
		
		char szReward[MAX_DEVILTEMTATION_REWARD_LENGTH+1] = {NULL,};
		ConvertRewardIndexStr(szReward, &vReward, static_cast<int>(USE_10_TICKET), MAX_DEVILTEMTATION_REWARD_LENGTH);
		list<int> listPresentIndex; 

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_DEVILTEMTATION_GiveProductReward(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), btTicketType, btTicketUseType, iAddSoulStone, btBonusChanceType, btResultProductIndex, listPresentIndex, szReward))
		{
			ss.btResult = RESULT_USE_TICKET_SUCCESS;

			for(int i = 0; i < USE_10_TICKET; ++i)
				ss.btResultProductIndex[i] = btResultProductIndex[i];

			m_iDevilTicket[btTicketType] -= (int)btTicketUseType;
			m_iSoulStone += iAddSoulStone;
			m_btBonusChanceType = btBonusChanceType;

			m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);

			if(bIsShout)
			{
				CCenterSvrProxy* pCenter = dynamic_cast<CCenterSvrProxy*>(GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY));
				if( NULL != pCenter )
				{
					SG2S_EVENT_SHOUT_REQ  info;
					info.iEventKind = EVENT_KIND_DEVILTEMTATION;
					info.btContentsType = SHOUT_CONTENTS_TYPE_GOLD_GRADE;
					strncpy_s(info.GameID, _countof(info.GameID), m_pUser->GetGameID(), _countof(info.GameID)-1);
					pCenter->SendPacket(G2S_EVENT_SHOUT_REQ, &info, sizeof(SG2S_EVENT_SHOUT_REQ));
				}
			}
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, NONE, "EVENT_DEVILTEMTATION_GiveProductReward 1 Fail, UserIDIndex:10752790", m_pUser->GetUserIDIndex());
}

	m_pUser->Send(S2C_DEVILTEMTATION_USE_TICKET_RES, &ss, sizeof(SS2C_DEVILTEMTATION_USE_TICKET_RES));

	return TRUE;

}

BOOL	CFSGameUserDevilTemtationEvent::UseSoulStone(BYTE btSoulStoneIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	int iRewardIndex = 0;
	int iPrice = 0;
	SS2C_DEVILTEMTATION_USE_SOULSTONE_RES	ss;
	
	if(CLOSED == DEVILTEMTATION.IsOpen())
	{
		ss.btResult = RESULT_USE_SOULSTONE_FAIL_EVENT_CLOSED;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_USE_SOULSTONE_FAIL_FULL_MAILBOX;
	}
	else if(FALSE == DEVILTEMTATION.GetBonusProductInfo(btSoulStoneIndex, iPrice, iRewardIndex) || 0 >= iRewardIndex)
	{
		ss.btResult = RESULT_USE_SOULSTONE_FAIL_WRONG_INFO;
	}
	else if(iPrice > m_iSoulStone)
	{
		ss.btResult = RESULT_USE_SOULSTONE_FAIL_LACK_SOULSTONE;
	}
	else
	{
		CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_BOOL(pODBC);

		char szReward[MAX_DEVILTEMTATION_REWARD_LENGTH+1] = {NULL,};
		sprintf_s(szReward, _countof(szReward), "10752790", iRewardIndex);
st<int> listPresentIndex; 
		BYTE	btResultProductIndex[USE_10_TICKET];
		btResultProductIndex[0] = btSoulStoneIndex;
		
		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_DEVILTEMTATION_GiveProductReward(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), 0, 0, -iPrice, 0, btResultProductIndex, listPresentIndex, szReward))
		{
			ss.btResult = RESULT_USE_SOULSTONE_SUCCESS;
			m_iSoulStone -= iPrice;

			m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, NONE, "EVENT_DEVILTEMTATION_GiveProductReward 2 Fail, UserIDIndex:10752790", m_pUser->GetUserIDIndex());
}

	m_pUser->Send(S2C_DEVILTEMTATION_USE_SOULSTONE_RES, &ss, sizeof(SS2C_DEVILTEMTATION_USE_SOULSTONE_RES));

	return TRUE;
}

void	CFSGameUserDevilTemtationEvent::SetTicketCount(BYTE btTicketType, int iCount)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DEVILTEMTATION.IsOpen());
	CHECK_CONDITION_RETURN_VOID(btTicketType >= MAX_TICKET_TYPE);

	m_iDevilTicket[btTicketType] = iCount;
}

