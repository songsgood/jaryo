qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserHalloweenGamePlayEvent
{
public:
	CFSGameUserHalloweenGamePlayEvent(CFSGameUser* pUser);
	~CFSGameUserHalloweenGamePlayEvent(void);

	BOOL				Load();
	void				SendEventInfo();
	RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM ExchangeItemReq(int iItemIndex, SS2C_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_RES& rs);
	RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY TryCandyReq(SS2C_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_RES& rs);
	RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX OpenRandomBoxReq(SS2C_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_RES& rs);
	BOOL AddTryCount();

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	int m_iCandy;
	int m_iGetCandy;
	int m_iGamePlayCnt;
	int m_iRemainTryCnt;
	int m_iBoxCnt;
};

