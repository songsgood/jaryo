qpragma once

class CFSGameUser;
class CFSGameUserHelloNewYearEvent
{
public:
	CFSGameUserHelloNewYearEvent(CFSGameUser* pUser);
	~CFSGameUserHelloNewYearEvent(void);

	BOOL					Load();
	void					SendUserInfo();
	BOOL					CheckRemainBuffSec(time_t tCheckTime, bool bNeedUpdate = FALSE);
	void					GetRewardReq();
	int						GetUserBuffRate(bool bCheckTime = false);

private:
	CFSGameUser*			m_pUser;
	BOOL					m_bDataLoaded;

	int						m_iRamainBuffSec;
	BYTE					m_btButtonStatus;
	time_t					m_tStartLoginTime;
};

