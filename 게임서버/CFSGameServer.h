qpragma once
qinclude "NexusServer.h"
qinclude "GameProxyManager.h"
qinclude "CFSGameUser.h"
qinclude "CGameIDStringChecker.h"
qinclude "StatusUpTable.h"
qinclude "LankUpManager.h"
qinclude "CAchievementManager.h"
qinclude "CAvatarCreateManager.h"
qinclude "FSServer.h"
qinclude "LockMap.h"
qinclude "ThreadODBCManager.h"
qinclude "CBillingManagerGame.h"
qinclude "BasketBallManager.h"

class CFSSvrList;
class CFSGameClient;
class CFSRankManager;
class CContentsManager;
class CFSSkillShop;
class CFSTrainingShop;
class CBillingManager;
class CAchievementManager;
class CActionShop;
class CChannelBuffManager;
class CMatchSvrProxy;
class CLeagueSvrProxy;

#define CRASH_CHECK_NUM	65000

class CFSGameServer : public CNexusServer  , public CFSServer
{
	friend class CScopedRefGameUser;

public: //구동을 위한 필수 - public
	CFSGameServer( CIOCP* pIOCP, LPCTSTR szServiceName, DWORD dwServerTimerInterval );
	virtual ~CFSGameServer();

	CSocketClient* IAllocateClient();
	virtual void FreeClient(CSocketClient* pSocketClient);
	virtual BOOL RemoveUser(CUser* pUser);
	BOOL RemoveUser(CFSGameUser* pUser, BOOL bExitChatChannel = TRUE);

	CGameIDStringChecker* GetGameIDStringChecker(){ return &m_GameIDStringChecker; };

	virtual BOOL OnInitialize();
	virtual void OnClose();
	//구동을 위한 필수 - End

public:
	CFSItemShop	   * GetItemShop()	  { return m_pItemShop;	   };
	CFSSkillShop   * GetSkillShop()	  { return m_pSkillShop;   };
	CFSTrainingShop* GetTrainingShop(){ return m_pTrainingShop; };
	CBasketBallManager*		GetBasketBallManager()		{ return &m_BasketBallManager; }
	CActionShop*	GetActionShop()		{ return m_pActionShop;		}
	CLimitedEditionItemManager* GetLimitedEditionItemManager() { return m_pLimitedEditionItemManager; }
	CLimitedTimeItemManager* GetLimitedTimeItemManager() { return m_pLimitedTimeItemManager; }

	virtual void OnTimer();
	void	OnEventTimer();
	void	ResetGameServerTimeCount() { m_iFSTimeCount = 1; };
	void	IncreaseGameServerTimeCount() { m_iFSTimeCount++; };
	int		GetGameServerTimeCount() { return m_iFSTimeCount; };

	void	OnCheckUserTimeOut(); 
	void	OnInitLimitedEdtionItem();
	void    UpdateChannelBuffTime();
	
	BOOL	IsSetUserLog() { return m_bSetUserLog; };

	void	BroadCastShout(CPacketComposer& PacketComposer);
	virtual void	BroadCast(CPacketComposer& PacketComposer, BOOL bExceptPlayingUser = FALSE);
	void	BroadCastNotifyMsg(int iOutputType, LPCTSTR szMsg , int iMsgLength);
	void	BroadCastShoutMsg(char* szOperatorID, int iErrorCode, int iShoutItemMode, LPCTSTR szMsg);	// 20071016 Add Shout Item mode

	BOOL	GetEnable(){ return m_bEnable; };

	int		GetCurrentUserCount()	{	return m_mapUserTotal.GetSize();	}

	static CFSGameServer*	GetInstance() { return CFSGameServer::m_pInstance; }
	static CFSGameServer*	m_pInstance;

	static bool IsWCGServer(){ return CFSGameServer::m_bWCGServer; };  ///// WCG 
	static bool m_bWCGServer;  ///// WCG 

	bool	IsCheckLog() { return m_bCheckLog; };

	BOOL CheckOpUser( LPCTSTR szOpUserID );

	void SetCodePage();
	CFSGameUser* IsSameUserGameServer( LPCTSTR szUserID ,int iPublisherIDIndex);
	int GetMatchMode() { return m_iMatchMode; }

	void			AllUserForceOut();
	BOOL			AllUserForceOutExceptMe(CFSGameUser* pUser);
	BOOL			AllUserSendPaper(int iSendGameIDIndex, char* szTitle, char* szText);

	int				CheckShoutTime(int iShoutItemMode);
	BOOL			GetShout() { return m_bShout; }
	void			UpdateShoutTime();

	void	BroadCastSendGMMsg( int iMsgType, LPCTSTR szMsg );

	void	SetClubCheerLeaderCode(int iClubSN, int iChangedCheerLeaderCode, BYTE btTypeNum);
	void    SetClubCheerLeaderCode(int iClubSN, int iExpiredCheerLeaderCode, int iChangedCheerLeaderCode);

	BOOL	IsNoWithdrawChannel();

	int		GetEqualizeLv() { return m_iEqualizeLV; };
	virtual BOOL	IsEqualizeChannel() {return m_bIsEqualizeChannel; };

	// 20090316 Tournament StatusFlag
	BOOL	IsTournamentEnable()		{ return m_bTournamentEnable; };

	// 20090402 Write Login Request Count Log
	BOOL	IsUseLoginRequestLog()		{return m_bUseLoginRequestLog;};
	void	IncreaseLoginRequestCount() { m_dwLoginRequestCount++;};
	DWORD	GetLoginRequestCount() { return m_dwLoginRequestCount;};
	// End

	// 20090518 Abuse user penalty reinforcement status flag.
	BOOL	IsAbusePenaltyReinforcementEnable()		{ return m_bAbusePenaltyReinforcementEnable; };

// 20090917 Leveling
	BOOL	LoadLevelingCurve();
// End

	BOOL	LoadMaxLevelPlayCountData();

	void	SetAbusePenaltyDisconnectPointCondition( int iDisconnectPointCondition )
			{ m_iAbusePenaltyDisconnectPointCondition = iDisconnectPointCondition; };
	int		GetAbusePenaltyDisconnectPointCondition( ) { return m_iAbusePenaltyDisconnectPointCondition; };
// End

// 20090917 Leveling
	virtual int		GetLvUpExp(int iLv); 
// End

	BOOL	GetMaxLevelStepUpLv(int iMaxLevelStep, int& iRequireLv);
	int		GetMaxLevelStepUpRewardIndex(int iMaxLevelStep);
	int		GetMaxLevelStepListSize();

	BOOL	LoadDataForContentsManager();

	// 2010.02.24
	// 서버 Config
	// ktKim
	BOOL			GetOptionFollow();
	BOOL			GetOptionStatusMod();
	void			GetStatusUp(int iPosition, int iLv, int* iStatus, int iNum);
	void			GetStatusUp(int iPosition, int iStartLv, int iEndLv, int* iStatus, int iNum);

	void			GetEventImageType(int iEventType, int& iEventImageType);

	void			BroadCastMatchServerConnect(CMatchSvrProxy* pProxy);
	void			BroadCastMatchServerLost(CMatchSvrProxy* pProxy);
	void			BroadCastChatServerStatus(int iServerStatus);
	void			BroadCastLeagueServerConnect(CLeagueSvrProxy* pProxy);
	void			BroadCastLeagueServerLost(CLeagueSvrProxy* pProxy);
	void			CheckAndLoadRankList();
	void			ProcessSeasonRankAchievement();
	void			CheckAndSetOptionWriteUserLogToDB();
	
	// 20070809 Single Contents Merge
	BOOL			SetUserSingleContent( SAvatarInfo* pAvatarInfo, BYTE &byCount );

	// 20070911 Add MiniGame Info
	BOOL			GetSingleContentBonus( SAvatarInfo* pAvatarInfo, int iUserStage, BYTE byTrainingKind, BYTE byStageIndex , int& iExp, int& iPoint);

	// 20070911 Add MiniGame Info
	bool			CheckFirstMiniGame( int iUserStage, int iStageNum );
	// End
	// 20080522 Send Point at last stage
	bool			CheckLastMiniGame( int iUserStage, int iStageNum ,int iTotalStageNum);
	// End

	void SetUserBaseEpisodeInfo( CUser* pUser );

	int GetEpisodeNum(int iEpisodeGroupIndex) { return m_paEpisodeList[iEpisodeGroupIndex]->GetSize(); }
	int GetEpisodeMaxStage( int iEpisodeGroupIndex, int iEpisodeNum );

	virtual int				GetFameLankUpRequire(int iFameLevel);	

	void Process_CompleteAchievement(SM2G_ACHIEVEMENT_COMPLETE rs);

	DWORD	GetForbidChatInterval() const { return m_dwForbidChatInterval; };

	bool	GetUseLuckItem() { return m_bUseLuckItem;};
	virtual int		GetUserInfoProtVerID() {return m_iUserInfoProtVerID;}
	BOOL	IsEnableAvatarFame() { return m_bEnableAvatarFame; }

	int		GetDiscountRate(int iType);
	virtual time_t	GetCurrentDBDate(void)	{	return m_tCurrentDBDate;	}
	SYSTEMTIME	GetCurrentDBDateToSystemTime(void);
	BYTE	GetServerIndex(void);
	
	virtual int GetShopItemSexCondition(int iItemCode);
	virtual	void BroadCastWordPuzzlesEventStatus(OPEN_STATUS eStatus);
	virtual void BroadCast_RandomBoxStatus(OPEN_STATUS eStatus);
	virtual void BroadCast_PuzzlesStatus(OPEN_STATUS eStatus);
	virtual void BroadCast_ThreeKingdomsEventStatus(OPEN_STATUS eStatus);
	virtual void BroadCast_MissionShopEventStatus();
	virtual void BroadCastHotGirlMissionEventStatus(OPEN_STATUS eStatus);
	void BroadCast_FactionStatus(FACTION_RACE_STATUS eStatus);
	virtual void BroadCast_LvUpEventStatus(OPEN_STATUS eStatus);
	virtual void BoradCast_RandomItemBoardEventStatus(OPEN_STATUS eStatus);
	virtual void BroadCast_EventStatus();
	virtual void BroadCast_EventStatus(int iEventKind, OPEN_STATUS eStatus, BYTE btSetUpType, BYTE btClickCloseButtonNotice);
	virtual void SendToChatChangePenaltyStatus(int iGameIDIndex, BOOL bPenaltyUser);
	virtual void BroadCast_UserChoiceMissionEventStatus(OPEN_STATUS eStatus);
	virtual void BroadCast_UserRandomItemTreeEvent(OPEN_STATUS eStatus);
	virtual void BroadCast_UserExpBoxItemEvent(OPEN_STATUS eStatus);
	virtual void BroadCast_UserRandomItemGroupEvent(OPEN_STATUS eStatus);
	virtual void BroadCast_RandomCardEventStatus(OPEN_STATUS eStatus);
	virtual void BroadCastShoppingFestivalEventStatus(BYTE btCurrentEventType, BYTE btOpenStatus);
	virtual void BroadCastShoppingFestivalEventInfo(BYTE btEventType);
	virtual void BroadCast_LoginPayback_ShopPopupStatus(OPEN_STATUS eStatus);
	virtual void BroadCast_SteelBagMissionEventStatus(OPEN_STATUS eStatus);

	void	UpdateRecordBoard();
	void	CheckAndResetRecordBoard();
	void	CheckAndUpdateRecordBoardRanking();

	void	SetClubPennantCode(int iClubSN, short stSlotIndex, int iPennantCode);
	void	GiveClubClothes(int iClubSN, int iItemCode);

	void	ResetIntensivePracticOnedayRecord();
	void	CheckAndLoadIntensivePracticeRanking();

	BOOL	CheckMatchingPoolCareTime();
	
	void	CheckHotGirlTime();
	void	GiveFactionRaceRankReward(SS2G_FACTION_GIVE_REWARD_REQ req, CReceivePacketBuffer* rp);
	void	ClearUserFactionInfo();
	void	CheckCoachCardStatus();
	void	CheckEventGoldenCrushStatus();
	void	CheckEventMatchingCardStatus();
	void	CheckEventAttendanceItemShopStatus();
	void	CheckEventAttendanceTodayItemReset();
	BOOL	UpdateCurrentDBDate();
	int		GetAchievementIndexWithSeasonRankIndex(int iSeasonRankIndex, int iMinRank = 0);
	void	CheckAndLoadUserAppraisal();

	BOOL	GetRandomGameUser( int iUserCount, vector<char*>& vGameUser );

	void	CheckContentsDefaultSetting();
	BOOL	CheckItemShopEventButtonRender();
	BOOL	CheckItemShopEventButtonSchedule();
	void	CheckBasketBallEventButtonSchedule();
	void	CheckLeagueModeStatus();
	void	CheckLeagueModeMatchOpenTime();
	void	UpdateLeagueModeSwitch();
	void	GivePCRoomUserLoginTimeReward();

	void	AddRankMatchLimitUser(CFSGameUser* pUser);
	void	RemoveRankMatchLimitUser(int iGameIDIndex);
	void	DecreaseRankMatchLimitTime();

	void	CheckAndLoadGameOfDiceRankList();
	void	CheckAndLoadMiniGameZoneRankList();
	void	UpdateDiscountItemEventItemPrice();
	void	UpdateShoppingEventLoginTimeMission();
	void	CheckAndSendFriendInviteKingRankList();
	void	UpdatePotionMakingEventLoginTimeMission();
	void	CheckAndLoadHippocampusRankList();
	void	CheckComebackBenefitAttendance();
	void	ResetSteelBagMission();
	void	GetLoseLoadConfigInfo(SLOSE_LEAD_CONFIG& info); 

	void	SetNewPCRoom(BOOL b ) { m_bIsNewPCRoom = b; }
	BOOL	GetNewPCRoom(void) { return m_bIsNewPCRoom;	}

	void	GiveClubMissionReward(vector<SCLUB_MISSION_GIVE_REWARD_INFO>& vPresent);
	void	CheckAndUpdateUserAppraisalRanking();

protected:// 2010.02.24
	// 서버 Config
	// ktKim
	void			SetConfig();
	void			SetOptionFlag();
	void			SetOptionStatusMod(BOOL bFlag);
	void			SetStatusUpTable();
	void			SetLankUpManagerConfig();
	// End
	void			RemoveSlotPackageConfig();

	void			AddMatchingPoolCareTimeCount();

public:
	CUser*	GetUser(char* gameID)	
	{ 
		m_mapUserTotal.Lock(); 

		CUser* user = m_mapUserTotal.FindUser(gameID); 
		if(!user)
		{ 
			m_mapUserTotal.Unlock(); 
		} 
		return user;
	}

	CUser*	GetUser(int iGameIDIndex)	
	{ 
		m_mapUserTotal.Lock(); 

		CUser* user = m_mapUserTotal.FindUser(iGameIDIndex); 
		if(!user)
		{ 
			m_mapUserTotal.Unlock(); 
		} 
		return user;
	}

	void	ReleaseUser(CUser* user) 
	{ 
		if(user)
		{ 
			m_mapUserTotal.Unlock();
		}
	}

	void			FactionTimer();
		
private:
	CFSItemShop			* m_pItemShop;
	CFSSkillShop		* m_pSkillShop;
	CFSTrainingShop		* m_pTrainingShop;
	CActionShop*		m_pActionShop;
	CLimitedEditionItemManager*	m_pLimitedEditionItemManager;
	CLimitedTimeItemManager* m_pLimitedTimeItemManager;

	CBasketBallManager		m_BasketBallManager;

	int	m_iFSTimeCount;

	BOOL m_bSetUserLog;

	CGameIDStringChecker m_GameIDStringChecker;

	BOOL m_bEnable;

	bool m_bCheckLog;

	COpUserInfoList		* m_pOpUserInfoList;		

	// 20090316 Tournament StatusFlag
	BOOL				m_bTournamentEnable;

	// 20090518 Abuse user penalty reinforcement status flag.
	BOOL				m_bAbusePenaltyReinforcementEnable;
	int					m_iAbusePenaltyDisconnectPointCondition;
	
	// 20090402 Write Login Request Count Log
	BOOL		m_bUseLoginRequestLog;
	DWORD		m_dwLoginRequestCount;

	int			m_iMatchMode;			// 기본 : 0, 3대3 제한 : 1
	SContentsConfig		m_ContentsConfig;
	BOOL				m_bNoWithdraw; // 20080616 초보유저 채널 강퇴 금지
	int					m_iEqualizeLV;
	BOOL				m_bIsEqualizeChannel;
	DWORD				m_dwShoutTime;
	BOOL				m_bShout;

// 20090917 Leveling
	vector<int>	m_vecLevelingCurve;
// End
	vector<SMaxLevelStepConfig>	m_vecMaxLevelStepLv;

	// 2010.02.23
	// 스탯보정
	// ktKim
	CStatusUpTable		m_StatusUpTable;
	// End
	// 2010.02.23
	// 옵션화
	// ktKim
	BOOL				m_bOptionFlagStatusMod;
	
	CLankUpManager		m_LankUpManager;

	// 20070809 Single Contents Merge
	CTypeList< SSingleContent > m_SingleContentsList;
	CTypeList< SSingleContentBonus > m_SingleContentBonusList;
	// End

	BOOL m_bUseSingleTrainingLastStageBonus;	// 20090401 Add member for using event option - use single training last stage bonus. 

	// 20070823 Add Story Mode
	CTypeList< SEpisode >* m_paEpisodeList[MAX_EPISODE_GROUP_COUNT];
	// End

	DWORD				m_dwForbidChatInterval;
	bool	m_bUseLuckItem;      
	int		m_iUserInfoProtVerID;
	BOOL	m_bEnableAvatarFame;
	vector<STCHAT_REPEAT_BAN>		vecChatRepeatBan;

	int m_iMatchingPoolCareTimeCount;

	time_t	m_tCurrentDBDate;
	BOOL	m_bItemShopEventButton;

	typedef map<int/*iGameIDIndex*/,CFSGameUser*> MAP_MATCHING_LIMIT_USER;
	MAP_MATCHING_LIMIT_USER		m_mapRankMatchLimitUser;
	LOCK_RESOURCE(m_csRankMatchLimitUser);

	SLOSE_LEAD_CONFIG			m_sLoseLeadConfig;

	BOOL			m_bIsNewPCRoom;

public:
	CCompEventManager		m_ComponentEventManager;

	DWORD					m_CrashCheckPacketNum[CRASH_CHECK_NUM];
	DWORD					m_CrashCheckTickAvg[CRASH_CHECK_NUM];
};

class CScopedRefGameUser
{
public:
	explicit CScopedRefGameUser(int iGameIDIndex)
	{
		m_pObj = (CFSGameUser*) CFSGameServer::GetInstance()->GetUser(iGameIDIndex);

		if (NULL != m_pObj && NULL != m_pObj->GetClient())
		{
			m_pObj->GetClient()->AddRef();
			CFSGameServer::GetInstance()->ReleaseUser(m_pObj);
		}
	}

	explicit CScopedRefGameUser(char* szGameID)
	{
		m_pObj = (CFSGameUser*) CFSGameServer::GetInstance()->GetUser(szGameID);

		if (NULL != m_pObj && NULL != m_pObj->GetClient())
		{
			m_pObj->GetClient()->AddRef();
			CFSGameServer::GetInstance()->ReleaseUser(m_pObj);
		}
	}

	~CScopedRefGameUser()
	{
		if (NULL != m_pObj && NULL != m_pObj->GetClient())
		{
			m_pObj->GetClient()->ReleaseRef();
		}
	}

	bool operator==(CFSGameUser* obj) { return m_pObj == obj; };
	bool operator!=(CFSGameUser* obj) { return m_pObj != obj; };
	CFSGameUser* operator->()	{	return m_pObj; };
	CFSGameUser* GetPointer() {	return m_pObj; };

private:
	CFSGameUser* m_pObj;
};

#define _GetCurrentDBDate CFSGameServer::GetInstance()->GetCurrentDBDate()
#define _GetCurrentDBDate_SystemTime CFSGameServer::GetInstance()->GetCurrentDBDateToSystemTime()
#define _GetServerIndex CFSGameServer::GetInstance()->GetServerIndex()