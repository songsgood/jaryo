qinclude "stdafx.h"
qinclude "FSGameUserSubCharDevelop.h"

qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "SubCharDevelopManager.h"

qinclude "CFSGameDBCfg.h"

extern CFSGameDBCfg* g_pFSDBCfg;

CFSGameUserSubCharDevelop::CFSGameUserSubCharDevelop(CFSGameUser* pUser)
	: m_pUser(pUser)
{
}
CFSGameUserSubCharDevelop::~CFSGameUserSubCharDevelop(void)
{
}

BOOL CFSGameUserSubCharDevelop::Load(void)
{
	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);

	if(ODBC_RETURN_SUCCESS != pODBCBase->SUBCHAR_DEVELOP_GetKeepExp(m_pUser->GetUserIDIndex(), m_mapExpUserInfo))
	{
		WRITE_LOG_NEW(LOG_TYPE_SUBCHAR_DEVELOP, DB_DATA_LOAD, FAIL, "SUBCHAR_DEVELOP_GetKeepExp failed");
		return FALSE;
	}

	return TRUE;
}

int CFSGameUserSubCharDevelop::ConvertPositionCode(BYTE btSubCharPositionType)
{
	switch(btSubCharPositionType)
	{
	case SUB_CHAR_DEVELOP_POSITION_CENTER:			return POSITION_CODE_CENTER;
	case SUB_CHAR_DEVELOP_POSITION_POWER_FOWARD:	return POSITION_CODE_POWER_FOWARD;
	case SUB_CHAR_DEVELOP_POSITION_SMALL_FOWARD:	return POSITION_CODE_SMALL_FOWARD;
	case SUB_CHAR_DEVELOP_POSITION_POINT_GUARD:		return POSITION_CODE_POINT_GUARD;
	case SUB_CHAR_DEVELOP_POSITION_SHOOT_GUARD:		return POSITION_CODE_SHOOT_GUARD;
	case SUB_CHAR_DEVELOP_POSITION_SWING_MAN:		return POSITION_CODE_SWING_MAN;
	}

	return POSITION_CODE_NO_POSITION;
}

BYTE CFSGameUserSubCharDevelop::ConvertPositionType(int iPositionCode)
{
	switch(iPositionCode)
	{
	case POSITION_CODE_CENTER:			return SUB_CHAR_DEVELOP_POSITION_CENTER;
	case POSITION_CODE_POWER_FOWARD:	return SUB_CHAR_DEVELOP_POSITION_POWER_FOWARD;
	case POSITION_CODE_SMALL_FOWARD:	return SUB_CHAR_DEVELOP_POSITION_SMALL_FOWARD;
	case POSITION_CODE_POINT_GUARD:		return SUB_CHAR_DEVELOP_POSITION_POINT_GUARD;
	case POSITION_CODE_SHOOT_GUARD:		return SUB_CHAR_DEVELOP_POSITION_SHOOT_GUARD;
	case POSITION_CODE_SWING_MAN:		return SUB_CHAR_DEVELOP_POSITION_SWING_MAN;
	}

	return MAX_SUB_CHAR_DEVELOP_POSITION_TYPE;
}

BOOL CFSGameUserSubCharDevelop::GetMaxExpCharacter(MY_AVATAR_LIST* pAvatarList, int iPositionCode, SMyAvatar* pMaxAvatar)
{
	BOOL bIsFind = FALSE;

	SMyAvatar* pTempMaxAvatar = nullptr;

	MY_AVATAR_LIST::iterator iter = pAvatarList->begin();
	MY_AVATAR_LIST::iterator iter_end = pAvatarList->end();
	for(iter; iter != iter_end; ++iter)
	{
		if(iPositionCode != (*iter)->iGamePosition ||
			AVATAR_LV_GRADE_AMATURE == GET_AVATAR_LV_GRADE((*iter)->iLv))
		{
			continue;
		}

		int iMaxExp = (nullptr == pTempMaxAvatar) ? 0 : pTempMaxAvatar->iExp;
		if(iMaxExp <= (*iter)->iExp)
		{
			bIsFind = TRUE;

			pTempMaxAvatar = (*iter);
		}
	}

	if(nullptr != pTempMaxAvatar)
	{
		memcpy(pMaxAvatar, pTempMaxAvatar, sizeof(SMyAvatar));
	}

	return bIsFind;
}

void CFSGameUserSubCharDevelop::SendExpUserInfo(void)
{
	CPacketComposer Packet(S2C_SUB_CHAR_DEVELOP_INFO_RES);

	SS2C_SUB_CHAR_DEVELOP_INFO_RES rs;
	for(BYTE bt = 0; bt < MAX_SUB_CHAR_DEVELOP_POSITION_TYPE; ++bt)
	{
		MAP_SUB_CHAR_DEVELOP_EXP_USER_INFO::iterator iter = m_mapExpUserInfo.find(bt);
		if(m_mapExpUserInfo.end() == iter)
		{
			rs.iPositionExp[bt] = 0;
			continue;
		}
		else
		{
			rs.iPositionExp[bt] = m_mapExpUserInfo[bt];
		}
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_INFO_RES));

	m_pUser->Send(&Packet);
}

void CFSGameUserSubCharDevelop::SendAvatarInfo(BYTE btPositionType)
{
	int iPositionCode = ConvertPositionCode(btPositionType);
	if(POSITION_CODE_NO_POSITION == iPositionCode)
	{
		CPacketComposer Packet(S2C_SUB_CHAR_DEVELOP_EXP_INFO_RES);

		SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES rs;
		rs.btResult = RESULT_SUB_CHAR_EXP_INFO_FAIL;
		rs.iPositionExp = 0;
		rs.btOption = SUB_CHAR_DEVELOP_LIST_OPTION_START;
		rs.iCharacterCount = 0;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES));
		m_pUser->Send(&Packet);

		return;
	}

	MY_AVATAR_LIST& listMyAvatar = m_pUser->GetListMyAvatar();
	if(true == listMyAvatar.empty())
	{
		m_pUser->LoadMyAvatarList();
	}

	SMyAvatar tMaxExpAvatar;
	if(FALSE == GetMaxExpCharacter(&listMyAvatar, iPositionCode, &tMaxExpAvatar))
	{
		CPacketComposer Packet(S2C_SUB_CHAR_DEVELOP_EXP_INFO_RES);

		SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES rs;
		rs.btResult = RESULT_SUB_CHAR_EXP_INFO_FAIL_NO_AVATAR;
		rs.iPositionExp = 0;
		rs.btOption = SUB_CHAR_DEVELOP_LIST_OPTION_START;
		rs.iCharacterCount = 0;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES));
		m_pUser->Send(&Packet);

		return;
	}

	MY_AVATAR_LIST listTempMyAvatar;
	MY_AVATAR_LIST::iterator iter_Avatar = listMyAvatar.begin();
	MY_AVATAR_LIST::iterator iter_Avatar_end = listMyAvatar.end();
	for(iter_Avatar; iter_Avatar != iter_Avatar_end; ++iter_Avatar)
	{
		if(iPositionCode != (*iter_Avatar)->iGamePosition || 
			GAME_LVUP_MAX == (*iter_Avatar)->iLv ||
			AVATAR_LV_GRADE_AMATURE == GET_AVATAR_LV_GRADE((*iter_Avatar)->iLv) ||
			tMaxExpAvatar.iExp <= (*iter_Avatar)->iExp)
		{
			continue;
		}

		listTempMyAvatar.push_back(*iter_Avatar);
	}

	if(0 == listTempMyAvatar.size())
	{
		CPacketComposer Packet(S2C_SUB_CHAR_DEVELOP_EXP_INFO_RES);

		SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES rs;
		rs.btResult = RESULT_SUB_CHAR_EXP_INFO_FAIL_NO_AVATAR;
		rs.iPositionExp = 0;
		rs.btOption = SUB_CHAR_DEVELOP_LIST_OPTION_START;
		rs.iCharacterCount = 0;

		Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES));
		m_pUser->Send(&Packet);

		return;
	}

	int iKeepExp = 0;
	MAP_SUB_CHAR_DEVELOP_EXP_USER_INFO::iterator iter = m_mapExpUserInfo.find(btPositionType);
	if(m_mapExpUserInfo.end() != iter)
	{
		iKeepExp = iter->second;
	}

	CPacketComposer Packet(S2C_SUB_CHAR_DEVELOP_EXP_INFO_RES);

	//start
	SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES rs;
	rs.btResult = RESULT_SUB_CHAR_EXP_INFO_SUCCESS;
	rs.iPositionExp = iKeepExp;
	rs.btOption = SUB_CHAR_DEVELOP_LIST_OPTION_START;
	rs.iCharacterCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES));

	m_pUser->Send(&Packet);

	//list
	Packet.Initialize(S2C_SUB_CHAR_DEVELOP_EXP_INFO_RES);

	rs.btResult = RESULT_SUB_CHAR_EXP_INFO_SUCCESS;
	rs.iPositionExp = iKeepExp;
	rs.btOption = SUB_CHAR_DEVELOP_LIST_OPTION_SEND;
	rs.iCharacterCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES));

	int iCount = 0;
	PBYTE pCount = Packet.GetTail() - sizeof(int);

	MY_AVATAR_LIST::iterator iter_Temp_Avatar = listTempMyAvatar.begin();
	MY_AVATAR_LIST::iterator iter_Temp_Avatar_end = listTempMyAvatar.end();
	for(iter_Temp_Avatar; iter_Temp_Avatar != iter_Temp_Avatar_end; ++iter_Temp_Avatar)
	{
		SUSER_SUB_CHAR_INFO info;
		info.iGameIDIndex = (*iter_Temp_Avatar)->iGameIDIndex;
		strncpy_s(info.szGameID, _countof(info.szGameID), (*iter_Temp_Avatar)->szGameID, MAX_GAMEID_LENGTH);
		info.iLv = (*iter_Temp_Avatar)->iLv;
		info.iMinExp = (*iter_Temp_Avatar)->iMinExp;
		info.iMaxExp = (*iter_Temp_Avatar)->iMaxExp;
		info.iExp = (*iter_Temp_Avatar)->iExp;
		info.iPosition = (*iter_Temp_Avatar)->iGamePosition;

		Packet.Add((PBYTE)&info, sizeof(SUSER_SUB_CHAR_INFO));

		++iCount;

		if(MAX_SEND_AVATAR_LIST_SIZE <= iCount)
		{
			memcpy(pCount, &iCount, sizeof(int));

			m_pUser->Send(&Packet);

			//list
			Packet.Initialize(S2C_SUB_CHAR_DEVELOP_EXP_INFO_RES);

			rs.btResult = RESULT_SUB_CHAR_EXP_INFO_SUCCESS;
			rs.iPositionExp = iKeepExp;
			rs.btOption = SUB_CHAR_DEVELOP_LIST_OPTION_SEND;
			rs.iCharacterCount = 0;

			Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES));

			iCount = 0;
			pCount = Packet.GetTail() - sizeof(int);
		}
	}

	memcpy(pCount, &iCount, sizeof(int));

	m_pUser->Send(&Packet);

	//end
	Packet.Initialize(S2C_SUB_CHAR_DEVELOP_EXP_INFO_RES);

	rs.btResult = RESULT_SUB_CHAR_EXP_INFO_SUCCESS;
	rs.iPositionExp = iKeepExp;
	rs.btOption = SUB_CHAR_DEVELOP_LIST_OPTION_END;
	rs.iCharacterCount = 0;

	Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_INFO_RES));

	m_pUser->Send(&Packet);
}

int CFSGameUserSubCharDevelop::AddExp(int iLevel, int iPositionCode, int iAddExp, short shEstEFF)
{
	CHECK_CONDITION_RETURN(AVATAR_LV_GRADE_AMATURE == GET_AVATAR_LV_GRADE(iLevel), 0);

	BYTE btPositionType = ConvertPositionType(iPositionCode);
	CHECK_CONDITION_RETURN(MAX_SUB_CHAR_DEVELOP_POSITION_TYPE == btPositionType, 0);

	MAP_SUB_CHAR_DEVELOP_EXP_USER_INFO::iterator iter = m_mapExpUserInfo.find(btPositionType);
	if(m_mapExpUserInfo.end() != iter)
	{
		if(SUBCHARDEVMANAGER.GetMaxSaveExp() <= m_mapExpUserInfo[btPositionType])
		{
			WRITE_LOG_NEW(LOG_TYPE_SUBCHAR_DEVELOP, DB_DATA_UPDATE, NONE, "CFSGameUserSubCharDevelop AddExp MAX EXP");
			return 0;
		}
	}

	if((GAME_LVUP_MAX == iLevel) && 
		(0 == iAddExp))
	{
		SFSConfigInfo GameResultConfig;
		ZeroMemory(&GameResultConfig, sizeof(SFSConfigInfo));

		g_pFSDBCfg->GetCfg(CFGCODE_EXP_POINT_16_50, (int)shEstEFF, GameResultConfig);
		iAddExp = GameResultConfig.iParam[CFSCODE_EXP_POINT_IDX_EXP];
	}

	int iTotalSaveExp = 0;
	int iRealAddExp = 0;

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBCBase, 0);

	int iResult = pODBCBase->SUBCHAR_DEVELOP_AddKeepExp(m_pUser->GetUserIDIndex(), btPositionType, iAddExp, iRealAddExp, iTotalSaveExp);
	if(ODBC_RETURN_SUCCESS != iResult)
	{
		if(1 != iResult)
		{
			WRITE_LOG_NEW(LOG_TYPE_SUBCHAR_DEVELOP, DB_DATA_UPDATE, FAIL, "SUBCHAR_DEVELOP_AddKeepExp failed error code: 10752790( 0, 17051648, -858993460, -858993460, -858993460 )", 
x(), btPositionType, iAddExp, iRealAddExp, iTotalSaveExp);
		}

		return 0;
	}

	m_mapExpUserInfo[btPositionType] = iTotalSaveExp;

	return iRealAddExp;
}

BYTE CFSGameUserSubCharDevelop::AcceptExpReq(int iGameIDIndex, int iAcceptExp, SS2C_SUB_CHAR_DEVELOP_EXP_ACCEPT_RES& rs)
{
	CHECK_CONDITION_RETURN(0 >= iAcceptExp, RESULT_SUB_CHAR_ACCEPT_EXP_FAIL_EXP_0);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_CONDITION_RETURN(NULL == pODBCBase, RESULT_SUB_CHAR_ACCEPT_EXP_FAIL);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_CONDITION_RETURN(NULL == pServer, RESULT_SUB_CHAR_ACCEPT_EXP_FAIL);

	MY_AVATAR_LIST& listMyAvatar = m_pUser->GetListMyAvatar();
	if(true == listMyAvatar.empty())
	{
		m_pUser->LoadMyAvatarList();
	}

	SMyAvatar tAcceptAvatar;
	CHECK_CONDITION_RETURN(FALSE == m_pUser->GetMyAvatar(iGameIDIndex, tAcceptAvatar), RESULT_SUB_CHAR_ACCEPT_EXP_FAIL);
	if(iGameIDIndex == m_pUser->GetGameIDIndex())
	{
		SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
		if(NULL != pAvatar)
		{
			tAcceptAvatar.iLv = pAvatar->iLv;
			tAcceptAvatar.iExp = pAvatar->iExp;
		}
	}

	CHECK_CONDITION_RETURN(AVATAR_LV_GRADE_AMATURE == GET_AVATAR_LV_GRADE(tAcceptAvatar.iLv), RESULT_SUB_CHAR_ACCEPT_EXP_FAIL_NOT_ENOUGH_LEVEL);

	BYTE btPositionType = ConvertPositionType(tAcceptAvatar.iGamePosition);
	MAP_SUB_CHAR_DEVELOP_EXP_USER_INFO::iterator iter = m_mapExpUserInfo.find(btPositionType);
	CHECK_CONDITION_RETURN(m_mapExpUserInfo.end() == iter || (iAcceptExp > iter->second), RESULT_SUB_CHAR_ACCEPT_EXP_FAIL_NOT_ENOUGH_EXP);

	SMyAvatar tMaxAvatar;
	CHECK_CONDITION_RETURN(FALSE == GetMaxExpCharacter(&listMyAvatar, tAcceptAvatar.iGamePosition, &tMaxAvatar), RESULT_SUB_CHAR_ACCEPT_EXP_FAIL);
	CHECK_CONDITION_RETURN(tMaxAvatar.iExp == tAcceptAvatar.iExp, RESULT_SUB_CHAR_ACCEPT_EXP_FAIL_THIS_MAX_AVATAR);

	const int iMaxAcceptExp = tMaxAvatar.iExp - tAcceptAvatar.iExp;

	int iRealAcceptExp = (iAcceptExp > iMaxAcceptExp) ? iMaxAcceptExp : iAcceptExp;

	int iPrevLevel = tAcceptAvatar.iLv;
	int iUpdateLevel = tAcceptAvatar.iLv;
	int iUpdateExp = tAcceptAvatar.iExp + iRealAcceptExp;

	while((GAME_LVUP_MAX > iUpdateLevel) && 
		(pServer->GetLvUpExp(iUpdateLevel + 1) <= iUpdateExp))
	{
		++iUpdateLevel;

		if(GAME_LVUP_MAX == iUpdateLevel)
		{
			iUpdateExp = pServer->GetLvUpExp(iUpdateLevel);
			break;
		}
	}

	if(ODBC_RETURN_SUCCESS != pODBCBase->SUBCHAR_DEVELOP_AcceptAvatarExp(m_pUser->GetUserIDIndex(), tAcceptAvatar.iGameIDIndex, btPositionType, iUpdateLevel, iUpdateExp, iRealAcceptExp))
	{
		WRITE_LOG_NEW(LOG_TYPE_SUBCHAR_DEVELOP, DB_DATA_UPDATE, FAIL, "SUBCHAR_DEVELOP_AcceptAvatarExp failed");
		return RESULT_SUB_CHAR_ACCEPT_EXP_FAIL;
	}

	if(iPrevLevel < iUpdateLevel)
	{
		m_pUser->ProcessLevelUp(iGameIDIndex, tAcceptAvatar.iGamePosition, iPrevLevel, iUpdateLevel, iUpdateExp);

		rs.bIsLevelUp = true;
	}
	else if(iPrevLevel == iUpdateLevel)
	{
		m_pUser->UpdateAvatarExp(tAcceptAvatar.iGameIDIndex, iUpdateLevel, iUpdateExp);

		if(tAcceptAvatar.iGameIDIndex == m_pUser->GetGameIDIndex())
		{
			SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
			if(NULL != pAvatar)
			{
				pAvatar->iLv = iUpdateLevel;
				pAvatar->iExp = iUpdateExp;		
			}
		}

		rs.bIsLevelUp = false;
	}

	iter->second -= iRealAcceptExp;

	rs.iRealAcceptExp = iRealAcceptExp;

	return (iRealAcceptExp == iAcceptExp) ? RESULT_SUB_CHAR_ACCEPT_EXP_SUCCESS : RESULT_SUB_CHAR_ACCEPT_EXP_SUCCESS_MAX_EXP;
}
