qinclude "stdafx.h"
qinclude "FSGameUserHalloweenGamePlayEvent.h"
qinclude "HalloweenGamePlayEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserHalloweenGamePlayEvent::CFSGameUserHalloweenGamePlayEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iCandy(0)
	, m_iGetCandy(0)
	, m_iGamePlayCnt(0)
	, m_iRemainTryCnt(0)
	, m_iBoxCnt(0)
{
}

CFSGameUserHalloweenGamePlayEvent::~CFSGameUserHalloweenGamePlayEvent(void)
{
}

BOOL CFSGameUserHalloweenGamePlayEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == HALLOWEENGAMEPLAYEVENT.IsOpen(), TRUE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_HALLOWEEN_GAMEPLAY_GetUserInfo(m_pUser->GetUserIDIndex(), m_iCandy, m_iGetCandy, m_iGamePlayCnt, m_iRemainTryCnt, m_iBoxCnt))
	{
		WRITE_LOG_NEW(LOG_TYPE_HALLOWEENGAMEPLAYEVENT, INITIALIZE_DATA, FAIL, "EVENT_HALLOWEEN_GAMEPLAY_GetUserInfo error");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserHalloweenGamePlayEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == HALLOWEENGAMEPLAYEVENT.IsOpen());

	SS2C_EVENT_HALLOWEEN_GAMEPLAY_INFO_RES rs;
	rs.iMyCandy = m_iCandy;
	rs.iRemainTryCnt = m_iRemainTryCnt;
	rs.iGetCandyCnt = m_iGetCandy;
	rs.iMyGamePlayCnt = m_iGamePlayCnt;
	rs.iMaxGamePlayCnt = HALLOWEENGAMEPLAYEVENT.GetMaxGamePlayCount();
	rs.iMyBoxCnt = m_iBoxCnt;
	rs.iItemCnt = 0;

	CPacketComposer Packet(S2C_EVENT_HALLOWEEN_GAMEPLAY_INFO_RES);
	HALLOWEENGAMEPLAYEVENT.MakeExchangeItemList(Packet, rs);
	m_pUser->Send(&Packet);
}

RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM CFSGameUserHalloweenGamePlayEvent::ExchangeItemReq(int iItemIndex, SS2C_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_RES& rs)
{
	CHECK_CONDITION_RETURN(CLOSED == HALLOWEENGAMEPLAYEVENT.IsOpen(), RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_FAIL_CLOSED_EVENT);

	int iNeedCandy;
	CHECK_CONDITION_RETURN(0 == (iNeedCandy = HALLOWEENGAMEPLAYEVENT.GetNeedCandy(iItemIndex)), RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_FAIL_ENOUGHT_CANDY);
	CHECK_CONDITION_RETURN(m_iCandy < iNeedCandy, RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_FAIL_ENOUGHT_CANDY);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_FAIL_FULL_MAILBOX);

	SRewardInfo sReward;
	HALLOWEENGAMEPLAYEVENT.GetExchangeItem(iItemIndex, sReward);
	SRewardConfig* pReward = REWARDMANAGER.GetReward(sReward.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_FAIL_ENOUGHT_CANDY);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_FAIL_ENOUGHT_CANDY);

	int iUpdatedCandy;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_HALLOWEEN_GAMEPLAY_ExchangeItem(m_pUser->GetUserIDIndex(), iNeedCandy, sReward.iRewardIndex, iUpdatedCandy), RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_FAIL_ENOUGHT_CANDY);

	m_iCandy = iUpdatedCandy;
	rs.iUpdatedCandy = iUpdatedCandy;

	pReward = REWARDMANAGER.GiveReward(m_pUser, sReward.iRewardIndex);
	if (NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_HALLOWEENGAMEPLAYEVENT, SET_DATA, FAIL, "ExchangeItemReq error - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
->GetUserIDIndex(), m_pUser->GetGameIDIndex(), sReward.iRewardIndex);
	}

	return RESULT_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_SUCCESS;
}

RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY CFSGameUserHalloweenGamePlayEvent::TryCandyReq(SS2C_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_RES& rs)
{
	CHECK_CONDITION_RETURN(CLOSED == HALLOWEENGAMEPLAYEVENT.IsOpen(), RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_FAIL_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(m_iRemainTryCnt <= 0, RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_FAIL_ENOUGHT_TRYCNT);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_FAIL_ENOUGHT_TRYCNT);
	
	int iAddCandy = 0;
	if(0 == m_iGetCandy)
		iAddCandy = 1;
	else
	{
		int iRandomRate = rand() ;
00;
		if(iRandomRate < 70)
			iAddCandy = 1;
	}

	BOOL bIsSucess = FALSE;
	if(1 == iAddCandy)
		bIsSucess = TRUE;

	int iUpdatedCandy; 
	int iUpdatedGetCandy; 
	int iUpdatedRemainTryCnt;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_HALLOWEEN_GAMEPLAY_Try(m_pUser->GetUserIDIndex(), iAddCandy, bIsSucess, iUpdatedCandy, iUpdatedGetCandy, iUpdatedRemainTryCnt), RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_FAIL_ENOUGHT_TRYCNT);

	m_iCandy = iUpdatedCandy;
	m_iGetCandy = iUpdatedGetCandy;
	m_iRemainTryCnt = iUpdatedRemainTryCnt;

	rs.iUpdatedCandy = iUpdatedCandy;
	rs.iUpdatedGetCandyCnt = iUpdatedGetCandy;
	rs.iUpdatedRemainTryCnt = iUpdatedRemainTryCnt;

	CHECK_CONDITION_RETURN(FALSE == bIsSucess, RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_FAIL);

	return RESULT_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_SUCCESS;
}

RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX CFSGameUserHalloweenGamePlayEvent::OpenRandomBoxReq(SS2C_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_RES& rs)
{
	CHECK_CONDITION_RETURN(CLOSED == HALLOWEENGAMEPLAYEVENT.IsOpen(), RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(m_iBoxCnt <= 0, RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_FAIL_ENOUGHT_BOX);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_FAIL_FULL_MAILBOX);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_FAIL);

	int iRewardIndex = HALLOWEENGAMEPLAYEVENT.GetRandomRewardIndex();
	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_FAIL);

	rs.btRewardType = pReward->GetRewardType();
	rs.iItemCode = 0;
	rs.iPropertyType = 0;
	rs.iPropertyValue = pReward->GetPropertyValue();

	SRewardConfigItem* pRewardItem = dynamic_cast<SRewardConfigItem*>(pReward);
	if(NULL != pRewardItem)
	{
		rs.iItemCode = pRewardItem->iItemCode;
		rs.iPropertyType = pRewardItem->iPropertyType;
	}

	BOOL IsUse = TRUE; 
	int iUpdatedGamePlayCnt; 
	int iUpdatedRemainTryCnt; 
	int iUpdatedBoxCnt;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_HALLOWEEN_GAMEPLAY_UpdateUserInfo(m_pUser->GetUserIDIndex(), IsUse, iRewardIndex, iUpdatedGamePlayCnt, iUpdatedRemainTryCnt, iUpdatedBoxCnt), RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_FAIL);

	m_iGamePlayCnt = iUpdatedGamePlayCnt;
	m_iRemainTryCnt = iUpdatedRemainTryCnt;
	m_iBoxCnt = iUpdatedBoxCnt;

	pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
	if (NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_HALLOWEENGAMEPLAYEVENT, SET_DATA, FAIL, "OpenRandomBoxReq error - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
	}

	return RESULT_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_SUCCESS;
}

BOOL CFSGameUserHalloweenGamePlayEvent::AddTryCount()
{
	CHECK_CONDITION_RETURN(CLOSED == HALLOWEENGAMEPLAYEVENT.IsOpen(), FALSE);

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, FALSE);

	BOOL IsUse = FALSE; 
	int iUpdatedGamePlayCnt; 
	int iUpdatedRemainTryCnt; 
	int iUpdatedBoxCnt;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_HALLOWEEN_GAMEPLAY_UpdateUserInfo(m_pUser->GetUserIDIndex(), IsUse, 0, iUpdatedGamePlayCnt, iUpdatedRemainTryCnt, iUpdatedBoxCnt), FALSE);

	m_iGamePlayCnt = iUpdatedGamePlayCnt;
	m_iRemainTryCnt = iUpdatedRemainTryCnt;
	m_iBoxCnt = iUpdatedBoxCnt;

	return TRUE;
}
