qinclude "stdafx.h"
qinclude "DiscountItemEventManager.h"
qinclude "FSGameUserDiscountItemEvent.h"
qinclude "TUser.h"
qinclude "FSServer.h"
qinclude "RewardManager.h"
qinclude "UserHighFrequencyItem.h"

CFSGameUserDiscountItemEvent::CFSGameUserDiscountItemEvent(CUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_tMissionCheckTime(-1)
	, m_bSave(FALSE)
{
}

CFSGameUserDiscountItemEvent::~CFSGameUserDiscountItemEvent(void)
{
}

BOOL CFSGameUserDiscountItemEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == DISCOUNTITEMEVENT.IsOpen(), TRUE);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBC);

	time_t tCurrentTime = _time64(NULL);
	m_tMissionCheckTime = tCurrentTime;

	vector<SUserDiscountItemEventBuyLog> vLog;
	if (ODBC_RETURN_SUCCESS != pODBC->EVENT_DISCOUNT_ITEM_GetUserData(m_pUser->GetUserIDIndex(), m_UserEvent._iCurrentStep, m_UserEvent._iDiscountRate, m_UserEvent._iRemainSecond) ||
		ODBC_RETURN_SUCCESS != pODBC->EVENT_DISCOUNT_ITEM_GetUserBuyLog(m_pUser->GetUserIDIndex(), vLog))
	{
		WRITE_LOG_NEW(LOG_TYPE_DISCOUNTITEM_EVENT, INITIALIZE_DATA, FAIL, "EVENT_DISCOUNT_ITEM_GetUserData error");
		return FALSE;
	}

	for(int i = 0; i < vLog.size(); ++i)
	{
		m_mapUserBuyLog[vLog[i]._iStep] = vLog[i];
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserDiscountItemEvent::UpdateItemPrice(BOOL bLogout)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == DISCOUNTITEMEVENT.IsOpen());

	time_t tCurrentDate = _time64(NULL);

	SUserDiscountItemEvent UserEvent;
	memcpy(&UserEvent, &m_UserEvent, sizeof(SUserDiscountItemEvent));

	int iStep = DISCOUNTITEMEVENT.GetCurrentStep();
	if(m_UserEvent._iCurrentStep != iStep)
	{
		CHECK_CONDITION_RETURN_VOID(m_UserEvent._iCurrentStep >= DISCOUNTITEMEVENT.GetMaxStep());
	
		UserEvent._iCurrentStep = iStep;
		UserEvent._iDiscountRate = 0;
		UserEvent._iRemainSecond = DISCOUNTITEMEVENT.GetClearSecond();
	}
	else
	{
		EVENT_DISCOUNT_ITEM_BUY_LOG_MAP::iterator iter = m_mapUserBuyLog.find(iStep);		
		if((TRUE == bLogout && FALSE == m_bSave) ||
			FALSE == bLogout)
		{
			CHECK_CONDITION_RETURN_VOID(iter != m_mapUserBuyLog.end());
			CHECK_CONDITION_RETURN_VOID(100 == m_UserEvent._iDiscountRate);
		}

		UserEvent._iRemainSecond = m_UserEvent._iRemainSecond - (tCurrentDate - m_tMissionCheckTime);
		if(m_UserEvent._iRemainSecond > 0)
			m_bSave = TRUE;

		if(UserEvent._iRemainSecond < 0)
			UserEvent._iRemainSecond = 0;

		UserEvent._iDiscountRate = m_UserEvent._iDiscountRate;
		if(0 == UserEvent._iRemainSecond && 
			100 > UserEvent._iDiscountRate &&
			iter == m_mapUserBuyLog.end())
		{
			UserEvent._iDiscountRate += DISCOUNTITEMEVENT.GetDiscountRate();
			UserEvent._iRemainSecond = DISCOUNTITEMEVENT.GetClearSecond();

			m_bSave = TRUE;
		}
	}
	
	if(TRUE == bLogout &&
		TRUE == m_bSave)
	{
		CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBC);

		CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->EVENT_DISCOUNT_ITEM_UpdateUserData(m_pUser->GetUserIDIndex(), UserEvent._iCurrentStep, UserEvent._iDiscountRate, UserEvent._iRemainSecond));
	}
	
	m_UserEvent._iCurrentStep = UserEvent._iCurrentStep;
	m_UserEvent._iDiscountRate = UserEvent._iDiscountRate;
	m_UserEvent._iRemainSecond = UserEvent._iRemainSecond;

	m_tMissionCheckTime = tCurrentDate;
}

BOOL CFSGameUserDiscountItemEvent::CheckEventUser()
{
	EVENT_DISCOUNT_ITEM_BUY_LOG_MAP::iterator iter = m_mapUserBuyLog.find(DISCOUNTITEMEVENT.GetMaxStep());
	CHECK_CONDITION_RETURN(iter != m_mapUserBuyLog.end(), FALSE);

	return TRUE;
}

void CFSGameUserDiscountItemEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == DISCOUNTITEMEVENT.IsOpen());
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	UpdateItemPrice();

	SS2C_EVENT_DISCOUNT_ITEM_INFO_RES rs;
	rs.iCurrentStep = m_UserEvent._iCurrentStep;
	rs.bIsBuy = FALSE;
	EVENT_DISCOUNT_ITEM_BUY_LOG_MAP::iterator iter = m_mapUserBuyLog.find(DISCOUNTITEMEVENT.GetCurrentStep());
	if(iter != m_mapUserBuyLog.end())
		rs.bIsBuy = TRUE;

	rs.iRemainSecond = m_UserEvent._iRemainSecond;
	rs.iDiscountRate = m_UserEvent._iDiscountRate;

	CPacketComposer Packet(S2C_EVENT_DISCOUNT_ITEM_INFO_RES);
	DISCOUNTITEMEVENT.MakeEventInfo(Packet, rs);
	m_pUser->Send(&Packet);
}

RESULT_EVENT_DISCOUNT_ITEM_BUY CFSGameUserDiscountItemEvent::CheckBuyReq(int iStep, SRewardInfo& sReward, int& iPrice, int& iDiscountPrice)
{
	CHECK_CONDITION_RETURN(CLOSED == DISCOUNTITEMEVENT.IsOpen(), RESULT_EVENT_DISCOUNT_ITEM_BUY_CLOSED_EVENT);
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, RESULT_EVENT_DISCOUNT_ITEM_BUY_FAIL);

	UpdateItemPrice();

	CHECK_CONDITION_RETURN(m_UserEvent._iCurrentStep != iStep, RESULT_EVENT_DISCOUNT_ITEM_BUY_NOT_BUYING_TIME);

	EVENT_DISCOUNT_ITEM_BUY_LOG_MAP::iterator iter = m_mapUserBuyLog.find(iStep);
	if(iter != m_mapUserBuyLog.end())
		return RESULT_EVENT_DISCOUNT_ITEM_BUY_ALREADY_BUY;

	iPrice = DISCOUNTITEMEVENT.GetPrice(iStep);
	iDiscountPrice = DISCOUNTITEMEVENT.GetDiscountPrice(iStep, m_UserEvent._iDiscountRate);
	CHECK_CONDITION_RETURN(m_pUser->GetCoin()+m_pUser->GetEventCoin() < iDiscountPrice, RESULT_EVENT_DISCOUNT_ITEM_BUY_NOT_EXIST_CASH);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_EVENT_DISCOUNT_ITEM_BUY_FULL_MAILBOX);

	CHECK_CONDITION_RETURN(FALSE == DISCOUNTITEMEVENT.GetRewardInfo(iStep, sReward), RESULT_EVENT_DISCOUNT_ITEM_BUY_FAIL);
	CHECK_CONDITION_RETURN(sReward.iRewardIndex <= 0, RESULT_EVENT_DISCOUNT_ITEM_BUY_FAIL);

	return RESULT_EVENT_DISCOUNT_ITEM_BUY_SUCCESS;
}

BOOL CFSGameUserDiscountItemEvent::BuyDiscountItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_DISCOUNTITEM_EVENT, INVALED_DATA, CHECK_FAIL, "BuyDiscountItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	int iStep = pBillingInfo->iParam5;
	int iRewardIndex = pBillingInfo->iParam6;
	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iPresentIndex;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_DISCOUNT_ITEM_BuyItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iStep, iRewardIndex, m_UserEvent._iDiscountRate, pBillingInfo->iItemPrice, pBillingInfo->iCashChange, iPrevCash, iPostCash, iPresentIndex), FALSE);

	m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iPresentIndex);

	SUserDiscountItemEventBuyLog Log;
	Log._iStep = iStep;
	Log._iRewardIndex = iRewardIndex;
	Log._iDiscountRate = m_UserEvent._iDiscountRate;
	m_mapUserBuyLog[iStep] = Log;

	m_pUser->SetUserBillResultAtMem(pBillingInfo->iSellType, iPostCash, pBillingInfo->iPointChange, 0, pBillingInfo->iBonusCoin);
	m_pUser->SendUserGold();

	SS2C_EVENT_DISCOUNT_ITEM_BUY_RES rs;
	rs.btResult = RESULT_EVENT_DISCOUNT_ITEM_BUY_SUCCESS;
	rs.iStep = iStep;
	m_pUser->Send(S2C_EVENT_DISCOUNT_ITEM_BUY_RES, &rs, sizeof(SS2C_EVENT_DISCOUNT_ITEM_BUY_RES));

	return TRUE;
}