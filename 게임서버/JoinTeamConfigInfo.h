qpragma once

class CFSODBCBase;

class CJoinTeamConfigInfo
{
public:
	CJoinTeamConfigInfo(void);
	~CJoinTeamConfigInfo(void);

	void	Load(int iGameIDIndex, CFSODBCBase* pODBC, SS2C_JOIN_TEAM_CONFIG_INFO& info);
	void	Save(CFSODBCBase* pODBC);

	SJoinTeamConfig* GetConfig()		{	return &m_JoinConfig; };

	void	SetPositions(int iMatchType, int iMatchTeamScale, int iaPosition[MAX_USER_SLOT_CONFIG_5ON5], int iAvoidTeam = -1);
	void	SetPVEPosition(int iLOD, int iPosition1, int iPosition2, int iPlayerNum);
	int		GetPVEInfo(SC2S_PVE_CREATE_ROOM_REQ& rq);

private:
	int	m_iGameIDIndex;
	SJoinTeamConfig m_JoinConfig;
};

