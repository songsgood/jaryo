qpragma once

qinclude "FSODBCCommon.h"

class CFSGameUser;
class CFSODBCBase;

enum EVENT_GAMEOFDICE_DB_LOG_TYPE
{
	EVENT_GAMEOFDICE_DB_LOG_TYPE_NONE,
	EVENT_GAMEOFDICE_DB_LOG_TYPE_GIVE_DAILY_REWARD,
	EVENT_GAMEOFDICE_DB_LOG_TYPE_GIVE_GAMERESULT_REWARD,
	EVENT_GAMEOFDICE_DB_LOG_TYPE_DICE_ROLL,
};

struct SGameOfDiceUserInfo
{
	int _iCharacterIndex;
	int _iDiceTicket;
	int _iEventCoin;
	int _iBankEventCoin;
	int _iCurrentTileNumber;
	int _iRemainPrisonTurn;		// 감옥 남은턴
	BOOL _bUseDiceTicket;		// 감옥 티켓사용여부(선택팝업창띄울때:TRUE,더블도전,다음턴탈출선택이후:FALSE)
	int _iRemainGoldCalfTurn;	// 황금송아지 남은턴
	int _iRemainFreeTurn;		// 더블 남은턴
	int _iRemainMiniGameTurn;	// 미니게임 남은턴
	int _iRankGameIDIndex;
	char _szRankeGameID[MAX_GAMEID_LENGTH+1];
	int _iDailyRewardUpdateDate;// 마지막 접속보상 지급일

	void SetUserInfo(SODBCGameOfDiceUserInfo ODBCUserInfo)
	{
		_iCharacterIndex = ODBCUserInfo._iCharacterIndex;
		_iDiceTicket = ODBCUserInfo._iDiceTicket;
		_iEventCoin = ODBCUserInfo._iEventCoin;
		_iBankEventCoin = ODBCUserInfo._iBankEventCoin;
		_iCurrentTileNumber = ODBCUserInfo._iCurrentTileNumber;
		_iRemainPrisonTurn = ODBCUserInfo._iRemainPrisonTurn;
		_bUseDiceTicket = ODBCUserInfo._bUseDiceTicket;
		_iRemainGoldCalfTurn = ODBCUserInfo._iRemainGoldCalfTurn;
		_iRemainFreeTurn = ODBCUserInfo._iRemainFreeTurn;
		_iRemainMiniGameTurn = ODBCUserInfo._iRemainMiniGameTurn;
		_iRankGameIDIndex = ODBCUserInfo._iRankGameIDIndex;
		memcpy(_szRankeGameID, ODBCUserInfo._szRankeGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);
		_iDailyRewardUpdateDate = ODBCUserInfo._iDailyRewardUpdateDate;
	}

	void GetUserInfo(SODBCGameOfDiceUserInfo& ODBCUserInfo)
	{
		ODBCUserInfo._iCharacterIndex = _iCharacterIndex;
		ODBCUserInfo._iDiceTicket = _iDiceTicket;
		ODBCUserInfo._iEventCoin = _iEventCoin;
		ODBCUserInfo._iBankEventCoin = _iBankEventCoin;
		ODBCUserInfo._iCurrentTileNumber = _iCurrentTileNumber;
		ODBCUserInfo._iRemainPrisonTurn = _iRemainPrisonTurn;
		ODBCUserInfo._bUseDiceTicket = _bUseDiceTicket;
		ODBCUserInfo._iRemainGoldCalfTurn = _iRemainGoldCalfTurn;
		ODBCUserInfo._iRemainFreeTurn = _iRemainFreeTurn;
		ODBCUserInfo._iRemainMiniGameTurn = _iRemainMiniGameTurn;
		ODBCUserInfo._iRankGameIDIndex = _iRankGameIDIndex;
		memcpy(ODBCUserInfo._szRankeGameID, _szRankeGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);
		ODBCUserInfo._iDailyRewardUpdateDate = _iDailyRewardUpdateDate;
	}

	SGameOfDiceUserInfo()
	{
		ZeroMemory(this, sizeof(SGameOfDiceUserInfo));
	}
};

typedef map<int/*iItemIndex*/,int/*iBuyCount*/> GAMEOFDICE_USER_ITEM_LOG_MAP;

struct SGameOfDiceRankSelectAvatar
{
	int _iGameIDIndex;
	char _szGameID[MAX_GAMEID_LENGTH+1];
	int _iGamePosition;
	BYTE _btSex;
	int _iLv;
	int _iExp;
	time_t _tLastDate;

	SGameOfDiceRankSelectAvatar()
	{
		ZeroMemory(this, sizeof(SGameOfDiceRankSelectAvatar));
	}

	void SetAvatar(SODBCGameOfDiceRankSelectAvatar Avatar)
	{
		_iGameIDIndex = Avatar._iGameIDIndex;
		strncpy_s(_szGameID, _countof(_szGameID), Avatar._szGameID, MAX_GAMEID_LENGTH);
		_iGamePosition = Avatar._iGamePosition;
		_btSex = Avatar._btSex;
		_iLv = Avatar._iLv;
		_iExp = Avatar._iExp;
		_tLastDate = Avatar._tLastDate;
	}

	static bool SortByLv(SGameOfDiceRankSelectAvatar* pAvatar1, SGameOfDiceRankSelectAvatar* pAvatar2)
	{
		if(pAvatar1->_iLv == pAvatar2->_iLv)
		{
			return pAvatar1->_iExp > pAvatar2->_iExp;
		}

		return pAvatar1->_iLv > pAvatar2->_iLv;
	}
};
typedef map<int/*_iGameIDIndex*/, SGameOfDiceRankSelectAvatar> GAMEOFDICE_RANK_AVATAR_MAP;
typedef list<SGameOfDiceRankSelectAvatar*> GAMEOFDICE_RANK_AVATAR_LIST;

struct SGameOfDiceUserLog
{
	int _iTotalGetDiceTicket;
	int _iTotalGetEventCoin;
	int _iGoingTurnCount;
	int _iGoingPrisonCount;
	int _iMiniGameWinCount;
	int _iMaxBankEventCoin;
	int _iGoldCalfCount;

	SGameOfDiceUserLog()
	{
		ZeroMemory(this, sizeof(SGameOfDiceUserLog));
	}

	void SetLog(SODBCGameOfDiceUserLog Log)
	{
		_iTotalGetDiceTicket = Log._iTotalGetDiceTicket;
		_iTotalGetEventCoin = Log._iTotalGetEventCoin;
		_iGoingTurnCount = Log._iGoingTurnCount;
		_iGoingPrisonCount = Log._iGoingPrisonCount;
		_iMiniGameWinCount = Log._iMiniGameWinCount;
		_iMaxBankEventCoin = Log._iMaxBankEventCoin;
		_iGoldCalfCount = Log._iGoldCalfCount;
	}

	void GetLog(SODBCGameOfDiceUserLog& Log)
	{
		Log._iTotalGetDiceTicket = _iTotalGetDiceTicket;
		Log._iTotalGetEventCoin = _iTotalGetEventCoin;
		Log._iGoingTurnCount = _iGoingTurnCount;
		Log._iGoingPrisonCount = _iGoingPrisonCount;
		Log._iMiniGameWinCount = _iMiniGameWinCount;
		Log._iMaxBankEventCoin = _iMaxBankEventCoin;
		Log._iGoldCalfCount = _iGoldCalfCount;
	}

	void MakeLog(SS2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT& not)
	{
		not.iTotalGetEventCoin = _iTotalGetEventCoin;
		not.iGoingPrisonCount = _iGoingPrisonCount;
		not.iMiniGameWinCount = _iMiniGameWinCount;
		not.iMaxBankEventCoin = _iMaxBankEventCoin;
		not.iGoldCalfCount = _iGoldCalfCount;
	}
};

class CFSGameUserGameOfDiceEvent
{
public:
	CFSGameUserGameOfDiceEvent(CFSGameUser* pUser);
	~CFSGameUserGameOfDiceEvent(void);

	BOOL				Load();

	void				CheckAndGiveReward();
	void				SendEventInfo();
	void				SendRankAvatarList();
	void				MakeEventInfo(CPacketComposer& Packet, SS2C_GAMEOFDICE_EVENT_INFO_RES& rs);
	RESULT_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR SelectRankAvatarReq(int iGameIDIndex);
	RESULT_GAMEOFDICE_EVENT_SELECT_CHARACTER SelectCharacterReq(int iCharacterIndex);
	RESULT_GAMEOFDICE_EVENT_DICE_ROLL DiceRollReq(BYTE btDiceRollType, SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES& rs);
	RESULT_GAMEOFDICE_EVENT_BUY_ITEM BuyItemReq(int iItemIndex, CPacketComposer& Packet, SS2C_GAMEOFDICE_EVENT_BUY_ITEM_RES& rs);
	int					GiveGameResultReward();
	void				MakeIntegrateDB_UpdateUserLog(SS2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT& not);
	void				CheckAndUpdateRankGameID(CFSODBCBase* pODBC = nullptr);

	void				SetDiceTicket(int iPropertyValue) { m_UserGameOfDice._iDiceTicket = iPropertyValue; }
	char*				GetRankGameID() { return m_UserGameOfDice._szRankeGameID; }
	int					GetValue(BYTE btRankType);

	RESULT_GAMEOFDICE_EVENT_DICE_ROLL TestDiceRollReq(int iDiceNumber, BYTE& btDiceRollType, SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES& rs);
	void TestInitDiceTicket();

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	SGameOfDiceUserInfo m_UserGameOfDice;
	GAMEOFDICE_USER_ITEM_LOG_MAP m_mapUserItemLog;
	GAMEOFDICE_RANK_AVATAR_MAP m_mapRankSelectAvatar;
	GAMEOFDICE_RANK_AVATAR_LIST m_listRankSelectAvatar;
	SGameOfDiceUserLog m_UserLog;
};

