qpragma once
qinclude "Lock.h"

class CFSGameUser;
class CFSGameUserBasketBall
{
public:

	CFSGameUserBasketBall(CFSGameUser* pUser);
	~CFSGameUserBasketBall();

	BOOL				Load();
	void				SetInventory(BOOL bUseOption, int iaBallPartSlot[MAX_BASKETBALL_BALLPART], vector<SBasketBallInventoryInfo> vecInventory[MAX_BASKETBALL_BALLPART]);
	BOOL				SendUserUseOption();
	BOOL				SendUserInventory(BYTE btBallPartType);
	BOOL				SendBasketBallShopList(BYTE btBallPartType, BYTE btItemType);
	BOOL				SendBasketBallProductInfo(SC2S_BASKETBALL_PRODUCT_INFO_REQ& rs);
	BOOL				SendNewMarkInfo();
	BOOL				ChangeBasketBallSkin(int iaBallPartIndex[MAX_BASKETBALL_BALLPART]);
	BOOL				ChangeBasketBallOption(BOOL bUseOption);
	BOOL				BuyBasketBallSkin(SC2S_BUY_BASKETBALL_REQ& rs);
	BOOL				BuyBasketBallSkin_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	void				GetBallPartSlotInfo(BOOL& bUseOption, int iaBallPartSlot[MAX_BASKETBALL_BALLPART]);
	void				CheckAndGiveConditionBallSkin(BYTE btBallPartType);
	void				CheckAndRemoveBasketBall();
	BOOL				CheckAndRemoveExpireBallPartSlot();
	BOOL				CheckAndRemoveExpireBallPartInventory(BYTE btBallPartType);
	void				GiveReward_BasketBall(BYTE btBallPartType, int iBallPartIndex, int iPropertyValue);

private:
	LOCK_RESOURCE(m_SyncUserBasketBall);
	// Need Lock
	BOOL				m_bUseOption;
	int					m_iaBallPartSlot[MAX_BASKETBALL_BALLPART];
	BALLPARTINFO_VEC	m_vecInventory[MAX_BASKETBALL_BALLPART];
	BALLPARTINFO_MAP	m_mapInventory[MAX_BASKETBALL_BALLPART];
	//

	CFSGameUser*		m_pUser;

};