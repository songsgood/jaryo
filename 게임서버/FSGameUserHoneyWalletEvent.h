qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserHoneyWalletEvent
{
public:
	CFSGameUserHoneyWalletEvent(CFSGameUser* pUser);
	~CFSGameUserHoneyWalletEvent(void);

	BOOL			Load(void);
	BOOL			IsEventOnGoing(void);
	void			SendUserWalletData(void);
	void			CheckCanOpenWalletToday(void);
	BOOL			BuyHoneyWallet_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL			OpenHoneyWallet(void);
	BOOL			UpdateUserHoneyWalletDate(int iRewardIndex, time_t tOpenNowDate, BOOL bIsMainReward, BOOL& bIsReplay);
	BOOL			GiveWalletReward(int& iRewardIndex, BYTE& btResult);
	void			SendMainRewardShout(void);

	BOOL			IsBuyHoneyWallet(void){return m_bIsBuyHoneyWallet;}

private:
	CFSGameUser*	m_pUser;
	BOOL			m_bDataLoaded;
	BOOL			m_bIsBuyHoneyWallet;			// ������ ����°�,
	BOOL			m_bIsCanOpenWalletToday;		// ���� ������ �����ִ°�,
	int				m_iUserRemainWalletDay;
	time_t			m_tWalletOpenRecentDate;
	time_t			m_tWalletExpireDate;	
};
