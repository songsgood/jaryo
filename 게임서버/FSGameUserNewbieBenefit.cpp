qinclude "stdafx.h"
qinclude "FSGameUserNewbieBenefit.h"
qinclude "CFSGameUser.h"
qinclude "ThreadODBCManager.h"
qinclude "NewbieBenefitManager.h"

CFSGameUserNewbieBenefit::CFSGameUserNewbieBenefit(CFSGameUser* pUser)
	:m_pUser(pUser)
	,m_iCompleteLevel(0)
	,m_bIsNewbie(false)
{
}


CFSGameUserNewbieBenefit::~CFSGameUserNewbieBenefit(void)
{
}

BOOL CFSGameUserNewbieBenefit::LoadNewbieBenefit( void )
{
	CHECK_CONDITION_RETURN(false == NEWBIE.CheckNewbieCondition(m_pUser->GetAccountCreateTime()), TRUE);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBCBase);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	if( ODBC_RETURN_SUCCESS != pODBCBase->NEWBIE_BENEFIT_GetUserInfo(m_pUser->GetUserIDIndex(),m_iCompleteLevel) )
	{
		WRITE_LOG_NEW(LOG_TYPE_NEWBIEBENEFIT, INITIALIZE_DATA, FAIL, "LoadNewbieBenefit UserIDIndex(10752790)", m_pUser->GetUserIDIndex());
rn FALSE;
	}

	m_bIsNewbie = true;

	return TRUE;
}

void CFSGameUserNewbieBenefit::SendNewbieBenefitInfoNot( void )
{
	CHECK_CONDITION_RETURN_VOID(false == m_bIsNewbie);
	CHECK_CONDITION_RETURN_VOID(false == NEWBIE.CheckNewbieCondition(m_pUser->GetAccountCreateTime()));

	CPacketComposer Packet(S2C_NEWBIE_BENEFIT_LOBBY_BUTTON_NOT);
	SS2C_NEWBIE_BENEFIT_LOBBY_BUTTON_NOT not;
	not.btButtonEnable = FALSE;

	// 신규유저인데, 아직 받지 않은 혜택이 있을때
	int iRewardMaxLv = NEWBIE.GetConfigMaxLv();
	if( m_iCompleteLevel < iRewardMaxLv ) 
	{
		not.btButtonEnable = TRUE;	
	}

	Packet.Add((PBYTE)&not, sizeof(SS2C_NEWBIE_BENEFIT_LOBBY_BUTTON_NOT));
	m_pUser->Send(&Packet, 1);	
}

void CFSGameUserNewbieBenefit::SendNewbieBenefitList( void )
{
	CHECK_CONDITION_RETURN_VOID(false == m_bIsNewbie);
	CHECK_CONDITION_RETURN_VOID(false == NEWBIE.CheckNewbieCondition(m_pUser->GetAccountCreateTime()));

	CNewbieBenefitManager* pManager = &NEWBIE;
	CHECK_NULL_POINTER_VOID(pManager);

	CPacketComposer Packet(S2C_NEWBIE_BENEFIT_LIST_RES);

	SS2C_NEWBIE_BENEFIT_LIST_RES info;

	info.iUserMaxLevel = m_pUser->GetMaxAvatarLevel();
	
	int iMaxConditionLv = pManager->GetConfigMaxLv();
	if( iMaxConditionLv <  info.iUserMaxLevel ) // client ui 에서는 리워드 최고레벨까지만 보여진다.
		info.iUserMaxLevel = iMaxConditionLv;

	info.iListCount = pManager->GetListCount();
	info.btFocusLvCondition = pManager->NextCompleteLevel(m_iCompleteLevel);
	CHECK_CONDITION_RETURN_VOID(0 >= info.btFocusLvCondition)

	Packet.Add((PBYTE)&info, sizeof(SS2C_NEWBIE_BENEFIT_LIST_RES));
	pManager->MakePacketLvConditionList(Packet, info.btFocusLvCondition);

	m_pUser->Send(&Packet);

	//SendNewbieBenefitRewardList(info.btFocusLvCondition);
}

void CFSGameUserNewbieBenefit::SendNewbieBenefitRewardList( SC2S_NEWBIE_BENEFIT_REWARD_INFO_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(false == m_bIsNewbie);
	CHECK_CONDITION_RETURN_VOID(false == NEWBIE.CheckNewbieCondition(m_pUser->GetAccountCreateTime()));

	CHECK_CONDITION_RETURN_VOID(FALSE == NEWBIE.CheckGiveLv((int)rq.btLevelCondition));
	CPacketComposer Packet(S2C_NEWBIE_BENEFIT_REWARD_INFO_RES);

	SS2C_NEWBIE_BENEFIT_REWARD_INFO_RES info;
	info.btRewardButtonEnable = FALSE;

	const int iUserMaxLv = m_pUser->GetMaxAvatarLevel();
	const int iNextLv = NEWBIE.NextCompleteLevel(m_iCompleteLevel);

	if( iUserMaxLv >= iNextLv &&
		NEWBIE.GetConfigMaxLv() != m_iCompleteLevel &&
		iNextLv == rq.btLevelCondition )
	{
		info.btRewardButtonEnable = TRUE;
	}

	info.btRewardGaveEnable = TRUE;
	if( NEWBIE.GetConfigMaxLv() == m_iCompleteLevel ||
		(NEWBIE.GetConfigMaxLv() != m_iCompleteLevel && NEWBIE.NextCompleteLevel(m_iCompleteLevel) > rq.btLevelCondition) )
	{
		info.btRewardGaveEnable = FALSE; 
	}

	info.btRewardCount = NEWBIE.GetRewardCount(rq.btLevelCondition);

	Packet.Add((PBYTE)&info, sizeof(SS2C_NEWBIE_BENEFIT_REWARD_INFO_RES));
	NEWBIE.MakePacketNewbieRewardList(Packet, rq.btLevelCondition);

	m_pUser->Send(&Packet);
}

void CFSGameUserNewbieBenefit::SendNewbieBenefitRewardList( BYTE btTargetLv )
{
	SC2S_NEWBIE_BENEFIT_REWARD_INFO_REQ rq;
	rq.btLevelCondition = btTargetLv;

	SendNewbieBenefitRewardList(rq);
}

void CFSGameUserNewbieBenefit::GiveRewardNewbieBenefit( SC2S_NEWBIE_BENEFIT_RECEIVING_REQ& rq )
{
	CHECK_CONDITION_RETURN_VOID(false == m_bIsNewbie);
	CHECK_CONDITION_RETURN_VOID(false == NEWBIE.CheckNewbieCondition(m_pUser->GetAccountCreateTime()));

	CPacketComposer Packet(S2C_NEWBIE_BENEFIT_RECEIVING_RES);
	SS2C_NEWBIE_BENEFIT_RECEIVING_RES rs;

	int iMailClearance = MAX_PRESENT_LIST - m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize();
	const int iUserMaxLv = m_pUser->GetMaxAvatarLevel();
	const int iNextLv = NEWBIE.NextCompleteLevel(m_iCompleteLevel);

	if(iUserMaxLv < iNextLv || iUserMaxLv < rq.btLevelCondition)
		rs.btResult = RESULT_NEWBIE_BENEFIT_REWARD_FAIL;
	else if(FALSE == NEWBIE.CheckGiveLv((int)rq.btLevelCondition))
		rs.btResult = RESULT_NEWBIE_BENEFIT_REWARD_FAIL;
	else if( iMailClearance < NEWBIE.GetRewardCount(rq.btLevelCondition) )
		rs.btResult = RESULT_NEWBIE_BENEFIT_REWARD_MAILBOX_FULL;
	else
	{
		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
		CHECK_NULL_POINTER_VOID(pODBCBase);

		list<int> listPresentIndex;
		if( ODBC_RETURN_SUCCESS != pODBCBase->NEWBIE_BENEFIT_UpdateUserCompleteLv(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), rq.btLevelCondition, listPresentIndex))
		{
			WRITE_LOG_NEW(LOG_TYPE_NEWBIEBENEFIT, CALL_SP, FAIL, "NEWBIE_BENEFIT_UpdateUserCompleteLv UserIDIndex:(10752790), GiveLv(0)"
 m_pUser->GetUserIDIndex(), rq.btLevelCondition);
			rs.btResult = RESULT_NEWBIE_BENEFIT_REWARD_FAIL;			
		}
		else
		{
			m_iCompleteLevel = rq.btLevelCondition;

			rs.btResult = RESULT_NEWBIE_BENEFIT_REWARD_SUCCESS;

			m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);
		}
	}

	Packet.Add((PBYTE)&rs, sizeof(SS2C_NEWBIE_BENEFIT_RECEIVING_RES));
	m_pUser->Send(&Packet);
}

void CFSGameUserNewbieBenefit::AllRemoveNewbieBenefit( void )
{
	CHECK_CONDITION_RETURN_VOID(false == m_bIsNewbie);

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	if(ODBC_RETURN_SUCCESS != pODBCBase->NEWBIE_BENEFIT_RemoveUserInfo(m_pUser->GetUserIDIndex()))
	{
		WRITE_LOG_NEW(LOG_TYPE_NEWBIEBENEFIT, CALL_SP, FAIL, "NEWBIE_BENEFIT_RemoveUserInfo UserIDIndex:(10752790)", m_pUser->GetUserIDIndex());
	m_bIsNewbie = false;
}
