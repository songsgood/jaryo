qpragma once

qinclude "MatchBaseSvrProxy.h"
qinclude "ThreadODBCManager.h"

class CFSGameUser;

#define DECLARE_GAMEMATCHPROXY_PROC_FUNC(x) void Proc_##x(CReceivePacketBuffer*);
#define DEFINE_GAMEMATCHPROXY_PROC_FUNC(x)	void CMatchSvrProxy::Proc_##x(CReceivePacketBuffer* rp)
#define CASE_GAMEMATCHPROXY_PACKET_PROC_FUNC(x)	case x:	Proc_##x(recvPacket);	break;


class CMatchSvrProxy : public CMatchBaseSvrProxy
{
public:
	CMatchSvrProxy(void);
	virtual ~CMatchSvrProxy(void);

	BOOL IsAutoTeam()		{	return m_bIsAutoTeam; }
	void SetAutoTeam(BOOL bSet)		{	m_bIsAutoTeam = bSet; }

public:
	void			ProcessPacket(CReceivePacketBuffer* recvPacket);

	void			Process_UserLogin(CReceivePacketBuffer* rp);
	void			Process_TeamCreate(CReceivePacketBuffer* rp);
	void			Process_MotionStateData(CReceivePacketBuffer* rp);
	void			Process_BroadcastWaitTeamList(CReceivePacketBuffer* rp);
	void			Process_TeamMemberListRes(CReceivePacketBuffer* rp);
	void			Process_TeamMemberRes(CReceivePacketBuffer* rp);
	void			Process_TeamInfoInTeamRes(CReceivePacketBuffer* rp);
	void			Process_AvoidTeamRes(CReceivePacketBuffer* rp);
	void			Process_ChangeTeamAllowObserveRes(CReceivePacketBuffer* rp);
	void			Process_ChangeTeamNameRes(CReceivePacketBuffer* rp);
	void			Process_ChangeTeamPasswordRes(CReceivePacketBuffer* rp);
	void			Process_ChangeTeamModeRes(CReceivePacketBuffer* rp);
	void			Process_ChatRes(CReceivePacketBuffer* rp);
	void			Process_BroadcastChatRes(CReceivePacketBuffer* rp);
	void			Process_TeamEnter(CReceivePacketBuffer* rp);
	void			Process_TeamExit(CReceivePacketBuffer* rp);
	void			Process_GotoTeamModeReq(CReceivePacketBuffer* rp);
	void			Process_WaitTeamListRes(CReceivePacketBuffer* rp);
	void			Process_TeamListUserSet(CReceivePacketBuffer* rp);
	void			Process_ChiefAssignCheckRes(CReceivePacketBuffer* rp);
	void			Process_ForceOutUserRes(CReceivePacketBuffer* rp);
	void			Process_RegTeamRes(CReceivePacketBuffer* rp);
	void			Process_AutoRegRes(CReceivePacketBuffer* rp);
	void			Process_AutoTeamRegRes(CReceivePacketBuffer* rp);
	void			Process_AutoTeamMakeComplete(CReceivePacketBuffer* rp);
	void			Process_ReadyTeamListRes(CReceivePacketBuffer* rp);
	void			Process_ReadyTeamNOT(CReceivePacketBuffer* rp);
	void			Process_RequestTeamMatchRes(CReceivePacketBuffer* rp);
	void			Process_EnterReadyTeamRes(CReceivePacketBuffer* rp);
	void			Process_ChangeCourtRes(CReceivePacketBuffer* rp);
	void			Process_ChangePracticeMethodRes(CReceivePacketBuffer* rp);	
	void			Process_TeamChangeRes(CReceivePacketBuffer* rp);	
	void			Process_TeamChangeInGameRes(CReceivePacketBuffer* rp);	
	void			Process_TeamInfoRes(CReceivePacketBuffer* rp);
	void			Process_RoomInfoInRoomRes(CReceivePacketBuffer* rp);
	void			Process_InviteUserRes(CReceivePacketBuffer* rp);
	void			Process_FollowFriendRes(CReceivePacketBuffer* rp);
	void			Process_QuickRoomInfoRes(CReceivePacketBuffer* rp);
	void			Process_ChangePositionRes(CReceivePacketBuffer* rp);
	void			Process_EventViewTypeRes(CReceivePacketBuffer* rp);
	void			Process_BroadcastRoomInfo(CReceivePacketBuffer* rp);
	void			Process_CheckGameStartPreparationReq(CReceivePacketBuffer* rp);
	void			Process_CheckPublicIPAddressForGameReq(CReceivePacketBuffer* rp);
	void			Process_StartUdpHolepunchingReq(CReceivePacketBuffer* rp);
	void			Process_StartBandWidthCheckReq(CReceivePacketBuffer* rp);
	void			Process_NoticeWhoIsHost(CReceivePacketBuffer* rp);
	void			Process_ReadyHostReq(CReceivePacketBuffer* rp);
	void			Process_SetBroadCaster(CReceivePacketBuffer* rp);
	void			Process_GameStartRes(CReceivePacketBuffer* rp);
	void			Process_SetUserState(CReceivePacketBuffer* rp);
	void			Process_SetUserLocation(CReceivePacketBuffer* rp);
	void			Process_DisplayCounter(CReceivePacketBuffer* rp);
	void			Process_ConnectHostReq(CReceivePacketBuffer* rp);
	void			Process_ConnectHostJoinReq(CReceivePacketBuffer* rp);
	void			Process_VoiceChattingUserStatusInfoNot(CReceivePacketBuffer* rp);	
	void			Process_VoiceChattingUserOptionInfoRes(CReceivePacketBuffer* rp);	
	void			Process_VoiceChattingCloseNot(CReceivePacketBuffer* rp);	
	void			Process_InitialGameReq(CReceivePacketBuffer* rp);
	void			Process_GameInitializedDataNOT(CReceivePacketBuffer* rp);
	void			Process_PlayCountEventInfoRes(CReceivePacketBuffer* rp);
	void			Process_MacroItemProssession(CReceivePacketBuffer* rp);
	void			Process_InitStartGame(CReceivePacketBuffer* rp);
	void			Process_LoadGameReq(CReceivePacketBuffer* rp);
	void			Process_LoadingProgressRes(CReceivePacketBuffer* rp);
	void			Process_StartGameAsObserverReq(CReceivePacketBuffer* rp);
	void			Process_StatCheckPacket(CReceivePacketBuffer* rp);
	void			Process_StatCheckSumPacket(CReceivePacketBuffer* rp);
	void			Process_GiveupEnableRes(CReceivePacketBuffer* rp);
	void			Process_ThumbnailRecord(CReceivePacketBuffer* rp);
	void			Process_DisconnectClient(CReceivePacketBuffer* rp);
	void			Process_GameResultRes(CReceivePacketBuffer* rp);
	void			Process_GameEnd(CReceivePacketBuffer* rp);
	void			Process_CreateRoomResult(CReceivePacketBuffer* rp);
	void			Process_RoomInfo(CReceivePacketBuffer* rp);
	void			Process_ChangeGameRoomNameRes(CReceivePacketBuffer* rp);
	void			Process_TeamEstimateRes(CReceivePacketBuffer* rp);
	void			Process_JoinRoomResult(CReceivePacketBuffer* rp);	
	void			Process_CancelTeamMatchRes(CReceivePacketBuffer* rp);
	void			Process_ExitRoomResult(CReceivePacketBuffer* rp);
	void			Process_GameStartPauseRes(CReceivePacketBuffer* recvPacket);
	void			Process_GameTerminatedReq(CReceivePacketBuffer* recvPacket);
	void			Process_UpdateVariationTrophy(CReceivePacketBuffer* recvPacket);
	void			Process_UpdateTrophyAndChampionCnt(CReceivePacketBuffer* recvPacket);
	void			Process_QuickJoinGameAsObserverRes(CReceivePacketBuffer* recvPacket);
	void			Process_RemainPlayTimeInRoomRes(CReceivePacketBuffer* recvPacket);
	void			Process_RemainPlayScoreInRoomRes(CReceivePacketBuffer* recvPacket);
	void			Process_ExitObservingGameRes(CReceivePacketBuffer* recvPacket);
	void			Process_ExitObservingGameToTeamRes(CReceivePacketBuffer* recvPacket);
	void			Process_SendPacketQueue(CReceivePacketBuffer* recvPacket);
	void			Process_AbuseUserGame(CReceivePacketBuffer* recvPacket);
	void			Process_GameStartHalfTimeRes(CReceivePacketBuffer* recvPacket);
	void			Process_GameStartHalfTimeNowRes(CReceivePacketBuffer* recvPacket);
	void			Process_GiveUpVoteRes(CReceivePacketBuffer* recvPacket);
	void			Process_IntentionalFoulRes(CReceivePacketBuffer* recvPacket);
	void			Process_UsePauseItemRes(CReceivePacketBuffer* recvPacket);
	void			Process_DiscountPauseItemReq(CReceivePacketBuffer* recvPacket);
	void			Process_GamePauseRes(CReceivePacketBuffer* recvPacket);
	void			Process_UseSecondBoostItemRes(CReceivePacketBuffer* recvPacket);
	void			Process_SecondBoostItemExpiredRes(CReceivePacketBuffer* recvPacket);
	void			Process_RoomMemberRes(CReceivePacketBuffer* recvPacket);
	void			Process_GMChangeHost(CReceivePacketBuffer* recvPacket);
	void			Process_RemoveFeature(CReceivePacketBuffer* recvPacket);
	void			Process_DecreaseFeature(CReceivePacketBuffer* recvPacket);
	void			Process_BonusPointLevelUp(CReceivePacketBuffer* rp);
	void			Process_UpdateMaxLevelStepAndTakeItemReward(CReceivePacketBuffer* rp);
	void			Process_JumpBallPlayer(CReceivePacketBuffer* rp);
	void			Process_MatchFailNot(CReceivePacketBuffer* rp);
	void			Process_EntranceScene(CReceivePacketBuffer* rp);
	void			Process_ExitScene(CReceivePacketBuffer* rp);
	void			Process_CompleteAchievementInGame(CReceivePacketBuffer* rp);
	void			Process_AnnounceAchievementComplete(CReceivePacketBuffer* rp);
	void			Process_EquipAchievementTitleBroadcastRes(CReceivePacketBuffer* rp);
	void			Process_RecordStepInfoRes(CReceivePacketBuffer* rp);
	void			Process_RecvPresentNot(CReceivePacketBuffer* rp);
	void			Process_EventMessage(CReceivePacketBuffer* rp);
	void			Process_RecordEventSendMessage(CReceivePacketBuffer* rp);
	void			Process_SendEventComponentMsg(CReceivePacketBuffer* rp);
	void			Process_ChangeDecideOutcomeDetailRes(CReceivePacketBuffer* rp);
	void			Process_InviteUserListStart(CReceivePacketBuffer* rp);
	void			Process_InviteUserListInfo(CReceivePacketBuffer* rp);
	void			Process_InviteUserListEnd(CReceivePacketBuffer* rp);
	void			Process_TournamentListInfo(CReceivePacketBuffer* rp);		
	void			Process_TournamentWaitListInfo(CReceivePacketBuffer* rp);		
	void			Process_TournamentNotifyRes(CReceivePacketBuffer* rp);	
	void			Process_TournamentProgramInfoRes(CReceivePacketBuffer* rp);	
	void			Process_TournamentEnterRes(CReceivePacketBuffer* rp);		
	void			Process_TournamentExitRes(CReceivePacketBuffer* rp);
	void			Process_TournamentCreatePreMadeTeamRes(CReceivePacketBuffer* rp);
	void			Process_TournamentInviteUserRes(CReceivePacketBuffer* rp);
	void			Process_TournamentInviteSuggestReq(CReceivePacketBuffer* rp);
	void			Process_TournamentEnterPreMadeTeam(CReceivePacketBuffer* rp);
	void			Process_TournamentExitPreMadeTeamRes(CReceivePacketBuffer* rp);
	void			Process_TournamentRegisterPreMadeTeamRes(CReceivePacketBuffer* rp);
	void			Process_TournamentButtonInfoRes(CReceivePacketBuffer* rp);
	void			Process_TournamentMoveSeasonRes(CReceivePacketBuffer* rp);
	void			Process_TournamentChief(CReceivePacketBuffer* rp);
	void			Process_TournamentChangeNameRes(CReceivePacketBuffer* rp);
	void			Process_TournamentChangePasswordRes(CReceivePacketBuffer* rp);
	void			Process_TournamentReadyCount(CReceivePacketBuffer* rp);
	void			Process_TournamentChangeTeamRes(CReceivePacketBuffer* rp);
	void			Process_TournamentEnterMakingTeam(CReceivePacketBuffer* rp);
	void			Process_TournamentExitMakingTeam(CReceivePacketBuffer* rp);
	void			Process_TournamentChangeStatus(CReceivePacketBuffer* rp);
	void			Process_TournamentEnterGameRoom(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentForceOutPreMadeTeamRes(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentGameResult(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentGameEnd(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentStartFail(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentMyTeamInfoRes(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentPublicOpenInfoRes(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentVictoryTeamInfo(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentCreatableRes(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentMoveNextSeasonNotify(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentStartCancelLackTeam(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentSeasonEnd(CReceivePacketBuffer* recvPacket);
	void			Process_TournamentVictory(CReceivePacketBuffer* rp);
	void			Process_EventcomponentRewardPopup( CReceivePacketBuffer* recvPacket );
	void			Process_AddPointAndPoint(CReceivePacketBuffer* rp);
	void			Process_CheatRatingPointRes(CReceivePacketBuffer* rp);
	void			Process_UseHackNotifyReq( CReceivePacketBuffer* rp );
	void			Process_MatchingCancelPenalty(CReceivePacketBuffer* rp);
	void			Process_GameEndGiveBuffItem(CReceivePacketBuffer* rp);
	void			Process_SendPlayTimeRemain(CReceivePacketBuffer* rp);
	void			Process_UserRatingUpdateNot(CReceivePacketBuffer* rp);
	void			Process_AchieveRewardNot(CReceivePacketBuffer* rp);
	void			Process_AchieveCometitionRewardNot(CReceivePacketBuffer* rp);
	void			Process_OpenPointRewardCardNot(CReceivePacketBuffer* rp);
	void			Process_ActionSlotRes(CReceivePacketBuffer* rp);
	void			Process_ActionAnimRes(CReceivePacketBuffer* rp);
	void			Process_EventGiveEventCash(CReceivePacketBuffer* rp);
	void			Process_GM_FORBID_MULITUSER_REQ( CReceivePacketBuffer* rp );
	void			Process_UpdateFriendPlayCount( CReceivePacketBuffer* rp );
	void			Process_EventSendPaper( CReceivePacketBuffer* rp );
	void			Process_UserPlayGameAchievement(CReceivePacketBuffer* rp);
	void			Process_MatchLocationNOT( CReceivePacketBuffer* rp );
	void			Process_UserInfoReq(CReceivePacketBuffer* recvPacket);
	void			Process_NetState(CReceivePacketBuffer* rp);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLIENT_INFO);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_GIVE_REWARD_EXHAUST_ITEM_TO_INVEN_REQ);
	void			Process_Potential_Ability( CReceivePacketBuffer* recvPacket );
	void			Process_Potential_Balance( CReceivePacketBuffer* recvPacket );
	void			Process_EquipProductListInRoomNot(CReceivePacketBuffer* recvPacket);
	void			Process_RageAvatarInfluenceList(CReceivePacketBuffer* recvPacket);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_ENTER_GAMEROOM_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_JOIN_PLAYER_ENTER_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_JOIN_PLAYER_EXIT_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUBTOURNAMENT_START_FAIL_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_LEFT_SEAT_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_LEFT_SEAT_NOTICE);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_MATCH_ADVANTAGE_INFLUENCE_LIST);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_BURNING_STEP_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CEHCK_OPENED_TEAM_COUNT_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_GIVE_REWARD_ACHIEVEMENT_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CHANGE_TEAM_FACTION_OPTION_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_MAKE_TEAM_FACTION_FAIL_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_UPDATE_HOTGIRL_MISSION_COUNT_REQ);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_UPDATE_MISSIONBINGO_MISSION_COUNT_REQ);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CHARACTOR_STAT_MODULATION_CHECK_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_USER_APPRAISAL_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_USER_APPRAISAL_UPDATE_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_TRANSFORM_STATUS_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_UPDATE_ROOM_INFO_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_AI_BALANCE_STAT_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_AIPVP_CHANGE_GAMEPLAYERNUM_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_CHANGE_GRADE_MODE_REMAIN_TIME_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_PVE_CHANGE_AI_TEAM_MEMBER_RES);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(G2M_FACTION_UPDATE_POINT_SCORE_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_HELLONEWYEAR_CHECK_EXP_BUFF_REQ);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_FRIEND_ACCOUNT_ADD_WITHPLAYCNT_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_GAME_RESULT_BEST_PLAYER_CURRENT_STATUS_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_CLUB_LEAGUE_OPEN_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_GAME_RESULT_BEST_PLAYER_UPDATE_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_GAME_APPRAISAL_MISSION_NOT);
	DECLARE_GAMEMATCHPROXY_PROC_FUNC(M2G_WEBDB_USER_GAMERECORD_NOT);

	virtual			void	SendFirstPacket();
	void			SendActionSlot(CFSGameUser* pUser);
	void			SendTeamInfoInTeamReq(SG2M_BASE Info);
	void			SendAvoidTeamReq(SG2M_AVOID_TEAM_REQ Info);
	void			SendChangeTeamAllowObserveReq(SG2M_CHANGE_TEAM_ALLOW_OBSERVE_REQ Info);
	void			SendChangeTeamNameReq(SG2M_CHANGE_TEAM_NAME_REQ Info);
	void			SendChangeTeamPasswordReq(SG2M_CHANGE_TEAM_PASSWORD_REQ Info);
	void			SendChangeTeamModeReq(SG2M_CHANGE_TEAM_MODE_REQ Info);
	void			SendTeamExitReq(SG2M_BASE Info);
	void			SendWaitTeamListReq(SG2M_WAIT_TEAM_LIST_REQ Info);
	void			SendChiefAssignCheckReq(SG2M_CHIEF_ASSIGN_CHECK_REQ Info);
	void			SendChiefAssignMoveReq(SG2M_CHIEF_ASSIGN_MOVE_REQ Info);
	void			SendForceOutUserReq(SG2M_FORCEOUT_USER_REQ Info);
	void			SendRegTeamReq(SG2M_REG_TEAM_REQ Info);
	void			SendAutoRegReq(SG2M_AUTO_REG_REQ Info);
	void			SendReadyTeamListReq(SG2M_READY_TEAM_LIST_REQ Info);
	void			SendRequestTeamMatchReq(SG2M_REQUEST_TEAM_MATCH_REQ Info);
	void			SendChangePositionReq(SG2M_CHANGE_POSITION_REQ Info);
	void			SendReadyReq(SG2M_BASE info);
	void			SendToggleIamReadyReq(const SG2M_BASE& info);
	void			SendCheckPublicIPAddressForGameRes(SG2M_CHECK_PUBLIC_IP_ADDRESS_FOR_GAME_RES info);
	void			SendStartUdoHolepunchingRes(SG2M_START_UDP_HOLEPUNCHING_RES info);
	void			SendStartBandWidthRes(SG2M_START_BANDWIDTH_CHECK_RES info);
	void			SendAddStatUpdate(SG2M_ADD_STAT_UPDATE info);
	void			SendInitialGameRes(SG2M_INITIALGAME_RES info);
	void			SendPlayReadyOkReq(SG2M_BASE info);
	void			SendChangeCourtReq(SG2M_CHANGE_COURT_REQ info);
	void			SendChangePracticeMethodReq(SG2M_CHANGE_PRACTICE_METHOD_REQ info);
	void			SendGameTimeRes(SG2M_GAME_TIME_RES info);
	void			SendShootTry(SG2M_SHOOT_TRY info);
	void			SendShootSuccessorFailure(SG2M_SHOOT_SUCCESS_OR_FAILURE info);
	void			SendRebound(SG2M_BASE info);
	void			SendAssist(SG2M_ASSIST info);
	void			SendSteal(SG2M_BASE info);
	void			SendBlock(SG2M_BLOCK_SUCCESS info);
	void			SendJumpBall(SG2M_BASE info);
	void			SendScoreAddReq(SG2M_SCORE_ADD_REQ info);
	void			SendGameResultReq(SG2M_GAME_RESULT_REQ info);
	void			SendQuarterEndAck(SG2M_BASE info);
	// Room List 요청
	void			SendRoomListReq(SG2M_ROOM_LIST_REQ info);
	// Room Game Mode 변경
	void			SendChangeGameModeReq(SG2M_CHANGE_GAME_MODE_REQ info);
	// Room Name 변경
	void			SendChangeGameRoomNameReq(SG2M_CHANGE_GAMEROOM_NAME_REQ info);
	// Room 내 Team 변경
	void			SendTeamChangeReq(SG2M_TEAM_CHANGE_REQ info);
	// 팀 평가 점수 요청
	void			SendTeamEstimateReq(SG2M_BASE info);
	// 일반전, 연습하기 시 방에 유저 입장
	void			SendJoinRoomReq(SG2M_JOIN_ROOM_REQ info);
	// WAIT_ROOM(매치가 된 룸 상태) 상태에서 팀 매치 취소
	void			SendCancelTeamMatchReq(SG2M_CANCEL_TEAM_MATCH_REQ info);
	// 룸에서 나가기 요청
	void			SendExitRoomReq(SG2M_EXIT_ROOM_REQ info);
	// 인스턴트 토너먼트 등록 취소
	void			SendTournamentExitReq(SG2M_BASE info);
	// 연장전 시작
	void			SendExtraQuarterStart(SG2M_BASE info);
	// 게임 중 유저간 통신이 끊김
	void			SendPeerDisconnected(SG2M_PEER_DISCONNECTED_REQ info);
	// 게임 시작 취소
	void			SendGameStartPauseReq(SG2M_BASE info);
	// 게임 종료
	void			SendGameTerminatedRes(SG2M_BASE info);
	// 룸의 관전 ID, Key Setting
	void			SendSetObserverRoomID(SG2M_SET_OBSERVER_ROOMID info);
	// 관전 퇴장
	void			SendExitObservingGameReq(SG2M_BASE info);
	// 클럽 팀 리스트 요청
	void			SendClubTeamListReq(SG2M_CLUB_TEAM_LIST_REQ info);
	// 유저 게임 옵션 동기화
	void			SendSetGameOption(SG2M_SET_GAME_OPTION info);
	// 올코드 후반전 시작
	void			SendGameStartHalfTime(SG2M_BASE info);
	// 친구, 거부 유저 추가
	void			SendAddFriend(SG2M_ADD_FRIEND info);
	// 친구, 거부 유저 삭제
	void			SendDelFriend(SG2M_DEL_FRIEND info);
	// ToT 징계
	void			SendIntentionalFoulReq(SG2M_INTENTIONAL_FOUL_REQ info);
	// 휴식 아이템 사용
	void			SendUsePauseItemReq(SG2M_BASE info);
	// 휴식 아이템 발동
	void			SendGamePauseReq(SG2M_BASE info);
	// 부스터 아이템 발동
	void			SendUseSecondBoostItemReq(SG2M_USE_SECOND_BOOST_ITEM_REQ info);
	// 부스터 아이템 만료
	void			SendSecondBoostItemExpiredReq(SG2M_SECOND_BOOST_ITEM_EXPIRED_REQ info);
	// 유저가 자의로 연결을 끊었는지 로그 전송
	void			SendLogoutTypeCheck(SG2M_LOGOUT_TYPE_CHECK info);
	// GM의 룸 리스트 요청
	void			SendGMRoomListReq(SG2M_BASE info);
	// GM의 룸 정보 요청
	void			SendGMRoomInfoReq(SG2M_GM_ROOMINFO_REQ info);
	// GM의 룸 채팅 요청
	void			SendGMChatReq(SG2M_GM_CHAT_REQ info);
	// GM의 강퇴 요청
	void			SendGMKickOutReq(SG2M_GM_KICKOUT_REQ info);
	// GM의 유저 정보 요청
	void			SendGMFindUserReq(SG2M_GM_FINDUSER_REQ info);
	// GM의 방 닫기 요청
	void			SendGMCloseRoomReq(SG2M_GM_CLOSEROOM_REQ info);
	// 유저 정보 전송 요청
	void			SendEquippedAchievementTitleBroadcastReq(SG2M_EQUIP_ACHIEVEMENT_TITLE_BROADCAST_REQ info);

	void			SendUserHackNorityRes( SG2M_FSG_USEHACK_NOTIRYRES info );

	void			SendActionAnimReq(SG2M_ACTION_ANIM_REQ Info);

	void			SendUpdateChannelBuffStatusReq(SG2M_UPDATE_CHANNEL_BUFF_STATUS info);

	void			SendCheerLeaderChangeReq(SG2M_CHEERLEADER_CHANGE_REQ info);
	void			SendUserInfoReq(SG2M_USER_INFO_REQ info);
	void			Process_Game_Guide_UI(CReceivePacketBuffer* rp);
	void			Process_EVENT_SportBigEvent(CReceivePacketBuffer* rp);

protected:

	BOOL			m_bIsAutoTeam;

};
