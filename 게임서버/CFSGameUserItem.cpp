// CFSGameUserItem.cpp: implementation of the CFSGameUserItem class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CFSGameUserItem.h"
qinclude "CFSGameDBCfg.h"
qinclude "CFSFileLog.h"
qinclude "CFSGameServer.h"
qinclude "CFSRankManager.h"
qinclude "CFSItemList.h"
qinclude "CFSGameODBC.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "CFSSvrList.h"
qinclude "FSGameCommon.h"
qinclude "MatchSvrProxy.h"
qinclude "HackingManager.h"
qinclude "CFSGameClient.h"
qinclude "CoachCardShop.h"
qinclude "Item.h"
qinclude "RandomCardShopManager.h"
qinclude "SecretStoreManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "FactionManager.h"
qinclude "CContentsManager.h"
qinclude "UserMissionShopEvent.h"
qinclude "HoneyWalletEventManager.h"
qinclude "HotGirlMissionEventManager.h"
qinclude "HotGirlSpecialBoxEventManager.h"
qinclude "SkyLuckyEventManager.h"
qinclude "LegendEventManager.h"
qinclude "DevilTemtationEventManager.h"
qinclude "RandomArrowEventManager.h"
qinclude "CAvatarCreateManager.h"
qinclude "TodayHotDeal.h"
qinclude "SalePlusEventManager.h"
qinclude "UserChoiceMissionEvent.h"
qinclude "SaleRandomItemEventManager.h"
qinclude "MissionMakeItemEventManager.h"
qinclude "ShoppingFestivalEventManager.h"
qinclude "RandomItemDrawEventManager.h"
qinclude "ClubConfigManager.h"
qinclude "FriendInviteManager.h"
qinclude "PotionMakingEventManager.h"
qinclude "PrimonGoEventManager.h"
qinclude "EventDateManager.h"
qinclude "PackageItemEventManager.h"
qinclude "RandomItemBoardEventManager.h"
qinclude "SelectClothesEventManager.h"
qinclude "ItemShopPackageItemEventManager.h"
qinclude "SecretShopEventManager.h"

extern CFSFileLog		g_FSFileLog;
extern CFSGameDBCfg*	g_pFSDBCfg; 

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFSGameUserItem::CFSGameUserItem()
{
	m_iMode = ITEM_PAGE_MODE_NONE;
	m_pUser = NULL;
	memset( &m_UseItem , -1 , sizeof( SFeatureInfo ) );
	memset( &m_UseTryItem , -1 , sizeof( SFeatureInfo ) );

	m_iUseItemMode = 0;

	m_paItemList = NULL;
}

CFSGameUserItem::~CFSGameUserItem()
{
	if( NULL != m_paItemList )
	{
		delete[] m_paItemList;
		m_paItemList = NULL;
	}
}

//// 유저인벤토리에 있는 아이템을 페이지 단위로 가져온다	
void CFSGameUserItem::GetItemListPage(int iInventorytKind, int iPage, int iBigKind, int iSmallKind, SUserItemInfo *aItemList, int &iItemNum)
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL != pAvatarItemList )
	{
		pAvatarItemList->GetItemList(iInventorytKind, iPage, iBigKind, iSmallKind, aItemList, iItemNum, CFSGameServer::GetInstance()->GetItemShop());
	}

}

BOOL CFSGameUserItem::UseItem(CFSItemShop *pItemShop , int iItemIdx ,  int *iaRemoveList, int & iErrorCode, int iPropertyIdx1, int iPropertyIdx2 )
{
	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );

	if( NULL == pFSODBC || NULL == m_pUser || NULL == pItemShop ) 
	{
		iErrorCode = -5;
		return FALSE;
	}

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar)
	{	
		iErrorCode = -5;
		return FALSE;
	}

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) 
	{
		iErrorCode = -5;
		return FALSE;
	}

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);

	if( NULL == pItem )
	{
		iErrorCode = -5;
		return FALSE;
	}

	SShopItemInfo ItemInfo;
	pItemShop->GetItem( pItem->iItemCode, ItemInfo);

	if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX &&
		pItem->iCategory == 0)
	{
		SShopItemInfo BuyItemInfo;
		if(FALSE == pItemShop->GetItemByPropertyKind(pItem->iPropertyKind, BuyItemInfo))
		{
			iErrorCode = -5;
			return FALSE;
		}
	}
	else
	{
		if(false == ItemInfo.IsLvCondition(pAvatar->iLv))
		{
			iErrorCode = -7;
			return FALSE;
		}
	}

	// 20080525 Don't Use Channel Zero
	if( pItem->iChannel == 0 && ( MIN_BUFF_ITEM_KIND_NUM > pItem->iPropertyKind || MAX_BUFF_ITEM_KIND_NUM < pItem->iPropertyKind ))
	{
		iErrorCode = -5;
		return FALSE;
	}

	// 20071011 Add SpecialCharacterIndex 
	if( FALSE == CheckSpecialTeamIndexCondition(pAvatar->iAvatarTeamIndex, ItemInfo.iTeamIndexCondition, ItemInfo.iUnableTeamIndexCondition) )	// 2011.02.28 jhwoo
	{
		iErrorCode = -5;
		return FALSE;
	}
	// End					

	if( FALSE == CheckSpecialAvatarIndexCondition(pAvatar->iSpecialCharacterIndex, ItemInfo.iSpecialIndexCondition) )	// 2011.02.28 jhwoo
	{
		iErrorCode = -5;
		return FALSE;
	}	

	// 20070903 New Item - 2
	if( (pAvatar->iSex != pItem->iSexCondition) && (ITEM_SEXCONDITION_UNISEX != pItem->iSexCondition ))
	{
		iErrorCode = -8;
		return FALSE;
	}

	if (SHOUT_ITEM_KIND_START <= pItem->iPropertyKind && pItem->iPropertyKind <= SHOUT_ITEM_KIND_END &&
		pItem->iPropertyKind != SHOUT_ITEM4_KIND_NUM)
	{
		iErrorCode = -5;
		return FALSE;
	}
	int iTargetIndex = 0 ;
	if( TRUE == AVATARCREATEMANAGER.CheckCloneAvailableCharacter(pAvatar->iSpecialCharacterIndex, iTargetIndex ))
	{
		if( (pAvatar->iSex != ItemInfo.iSexCondition) && (2 != ItemInfo.iSexCondition ))
		{
			iErrorCode = -8;
			return FALSE;
		}
	}
	// 20080212 Korea Shout Item
	if( pItem->iPropertyKind == SHOUT_ITEM4_KIND_NUM )
	{
		return UpdateFuncItemStatus( pFSODBC, pItem->iItemIdx, 3, iaRemoveList );
	}
	if( pItem->iPropertyKind >= MIN_BUFF_ITEM_KIND_NUM && pItem->iPropertyKind <= MAX_BUFF_ITEM_KIND_NUM )
	{
		return UpdateBuffItemStatus( pFSODBC, pItem->iItemIdx, 3, iaRemoveList );
	}

	if( ITEM_STATUS_WEAR == pItem->iStatus || 
		ITEM_STATUS_RESELL == pItem->iStatus || 
		ITEM_STATUS_ONLY_RESELL == pItem->iStatus || 
		ITEM_STATUS_EXPIRED == pItem->iStatus) //착용중이거나 팔아버린것이면
	{
		iErrorCode = -5;
		return FALSE;
	}

	BOOL bRemove = FALSE;		// 이 함수 내에서 사용하지 않음
	if( FALSE == RemoveItemWithRemoveList(pFSODBC, pAvatar, pItem->iChannel, pItem->iPropertyKind, m_UseItem, iaRemoveList, bRemove))
	{
		iErrorCode = -5;
		return FALSE;
	}

	CBindaccountItem* pBindaccountItem = m_pUser->GetBindaccountItem();
	if (NULL == pBindaccountItem)
	{
		iErrorCode = -5;
		return FALSE;
	}

	SBindaccountItem sBindaccountItem;
	BOOL bIsBindaccountItem = FALSE;

	if (TRUE == pBindaccountItem->GetBindaccountItem(pItem->iItemCode, sBindaccountItem))
	{
		bIsBindaccountItem = TRUE;
	}

	BYTE bStatus = 0;
	if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
	{
		int iSlot = FindChangeDressSpaceSlot(m_UseItem.iaChangeDressFeature);
		if(-1 == iSlot)
		{
			iErrorCode = -5;
			return FALSE;
		}

		m_UseItem.iaChangeDressFeature[iSlot] = pItem->iItemCode;
		m_UseItem.iaChangeDressItemIdx[iSlot] = iItemIdx;

		m_UseItem.iChangeDressChannel |= pItem->iChannel;
	}
	else
	{
		int iSlot = FindSpaceSlot(m_UseItem.iaFeature);
		if( -1 == iSlot )
		{
			iErrorCode = -5;
			return FALSE;
		}

		if((m_UseItem.iChannel & pItem->iChannel) > 0 &&
			(ItemInfo.iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
		{
			SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_FACE_ACC2, pAvatar->iSex, pAvatar->iSpecialCharacterIndex);

			if(pItem != NULL)
			{
				switch(pItem->iItemCode)
				{
				case WARDEN_ACC2_ITEM_CODE:
				case WANGLIN_ACC2_ITEM_CODE:
				case HWAWOON_ACC2_ITEM_CODE:
					break;

				default:
					iErrorCode = -5;
					return FALSE;
				}
			}
		}

		m_UseItem.iaFeature[iSlot] = pItem->iItemCode;
		m_UseItem.iaItemIdx[iSlot] = iItemIdx;
		m_UseItem.iChannel |= pItem->iChannel;

		bStatus = pAvatarItemList->GetUserPropertyAccItemStatus(pItem->iItemIdx, pItem->iBigKind, pItem->iStatus, pItem->iPropertyKind);

		if((pItem->iBigKind == ITEM_BIG_KIND_ACC || (MIN_LINK_ITEM_PROPERTY_KIND_NUM <= pItem->iPropertyKind && pItem->iPropertyKind <= MAX_LINK_ITEM_PROPERTY_KIND_NUM) || ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC == pItem->iPropertyKind) 
			&& pItem->iPropertyKind != -1)
		{
			vector< SUserItemProperty > vUserItemProperty;
			pAvatarItemList->GetItemProperty(pItem->iItemIdx, vUserItemProperty);
			if(vUserItemProperty.size() > 0)
			{
				if(false == pAvatarItemList->IsFullApplyAccItemProperty())
				{
					if(ODBC_RETURN_SUCCESS == ((CFSODBCBase *)pFSODBC)->ITEM_InsertUserApplyAccItemProperty(m_pUser->GetGameIDIndex(), iItemIdx))
					{
						pAvatarItemList->AddUserApplyAccItemProperty(iItemIdx);
					}
				}
			}		
		}

		//선택한 PC방 아이템 능력치 넣어줌
		if ( pItem->iPropertyKind >= MIN_PREMIUM_STAT_OPTION_ITEM_NUM && pItem->iPropertyKind <= MAX_PREMIUM_STAT_OPTION_ITEM_NUM )
		{
			pItem->iPropertyNum = 0;
			GetCurAvatarItemList()->RemoveItemProperty(iItemIdx);

			CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
			CHECK_NULL_POINTER_BOOL(pItemShop) ;

			if (iPropertyIdx1 != -1)
			{
				CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList( iPropertyIdx1 );
				CHECK_NULL_POINTER_BOOL(pItemPropertyList);

				SItemProperty* pItemProperty = pItemPropertyList->GetItemProperty( 0 );
				CHECK_NULL_POINTER_BOOL(pItemProperty);

				SUserItemProperty tempUserItemProperty;
				tempUserItemProperty.iGameIDIndex	= pAvatar->iGameIDIndex;
				tempUserItemProperty.iItemIdx		= iItemIdx;
				tempUserItemProperty.iProperty		= pItemProperty->iProperty;
				tempUserItemProperty.iValue			= pItemProperty->iValue;

				pAvatarItemList->AddUserItemProperty( tempUserItemProperty );			
				pItem->iPropertyNum++;
			}
			if (iPropertyIdx2 != -1)
			{
				CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList( iPropertyIdx2 );
				CHECK_NULL_POINTER_BOOL(pItemPropertyList);

				SItemProperty* pItemProperty = pItemPropertyList->GetItemProperty( 0 );
				CHECK_NULL_POINTER_BOOL(pItemProperty);

				SUserItemProperty tempUserItemProperty;
				tempUserItemProperty.iGameIDIndex	= pAvatar->iGameIDIndex;
				tempUserItemProperty.iItemIdx		= iItemIdx;
				tempUserItemProperty.iProperty		= pItemProperty->iProperty;
				tempUserItemProperty.iValue			= pItemProperty->iValue;

				pAvatarItemList->AddUserItemProperty( tempUserItemProperty );			
				pItem->iPropertyNum++;
			}
		}
	}	
	
	CheckBasicItemNeed(m_UseItem);

	pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR, bStatus);

	if( !( pItem->bIsMemoryItem() ) )	// 실제 인벤에 존재하는 아이템일 경우에만
	{
		/// 착용된 PC방 메모리 전용 아이템이 있었으면 ///
		if( CheckUserMemoryItem() )
		{
			if( false == UseOrigianlItem( 0 , pItem ) )
			{
				RestoreUseFeatureInfo();

				iErrorCode = -5;
				return FALSE;
			}
		}
		else
		{
			memcpy( &m_OriginalItem , &m_UseItem , sizeof(SFeatureInfo) );
		}

		if( pItem->iChannel != 0 )
		{
			if( ODBC_RETURN_SUCCESS != pFSODBC->spFSUseItem(pAvatar->iGameIDIndex, m_UseItem))
			{
				RestoreUseFeatureInfo();
				iErrorCode = -5;
				return FALSE;
			}
		}
		else
		{
			RestoreUseFeatureInfo();
			iErrorCode = -5;
			return FALSE;
		}
	}

	if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
	{
		memcpy( pAvatar->AvatarFeature.ChangeDressFeatureInfo , m_UseItem.iaChangeDressFeature , sizeof(int)*MAX_CHANGE_DRESS_NUM );
		pAvatar->AvatarFeature.iTotalChangeDressChannel = m_UseItem.iChangeDressChannel;

		for(int i=0; i<MAX_CHANGE_DRESS_NUM; i++)
		{
			if(-1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i]);

				if(NULL != pSlotItem)
				{
					pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[i] = pSlotItem->iItemIdx;
					pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i] = pSlotItem->iChannel;
				}
			}
		}
	}
	else
	{
		memcpy( pAvatar->AvatarFeature.FeatureInfo , m_UseItem.iaFeature , sizeof(int)*MAX_ITEMCHANNEL_NUM );
		pAvatar->AvatarFeature.iTotalChannel = m_UseItem.iChannel;

		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != pAvatar->AvatarFeature.FeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.FeatureInfo[i]);

				if(NULL != pSlotItem)
				{
					pAvatar->AvatarFeature.FeatureItemIndex[i] = pSlotItem->iItemIdx;
					pAvatar->AvatarFeature.FeatureProtertyType[i] = pSlotItem->iPropertyType;
					pAvatar->AvatarFeature.FeatureProtertyKind[i] = pSlotItem->iPropertyKind;
					pAvatar->AvatarFeature.FeatureProtertyChannel[i] = pSlotItem->iChannel;
					pAvatar->AvatarFeature.FeatureProtertyValue[i] = pSlotItem->iPropertyTypeValue;
				}
			}
		}
	}

	SAvatarStyleInfo* pAvatarStyleInfo = m_pUser->GetStyleInfo();
	if (NULL != pAvatarStyleInfo)
	{
		pAvatarStyleInfo->SetCurrentStyleItemIdx(m_UseItem);
	}

	return TRUE;
}

BOOL CFSGameUserItem::UseTryItem(int iItemCode , CFSItemShop *pItemShop, int & iErrorCode)
{
	if( NULL == m_pUser) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	if( NULL == pItemShop )
	{
		iErrorCode = -5;
		return FALSE;
	}

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar)
	{
		iErrorCode = -5;
		return FALSE;
	}

	// 20070903 New Item - 2
	SShopItemInfo pItem;
	if( FALSE == pItemShop->GetItem(iItemCode, pItem))
		// End
	{
		iErrorCode = -5;	
		return FALSE;
	}

	// 20071213 Face Off
	if( pItem.iPropertyKind == FACE_OFF_KIND_NUM )
	{
		int iFace = 0;

		switch(iItemCode)
		{
		case BASE_FACE_OFF_NUM_7:	iFace = 8;					break;
		case BASE_FACE_OFF_NUM_8:	iFace = 0;					break;
		default:	iFace = iItemCode - BASE_FACE_OFF_NUM;		break;
		}	
		
		if( iFace < 0 || iFace > FACE_OFF_ITEM_MAX_NUM )
		{
			iErrorCode = -5;
			return FALSE;
		}

		if( pAvatar->iSpecialCharacterIndex != -1 )
		{
			iErrorCode = -17;
			return FALSE;
		}

		m_UseTryItem.iFace = iFace;
		return TRUE;
	}
	// End
	// 체형변경권
	if( pItem.iPropertyKind == CHANGE_BODYSHAPE_KIND_NUM )
	{
		int iBodyShape = ((iItemCode / 10) + 4) ;
0;

		if( iBodyShape < 0 || iBodyShape > FACE_OFF_ITEM_MAX_NUM )
		{
			iErrorCode = -5;
			return FALSE;
		}

		if( pAvatar->iSpecialCharacterIndex != -1 )
		{
			iErrorCode = -17;
			return FALSE;
		}

		m_UseTryItem.iBodyShape = iBodyShape;
		return TRUE;
	}	// End

	if( !pItem.IsPositionCondition(m_pUser->GetCurUsedAvatarPosition() ) )
	{
		iErrorCode = TRY_USE_ITEM_ERROR_MISMATCH_POSITION;
		return FALSE;
	}

	//착용불가 아이템 - 캐릭터 슬롯과 같은////
	// 20081204 Add Wonder Girls SpeCialCharacter Slot Type
	if( CHARACTER_SLOT_TYPE_NORMAL <= pItem.iCharacterSlotType)
	{
		iErrorCode = -5;	
		return FALSE;
	}

	if(pAvatar->iCharacterType == NEW_FEMALE_CHARACTER_TYPE &&
		pItem.bIsNotNewCharBuyItem == TRUE)
	{
		iErrorCode = TRY_USE_ITEM_ERROR_NEW_AVATAR_CONDITION;
		return FALSE;
	}

	if( 403 <= pAvatar->iSpecialCharacterIndex && 406 >= pAvatar->iSpecialCharacterIndex
		&& TRUE == pItem.bIsNotNewCharBuyItem )
	{
		iErrorCode = TRY_USE_ITEM_ERROR_NEW_AVATAR_CONDITION;
		return FALSE;
	}
	
	
	// 20071011 Add SpecialCharacterIndex 
	//if( pAvatar->iSpecialCharacterIndex == -1 && pItem.iPropertyKind >= MIN_SPECIAL_ITEM_KIND_NUM && pItem.iPropertyKind <= MAX_SPECIAL_ITEM_KIND_NUM )
	if( FALSE == CheckSpecialAvatarIndexCondition(pAvatar->iSpecialCharacterIndex, pItem.iSpecialIndexCondition) )
	{
		iErrorCode = TRY_USE_ITEM_ERROR_SPECIAL_AVATAR_CONDITION;
		return FALSE;
	}

	// 20080108 Fix Item Use
	//if( pAvatar->iSpecialCharacterIndex != pItem.iSpecialIndexCondition && pItem.iPropertyKind >= MIN_SPECIAL_ITEM_KIND_NUM && pItem.iPropertyKind <= MAX_SPECIAL_ITEM_KIND_NUM )
	if( FALSE == CheckSpecialTeamIndexCondition(pAvatar->iAvatarTeamIndex, pItem.iTeamIndexCondition, pItem.iUnableTeamIndexCondition) )
	{
		iErrorCode = TRY_USE_ITEM_ERROR_SPECIAL_TEAM_CONDITION;
		return FALSE;
	}
	// End

	// 20070919 Functional Item
	if((pItem.iPropertyKind >= MIN_ONE_GAME_BOOST_ITEM_KIND_NUM && pItem.iPropertyKind <= MAX_ONE_GAME_BOOST_ITEM_KIND_NUM) ||
		(pItem.iPropertyKind >= MIN_SECOND_BOOST_ITEM_KIND_NUM && pItem.iPropertyKind <= MAX_SECOND_BOOST_ITEM_KIND_NUM) ||
		(pItem.iPropertyKind >= MIN_POSITION_POWER_UP_ITEM_KIND_NUM && pItem.iPropertyKind <= MAX_POSITION_POWER_UP_ITEM_KIND_NUM))
	{
		iErrorCode = -5;	
		return FALSE;
	}	
	//End

	// 20070903 New Item - 2
	if( (pAvatar->iSex != pItem.iSexCondition) && (ITEM_SEXCONDITION_UNISEX != pItem.iSexCondition ) )
	{
		iErrorCode = -8;
		return FALSE;
	}

	BOOL bRemove = FALSE;
	if(GetMode() == ITEM_PAGE_MODE_CLUBSHOP &&
		ITEM_SMALL_KIND_HAIR <= pItem.iSmallKind && ITEM_SMALL_KIND_ACC_ETC >= pItem.iSmallKind) // club shop
	{	
		if( FALSE == RemoveChangeDressItem(pAvatar , pItem.iChannel , m_UseTryItem , pItemShop , bRemove))
		{
			iErrorCode = -5;
			return FALSE;
		}

		int iSlot = FindChangeDressSpaceSlot(m_UseTryItem.iaChangeDressFeature);
		if( -1 == iSlot )
		{
			iErrorCode = -5;
			return FALSE;
		}

		m_UseTryItem.iaChangeDressFeature[iSlot] = iItemCode;
		m_UseTryItem.iaChangeDressItemIdx[iSlot] = -3;
		m_UseTryItem.iChangeDressChannel |= pItem.iChannel;
	}
	else
	{
		if( FALSE == RemoveItem(pAvatar , pItem.iChannel , m_UseTryItem , pItemShop , bRemove))
		{
			iErrorCode = -5;
			return FALSE;
		}

		int iSlot = FindSpaceSlot(m_UseTryItem.iaFeature);
		if( -1 == iSlot )
		{
			iErrorCode = -5;
			return FALSE;
		}

		m_UseTryItem.iaFeature[iSlot] = iItemCode;
		m_UseTryItem.iaItemIdx[iSlot] = -3;
		m_UseTryItem.iChannel |= pItem.iChannel;

		CheckBasicItemNeed(m_UseTryItem);
	}

	if( PROPENSITY_ITEM_KIND_START <= pItem.iPropertyKind && PROPENSITY_ITEM_KIND_END >= pItem.iPropertyKind )
	{
		CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList( pItem.iPropertyKind );
		if( NULL != pItemPropertyBoxList )
		{
			vector< SItemPropertyBox > vItemPropertyBox;
			vector< SItemProperty > vItemProperty;

			int iMaxBoxIdx = pItemPropertyBoxList->GetMaxBoxIdxCount( m_pUser->GetCurUsedAvtarLv(), PROPERTY_ASSIGN_TYPE_NONE );
			for( int iBoxIdx = 0; iBoxIdx < iMaxBoxIdx; ++iBoxIdx )
			{
				pItemPropertyBoxList->GetItemPropertyBox(iBoxIdx, m_pUser->GetCurUsedAvtarLv(), PROPERTY_ASSIGN_TYPE_NONE, vItemPropertyBox);
			}

			int iItemPropertyBoxSize = vItemPropertyBox.size();
			for( int i = 0; i < iItemPropertyBoxSize; ++i )
			{
				int iPropertyIndex = vItemPropertyBox[i].iPropertyIndex;

				CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList( iPropertyIndex );
				if( NULL == pItemProperty )	continue;

				pItemProperty->GetItemProperty( vItemProperty );
			}	

			int iaStatAbility[MAX_STAT_TYPE_COUNT] = {0};
			for( int i = 0; i < vItemProperty.size(); ++i )
			{
				if (0 > vItemProperty[i].iProperty || MAX_STAT_TYPE_COUNT <= vItemProperty[i].iProperty)
					continue;

				iaStatAbility[vItemProperty[i].iProperty] = vItemProperty[i].iValue;
			}
	
			m_pUser->SendUserStat(iaStatAbility);
		}
	}

	return TRUE;
}

BOOL CFSGameUserItem::UndressItem(int iItemCode , int iChannel, CFSItemShop *pItemShop, int & iErrorCode)
{
	if( NULL == m_pUser) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	if( NULL == pItemShop )
	{
		iErrorCode = -5;
		return FALSE;
	}

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar)
	{
		iErrorCode = -5;
		return FALSE;
	}

	// 20070903 New Item - 2
	SShopItemInfo pItem;
	if( FALSE == pItemShop->GetItem(iItemCode, pItem))
		// End
	{
		iErrorCode = -5;	
		return FALSE;
	}

	BOOL bRemove = FALSE;
	if(GetMode() == ITEM_PAGE_MODE_CLUBSHOP &&
		ITEM_SMALL_KIND_HAIR <= pItem.iSmallKind && ITEM_SMALL_KIND_ACC_ETC >= pItem.iSmallKind) // club shop
	{
		if( FALSE == RemoveChangeDressItem(pAvatar , iChannel , m_UseTryItem , pItemShop , bRemove))
		{
			iErrorCode = -5;
			return FALSE;
		}

		CheckOriginalChangeDressItemNeed(m_UseTryItem);
	}
	else
	{
		RemoveBasicItem(m_UseTryItem);

		if( FALSE == RemoveItem(pAvatar , iChannel , m_UseTryItem , pItemShop , bRemove))
		{
			iErrorCode = -5;
			return FALSE;
		}

		CheckOriginalItemNeed(m_UseTryItem);

		CheckBasicItemNeed(m_UseTryItem);
	}

	if(pItem.iSpecialPartsIndex > 0)
		m_pUser->CheckSpecialPartsUserProperty();

	return TRUE;
}


BOOL CFSGameUserItem::UseTryUniform(int iItemCode , CFSItemShop *pItemShop, int & iErrorCode)
{
	if( NULL == m_pUser) 
	{
		iErrorCode = -5;
		return FALSE;
	}
	if( NULL == pItemShop )
	{
		iErrorCode = -5;
		return FALSE;
	}

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar)
	{
		iErrorCode = -5;
		return FALSE;
	}


	// 20070907 New Item - 3
	SShopItemInfo pItem;
	if( FALSE == pItemShop->GetItem(iItemCode, pItem))
		// End
	{
		iErrorCode = -5;	
		return FALSE;
	}

	//착용불가 아이템 - 캐릭터 슬롯과 같은
	if( CHARACTER_SLOT_TYPE_NORMAL <= pItem.iCharacterSlotType || pItem.iPropertyKind == SKILL_SLOT_KIND_NUM )
	{
		iErrorCode = -5;	
		return FALSE;
	}

	// 20070907 New Item - 3
	if( (pAvatar->iSex != pItem.iSexCondition) && (ITEM_SEXCONDITION_UNISEX != pItem.iSexCondition ) )
	{
		iErrorCode = -8;
		return FALSE;
	}

	BOOL bRemove = FALSE;
	if( FALSE == RemoveItem(pAvatar , pItem.iChannel , m_UseTryItem , pItemShop , bRemove))
	{
		iErrorCode = -5;
		return FALSE;
	}

	int iSlot = FindSpaceSlot(m_UseTryItem.iaFeature);
	if( -1 == iSlot )
	{
		iErrorCode = -5;
		return FALSE;
	}

	m_UseTryItem.iaFeature[iSlot] = iItemCode;
	m_UseTryItem.iaItemIdx[iSlot] = -3;

	RemoveBasicItem(m_UseTryItem);
	CheckOriginalItemNeed(m_UseTryItem);
	CheckBasicItemNeed(m_UseTryItem);

	return TRUE;
}



BOOL CFSGameUserItem::RemoveItemWithRemoveList(CFSODBCBase *pODBC, SAvatarInfo *pAvatar, int iChannel, int iPropertyKind, SFeatureInfo &FeatureInfo, int *iaRemoveList, BOOL &bRemove)
{
	CHECK_NULL_POINTER_BOOL(pODBC);
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	int k=0;

	if(iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
	{
		for(int i=0;i<MAX_CHANGE_DRESS_NUM;i++)
		{
			if(-1 != FeatureInfo.iaChangeDressFeature[i])
			{
				SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCodeChangeDressItem(FeatureInfo.iaChangeDressFeature[i]);

				if(NULL == pSlotItem) return FALSE;

				if((pSlotItem->iChannel & iChannel) > 0)
				{
					if( FALSE == RemoveItemWithStatus(pAvatar ,i, pSlotItem, FeatureInfo, iChannel)) return FALSE;
					iaRemoveList[k++] = pSlotItem->iItemIdx;
					bRemove = TRUE;
				}
			}
		}
	}
	else
	{
		for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
		{
			if( -1 != FeatureInfo.iaFeature[i] )
			{
				SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCodeNotChangeDressItem(FeatureInfo.iaFeature[i]);

				if( NULL == pSlotItem ) return FALSE;

				if( (pSlotItem->iChannel & iChannel) > 0)
				{
					if(TRUE == pAvatarItemList->CheckUserApplyAccItemProperty(pSlotItem->iItemIdx))
					{
						if(ODBC_RETURN_SUCCESS == (pODBC->ITEM_DeleteUserApplyAccItemProperty(pAvatar->iGameIDIndex, pSlotItem->iItemIdx)))				
						{
							pAvatarItemList->RemoveUserApplyAccItemProperty(pSlotItem->iItemIdx);
						}
					}

					if( FALSE == RemoveItemWithStatus( pAvatar , i , pSlotItem , FeatureInfo, iChannel )) return FALSE;
					iaRemoveList[k++] = pSlotItem->iItemIdx;
					bRemove = TRUE;

				}
			}
		}
	}

	return TRUE;
}

BOOL CFSGameUserItem::RemoveItem(SAvatarInfo *pAvatar, int iChannel, SFeatureInfo &FeatureInfo, CFSItemShop *pItemShop, BOOL &bRemove)
{
	if( NULL == pItemShop ) return FALSE;
	if( NULL == pAvatar ) return FALSE;
	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	int iFlag=0;
	for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != FeatureInfo.iaFeature[i] )
		{
			// 20070903 New Item - 2
			SShopItemInfo ItemInfo;
			memset( &ItemInfo , 0 , sizeof( SShopItemInfo ));
			if( FALSE == pItemShop->GetItem(FeatureInfo.iaFeature[i],ItemInfo))
			{
				return FALSE;
			}
			// End

			if( (ItemInfo.iChannel & iChannel) > 0 )
			{
				if((iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
				{
					SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_FACE_ACC2, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );

					if(pItem != NULL)
					{
						switch(pItem->iItemCode)
						{
						case WARDEN_ACC2_ITEM_CODE:
						case WANGLIN_ACC2_ITEM_CODE:
						case HWAWOON_ACC2_ITEM_CODE:
							break;

						default:
							return FALSE;
						}
					}
				}

				RemoveItemWith( i , ItemInfo , FeatureInfo );
				iFlag = 1;

			}
		}
	}
	//	if( 1 == iFlag ) return TRUE;

	return TRUE;
}

BOOL CFSGameUserItem::RemoveChangeDressItem(SAvatarInfo *pAvatar, int iChannel, SFeatureInfo &FeatureInfo, CFSItemShop *pItemShop, BOOL &bRemove)
{
	if( NULL == pItemShop ) return FALSE;
	if( NULL == pAvatar ) return FALSE;
	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	int iFlag=0;
	for(int i=0;i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if( -1 != FeatureInfo.iaChangeDressFeature[i] )
		{
			SShopItemInfo ShopItemInfo;
			pItemShop->GetItem(FeatureInfo.iaChangeDressFeature[i], ShopItemInfo);

			if((ShopItemInfo.iChannel & iChannel) > 0)
			{
				RemoveChangeDressItemWith(i, ShopItemInfo.iChannel, FeatureInfo);
				iFlag = 1;

			}
		}
	}
	//	if( 1 == iFlag ) return TRUE;

	return TRUE;
}

BOOL CFSGameUserItem::RemoveItemWithStatus( SAvatarInfo* pAvatar , int iSlot , SUserItemInfo * pItem ,SFeatureInfo &FeatureInfo, int iChannel)
{
	if( NULL == pAvatar ) return FALSE;
	if( NULL == pItem ) return FALSE;
	if( -1 == iSlot ) return FALSE;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
	{
		FeatureInfo.iaChangeDressFeature[iSlot] = -1;
		FeatureInfo.iaChangeDressItemIdx[iSlot] = -1;
		FeatureInfo.iChangeDressChannel ^= pItem->iChannel;
	}
	else
	{
		if((FeatureInfo.iChannel & pItem->iChannel) > 0 &&
			(iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
		{
			SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_FACE_ACC2, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );

			if(pItem != NULL)
			{
				switch(pItem->iItemCode)
				{
				case WARDEN_ACC2_ITEM_CODE:
				case WANGLIN_ACC2_ITEM_CODE:
				case HWAWOON_ACC2_ITEM_CODE:
					break;

				default:
					return FALSE;
				}
			}
		}

		FeatureInfo.iaFeature[iSlot] = -1;
		FeatureInfo.iaItemIdx[iSlot] = -1;
		FeatureInfo.iChannel ^= pItem->iChannel;
	}

	pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_INVENTORY);

	return TRUE;
}
// 20070903 New Item - 2
BOOL CFSGameUserItem::RemoveItemWith( int iSlot , SShopItemInfo &ItemInfo ,SFeatureInfo &FeatureInfo)
{
	//if( NULL == pItem ) return FALSE;
	if( -1 == iSlot ) return FALSE;

	FeatureInfo.iaFeature[iSlot] = -1;
	FeatureInfo.iaItemIdx[iSlot] =-1;
	FeatureInfo.iChannel ^= ItemInfo.iChannel;

	return TRUE;
}
// End

BOOL CFSGameUserItem::RemoveChangeDressItemWith(int iSlot, int iChannel, SFeatureInfo &FeatureInfo)
{
	//if( NULL == pItem ) return FALSE;
	if( -1 == iSlot ) return FALSE;

	FeatureInfo.iaChangeDressFeature[iSlot] = -1;
	FeatureInfo.iaChangeDressItemIdx[iSlot] =-1;
	FeatureInfo.iChannel ^= iChannel;

	return TRUE;
}

void CFSGameUserItem::CheckBasicItemNeed(SFeatureInfo &FeatureInfo, BOOL bChangeStatus)
{
	int iSlot = -1;
	
	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	if( 0 == (FeatureInfo.iChannel & ITEMCHANNEL_HAIR) )
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_HAIR, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if( NULL == pItem ) return;

		iSlot = FindSpaceSlot(FeatureInfo.iaFeature);
		if( -1 != iSlot )
		{
			// 20070903 New Item - 2
			FeatureInfo.iaFeature[iSlot] = pItem->iItemCode;
			// End
			FeatureInfo.iaItemIdx[iSlot] = pItem->iItemIdx;
			FeatureInfo.iChannel |= pItem->iChannel;
		
			if(bChangeStatus == TRUE)
				pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
		}
	}
	if( 0 == (FeatureInfo.iChannel & ITEMCHANNEL_UP) )
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_UP, pAvatar->iSex, pAvatar->iSpecialCharacterIndex);
		if( NULL == pItem ) return;

		iSlot = FindSpaceSlot(FeatureInfo.iaFeature);
		if( -1 != iSlot )
		{
			// 20070903 New Item - 2
			FeatureInfo.iaFeature[iSlot] = pItem->iItemCode;
			// End
			FeatureInfo.iaItemIdx[iSlot] = pItem->iItemIdx;
			FeatureInfo.iChannel |= pItem->iChannel;

			if(bChangeStatus == TRUE)
				pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
		}
	}
	if( 0 == (FeatureInfo.iChannel & ITEMCHANNEL_DOWN) )
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_DOWN, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if( NULL == pItem ) return;

		iSlot = FindSpaceSlot(FeatureInfo.iaFeature);
		if( -1 != iSlot )
		{
			// 20070903 New Item - 2
			FeatureInfo.iaFeature[iSlot] = pItem->iItemCode;
			// End
			FeatureInfo.iaItemIdx[iSlot] = pItem->iItemIdx;
			FeatureInfo.iChannel |= pItem->iChannel;

			if(bChangeStatus == TRUE)
				pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
		}
	}
	if( 0 == (FeatureInfo.iChannel & ITEMCHANNEL_SHOES) )
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_SHOES, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if( NULL == pItem ) return;

		iSlot = FindSpaceSlot(FeatureInfo.iaFeature);
		if( -1 != iSlot )
		{
			// 20070903 New Item - 2
			FeatureInfo.iaFeature[iSlot] = pItem->iItemCode;
			// End
			FeatureInfo.iaItemIdx[iSlot] = pItem->iItemIdx;
			FeatureInfo.iChannel |= pItem->iChannel;

			if(bChangeStatus == TRUE)
				pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
		}
	}
	if( 0 == (FeatureInfo.iChannel & ITEMCHANNEL_FACE_ACC2) )
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_FACE_ACC2, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if( NULL == pItem ) return;

		iSlot = FindSpaceSlot(FeatureInfo.iaFeature);
		if( -1 != iSlot )
		{
			// 20070903 New Item - 2
			FeatureInfo.iaFeature[iSlot] = pItem->iItemCode;
			// End
			FeatureInfo.iaItemIdx[iSlot] = pItem->iItemIdx;
			FeatureInfo.iChannel |= pItem->iChannel;

			if(bChangeStatus == TRUE)
				pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
		}
	}
}

void CFSGameUserItem::RemoveBasicItem(SFeatureInfo &FeatureInfo)
{
	int iSlot = -1;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	if((FeatureInfo.iChannel & ITEMCHANNEL_HAIR) > 0)
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_HAIR, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if(pItem == NULL)
			return;

		for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
		{
			SShopItemInfo ItemInfo;
			memset( &ItemInfo , 0 , sizeof( SShopItemInfo ));
			if( FALSE == pItemShop->GetItem(FeatureInfo.iaFeature[i],ItemInfo))
			{
				continue;
			}
			if((ItemInfo.iChannel & ITEMCHANNEL_HAIR) > 0 && (pItem->iItemCode == FeatureInfo.iaFeature[i]))
			{
				FeatureInfo.iaFeature[i] = -1;
				FeatureInfo.iaItemIdx[i] = -1;
				FeatureInfo.iChannel -= ItemInfo.iChannel;
			}
		}
	}

	if((FeatureInfo.iChannel & ITEMCHANNEL_UP) > 0)
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_UP, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if(pItem == NULL)
			return;
	
		for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
		{
			SShopItemInfo ItemInfo;
			memset( &ItemInfo , 0 , sizeof( SShopItemInfo ));
			if( FALSE == pItemShop->GetItem(FeatureInfo.iaFeature[i],ItemInfo))
			{
				continue;
			}
			if((ItemInfo.iChannel & ITEMCHANNEL_UP) > 0 && (pItem->iItemCode == FeatureInfo.iaFeature[i]))
			{
				FeatureInfo.iaFeature[i] = -1;
				FeatureInfo.iaItemIdx[i] = -1;
				FeatureInfo.iChannel -= ItemInfo.iChannel;
			}
		}
	}

	if((FeatureInfo.iChannel & ITEMCHANNEL_DOWN ) > 0)
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_DOWN, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if(pItem == NULL)
			return;
		
		for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
		{
			SShopItemInfo ItemInfo;
			memset( &ItemInfo , 0 , sizeof( SShopItemInfo ));
			if( FALSE == pItemShop->GetItem(FeatureInfo.iaFeature[i],ItemInfo))
			{
				continue;
			}
			if((ItemInfo.iChannel & ITEMCHANNEL_DOWN) > 0 && (pItem->iItemCode == FeatureInfo.iaFeature[i]))
			{
				FeatureInfo.iaFeature[i] = -1;
				FeatureInfo.iaItemIdx[i] = -1;
				FeatureInfo.iChannel -= ItemInfo.iChannel;
			}
		}
	}

	if((FeatureInfo.iChannel & ITEMCHANNEL_SHOES ) > 0)
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_SHOES, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if(pItem == NULL)
			return;

		for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
		{
			SShopItemInfo ItemInfo;
			memset( &ItemInfo , 0 , sizeof( SShopItemInfo ));
			if( FALSE == pItemShop->GetItem(FeatureInfo.iaFeature[i],ItemInfo))
			{
				continue;
			}
			if((ItemInfo.iChannel & ITEMCHANNEL_SHOES) > 0 && (pItem->iItemCode == FeatureInfo.iaFeature[i]))
			{
				FeatureInfo.iaFeature[i] = -1;
				FeatureInfo.iaItemIdx[i] = -1;
				FeatureInfo.iChannel -= ItemInfo.iChannel;
			}
		}
	}

	if((FeatureInfo.iChannel & ITEMCHANNEL_FACE_ACC2 ) > 0)
	{
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( ITEMCHANNEL_FACE_ACC2, pAvatar->iSex, pAvatar->iSpecialCharacterIndex );
		if(pItem == NULL)
			return;

		for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
		{
			SShopItemInfo ItemInfo;
			memset( &ItemInfo , 0 , sizeof( SShopItemInfo ));
			if( FALSE == pItemShop->GetItem(FeatureInfo.iaFeature[i],ItemInfo))
			{
				continue;
			}
			if((ItemInfo.iChannel & ITEMCHANNEL_FACE_ACC2) > 0 && (pItem->iItemCode == FeatureInfo.iaFeature[i]))
			{
				FeatureInfo.iaFeature[i] = -1;
				FeatureInfo.iaItemIdx[i] = -1;
				FeatureInfo.iChannel -= ItemInfo.iChannel;
			}
		}
	}
}

void CFSGameUserItem::CheckOriginalItemNeed(SFeatureInfo &FeatureInfo)
{
	int iSlot = -1;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	for(int i = ITEMCHANNEL_HAIR; i <= ITEMCHANNEL_BOOST2; i = i * 2)
	{
		if(0 == (FeatureInfo.iChannel & i))
		{
			SUserItemInfo * pItem = pAvatarItemList->GetEquipItemWithChannel(i);
			if(pItem)
			{
				bool bIsok = true;

				for(int j = ITEMCHANNEL_HAIR; j <= ITEMCHANNEL_BOOST2; j = j * 2)
				{
					if((pItem->iChannel & j) > 0)
					{
						if((FeatureInfo.iChannel & j) > 0)
						{
							bIsok = false;
							break;
						}
					}
				}
				if(bIsok)
				{
					iSlot = FindSpaceSlot(FeatureInfo.iaFeature);
					// 20070903 New Item - 2
					if(-1 != iSlot)
					{
						FeatureInfo.iaFeature[iSlot] = pItem->iItemCode;
						// End
						FeatureInfo.iaItemIdx[iSlot] = pItem->iItemIdx;
						FeatureInfo.iChannel |= pItem->iChannel;
						pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
					}
				}
			}
		}
	}
}

void CFSGameUserItem::CheckOriginalChangeDressItemNeed(SFeatureInfo &FeatureInfo)
{
	int iSlot = -1;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	for(int i = ITEMCHANNEL_HAIR; i <= ITEMCHANNEL_BOOST2; i = i * 2)
	{
		if(0 == (FeatureInfo.iChannel & i))
		{
			SUserItemInfo * pItem = pAvatarItemList->GetEquipItemWithChannel(i);
			if(pItem)
			{
				bool bIsok = true;

				for(int j = ITEMCHANNEL_HAIR; j <= ITEMCHANNEL_BOOST2; j = j * 2)
				{
					if((pItem->iChannel & j) > 0)
					{
						if((FeatureInfo.iChannel & j) > 0)
						{
							bIsok = false;
							break;
						}
					}
				}
				if(bIsok)
				{
					iSlot = FindChangeDressSpaceSlot(FeatureInfo.iaChangeDressFeature);

					if(iSlot >= 0 && iSlot < MAX_ITEMCHANNEL_NUM)
					{
						FeatureInfo.iaChangeDressFeature[iSlot] = pItem->iItemCode;
						FeatureInfo.iaChangeDressItemIdx[iSlot] = pItem->iItemIdx;
						FeatureInfo.iChangeDressChannel |= pItem->iChannel;
						pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
					}
				}
			}
		}
	}
}

BOOL CFSGameUserItem::RemoveItemWithItemIdx( int iItemIdx )
{
	CFSGameODBC *pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pODBC );

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar) return FALSE;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;


	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);

	if( NULL == pItem ) return FALSE;
	// 20080229 Modify Equip Item
	if( ITEM_STATUS_WEAR > pItem->iStatus || ITEM_STATUS_ONLY_RESELL == pItem->iStatus || ITEM_STATUS_EXPIRED == pItem->iStatus || ITEM_STATUS_FACE == pItem->iStatus ) return FALSE;
	// End

	// 20080225 Add Korea Premium Item
	if( (pItem->iPropertyKind >  PREMIUM_ITEM_KIND_START && pItem->iPropertyKind < PREMIUM_ITEM_KIND_END) ||
		pItem->iPropertyKind == LICK_ITEM_KIND_NUM)
	{
		return FALSE;
	}
	// End

	// 20080212 Korea Shout Item
	if( pItem->iPropertyKind == SHOUT_ITEM4_KIND_NUM )
	{
		return UpdateFuncItemStatus(pODBC, pItem->iItemIdx, 1);
	}
	// End
	// 20070903 New Item - 2

	int iSlot = -1;
	if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
	{
		iSlot = FindChangeDressItemSlot(m_UseItem.iaChangeDressFeature, pItem->iItemCode);

		if( -1 == iSlot ) return FALSE;

		m_UseItem.iaChangeDressFeature[iSlot] = -1;
		m_UseItem.iaChangeDressItemIdx[iSlot] = -1;
		m_UseItem.iChangeDressChannel ^= pItem->iChannel;
	}
	else
	{
		iSlot = FindItemSlot(m_UseItem.iaFeature, pItem->iItemCode);

		if( -1 == iSlot ) return FALSE;

		m_UseItem.iaFeature[iSlot] = -1;
		m_UseItem.iaItemIdx[iSlot] = -1;
		m_UseItem.iChannel ^= pItem->iChannel;
	}

	pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_INVENTORY);
	CheckBasicItemNeed(m_UseItem);

	if(!pItem->bIsMemoryItem()) 
	{
		if( false == UseOrigianlItem( 1, pItem ) )
		{
			RestoreUseFeatureInfo();
			return FALSE;
		}
	}
	else
	{
		if( !CheckUserMemoryItem() )
		{
			memcpy( &m_OriginalItem , &m_UseItem , sizeof(SFeatureInfo) );
		}
	}

	if(ODBC_RETURN_SUCCESS != pODBC->spFSUseItem(pAvatar->iGameIDIndex, m_UseItem))
	{
		RestoreUseFeatureInfo();
		return FALSE;
	}

	if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
	{
		memcpy( pAvatar->AvatarFeature.ChangeDressFeatureInfo , m_UseItem.iaChangeDressFeature , sizeof(int)*MAX_CHANGE_DRESS_NUM );
		pAvatar->AvatarFeature.iTotalChangeDressChannel = m_UseItem.iChangeDressChannel;

		for(int i=0; i<MAX_CHANGE_DRESS_NUM; i++)
		{
			if(-1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i]);

				if(NULL != pSlotItem)
				{
					pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[i] = pSlotItem->iItemIdx;
					pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i] = pSlotItem->iChannel;
				}
			}
		}
	}
	else
	{
		memcpy( pAvatar->AvatarFeature.FeatureInfo , m_UseItem.iaFeature , sizeof(int)*MAX_ITEMCHANNEL_NUM );
		pAvatar->AvatarFeature.iTotalChannel = m_UseItem.iChannel;

		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != pAvatar->AvatarFeature.FeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.FeatureInfo[i]);

				if(NULL != pSlotItem)
				{
					pAvatar->AvatarFeature.FeatureItemIndex[i] = pSlotItem->iItemIdx;
					pAvatar->AvatarFeature.FeatureProtertyType[i] = pSlotItem->iPropertyType;
					pAvatar->AvatarFeature.FeatureProtertyKind[i] = pSlotItem->iPropertyKind;
					pAvatar->AvatarFeature.FeatureProtertyChannel[i] = pSlotItem->iChannel;
					pAvatar->AvatarFeature.FeatureProtertyValue[i] = pSlotItem->iPropertyTypeValue;
				}
			}
		}

		if(TRUE == pAvatarItemList->CheckUserApplyAccItemProperty(iItemIdx))
		{
			if(ODBC_RETURN_SUCCESS == pODBC->ITEM_DeleteUserApplyAccItemProperty(pAvatar->iGameIDIndex, pItem->iItemIdx))				
			{
				pAvatarItemList->RemoveUserApplyAccItemProperty(pItem->iItemIdx);
			}
		}
	}

	SAvatarStyleInfo* pAvatarStyleInfo = m_pUser->GetStyleInfo();
	if (NULL != pAvatarStyleInfo)
	{
		pAvatarStyleInfo->SetCurrentStyleItemIdx(m_UseItem);
	}

	if(pItem->iSpecialPartsIndex > 0)
		m_pUser->CheckSpecialPartsUserProperty();

	return TRUE;
}


BOOL CFSGameUserItem::DeleteExhaustedItem(SUserItemInfo* pItem , int iSlot)
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar) return FALSE;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	if( NULL == pItem ) return FALSE;

	if( -1 == iSlot ) return FALSE;

	if( pItem->iStatus == ITEM_STATUS_RESELL)	return FALSE;

	m_UseItem.iaFeature[iSlot] = -1;
	m_UseItem.iaItemIdx[iSlot] = -1;
	m_UseItem.iChannel ^= pItem->iChannel;

	if( pItem->iStatus == ITEM_STATUS_WEAR)
	{
		CheckBasicItemNeed(m_UseItem);
	}

	if( CheckUserMemoryItem() )
	{
		if( false == UseOrigianlItem( 1, pItem ) )
		{
			RestoreUseFeatureInfo();
			return FALSE;
		}
	}
	else
	{
		memcpy( &m_OriginalItem , &m_UseItem , sizeof(SFeatureInfo) );
	}

	pAvatarItemList->RemoveItem(pItem);

	memcpy( pAvatar->AvatarFeature.FeatureInfo , m_UseItem.iaFeature , sizeof(int)*MAX_ITEMCHANNEL_NUM );
	pAvatar->AvatarFeature.iTotalChannel = m_UseItem.iChannel;

	for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
	{
		if(-1 != pAvatar->AvatarFeature.FeatureInfo[i])
		{
			SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.FeatureInfo[i]);

			if(NULL != pSlotItem)
			{
				pAvatar->AvatarFeature.FeatureItemIndex[i] = pSlotItem->iItemIdx;
				pAvatar->AvatarFeature.FeatureProtertyType[i] = pSlotItem->iPropertyType;
				pAvatar->AvatarFeature.FeatureProtertyKind[i] = pSlotItem->iPropertyKind;
				pAvatar->AvatarFeature.FeatureProtertyChannel[i] = pSlotItem->iChannel;
				pAvatar->AvatarFeature.FeatureProtertyValue[i] = pSlotItem->iPropertyTypeValue;
			}
		}
		else
		{
			pAvatar->AvatarFeature.FeatureItemIndex[i] = -1;
			pAvatar->AvatarFeature.FeatureProtertyType[i] = -1;
			pAvatar->AvatarFeature.FeatureProtertyKind[i] = -1;
			pAvatar->AvatarFeature.FeatureProtertyChannel[i] = -1;
			pAvatar->AvatarFeature.FeatureProtertyValue[i] = -1;
		}
	}

	SAvatarStyleInfo* pAvatarStyleInfo = m_pUser->GetStyleInfo();
	if (NULL != pAvatarStyleInfo)
	{
		pAvatarStyleInfo->SetCurrentStyleItemIdx(m_UseItem);
	}

	return TRUE;
}

//20050930 time based item
BOOL CFSGameUserItem::ExpireItem(CFSGameODBC *pODBC, int iItemIdx, int iUpdateItemStatus/* = ITEM_STATUS_EXPIRED*/)
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar) return FALSE;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if( NULL == pItem ) return FALSE;

	switch( pItem->iStatus )
	{
	case ITEM_STATUS_RESELL: // 삭제된 Item
		return FALSE;
	case ITEM_STATUS_FLAME_MAN:
		break;
	case ITEM_STATUS_INVENTORY: // 착용하지 않은 Item
		{//현재 사용중이지 않은 스타일의 아이템이 만료될때 스타일데이터가 삭제가 안되는 문제가 있어 삭제처리함
			SAvatarStyleInfo* pStyleInfo = m_pUser->GetStyleInfo();
			CHECK_NULL_POINTER_BOOL(pStyleInfo);
			pStyleInfo->RemoveItem(pItem->iItemIdx);
		} break;
	case ITEM_STATUS_WEAR:	// 착용한 Item
		{
			if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
			{
				int iSlot = FindChangeDressItemSlot(m_UseItem.iaChangeDressFeature, pItem->iItemCode);

				if( -1 == iSlot ) return FALSE;

				m_UseItem.iaChangeDressFeature[iSlot] = -1;
				m_UseItem.iaChangeDressItemIdx[iSlot] = -1;
				m_UseItem.iChangeDressChannel ^= pItem->iChannel;
			}
			else
			{
				int iSlot = FindItemSlot(m_UseItem.iaFeature , pItem->iItemCode);

				if( -1 == iSlot ) return FALSE;

				m_UseItem.iaFeature[iSlot] = -1;
				m_UseItem.iaItemIdx[iSlot] = -1;
				m_UseItem.iChannel ^= pItem->iChannel;

				CheckBasicItemNeed(m_UseItem);
			}
		}
		break;
	default:
		return FALSE;
	}

	SShopItemInfo ItemInfo;
	CFSGameServer::GetInstance()->GetItemShop()->GetItem(pItem->iItemCode , ItemInfo);
	if (0 == (ItemInfo.iInventoryButtonType & INVENTORY_BUTTON_RECHARGE))
	{
		iUpdateItemStatus = ITEM_STATUS_RESELL;
	}

	if( !( pItem->bIsMemoryItem() ) )	// 실제 인벤에 존재하는 아이템일 경우에만
	{
		if( CheckUserMemoryItem() )
		{
			if( false == UseOrigianlItem( 1, pItem ) )
			{
				RestoreUseFeatureInfo();
				return FALSE;
			}
		}
		else
		{
			memcpy( &m_OriginalItem , &m_UseItem , sizeof(SFeatureInfo) );
		}
	}

	if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
	{
		iUpdateItemStatus = ITEM_STATUS_RESELL;
		if(pItem->iItemIdx > 0 && ODBC_RETURN_SUCCESS != pODBC->ITEM_UpdateChangeDressItemExpire(pAvatar->iGameIDIndex, m_UseItem.iaChangeDressItemIdx, 
			pItem->iItemIdx, pItem->iItemCode, iUpdateItemStatus))
		{
			RestoreUseFeatureInfo();
			return FALSE;
		}

		memcpy( pAvatar->AvatarFeature.ChangeDressFeatureInfo , m_UseItem.iaChangeDressFeature , sizeof(int)*MAX_CHANGE_DRESS_NUM );
		pAvatar->AvatarFeature.iTotalChangeDressChannel = m_UseItem.iChangeDressChannel;

		for(int i=0; i<MAX_CHANGE_DRESS_NUM; i++)
		{
			if(-1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i]);

				if(NULL != pSlotItem)
				{
					pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[i] = pSlotItem->iItemIdx;
					pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i] = pSlotItem->iChannel;
				}
			}
		}
	}
	else
	{
		if(pItem->iItemIdx > 0 &&  ODBC_RETURN_SUCCESS != pODBC->spUpdateItemExpire(pAvatar->iGameIDIndex, m_UseItem.iaItemIdx , m_UseItem.iaFeature , m_UseItem.iChannel , pItem->iItemIdx , pItem->iItemCode, iUpdateItemStatus ))
		{
			RestoreUseFeatureInfo();
			return FALSE;
		}

		memcpy( pAvatar->AvatarFeature.FeatureInfo , m_UseItem.iaFeature , sizeof(int)*MAX_ITEMCHANNEL_NUM );
		pAvatar->AvatarFeature.iTotalChannel = m_UseItem.iChannel;

		for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
		{
			if(-1 != pAvatar->AvatarFeature.FeatureInfo[i])
			{
				SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.FeatureInfo[i]);

				if(NULL != pSlotItem)
				{
					pAvatar->AvatarFeature.FeatureItemIndex[i] = pSlotItem->iItemIdx;
					pAvatar->AvatarFeature.FeatureProtertyType[i] = pSlotItem->iPropertyType;
					pAvatar->AvatarFeature.FeatureProtertyKind[i] = pSlotItem->iPropertyKind;
					pAvatar->AvatarFeature.FeatureProtertyChannel[i] = pSlotItem->iChannel;
					pAvatar->AvatarFeature.FeatureProtertyValue[i] = pSlotItem->iPropertyTypeValue;
				}
			}
		}
	}
	
	if(TRUE == pAvatarItemList->CheckUserApplyAccItemProperty(iItemIdx))  
    { 
        if(ODBC_RETURN_SUCCESS == pODBC->ITEM_DeleteUserApplyAccItemProperty(pAvatar->iGameIDIndex, pItem->iItemIdx))                           
        { 
			pAvatarItemList->RemoveUserApplyAccItemProperty(pItem->iItemIdx); 
        } 
    }

	pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, iUpdateItemStatus);

	SAvatarStyleInfo* pAvatarStyleInfo = m_pUser->GetStyleInfo();
	if (NULL != pAvatarStyleInfo)
	{
		pAvatarStyleInfo->SetCurrentStyleItemIdx(m_UseItem);
	}

	return TRUE;
}

BOOL CFSGameUserItem::DeleteExpiredItem(CFSODBCBase *pODBC, int iItemIdx)
{
	CHECK_NULL_POINTER_BOOL(pODBC);

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	CHECK_NULL_POINTER_BOOL(pItem);

	CHECK_CONDITION_RETURN(ITEM_STATUS_EXPIRED != pItem->iStatus, FALSE);

	if (ODBC_RETURN_SUCCESS != pODBC->ITEM_DeleteUserExpiredItem(m_pUser->GetGameIDIndex(), iItemIdx))
	{
		return FALSE;
	}

	pAvatarItemList->ChangeItemStatus(iItemIdx, ITEM_STATUS_RESELL);
	
	return TRUE;
}

BOOL CFSGameUserItem::DeleteItem(CFSODBCBase *pODBC, SUserItemInfo* pItem)
{
	CHECK_NULL_POINTER_BOOL(pODBC);
	CHECK_NULL_POINTER_BOOL(pItem);

	CHECK_CONDITION_RETURN(ITEM_STATUS_RESELL == pItem->iStatus, FALSE);

	if (ODBC_RETURN_SUCCESS != pODBC->ITEM_DeleteUserItem(m_pUser->GetGameIDIndex(), pItem->iItemIdx))
	{
		return FALSE;
	}

	GetCurAvatarItemList()->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_RESELL);

	return TRUE;
}

BOOL CFSGameUserItem::InsertNewItem(SUserItemInfo &UserItemInfo)
{

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar ) return FALSE;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	pAvatarItemList->AddItem(UserItemInfo);

	return TRUE;
}


BOOL CFSGameUserItem::SendItemPresent(CFSGameODBC *pODBC, CFSItemShop *pItemShop, int iItemCode, char* SendUserID , char* szMsg)
{
	if( NULL == pODBC || NULL == pItemShop ) return FALSE;
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar ) return FALSE;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;


	SUserItemInfo ItemInfo;
	// 20070907 New Item - 3
	SShopItemInfo ShopItemInfo;

	if( FALSE == pItemShop->GetItem( iItemCode , ShopItemInfo )) return FALSE;


	if( 1 == ShopItemInfo.iSellType )
	{

	}
	else if( 2 == ShopItemInfo.iSellType )
	{
		if( m_pUser->GetSkillPoint() < ShopItemInfo.iPrice )
			return FALSE;
	}

	//	if( pAvatar->iSex != ShopItemInfo.iCondition ) return FALSE;


	return TRUE;
}




BOOL CFSGameUserItem::SellItem( CFSItemShop *pItemShop,int iItemIdx, int &iRemove, int & iErrorCode )
{
	CFSGameODBC *pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pODBC );

	if( NULL == pODBC || NULL == pItemShop ) 
		return FALSE;

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) 
		return FALSE;

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if ( NULL == pAvatar ) 
		return FALSE;

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(iItemIdx);
	if( NULL == pItem ) 
		return FALSE;

	if( 0 == pItem->iSellType ) 
		return FALSE;

	if( 2 == pItem->iStatus || 0 == pItem->iStatus ) 
		return FALSE;

	int iSellPrice = 0;

	if( pItem->iPropertyType == ITEM_PROPERTY_EXHAUST && pItem->iChannel != ITEMCHANNEL_BOOST1 && pItem->iChannel != ITEMCHANNEL_BOOST2)	// 개수제 아이템 // 20100108 퀵차지, 파워스낵 제외
	{
		iSellPrice = ( ( pItem->iSellPrice) * pItem->iPropertyTypeValue * 5 ) / 100 ;
	}
	else if( pItem->iPropertyType == ITEM_PROPERTY_TIME && pItem->iPropertyTypeValue != -1 )		// 기간제 아이템
	{
		if (pItem->iPropertyTypeValue == -1)
		{
			iSellPrice = (pItem->iSellPrice * 5) / 100;
		}
		else
		{
			time_t CurrentTime = _time64(NULL);
			int iRemainHours = 0;
			double dDiffSeconds = difftime(CurrentTime, pItem->ExpireDate);
			if( dDiffSeconds > 0 )
			{
				iSellPrice = 0;
			}
			else
			{
				iRemainHours = 	(int)(dDiffSeconds / 3600)*(-1);
				if( pItem->iPropertyTypeValue != 0 )
				{
					iSellPrice = ((pItem->iSellPrice * 5) / 100) * iRemainHours / pItem->iPropertyTypeValue;
				}
				else
				{
					iSellPrice = 0;
				}
			}
		}
	}
	else
	{
		iSellPrice = (pItem->iSellPrice * 5) / 100;
	}

	int iMoney = m_pUser->GetCoin();

	if( ODBC_RETURN_SUCCESS != pODBC->spFSSellItem(m_pUser->GetUserIDIndex(), pAvatar->iGameIDIndex , iItemIdx , pItem->iItemCode , iSellPrice,  iMoney ))
		return FALSE;

	pAvatarItemList->RemoveItem(pItem->iItemIdx);

	m_pUser->SetSkillPoint( m_pUser->GetSkillPoint() + iSellPrice );
	m_pUser->AddPointAchievements(); 

	//m_pUser->CheckSpecialPartsUserProperty();
	m_pUser->SendUserStat();

	return TRUE;
}



BOOL CFSGameUserItem::ConfirmItemChange(CFSGameODBC *pFSODBC)
{

	return TRUE;
}


int CFSGameUserItem::FindSpaceSlot(int *iaFeature)
{
	for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if(-1 == iaFeature[i])
		{
			return i;
		}
	}
	return -1;
}

int CFSGameUserItem::FindChangeDressSpaceSlot(int *iaFeature)
{
	for(int i=0;i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if(-1 == iaFeature[i])
		{
			return i;
		}
	}
	return -1;
}

int CFSGameUserItem::FindItemSlot(int *iaFeature, int iItemCode)
{
	for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if(iItemCode == iaFeature[i])
		{
			return i;
		}
	}
	return -1;
}

int CFSGameUserItem::FindChangeDressItemSlot(int iaFeature[], int iItemCode)
{
	for(int i=0;i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if(iItemCode == iaFeature[i])
		{
			return i;
		}
	}
	return -1;
}

BOOL CFSGameUserItem::BackUpUseFeatureInfo()
{
	CHECK_NULL_POINTER_BOOL(m_pUser);

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	memcpy( m_UseItem.iaFeature, pAvatar->AvatarFeature.FeatureInfo, sizeof(int)*MAX_ITEMCHANNEL_NUM );
	m_UseItem.iChannel = pAvatar->AvatarFeature.iTotalChannel;
	m_UseItem.iFace = pAvatar->iFace;
	m_UseItem.iBodyShape = pAvatar->iCharacterType;
	memcpy( m_UseItem.iaChangeDressFeature, pAvatar->AvatarFeature.ChangeDressFeatureInfo, sizeof(int)*MAX_CHANGE_DRESS_NUM );
	m_UseItem.iChangeDressChannel = pAvatar->AvatarFeature.iTotalChangeDressChannel;

	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 == m_UseItem.iaFeature[i] )
		{
			m_UseItem.iaItemIdx[i] = -1;

		}
		else
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCode(m_UseItem.iaFeature[i]);
			if( NULL == pItem ) return FALSE;
			m_UseItem.iaItemIdx[i] = pItem->iItemIdx;
		}
	}

	for(int i=0; i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if( -1 == m_UseItem.iaChangeDressFeature[i] )
		{
			m_UseItem.iaChangeDressItemIdx[i] = -1;

		}
		else
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCode(m_UseItem.iaChangeDressFeature[i]);
			if( NULL == pItem ) return FALSE;
			m_UseItem.iaChangeDressItemIdx[i] = pItem->iItemIdx;
		}
	}

	return TRUE;
}

void CFSGameUserItem::BackUpUseTryFeatureInfo()
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	memcpy(m_UseTryItem.iaFeature, m_UseItem.iaFeature, sizeof(int)*MAX_ITEMCHANNEL_NUM);
	m_UseTryItem.iFace = m_UseItem.iFace;
	m_UseTryItem.iBodyShape = m_UseItem.iBodyShape;
	m_UseTryItem.iChannel = m_UseItem.iChannel; 

	memcpy( m_UseTryItem.iaChangeDressFeature , m_UseItem.iaChangeDressFeature  , sizeof(int)*MAX_CHANGE_DRESS_NUM );
	m_UseTryItem.iChangeDressChannel = m_UseItem.iChangeDressChannel; 

	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 == m_UseTryItem.iaFeature[i] )
		{
			m_UseTryItem.iaItemIdx[i] = -1;

		}
		else
		{
			m_UseTryItem.iaItemIdx[i] = -2;
		}
	}

	for(int i=0; i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if( -1 == m_UseTryItem.iaChangeDressFeature[i] )
		{
			m_UseTryItem.iaChangeDressItemIdx[i] = -1;

		}
		else
		{
			m_UseTryItem.iaChangeDressItemIdx[i] = -2;
		}
	}
}

void CFSGameUserItem::RestoreFeatureInfo()
{
	if( ITEM_PAGE_MODE_SHOP == m_iMode || ITEM_PAGE_MODE_CLUBSHOP == m_iMode ) 
	{
		BackUpUseTryFeatureInfo();
	}
	else if( ITEM_PAGE_MODE_MYITEM == m_iMode )
	{
		RestoreUseFeatureInfo();
	}
	else if( ITEM_PAGE_MODE_NONE == m_iMode)
	{
		FeatureInfoInitilaize();
	}
}

void CFSGameUserItem::RestoreUseFeatureInfo()
{
	if( NULL == m_pUser ) return;

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar ) return;	

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != m_UseItem.iaFeature[i] )
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCode(m_UseItem.iaFeature[i]);
			if( NULL == pItem ) return;

			pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_INVENTORY);
		}
	}

	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != pAvatar->AvatarFeature.FeatureInfo[i] )
		{

			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCodeInAll(pAvatar->AvatarFeature.FeatureInfo[i]);
			if( NULL == pItem ) return;

			pAvatarItemList->ChangeItemStatus(pItem->iItemIdx , ITEM_STATUS_WEAR);

		}
	}

	for(int i=0; i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if( -1 !=  m_UseItem.iaChangeDressFeature[i] )
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCode(m_UseItem.iaChangeDressFeature[i]);
			CHECK_NULL_POINTER_VOID(pItem);

			pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_INVENTORY);
		}
	}
	for(int i=0; i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if( -1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[i] )
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCodeInAll(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i]);
			CHECK_NULL_POINTER_VOID(pItem);

			pAvatarItemList->ChangeItemStatus(pItem->iItemIdx , ITEM_STATUS_WEAR);
		}
	}

	BackUpUseFeatureInfo();

}

BOOL CFSGameUserItem::GetAvatarInfoInItemPage(SAvatarInfo & AvatarInfo)
{
	memset(&AvatarInfo, 0, sizeof(SAvatarInfo));

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if(pAvatar)
	{
		memcpy(&AvatarInfo, pAvatar, sizeof(SAvatarInfo));
		return TRUE;
	}

	return FALSE;
}

void CFSGameUserItem::GetTempItemFeature(SFeatureInfo& FeatureInfo)
{
	if( ITEM_PAGE_MODE_SHOP == m_iMode  || ITEM_PAGE_MODE_CLUBSHOP == m_iMode) //shop, club shop
	{
		memcpy( FeatureInfo.iaFeature , m_UseTryItem.iaFeature , sizeof(int) * MAX_ITEMCHANNEL_NUM  );
		FeatureInfo.iChannel = m_UseTryItem.iChannel;
	}
	else if( ITEM_PAGE_MODE_MYITEM == m_iMode ) //user 
	{
		memcpy(FeatureInfo.iaFeature, m_UseItem.iaFeature, sizeof(int) * MAX_ITEMCHANNEL_NUM);
	}
}

void CFSGameUserItem::GetTempChangeDressItemFeature(SFeatureInfo& FeatureInfo)
{
	if( ITEM_PAGE_MODE_CLUBSHOP == m_iMode ) //shop
	{
		memcpy( FeatureInfo.iaChangeDressFeature , m_UseTryItem.iaChangeDressFeature , sizeof(int) * MAX_CHANGE_DRESS_NUM  );
		memcpy( FeatureInfo.iaChangeDressItemIdx , m_UseTryItem.iaChangeDressItemIdx , sizeof(int) * MAX_CHANGE_DRESS_NUM  );
		FeatureInfo.iChangeDressChannel = m_UseTryItem.iChangeDressChannel;
	}
	else
	{
		memcpy( FeatureInfo.iaChangeDressFeature , m_UseItem.iaChangeDressFeature , sizeof(int) * MAX_CHANGE_DRESS_NUM  );
		memcpy( FeatureInfo.iaChangeDressItemIdx , m_UseItem.iaChangeDressItemIdx , sizeof(int) * MAX_CHANGE_DRESS_NUM  );
	}
}

void CFSGameUserItem::GetItemFeatureEventPreview(SFeatureInfo& FeatureInfo)
{
	memcpy(FeatureInfo.iaFeature, m_UseItem.iaFeature, sizeof(int) * MAX_ITEMCHANNEL_NUM);
	FeatureInfo.iChannel = m_UseItem.iChannel;
	memcpy( FeatureInfo.iaChangeDressFeature , m_UseItem.iaChangeDressFeature , sizeof(int) * MAX_CHANGE_DRESS_NUM  );
	memcpy( FeatureInfo.iaChangeDressItemIdx , m_UseItem.iaChangeDressItemIdx , sizeof(int) * MAX_CHANGE_DRESS_NUM  );
	FeatureInfo.iChangeDressChannel = m_UseItem.iChangeDressChannel;
}

BOOL CFSGameUserItem::IsInMyInventory( int iItemCode )
{
	if(-1 == iItemCode) return FALSE;

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCodeInAll(iItemCode);

	if(pItem && pItem->iStatus != 9) return TRUE;

	return FALSE;
}

BOOL CFSGameUserItem::IsInMyInventoryAll( int iItemCode )
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithOnlyItemCode( iItemCode );

	if(pItem && pItem->iStatus != 9) 
		return TRUE;

	return FALSE;
}

BOOL CFSGameUserItem::IsInMyInventoryWithPropertyKind( int iPropertyKind )
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	SUserItemInfo * pItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iPropertyKind);

	if(pItem && pItem->iStatus != 9) return TRUE;

	return FALSE;
}

BOOL CFSGameUserItem::LoadAvatarItemList( CFSGameODBC * pGameODBC , SAvatarInfo* pAvatar)
{
	CHECK_NULL_POINTER_BOOL(pAvatar);
	CHECK_NULL_POINTER_BOOL(pGameODBC);

	m_paItemList = new CAvatarItemList[1];
	//20061030 Server Crash Fix
	if ( m_paItemList == NULL)
	{
		return FALSE;
	}
	//End

	if( ODBC_RETURN_FAIL == pGameODBC->ITEM_GetUserApplyAccItemPropertyList(m_paItemList, pAvatar->iGameIDIndex))
	{
		return FALSE;
	}

	// 20070828 Modify GameIDIndex	
	if( ODBC_RETURN_FAIL == pGameODBC->FSGetAvatarItemList( m_paItemList , pAvatar ))
	{
		return FALSE;
	}

	// 20070905 요게 문제가 되네 
	// 20070829 New ItemShop
	if( ODBC_RETURN_FAIL == pGameODBC->FSGetAvatarPropertyList( m_paItemList , pAvatar->iGameIDIndex ))

	{
		return FALSE;
	}
	// End

	m_VoiceUserSettingInfo.clear();
	if ( ODBC_RETURN_FAIL == pGameODBC->VOICE_GetUserSettingInfo( pAvatar->iGameIDIndex, m_VoiceUserSettingInfo ) )
	{
		return FALSE;
	}
	
	vector<SUserLimitedTimeItem> vInfo;
	if(ODBC_RETURN_FAIL == pGameODBC->ITEM_GetUserLimitedTimeItem(pAvatar->iUserIDIndex, pAvatar->iGameIDIndex, vInfo))
	{
		return FALSE;
	}

	for(int i = 0; i < vInfo.size(); i++)
		m_paItemList->AddLimitedTimeItem(vInfo[i]);

	return TRUE;
}


CAvatarItemList* CFSGameUserItem::GetCurAvatarItemList()
{
	return m_paItemList;	
}
//

// 20080313 Add Shout Item for China
BOOL CFSGameUserItem::ReadyShoutItem(int iShoutItemMode)
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();

	if( NULL == pAvatarItemList ) return FALSE;

	int iShoutItemCode = pAvatarItemList->GetShoutItemCodeByMode(iShoutItemMode);
	SUserItemInfo* ItemInfo = NULL;
	if( iShoutItemCode  != 0)
	{
		ItemInfo = pAvatarItemList->GetItemWithChannelAndItemCode(ITEMCHANNEL_SHOUT, iShoutItemCode);
	}


	if( ItemInfo != NULL )
	{
		if( ItemInfo->iPropertyTypeValue > 0 && ItemInfo->iStatus == 1 )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	return FALSE;
}

int CFSGameUserItem::GetShoutItemCount(int iShoutItemMode)	
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();

	if( NULL == pAvatarItemList ) return FALSE;

	int iShoutCount = 0;

	iShoutCount = pAvatarItemList->GetShoutItemCount(iShoutItemMode);

	return iShoutCount;
}

int CFSGameUserItem::CheckShoutItem( CFSGameODBC *pODBC, int iShoutItemMode )
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar) return -4;

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return -4;

	int iShouItemCode = pAvatarItemList->GetShoutItemCodeByMode(iShoutItemMode);
	SUserItemInfo* ItemInfo = pAvatarItemList->GetItemWithChannelAndItemCode(ITEMCHANNEL_SHOUT, iShouItemCode);

	int iReturn = -4;
	if( ItemInfo != NULL )
	{
		if( ItemInfo->iPropertyType == ITEM_PROPERTY_TIME )
		{
			if( ItemInfo->iPropertyTypeValue != -1 )
			{

			}
		}

		if( ItemInfo->iPropertyType == ITEM_PROPERTY_EXHAUST )
		{
			if( ( ItemInfo->iPropertyTypeValue - 1 ) <= 0 )
			{
				if( ( ItemInfo->iPropertyTypeValue - 1 ) != 0 )
				{

				}

				int iEmptySlot = -1;
				if( ODBC_RETURN_SUCCESS != pODBC->spUpdateNoEquipItemExhaustNew( pAvatar->iGameIDIndex, ItemInfo->iChannel, iEmptySlot , ItemInfo->iItemIdx , ItemInfo->iItemCode ))
				{
					return iReturn;
				}

				ItemInfo->iPropertyTypeValue--;

				if( ItemInfo->iPropertyTypeValue <= 0 )
				{
					pAvatarItemList->RemoveItem(ItemInfo->iItemIdx);
				}

				iReturn = 0;
			}
			else
			{
				if( ODBC_RETURN_SUCCESS == pODBC->spUpdateNoEquipItemCount(pAvatar->iGameIDIndex , ItemInfo->iItemIdx , ItemInfo->iItemCode ) )
				{
					ItemInfo->iPropertyTypeValue--;
					iReturn = 0;
				}
			}

		}
	}
	else
	{
		iReturn = -2;
	}

	if (0 == iReturn)
	{
		int iAccumulativeCount = 0;
		int iItemPropertyKind = pAvatarItemList->GetShoutItemPropertyKindByMode(iShoutItemMode);
		pODBC->AVATAR_UpdateAccumulativeConsumedItem(m_pUser->GetGameIDIndex(), iItemPropertyKind, iAccumulativeCount);
	}

	return iReturn;
}
// End

// 20080313 Add Pause Item
int CFSGameUserItem::GetPauseItemCount()	
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();

	if( NULL == pAvatarItemList ) return FALSE;

	int iPauseItemCount = 0;
	iPauseItemCount = pAvatarItemList->GetPauseItemCount();

	return iPauseItemCount;
}
// End

// 20070810 Send My Info
void CFSGameUserItem::GetEquipItemList( vector< SUserItemInfo* >& vUserItemInfo )
{
	if( NULL == m_pUser ) return;

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();

	if( NULL != pAvatarItemList )
	{
		pAvatarItemList->GetEquipItemList( vUserItemInfo );
	}

}
// End


// 20070829 New ItemShop 
// 20070910 Buy Item Cash
// 20070920 Add Quick Inven
// 테스트 코드
BOOL CFSGameUserItem::BuyItem( BOOL bIsLocalPublisher, CFSItemShop *pItemShop, int iItemCode, 
	int iLoopCount, int iPropertyValue, int iPropertyAssignType, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT], int iUseCouponPropertyKind,
	int& iTotalCash, int& iTotalPoint, int& iTotalTrophy, int & iErrorCode, int iOperationCode /*= -1*/, BOOL bIsUseDressItemToken/* = FALSE*/, int iRenewItemIdx /*=-1*/,
	char* recvID /*= NULL*/, char* title /*= NULL*/, char* text /*= NULL*/, int *res /*= NULL*/, int* index /*= NULL*/ )
{
	CFSGameODBC *pODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pODBC );

	CFSLogODBC* pLogODBC = (CFSLogODBC*)ODBCManager.GetODBC( ODBC_LOG );
	CHECK_NULL_POINTER_BOOL( pLogODBC );
	
	CFSLoginGlobalODBC* pLoginODBC = (CFSLoginGlobalODBC*)ODBCManager.GetODBC( ODBC_LOGINGLOBAL );
	CHECK_NULL_POINTER_BOOL( pLoginODBC );

	if( NULL == pODBC || NULL == pItemShop ) return FALSE;

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar ) return FALSE;

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_BOOL(pServer);

	CLimitedEditionItemManager* pLimitedEditionItemManager = pServer->GetLimitedEditionItemManager();
	CHECK_NULL_POINTER_BOOL(pLimitedEditionItemManager);

	CLimitedTimeItemManager* pLimitedTimeItemManager = pServer->GetLimitedTimeItemManager();
	CHECK_NULL_POINTER_BOOL(pLimitedTimeItemManager);

	m_pUser->CheckExpiredEventCoin();

	///////////////////////////////////////// 데이터를 가져오고 초기 설정 ///////////////////////////////////////////
	iErrorCode = BUY_ITEM_ERROR_GENERAL;
	if(res) *res = SEND_ITEM_ERROR_GENERAL;

	SShopItemInfo ShopItemInfo;

	if( FALSE == pItemShop->GetItem( iItemCode , ShopItemInfo )) 
	{
		iErrorCode = BUY_ITEM_ERROR_GENERAL;
		if(res) *res = SEND_ITEM_ERROR_GENERAL;
		return FALSE;
	}

	int iRecvUserGameIDIndex = -1;
	BOOL bIsSendItem = FALSE;

	// 선물이면 받는 사람 아이디를 검색하여 저장하고 bIsSendItem를 TRUE로 사용함
	if( recvID != NULL )
	{
		int iFindRecvUserResult = -1;
		if( ODBC_RETURN_SUCCESS != pODBC->spGetGameIDIndex( recvID , iRecvUserGameIDIndex, iFindRecvUserResult ) || iFindRecvUserResult == -1 )
		{
			if(res) *res = SEND_ITEM_ERROR_NOT_EXIST_USER;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}
		else
		{			
			if(ITEM_PROPERTY_KIND_CHANGE_BONUS_STAT == ShopItemInfo.iPropertyKind)
			{
				int iRecvSpecialAvatarIndex = 0;
				if( ODBC_RETURN_SUCCESS != pODBC->GetSpecialAvatarIndexGameID( recvID , iRecvSpecialAvatarIndex ))
				{
					if(res) *res = SEND_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
					iErrorCode = BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
					return FALSE;
				}
				else
				{
					CAvatarCreateManager* pAvatarCreateManager = &AVATARCREATEMANAGER;
					if(NULL == pAvatarCreateManager)
					{
						if(res) *res = SEND_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						iErrorCode = BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						return FALSE;
					}

					char szSpecialAvatarKey[MAX_SPECIAL_AVATAR_KEY_STRING_LENGTH + 1];

					pAvatarCreateManager->MakeSpecialAvatarKeyString(iRecvSpecialAvatarIndex, 0, szSpecialAvatarKey, _countof(szSpecialAvatarKey));
					SSpecialAvatarConfig* pSpecialAvatarConfig = pAvatarCreateManager->GetSpecialAvatarConfigByAvatarKey(szSpecialAvatarKey);
					if(NULL == pSpecialAvatarConfig)
					{
						if(res) *res = SEND_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						iErrorCode = BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						return FALSE;
					}

					SpecialAvatarStatInfo* pStat = pSpecialAvatarConfig->GetFristStatInfo();
					if(NULL == pStat)
					{
						if(res) *res = SEND_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						iErrorCode = BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						return FALSE;
					}

					if(pStat->iBonusStat == 0)
					{
						if(res) *res = SEND_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						iErrorCode = BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
						return FALSE;
					}
				}
			}

			bIsSendItem = TRUE;
		}
	}
	else
	{
		if(ITEM_PROPERTY_KIND_CHANGE_BONUS_STAT == ShopItemInfo.iPropertyKind)
		{			
			SpecialAvatarStatInfo* pStat = AVATARCREATEMANAGER.GetSpecialAvatarAddStat(pAvatar->iSpecialCharacterIndex, pAvatar->Status.iHeight, pAvatar->Status.iGamePosition);
			if(NULL == pStat)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
				return FALSE;
			}

			if(pStat->iBonusStat == 0)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_USE_BONUS_STAT_CHAR;
				return FALSE;
			}
		}
	}

	// 20080522 Fix Double Logo - 풀코트 유니폼이면 아이템 코드를 첫번째 것으로?
	if( ShopItemInfo.iIsUniform == 1 || ShopItemInfo.iIsUniform == 2 )
	{
		iItemCode = ShopItemInfo.iItemCode0;
	}

	//20070927 Character Slot Send Modification	// 20081204 Add Wonder Girls SpeCialCharacter Slot Type - 이건 또 뭐지;;
	if( CHARACTER_SLOT_TYPE_NORMAL <= ShopItemInfo.iCharacterSlotType )
	{
		iPropertyValue = -1;
	}

	if(ITEM_CATEGORY_CLUB == ShopItemInfo.iCategory)
	{
		if(0 >= pAvatar->iClubSN)
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_JOIN_CLUB;
			return FALSE;
		}

		int iResult = CLUBCONFIGMANAGER.CanBuyClubItem(ShopItemInfo.iItemCode0, pAvatar->btClubGrade, pAvatar->iClubContributionPoint);
		if(iResult != BUY_ITEM_ERROR_SUCCESS)
		{
			iErrorCode = iResult;
			return FALSE;
		}
	}

	int iDressItemTokenItemIdx = -1;
	///////////////////////////////////////// 조건 검사 ///////////////////////////////////////////
	std::string	strItemName;
	int iRecvUserLevel = -1;

	///////////////////////////////////////// 캐쉬 및 포인트 값에 대한 체크 ///////////////////////////////////////////
	int iBuyPriceCash = 0, iBuyPricePoint = 0, iBuyPriceTrophy = 0, iBuyPriceClubCoin = 0;	
	int iItemPropertyPriceCash = 0;
	int iItemPropertyPricePoint = 0;

	SSecretStore* pStoreItem = NULL;
	BYTE btTodayDayIndex = 0;

	if(iOperationCode == FS_ITEM_OP_BUY_SECRETSTOREITEM)
	{
		CHECK_CONDITION_RETURN(FALSE == m_pUser->IsOpenSecretStore(), FALSE);

		pStoreItem = SECRETSTOREMANAGER.GetStoreItem_ItemCode(m_pUser->GetSecretStoreMaxLv_NPU(), iItemCode, m_pUser->GetSecretStoreGrade(iItemCode));

		CHECK_CONDITION_RETURN( NULL == pStoreItem , FALSE );

		iBuyPriceCash = pStoreItem->sItemInfo.iSalePrice;
	}
	else if(FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOperationCode)
	{
		if(CLOSED == SALEPLUS.IsOpen())
		{
			iErrorCode = BUY_ITEM_ERROR_EVENT_SALEPLUS_FAIL;
			return FALSE;
		}

		int iYYYYMMDD = GetYYYYMMDD();
		btTodayDayIndex = SALEPLUS.GetDayIndex(iYYYYMMDD);
		if(TRUE ==  m_pUser->GetUserSalePlusEvent()->GetBuyInfo(btTodayDayIndex))
		{
			iErrorCode = BUY_ITEM_ERROR_EVENT_SALEPLUS_FAIL;
			return FALSE;
		}

		int iSalePrice = 0;
		BOOL bIsBuyYesterday = m_pUser->GetUserSalePlusEvent()->GetBuyInfo(btTodayDayIndex - 1);
		if(FALSE == SALEPLUS.GetTodayProductSalePrice(iYYYYMMDD, ShopItemInfo.iItemCode0, iPropertyValue, bIsBuyYesterday, iSalePrice))
		{
			iErrorCode = BUY_ITEM_ERROR_EVENT_SALEPLUS_FAIL;
			return FALSE;
		}

		iBuyPriceCash = iSalePrice;

		if( FALSE == SetPropertyPriceAndType( pItemShop, ShopItemInfo, iPropertyValue, iPropertyAssignType, aiPropertyIndex, iRecvUserLevel, iBuyPriceCash, iBuyPricePoint,
			iItemPropertyPriceCash, iItemPropertyPricePoint, iErrorCode ) )
		{
			return FALSE;
		}
	}
	else
	{
		if ( FALSE == BuyItemProcess_BeforePay( pODBC, pAvatar, pAvatarItemList, pItemShop, iItemCode, bIsSendItem, iRecvUserGameIDIndex, ShopItemInfo, 
			strItemName, iErrorCode, res, iRecvUserLevel, iPropertyValue ) )
		{
			return FALSE;
		}

		////////////////////////////////////////////////////////////////////////// 해킹 검사. //////////////////////////////////////////////////////////////////////////////	
		if ( FALSE == BuyItemProcess_CheckHacking(pItemShop, pLogODBC, pLoginODBC, ShopItemInfo,iRecvUserLevel,iPropertyValue, iPropertyAssignType,aiPropertyIndex ) )
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
			if(res) *res = SEND_ITEM_ERROR_GENERAL;
			return FALSE;
		}	

		if (TRUE == bIsUseDressItemToken)
		{
			int iDressItemTokenPropertyKind = SShopItemInfo::GetDressItemTokenPropertyKind(ShopItemInfo.iSmallKind);
			SUserItemInfo* pUserItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iDressItemTokenPropertyKind);
			CHECK_NULL_POINTER_BOOL(pUserItem);
			CHECK_CONDITION_RETURN(0 >= pUserItem->iPropertyTypeValue, FALSE);

			iDressItemTokenItemIdx = pUserItem->iItemIdx;
		}
		else if( FS_ITEM_OP_BUY_TODAYHOTDEAL == iOperationCode )
		{
			int iSaleDate = 0;
			SHotDealItem sInfo;

			if(FALSE == TODAYHOTDEALManager.GetTodayHotDealItem(ShopItemInfo.iItemCode0, iPropertyValue, iSaleDate, sInfo) ||
				TODAYHOTDEALManager.GetTodayHotDealSeason(iSaleDate) != m_pUser->GetUserTodayHotDealBuyCount()->_btSeason)
			{
				iErrorCode = BUY_ITEM_ERROR_TODAYHOTDEAL_TIMEOVER;
				return FALSE;
			}
			else if(m_pUser->GetUserTodayHotDealBuyCount()->_iTodayRemainCount[sInfo.btIdx] <= 0)
			{
				iErrorCode = BUY_ITEM_ERROR_TODAYHOTDEAL_LIMITOVER;
				return FALSE;
			}

			switch( sInfo.iSellType )
			{
			case SELL_TYPE_CASH:	iBuyPriceCash = sInfo.iSalePrice;	break;
			case SELL_TYPE_POINT:	iBuyPricePoint = sInfo.iSalePrice;	break;
			case SELL_TYPE_TROPHY:	iBuyPriceTrophy = sInfo.iSalePrice;	break;
			}

			if( FALSE == SetPropertyPriceAndType( pItemShop, ShopItemInfo, iPropertyValue, iPropertyAssignType, aiPropertyIndex, iRecvUserLevel, iBuyPriceCash, iBuyPricePoint,
				iItemPropertyPriceCash, iItemPropertyPricePoint, iErrorCode ) )
			{
				return FALSE;
			}
		}
		else if(FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOperationCode)
		{
			if(FALSE == m_pUser->GetUserSaleRandomItemEvent()->CanBuyStatus())
			{
				iErrorCode = BUY_ITEM_ERROR_SALE_RANDOMITEM_TIME_OVER;
				return FALSE;
			}

			int iSalePrice;
			if(0 == (iSalePrice = SALERANDOMITEMEVENT.GetSalePrice(m_pUser->GetUserSaleRandomItemEvent()->GetCurrentRewardIndex())))
			{
				iErrorCode = BUY_ITEM_ERROR_SALE_RANDOMITEM_TIME_OVER;
				return FALSE;
			}

			iBuyPriceCash = iSalePrice;
		}
		else
		{
			if( FALSE == SetItemPriceBySellType( pItemShop, ShopItemInfo,bIsLocalPublisher, iPropertyValue, iBuyPriceCash, iBuyPricePoint, iBuyPriceTrophy, iBuyPriceClubCoin, iErrorCode ) )
			{
				return FALSE;
			}

			if( FALSE == SetPropertyPriceAndType( pItemShop, ShopItemInfo, iPropertyValue, iPropertyAssignType, aiPropertyIndex, iRecvUserLevel, iBuyPriceCash, iBuyPricePoint,
				iItemPropertyPriceCash, iItemPropertyPricePoint, iErrorCode ) )
			{
				return FALSE;
			}
		}

		if(TRUE == pLimitedEditionItemManager->CheckLimitedEditionItem(iItemCode))
		{
			int iLimitedEditionCount = pLimitedEditionItemManager->GetLimitedEditionCount(iItemCode);
			if(iLimitedEditionCount <= 0)
			{
				iErrorCode = BUY_ITEM_ERROR_LIMITED_EDITION_ERROR;
				return FALSE;
			}
		}

		if(TRUE == pLimitedTimeItemManager->CheckLimitedTimeItem(iItemCode))
		{
			if(FALSE == pLimitedTimeItemManager->CheckCanBuyLimitedTimeItem(iItemCode))
			{
				iErrorCode = BUY_ITEM_ERROR_LIMITED_TIME_ERROR;

				if(TRUE == ITEMSHOPPACKAGEITEMEVENT.CheckPackageItem(ShopItemInfo.iItemCode0))
					iErrorCode = BUY_ITEM_ERROR_ALREADY_BUY_EVENT_PACKAGEITEM;
				else if(ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY == ShopItemInfo.iPropertyKind)
					iErrorCode = BUY_ITEM_ERROR_ALREADY_MAX_LIMITED_EVENT_SUMMER_CANDY;

				return FALSE;
			}

			int iRemainTime = pAvatarItemList->GetBuyRemainTimeLimitedTimeItem(pODBC, m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, pLimitedTimeItemManager->GetMaxCount(iItemCode));
			if(iRemainTime > 0)
			{
				iErrorCode = BUY_ITEM_ERROR_LIMITED_TIME_ERROR;

				if(TRUE == ITEMSHOPPACKAGEITEMEVENT.CheckPackageItem(ShopItemInfo.iItemCode0))
					iErrorCode = BUY_ITEM_ERROR_ALREADY_BUY_EVENT_PACKAGEITEM;
				else if(ITEM_PROPERTY_KIND_EVENT_SUMMER_CANDY == ShopItemInfo.iPropertyKind)
					iErrorCode = BUY_ITEM_ERROR_ALREADY_MAX_LIMITED_EVENT_SUMMER_CANDY;

				return FALSE;
			}
		}

		if(ITEM_PROPERTY_KIND_HONEYWALLET_ITEM == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == HONEYWALLET.IsSellOpen())	// 꿀지갑 아이템 판매기간 종료 체크
			{
				iErrorCode = BUY_ITEM_ERROR_HONEYWALLET_SELL_CLOSE;
				return FALSE;
			}
			else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
			{
				iErrorCode = BUY_ITEM_ERROR_HONEYWALLET_FULL_MAILBOX;
				return FALSE;
			}
			else if(m_pUser->GetUserHoneyWalletEvent()->IsBuyHoneyWallet())
			{
				iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_HOTGIRLSPECIALBOX_ITEM == ShopItemInfo.iPropertyKind)
		{
			if(SEASON_NONE == HOTGIRLSPECIALBOX.GetCurrentSeason())	// 미녀의 특급상자 아이템 판매기간 종료 체크
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_EVENT_POWERUP_CAPSULE == ShopItemInfo.iPropertyKind)
		{
			const OPEN_STATUS _Open = EVENTDATEMANAGER.IsOpen(EVENT_KIND_POWERUP_CAPSULE);
			const BOOL bJustView = EVENTDATEMANAGER.IsView(EVENT_KIND_POWERUP_CAPSULE);

			if(CLOSED == _Open || TRUE == bJustView)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;

				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_FIRSTCLASS_TICKET_ITEM == ShopItemInfo.iPropertyKind)
		{
			m_pUser->GetUserSkyLuckyEvent()->CheckResetDate();

			BYTE btCurrentStepDay = m_pUser->GetUserSkyLuckyEvent()->GetCurrentStepDay();

			if(FALSE == SKYLUCKY.IsCanBuyTicketToday(btCurrentStepDay) ||
				CLOSED == SKYLUCKY.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
			
			int	iBuyTicketTypeFlag = m_pUser->GetUserSkyLuckyEvent()->GetBuyTicketTypeFlag();

			if(TRUE == SKYLUCKY.CheckBuyTicket(btCurrentStepDay, iBuyTicketTypeFlag))
			{
				iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_LEGEND_KEY_ITEM == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == LEGEND.IsOpen())	// 레전드 이벤트 열쇠 아이템 판매기간 종료 체크
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_RANDOM_ARROW_ITEM == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == RANDOMARROW.IsOpen())	// 이벤트 아이템 판매기간 종료 체크
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_DEVILTEMTATION_NORMAL_ITEM == ShopItemInfo.iPropertyKind ||
			ITEM_PROPERTY_KIND_DEVILTEMTATION_PRIMIUM_ITEM == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == DEVILTEMTATION.IsOpen())	// 이벤트 아이템 판매기간 종료 체크
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_SUMMON_STONE == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_COVET))	// 이벤트 아이템 판매기간 종료 체크
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_MISSION_MAKEITEM_CASH_ITEM == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == MISSIONMAKEITEM.IsOpen())	// 이벤트 아이템 판매기간 종료 체크
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_RANDOMITEM_DRAW_COIN == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == RANDOMITEMDRAWEVENT.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_RANDOMITEM_DRAW_COIN == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == SELECTCLOTHESEVENT.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_GOLD_POTION == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == POTIONMAKEEVENT.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_PRIMONBALL == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == PRIMONGO.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}
		}
		else if(MIN_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM <= ShopItemInfo.iPropertyKind &&
			MAX_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM >= ShopItemInfo.iPropertyKind)
		{
			if(FALSE == PACKAGEITEMEVENT.CheckPackageItem(ShopItemInfo.iItemCode0))
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			if(CLOSED == PACKAGEITEMEVENT.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			CShopItemList* pShopItemList = pItemShop->GetShopItemList();
			if(NULL == pShopItemList)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			int iCount = pShopItemList->GetPackageItemComponentCount(ShopItemInfo.iItemCode0);
			if(iCount <= 0)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+iCount > MAX_PRESENT_LIST)
			{
				iErrorCode = BUY_ITEM_ERROR_PRESENT_LIST_FULL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_PACKAGE_ITEM == ShopItemInfo.iPropertyKind)
		{
			if(TRUE == ITEMSHOPPACKAGEITEMEVENT.CheckPackageItem(ShopItemInfo.iItemCode0))
			{
				CShopItemList* pShopItemList = pItemShop->GetShopItemList();
				if(NULL == pShopItemList)
				{
					iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
					return FALSE;
				}

				int iCount = pShopItemList->GetPackageItemComponentCount(ShopItemInfo.iItemCode0);
				if(iCount <= 0)
				{
					iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
					return FALSE;
				}

				if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+iCount > MAX_PRESENT_LIST)
				{
					iErrorCode = BUY_ITEM_ERROR_PRESENT_LIST_FULL;
					return FALSE;
				}
			}
		}
		else if(ITEM_PROPERTY_KIND_PLAYSOUND_ITEM == ShopItemInfo.iPropertyKind)
		{
			if( pAvatar->iSpecialCharacterIndex != KYOUNGLEE_CHARACTER_SPECIALINDEX)
			{
				iErrorCode = BUY_ITEM_ERROR_KYOUNGLEE_PACKAGE_ONLYITEM;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_EVENT_PACKAGE_KYOUNGLEE == ShopItemInfo.iPropertyKind)
		{
			if( pAvatar->iSpecialCharacterIndex != KYOUNGLEE_CHARACTER_SPECIALINDEX)
			{
				iErrorCode = BUY_ITEM_ERROR_KYOUNGLEE_PACKAGE_ONLYITEM;
				return FALSE;
			}

			if(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_KYOUNGLEE_PACKAGE_ITEM))
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			if( FALSE == m_pUser->CheckBuyUser_KyoungLeePackageItem())
			{
				iErrorCode = BUY_ITEM_ERROR_KYOUNGLEE_PACKAGE_BUY_ITEM;
				return FALSE;
			}
			
			CShopItemList* pShopItemList = pItemShop->GetShopItemList();
			if(NULL == pShopItemList)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			int iCount = pShopItemList->GetPackageItemComponentCount(ShopItemInfo.iItemCode0);
			if(iCount <= 0)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			if(m_pUser->GetPresentList(MAIL_PRESENT_ON_AVATAR)->GetSize()+iCount > MAX_PRESENT_LIST)
			{
				iErrorCode = BUY_ITEM_ERROR_PRESENT_LIST_FULL;
				return FALSE;
			}
		}
		else if(ITEM_PROPERTY_KIND_EVENT_SECRET_SHOP_SLOT == ShopItemInfo.iPropertyKind)
		{
			if(CLOSED == SECRETSHOP.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				return FALSE;
			}

			int iUserCashSlotCount = 0;
			int iMaxCashSlotCount = 2;
			SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_EVENT_SECRET_SHOP_SLOT);
			if(NULL != pItem &&
				pItem->iPropertyTypeValue >= iMaxCashSlotCount)
			{
				iErrorCode = BUY_ITEM_ERROR_ALREADY_MAX_LIMITED_EVENT_SECRETSHOP_CASH_SLOT;
				return FALSE;
			}			
		}

		if(ShopItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
			ShopItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
		{
			if(CLOSED == SHOPPINGFESTIVAL.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_CLOSED_EVENT_SALE_COUPON;
				return FALSE;
			}

			BYTE btCurrentEventType = SHOPPINGFESTIVAL.GetCurrentEventType();
			if(EVENT_SHOPPINGFESTIVAL_TYPE_SALE_COUPON != btCurrentEventType)
			{
				iErrorCode = BUY_ITEM_ERROR_CLOSED_EVENT_SALE_COUPON;
				return FALSE;
			}

			if(EVENT_SHOPPINGFESTIVAL_OPEN_STATUS_OPEN != SHOPPINGFESTIVAL.GetOpenStatus(btCurrentEventType))
			{
				iErrorCode = BUY_ITEM_ERROR_CLOSED_EVENT_SALE_COUPON;
				return FALSE;
			}
		}
		else if(ShopItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_EVENT_ACC_BUBBLE_MIN &&
			ShopItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_EVENT_ACC_BUBBLE_MAX)
		{
			if(TRUE == m_pUser->CheckHaveBubbleItemWithManager(ShopItemInfo.iItemCode0))
			{
				iErrorCode = BUY_ITEM_ERROR_HAVE_ITEM;
				return FALSE;
			}
		}

		if(iUseCouponPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
			iUseCouponPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
		{
			if(CLOSED == SHOPPINGFESTIVAL.IsOpen())
			{
				iErrorCode = BUY_ITEM_ERROR_CLOSED_EVENT_SALE_COUPON;
				return FALSE;
			}

			if(ShopItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
				ShopItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_USE_EVENT_SALE_COUPON;
				return FALSE;
			}

			if(ITEM_BIG_KIND_DRESS != ShopItemInfo.iBigKind && ITEM_BIG_KIND_ACC != ShopItemInfo.iBigKind && ITEM_BIG_KIND_ETC != ShopItemInfo.iBigKind)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_USE_EVENT_SALE_COUPON;
				return FALSE;
			}

			if(FALSE == m_pUser->GetUserShoppingFestivalEvent()->CheckUseableSaleCoupon(iUseCouponPropertyKind, iBuyPriceCash))
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_USE_EVENT_SALE_COUPON;
				return FALSE;
			}

			int iSaleCouponCash = 0;
			if(-1 == (iSaleCouponCash = SHOPPINGFESTIVAL.GetSaleCouponCash(iUseCouponPropertyKind)))
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_USE_EVENT_SALE_COUPON;
				return FALSE;
			}

			iBuyPriceCash -= iSaleCouponCash;
			if(iBuyPriceCash < 0)
				iBuyPriceCash = 0;
		}
	}

	if( FALSE == CheckPayAbility( ShopItemInfo, iOperationCode, iLoopCount, iBuyPriceCash, iTotalCash, iBuyPricePoint, iTotalPoint, iBuyPriceTrophy, iTotalTrophy, iBuyPriceClubCoin, res, iErrorCode ) )
	{
		return FALSE;
	}	

	SBillingInfo BillingInfo;

	int iBuyPriceType = -1;

	if( iBuyPriceCash > 0 && iBuyPricePoint > 0 )
	{
		iBuyPriceType = SELL_TYPE_CASH_AND_POINT;
		BillingInfo.iItemPrice = iBuyPriceCash;
	}
	else if( iBuyPriceCash > 0 )	
	{
		iBuyPriceType = SELL_TYPE_CASH;
		BillingInfo.iItemPrice = iBuyPriceCash;
	}
	else if( iBuyPricePoint > 0 )	
	{
		iBuyPriceType = SELL_TYPE_POINT;
		BillingInfo.iItemPrice = iBuyPricePoint;
	}
	else if( iBuyPriceTrophy > 0 )	
	{
		iBuyPriceType = SELL_TYPE_TROPHY;
		BillingInfo.iItemPrice = iBuyPriceTrophy;
	}
	else if( iBuyPriceClubCoin > 0 )
	{
		iBuyPriceType = SELL_TYPE_CLUBCOIN;
		BillingInfo.iItemPrice = iBuyPriceClubCoin;
	}
	else
	{
		iBuyPriceType = ShopItemInfo.iSellType;
	}
	///////////////////////////////////////// 여기서 부터 구입 ///////////////////////////////////////////
	int	iPrevMoney = m_pUser->GetCoin();
	int iPostMoney = iPrevMoney;

	if ( FS_ITEM_OP_RENEW == iOperationCode )
	{
		BillingInfo.iRenewIndex = iRenewItemIdx;
	}

	BillingInfo.pUser = m_pUser;
	memcpy( BillingInfo.szUserID, m_pUser->GetUserID(), MAX_USERID_LENGTH + 1 );	
	memcpy( BillingInfo.szGameID, m_pUser->GetGameID(), MAX_GAMEID_LENGTH + 1 );	
	BillingInfo.iItemCode = iItemCode;
	BillingInfo.bIsPurchasableInEventCoin = ShopItemInfo.bIsPurchasableInEventCoin;
	BillingInfo.iSellType = iBuyPriceType;
	BillingInfo.iCashChange = iBuyPriceCash;
	BillingInfo.iPointChange = iBuyPricePoint;
	BillingInfo.iTrophyChange = iBuyPriceTrophy;
	BillingInfo.iClubCoinChange = iBuyPriceClubCoin;
	BillingInfo.iCurrentCash = iPrevMoney;
	BillingInfo.iSerialNum = m_pUser->GetUserIDIndex();
	BillingInfo.iAvatarLv = m_pUser->GetCurUsedAvatar()->iLv;
	memcpy( BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1);
	memcpy( BillingInfo.szIPAddress, m_pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1 );
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	BillingInfo.iTokenItemIdx = iDressItemTokenItemIdx;
	BillingInfo.iOperationCode = iOperationCode;
	
#ifdef _T2CN_NEW_VERSION_
	BillingInfo.iPropertyType = CheckAndGetPropertyType(ShopItemInfo.iPropertyType, iPropertyValue);
#endif

	BillingInfo.bIsSendItem = bIsSendItem;
	BillingInfo.bIsLocalPublisher = bIsLocalPublisher;
	if( bIsSendItem == TRUE )
	{
		memcpy( BillingInfo.szRecvGameID, recvID, MAX_GAMEID_LENGTH + 1 );	
		memcpy( BillingInfo.szTitle, title, MAX_PRESENT_TITLE_LENGTH + 1 );
		memcpy( BillingInfo.szText, text, MAX_PRESENT_TEXT_LENGTH + 1 );
		BillingInfo.iRecvAvatarLv = iRecvUserLevel;
	}
	

	if( ITEM_PROPERTY_KIND_CLONE_CHARACTER_ITEM == ShopItemInfo.iPropertyKind ) // 혼인서약 아이템이라면.
	{
		return FALSE;
	}

	if( FS_ITEM_OP_BUY_SECRETSTOREITEM == iOperationCode )
	{
		if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
		{
			iErrorCode = BUY_ITEM_ERROR_PRESENT_LIST_FULL;
			return FALSE;	
		}

		CHECK_CONDITION_RETURN(NULL == pStoreItem, FALSE);

		BillingInfo.iEventCode = EVENT_BUY_SECRETSTORE_ITEM;
		BillingInfo.iTerm = iPropertyValue;

		BillingInfo.iValue0 = (-1 == pStoreItem->iStoreIndex) ? m_pUser->GetSecretStoreIndex(iItemCode) : pStoreItem->iStoreIndex;
		BillingInfo.iParam0 = iPropertyValue;
		BillingInfo.iParam1 = aiPropertyIndex[ITEM_PROPERTYINDEX_0];
		BillingInfo.iParam2 = aiPropertyIndex[ITEM_PROPERTYINDEX_1];
		BillingInfo.iParam3 = aiPropertyIndex[ITEM_PROPERTYINDEX_2];	
		BillingInfo.iParam4 = aiPropertyIndex[ITEM_PROPERTYINDEX_3];
		BillingInfo.iParam5 = aiPropertyIndex[ITEM_PROPERTYINDEX_4];
		BillingInfo.iParam6 = aiPropertyIndex[ITEM_PROPERTYINDEX_5];
		BillingInfo.iCurrentCash = iPrevMoney;
		BillingInfo.iItemPropertyPrice = iItemPropertyPricePoint;
		BillingInfo.iItemPropertyType  = PRICE_TYPE_POINT;
		BillingInfo.iItemPropertyPrice2 = iItemPropertyPriceCash;
		BillingInfo.iItemPropertyType2  = PRICE_TYPE_CASH;
	}
	//////////  캐릭터 슬롯이면
	else if( CHARACTER_SLOT_TYPE_NORMAL <= ShopItemInfo.iCharacterSlotType )
	{
		int iCharacterSlotType = 0;								// 20081204 Add Wonder Girls SpeCialCharacter Slot Type

		if( ShopItemInfo.iPropertyKind == CHARACTER_SLOT_KIND_NUM || ShopItemInfo.iPropertyKind == CHARACTER_SLOT_POINT_KIND_NUM)
		{
			iCharacterSlotType = 0;
		}
		else if( ShopItemInfo.iPropertyKind == EXTRA_CHARACTER_SLOT_KIND_NUM || ShopItemInfo.iPropertyKind == EXTRA_CHARACTER_SLOT_POINT_KIND_NUM) 
		{
			iCharacterSlotType = 1;
		}

		BillingInfo.iEventCode = EVENT_BUY_CHARACTERSLOT;
		BillingInfo.iParam0 = iCharacterSlotType;
	}
	/////// 스킬 슬롯이면
	else if( ShopItemInfo.iPropertyKind == SKILL_SLOT_KIND_NUM )
	{
		BillingInfo.iEventCode = EVENT_BUY_SKILLSLOT;
		BillingInfo.iParam0 = iPropertyValue;	
		BillingInfo.iTerm = iPropertyValue;
	}
	/////// 얼굴 바꾸기 아이템
	else if( ShopItemInfo.iPropertyKind == FACE_OFF_KIND_NUM )
	{		
		int iFaceNumber = 0;

		switch(ShopItemInfo.iItemCode0)
		{
		case BASE_FACE_OFF_NUM_7:	iFaceNumber = 8;					break;
		case BASE_FACE_OFF_NUM_8:	iFaceNumber = 0;					break;
		default:	iFaceNumber = ShopItemInfo.iItemCode0 - BASE_FACE_OFF_NUM;		break;
		}	

		BillingInfo.iEventCode = EVENT_BUY_FACEOFF;	// ContentsCode

		BillingInfo.iParam0 = iFaceNumber;	// Face Information
	}
	/////// 체형변경 아이템
	else if( ShopItemInfo.iPropertyKind == CHANGE_BODYSHAPE_KIND_NUM )
	{
		int iBodyShape = ((iItemCode / 10) + 4) ;
0;

		BillingInfo.iEventCode = EVENT_BUY_CHANGE_BODYSHAPE;	// ContentsCode

		BillingInfo.iParam0 = iBodyShape;	// character type Information
	}
	/////// 프리미엄 아이템
	else if( ShopItemInfo.iPropertyKind > PREMIUM_ITEM_KIND_START && ShopItemInfo.iPropertyKind < PREMIUM_ITEM_KIND_END )
	{		
		if( ShopItemInfo.iPropertyKind == PREMIUM_ITEM_KIND_FLAME_MAN )
		{
			aiPropertyIndex[ITEM_PROPERTYINDEX_0] = 1000000;
		}
		
		iPrevMoney = m_pUser->GetCoin() - iTotalCash;
		iPostMoney = iPrevMoney;

		BillingInfo.iEventCode = EVENT_BUY_PREMIUM_ITEM;
		BillingInfo.iParam0 = iPropertyValue;
		BillingInfo.iTerm = iPropertyValue;
		BillingInfo.iParam1 = aiPropertyIndex[ITEM_PROPERTYINDEX_0];
		BillingInfo.iParam2 = aiPropertyIndex[ITEM_PROPERTYINDEX_1];
		BillingInfo.iParam3 = aiPropertyIndex[ITEM_PROPERTYINDEX_2];		
		BillingInfo.iParam4 = aiPropertyIndex[ITEM_PROPERTYINDEX_3];		
		BillingInfo.iParam5 = aiPropertyIndex[ITEM_PROPERTYINDEX_4];		
		BillingInfo.iParam6 = aiPropertyIndex[ITEM_PROPERTYINDEX_5];		
		BillingInfo.iCurrentCash = iPrevMoney;
		BillingInfo.iItemPropertyPrice = iItemPropertyPriceCash;
		BillingInfo.iItemPropertyType  = PRICE_TYPE_CASH;
		BillingInfo.iItemPropertyPrice2 = iItemPropertyPriceCash;
		BillingInfo.iItemPropertyType2  = PRICE_TYPE_POINT;
	}
	else if( ShopItemInfo.iPropertyKind > BIND_ACCOUNT_ITEM_KIND_START && ShopItemInfo.iPropertyKind < BIND_ACCOUNT_ITEM_KIND_END )
	{
		BillingInfo.iEventCode = EVENT_BUY_BIND_ACCOUNT_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
		BillingInfo.iTerm = iPropertyValue;
	}
	else if( ShopItemInfo.iPropertyKind == RESET_SEASON_RECORD_ITEM_KIND_NUM || ShopItemInfo.iPropertyKind == RESET_TOTAL_RECORD_ITEM_KIND_NUM)
	{
		BillingInfo.iEventCode = EVENT_BUY_RESET_RECORD_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
		BillingInfo.iTerm = iPropertyValue;

		float fStartTime = 0.f, fEndTime = 0.f;
		if( CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_RESET_RECORD_CLOSEITEM, CONTENTS_TYPE_RESET_SEASONRECORD_CLOSEITEM_STARTTIME, fStartTime) && 
			CONTENTSMANAGER.GetContentsSetting(CONTENTS_INDEX_RESET_RECORD_CLOSEITEM, CONTENTS_TYPE_RESET_SEASONRECORD_CLOSEITEM_ENDTIME, fEndTime) )
		{
			BOOL bCloseTimeCheck = CheckSeasonResetItem_CloseTime(static_cast<int>(fStartTime), static_cast<int>(fEndTime));

			if( TRUE == bCloseTimeCheck )
			{
				iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
				return FALSE;
			}
		}
	}
	/////// 레벨업이나 경험치업 아이템이라면 - 20090610 Add Tournament Trophy System
	else if( ( ShopItemInfo.iPropertyKind >= MIN_EXP_UP_ITEM_KIND_NUM  && ShopItemInfo.iPropertyKind <= MAX_EXP_UP_ITEM_KIND_NUM  ) ||
		ShopItemInfo.iPropertyKind >= MIN_LEVEL_UP_ITEM_KIND_NUM  && ShopItemInfo.iPropertyKind <= MAX_LEVEL_UP_ITEM_KIND_NUM )
	{
		BillingInfo.iEventCode = EVENT_BUY_LV_UP_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;

		if( TRUE == BILLING_GAME.BuyContents( &BillingInfo, PAY_RESULT_SUCCESS ) )
		{
			iErrorCode = BUY_ITEM_ERROR_SUCCESS;
			return TRUE;
		}
	}
	else if(PROPENSITY_ITEM_KIND_START <= ShopItemInfo.iPropertyKind  && ShopItemInfo.iPropertyKind <= PROPENSITY_ITEM_KIND_END)
	{
		iPrevMoney = m_pUser->GetCoin() - iTotalCash;
		iPostMoney = iPrevMoney;

		BillingInfo.iEventCode = EVENT_BUY_ITEM;
		BillingInfo.iParam0 = iPropertyValue;
		BillingInfo.iTerm = iPropertyValue;
		BillingInfo.iCurrentCash = iPrevMoney;
		BillingInfo.iItemPropertyPrice = iItemPropertyPricePoint;
		BillingInfo.iItemPropertyType  = PRICE_TYPE_POINT;
		BillingInfo.iItemPropertyPrice2 = iItemPropertyPriceCash;
		BillingInfo.iItemPropertyType2  = PRICE_TYPE_CASH;

		int iLevel = 0;
		if( NULL != recvID ) // 선물이라면
		{
			if( ODBC_RETURN_SUCCESS != pODBC->SP_GetAvatarLv_GameID(recvID, iLevel))
			{
				iErrorCode = BUY_ITEM_END_OPERATION;
				return FALSE;
			}

			if( iLevel < ShopItemInfo.iLvCondition)
			{
				iErrorCode = SEND_ITEM_ERROR_MISMATCH_LEVEL;
				if(res) 
					*res = SEND_ITEM_ERROR_MISMATCH_LEVEL;
				
				return FALSE;
			}
		}
		else
			iLevel = m_pUser->GetCurUsedAvtarLv();
		

		CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(ShopItemInfo.iPropertyKind);
		if( NULL != pItemPropertyBoxList )
		{
			int iMaxBoxIdx = pItemPropertyBoxList->GetMaxBoxIdxCount(iLevel, PROPERTY_ASSIGN_TYPE_NONE);
			vector< SItemPropertyBox > vItemPropertyBox;
			
			for( int iBoxIdx = 0; iBoxIdx < iMaxBoxIdx; ++iBoxIdx )
			{
				pItemPropertyBoxList->GetItemPropertyBox(iBoxIdx, iLevel, PROPERTY_ASSIGN_TYPE_NONE, vItemPropertyBox);
			}

			int aiIndex[MAX_ITEM_PROPERTYINDEX_COUNT];
			memset( aiIndex, -1, sizeof(int) * MAX_ITEM_PROPERTYINDEX_COUNT );

			int iItemPropertyBoxSize = vItemPropertyBox.size();
			for( int i = 0; i < iItemPropertyBoxSize; ++i )
			{
				if( MAX_ITEM_PROPERTYINDEX_COUNT <= i )
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, BUY_ITEM, FAIL, "PropertyBoxSize:11866902", iItemPropertyBoxSize);
reak;
				}

				aiIndex[i] = vItemPropertyBox[i].iPropertyIndex;
			}	

			BillingInfo.iParam1 = aiIndex[ITEM_PROPERTYINDEX_0];
			BillingInfo.iParam2 = aiIndex[ITEM_PROPERTYINDEX_1];
			BillingInfo.iParam3 = aiIndex[ITEM_PROPERTYINDEX_2];	
			BillingInfo.iParam4 = aiIndex[ITEM_PROPERTYINDEX_3];
			BillingInfo.iParam5 = aiIndex[ITEM_PROPERTYINDEX_4];
			BillingInfo.iParam6 = aiIndex[ITEM_PROPERTYINDEX_5];
		}	
	}
	else if ( ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MIN <= ShopItemInfo.iPropertyKind && ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MAX >= ShopItemInfo.iPropertyKind &&
		FS_ITEM_OP_BUY_SECRETSTOREITEM != iOperationCode)
	{
		if(ITEM_PROPERTY_KIND_RANDOMITEM_BOARD_TICKET == ShopItemInfo.iPropertyKind &&
			CLOSED == RANDOMITEMBOARDEVENT.IsOpen())
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
			return FALSE;
		}

		BillingInfo.iEventCode = EVENT_BUY_HIGH_FREQUENCY_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if ((ShopItemInfo.iPropertyKind != ITEM_PROPERTY_KIND_EVENT_PACKAGE_KYOUNGLEE) && 
		((ITEM_BIG_KIND_COACHCARD == ShopItemInfo.iBigKind) || (ITEM_BIG_KIND_CLUB_PRIMIUM == ShopItemInfo.iBigKind && ITEM_SMALL_KIND_CLUB_COACHCARD == ShopItemInfo.iSmallKind)))
	{
		CItem* pCoachCardItem = COACHCARD.FindItemByShopItemCode(ShopItemInfo.iItemCode0);
		CHECK_NULL_POINTER_BOOL(pCoachCardItem);

		BillingInfo.iEventCode = EVENT_BUY_COACH_CARD;
		BillingInfo.iParam0 = pCoachCardItem->GetItemIDNumber();
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_HONEYWALLET_ITEM == ShopItemInfo.iPropertyKind)
	{
		BillingInfo.iEventCode = EVENT_BUY_HONEYWALLET_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_HOTGIRLSPECIALBOX_ITEM == ShopItemInfo.iPropertyKind &&
		bIsSendItem == FALSE)
	{
		BillingInfo.iEventCode = EVENT_BUY_HOTGIRLSPECIALBOX_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_FIRSTCLASS_TICKET_ITEM == ShopItemInfo.iPropertyKind)
	{
		BillingInfo.iEventCode = EVENT_BUY_FIRSTCLASS_TICKET_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_LEGEND_KEY_ITEM == ShopItemInfo.iPropertyKind &&
		FALSE == bIsSendItem)
	{
		BillingInfo.iEventCode = EVENT_BUY_LEGEND_KEY_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_RANDOM_ARROW_ITEM == ShopItemInfo.iPropertyKind &&
		FALSE == bIsSendItem)
	{
		BillingInfo.iEventCode = EVENT_BUY_RANDOM_ARROW_ITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if((ITEM_PROPERTY_KIND_DEVILTEMTATION_NORMAL_ITEM == ShopItemInfo.iPropertyKind || ITEM_PROPERTY_KIND_DEVILTEMTATION_PRIMIUM_ITEM == ShopItemInfo.iPropertyKind) &&
		FALSE == bIsSendItem)
	{
		BillingInfo.iEventCode = EVENT_BUY_DEVILTEMTATION;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_SUMMON_STONE == ShopItemInfo.iPropertyKind)
	{
		if(TRUE == BillingInfo.bIsSendItem)
		{
			BillingInfo.iEventCode = EVENT_BUY_ITEM;
			BillingInfo.iParam0 = iPropertyValue;
			BillingInfo.iParam1 = aiPropertyIndex[ITEM_PROPERTYINDEX_0];
			BillingInfo.iParam2 = aiPropertyIndex[ITEM_PROPERTYINDEX_1];
			BillingInfo.iParam3 = aiPropertyIndex[ITEM_PROPERTYINDEX_2];	
			BillingInfo.iParam4 = aiPropertyIndex[ITEM_PROPERTYINDEX_3];
			BillingInfo.iParam5 = aiPropertyIndex[ITEM_PROPERTYINDEX_4];
			BillingInfo.iParam6 = aiPropertyIndex[ITEM_PROPERTYINDEX_5];
		}
		else
		{
			BillingInfo.iEventCode = EVENT_BUY_COVET_SUMMON_STONE;
			BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
			BillingInfo.iParam1 = iPropertyValue;
		}
	}
	else if(ITEM_PROPERTY_KIND_MISSION_MAKEITEM_CASH_ITEM == ShopItemInfo.iPropertyKind &&
		FALSE == bIsSendItem)
	{
		BillingInfo.iEventCode = EVENT_BUY_MISSIONMAKEITEM;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_EVENT_HALFPRICE_NEEDLE == ShopItemInfo.iPropertyKind &&
		FALSE == bIsSendItem)
	{
		BillingInfo.iEventCode = EVENT_BUY_EVENT_HALFPRICE_NEEDLE;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_PACKAGE_DARKMARKET == ShopItemInfo.iPropertyKind)
	{
		if(FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE != iOperationCode)
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
			return FALSE;
		}

		BYTE btProductIndex = 0;
		int iBonusCnt = 0;

		if(FALSE == m_pUser->GetUserDarkMarketEvent()->CheckCanBuyPackageProduct(ShopItemInfo.iItemCode0, btProductIndex, iBonusCnt, iErrorCode))
		{
			return FALSE;
		}

		BillingInfo.iEventCode = EVENT_BUY_DARKMARKET_PACKAGE_PRODUCT;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = static_cast<int>(btProductIndex);
		BillingInfo.iParam2 = iBonusCnt;
	}
	else if(ITEM_PROPERTY_KIND_CHEERLEADER == ShopItemInfo.iPropertyKind)
	{
		SCheerLeaderConfig* pConfig = CLUBCONFIGMANAGER.GetCheerLeaderConfig(ShopItemInfo.iCheerLeaderIndex);
		CHECK_CONDITION_RETURN(NULL == pConfig, FALSE);

		BillingInfo.iEventCode = EVENT_BUY_CHEERLEADER;
		strcpy_s(BillingInfo.szItemName, "Buy CheerLeader");
		BillingInfo.iParam0 = ShopItemInfo.iCheerLeaderIndex;
		BillingInfo.iCurrentCash = iPrevMoney;
		BillingInfo.iItemPropertyPrice = iItemPropertyPricePoint;
		BillingInfo.iItemPropertyType  = PRICE_TYPE_POINT;
		BillingInfo.iItemPropertyPrice2 = iItemPropertyPriceCash;
		BillingInfo.iItemPropertyType2  = PRICE_TYPE_CASH;
		BillingInfo.iParam0 = iPropertyValue;
	}
	else if(ITEM_PROPERTY_KIND_PACKAGE_ITEM_GIVE_INVENTORY == ShopItemInfo.iPropertyKind)
	{
		if(LV_INTERVAL_BUFF_PACKAGE_ITEMCODE == ShopItemInfo.iItemCode0)
		{
			CHECK_NULL_POINTER_BOOL(m_paItemList);

			for(int i = MIN_ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF; i <= MAX_ITEM_PROPERTY_KIND_LV_INTERVAL_BUFF; ++i )
			{
				if(NULL != m_paItemList->GetLvIntervalBuffItem(i))
				{
					iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
					return FALSE;
				}
			}
		}

		BillingInfo.iEventCode = EVENT_BUY_PACKAGE_ITEM_GIVE_INVENTORY;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else if(MIN_ITEM_PROPERTY_KIND_PIECE_TYPE <= ShopItemInfo.iPropertyKind && MAX_ITEM_PROPERTY_KIND_PIECE_TYPE >= ShopItemInfo.iPropertyKind && 0 == bIsSendItem)
	{
		BillingInfo.iEventCode = EVENT_BUY_SPECIAL_PIECE;
		BillingInfo.iParam0 = ShopItemInfo.iPropertyKind;
		BillingInfo.iParam1 = iPropertyValue;
	}
	else
	{	
		iPrevMoney = m_pUser->GetCoin() - iTotalCash;
		iPostMoney = iPrevMoney;

		BillingInfo.iEventCode = EVENT_BUY_ITEM;
		if(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN <= ShopItemInfo.iPropertyKind  && ShopItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
		{
			BillingInfo.iEventCode = EVENT_BUY_CHANGE_DRESS;
		}
		else if(ShopItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_PACKAGE_ITEM ||
			ShopItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_EVENT_PACKAGE_KYOUNGLEE)
		{
			BillingInfo.iEventCode = EVENT_BUY_PACKAGE_ITEM;
		}
		else if(MIN_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM <= ShopItemInfo.iPropertyKind &&
			MAX_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM >= ShopItemInfo.iPropertyKind)
		{
			BillingInfo.iEventCode = EVENT_BUY_PACKAGE_ITEM;
		}
			
		BillingInfo.iTerm	= iPropertyValue;
		BillingInfo.iParam0 = iPropertyValue;
		BillingInfo.iParam1 = aiPropertyIndex[ITEM_PROPERTYINDEX_0];
		BillingInfo.iParam2 = aiPropertyIndex[ITEM_PROPERTYINDEX_1];
		BillingInfo.iParam3 = aiPropertyIndex[ITEM_PROPERTYINDEX_2];	
		BillingInfo.iParam4 = aiPropertyIndex[ITEM_PROPERTYINDEX_3];
		BillingInfo.iParam5 = aiPropertyIndex[ITEM_PROPERTYINDEX_4];
		BillingInfo.iParam6 = aiPropertyIndex[ITEM_PROPERTYINDEX_5];
		BillingInfo.iCurrentCash = iPrevMoney;
		BillingInfo.iItemPropertyPrice = iItemPropertyPricePoint;
		BillingInfo.iItemPropertyType  = PRICE_TYPE_POINT;
		BillingInfo.iItemPropertyPrice2 = iItemPropertyPriceCash;
		BillingInfo.iItemPropertyType2  = PRICE_TYPE_CASH;
	}

	m_pUser->SetItemCode( BillingInfo.iItemCode );
	m_pUser->SetPayCash( BillingInfo.iCashChange );

	char* szItemName = pItemShop->GetItemTextCheckAndReturnEventCodeDescription(BillingInfo.iItemCode, BillingInfo.iEventCode );
	
	if( NULL == szItemName )
		printf(BillingInfo.szItemName, "Buy_ItemShop");
	else
		memcpy( BillingInfo.szItemName, szItemName, MAX_ITEMNAME_LENGTH + 1 );	

	if(FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOperationCode)
	{
		BillingInfo.iEventCode = SALERANDOMITEMEVENT.GetBuyEventCode(iItemCode, iPropertyValue);
		if(BillingInfo.iEventCode == 0)
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
			return FALSE;
		}
		BillingInfo.bSaleRandomItemEvent = TRUE;
		BillingInfo.iPropertyValue = iPropertyValue;
	}
	else if(FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOperationCode)
	{
		BillingInfo.iValue0 = static_cast<int>(btTodayDayIndex);
	}

	if( TRUE == BILLING_GAME.RequestPurchaseItem( BillingInfo) )
	{
		m_pUser->CheckEvent(PERFORM_TIME_ITEMBUY,pODBC);

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		if(res) *res = SEND_ITEM_ERROR_SUCCESS;

		iTotalCash = iTotalCash + iBuyPriceCash;	
		iTotalPoint = iTotalPoint + iBuyPricePoint;	
		iTotalTrophy = iTotalTrophy + iBuyPriceTrophy;	

		if(FS_ITEM_OP_BUY_SALE_RANDOMITEM != iOperationCode)
		{
			if(TRUE == pLimitedEditionItemManager->CheckLimitedEditionItem(iItemCode))
			{
				pLimitedEditionItemManager->UpdateLimitedEditionItemDB(pODBC, ShopItemInfo.iItemCode0);

				CCenterSvrProxy* pCenterSvrProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
				if(pCenterSvrProxy)
				{
					CPacketComposer PacketComposer(G2S_LIMITED_EDITION_ITEM_COUNT_REQ);
					SG2S_LIMITED_EDITION_ITEM_COUNT info;
					info.btKind = 1;
					info.iItemCode = ShopItemInfo.iItemCode0;
					info.iCurrentCount  = pLimitedEditionItemManager->GetCurrentCount(ShopItemInfo.iItemCode0)+1;
					info.iLimitedEditionCount = pLimitedEditionItemManager->GetMaxCount(ShopItemInfo.iItemCode0) - info.iCurrentCount;
					PacketComposer.Add( (BYTE*)&info,  sizeof(SG2S_LIMITED_EDITION_ITEM_COUNT));
					pCenterSvrProxy->Send(&PacketComposer);
				}
			}
			else if(TRUE == pLimitedTimeItemManager->CheckLimitedTimeItem(iItemCode))
			{
				int iBuyCount = 1;
				pAvatarItemList->UpdateLimitedTimeItem(pODBC, m_pUser->GetGameIDIndex(), iItemCode, iBuyCount);

				//m_pUser->SendRookieItemNotice(TRUE);
			}
		}

		if(BillingInfo.iPointChange > 0)
		{
			m_pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BUY_POINT_ITEM);
			m_pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_BUY_POINT_ITEM);
		}

		if(iUseCouponPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
			iUseCouponPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
		{
			m_pUser->GetUserShoppingFestivalEvent()->UseSaleCoupon(BillingInfo.iEventCode, iUseCouponPropertyKind, BillingInfo.iItemCode);
			m_pUser->GetUserShoppingFestivalEvent()->SendEventOpenStatus();
		}

		if(ITEM_CATEGORY_CLUB == ShopItemInfo.iCategory)
		{
			m_pUser->SendClubShopUserCashInfo();
		}

		return TRUE;
	}
	m_pUser->ResetUserData();

	return FALSE;
}


// 20070903 New Item - 2
BOOL CFSGameUserItem::InsertUserItemProperty( int iGameIDIndex, int iItemIdx, CAvatarItemList *pAvatarItemList, vector< SItemProperty > vItemProperty )
{
	if( vItemProperty.empty() )
	{
		return FALSE;
	}

	for( int i = 0; i < vItemProperty.size() ; i ++ )
	{
		SUserItemProperty tempUserItemProperty;
		tempUserItemProperty.iGameIDIndex	= iGameIDIndex;
		tempUserItemProperty.iItemIdx		= iItemIdx;
		tempUserItemProperty.iProperty		= vItemProperty[i].iProperty;
		tempUserItemProperty.iValue			= vItemProperty[i].iValue;

		pAvatarItemList->AddUserItemProperty( tempUserItemProperty );
	}

	return TRUE;
}
// End

// 20070913 Quick Inven Merge
BOOL CFSGameUserItem::ApplyBagItem(SAvatarInfo* pAvatar, short *naItemIdx, BYTE byItemNum, int iApplyAccItemIdx[MAX_USER_APPLY_ACCITEM_PROPERTY], int *naPCRoomItemPropertyIdx1, int *naPCRoomItemPropertyIdx2, map<int/*iSpecialPartsIndex*/, vector<int/*iProperty*/>> mapProperty)
{
	if(!pAvatar) return FALSE;

	CAvatarItemList* pAvatarItemList = GetCurAvatarItemList();
	if(!pAvatarItemList) return FALSE;

	CFSGameServer* pServer = (CFSGameServer*)CFSGameServer::GetInstance();
	if(!pServer) return FALSE;

	CFSItemShop* pItemShop = pServer->GetItemShop();
	if(!pItemShop) return FALSE;

	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pFSODBC );

	if(FALSE == pAvatarItemList->ApplyBagItemUserApplyAccItemProperty((CFSODBCBase*)pFSODBC, pAvatar->iGameIDIndex, iApplyAccItemIdx))
	{
		//return FALSE;
	}

	//착용중 아이템들 초기화 하고
	for(int j=0; j<MAX_ITEMCHANNEL_NUM; ++j)
	{
		if(m_UseItem.iaItemIdx[j] == -1) continue;

		pAvatarItemList->ChangeItemStatus(m_UseItem.iaItemIdx[j], ITEM_STATUS_INVENTORY);
		m_UseItem.iaItemIdx[j] = -1;	
		m_UseItem.iaFeature[j] = -1;
	}
	m_UseItem.iChannel = 0;

	for(int i=0; i<MAX_CHANGE_DRESS_NUM; ++i)
	{
		if(m_UseItem.iaChangeDressItemIdx[i] == -1) 
			continue;

		pAvatarItemList->ChangeItemStatus(m_UseItem.iaChangeDressItemIdx[i], ITEM_STATUS_INVENTORY);
		m_UseItem.iaChangeDressItemIdx[i] = -1;	
		m_UseItem.iaChangeDressFeature[i] = -1;
	}
	m_UseItem.iChangeDressChannel = 0;

	int iItemNum = 0, iChangeDressItemNum = 0;
	for(BYTE i=0; i<byItemNum; ++i)
	{
		SUserItemInfo* pApplyItem = pAvatarItemList->GetItemWithItemIdx(naItemIdx[i]);
		if(!pApplyItem)
		{
			RestoreUseFeatureInfo();
			return FALSE;
		}

		if(pApplyItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pApplyItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
		{
			if((m_UseItem.iChangeDressChannel & pApplyItem->iChannel) > 0)
			{
				RestoreUseFeatureInfo();
				return FALSE;
			}

			m_UseItem.iaChangeDressFeature[iChangeDressItemNum] = pApplyItem->iItemCode;
			m_UseItem.iaChangeDressItemIdx[iChangeDressItemNum] = naItemIdx[i];
			m_UseItem.iChangeDressChannel |= pApplyItem->iChannel;

			iChangeDressItemNum++;
		}
		else
		{	
			if((m_UseItem.iChannel & pApplyItem->iChannel) > 0)
			{
				RestoreUseFeatureInfo();
				return FALSE;
			}

			m_UseItem.iaFeature[iItemNum] = pApplyItem->iItemCode;
			m_UseItem.iaItemIdx[iItemNum] = naItemIdx[i];
			m_UseItem.iChannel |= pApplyItem->iChannel;

			//선택한 PC방 아이템 능력치 넣어줌
			if ( pApplyItem->iPropertyKind >= MIN_PREMIUM_STAT_OPTION_ITEM_NUM && pApplyItem->iPropertyKind <= MAX_PREMIUM_STAT_OPTION_ITEM_NUM )
			{
				pApplyItem->iPropertyNum = 0;
				GetCurAvatarItemList()->RemoveItemProperty(pApplyItem->iItemIdx);

				if (naPCRoomItemPropertyIdx1[i] != -1)
				{
					CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList( naPCRoomItemPropertyIdx1[i] );
					CHECK_NULL_POINTER_BOOL(pItemPropertyList);

					SItemProperty* pItemProperty = pItemPropertyList->GetItemProperty( 0 );
					CHECK_NULL_POINTER_BOOL(pItemProperty);

					SUserItemProperty tempUserItemProperty;
					tempUserItemProperty.iGameIDIndex	= pAvatar->iGameIDIndex;
					tempUserItemProperty.iItemIdx		= pApplyItem->iItemIdx;
					tempUserItemProperty.iProperty		= pItemProperty->iProperty;
					tempUserItemProperty.iValue			= pItemProperty->iValue;

					pAvatarItemList->AddUserItemProperty( tempUserItemProperty );			
					pApplyItem->iPropertyNum++;
				}
				if (naPCRoomItemPropertyIdx2[i] != -1)
				{
					CItemPropertyList* pItemPropertyList = pItemShop->GetItemPropertyList( naPCRoomItemPropertyIdx2[i] );
					CHECK_NULL_POINTER_BOOL(pItemPropertyList);

					SItemProperty* pItemProperty = pItemPropertyList->GetItemProperty( 0 );
					CHECK_NULL_POINTER_BOOL(pItemProperty);

					SUserItemProperty tempUserItemProperty;
					tempUserItemProperty.iGameIDIndex	= pAvatar->iGameIDIndex;
					tempUserItemProperty.iItemIdx		= pApplyItem->iItemIdx;
					tempUserItemProperty.iProperty		= pItemProperty->iProperty;
					tempUserItemProperty.iValue			= pItemProperty->iValue;

					pAvatarItemList->AddUserItemProperty( tempUserItemProperty );			
					pApplyItem->iPropertyNum++;
				}
			}

			iItemNum++;
		}

		BYTE bStatus = 0;
		bStatus = pAvatarItemList->GetUserPropertyAccItemStatus(pApplyItem->iItemIdx, pApplyItem->iBigKind, pApplyItem->iStatus, pApplyItem->iPropertyKind);

		pAvatarItemList->ChangeItemStatus(pApplyItem->iItemIdx, ITEM_STATUS_WEAR, bStatus);
	}

	CheckBasicItemNeed(m_UseItem);

	if(ODBC_RETURN_SUCCESS != pFSODBC->spFSUseItem(pAvatar->iGameIDIndex, m_UseItem))
	{
		RestoreUseFeatureInfo();
		return FALSE;
	}

	memcpy( pAvatar->AvatarFeature.FeatureInfo, m_UseItem.iaFeature, sizeof(int)*MAX_ITEMCHANNEL_NUM );
	pAvatar->AvatarFeature.iTotalChannel = m_UseItem.iChannel;

	for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
	{
		if(-1 != pAvatar->AvatarFeature.FeatureInfo[i])
		{
			SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.FeatureInfo[i]);
			if(NULL != pSlotItem)
			{
				pAvatar->AvatarFeature.FeatureItemIndex[i] = pSlotItem->iItemIdx;
				pAvatar->AvatarFeature.FeatureProtertyType[i] = pSlotItem->iPropertyType;
				pAvatar->AvatarFeature.FeatureProtertyKind[i] = pSlotItem->iPropertyKind;
				pAvatar->AvatarFeature.FeatureProtertyChannel[i] = pSlotItem->iChannel;
				pAvatar->AvatarFeature.FeatureProtertyValue[i] = pSlotItem->iPropertyTypeValue;
			}
		}
	}

	memcpy( pAvatar->AvatarFeature.ChangeDressFeatureInfo, m_UseItem.iaChangeDressFeature, sizeof(int)*MAX_CHANGE_DRESS_NUM );
	pAvatar->AvatarFeature.iTotalChangeDressChannel = m_UseItem.iChangeDressChannel;

	for(int i=0; i<MAX_CHANGE_DRESS_NUM; i++)
	{
		if(-1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[i])
		{
			SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i]);
			if(NULL != pSlotItem)
			{
				pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[i] = pSlotItem->iItemIdx;
				pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i] = pSlotItem->iChannel;
			}
		}
	}

	SAvatarStyleInfo* pAvatarStyleInfo = m_pUser->GetStyleInfo();
	if (NULL != pAvatarStyleInfo)
	{
		pAvatarStyleInfo->SetCurrentStyleItemIdx(m_UseItem);
	}

	int iFaceItemCode = pAvatar->AvatarFeature.GetFaceItemCode(pAvatar->iSpecialCharacterIndex);
	int iFace = AVATARCREATEMANAGER.GetMultipleFaceCharacterFace(pAvatar->iSpecialCharacterIndex, iFaceItemCode);	 
	if(iFace != -1)
		pAvatar->iFace = iFace;

	memcpy(&m_OriginalItem, &m_UseItem, sizeof(SFeatureInfo));

	map<int/*iSpecialPartsIndex*/, vector<int/*iProperty*/>>::iterator iter = mapProperty.begin();
	for( ; iter != mapProperty.end(); ++iter)
	{
		int iSpecialPartsIndex = iter->first;
		vector<int/*iProperty*/> vProperty = iter->second;

		BYTE btEquipCount = pAvatarItemList->GetSpecialPartsItemEquipCount(iSpecialPartsIndex);

		SSpecialPartsPropertyKey sKey(iSpecialPartsIndex, btEquipCount);
		vector<SSpecialPartsPropertyList> vecPropertyList;
		SPECIALPARTS.GetSpecialPartsChoicePropertyList(sKey, vecPropertyList);
		int iStatNum = vecPropertyList.size();

		if(vProperty.size() != iStatNum)
			continue;

		m_pUser->UpdateSpecialPartsUserProperty(iSpecialPartsIndex, vProperty);
	}

	return TRUE;
}
// End

// 20070919 Functional Item
BOOL CFSGameUserItem::UseSecondBoostItem(int& iItemCode)
{
	iItemCode = -1;

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();

	if (NULL == pAvatarItemList) 
	{			
		return FALSE;
	}

	vector< SUserItemInfo* > vUserItemInfo;

	pAvatarItemList->GetEquipItemList( vUserItemInfo );

	for(int i=0 ; i< vUserItemInfo.size() ;i++)
	{
		if (vUserItemInfo[i]->iPropertyKind >= MIN_SECOND_BOOST_ITEM_KIND_NUM && vUserItemInfo[i]->iPropertyKind <= MAX_SECOND_BOOST_ITEM_KIND_NUM)	
		{
			if (vUserItemInfo[i]->iStatus == 2)	
			{
				iItemCode = vUserItemInfo[i]->iItemCode;	

#ifdef _DEBUG
				TRACE(" ItemCode	--> [11866902]\n", vUserItemInfo[i]->iItemCode);
ACE(" PropertyKind	--> [11866902]\n", vUserItemInfo[i]->iPropertyKind );
ACE(" Channel	--> [11866902]\n", vUserItemInfo[i]->iChannel);

				return TRUE;
			}						
		}
	}

	return FALSE;
}


// 20070918 EquipItem
int CFSGameUserItem::GetMyInventoryItemCount()
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	return pAvatarItemList->GetItemEquipAllCount();
}
// End

// 20071213 Face Off
void CFSGameUserItem::GetTempFace(int& iFace)
{
	if( ITEM_PAGE_MODE_SHOP == m_iMode ) //shop
		iFace = m_UseTryItem.iFace;
	else if(ITEM_PAGE_MODE_MYITEM == m_iMode || ITEM_PAGE_MODE_CLUBSHOP == m_iMode ) //user, club shop 
		iFace = m_UseItem.iFace;
}
// End

// 20080623 China Billing Process
void CFSGameUserItem::SetTempFace(int iFace)
{
	m_UseTryItem.iFace = iFace;
	m_UseItem.iFace= iFace;
}
// End

void CFSGameUserItem::GetTempCharacterType( int& iCharacterType )
{
	if( ITEM_PAGE_MODE_SHOP == m_iMode ) //shop
		iCharacterType = m_UseTryItem.iBodyShape;
	else if( ITEM_PAGE_MODE_MYITEM == m_iMode || ITEM_PAGE_MODE_CLUBSHOP == m_iMode ) //user, club shop 
		iCharacterType = m_UseItem.iBodyShape;
}

void CFSGameUserItem::SetTempCharacterType( int iCharacterType )
{
	m_UseTryItem.iBodyShape = iCharacterType;
	m_UseItem.iBodyShape = iCharacterType;

}

// 20080212 Korea Shout Item
BOOL	CFSGameUserItem::UpdateFuncItemStatus(CFSGameODBC *pFSODBC, int iItemIdx, int iStatus, int* iaRemoveItemIdx /*= NULL*/)
{
	CAvatarItemList* pItemList = GetCurAvatarItemList();
	if( NULL == pItemList ) return FALSE;

	SUserItemInfo* pItem = pItemList->GetItemWithItemIdx( iItemIdx );
	if( NULL == pItem ) return FALSE;

	int iRemoveItemIdx = -1;
	SUserItemInfo* pRemoveItem = NULL;

	if( iStatus == 3 )
	{
		pRemoveItem = pItemList->GetItemWithPropertyKind(pItem->iPropertyKind);
		if( NULL != pRemoveItem && NULL != iaRemoveItemIdx)
		{
			iaRemoveItemIdx[0] = pRemoveItem->iItemIdx;
			iRemoveItemIdx = pRemoveItem->iItemIdx;
		}
	}

	if( ODBC_RETURN_SUCCESS == pFSODBC->spUpdateItemStatus(m_pUser->GetGameIDIndex(), pItem->iItemIdx, iStatus, iRemoveItemIdx) )
	{
		if( NULL != pRemoveItem )
		{
			pRemoveItem->iStatus = 1;
		}

		pItem->iStatus = iStatus;
		return TRUE;
	}

	return FALSE;
}
// End
BOOL	CFSGameUserItem::UpdateBuffItemStatus(CFSGameODBC *pFSODBC, int iItemIdx, int iStatus, int* iaRemoveItemIdx /*= NULL*/)
{
	CAvatarItemList* pItemList = GetCurAvatarItemList();
	if( NULL == pItemList ) return FALSE;

	SUserItemInfo* pItem = pItemList->GetItemWithItemIdx( iItemIdx );
	if( NULL == pItem ) return FALSE;

	int iRemoveItemIdx = -1;
	SUserItemInfo* pRemoveItem = NULL;

	if( iStatus == 3 )
	{
		pRemoveItem = pItemList->GetItemWithPropertyKind(pItem->iPropertyKind);
		if( NULL != pRemoveItem && NULL != iaRemoveItemIdx)
		{
			iaRemoveItemIdx[0] = pRemoveItem->iItemIdx;
			iRemoveItemIdx = pRemoveItem->iItemIdx;
		}
	}
	time_t ExpireTime = -1;

	if( ODBC_RETURN_SUCCESS == pFSODBC->spUpdateBuffItemStatus(m_pUser->GetGameIDIndex(), pItem->iItemIdx, iStatus, iRemoveItemIdx, ExpireTime) )
	{
		if( NULL != pRemoveItem )
		{
			pRemoveItem->iStatus = 0;
		}
		pItem->iStatus = iStatus;
		pItem->iPropertyTypeValue = pItem->iSellPrice;
		pItem->ExpireDate = ExpireTime;

		return TRUE;
	}

	return FALSE;
}
// 20080304 Add SellPrice Module
int CFSGameUserItem::GetSellPrice( int iBuyPriceCash, int iBuyPricePoint )
{
	int iAddCash = 0;
	int iAddPoint = 0;
	int iRetPoint = 0;

	if( iBuyPriceCash > 0 )
	{
		iAddCash = iBuyPriceCash * 18;
	}
	if( iBuyPricePoint > 0 )
	{
		iAddPoint = iBuyPricePoint;
	}

	iRetPoint = iAddCash + iAddPoint;

	return iRetPoint;
}

// 20080319 Add Premium Item
bool CFSGameUserItem::InsertPremiumItem( CAvatarItemList *pAvatarItemList, SShopItemInfo &ShopItemInfo , SUserItemInfo &UserItemInfo, int iPropertyIndex1, int iPropertyIndex2, int iPropertyIndex3)
{
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	if( pItemShop == NULL ) return false;

	UserItemInfo.SetBaseInfo(ShopItemInfo);
	UserItemInfo.iSellType = SELL_TYPE_POINT;
	UserItemInfo.iSellPrice = 0;
	UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;

	if( ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST )
	{
		UserItemInfo.iPropertyKind = 0;
	}
	else
	{
		UserItemInfo.iPropertyKind = ShopItemInfo.iPropertyKind;
	}

	int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {iPropertyIndex1,iPropertyIndex2,iPropertyIndex3,-1,-1,-1};
	ITEM_PROPERTY_VECTOR vecItemProperty;
	pItemShop->GetItemPropertyList(iaPropertyIndex, vecItemProperty);

	InsertUserItemProperty( m_pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, pAvatarItemList, vecItemProperty);
	UserItemInfo.iPropertyNum = vecItemProperty.size();
					
	pAvatarItemList->AddItem(UserItemInfo);
	return true;	
}

// 20080424 Add Pause Item
int CFSGameUserItem::DiscountPauseItem()
{
	CFSGameODBC *pODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_CONDITION_RETURN( NULL == pODBC, -4);

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar) 
	{
		return -4;
	}

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList )
	{
		return -4;
	}

	SUserItemInfo* ItemInfo = pAvatarItemList->GetItemWithChannelAndItemCode(0,PAUSE_ITEM_CODE);

	if( ItemInfo != NULL )
	{
		if( ItemInfo->iPropertyType == ITEM_PROPERTY_EXHAUST )
		{
			if( ( ItemInfo->iPropertyTypeValue - 1 ) <= 0 )
			{
				if( ( ItemInfo->iPropertyTypeValue - 1 ) != 0 )
				{
					return -4;
				}

				int iEmptySlot = -1;
				if( ODBC_RETURN_SUCCESS != pODBC->spUpdateNoEquipItemExhaustNew( pAvatar->iGameIDIndex, ItemInfo->iChannel, iEmptySlot , ItemInfo->iItemIdx , ItemInfo->iItemCode ))
				{
					return -4;
				}

				ItemInfo->iPropertyTypeValue--;

				if( ItemInfo->iPropertyTypeValue <= 0 )
				{
					pAvatarItemList->RemoveItem(ItemInfo->iItemIdx);
					return -1;
				}

				return ItemInfo->iPropertyTypeValue;
			}
			else
			{
				if( ODBC_RETURN_SUCCESS == pODBC->spUpdateNoEquipItemCount(pAvatar->iGameIDIndex , ItemInfo->iItemIdx , ItemInfo->iItemCode ) )
				{
					ItemInfo->iPropertyTypeValue--;

					return ItemInfo->iPropertyTypeValue;
				}
			}

		}
	}
	else
	{
		return -2;
	}

	return -1;
}
// End

// 20080826 Add BillingInfo In Billing System
int CFSGameUserItem::CheckAndGetPropertyType(int iPropertyType, int iPropertyValue)
{
	if(iPropertyType == 1 && iPropertyValue == -1)	// iPropertyType 이 시간제라도 PropertyValue가 -1이면 영구제로 취급
	{
		iPropertyType = 0;
	}

	return iPropertyType;
}
// End

// 20081120 아이템 Lock 기능 추가
BOOL CFSGameUserItem::ChangeItemLockStatus( int iItemIdx, int iItemCode, int& iRet )
{
	CFSGameODBC *pFSODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_BOOL( pFSODBC );

	CAvatarItemList* pItemList = GetCurAvatarItemList();
	if( NULL == pItemList ) return FALSE;

	SUserItemInfo* pItem = pItemList->GetItemWithItemIdx( iItemIdx );
	if( NULL == pItem ) return FALSE;

	if( ODBC_RETURN_SUCCESS != pFSODBC->LOCK_UpdateItem( m_pUser->GetGameIDIndex(), iItemIdx, iItemCode, iRet ))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "LOCK_UpdateItem - GameIDIndex:11866902, ItemIdx:0, ItemCode:5156864", m_pUser->GetGameIDIndex(), iItemIdx, iItemCode);
ALSE;
	}

	if( iRet == -1 )
	{
		return FALSE;
	}

	if( iRet == 0 )
	{
		pItem->sIsLock = 1;
		return TRUE;
	}

	return FALSE;
}
// End

BOOL CFSGameUserItem::BuyItemProcess_BeforePay( CFSGameODBC *pFSODBC, SAvatarInfo* pAvatar, CAvatarItemList *pAvatarItemList, CFSItemShop *pItemShop, 
											   int iItemCode, BOOL bIsSendItem, int iRecvUserGameIDIndex, SShopItemInfo &ShopItemInfo, 
											   std::string &strItemName, int &iErrorCode, int* res, int &iContrastVar_Lv, int iPropertyValue )
{
	if( pFSODBC == NULL || pAvatar == NULL || pAvatarItemList == NULL )
	{
		return FALSE;
	}
	
	// 구입자인지 선물 받는 사람인지 구분이 중요
	int iContrastVar_Sex = -1;
	int iContrastVar_SpecialCharacterIndex = -1;
	int iContrastVar_SpecialTeamIndex = -1;
	int iContrastVariable_PresentCnt = 0;
	int iContrastVar_IsHeight = -1;
	int iContrastVar_Position = -1;
	int iContrastVar_CharacterType = -1;

	// 비교할 데이터 설정 - 성별, 레벨, 스페셜캐릭터인덱스, 우승횟수	
	int iContrastVariable_ChampionCnt = m_pUser->GetTournamentChampionCnt();
	if( bIsSendItem == TRUE )
	{
		if( ODBC_RETURN_SUCCESS != pFSODBC->spGetAvatarSexLv( iRecvUserGameIDIndex , iContrastVar_Lv, iContrastVar_Sex, iContrastVar_Position, iContrastVar_SpecialCharacterIndex, iContrastVar_CharacterType ) 
			|| ODBC_RETURN_SUCCESS != pFSODBC->GetPresentCnt( MAIL_PRESENT_ON_AVATAR, -1, iRecvUserGameIDIndex, iContrastVariable_PresentCnt ) 
			|| ODBC_RETURN_SUCCESS != pFSODBC->GetIsHeight( iContrastVar_SpecialCharacterIndex, iContrastVar_IsHeight,iContrastVar_SpecialTeamIndex ) )
			 
		{			
			if(res) *res = SEND_ITEM_ERROR_NOT_EXIST_USER;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;				
		}
	}
	else
	{
		iContrastVar_Sex = pAvatar->iSex;
		iContrastVar_Lv = pAvatar->iLv;
		iContrastVar_SpecialCharacterIndex = pAvatar->iSpecialCharacterIndex;
		iContrastVar_SpecialTeamIndex = pAvatar->iAvatarTeamIndex;
		iContrastVar_Position = m_pUser->GetCurUsedAvatarPosition();
		iContrastVar_CharacterType = pAvatar->iCharacterType;
	}

	// 비교 부분
	if( bIsSendItem == TRUE )
	{	
		if( ( iContrastVar_Sex != ShopItemInfo.iSexCondition) && ( ITEM_INFO_SEX_COMMON != ShopItemInfo.iSexCondition ) )
		{
			int iTargetIndex = 0;
			if( FALSE == AVATARCREATEMANAGER.CheckCloneAvailableCharacter(iContrastVar_SpecialCharacterIndex, iTargetIndex))
			{
				if(res) *res = SEND_ITEM_ERROR_MISMATCH_SEX;
				iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
				return FALSE;
			}
		}

		if( false == ShopItemInfo.IsLvCondition(iContrastVar_Lv) && CHARACTER_SLOT_TYPE_NORMAL <= ShopItemInfo.iCharacterSlotType )
		{
			if(res) *res = SEND_ITEM_ERROR_MISMATCH_LEVEL;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;				
		}

		if( iContrastVariable_PresentCnt >= MAX_PRESENT_LIST ) 
		{			
			if(res) *res = SEND_ITEM_ERROR_PRESENTBOX_FULL;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;				
		}

		if( !ShopItemInfo.IsPositionCondition(iContrastVar_Position) )
		{
			if(res) *res = SEND_ITEM_ERROR_MISMATCH_POSITION;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}

		// TODO - 신규캐릭터 특정 아이템 못사게 막음. 아트작업 완료시 코드 삭제 
		if(iContrastVar_CharacterType == NEW_FEMALE_CHARACTER_TYPE &&
			ShopItemInfo.bIsNotNewCharBuyItem == TRUE)
		{
			if(res) *res = SEND_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}

		if( 403 <= iContrastVar_SpecialCharacterIndex && 406 >= iContrastVar_SpecialCharacterIndex 
			&& TRUE == ShopItemInfo.bIsNotNewCharBuyItem )
		{
			if(res) *res = SEND_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}

		if((iContrastVar_SpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX1 ||
			iContrastVar_SpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX3) &&
			(ShopItemInfo.iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
		{
			if(res) *res = SEND_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}

		if( ITEM_PROPERTY_KIND_CLONE_CHARACTER_ITEM == ShopItemInfo.iPropertyKind &&
			(THREEKINGDOM_CHARACTER_SPECIALINDEX4 > iContrastVar_SpecialCharacterIndex ||
			THREEKINGDOM_CHARACTER_SPECIALINDEX7 < iContrastVar_SpecialCharacterIndex) )
		{
			if(res) *res = SEND_ITEM_ERROR_NO_CLONE_CHARACTER;
			iErrorCode = SEND_ITEM_ERROR_NO_CLONE_CHARACTER;
			return FALSE;
		}
	}
	else
	{
		if( ( iContrastVar_Sex != ShopItemInfo.iSexCondition) && ( ITEM_INFO_SEX_COMMON != ShopItemInfo.iSexCondition ) )
		{
			iErrorCode = BUY_ITEM_ERROR_MISMATCH_SEX;
			return FALSE;
		}

		if (false == ShopItemInfo.IsLvCondition(iContrastVar_Lv))
		{
			iErrorCode = BUY_ITEM_ERROR_MISMATCH_LEVEL;
			return FALSE;
		}

		if( iContrastVariable_ChampionCnt < ShopItemInfo.iChampionCondition )
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CHAMPIONCOUNT;
			return FALSE;
		}

		if( !ShopItemInfo.IsPositionCondition(iContrastVar_Position) )
		{
			iErrorCode = BUY_ITEM_ERROR_MISMATCH_POSITION;
			return FALSE;
		}

		// TODO - 신규캐릭터 특정 아이템 못사게 막음. 아트작업 완료시 코드 삭제 
		if(iContrastVar_CharacterType == NEW_FEMALE_CHARACTER_TYPE &&
			ShopItemInfo.bIsNotNewCharBuyItem == TRUE)
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
			return FALSE;
		}

		if( 403 <= iContrastVar_SpecialCharacterIndex && 406 >= iContrastVar_SpecialCharacterIndex
			&& TRUE == ShopItemInfo.bIsNotNewCharBuyItem )
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
			return FALSE;
		}

		if((iContrastVar_SpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX1 ||
			iContrastVar_SpecialCharacterIndex == THREEKINGDOM_CHARACTER_SPECIALINDEX3) &&
			(ShopItemInfo.iChannel & ITEMCHANNEL_FACE_ACC2) > 0)
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
			return FALSE;
		}
	}

	// 2011.03.02 jhwoo
	int iErrorType = 0;
	if( FALSE == CheckSpecialAvatarIndexCondition(iContrastVar_SpecialCharacterIndex, ShopItemInfo.iSpecialIndexCondition) )	// 2011.02.28 jhwoo
	{
		if( iContrastVar_SpecialCharacterIndex == -1 )
		{
			if( bIsSendItem == FALSE )
			{
				iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_ITEM;
			}
			else
			{
				iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
				if(res) *res = SEND_ITEM_ERROR_SPECIALCHARACTER_ITEM;
			}
		}
		else if( iContrastVar_SpecialCharacterIndex != ShopItemInfo.iSpecialIndexCondition )
		{
			if( bIsSendItem == FALSE )
			{
				iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
			}
			else
			{
				if(res) *res = SEND_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
				iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			}			
		}
		return FALSE;
	}

	if( FALSE == CheckSpecialTeamIndexCondition(iContrastVar_SpecialTeamIndex, ShopItemInfo.iTeamIndexCondition, ShopItemInfo.iUnableTeamIndexCondition) )	// 2011.02.28 jhwoo
	{
		if( bIsSendItem == FALSE )
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIAL_TEAM_CONDITION;
		}
		else
		{
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			if(res) *res = SEND_ITEM_ERROR_SPECIAL_TEAM_CONDITION;
		}
		return FALSE;
	}

	if( FALSE == CheckItemBuyConditionWithPropertyKind( pFSODBC, ShopItemInfo, pAvatar, pAvatarItemList, bIsSendItem, iContrastVar_SpecialCharacterIndex, iContrastVar_IsHeight, res, iErrorCode, iPropertyValue ) )
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUserItem::CheckItemBuyConditionWithPropertyKind( CFSGameODBC *pFSODBC, SShopItemInfo &ShopItemInfo, SAvatarInfo* pAvatar, CAvatarItemList *pAvatarItemList,
															BOOL bIsSendItem, int iContrastVar_SpecialCharacterIndex, int iContrastVar_IsHeight, int* res, int &iErrorCode, int iPropertyValue )
{	
	if(bIsSendItem != TRUE)
	{
		// 같이 구매 불가능한 아이템 체크
		if(ShopItemInfo.iNoDuplicationItemCode > 0)
		{
			SUserItemInfo* pCheckItem = pAvatarItemList->GetItemWithItemCodeInAll(ShopItemInfo.iNoDuplicationItemCode);
			if(pCheckItem != NULL)
			{
				iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
				if(pCheckItem->iPropertyKind == PREMIUM_ITEM_KIND_POWERUP || pCheckItem->iPropertyKind == PREMIUM_ITEM_KIND_POWERUP_PLUS)
				{
					iErrorCode = BUY_ITEM_ERROR_POWERUP_ITEM_HAVE_ALREADY;
				}

				return FALSE;	
			}
		}

		// 중복 구매 불가능 아이템 체크
		if(ShopItemInfo.iDuplicationPurchase == 0)
		{
			if(ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MIN <= ShopItemInfo.iPropertyKind &&
				ITEM_PROPERTY_KIND_HIGH_FREQUENCY_ITEM_MAX >= ShopItemInfo.iPropertyKind)
			{
				SUserHighFrequencyItem* pCheckItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ShopItemInfo.iPropertyKind);
				if(NULL != pCheckItem && 
					pCheckItem->iPropertyTypeValue > 0)
				{
					iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
					return FALSE;	
				}
			}
			else
			{
				SUserItemInfo* pCheckItem = pAvatarItemList->GetInventoryItemWithPropertyKind(ShopItemInfo.iPropertyKind);
				if(pCheckItem != NULL)
				{
					iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
					if(pCheckItem->iPropertyKind == PREMIUM_ITEM_KIND_POWERUP || pCheckItem->iPropertyKind == PREMIUM_ITEM_KIND_POWERUP_PLUS)
					{
						iErrorCode = BUY_ITEM_ERROR_POWERUP_ITEM_HAVE_ALREADY;
					}
					return FALSE;	
				}
			}
		}

		// 다른 종류의 VIP 아이템 구매라면 실패 
		if(ShopItemInfo.iPropertyKind > BIND_ACCOUNT_ITEM_KIND_START && ShopItemInfo.iPropertyKind < BIND_ACCOUNT_ITEM_KIND_END)
		{
			if( FALSE == CheckHaveBindAccountItem(ShopItemInfo.iPropertyKind, iErrorCode))
			{
				TRACE("CheckHaveBindAccountItem Result False iErrorCode = 11866902\n", iErrorCode);
rrorCode = BUY_ITEM_ERROR_VIP_ITEM_HAVE_ALREADY;
				return FALSE;
			}

		}

		// 스킬 슬롯 아이템
		if(ShopItemInfo.iPropertyKind == SKILL_SLOT_KIND_NUM/* && bIsSendItem == FALSE*/)
		{
			int nMaxSkillSlot = MAX_SKILL_SLOT_OTHERS;

			if( m_pUser->GetSkillSlotCount( pAvatar->Skill.SkillSlot, ShopItemInfo.iItemCode0 ) > nMaxSkillSlot )	// 최대로 가질 수 있는 스킬슬롯의 수를 넘어 섰다면
			{	
				if(-1 != iPropertyValue ||
					(-1 == iPropertyValue && FALSE == m_pUser->CheckSkillSlotExchangeStatus(pAvatar->Skill.SkillSlot, ShopItemInfo.iItemCode0)))
				{
					iErrorCode = BUY_ITEM_ERROR_USER_SKILLSLOT_OVER;
					return FALSE;
				}
			}
		}

		// 개인 치어리더
		if(ShopItemInfo.iPropertyKind == ITEM_PROPERTY_KIND_CHEERLEADER)
		{
			if(ShopItemInfo.iDuplicationPurchase == 0)
			{
				if(TRUE == m_pUser->GetUserCheerLeader()->CheckCheerLeader(ShopItemInfo.iCheerLeaderIndex))
				{
					iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
					return FALSE;
				}
				
				// 경리
				if(ShopItemInfo.iCheerLeaderIndex == 3022 ||
					ShopItemInfo.iCheerLeaderIndex == 3023)
				{
					int iCheckCheerLeaderIndex = 3022;
					if(ShopItemInfo.iCheerLeaderIndex == 3022)
						iCheckCheerLeaderIndex = 3023;

					if(TRUE == m_pUser->GetUserCheerLeader()->CheckCheerLeader(iCheckCheerLeaderIndex))
					{
						iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
						return FALSE;
					}
				}
			}

			int iAddTrophyCount = CLUBCONFIGMANAGER.GetCheerLeaderProbabillityByAbillity(ShopItemInfo.iCheerLeaderIndex, ShopItemInfo.btCheerLeaderTypeNum, CHEERLEADR_ABILLITY_BUY_GIVE_TROPHY);
			if(iAddTrophyCount > 0)
			{
				if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST)
				{
					iErrorCode = BUY_ITEM_ERROR_PRESENT_LIST_FULL;
					return FALSE;
				}
			}
		}
	}
	else
	{
		// 선물 불가능 아이템 체크
		if(!(ShopItemInfo.iShopButtonType & 2))
		{
			if(res) *res = SEND_ITEM_ERROR_IMPOSSIBLE_SENDING;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}
	}

	// 키 변경 아이템	
	if ( bIsSendItem )
	{
		if( ShopItemInfo.iPropertyKind == CHANGE_HEIGHT_KIND_NUM && iContrastVar_IsHeight == 0)//선물이고, 받는 캐릭터가 키변경이 되지 않을때
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_IMPOSSIBLE;
			return FALSE;
		}
	} 
	else
	{
		if( ShopItemInfo.iPropertyKind == CHANGE_HEIGHT_KIND_NUM && pAvatar->iEnableChangeHeight == 0 && iContrastVar_SpecialCharacterIndex != -1 )//
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_IMPOSSIBLE;
			return FALSE;
		}
	}
	

	//// 스페셜캐릭터 전용 아이템
	//if( ShopItemInfo.iPropertyKind >= MIN_SPECIAL_ITEM_KIND_NUM && ShopItemInfo.iPropertyKind <= MAX_SPECIAL_ITEM_KIND_NUM )
	//{
	//	if( iContrastVar_SpecialCharacterIndex == -1 )
	//	{
	//		if( bIsSendItem == FALSE )
	//		{
	//			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_ITEM;
	//		}
	//		else
	//		{
	//			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
	//			if(res) *res = SEND_ITEM_ERROR_SPECIALCHARACTER_ITEM;
	//		}

	//		return FALSE;

	//	}
	//	else if( iContrastVar_SpecialCharacterIndex != ShopItemInfo.iSpecialIndexCondition )
	//	{
	//		if( bIsSendItem == FALSE )
	//		{
	//			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
	//		}
	//		else
	//		{
	//			if(res) *res = SEND_ITEM_ERROR_SPECIALCHARACTER_INDEX_MISMATCH;
	//			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
	//		}
	//		return FALSE;
	//	}
	//}

	// 레벨업 아이템
	//if( ( ShopItemInfo.iPropertyKind >= MIN_EXP_UP_ITEM_KIND_NUM  && ShopItemInfo.iPropertyKind <= MAX_EXP_UP_ITEM_KIND_NUM  ) ||
	//	ShopItemInfo.iPropertyKind >= MIN_LEVEL_UP_ITEM_KIND_NUM  && ShopItemInfo.iPropertyKind <= MAX_LEVEL_UP_ITEM_KIND_NUM )
	//{
	//	if( bIsSendItem == TRUE )
	//	{
	//		if(res) *res = SEND_ITEM_ERROR_IMPOSSIBLE_SENDING;
	//		iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
	//		return FALSE;
	//	}
	//}

	// 얼굴 변경 아이템
	if( ShopItemInfo.iPropertyKind == FACE_OFF_KIND_NUM )
	{	
		if( bIsSendItem == TRUE )
		{
			if(res) *res = SEND_ITEM_ERROR_IMPOSSIBLE_SENDING;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}

		if( (ShopItemInfo.iItemCode0 <= BASE_FACE_OFF_NUM || ShopItemInfo.iItemCode0 > BASE_FACE_OFF_NUM + FACE_OFF_ITEM_MAX_NUM ) &&
			(  ShopItemInfo.iItemCode0 != BASE_FACE_OFF_NUM_7 ) && ( ShopItemInfo.iItemCode0 != BASE_FACE_OFF_NUM_8 ) ) 
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
			return FALSE;
		}

		int iFaceNumber = 0;
		switch(ShopItemInfo.iItemCode0)
		{
			case BASE_FACE_OFF_NUM_7:	iFaceNumber = 8;					break;
			case BASE_FACE_OFF_NUM_8:	iFaceNumber = 0;					break;
			default:	iFaceNumber = ShopItemInfo.iItemCode0 - BASE_FACE_OFF_NUM;		break;
		}	

		if( pAvatar->iFace == iFaceNumber )
		{
			iErrorCode = BUY_ITEM_ERROR_OVERLAP_FACEOFF;
			return FALSE;
		}

		if( pAvatar->iSpecialCharacterIndex != -1 )
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_IMPOSSIBLE;
			return FALSE;
		}
	}
	// 체형변경 아이템
	if( ShopItemInfo.iPropertyKind == CHANGE_BODYSHAPE_KIND_NUM )
	{
		if( bIsSendItem == TRUE )
		{
			if(res) *res = SEND_ITEM_ERROR_IMPOSSIBLE_SENDING;
			iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
			return FALSE;
		}

		int iBodyShape = ((ShopItemInfo.iItemCode0 / 10) + 4) ;
0;

		if( iBodyShape < 1 || CHANGE_BODYSHAPE_ITEM_MAX_NUM < iBodyShape )
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
			return FALSE;
		}

		if( pAvatar->iCharacterType == iBodyShape )
		{
			iErrorCode = BUY_ITEM_ERROR_OVERLAP_CHARACTERTYPE;
			return FALSE;
		}

		if( pAvatar->iSpecialCharacterIndex != -1 )
		{
			iErrorCode = BUY_ITEM_ERROR_SPECIALCHARACTER_IMPOSSIBLE;
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CFSGameUserItem::SetItemPriceBySellType(CFSItemShop *pItemShop, SShopItemInfo &ShopItemInfo,BOOL bIsLocalPublisher, int iPropertyValue, int &iBuyPriceCash, int &iBuyPricePoint, int &iBuyPriceTrophy, int& iBuyPriceClubCoin, int &iErrorCode )
{
	CItemPriceList *pItemPriceList = pItemShop->GetItemPriceList( ShopItemInfo.iItemCode0 );
	if( pItemPriceList == NULL )
	{		
		iErrorCode = BUY_ITEM_ERROR_GENERAL;
		return FALSE;
	}

	SItemPrice* pItemPrice = NULL;
	pItemPrice = pItemPriceList->GetItemPrice( ShopItemInfo.iSellType, iPropertyValue ,bIsLocalPublisher);

	if( pItemPrice == NULL )
	{		
		iErrorCode = BUY_ITEM_ERROR_GENERAL;
		return FALSE;
	}

	time_t CurrentTime = _time64(NULL);

	if( pItemPrice->GetPrice(CurrentTime) == -1 )
	{
		iErrorCode = BUY_ITEM_ERROR_GENERAL;
		return FALSE;
	}

	int iPrice;

	if(TRUE == m_pUser->GetAbleSaleItem())
	{
		int iDiscountRate = 0;
		if (TRUE == ShopItemInfo.bIsDiscountTarget)
		{
			iDiscountRate = m_pUser->GetDiscountRate(ShopItemInfo.iSellType, ShopItemInfo.iCategory);
		}
		iPrice = pItemPrice->GetPrice(CurrentTime, iDiscountRate);
	}
	else
	{
		iPrice = pItemPrice->GetOriginalPrice();
	}

	if( BUY_ITEM_SELL_TYPE_CASH == ShopItemInfo.iSellType )
	{
		iBuyPriceCash = iPrice;
	}
	else if( BUY_ITEM_SELL_TYPE_POINT == ShopItemInfo.iSellType )
	{
		iBuyPricePoint = iPrice;
	}
	else if( BUY_ITEM_SELL_TYPE_TROPHY == ShopItemInfo.iSellType )	// 20090610 Add Tournament Trophy System
	{
		iBuyPriceTrophy = iPrice;
	}
	else if( BUY_ITEM_SELL_TYPE_CLUBCOIN == ShopItemInfo.iSellType )
	{
		iBuyPriceClubCoin = iPrice;
	}

	return TRUE;
}

// 한개의 아이템 가격을 구한다.
BOOL CFSGameUserItem::SetOneUnitItemPrice( CFSGameODBC* pGameODBC, CFSItemShop *pItemShop,BOOL bIsLocalPublisher , SShopItemInfo &ShopItemInfo, int iPropertyValue,int iItemPriceCash,int iItemPricePoint, int &iBuyOnePriceCash, int &iBuyOnePricePoint, int &iErrorCode )
{
	CItemPriceList *pItemPriceList = pItemShop->GetItemPriceList( ShopItemInfo.iItemCode0 );
	if( pItemPriceList == NULL )
	{		
		iErrorCode = BUY_ITEM_ERROR_GENERAL;
		return FALSE;
	}

	SItemPrice* pItemPrice = pItemPriceList->GetItemPrice( ShopItemInfo.iSellType, iPropertyValue ,bIsLocalPublisher);
	if( pItemPrice == NULL )
	{		
		iErrorCode = BUY_ITEM_ERROR_GENERAL;
		return FALSE;
	}

	time_t CurrentTime = _time64(NULL);

	if( pItemPrice->GetPrice(CurrentTime) == -1 )
	{
		iErrorCode = BUY_ITEM_ERROR_GENERAL;
		return FALSE;
	}

	// 20080610 Change TaToo Item SellPrice
	if( ShopItemInfo.iChannel == ITEMCHANNEL_TATTOO && ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST )
	// 문신 아이템이라면 SellPrice를 10개 가격으로 넣는다
	{
		SItemPrice* p10UnitItemPrice = pItemPriceList->GetItemPrice(ShopItemInfo.iSellType, ITEM_PROPERTYVALUE_10UNIT,bIsLocalPublisher );
		if( p10UnitItemPrice == NULL )
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
			return FALSE;
		}

		int iPrice;

		if(TRUE == m_pUser->GetAbleSaleItem())
		{
			int iDiscountRate = 0;
			if (TRUE == ShopItemInfo.bIsDiscountTarget)
			{
				iDiscountRate = m_pUser->GetDiscountRate(ShopItemInfo.iSellType, ShopItemInfo.iCategory);
			}
			iPrice = p10UnitItemPrice->GetPrice(CurrentTime, iDiscountRate);
		}
		else
		{
			iPrice = p10UnitItemPrice->GetOriginalPrice();
		}

		if( SELL_TYPE_CASH == ShopItemInfo.iSellType )
		{
			iBuyOnePriceCash = iPrice;
			iBuyOnePriceCash /= 10;	// 개당 가격으로 변경
		}
		else if( SELL_TYPE_POINT == ShopItemInfo.iSellType )
		{
			iBuyOnePricePoint = iPrice; //문신류는 포인트 구매가 없으므로 
			iBuyOnePricePoint /= 10;
		}
	}
	else if( ShopItemInfo.iPropertyKind == PROTECT_RING_ITEM_KIND_NUM && ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST)	// 호신 반지
	{
			iBuyOnePriceCash = 0;
			iBuyOnePricePoint = 0; 
	}
	else if( ShopItemInfo.iChannel == ITEMCHANNEL_SHOUT && ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST )
		// 외치기 아이템 이라면 SellPrice를 1개 가격으로 넣는다
	{
		if( SELL_TYPE_CASH == ShopItemInfo.iSellType )
		{
			iBuyOnePriceCash = iItemPriceCash / iPropertyValue;
		}
		else if( SELL_TYPE_POINT == ShopItemInfo.iSellType )
		{
			iBuyOnePricePoint = iItemPricePoint/ iPropertyValue;
		}
	}
	else
	{
		iBuyOnePriceCash = iItemPriceCash;
		iBuyOnePricePoint = iItemPricePoint;
	}

	return TRUE;
}

BOOL CFSGameUserItem::SetPropertyPriceAndType( CFSItemShop *pItemShop, SShopItemInfo &ShopItemInfo, int iPropertyValue, int iPropertyAssignType, int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT], int iRecvUserLevel,
											  int &iBuyPriceCash, int &iBuyPricePoint, int &iItemPropertyPriceCash, int &iItemPropertyPricePoint, int &iErrorCode )
{
	// 능력치 아이템일경우 해당 능력치의 가격을 가져온다.
	if(iaPropertyIndex[ITEM_PROPERTYINDEX_0] != -1 &&
		((ShopItemInfo.iPropertyKind >= MIN_STAT_PROPERTY_NUM && ShopItemInfo.iPropertyKind <= MAX_STAT_PROPERTY_NUM) ||
		(ShopItemInfo.iPropertyKind > MIN_SPECIAL_ITEM_KIND_NUM && ShopItemInfo.iPropertyKind <= MAX_SPECIAL_ITEM_KIND_NUM) ||
		(ShopItemInfo.iPropertyKind >= MIN_SECOND_BOOST_ITEM_KIND_NUM && ShopItemInfo.iPropertyKind <= MAX_SECOND_BOOST_ITEM_KIND_NUM) ||
		(ShopItemInfo.iPropertyKind >= MIN_ONE_GAME_BOOST_ITEM_KIND_NUM && ShopItemInfo.iPropertyKind <= MAX_ONE_GAME_BOOST_ITEM_KIND_NUM) ||
		(ShopItemInfo.iPropertyKind >= MIN_POSITION_POWER_UP_ITEM_KIND_NUM && ShopItemInfo.iPropertyKind <= MAX_POSITION_POWER_UP_ITEM_KIND_NUM) ||
		(ShopItemInfo.iPropertyKind >= MIN_AURA_ITEM_KIND_NUM && ShopItemInfo.iPropertyKind <= MAX_AURA_ITEM_KIND_NUM) ||
		(ShopItemInfo.iPropertyKind > PREMIUM_ITEM_KIND_START && ShopItemInfo.iPropertyKind < PREMIUM_ITEM_KIND_END) || 
		(ShopItemInfo.iPropertyKind == 6201/*Acc_2*/) ||
		(ShopItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_EVENT_ACC_BUBBLE_MIN && ShopItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_EVENT_ACC_BUBBLE_MAX))
		)
	{
		CItemPropertyBoxList* pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList( ShopItemInfo.iPropertyKind );
		if( pItemPropertyBoxList == NULL )
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
			return FALSE;
		}

		CItemPropertyPriceList* pItemPropertyPriceList = pItemShop->GetItemPropertyPriceList( ShopItemInfo.iPropertyKind );	
		if( pItemPropertyPriceList == NULL )
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
			return FALSE;
		}

		SItemPropertyPrice* pItemPropertyPrice = pItemPropertyPriceList->GetItemPropertyPrice( iPropertyValue, iRecvUserLevel, iPropertyAssignType );	
		if( pItemPropertyPrice == NULL )
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
			return FALSE;
		}

		int iLastAssignChannel = -999;
		for (int i = 0; i < MAX_ITEM_PROPERTYINDEX_COUNT; ++i)
		{
			if (-1 == iaPropertyIndex[i])	break;

			SItemPropertyBox* pItemPropertyBox = pItemPropertyBoxList->GetItemPropertyBoxWithIndex( iPropertyAssignType, iaPropertyIndex[i] ,iRecvUserLevel );
			if( pItemPropertyBox == NULL )
			{
				iErrorCode = BUY_ITEM_ERROR_GENERAL;
				return FALSE;
			}

			if (iLastAssignChannel != pItemPropertyBox->iAssignChannel)
			{
				if( pItemPropertyBox->iPriceType == PRICE_TYPE_CASH )
				{			
					iBuyPriceCash += pItemPropertyPrice->iPriceCash;
					iItemPropertyPriceCash += pItemPropertyPrice->iPriceCash;	
				}
				else if( pItemPropertyBox->iPriceType == PRICE_TYPE_POINT ) 
				{
					iBuyPricePoint += pItemPropertyPrice->iPricePoint;
					iItemPropertyPricePoint += pItemPropertyPrice->iPricePoint;	
				}

				iLastAssignChannel = pItemPropertyBox->iAssignChannel;
			}
		}
	}

	return TRUE;
}
// 20090520 CHN Challenge letter function task - check have.
BOOL CFSGameUserItem::CheckChallengeLetterItem( )
{
	CAvatarItemList* pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL( pAvatarItemList )

		SUserItemInfo* pItemInfo = pAvatarItemList->GetItemWithChannelAndPropertyKind( NO_EQUIP_ITEM_CHANNEL, CHALLENGE_ITEM_KIND_NUM );

	if( pItemInfo != NULL )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

// 20090520 CHN Challenge letter function task - use one and discount the item count.
void CFSGameUserItem::ExhaustChallengeLetter( CFSGameODBC *pODBC )
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID( pAvatar )

		CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID( pAvatarItemList )

		SUserItemInfo* pItemInfo = pAvatarItemList->GetItemWithChannelAndPropertyKind( NO_EQUIP_ITEM_CHANNEL, CHALLENGE_ITEM_KIND_NUM );

	if( pItemInfo != NULL )
	{
		if( pItemInfo->iPropertyTypeValue <= 1 )
		{			
			int iEmptySlot = -1;			// Not used in stored procedure.

			if( ODBC_RETURN_SUCCESS != pODBC->spUpdateNoEquipItemExhaustNew( pAvatar->iGameIDIndex, pItemInfo->iChannel, iEmptySlot , pItemInfo->iItemIdx , pItemInfo->iItemCode ))
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spUpdateNoEquipItemExhaustNew - GameID:��4", pAvatar->szGameID);
			return;
			}

			pItemInfo->iPropertyTypeValue--;

			pAvatarItemList->RemoveItem(pItemInfo->iItemIdx);		// status = 0 means consume all.
		}
		else
		{
			if( ODBC_RETURN_SUCCESS == pODBC->spUpdateNoEquipItemCount( pAvatar->iGameIDIndex , pItemInfo->iItemIdx , pItemInfo->iItemCode ) )
			{
				pItemInfo->iPropertyTypeValue--;
			}
		}

	}
	return;
}

BOOL CFSGameUserItem::BuySkillSlot_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuySkillSlot_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_SKILL_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;	
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPropertyValue = pBillingInfo->iParam0;
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;
	char* szProductCode = pBillingInfo->szProductCode;

	// 선물에 쓰이는 변수들
	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;
	if( bIsSendItem == TRUE )
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
	}
	else
	{
		iPacketOperationCode = FS_ITEM_OP_BUY;
	}

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			SExtraSkillSlot SkillSlot;
			int iSlotIdx = 0;			
			int iPresentIdx = -1;

			if( bIsSendItem == TRUE )
			{
				int iRetError = -1;
				iRetError = pGameODBC->spFSBuyAndSendSkillSlot( szUserID, iUserIDIndex, iGameIDIndex, szGameID, szRecvGameID, iPresentIdx, szTitle, 
					szText, iItemIndex,	iPropertyValue, iPrevMoney, iPostMoney,	szBillingKey ,szProductCode, pBillingInfo->iItemPrice );
				if( ODBC_RETURN_SUCCESS == iRetError )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS_SKILLSLOT;
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
					CFSGameServer* pServer = CFSGameServer::GetInstance();
					if( pServer != NULL )
					{
						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
						if( pCenter != NULL ) 
						{
							pCenter->SendUserNotify( szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR );				
						}
					}
				}
				else
				{
					if ( iRetError == 2 )
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
					}
					else
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
					}
					
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSBuyAndSendSkillSlot - ItemCode:%d, UserID:%s, GameID:%s, RecvGameID:%s", iItemIndex, szUserID, szGameID, szRecvGameID );
				}

			}
			else
			{
				BOOL bExchange = FALSE;
				SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
				if(pAvatar != NULL)
				{
					int nMaxSkillSlot = MAX_SKILL_SLOT_OTHERS;
					if(m_pUser->GetSkillSlotCount( pAvatar->Skill.SkillSlot, iItemIndex) >= nMaxSkillSlot)
					{
						if(-1 == iPropertyValue &&
							TRUE == pUser->CheckSkillSlotExchangeStatus(pAvatar->Skill.SkillSlot, iItemIndex))
						{
							bExchange = TRUE;
						}
					}
				}

				if( ODBC_RETURN_SUCCESS == pGameODBC->spBuySkillSlot( szUserID,	iUserIDIndex, iGameIDIndex, SkillSlot, iItemIndex, iPropertyValue, iSlotIdx, iPrevMoney, 
					iPostMoney, iItemPricePoint, iBuyPriceType, szBillingKey, szProductCode, pBillingInfo->iItemPrice, bExchange ) )
				{			
					iErrorCode = 1;
					SAvatarInfo	*pAvatar = pUser->GetCurUsedAvatar();
					if( pAvatar != NULL )
					{
						GetAdditionalSkillSlot( pAvatar, SkillSlot, iItemIndex, iPropertyValue, iSlotIdx );	
						pUser->GetUserShoppingFestivalEvent()->RemoveShoppingBasketItem(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_ITEM, 0, iItemIndex);
					}	
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spBuySkillSlot - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID );
				}
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

			SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );

		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();
	}

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );
}

void CFSGameUserItem::SetUserBillResultAtMem( CFSGameUser* pUser, CFSGameODBC* pGameODBC, int iBuyPriceType, int iPostMoney, int iItemPricePoint, int iItemPriceTrophy, int iBonusCash )
{
	CHECK_NULL_POINTER_VOID( pUser );

	int iUseCoin = 0;
	int	iUsePoint = 0;

	if( SELL_TYPE_CASH == iBuyPriceType )
	{
		iUseCoin = pUser->GetCoin() - iPostMoney;

		pUser->SetCoin( iPostMoney );
		pUser->SetBonusCoin( iBonusCash );
	}
	else if( SELL_TYPE_POINT == iBuyPriceType )
	{
		iUsePoint = iItemPricePoint;

		pUser->SetSkillPoint( pUser->GetSkillPoint() - iItemPricePoint );			
	}	
	else if( SELL_TYPE_CASH_AND_POINT == iBuyPriceType )
	{		
		iUseCoin = pUser->GetCoin() - iPostMoney;
		iUsePoint = iItemPricePoint;

		pUser->SetCoin( iPostMoney );
		pUser->SetBonusCoin( iBonusCash );
		pUser->SetSkillPoint( pUser->GetSkillPoint() - iItemPricePoint );
	}
	else if( SELL_TYPE_TROPHY == iBuyPriceType )
	{
		pUser->SetUserTrophy( pUser->GetUserTrophy() - iItemPriceTrophy );
	}

	int iTempSkillPoint = 0;
	if( ODBC_RETURN_SUCCESS == pGameODBC->spUserGetUserPointInfo( pUser->GetUserIDIndex(), iTempSkillPoint ))
	{	
		pUser->SetSkillPoint( iTempSkillPoint );
		pUser->AddPointAchievements(); 
	}

	int iTempTrophyCount = 0;
	if( ODBC_RETURN_SUCCESS == pGameODBC->spUserGetUserTrophyInfo( pUser->GetUserIDIndex(), iTempTrophyCount ))
	{	
		pUser->SetUserTrophy( iTempTrophyCount );
	}

	if(SELL_TYPE_CASH == iBuyPriceType ||
		SELL_TYPE_POINT == iBuyPriceType ||
		SELL_TYPE_CASH_AND_POINT == iBuyPriceType)
	{
		if(iUsePoint > 0)
		{
			pUser->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_POINT_USE, iUsePoint);
			pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_POINT, iUsePoint);
			pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_POINT, iUsePoint);
			pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POINT_ITEM);
			pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_USEPOINT, iUsePoint);	
			pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_POINT, iUsePoint);
			pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USE_POINT, iUsePoint);
		}

		if(iUseCoin > 0)
		{
			pUser->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_USE_CASH, iUseCoin);
			pUser->GetUserPotionMakingEvent()->CheckMission(SEVENT_POTION_MAKING_MISSION_TYPE_USE_CASH, iUseCoin);	
			pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_CASH);
		}
	}

	return;
}

void CFSGameUserItem::GetAdditionalSkillSlot( SAvatarInfo *pAvatar, SExtraSkillSlot &SkillSlot, int iItemIndex, int iPropertyValue, int iSlotIdx )
{
	CHECK_NULL_POINTER_VOID( pAvatar )

	pAvatar->Skill.SkillSlot[iSlotIdx].bUse = true;
	pAvatar->Skill.SkillSlot[iSlotIdx].iItemCode = iItemIndex;
	pAvatar->Skill.SkillSlot[iSlotIdx].sSlotCount = SkillSlot.sSlotCount;	
	pAvatar->Skill.SkillSlot[iSlotIdx].EndDate.iYear = SkillSlot.EndDate.iYear;
	pAvatar->Skill.SkillSlot[iSlotIdx].EndDate.iMonth = SkillSlot.EndDate.iMonth;
	pAvatar->Skill.SkillSlot[iSlotIdx].EndDate.iDay = SkillSlot.EndDate.iDay;
	pAvatar->Skill.SkillSlot[iSlotIdx].EndDate.iHour = SkillSlot.EndDate.iHour;
	pAvatar->Skill.SkillSlot[iSlotIdx].EndDate.iMin = SkillSlot.EndDate.iMin;
	pAvatar->Skill.SkillSlot[iSlotIdx].iUseTerm = iPropertyValue;

	return;
}

BOOL CFSGameUserItem::BuyFaceOff_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyFaceOff_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iFaceNumber = pBillingInfo->iParam0;
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szProductCode = pBillingInfo->szProductCode;

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			int iTempResult = 0;
			int iTempCash = 0;

			SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx(pUser->GetCurAvatarIdx());
			if( NULL == pAvatar )
			{
				return FALSE;
			}

			if( ODBC_RETURN_SUCCESS == pGameODBC->spBuyChangeFace( szUserID, iUserIDIndex, iGameIDIndex, iItemIndex, iFaceNumber, iBuyPriceType, iItemPricePoint,
				iPrevMoney, iPostMoney, iTempCash, iTempResult, szBillingKey, szProductCode, pBillingInfo->iItemPrice ) )			
			{			
				pUser->GetUserItemList()->SetTempFace( iFaceNumber );
				pAvatar->iFace = iFaceNumber;
				iErrorCode = BUY_ITEM_ERROR_SUCCESS;
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spBuyChangeFace - ItemCode:%d, UserID:%s", iItemIndex, szUserID );
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

			SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
		}
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();
		pUser->SendAvatarInfo();		
		pUser->SendAllMyOwnItemInfo();
	}	

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );
}

BOOL CFSGameUserItem::BuyChangeBodyShape_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyFaceOff_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iChangeBodyShape_CharacterType = pBillingInfo->iParam0;
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szProductCode = pBillingInfo->szProductCode;

	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			int iTempResult = 0;
			int iTempCash = 0;

			SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx(pUser->GetCurAvatarIdx());
			if( NULL == pAvatar )
			{
				return FALSE;
			}

			if( ODBC_RETURN_SUCCESS == pGameODBC->spBuyChangeBodyShape( szUserID, iUserIDIndex, iGameIDIndex, iItemIndex, iChangeBodyShape_CharacterType, iBuyPriceType, iItemPricePoint,
				iPrevMoney, iPostMoney, iTempCash, iTempResult, szBillingKey, szProductCode, pBillingInfo->iItemPrice ) )			
			{			
				pUser->GetUserItemList()->SetTempCharacterType( iChangeBodyShape_CharacterType );
				pAvatar->iCharacterType = iChangeBodyShape_CharacterType;
				iErrorCode = BUY_ITEM_ERROR_SUCCESS;
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spBuyChangeBodyShape - ItemCode:%d, UserID:%s", iItemIndex, szUserID );
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

			SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
		}
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();
		pUser->SendAvatarInfo();		
		pUser->SendAllMyOwnItemInfo();
	}	

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode ) ? TRUE : FALSE;
}

BOOL CFSGameUserItem::BuyCharacterSlot_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyCharacterSlot_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_SKILL_ERROR_GENERAL;
	int iSlotBonusPoint = 0;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iCharacterSlotType = pBillingInfo->iParam0;				// 0: 일반, 1: 엑스트라 캐릭터
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;
	char* szProductCode = pBillingInfo->szProductCode;

	// 선물에 쓰이는 변수들
	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;
	if( bIsSendItem == TRUE )
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
	}
	else
	{
		iPacketOperationCode = FS_ITEM_OP_BUY;
	}

	if(TRUE == pBillingInfo->bSaleRandomItemEvent)
	{
		iPacketOperationCode = FS_ITEM_OP_BUY_SALE_RANDOMITEM;
	}

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			if( bIsSendItem == TRUE )
			{
				int iPresentIdx = -1;
				int iRetError = -1;
				iRetError = pGameODBC->spFSBuyAndSendCharacterSlot( szUserID, iUserIDIndex, iGameIDIndex, szRecvGameID, iPresentIdx, szGameID, szTitle,
					szText, iItemIndex,	iSlotBonusPoint, iPrevMoney, iPostMoney, szBillingKey, szProductCode, pBillingInfo->iItemPrice );

				if( ODBC_RETURN_SUCCESS == iRetError )					
				{
					CFSGameServer* pServer = CFSGameServer::GetInstance();
					if( pServer != NULL )
					{
						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
						if( pCenter != NULL ) 
						{
							pCenter->SendUserNotify( szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR );				
						}
					}				
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSBuyAndSendCharacterSlot - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
					if ( iRetError == 2 )
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
					}
					else
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
					}
				}
			}
			else
			{
				if( ODBC_RETURN_SUCCESS == pGameODBC->spBuyCharacterSlot( iUserIDIndex, iCharacterSlotType, iItemIndex, iSlotBonusPoint, iPrevMoney, iPostMoney, szBillingKey ) )
				{			
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spBuyCharacterSlot - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
				}
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

			SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);			
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();
	}	

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );
}

BOOL CFSGameUserItem::BuyItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );
	// 이벤트를 위해 필요
	CFSGameServer* pServer = CFSGameServer::GetInstance();	
	CHECK_NULL_POINTER_BOOL( pServer );
	// 20080929 프리미엄 아이템 사기 - pAvatarItemList와 pAvatar는 프리미엄 아이템 때문에 필요함
	CAvatarItemList *pAvatarItemList = pUser->GetCurAvatarItemListWithGameID( pUser->GetGameID() );
	CHECK_NULL_POINTER_BOOL( pAvatarItemList );
	SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx( pUser->GetCurAvatarIdx()) ;
	CHECK_NULL_POINTER_BOOL( pAvatar );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPropertyValue = pBillingInfo->iParam0;
	int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
	iaPropertyIndex[ITEM_PROPERTYINDEX_0] = pBillingInfo->iParam1;
	iaPropertyIndex[ITEM_PROPERTYINDEX_1] = pBillingInfo->iParam2;
	iaPropertyIndex[ITEM_PROPERTYINDEX_2] = pBillingInfo->iParam3;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_3] = pBillingInfo->iParam4;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_4] = pBillingInfo->iParam5;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_5] = pBillingInfo->iParam6;	
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;	
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iItemPriceClubCoin = pBillingInfo->iClubCoinChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;

	int iRenewItemIdx = pBillingInfo->iRenewIndex;
	int iOperationCode = pBillingInfo->iOperationCode;

	vector<SUserItemInfo> vUserTempItemInfo;	
	int iItemPropertyPrice = pBillingInfo->iItemPropertyPrice;
	int iItemPropertyType = pBillingInfo->iItemPropertyType;
	int iItemPropertyPrice2 = pBillingInfo->iItemPropertyPrice2;
	int iItemPropertyType2 = pBillingInfo->iItemPropertyType2;
	char* szProductCode = pBillingInfo->szProductCode;
	BOOL bIsLocalPublisher = pBillingInfo->bIsLocalPublisher;

	// 선물에 쓰이는 변수들
	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;

	if ( -1 != iRenewItemIdx )
	{
		iPacketOperationCode = FS_ITEM_OP_RENEW;
	}
	else if( bIsSendItem == TRUE )
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
	}
	else
	{
		iPacketOperationCode = FS_ITEM_OP_BUY;
	}

	if(TRUE == pBillingInfo->bSaleRandomItemEvent)
	{
		iPacketOperationCode = FS_ITEM_OP_BUY_SALE_RANDOMITEM;
	}

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	SShopItemInfo ShopItemInfo;			
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			pItemShop->GetItem(iItemIndex , ShopItemInfo);	

			int iSetItemPropertyKind = -1;
			int iBuyTatooPriceCash = 0;
			int iBuyTatooPricePoint = 0;

			int iBuyOnePriceCash = 0; // 수량제 아이템의 경우 1개.
			int iBuyOnePricePoint = 0; // 수량제 아이템의 경우 1개.


			int iBuyFunctionalItem = 0;

			pItemShop->SetItemPropertyKind( ShopItemInfo.iPropertyKind , iSetItemPropertyKind );
			SetOneUnitItemPrice(pGameODBC, pItemShop,bIsLocalPublisher, ShopItemInfo, iPropertyValue,iItemPriceCash,iItemPricePoint, iBuyOnePriceCash, iBuyOnePricePoint, iErrorCode );

			SUserItemInfo UserItemInfo;
			UserItemInfo.iItemCode = iItemIndex;
			UserItemInfo.iChannel = ShopItemInfo.iChannel;
			UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;		
			UserItemInfo.iPropertyType = ShopItemInfo.iPropertyType;
			UserItemInfo.iPropertyKind = iSetItemPropertyKind;
			UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
			UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
			UserItemInfo.iPropertyTypeValue = iPropertyValue;
			UserItemInfo.iStatus = 1;
			UserItemInfo.iSellType = REFUND_TYPE_POINT;

			UserItemInfo.iSellPrice = GetSellPrice( iBuyOnePriceCash, iBuyOnePricePoint );

			int	iCntSupplyItem = 0;
			SUserItemInfo itemList[MAX_ITEMS_IN_PACKAGE];

			int iItemPrice = iItemPricePoint;
			if(iBuyPriceType == SELL_TYPE_TROPHY)
				iItemPrice = iItemPriceTrophy;
			else if(iBuyPriceType == SELL_TYPE_CLUBCOIN)
				iItemPrice = iItemPriceClubCoin;

			if(  -1 != iRenewItemIdx )
			{
				int errorcode = 0;
				if ( 0 == ShopItemInfo.iUseOption )
				{
					iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
				}
				else if( ODBC_RETURN_SUCCESS == pGameODBC->spFSItemRenew( iUserIDIndex, iGameIDIndex, iRenewItemIdx, iBuyPriceType, iItemPrice,
					iPrevMoney, iPostMoney, UserItemInfo, szBillingKey, itemList, iCntSupplyItem, szProductCode, errorcode ) )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS; 

					pAvatarItemList->ChangeItemPropertyValueAndExpireDateWithItemIndex(iRenewItemIdx,itemList[0].iPropertyTypeValue,itemList[0].ExpireDate);
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSItemRenew - ErrorCode:%d, ItemCode:%d, UserID:%s, GameID:%s", errorcode, iItemIndex, szUserID, szGameID);
				}
			}
			else if( bIsSendItem == FALSE )
			{
				int iUpdatedTokenItemCount = 0;
				int iUpdatedClubCoin = 0;
				int errorcode = 0;
				if( ODBC_RETURN_SUCCESS == pGameODBC->spFSBuyItem( iUserIDIndex, iGameIDIndex, ShopItemInfo.iItemCode0, iBuyPriceType, iItemPrice,
					iPrevMoney, iPostMoney, UserItemInfo, iaPropertyIndex, iItemPropertyType, iItemPropertyPrice,
					iItemPropertyType2, iItemPropertyPrice2, szBillingKey, itemList, iCntSupplyItem, szProductCode, errorcode, pBillingInfo->iTokenItemIdx, iUpdatedTokenItemCount, pBillingInfo->iItemPrice, m_pUser->GetClubSN(), iUpdatedClubCoin, iOperationCode) )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS; 

					ArrangeItemInfoToAddBuyItem( pAvatarItemList, pUser, pItemShop, ShopItemInfo, UserItemInfo, iCntSupplyItem, iaPropertyIndex,
						itemList, vUserTempItemInfo );

					if(UserItemInfo.iBigKind == ITEM_BIG_KIND_ACC ||
						(MIN_LINK_ITEM_PROPERTY_KIND_NUM <= UserItemInfo.iPropertyKind && UserItemInfo.iPropertyKind <= MAX_LINK_ITEM_PROPERTY_KIND_NUM) || 
						ITEM_PROPERTY_KIND_SPECIAL_PROPERTY_ACC == UserItemInfo.iPropertyKind)
					{
						if(iaPropertyIndex[0] == -1 && 
							iaPropertyIndex[1] == -1 && 
							iaPropertyIndex[2] == -1 &&
							iaPropertyIndex[3] == -1 &&
							iaPropertyIndex[4] == -1 &&
							iaPropertyIndex[5] == -1 )
						{
							if( ITEM_PROPERTY_KIND_COMPOUNDING_ITEM != UserItemInfo.iPropertyKind )
							{
								UserItemInfo.iPropertyKind = -1;
							}
						}
					
						UserItemInfo.BuyDate = _time64(NULL);
					}
					
					if(ITEM_PROPERTY_KIND_EVENT_ACC_BUBBLE_MIN <= ShopItemInfo.iPropertyKind && ShopItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_EVENT_ACC_BUBBLE_MAX)
					{
						pUser->UpdateBubbleItem(ShopItemInfo.iItemCode0, iaPropertyIndex[ITEM_PROPERTYINDEX_0],iaPropertyIndex[ITEM_PROPERTYINDEX_1]);
					}

					AddItemToUserItemList( pAvatarItemList, iCntSupplyItem, UserItemInfo, vUserTempItemInfo);

					if (0 < pBillingInfo->iTokenItemIdx)
					{
						if (0 == iUpdatedTokenItemCount)
							pAvatarItemList->RemoveItem(pBillingInfo->iTokenItemIdx);
						else
							pAvatarItemList->UpdateItemCount(pBillingInfo->iTokenItemIdx, iUpdatedTokenItemCount);
					}

					if(PROPENSITY_ITEM_KIND_START <= ShopItemInfo.iPropertyKind && ShopItemInfo.iPropertyKind <= PROPENSITY_ITEM_KIND_END)
						pUser->GetUserShoppingFestivalEvent()->RemoveShoppingBasketItem(EVENT_SHOPPINGFESTIVAL_SHOPPINGBASKET_KIND_ITEM, 0, iItemIndex);

					if(SELL_TYPE_CLUBCOIN == ShopItemInfo.iSellType &&
						m_pUser->GetClubSN() > 0)
					{
						m_pUser->SetUserClubCoin(iUpdatedClubCoin);
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSBuyItem - ErrorCode:%d, ItemCode:%d, UserID:%s, GameID:%s", errorcode, iItemIndex, szUserID, szGameID);
				}
			}
			else if( bIsSendItem == TRUE )
			{				
				int iPresentIdx = -1;
				int iRetError = -1;

				iRetError = pGameODBC->spFSBuyAndSendItem( szUserID, iUserIDIndex, iGameIDIndex, szGameID, szRecvGameID, iPresentIdx, szTitle,
					szText, ShopItemInfo.iItemCode0, iItemPropertyType, iItemPropertyPrice, iItemPropertyType2, iItemPropertyPrice2, iBuyPriceType, iItemPrice, iPrevMoney, iPostMoney,
					iaPropertyIndex, UserItemInfo, szBillingKey, szProductCode, pBillingInfo->iItemPrice );
					
				if( ODBC_RETURN_SUCCESS == iRetError )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;					
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
					CFSGameServer* pServer = CFSGameServer::GetInstance();
					if( pServer != NULL )
					{
						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
						if( pCenter != NULL ) 
						{
							pCenter->SendUserNotify( szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR );				
						}
					}
				}
				else if ( iRetError == -11 )
				{
					iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSBuyAndSendItem - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
					
					iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
				}
			}

			if (BUY_ITEM_ERROR_SUCCESS == iErrorCode)
			{
				int iPreSkillPoint = pUser->GetSkillPoint();
				int iPreEventCoin = pUser->GetEventCoin();

				m_pUser->SetPayCash(pBillingInfo->iCashChange);
				if(pBillingInfo->bUseEventCoin == TRUE)
					m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

				pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

				iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
				iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
				if(iEventBonusPoint > 0 || iEventBonusCash > 0)
					iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

				SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
			}
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserStat();
		pUser->SendAllMyOwnItemInfo();
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();

		ProcessAchievementBuyItem(pGameODBC, pUser, pAvatarItemList);

		if(ITEM_CATEGORY_CLUB == ShopItemInfo.iCategory)
			pUser->SendClubShopUserCashInfo();
	}

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );	
}

BOOL CFSGameUserItem::BuyChangeDressItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyChangeDressItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );
	CFSGameServer* pServer = CFSGameServer::GetInstance();	
	CHECK_NULL_POINTER_BOOL( pServer );
	CAvatarItemList *pAvatarItemList = pUser->GetCurAvatarItemListWithGameID( pUser->GetGameID() );
	CHECK_NULL_POINTER_BOOL( pAvatarItemList );
	SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx( pUser->GetCurAvatarIdx()) ;
	CHECK_NULL_POINTER_BOOL( pAvatar );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPropertyValue = pBillingInfo->iParam0;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;	
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iItemPriceClubCoin = pBillingInfo->iClubCoinChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;

	int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
	iaPropertyIndex[ITEM_PROPERTYINDEX_0] = pBillingInfo->iParam1;
	iaPropertyIndex[ITEM_PROPERTYINDEX_1] = pBillingInfo->iParam2;
	iaPropertyIndex[ITEM_PROPERTYINDEX_2] = pBillingInfo->iParam3;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_3] = pBillingInfo->iParam4;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_4] = pBillingInfo->iParam5;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_5] = pBillingInfo->iParam6;	

	vector<SUserItemInfo> vUserTempItemInfo;	
	int iItemPropertyPrice = pBillingInfo->iItemPropertyPrice;
	int iItemPropertyType = pBillingInfo->iItemPropertyType;
	int iItemPropertyPrice2 = pBillingInfo->iItemPropertyPrice2;
	int iItemPropertyType2 = pBillingInfo->iItemPropertyType2;
	char* szProductCode = pBillingInfo->szProductCode;
	BOOL bIsLocalPublisher = pBillingInfo->bIsLocalPublisher;

	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;

	if( bIsSendItem == TRUE )
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
	}
	else
	{
		iPacketOperationCode = FS_ITEM_OP_BUY;
	}

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	SShopItemInfo ShopItemInfo;			
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			pItemShop->GetItem(iItemIndex , ShopItemInfo);	

			int iSetItemPropertyKind = -1;
			int iBuyTatooPriceCash = 0;
			int iBuyTatooPricePoint = 0;

			int iBuyOnePriceCash = 0; 
			int iBuyOnePricePoint = 0;

			int iBuyFunctionalItem = 0;

			pItemShop->SetItemPropertyKind( ShopItemInfo.iPropertyKind , iSetItemPropertyKind );
			SetOneUnitItemPrice(pGameODBC, pItemShop,bIsLocalPublisher, ShopItemInfo, iPropertyValue,iItemPriceCash,iItemPricePoint, iBuyOnePriceCash, iBuyOnePricePoint, iErrorCode );

			SUserItemInfo UserItemInfo;
			UserItemInfo.iItemCode = iItemIndex;
			UserItemInfo.iChannel = ShopItemInfo.iChannel;
			UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;		
			UserItemInfo.iPropertyType = ShopItemInfo.iPropertyType;
			UserItemInfo.iPropertyKind = iSetItemPropertyKind;
			UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
			UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
			UserItemInfo.iPropertyTypeValue = iPropertyValue;
			UserItemInfo.iStatus = 1;
			UserItemInfo.iSellType = REFUND_TYPE_POINT;

			UserItemInfo.iSellPrice = GetSellPrice( iBuyOnePriceCash, iBuyOnePricePoint );

			int	iCntSupplyItem = 0;
			SUserItemInfo itemList[MAX_ITEMS_IN_PACKAGE];

			int iItemPrice = iItemPricePoint;
			if(iBuyPriceType == SELL_TYPE_TROPHY)
				iItemPrice = iItemPriceTrophy;
			else if(iBuyPriceType == SELL_TYPE_CLUBCOIN)
				iItemPrice = iItemPriceClubCoin;

			if( bIsSendItem == FALSE )
			{
				int errorcode = 0;
				int iUpdatedClubCoin = 0;
				if( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyChangeDressItem( iUserIDIndex, iGameIDIndex, ShopItemInfo.iItemCode0, iBuyPriceType, iItemPrice,
					iPrevMoney, iPostMoney, UserItemInfo, iItemPropertyType, iItemPropertyPrice, iItemPropertyType2, iItemPropertyPrice2, szBillingKey, itemList, 
					iCntSupplyItem, szProductCode, pBillingInfo->iItemPrice, errorcode, pUser->GetClubSN(), iUpdatedClubCoin ) )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS; 

					for(int i = 0; i < iCntSupplyItem; i++)
					{
						SUserItemInfo* pUserItemInfo = pAvatarItemList->GetItemWithItemIdx(itemList[i].iItemIdx);
						if(pUserItemInfo == NULL)
						{
							SShopItemInfo ShopItemInfo;			
							pItemShop->GetItem(itemList[i].iItemCode, ShopItemInfo);	

							int iSetItemPropertyKind = -1;
							int iBuyTatooPriceCash = 0;
							int iBuyTatooPricePoint = 0;
							int iBuyOnePriceCash = 0; 
							int iBuyOnePricePoint = 0;
							int iBuyFunctionalItem = 0;

							pItemShop->SetItemPropertyKind( ShopItemInfo.iPropertyKind, iSetItemPropertyKind);
							SetOneUnitItemPrice(pGameODBC, pItemShop, bIsLocalPublisher, ShopItemInfo, iPropertyValue,iItemPriceCash,iItemPricePoint, iBuyOnePriceCash, iBuyOnePricePoint, iErrorCode );

							itemList[i].iCategory = ShopItemInfo.iCategory;
							itemList[i].iChannel = ShopItemInfo.iChannel;
							itemList[i].iSexCondition = ShopItemInfo.iSexCondition;		
							itemList[i].iPropertyType = ShopItemInfo.iPropertyType;
							itemList[i].iPropertyKind = iSetItemPropertyKind;
							itemList[i].iBigKind = ShopItemInfo.iBigKind;
							itemList[i].iSmallKind = ShopItemInfo.iSmallKind;
							itemList[i].iPropertyTypeValue = iPropertyValue;
							itemList[i].iStatus = 1;
							itemList[i].iSellType = REFUND_TYPE_POINT;

							InsertNewItem(itemList[i]);
						}
						else
						{
							if(pUserItemInfo->iPropertyTypeValue != -1)
								pUserItemInfo->iPropertyTypeValue += UserItemInfo.iPropertyTypeValue;

							pUserItemInfo->ExpireDate = itemList[i].ExpireDate;
						}

						if(SELL_TYPE_CLUBCOIN == ShopItemInfo.iSellType &&
							m_pUser->GetClubSN() > 0)
						{
							m_pUser->SetUserClubCoin(iUpdatedClubCoin);
						}
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyChangeDressItem - ErrorCode:%d, ItemCode:%d, UserID:%s, GameID:%s", errorcode, iItemIndex, szUserID, szGameID);
				}
			}
			else if( bIsSendItem == TRUE )
			{				
				int iPresentIdx = -1;
				int iRetError = -1;
				iRetError = pGameODBC->spFSBuyAndSendItem( szUserID, iUserIDIndex, iGameIDIndex, szGameID, szRecvGameID, iPresentIdx, szTitle,
					szText, ShopItemInfo.iItemCode0, iItemPropertyType, iItemPropertyPrice, iItemPropertyType2, iItemPropertyPrice2, iBuyPriceType, iItemPrice, iPrevMoney, iPostMoney,
					iaPropertyIndex, UserItemInfo, szBillingKey, szProductCode, pBillingInfo->iItemPrice );

				if( ODBC_RETURN_SUCCESS == iRetError )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;					
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
					CFSGameServer* pServer = CFSGameServer::GetInstance();
					if( pServer != NULL )
					{
						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
						if( pCenter != NULL ) 
						{
							pCenter->SendUserNotify( szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR );				
						}
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSBuyAndSendItem - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
					if ( iRetError == -11 )
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
					}
					else
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
					}
				}
			}

			if (BUY_ITEM_ERROR_SUCCESS == iErrorCode)
			{
				int iPreSkillPoint = pUser->GetSkillPoint();
				int iPreEventCoin = pUser->GetEventCoin();

				m_pUser->SetPayCash(pBillingInfo->iCashChange);
				if(pBillingInfo->bUseEventCoin == TRUE)
					m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

				pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

				iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
				iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
				if(iEventBonusPoint > 0 || iEventBonusCash > 0)
					iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

				SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
			}
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserStat();		
		pUser->SendAllMyOwnItemInfo();
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();

		ProcessAchievementBuyItem(pGameODBC, pUser, pAvatarItemList);

		if(ITEM_CATEGORY_CLUB == ShopItemInfo.iCategory)
			pUser->SendClubShopUserCashInfo();
	}

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );	
}

BOOL CFSGameUserItem::ArrangeItemInfoToAddBuyItem( CAvatarItemList *pAvatarItemList, CFSGameUser* pUser, CFSItemShop* pItemShop, SShopItemInfo &ShopItemInfo,
												  SUserItemInfo &UserItemInfo, int iCntSupplyItem, int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT],
												  SUserItemInfo itemList[MAX_ITEMS_IN_PACKAGE], vector<SUserItemInfo>& vUserTempItemInfo )
{
	CHECK_NULL_POINTER_BOOL(pItemShop);
	CHECK_NULL_POINTER_BOOL(pUser);
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);	

	vector< SItemProperty > vItemProperty;
	pItemShop->GetItemPropertyList(iaPropertyIndex, vItemProperty);

	if (iCntSupplyItem == 1)
	{
		UserItemInfo.iItemIdx = itemList[0].iItemIdx;
		UserItemInfo.ExpireDate = itemList[0].ExpireDate;
		UserItemInfo.iPropertyTypeValue = itemList[0].iPropertyTypeValue;

		if(ITEM_SMALL_KIND_CLUB_PRIMIUM_ETC == UserItemInfo.iSmallKind)
			UserItemInfo.iSmallKind = ITEM_SMALL_KIND_ETC;

		InsertUserItemProperty( pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, pAvatarItemList, vItemProperty );
		UserItemInfo.iPropertyNum = vItemProperty.size();					
	}
	else
	{
		int iLoopIndex;
		for (iLoopIndex = 0; iLoopIndex < iCntSupplyItem; iLoopIndex++)
		{
			InsertUserItemProperty( pUser->GetGameIDIndex(), itemList[iLoopIndex].iItemIdx, pAvatarItemList, vItemProperty );
			itemList[iLoopIndex].iPropertyNum	+= vItemProperty.size();					

			pItemShop->GetItem( itemList[iLoopIndex].iItemCode, ShopItemInfo );	
			itemList[iLoopIndex].iPropertyKind = ShopItemInfo.iPropertyKind;
			itemList[iLoopIndex].iCategory = ShopItemInfo.iCategory;
			itemList[iLoopIndex].iBigKind = ShopItemInfo.iBigKind;
			itemList[iLoopIndex].iSmallKind = ShopItemInfo.iSmallKind;

			if(ITEM_SMALL_KIND_CLUB_PRIMIUM_ETC == ShopItemInfo.iSmallKind)
				itemList[iLoopIndex].iSmallKind = ITEM_SMALL_KIND_ETC;

			itemList[iLoopIndex].iSellType = ShopItemInfo.iSellType;
			itemList[iLoopIndex].iChannel = ShopItemInfo.iChannel;					
			itemList[iLoopIndex].iStatus = ShopItemInfo.iStatus;
			itemList[iLoopIndex].iSexCondition = ShopItemInfo.iSexCondition;					
			itemList[iLoopIndex].iStatus = ITEM_STATUS_INVENTORY;
			itemList[iLoopIndex].iPropertyType = ShopItemInfo.iPropertyType;
			itemList[iLoopIndex].iPropertyTypeValue = UserItemInfo.iPropertyTypeValue;
			vUserTempItemInfo.push_back(itemList[iLoopIndex]);
		}						
	}

	return TRUE;
}

void CFSGameUserItem::AddItemToUserItemList( CAvatarItemList *pAvatarItemList, int iCntSupplyItem, SUserItemInfo &UserItemInfo, vector<SUserItemInfo>& vUserTempItemInfo)
{
	CHECK_NULL_POINTER_VOID( pAvatarItemList );

	if (iCntSupplyItem == 1)
	{
		if (ITEM_PROPERTY_EXHAUST == UserItemInfo.iPropertyType)
		{
			pAvatarItemList->UpdateExhaustItemPropertyValue(UserItemInfo);
		}
		else
		{
			/*	50114171,	50114172,	50114173	윙윙 프로렐러
				50910731							블링블링 안경
				51411371							이발소 간판*/
			 // 오늘의 핫딜 이벤트 동안만 이렇게 쓰고 지워야 합니다. ( 한국만 )
			if( 50114171 == UserItemInfo.iItemCode ||
				50114172 == UserItemInfo.iItemCode ||
				50114173 == UserItemInfo.iItemCode)
			{
				UserItemInfo.iBigKind = ITEM_BIG_KIND_ACC;
				UserItemInfo.iSmallKind = ITEM_BIG_KIND_DRESS;
			}
			else if( 50910731 == UserItemInfo.iItemCode )
			{
				UserItemInfo.iBigKind = ITEM_BIG_KIND_ACC;
				UserItemInfo.iSmallKind = ITEM_SMALL_KIND_ACC_FACE;
			}
			else if ( 51411371 == UserItemInfo.iItemCode )
			{
				UserItemInfo.iBigKind = ITEM_BIG_KIND_ACC;
				UserItemInfo.iSmallKind = ITEM_SMALL_KIND_ACC_BODY;
			}

			pAvatarItemList->AddItem(UserItemInfo);
		}
	}
	else
	{
		if( !vUserTempItemInfo.empty() )
		{
			for( int iTempItemCount = 0; iTempItemCount < vUserTempItemInfo.size(); iTempItemCount++ )
			{
				pAvatarItemList->AddItem(vUserTempItemInfo[iTempItemCount]);
			}
		}
	}
}

BOOL CFSGameUserItem::BuyPremiumItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyPremiumItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );
	// 20080929 프리미엄 아이템 사기 - pAvatarItemList와 pAvatar는 프리미엄 아이템 때문에 필요함
	CAvatarItemList *pAvatarItemList = pUser->GetCurAvatarItemListWithGameID( pUser->GetGameID() );
	CHECK_NULL_POINTER_BOOL( pAvatarItemList );
	SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx( pUser->GetCurAvatarIdx()) ;
	CHECK_NULL_POINTER_BOOL( pAvatar );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iPropertyValue = pBillingInfo->iParam0;
	int iPropertyIndex1 = pBillingInfo->iParam1;
	int iPropertyIndex2 = pBillingInfo->iParam2;
	int iPropertyIndex3 = pBillingInfo->iParam3;
	int iItemPriceCash = pBillingInfo->iCashChange;	
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;
	int iItemPropertyPrice = pBillingInfo->iItemPropertyPrice;
	int iItemPropertyType = pBillingInfo->iItemPropertyType;
	int iItemPropertyPrice2 = pBillingInfo->iItemPropertyPrice2;
	int iItemPropertyType2 = pBillingInfo->iItemPropertyType2;
	char* szProductCode = pBillingInfo->szProductCode;
	BOOL bIsLocalPublisher = pBillingInfo->bIsLocalPublisher;

	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = FS_ITEM_OP_BUY;
	if( bIsSendItem == TRUE )
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
	}

	if(TRUE == pBillingInfo->bSaleRandomItemEvent)
	{
		iPacketOperationCode = FS_ITEM_OP_BUY_SALE_RANDOMITEM;
	}

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS :
		{
			SShopItemInfo ShopItemInfo;			
			pItemShop->GetItem(iItemIndex , ShopItemInfo);	

			int iSetItemPropertyKind = -1;
			int iBuyTatooPriceCash = 0;
			int iBuyTatooPricePoint = 0;
			int iBuyOnePriceCash = 0; // 수량제 아이템의 경우 1개.
			int iBuyOnePricePoint = 0; // 수량제 아이템의 경우 1개.


			int iBuyFunctionalItem = 0;

			pItemShop->SetItemPropertyKind( ShopItemInfo.iPropertyKind , iSetItemPropertyKind );
			SetOneUnitItemPrice(pGameODBC, pItemShop,bIsLocalPublisher, ShopItemInfo, iPropertyValue, iItemPriceCash, iItemPricePoint, iBuyOnePriceCash, iBuyOnePricePoint, iErrorCode );

			SUserItemInfo UserItemInfo;
			UserItemInfo.iItemCode = iItemIndex;
			UserItemInfo.iChannel = ShopItemInfo.iChannel;
			UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;		
			UserItemInfo.iPropertyType = ShopItemInfo.iPropertyType;
			UserItemInfo.iPropertyKind = iSetItemPropertyKind;
			UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
			UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
			UserItemInfo.iPropertyTypeValue = iPropertyValue;
			UserItemInfo.iStatus = 1;
			UserItemInfo.iSellType = REFUND_TYPE_POINT;

			if( ShopItemInfo.iChannel == ITEMCHANNEL_TATTOO && ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST )
			{
				UserItemInfo.iSellPrice = GetSellPrice( iBuyTatooPriceCash, iBuyTatooPricePoint );
			}
			else
			{
				UserItemInfo.iSellPrice = GetSellPrice( iItemPriceCash, iItemPricePoint );
			}

			SPremiumItem *pPremiumItemInfo = pItemShop->GetShopItemList()->GetPremiumItem(iItemIndex);
			CHECK_NULL_POINTER_BOOL(pPremiumItemInfo)

			 if(ShopItemInfo.iPropertyType == ITEM_PROPERTY_EXHAUST)
			{
				UserItemInfo.iPropertyTypeValue = 3;									// 수량 아이템
			}
			if( ShopItemInfo.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP)
            {
				UserItemInfo.iPropertyTypeValue = pBillingInfo->iTerm;
            }

			UserItemInfo.iSellPrice = GetSellPrice(iItemPriceCash, iItemPricePoint);

			int	iaOutItemIdx[MAX_PREMIUMITEM_ITEMNUM] = {0};

			if(!bIsSendItem)
			{
				if( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyPremiumItem( szUserID, iUserIDIndex, iGameIDIndex, iItemIndex, iBuyPriceType,
					iItemPricePoint, iPrevMoney, iPostMoney, UserItemInfo, iPropertyIndex1 ,iPropertyIndex2, iPropertyIndex3, iaOutItemIdx, " ",  pBillingInfo->iItemPrice, iErrorCode, szProductCode ) )
				{			
					for( int iLoopIndex = 0; iLoopIndex < MAX_PREMIUMITEM_ITEMNUM; iLoopIndex++ )
					{
						if( pPremiumItemInfo->iItemCode[iLoopIndex] > 0 && iaOutItemIdx[iLoopIndex] > 0 )
						{						
							SShopItemInfo  PremiumItemInfoOne;
							if( FALSE == pItemShop->GetItem( pPremiumItemInfo->iItemCode[iLoopIndex] , PremiumItemInfoOne )) 
							{
								return FALSE;
							}

							SUserItemInfo PremiumUserItemInfo;
							PremiumUserItemInfo.iItemCode = pPremiumItemInfo->iItemCode[iLoopIndex];

							if( PremiumItemInfoOne.iPropertyType == ITEM_PROPERTY_TIME )
							{
								PremiumUserItemInfo.iPropertyTypeValue = pPremiumItemInfo->iUseTerm;			// 기간제 아이템
							}
							else if( PremiumItemInfoOne.iPropertyType == ITEM_PROPERTY_EXHAUST )
							{
								if(ShopItemInfo.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP || ShopItemInfo.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP_PLUS)
								{
									PremiumUserItemInfo.iPropertyTypeValue = 10;	
								}
								else
								{
									PremiumUserItemInfo.iPropertyTypeValue = 3;	
								}									
							}

							PremiumUserItemInfo.iItemIdx = iaOutItemIdx[iLoopIndex];
							PremiumUserItemInfo.ExpireDate = UserItemInfo.ExpireDate;

							InsertPremiumItem( pAvatarItemList, PremiumItemInfoOne, PremiumUserItemInfo, pPremiumItemInfo->iPropertyIndex1[iLoopIndex], pPremiumItemInfo->iPropertyIndex2[iLoopIndex], pPremiumItemInfo->iPropertyIndex3[iLoopIndex]);
						}
					}

					if( ShopItemInfo.iPropertyKind == PREMIUM_ITEM_KIND_POWERUP ||ShopItemInfo.iPropertyKind == PREMIUM_ITEM_KIND_SUPER_ROOKIE )
					{
						SUserItemInfo* pCheckItem = pAvatarItemList->GetInventoryItemWithPropertyKind(ShopItemInfo.iPropertyKind);
						if( pCheckItem)
						{
							pAvatarItemList->ChangeItemPropertyValueAndExpireDateWithItemIndex(pCheckItem->iItemIdx,pCheckItem->iPropertyTypeValue + UserItemInfo.iPropertyTypeValue,UserItemInfo.ExpireDate);
						}
						else
						{
							InsertPremiumItem( pAvatarItemList, ShopItemInfo, UserItemInfo, iPropertyIndex1, iPropertyIndex2, iPropertyIndex3 );
						}
					}
					else
					{
						InsertPremiumItem( pAvatarItemList, ShopItemInfo, UserItemInfo, iPropertyIndex1, iPropertyIndex2, iPropertyIndex3 );
					}

					iErrorCode = BUY_ITEM_ERROR_SUCCESS;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyPremiumItem - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
					return FALSE;
				}
			}
			else
			{
				int iPresentIdx = -1;
				int iRetError = -1;
				int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
				iaPropertyIndex[ITEM_PROPERTYINDEX_0] = iPropertyIndex1;
				iaPropertyIndex[ITEM_PROPERTYINDEX_1] = iPropertyIndex2;
				iaPropertyIndex[ITEM_PROPERTYINDEX_2] = iPropertyIndex3;

				iRetError = pGameODBC->spFSBuyAndSendItem(szUserID, iUserIDIndex, iGameIDIndex, szGameID, szRecvGameID, iPresentIdx, szTitle, szText,
					ShopItemInfo.iItemCode0, iItemPropertyType, iItemPropertyPrice, iItemPropertyType2, iItemPropertyPrice2, iBuyPriceType, iItemPricePoint, iPrevMoney, iPostMoney,
					iaPropertyIndex, UserItemInfo, szBillingKey ,szProductCode, pBillingInfo->iItemPrice);
				if( ODBC_RETURN_SUCCESS == iRetError )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;					
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
					CFSGameServer* pServer = CFSGameServer::GetInstance();
					if( pServer != NULL )
					{
						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
						if( pCenter != NULL ) 
						{
							pCenter->SendUserNotify( szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR );				
						}
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyPremiumItem - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
					if ( iRetError == -11 )
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
					}
					else
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
					}
				}
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

			SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default:
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if( BUY_ITEM_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserStat();		
		pUser->SendAllMyOwnItemInfo();
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();

		ProcessAchievementBuyItem(pGameODBC, pUser, pAvatarItemList);
	}

	return (iErrorCode >= BUY_ITEM_ERROR_SUCCESS);	
}

BOOL CFSGameUserItem::BuyLvUpItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyLvUpItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx( pUser->GetCurAvatarIdx()) ;
	CHECK_NULL_POINTER_BOOL( pAvatar );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_SUCCESS;

	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iPropertyKind = pBillingInfo->iParam0;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;

	int iCurrentTrophyCount = 0;
	int iGetItemExp = 0;
	int iResult = 0;
	int iRequireLv = 0, iBonusPoint = 0;

	if( ODBC_RETURN_SUCCESS !=pGameODBC->ITEM_BuyLvAndExpUpItem( pAvatar->iGameIDIndex, iItemIndex, iPropertyKind, iItemPriceTrophy, pBillingInfo->iItemPrice, iCurrentTrophyCount, iGetItemExp, iResult, iRequireLv, iBonusPoint) )
	{			
		if(iResult == -3)
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_TROPHY;
		}
		else if(iResult == -5)
		{
			iErrorCode = BUY_ITEM_ERROR_MISMATCH_LEVEL;
		}
		else
		{
			iErrorCode = BUY_ITEM_ERROR_GENERAL;
		}
		WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyLvAndExpUpItem - ErrorCode:%d, ItemCode:%d, GameID:%s", iErrorCode, iItemIndex, pBillingInfo->szGameID);
	}
	else
	{		
		int iPreLv = pUser->GetCurUsedAvtarLv();
		if( TRUE == pUser->CheckLvUp(iGetItemExp, pGameODBC) )
		{
			if( TRUE == pUser->ProcessStatUp(pGameODBC) )
			{
				iErrorCode = BUY_ITEM_ERROR_SUCCESS;				
				pUser->SetUserTrophy( iCurrentTrophyCount );
				pUser->SetBonusPointTerms(iRequireLv, iBonusPoint);

				pUser->SetBonusPointTerms(iRequireLv, iBonusPoint);				
				if(iBonusPoint > 0)
				{
					pUser->SetUserSkillPoint(pUser->GetUserSkillPoint() + iBonusPoint);
				}
				pUser->GetUserCharacterCollection()->CheckLvUp(iRequireLv);
				pUser->UpdateAvatarExp(pUser->GetGameIDIndex(), pUser->GetCurUsedAvtarLv(), pUser->GetCurUsedAvtarExp());
			}
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_ITEM, EXEC_FUNCTION, FAIL, "CheckLvUp - ItemCode:%d, GameID:%s, GetExpFromSP:%d", iItemIndex, pBillingInfo->szGameID, iGetItemExp);
		}
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);
	if(BUY_ITEM_ERROR_MISMATCH_LEVEL == iErrorCode)
	{
		CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
		if(pItemShop)
		{
			SShopItemInfo ShopItemInfo;
			pItemShop->GetItem( iItemIndex, ShopItemInfo );

			PacketComposer.Add(ShopItemInfo.iLvCondition);
			PacketComposer.Add(ShopItemInfo.iMaxLvCondition);
		}
		else
		{
			PacketComposer.Add((int)0);
			PacketComposer.Add((int)0);
		}		
	}
	pClient->Send(&PacketComposer);

	if( BUY_ITEM_ERROR_SUCCESS == iErrorCode )
	{
		pUser->SendUserStat();		
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();
	}

	return TRUE;
}

BOOL CFSGameUserItem::CheckPayAbility( SShopItemInfo& ShopItemInfo, int iOperationCode, int iLoopCount, int iBuyPriceCash, int iTotalCash,
									  int iBuyPricePoint, int iTotalPoint, int iBuyPriceTrophy, int iTotalTrophy, int iBuyPriceClubCoin, int *res, int &iErrorCode )
{	
	BOOL bReturn = TRUE;	

	if( FS_ITEM_OP_BUY == iOperationCode || FS_ITEM_OP_RENEW == iOperationCode || FS_ITEM_OP_BUY_SECRETSTOREITEM == iOperationCode
		|| FS_ITEM_OP_BUY_TODAYHOTDEAL == iOperationCode || FS_ITEM_OP_BUY_SALE_RANDOMITEM == iOperationCode  || FS_ITEM_OP_BUY_EVENT_SALEPLUS == iOperationCode)
	{
		if(ShopItemInfo.iPropertyKind >= ITEM_PROPERTY_KIND_SALE_COUPON_MIN &&
			ShopItemInfo.iPropertyKind <= ITEM_PROPERTY_KIND_SALE_COUPON_MAX)
		{
			if( m_pUser->GetCoin() < iBuyPriceCash + iTotalCash )
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
				if(res) *res = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
				bReturn = FALSE;
			}
		}
		else if( m_pUser->GetCoin() + m_pUser->GetEventCoin() < iBuyPriceCash + iTotalCash )
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			if(res) *res = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
			bReturn = FALSE;
		}
		else if (FALSE == ShopItemInfo.bIsPurchasableInEventCoin &&
				m_pUser->GetCoin() < iBuyPriceCash + iTotalCash )
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			if(res) *res = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
			bReturn = FALSE;
		}
	}
	else
	{
		if(FS_ITEM_OP_SEND == iOperationCode)
		{
			if(iBuyPriceCash > 0 &&
				m_pUser->GetBonusCoin() > 0 &&
				m_pUser->CheckGMUser() == FALSE &&
				m_pUser->CheckUseableBonusCoinUser() == FALSE)
			{
				iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
				if(res) *res = SEND_ITEM_ERROR_CANT_PURCHASE_IN_EVENTCASH;
				bReturn = FALSE;
			}
		}
		
		if( m_pUser->GetCoin() + m_pUser->GetEventCoin() < iBuyPriceCash + iTotalCash )
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			if(res) *res = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
			bReturn = FALSE;
		}
		else if( m_pUser->GetCoin() < iBuyPriceCash + iTotalCash )
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			if(res) *res = SEND_ITEM_ERROR_CANT_PURCHASE_IN_EVENTCASH;
			bReturn = FALSE;
		}
	}

	if( m_pUser->GetSkillPoint() < iBuyPricePoint + iTotalPoint )
	{
		if(res) *res = SEND_ITEM_ERROR_NOT_ENOUGH_POINT;		
		iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_POINT;
		bReturn = FALSE;
	}

	if( m_pUser->GetUserTrophy() < iBuyPriceTrophy + iTotalTrophy )
	{
		iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_TROPHY;
		if(res) *res = SEND_ITEM_ERROR_NOT_ENOUGH_TROPHY;
		bReturn = FALSE;
	}

	if( m_pUser->GetUserClubCoin() < iBuyPriceClubCoin )
	{
		iErrorCode = BUY_ITEM_ERROR_ENOUGH_CLUB_COIN;
		if(res) *res = BUY_ITEM_ERROR_ENOUGH_CLUB_COIN;
		bReturn = FALSE;
	}

	if( FS_ITEM_OP_SEND == iOperationCode && bReturn == FALSE )
	{
		iErrorCode = BUY_ITEM_ERROR_REFER_SEND_ERROR;
	}

	return bReturn;
}

BOOL CFSGameUserItem::CheckHaveBindAccountItem(int iPropertyKind, int& iErrorCode)
{
	CFSGameUser* pUser = GetGameUser();
	if(NULL == pUser)
	{
		iErrorCode = SEND_ITEM_ERROR_NOT_EXIST_USER;
		return FALSE;
	}

	if(TRUE == pUser->CheckHaveBindAccountItem(iPropertyKind))
	{
		iErrorCode = BUY_ITEM_ERROR_ITEM_HAVE_ALREADY;
		return FALSE;
	}

	return TRUE;
}

BOOL CFSGameUserItem::BuyBinAccountItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyBinAccountItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	// 선물에 쓰이는 변수들
	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;

	iPacketOperationCode = FS_ITEM_OP_BUY;
	if(pBillingInfo->bIsSendItem == TRUE)
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
	}

	if(TRUE == pBillingInfo->bSaleRandomItemEvent)
	{
		iPacketOperationCode = FS_ITEM_OP_BUY_SALE_RANDOMITEM;
	}

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS :
		{
			int iaPresentIndex[MAX_EVENT_PRESENT_SIZE];
			memset(iaPresentIndex, -1, (sizeof(int) * MAX_EVENT_PRESENT_SIZE));
			CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
			if(NULL == pGameODBC)
			{
				iErrorCode = BUY_ITEM_ERROR_SUCCESS;
				return FALSE;
			}

			SBuyItemInputParam BuyItemInputParam;
			SBindAccountData BindAccountData;

			if(!pBillingInfo->bIsSendItem)
			{
				BuyItemInputParam.szUserID = pBillingInfo->szUserID;
				BuyItemInputParam.szBillingKey= pBillingInfo->szBillingKey;
				BuyItemInputParam.iUserIDIndex = pBillingInfo->iSerialNum;
				BuyItemInputParam.iGameIDIndex = pUser->GetGameIDIndex();
				BuyItemInputParam.iBuyPointPrice = pBillingInfo->iPointChange;
				BuyItemInputParam.iPropertyKind = pBillingInfo->iParam0;
				BuyItemInputParam.iPropertyTypeValue = pBillingInfo->iParam1;
				BuyItemInputParam.iBuyType = pBillingInfo->iSellType;
				BuyItemInputParam.iItemCode = pBillingInfo->iItemCode;
				BuyItemInputParam.iPrevMoney = pBillingInfo->iCurrentCash;
				BuyItemInputParam.iPostMoney = pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;
				char* szProductCode = pBillingInfo->szProductCode;
				int iOperationCode = pBillingInfo->iOperationCode;
				BuyItemInputParam.iItemPrice = pBillingInfo->iItemPrice;

				if(ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyBindAccountItem(BuyItemInputParam, BindAccountData.ExpireDate, iaPresentIndex, iErrorCode, szProductCode, iOperationCode))
				{			
					BindAccountData.iBindAccountItemPropertyKind = BuyItemInputParam.iPropertyKind;
					pUser->SetBindAccountData(BindAccountData);
					pUser->SendBindAccountInfo();

					for(int i = 0; i < MAX_EVENT_PRESENT_SIZE; i++)
					{
						if(iaPresentIndex[i] != -1)
						{
							pUser->RecvPresent(MAIL_PRESENT_ON_AVATAR, iaPresentIndex[i]);
						}
					}

					iErrorCode = BUY_ITEM_ERROR_SUCCESS;

					if(pUser->GetBindAccountItemPropertyKind() == BIND_ACCOUNT_ITEM_KIND_VIP_PLUS)
					{
						if(0 < pBillingInfo->iCashChange)
						{
							const int iConditionValue = FRIENDINVITE.GetConfigValue(FIC_BONUS_VVIP, FP0);
							if(iConditionValue == BuyItemInputParam.iPropertyTypeValue)
							{
								pUser->GetUserFriendInvite()->CheckAndGiveFriendBenefit(FRIEND_INVITE_BENEFIT_INDEX_VVIP);
							}
						}
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyBindAccountItem - ErrorCode:%d, ItemCode:%d, UserID:%s, GameID:%s", iErrorCode, pBillingInfo->iItemCode, pBillingInfo->szUserID, pBillingInfo->szGameID);
					
					return FALSE;
				}
			}
			else
			{
				SBuyandSendBindAccountItemInputParam BuyandSendItemInputParam;
				BuyandSendItemInputParam.szUserID			= pBillingInfo->szUserID;
				BuyandSendItemInputParam.iSendUserIDIndex	= pBillingInfo->iSerialNum;
				BuyandSendItemInputParam.iSendGameIDIndex	= pUser->GetGameIDIndex();
				BuyandSendItemInputParam.szSendGameID		= pBillingInfo->szGameID;
				BuyandSendItemInputParam.szRecvGameID		= pBillingInfo->szRecvGameID;
				BuyandSendItemInputParam.szTitle			= pBillingInfo->szTitle;
				BuyandSendItemInputParam.szText				= pBillingInfo->szText;
				BuyandSendItemInputParam.iItemCode			= pBillingInfo->iItemCode;
				BuyandSendItemInputParam.iPropertyValue		= pBillingInfo->iParam1;
				BuyandSendItemInputParam.iBuyType			= pBillingInfo->iSellType;
				BuyandSendItemInputParam.iBuyPrice			= pBillingInfo->iPointChange;
				BuyandSendItemInputParam.iPrevMoney			= pBillingInfo->iCurrentCash;
				BuyandSendItemInputParam.iPostMoney			= pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;
				BuyandSendItemInputParam.szOID				= pBillingInfo->szBillingKey;
				BuyandSendItemInputParam.szProductCode      = pBillingInfo->szProductCode;
				BuyandSendItemInputParam.iItemPrice			= pBillingInfo->iItemPrice;
				int iPresentIdx = -1;
				int iRetError = -1;
				iRetError = pGameODBC->ITEM_BuyAndSendBindAccountItem(BuyandSendItemInputParam, iPresentIdx);
				if( ODBC_RETURN_SUCCESS == iRetError )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;					
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;
					CFSGameServer* pServer = CFSGameServer::GetInstance();
					if(pServer != NULL)
					{
						CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
						if(pCenter != NULL) 
						{
							char* szRecvGameID = pBillingInfo->szRecvGameID;
							pCenter->SendUserNotify(szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR);				
						}
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyAndSendBindAccountItem - ItemCode:%d, UserID:%s, GameID:%s", pBillingInfo->iItemCode, pBillingInfo->szUserID, pBillingInfo->szGameID);
					if ( iRetError == -5 )
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
					}
					else
					{
						iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
					}
				}	
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

			SetUserBillResultAtMem(pUser, pGameODBC, pBillingInfo->iSellType, pBillingInfo->iCurrentCash -pBillingInfo->iCashChange, pBillingInfo->iPointChange, pBillingInfo->iTrophyChange, pBillingInfo->iBonusCoin );
		}
		break;
	case PAY_RESULT_FAIL_CASH:
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default:
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(pBillingInfo->iItemCode);
	if(BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode)
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if(BUY_ITEM_ERROR_SUCCESS <= iErrorCode)
	{
		pUser->SendUserStat();		
		pUser->SendAllMyOwnItemInfo();
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();

	}

	return (iErrorCode >= BUY_ITEM_ERROR_SUCCESS);	
}

BOOL CFSGameUserItem::BuyResetRecordItem_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyResetRecordItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int	iEventBonusCash = 0;

	SBuyItemInputParam BuyItemInputParam;
	BuyItemInputParam.iUserIDIndex = pBillingInfo->iSerialNum;
	BuyItemInputParam.iGameIDIndex = pUser->GetGameIDIndex();
	BuyItemInputParam.iItemCode = pBillingInfo->iItemCode;
	BuyItemInputParam.iBuyType = pBillingInfo->iSellType;
	BuyItemInputParam.iBuyPointPrice = pBillingInfo->iPointChange;
	BuyItemInputParam.iPrevMoney = pBillingInfo->iCurrentCash;
	BuyItemInputParam.iPostMoney = pBillingInfo->iCurrentCash - pBillingInfo->iCashChange;
	BuyItemInputParam.szBillingKey= pBillingInfo->szBillingKey;
	BuyItemInputParam.iPropertyKind = pBillingInfo->iParam0;
	BuyItemInputParam.szProductCode= pBillingInfo->szProductCode;
	int iOperationCode = pBillingInfo->iOperationCode;
	BuyItemInputParam.iItemPrice = pBillingInfo->iItemPrice;

	int iPacketOperationCode = FS_ITEM_OP_BUY;
	if(TRUE == pBillingInfo->bSaleRandomItemEvent)
	{
		iPacketOperationCode = FS_ITEM_OP_BUY_SALE_RANDOMITEM;
	}

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
			if(NULL == pGameODBC)
			{
				iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
				return FALSE;
			}
			iErrorCode =  pGameODBC->RESET_UserMatchRecord( BuyItemInputParam , iOperationCode);
			if( ODBC_RETURN_SUCCESS == iErrorCode )
			{	
				iErrorCode = BUY_ITEM_ERROR_SUCCESS;

				SAvatarInfo AvatarInfo;
				if( FALSE == pUser->CurUsedAvatarSnapShot( AvatarInfo ) ) break;

				CFSGameServer* pServer = CFSGameServer::GetInstance();
				if(NULL == pServer) break;
				BYTE btAvatarLvGrade = pUser->GetCurUsedAvtarLvGrade();
				int iRankType = GetRankTypeByPropertyKind(BuyItemInputParam.iPropertyKind);

				RANK.ResetAvatarRankInTopRank(AvatarInfo.iGameIDIndex, iRankType, btAvatarLvGrade);

				pUser->ResetRecordAndRank(iRankType, GetCurrentSeasonIndex());
			}
			else if( ODBC_RETURN_FAIL == iErrorCode )
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "RESET_UserMatchRecord 1 - ItemCode:%d, UserID:%s, GameID:%s, iErrorCode : %d", pBillingInfo->iItemCode, pBillingInfo->szUserID, pBillingInfo->szGameID, iErrorCode);

				iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
				CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
				PacketComposer.Add(iPacketOperationCode);
				PacketComposer.Add(iErrorCode);
				PacketComposer.Add(pBillingInfo->iItemCode);
				int iItemCodenext = -1;
				PacketComposer.Add(iItemCodenext);
				pClient->Send(&PacketComposer);
				return FALSE;
			}
			else
			{
				WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "RESET_UserMatchRecord 2 - ItemCode:%d, UserID:%s, GameID:%s, iErrorCode : %d", pBillingInfo->iItemCode, pBillingInfo->szUserID, pBillingInfo->szGameID, iErrorCode);

				iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
				return FALSE;
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

			SetUserBillResultAtMem( pUser, pGameODBC, pBillingInfo->iSellType, BuyItemInputParam.iPostMoney, pBillingInfo->iPointChange, pBillingInfo->iTrophyChange, pBillingInfo->iBonusCoin );
		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default:
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(pBillingInfo->iItemCode);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);
	pClient->Send(&PacketComposer);

	if( BUY_ITEM_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserStat();		
		pUser->SendAllMyOwnItemInfo();
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();
	}

	return (iErrorCode >= BUY_ITEM_ERROR_SUCCESS);	
}

int CFSGameUserItem::GetRankTypeByPropertyKind(int iPropertyKind)
{
	int iRankType = -1;
	switch(iPropertyKind)
	{
	case RESET_SEASON_RECORD_ITEM_KIND_NUM:
		iRankType = RANK_TYPE_SEASON;
		break;
	case RESET_TOTAL_RECORD_ITEM_KIND_NUM:
		iRankType = RANK_TYPE_TOTAL;
		break;
	}
	return iRankType;
}

BOOL CFSGameUserItem::BuyItemProcess_CheckHacking(CFSItemShop* pItemShop, CFSLogODBC* pLogODBC, CFSLoginGlobalODBC* pLoginODBC, SShopItemInfo ShopItemInfo,int iRecvUserLevel, int iPropertyValue, int iPropertyAssignType, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT])
{
	BOOL bReturnValue = TRUE;
	int iPropertyNumber = 0;

	if (0 == ShopItemInfo.iUseOption &&
		ITEM_PROPERTY_KIND_PREMIUN_CHALLENGE_TICKET != ShopItemInfo.iPropertyKind &&
		ITEM_PROPERTY_KIND_RANDOMITEM_BOARD_TICKET != ShopItemInfo.iPropertyKind &&
		ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_NORMAL_COIN != ShopItemInfo.iPropertyKind &&
		ITEM_PROPERTY_KIND_RANDOMITEM_GROUP_PREMIUM_COIN != ShopItemInfo.iPropertyKind && 
		ITEM_PROPERTY_KIND_PACKAGE_DARKMARKET != ShopItemInfo.iPropertyKind &&
		(ITEM_PROPERTY_KIND_SALE_COUPON_MIN > ShopItemInfo.iPropertyKind || ITEM_PROPERTY_KIND_SALE_COUPON_MAX < ShopItemInfo.iPropertyKind) &&
		(MIN_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM > ShopItemInfo.iPropertyKind || MAX_ITEM_PROPERTY_KIND_EVENT_PACKAGE_ITEM < ShopItemInfo.iPropertyKind))
	{
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_ITEM_BUYING, m_pUser, "BuyItem\tInvalid UseOption");
		return FALSE;
	}

	BOOL iRet = 0;
	iRet = BuyItemProcess_CheckPropertyIndexNumber(pItemShop,ShopItemInfo.iPropertyKind,iRecvUserLevel,iPropertyAssignType,aiPropertyIndex,iPropertyNumber);
	if (iRet == FALSE)
	{
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_ITEM_BUYING, m_pUser, "BuyItem\tKind=%d\tAssignType=%d\tPropertyIndex=%d:%d:%d:%d:%d:%d", 
			ShopItemInfo.iSmallKind, iPropertyAssignType, 
			aiPropertyIndex[ITEM_PROPERTYINDEX_0], aiPropertyIndex[ITEM_PROPERTYINDEX_1], aiPropertyIndex[ITEM_PROPERTYINDEX_2], aiPropertyIndex[ITEM_PROPERTYINDEX_3], aiPropertyIndex[ITEM_PROPERTYINDEX_4], aiPropertyIndex[ITEM_PROPERTYINDEX_5]);
		return FALSE;
	}

	iRet = BuyItemProcess_CheckPropertyIndex(pItemShop,ShopItemInfo.iPropertyKind,iRecvUserLevel,iPropertyAssignType,iPropertyNumber,aiPropertyIndex);
	if (iRet == FALSE)
	{
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_ITEM_BUYING, m_pUser, "BuyItem\tKind=%d\tPropertyIndex=%d:%d:%d:%d:%d:%d", 
			ShopItemInfo.iSmallKind, 
			aiPropertyIndex[ITEM_PROPERTYINDEX_0], aiPropertyIndex[ITEM_PROPERTYINDEX_1], aiPropertyIndex[ITEM_PROPERTYINDEX_2], aiPropertyIndex[ITEM_PROPERTYINDEX_3], aiPropertyIndex[ITEM_PROPERTYINDEX_4], aiPropertyIndex[ITEM_PROPERTYINDEX_5]);
		return FALSE;
	}

	if ( FALSE == BuyItemProcess_CheckItemPropertyValue( pItemShop, ShopItemInfo, iPropertyValue))
	{
		HACKINGMANAGER.TakeActionAgainstHack(HACK_TYPE_ITEM_BUYING, m_pUser, "BuyItem\tKind=%d\tPropertyKind=%d\tPropertyValue=%d",
			ShopItemInfo.iSmallKind, ShopItemInfo.iPropertyKind, iPropertyValue);
		return FALSE;
	}

	return TRUE;
}


BOOL CFSGameUserItem::BuyItemProcess_CheckPropertyIndexNumber(CFSItemShop* pItemShop, int iPropertyKind,int iRecvUserLevel, int iPropertyAssignType, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] , int& iPropertyNumber)
{

	int iPropertyIndexCount = 0 ; 

	for (int i = 0 ; i < MAX_ITEM_PROPERTYINDEX_COUNT ; i++)
	{
		if(aiPropertyIndex[i] != -1)
		{
			++iPropertyIndexCount;
		}
	}

	if (iPropertyIndexCount == 0)
	{
		iPropertyNumber = 0;
		return TRUE;
	}

	CItemPropertyBoxList* pItemPropertyBoxList = NULL;
	pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(iPropertyKind);

	if (pItemPropertyBoxList == NULL)
	{
		return FALSE;
	}

	int iMaxBoxIdx = 0 ;
	iMaxBoxIdx = pItemPropertyBoxList->GetMaxBoxIdxCount(iRecvUserLevel,iPropertyAssignType);
	
	if(iPropertyIndexCount != iMaxBoxIdx)
	{
		return FALSE;
	}

	iPropertyNumber = iMaxBoxIdx;

	return TRUE;
}
BOOL CFSGameUserItem::BuyItemProcess_CheckPropertyIndex(CFSItemShop* pItemShop,int iPropertyKind,int iRecvUserLevel,int iPropertyAssignType,int iPropertyNumber, int aiPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT])
{
	CItemPropertyBoxList* pItemPropertyBoxList = NULL;

	if (iPropertyNumber == 0)
	{
		return TRUE;
	}

	pItemPropertyBoxList = pItemShop->GetItemPropertyBoxList(iPropertyKind);

	if (pItemPropertyBoxList == NULL)
	{
		return FALSE;
	}

	BOOL bIsUsePropertyCategory = TRUE;
	if (GAME_LEAGUE_UP_PRO > iRecvUserLevel)	//제한 렙 보다 작으면 능력치 부위당 1개씩 붙일수 있음 , 세트의 경우는 카테고리 구분이 없음
	{
		bIsUsePropertyCategory = FALSE;
	}

	int iCategory = 0;
	SItemPropertyBox* pItemPropertyBox = NULL;
	for (int i = 0 ; i < iPropertyNumber ; i++)
	{
		if (i == 0 || 
			FALSE == bIsUsePropertyCategory ||
			(PROPERTY_ASSIGN_TYPE_CHANNEL == iPropertyAssignType && 0 == i % 2))	//세트2아템에 개별 능력치 부여시 채널별 선능력치(0,2,4) 검사위해서 .. 부위별 검사는 어케하냐???
		{
			pItemPropertyBox = pItemPropertyBoxList->GetItemPropertyBoxWithIndex(iPropertyAssignType,aiPropertyIndex[i] ,iRecvUserLevel);
		}
		else
		{
			pItemPropertyBox = pItemPropertyBoxList->GetItemPropertyBoxWithIndexAndCategory(aiPropertyIndex[i],iCategory,iRecvUserLevel);
		}

		if (pItemPropertyBox == NULL)
		{
			return FALSE; // 해킹 감지
		}
		iCategory = pItemPropertyBox->iCategory; // 널 검사를 하고 나서 넣어야 한다.
	}


	for ( int i = 0; i < iPropertyNumber && i < MAX_ITEM_PROPERTYINDEX_COUNT; ++ i )
	{
		if ( FALSE == pItemPropertyBoxList->CheckPropertyIndex( aiPropertyIndex[i], i ) )
		{
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CFSGameUserItem::BuyItemProcess_CheckItemPropertyValue(CFSItemShop *pItemShop, SShopItemInfo& ShopItemInfo, int iPropertyValue)
{
	CHECK_NULL_POINTER_BOOL( pItemShop )
	 	
	// 矜狼몽앴돛야돨잚謹、橄昑잚謹된윱털뙤페꽝鑒돨攣횅昑
	if (ShopItemInfo.iCharacterSlotType > 0 )
	{	// 흔벎槨景喝실��
		if (0 < iPropertyValue)
		{
			return FALSE;
		}
	}
	else
	{
		// 꽝鑒렀鍋털뙤 
		if (0 == ShopItemInfo.iPropertyType && (-1 != iPropertyValue ))
		{
			return FALSE;
		}

		if ( 1 == ShopItemInfo.iPropertyType )
		{
			if ( iPropertyValue < -1 || iPropertyValue == 0)
			{
				return FALSE;
			}

			if ( iPropertyValue > 30*60*24 ) // 畇侶척낀珂쇌角槨죄윈嵐，럽攣苟충뻘唐털뙤돨 
			{
				return FALSE;
			}
		}

		if ( 2 == ShopItemInfo.iPropertyType )
		{	// 몸鑒털뙤
			if ( iPropertyValue <= 0 || iPropertyValue > 10000)
			{
				return FALSE;
			}
		}
	}

	// 꽝鑒茄셥털뙤 
	CItemPriceList* pPriceList = NULL;
	pPriceList = pItemShop->GetItemPriceList( ShopItemInfo.iItemCode0 );
	CHECK_NULL_POINTER_BOOL( pPriceList );
	
	if (ShopItemInfo.iCharacterSlotType > 0 )
	{	// 실�ゲ滂읍喪뿐芹轍獨肪�
		return TRUE;
	}

	// 털뙤橄昑 송목角뤠닸瞳 
	SItemPrice* pPropertyPrice = NULL;
	pPropertyPrice = pPriceList->GetItemPrice(ShopItemInfo.iSellType, iPropertyValue);
	CHECK_NULL_POINTER_BOOL( pPropertyPrice )
	
	return TRUE;
}

BOOL CFSGameUserItem::CheckSpecialTeamIndexCondition(const int iAvatarsSpecialTeamIndex, const int iSpecialTeamIndexCondition, const int iUnableSpecialTeamIndexCondition)
{
	BOOL bResult = FALSE;

	if ( iSpecialTeamIndexCondition <= 0 || 
		iSpecialTeamIndexCondition == iAvatarsSpecialTeamIndex) 
	{
		bResult = TRUE;
	}  

	if (0 < iUnableSpecialTeamIndexCondition &&
		iUnableSpecialTeamIndexCondition == iAvatarsSpecialTeamIndex)
	{
		bResult = FALSE;
	}
	
	return bResult;
}

BOOL CFSGameUserItem::CheckSpecialAvatarIndexCondition(const int iAvatarsSpecialAvatarIndex, const int iSpecialAvatarIndexCondition)
{
	BOOL bResult = FALSE;

	if ( iSpecialAvatarIndexCondition <= 0 || iSpecialAvatarIndexCondition == iAvatarsSpecialAvatarIndex ) 
	{
		bResult = TRUE;
	} 

	return bResult;
}

void CFSGameUserItem::ProcessAchievementBuyItem(CFSGameODBC *pFSODBC, CFSGameUser* pUser, CAvatarItemList *pAvatarItemList)
{
	vector<int> vInventoryItemCounts;

	CFSGameServer* pServer = CFSGameServer::GetInstance();	
	CHECK_NULL_POINTER_VOID(pServer);
	CHECK_NULL_POINTER_VOID(pFSODBC);
	CHECK_NULL_POINTER_VOID(pUser);
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	pAvatarItemList->GetInventoryItemCountforAchievement(vInventoryItemCounts);
	pUser->ProcessAchievementbyGroupAndComplete(ACHIEVEMENT_CONDITION_GROUP_INVENTORY, ACHIEVEMENT_LOG_GROUP_SHOP, vInventoryItemCounts);
}

int CFSGameUserItem::GetItemCountWithKind(int iItemBigKind, int iItemSmallKind)
{
	return m_paItemList == NULL ? -1 : m_paItemList->GetItemCountWithKind(iItemBigKind, iItemSmallKind);
}

BOOL CFSGameUserItem::ArrangeItemInfoToAddItem(CFSItemShop* pItemShop, int iCntSupplyItem, SUserItemInfoEx itemList[MAX_ITEMS_IN_PACKAGE])
{
	CHECK_NULL_POINTER_BOOL(pItemShop);
	CHECK_NULL_POINTER_BOOL(m_pUser);
	CHECK_NULL_POINTER_BOOL(m_paItemList);	

	CItemPropertyList* pItemProperty = NULL;
	int iLoopIndex = 0;
	SUserItemInfo UserItemInfo;
	SShopItemInfo ShopItemInfo;
	for (iLoopIndex = 0; iLoopIndex < iCntSupplyItem; iLoopIndex++)
	{
		pItemShop->GetItem( itemList[iLoopIndex].iItemCode, ShopItemInfo );
		const SUserItemInfoEx& UserItemInfoEx = itemList[iLoopIndex];
		memcpy(&UserItemInfo, &UserItemInfoEx, sizeof(SUserItemInfo));

		UserItemInfo.iItemIdx = itemList[iLoopIndex].iItemIdx;
		UserItemInfo.ExpireDate = itemList[iLoopIndex].ExpireDate;
		UserItemInfo.iPropertyKind = itemList[iLoopIndex].iPropertyKind;
		UserItemInfo.iCategory = ShopItemInfo.iCategory;
		UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
		UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
		UserItemInfo.iSellType = ShopItemInfo.iSellType+1;
		UserItemInfo.iChannel = ShopItemInfo.iChannel;					
		UserItemInfo.iStatus = ShopItemInfo.iStatus;
		UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;					
		UserItemInfo.iStatus = ITEM_STATUS_INVENTORY;  

		int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {UserItemInfoEx.iPropertyIndex1,UserItemInfoEx.iPropertyIndex2,UserItemInfoEx.iPropertyIndex3,-1,-1,-1};
		ITEM_PROPERTY_VECTOR vecItemProperty;
		pItemShop->GetItemPropertyList(iaPropertyIndex, vecItemProperty);

		InsertUserItemProperty( m_pUser->GetGameIDIndex(), UserItemInfo.iItemIdx, m_paItemList, vecItemProperty);
		UserItemInfo.iPropertyNum = vecItemProperty.size();
	
		m_paItemList->AddItem( UserItemInfo );
	}						

	return TRUE;
}

BYTE CFSGameUserItem::GetUserPropertyAccItemStatus(int iItemIdx, int iBigKind, int iStatus, int iPropertyKind)
{
	/*CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();

	if( NULL != pAvatarItemList )
	{
		return pAvatarItemList->GetUserPropertyAccItemStatus(iItemIdx, iBigKind, iStatus, iPropertyKind);	
	}*/

	return ACCITEM_STATUS_NOT_CHECKBOX;
}

BYTE CFSGameUserItem::GetDifferentUserPropertyAccItemStatus()
{
	/*CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();

	if( NULL != pAvatarItemList )
	{
		return pAvatarItemList->GetDifferentUserPropertyAccItemStatus();
	}*/

	return ACCITEM_STATUS_NOT_CHECKBOX;
}

void CFSGameUserItem::CheckAndSendChangeDressItem(CPacketComposer& PacketComposer)
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CAvatarItemList* pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iChannel = 0;
	if(NULL != pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SET) ||
		NULL != pAvatarItemList->GetInventoryClubItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SET))
		iChannel |= 1;

	if(NULL != pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_HAIR) ||
		NULL != pAvatarItemList->GetInventoryClubItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_HAIR))
		iChannel |= 4;

	if(NULL != pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_UPPER) ||
		NULL != pAvatarItemList->GetInventoryClubItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_UPPER))
		iChannel |= 8;

	if(NULL != pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_LOWER) ||
		NULL != pAvatarItemList->GetInventoryClubItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_LOWER))
		iChannel |= 16;

	if(NULL != pAvatarItemList->GetInventoryItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SHOES) ||
		NULL != pAvatarItemList->GetInventoryClubItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SHOES))
		iChannel |= 64;

	if(NULL != pAvatarItemList->GetInventoryClubItemWithPropertyKind(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_ACC_BACK))
		iChannel |= 8192;

	PacketComposer.Add(iChannel);

	SUserItemInfo* pItem = pAvatarItemList->GetBasicItem(ITEMCHANNEL_HAIR, pAvatar->iSex, pAvatar->iSpecialCharacterIndex);
	if(pItem != NULL)
	{
		PacketComposer.Add(pItem->iItemCode);
	}
	else
	{
		PacketComposer.Add((int)-1);
	}

	m_pUser->Send(&PacketComposer);
}


BOOL CFSGameUserItem::ResigterChangeDressItem( CFSItemShop* pItemShop, BYTE btIdxCount, BYTE* btChangeDressItemType, int* iaChangeItemCodeList, CPacketComposer& PacketComposer)
{
	CFSGameODBC* pODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pODBC);
	CHECK_NULL_POINTER_BOOL(pItemShop);
	CHECK_NULL_POINTER_BOOL(m_pUser);

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CAvatarItemList* pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	BYTE btResut = 1;
	int iPropertyKind = 0;
	int iaItemIdxList[MAX_CHANGE_DRESS_NUM];
	int iaItemChannelList[MAX_CHANGE_DRESS_NUM];
	memset(iaItemIdxList, -1, sizeof(int)*MAX_CHANGE_DRESS_NUM);
	memset(iaItemChannelList, -1, sizeof(int)*MAX_CHANGE_DRESS_NUM);

	for(BYTE i = 0; i < btIdxCount && i < MAX_CHANGE_DRESS_NUM; i++)
	{
		SShopItemInfo sShopChangeItemInfo;
		if(FALSE == pItemShop->GetItem(iaChangeItemCodeList[i], sShopChangeItemInfo))
		{
			PacketComposer.Add(btResut);
			m_pUser->Send(&PacketComposer);
			return FALSE;
		}

		if(btChangeDressItemType[i] == DRESS_CHANGE_ITEM_TYPE_SET)			iPropertyKind = ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SET;
		else if(btChangeDressItemType[i] == DRESS_CHANGE_ITEM_TYPE_HAIR)	iPropertyKind = ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_HAIR;
		else if(btChangeDressItemType[i] == DRESS_CHANGE_ITEM_TYPE_UPPER)	iPropertyKind = ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_UPPER;
		else if(btChangeDressItemType[i] == DRESS_CHANGE_ITEM_TYPE_LOWER)	iPropertyKind = ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_LOWER;
		else if(btChangeDressItemType[i] == DRESS_CHANGE_ITEM_TYPE_SHOES)	iPropertyKind = ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SHOES;
		else iPropertyKind = sShopChangeItemInfo.iPropertyKind;

		SUserItemInfo* pItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iPropertyKind);
		if(pItem == NULL)
		{
			PacketComposer.Add(btResut);
			m_pUser->Send(&PacketComposer);
			return FALSE;
		}

		if(pItem->iPropertyKind < ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind > ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
		{
			PacketComposer.Add(btResut);
			m_pUser->Send(&PacketComposer);
			return FALSE;
		}

		if((iPropertyKind == ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SET && sShopChangeItemInfo.iSmallKind != ITEM_SMALL_KIND_SET) ||
			(iPropertyKind == ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_HAIR && sShopChangeItemInfo.iSmallKind != ITEM_SMALL_KIND_HAIR) ||
			(iPropertyKind == ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_UPPER && sShopChangeItemInfo.iSmallKind != ITEM_SMALL_KIND_UPPER) ||
			(iPropertyKind == ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_LOWER && sShopChangeItemInfo.iSmallKind != ITEM_SMALL_KIND_LOWER) ||
			(iPropertyKind == ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SHOES && sShopChangeItemInfo.iSmallKind != ITEM_SMALL_KIND_SHOES))
		{
			PacketComposer.Add(btResut);
			m_pUser->Send(&PacketComposer);
			return FALSE;
		}

		if((sShopChangeItemInfo.iSexCondition != pAvatar->iSex) && (sShopChangeItemInfo.iSexCondition != ITEM_SEXCONDITION_UNISEX))
		{
			PacketComposer.Add(btResut);
			m_pUser->Send(&PacketComposer);
			return FALSE;
		}

		iaItemIdxList[i] = pItem->iItemIdx;	
		iaItemChannelList[i] = sShopChangeItemInfo.iChannel;
	}

	int iTotalChannelList = 0;
	for(BYTE i = 0; i < btIdxCount && i < MAX_CHANGE_DRESS_NUM; i++)
		iTotalChannelList |= iaItemChannelList[i];

	for(BYTE i = 0; i < MAX_CHANGE_DRESS_NUM; i++)
	{
		if(-1 == pAvatar->AvatarFeature.ChangeDressFeatureInfo[i])
			continue;

		SShopItemInfo sShopChangeItemInfo;
		if(TRUE == pItemShop->GetItem(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i], sShopChangeItemInfo))
		{
			if(sShopChangeItemInfo.iCategory != 0 &&
				(((iTotalChannelList & pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i]) == 0) ||
				(pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i] == ITEMCHANNEL_BACK_ACC)))
			{
				iaChangeItemCodeList[btIdxCount] = pAvatar->AvatarFeature.ChangeDressFeatureInfo[i];
				iaItemIdxList[btIdxCount] = pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[i];
				iaItemChannelList[btIdxCount] = pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i];

				btIdxCount++;
			}
		}
	}

	if(ODBC_RETURN_SUCCESS != pODBC->ITEM_RegisterChangeDressItem(pAvatar->iGameIDIndex, iaItemIdxList, iaChangeItemCodeList))
	{
		PacketComposer.Add(btResut);
		m_pUser->Send(&PacketComposer);
		return FALSE;
	}

	int iRemovePropertyKind = ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_SET;
	for(BYTE i = 0; i < MAX_CHANGE_DRESS_NUM; i++)
	{
		vector<SUserItemInfo*> vItemInfo;
		pAvatarItemList->GetInventoryItemListWithPropertyKind(iRemovePropertyKind, vItemInfo);
		for(BYTE j = 0; j < vItemInfo.size(); j++)
		{
			if(vItemInfo[j])
				pAvatarItemList->ChangeItemStatus(vItemInfo[j]->iItemIdx, 1);
		}
		iRemovePropertyKind++;
	}

	for(BYTE i = 0; i < btIdxCount && i < MAX_CHANGE_DRESS_NUM; i++)
	{
		SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iaItemIdxList[i]);
		if(pItem != NULL)
		{
			pItem->iItemCode	= iaChangeItemCodeList[i];
			pItem->iChannel		= iaItemChannelList[i];
			pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, 2);
		}
	}

	memset(m_UseItem.iaChangeDressFeature, -1, sizeof(int)*MAX_CHANGE_DRESS_NUM);
	m_UseItem.iChangeDressChannel = 0;
	for(BYTE i = 0; i < btIdxCount && i < MAX_CHANGE_DRESS_NUM; i++)
	{
		m_UseItem.iaChangeDressFeature[i] = iaChangeItemCodeList[i];
		m_UseItem.iaChangeDressItemIdx[i] = iaItemIdxList[i];
		m_UseItem.iChangeDressChannel |= iaItemChannelList[i];
	}

	memcpy(pAvatar->AvatarFeature.ChangeDressFeatureInfo, iaChangeItemCodeList, sizeof(int)*MAX_CHANGE_DRESS_NUM);
	memcpy(pAvatar->AvatarFeature.ChangeDressFeatureItemIndex, iaItemIdxList, sizeof(int)*MAX_CHANGE_DRESS_NUM);
	memcpy(pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel, iaItemChannelList, sizeof(int)*MAX_CHANGE_DRESS_NUM);
	pAvatar->AvatarFeature.iTotalChangeDressChannel = m_UseItem.iChangeDressChannel;

	btResut = 0;
	PacketComposer.Add(btResut);
	m_pUser->Send(&PacketComposer);

	m_pUser->SendAvatarInfo();		

	return TRUE;
}

BOOL CFSGameUserItem::BuyPackageItem_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyPackageItem_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	CFSGameServer* pServer = CFSGameServer::GetInstance();	
	CHECK_NULL_POINTER_BOOL(pServer);

	CAvatarItemList *pAvatarItemList = pUser->GetCurAvatarItemListWithGameID(pUser->GetGameID());
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx( pUser->GetCurAvatarIdx());
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	char* szRecvGameID = pBillingInfo->szGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemCode = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;	
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;
	char* szProductCode = pBillingInfo->szProductCode;
	BOOL bIsLocalPublisher = pBillingInfo->bIsLocalPublisher;
	
	int iPacketOperationCode = FS_ITEM_OP_BUY;
	BYTE btPackageItemBuyType = PACKAGEITEM_BUY_TYPE_BUY;
	if(bIsSendItem == TRUE)
	{
		iPacketOperationCode = FS_ITEM_OP_SEND;
		btPackageItemBuyType = PACKAGEITEM_BUY_TYPE_SEND;

		szRecvGameID = pBillingInfo->szRecvGameID;
	}

	int iItemPrice = iBuyPriceType == SELL_TYPE_TROPHY ? iItemPriceTrophy : iItemPricePoint;

	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			int iPoint = 0;
			int iPresentIdx[MAX_PACKAGE_ITEM_COMPONET];
			memset(&iPresentIdx, -1, sizeof(int) * MAX_PACKAGE_ITEM_COMPONET);
			BYTE btMailType;

			if(ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyAndSendPackageItem(btPackageItemBuyType, iUserIDIndex, iGameIDIndex, szGameID, szRecvGameID,
					iItemCode, iBuyPriceType, iItemPrice, iPrevMoney, iPostMoney, szBillingKey, szProductCode, szText, pBillingInfo->iItemPrice, iPoint, iPresentIdx, iErrorCode, btMailType))
			{
				iErrorCode = BUY_ITEM_ERROR_SUCCESS; 
				if(btPackageItemBuyType == PACKAGEITEM_BUY_TYPE_BUY)	
				{
					for(int i = 0; i < MAX_PACKAGE_ITEM_COMPONET; i++)
					{
						if(iPresentIdx[i] > -1)
						{
							pUser->RecvPresent(btMailType, iPresentIdx[i]);
						}
					}

					if(iPoint > 0)
						pUser->AddSkillPoint(iPoint);
				}
				else
				{
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;

					CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
					if(pCenter != NULL) 
					{
						for(int i = 0; i < MAX_PACKAGE_ITEM_COMPONET; i++)
						{
							if(iPresentIdx[i] > -1)
								pCenter->SendUserNotify(szRecvGameID, iPresentIdx[i], btMailType);						
						}
					}
				}				
			}
			else
			{

				if(btPackageItemBuyType == PACKAGEITEM_BUY_TYPE_BUY)
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyAndSendPackageItem - ItemCode:%d, UserID:%s, GameID:%s", iItemCode, szUserID, szGameID);
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyAndSendPackageItem - ItemCode:%d, UserID:%s, GameID:%s, RecvGameID:%s", iItemCode, szUserID, szGameID, szRecvGameID);
					iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
				}

				if(iErrorCode != -6 )
				{
					iErrorCode = -4;
				}
				else
				{
					iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
				}
			}

			if (BUY_ITEM_ERROR_SUCCESS == iErrorCode)
				SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
		}
		break;
	case PAY_RESULT_FAIL_CASH:
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default:
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);		
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pUser->Send(&PacketComposer);

	if(BUY_SKILL_ERROR_SUCCESS <= iErrorCode)
	{
		pUser->SendUserStat();		
		pUser->SendAllMyOwnItemInfo();
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();

		ProcessAchievementBuyItem(pGameODBC, pUser, pAvatarItemList);
	}

	return (BUY_SKILL_ERROR_SUCCESS <= iErrorCode);	
}

void CFSGameUserItem::RemoveClubItem()
{
	SAvatarInfo* pAvatar = m_pUser->GetAvatarInfoWithIdx(m_pUser->GetCurAvatarIdx());
	CHECK_NULL_POINTER_VOID(pAvatar);

	CAvatarItemList* pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	vector<int> vItemIdx;
	pAvatarItemList->GetClubItemList(vItemIdx);
	for(int i = 0; i < vItemIdx.size(); ++i)
	{
		SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(vItemIdx[i]);
		if(pItem)
		{
			if(pItem->iStatus == 2)
			{
				for(int iSlot = 0; iSlot < MAX_CHANGE_DRESS_NUM; iSlot++)
				{
					if(-1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[iSlot])
					{
						if(pAvatar->AvatarFeature.ChangeDressFeatureInfo[iSlot] == pItem->iItemCode)
						{
							pAvatar->AvatarFeature.ChangeDressFeatureInfo[iSlot] = -1;
							pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[iSlot] = -1;
							pAvatar->AvatarFeature.iTotalChangeDressChannel ^= pItem->iChannel;
						}
					}
				}
			}
			pAvatarItemList->RemoveItem(pItem->iItemIdx);
		}
	}
};

int CFSGameUserItem::GetSkillTicketCount()
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	int iTicketCount = pAvatarItemList->GetItemCountWithPropertyKind(SKILL_TICKET_PROPERTYKIND);

	return iTicketCount;
}

BOOL CFSGameUserItem::RemoveSkillTicket( int iTicketInventoryIndex )
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	if(pAvatarItemList->RemoveSkillTicket(iTicketInventoryIndex))
	{
		return TRUE;
	}

	return FALSE;
}

void CFSGameUserItem::GetVoiceUserSettingInfo( vector<SVoiceUserSettingInfo*>& vVoiceUserSettingInfo, int iItemIndex /* = -1 */, int iVoiceActionCode /* = VOICE_ACTION_CODE_DEFAULT */ )
{
	if ( m_VoiceUserSettingInfo.size() <= 0 )
	{
		return;
	}

	vector<SVoiceUserSettingInfo*>::iterator iter = m_VoiceUserSettingInfo.begin();
	for ( ; iter != m_VoiceUserSettingInfo.end(); iter++ )
	{
		SVoiceUserSettingInfo* pInfo = *iter;
		if ( pInfo && ( iItemIndex == -1 || pInfo->iItemIndex == iItemIndex ) && pInfo->iActionCode != -1 
			&& ( pInfo->iActionCode == iVoiceActionCode || iVoiceActionCode == VOICE_ACTION_CODE_DEFAULT) )
		{
			vVoiceUserSettingInfo.push_back( pInfo );
		}
	}
}

void CFSGameUserItem::SetVoiceInfo( int iItemIndex, int iVoiceCode, int iVoiceIndex, int iVoiceActionCode )
{
	vector<SVoiceUserSettingInfo*>::iterator iter = m_VoiceUserSettingInfo.begin();
	for ( ; iter != m_VoiceUserSettingInfo.end(); iter++ )
	{
		SVoiceUserSettingInfo* pInfo = *iter;
		if ( pInfo && pInfo->iItemIndex == iItemIndex )
		{
			pInfo->iVoiceIndex = iVoiceIndex;
			pInfo->iActionCode = iVoiceActionCode;
			return;
		}
	}

	SVoiceUserSettingInfo* pSettingInfo;
	allocData( pSettingInfo );

	pSettingInfo->iItemIndex = iItemIndex;
	pSettingInfo->iVoiceCode = iVoiceCode;
	pSettingInfo->iVoiceIndex = iVoiceIndex;
	pSettingInfo->iActionCode = iVoiceActionCode;

	m_VoiceUserSettingInfo.push_back( pSettingInfo );
}

BOOL CFSGameUserItem::DeleteVoiceSettingInfo( int iItemIndex )
{

	vector<SVoiceUserSettingInfo*>::iterator iter = m_VoiceUserSettingInfo.begin();
	for ( ; iter != m_VoiceUserSettingInfo.end(); iter++ )
	{
		SVoiceUserSettingInfo* pInfo = *iter;
		if ( pInfo && pInfo->iItemIndex == iItemIndex )
		{
			m_VoiceUserSettingInfo.erase( iter );
			return TRUE;
		}
	}
	return FALSE;
}

void CFSGameUserItem::FeatureInfoInitilaize()
{
	CHECK_NULL_POINTER_VOID(m_pUser);

	SAvatarInfo* pAvatar = m_pUser->GetAvatarInfoWithIdx(m_pUser->GetCurAvatarIdx());
	CHECK_NULL_POINTER_VOID(pAvatar);

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	m_UseItem.iFace = pAvatar->iFace;
	m_UseItem.iBodyShape = pAvatar->iCharacterType;

	//전체 아이템 상태초기화
	pAvatarItemList->ChangeItemStatusAll(ITEM_STATUS_INVENTORY);

	//외형변경 아이템 상태 갱신
	for(int i=0;i<MAX_CHANGE_DRESS_NUM;i++)
	{
		if(-1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[i])
		{
			SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemCodeInAll(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i]);
			if(pItem)
			{	
				pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[i] = pItem->iItemIdx;
				pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[i] = pItem->iChannel;
				pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
			}
		}
	}

	m_UseItem.iChangeDressChannel = pAvatar->AvatarFeature.iTotalChangeDressChannel;
	memcpy(m_UseItem.iaChangeDressFeature, pAvatar->AvatarFeature.ChangeDressFeatureInfo, sizeof(int)*MAX_CHANGE_DRESS_NUM);
	memcpy(m_UseItem.iaChangeDressItemIdx, pAvatar->AvatarFeature.ChangeDressFeatureItemIndex, sizeof(int)*MAX_CHANGE_DRESS_NUM);

	//현재 스타일 가져오고
	SAvatarStyleInfo* pStyleInfo = m_pUser->GetStyleInfo();
	CHECK_NULL_POINTER_VOID(pStyleInfo)

	int iStyleIndex = pStyleInfo->iUseStyle - 1;
	CHECK_CONDITION_RETURN_VOID(0 > iStyleIndex || MAX_STYLE_COUNT <= iStyleIndex);

	int iFeatureItemIdx = 0;
	int iChangeDressFeatureItemIdx = 0;
	BOOL iaIsItemEquiped[MAX_ITEMCHANNEL_NUM] = {FALSE,};

	//일반스타일아이템 상태 갱신
	pAvatar->AvatarFeature.InitializeFeatureItem();
//	pAvatar->AvatarFeature.InitializeChangeDressItem();
	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != pStyleInfo->iItemIdx[iStyleIndex][i] )
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(pStyleInfo->iItemIdx[iStyleIndex][i]);
			if (pItem && ITEM_STATUS_INVENTORY == pItem->iStatus)
			{
				if(ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN <= pItem->iPropertyKind && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
				{
					if (MAX_CHANGE_DRESS_NUM <= iChangeDressFeatureItemIdx) continue;

					pAvatar->AvatarFeature.ChangeDressFeatureItemIndex[iChangeDressFeatureItemIdx] = pItem->iItemIdx;
					pAvatar->AvatarFeature.ChangeDressFeatureInfo[iChangeDressFeatureItemIdx] = pItem->iItemCode;
					pAvatar->AvatarFeature.ChangeDressFeatureProtertyChannel[iChangeDressFeatureItemIdx] = pItem->iChannel;
					pAvatarItemList->ChangeItemStatus(pItem, ITEM_STATUS_WEAR);

					++iChangeDressFeatureItemIdx;
				}
				else
				{
					if (MAX_ITEMCHANNEL_NUM <= iFeatureItemIdx) continue;

					pAvatar->AvatarFeature.FeatureItemIndex[iFeatureItemIdx] = pItem->iItemIdx;
					pAvatar->AvatarFeature.FeatureInfo[iFeatureItemIdx] = pItem->iItemCode;
					pAvatar->AvatarFeature.FeatureProtertyChannel[iFeatureItemIdx] = pItem->iChannel;
					pAvatar->AvatarFeature.FeatureProtertyType[iFeatureItemIdx] = pItem->iPropertyType;
					pAvatar->AvatarFeature.FeatureProtertyKind[iFeatureItemIdx] = pItem->iPropertyKind;
					pAvatar->AvatarFeature.FeatureProtertyValue[iFeatureItemIdx] = pItem->iPropertyTypeValue;

					pAvatarItemList->ChangeItemStatus(pItem , ITEM_STATUS_WEAR);
					pAvatar->AvatarFeature.iTotalChannel |= pItem->iChannel;

					iaIsItemEquiped[iFeatureItemIdx] = TRUE;

					++iFeatureItemIdx;
				}
			}
		}
	}

	//기본 아이템 체크하고
	if (TRUE == pAvatar->AvatarFeature.CheckBasicItem())
	{
		//있으면 상태갱신 및 스타일데이터에도 적용시킴
		for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
		{
			if (FALSE == iaIsItemEquiped[i] && -1 < pAvatar->AvatarFeature.FeatureItemIndex[i])
			{
				SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(pAvatar->AvatarFeature.FeatureItemIndex[i]);
				if(pItem)
				{
					pAvatar->AvatarFeature.FeatureInfo[i] = pItem->iItemCode;
					pAvatar->AvatarFeature.FeatureProtertyChannel[i] = pItem->iChannel;
					pAvatar->AvatarFeature.FeatureProtertyType[i] = pItem->iPropertyType;
					pAvatar->AvatarFeature.FeatureProtertyKind[i] = pItem->iPropertyKind;
					pAvatar->AvatarFeature.FeatureProtertyValue[i] = pItem->iPropertyTypeValue;

					pAvatarItemList->ChangeItemStatus(pItem->iItemIdx, ITEM_STATUS_WEAR);
					pAvatar->AvatarFeature.iTotalChannel |= pItem->iChannel;

					pStyleInfo->AddItemIdx(iStyleIndex, pItem->iItemIdx);
				}
			}
		}
	}

	m_UseItem.iChannel = pAvatar->AvatarFeature.iTotalChannel;
	memcpy(m_UseItem.iaFeature, pAvatar->AvatarFeature.FeatureInfo, sizeof(int)*MAX_ITEMCHANNEL_NUM);
	memcpy(m_UseItem.iaItemIdx, pAvatar->AvatarFeature.FeatureItemIndex, sizeof(int)*MAX_ITEMCHANNEL_NUM);

	m_UseItem.iChangeDressChannel = pAvatar->AvatarFeature.iTotalChangeDressChannel;
	memcpy(m_UseItem.iaChangeDressFeature , pAvatar->AvatarFeature.ChangeDressFeatureInfo, sizeof(int)*MAX_CHANGE_DRESS_NUM);

	int iFace = AVATARCREATEMANAGER.GetMultipleFaceCharacterFace(pAvatar->iSpecialCharacterIndex, pAvatar->AvatarFeature.GetFaceItemCode(pAvatar->iSpecialCharacterIndex), iStyleIndex);
	if(iFace != -1)
	{
		pAvatar->iFace = iFace;
		m_pUser->GetUserItemList()->SetTempFace(iFace);
	}
}

void CFSGameUserItem::ChangeUseTryItem(int iStyleIndex)
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	SAvatarStyleInfo* pStyleInfo = m_pUser->GetStyleInfo();
	CHECK_NULL_POINTER_VOID(pStyleInfo);

	CHECK_CONDITION_RETURN_VOID(FALSE == pStyleInfo->IsValidStyleIndex(iStyleIndex));

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);

	int iFace = AVATARCREATEMANAGER.GetMultipleCharacterFace(pAvatar->iSpecialCharacterIndex, iStyleIndex-1);
	if (-1 == iFace)
	{
		iFace = m_UseTryItem.iFace;
	}

	int iBodyShape = m_UseTryItem.iBodyShape;

	m_UseTryItem.Initialize();

	iStyleIndex = iStyleIndex - 1;

	int iFeatureItemIdx = 0;
	int iChangeDressFeatureItemIdx = 0;
	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != pStyleInfo->iItemIdx[iStyleIndex][i] )
		{
			SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemIdx(pStyleInfo->iItemIdx[iStyleIndex][i]);
			if (pItem && (ITEM_STATUS_INVENTORY == pItem->iStatus || ITEM_STATUS_WEAR == pItem->iStatus))
			{
				if (ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN <= pItem->iPropertyKind && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
				{
					if (MAX_CHANGE_DRESS_NUM <= iChangeDressFeatureItemIdx) continue;

					m_UseTryItem.iaChangeDressFeature[iChangeDressFeatureItemIdx] = pItem->iItemCode;
					m_UseTryItem.iChangeDressChannel |= pItem->iChannel;

					++iChangeDressFeatureItemIdx;
				}
				else
				{
					if (MAX_ITEMCHANNEL_NUM <= iFeatureItemIdx) continue;

					m_UseTryItem.iaFeature[iFeatureItemIdx] = pItem->iItemCode;
					m_UseTryItem.iChannel |= pItem->iChannel;

					++iFeatureItemIdx;
				}
			}
		}
	}

	m_UseTryItem.iFace = iFace;
	m_UseTryItem.iBodyShape = iBodyShape;

	CheckBasicItemNeed(m_UseTryItem);
}

bool CFSGameUserItem::CheckGetBasicFullCrtUniform()
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return FALSE;

	SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCodeInAll(FULL_COURT_BASIC_UNIFORM_ITEMCODE);
	if( pItem == NULL ) return false;

	return true;
}

void CFSGameUserItem::InsertBasicFullCrtUniform()
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC)
	
	SAvatarInfo *pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);
	
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList);
	
	int iItemCode = FULL_COURT_BASIC_UNIFORM_ITEMCODE; 
	int iItemIdx = 0;
	
	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	if( NULL == pItemShop ) return;
	
	SShopItemInfo ShopItemInfo;
	if( FALSE == pItemShop->GetItem(iItemCode , ShopItemInfo) )  
	{
		return;
	}

	if( ODBC_RETURN_SUCCESS == pGameODBC->SP_InsertFullCrtUniform(pAvatar->iGameIDIndex , iItemCode , iItemIdx) )
	{
		SUserItemInfo UserItemInfo;
		UserItemInfo.SetBaseInfo(ShopItemInfo);
		UserItemInfo.iItemCode = iItemCode;
		UserItemInfo.iItemIdx = iItemIdx;
		UserItemInfo.iSmallKind = ITEM_SMALL_KIND_BASIC_FULL_COURT_UNIFORM;
		UserItemInfo.iSellType = 0; /// 기본유니폼은 type = 0으로 
		UserItemInfo.iStatus = ITEM_STATUS_BASE_UNIFORM;
		UserItemInfo.iPropertyTypeValue = -1;	

		pAvatarItemList->AddItem(UserItemInfo);
		
		CPacketComposer Packet(S2C_NOTICE);  //// 유니폼 지급을 알림
		Packet.Add((int)19);
		Packet.Add((int)4);
		Packet.Add(iItemCode);
		m_pUser->Send(&Packet);
		
		//통합 작업대기 - 5vs5 uniform
		//iFullCourtUniformCode[UNIFORM_TOP] = iItemCode;	
	}	
}

void CFSGameUserItem::RemovePCRoomItem(int iSex)
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	CHECK_NULL_POINTER_VOID(pAvatarItemList );

	pAvatarItemList->RemovePCRoomItem(iSex);

	memcpy(&m_UseItem, &m_OriginalItem, sizeof(SFeatureInfo));

	memcpy( pAvatar->AvatarFeature.FeatureInfo , m_UseItem.iaFeature , sizeof(int)*MAX_ITEMCHANNEL_NUM );
	memcpy( pAvatar->AvatarFeature.FeatureItemIndex , m_UseItem.iaItemIdx , sizeof(int)*MAX_ITEMCHANNEL_NUM );
	pAvatar->AvatarFeature.iTotalChannel = m_UseItem.iChannel;

	for(int i=0; i<MAX_ITEMCHANNEL_NUM; i++)
	{
		if (-1 != m_UseItem.iaItemIdx[i])
		{
			SUserItemInfo* pSlotItem = pAvatarItemList->GetItemWithItemIdx(m_UseItem.iaItemIdx[i]);
			if(NULL != pSlotItem)
			{
				pAvatar->AvatarFeature.FeatureItemIndex[i] = pSlotItem->iItemIdx;
				pAvatar->AvatarFeature.FeatureProtertyType[i] = pSlotItem->iPropertyType;
				pAvatar->AvatarFeature.FeatureProtertyKind[i] = pSlotItem->iPropertyKind;
				pAvatar->AvatarFeature.FeatureProtertyChannel[i] = pSlotItem->iChannel;
				pAvatar->AvatarFeature.FeatureProtertyValue[i] = pSlotItem->iPropertyTypeValue;
	
				pAvatarItemList->ChangeItemStatus(m_UseItem.iaItemIdx[i], ITEM_STATUS_WEAR);
			}
		}
	}

	SAvatarStyleInfo* pAvatarStyleInfo = m_pUser->GetStyleInfo();
	if (NULL != pAvatarStyleInfo)
	{
		pAvatarStyleInfo->SetCurrentStyleItemIdx(m_UseItem);
	}
}

BOOL CFSGameUserItem::CheckUseItem(int iItemCode)
{
	for(int i = 0; i < MAX_ITEMCHANNEL_NUM; ++i)
	{
		if(m_UseItem.iaFeature[i] == iItemCode)
			return TRUE;
	}

	return FALSE;
}

void CFSGameUserItem::UpdatePCRoomItemStat(int iLv)
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	CFSItemShop *pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	if( NULL == pItemShop ) return;

	CPCRoomItemList *pItemList = pItemShop->GetPCRoomItemList();
	if( NULL == pItemList ) return;


	pAvatarItemList->Lock();

	{
		CTypeList< SUserItemInfo* >::POSITION pos = pAvatarItemList->GetHeadPosition();
		CTypeList< SUserItemInfo* >::POSITION pEnd = pAvatarItemList->GetEndPosition();

		while( pos != pEnd )
		{
			SUserItemInfo* pItem = (*pos);

			if( pItem && pItem->iSmallKind == ITEM_SMALL_KIND_PCROOM_NORMAL)
			{
				SPCRoomItem PCRoomItem;
				if( pItemList->GetItem( pItem->iItemCode , PCRoomItem ) )
				{
					if( iLv == GAME_LEAGUE_UP_PRO )
					{
						pAvatarItemList->SetItemPropertyValue((*pos)->iItemCode, (*pos)->iItemIdx, PCRoomItem.sLv16StatValue );
					}
					else if( iLv == GAME_LVUP_BOUND2 )
					{
						pAvatarItemList->SetItemPropertyValue((*pos)->iItemCode, (*pos)->iItemIdx, PCRoomItem.sLv31StatValue );
					}
				}
			}
			pos = pAvatarItemList->GetNextPosition(pos);
		}
	}

	pAvatarItemList->Unlock();
}

bool CFSGameUserItem::CheckUserMemoryItem()
{
	for(int i=0; i<MAX_ITEMCHANNEL_NUM;i++)
	{
		if( -1 != m_UseItem.iaFeature[i] )
		{
			if( m_UseItem.iaItemIdx[i] < MEMORY_ITEM_INDEX_BOUNDARY ) return true;
		}
	}

	return false;
}


void CFSGameUserItem::SetOriginalItem()
{
	if( NULL == m_pUser ) return;

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatar ) return;

	//CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	//if( NULL == pAvatarItemList ) return;

	memcpy( m_OriginalItem.iaFeature , pAvatar->AvatarFeature.FeatureInfo , sizeof(int)*MAX_ITEMCHANNEL_NUM );
	memcpy( m_OriginalItem.iaItemIdx , pAvatar->AvatarFeature.FeatureItemIndex , sizeof(int)*MAX_ITEMCHANNEL_NUM );
	m_OriginalItem.iChannel = pAvatar->AvatarFeature.iTotalChannel;

	//memcpy( m_OriginalItem.iaChangeDressFeature , pAvatar->AvatarFeature.ChangeDressFeatureInfo , sizeof(int)*MAX_CHANGE_DRESS_NUM );
	//m_OriginalItem.iChangeDressChannel = pAvatar->AvatarFeature.iTotalChangeDressChannel;

	//for(int i=0; i<MAX_CHANGE_DRESS_NUM;i++)
	//{
	//	if( -1 != pAvatar->AvatarFeature.ChangeDressFeatureInfo[i] )
	//	{
	//		SUserItemInfo * pItem = pAvatarItemList->GetItemWithItemCode(pAvatar->AvatarFeature.ChangeDressFeatureInfo[i]);
	//		if( NULL == pItem ) return;
	//		m_OriginalItem.iaChangeDressItemIdx[i] = pItem->iItemIdx;
	//	}
	//}

	return;
}

bool CFSGameUserItem::UseOrigianlItem(int iType , SUserItemInfo * pItem)
{
	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return false;

	if( iType == 0 ) // 입기 
	{
		if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
		{
			//for(int i=0;i<MAX_CHANGE_DRESS_NUM;i++)
			//{
			//	if( -1 != m_OriginalItem.iaChangeDressFeature[i] )
			//	{
			//		SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCodeInAll(m_OriginalItem.iaChangeDressFeature[i]);
			//		if( NULL == pSlotItem ) return false;

			//		if( (pSlotItem->iChannel & pItem->iChannel) > 0 )
			//		{
			//			m_OriginalItem.iaChangeDressFeature[i] = -1;
			//			m_OriginalItem.iaChangeDressItemIdx[i] = -1;
			//			m_OriginalItem.iChangeDressChannel ^= pSlotItem->iChannel;
			//		}
			//	}
			//}

			//int iSlot = FindSpaceSlot(m_OriginalItem.iaChangeDressFeature);
			//if( -1 == iSlot )
			//{
			//	return false;
			//}

			//m_OriginalItem.iaChangeDressFeature[iSlot] = pItem->iItemCode;
			//m_OriginalItem.iaChangeDressItemIdx[iSlot] = pItem->iItemIdx;
			//m_OriginalItem.iChangeDressChannel |= pItem->iChannel;
		}
		else
		{
			for(int i=0;i<MAX_ITEMCHANNEL_NUM;i++)
			{
				if( -1 != m_OriginalItem.iaFeature[i] )
				{
					SUserItemInfo * pSlotItem = pAvatarItemList->GetItemWithItemCodeInAll(m_OriginalItem.iaFeature[i]);
					if( NULL == pSlotItem ) return false;

					if( (pSlotItem->iChannel & pItem->iChannel) > 0 )
					{
						m_OriginalItem.iaFeature[i] = -1;
						m_OriginalItem.iaItemIdx[i] = -1;
						m_OriginalItem.iChannel ^= pSlotItem->iChannel;
					}
				}
			}

			int iSlot = FindSpaceSlot(m_OriginalItem.iaFeature);
			if( -1 == iSlot )
			{
				return false;
			}

			m_OriginalItem.iaFeature[iSlot] = pItem->iItemCode;
			m_OriginalItem.iaItemIdx[iSlot] = pItem->iItemIdx;
			m_OriginalItem.iChannel |= pItem->iChannel;
		}
	}
	else //벗기
	{
		if(pItem->iPropertyKind >= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MIN && pItem->iPropertyKind <= ITEM_PROPERTY_KIND_CHANGE_DRESS_ITEM_MAX)
		{
			//int iSlot = FindChangeDressItemSlot(m_OriginalItem.iaChangeDressFeature , pItem->iItemCode);
			//if( -1 == iSlot ) return false;

			//m_OriginalItem.iaChangeDressFeature[iSlot] = -1;
			//m_OriginalItem.iaChangeDressItemIdx[iSlot] = -1;
			//m_OriginalItem.iChangeDressChannel ^= pItem->iChannel;
		}
		else
		{
			int iSlot = FindItemSlot(m_OriginalItem.iaFeature , pItem->iItemCode);
			if( -1 == iSlot ) return false;

			m_OriginalItem.iaFeature[iSlot] = -1;
			m_OriginalItem.iaItemIdx[iSlot] = -1;
			m_OriginalItem.iChannel ^= pItem->iChannel;
		}
	}

	CheckBasicItemNeed(m_OriginalItem, FALSE);
	return true;
}

BOOL CFSGameUserItem::BuyRandomCard_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		g_LogManager.WriteLogToFile("pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC)

	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL(pUser);

	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL(pClient);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	CAvatarItemList* pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;

	BOOL bUseRandomCardTicket = pBillingInfo->bUseRandomCardTicket;
	int iRandomCardTicketItemIndex = pBillingInfo->iParam4;
	int iUseRandomCardTicketItemIndex = iRandomCardTicketItemIndex;
	if(FALSE == bUseRandomCardTicket)
		iUseRandomCardTicketItemIndex = -1;

	int iItemCode = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPrevMoney = pBillingInfo->iCurrentCash;

	if(TRUE == bUseRandomCardTicket)
		pBillingInfo->iCashChange = 0;

	int iItemPriceCash = pBillingInfo->iCashChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;

	int iUpdatedRandomCardTicketCount = 0;

	int iPrice = 0;
	switch(iBuyPriceType)
	{
	case SELL_TYPE_CASH:	iPrice = iItemPriceCash;	break;
	case SELL_TYPE_POINT:	iPrice = iItemPricePoint;	break;
	case SELL_TYPE_TROPHY:	iPrice = iItemPriceTrophy;	break;
	}

	SShopItemInfo ItemInfo;
	if( FALSE == pItemShop->GetItem(iItemCode, ItemInfo))
	{
		iErrorCode = BUY_RANDOMCARD_DB_FAIL;
		WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, INVALED_DATA, CHECK_FAIL, "pItemShop->GetItem ItemCode = %d", iItemCode);			
		return FALSE;
	}

	int iGuaranteeVal;
	BYTE btCategory = RANDOMCARDSHOP.GetRandomCardCategory(ItemInfo.iItemCode0);
	SGuaranteeInfo sGuaranteeInfo;
	if(GUARANTEE_SUCCESS == RANDOMCARDSHOP.GetGuaranteeCardInfo(pUser->GetUserIDIndex(), sGuaranteeInfo))
	{
		pUser->SetGuaranteeCardInfo(sGuaranteeInfo);
		iGuaranteeVal = pUser->GetGuaranteeCardValue(btCategory);
	}
	else 
		iGuaranteeVal = GUARANTEECARD_NOGOT;
	
	SRandomItem RandomItem;
	if(iGuaranteeVal == 1) // 무조건 1등상품 order 1
	{
		RANDOMCARDSHOP.GetRandomItem1st(ItemInfo.iItemCode0, RandomItem);	
	}
	else
	{
		RANDOMCARDSHOP.GetRandomItem(ItemInfo.iItemCode0, RandomItem);
	}

	int iPresentIdx = -1;
	if(RandomItem.btRewardType == RANDOMCARD_REWARD_TYPE_ITEM)
	{
		SShopItemInfo ShopItemInfo;
		if( FALSE == pItemShop->GetItem(RandomItem.iItemCode, ShopItemInfo))
		{
			iErrorCode = BUY_RANDOMCARD_DB_FAIL;
			WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, INVALED_DATA, CHECK_FAIL, "pItemShop->GetItem ItemCode = %d", RandomItem.iItemCode);			
			return FALSE;
		}

		int iSellPrice = RANDOMCARDSHOP.GetRandomCardSellPrice(ItemInfo.iItemCode0);
		int iSupplyItemCode = ShopItemInfo.iItemCode0;

		if( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyRandomCard(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), ItemInfo.iItemCode0,
			RANDOMCARD_REWARD_TYPE_ITEM, iSupplyItemCode, RandomItem.iPropertyTypeValue, iPrevMoney, iPostMoney, iUseRandomCardTicketItemIndex, iPrice, iPresentIdx, iUpdatedRandomCardTicketCount))
		{
			iErrorCode = BUY_RANDOMCARD_SUCCESS;

			int iMailType = (btCategory == 0 && ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_EVENT == ItemInfo.iSmallKind) ? MAIL_PRESENT_ON_ACCOUNT : MAIL_PRESENT_ON_AVATAR;

			pUser->RecvPresent(iMailType,iPresentIdx);
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, DB_DATA_UPDATE, FAIL, "ITEM_BuyRandomCard - ItemCode:%d, UserID:%s, GameID:%s", 
				iItemCode, pUser->GetUserID(), pUser->GetGameID());			
			iErrorCode = BUY_RANDOMCARD_DB_FAIL;

			return FALSE;
		}
	}
	else if(RandomItem.btRewardType == RANDOMCARD_REWARD_TYPE_POINT)
	{
		if( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyRandomCard(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), ItemInfo.iItemCode0,
			RANDOMCARD_REWARD_TYPE_POINT, -1, RandomItem.iPropertyTypeValue, iPrevMoney, iPostMoney, iUseRandomCardTicketItemIndex, iPrice, iPresentIdx, iUpdatedRandomCardTicketCount))
		{
			pUser->SetSkillPoint(pUser->GetSkillPoint() + RandomItem.iPropertyTypeValue);		
			iErrorCode = BUY_RANDOMCARD_SUCCESS;
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, DB_DATA_UPDATE, FAIL, "ITEM_BuyRandomCard(Point) - ItemCode:%d, UserID:%s, GameID:%s", 
				iItemCode, pUser->GetUserID(), pUser->GetGameID());		
			iErrorCode = BUY_RANDOMCARD_DB_FAIL;

			return FALSE;
		}
	}
	else if (RandomItem.btRewardType == RANDOMCARD_REWARD_TYPE_TROPHY)
	{
		if( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyRandomCard(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), ItemInfo.iItemCode0, 
			RANDOMCARD_REWARD_TYPE_TROPHY, RandomItem.iItemCode, RandomItem.iPropertyTypeValue, iPrevMoney, iPostMoney, iUseRandomCardTicketItemIndex, iPrice, iPresentIdx, iUpdatedRandomCardTicketCount))
		{
			pUser->SetUserTrophy(pUser->GetUserTrophy() + RandomItem.iPropertyTypeValue);
			iErrorCode = BUY_RANDOMCARD_SUCCESS;
		}
		else
		{
			WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, DB_DATA_UPDATE, FAIL, "ITEM_BuyRandomCard(Trophy) - ItemCode:%d, UserID:%s, GameID:%s", 
				iItemCode, pUser->GetUserID(), pUser->GetGameID());		
			iErrorCode = BUY_RANDOMCARD_DB_FAIL;

			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}

	if(TRUE == bUseRandomCardTicket)
	{
		if (0 == iUpdatedRandomCardTicketCount)
		{
			pAvatarItemList->RemoveItem(iRandomCardTicketItemIndex);
		}
		else
		{
			pAvatarItemList->UpdateItemCount(iRandomCardTicketItemIndex, iUpdatedRandomCardTicketCount);
		}
	}
	else
	{
		SUserItemInfo* pItem = pAvatarItemList->GetItemWithItemIdx(iRandomCardTicketItemIndex);
		if(pItem)
			iUpdatedRandomCardTicketCount = pItem->iPropertyTypeValue;
	}
	
	int iResult = -1;
	if(GUARANTEECARD_NOGOT != iGuaranteeVal)
	{
		int iGuaranteeUpdateType = ((TRUE == RANDOMCARDSHOP.CheckRandomCard1st(ItemInfo.iItemCode0, RandomItem)) ? GUARANTEE_UPDATE_TYPE_INIT : GUARANTEE_UPDATE_TYPE_BUY);
		pGameODBC->GUARANTEE_UpdateGuaranteeCard(pUser->GetUserIDIndex(), (int)btCategory-1 
			, iGuaranteeUpdateType
			, iResult); // 일등이면 초기화 아니면 카드차감
	}

	int iPreSkillPoint = pUser->GetSkillPoint();
	int iPreEventCoin = pUser->GetEventCoin();

	m_pUser->SetPayCash(iItemPriceCash);
	if(pBillingInfo->bUseEventCoin == TRUE)
		m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

	pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);
	m_pUser->ResetUserData();

	int iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
	int iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
	if(iEventBonusPoint > 0 || iEventBonusCash > 0)
		iErrorCode = BUY_RANDOMCARD_SUCCESS_WITH_BONUSPOINT;

	SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );

	RANDOMCARDSHOP.GetGuaranteeCardInfo(pUser->GetUserIDIndex(), sGuaranteeInfo);
	pUser->SetGuaranteeCardInfo(sGuaranteeInfo);

	iGuaranteeVal = pUser->GetGuaranteeCardValue(btCategory);

	pUser->SendUserGold();
	pUser->SendCurAvatarTrophy();

	CPacketComposer Packet(S2C_EPORTSCARD_BUY_RES);

	Packet.Add(iErrorCode);	
	if(iErrorCode == BUY_RANDOMCARD_SUCCESS_WITH_BONUSPOINT)
	{
		Packet.Add(iEventBonusPoint);	
		Packet.Add(iEventBonusCash);	
	}

	Packet.Add(RandomItem.btRewardType);

	if(RandomItem.btRewardType == RANDOMCARD_REWARD_TYPE_ITEM)
	{
		Packet.Add(RandomItem.iItemCode);
		Packet.Add((BYTE)RandomItem.iPropertyType);
		Packet.Add(RandomItem.iPropertyTypeValue);	
	}
	else
		Packet.Add(RandomItem.iPropertyTypeValue);

	Packet.Add(iGuaranteeVal);

	int iLimit, iGuaranteePrice;
	if(TRUE == RANDOMCARDSHOP.GetGuaranteeCardLimitAndPrice(btCategory-1, iLimit, iGuaranteePrice)) 
		Packet.Add(iGuaranteePrice);
	
	else 
		Packet.Add((int)GUARANTEECARD_NOGOT);

	Packet.Add(iUpdatedRandomCardTicketCount);

	pUser->Send(&Packet);
	pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_EPORTSCARD);

	return TRUE;
}

BOOL CFSGameUserItem::BuyGuaranteeCard_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		g_LogManager.WriteLogToFile("pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC)

	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL(pUser);

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iUserIDIndex = pBillingInfo->iSerialNum;
	int iGameIDIndex = pUser->GetGameIDIndex();

	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	int iGuaranteeCardType = pBillingInfo->iParam1;
	int iLimit = pBillingInfo->iParam2;
	int iHotDealItemCode = pBillingInfo->iParam3;

	CPacketComposer PacketComposer(S2C_GUARANTEE_BUY_RES);

	if(ODBC_RETURN_SUCCESS != pGameODBC->GUARANTEE_BuyGuaranteeCard(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), iGuaranteeCardType, 
		iLimit, iPrevMoney, iPostMoney, SELL_TYPE_CASH, iItemPriceCash,iErrorCode))
	{
		WRITE_LOG_NEW(LOG_TYPE_RANDOMCARD, DB_DATA_UPDATE, FAIL, "GUARANTEE_BuyGuaranteeCard - UserID:%s,   GameID:%s, ErrorCode:%d \n", 
			pUser->GetUserID(), pUser->GetGameID(), iErrorCode);		

		PacketComposer.Add(iErrorCode);	
		pUser->Send(&PacketComposer);

		return FALSE;
	}

	if(iHotDealItemCode > 0)
	{
		int iPropertyValue = 1;
		pUser->HotDealItemCheckAndUpdateLimitCount(iHotDealItemCode, iPropertyValue);
	}

	SetUserBillResultAtMem(pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin);

	pUser->SendUserGold();
	pUser->SendCurAvatarTrophy();

	iErrorCode = BUY_GUARANTEECARD_SUCCESS;

	SGuaranteeInfo sGuaranteeInfo;
	RANDOMCARDSHOP.GetGuaranteeCardInfo(iUserIDIndex, sGuaranteeInfo);
	pUser->SetGuaranteeCardInfo(sGuaranteeInfo);

	int iPreSkillPoint = pUser->GetSkillPoint();
	int iPreEventCoin = pUser->GetEventCoin();

	m_pUser->SetPayCash(iItemPriceCash);
	if(pBillingInfo->bUseEventCoin == TRUE)
		m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

	pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY, pGameODBC);
	m_pUser->ResetUserData();

	int iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
	int iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
	if(iEventBonusPoint > 0 || iEventBonusCash > 0)
		iErrorCode = BUY_GUARANTEECARD_SUCCESS_WITH_BONUSPOINT;

	PacketComposer.Add(iErrorCode);
	if( BUY_GUARANTEECARD_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}

	PacketComposer.Add(iGuaranteeCardType);

	pUser->Send(&PacketComposer);

	return TRUE;
}

BOOL CFSGameUserItem::BuySecretStore_AfterPay( SBillingInfo* pBillingInfo, int iPayResult )
{
	if( FALSE != ::IsBadReadPtr( pBillingInfo, sizeof(SBillingInfo) ) )
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuySecretStore_AfterPay - pBillingInfo has Bad_Pointer \n");
		return false;
	}

	/////////////////////////////////////// 데이터 정리 ///////////////////////////////////////
	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL( pUser );
	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL( pClient );
	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL( pItemShop );
	// 이벤트를 위해 필요
	CFSGameServer* pServer = CFSGameServer::GetInstance();	
	CHECK_NULL_POINTER_BOOL( pServer );
	// 20080929 프리미엄 아이템 사기 - pAvatarItemList와 pAvatar는 프리미엄 아이템 때문에 필요함
	CAvatarItemList *pAvatarItemList = pUser->GetCurAvatarItemListWithGameID( pUser->GetGameID() );
	CHECK_NULL_POINTER_BOOL( pAvatarItemList );
	SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx( pUser->GetCurAvatarIdx()) ;
	CHECK_NULL_POINTER_BOOL( pAvatar );
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL(pGameODBC)

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPropertyValue = pBillingInfo->iTerm;
	int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
	iaPropertyIndex[ITEM_PROPERTYINDEX_0] = pBillingInfo->iParam1;
	iaPropertyIndex[ITEM_PROPERTYINDEX_1] = pBillingInfo->iParam2;
	iaPropertyIndex[ITEM_PROPERTYINDEX_2] = pBillingInfo->iParam3;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_3] = pBillingInfo->iParam4;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_4] = pBillingInfo->iParam5;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_5] = pBillingInfo->iParam6;	
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;	
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;

	int iRenewItemIdx = pBillingInfo->iRenewIndex;

	vector<SUserItemInfo> vUserTempItemInfo;	
	int iItemPropertyPrice = pBillingInfo->iItemPropertyPrice;
	int iItemPropertyType = pBillingInfo->iItemPropertyType;
	int iItemPropertyPrice2 = pBillingInfo->iItemPropertyPrice2;
	int iItemPropertyType2 = pBillingInfo->iItemPropertyType2;
	char* szProductCode = pBillingInfo->szProductCode;
	BOOL bIsLocalPublisher = pBillingInfo->bIsLocalPublisher;

	// 선물에 쓰이는 변수들
	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;
	iPacketOperationCode = FS_ITEM_OP_BUY_SECRETSTOREITEM;


	int iStoreIndex = pBillingInfo->iValue0;

	/////////////////////////////////////// 실제 구입 부분 ///////////////////////////////////////
	switch( iPayResult )
	{
	case PAY_RESULT_SUCCESS :
		{
			SShopItemInfo ShopItemInfo;			
			pItemShop->GetItem(iItemIndex , ShopItemInfo);	

			int iSetItemPropertyKind = -1;
			int iBuyTatooPriceCash = 0;
			int iBuyTatooPricePoint = 0;
			int iBuyOnePriceCash = 0; // 수량제 아이템의 경우 1개.
			int iBuyOnePricePoint = 0; // 수량제 아이템의 경우 1개.
			int iBuyFunctionalItem = 0;

			pItemShop->SetItemPropertyKind( ShopItemInfo.iPropertyKind , iSetItemPropertyKind );
			SetOneUnitItemPrice(pGameODBC, pItemShop,bIsLocalPublisher, ShopItemInfo, iPropertyValue,iItemPriceCash,iItemPricePoint, iBuyOnePriceCash, iBuyOnePricePoint, iErrorCode );

			SUserItemInfo UserItemInfo;
			UserItemInfo.iItemCode = iItemIndex;
			UserItemInfo.iChannel = ShopItemInfo.iChannel;
			UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;		
			UserItemInfo.iPropertyType = ShopItemInfo.iPropertyType;
			UserItemInfo.iPropertyKind = iSetItemPropertyKind;
			UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
			UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
			UserItemInfo.iPropertyTypeValue = iPropertyValue;
			UserItemInfo.iStatus = 1;
			UserItemInfo.iSellType = REFUND_TYPE_POINT;

			UserItemInfo.iSellPrice = GetSellPrice( iBuyOnePriceCash, iBuyOnePricePoint );

			int	iCntSupplyItem = 0;
			SUserItemInfo itemList[MAX_ITEMS_IN_PACKAGE];

			{
				int iPresentIdx = -1;
				int errorcode = 0;
				if( ODBC_RETURN_SUCCESS == pGameODBC->spFSBuySecretStoreItem( iUserIDIndex, iGameIDIndex, ShopItemInfo.iItemCode0, iBuyPriceType, iBuyPriceType,
					iPrevMoney, iPostMoney, UserItemInfo, iaPropertyIndex, iItemPropertyType, iItemPropertyPrice,
					iItemPropertyType2, iItemPropertyPrice2, szBillingKey, szProductCode, iStoreIndex, pBillingInfo->iItemPrice, errorcode, iPresentIdx ) )
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS;					
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;

					pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT ,iPresentIdx);

					if ( SECRETSTOREMANAGER.CheckIsUpdateGrade() )
					{
						pUser->BuySuccessSecretStoreItem(iStoreIndex);
					}
					SetUserBillResultAtMem( pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin );
				}
				else if ( errorcode == -7 )
				{
					iErrorCode = BUY_ITEM_ERROR_PRESENT_LIST_FULL;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSBuySecretStoreItem - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
					iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
				}
			}

			int iPreSkillPoint = pUser->GetSkillPoint();
			int iPreEventCoin = pUser->GetEventCoin();

			m_pUser->SetPayCash(pBillingInfo->iCashChange);
			if(pBillingInfo->bUseEventCoin == TRUE)
				m_pUser->SetPayCash(pBillingInfo->iCashChange + pBillingInfo->iUseEventCoin);

			pUser->CheckEvent(PERFORM_TIME_CASH_ITEMBUY,pGameODBC);

			iEventBonusPoint = pUser->GetSkillPoint() - iPreSkillPoint;
			iEventBonusCash = pUser->GetEventCoin() - iPreEventCoin;
			if(iEventBonusPoint > 0 || iEventBonusCash > 0)
				iErrorCode = BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT;

		}
		break;
	case PAY_RESULT_FAIL_CASH :
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default :
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if( BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode )
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if( BUY_SKILL_ERROR_SUCCESS <= iErrorCode )
	{
		pUser->SendUserStat();		
		pUser->SendAllMyOwnItemInfo();
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();

		ProcessAchievementBuyItem(pGameODBC, pUser, pAvatarItemList);
	}

	return ( BUY_SKILL_ERROR_SUCCESS <= iErrorCode );	
}


int CFSGameUserItem::BuyFactionShopItem(int iItemCode)
{
	CHECK_CONDITION_RETURN(FACTION_RACE_CLOSED == FACTION.GetRaceStatus(), BUY_ITEM_ERROR_DO_NOT_RESPONSE);

	SUserFactionInfo* pUserFaction = m_pUser->GetUserFaction();
	CHECK_CONDITION_RETURN(NULL == pUserFaction, BUY_ITEM_ERROR_DO_NOT_RESPONSE);

	SFactionShopItemConfig* pItem = FACTION.GetFactionShopItem(iItemCode);
	CHECK_CONDITION_RETURN(NULL == pItem, BUY_ITEM_ERROR_DO_NOT_RESPONSE);

	CHECK_CONDITION_RETURN(pUserFaction->_iFactionPoint < pItem->_iPrice, BUY_ITEM_ERROR_NOT_ENOUGH_FACTIONPOINT);

	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, BUY_ITEM_ERROR_PRESENT_LIST_FULL);

	CFSGameServer* pServer = CFSGameServer::GetInstance();		
	if( NULL == pServer ) return BUY_ITEM_ERROR_DO_NOT_RESPONSE;

	CFSItemShop* pItemShop = pServer->GetItemShop();			
	if( NULL == pItemShop ) return BUY_ITEM_ERROR_DO_NOT_RESPONSE;

	SAvatarInfo * pAvatarInfo = m_pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return BUY_ITEM_ERROR_DO_NOT_RESPONSE;

	SShopItemInfo shopItem;
	if(FALSE == pItemShop->GetItem(iItemCode, shopItem)) return BUY_ITEM_ERROR_DO_NOT_RESPONSE;

	if(shopItem.iSexCondition != 2 &&
		shopItem.iSexCondition != pAvatarInfo->iSex)
	{
		return BUY_ITEM_ERROR_MISMATCH_SEX;
	}

	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, BUY_ITEM_ERROR_DO_NOT_RESPONSE);

	int iPresentIdx = 0;
	int	iRet = pODBC->ITEM_BuyFactionShopItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, pItem->_iPropertyValue, pItem->_iPrice, iPresentIdx);
	if (ODBC_RETURN_SUCCESS != iRet)
	{
		if(-1 == iRet)
		{
			return BUY_ITEM_ERROR_LIMIT_BUYING_COUNT;
		}

		WRITE_LOG_NEW(LOG_TYPE_FACTION, EXEC_FUNCTION, FAIL, "BuyFactionShopItem - UserIDIndex:%d, GameIDIndex:%d, ItemCode:%d", m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode);
		return BUY_ITEM_ERROR_GENERAL;
	}

	m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT ,iPresentIdx);

	pUserFaction->DecreaseFactionPoint(pItem->_iPrice);

	SS2C_FACTION_USER_POINT_NOT noticePoint;
	noticePoint.iFactionPoint = pUserFaction->_iFactionPoint;
	m_pUser->Send(S2C_FACTION_USER_POINT_NOT, &noticePoint, sizeof(SS2C_FACTION_USER_POINT_NOT));

	if (pItem->_iUserLimitCount > 0)
	{
		SS2C_FACTION_SHOP_ITEM_UPDATED_NOT notItemCount;
		notItemCount.iItemCode = iItemCode;
		notItemCount.shMyBuyingCount = pUserFaction->IncreaseItemBuyingCount(iItemCode);
		m_pUser->Send(S2C_FACTION_SHOP_ITEM_UPDATED_NOT, &notItemCount, sizeof(SS2C_FACTION_SHOP_ITEM_UPDATED_NOT));
	}

	return BUY_ITEM_ERROR_SUCCESS;
}

BOOL CFSGameUserItem::CheckSeasonResetItem_CloseTime( int iStartTime, int iEndTime )
{
	SYSTEMTIME sysCurrenttime;
	::GetLocalTime(&sysCurrenttime);

	if( sysCurrenttime.wHour >= (iStartTime / 100) && sysCurrenttime.wMinute >= (iStartTime % 100 ))
	{
		if( sysCurrenttime.wHour <= (iEndTime / 100))
		{
			if( sysCurrenttime.wMinute <= (iEndTime % 100 ))
			{
				return TRUE;
			}
		}
	}
	
	return FALSE;
}

BOOL CFSGameUserItem::ChangeCloneCharacterItem( int iSex )
{
	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CAvatarItemList *pAvatarItemList = GetCurAvatarItemList();
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	int iSize = pAvatarItemList->GetSize();

	vector<SUserItemInfo*>	vecItemList;
	vecItemList.reserve(iSize);
	pAvatarItemList->GetAllItemList(vecItemList);

	for( size_t iIndex = 0 ; iIndex < vecItemList.size() ; ++iIndex )
	{
		SUserItemInfo* pUserItem = vecItemList[iIndex];
		if( NULL != pUserItem )
		{
			if( ITEM_SEXCONDITION_UNISEX != pUserItem->iSexCondition && iSex != pUserItem->iSexCondition )
			{
				pAvatarItemList->ChangeItemStatus(pUserItem->iItemIdx, ITEM_STATUS_ONLY_RESELL); 
			}
			else if ( pUserItem->iStatus == ITEM_STATUS_ONLY_RESELL )
			{
				pAvatarItemList->ChangeItemStatus(pUserItem->iItemIdx, ITEM_STATUS_INVENTORY); 
			}
		}
	}

	return TRUE;
}

void CFSGameUserItem::ChangeBasicItem_Sex( int iSex )
{
	CAvatarItemList *pAvatarItemList =  GetCurAvatarItemList();
	if( NULL == pAvatarItemList ) return;

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	BOOL bIsExistBasicItem = FALSE;
	const int iaItemChannel[] = { ITEMCHANNEL_HAIR, ITEMCHANNEL_UP, ITEMCHANNEL_DOWN, ITEMCHANNEL_SHOES, ITEMCHANNEL_FACE_ACC2};
	const int iMaxItemChannelCount = 5;

	for (int i = 0; i < iMaxItemChannelCount; ++i)
	{
		const int iChannel = iaItemChannel[i];
		SUserItemInfo * pItem = pAvatarItemList->GetBasicItem( iChannel, iSex, pAvatar->iSpecialCharacterIndex );

		pAvatar->AvatarFeature.BasicItemInfo[i].iItemCode = -1;
		pAvatar->AvatarFeature.BasicItemInfo[i].iItemChannel = -1;
		pAvatar->AvatarFeature.BasicItemInfo[i].iItemIdx = -1;

		if( NULL != pItem )
		{
			pAvatar->AvatarFeature.BasicItemInfo[i].iItemCode = pItem->iItemCode;
			pAvatar->AvatarFeature.BasicItemInfo[i].iItemChannel = pItem->iChannel;
			pAvatar->AvatarFeature.BasicItemInfo[i].iItemIdx = pItem->iItemIdx;
		}
	}
}

int CFSGameUserItem::GetRandomCardTicketCount(ITEM_SMALL_KIND eSmallKind, int iItemCode)
{
	CAvatarItemList* pAvatarItemList = (CAvatarItemList*)GetCurAvatarItemList();
	CHECK_CONDITION_RETURN(pAvatarItemList == NULL, 0);

	int iPropertyKind;
	if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_NORMAL) || 
		(eSmallKind == ITEM_SMALL_KIND_ETC && iItemCode == DEFAULT_BASIC_CHAR_ITEMCODE) )
		iPropertyKind = ITEM_PROPERTY_KIND_BRONZE_RANDOMCARD_TICKET;
	else if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_SILVER) ||
		(eSmallKind == ITEM_SMALL_KIND_ETC && iItemCode == SKILL_SLOT_ITEMCODE) )
		iPropertyKind = ITEM_PROPERTY_KIND_SILVER_RANDOMCARD_TICKET;
	else if(eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_GOLD)
		iPropertyKind = ITEM_PROPERTY_KIND_GOLD_RANDOMCARD_TICKET;
	else if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_PLATINUM) ||
		(eSmallKind == ITEM_SMALL_KIND_CHAR_SLOT && iItemCode == 52110291))
		iPropertyKind = ITEM_PROPERTY_KIND_PLATINUM_RANDOMCARD_TICKET;
	else if(eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_DIAMOND)
		iPropertyKind = ITEM_PROPERTY_KIND_DIAMOND_RANDOMCARD_TICKET;
	else if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_POINT) ||
		(eSmallKind == ITEM_SMALL_KIND_CHAR_SLOT && iItemCode == 3911981))
		iPropertyKind = ITEM_PROPERTY_KIND_POINT_RANDOMCARD_TICKET;
	else
		return 0;

	SUserItemInfo* pItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iPropertyKind);
	if (NULL != pItem && 
		0 < pItem->iPropertyTypeValue)
		return pItem->iPropertyTypeValue;

	return 0;
}

int CFSGameUserItem::GetRandomCardTicketItemIndex(ITEM_SMALL_KIND eSmallKind, int iItemCode)
{
	CAvatarItemList* pAvatarItemList = (CAvatarItemList*)GetCurAvatarItemList();
	CHECK_CONDITION_RETURN(pAvatarItemList == NULL, -1);

	int iPropertyKind;
	if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_NORMAL) || 
		(eSmallKind == ITEM_SMALL_KIND_ETC && iItemCode == DEFAULT_BASIC_CHAR_ITEMCODE) )
		iPropertyKind = ITEM_PROPERTY_KIND_BRONZE_RANDOMCARD_TICKET;
	else if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_SILVER) ||
		(eSmallKind == ITEM_SMALL_KIND_ETC && iItemCode == SKILL_SLOT_ITEMCODE) )
		iPropertyKind = ITEM_PROPERTY_KIND_SILVER_RANDOMCARD_TICKET;
	else if(eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_GOLD)
		iPropertyKind = ITEM_PROPERTY_KIND_GOLD_RANDOMCARD_TICKET;
	else if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_PLATINUM) ||
		(eSmallKind == ITEM_SMALL_KIND_CHAR_SLOT && iItemCode == 52110291))
		iPropertyKind = ITEM_PROPERTY_KIND_PLATINUM_RANDOMCARD_TICKET;
	else if(eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_DIAMOND)
		iPropertyKind = ITEM_PROPERTY_KIND_DIAMOND_RANDOMCARD_TICKET;
	else if((eSmallKind == ITEM_SMALL_KIND_EPORTSCARD_CATEGORY_POINT) ||
		(eSmallKind == ITEM_SMALL_KIND_CHAR_SLOT && iItemCode == 3911981))
		iPropertyKind = ITEM_PROPERTY_KIND_POINT_RANDOMCARD_TICKET;
	else
		return -1;

	SUserItemInfo* pItem = pAvatarItemList->GetInventoryItemWithPropertyKind(iPropertyKind);
	if (NULL != pItem && 
		0 < pItem->iPropertyTypeValue)
		return pItem->iItemIdx;

	return -1;
}

BOOL CFSGameUserItem::BuyCheerLeader_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_ITEM, INVALED_DATA, CHECK_FAIL, "BuyCheerLeader_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	CFSGameUser* pUser = (CFSGameUser*)pBillingInfo->pUser;
	CHECK_NULL_POINTER_BOOL(pUser);

	CFSGameClient* pClient = (CFSGameClient*)pUser->GetClient();
	CHECK_NULL_POINTER_BOOL(pClient);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_BOOL(pItemShop);

	CFSGameServer* pServer = CFSGameServer::GetInstance();	
	CHECK_NULL_POINTER_BOOL(pServer);

	CAvatarItemList* pAvatarItemList = pUser->GetCurAvatarItemListWithGameID(pUser->GetGameID());
	CHECK_NULL_POINTER_BOOL(pAvatarItemList);

	SAvatarInfo* pAvatar = pUser->GetAvatarInfoWithIdx(pUser->GetCurAvatarIdx());
	CHECK_NULL_POINTER_BOOL(pAvatar);

	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_BOOL( pGameODBC );

	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iEventBonusPoint = 0;
	int iEventBonusCash = 0;

	int iUserIDIndex = pBillingInfo->iSerialNum;
	char* szUserID = pBillingInfo->szUserID;
	char* szGameID = pBillingInfo->szGameID;
	char* szRecvGameID = pBillingInfo->szRecvGameID;
	int iGameIDIndex = pUser->GetGameIDIndex();
	int iItemIndex = pBillingInfo->iItemCode;
	int iBuyPriceType = pBillingInfo->iSellType;
	int iPropertyValue = pBillingInfo->iParam0;
	int iaPropertyIndex[MAX_ITEM_PROPERTYINDEX_COUNT] = {-1, -1, -1, -1, -1, -1};
	iaPropertyIndex[ITEM_PROPERTYINDEX_0] = pBillingInfo->iParam1;
	iaPropertyIndex[ITEM_PROPERTYINDEX_1] = pBillingInfo->iParam2;
	iaPropertyIndex[ITEM_PROPERTYINDEX_2] = pBillingInfo->iParam3;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_3] = pBillingInfo->iParam4;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_4] = pBillingInfo->iParam5;	
	iaPropertyIndex[ITEM_PROPERTYINDEX_5] = pBillingInfo->iParam6;	
	int iPrevMoney = pBillingInfo->iCurrentCash;
	int iItemPriceCash = pBillingInfo->iCashChange;	
	int iItemPricePoint = pBillingInfo->iPointChange;
	int iItemPriceTrophy = pBillingInfo->iTrophyChange;
	int iItemPriceClubCoin = pBillingInfo->iClubCoinChange;
	int iItemPrice = pBillingInfo->iItemPrice;
	int iPostMoney = iPrevMoney - iItemPriceCash;
	char* szBillingKey = pBillingInfo->szBillingKey;
	char* szTitle = pBillingInfo->szTitle;
	char* szText = pBillingInfo->szText;
	BOOL bIsSendItem = pBillingInfo->bIsSendItem;
	int iRenewItemIdx = pBillingInfo->iRenewIndex;

	vector<SUserItemInfo> vUserTempItemInfo;	
	int iItemPropertyPrice = pBillingInfo->iItemPropertyPrice;
	int iItemPropertyType = pBillingInfo->iItemPropertyType;
	int iItemPropertyPrice2 = pBillingInfo->iItemPropertyPrice2;
	int iItemPropertyType2 = pBillingInfo->iItemPropertyType2;
	char* szProductCode = pBillingInfo->szProductCode;
	BOOL bIsLocalPublisher = pBillingInfo->bIsLocalPublisher;
	int iOperationCode = pBillingInfo->iOperationCode;

	int iErroeCode_SendItem = SEND_ITEM_ERROR_GENERAL;
	int iPacketOperationCode = -1;

	if(bIsSendItem == TRUE) 
		iPacketOperationCode = FS_ITEM_OP_SEND;
	else
		iPacketOperationCode = FS_ITEM_OP_BUY;

	if(TRUE == pBillingInfo->bSaleRandomItemEvent)
		iPacketOperationCode = FS_ITEM_OP_BUY_SALE_RANDOMITEM;

	SShopItemInfo ShopItemInfo;			
	pItemShop->GetItem(iItemIndex, ShopItemInfo);	

	switch(iPayResult)
	{
	case PAY_RESULT_SUCCESS:
		{
			int iSetItemPropertyKind = -1;
			int iBuyTatooPriceCash = 0;
			int iBuyTatooPricePoint = 0;

			int iBuyOnePriceCash = 0; // 수량제 아이템의 경우 1개.
			int iBuyOnePricePoint = 0; // 수량제 아이템의 경우 1개.

			pItemShop->SetItemPropertyKind( ShopItemInfo.iPropertyKind , iSetItemPropertyKind );
			SetOneUnitItemPrice(pGameODBC, pItemShop,bIsLocalPublisher, ShopItemInfo, iPropertyValue, iItemPriceCash, iItemPricePoint, iBuyOnePriceCash, iBuyOnePricePoint, iErrorCode);

			SUserItemInfo UserItemInfo;
			UserItemInfo.iItemCode = iItemIndex;
			UserItemInfo.iChannel = ShopItemInfo.iChannel;
			UserItemInfo.iSexCondition = ShopItemInfo.iSexCondition;		
			UserItemInfo.iPropertyType = ShopItemInfo.iPropertyType;
			UserItemInfo.iPropertyKind = iSetItemPropertyKind;
			UserItemInfo.iBigKind = ShopItemInfo.iBigKind;
			UserItemInfo.iSmallKind = ShopItemInfo.iSmallKind;
			UserItemInfo.iPropertyTypeValue = iPropertyValue;
			UserItemInfo.iStatus = 1;
			UserItemInfo.iSellType = REFUND_TYPE_POINT;
			UserItemInfo.iSellPrice = GetSellPrice(iBuyOnePriceCash, iBuyOnePricePoint);

			if(bIsSendItem == FALSE)
			{
				time_t tCurrentDate = _time64(NULL);

				SAvatarCheerLeader AvatarCheerLeader;
				if(FALSE == m_pUser->GetUserCheerLeader()->GetCheerLeader(ShopItemInfo.iCheerLeaderIndex, AvatarCheerLeader))
				{
					AvatarCheerLeader.iCheerLeaderIndex = ShopItemInfo.iCheerLeaderIndex;
					AvatarCheerLeader.tBuyDate = tCurrentDate;
					AvatarCheerLeader.btTypeNum = ShopItemInfo.btCheerLeaderTypeNum;
				}

				int iPresentIndex = -1;
				int iAddTrophyCount = CLUBCONFIGMANAGER.GetCheerLeaderProbabillityByAbillity(AvatarCheerLeader.iCheerLeaderIndex, AvatarCheerLeader.btTypeNum, CHEERLEADR_ABILLITY_BUY_GIVE_TROPHY);

				if(ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyCheerLeader(iGameIDIndex, ShopItemInfo.iItemCode0, ShopItemInfo.iCheerLeaderIndex, AvatarCheerLeader.btTypeNum, iPropertyValue, iBuyPriceType, iItemPrice, iPrevMoney, iPostMoney, iAddTrophyCount, AvatarCheerLeader.tExpireDate, iPresentIndex))
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS_CHEERLEADER; 

					m_pUser->GetUserCheerLeader()->AddCheerLeader(AvatarCheerLeader);
					m_pUser->SetCheerLeaderCode(AvatarCheerLeader.iCheerLeaderIndex);
					m_pUser->SetCheerLeaderTypeNum(AvatarCheerLeader.btTypeNum);

					if(iPresentIndex > -1)
					{
						m_pUser->RecvPresent(MAIL_PRESENT_ON_ACCOUNT, iPresentIndex);
					}
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "ITEM_BuyCheerLeader - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
				}			
			}
			else
			{				
				int iPresentIdx = -1;
				int iRetError = -1;

				iRetError = pGameODBC->spFSBuyAndSendItem(szUserID, iUserIDIndex, iGameIDIndex, szGameID, szRecvGameID, iPresentIdx, szTitle,
					szText, ShopItemInfo.iItemCode0, iItemPropertyType, iItemPropertyPrice, iItemPropertyType2, iItemPropertyPrice2, iBuyPriceType, iItemPrice, iPrevMoney, iPostMoney,
					iaPropertyIndex, UserItemInfo, szBillingKey, szProductCode, iItemPrice);

				if(ODBC_RETURN_SUCCESS == iRetError)
				{
					iErrorCode = BUY_ITEM_ERROR_SUCCESS_CHEERLEADER;					
					iErroeCode_SendItem = SEND_ITEM_ERROR_SUCCESS;

					CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
					if( pCenter != NULL ) 
					{
						pCenter->SendUserNotify(szRecvGameID, iPresentIdx, MAIL_PRESENT_ON_AVATAR);				
					}
				}
				else if (iRetError == -11)
				{
					iErroeCode_SendItem = SEND_ITEM_ERROR_PRESENTBOX_FULL;
				}
				else
				{
					WRITE_LOG_NEW(LOG_TYPE_ITEM, DB_DATA_UPDATE, FAIL, "spFSBuyAndSendItem - ItemCode:%d, UserID:%s, GameID:%s", iItemIndex, szUserID, szGameID);
					iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_EXIST_USER;
				}
			}

			if (BUY_ITEM_ERROR_SUCCESS_CHEERLEADER == iErrorCode)
			{
				SetUserBillResultAtMem(pUser, pGameODBC, iBuyPriceType, iPostMoney, iItemPricePoint, iItemPriceTrophy, pBillingInfo->iBonusCoin);
			}
		}
		break;
	case PAY_RESULT_FAIL_CASH:
		{
			iErrorCode = BUY_ITEM_ERROR_NOT_ENOUGH_CASH;
			iErroeCode_SendItem = SEND_ITEM_ERROR_NOT_ENOUGH_CASH;
		}
		break;
	default:
		{
			iErrorCode = BUY_ITEM_ERROR_BILLING_FAIL;
			iErroeCode_SendItem = SEND_ITEM_ERROR_BILLING_FAIL;
		}
		break;
	}

	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(iPacketOperationCode);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemIndex);
	if(BUY_ITEM_ERROR_SUCCESS_WITH_BONUSPOINT == iErrorCode)
	{
		PacketComposer.Add(iEventBonusPoint);
		PacketComposer.Add(iEventBonusCash);
	}
	PacketComposer.Add(BUY_ITEM_END_OPERATION);
	PacketComposer.Add((BYTE)iErroeCode_SendItem);			
	PacketComposer.Add((BYTE*)szRecvGameID, MAX_GAMEID_LENGTH + 1);
	pClient->Send(&PacketComposer);

	if(BUY_SKILL_ERROR_SUCCESS <= iErrorCode)
	{
		pUser->SendUserStat();		
		pUser->SendUserGold();
		pUser->SendCurAvatarTrophy();

		ProcessAchievementBuyItem(pGameODBC, pUser, pAvatarItemList);

		if(ITEM_CATEGORY_CLUB == ShopItemInfo.iCategory)
			pUser->SendClubShopUserCashInfo();
	}

	return (BUY_SKILL_ERROR_SUCCESS <= iErrorCode);	
}


