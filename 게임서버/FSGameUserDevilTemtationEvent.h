qpragma once

using namespace DevilTemtationEvent;

class CFSGameUser;

class CFSGameUserDevilTemtationEvent
{
public:
	CFSGameUserDevilTemtationEvent(CFSGameUser* pUser);
	~CFSGameUserDevilTemtationEvent(void);

	BOOL				Load();
	void				SendEventProductInfo();
	void				SendEventBonusProductInfo();
	void				SendUserInfo();
	BOOL				BuyDevilTemtationTicket_AfterPay(SBillingInfo* pBillingInfo, int iPayResult);
	BOOL				UseDevilTemtation(BYTE btTicketType, BYTE btTicketUseType);
	BOOL				UseSoulStone(BYTE btSoulStoneIndex);
	void				SetTicketCount(BYTE btTicketType, int iCount);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;
	int					m_iDevilTicket[MAX_TICKET_TYPE];
	int					m_iSoulStone;
	BYTE				m_btBonusChanceType;
};

