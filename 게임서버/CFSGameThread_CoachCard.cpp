qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CoachCardShop.h"
qinclude "MatchSvrProxy.h"
qinclude "CActionShop.h"
qinclude "ActionInfluenceList.h"
qinclude "Product.h"
qinclude "SkillUpgrade.h"

void CFSGameThread::Process_EnterCoachCardPageReq(CFSGameClient* pFSClient)
{
	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->CheckCoachCardExpireItem();

	BOOL bResult = TRUE;
	int iErrorCode = ERROR_CODE_NONE;

	if(FALSE == pFSClient->SetState(FS_COACH_CARD))
	{
		bResult = FALSE;
		iErrorCode = ERROR_CODE_SYSTEM;
	}

	CPacketComposer PacketComposer(S2C_ENTER_COACH_CARD_PAGE_RES);
	PacketComposer.Add(bResult);
	PacketComposer.Add(iErrorCode);
	pFSClient->Send(&PacketComposer);

	if (TRUE == bResult)
	{
		pUser->CheckAndSendPausedPotenUpgradeInfo();
		pUser->GetUserPurchaseGrade()->SendUserGradeCardCombineRateUp();
	}
}

void CFSGameThread::Process_CoachCardShopItemListReq(CFSGameClient* pFSClient)
{
	//통합 삭제대기 구 코치카드 샵
	//CPacketComposer PacketComposer(S2C_COACH_CARD_SHOP_ITEM_LIST_RES);

	//if( FALSE == COACHCARD.MakeCoachCardShopItemListRes(PacketComposer))
	//{
	//	g_LogManager.WriteLogToFile("Fail MakeCoachCardShopItemListPacket");
	//	return;
	//}

	//pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_BuyItemReq(CFSGameClient* pFSClient)
{
	//CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	//CHECK_NULL_POINTER_VOID(pGameODBC)

	//CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//int iExripeDay = 0;
	//int iShopType = SHOP_TYPE_COACH_CARD;
	//int iItemIDNumber = INVALID_IDINDEX;
	//BYTE byTermType = TERM_TYPE_PERMANENT;

	//m_ReceivePacketBuffer.Read(&iShopType);
	//m_ReceivePacketBuffer.Read(&iItemIDNumber);
	//m_ReceivePacketBuffer.Read(&byTermType);

	//
	//int iErrorCode = ERROR_CODE_NONE;
	//if( FALSE == COACHCARD.FindAndCheckBuyCondition(iItemIDNumber, byTermType, pUser->GetCurUsedAvtarLv(), iErrorCode))
	//{
	//	pFSClient->SendBuyItemRes(FALSE, iErrorCode, iItemIDNumber);
	//	return;
	//}

	//SItemPrimaryObjectPriceData sItemPrimaryObjectPriceData;
	//if( FALSE == COACHCARD.GetItemPrice(iItemIDNumber,byTermType, sItemPrimaryObjectPriceData))
	//{
	//	iErrorCode = ERROR_CODE_SYSTEM;
	//	pFSClient->SendBuyItemRes(FALSE, iErrorCode, iItemIDNumber);
	//	return;
	//}

	//if( FALSE == pUser->CheckCapital(sItemPrimaryObjectPriceData.bySellType, sItemPrimaryObjectPriceData.iPrice, iErrorCode))
	//{
	//	pFSClient->SendBuyItemRes(FALSE, iErrorCode, iItemIDNumber);
	//	return;
	//}
	//
	//if (MAX_COACH_CARD_SHOP_INVENTORY_ITEMCOUNT <= pUser->GetProductInventoryCount())
	//{
	//	iErrorCode = ERROR_CODE_CAN_NOT_ITEM_COUNT;
	//	pFSClient->SendBuyItemRes(FALSE, iErrorCode, iItemIDNumber);
	//	return;
	//}
	//
	//int iInventoryIndex = INVALID_IDINDEX;
	//int iProductIndex = INVALID_IDINDEX;
	//int iItemType = COACHCARD.GetItemType( iItemIDNumber );
	//int iTendencyType = TENDENCY_TYPE_NONE;
	//int iGradeLevel = 0;
	//SDateInfo sProductionDate, sExpireDate;

	//vector<int> vecGradeLevel;
	//COACHCARD.GetGradeLevel(iItemIDNumber, vecGradeLevel);

	//if( 1 < vecGradeLevel.size() )	
	//	iGradeLevel = RANDOMCARD_GRADE_LEVEL_ALL;
	//else
	//	iGradeLevel = vecGradeLevel[0];

	////통합 코치카드 구입
	//int iAddProductCount = 0;
	//int iNewProductCount = 0;

	//if( ODBC_RETURN_SUCCESS == pGameODBC->ITEM_BuyCoachCardShopItem(pUser->GetUserIDIndex(), pUser->GetGameIDIndex(), iItemIDNumber, byTermType, iTendencyType, iGradeLevel,sItemPrimaryObjectPriceData.bySellType, 
	//								iItemType, sItemPrimaryObjectPriceData.iPrice, iInventoryIndex, iProductIndex, iAddProductCount, iNewProductCount, sProductionDate, sExpireDate))
	//{
	//	pUser->SubtractCapital(sItemPrimaryObjectPriceData.bySellType, sItemPrimaryObjectPriceData.iPrice);

	//	SProductData* pProductData = new SProductData;
	//	if(NULL != pProductData)
	//	{
	//		pProductData->byTermType = byTermType;
	//		pProductData->iInventoryIndex = iInventoryIndex;
	//		pProductData->iProductIndex = iProductIndex;
	//		pProductData->iItemIDNumber =iItemIDNumber;
	//		pProductData->iItemType = iItemType;
	//		pProductData->iTendencyType = iTendencyType;
	//		pProductData->iRarityLevel = RARITY_LEVEL_NONE;
	//		pProductData->iGradeLevel = iGradeLevel;
	//		pProductData->sProductionDate = sProductionDate;
	//		pProductData->sExpireDate = sExpireDate;

	//		pUser->AddProductToInventory(pProductData);
	//	}
	//	pFSClient->SendBuyItemRes(TRUE, iErrorCode, iItemIDNumber);
	//	pUser->SendCurAvatarTrophy(pUser->GetUserTrophy(), pUser->IsPlaying());
	//}
	//else
	//{
	//	g_LogManager.WriteLogToFile("Fail ITEM_BuyCoachCardShopItem");
	//	iErrorCode = ERROR_CODE_SYSTEM;
	//	pFSClient->SendBuyItemRes(FALSE, iErrorCode, iItemIDNumber);
	//}
	//
}

void CFSGameThread::Process_CoachCardShopInventoryListReq(CFSGameClient* pFSClient)
{
	int iRequestPage = 0;
	BYTE byInventoryRange = INVENTORY_RANGE_WHOLE;
	int iRequestTendencyType = TENDENCY_TYPE_OFFENCE;
	int iSortPropertyType = -1;
	int iSortOrder = 0;
	int iRequestType = 0;

	m_ReceivePacketBuffer.Read(&iRequestPage);
	m_ReceivePacketBuffer.Read(&byInventoryRange);
	m_ReceivePacketBuffer.Read(&iRequestTendencyType);
	m_ReceivePacketBuffer.Read(&iSortPropertyType);
	m_ReceivePacketBuffer.Read(&iSortOrder);
	m_ReceivePacketBuffer.Read(&iRequestType);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendCoachCardShopInventoryListRes(iRequestPage, byInventoryRange, iRequestTendencyType,iSortPropertyType,iSortOrder,iRequestType);
}

void CFSGameThread::Process_UseItemReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	int iUseProductIndex = INVALID_INDEX;
	int iShopType = SHOP_TYPE_COACH_CARD;

	m_ReceivePacketBuffer.Read(&iShopType);
	m_ReceivePacketBuffer.Read(&iUseProductIndex);

	CProduct* pProduct = pUser->GetInventoryProduct(iUseProductIndex);
	CHECK_NULL_POINTER_VOID(pProduct);

	int iUseInventoryIndex = pProduct->GetInventoryIndex();
	int iTendencyType = pProduct->GetTendencyType();
	int iItemIDNumber = pProduct->GetItemIDNumber();
	int iRarityLevel = pProduct->GetRarityLevel();
	if(iRarityLevel == RARITY_LEVEL_NONE)
		iRarityLevel = RARITY_LEVEL_NORMAL;

	int iReceiveInventoryIndex = INVALID_INDEX;
	int iReceiveProductIndex = INVALID_INDEX;	
	int iReceiveItemIDNumber = INVALID_IDINDEX;
	int iRewardProductIndex = INVALID_IDINDEX;
	int iProductCount = 0;
	
	if(INVALID_IDINDEX != iItemIDNumber)
	{
		if( FALSE == COACHCARD.IsThereItem(iItemIDNumber) )
			return;

		vector<SActionInfluenceConfig> vecAddedActionInfluence;
		SProductData* pProductData = new SProductData;

		if (FALSE == COACHCARD.UseRandomCoachCard(pUser->GetGameIDIndex(),iUseProductIndex, iUseInventoryIndex, OWNERSHIP_TYPE_USE_RANDOM ,pProductData, vecAddedActionInfluence , iItemIDNumber, iRarityLevel, iProductCount))
		{
			return;
		}

		pUser->ProcessAfterRandomCoachCardUse(iTendencyType,iUseProductIndex, iProductCount, pProductData, vecAddedActionInfluence);

		if ( pProductData->iRarityLevel == RARITY_LEVEL_RARE)
		{
			CombineDataVec vecCombineData;
			SPotentialComponentList sPotentialListInfo;
			POTENTIAL_COMPONENT eComponentIndex = COACHCARD.GetComponentIndex(iItemIDNumber);
			if(TRUE == COACHCARD.AddRandomPotentialComponent( pProductData->iProductIndex, eComponentIndex, sPotentialListInfo, pUser->GetGameIDIndex()))
				pUser->SetPotentialComponent( pProductData->iProductIndex, sPotentialListInfo.sPotentialInfo );
		}

		iRewardProductIndex = pProductData->iProductIndex;
		pUser->SendCoachCardRewardNot(STATE_COACH_CARD_SHOP_ITEM_USE, iRewardProductIndex, iUseProductIndex, iProductCount );
	}
}

void CFSGameThread::Process_EquipCoachCardReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);	

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	SSlotData sSlotData;
	m_ReceivePacketBuffer.Read(&sSlotData.iProductIndex);
	m_ReceivePacketBuffer.Read(&sSlotData.iTendencyType);


	BOOL bResult = TRUE;
	int iErrorCode = ERROR_CODE_NONE;
	int iItemIDNumber = INVALID_IDINDEX;

	bResult = pUser->EquipProduct(sSlotData, iErrorCode);	
	pUser->SendEquipCoachCardRes(bResult, iErrorCode, sSlotData.iProductIndex);

	if (bResult == TRUE)
	{
		CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		CHECK_NULL_POINTER_VOID(pMatch);

		// ActionInfluence list
		vector<int> vecEquipActionInfluenceIndex;
		pUser->GetEquipActionInfluence(sSlotData.iProductIndex, vecEquipActionInfluenceIndex);

		// Potential list
		SPotentialComponent sPotentialInfo;
		SPotentialComponentConfig sPotentialConfigInfo;
		pUser->GetPotentialComponent(sSlotData.iProductIndex, sPotentialInfo);
		COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );


		SG2M_EQUIP_PRODUCT_NOT req;
		req.iGameIDIndex = pUser->GetGameIDIndex();
		req.btServerIndex = _GetServerIndex;
		req.SlotData = sSlotData;

		CPacketComposer PacketComposer(G2M_EQUIP_PRODUCT_NOT);
		PacketComposer.Add((BYTE*)&req, sizeof(SG2M_EQUIP_PRODUCT_NOT));

		PacketComposer.Add(vecEquipActionInfluenceIndex.size());
		for(int i = 0; i < vecEquipActionInfluenceIndex.size(); i++)
		{
			PacketComposer.Add(vecEquipActionInfluenceIndex[i] );
		}

		PacketComposer.Add((int)sPotentialConfigInfo.eComponentIndex);
		PacketComposer.Add(sPotentialConfigInfo.iRelateIndex);
		PacketComposer.Add(sPotentialConfigInfo.iParam);

		pMatch->Send(&PacketComposer);
	}
}

void CFSGameThread::Process_UnEquipCoachCardReq(CFSGameClient* pFSClient)
{
	int iProductIndex = INVALID_IDINDEX;
	int iTendencyType = TENDENCY_TYPE_NONE;

	m_ReceivePacketBuffer.Read(&iProductIndex);
	m_ReceivePacketBuffer.Read(&iTendencyType);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	BOOL bResult = TRUE;
	int iErrorCode = ERROR_CODE_NONE;

	bResult = pUser->UnEquipProduct(iTendencyType, iErrorCode);

	pUser->SendUnEquipCoachCardRes(bResult, iErrorCode, iProductIndex);
}

void CFSGameThread::Process_Register_Coach_Card_Combine(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CPacketComposer PacketComposer(S2C_REGISTER_COACH_CARD_COMBINE_LIST_RES);

	int iTendencyType = TENDENCY_TYPE_NONE;
	SCombineData CombineData;	

	m_ReceivePacketBuffer.Read(&CombineData.iProductIndex);

	CProduct* pProduct = pUser->GetInventoryProduct(CombineData.iProductIndex);
	CHECK_NULL_POINTER_VOID(pProduct);

	CombineData.iGradeLevel = pProduct->GetGradeLevel();
	CombineData.iTendencyType = pProduct->GetTendencyType();
	CombineData.iRarityLevel = pProduct->GetRarityLevel();
	CombineData.iInventoryIndex = pProduct->GetInventoryIndex();
	CombineData.eComponentIndex = pProduct->GetPotentialComponentIndex();
	CombineData.iSpecialCardIndex = pProduct->GetSpecialCardIndex();
	pProduct->GetActionInfluenceIndex(CombineData.iaActionInfluence);

	if (CombineData.iGradeLevel == INVALID_IDINDEX)
	{		
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_DONT_FIND_ITEM);
		PacketComposer.Add(CombineData.iProductIndex);
		pUser->Send(&PacketComposer);
		return;
	}

	if (FALSE == pUser->RegisterItemCombine(CombineData))
	{
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_DIFFERENT_CARD_LEVEL);
		PacketComposer.Add(CombineData.iProductIndex);
		pUser->Send(&PacketComposer);
		return;
	}

	PacketComposer.Add(1);
	PacketComposer.Add(ERROR_CODE_NONE);
	PacketComposer.Add(CombineData.iProductIndex);
	pUser->Send(&PacketComposer);
	return;
}
void CFSGameThread::Process_UnregisterCoachCardCombine(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CPacketComposer PacketComposer(S2C_UNREGISTER_COACH_CARD_COMBINE_LIST_RES);

	int iTendencyType = TENDENCY_TYPE_NONE;
	int iProductIndex = 0;
	int iItemIDNumber = 0;
	int iItemLv = 0;
	m_ReceivePacketBuffer.Read(&iProductIndex);
	m_ReceivePacketBuffer.Read(&iTendencyType);

	if (FALSE == pUser->IsThrereProduct(iProductIndex) ) 
	{		
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_DONT_FIND_ITEM);
		PacketComposer.Add(iProductIndex);
		pUser->Send(&PacketComposer);
		return;
	}

	if(FALSE == pUser->UnregisterItemCombine(iProductIndex))
	{
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_DONT_FIND_ITEM);
		PacketComposer.Add(iProductIndex);
		pUser->Send(&PacketComposer);
		return;
	}

	PacketComposer.Add(1);
	PacketComposer.Add(ERROR_CODE_NONE);
	PacketComposer.Add(iProductIndex);
	pUser->Send(&PacketComposer);
}


void CFSGameThread::Process_NeedPriceForItemCombineReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	int iCombineLevel = INVALID_IDINDEX;
	int iTotalPrice = 0;
	CombineDataVec vecCombineData;
	SItem_CombinePrice sItemCombinePrice;

	int iMixType = COMBINE_TYPE_MAIN_ACTIONINFLUENCE_RANDOM;
	m_ReceivePacketBuffer.Read( &iMixType );

	pUser->GetCombineInventoryIndex( vecCombineData );
	
	int iCombineDataSize = vecCombineData.size();
	for( int i = 0; i < iCombineDataSize; ++i )
	{
		sItemCombinePrice.Initialize();

		if( FALSE == pUser->GetCombineLevel(i, iCombineLevel) )		return;
		if( FALSE == COACHCARD.GetCombinePrice(iCombineLevel, iMixType, sItemCombinePrice) )		return;
		if( SELL_TYPE_POINT != sItemCombinePrice.iSellType )		return;

		iTotalPrice += sItemCombinePrice.iPrice;
	}
	
	CPacketComposer PacketComposer(S2C_NEED_PRICE_FOR_ITEM_COMBINE_RES);
	PacketComposer.Add(sItemCombinePrice.iSellType);
	PacketComposer.Add(iTotalPrice);
	pUser->Send(&PacketComposer);
}
void CFSGameThread::Process_EnterCoachCardCombinationTabRes(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	pUser->ClearItemCombineList();

	CPacketComposer PacketComposer(S2C_ENTER_COACH_CARD_COMBINATION_TAB_RES);
	PacketComposer.Add(1);
	PacketComposer.Add(ERROR_CODE_NONE);
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_CombineItemReq(CFSGameClient* pFSClient)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC)

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	int iTotalPrice = 0;
	int iCombineType = COMBINE_TYPE_MAIN_ACTIONINFLUENCE_RANDOM;
	CombineDataVec vecCombineData;
	SItem_CombinePrice sItemCombinePrice;
	
	m_ReceivePacketBuffer.Read(&iCombineType);

	if( COMBINE_TYPE_MAIN_ACTIONINFLUENCE_RANGE < iCombineType )
		iCombineType = COMBINE_TYPE_MAIN_ACTIONINFLUENCE_RANGE;

	if( FALSE == pUser->GetCombineInventoryIndex(vecCombineData) )
		return;

	int iCombineLevel = INVALID_INDEX;

	// Total Point Price 
	int iCombineDataSize = vecCombineData.size();
	for( int i = 0; i < iCombineDataSize; ++i )
	{
		sItemCombinePrice.Initialize();

		if( FALSE == pUser->GetCombineLevel(i, iCombineLevel) )		return;
		if( FALSE == COACHCARD.GetCombinePrice(iCombineLevel,iCombineType, sItemCombinePrice) )		return;
		if( SELL_TYPE_POINT != sItemCombinePrice.iSellType )		return;

		iTotalPrice += sItemCombinePrice.iPrice;
	}

	// Check User Cash 
	if( SELL_TYPE_POINT == sItemCombinePrice.iSellType && pUser->GetUserSkillPoint() < iTotalPrice )
	{
		CPacketComposer PacketComposer(S2C_COMBINE_ITEM_RES);
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_NOT_ENOUGH_CAPITAL);
		pUser->Send(&PacketComposer);		 
		return;
	}

	int iaArrayInventory[MAX_COMBINE_COUNT];
	memset( iaArrayInventory, -1, sizeof(int) * MAX_COMBINE_COUNT );

	// input combine coach-card inventory index
	for( int i = 0; i < MAX_COMBINE_COUNT; ++i )
	{
		if( i >= iCombineDataSize )	break;

		iaArrayInventory[i] = vecCombineData[i].iInventoryIndex;
	}


	int iaMainActionInfluence[MAX_POTENCARD_MAIN_INFLUENCE_COUNT] = {0,};

	/// @note : Combine Rate
	BYTE byTermType = TERM_TYPE_LIMITED;
	
	float fSentenceUp = pUser->GetUserCharacterCollection()->GetSentenceRewardValue(CHARACTER_COLLECTION_REWARD_TYPE_COACHCARD_RATE_UP);
	fSentenceUp += pUser->GetUserPurchaseGrade()->GetCardCombineRateUpValue();
	int iGradeLevel = COACHCARD.CombineGradeLevel( vecCombineData, fSentenceUp );
	int iRarityLevel = COACHCARD.CombineRarityLevel( vecCombineData );
	int iItemType = ITEM_TYPE_COACH_CARD;
	int iTendencyType = TENDENCY_TYPE_NONE;

	if(COMBINE_TYPE_MAIN_ACTIONINFLUENCE_RANGE == iCombineType )	// 고정 합성
	{
		COACHCARD.GetRandomMainActionInfluence(vecCombineData, iGradeLevel, iaMainActionInfluence, iTendencyType);
	}
	else
	{
		iTendencyType = rand() TENDENCY_TYPE_ALL;		// set Random Combine TendencyType

	}

	int iSpecialCardIndex = COACHCARD.CombineSpecialCardIndex( vecCombineData );
	/// ! Combine Rate

	SDateInfo sExpireDate;
	SDateInfo sProductionDate;

	int iItemIDNumber = INVALID_IDINDEX;
	int iItemProductcode = 0;
	int iItemInventoryIndex = 0;
	int iErrorCode = ERROR_CODE_NONE;
	if ( ODBC_RETURN_SUCCESS != pGameODBC->ITEM_Combination(pUser->GetGameIDIndex(), iaArrayInventory,
															sItemCombinePrice.iSellType, iTotalPrice, iGradeLevel, iRarityLevel, iItemType, iTendencyType, byTermType, iItemIDNumber, 
															iItemProductcode, iItemInventoryIndex, sExpireDate, sProductionDate, iSpecialCardIndex) )
	{
		CPacketComposer PacketComposer(S2C_COMBINE_ITEM_RES);
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_NOT_ENOUGH_CAPITAL);
		pUser->Send(&PacketComposer);		 
		return;
	}

	vector<SPotentialComponent> vecComponentInfo;
	vecComponentInfo.clear();
	SPotentialComponent sPotentialInfo;
	for (int i = 0 ; i < vecCombineData.size();i++)
	{
		pUser->GetPotentialComponent(vecCombineData[i].iProductIndex, sPotentialInfo);
		if(sPotentialInfo.iRelateIndex != -1)
			vecComponentInfo.push_back(sPotentialInfo);

		pUser->RemoveProductToInventory(vecCombineData[i].iTendencyType, vecCombineData[i].iProductIndex);
	}	

	int iMainActionInfluenceCount = 0;
	vector<SActionInfluenceConfig> vecActionInfluence;

	// Set ActionInfluence
	switch( iCombineType )
	{
	case COMBINE_TYPE_MAIN_ACTIONINFLUENCE_RANDOM:	// 랜덤 합성
		{
			vecActionInfluence.clear();
			COACHCARD.AddRandomizedActionInfluenceIndex( iItemProductcode, PRIMARY_POSTION_MAIN, iTendencyType, iGradeLevel, iRarityLevel, vecActionInfluence ,iSpecialCardIndex, pUser->GetGameIDIndex());
			if(vecActionInfluence.size() < 1)
				return;

			iMainActionInfluenceCount = vecActionInfluence.size();
			for (int i = 0; i < iMainActionInfluenceCount && i < MAX_POTENCARD_MAIN_INFLUENCE_COUNT; ++i)
				iaMainActionInfluence[i] = vecActionInfluence[i].iActionInfluenceIndex;

			COACHCARD.AddRandomizedActionInfluenceIndex( iItemProductcode, PRIMARY_POSTION_SUB, iTendencyType, iGradeLevel, iRarityLevel, vecActionInfluence , iSpecialCardIndex, pUser->GetGameIDIndex());
		}break;
	case COMBINE_TYPE_MAIN_ACTIONINFLUENCE_RANGE:	// 고정 합성
		{	// 주효과 DB insert
			COACHCARD.AddMainActionInfluenceIndex( iItemProductcode, iaMainActionInfluence, pUser->GetGameIDIndex());

			SActionInfluenceConfig sActionInfluenceConfig;
			vecActionInfluence.clear();
			for (int i = 0; i < MAX_POTENCARD_MAIN_INFLUENCE_COUNT; ++i)
			{
				if (0 >= iaMainActionInfluence[i])
					continue;

				COACHCARD.FindAndGetActionInfluenceConfig( iaMainActionInfluence[i], sActionInfluenceConfig );
				vecActionInfluence.push_back( sActionInfluenceConfig );
				iMainActionInfluenceCount++;
			}

			COACHCARD.AddRandomizedActionInfluenceIndex( iItemProductcode, PRIMARY_POSTION_SUB, iTendencyType, iGradeLevel, iRarityLevel, vecActionInfluence, iSpecialCardIndex, pUser->GetGameIDIndex());
		}break;
	default:
		{
		}break;
	};

	// Set Potential-Component
	SPotentialComponentList sPotentialListInfo;
	BOOL bPotential = FALSE;

	if(iRarityLevel == RARITY_LEVEL_RARE)
	{
		bPotential = COACHCARD.AddRandomPotentialComponent( iItemProductcode, vecCombineData, vecComponentInfo, sPotentialListInfo, pUser->GetGameIDIndex() );
	}

	// Input product data
	SProductData* pProductData = new SProductData;
	if(NULL != pProductData)
	{
		pProductData->byTermType = byTermType;
		pProductData->iInventoryIndex = iItemInventoryIndex;
		pProductData->iProductIndex = iItemProductcode;
		pProductData->iGradeLevel = iGradeLevel;
		pProductData->iRarityLevel = iRarityLevel;
		pProductData->iTendencyType = iTendencyType;
		pProductData->iItemType = iItemType;
		pProductData->sExpireDate = sExpireDate;
		pProductData->sProductionDate = sProductionDate;
		pProductData->iSpecialCardIndex = iSpecialCardIndex;
		memcpy(pProductData->iaActionInfluenceIndex, iaMainActionInfluence, sizeof(int)*MAX_POTENCARD_MAIN_INFLUENCE_COUNT);

		pUser->AddProductToInventory(pProductData);

		int iSubEffectSize = vecActionInfluence.size();
		for( int i = iMainActionInfluenceCount; i < iSubEffectSize; ++i )
		{
			if( i >= iMainActionInfluenceCount && PRIMARY_POSTION_SUB == vecActionInfluence[i].iPrimaryPosition )
				pUser->AddProductSubActionInfluence( iItemProductcode, vecActionInfluence[i].iActionInfluenceIndex );
		}
	}	// !if

	if( TRUE == bPotential )
	{
		pUser->SetPotentialComponent( iItemProductcode, sPotentialListInfo.sPotentialInfo );
	}

	int iCurrentPoint = pUser->GetSkillPoint() - iTotalPrice;
	pUser->SetSkillPoint(iCurrentPoint);
	pUser->SendUserGold();	
	pUser->AddPointAchievements(); 

	pFSClient->SendBuyItemRes(TRUE, iErrorCode, iItemIDNumber);

	CPacketComposer PacketComposer(S2C_COMBINE_ITEM_RES);
	PacketComposer.Add(1);
	PacketComposer.Add(ERROR_CODE_NONE);
	PacketComposer.Add(pUser->GetProductInventoryCount());
	PacketComposer.Add(iItemProductcode);
	PacketComposer.Add((int)0);	//통합 삭제대기 코치카드 합성이벤트 iRewardType
	PacketComposer.Add((int)0);	//통합 삭제대기 코치카드 합성이벤트 iItemCode
	PacketComposer.Add((int)0);	//통합 삭제대기 코치카드 합성이벤트 iCount
	PacketComposer.Add(sPotentialListInfo.sPotentialInfo.eComponentIndex);
	pUser->Send(&PacketComposer);

	pUser->ClearItemCombineList();

	pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POTENCARD_COMBINE);

	pUser->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_POTENTIAL_CARD_COMBINE_TRY);
	pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_COMBINE_POTENTIAL_CARD);
	pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_POTENCARD_COMBINE_COUNT);

	if(SELL_TYPE_POINT == sItemCombinePrice.iSellType && iTotalPrice > 0)
	{
		pUser->GetUserMissionBingoEvent()->CheckAndUpdateMission(MISSION_BINGO_CONDITION_TYPE_USE_POINT, iTotalPrice);
		pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_USE_POINT, iTotalPrice);
		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_BUY_POINT_ITEM);
		pUser->GetUserWordPuzzle()->UpdateLetter(WORDPUZZLES_REWARDTIME_NONE, WORDPUZZLES_REWARDTYPE_USEPOINT, iTotalPrice);
		pUser->GetUserMagicMissionEvent()->CheckMission(EVENT_MAGIC_MISSION_TYPE_USE_POINT, iTotalPrice);
		pUser->GetUserPremiumPassEvent()->CheckAndUpdateMission(EVENT_PREMIUM_PASS_MISSION_TYPE_USE_POINT, iTotalPrice);
	}
}

void CFSGameThread::Process_ThrowAwayItemReq(CFSGameClient* pFSClient)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC(ODBC_GAME);
	CHECK_NULL_POINTER_VOID(pGameODBC);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iTendencyType = TENDENCY_TYPE_NONE;
	int iInventoryIndex = 0;
	int iProductIndex = 0;
	m_ReceivePacketBuffer.Read(&iProductIndex);
	m_ReceivePacketBuffer.Read(&iTendencyType);

	CPacketComposer PacketComposer(S2C_THROW_AWAY_ITEM_RES);

	CProduct* pProduct = pUser->GetInventoryProduct(iProductIndex);
	if (NULL == pProduct)
	{
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_DONT_FIND_ITEM);
		pUser->Send(&PacketComposer);		
		return ;
	}

	iInventoryIndex = pProduct->GetInventoryIndex();
	
	if (TRUE == pProduct->IsEquiped())
	{
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_EQUIP_ITEM);
		pUser->Send(&PacketComposer);
		return;
	}

	if( TRUE == pProduct->IsPotentialComponent() )
	{
		COACHCARD.AddPotentialComponent( -1 );
	}

	int iProductCount = 0;
	if (ODBC_RETURN_FAIL == pGameODBC->AVATAR_ThrowAwayItem(pUser->GetGameIDIndex(),iInventoryIndex,iProductIndex,OWNERSHIP_TYPE_THROW_CARD,iProductCount))
	{
		PacketComposer.Add(0);
		PacketComposer.Add(ERROR_CODE_DONT_FIND_ITEM);
		pUser->Send(&PacketComposer);
		return ;
	}

	pUser->RemoveProductToInventory(iTendencyType, iProductIndex, iProductCount);


	PacketComposer.Add(1);
	PacketComposer.Add(ERROR_CODE_NONE);
	PacketComposer.Add(iProductIndex);
	PacketComposer.Add(iProductCount);
	pUser->Send(&PacketComposer);

}

void CFSGameThread::Process_CoachCardSlotPackageListReq( CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	pUser->SendCoachCardSlotPackageListRes();
}

void CFSGameThread::Process_CoachCardSubActionInfluence(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iProductIndex = 0;
	m_ReceivePacketBuffer.Read( &iProductIndex );

	CPacketComposer PacketComposer( S2C_COACH_CARD_SUB_ACTIONINFLUENCE_RES );
	pUser->MakePacketForCoachCardSubActionInfluence(PacketComposer, iProductIndex);
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_CoachCardInfo(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iProductIndex = 0;
	m_ReceivePacketBuffer.Read( &iProductIndex );

	CPacketComposer PacketComposer( S2C_COACH_CARD_INFO_RES );
	pUser->MakePacketForCoachCardInfo(PacketComposer, iProductIndex);
	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_CoachCardPotentialAbility(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	int iProductIndex = 0;
	m_ReceivePacketBuffer.Read( &iProductIndex );

	S_S2C_COACH_CARD_POTENTIAL_ABILITY_RES sPacketData;
	sPacketData.iProductIndex = iProductIndex;
	sPacketData.iResult = RESULT_SUCCESS;

	do{
		// The user have coach-card Product-Potential information
		SPotentialComponent sPotentialInfo;
		if( FALSE == pUser->GetPotentialComponent(iProductIndex, sPotentialInfo) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_PRODUCT_INFO;
			break;
		}

		if( POTENTIAL_COMPONENT_ABILITY != sPotentialInfo.eComponentIndex )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_COMPONENT_TYPE;
			break;
		}

		// This is potential-component(table) find information
		SPotentialComponentConfig sPotentialConfigInfo;
		if( FALSE == COACHCARD.FindPotentialComponentConfig(sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_INFO;
			break;
		}

		// The potential-card ability information output in the property-list
		int iPropertyIndex = sPotentialConfigInfo.iParam;
		CItemPropertyList* pItemProperty = pItemShop->GetItemPropertyList( iPropertyIndex );
		CHECK_NULL_POINTER_VOID(pItemProperty);

		pItemProperty->GetItemProperty( sPacketData.vecItemProperty );
	}while(0);	// just once

	// Set Packet information
	CPacketComposer PacketComposer( S2C_COACH_CARD_POTENTIAL_ABILITY_RES );
	PacketComposer.Add( sPacketData.iProductIndex );
	PacketComposer.Add( sPacketData.iResult );

	int iSize = sPacketData.vecItemProperty.size();
	PacketComposer.Add( iSize );
	for( int i = 0; i < iSize; ++i )
	{
		PacketComposer.Add( sPacketData.vecItemProperty[i].iProperty );
		PacketComposer.Add( sPacketData.vecItemProperty[i].iValue );
	}

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_CoachCardPotentialRageBalance(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iProductIndex = 0;
	m_ReceivePacketBuffer.Read( &iProductIndex );

	S_S2C_COACH_CARD_POTENTIAL_BALANCE_RES sPacketData;
	sPacketData.iProductIndex = iProductIndex;
	sPacketData.iResult = RESULT_SUCCESS;

	do{
		// The user have coach-card Product-Potential information
		SPotentialComponent sPotentialInfo;
		if( FALSE == pUser->GetPotentialComponent(iProductIndex, sPotentialInfo) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_PRODUCT_INFO;
			break;
		}

		if( POTENTIAL_COMPONENT_BALANCE != sPotentialInfo.eComponentIndex )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_COMPONENT_TYPE;
			break;
		}

		// This is potential-component(table) find information
		SPotentialComponentConfig sPotentialConfigInfo;
		if( FALSE == COACHCARD.FindPotentialComponentConfig(sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_INFO;
			break;
		}

		sPacketData.iPointGap = sPotentialConfigInfo.iParam;	// Play game point-gap

	}while(0);	// just once

	// Set Packet information
	CPacketComposer PacketComposer( S2C_COACH_CARD_POTENTIAL_BALANCE_RES );
	PacketComposer.Add( sPacketData.iProductIndex );
	PacketComposer.Add( sPacketData.iResult );
	PacketComposer.Add( sPacketData.iPointGap );

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_CoachCardPotentialAction(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CActionShop* pActionShop = pServer->GetActionShop();
	CHECK_NULL_POINTER_VOID(pActionShop);

	int iProductIndex = 0;
	m_ReceivePacketBuffer.Read( &iProductIndex );

	S_S2C_COACH_CARD_POTENTIAL_ACTION_RES sPacketData;
	sPacketData.iProductIndex = iProductIndex;
	sPacketData.iResult = RESULT_SUCCESS;

	do{
		// The user have coach-card Product-Potential information
		SPotentialComponent sPotentialInfo;
		if( FALSE == pUser->GetPotentialComponent(iProductIndex, sPotentialInfo) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_PRODUCT_INFO;
			break;
		}
		
		if( POTENTIAL_COMPONENT_ACTION != sPotentialInfo.eComponentIndex )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_COMPONENT_TYPE;
			break;
		}

		// This is potential-component(table) find information
		SPotentialComponentConfig sPotentialConfigInfo;
		if( FALSE == COACHCARD.FindPotentialComponentConfig(sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_INFO;
			break;
		}

		sPacketData.iActionCode = sPotentialConfigInfo.iParam;	// Action-code
		sPacketData.btCategory = pActionShop->GetActionCategory( sPacketData.iActionCode );

	}while(0);	// just once

	// Set Packet information
	CPacketComposer PacketComposer( S2C_COACH_CARD_POTENTIAL_ACTION_RES );
	PacketComposer.Add( sPacketData.iProductIndex );
	PacketComposer.Add( sPacketData.iResult );
	PacketComposer.Add( sPacketData.iActionCode );
	PacketComposer.Add( sPacketData.btCategory );

	pUser->Send(&PacketComposer);
}

void CFSGameThread::Process_CoachCardPotentialFreeStyle(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSSkillShop* pSkillShop = pServer->GetSkillShop();
	CHECK_NULL_POINTER_VOID(pSkillShop);

	int iProductIndex = 0;
	m_ReceivePacketBuffer.Read( &iProductIndex );

	S_S2C_COACH_CARD_POTENTIAL_FREESTYLE_RES sPacketData;
	sPacketData.iProductIndex = iProductIndex;
	sPacketData.iResult = RESULT_SUCCESS;

	do{
		// The user have coach-card Product-Potential information
		SPotentialComponent sPotentialInfo;
		if( FALSE == pUser->GetPotentialComponent(iProductIndex, sPotentialInfo) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_PRODUCT_INFO;
			break;
		}

		if( POTENTIAL_COMPONENT_FREESTYLE != sPotentialInfo.eComponentIndex )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_COMPONENT_TYPE;
			break;
		}

		// This is potential-component(table) find information
		SPotentialComponentConfig sPotentialConfigInfo;
		if( FALSE == COACHCARD.FindPotentialComponentConfig(sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex) )
		{
			sPacketData.iResult = RESULT_INVALID_POTENTIAL_INFO;
			break;
		}

		sPacketData.iFreeStyleIndex = sPotentialConfigInfo.iParam;

		//SSkillInfo SkillInfo;
		//pSkillShop->GetSkill( pUser->GetCurUsedAvatarPosition(), SKILL_TYPE_FREESTYLE, sPacketData.iFreeStyleIndex, SkillInfo );
	}while(0);	// just once

	// Set Packet information
	CPacketComposer PacketComposer( S2C_COACH_CARD_POTENTIAL_FREESTYLE_RES );
	PacketComposer.Add( sPacketData.iProductIndex );
	PacketComposer.Add( sPacketData.iResult );
	PacketComposer.Add( sPacketData.iFreeStyleIndex );

	int iUpgradeIndex = 0, iUpgradeStep = 0;
	if (RESULT_SUCCESS == sPacketData.iResult)
	{
		pUser->GetPotentialFreeStyleUpgradeInfo(sPacketData.iFreeStyleIndex, iUpgradeIndex, iUpgradeStep);
	}
	PacketComposer.Add((BYTE)iUpgradeIndex);
	PacketComposer.Add((BYTE)iUpgradeStep);

	pUser->Send(&PacketComposer);
}


void CFSGameThread::Process_CoachCardSort( CFSGameClient* pFSClient)
{
	int iSortPropertyType = SORT_PROPERTY_TYPE_BUY_DATE;
	int iSortOrder = SORT_ORDER_ASC;

	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	m_ReceivePacketBuffer.Read(&iSortPropertyType);
	m_ReceivePacketBuffer.Read(&iSortOrder);

	pUser->SetAndSortCoachCardInventory(iSortPropertyType, iSortOrder);
}

void CFSGameThread::Process_CoachCardTermExtendPriceListReq( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CActionInfluenceList* pActionInfluenceList = COACHCARD.GetActionInfluenceList();
	CHECK_NULL_POINTER_VOID(pActionInfluenceList);

	int iProductIndex;
	int iGradeLevel;
	int iTendencyType = 0;
	int iRarity;
	int iRemainTime;
	int iaActionInfluence[MAX_POTENCARD_MAIN_INFLUENCE_COUNT] = {0,};
	SActionInfluenceConfig sActionInfluenceConfig;

	m_ReceivePacketBuffer.Read(&iProductIndex);

	CProduct* pProduct = pUser->GetInventoryProduct(iProductIndex);
	CHECK_NULL_POINTER_VOID(pProduct);

	iGradeLevel =pProduct->GetGradeLevel();
	iTendencyType = pProduct->GetTendencyType();
	iRarity = pProduct->GetRarityLevel();
	iRemainTime = pProduct->GetRemainTerm() / 60;
	pProduct->GetActionInfluenceIndex(iaActionInfluence);
	int iSpecialCardIndex = pProduct->GetSpecialCardIndex();

	vector<STermExtend*> vecTermExtend;
	if (FALSE == COACHCARD.GetCoachCardTermExtendConfig(iGradeLevel,vecTermExtend))
	{
		return;
	}

	SPotentialComponent sPotentialInfo;
	pUser->GetPotentialComponent( iProductIndex, sPotentialInfo );	

	CPacketComposer PacketComposer(S2C_COACH_CARD_TERM_EXTEND_PRICELIST_RES);

	int iTermSize = vecTermExtend.size();

	PacketComposer.Add(iProductIndex);
	PacketComposer.Add(iTendencyType);
	PacketComposer.Add(iGradeLevel);
	PacketComposer.Add(iRarity);
	PacketComposer.Add(iRemainTime);
	PacketComposer.Add((BOOL)iSpecialCardIndex); // Client 는 현재 BOOL 형으로 ThreeKingdom 만 구분한다.

	BYTE btMainInfluenceCount = 0;
	PBYTE pCountLocation = PacketComposer.GetTail();
	PacketComposer.Add(btMainInfluenceCount);
	for (int i = 0; i < MAX_POTENCARD_MAIN_INFLUENCE_COUNT; ++i)
	{
		if (0 >= iaActionInfluence[i])	
			continue;

		pActionInfluenceList->FindAndGetActionInfluenceConfig(iaActionInfluence[i],sActionInfluenceConfig);
		PacketComposer.Add(sActionInfluenceConfig.iActionType);
		PacketComposer.Add(sActionInfluenceConfig.iInfluenceType);
		++btMainInfluenceCount;
	}
	memcpy(pCountLocation, &btMainInfluenceCount, sizeof(BYTE));

	PacketComposer.Add(sPotentialInfo.eComponentIndex);
	PacketComposer.Add(iTermSize);
	for (int i = 0 ; i < iTermSize ; i++)
	{
		STermExtend* pTermExtend = vecTermExtend[i];
		CHECK_NULL_POINTER_VOID(pTermExtend);
		PacketComposer.Add(pTermExtend->iTermExtend);
		PacketComposer.Add(pTermExtend->iSellType);
		PacketComposer.Add(pTermExtend->iPrice);
	}

	pUser->Send(&PacketComposer);

}

void CFSGameThread::Process_CoachCardTermExtend( CFSGameClient * pFSClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iProductIndex;
	int iTermExtend;
	int iSellType;
	int iPrice;

	m_ReceivePacketBuffer.Read(&iProductIndex);
	m_ReceivePacketBuffer.Read(&iTermExtend);
	m_ReceivePacketBuffer.Read(&iSellType);
	m_ReceivePacketBuffer.Read(&iPrice);

	CPacketComposer PacketComposer(S2C_COACH_CARD_TERM_EXTEND_RES);
	vector<STermExtend*> vecTermExtend;
	int iGradeLevel;

	iGradeLevel = pUser->GetInventoryGradeLevel(iProductIndex);
	if (FALSE == COACHCARD.GetCoachCardTermExtendConfig(iGradeLevel,vecTermExtend))
	{
		return;
	}

	// check point
	switch( iSellType )
	{
	case SELL_TYPE_POINT:
		{
			if( pUser->GetSkillPoint() < iPrice )
			{
				PacketComposer.Add(FALSE);
				PacketComposer.Add(iProductIndex);
				PacketComposer.Add(0);

				pUser->Send(&PacketComposer);
				return;
			}
		}break;
	//case SELL_TYPE_CASH:
	default:
		{
			PacketComposer.Add(FALSE);
			PacketComposer.Add(iProductIndex);
			PacketComposer.Add(0);

			pUser->Send(&PacketComposer);
			return;
		}break;
	}	// !switch


	BOOL iRet = FALSE;
	int iRemainTime = 0;
	for (int i = 0; i < vecTermExtend.size(); i++)
	{
		if (vecTermExtend[i] != NULL &&
			vecTermExtend[i]->iTermExtend == iTermExtend && vecTermExtend[i]->iSellType == iSellType && vecTermExtend[i]->iPrice == iPrice)
		{		
			if (TRUE == pUser->BuyCoachCardTermExtend(iProductIndex,iTermExtend,iSellType,iPrice,iRemainTime))
			{
				iRet = TRUE;
			}
			break;
		}		
	}

	if( TRUE == iRet )
	{
		pUser->CheckEquipPotentialProductSetting( iProductIndex );
	}

	PacketComposer.Add(iRet);
	PacketComposer.Add(iProductIndex);
	PacketComposer.Add(iRemainTime/60);

	pUser->Send(&PacketComposer);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_UPGRADE_POTENTIAL_CARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_UPGRADE_POTENTIAL_CARD_INFO_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_UPGRADE_POTENTIAL_CARD_INFO_REQ));

	CProduct* pProduct = pUser->GetInventoryProduct(req.iProductIndex);
	CHECK_NULL_POINTER_VOID(pProduct);

	SS2C_UPGRADE_POTENTIAL_CARD_INFO_RES res;
	res.iProductIndex = req.iProductIndex;
	res.iPointCost = COACHCARD.GetPotenUpgradePointCost();
	res.iTargetFreestyleNo = 0;
	res.btUpgradeListCount = 0;

	SPotentialComponent sPotentialInfo = pProduct->GetPotentialComponent();

	if (POTENTIAL_COMPONENT_FREESTYLE != sPotentialInfo.eComponentIndex)
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_INFO_NOT_FREESTYLE_POTENTIAL;
		pUser->Send(S2C_UPGRADE_POTENTIAL_CARD_INFO_RES,&res,sizeof(SS2C_UPGRADE_POTENTIAL_CARD_INFO_RES));
		return;
	}

	if (TRUE == pProduct->IsEquiped())
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_INFO_EQUIPED;
		pUser->Send(S2C_UPGRADE_POTENTIAL_CARD_INFO_RES,&res,sizeof(SS2C_UPGRADE_POTENTIAL_CARD_INFO_RES));
		return;
	}
	
	SPotentialComponentConfig sPotentialConfigInfo;
	COACHCARD.FindPotentialComponentConfig( sPotentialConfigInfo, sPotentialInfo.eComponentIndex, sPotentialInfo.iRelateIndex );

	res.iTargetFreestyleNo = sPotentialConfigInfo.iParam;

	vector<int> vecUpgradeIndex;
	if (FALSE == SKILLUPGRADE.GetUpgradeIndexList(res.iTargetFreestyleNo, vecUpgradeIndex))
	{
		res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_INFO_FAIL;
		pUser->Send(S2C_UPGRADE_POTENTIAL_CARD_INFO_RES,&res,sizeof(SS2C_UPGRADE_POTENTIAL_CARD_INFO_RES));
		return;
	}

	res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_INFO_SUCCESS;
	res.btUpgradeListCount = vecUpgradeIndex.size();

	CPacketComposer Packet(S2C_UPGRADE_POTENTIAL_CARD_INFO_RES);
	Packet.Add((PBYTE)&res, sizeof(SS2C_UPGRADE_POTENTIAL_CARD_INFO_RES));

	for (int i = 0; i < vecUpgradeIndex.size(); ++i)
	{
		Packet.Add((BYTE)vecUpgradeIndex[i]);
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_UPGRADE_POTENTIAL_CARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_UPGRADE_POTENTIAL_CARD_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_UPGRADE_POTENTIAL_CARD_REQ));

	SS2C_UPGRADE_POTENTIAL_CARD_RES res;
	res.btResult = RESULT_UPGRADE_POTENTIAL_CARD_FAIL;
	pUser->UpgradePotenCard(req, res);

	pUser->Send(S2C_UPGRADE_POTENTIAL_CARD_RES, &res, sizeof(SS2C_UPGRADE_POTENTIAL_CARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_REQ req;
	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_REQ));

	SS2C_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_RES res;
	res.iTargetProductIndex = req.iTargetProductIndex;
	res.btResult = pUser->DecideUpgradePotenCard(req.iTargetProductIndex, req.btChooseNewUpgrade);
	
	pUser->Send(S2C_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_RES, &res, sizeof(SS2C_CHOOSE_POTENTIAL_CARD_UPGRADE_RESULT_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_POTENCARD_TERM_EXTEND_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_POTENCARD_TERM_EXTEND_LIST_REQ	info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_POTENCARD_TERM_EXTEND_LIST_REQ));

	SS2C_POTENCARD_TERM_EXTEND_LIST_RES	rs;
	rs.iErrorCode = pUser->PotenCardTermExtendforEquipCard(info.iPropertyValue);
	
	CPacketComposer Packet(S2C_POTENCARD_TERM_EXTEND_LIST_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_POTENCARD_TERM_EXTEND_LIST_RES));

	pUser->Send(&Packet);
}
