qinclude "stdafx.h"
qinclude "FSGameUserGoldenSafeEvent.h"
qinclude "CFSGameUser.h"
qinclude "GoldenSafeEventManager.h"
qinclude "RewardManager.h"

CFSGameUserGoldenSafeEvent::CFSGameUserGoldenSafeEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iKeepStepFlag(0)
	, m_btCurrentStep(0)
	, m_btCurrentStepStatus(0)
	, m_tRecentCheckDate(-1)
	, m_bIsGetKeepReward(FALSE)
{
}

CFSGameUserGoldenSafeEvent::~CFSGameUserGoldenSafeEvent(void)
{
}

BOOL	CFSGameUserGoldenSafeEvent::Load()
{
	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iRet = pODBC->EVENT_GOLDENSAFE_GetUserData(m_pUser->GetUserIDIndex(), m_iKeepStepFlag, m_btCurrentStep, m_btCurrentStepStatus, m_tRecentCheckDate, m_bIsGetKeepReward);
	if(ODBC_RETURN_SUCCESS != iRet)
	{
		if(iRet == -1)	// 이벤트 단계 모두 완료
		{
			return TRUE;
		}
		WRITE_LOG_NEW(LOG_TYPE_SKYLUCKY, DB_DATA_LOAD, FAIL, "EVENT_GOLDENSAFE_GetUserData failed");
		return FALSE;
	}

	m_bDataLoaded = TRUE;

	CheckResetDate();

	return TRUE;
}

BOOL	CFSGameUserGoldenSafeEvent::CheckResetDate(time_t tCurrentTime /*= _time64(NULL)*/)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(TRUE == m_bIsGetKeepReward, FALSE);
	CHECK_CONDITION_RETURN(m_btCurrentStep >= GOLDENSAFE_STEP_MAX_COUNT, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == GOLDENSAFE.IsRewardOpen(), FALSE);
	CHECK_CONDITION_RETURN(CLOSED == GOLDENSAFE.IsEventOpen() && m_btCurrentStep == 0 && m_tRecentCheckDate == -1, FALSE);	// 이벤트 시작안한 유저 

	BOOL	bResult = FALSE;
	int		iPassStep = 0;

	if(-1 == m_tRecentCheckDate)
	{
		bResult = TRUE;
	}
	else
	{
		TIMESTAMP_STRUCT	RecentResetDate;
		TIMESTAMP_STRUCT	RecentResetHourDate;
		TimetToTimeStruct(m_tRecentCheckDate, RecentResetDate);
		ZeroMemory(&RecentResetHourDate, sizeof(TIMESTAMP_STRUCT));

		RecentResetHourDate.year	= RecentResetDate.year;
		RecentResetHourDate.month	= RecentResetDate.month;
		RecentResetHourDate.day		= RecentResetDate.day;
		RecentResetHourDate.hour	= EVENT_RESET_HOUR; 

		for(time_t tTime = TimeStructToTimet(RecentResetHourDate); tTime < tCurrentTime; tTime += (24*60*60))
		{
			if(m_tRecentCheckDate <= tTime && tTime <= tCurrentTime)
			{
				iPassStep ++;
			}
		}

		if(iPassStep > 0)
		{
			bResult = TRUE;
		}
	}	

	if(bResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		BYTE btUpdatedStep = m_btCurrentStep + iPassStep;
		
		if(-1 == m_tRecentCheckDate)
		{
			btUpdatedStep = 0;
		}
		else if(btUpdatedStep >= GOLDENSAFE_STEP_MAX_COUNT)
		{
			btUpdatedStep = GOLDENSAFE_STEP_MAX_COUNT;
		}

		int iUpdatedKeepStepFlag = m_iKeepStepFlag | GOLDENSAFE.GetKeepFlag(btUpdatedStep);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_GOLDENSAFE_GoToNextStep(m_pUser->GetUserIDIndex(), iUpdatedKeepStepFlag, btUpdatedStep, tCurrentTime))
		{
			m_iKeepStepFlag			= iUpdatedKeepStepFlag;
			m_btCurrentStep			= btUpdatedStep;
			m_btCurrentStepStatus	= GOLDENSAFE_STEP_STATUS_CHECK_OK;
			m_tRecentCheckDate		= tCurrentTime;
		}
	}

	return bResult;
}

BOOL	CFSGameUserGoldenSafeEvent::IsGoldenSafeEventButtonOpen()
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);
	CHECK_CONDITION_RETURN(TRUE == m_bIsGetKeepReward, FALSE);
	CHECK_CONDITION_RETURN(CLOSED == GOLDENSAFE.IsRewardOpen(), FALSE);
	CHECK_CONDITION_RETURN(CLOSED == GOLDENSAFE.IsEventOpen() && m_btCurrentStep == 0 && m_tRecentCheckDate == -1, FALSE);	// 이벤트 시작안한 유저 
	return TRUE;
}

BOOL	CFSGameUserGoldenSafeEvent::SendEventInfo()
{
	CHECK_CONDITION_RETURN(FALSE == IsGoldenSafeEventButtonOpen(), FALSE);

//	SS2C_GOLDENSAFE_EVENT_INFO_NOT	ss;
//	ZeroMemory(&ss, sizeof(SS2C_GOLDENSAFE_EVENT_INFO_NOT));

//	GOLDENSAFE.GetRewardInfo(ss.stKeepRewardInfo);

	CPacketComposer	Packet(S2C_GOLDENSAFE_EVENT_INFO_NOT);
//	Packet.Add((PBYTE)&ss, sizeof(SS2C_GOLDENSAFE_EVENT_INFO_NOT));

	m_pUser->Send(&Packet);

	return TRUE;
}

void	CFSGameUserGoldenSafeEvent::SendUserEventData()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	CheckResetDate();

	SS2C_GOLDENSAFE_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_GOLDENSAFE_USER_INFO_RES));

	GOLDENSAFE.GetIsKeepReward(m_iKeepStepFlag, ss.bIsKeepReward);

	ss.btCurrentStep		= (m_btCurrentStep > GOLDENSAFE_STEP_MAX_COUNT - 1) ? GOLDENSAFE_STEP_MAX_COUNT - 1 : m_btCurrentStep;
	ss.btCurrentStepStatus	= m_btCurrentStepStatus;	

	CPacketComposer	Packet(S2C_GOLDENSAFE_USER_INFO_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_GOLDENSAFE_USER_INFO_RES));

	m_pUser->Send(&Packet);
}

void	CFSGameUserGoldenSafeEvent::GiveGoldenSafeReward()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);

	time_t	tCurrentTime = _time64(NULL);

	BOOL	bIsReset = CheckResetDate(tCurrentTime);
	BOOL	bIsGetKeepReward = FALSE;
	int iDailyRewardIndex = 0;
	vector<int/*iRewardIndex*/>	vecRewardIndex;
	
	SS2C_GOLDENSAFE_GET_REWARD_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_GOLDENSAFE_GET_REWARD_RES));

	ss.btResult = RESULT_GOLDENSAFE_GET_REWARD_SUCCESS;

	if(TRUE == m_bIsGetKeepReward || CLOSED == GOLDENSAFE.IsRewardOpen())
	{
		ss.btResult = RESULT_GOLDENSAFE_GET_REWARD_EVENT_CLOSED_FAIL;
	}
	else if(GOLDENSAFE_STEP_STATUS_REWARD_OK == m_btCurrentStepStatus)
	{
		ss.btResult = RESULT_GOLDENSAFE_GET_REWARD_ALREADY_GET_FAIL;
	}	
	else if(bIsReset)
	{
		ss.btResult = RESULT_GOLDENSAFE_GET_REWARD_IS_RESET_FAIL;
	}
	else
	{
		
		if(GOLDENSAFE_STEP_STATUS_CHECK_OK == m_btCurrentStepStatus && m_btCurrentStep < GOLDENSAFE_STEP_MAX_COUNT)
		{
			iDailyRewardIndex = GOLDENSAFE.GetDailyRewardIndex(m_btCurrentStep);				

			if(iDailyRewardIndex <= 0)
			{
				ss.btResult = RESULT_GOLDENSAFE_GET_REWARD_GIVE_FAIL;
			}
			else
			{
				vecRewardIndex.push_back(iDailyRewardIndex);
			}			
		}	

		if(GOLDENSAFE_STEP_MAX_COUNT - 1 == m_btCurrentStep ||
			(CLOSED == GOLDENSAFE.IsEventOpen() && m_btCurrentStep >= GOLDENSAFE_STEP_MAX_COUNT)) // 마지막 단계 또는 5일 지나서 왔을 경우 모든 보상 지급
		{
			GOLDENSAFE.GetKeepRewardIndex(m_iKeepStepFlag, vecRewardIndex);
			bIsGetKeepReward = TRUE;
		}

		if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + vecRewardIndex.size() > MAX_PRESENT_LIST)
		{
			ss.btResult = RESULT_GOLDENSAFE_GET_REWARD_FULL_MAILBOX_FAIL;
		}
	}

	if(RESULT_GOLDENSAFE_GET_REWARD_SUCCESS == ss.btResult)
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_VOID(pODBC);

		if(ODBC_RETURN_SUCCESS == pODBC->EVENT_GOLDENSAFE_GiveDailyReward(m_pUser->GetUserIDIndex(), iDailyRewardIndex, m_btCurrentStep, tCurrentTime, bIsGetKeepReward))
		{
			SRewardConfig* pReward = NULL;

			for(int i = 0; i < vecRewardIndex.size(); ++i)
			{
				pReward = REWARDMANAGER.GiveReward(m_pUser, vecRewardIndex[i]);

				if(NULL == pReward)
				{
					WRITE_LOG_NEW(LOG_TYPE_GOLDENSAFE, LA_DEFAULT, NONE, "EVENT_GOLDENSAFE_GiveReward Fail, UserIDIndex:10752790, RewardIndex:0, CurrentStep:7106560, CurrentStepStatus:-858993460"
etUserIDIndex(), vecRewardIndex[i], m_btCurrentStep, m_btCurrentStepStatus);
				}
				else if(i == 0 && iDailyRewardIndex > 0)	// 매일보상에 대한 정보 보냄
				{
					ss.btRewardType = pReward->iRewardType;

					if(REWARD_TYPE_POINT == ss.btRewardType)
					{
						ss.iItemCode		= -1;
						ss.iPropertyValue	= pReward->iRewardValue;
					}
					else if(REWARD_TYPE_ITEM == ss.btRewardType)
					{
						SRewardConfigItem* pRewardItem = (SRewardConfigItem*)pReward;

						ss.iItemCode		= pRewardItem->iItemCode;
						ss.btPropertyType	= pRewardItem->iPropertyType;
						ss.iPropertyValue	= pRewardItem->iPropertyValue;
					}

					m_btCurrentStepStatus = GOLDENSAFE_STEP_STATUS_REWARD_OK;
				}
			}

			m_bIsGetKeepReward = bIsGetKeepReward;
		}
	}

	CPacketComposer	Packet(S2C_GOLDENSAFE_GET_REWARD_RES);
	Packet.Add((PBYTE)&ss, sizeof(SS2C_GOLDENSAFE_GET_REWARD_RES));
	m_pUser->Send(&Packet);
}