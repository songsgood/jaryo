qinclude "stdafx.h"
qinclude "FSGameUserDarkMarketEvent.h"
qinclude "DarkMarketEventManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserDarkMarketEvent::CFSGameUserDarkMarketEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iMyDiamond(0)
{
	for(int i = 0; i < DIAMOND_PRODUCT_CNT; ++i)
	{
		m_iDiamondProductRemainCnt[i] = -1;
	}

	for(int i = 0; i < PACKAGE_PRODUCT_CNT; ++i)
	{
		m_iPackageProductRemainCnt[i] = -1;
	}
}
CFSGameUserDarkMarketEvent::~CFSGameUserDarkMarketEvent(void)
{
}

BOOL CFSGameUserDarkMarketEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == DARKMARKET.IsOpen(), TRUE);

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_DARKMARKET_GetUserData(m_pUser->GetUserIDIndex(), m_iMyDiamond, m_iDiamondProductRemainCnt, m_iPackageProductRemainCnt))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_DARKMARKET_GetUserData failed");
		return FALSE;	
	}

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserDarkMarketEvent::SendEventInfo(BYTE btMarketType)
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DARKMARKET.IsOpen());

	switch(btMarketType)
	{
	case DIAMOND_MARKET: m_pUser->Send(DARKMARKET.GetPacketEventDiamondProductInfo()); return;
	case PACKAGE_MARKET: m_pUser->Send(DARKMARKET.GetPacketEventPackageProductInfo()); return;
	}
}

void CFSGameUserDarkMarketEvent::SendUserInfo()
{
	CHECK_CONDITION_RETURN_VOID(FALSE == m_bDataLoaded);
	CHECK_CONDITION_RETURN_VOID(CLOSED == DARKMARKET.IsOpen());

	SS2C_DARKMARKET_USER_INFO_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_DARKMARKET_USER_INFO_RES));

	ss.iMyDiamond = m_iMyDiamond;

	for(int i = 0; i < DarkMarketEvent::DIAMOND_PRODUCT_CNT; ++i)
	{
		ss.iDiamondProductRemainCnt[i] = m_iDiamondProductRemainCnt[i];
	}

	for(int i = 0; i < DarkMarketEvent::PACKAGE_PRODUCT_CNT; ++i)
	{
		ss.iPackageProductRemainCnt[i] = m_iPackageProductRemainCnt[i];
	}

	m_pUser->Send(S2C_DARKMARKET_USER_INFO_RES, &ss, sizeof(SS2C_DARKMARKET_USER_INFO_RES));
}

BOOL CFSGameUserDarkMarketEvent::CheckUseCash(int iUseCash, int iBonusCnt)
{
	CHECK_CONDITION_RETURN(CLOSED == DARKMARKET.IsOpen(), FALSE);
	CHECK_CONDITION_RETURN((iUseCash * PAYBACK_DIAMOND_PERCENT / 100) <= 0, FALSE);

	SS2C_DARKMARKET_GIVE_DIAMOND_NOT	ss;
	ss.iGiveDiamond = (iUseCash * PAYBACK_DIAMOND_PERCENT / 100) + iBonusCnt;

	CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pODBC);

	if(ODBC_RETURN_SUCCESS != pODBC->EVENT_DARKMARKET_UseCash(m_pUser->GetUserIDIndex(), iUseCash, ss.iGiveDiamond))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INITIALIZE_DATA, FAIL, "EVENT_DARKMARKET_UseCash\tUserIDIndex:10752790, iGiveDiamond:0", m_pUser->GetUserIDIndex(), ss.iGiveDiamond);
urn FALSE;
	}

	m_iMyDiamond += ss.iGiveDiamond;

	m_pUser->Send(S2C_DARKMARKET_GIVE_DIAMOND_NOT, &ss, sizeof(SS2C_DARKMARKET_GIVE_DIAMOND_NOT));

	return TRUE;
}

BOOL CFSGameUserDarkMarketEvent::BuyDiamondProduct(BYTE btProductIndex)
{
	CHECK_CONDITION_RETURN(FALSE == m_bDataLoaded, FALSE);

	SS2C_DARKMARKET_BUY_PRODUCT_RES	ss;
	ZeroMemory(&ss, sizeof(SS2C_DARKMARKET_BUY_PRODUCT_RES));

	int iDiamondPrice = 0, iRewardIndex = 0;

	if(CLOSED == DARKMARKET.IsOpen())
	{
		ss.btResult = RESULT_BUY_PRODUCT_FAIL_EVENT_CLOSED;
	}
	else if(btProductIndex >= DIAMOND_PRODUCT_CNT || FALSE == DARKMARKET.GetDiamondProductInfo(btProductIndex, iDiamondPrice, iRewardIndex))
	{
		ss.btResult = RESULT_BUY_PRODUCT_FAIL_WRONG_PRODUCT;
	}
	else if(m_iMyDiamond < iDiamondPrice)
	{
		ss.btResult = RESULT_BUY_PRODUCT_FAIL_LACK_DIAMOND;
	}
	else if(0 == m_iDiamondProductRemainCnt[btProductIndex])
	{
		ss.btResult = RESULT_BUY_PRODUCT_FAIL_NO_REMAIN_CNT;
	}
	else if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + 1 > MAX_PRESENT_LIST)
	{
		ss.btResult = RESULT_BUY_PRODUCT_FAIL_FULL_MAILBOX;
	}
	else
	{
		CFSEventODBC* pODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
		CHECK_NULL_POINTER_BOOL(pODBC);

		ss.btResult = RESULT_BUY_PRODUCT_FAIL;

		int iRet = pODBC->EVENT_DARKMARKET_BuyProduct(m_pUser->GetUserIDIndex(), DIAMOND_MARKET, btProductIndex);

		if(ODBC_RETURN_SUCCESS == iRet)
		{			
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(pReward == NULL)
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, NONE, "BuyDiamondProduct REWARDMANAGER.GiveReward Fail, UserIDIndex:10752790, ProductIndex:0", m_pUser->GetUserIDIndex(), btProductIndex);

		else
			{
				m_iMyDiamond -= iDiamondPrice;
				if(-1 != m_iDiamondProductRemainCnt[btProductIndex])
				{
					-- m_iDiamondProductRemainCnt[btProductIndex];
				}

				ss.btResult = RESULT_BUY_PRODUCT_SUCCESS;
			}
		}
		else
		{
			ss.btResult = static_cast<BYTE>(iRet);
		}
	}

	m_pUser->Send(S2C_DARKMARKET_BUY_PRODUCT_RES, &ss, sizeof(SS2C_DARKMARKET_BUY_PRODUCT_RES));

	return TRUE;
}

BOOL CFSGameUserDarkMarketEvent::BuyPackageProduct_AfterPay(SBillingInfo* pBillingInfo, int iPayResult)
{
	CHECK_CONDITION_RETURN(PAY_RESULT_SUCCESS != iPayResult, FALSE);

	if(FALSE != ::IsBadReadPtr(pBillingInfo, sizeof(SBillingInfo)))
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, INVALED_DATA, CHECK_FAIL, "BuyPackageProduct_AfterPay - pBillingInfo has Bad_Pointer \n");
		return FALSE;
	}

	int iItemCode = pBillingInfo->iItemCode;
	int iPrevCash = pBillingInfo->iCurrentCash;
	int iPostCash = iPrevCash - pBillingInfo->iCashChange;
	int	iBuyCount = -1;
	int iErrorCode = BUY_ITEM_ERROR_GENERAL;
	int iErrorCode_SendItem = SEND_ITEM_ERROR_GENERAL;

	BYTE btProductIndex = static_cast<BYTE>(pBillingInfo->iParam1);
	CHECK_CONDITION_RETURN(btProductIndex >= PACKAGE_PRODUCT_CNT, FALSE);

	vector<int/*iRewardIndex*/>	vReward;
	CHECK_CONDITION_RETURN(FALSE == DARKMARKET.GetPackageRewardList(iItemCode, vReward), FALSE);

	char szReward[MAX_EVENT_REWARD_LENGTH+1] = {NULL,};
	ConvertRewardIndexStr(szReward, &vReward, vReward.size(), MAX_EVENT_REWARD_LENGTH);
	list<int> listPresentIndex; 

	CFSEventODBC* pEventODBC = (CFSEventODBC*)ODBCManager.GetODBC(ODBC_EVENT);
	CHECK_NULL_POINTER_BOOL(pEventODBC);

	CFSODBCBase* pBaseODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pBaseODBC);

	if(ODBC_RETURN_SUCCESS == pEventODBC->EVENT_DARKMARKET_BuyProduct(m_pUser->GetUserIDIndex(), PACKAGE_MARKET, btProductIndex) &&
		ODBC_RETURN_SUCCESS == pBaseODBC->EVENT_BuyEventCashItemAndGiveReward(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iItemCode, iBuyCount, iPrevCash, iPostCash, pBillingInfo->iItemPrice, EVENT_KIND_DARKMARKET, 0, listPresentIndex, szReward))
	{
		if(0 < m_iPackageProductRemainCnt[btProductIndex])
		{
			-- m_iPackageProductRemainCnt[btProductIndex];
		}

		m_pUser->RecvPresentList(MAIL_PRESENT_ON_ACCOUNT, listPresentIndex);

		iErrorCode = BUY_ITEM_ERROR_SUCCESS;
		iErrorCode_SendItem = SEND_ITEM_ERROR_SUCCESS;

		int iCostType = pBillingInfo->iSellType;
		m_pUser->SetUserBillResultAtMem(iCostType, iPostCash, 0, 0, pBillingInfo->iBonusCoin);
	}
	else
	{
		WRITE_LOG_NEW(LOG_TYPE_EVENT, LA_DEFAULT, NONE, "EVENT_BuyEventCashItemAndGiveReward Fail, UserIDIndex:10752790, ProductIndex:0", m_pUser->GetUserIDIndex(), btProductIndex);

// 아이템 구매요청 Result 패킷 보냄.
	CPacketComposer PacketComposer(S2C_ITEM_SELECT_RES);
	PacketComposer.Add(FS_ITEM_OP_BUY_EVENT_DARKMARKET_PACKAGE);
	PacketComposer.Add(iErrorCode);
	PacketComposer.Add(iItemCode);
	PacketComposer.Add(BUY_ITEM_END_OPERATION);				// EndOp			
	PacketComposer.Add((BYTE)iErrorCode_SendItem);			
	PacketComposer.Add((BYTE*)pBillingInfo->szRecvGameID, MAX_GAMEID_LENGTH + 1);
	m_pUser->Send(&PacketComposer);

	m_pUser->SendUserGold();

	return TRUE;
}

BOOL CFSGameUserDarkMarketEvent::CheckCanBuyPackageProduct(int iPackageItemCode, BYTE& btProductIndex, int& iBonusCnt, int& iErrorCode)
{
	if(CLOSED == DARKMARKET.IsOpen())
	{
		iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
		return FALSE;
	}

	int iProductCnt = 0;

	if(FALSE == DARKMARKET.GetPackageProductInfo(iPackageItemCode, btProductIndex, iProductCnt, iBonusCnt))
	{
		iErrorCode = BUY_ITEM_ERROR_NOT_SELL;
		return FALSE;
	}

	if(btProductIndex >= PACKAGE_PRODUCT_CNT || 0 == m_iPackageProductRemainCnt[btProductIndex])
	{
		iErrorCode = BUY_ITEM_ERROR_LIMITED_EDITION_ERROR;
		return FALSE;
	}

	if(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() + iProductCnt > MAX_PRESENT_LIST)
	{
		iErrorCode = BUY_ITEM_ERROR_PRESENT_LIST_FULL;
		return FALSE;
	}

	return TRUE;
}
