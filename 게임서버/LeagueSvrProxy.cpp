qinclude "stdafx.h"
qinclude "LeagueSvrProxy.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CFSGameServer.h"
qinclude <TBuffer>
qinclude "FSCommon.h"
qinclude "HackingManager.h"
qinclude "CFSGameClient.h"
qinclude "UserAppraisal.h"
qinclude "CenterSvrProxy.h"
qinclude "ClubSvrProxy.h"
qinclude "LogSvrProxy.h"
qinclude "RankMatchManager.h"
qinclude "ODBCSvrProxy.h"

#define DEFINE_LEAGUEPROXY_PROC_FUNC(x) void CLeagueSvrProxy::Proc_##x(CReceivePacketBuffer* rp)

CLeagueSvrProxy::CLeagueSvrProxy(void)
	:m_bIsAutoTeam(TRUE)
{
	SetState(FS_LEAGUE_SERVER_PROXY);
}


CLeagueSvrProxy::~CLeagueSvrProxy(void)
{
}

void CLeagueSvrProxy::ProcessPacket( CReceivePacketBuffer* recvPacket )
{
#ifndef CASE_LEAGUE_PROXY_PACKET_PROC
#define CASE_LEAGUE_PROXY_PACKET_PROC(x)	case x:		Proc_##x(recvPacket);	break;
#endif

	switch(recvPacket->GetCommand())
	{
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_RANKMATCH_READY_TIME_INFO_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_RANKMATCH_MATCHING_COMPLATED_REQ);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_USER_APPRAISAL_UPDATE_REQ);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_USER_APPRAISAL_RES);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_ROOMCHAR_RES);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_CLIENT_INFO);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_MESSAGE_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_LEAGUE_LOCATION_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_HACKING_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_AUTO_TEAM_REG_RES);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_RANKMATCH_PLAYERS_WAITING_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_RANKMATCH_ROOM_READY_RES);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_UPDATE_RESULT_DB_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_UPDATE_RATING_POINT_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_UPDATE_SEASON_RECORD_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_USER_APPRAISAL_UPDATE_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_USER_UPDATE_RECORD_DB_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_USER_ABUSEPOINT_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_USER_UPDATE_LV_EXP_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_HOTGIRLTIME_SHOUT_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_CLUB_GAME_RESULT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_UPDATE_RATING_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_RANKMATCH_GAME_EXIT_RES);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_RANKMATCH_GAME_EXIT_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_RANKMATCH_USER_CLOSE_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_HOST_SERVER_INFO_RES);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_DELETE_EXHAUSTEDITEM_NOT);
		CASE_LEAGUE_PROXY_PACKET_PROC(L2G_DECREASE_EXHAUSTEDITEM_NOT);
	default:
		{
			CMatchSvrProxy::ProcessPacket(recvPacket);
		}
		break;
	}
}

void CLeagueSvrProxy::SendFirstPacket()
{
	CPacketComposer PacketComposer(G2L_CLIENT_INFO);
	SS2L_CLIENT_INFO info;

	CFSGameServer* pServer = (CFSGameServer*)GetServer();

	info.iProcessID = pServer->GetProcessID();	
	info.iServerType = pServer->GetServerType();
	info.btServerIndex = pServer->GetServerIndex();

	PacketComposer.Add((BYTE*)&info, sizeof(SS2L_CLIENT_INFO));

	Send(&PacketComposer);	
}

void CLeagueSvrProxy::SetAutoTeam( BYTE btAutoTeam )
{
	m_bIsAutoTeam = (BOOL)btAutoTeam;
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_RANKMATCH_READY_TIME_INFO_NOT)
{
	SL2G_RANKMATCH_READY_TIME_INFO_NOT info;
	rp->Read((PBYTE)&info, sizeof(SL2G_RANKMATCH_READY_TIME_INFO_NOT));

	CScopedRefGameUser user(info.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_RANKMATCH_READY_TIME_INFO_NOT	not;

		not.shWaitTime = info.shWaitTime;
		CPacketComposer	Packet(S2C_RANKMATCH_READY_TIME_INFO_NOT);
		Packet.Add((PBYTE)&not, sizeof(not));

		user->Send(&Packet);
	}		
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_RANKMATCH_MATCHING_COMPLATED_REQ)
{
	SL2G_RANKMATCH_MATCHING_COMPLETED_REQ info;
	rp->Read((PBYTE)&info, sizeof(SL2G_RANKMATCH_MATCHING_COMPLETED_REQ));

	CScopedRefGameUser user(info.iGameIDIndex);
	if(user != NULL)
	{
		SS2C_RANKMATCH_MATCHING_COMPLETED_REQ	req;

		req.btResult = info.btResult;
		req.btTime = info.btTime;
		
		user->Send(S2C_RANKMATCH_MATCHING_COMPLETED_REQ, &req, sizeof(SS2C_RANKMATCH_MATCHING_COMPLETED_REQ));
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_USER_APPRAISAL_UPDATE_REQ)
{
	SL2G_USER_APPRAISAL_UPDATE_REQ info;
	rp->Read((PBYTE)&info, sizeof(SL2G_USER_APPRAISAL_UPDATE_REQ));

	SG2L_USER_APPRAISAL_UPDATE_RES rs;
	rs.iGameIDIndex = info.iGameIDIndex;
	rs.btServerIndex = info.btServerIndex;
	rs.btMatchType = info.btMatchType;
	rs.iReqGameIDIndex = info.iReqGameIDIndex;
	rs.btReqServerIndex = info.btReqServerIndex;
	rs.btKind = info.btKind;
	rs.iRoomID = info.iRoomID;
	
	int iPoint[MAX_USER_APPRAISAL_KIND_COUNT];
	ZeroMemory(iPoint, sizeof(int)*MAX_USER_APPRAISAL_KIND_COUNT);
	iPoint[info.btKind] = info.iPoint;

	if(FALSE == USERAPPRAISAL.UpdateUserAppraisal(info.iReqGameIDIndex, iPoint, info.iGameIDIndex, rs.iUpdateDate))
	{
		rs.btResult = RESULT_USER_APPRAISAL_FAIL;
		return;
	}

	CScopedRefGameUser user(info.iReqGameIDIndex);
	if(user != NULL)
	{
		user->GetUserAppraisal()->UpdatePoint();

		if(info.btKind >= USER_APPRAISAL_KIND_PRAISE && info.btKind <= MAX_USER_APPRAISAL_KIND_COUNT)
		{
			SS2C_USER_APPRAISAL_RECIVE_NOT not;
			not.btKind = info.btKind;
			user->Send(S2C_USER_APPRAISAL_RECIVE_NOT, &not, sizeof(SS2C_USER_APPRAISAL_RECIVE_NOT));
		}
	}

	rs.btResult = RESULT_USER_APPRAISAL_SUCCESS;

	CPacketComposer Packet(G2L_USER_APPRAISAL_UPDATE_RES);
	Packet.Add((PBYTE)&rs, sizeof(SG2L_USER_APPRAISAL_UPDATE_RES));
	Send(&Packet);	
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_USER_APPRAISAL_RES)
{
	//SL2G_USER_APPRAISAL_RES info;
	//rp->Read((PBYTE)&info, sizeof(SL2G_USER_APPRAISAL_RES));

	//CScopedRefGameUser user(info.iGameIDIndex);
	//if(user != NULL)
	//{
	//	user->GetUserAppraisal()->AddAppraisalUser(info.iReqGameIDIndex, info.iUpdateDate);

	//	SS2C_USER_APPRAISAL_RES rs;
	//	rs.btResult = info.btResult;
	//	rs.btMatchType = info.btMatchType;
	//	rs.btServerIndex = info.btReqServerIndex;
	//	rs.iGameIDIndex = info.iReqGameIDIndex;
	//	user->Send(S2C_USER_APPRAISAL_RES, &rs, sizeof(SS2C_USER_APPRAISAL_RES));
	//}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_ROOMCHAR_RES)
{
	int iGameIDIndex = 0;
	rp->Read(&iGameIDIndex);

	CScopedRefGameUser pUser(iGameIDIndex);

	if (pUser != NULL)
	{
		SL2G_ROOMCHAT_RES info ;
		char szText[MAX_CHATBUFF_LENGTH+1] = {0};

		rp->Read((PBYTE)&info, sizeof( SL2G_ROOMCHAT_RES));

		if( 0 > info.iChatSize || MAX_CHAT_LENGTH < info.iChatSize )
			return;

		rp->Read((PBYTE)szText, info.iChatSize);

		CHECK_CONDITION_RETURN_VOID( CHAT_TYPE_LEAGUE != info.iChatState );
		
		CPacketComposer Packet(S2C_CHAT);
		Packet.Add((int)info.iChatState);
		Packet.Add((BYTE)0);//GM
		Packet.Add((PBYTE)info.szGameID, MAX_GAMEID_LENGTH + 1 );	
		Packet.Add(&info.iChatSize);
		Packet.Add((PBYTE)szText, info.iChatSize + 1) ;

		pUser->Send(&Packet);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_CLIENT_INFO)
{
	SL2G_CLIENT_INFO	info;

	rp->Read((PBYTE)&info, sizeof(SL2G_CLIENT_INFO));

	SetAutoTeam(info.btIsAutoRegTeam);
		
	CFSGameServer::GetInstance()->BroadCastLeagueServerConnect(this);
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_MESSAGE_NOT)
{
	SL2G_MESSAGE_NOT	info;

	rp->Read((PBYTE)&info, sizeof(SL2G_MESSAGE_NOT));

	SS2C_RANKMATCH_MESSAGE_NOT	not;

	not.btFlag = info.btMessageNot;

	CPacketComposer	Packet(S2C_RANKMATCH_MESSAGE_NOT);
	Packet.Add((PBYTE)&not, sizeof(SS2C_RANKMATCH_MESSAGE_NOT));

	CScopedRefGameUser pUser(info.iGameIDIndex);
	if (pUser != NULL)
	{
		pUser->Send(&Packet);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_LEAGUE_LOCATION_NOT)
{
	SL2G_LEAGUE_LOCATION_NOT info;

	rp->Read((PBYTE)&info, sizeof(SL2G_LEAGUE_LOCATION_NOT));

	CScopedRefGameUser User(info.iGameIDIndex);
	CHECK_NULL_POINTER_VOID(User);

	//User->GetMatchLocation();
	if (TRUE == info.btIsLogon)
	{
		if (0 < User->GetMatchLocation() && 
			GetProcessID() != User->GetMatchLocation())
		{
			//이전 매치서버 정보가 있으면 로그아웃시킴
			CMatchBaseSvrProxy* pOldProxy = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(User->GetMatchLocation());
			if (NULL != pOldProxy) 
				pOldProxy->SendUserLogout(User.GetPointer());
		}
		User->SetMatchLocation(GetProcessID());
		User->SendAvatarInfoUpdateToMatch();
	}
	else
	{
		if (GetProcessID() == User->GetMatchLocation())
			User->SetMatchLocation(-1);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_HACKING_NOT)
{
	SL2G_HACKING_NOT info;

	rp->Read((PBYTE)&info, sizeof(SL2G_HACKING_NOT));

	CScopedRefGameUser user(info.iGameIDIndex);

	if( user != nullptr )
	{
		HACKINGMANAGER.TakeActionAgainstHack((int)info.btHackType, user.GetPointer(), info.szLog );
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_AUTO_TEAM_REG_RES)
{
	SL2G_AUTO_TEAM_REG_RES rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_AUTO_TEAM_REG_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);
	
	if( user != nullptr )
	{
		if (RESULT_AUTO_TEAM_REG_SUCCESS != rs.info.btResult)
		{
			user->SetMatching(FALSE);
		}

		user->Send(S2C_AUTO_TEAM_REG_RES, &rs.info, sizeof(SS2C_AUTO_TEAM_REG_RES));
	}
	else if( REG_OK == rs.info.btRegMode )
	{
		SendUserLogout(user.GetPointer());
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_RANKMATCH_PLAYERS_WAITING_NOT)
{
	int iGameIDIndex = 0;

	rp->Read(&iGameIDIndex);

	SL2G_RANKMATCH_PLAYERS_WAITING_NOT	not;
	rp->Read((PBYTE)&not , sizeof(SL2G_RANKMATCH_PLAYERS_WAITING_NOT));

	vector<SRANKMATCH_PLAYERS_WAITING_INFO>	vecinfo;
	for( size_t index = 0 ; index < not.btUserCount ; ++index )
	{
		SRANKMATCH_PLAYERS_WAITING_INFO	userinfo;
		rp->Read((PBYTE)&userinfo, sizeof(SRANKMATCH_PLAYERS_WAITING_INFO));

		vecinfo.push_back(userinfo);
	}

	CScopedRefGameUser	user(iGameIDIndex);
	if( user != NULL )
	{
		if(RANKMATCH_PLAYERS_WAITING_STATUS_SUCCESS == not.btResult)
		{
			CFSGameClient* pClient = (CFSGameClient*)user->GetClient();
			if( NULL != pClient )
			{
				pClient->SetState(FS_TEAM);
			}
		}

		CPacketComposer Packet(S2C_RANKMATCH_PLAYERS_WAITING_NOT);

		SS2C_RANKMATCH_PLAYERS_WAITING_NOT info;

		info.btResult = not.btResult;
		info.btTime = not.btTime;
		info.btUserCount = not.btUserCount;

		Packet.Add((PBYTE)&info, sizeof(SS2C_RANKMATCH_PLAYERS_WAITING_NOT));

		for( auto iter = vecinfo.begin() ; iter != vecinfo.end() ; ++iter )
		{
			Packet.Add((PBYTE)&(*iter), sizeof(SRANKMATCH_PLAYERS_WAITING_INFO));
		}

		user->Send(&Packet);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_RANKMATCH_ROOM_READY_RES)
{
	int iGameIDIndex = 0;
	rp->Read(&iGameIDIndex);

	CScopedRefGameUser user(iGameIDIndex);

	if( user != nullptr )
	{
		CPacketComposer Packet(S2C_RANKMATCH_ROOM_READY_RES);
		Packet.Add(rp->GetRemainedData(), rp->GetRemainedDataSize());
		user->Send(&Packet);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_UPDATE_RESULT_DB_NOT)
{
	SL2G_UPDATE_RESULT_DB_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_UPDATE_RESULT_DB_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
		CFSODBCBase* pODBCBase = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
		CHECK_NULL_POINTER_VOID(pODBCBase);

		int iRequireLv = 0 , iBonusPoint = 0;
		pODBCBase->spFSUpdateLvExp(user->GetUserIDIndex(),
			rs.iGameIDIndex, 
			user->GetCurUsedAvtarLv(), 
			rs.btMaxLvStep, 
			rs.iExpPenalty,
			user->GetCurUsedAvtarFameLevel(), 
			0, 
			rs.iSkillPointPenalty, 
			rs.iDisconnected, 
			iRequireLv, 
			iBonusPoint);
	}

}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_UPDATE_RATING_POINT_NOT)
{
	SL2G_UPDATE_RATING_POINT_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_UPDATE_RATING_POINT_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);

	RANKMATCH.UpdateAvatarRatingInfo(rs.iGameIDIndex, rs.fRatingPoint);

	if( user != nullptr )
	{
		user->GetUserRankMatch()->SetLeagueRatingPoint(rs.fRatingPoint);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_UPDATE_SEASON_RECORD_NOT)
{
	SL2G_UPDATE_SEASON_RECORD_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_UPDATE_SEASON_RECORD_NOT));

	CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_NULL_POINTER_VOID(pRankODBC);

	SFSGameRecord GameRecord;

	pRankODBC->UpdateSeasonRecord(
		rs.iGameIDIndex, 
		rs.btLeague,
		rs.btRecordType, 
		rs.iCurrentSeasonIndex, 0, 0, 
		GameRecord, 4);
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_USER_APPRAISAL_UPDATE_NOT)
{
	SL2G_USER_APPRAISAL_UPDATE_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_USER_APPRAISAL_UPDATE_NOT));

	int iUpdateDate = 0;
	char szGameID[MAX_GAMEID_LENGTH+1] = {NULL};
	USERAPPRAISAL.UpdateUserAppraisal(rs.iGameIDIndex, rs.iPoint, rs.iReqGameIDIndex, iUpdateDate);
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_USER_UPDATE_RECORD_DB_NOT)
{
	SL2G_USER_UPDATE_RECORD_DB_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_USER_UPDATE_RECORD_DB_NOT));

	CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_NULL_POINTER_VOID(pRankODBC);

	pRankODBC->UpdateSeasonRecord(
		rs.iGameIDIndex
		, rs._ucLeague
		, rs._ucRecordType
		, rs._iSeasonIndex
		, rs._iWin
		, rs._iLose
		, rs._GameRecord
		, rs._iDisconnectPoint);
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_USER_ABUSEPOINT_NOT)
{
	SL2G_USER_ABUSEPOINT_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_USER_ABUSEPOINT_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
		CFSRankODBC* pRankODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
		CHECK_NULL_POINTER_VOID(pRankODBC);
		
		SAvatarInfo* pAvatarInfo = user->GetCurUsedAvatar();
		if( nullptr != pAvatarInfo && pAvatarInfo->DisconnectPoint > 0)
		{
			if(ODBC_RETURN_SUCCESS == pRankODBC->spUpdateAbusePoint(rs.iGameIDIndex))
			{
				pAvatarInfo->DisconnectPoint--;
			}
		}		
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_USER_UPDATE_LV_EXP_NOT)
{
	SL2G_USER_UPDATE_LV_EXP_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_USER_UPDATE_LV_EXP_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
// 		CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
// 		CHECK_NULL_POINTER_VOID(pODBCBase);
// 
// 		int iRequireLv = 0, iBonusPoint = 0;
// 		if( ODBC_RETURN_FAIL == pODBCBase->spFSUpdateLvExp(	user->GetUserIDIndex(), rs.iGameIDIndex, rs.iLv, rs.btMaxLevelStep,
// 			rs.iExp, rs.iFameLv, rs.iFamePoint,	rs.iSkillPoint, rs.iDisconnected, iRequireLv, iBonusPoint))
// 		{
// 			WRITE_LOG_NEW(LOG_TYPE_USER, DB_DATA_UPDATE, FAIL, "SP_UpdateUserLvExp In UpdateUserLvExpDB GameIDIndex =\t10752790", rs.iGameIDIndex);

// 		else
// 		{
			if( TRUE == rs.bLvUp )
			{
				if( 0 < rs.iClubSN )
				{
					SM2CL_CLUB_UDPATE_MEMBER_LV info;
					info.iClubIndex = rs.iClubSN;
					info.iGameIDIndex = rs.iGameIDIndex;
					info.iUpdatedLv = rs.iLv;

					CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
					if( nullptr != pClubProxy )
					{
						CPacketComposer Packet(M2CL_CLUB_UDPATE_MEMBER_LV);
						Packet.Add((PBYTE)&info, sizeof(SM2CL_CLUB_UDPATE_MEMBER_LV));
						pClubProxy->Send(&Packet); 
					}
				}

// 				if(ODBC_RETURN_SUCCESS != pODBCBase->spFSUpdateStat(rs.iGameIDIndex, rs.iaAddStat))
// 				{
// 					WRITE_LOG_NEW(LOG_TYPE_USER, DB_DATA_UPDATE, FAIL, "UpdateUserStatDB In UpdateUserLvExpDB GameIDIndex =\t10752790", rs.iGameIDIndex);
	}
			}

// 			user->SetBonusPointTerms(iRequireLv, iBonusPoint);
// 
// 			if( 0 < rs.iFactionPoint )
// 			{
// 				pODBCBase->FACTION_UpdateUserFactionPoint(user->GetUserIDIndex(), rs.iFactionPoint);
// 			}
// 
// 			if( 0 < rs.iFactionScore )
// 			{
// 				pODBCBase->FACTION_UpdateUserFactionPoint(user->GetUserIDIndex(), rs.iFactionScore);
// 			}
// 		}
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_HOTGIRLTIME_SHOUT_NOT)
{
	SL2G_HOTGIRLTIME_SHOUT_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_HOTGIRLTIME_SHOUT_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{

		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if( NULL != pCenter) 
		{
		 	CPacketComposer	Packet(A2S_HOTGIRLTIME_SHOUT_NOT);
		 	SA2S_HOTGIRLTIME_SHOUT_NOT info;
		 	strncpy_s(info.GameID, _countof(info.GameID), user->GetGameID(), _countof(info.GameID)-1);
		 	info.iGameIDIndex = user->GetGameIDIndex();
		 	info.iRewardIndex = rs.iRewardIndex;
		 
		 	Packet.Add((PBYTE)&info, sizeof(SA2S_HOTGIRLTIME_SHOUT_NOT));
		 
		 	pCenter->Send(&Packet);
		}
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_CLUB_GAME_RESULT)
{
	SL2G_CLUB_GAME_RESULT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_CLUB_GAME_RESULT));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
		if( 0 < user->GetClubSN() )
		{
			CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
			if( nullptr != pClubProxy )
			{
				CPacketComposer Packet(G2CL_LEAGUE_GAME_RESULT);
				
				SG2CL_LEAGUE_GAME_RESULT	result;

				memcpy(result.szGameID, user->GetGameID() , MAX_GAMEID_LENGTH+1);
				result.iClubSN = user->GetClubSN();
				result.iMatchType = rs.iMatchType;
				result.iGameCount = rs.iGameCount;
				memcpy(&result.sMatchResult, &rs.sMatchResult, sizeof(SClubMatchResult));
				memcpy(&result.sMatchDetailResult, &rs.sMatchDetailResult, sizeof(SClubMatchDetailResult));
				result.sMatchDetailResult.iGiveClubLeaguePoint = 0;
				result.sMatchDetailResult.iGiveUserClubLeaguePoint = 0;

				Packet.Add((PBYTE)&result, sizeof(SG2CL_LEAGUE_GAME_RESULT));

				pClubProxy->Send(&Packet);
			}
		}
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_UPDATE_RATING_NOT)
{
	SL2G_UPDATE_RATING_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_UPDATE_RATING_NOT));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
		CLogSvrProxy* pLogSvrProxy = (CLogSvrProxy*)GAMEPROXY.GetProxy(FS_LOG_SERVER_PROXY);
		if(nullptr != pLogSvrProxy)
		{
			CPacketComposer Packet(M2L_RATING_DETAIL_RECORD);
			Packet.Add((PBYTE)&rs.rs, sizeof(SM2L_RATING_DETAIL_RECORD));
			pLogSvrProxy->Send(&Packet);
		}

		RANKMATCH.UpdateAvatarRatingInfo(user->GetGameIDIndex(), rs.fRatingPoint);
		user->GetUserRankMatch()->SetLeagueRatingPoint(rs.fRatingPoint);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_RANKMATCH_GAME_EXIT_RES)
{
	SL2G_RANKMATCH_GAME_EXIT_RES rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_RANKMATCH_GAME_EXIT_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
		SS2C_RANKMATCH_GAME_EXIT_RES info;

		info.btResult = rs.btResult;

		user->Send(S2C_RANKMATCH_GAME_EXIT_RES, &info , sizeof(SS2C_RANKMATCH_GAME_EXIT_RES));

		if( TRUE == info.btResult )
		{
			user->SetLocation(U_LOBBY);
			user->GetClient()->SetState(NEXUS_LOBBY);

			if (GetProcessID() == user->GetMatchLocation())
				user->SetMatchLocation(-1);
		}
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_RANKMATCH_GAME_EXIT_NOT)
{
	SL2G_RANKMATCH_GAME_EXIT_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_RANKMATCH_GAME_EXIT_NOT));
	rs.szGameID[MAX_GAMEID_LENGTH] = 0;

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
		CPacketComposer	Packet(S2C_RANKMATCH_GAME_EXIT_NOT);
		SS2C_RANKMATCH_GAME_EXIT_NOT	not;

		strncpy_s(not.szGameID, _countof(not.szGameID), rs.szGameID, _countof(not.szGameID)-1);
		not.btServerIndex = rs.btServerIndex;

		Packet.Add((PBYTE)&not, sizeof(SS2C_RANKMATCH_GAME_EXIT_NOT));	

		user->Send(&Packet);
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_RANKMATCH_USER_CLOSE_NOT)
{
	SL2G_RANKMATCH_USER_CLOSE_NOT rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_RANKMATCH_USER_CLOSE_NOT));

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);
	
	// 게임중 아니면 방 깨고 나감
	int iPoint = 0;
	if( LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST == rs.sInfo.iSubIndex ) // 배치고사
	{
		iPoint = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_UserCloseLeaguePoint_Test);
	}
	else
	{
		iPoint = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_UserCloseLeaguePoint); // 통일
	}

	int iPrevLeaguePoint = rs.sInfo.iLeaguePoint;
	bool bcheckleaguepoint = false;

	if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_UpdateAvatarTerminate(rs.iGameIDIndex, iPoint))
		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_UpdateAvatarTerminate (close) GameIDIndex = 10752790, DownPoint = 0", rs.iGameIDIndex, iPoint);
( LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING == rs.sInfo.iSubIndex ) // 승강전
	{
		SLeagueSubTable subTable;
		subTable.Init();

		if( 100 == rs.sInfo.iLeaguePoint ) // 승급전 중일땐
		{
			int iWinCondition = 0;
			if( RANKMATCH_RANK_RATING_1 == rs.sInfo.btRating ) // 티어 승급이라면
			{
				iWinCondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Advancement_Tier_WinCondition);
			}
			else
			{
				iWinCondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Advancement_Rating_WinCondition);
			}

			int iLoseCount = 0;
			for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
			{
				if( LEAGUE_SUB_TABLE_OUTCOME_NONE == rs.sInfo.btOutCome[i] || 
					LEAGUE_SUB_TABLE_OUTCOME_NOTYET == rs.sInfo.btOutCome[i])
				{
					rs.sInfo.btOutCome[i] = LEAGUE_SUB_TABLE_OUTCOME_LOSE;
					++iLoseCount;
					break;
				}

				if( LEAGUE_SUB_TABLE_OUTCOME_LOSE == rs.sInfo.btOutCome[i])
					++iLoseCount;
			}

			if( iLoseCount >= iWinCondition )
			{ // 강종때문에 승급 실패했다면.
				rs.sInfo.iLeaguePoint = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Advancement_LeaguePoint_Fail);
								
				subTable.Init();
				subTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_NONE;
			}
			else
			{
				subTable.Init();
				subTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING;
				
				for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT; ++i )
				{
					subTable.btOutCome[i] = rs.sInfo.btOutCome[i];
				}
			}

			RANKMATCH.UpdateAvatarSubTable(rs.iGameIDIndex, &subTable);
		}
		else  // 강등전 중일땐
		{
			int iLoseCondition = 0;
			if( RANKMATCH_RANK_RATING_5 == rs.sInfo.btRating || 
				LEAGUE_MAIN_CATEGORY_DIAMOND == rs.sInfo.btRating ) // 티어 하락이라면
			{
				iLoseCondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Demotion_Tier_LoseCondition);
			}
			else
			{
				iLoseCondition = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Demotion_Rating_LoseCondition);
			}

			int iLoseContinue = 0;
			for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
			{
				if( rs.sInfo.btOutCome[i] == LEAGUE_SUB_TABLE_OUTCOME_LOSE )
				{
					iLoseContinue++;
				}
				else
					break; // 강등은 연패를 봐야함
			}

			if( iLoseContinue >= iLoseCondition )
			{ // 강등
				subTable.Init();

				subTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_NONE;

				if( RANKMATCH_RANK_RATING_5 == rs.sInfo.btRating || 
					LEAGUE_MAIN_CATEGORY_DIAMOND == rs.sInfo.btTier) // 티어 하락이라면
				{
					if( LEAGUE_MAIN_CATEGORY_BRONZE < rs.sInfo.btTier )
					{
						rs.sInfo.btTier--;
						rs.sInfo.btRating = RANKMATCH_RANK_RATING_1;
						rs.sInfo.iLeaguePoint = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Demotion_LeaguePoint_Success);
						rs.sInfo.iAddPoint = 0;
					}
				}
				else
				{
					if( RANKMATCH_RANK_RATING_5 > rs.sInfo.btRating )
					{
						rs.sInfo.btRating++;
						rs.sInfo.iLeaguePoint = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_Demotion_LeaguePoint_Success);
						rs.sInfo.iAddPoint = 0;
					}
				}

				CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
				SS2O_RANKMATCH_INSERT_BASIC_JUST_LEAGUE_INFO_NOT not;

				not.iGameIDIndex = rs.iGameIDIndex;
				not.btServerIndex = (BYTE)_GetServerIndex;
				not.btTier = rs.sInfo.btTier;
				not.btRating = rs.sInfo.btRating;
				not.iGroup = rs.sInfo.iGroup;
				not.iLeaguePoint = rs.sInfo.iLeaguePoint;

				if( nullptr != pProxy )
				{
					CPacketComposer Packet(S2O_RANKMATCH_INSERT_BASIC_JUST_LEAGUE_INFO_NOT);
					Packet.Add((BYTE*)&not, sizeof(SS2O_RANKMATCH_INSERT_BASIC_JUST_LEAGUE_INFO_NOT));
					pProxy->Send(&Packet);
				}
				else
				{
					CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
					CHECK_NULL_POINTER_VOID(pODBC);

					if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_UpdateBasicJustLeagueInfo(not))
					{
						WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_UpdateBasicJustLeagueInfo GameIDIndex = 10752790 , ServerIndex = 0", not.iGameIDIndex, not.btServerIndex);
	return;
					}
					WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_UpdateBasicJustLeagueInfo LeagueSvrProxy Fail GameIDIndex = 10752790 , ServerIndex = 0 , Tier = 17051648 ", not.iGameIDIndex, not.btServerIndex, not.btTier);

			else
			{//강등안했다면.
				subTable.Init();
				subTable.iSubIndex = rs.sInfo.iSubIndex = LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING;

				for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
				{
					if( LEAGUE_SUB_TABLE_OUTCOME_NONE == rs.sInfo.btOutCome[i] || 
						LEAGUE_SUB_TABLE_OUTCOME_NOTYET == rs.sInfo.btOutCome[i])
					{
						subTable.btOutCome[i] = rs.sInfo.btOutCome[i] = LEAGUE_SUB_TABLE_OUTCOME_LOSE;
						break;
					}
					else
					{
						subTable.btOutCome[i] = rs.sInfo.btOutCome[i];
					}
				}
			}

			RANKMATCH.UpdateAvatarSubTable(rs.iGameIDIndex, &subTable);
		}
	}
	else if( LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST == rs.sInfo.iSubIndex ) // 배치고사
	{
		if( ROOM_GAME == rs.btRoomStatus ) // 대기방의 배치고사는 적용 안함.
		{
			if( 0 < rs.sInfo.btRemainPlacementTest )
				rs.sInfo.btRemainPlacementTest--;

			rs.sInfo.fTestRatingPoint -= static_cast<float>(iPoint);

			if( 0 > rs.sInfo.fTestRatingPoint )
				rs.sInfo.fTestRatingPoint = 0.f;

			SLeagueSubTable subTable;
			subTable.Init();

			if( 0 == rs.sInfo.btRemainPlacementTest ) // 배치고사 끝남
			{
				SLeagueRatingConfig Configinfo;
				CODBCSvrProxy* pODBCProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);

				SS2O_RANKMATCH_INSERT_BASIC_REQ req;

				req.iGameIDIndex = rs.iGameIDIndex;
				req.btServerIndex = _GetServerIndex;
				req.btTier = LEAGUE_MAIN_CATEGORY_BRONZE;
				req.btRating = RANKMATCH_RANK_RATING_5;
				req.iGroup = 0;
				req.iLeaguePoint = RANKMATCH.GetConfigValue(LEAGUE_CONFIG_StartLeaguePoint);
				strncpy_s(req.szGameID, _countof(req.szGameID), rs.szGameID, MAX_GAMEID_LENGTH);
				
				req.btGamePosition = rs.btGamePosition;

				if( nullptr != pODBCProxy &&
					TRUE == RANKMATCH.GetLeagueRatingInfo(rs.sInfo.fTestRatingPoint, Configinfo))
				{
					req.btTier = Configinfo.btTier;
					req.btRating = Configinfo.btRating;

					CPacketComposer Packet(S2O_RANKMATCH_INSERT_BASIC_REQ);
					Packet.Add((PBYTE)&req, sizeof(SS2O_RANKMATCH_INSERT_BASIC_REQ));
					pODBCProxy->Send(&Packet);

					bcheckleaguepoint = true;
				}
				else
				{
					RANKMATCH.GetLeagueRatingInfo(rs.sInfo.fTestRatingPoint, Configinfo);

					req.btTier = Configinfo.btTier;
					req.btRating = Configinfo.btRating;

					CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
					CHECK_NULL_POINTER_VOID(pODBC);

					if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_InsertBasic(req))
					{
						WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_InsertBasic GameIDIndex = 10752790 , ServerIndex = 0", req.iGameIDIndex, req.btServerIndex);
}
					WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_InsertBasic OdbcSvrFail GameIDIndex = 10752790 , ServerIndex = 0, Tier = 17051648", req.iGameIDIndex, req.btServerIndex, req.btTier);
		subTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_NONE;
				subTable.fTestRating = 0;
				subTable.btRemainPlacementTest = 0;
				subTable.bDbLoad = FALSE;

				ZeroMemory(subTable.btOutCome, sizeof(BYTE)*MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT );

			}
			else
			{
				subTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST;
				subTable.fTestRating = rs.sInfo.fTestRatingPoint;
				subTable.btRemainPlacementTest = rs.sInfo.btRemainPlacementTest;
				subTable.bDbLoad = FALSE;

				ZeroMemory(subTable.btOutCome, sizeof(BYTE)*MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT );

				for( int i = 0 ; i < MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT ; ++i )
				{
					if( LEAGUE_SUB_TABLE_OUTCOME_NONE == rs.sInfo.btOutCome[i] ||
						LEAGUE_SUB_TABLE_OUTCOME_NOTYET == rs.sInfo.btOutCome[i])
					{
						subTable.btOutCome[i] = LEAGUE_SUB_TABLE_OUTCOME_LOSE;
						break;
					}
					else
					{
						subTable.btOutCome[i] = rs.sInfo.btOutCome[i];
					}
				}
			}
			RANKMATCH.UpdateAvatarSubTable(rs.iGameIDIndex, &subTable);
		}
	}
	else
	{
		rs.sInfo.iLeaguePoint -= iPoint;

		if( 0 > rs.sInfo.iLeaguePoint )
		{
			rs.sInfo.iLeaguePoint = 0;

			if( rs.sInfo.btTier == LEAGUE_MAIN_CATEGORY_BRONZE && 
				rs.sInfo.btRating == RANKMATCH_RANK_RATING_5 )
			{}
			else
			{
				// 강등전
				SLeagueSubTable subTable;
				subTable.iSubIndex = LEAGUE_SUB_TABLE_INDEX_EVALUATION_RATING;
				subTable.fTestRating = 0.f;
				subTable.btRemainPlacementTest = 0;
				ZeroMemory(subTable.btOutCome, sizeof(BYTE)*MAX_LEAGUE_SUB_TABLE_OUTCOME_COUNT );
				RANKMATCH.UpdateAvatarSubTable(rs.iGameIDIndex, &subTable);
			}
		}
	}

	if( LEAGUE_SUB_TABLE_INDEX_PLACEMENT_TEST != rs.sInfo.iSubIndex &&
		iPrevLeaguePoint != rs.sInfo.iLeaguePoint && false == bcheckleaguepoint )
	{
		CODBCSvrProxy* pProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);

		SS2O_RANKMATCH_UPDATE_LEAGUEPOINT_NOT not;

		not.iGameIDIndex = rs.iGameIDIndex;
		not.iLeaguePoint = rs.sInfo.iLeaguePoint;
		not.iServerIndex = rs.btServerIndex;
		not.btIsTerminate = TRUE;

		if( nullptr != pProxy )
		{
			CPacketComposer Packet(S2O_RANKMATCH_UPDATE_LEAGUEPOINT_NOT);

			Packet.Add((PBYTE)&not, sizeof(SS2O_RANKMATCH_UPDATE_LEAGUEPOINT_NOT));

			pProxy->Send(&Packet);
		}
		else
		{
			CFSLeagueODBC* pODBC = (CFSLeagueODBC*)ODBCManager.GetODBC(ODBC_LEAGUE);
			CHECK_NULL_POINTER_VOID(pODBC);

			if( ODBC_RETURN_SUCCESS != pODBC->LEAGUE_Record_UpdateLeaguePoint(
				not.iGameIDIndex, not.iServerIndex, not.iLeaguePoint, not.btIsTerminate ))
			{
				WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_UpdateLeaguePoint GameIDIndex = 10752790 , ServerIndex = 0", not.iGameIDIndex, not.iServerIndex);


		WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_Record_UpdateLeaguePoint ODBCSvrProxy GameIDIndex = 10752790 , ServerIndex = 0", not.iGameIDIndex, not.iServerIndex);
	}

	if( rs.btRoomStatus == ROOM_GAME )
	{
		if( ODBC_RETURN_SUCCESS != pODBCBase->LEAGUE_UpdateAvatarTeamPositionSet(rs.iGameIDIndex, (int)rs.cRankMatchTeamIndex, (BYTE)FALSE /**/))
		{
			WRITE_LOG_NEW(LOG_TYPE_LEAGUE, DB_DATA_LOAD, FAIL, "LEAGUE_UpdateAvatarTeamPositionSet (close) GameIDIndex = 10752790", rs.iGameIDIndex);
}
	
	RANKMATCH.UpdateAvatarRatingInfo(rs.iGameIDIndex, rs.fRatingPoint);
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_HOST_SERVER_INFO_RES)
{
	SL2G_HOST_SERVER_INFO_RES rs;

	rp->Read((PBYTE)&rs, sizeof(SL2G_HOST_SERVER_INFO_RES));

	CScopedRefGameUser user(rs.iGameIDIndex);

	if( user != nullptr )
	{
		SS2C_USER_PING_CHECK_REQ	info;

		strncpy_s(info.szIPAddress, _countof(info.szIPAddress), rs.szIPAddress, _countof(info.szIPAddress)-1);
		info.wMainPort = rs.wMainPort;
		info.nP2PNetLibType = rs.nP2PNetLibType;
		info.iCheckBandwidthSize = rs.iCheckBandwidthSize;

		user->Send(S2C_USER_PING_CHECK_REQ,&info, sizeof(SS2C_USER_PING_CHECK_REQ));
	}

}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_DELETE_EXHAUSTEDITEM_NOT)
{
	SL2G_DELETE_EXHAUSTEDITEM_NOT info;

	rp->Read((PBYTE)&info, sizeof(SL2G_DELETE_EXHAUSTEDITEM_NOT));

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	int iItemCode = info.iItemCode;
	int iItemIndex = info.iItemIndex;
	int iChannel = 0;
	int iSlot = 0;

	if(ODBC_RETURN_SUCCESS == pODBCBase->spUpdateItemExhaust(info.iGameIDIndex, iChannel, iSlot, iItemIndex, iItemCode))
	{
		CScopedRefGameUser user(info.iGameIDIndex);
		if(user != NULL)
		{
			user->RemoveFeature(info.iIdx);
		}
	}
}

DEFINE_LEAGUEPROXY_PROC_FUNC(L2G_DECREASE_EXHAUSTEDITEM_NOT)
{
	SL2G_DECREASE_EXHAUSTEDITEM_NOT info;

	rp->Read((PBYTE)&info, sizeof(SL2G_DECREASE_EXHAUSTEDITEM_NOT));

	CFSODBCBase* pODBCBase = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_VOID(pODBCBase);

	int iItemCode = info.iItemCode;
	int iItemIndex = info.iItemIndex;

	if(ODBC_RETURN_SUCCESS == pODBCBase->spUpdateItemCount(info.iGameIDIndex, iItemIndex, iItemCode, info.iDecreaseCount))
	{
		CScopedRefGameUser user(info.iGameIDIndex);
		if(user != NULL)
		{
			user->DecreaseFeature(info.iIdx, info.iDecreaseCount);
		}
	}
}