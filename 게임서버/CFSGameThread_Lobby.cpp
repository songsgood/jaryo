qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "CFSLobby.h"
qinclude "ClubSvrProxy.h"
qinclude "CenterSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "ChatSvrProxy.h"
qinclude "GameCheat.h"
qinclude "CFSGameUserItem.h"
qinclude "GMSvrProxy.h"
qinclude "RewardManager.h"
qinclude "BingoManager.h"
qinclude "CRecordBoardManager.h"
qinclude "CIntensivePracticeManager.h"
qinclude "LuckyBoxManager.h"
qinclude "RandomBoxManager.h"
qinclude "PuzzlesManager.h"
qinclude "ThreeKingdomsEventManager.h"
qinclude "MissionShopManager.h"
qinclude "HotGirlMissionEventManager.h"
qinclude "CheerLeaderEventManager.h"
qinclude "GoldenCrushManager.h"
qinclude "UserHighFrequencyItem.h"
qinclude "ClubConfigManager.h"
qinclude "AttendanceItemShop.h"
qinclude "RandomItemBoardEventManager.h"
qinclude "CFSRankManager.h"
qinclude "EventDateManager.h"
qinclude "MissionEventManager.h"
qinclude "UserMissionEvent.h"
qinclude "LeagueSvrProxy.h"
qinclude "ShoppingFestivalEventManager.h"
qinclude "EventHalfPrice.h"
qinclude "MiniGameZoneEventManager.h"
qinclude "LovePaybackEventManager.h"
qinclude "ODBCSvrProxy.h"
qinclude "GameOfDiceEventManager.h"
qinclude "RandomItemDrawEventManager.h"
qinclude "PotionMakingEventManager.h"
qinclude "HippocampusEventManager.h"
qinclude "ShoppingGodEventManager.h"
qinclude "PackageItemEventManager.h"
qinclude "WhitedayEventManager.h"
qinclude "SummerCandyEventManager.h"
qinclude "DailyLoginRewardEventManager.h"
qinclude "SpeechBubbleManager.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// Modify for Match Server
void CFSGameThread::Process_FSLRequestRoomList( CFSGameClient* pClient )
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_ROOM_LIST_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read((PBYTE)&info.listInfo, sizeof(SC2S_REQ_ROOMLIST));

	CMatchSvrProxy* pMatch = nullptr;
	if( MATCH_TYPE_PVE == info.listInfo.btMatchType )
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE);
	else if( MATCH_TYPE_PVE_3CENTER == info.listInfo.btMatchType )
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_3CENTER);
	else if( MATCH_TYPE_PVE_CHANGE_GRADE == info.listInfo.btMatchType )
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_CHANGE_GRADE);
	else if( MATCH_TYPE_PVE_AFOOTBALL == info.listInfo.btMatchType )
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PVE_AFOOTBALL);
	else
		pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PRACTICE);

	CHECK_NULL_POINTER_VOID(pMatch);
	pMatch->SendRoomListReq(info);
}

void CFSGameThread::Process_Cheat(CFSGameClient * pClient)
{
	int iCode=0, iRequest1=0, iRequest2=0;
	char szName[10+1]={0,};
	m_ReceivePacketBuffer.Read(&iCode);
	m_ReceivePacketBuffer.Read(&iRequest1);
	m_ReceivePacketBuffer.Read(&iRequest2);

	pClient->SetCheatCode(iCode, iRequest1, iRequest2);
	CGameCheat::GetInstance()->CheatDivide(pClient);
}

void CFSGameThread::Process_FSChat(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	int iChatSize = 0, iType = 0, iMacroNum = 0;
	int iVoiceActionCode = 0;
	DECLARE_INIT_TCHAR_ARRAY(szText, MAX_CHATBUFF_LENGTH+1);
	DECLARE_INIT_TCHAR_ARRAY(szToGameID, MAX_GAMEID_LENGTH+1);

	m_ReceivePacketBuffer.Read(&iType); 

	if ( CHAT_TYPE_LOBBY == iType || CHAT_TYPE_TEAM_VOICE == iType )
	{
		int iForbidTime = pUser->GetForbidChatTime(pServer->GetForbidChatInterval());
		if ( iForbidTime )
		{
			int iResult = RESULT_CHAT_FAIL_FORBID;
			CPacketComposer packet( S2C_REQ_CHAT_RES );	
			packet.Add( (int)iResult );						
			packet.Add( iForbidTime );					
			pClient->Send( &packet );
			return;
		}
	}
	
	if(CHAT_TYPE_WHISPER == iType)
	{
		m_ReceivePacketBuffer.Read((PBYTE)szToGameID, sizeof(char) * (MAX_GAMEID_LENGTH + 1) ); 
		szToGameID[MAX_GAMEID_LENGTH] = 0;
	}
	
	if(CHAT_TYPE_MACRO == iType || CHAT_TYPE_AI_MACRO == iType)
	{
		m_ReceivePacketBuffer.Read(&iMacroNum); 
	}
	else if ( CHAT_TYPE_TEAM_VOICE == iType )
	{
		m_ReceivePacketBuffer.Read(&iVoiceActionCode); 
	}
	else
	{
		m_ReceivePacketBuffer.Read(&iChatSize); 
		
		if(0 > iChatSize) 
		{
			return;
		}
		else if(MAX_CHATBUFF_LENGTH < iChatSize) 
		{
			iChatSize = MAX_CHATBUFF_LENGTH;
		}
		
		m_ReceivePacketBuffer.Read((PBYTE)szText, iChatSize);
		szText[iChatSize] = 0;
	}

	if(CHAT_TYPE_CLUB == iType)
	{
		int CSN = pUser->GetClubSN();
		if( CSN <= 0 ) return;
		
		CClubSvrProxy* pClubProxy = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
		if(pClubProxy)
		{
			SG2CL_CLUB_CHAT_REQ info;
			info.iClubSN = CSN;
			strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);
			info.iMsgLen = iChatSize+1;
			strncpy_s(info.szMsg, _countof(info.szMsg), szText, MAX_CHATBUFF_LENGTH);
			pClubProxy->SendClubChatReq(info);
		}
	}
	else if(CHAT_TYPE_WHISPER == iType)
	{
		if(0 == strncmp(szToGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH+1)) return;
		
		int iResult = -1;
		BYTE bisGM = pUser->CheckGMUser();
		
		int iFrConceernStatus = -1;
		BEGIN_LOCK
			CScopedLockFriend fr(pUser->GetFriendList(), szToGameID);
			if ( fr != NULL)
			{
				iFrConceernStatus = fr->GetConcernStatus();
			}
		END_LOCK;

		CScopedRefGameUser pToUser(szToGameID);
		if (pToUser != NULL && pToUser->GetClient())
		{
			if ( pToUser->GetClient()->GetState() == FS_SINGLE )
			{
				iResult = WHISPER_USER_REFUSE_SIGNPLAY;
			}
			else if( pToUser->CanWhisper()  || iFrConceernStatus == FRIEND_ST_FRIEND_USER)
			{
				BEGIN_LOCK
					CScopedLockFriend fr1(pToUser->GetFriendList(), pUser->GetGameID());
					if( fr1 != NULL && fr1->GetConcernStatus() == FRIEND_ST_BANNED_USER )
					{
						iResult = WHISPER_USER_BANNED;
					}
					else
					{
						CPacketComposer Packet(S2C_CHAT);
						Packet.Add(&iType);
						Packet.Add(&bisGM);
						Packet.Add((PBYTE)pUser->GetGameID(), MAX_GAMEID_LENGTH + 1 );
						Packet.Add(&iChatSize);
						Packet.Add((PBYTE)szText, iChatSize + 1) ;
						pToUser->Send(&Packet);
						iResult = WHISPER_SUCCESS;
					}
				END_LOCK;
			}
			else
			{
				iResult = WHISPER_USER_REFUSE;
			}
		}
		else
		{
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
			if(pCenter)
			{
				pCenter->SendWhisper((char*)pUser->GetGameID(), szToGameID, szText);
				return;
			}
		}		
		
		CPacketComposer PacketRes(S2C_CHAT); 
		PacketRes.Add((int)CHAT_TYPE_WHISPER_RETURN);
		PacketRes.Add(&bisGM);
		PacketRes.Add(&iResult);
		PacketRes.Add((PBYTE)szToGameID, MAX_GAMEID_LENGTH + 1 );
		PacketRes.Add(&iChatSize);
		PacketRes.Add((PBYTE)szText, iChatSize + 1) ;
		pUser->Send(&PacketRes);
	}	
	else if (CHAT_TYPE_PRIVATE == iType)
	{
		CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
		CHECK_NULL_POINTER_VOID(pChat);

		SS2T_CHAT info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.iChatSize = iChatSize;

		CPacketComposer Packet(S2T_CHAT); 
		Packet.Add((PBYTE)&info, sizeof(SS2T_CHAT));
		Packet.Add((PBYTE)szText, iChatSize);
		pChat->Send(&Packet);
	}
	else if( CHAT_TYPE_LOBBY == iType) 
	{
		CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
		CHECK_NULL_POINTER_VOID(pChat);

		SS2T_CHAT info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.iChatSize = iChatSize;
		info.btIsGM = pUser->CheckGMUser();

		CPacketComposer Packet(S2T_LOBBYCHAT); 
		Packet.Add((PBYTE)&info, sizeof(SS2T_CHAT));
		Packet.Add((PBYTE)szText, iChatSize);
		pChat->Send(&Packet);

		CGMSvrProxy* pGMSvr = (CGMSvrProxy*)GAMEPROXY.GetProxy(FS_GM_SERVER_PROXY);
		if ( pGMSvr )
		{

			SM2G_BROADCAST_CHAT_RES rs;
			rs.iType = iType;
			strncpy_s(rs.GameID, _countof(rs.GameID), pUser->GetGameID(), _countof(rs.GameID)-1);
			rs.iChatSize = iChatSize;
			memcpy(rs.szText, szText, MAX_CHATBUFF_LENGTH+1);
			rs.iMatchProcessID = -1;
			rs.bIsGM = pUser->CheckGMUser();

			CPacketComposer Packet(G2GM_CHAT_INFO); 
			Packet.Add((BYTE*)&rs, sizeof(SM2G_BROADCAST_CHAT_RES));
			pGMSvr->Send(&Packet);
		}
	}
	else if(CHAT_TYPE_LEAGUE == iType)
	{
		CLeagueSvrProxy* pLeague = (CLeagueSvrProxy*)GAMEPROXY.GetProxy(FS_LEAGUE_SERVER_PROXY);
		CHECK_NULL_POINTER_VOID(pLeague);

		SG2L_ROOMCHAT_REQ info;
		info.iChatstate = iType;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.btServerIndex = CFSGameServer::GetInstance()->GetServerIndex();
		info.iChatSize = iChatSize;

		CPacketComposer Packet(G2L_ROOMCHAT_REQ); 
		Packet.Add((PBYTE)&info, sizeof(SG2L_ROOMCHAT_REQ));
		Packet.Add((PBYTE)szText, iChatSize);
		pLeague->Send(&Packet);
	}
	else if(CHAT_TYPE_FACTION == iType)
	{
		CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
		CHECK_NULL_POINTER_VOID(pChat);

		SS2T_CHAT info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.iChatSize = iChatSize;

		int iFactionIndex = pUser->GetFactionIndex();
		CHECK_CONDITION_RETURN_VOID(iFactionIndex <= FACTION_INDEX_NONE || MAX_FACTION_INDEX_COUNT <= iFactionIndex);

		CPacketComposer Packet(S2T_FACTION_CHAT); 
		Packet.Add((PBYTE)&info, sizeof(SS2T_CHAT));
		Packet.Add(&iFactionIndex);
		Packet.Add((PBYTE)szText, iChatSize);
		pChat->Send(&Packet);
	}
	else if (CHAT_TYPE_TEAM == iType ||
			CHAT_TYPE_MACRO == iType ||
			CHAT_TYPE_OBSERVER == iType ||
			CHAT_TYPE_MATCHROOM == iType ||
			CHAT_TYPE_MAGICRING == iType ||
			CHAT_TYPE_AI_MACRO == iType ||
			CHAT_TYPE_TEAM_VOICE == iType)
	{
		CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
		CHECK_NULL_POINTER_VOID(pMatch);

		SG2M_CHAT_REQ info;
		info.iGameIDIndex = pUser->GetGameIDIndex();
		info.iType = iType;
		info.btServerIndex = _GetServerIndex;

		if ( CHAT_TYPE_TEAM_VOICE == iType )
		{
			vector<SVoiceUserSettingInfo*> vVoiceUserSettingInfo;
			pUser->GetUserItemList()->GetVoiceUserSettingInfo( vVoiceUserSettingInfo, -1, iVoiceActionCode );
			if ( vVoiceUserSettingInfo.size() != 1 )
			{
				return;
			}
			info.iVoiceIndex = vVoiceUserSettingInfo[0]->iVoiceIndex;
		}
		else
		{
			info.iChatSize = iChatSize;
			info.iMacroNum = iMacroNum;
			strncpy_s(info.szText, MAX_CHATBUFF_LENGTH+1, szText, MAX_CHATBUFF_LENGTH);
			info.bIsGM = pUser->CheckGMUser();
		}
		pMatch->SendPacket(G2M_CHAT_REQ, &info, sizeof(SG2M_CHAT_REQ));
	}
}

void CFSGameThread::Process_EventListInfoReq(CFSGameClient* pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);
	pUser->SendEventListInfo();
}

// Modify for Match Server
void CFSGameThread::Process_FSLCreateRoom(CFSGameClient * pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PRACTICE);
	CHECK_NULL_POINTER_VOID(pMatch);

	if(NULL == pUser->GetCurUsedAvatar())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, CHECK_FAIL, "Process_FSLCreateRoom - �4", pUser->GetGameID());
	return;
	}

	SG2M_CREATE_ROOM_REQ info;
	pUser->GetMatchLoginUserInfo(info, pMatch->GetMatchType());
	
	m_ReceivePacketBuffer.Read((BYTE*)info.szName, sizeof(char)*(MAX_ROOM_NAME_LENGTH+1));
	m_ReceivePacketBuffer.Read((BYTE*)info.szPass, sizeof(char)*(MAX_TEAM_PASS_LENGTH+1));
	m_ReceivePacketBuffer.Read(&info.uMaxUser);
	m_ReceivePacketBuffer.Read(&info.iPracticeMethod);
	m_ReceivePacketBuffer.Read(&info.iPracticeMethodDetail);

	if (pUser->IsShutdownPlayImpossibleTime())
	{
		int iResult = (int)ERRORCODE_SHUTDOWN_USER_RESTRICTION;
		CPacketComposer PacketComposer(S2C_CREATE_ROOM_RESULT);
		PacketComposer.Add(iResult);
		pUser->Send(&PacketComposer);
		return;
	}
	
	pMatch->SendPacket(G2M_CREATE_ROOM_REQ, &info, sizeof(SG2M_CREATE_ROOM_REQ));
}

// Modify for Match Server
void CFSGameThread::Process_FSLJoinRoom(CFSGameClient * pClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_PRACTICE);
	CHECK_NULL_POINTER_VOID(pMatch);

	if(NULL == pUser->GetCurUsedAvatar())
	{
		WRITE_LOG_NEW(LOG_TYPE_USER, INVALED_DATA, CHECK_FAIL, "Process_FSLJoinRoom - �4", pUser->GetGameID());
	return;
	}

	SG2M_JOIN_ROOM_REQ info;
	pUser->GetMatchLoginUserInfo(info, pMatch->GetMatchType());

	m_ReceivePacketBuffer.Read(&info.iMode);
	m_ReceivePacketBuffer.Read(&info.uRoomID);
	m_ReceivePacketBuffer.Read(&info.iRoomMethod);
	m_ReceivePacketBuffer.Read(&info.iRoomMethodDetail);

	if (pUser->IsShutdownPlayImpossibleTime())
	{
		int iTeamMode = -1;
		CPacketComposer PacketComposer(S2C_JOIN_ROOM_RESULT);
		PacketComposer.Add(iTeamMode);
		PacketComposer.Add(RESULT_ENTER_WAIT_SHUTDOWN_USER);
		pUser->Send(&PacketComposer);
		return;
	}

	pUser->CheckExpireItem();
	
	pMatch->SendJoinRoomReq(info);
}

void CFSGameThread::Process_FSLRequestUserInfo(CFSGameClient * pFSClient)
{
	short sType = 0;
	int iSeason = 0;
	int iSkillType = 1; // �⺻ 1 : ��ų , 2: ������Ÿ��
	int iRecordType = 0;
	int iCoachCardProductIndex = 0;
	int iCoachCardFront = 0;
	BYTE btMatchType = MATCH_TYPE_NONE;
	BYTE btAvatarLvGrade = AVATAR_LV_GRADE_ALL;
	DECLARE_INIT_TCHAR_ARRAY(szGameID, MAX_GAMEID_LENGTH+1);	

	m_ReceivePacketBuffer.Read(&sType);
	
	if( sType == USER_INFO_RANK )
	{
		m_ReceivePacketBuffer.Read(&iSeason);
	}
	else if(USER_INFO_STAT == sType)
	{
		m_ReceivePacketBuffer.Read(&btMatchType);
	}
	else if (sType == USER_INFO_COACHCARD_DETAIL)
	{
		m_ReceivePacketBuffer.Read(&iCoachCardProductIndex);
		m_ReceivePacketBuffer.Read(&iCoachCardFront);
	}

	m_ReceivePacketBuffer.Read((PBYTE)szGameID, MAX_GAMEID_LENGTH+1);	
	szGameID[MAX_GAMEID_LENGTH] = 0;	

	if( sType == USER_INFO_RANK )
	{
		m_ReceivePacketBuffer.Read(&iRecordType);
	}

	CFSGameUser * pMyUser = (CFSGameUser *)pFSClient->GetUser();
	if( NULL == pMyUser ) return;

	if(USER_INFO_STAT == sType)
	{
		if ( btMatchType > MATCH_TYPE_NONE )
		{
			btAvatarLvGrade = pMyUser->GetCurUsedAvtarLvGrade();
			CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TYPE_COUNT <= btMatchType);
			CHECK_CONDITION_RETURN_VOID(AVATAR_LV_GRADE_ALL <= btAvatarLvGrade);

			CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy( btMatchType, btAvatarLvGrade );
			if(pMatch) 
			{
				SG2M_USER_INFO_REQ info;
				info.iGameIDIndex = pMyUser->GetGameIDIndex();

				info.shType = sType;
				strncpy_s(info.szGameID, _countof(info.szGameID), szGameID, _countof(info.szGameID)-1);

				pMatch->SendUserInfoReq(info);

				return;
			}
		}
	}

	CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
	if( NULL == pServer ) return;

	CPacketComposer PacketComposer(S2C_USER_INFO_RES);
	PacketComposer.Add((BYTE)1);	// ã�� ����

	if( 0 == strcmp( pMyUser->GetGameID(), szGameID )) // ����������
	{		
		PacketComposer.Add(sType);

		if( sType == USER_INFO_BASE ) /// �⺻���� 
		{
			pMyUser->MakePacketForUserInfoBase(PacketComposer, pServer, INFO_TYPE_MINE);
		}
		else if( sType == USER_INFO_RANK ) // ��ŷ���� 
		{
			pMyUser->MakePacketForUserInfoRank(PacketComposer, iRecordType, iSeason);
		}
		else if(sType == USER_INFO_STAT) // �������� 
		{
			pMyUser->MakePacketForUserInfoStat(PacketComposer, pServer);
		}
		else if(sType == USER_INFO_COACHCARD)
		{
			pMyUser->MakeCoachCardSlotPackageListRes(PacketComposer);
		}
		else if(sType == USER_INFO_COACHCARD_DETAIL) 
		{
			pMyUser->MakePacketForUserInfoCoachCard(PacketComposer, iCoachCardProductIndex, iCoachCardFront);
		}
		else if(sType == USER_INFO_CHARACTER_COLLECTION)
		{
			pMyUser->MakeDataForUserInfoCharacterCollection(PacketComposer);
		}
	}
	else 
	{		
		CScopedRefGameUser pUser(szGameID);
		if( pUser != NULL )
		{
			PacketComposer.Add(sType);

			if(sType == USER_INFO_BASE) /// �⺻���� 
			{
				short sIsFriend = 0;
				short sFiendStatus = 0;
				int iFriendIDIndex = 0;
				BYTE btFriendAccountStatus = FriendAccount::STATUS_NONE;
				BYTE btFriendAccountChannelType = FriendAccount::CHANNEL_TYPE_OFFLINE;

				BEGIN_LOCK

					CScopedRefGameUser sender(pMyUser->GetGameIDIndex());
					if(sender != nullptr)
					{
						iFriendIDIndex = sender->GetUserIDIndex();
					}

				END_LOCK

				BEGIN_LOCK

					CScopedLockFriend fr(pMyUser->GetFriendList(), szGameID);
					if(fr != NULL)
					{
						sIsFriend = fr->GetConcernStatus();
						if(fr->GetStatus() != USER_ST_OFFLINE)
						{				
							if(pUser->IsPlaying()) sFiendStatus = 2;	
							else sFiendStatus = 1;
						}
					}

					CScopedLockFriendAccount frAccount(pUser->GetFriendAccountList(), iFriendIDIndex);
					if(frAccount != nullptr && frAccount->GetChannelType() != FriendAccount::CHANNEL_TYPE_OFFLINE)
					{
						btFriendAccountStatus = frAccount->GetStatus();

						if(frAccount->GetChannelType() != FriendAccount::CHANNEL_TYPE_OFFLINE)
						{
							if(pUser->IsPlaying())
								btFriendAccountChannelType = FriendAccount::CHANNEL_TYPE_ONLINE_SAME_SVR;
							else
								btFriendAccountChannelType = FriendAccount::CHANNEL_TYPE_ONLINE_DIFF_SVR;
						}
					}

				END_LOCK;

				pUser->MakePacketForUserInfoBase(PacketComposer, pServer, INFO_TYPE_OTHER, sIsFriend, sFiendStatus, btFriendAccountStatus, btFriendAccountChannelType);
			}
			else if( sType == USER_INFO_RANK ) // ��ŷ���� 
			{
				pUser->MakePacketForUserInfoRank(PacketComposer, iRecordType, iSeason);
			}
			else if( sType == USER_INFO_STAT ) // �������� 
			{
				pUser->MakePacketForUserInfoStat(PacketComposer, pServer);			
			}
			else if(sType == USER_INFO_COACHCARD)
			{
				pUser->MakeCoachCardSlotPackageListRes(PacketComposer);
			}
			else if(sType == USER_INFO_COACHCARD_DETAIL) 
			{
				pUser->MakePacketForUserInfoCoachCard(PacketComposer, iCoachCardProductIndex, iCoachCardFront);
			}
			else if(sType == USER_INFO_CHARACTER_COLLECTION)
			{
				pUser->MakeDataForUserInfoCharacterCollection(PacketComposer);
			}
		}
		else
		{
			int	iLinkItemAddStat[MAX_STAT_NUM] = {0};
			CFSGameServer * pServer = (CFSGameServer *)pFSClient->GetServer();
			CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);

			if(pCenter) pCenter->SendUserSInfo(szGameID, (char*)pFSClient->GetUser()->GetGameID(), sType, iRecordType, iSeason, iLinkItemAddStat, iCoachCardProductIndex, iCoachCardFront );
			return;
		}
	}

	pFSClient->Send(&PacketComposer);
}

void CFSGameThread::Process_FSLRoomMemberList(CFSGameClient * pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_ROOM_MEMBER_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read((PBYTE)&info.req, sizeof(SC2S_ROOM_MEMBER_REQ));

	CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TYPE_COUNT <= info.req.btMatchType);
	CHECK_CONDITION_RETURN_VOID(AVATAR_LV_GRADE_ALL <= info.req.btAvatarLvGrade);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.req.btMatchType, info.req.btAvatarLvGrade);
	if(pMatch) 
	{
		pMatch->SendPacket(G2M_ROOM_MEMBER_REQ, &info, sizeof(SG2M_ROOM_MEMBER_REQ));
	}
}

void CFSGameThread::Process_FSLSelectPosition(CFSGameClient * pFSClient)
{
	int iPosition, iResult;
	m_ReceivePacketBuffer.Read(&iPosition);
	
	CFSGameUser* pUser  = (CFSGameUser*) pFSClient->GetUser();
	
	if(NULL == pUser)
	{
		return;
	}
	
	if(pUser->SetNewPosition(iPosition))
	{
		iResult = 0;
		pUser->CheckAndGiveSkillAndSlot();

		CCenterSvrProxy* pCenter = (CCenterSvrProxy*) GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if(pCenter)
		{
			SG2S_UPDATE_USER_INFO_NOT info;
			strncpy_s(info.GameID, MAX_GAMEID_LENGTH+1, pUser->GetGameID(), MAX_GAMEID_LENGTH);
			info.btUpdateType = CU_UPDATE_GAMEPOSITON;
			info.iUpdateInfo = iPosition;
			pCenter->SendPacket(G2S_UPDATE_USER_INFO_NOT, &info, sizeof(SG2S_UPDATE_USER_INFO_NOT));
		}		
	}
	else
	{
		iResult = -1;
	}
	//POSITION_SELECT
	//������� 
	CPacketComposer PacketComposer(S2C_POSITION_SELECT_REQ);
	PacketComposer.Add(POSITION_RESULT);
	PacketComposer.Add(iResult);
	PacketComposer.Add(iPosition);
	
	pFSClient->Send(&PacketComposer);
	//
}

void CFSGameThread::Process_FSLRequestRoomORTeamInfo(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_QUICK_ROOM_INFO_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read((PBYTE)&info.info, sizeof(SC2S_QUICK_ROOM_INFO_REQ));

	CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TYPE_COUNT <= info.info.btMatchType);

	BYTE btAvatarLvGrade = pUser->GetCurUsedAvtarLvGrade();
	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.info.btMatchType, btAvatarLvGrade);
	if (NULL != pMatch)
	{
		pMatch->SendPacket(G2M_QUICK_ROOM_INFO_REQ, &info, sizeof(SG2M_QUICK_ROOM_INFO_REQ));
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSLTeamList(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_WAIT_TEAM_LIST_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	m_ReceivePacketBuffer.Read((PBYTE)&info.listInfo, sizeof(SC2S_WAIT_TEAM_LIST_REQ));

	CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TYPE_COUNT <= info.listInfo.btMatchType);
	CHECK_CONDITION_RETURN_VOID(AVATAR_LV_GRADE_ALL <= info.listInfo.btAvatarLvGrade);

	if(MATCH_TYPE_CLUB_LEAGUE == info.listInfo.btMatchType)
	{
		if(CLOSED == CLUBCONFIGMANAGER.GetClubLeagueOpenState())
		{
			WRITE_LOG_NEW(LOG_TYPE_SYSTEM, INVALED_DATA, FAIL, "Process_FSLTeamList - MatchType:11866902, GameIDIndex:0", info.listInfo.btMatchType, pUser->GetGameIDIndex());
turn;
		}
	}

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.listInfo.btMatchType, info.listInfo.btAvatarLvGrade);
	if(pMatch) 
	{
		info.iClubIndex = pUser->GetClubSN();
		pMatch->SendWaitTeamListReq(info);
	}
}

// Modify for Match Server
void CFSGameThread::Process_FSLClubTeamList(CFSGameClient* pFSClient)
{
	//TD - Ŭ�� �� ����Ʈ
	return;

	//if(FALSE == CFSGameServer::IsClubServer())
	//{
	//	return;  //// Ŭ�������� �ƴϸ� 
	//}

	//CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	//CHECK_NULL_POINTER_VOID(pUser);

	//SG2M_CLUB_TEAM_LIST_REQ info;
	//info.iGameIDIndex = pUser->GetGameIDIndex();
	//m_ReceivePacketBuffer.Read(&info.iType);
	//m_ReceivePacketBuffer.Read(&info.iReadMode);

	//if(1 == info.iType && LIST_REQUESTMODE_WHOLE == info.iReadMode)
	//{
	//	m_ReceivePacketBuffer.Read(&info.iPage);
	//	m_ReceivePacketBuffer.Read(&info.iDir);
	//}
	//else
	//{
	//	m_ReceivePacketBuffer.Read(&info.iFindStartID);
	//	m_ReceivePacketBuffer.Read(&info.iDir);
	//}

	//CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(MATCH_TYPE_RATING, pUser->GetCurUsedAvtarLv());
	//if(pMatch) 
	//{
	//	pMatch->SendClubTeamListReq(info);
	//}
}

// Modify for Match Server
void CFSGameThread::Process_FSLCreateTeam(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_TEAM_CREATE info;	
	m_ReceivePacketBuffer.Read((PBYTE)&info.TeamInfo, sizeof(SC2S_CREATE_TEAM_REQ));
	info.TeamInfo.szName[MAX_TEAM_NAME_LENGTH] = '\0';
	info.TeamInfo.szPass[MAX_TEAM_PASS_LENGTH] = '\0';

	CHECK_CONDITION_RETURN_VOID(MATCH_TYPE_RATING != info.TeamInfo.btMatchType && MATCH_TYPE_CLUB_LEAGUE != info.TeamInfo.btMatchType &&
		MATCH_TYPE_FREE != info.TeamInfo.btMatchType && MATCH_TYPE_AIPVP != info.TeamInfo.btMatchType);	
	CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TEAM_SCALE <= info.TeamInfo.btMatchTeamScale);

	pUser->GetMatchLoginUserInfo(info, info.TeamInfo.btMatchType);

	if(TRUE == pUser->GetUserAppraisal()->CheckPenaltyUser())
	{
		SS2C_CREATE_TEAM_RES rs;
		rs.btResult = RESULT_CREATE_TEAM_FAILED_PENALTYUSER;
		rs.tPenaltyRemainTime = pUser->GetUserAppraisal()->GetPenaltyRemainTime();
		pUser->Send(S2C_CREATE_TEAM_RES, &rs, sizeof(SS2C_CREATE_TEAM_RES));
		return;
	}

	if(MATCH_TYPE_CLUB_LEAGUE == info.TeamInfo.btMatchType)
	{
		if(MATCH_TEAM_SCALE_3ON3CLUB != info.TeamInfo.btMatchTeamScale)
		{
			SS2C_CREATE_TEAM_RES rs;
			rs.btResult = RESULT_CREATE_TEAM_FAILED;
			pUser->Send(S2C_CREATE_TEAM_RES, &rs, sizeof(SS2C_CREATE_TEAM_RES));
			return;
		}

		if(CLOSED == CLUBCONFIGMANAGER.GetClubLeagueOpenState())
		{
			SS2C_CREATE_TEAM_RES rs;
			rs.btResult = RESULT_CREATE_TEAM_FAILED_CLOSED_CLUB_LEAGUE;
			pUser->Send(S2C_CREATE_TEAM_RES, &rs, sizeof(SS2C_CREATE_TEAM_RES));
			return;
		}

		if(0 >= pUser->GetClubSN())
		{
			SS2C_CREATE_TEAM_RES rs;
			rs.btResult = RESULT_CREATE_TEAM_FAILED_NOT_CLUB_USER;
			pUser->Send(S2C_CREATE_TEAM_RES, &rs, sizeof(SS2C_CREATE_TEAM_RES));
			return;
		}

		SAvatarInfo* pAvatarInfo = pUser->GetCurUsedAvatar();
		CHECK_NULL_POINTER_VOID(pAvatarInfo);

		if(pAvatarInfo->iClubContributionPoint < CLUBCONFIGMANAGER.GetNeedContributionPoint())
		{
			SS2C_CREATE_TEAM_RES rs;
			rs.btResult = RESULT_CREATE_TEAM_FAILED_ENOUGH_CONTRIBUTION_POINT;
			rs.iNeedClubContributionPoint = CLUBCONFIGMANAGER.GetNeedContributionPoint();
			pUser->Send(S2C_CREATE_TEAM_RES, &rs, sizeof(SS2C_CREATE_TEAM_RES));
			return;
		}
	}

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.TeamInfo.btMatchType, pUser->GetCurUsedAvtarLv());
	if(pMatch) 
	{
		pMatch->SendPacket(G2M_TEAM_CREATE, &info, sizeof(SG2M_TEAM_CREATE));
	}
}

// Modify for Match Server
void CFSGameThread::Process_RemainPlayTime_In_Room(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SG2M_REMAIN_PLAY_TIME_IN_ROOM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	m_ReceivePacketBuffer.Read((PBYTE)&info.req, sizeof(SC2S_REMAIN_PLAY_TIME_IN_ROOM_REQ));

	CHECK_CONDITION_RETURN_VOID(MAX_MATCH_TYPE_COUNT <= info.req.btMatchType);
	CHECK_CONDITION_RETURN_VOID(AVATAR_LV_GRADE_ALL <= info.req.btAvatarLvGrade);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.GetMatchProxy(info.req.btMatchType, info.req.btAvatarLvGrade);
	if(pMatch) 
	{
		pMatch->SendPacket(G2M_REMAIN_PLAY_TIME_IN_ROOM_REQ, &info, sizeof(SG2M_REMAIN_PLAY_TIME_IN_ROOM_REQ));
	}
}

void CFSGameThread::Process_FSFollowFriend(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	if (pUser->IsShutdownPlayImpossibleTime())
	{
		BYTE btResult = (BYTE)ERRORCODE_SHUTDOWN_USER_RESTRICTION;
		CPacketComposer PacketComposer(S2C_FOLLOW_FRIEND_RES);
		PacketComposer.Add(btResult);
		pUser->Send(&PacketComposer);
		return;
	}

	SG2M_FOLLOW_FRIEND_REQ info;

	m_ReceivePacketBuffer.Read((BYTE*)&info.FriendInfo, sizeof(SC2S_FOLLOW_FRIEND_REQ));
	info.FriendInfo.szFriend[MAX_GAMEID_LENGTH] = 0;

	CScopedRefGameUser pFriend(info.FriendInfo.szFriend);
	if(pFriend != NULL)
	{
		CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pFriend->GetMatchLocation());
		if (pMatch) 
		{
			int iLv = pUser->GetCurUsedAvtarLv();

			info.iGameIDIndex = pUser->GetGameIDIndex();
			info.iAvatarLvGrade = GET_AVATAR_LV_GRADE(iLv);
			info.iLobbySvrID = CFSGameServer::GetInstance()->GetProcessID();
			info.iFriendGameIDIndex = pFriend->GetGameIDIndex();
			info.iClubIndex = pUser->GetClubSN();
			info.iClubContributionPoint = pUser->GetClubContributionPoint();
			pMatch->SendPacket(G2M_FOLLOW_FRIEND_REQ, &info, sizeof(SG2M_FOLLOW_FRIEND_REQ));
		}		
		else
		{
			SS2C_FOLLOW_FRIEND_RES rs;
			rs.btResult = RESULT_FOLLOW_FRIEND_NOT_IN_ROOM;
			pUser->Send(S2C_FOLLOW_FRIEND_RES, &rs, sizeof(SS2C_FOLLOW_FRIEND_RES)); 
		}
	}
	else
	{
		// �ٸ� �κ�
		CCenterSvrProxy* pCenterProxy = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if(pCenterProxy != NULL) 
		{
			int iLv = pUser->GetCurUsedAvtarLv();

			SG2S_FOLLOW_FRIEND_REQ ss;
			ss.iGameIDIndex = pUser->GetGameIDIndex();
			ss.iAvatarLvGrade = GET_AVATAR_LV_GRADE(iLv);
			strncpy_s(ss.szFriend, MAX_GAMEID_LENGTH+1, info.FriendInfo.szFriend, MAX_GAMEID_LENGTH);
			ss.iClubIndex = pUser->GetClubSN();
			ss.iClubContributionPoint = pUser->GetClubContributionPoint();
			pCenterProxy->SendPacket(G2S_FOLLOW_FRIEND_REQ, &ss, sizeof(SG2S_FOLLOW_FRIEND_REQ));
		}
	}
}

void CFSGameThread::Process_EnterPrivateChatReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	char szRoomName[MAX_PRIVATE_CHAT_ROOM_NAME_LEN+1] = {0};
	m_ReceivePacketBuffer.Read((PBYTE)szRoomName, MAX_PRIVATE_CHAT_ROOM_NAME_LEN);

	SS2T_ENTER_PRIVATE_CHAT_ROOM_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	strcpy_s(info.szChatName, _countof(info.szChatName), szRoomName);
	strcpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID());

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_ENTER_PRIVATE_CHAT_ROOM_REQ, &info, sizeof(SS2T_ENTER_PRIVATE_CHAT_ROOM_REQ));
}

void CFSGameThread::Process_ExitPrivateChatReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SS2T_BASE info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_EXIT_PRIVATE_CHAT_ROOM_REQ, &info, sizeof(SS2T_BASE));
}

void CFSGameThread::Process_PrivateChatUserPageStatusNot(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SS2T_PRIVATE_CHAT_USER_PAGE_STATUS_NOT info;
	m_ReceivePacketBuffer.Read(&info.btUserPageState);

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_PRIVATE_CHAT_USER_PAGE_STATUS_NOT, &info, sizeof(SS2T_PRIVATE_CHAT_USER_PAGE_STATUS_NOT));
}

void CFSGameThread::Process_PrivateChatUserListReq(CFSGameClient* pFSClient)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	SS2T_BASE info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_PRIVATE_CHAT_USER_LIST_REQ, &info, sizeof(ST2S_BASE));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_LIST_REQ)
{
	CFSGameUser*	pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CPacketComposer	PackerComposer(S2C_LOBBY_CHAT_ROOM_LIST_RES);

	SS2T_LOBBY_CHAT_ROOM_LIST_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);
		
	pChat->SendPacket(S2T_LOBBY_CHAT_ROOM_LIST_REQ, &info, sizeof(SS2T_ENTER_PRIVATE_CHAT_ROOM_REQ));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_LOBBY_CHAT_ROOM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2T_ENTER_LOBBY_CHAT_ROOM_REQ info;
	
	m_ReceivePacketBuffer.Read(&info.iRoomType);
	m_ReceivePacketBuffer.Read(&info.iRoomIndex);
	info.iGameIDIndex = pUser->GetGameIDIndex();

	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_ENTER_LOBBY_CHAT_ROOM_REQ, &info, sizeof(SS2T_ENTER_LOBBY_CHAT_ROOM_REQ));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_AUTO_ENTER_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2T_CHAT_LOBBY_CHAT_USER_INFO	info;

	strncpy_s(info.szGameID, _countof(info.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
	info.iGameIDIndex				= pUser->GetGameIDIndex();
	info.iGamePosition				= pUser->GetCurUsedAvatarPosition();
	info.iLv						= pUser->GetCurUsedAvtarLv();
	info.iFameLevel					= pUser->GetCurUsedAvtarFameLevel();
	info.btSex						= pUser->GetCurUsedAvatarSex();
	info.iClubMark					= pUser->GetClubMarkCode();
	info.iEquippedAchievementTitle	= pUser->GetEquippedAchievementTitle();
	info.bPenaltyUser				= pUser->GetUserAppraisal()->CheckPenaltyUserVisible();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);
	pChat->SendPacket(S2T_AUTO_ENTER_LOBBY_CHAT_ROOM_REQ, &info, sizeof(SS2T_CHAT_LOBBY_CHAT_USER_INFO));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOBBY_CHAT_ROOM_USER_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2T_LOBBY_CHAT_ROOM_USER_LIST_REQ	info;

	m_ReceivePacketBuffer.Read(&info.iPageNum);
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	pChat->SendPacket(S2T_LOBBY_CHAT_ROOM_USER_LIST_REQ, &info, sizeof(SS2T_LOBBY_CHAT_ROOM_USER_LIST_REQ));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHAT_REPEAT_BAN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CChatSvrProxy* pChat = (CChatSvrProxy*)GAMEPROXY.GetProxy(FS_CHAT_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pChat);

	SS2T_BASE	info;

	info.iGameIDIndex = pUser->GetGameIDIndex();

	pChat->SendPacket(S2T_CHAT_REPEAT_BAN_REQ, &info, sizeof(SS2T_BASE));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ITEM_BUYING_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_ITEM_BUYING_INFO_REQ info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_ITEM_BUYING_INFO_REQ));

	CHECK_CONDITION_RETURN_VOID(0 >= info.iItemPropertyKind);

	CPacketComposer PacketComposer(S2C_SHOPITEM_SELECT_RES);
	if (TRUE == pUser->GetShopItemSelectInfo(PacketComposer, info.iItemPropertyKind))
	{
		pFSClient->Send(&PacketComposer);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_BOARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == BINGO.IsOpen());

	pUser->GetUserBingo()->Load();

	pUser->GetUserBingo()->SendBingoBoardInfo();
	pUser->GetUserBingo()->SendBingoSlotList();
	pUser->GetUserBingo()->SendBingoLineList();
	pUser->GetUserBingo()->SendBingoPremiumRewardInfo();
	pUser->SendHighFrequencyItemCount(ITEM_PROPERTY_KIND_BINGO_TATOO);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USE_BINGO_TATOO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	int iMarkedSlotIndex = 0;
	SS2C_USE_BINGO_TATOO_RES rs;

	CHECK_CONDITION_RETURN_VOID( TRUE == pUser->GetUserBingo()->CheckContinuousCount() );

	rs.btResult = pUser->GetUserBingo()->PickNumber(rs.btNumber, rs.iUpdatedTatooCount, iMarkedSlotIndex);

	pUser->Send(S2C_USE_BINGO_TATOO_RES, &rs, sizeof(SS2C_USE_BINGO_TATOO_RES));

	if (RESULT_USE_BINGO_TATOO_SUCCESS == rs.btResult &&
		0 <= iMarkedSlotIndex)
	{
		pUser->GetUserBingo()->CheckLineMade(iMarkedSlotIndex);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_SHUFFLE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

	CPacketComposer Packet(S2C_BINGO_SHUFFLE_INFO_RES);
	BINGO.MakePacketShuffleCost(Packet, pUser->GetUserBingo()->GetShuffleCount(), pUser->GetEventCoin());
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_SHUFFLE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

	SS2C_BINGO_SHUFFLE_RES rs;
	rs.btResult = pUser->GetUserBingo()->Shuffle();
	if (RESULT_BINGO_SHUFFLE_SUCCESS != rs.btResult)
	{
		pUser->Send(S2C_BINGO_SHUFFLE_RES, &rs, sizeof(SS2C_BINGO_SHUFFLE_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_INITIALIZE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();

	SS2S_BINGO_INITIALIZE_RES rs;
	rs.btBingoCompleteSuccessCount = 0;
	rs.btResult = pUser->GetUserBingo()->Initialize(rs.btBingoCompleteSuccessCount);
	pUser->Send(S2S_BINGO_INITIALIZE_RES, &rs, sizeof(SS2S_BINGO_INITIALIZE_RES));

	if (RESULT_BINGO_INITIALIZE_SUCCESS == rs.btResult)
	{
		pUser->GetUserBingo()->SendBingoSlotList();
		pUser->GetUserBingo()->SendBingoLineList();
		pUser->GetUserBingo()->SendBingoPremiumRewardInfo();
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_INITIALIZE_REWARD_COST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_BINGO_INITIALIZE_REWARD_COST_RES info;
	info.iBingoCouponCost = BINGO.GetInitRewardCost();
	info.iUserBingoCouponCount = pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_BINGO_TATOO );
	
	pUser->Send(S2C_BINGO_INITIALIZE_REWARD_COST_RES, &info, sizeof(SS2C_BINGO_INITIALIZE_REWARD_COST_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_INITIALIZE_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_BINGO_INITIALIZE_REWARD_RES rs;
	rs.btResult = pUser->GetUserBingo()->InitializeReward();
	if (RESULT_BINGO_INITIALIZE_REWARD_SUCCESS != rs.btResult)
	{
		pUser->Send(S2C_BINGO_INITIALIZE_REWARD_RES, &rs, sizeof(SS2C_BINGO_INITIALIZE_REWARD_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_REWARD_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserBingo()->SendBingoRewardList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_MAIN_REWARD_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserBingo()->SendBingoMainRewardList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BINGO_TOKEN_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserBingo()->SendBingoTokenInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_RECORDBOARD_INFO_RES);
	pUser->GetUserRecordBoard()->MakePacketForPageInfo(Packet);
	pUser->Send(&Packet);

	Packet.Initialize(S2C_RECORDBOARD_MATCH_INFO_RES);
	if(TRUE == pUser->GetUserRecordBoard()->MakePacketForMatchInfo(pUser->GetUserRecordBoard()->GetProgressedPage(), Packet))
		pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_MATCH_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RECORDBOARD_MATCH_INFO_REQ info;
	m_ReceivePacketBuffer.Read(&info.iPage);

	CPacketComposer Packet(S2C_RECORDBOARD_MATCH_INFO_RES);
	if(TRUE == pUser->GetUserRecordBoard()->MakePacketForMatchInfo(info.iPage, Packet))
		pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_MATCH_DETAIL_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SRECORDBOARD_MATCH_DETAIL_INFO_REQ info;
	m_ReceivePacketBuffer.Read(&info.iMatchIndex);

	CPacketComposer Packet(S2C_RECORDBOARD_MATCH_DETAIL_INFO_RES);
	if(TRUE == pUser->GetUserRecordBoard()->MakePacketForMatchDetailInfo(info.iMatchIndex, Packet))
		pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_CREATE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_RECORDBOARD_CREATE_RES);
	SS2C_RECORDBOARD_CREATE_RES rs;
	rs.btResult = pUser->GetUserRecordBoard()->CreateRecordBoardReq();
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RECORDBOARD_CREATE_RES));
	pUser->Send(&Packet);

	pUser->SendRecordBoardStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_RESET_PAGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_RECORDBOARD_RESET_PAGE_RES);
	SS2C_RECORDBOARD_RESET_PAGE_RES rs;
	rs.btResult = pUser->GetUserRecordBoard()->ResetRecordBoardReq();
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RECORDBOARD_RESET_PAGE_RES));
	pUser->Send(&Packet);

	pUser->SendRecordBoardStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_ADD_PAGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_RECORDBOARD_ADD_PAGE_RES);
	SS2C_RECORDBOARD_ADD_PAGE_RES rs;
	pUser->GetUserRecordBoard()->AddPageReq(rs);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RECORDBOARD_ADD_PAGE_RES));
	pUser->Send(&Packet);

	pUser->SendRecordBoardStatus();

	if(rs.btResult == RESULT_RECORDBOARD_ADD_PAGE_SUCCESS)
		pUser->GetUserMissionShopEvent()->CheckMissionValue(CONDITION_TYPE_RECORDBOARD_PAGE_CLEAR);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_ADD_CASH_PAGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CPacketComposer Packet(S2C_RECORDBOARD_ADD_CASH_PAGE_RES);
	SS2C_RECORDBOARD_ADD_CASH_PAGE_RES rs;

	rs.btResult = pUser->GetUserRecordBoard()->CheckAddCashPage();
	if(rs.btResult != RESULT_RECORDBOARD_ADD_CASH_PAGE_SUCCESS)
	{
		Packet.Add((PBYTE)&rs, sizeof(SS2C_RECORDBOARD_ADD_CASH_PAGE_RES));
		pUser->Send(&Packet);
		return;
	}

	SBillingInfo BillingInfo;
	BillingInfo.iEventCode		= EVENT_BUY_RECORDBOARD_PAGE;	
	BillingInfo.pUser			= pUser;
	memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
	BillingInfo.iSerialNum		= pUser->GetUserIDIndex();
	memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH+1);	
	memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH+1);
	memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	BillingInfo.iSellType       = SELL_TYPE_CASH;
	BillingInfo.iCurrentCash	= pUser->GetCoin();
	BillingInfo.iCashChange		= RECORDBOARDMANAGER.GetAddPagePrice();
	BillingInfo.iParam0			= pUser->GetGameIDIndex();

	char* szItemName = pItemShop->GetEventCode_Drscription( BillingInfo.iEventCode );
	if( NULL == szItemName )
		strcpy_s(	BillingInfo.szItemName, "RecordBoard_AddCashPage");
	else
		memcpy( BillingInfo.szItemName, szItemName, MAX_ITEMNAME_LENGTH + 1 );	

	BILLING_GAME.RequestPurchaseItem(BillingInfo);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_GET_DAY_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_RECORDBOARD_GET_DAY_REWARD_RES);
	SS2C_RECORDBOARD_GET_DAY_REWARD_RES rs;
	rs.btResult = pUser->GetUserRecordBoard()->RecordBoardGetDayRewardReq();
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RECORDBOARD_GET_DAY_REWARD_RES));
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_FINISH_PAGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_RECORDBOARD_FINISH_PAGE_RES);
	pUser->GetUserRecordBoard()->FinishRecordBoardReq(Packet);
	pUser->Send(&Packet);

	pUser->SendRecordBoardStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_RANKING_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RECORDBOARD_RANKING_LIST_REQ info;
	m_ReceivePacketBuffer.Read(&info.btKind);
	m_ReceivePacketBuffer.Read(&info.iGamePositon);
	m_ReceivePacketBuffer.Read(&info.iPage);

	CPacketComposer Packet(S2C_RECORDBOARD_RANKING_LIST_RES);
	RECORDBOARDMANAGER.RankingListReq(pUser->GetGameIDIndex(), info, Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECORDBOARD_RANKING_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RECORDBOARD_RANKING_USER_INFO_REQ info;
	m_ReceivePacketBuffer.Read(&info.iGameIDIndex);

	CPacketComposer Packet(S2C_RECORDBOARD_RANKING_USER_INFO_RES);
	RECORDBOARDMANAGER.RankingUserInfoReq(info.iGameIDIndex, Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_TEST_RECORDBOARD_ADD_MATCH_DATA_REQ)
{
#ifdef _DEBUG
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_TEST_RECORDBOARD_ADD_MATCH_DATA_RES);
	pUser->GetUserRecordBoard()->AddTestMatchResult();

	Packet.Initialize(S2C_RECORDBOARD_MATCH_INFO_RES);
	if(TRUE == pUser->GetUserRecordBoard()->MakePacketForMatchInfo(pUser->GetUserRecordBoard()->GetProgressedPage(), Packet))
		pUser->Send(&Packet);

	pUser->SendRecordBoardStatus();
#endif
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_INTENSIVEPRACTICE_ROOM_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_INTENSIVEPRACTICE_ROOM_LIST_RES);

	SS2C_INTENSIVEPRACTICE_ROOM_LIST_RES rs;
	rs.iRoomCount = INTENSIVEPRACTICEMANAGER.GetRoomCount();
	Packet.Add((PBYTE)&rs, sizeof(SS2C_INTENSIVEPRACTICE_ROOM_LIST_RES));

	SS2C_INTENSIVEPRACTICE_ROOM_INFO info;
	for(int iOrder = 0; iOrder < rs.iRoomCount; iOrder++)
	{
		INTENSIVEPRACTICEMANAGER.MakeRoomInfo(iOrder, info);
		info.iUserBestPoint = pUser->GetUserIntensivePractice()->GetBestPoint(info.iPracticeType);
		Packet.Add((PBYTE)&info, sizeof(SS2C_INTENSIVEPRACTICE_ROOM_INFO));
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_START_INTENSIVEPRACTICE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_START_INTENSIVEPRACTICE_RES rs;
	m_ReceivePacketBuffer.Read(&rs.iRoomMode);
	m_ReceivePacketBuffer.Read(&rs.iLevel);
	m_ReceivePacketBuffer.Read(&rs.iPracticeType);

	CPacketComposer Packet(S2C_START_INTENSIVEPRACTICE_RES);
	rs.btResult = RESULT_START_INTENSIVEPRACTICE_SUCCESS;
	rs.btNPCCount = 0;

	CHECK_CONDITION_RETURN_VOID(0 < pUser->GetMatchLocation());
	CHECK_CONDITION_RETURN_VOID(rs.iRoomMode < INTENSIVEPRACTICE_ROOM_MODE_NONE || rs.iRoomMode > INTENSIVEPRACTICE_ROOM_MODE_RANKING);
	CHECK_CONDITION_RETURN_VOID(rs.iLevel <= INTENSIVEPRACTICE_LEVEL_NONE || rs.iLevel >= MAX_INTENSIVEPRACTICE_LEVEL_COUNT);
	CHECK_CONDITION_RETURN_VOID(rs.iPracticeType <= INTENSIVEPRACTICE_TYPE_NONE || rs.iPracticeType >= MAX_INTENSIVEPRACTICE_TYPE_COUNT);
	
	if(TRUE == pUser->GetUserAppraisal()->CheckPenaltyUser())
	{
		rs.btResult = RESULT_START_INTENSIVEPRACTICE_FAILED_PENALTYUSER;
		rs.tPenaltyRemainTime = pUser->GetUserAppraisal()->GetPenaltyRemainTime();
		Packet.Add((PBYTE)&rs, sizeof(SS2C_START_INTENSIVEPRACTICE_RES));
		pUser->Send(&Packet);
		return;
	}

	if(FALSE == INTENSIVEPRACTICEMANAGER.CheckCanEnter(rs.iPracticeType, pUser->GetCurUsedAvatarPosition()))
	{
		rs.btResult = RESULT_START_INTENSIVEPRACTICE_FAILED;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_START_INTENSIVEPRACTICE_RES));
		pUser->Send(&Packet);
		return;
	}

	if(FALSE == pFSClient->SetState(FS_INTENSIVEPRACTICE))
	{
		rs.btResult = RESULT_START_INTENSIVEPRACTICE_FAILED;
		Packet.Add((PBYTE)&rs, sizeof(SS2C_START_INTENSIVEPRACTICE_RES));
		pUser->Send(&Packet);
		return;
	}

	pUser->SetOptionEnable(OPTION_TYPE_WHISPER, TRUE);
	pUser->SetLocation(U_TRAINING);
	
	pUser->GetUserIntensivePractice()->SetGameStartInfo(rs);
	pUser->GetUserIntensivePractice()->MakeGameStartInfo(Packet, rs);

	// ���� ������ �κп� �߰��մϴ�. Client Crash ���� Enable ������� �����ϴ�. 
	pUser->UpdateRand_Rc4Key(HACK_TYPE_SOME_ACTION_CHANGE_STATUS);
	Packet.Add((PBYTE)pUser->GetAnyOperation_Rc4Key(), RC4_KEY_LENGTH+1);

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_INTENSIVEPRACTICE_RANK_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_INTENSIVEPRACTICE_RANK_LIST_RES rs;
	m_ReceivePacketBuffer.Read(&rs.iRankType);
	m_ReceivePacketBuffer.Read(&rs.iPracticeType);

	CPacketComposer Packet(S2C_INTENSIVEPRACTICE_RANK_LIST_RES);
	rs.iMyBestPoint = pUser->GetUserIntensivePractice()->GetBestPoint(rs.iPracticeType);

	if(rs.iRankType == INTENSIVEPRACTICE_RANK_TYPE_ALL)
	{
		CHECK_CONDITION_RETURN_VOID(FALSE == INTENSIVEPRACTICEMANAGER.MakeRankList(pUser->GetGameIDIndex(), Packet, rs));
		pUser->GetUserIntensivePractice()->GiveAchievement();
		pUser->Send(&Packet);
	}
	else if(rs.iRankType == INTENSIVEPRACTICE_RANK_TYPE_FRIEND)
	{
		CHECK_CONDITION_RETURN_VOID(FALSE == pUser->GetUserIntensivePractice()->MakeRankList(pUser->GetGameIDIndex(), Packet, rs));
		pUser->Send(&Packet);
	}
	else if(rs.iRankType == INTENSIVEPRACTICE_RANK_TYPE_CLUB)
	{
		if(pUser->GetClubSN() > 0)
		{
			CClubSvrProxy* pClub = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
			if(pClub)
			{
				SG2CL_INTENSIVEPRACTICE_RANK_LIST_REQ rq;
				rq.iClubSN = pUser->GetClubSN();
				strncpy_s(rq.szGameID, _countof(rq.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
				rq.iGameIDIndex = pUser->GetGameIDIndex();
				rq.info.iRankType = rs.iRankType;
				rq.info.iPracticeType = rs.iPracticeType;
				pClub->SendPacket(G2CL_INTENSIVEPRACTICE_RANK_LIST_REQ, &rq, sizeof(SG2CL_INTENSIVEPRACTICE_RANK_LIST_REQ));
			}
		}
		else
		{
			rs.iRankCount = 0;
			rs.iMyRank = 0;
			Packet.Add((PBYTE)&rs, sizeof(SS2C_INTENSIVEPRACTICE_RANK_LIST_RES));
			pUser->Send(&Packet);
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LUCKYBOX_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLuckyBox()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LUCKYBOX_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLuckyBox()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USE_LUCKYTICKET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_USE_LUCKYTICKET_RES rs;
	ZeroMemory(&rs, sizeof(SS2C_USE_LUCKYTICKET_RES));

	rs.btResult = pUser->GetUserLuckyBox()->UseLuckyTicket(rs.btProductIndex, rs.iUpdatedLuckyTicketCount);
	pUser->Send(S2C_USE_LUCKYTICKET_RES, &rs, sizeof(SS2C_USE_LUCKYTICKET_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMBOX_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());

	pUser->GetUserRandomBox()->SendRandomBoxInfo();
	pUser->SendHighFrequencyItemCount(ITEM_PROPERTY_KIND_RANDOM_BAGS);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMBOX_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMBOX_REWARD_INFO_REQ rq;
	m_ReceivePacketBuffer.Read(&rq.iBoxItemCode);

	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());

	pUser->GetUserRandomBox()->SendRandomBoxRewardInfo(rq.iBoxItemCode);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USE_RANDOMBOX_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());

	SC2S_USE_RANDOMBOX_REQ rq;
	m_ReceivePacketBuffer.Read(&rq.iBoxItemCode);

	SS2C_USE_RANDOMBOX_RES rs;
	rs.iBoxItemCode = rq.iBoxItemCode;
	rs.btResult = pUser->GetUserRandomBox()->UseRandomBox(rs.iBoxItemCode, rs.iRewardIndex, rs.iUpdatedBoxCount);
	pUser->Send(S2C_USE_RANDOMBOX_RES, &rs, sizeof(SS2C_USE_RANDOMBOX_RES));
	pUser->SendHighFrequencyItemCount(ITEM_PROPERTY_KIND_RANDOM_BAGS);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMBOX_MISSION_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMBOX.IsOpen());

	SS2C_RANDOMBOX_MISSION_REWARD_RES rs;
	rs.btResult = pUser->GetUserRandomBox()->RecvMissionReward(rs);
	pUser->Send(S2C_RANDOMBOX_MISSION_REWARD_RES, &rs, sizeof(SS2C_RANDOMBOX_MISSION_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == PUZZLES.IsOpen());

	int iItemCount = pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN );

	pUser->GetUserPuzzle()->SendPuzzleInfo(iItemCount);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == PUZZLES.IsOpen());

	SC2S_PUZZLES_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	SS2C_PUZZLES_GET_REWARD_RES rs;
	rs.iPuzzleIndex = rq.iPuzzleIndex;

	int iUpdateTokenCount = 0;
	rs.btResult = pUser->GetUserPuzzle()->GetRewardReq(rq.iPuzzleIndex, iUpdateTokenCount);
	pUser->Send(S2C_PUZZLES_GET_REWARD_RES, &rs, sizeof(SS2C_PUZZLES_GET_REWARD_RES));
	
	if( RESULT_PUZZLES_GET_REWARD_SUCCESS == rs. btResult && iUpdateTokenCount > 0 )
	{
		SUserHighFrequencyItem sUserHighFrequencyItem;
		sUserHighFrequencyItem.iPropertyKind = ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN;
		sUserHighFrequencyItem.iPropertyTypeValue = iUpdateTokenCount;
		pUser->GetUserHighFrequencyItem()->AddUserHighFrequencyItem(sUserHighFrequencyItem);
	}
	
	int iItemCount = pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItemCount( ITEM_PROPERTY_KIND_TOKEN_HALLOWEEN );

	pUser->GetUserPuzzle()->SendPuzzleInfo(iItemCount);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_WANT_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == PUZZLES.IsOpen());

	SC2S_PUZZLES_WANT_REWARD_REQ	rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GiveAndSendEventPuzzleReward((int)rq.btIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_THREE_KINGDOMS_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == THREEKINGDOMSEVENT.IsOpen());

	pUser->GetUserThreeKingdomsEvent()->SendThreeKingdomsEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_THREE_KINGDOMS_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = (CFSGameServer*)pFSClient->GetServer();
	CHECK_NULL_POINTER_VOID(pServer);

	CHECK_CONDITION_RETURN_VOID(CLOSED == THREEKINGDOMSEVENT.IsOpen());

	SC2S_EVENT_THREE_KINGDOMS_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	SS2C_EVENT_THREE_KINGDOMS_GET_REWARD_RES rs;
	rs.iRewardIndex = rq.iRewardIndex;
	rs.btResult = pUser->GetUserThreeKingdomsEvent()->RecvMissionReward(pServer->GetItemShop(), rq.btEventRewardType, rs.iRewardIndex);
	pUser->Send(S2C_EVENT_THREE_KINGDOMS_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_THREE_KINGDOMS_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONSHOP.IsShopOpen());

	pUser->GetUserMissionShopEvent()->SendMissionShopEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_PREVIEW_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONSHOP.IsShopOpen());

	SC2S_MISSIONSHOP_PREVIEW_LIST_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GetUserMissionShopEvent()->SendMissionShopPriviewList(rq.iProductIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_BUY_PRODUCT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONSHOP.IsShopOpen());

	SC2S_MISSIONSHOP_BUY_PRODUCT_REQ	rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GetUserMissionShopEvent()->BuyMissionShopProductRes(rq.iProductIndex, rq.iUseCoinType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_MISSION_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONSHOP.IsShopOpen());

	SC2S_MISSIONSHOP_MISSION_LIST_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GetUserMissionShopEvent()->SendMissionShopMissionList(rq.iMissionType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSIONSHOP_MISSION_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == MISSIONSHOP.IsShopOpen());

	SC2S_MISSIONSHOP_MISSION_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GetUserMissionShopEvent()->GiveMissionShopMissionReward(rq.iMissionIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HONEYWALLET_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHoneyWalletEvent()->SendUserWalletData();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HONEYWALLET_OPEN_WALLET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHoneyWalletEvent()->OpenHoneyWallet();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_CHARACTER_COLLECTION_REQ)
{	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_ENTER_CHARACTER_COLLECTION_RES rs;
	if(FALSE == pFSClient->SetState(FS_CHARACTER_COLLECTION))
	{
		rs.btResult = RESULT_ENTER_CHARACTER_COLLECTION_FAIL;
	}

	rs.btResult = RESULT_ENTER_CHARACTER_COLLECTION_SUCCESS;
	pUser->Send(S2C_ENTER_CHARACTER_COLLECTION_RES, &rs, sizeof(SS2C_ENTER_CHARACTER_COLLECTION_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_CHARACTER_COLLECTION_REQ)
{	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EXIT_CHARACTER_COLLECTION_RES rs;
	if(FALSE == pFSClient->SetState(NEXUS_LOBBY))
	{
		rs.btResult = RESULT_EXIT_CHARACTER_COLLECTION_FAIL;
	}

	rs.btResult = RESULT_EXIT_CHARACTER_COLLECTION_SUCCESS;
	pUser->Send(S2C_EXIT_CHARACTER_COLLECTION_RES, &rs, sizeof(SS2C_EXIT_CHARACTER_COLLECTION_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_CHAR_LIST_REQ)
{	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHARACTER_COLLECTION_CHAR_LIST_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GetUserCharacterCollection()->SendCharList(rq.iPageNum);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_SENTENCE_REQ)
{	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCharacterCollection()->SendWearSentenceList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_SENTENCE_LIST_REQ)
{	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHARACTER_COLLECTION_SENTENCE_LIST_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	pUser->GetUserCharacterCollection()->SendSentenceList(rq.iPageNum);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_REQ)
{	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCharacterCollection()->SendHaveSentenceList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_WEAR_REQ)
{	
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHARACTER_COLLECTION_WEAR_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	SS2C_CHARACTER_COLLECTION_WEAR_RES rs;
	rs.btResult = pUser->GetUserCharacterCollection()->SentenceWearReq(rq, rs.iReleaseSentenceIndex);
	rs.btType = rq.btType;
	rs.iSlotIndex = rq.iSlotIndex;
	rs.iSentenceIndex = rq.iSentenceIndex;
	pUser->Send(S2C_CHARACTER_COLLECTION_WEAR_RES, &rs, sizeof(SS2C_CHARACTER_COLLECTION_WEAR_RES));

	if(RESULT_CHARACTER_COLLECTION_WEAR_SUCCESS == rs.btResult)
		pUser->SendAvatarInfoUpdateToMatch();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHARACTER_COLLECTION_IN_ROOM_WEAR_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHARACTER_COLLECTION_IN_ROOM_WEAR_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	SS2C_CHARACTER_COLLECTION_IN_ROOM_WEAR_RES rs;
	rs.btResult = pUser->GetUserCharacterCollection()->SentenceWearReq(rq);
	memcpy(rs.iSentenceIndex, rq.iSentenceIndex, sizeof(int)*MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT);
	pUser->Send(S2C_CHARACTER_COLLECTION_IN_ROOM_WEAR_RES, &rs, sizeof(SS2C_CHARACTER_COLLECTION_IN_ROOM_WEAR_RES));

	if(RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_SUCCESS == rs.btResult)
		pUser->SendAvatarInfoUpdateToMatch();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HOTGIRL_MISSION_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HOTGIRL_MISSION_INFO_REQ));

	if (pUser->GetClubSN() > 0)
		pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus( EVENT_HOTGIRL_MISSION_TYPE_JOIN_CLUB );

	pUser->GetUserHotGirlMissionEvent()->SendMissionInfo(rq.btMissionStepType, rq.bIsEventIcon);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HOTGIRL_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HOTGIRL_GET_REWARD_REQ));

	SHotGirlMissionEventRewardConfig RewardConfig;
	HOTGIRLMISSION.GetEventRewardConfig(rq.iRewardIndex, pUser->GetUserHotGirlMissionEvent()->GetMissionStepType(), pUser->GetUserHotGirlMissionEvent()->GetMissionStep(), pUser->GetUserHotGirlMissionEvent()->GetMissionSmallStep(), RewardConfig);

	SS2C_EVENT_HOTGIRL_GET_REWARD_RES rs;
	rs.RewardInfo.iRewardIndex = RewardConfig.iRewardIndex;
	rs.RewardInfo.iRewardType = RewardConfig.iRewardType;
	rs.RewardInfo.iItemCode = RewardConfig.iItemCode;
	rs.RewardInfo.iPropertyType = RewardConfig.iPropertyType;
	rs.RewardInfo.iValue = RewardConfig.iValue;
	rs.RewardInfo.iPropertyKind = RewardConfig.iPropertyKind;
	rs.RewardInfo.iTeamIndex = RewardConfig.iTeamIndex;
	rs.iGiveCouponCount = 0;

	rs.btResult = pUser->GetUserHotGirlMissionEvent()->GetRewardReq(rq.iRewardIndex, rs.iGiveCouponCount);
	pUser->Send(S2C_EVENT_HOTGIRL_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_HOTGIRL_GET_REWARD_RES));

	pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BONUS_MISSION_1);
	pUser->GetUserHotGirlMissionEvent()->CheckAndUpdateMissionStatus(EVENT_HOTGIRL_MISSION_TYPE_BONUS_MISSION_2);

	pUser->GetUserHotGirlMissionEvent()->SendMissionInfo(pUser->GetUserHotGirlMissionEvent()->GetMissionStepType());
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHotGirlMissionEvent()->SendBonusMissionInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_REQ));

	SS2C_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_RES rs;
	rs.iMissionType = rq.iMissionType;
	rs.btResult = pUser->GetUserHotGirlMissionEvent()->GetBonusMissionReward(rq.iMissionType);
	pUser->Send(S2C_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_HOTGIRL_BONUS_MISSION_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHotGirlMissionEvent()->SendBonusRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_REQ));

	SS2C_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_RES rs;
	rs.iRewardIndex = rq.iRewardIndex;
	rs.btResult = pUser->GetUserHotGirlMissionEvent()->GetBonusReward(rq.iRewardIndex, rs.iUpdatedCouponCount);
	pUser->Send(S2C_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_HOTGIRL_BONUS_REWARD_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHotGirlSpecialBoxEvent()->SendHotGirlSpecialBoxUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_OPEN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_HOTGIRLSPECIALBOX_OPEN_REQ	rs;
	m_ReceivePacketBuffer.Read(&rs);

	pUser->GetUserHotGirlSpecialBoxEvent()->OpenSpecialBox(rs.btCurrentSeason);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_HOTGIRLSPECIALBOX_MAIN_REWARD_GET_REQ	rs;
	m_ReceivePacketBuffer.Read(&rs);

	pUser->GetUserHotGirlSpecialBoxEvent()->GiveMainReward(rs.btCurrentSeason);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TODAY_HOTDEAL_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_TODAY_HOTDEAL_INFO_RES	rs;
	
	if( TRUE == pUser->GetTodayHotDealRes(rs) )
	{
		CPacketComposer Packet(S2C_EVENT_TODAY_HOTDEAL_INFO_RES);
		Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_TODAY_HOTDEAL_INFO_RES));

		pUser->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SKYLUCKY_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSkyLuckyEvent()->SendUserEventData();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SKYLUCKY_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSkyLuckyEvent()->GiveSkyLuckyReward();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHEERLEADER_EVENT_CHEERLEADER_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHEERLEADEREVENT.SendEventCheerLeaderList(pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHEERLEADER_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHEERLEADER_EVENT_INFO_REQ	rq;
	m_ReceivePacketBuffer.Read(&rq);

	CPacketComposer Packet(S2C_CHEERLEADER_EVENT_INFO_RES);
	CHEERLEADEREVENT.MakeCheerLeaderEventInfo(rq.iCheerLeaderCode, Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BUY_EVENT_CHEERLEADER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_BUY_EVENT_CHEERLEADER_INFO_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	CPacketComposer Packet(S2C_BUY_EVENT_CHEERLEADER_INFO_RES);
	SS2C_BUY_EVENT_CHEERLEADER_INFO_RES rs;
	CHEERLEADEREVENT.MakeCheerLeaderBuyInfo(rq.iCheerLeaderCode, Packet, rs);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BUY_EVENT_CHEERLEADER_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_BUY_EVENT_CHEERLEADER_REQ	rq;
	m_ReceivePacketBuffer.Read(&rq);

	SS2C_BUY_EVENT_CHEERLEADER_RES rs;
	rs.btResult = pUser->GetUserCheerLeader()->BuyEventCheerLeader(rq.iCheerLeaderCode, rq.btTypeNum, rq.iPeriod);

	if(rs.btResult != BUY_EVENT_CHEERLEADER_RESULT_SUCCESS)
		pUser->Send(S2C_BUY_EVENT_CHEERLEADER_RES, &rs, sizeof(SS2C_BUY_EVENT_CHEERLEADER_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
			
	pUser->CheckAndSendGoldenCrushInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_INITIALIZE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	pUser->InitializeGoldenCrushAndSend();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GOLDENCRUSH_CRUSH_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID( pUser );

	SC2S_EVENT_GOLDENCRUSH_CRUSH_REQ	info;

	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_EVENT_GOLDENCRUSH_CRUSH_REQ));
	
	SS2C_EVENT_GOLDENCRUSH_CRUSH_RES	rs;
	ZeroMemory(&rs, sizeof(SS2C_EVENT_GOLDENCRUSH_CRUSH_RES));
	
	rs.btType = info.btType;
	pUser->CrushEventGoldenCrush(info.btType, rs);
	
	if( RESULT_GOLDENCRUSH_CRUSH_SUCCESS != rs.iResult )
		pUser->Send(S2C_EVENT_GOLDENCRUSH_CRUSH_RES, &rs, sizeof(SS2C_EVENT_GOLDENCRUSH_CRUSH_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GOLDENSAFE_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserGoldenSafeEvent()->SendUserEventData();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GOLDENSAFE_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserGoldenSafeEvent()->GiveGoldenSafeReward();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_BASKETBALL_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_ENTER_BASKETBALL_RES ss;
	ss.bResult = true;
	if(FALSE == pFSClient->SetState(FS_BASKETBALL))
	{
		ss.bResult = false;
	}

	pUser->Send(S2C_ENTER_BASKETBALL_RES, &ss, sizeof(SS2C_ENTER_BASKETBALL_RES));
	pUser->SendFirstCashBackEventStatus();
	pUser->GetUserCashItemRewardEvent()->SendEventStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_BASKETBALL_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	SS2C_EXIT_BASKETBALL_RES ss;
	ss.bResult = true;
	if(FALSE == pFSClient->SetState(NEXUS_LOBBY))
	{
		ss.bResult = false;
	}

	pUser->Send(S2C_EXIT_BASKETBALL_RES, &ss, sizeof(SS2C_EXIT_BASKETBALL_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BASKETBALL_MY_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_BASKETBALL_MY_LIST_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_BASKETBALL_MY_LIST_REQ));

	pUser->GetUserBasketBall()->SendUserInventory(rs.btBallPartType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BASKETBALL_SHOP_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_BASKETBALL_SHOP_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_BASKETBALL_SHOP_LIST_REQ));

	pUser->GetUserBasketBall()->SendBasketBallShopList(rq.btBallPartType, rq.btItemType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BASKETBALL_PRODUCT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_BASKETBALL_PRODUCT_INFO_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_BASKETBALL_PRODUCT_INFO_REQ));

	pUser->GetUserBasketBall()->SendBasketBallProductInfo(rs);
}


DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHANGE_BASKETBALL_SKIN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHANGE_BASKETBALL_SKIN_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_CHANGE_BASKETBALL_SKIN_REQ));

	pUser->GetUserBasketBall()->ChangeBasketBallSkin(rs.iaBallPartIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CHANGE_BASKETBALL_OPTION_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CHANGE_BASKETBALL_OPTION_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_CHANGE_BASKETBALL_OPTION_REQ));

	pUser->GetUserBasketBall()->ChangeBasketBallOption(rs.bUseOption);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BUY_BASKETBALL_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_BUY_BASKETBALL_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_BUY_BASKETBALL_REQ));

	pUser->GetUserBasketBall()->BuyBasketBallSkin(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	pUser->GetUserShoppingEvent()->SendShoppingEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_EVENT_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserShoppingEvent()->SendShoppingEventRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_SELL_PRODUCT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SHOPPING_SELL_PRODUCT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SHOPPING_SELL_PRODUCT_REQ));

	SS2C_SHOPPING_SELL_PRODUCT_RES rs;
	rs.iProductIndex = rq.iProductIndex;
	rs.btResult = pUser->GetUserShoppingEvent()->SellProduct(rq.iProductIndex, rs);
	pUser->Send(S2C_SHOPPING_SELL_PRODUCT_RES, &rs, sizeof(SS2C_SHOPPING_SELL_PRODUCT_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_USE_COIN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SHOPPING_USE_COIN_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SHOPPING_USE_COIN_REQ));

	SS2C_SHOPPING_USE_COIN_RES rs;
	rs.iExchangeProductIndex = rq.iExchangeProductIndex;
	rs.btResult = pUser->GetUserShoppingEvent()->UseCoin(rq.iExchangeProductIndex);
	rs.iUpdatedCoin = pUser->GetUserShoppingEvent()->GetCurrentCoin();
	pUser->Send(S2C_SHOPPING_USE_COIN_RES, &rs, sizeof(SS2C_SHOPPING_USE_COIN_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLegendEvent()->SendLegendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLegendEvent()->SendLegendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_USE_KEY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_LEGEND_EVENT_USE_KEY_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_LEGEND_EVENT_USE_KEY_REQ));

	pUser->GetUserLegendEvent()->UseKey(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_USE_STAR_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_LEGEND_EVENT_USE_STAR_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_LEGEND_EVENT_USE_STAR_REQ));

	pUser->GetUserLegendEvent()->UseStar(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LEGEND_EVENT_GET_TRY_REWARD_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_LEGEND_EVENT_GET_TRY_REWARD_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_LEGEND_EVENT_GET_TRY_REWARD_ITEM_REQ));

	SS2C_LEGEND_EVENT_GET_TRY_REWARD_ITEM_RES rs;
	rs.iRewardIndex = rq.iRewardIndex;
	rs.btResult = pUser->GetUserLegendEvent()->GetTryRewardReq(rq.iRewardIndex);
	pUser->Send(S2C_LEGEND_EVENT_GET_TRY_REWARD_ITEM_RES, &rs, sizeof(SS2C_LEGEND_EVENT_GET_TRY_REWARD_ITEM_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANK_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_RANK_LIST_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_EVENT_RANK_LIST_REQ));

	if(EVENT_KIND_MINIGAMEZONE == rs.iEventKind)
	{
		CHECK_CONDITION_RETURN_VOID(CLOSED == MINIGAMEZONE.IsOpen());

		switch(rs.btVeiwType)
		{
		case MINIGAMEZONE_RANK_VIEW_TYPE_ALL:
			{
				SMyAvatar sAvatar;
				pUser->GetMyAvatarGameID(pUser->GetUserMiniGameZoneEvent()->GetMainGameIDIndex(), sAvatar);
				MINIGAMEZONE.SendRankList(rs, _GetServerIndex, pUser, sAvatar.szGameID, pUser->GetUserMiniGameZoneEvent()->GetLastScore(rs.btSeasonType, rs.btRecordType));
			}
			break;

		/*case MINIGAMEZONE_RANK_VIEW_TYPE_FRIENDS:
			{
				CHECK_CONDITION_RETURN_VOID(MINIGAMEZONE_RANK_SEASON_TYPE_CURRENT != rs.btSeasonType);

				pUser->GetUserMiniGameZoneEvent()->CheckAndLoadRankList(rs.btVeiwType, rs.btRecordType, pUser->GetGameIDIndex(), pUser->GetFriendCnt() + 1);
				pUser->GetUserMiniGameZoneEvent()->SendRankList(rs, 0, pUser, pUser->GetGameID(), pUser->GetUserMiniGameZoneEvent()->GetLastScore(rs.btRecordType));
			}
			break;
			
		case MINIGAMEZONE_RANK_VIEW_TYPE_CLUB:
			{
				CHECK_CONDITION_RETURN_VOID(MINIGAMEZONE_RANK_SEASON_TYPE_CURRENT != rs.btSeasonType);

				if(pUser->GetClubSN() > 0)
				{
					CClubSvrProxy* pClub = (CClubSvrProxy*)GAMEPROXY.GetProxy(FS_CLUB_SERVER_PROXY);
					if(pClub)
					{
						SG2CL_EVENT_RANK_LIST_REQ	ss;
						ss.iClubSN = pUser->GetClubSN();
						strncpy_s(ss.szGameID, _countof(ss.szGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
						ss.info.iEventKind = rs.iEventKind;
						ss.info.btRecordType = rs.btRecordType;
						ss.info.btSeasonType = rs.btSeasonType;
						ss.info.btVeiwType = rs.btVeiwType;
						ss.iGameIDIndex = pUser->GetGameIDIndex();

						pClub->SendPacket(G2CL_EVENT_RANK_LIST_REQ, &ss, sizeof(SG2CL_EVENT_RANK_LIST_REQ));
					}
				}
				else
				{
					SS2C_EVENT_RANK_LIST_RES	ss;
					ZeroMemory(&ss, sizeof(SS2C_EVENT_RANK_LIST_RES));

					ss.iEventKind = rs.iEventKind;
					ss.btVeiwType = rs.btVeiwType;
					ss.btRecordType = rs.btRecordType;
					ss.btSeasonType = rs.btSeasonType;
					ss.btListIndex = 0;
					ss.bLastList = TRUE;
					ss.iMyRank = 0;
					ss.btMyServerIndex = btServerIndex;
					strncpy_s(ss.szMyGameID, _countof(ss.szMyGameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
					ss.iValue = 0;
					ss.iRankCount = 0;

					pUser->Send(S2C_EVENT_RANK_LIST_RES, &ss, sizeof(SS2C_EVENT_RANK_LIST_RES));
				}
			}
			break;*/
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANK_MY_AVATAR_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendEventRankMyAvatarList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANK_MAIN_GAMEID_SELECT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_RANK_MAIN_GAMEID_SELECT_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_EVENT_RANK_MAIN_GAMEID_SELECT_REQ));

	pUser->SelectEventRankMainGameID(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMiniGameZoneEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMiniGameZoneEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_USE_TICKET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MINIGAMEZONE_USE_TICKET_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_MINIGAMEZONE_USE_TICKET_REQ));

	pUser->GetUserMiniGameZoneEvent()->UseTicket(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_USE_COIN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MINIGAMEZONE_USE_COIN_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_MINIGAMEZONE_USE_COIN_REQ));

	pUser->GetUserMiniGameZoneEvent()->UseCoin(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MINIGAMEZONE_UPDATE_SCORE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MINIGAMEZONE_UPDATE_SCORE_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_MINIGAMEZONE_UPDATE_SCORE_REQ));

	BOOL bIsBestScore = FALSE;
	pUser->GetUserMiniGameZoneEvent()->UpdateScore(rs, bIsBestScore);

	if(bIsBestScore)
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			SS2O_EVENT_MINIGAMEZONE_UPDATE_USER_LOG_NOT ss;

			pUser->GetUserMiniGameZoneEvent()->CheckResetDate();

			ss.iUserIDIndex = pUser->GetUserIDIndex();
			ss.btServerIndex = _GetServerIndex;
			ss.iSeason = pUser->GetUserMiniGameZoneEvent()->GetCurrentSeason();
			ss.iGameIDIndex = pUser->GetUserMiniGameZoneEvent()->GetMainGameIDIndex();

			SMyAvatar sAvatar;
			pUser->GetMyAvatarGameID(ss.iGameIDIndex, sAvatar);
			memcpy(ss.szGameID, sAvatar.szGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);

			switch(rs.btGameType)
			{
			case MINIGAMEZONE_GAME_TYPE_FIND_ORDER_NUM:
				ss.btRecordType = MINIGAMEZONE_RANK_RECORD_TYPE_FIND_ORDER_NUM;
				break;
			case MINIGAMEZONE_GAME_TYPE_FIND_PAIR_NUM:
				ss.btRecordType = MINIGAMEZONE_RANK_RECORD_TYPE_FIND_PAIR_NUM;
				break;
			case MINIGAMEZONE_GAME_TYPE_BASKETBALL:
				ss.btRecordType = MINIGAMEZONE_RANK_RECORD_TYPE_BASKETBALL;
				break;
			}
			ss.iBestScore = pUser->GetUserMiniGameZoneEvent()->GetBestScore(rs.btGameType);

			CPacketComposer	Packet(S2O_EVENT_MINIGAMEZONE_UPDATE_USER_LOG_NOT);
			Packet.Add((PBYTE)&ss,sizeof(SS2O_EVENT_MINIGAMEZONE_UPDATE_USER_LOG_NOT));

			pODBCSvr->Send(&Packet);
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOVEPAYBACK_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CHECK_CONDITION_RETURN_VOID(CLOSED == LOVEPAYBACK.IsOpen());

	CPacketComposer* pPacket = LOVEPAYBACK.GetPacketEventInfo();
	pUser->Send(pPacket);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOVEPAYBACK_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLovePaybackEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LOVEPAYBACK_USE_LOVE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_LOVEPAYBACK_USE_LOVE_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_LOVEPAYBACK_USE_LOVE_REQ));

	pUser->GetUserLovePaybackEvent()->UseLove(rs.btProductIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BIGWHEEL_LOGIN_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserBigWheelLoginEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BIGWHEEL_LOGIN_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserBigWheelLoginEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_BIGWHEEL_LOGIN_STAMP_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_BIGWHEEL_LOGIN_STAMP_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_BIGWHEEL_LOGIN_STAMP_REQ));

	CHECK_CONDITION_RETURN_VOID(
		BIGWHEEL_LOGIN_STAMP_TYPE_TODAY != rs.btStampType && 
		BIGWHEEL_LOGIN_STAMP_TYPE_LAST != rs.btStampType);

	SS2C_BIGWHEEL_LOGIN_STAMP_RES ss;
	ss.btResult = pUser->GetUserBigWheelLoginEvent()->Stamp(rs.btStampType);
	pUser->Send(S2C_BIGWHEEL_LOGIN_STAMP_RES, &ss, sizeof(SS2C_BIGWHEEL_LOGIN_STAMP_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SALEPLUS_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSalePlusEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MISSION_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDriveMissionEvent()->SendEventMissionInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDriveMissionEvent()->SendEventProductInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDriveMissionEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_USER_CHOICE_MISSION_2ND_CHOICE_MISSION_REQ));

	pUser->GetUserDriveMissionEvent()->ChoiceMission(rs.btMissionIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_MONEY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDriveMissionEvent()->GiveMoney(); 
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_USER_CHOICE_MISSION_2ND_BUY_PRODUCT_REQ));

	pUser->GetUserDriveMissionEvent()->BuyProduct(rs.btProductIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_2ND_BUTTON_STATUS_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDriveMissionEvent()->SendButtonStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_FIRSTCASH_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserFirstCashEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRandomArrowEvent()->SendEventInfo_Product();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_POT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRandomArrowEvent()->SendEventInfo_Pot();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRandomArrowEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_USE_ARROW_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMARROW_USE_ARROW_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_RANDOMARROW_USE_ARROW_REQ));

	pUser->GetUserRandomArrowEvent()->UseArrow(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMARROW_USE_POT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMARROW_USE_POT_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_RANDOMARROW_USE_POT_REQ));

	pUser->GetUserRandomArrowEvent()->UsePot(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserVendingMachineEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserVendingMachineEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_INPUT_MONEY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserVendingMachineEvent()->InputMoney();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_VENDINGMACHINE_USE_MONEY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_VENDINGMACHINE_USE_MONEY_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_VENDINGMACHINE_USE_MONEY_REQ));

	pUser->GetUserVendingMachineEvent()->UseMoney(rs.btProductIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDevilTemtationEvent()->SendEventProductInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_BONUS_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDevilTemtationEvent()->SendEventBonusProductInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDevilTemtationEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_USE_TICKET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_DEVILTEMTATION_USE_TICKET_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_DEVILTEMTATION_USE_TICKET_REQ));

	pUser->GetUserDevilTemtationEvent()->UseDevilTemtation(rs.btTicketType, rs.btTicketUseType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DEVILTEMTATION_USE_SOULSTONE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_DEVILTEMTATION_USE_SOULSTONE_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_DEVILTEMTATION_USE_SOULSTONE_REQ));

	pUser->GetUserDevilTemtationEvent()->UseSoulStone(rs.btSoulStoneIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LVUPEVENT_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendJumpingLvUpEventUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENTCASH_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->CheckAndSendEventCoinInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PUZZLES_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendPuzzleRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_ATTENDANCE_ITEMSHOP_BOARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CPacketComposer	Packet(S2C_EVENT_ATTENDANCE_ITEMSHOP_BOARD_INFO_RES);

	if( TRUE == ATTENDANCEITEMSHOP.MakePacketBoardInfo(Packet))
	{
		pUser->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LVUPEVENT_GET_CHAR_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	//pUser->CheckMainRewardLog();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRandomItemBoardEvent()->SendRandomItemBoardEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_RANDOMITEMBOARD_EVENT_USE_BOARDTICKET_REQ));

	pUser->GetUserRandomItemBoardEvent()->UseBoardTicket(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_RANDOMITEMBOARD_EVENT_SAVE_ITEMBAR_REQ));

	pUser->GetUserRandomItemBoardEvent()->SaveRandomItemBoard(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEPLAY_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_GAMEPLAY_EVENT_INFO_RES);
	CHECK_CONDITION_RETURN_VOID(FALSE == pUser->GetUserGamePlayEvent()->MakeGamePlayEventInfo(Packet));

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEPLAY_EVNET_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserGamePlayEvent()->GetRewardReq();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_OPEN_EVENT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	EVENTDATEMANAGER.SendOpenEventList(pUser, FALSE);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_LUCKYSTAR_SESSION_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == EVENTDATEMANAGER.IsOpen(EVENT_KIND_LUCKYSTART));

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_LUCKYSTAR_SESSION_INFO_REQ rq;
	m_ReceivePacketBuffer.Read(&rq);

	SG2S_LUCKYSTAR_SESSION_INFO_REQ info;
	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.iMode = rq.iMode;
	info.iSessionIdx = rq.iSessionIdx;

	CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
	if(pCenter)
	{
		CPacketComposer packet( G2S_LUCKYSTAR_SESSION_INFO_REQ );
		packet.Add( (PBYTE)&info, sizeof( SG2S_LUCKYSTAR_SESSION_INFO_REQ ) );
		pCenter->Send( &packet );
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_MISSION_INFO_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_EVENT_MISSION_INFO_REQ));

	if(MISSIONEVENT_MISSION_NUM_1ST == rs.btMissionNum)
	{
		MISSIONEVENT.SendMissionInfo(pUser);
	}
	else if(MISSIONEVENT_MISSION_NUM_2ND == rs.btMissionNum)
	{
		MISSIONEVENT.SendMissionInfo_2nd(pUser);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MISSION_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_MISSION_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_MISSION_GET_REWARD_REQ)); 

	SS2C_EVENT_MISSION_GET_REWARD_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.iRandomResultValue = 0;
	rs.btResult = pUser->GetUserMissionEvent()->GetRewardReq(rq.iMissionIndex, rs.iRandomResultValue);
	pUser->Send(S2C_EVENT_MISSION_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_MISSION_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MISSION_CHECK_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_MISSION_CHECK_GET_REWARD_RES rs;
	rs.bIsGetReward = FALSE;
	if(OPEN == EVENTDATEMANAGER.IsOpen(EVENT_KIND_MISSION_2ND))
		rs.bIsGetReward = pUser->GetUserMissionEvent()->CheckCompletedMission();
	pUser->Send(S2C_EVENT_MISSION_CHECK_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_MISSION_CHECK_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	// ����Ƚ�� �ʱ�ȭ
	if(pUser->GetLocation() == U_LOBBY)
		pUser->GetUserChoiceMissionEvent()->CheckAndUpdateMission(USERCHOICE_MISSIONEVENT_CONDITION_TYPE_GAMEPLAY_WINCONTINUE_COUNT, 0);

	pUser->GetUserChoiceMissionEvent()->SendMissionInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_CHANGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_CHOICE_MISSION_CHANGE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_USER_CHOICE_MISSION_CHANGE_REQ)); 

	SS2C_USER_CHOICE_MISSION_CHANGE_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.btResult = pUser->GetUserChoiceMissionEvent()->ChangeMissionReq(rq.iMissionIndex);
	pUser->Send(S2C_USER_CHOICE_MISSION_CHANGE_RES, &rs, sizeof(SS2C_USER_CHOICE_MISSION_CHANGE_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_CHOICE_MISSION_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_CHOICE_MISSION_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_USER_CHOICE_MISSION_GET_REWARD_REQ)); 

	SS2C_USER_CHOICE_MISSION_GET_REWARD_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.btResult = pUser->GetUserChoiceMissionEvent()->GetRewardReq(rq.iMissionIndex);
	pUser->Send(S2C_USER_CHOICE_MISSION_GET_REWARD_RES, &rs, sizeof(SS2C_USER_CHOICE_MISSION_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_TREE_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRandomItemTreeEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_TREE_EVENT_USE_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_RANDOMITEM_TREE_EVENT_USE_ITEM_RES rs;
	if(RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_SUCCESS != (rs.btResult = pUser->GetUserRandomItemTreeEvent()->UseItemReq()))
	{
		rs.iRewardCount = 0;
		pUser->Send(S2C_RANDOMITEM_TREE_EVENT_USE_ITEM_RES, &rs, sizeof(SS2C_RANDOMITEM_TREE_EVENT_USE_ITEM_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserExpBoxItemEvent()->SendBoxList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_AVATAR_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserExpBoxItemEvent()->SendAvatarList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_REQ)); 

	int iPreLv = pUser->GetCurUsedAvtarLv();

	SS2C_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_RES rs;
	pUser->GetUserExpBoxItemEvent()->GetRewardReq(CFSGameServer::GetInstance(), rq, rs);
	pUser->Send(S2C_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_RES, &rs, sizeof(SS2C_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_RES));

	if(rs.btResult == RESULT_USER_EXPBOXITEM_EVENT_BOX_GET_REWARD_SUCCESS)
	{
		if ( rs.iLv > iPreLv )
		{
			SMyAvatar	sAvatar;
			int iGamePosition = pUser->GetMyAvatar(rs.iGameIDIndex, sAvatar);
			pUser->ProcessLevelUp(rs.iGameIDIndex, sAvatar.iGamePosition, iPreLv, rs.iLv, rs.iExp);			
		}
	
		if(FALSE == rs.bBonusBox)
			pUser->GetUserExpBoxItemEvent()->SendEventOpenStatus();
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOGINPAYBACK_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLoginPayBackEvent()->SendLoginPayBackUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOGINPAYBACK_REQUEST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLoginPayBackEvent()->RequestPayBack();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSpecialAvatarPieceEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_CHANGE_PIECE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SPEICAL_AVATAR_PIECE_CHANGE_PIECE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SPEICAL_AVATAR_PIECE_CHANGE_PIECE_REQ));

	pUser->GetUserSpecialAvatarPieceEvent()->ChangePiece(rq.btDelPieceType, rq.btAddPieceType, rq.iAddCount);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SPEICAL_AVATAR_PIECE_USE_PIECE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SPEICAL_AVATAR_PIECE_USE_PIECE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SPEICAL_AVATAR_PIECE_USE_PIECE_REQ));

	pUser->GetUserSpecialAvatarPieceEvent()->UsePiece(rq.btPieceType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SONOFCHOICE_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendSonOfChoiceEventProductList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SONOFCHOICE_BUY_RANDOM_PRODUCT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SONOFCHOICE_BUY_RANDOM_PRODUCT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SONOFCHOICE_BUY_RANDOM_PRODUCT_REQ));

	pUser->BuySonOfChoiceEventRandomProduct(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_BOARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMissionBingoEvent()->SendBoardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMissionBingoEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MISSION_BINGO_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_MISSION_BINGO_GET_REWARD_REQ));

	pUser->GetUserMissionBingoEvent()->GiveBingoReward(rq.btRewardPositionType, rq.btPosX, rq.btPosY);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSION_BINGO_BOARD_RESET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMissionBingoEvent()->ResetMissionBingo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRandomItemGroupEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_REWARD_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMITEM_GROUP_EVENT_REWARD_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_RANDOMITEM_GROUP_EVENT_REWARD_LIST_REQ)); 

	pUser->GetUserRandomItemGroupEvent()->SendRewardList(rq.btGroupType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRandomItemGroupEvent()->SendExchangeProductList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_USE_COIN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMITEM_GROUP_EVENT_USE_COIN_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_RANDOMITEM_GROUP_EVENT_USE_COIN_REQ)); 

	SS2C_RANDOMITEM_GROUP_EVENT_USE_COIN_RES rs;
	rs.btGroupType = rq.btGroupType;
	rs.btResult = pUser->GetUserRandomItemGroupEvent()->UseCoinReq(rq.btGroupType, rs);
	pUser->Send(S2C_RANDOMITEM_GROUP_EVENT_USE_COIN_RES, &rs, sizeof(SS2C_RANDOMITEM_GROUP_EVENT_USE_COIN_RES));

	if(RESULT_RANDOMITEM_GROUP_EVENT_USE_COIN_SUCCESS == rs.btResult)
		pUser->GetUserMissionEvent()->CheckAndUpdateMission(MISSIONEVENT_CONDITION_TYPE_RANDOMITEM_GROUP_OPEN);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_REQ)); 

	SS2C_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_RES rs;
	rs.iProductIndex = rq.iProductIndex;
	rs.btResult = pUser->GetUserRandomItemGroupEvent()->ExchangeProductReq(rq.iProductIndex, rs);
	pUser->Send(S2C_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_RES, &rs, sizeof(SS2C_RANDOMITEM_GROUP_EVENT_EXCHANGE_PRODUCT_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SALE_RANDOMITEM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSaleRandomItemEvent()->ClearItem();
	pUser->GetUserSaleRandomItemEvent()->SendSaleItemList();
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SALE_RANDOMITEM_OPEN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_SALE_RANDOMITEM_OPEN_RES rs;
	rs.btResult = pUser->GetUserSaleRandomItemEvent()->OpenReq(rs._sReward);
	pUser->Send(S2C_SALE_RANDOMITEM_OPEN_RES, &rs, sizeof(SS2C_SALE_RANDOMITEM_OPEN_RES));
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RAINBOW_WEEK_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserRainbowWeekEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RAINBOW_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_RAINBOW_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_RAINBOW_GET_REWARD_REQ)); 

	SS2C_EVENT_RAINBOW_GET_REWARD_RES rs;
	rs.btResult = pUser->GetUserRainbowWeekEvent()->GetRewardReq(rq.iRewardIndex);
	rs.btRewardStatus = pUser->GetUserRainbowWeekEvent()->GetCurrentRewardStatus();
	rs.iRewardIndex = rq.iRewardIndex;
	pUser->Send(S2C_EVENT_RAINBOW_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_RAINBOW_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserGameOfDiceEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserGameOfDiceEvent()->SendRankAvatarList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_REQ)); 

	SS2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_RES rs;
	rs.btResult = pUser->GetUserGameOfDiceEvent()->SelectRankAvatarReq(rq.iGameIDIndex);
	rs.iGameIDIndex = rq.iGameIDIndex;
	pUser->Send(S2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_RES, &rs, sizeof(SS2C_GAMEOFDICE_EVENT_SELECT_RANK_AVATAR_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_SELECT_CHARACTER_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_GAMEOFDICE_EVENT_SELECT_CHARACTER_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_GAMEOFDICE_EVENT_SELECT_CHARACTER_REQ)); 

	SS2C_GAMEOFDICE_EVENT_SELECT_CHARACTER_RES rs;
	rs.btResult = pUser->GetUserGameOfDiceEvent()->SelectCharacterReq(rq.iCharacterIndex);
	rs.iCharacterIndex = rq.iCharacterIndex;
	pUser->Send(S2C_GAMEOFDICE_EVENT_SELECT_CHARACTER_RES, &rs, sizeof(SS2C_GAMEOFDICE_EVENT_SELECT_CHARACTER_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_DICE_ROLL_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_GAMEOFDICE_EVENT_DICE_ROLL_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_GAMEOFDICE_EVENT_DICE_ROLL_REQ)); 

	SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES rs;
	rs.btDiceRollType = rq.btDiceRollType;
	rs.btResult = pUser->GetUserGameOfDiceEvent()->DiceRollReq(rq.btDiceRollType, rs);

	CPacketComposer Packet(S2C_GAMEOFDICE_EVENT_DICE_ROLL_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES));
	pUser->Send(&Packet);

	if(RESULT_GAMEOFDICE_EVENT_DICE_ROLL_SUCCESS == rs.btResult)
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			SS2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT not;
			not.btServerIndex = _GetServerIndex;
			pUser->GetUserGameOfDiceEvent()->MakeIntegrateDB_UpdateUserLog(not);

			CPacketComposer	Packet(S2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT);
			Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_GAMEOFDICE_USER_LOG_UPDATE_NOT));

			pODBCSvr->Send(&Packet);
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_BUY_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_GAMEOFDICE_EVENT_BUY_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_GAMEOFDICE_EVENT_BUY_ITEM_REQ)); 

	CPacketComposer Packet(S2C_GAMEOFDICE_EVENT_BUY_ITEM_RES);
	SS2C_GAMEOFDICE_EVENT_BUY_ITEM_RES rs;
	rs.iItemShopItemCnt = 0;
	if(RESULT_GAMEOFDICE_EVENT_BUY_ITEM_SUCCESS != (rs.btResult = pUser->GetUserGameOfDiceEvent()->BuyItemReq(rq.iItemIndex, Packet, rs)))
		Packet.Add((PBYTE)&rs, sizeof(SS2C_GAMEOFDICE_EVENT_BUY_ITEM_RES));
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_CHEAT_DICE_ROLL_REQ)
{
#ifdef _DEBUG
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_GAMEOFDICE_EVENT_CHEAT_DICE_ROLL_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_GAMEOFDICE_EVENT_CHEAT_DICE_ROLL_REQ)); 

	SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES rs;
	rs.btResult = pUser->GetUserGameOfDiceEvent()->TestDiceRollReq(rq.iDiceNumber, rs.btDiceRollType, rs);

	CPacketComposer Packet(S2C_GAMEOFDICE_EVENT_DICE_ROLL_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES));
	pUser->Send(&Packet);
#endif
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_CHEAT_TICKET_INIT_REQ)
{
#ifdef _DEBUG
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_GAMEOFDICE_EVENT_DICE_ROLL_RES rs;
	pUser->GetUserGameOfDiceEvent()->TestInitDiceTicket();
	pUser->GetUserGameOfDiceEvent()->SendEventInfo();
#endif
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_RANK_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_GAMEOFDICE_EVENT_RANK_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_GAMEOFDICE_EVENT_RANK_INFO_REQ)); 

	SS2C_GAMEOFDICE_EVENT_RANK_INFO_RES rs;
	rs.btRankType = rq.btRankType;
	rs.iMyRank = 0;
	rs.btMyServerIndex = _GetServerIndex;
	memcpy(rs.szMyGameID, pUser->GetUserGameOfDiceEvent()->GetRankGameID(), sizeof(char)*MAX_GAMEID_LENGTH+1);
	rs.iValue = pUser->GetUserGameOfDiceEvent()->GetValue(rq.btRankType);
	rs.iTotalRankCount = 0;
	rs.iRewardCount = 0;

	GAMEOFDICEEVENT.SendRankInfo(_GetServerIndex, rq.btRankType, pUser, rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GAMEOFDICE_EVENT_RANK_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_GAMEOFDICE_EVENT_RANK_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_GAMEOFDICE_EVENT_RANK_LIST_REQ)); 

	GAMEOFDICEEVENT.SendRankList(rq.btRankType, pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_WELCOME_USER_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserWelcomeUserEvent()->SendWelcomeUserEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_WELCOME_USER_EVENT_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_WELCOME_USER_EVENT_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_WELCOME_USER_EVENT_GET_REWARD_REQ)); 

	SS2C_WELCOME_USER_EVENT_GET_REWARD_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.btResult = pUser->GetUserWelcomeUserEvent()->GetRewardReq(rq.iMissionIndex);
	pUser->Send(S2C_WELCOME_USER_EVENT_GET_REWARD_RES, &rs, sizeof(SS2C_WELCOME_USER_EVENT_GET_REWARD_RES));

	if(TRUE == pUser->GetUserWelcomeUserEvent()->CheckCompletedGetReward())
	{
		EVENTDATEMANAGER.SendOpenEventList(pUser, FALSE);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PCROOM_BENEFIT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPCRoomEvent()->SendPCRoomBenefitInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CASHITEM_REWARD_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCashItemRewardEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CASHITEM_REWARD_EVENT_OPEN_BOX_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CASHITEM_REWARD_EVENT_OPEN_BOX_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_CASHITEM_REWARD_EVENT_OPEN_BOX_REQ)); 

	SS2C_CASHITEM_REWARD_EVENT_OPEN_BOX_RES rs;
	rs.btResult = pUser->GetUserCashItemRewardEvent()->OpenBoxReq(rq.btBoxType, rs);
	pUser->Send(S2C_CASHITEM_REWARD_EVENT_OPEN_BOX_RES, &rs, sizeof(SS2C_CASHITEM_REWARD_EVENT_OPEN_BOX_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_GOTO_LOBBY_IN_CHANNEL_NOT)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	// TODO : �κ�� �̵� ������ Ŭ���̾�Ʈ���� ����
	pUser->GetUserRankMatch()->LoadUserNotice();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPasswordEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPasswordEvent()->SendEventRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_TRY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SET_PASSWORD_EVENT_TRY_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SET_PASSWORD_EVENT_TRY_REQ)); 

	SS2C_SET_PASSWORD_EVENT_TRY_RES rs;
	rs.btResult = pUser->GetUserPasswordEvent()->TryReq(rq, rs);
	pUser->Send(S2C_SET_PASSWORD_EVENT_TRY_RES, &rs, sizeof(SS2C_SET_PASSWORD_EVENT_TRY_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SET_PASSWORD_EVENT_INITIALIZE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPasswordEvent()->InitializePassword();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHalloweenGamePlayEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_REQ)); 

	SS2C_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_RES rs;
	rs.iItemIndex = rq.iItemIndex;
	rs.btResult = pUser->GetUserHalloweenGamePlayEvent()->ExchangeItemReq(rq.iItemIndex, rs);
	pUser->Send(S2C_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_RES, &rs, sizeof(SS2C_EVENT_HALLOWEEN_GAMEPLAY_EXCHANGE_ITEM_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_RES rs;
	rs.btResult = pUser->GetUserHalloweenGamePlayEvent()->TryCandyReq(rs);
	pUser->Send(S2C_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_RES, &rs, sizeof(SS2C_EVENT_HALLOWEEN_GAMEPLAY_TRY_CANDY_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_RES rs;
	rs.btResult = pUser->GetUserHalloweenGamePlayEvent()->OpenRandomBoxReq(rs);
	pUser->Send(S2C_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_RES, &rs, sizeof(SS2C_EVENT_HALLOWEEN_GAMEPLAY_OPEN_RANDOMBOX_RES));
	pUser->GetUserHalloweenGamePlayEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_SHOPPING_FESTIVAL_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_INFO_REQ)); 

	pUser->GetUserShoppingFestivalEvent()->SendEventInfo(rq.btEventType);
}

DEFINE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserShoppingFestivalEvent()->SendBuyShoppingBasketInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_RES rs;
	rs.btResult = pUser->GetUserShoppingFestivalEvent()->CheckBuyShoppingBasketReq();
	if(RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_SUCCESS != rs.btResult)
	{
		pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
		pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_RES));
		return;
	}

	rs.btResult = pUser->BuyShoppingBasket();
	if(RESULT_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_SUCCESS != rs.btResult)
	{
		pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
		pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_BUY_SHOPPINGBASKET_RES));
		return;
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_REQ)); 

	SC2S_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_RES rs;
	rs.iIndex = rq.iIndex;
	rs.btResult = pUser->GetUserShoppingFestivalEvent()->RemoveShoppingBasketItemReq(rq.iIndex);
	pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_REMOVE_SHOPPINGBASKET_ITEM_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_REQ)); 

	SODBCEventUserShoppingBasketItem Item;
	SC2S_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_RES rs;
	rs.btResult = pUser->CheckAddShoppingBasketItemReq(rq, Item);
	if(RESULT_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_SUCCESS != rs.btResult)
	{
		pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_RES));
		return;
	}

	rs.btResult = pUser->GetUserShoppingFestivalEvent()->AddShoppingBasketItemReq(Item);
	pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_ADD_SHOPPINGBASKET_ITEM_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	// �������� ���� �̺�Ʈ
	//SC2S_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_REQ rq;
	//m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_REQ)); 

	//SS2C_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_RES rs;
	//rs.iPropertyKind = rq.iPropertyKind;

	//rs.btResult = pUser->GetUserShoppingFestivalEvent()->GetSaleCouponReq(rq.iPropertyKind);
	//pUser->Send(S2C_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_RES, &rs, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_GET_SALE_COUPON_RES));
	//pUser->GetUserShoppingFestivalEvent()->SendEventOpenStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_REQ)); 

	SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_RES rs;
	rs.iIndex = rq.iIndex;
	pUser->GetUserShoppingFestivalEvent()->MakeLimitedSaleItemBuyInfo(rs);
	pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_INFO_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(S2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SS2C_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_REQ)); 

	SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES rs;
	rs.iIndex = rq.iIndex;

	SEventShoppingFestivalLimitedSaleItem sItem;
	rs.btResult = pUser->GetUserShoppingFestivalEvent()->CheckBuyLimitedSaleItemReq(rq.iIndex, sItem);
	if(RESULT_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_SUCCESS != rs.btResult)
	{
		pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES));
		return;
	}

	if(LIMITED_SALEITEM_ITEMTYPE_SERVER_LIMITED == sItem._iItemType)
	{
		if(0 == sItem._iRemainSellCount)
		{
			rs.btResult = RESULT_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_ENOUGHT_SELLCOUNT;
			pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES));
			return;
		}

		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if(!pCenter)
		{
			rs.btResult = RESULT_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_FAIL;
			pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES));
			return;
		}

		pUser->GetUserShoppingFestivalEvent()->SetIsBuying(TRUE);

		SG2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_REQ ss;
		strncpy_s(ss.GameID, _countof(ss.GameID), pUser->GetGameID(), MAX_GAMEID_LENGTH);	
		ss.iGameIDIndex = pUser->GetGameIDIndex();
		ss.iIndex = rq.iIndex;

		CPacketComposer Packet(G2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_REQ);
		Packet.Add((BYTE*)&ss, sizeof(SG2S_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_REQ));
		pCenter->Send(&Packet);
	}
	else
	{
		CFSItemShop* pItemShop = CFSGameServer::GetInstance()->GetItemShop();
		CHECK_NULL_POINTER_VOID(pItemShop);

		SBillingInfo BillingInfo;
		BillingInfo.iEventCode = EVENT_BUY_EVENT_LIMITED_SALEITEM;
		BillingInfo.pUser = pUser;
		memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH+1);
		BillingInfo.iSerialNum = pUser->GetUserIDIndex();
		memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH+1);	
		memcpy(BillingInfo.szPublisherUserNo, BillingInfo.pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH+1);
		memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
		BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
		char* szItemName = pItemShop->GetItemTextCheckAndReturnEventCodeDescription(BillingInfo.iItemCode, BillingInfo.iEventCode);
		if(NULL == szItemName)
			printf(BillingInfo.szItemName, "Buy_ItemShop");
		else
			memcpy(BillingInfo.szItemName, szItemName, MAX_ITEMNAME_LENGTH + 1);	

		BillingInfo.iSellType = SELL_TYPE_CASH;
		BillingInfo.iCurrentCash = pUser->GetCoin();
		BillingInfo.iItemCode = sItem._sReward.iItemCode;
		BillingInfo.iCashChange = sItem._iSalePrice;
		BillingInfo.iPointChange = 0;
		BillingInfo.iTrophyChange = 0;
		BillingInfo.iParam5 = rq.iIndex;
		BillingInfo.iParam6 = sItem._sReward.iRewardIndex;

		pUser->GetUserShoppingFestivalEvent()->SetIsBuying(TRUE);
		if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
		{
			pUser->GetUserShoppingFestivalEvent()->SetIsBuying(FALSE);
			rs.btResult = RESULT_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_FAIL;
			pUser->Send(C2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES, &rs, sizeof(SC2S_EVENT_SHOPPING_FESTIVAL_LIMTED_SALEITEM_BUY_ITEM_RES));
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALFPRICE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
			
	pUser->SendEventHalfPriceUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALFPRICE_ITEMLIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_EVENT_HALFPRICE_ITEMLIST_RES);
	HALFPRICE.MakePacketItemList(Packet);

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HALFPRICE_USE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HALFPRICE_USE_REQ req;

	m_ReceivePacketBuffer.Read((PBYTE)&req, sizeof(SC2S_EVENT_HALFPRICE_USE_REQ)); 

	pUser->UseHalfPrice(req);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserLottoEvent()->CheckAndGiveReward();
	pUser->GetUserLottoEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_REQ)); 

	SS2C_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_RES rs;
	rs.iSlot = rq.iSlot;
	rs.iLotteryNumber = 0;
	rs.btResult = pUser->GetUserLottoEvent()->OpenLotteryNumberReq(rq.iSlot, rs.iLotteryNumber);
	pUser->Send(S2C_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_RES, &rs, sizeof(SS2C_EVENT_LOTTO_OPEN_LOTTERY_NUMBER_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_RANDOM_NUMBER_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_LOTTO_OPEN_RANDOM_NUMBER_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_LOTTO_OPEN_RANDOM_NUMBER_REQ)); 

	SS2C_EVENT_LOTTO_OPEN_RANDOM_NUMBER_RES rs;
	rs.iSlotCount = 0;
	if(RESULT_EVENT_LOTTO_OPEN_RANDOM_NUMBER_SUCCESS != (rs.btResult = pUser->GetUserLottoEvent()->OpenRandomNumberReq(rq)))
	{		
		pUser->Send(S2C_EVENT_LOTTO_OPEN_RANDOM_NUMBER_RES, &rs, sizeof(SS2C_EVENT_LOTTO_OPEN_RANDOM_NUMBER_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_LOTTO_OPEN_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_LOTTO_OPEN_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_LOTTO_OPEN_REWARD_REQ)); 

	SS2C_EVENT_LOTTO_OPEN_REWARD_RES rs;
	rs.iSlotCount = 0;
	if(RESULT_EVENT_LOTTO_OPEN_REWARD_SUCCESS != (rs.btResult = pUser->GetUserLottoEvent()->OpenRewardReq(rq)))
	{
		pUser->Send(S2C_EVENT_LOTTO_OPEN_RANDOM_NUMBER_RES, &rs, sizeof(SS2C_EVENT_LOTTO_OPEN_REWARD_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPresentFromDeveloperEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_PRESENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPresentFromDeveloperEvent()->SendPresentInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_REQ)); 

	SS2C_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_RES rs;
	rs.btEventRewardType = rq.btEventRewardType;
	rs.btResult = pUser->GetUserPresentFromDeveloperEvent()->GetPresentReq(rq.btEventRewardType);
	pUser->Send(S2C_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_RES, &rs, sizeof(SS2C_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANDOMITEM_DRAW_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_RANDOMITEM_DRAW_INFO_RES rs;
	rs.iCoin = 0;
	SUserHighFrequencyItem* pItem = pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_DRAW_COIN);
	if(NULL != pItem)
		rs.iCoin = pItem->iPropertyTypeValue;

	CPacketComposer Packet(S2C_EVENT_RANDOMITEM_DRAW_INFO_RES);
	RANDOMITEMDRAWEVENT.MakeEventInfo(rs, Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_RANDOMITEM_DRAW_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_RANDOMITEM_DRAW_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_RANDOMITEM_DRAW_REQ)); 

	pUser->EventRandomItemDrawReq(rq.iProductGroupIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DISCOUNT_ITEM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDiscountItemEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DISCOUNT_ITEM_BUY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_DISCOUNT_ITEM_BUY_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_DISCOUNT_ITEM_BUY_REQ)); 

	SS2C_EVENT_DISCOUNT_ITEM_BUY_RES rs;
	rs.iStep = rq.iStep;
	int iPrice = 0, iDiscountPrice = 0;
	SRewardInfo sReward;
	rs.btResult = pUser->GetUserDiscountItemEvent()->CheckBuyReq(rq.iStep, sReward, iPrice, iDiscountPrice);
	if(rs.btResult != RESULT_EVENT_DISCOUNT_ITEM_BUY_SUCCESS)
	{
		pUser->Send(S2C_EVENT_DISCOUNT_ITEM_BUY_RES, &rs, sizeof(SS2C_EVENT_DISCOUNT_ITEM_BUY_RES));
		return;
	}

	SBillingInfo BillingInfo;
	BillingInfo.iEventCode = EVENT_BUY_EVENT_DISCOUNT_ITEM;
	BillingInfo.pUser = pUser;
	memcpy(BillingInfo.szUserID, pUser->GetUserID(), MAX_USERID_LENGTH + 1);	
	memcpy(BillingInfo.szGameID, pUser->GetGameID(), MAX_GAMEID_LENGTH + 1);	
	memcpy(BillingInfo.szPublisherUserNo, pUser->GetPublisherUserNo(), MAX_PUBLISHER_USERNO_LENGTH + 1);
	memcpy(BillingInfo.szIPAddress, pUser->GetIPAddress(), MAX_IPADDRESS_LENGTH+1);
	BillingInfo.szIPAddress[MAX_IPADDRESS_LENGTH] = 0;
	BillingInfo.iPropertyType = sReward.iPropertyType;
	BillingInfo.iTerm = sReward.iPropertyValue;
	BillingInfo.iSellType = SELL_TYPE_CASH;
	BillingInfo.iTrophyChange = 0;
	BillingInfo.iSerialNum = pUser->GetUserIDIndex();
	BillingInfo.iAvatarLv = pUser->GetCurUsedAvatar()->iLv;
	std::string	strItemName;
	memcpy(BillingInfo.szItemName, strItemName.c_str(), MAX_ITEMNAME_LENGTH + 1);	
	BillingInfo.iItemCode = sReward.iItemCode;
	BillingInfo.iCurrentCash = pUser->GetCoin();
	BillingInfo.iCashChange = iDiscountPrice;
	BillingInfo.iItemPrice = iPrice;
	BillingInfo.iPointChange = 0;
	BillingInfo.iParam5 = rq.iStep;
	BillingInfo.iParam6 = sReward.iRewardIndex;

	if(FALSE == BILLING_GAME.RequestPurchaseItem(BillingInfo))
	{
		pUser->Send(S2C_EVENT_DISCOUNT_ITEM_BUY_RES, &rs, sizeof(SS2C_EVENT_DISCOUNT_ITEM_BUY_RES));
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_OPEN_BOX_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserOpenBoxEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_OPEN_BOX_UPDATE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_OPEN_BOX_UPDATE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_OPEN_BOX_UPDATE_REQ)); 

	SS2C_EVENT_OPEN_BOX_UPDATE_RES rs;
	rs.btType = rq.btType;
	rs.btResult = pUser->GetUserOpenBoxEvent()->UpdateOpenBoxReq(rq.btType, rs);
	pUser->Send(S2C_EVENT_OPEN_BOX_UPDATE_RES, &rs, sizeof(SS2C_EVENT_OPEN_BOX_UPDATE_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_NEWBIE_BENEFIT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserNewbieBenefit()->SendNewbieBenefitList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_NEWBIE_BENEFIT_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_NEWBIE_BENEFIT_REWARD_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq,sizeof(SC2S_NEWBIE_BENEFIT_REWARD_INFO_REQ));

	pUser->GetUserNewbieBenefit()->SendNewbieBenefitRewardList(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_NEWBIE_BENEFIT_RECEIVING_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_NEWBIE_BENEFIT_RECEIVING_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_NEWBIE_BENEFIT_RECEIVING_REQ));

	pUser->GetUserNewbieBenefit()->GiveRewardNewbieBenefit(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSION_MAKEITEM_PRODUCT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMissionMakeItemEvent()->SendEventProductInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSION_MAKEITEM_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMissionMakeItemEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSION_MAKEITEM_MAKE_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MISSION_MAKEITEM_MAKE_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq,sizeof(SC2S_MISSION_MAKEITEM_MAKE_ITEM_REQ));

	pUser->GetUserMissionMakeItemEvent()->MakeItem(rq.btProductIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSIONCASHLIMIT_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMissionCashLimitEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MISSIONCASHLIMIT_GIVE_CASH_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_MISSIONCASHLIMIT_GIVE_CASH_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq,sizeof(SC2S_MISSIONCASHLIMIT_GIVE_CASH_REQ));

	BYTE btResult = pUser->GetUserMissionCashLimitEvent()->GiveCashReq(rq.btMissionIndex);
	if(MissionCashLimitEvent::RESULT_GIVE_CASH_SUCCESS == btResult)
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);	
		if(pCenter)
		{
			SS2G_MISSIONCASHLIMIT_DECREASE_CASH_NOT	req;
			req.iGameIDIndex = pUser->GetGameIDIndex();
			req.iUserIDIndex = pUser->GetUserIDIndex();
			req.btMissionIndex = rq.btMissionIndex;

			CPacketComposer Packet(G2S_MISSIONCASHLIMIT_GIVECASH_REQ);
			Packet.Add((PBYTE)&req, sizeof(SG2S_MISSIONCASHLIMIT_GIVECASH_REQ));
			pCenter->Send(&Packet);
		}
		else
		{
			btResult = MissionCashLimitEvent::RESULT_GIVE_CASH_FAIL;
		}
	}

	CHECK_CONDITION_RETURN_VOID(MissionCashLimitEvent::RESULT_GIVE_CASH_SUCCESS == btResult);

	SS2C_MISSIONCASHLIMIT_GIVE_CASH_RES	ss;
	ss.btResult = btResult;
	pUser->Send(S2C_MISSIONCASHLIMIT_GIVE_CASH_RES, &ss, sizeof(SS2C_MISSIONCASHLIMIT_GIVE_CASH_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DARKMARKET_PRODUCT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_DARKMARKET_PRODUCT_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_DARKMARKET_PRODUCT_INFO_REQ));

	pUser->GetUserDarkMarketEvent()->SendEventInfo(rq.btMarketType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DARKMARKET_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserDarkMarketEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_DARKMARKET_BUY_PRODUCT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_DARKMARKET_BUY_PRODUCT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_DARKMARKET_BUY_PRODUCT_REQ));

	pUser->GetUserDarkMarketEvent()->BuyDiamondProduct(rq.btProductIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CLOSE_BUTTON_CLICK_NOTICE)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendSecretStoreOpen();
	pUser->GetUserCongratulationEvent()->CheckAndSendBalloonMissionNoClearNot();
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_MY_AVATAR_LIST_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendMyAvatarList();
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MAIN_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCongratulationEvent()->SendEventInfo(CongratulationEvent::MAX_CNT_CONGRATULATION_SUB_EVENT_KIND);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCongratulationEvent()->SendEventInfo(CongratulationEvent::BALLOON_MISSION_EVENT);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCongratulationEvent()->SendUserInfo(CongratulationEvent::BALLOON_MISSION_EVENT);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_CONGRATULATION_BALLOON_MISSION_GET_BALLOON_REQ));

	pUser->GetUserCongratulationEvent()->GiveBalloon(rs.btProperty);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_CONGRATULATION_BALLOON_MISSION_OPEN_BOX_REQ));

	pUser->GetUserCongratulationEvent()->OpenBox(rs.btMissionIndex);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCongratulationEvent()->SendEventInfo(CongratulationEvent::MULTIPLE_GIFTSHOP_EVENT);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCongratulationEvent()->SendUserInfo(CongratulationEvent::MULTIPLE_GIFTSHOP_EVENT);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_CONGRATULATION_MULTIPLE_GIFTSHOP_BUY_GIFT_REQ));

	pUser->GetUserCongratulationEvent()->BuyMultipleGift(rs.btGiftIndex);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCongratulationEvent()->SendUserInfo(CongratulationEvent::SPECIAL_PEAKTIME_EVENT);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_CONGRATULATION_SPECIAL_PEAKTIME_GET_EXP_REQ));

	pUser->GetUserCongratulationEvent()->UseTotalExp(rs.szGameID);
};

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HELLONEWYEAR_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHelloNewYearEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_HELLONEWYEAR_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHelloNewYearEvent()->GetRewardReq();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COLORINGPLAY_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserColoringPlayEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COLORINGPLAY_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserColoringPlayEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COLORPLAY_USE_PENCIL_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_COLORPLAY_USE_PENCIL_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_COLORPLAY_USE_PENCIL_REQ));

	pUser->GetUserColoringPlayEvent()->UsePencil(rq.btPictureIndex, rq.btColorIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COLORPLAY_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_COLORPLAY_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_COLORPLAY_GET_REWARD_REQ));

	pUser->GetUserColoringPlayEvent()->GetReward(rq.btPictureIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RECOMMEND_ABILITY_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SAvatarInfo* pAvatar = pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSItemShop *pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CShopItemList* pShopItemList = pItemShop->GetShopItemList();
	CHECK_NULL_POINTER_VOID(pShopItemList);

	vector<int>	vPosition;
	switch(pAvatar->Status.iGamePosition)
	{
	case POSITION_CODE_FOWARD:
		{
			vPosition.push_back(POSITION_CODE_POWER_FOWARD);
			vPosition.push_back(POSITION_CODE_SMALL_FOWARD);
		}
		break;

	case POSITION_CODE_GUARD:
		{
			vPosition.push_back(POSITION_CODE_POINT_GUARD);
			vPosition.push_back(POSITION_CODE_SHOOT_GUARD);
		}
		break;

	default:
		{
			vPosition.push_back(pAvatar->Status.iGamePosition);
		}
		break;
	}

	CPacketComposer Packet(S2C_RECOMMEND_ABILITY_INFO_RES);

	SS2C_RECOMMEND_ABILITY_INFO_RES	ss;
	ss.iCnt = vPosition.size();
	Packet.Add((PBYTE)&ss, sizeof(SS2C_RECOMMEND_ABILITY_INFO_RES));

	SRECOMMEND_ABILITY_INFO	sAbility;

	for(int i = 0; i < ss.iCnt; ++i)
	{
		pShopItemList->GetRecommendProperty(vPosition[i], sAbility);
		Packet.Add((PBYTE)&sAbility, sizeof(SRECOMMEND_ABILITY_INFO));
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserGamePlayLoginEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_REQ));

	SS2C_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_RES rs;
	rs.btRewardType = rq.btRewardType;
	rs.btResult = pUser->GetUserGamePlayLoginEvent()->SelectRewardReq(rq.btRewardType, rs.btCurrentStatus);
	pUser->Send(S2C_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_RES, &rs, sizeof(SS2C_EVENT_GAMEPLAY_LOGIN_SELECT_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_UPDATE_REWARD_STATUS_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_GAMEPLAY_LOGIN_UPDATE_REWARD_STATUS_RES rs;
	rs.btResult = pUser->GetUserGamePlayLoginEvent()->UpdateRewardStatusReq(rs);
	pUser->Send(S2C_EVENT_GAMEPLAY_LOGIN_UPDATE_REWARD_STATUS_RES, &rs, sizeof(SS2C_EVENT_GAMEPLAY_LOGIN_UPDATE_REWARD_STATUS_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GAMEPLAY_LOGIN_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_GAMEPLAY_LOGIN_GET_REWARD_RES rs;
	rs.btResult = pUser->GetUserGamePlayLoginEvent()->GetRewardReq(rs);
	pUser->Send(S2C_EVENT_GAMEPLAY_LOGIN_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_GAMEPLAY_LOGIN_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POTION_MAKING_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPotionMakingEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POTION_MAKING_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_POTION_MAKING_REWARD_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_POTION_MAKING_REWARD_INFO_REQ));

	CPacketComposer Packet(S2C_EVENT_POTION_MAKING_REWARD_INFO_RES);
	POTIONMAKEEVENT.MakePotionRewardList(rq.btPotionType, Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POTION_MAKING_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_POTION_MAKING_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_POTION_MAKING_REQ));

	SS2C_EVENT_POTION_MAKING_RES rs;
	rs.btPotionType = rq.btPotionType;
	rs.btResult = pUser->GetUserPotionMakingEvent()->PotionMakingReq(rq.btPotionType, rs.sReward);
	pUser->Send(S2C_EVENT_POTION_MAKING_RES, &rs, sizeof(SS2C_EVENT_POTION_MAKING_RES));

	if(RESULT_EVENT_POTION_MAKING_SUCCESS == rs.btResult ||
		RESULT_EVENT_POTION_MAKING_SUCCESS_POINT_REWARD == rs.btResult)
		pUser->GetUserPotionMakingEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPrimonGoEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_OPEN_PRIMONBALL_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_PRIMONGO_OPEN_PRIMONBALL_RES rs;
	rs.btResult = pUser->GetUserPrimonGoEvent()->OpenPrimonBallReq(rs.btRewardType, rs.sReward);
	pUser->Send(S2C_EVENT_PRIMONGO_OPEN_PRIMONBALL_RES, &rs, sizeof(SS2C_EVENT_PRIMONGO_OPEN_PRIMONBALL_RES));

	if(RESULT_EVENT_PRIMONGO_OPEN_PRIMONBAL_SUCCESS == rs.btResult)
		pUser->GetUserPrimonGoEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_GET_FREE_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_PRIMONGO_GET_FREE_REWARD_RES rs;
	rs.btResult = pUser->GetUserPrimonGoEvent()->GetFreeRewardReq();
	pUser->Send(S2C_EVENT_PRIMONGO_GET_FREE_REWARD_RES, &rs, sizeof(SS2C_EVENT_PRIMONGO_GET_FREE_REWARD_RES));

	if(RESULT_EVENT_PRIMONGO_GET_FREE_REWARD_SUCCESS == rs.btResult)
		pUser->GetUserPrimonGoEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE_RES rs;
	rs.btResult = pUser->GetUserPrimonGoEvent()->InitializeTryTypeReq();
	pUser->Send(S2C_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE_RES, &rs, sizeof(SS2C_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE_RES));

	if(RESULT_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE_SUCCESS == rs.btResult)
		pUser->GetUserPrimonGoEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPrivateRoomEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_EXCHANGEITEM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPrivateRoomEvent()->SendExchangeItemInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_GET_PRODUCT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_PRIVATEROOM_GET_PRODUCT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_PRIVATEROOM_GET_PRODUCT_REQ));

	SS2C_EVENT_PRIVATEROOM_GET_PRODUCT_RES rs;
	rs.iProductIndex = rq.iProductIndex;
	rs.iPropertyValue = rq.iPropertyValue;
	rs.btResult = pUser->GetUserPrivateRoomEvent()->GetProductReq(rq.iProductIndex, rq.iPropertyValue);
	pUser->Send(S2C_EVENT_PRIVATEROOM_GET_PRODUCT_RES, &rs, sizeof(SS2C_EVENT_PRIVATEROOM_GET_PRODUCT_RES));

	if(RESULT_EVENT_PRIVATEROOM_GET_PRODUCT_SUCCESS == rs.btResult)
		pUser->GetUserPrivateRoomEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_REQ));

	SS2C_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_RES rs;
	rs.iRewardIndex = rq.iRewardIndex;
	rs.btResult = pUser->GetUserPrivateRoomEvent()->GetExchangeItemReq(rq.iRewardIndex, rs.iUpdatedTicketCount);
	pUser->Send(S2C_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_RES, &rs, sizeof(SS2C_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_RES));

	if(RESULT_EVENT_PRIVATEROOM_GET_EXCHANGEITEM_SUCCESS == rs.btResult)
		pUser->GetUserPrivateRoomEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHippocampusEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHippocampusEvent()->SendRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_EAT_FOOD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HIPPOCAMPUS_EAT_FOOD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HIPPOCAMPUS_EAT_FOOD_REQ));

	SS2C_EVENT_HIPPOCAMPUS_EAT_FOOD_RES rs;
	rs.btResult = pUser->GetUserHippocampusEvent()->EatFoodReq(rq.btFoodType, rs);
	pUser->Send(S2C_EVENT_HIPPOCAMPUS_EAT_FOOD_RES, &rs, sizeof(SS2C_EVENT_HIPPOCAMPUS_EAT_FOOD_RES));

	if(RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_GRADE_UP_FAIL == rs.btResult ||
		RESULT_EVENT_PHIPPOCAMPUS_EAT_FOOD_FULL_MAIL_BOX_GRADE_UP_FAIL == rs.btResult)
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT not;
			not.btServerIndex = _GetServerIndex;
			pUser->GetUserHippocampusEvent()->MakeIntegrateDB_UpdateUserLog(not);

			CPacketComposer	Packet(S2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT);
			Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT));

			pODBCSvr->Send(&Packet);
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_REQ));

	SS2C_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_RES rs;
	rs.btResult = pUser->GetUserHippocampusEvent()->GetBonusExpReq(rq.btBonusExpType, rs);
	pUser->Send(S2C_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_RES, &rs, sizeof(SS2C_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_RES));

	if(RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_GRADE_UP_FAIL == rs.btResult ||
		RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_EXP_UP_FAIL == rs.btResult ||
		RESULT_EVENT_HIPPOCAMPUS_GET_BONUS_EXP_FULL_MAIL_BOX_GRADE_UP_FAIL == rs.btResult)
	{
		CODBCSvrProxy* pODBCSvr = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
		if(NULL != pODBCSvr)
		{
			SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT not;
			not.btServerIndex = _GetServerIndex;
			pUser->GetUserHippocampusEvent()->MakeIntegrateDB_UpdateUserLog(not);

			CPacketComposer	Packet(S2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT);
			Packet.Add((PBYTE)&not,sizeof(SS2O_EVENT_HIPPOCAMPUS_USER_LOG_UPDATE_NOT));

			pODBCSvr->Send(&Packet);
		}
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_RANK_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HIPPOCAMPUS_RANK_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HIPPOCAMPUS_RANK_INFO_REQ));

	pUser->GetUserHippocampusEvent()->SendRankInfo(rq.btRankType, _GetServerIndex);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_RANK_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_HIPPOCAMPUS_RANK_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_HIPPOCAMPUS_RANK_LIST_REQ));

	HIPPOCAMPUS.SendRankList(rq.btRankType, pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_HIPPOCAMPUS_TEST_ADD_FOOD0_REQ)
{
#ifdef _DEBUG
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserHippocampusEvent()->TestAddFood();
#endif
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_STEELBAG_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSteelBagMissionEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_STEELBAG_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_STEELBAG_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_STEELBAG_GET_REWARD_REQ));

	CPacketComposer Packet(S2C_EVENT_STEELBAG_GET_REWARD_RES);
	SS2C_EVENT_STEELBAG_GET_REWARD_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.iRewardCnt = 0;

	vector<SRewardInfo> vReward;
	rs.btResult = pUser->GetUserSteelBagMissionEvent()->GetRewardReq(rq.iMissionIndex, vReward);
	rs.iRewardCnt = vReward.size();
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_STEELBAG_GET_REWARD_RES));
	if(RESULT_EVENT_STEELBAG_GET_REWARD_SUCCESS == rs.btResult)
	{
		SREWARD_INFO info;
		for(int i = 0; i < vReward.size(); ++i)
		{
			info.btRewardType = vReward[i].btRewardType;
			info.iItemCode = vReward[i].iItemCode;
			info.iPropertyType = vReward[i].iPropertyType;
			info.iPropertyValue = vReward[i].iPropertyValue;
			Packet.Add((PBYTE)&info, sizeof(SREWARD_INFO));
		}
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_STEELBAG_STAMP_COUPON_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_STEELBAG_STAMP_COUPON_RES rs;
	rs.btResult = pUser->GetUserSteelBagMissionEvent()->StampCouponReq(rs);
	pUser->Send(S2C_EVENT_STEELBAG_STAMP_COUPON_RES, &rs, sizeof(SS2C_EVENT_STEELBAG_STAMP_COUPON_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COMEBACK_BENEFIT_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserComebackBenefit()->SendComebackBenefitList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COMEBACK_BENEFIT_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_COMEBACK_BENEFIT_REWARD_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_COMEBACK_BENEFIT_REWARD_INFO_REQ));

	pUser->GetUserComebackBenefit()->SendComebackBenefitRewardList(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_COMEBACK_BENEFIT_RECEIVING_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_COMEBACK_BENEFIT_RECEIVING_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_COMEBACK_BENEFIT_RECEIVING_REQ));

	pUser->GetUserComebackBenefit()->GiveRewardComebackBenefit(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPowerupCapsule()->SendPowerupCapsuleInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_USE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_POWERUP_CAPSULE_USE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_POWERUP_CAPSULE_USE_REQ));

	pUser->GetUserPowerupCapsule()->UsePowerupCapsule(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_POWERUP_CAPSULE_APPLY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_POWERUP_CAPSULE_APPLY_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_POWERUP_CAPSULE_APPLY_REQ));

	pUser->GetUserPowerupCapsule()->ApplyPowerupCapsule(rq.btApply);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GIVE_MAGICBALL_STATUS_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendEventGiveMagicBallStatus();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_GIVE_MAGICBALL_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_GIVE_MAGICBALL_GET_REWARD_RES rs;
	rs.btResult = pUser->GetEventGiveMagicBallReward();
	pUser->Send(S2C_EVENT_GIVE_MAGICBALL_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_GIVE_MAGICBALL_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_UNLIMITED_TATOO_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserUnlimitedTatooEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_UNLIMITED_TATOO_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_UNLIMITED_TATOO_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_UNLIMITED_TATOO_GET_REWARD_REQ));

	SS2C_EVENT_UNLIMITED_TATOO_GET_REWARD_RES rs;
	rs.iRewardIndex = rq.iRewardIndex;
	rs.btResult = pUser->GetUserUnlimitedTatooEvent()->GetRewardReq(rq.iRewardIndex);
	pUser->Send(S2C_EVENT_UNLIMITED_TATOO_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_UNLIMITED_TATOO_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPVEMission()->SendMissionInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_MISSION_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_MISSION_LIST_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_MISSION_LIST_REQ));

	pUser->GetUserPVEMission()->SendMissionList(rq.btPVEType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_MISSION_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_MISSION_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_MISSION_GET_REWARD_REQ));

	SS2C_PVE_MISSION_GET_REWARD_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.btResult = pUser->GetUserPVEMission()->GetRewardReq(rq.iMissionIndex, rs.sReward);
	pUser->Send(S2C_PVE_MISSION_GET_REWARD_RES, &rs, sizeof(SS2C_PVE_MISSION_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_DAILY_MISSION_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_PVE_DAILY_MISSION_GET_REWARD_RES rs;
	rs.btResult = pUser->GetUserPVEMission()->GetDailyRewardReq(rs.sReward);
	pUser->Send(S2C_PVE_DAILY_MISSION_GET_REWARD_RES, &rs, sizeof(SS2C_PVE_DAILY_MISSION_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPVEMission()->SendAvatarPieceInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_REQ));

	SS2C_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_RES rs;
	rs.btUsePieceType = rq.btUsePieceType;
	rs.iUsePieceCount = rq.iUsePieceCount;
	rs.btGetPieceType = rq.btGetPieceType;
	rs.iGetPieceCount = rq.iGetPieceCount;
	rs.btResult = pUser->GetUserPVEMission()->ExchangePieceReq(rq);
	pUser->Send(S2C_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_RES, &rs, sizeof(SS2C_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_REQ));

	SS2C_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_RES rs;
	rs.btPieceType = rq.btPieceType;
	rs.btResult = pUser->GetUserPVEMission()->GetRewardPiece(rq.btPieceType, rs.sReward);
	pUser->Send(S2C_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_RES, &rs, sizeof(SS2C_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCovet()->SendCovetUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_ITEM_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCovet()->SendCovetItemList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_ITEM_SELECT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_COVET_ITEM_SELECT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_COVET_ITEM_SELECT_REQ));

	pUser->GetUserCovet()->SendCovetItemSelect(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_SHOP_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserCovet()->SendCovetShop();	
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_SHOP_BUY_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_COVET_SHOP_BUY_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_COVET_SHOP_BUY_REQ));

	pUser->GetUserCovet()->BuyCovetShopItem(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_COVET_USE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_COVET_USE_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_COVET_USE_REQ));

	pUser->GetUserCovet()->UseCovet(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PROMISE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendPromiseUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PROMISE_RECEIVING_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_PROMISE_RECEIVING_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_PROMISE_RECEIVING_REQ));

	pUser->GivePromiseReward(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PACKAGEITEM_PACK_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CFSGameServer* pServer = CFSGameServer::GetInstance();
	CHECK_NULL_POINTER_VOID(pServer);

	CFSItemShop* pItemShop = pServer->GetItemShop();
	CHECK_NULL_POINTER_VOID(pItemShop);

	CShopItemList* pShopItemList = pItemShop->GetShopItemList();
	CHECK_NULL_POINTER_VOID(pShopItemList);

	int iItemCode = -1;
	m_ReceivePacketBuffer.Read(&iItemCode);

	CPacketComposer PacketComposer(S2C_EVENT_PACKAGEITEM_PACK_INFO_RES);
	pShopItemList->MakePackageItemComponentList(pUser->GetCurUsedAvatarSex(), pUser->GetCurUsedAvatarPosition(), PacketComposer, iItemCode);
	pUser->Send(&PacketComposer);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserMagicMissionEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_USE_MAGIC_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_MAGIC_MISSION_USE_MAGIC_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_MAGIC_MISSION_USE_MAGIC_REQ));

	SS2C_EVENT_MAGIC_MISSION_USE_MAGIC_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.btResult = pUser->GetUserMagicMissionEvent()->UseMagic(rq.iMissionIndex, rs.btMagicType);
	pUser->Send(S2C_EVENT_MAGIC_MISSION_USE_MAGIC_RES, &rs, sizeof(SS2C_EVENT_MAGIC_MISSION_USE_MAGIC_RES));

	if(RESULT_EVENT_MAGIC_MISSION_USE_MAGIC_SUCCESS == rs.btResult ||
		RESULT_EVENT_MAGIC_MISSION_USE_MAGIC_FAIL_USE_MAGIC == rs.btResult)
	{
		pUser->SendUserGold();
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_MAGIC_MISSION_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_MAGIC_MISSION_GET_REWARD_REQ));

	SS2C_EVENT_MAGIC_MISSION_GET_REWARD_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.btResult = pUser->GetUserMagicMissionEvent()->GetReward(rq.iMissionIndex);
	pUser->Send(S2C_EVENT_MAGIC_MISSION_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_MAGIC_MISSION_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*)pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_REQ));

	SS2C_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_RES rs;
	rs.iRewardIndex = rq.iRewardIndex;
	rs.btResult = pUser->GetUserMagicMissionEvent()->ExchangeReward(rq.iRewardIndex);
	pUser->Send(S2C_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_RES, &rs, sizeof(SS2C_EVENT_MAGIC_MISSION_EXCHANGE_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_EVENT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserShoppingGodEvent()->SendShoppingGodEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserShoppingGodEvent()->SendShoppingGodUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_CHECK_LOGIN_TIME_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserShoppingGodEvent()->SendLoginTimeInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SHOPPING_GOD_USE_CHIP_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SHOPPING_GOD_USE_CHIP_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_SHOPPING_GOD_USE_CHIP_REQ));

	pUser->GetUserShoppingGodEvent()->UseChip(rs);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PACKAGEITEM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_EVENT_PACKAGEITEM_INFO_RES);
	PACKAGEITEMEVENT.MakeEventInfo(Packet);
	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_NEW_CLUB_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendEventNewClubInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_NEW_CLUB_INFO_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetEventNewClubReward();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_WHITEDAY_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	WHITEDAY.SendWhiteDayInfo(pUser);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_WHITEDAY_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_WHITEDAY_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_WHITEDAY_GET_REWARD_REQ));

	WHITEDAY.GiveWhiteDayReward(pUser,rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSubCharDevelop()->SendExpUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_EXP_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SUB_CHAR_DEVELOP_EXP_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SUB_CHAR_DEVELOP_EXP_INFO_REQ));

	pUser->GetUserSubCharDevelop()->SendAvatarInfo(rq.btPositionType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_SUB_CHAR_DEVELOP_EXP_ACCEPT_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_SUB_CHAR_DEVELOP_EXP_ACCEPT_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_SUB_CHAR_DEVELOP_EXP_ACCEPT_REQ));

	CPacketComposer Packet(S2C_SUB_CHAR_DEVELOP_EXP_ACCEPT_RES);

	SS2C_SUB_CHAR_DEVELOP_EXP_ACCEPT_RES rs;
	rs.btResult = pUser->GetUserSubCharDevelop()->AcceptExpReq(rq.iGameIDIndex, rq.iAcceptExp, rs);

	Packet.Add((PBYTE)&rs, sizeof(SS2C_SUB_CHAR_DEVELOP_EXP_ACCEPT_RES));

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_GACHA_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSelectClothesEvent()->SendGachaRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_SELECT_CLOTHES_GACHA_GET_REWARD_REQ));

	pUser->GetUserSelectClothesEvent()->SendOpenBoxReq(rq.btOpenType, rq.btSelectIndex[EVENT_SELECT_CLOTHES_GACHA_REWARD_INDEX_0], rq.btSelectIndex[EVENT_SELECT_CLOTHES_GACHA_REWARD_INDEX_1]);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_RANK_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSelectClothesEvent()->SendRankRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_RANK_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSelectClothesEvent()->SendRankList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSelectClothesEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SELECT_CLOTHES_AVATAR_FEATURE_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSelectClothesEvent()->SendAvatarFeatureInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_USER_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSummerCandyEvent()->SendUserInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CPacketComposer Packet(S2C_EVENT_SUMMER_CANDY_REWARD_INFO_RES);
	SUMMERCANDYEVENT.MakeRewardList(Packet);

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SUMMER_CANDY_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_SUMMER_CANDY_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_SUMMER_CANDY_GET_REWARD_REQ));

	SS2C_EVENT_SUMMER_CANDY_GET_REWARD_RES rs;
	rs.iRewardIndex = rq.iRewardIndex;
	rs.btResult = pUser->GetUserSummerCandyEvent()->GetRewardReq(rq.iRewardIndex);
	pUser->Send(S2C_EVENT_SUMMER_CANDY_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_SUMMER_CANDY_GET_REWARD_RES));
	pUser->GetUserSummerCandyEvent()->SendUserCandyInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DAILY_LOGIN_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	vector<SRewardInfo> vReward;
	CHECK_CONDITION_RETURN_VOID(FALSE == DAILYLOGINREWARDEVENT.GetRewardInfo(vReward));

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	CHECK_NULL_POINTER_VOID(pEventODBC);

	SS2C_EVENT_DAILY_LOGIN_REWARD_INFO_RES rs;
	rs.btRewardStatus = EVENT_DAILY_LOGIN_REWARD_STATUS_DISABLE;
	if(ODBC_RETURN_SUCCESS == pEventODBC->EVENT_DAILY_LOGIN_REWARD_USER_REWARD_CheckLog(pUser->GetUserIDIndex()))
		rs.btRewardStatus = EVENT_DAILY_LOGIN_REWARD_STATUS_ENABLE;

	rs.iRewardCount = vReward.size();
	
	CPacketComposer Packet(S2C_EVENT_DAILY_LOGIN_REWARD_INFO_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_EVENT_DAILY_LOGIN_REWARD_INFO_RES));

	for(int i = 0; i < vReward.size(); ++i)
	{
		SREWARD_INFO info;
		info.btRewardType = vReward[i].btRewardType;
		info.iItemCode = vReward[i].iItemCode;
		info.iPropertyType = vReward[i].iPropertyType;
		info.iPropertyValue = vReward[i].iPropertyValue;
		Packet.Add((PBYTE)&info, sizeof(SREWARD_INFO));
	}

	pUser->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_DAILY_LOGIN_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_DAILY_LOGIN_GET_REWARD_RES rs;
	rs.btResult = RESULT_EVENT_DAILY_LOGIN_GET_REWARD_SUCCESS;

	vector<SRewardInfo> vReward;

	CFSEventODBC* pEventODBC = dynamic_cast<CFSEventODBC*>(ODBCManager.GetODBC(ODBC_EVENT));
	if(NULL == pEventODBC)
		rs.btResult = RESULT_EVENT_DAILY_LOGIN_GET_REWARD_FAIL;
	else if(FALSE == DAILYLOGINREWARDEVENT.GetRewardInfo(vReward))
		rs.btResult = RESULT_EVENT_DAILY_LOGIN_GET_REWARD_CLOSED_EVENT;
	else if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_DAILY_LOGIN_REWARD_USER_REWARD_CheckLog(pUser->GetUserIDIndex()))
		rs.btResult = RESULT_EVENT_DAILY_LOGIN_GET_REWARD_ALREADY_GET_REWARD;
	else if((pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+vReward.size()) > MAX_PRESENT_LIST)
		rs.btResult = RESULT_EVENT_DAILY_LOGIN_GET_REWARD_FULL_MAILBOX;

	if(RESULT_EVENT_DAILY_LOGIN_GET_REWARD_SUCCESS == rs.btResult)
	{
		for(int i = 0; i < vReward.size(); ++i)
		{
			SRewardConfig* pReward = REWARDMANAGER.GiveReward(pUser, vReward[i].iRewardIndex);
			if(NULL == pReward)
			{
				WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "REWARDMANAGER.GiveReward failed - UserIDIndex:11866902, RewardIndex:0", pUser->GetUserIDIndex(), vReward[i].iRewardIndex);
		
		}

		if(ODBC_RETURN_SUCCESS != pEventODBC->EVENT_DAILY_LOGIN_REWARD_USER_REWARD_InsertLog(pUser->GetUserIDIndex()))
		{
			WRITE_LOG_NEW(LOG_TYPE_EVENT, DB_DATA_LOAD, FAIL, "EVENT_DAILY_LOGIN_REWARD_USER_REWARD_InsertLog failed - UserIDIndex:11866902", pUser->GetUserIDIndex());
}	

	pUser->Send(S2C_EVENT_DAILY_LOGIN_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_DAILY_LOGIN_GET_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPremiumPassEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GRADE_REWARD_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserPremiumPassEvent()->SendGradeRewardInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_MISSION_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_PREMIUM_PASS_MISSION_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_PREMIUM_PASS_MISSION_INFO_REQ));

	pUser->GetUserPremiumPassEvent()->SendMissionInfo(rq);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_REQ));

	SS2C_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_RES rs;
	rs.btReqType = rq.btReqType;
	rs.btGrade = rq.btGrade;
	rs.btPassType = rq.btPassType;
	rs.btResult = pUser->GetUserPremiumPassEvent()->GetGradeRewardReq(rq);
	pUser->Send(S2C_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_RES, &rs, sizeof(SS2C_EVENT_PREMIUM_PASS_GET_GRADE_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_REQ));

	SS2C_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_RES rs;
	rs.iMissionIndex = rq.iMissionIndex;
	rs.btResult = pUser->GetUserPremiumPassEvent()->GetMissionRewardReq(rq.iMissionIndex);
	pUser->Send(S2C_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_RES, &rs, sizeof(SS2C_EVENT_PREMIUM_PASS_GET_MISSION_REWARD_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SECRET_SHOP_LIST_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserSecretShopEvent()->SendShopList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SECRET_SHOP_BUY_ITEM_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_SECRET_SHOP_BUY_ITEM_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_SECRET_SHOP_BUY_ITEM_REQ));

	SS2C_EVENT_SECRET_SHOP_BUY_ITEM_RES rs;
	rs.iSlotIndex = rq.iSlotIndex;
	rs.btResult = pUser->GetUserSecretShopEvent()->BuyItemReq(rq.iSlotIndex);
	pUser->Send(S2C_EVENT_SECRET_SHOP_BUY_ITEM_RES, &rs, sizeof(SS2C_EVENT_SECRET_SHOP_BUY_ITEM_RES));
	pUser->GetUserSecretShopEvent()->SendShopList();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_SPEECH_BUBBLE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->SendSpeechBubbleInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_PACKAGE_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserTransferJoycityPackageEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_REQ));

	SS2C_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_RES rs;
	rs.btResult = pUser->GetUserTransferJoycityPackageEvent()->GetRewardReq(rq.btRewardType, rs);
	pUser->Send(S2C_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_RES));
	pUser->GetUserTransferJoycityPackageEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserTransferJoycityShopEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_SLOT_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	pUser->GetUserTransferJoycityShopEvent()->SendEventShopItemInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_REQ));

	SS2C_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_RES rs;
	rs.iSlotIndex = rq.iSlotIndex;
	rs.btResult = pUser->GetUserTransferJoycityShopEvent()->GetRewardReq(rq.iSlotIndex);
	pUser->Send(S2C_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_RES, &rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_SHOP_GET_REWARD_RES));
	pUser->GetUserTransferJoycityShopEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_RES rs;
	rs.btResult = pUser->GetUserTransferJoycityShopEvent()->GetDailyCoinReq();
	pUser->Send(S2C_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_RES, &rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_SHOP_GET_DAILYCOIN_RES));
	pUser->GetUserTransferJoycityShopEvent()->SendEventInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_TRANSFERJOYCITY_100DREAM_INFO_REQ));

	pUser->GetUserTransferJoycity100DreamEvent()->SendEventInfo(rq.btType);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES rs;
	rs.btResult = pUser->GetUserTransferJoycity100DreamEvent()->TryWinReq(rs.iGiveCash, rs.iRankNumber);
	if(RESULT_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_SUCCESS != rs.btResult)
		pUser->Send(S2C_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES, &rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ)
{
	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	SC2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ rq;
	m_ReceivePacketBuffer.Read((PBYTE)&rq, sizeof(SC2S_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_REQ));

	SS2C_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES rs;
	rs.btResult = pUser->GetUserTransferJoycity100DreamEvent()->WriteWordsReq(rq);
	if(RESULT_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_SUCCESS != rs.btResult)
		pUser->Send(S2C_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES, &rs, sizeof(SS2C_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES));
}
