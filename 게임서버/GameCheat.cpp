qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "GameCheat.h"
qinclude "CFSGameClient.h"
qinclude "ClubSvrProxy.h"
qinclude "CenterSvrProxy.h"
qinclude "MatchSvrProxy.h"
qinclude "CFSGameUser.h"

CGameCheat* CGameCheat::m_pInstance = NULL;

CGameCheat::CGameCheat(void)
{
	m_pInstance = this;
	m_bOneManTeam = FALSE;
	Initialize();
}

CGameCheat::~CGameCheat(void)
{
}

bool CGameCheat::Initialize()
{
	FILE *fp=NULL;
	fopen_s(&fp, "CheatSetting.txt", "r");

	if(fp == NULL)
	{
		m_IsCheatInit = FALSE;
		return FALSE;
	}
	else
	{
		m_IsCheatInit = TRUE;
		return TRUE;
	}

}

void CGameCheat::CheatDivide(CFSGameClient * pClient)
{
	CFSGameODBC* pGameODBC = (CFSGameODBC*)ODBCManager.GetODBC( ODBC_GAME );
	CHECK_NULL_POINTER_VOID( pGameODBC );

	if(m_IsCheatInit == FALSE) return;
	if(pClient == NULL) return;
	if( !CheatLogin(pClient,pGameODBC) ) return;
	
	int iCode,iRequest1,iRequest2;
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);
	switch(iCode)
	{
		case 1:
			CheatLevel(pClient,pGameODBC);
			break;
		case 2:
			CheatTrophy(pClient,pGameODBC);
			break;
		case 3:
			CheatChampCount(pClient,pGameODBC);
			break;
		case 4:
			CheatPoint(pClient,pGameODBC);
			break;
		case 5:
			CheatCash(pClient,pGameODBC);
			break;
		case 6:
			CheatTraining(pClient,pGameODBC);
			break;
		case 7:
			CheatAllSkill(pClient,pGameODBC);
			break;
		case 8:
			CheatAllFreeStyle(pClient,pGameODBC);
			break;
		case 9:
			CheatSkillSlot(pClient,pGameODBC);
			break;
		case 10:
			break;
		case 11:
			CheatOneManTeam(pClient,pGameODBC);
			break;
	}
}

bool CGameCheat::CheatLogin(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return false;

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return false;
	
	
	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatLogin(pUser->GetUserIDIndex()) )
		return true;
	else
		return false;

}

void CGameCheat::CheatLevel(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;

	pClient->GetCheatCode(iCode, iRequest1, iRequest2);
	
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;
	
	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	
	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	if(iRequest1 > GAME_LVUP_MAX || iRequest1 < 0)
	{
		iSuccess = -1;
		PacketComposer.Add(&iCode);
		PacketComposer.Add(&iSuccess);
		PacketComposer.Add(&iRequest1);
		pClient->Send(&PacketComposer);
		return;
	}
	
	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatLevel(pAvatarInfo->iGameIDIndex, iRequest1) )
		iSuccess = 1;
	else
		iSuccess = 0;

	PacketComposer.Add(&iCode);
	PacketComposer.Add(&iSuccess);
	PacketComposer.Add(&iRequest1);
	
	pClient->Send(&PacketComposer);
	
	if(iSuccess == 1)
	{
		pAvatarInfo->iLv = iRequest1;
		
		if(iRequest1 < 16)
			pAvatarInfo->iLeague = 0;
		else
			pAvatarInfo->iLeague = 1;
		vector <int> vecLevelingCurve;	
		
		if( ODBC_RETURN_FAIL == pGameODBC->CONFIG_GetConfigLv(vecLevelingCurve))
		{
			WRITE_LOG_NEW(LOG_TYPE_SYSTEM, DB_DATA_LOAD, FAIL, "CONFIG_GetConfigLv");
		}
		
		pAvatarInfo->iExp = vecLevelingCurve[iRequest1-1];
		vecLevelingCurve.clear();
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatTrophy(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	if(iRequest1 < 0)
	{
		iSuccess = 0;
		PacketComposer.Add(&iCode);
		PacketComposer.Add(&iSuccess);
		PacketComposer.Add( -1 );
		pClient->Send(&PacketComposer);
		return;
	}

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;

	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatTrophyCnt(pAvatarInfo->iGameIDIndex, iRequest1) )
	{
		pAvatarInfo->nTrophyCount = iRequest1;
		iSuccess = 1;
	}
	else
	{
		iSuccess = 0;
	}

	PacketComposer.Add(&iCode);
	PacketComposer.Add(&iSuccess);
	PacketComposer.Add(&pAvatarInfo->nTrophyCount);
	
	pClient->Send(&PacketComposer);

	if(iSuccess == 1)
	{
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatChampCount(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	
	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	if(iRequest1 < 0)
	{
		iSuccess = 0;
		PacketComposer.Add(&iCode);
		PacketComposer.Add(&iSuccess);
		PacketComposer.Add( -1 );
		pClient->Send(&PacketComposer);
		return;
	}

	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatTournChampionCnt(pUser->GetUserIDIndex(), iRequest1) )
	{	
		pUser->SetTournamentChampionCnt( iRequest1 );
		iSuccess = 1;
	}
	else
	{
		iSuccess = 0;
	}

	PacketComposer.Add(&iCode);
	PacketComposer.Add(&iSuccess);
	PacketComposer.Add( pUser->GetTournamentChampionCnt() );

	pClient->Send(&PacketComposer);
	if(iSuccess == 1)
	{
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatPoint(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	
	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	if(iRequest1 < 0 && pUser->GetSkillPoint() < abs(iRequest1))
	{
		iSuccess = 0;
		PacketComposer.Add( &iCode);
		PacketComposer.Add( &iSuccess);	
		PacketComposer.Add(  -1 );
		pClient->Send(&PacketComposer);
		return;
	}

	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatPoint(pUser->GetUserIDIndex(), pAvatarInfo->iGameIDIndex, iRequest1) )
	{
		pUser->SetSkillPoint( pUser->GetSkillPoint() + iRequest1);
		iSuccess = 1;
	}
	else
	{
		iSuccess = 0;
	}
	
	PacketComposer.Add( &iCode);
	PacketComposer.Add( &iSuccess);	
	PacketComposer.Add( pUser->GetSkillPoint());

	pClient->Send(&PacketComposer);

	if(iSuccess == 1)
	{
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatCash(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	CPacketComposer PacketComposer(S2C_CHEAT_RES);
	
	if( iRequest1 < 0 && pUser->GetCoin() < abs(iRequest1)) 
	{
		iSuccess = 0;
		PacketComposer.Add( &iCode);
		PacketComposer.Add( &iSuccess);	
		PacketComposer.Add(  -1 );
		pClient->Send(&PacketComposer);
		return;
	}
	
	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatCash(pUser->GetUserIDIndex(), pAvatarInfo->iGameIDIndex, iRequest1) )
	{
		pUser->SetCoin( pUser->GetCoin() + iRequest1 );
		pUser->SetBonusCoin(0);
		iSuccess = 1;
	}
	else
	{
		iSuccess = 0;
	}
	
	
	PacketComposer.Add( &iCode);
	PacketComposer.Add( &iSuccess);	
	PacketComposer.Add( pUser->GetCoin() );

	pClient->Send(&PacketComposer);

	if(iSuccess == 1)
	{
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatTraining(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	
	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	SAvatarStatus Status;
	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatTraining(pAvatarInfo->iGameIDIndex, Status) )
		iSuccess = 1;
	else
		iSuccess = 0;
	
	PacketComposer.Add( &iCode);
	PacketComposer.Add( &iSuccess);
	pClient->Send(&PacketComposer);

	if(iSuccess == 1)
	{
		pAvatarInfo->Status.iStatRun += Status.iStatRunBase;
		pAvatarInfo->Status.iStatJump += Status.iStatJumpBase;
		pAvatarInfo->Status.iStatStr += Status.iStatStrBase;
		pAvatarInfo->Status.iStatPass += Status.iStatPassBase;
		pAvatarInfo->Status.iStatDribble += Status.iStatDribbleBase;
		pAvatarInfo->Status.iStatRebound += Status.iStatReboundBase;
		pAvatarInfo->Status.iStatBlock += Status.iStatBlockBase;
		pAvatarInfo->Status.iStatSteal += Status.iStatStealBase;
		pAvatarInfo->Status.iStat2Point += Status.iStat2PointBase;
		pAvatarInfo->Status.iStat3Point += Status.iStat3PointBase;
		pAvatarInfo->Status.iStatDrive += Status.iStatDriveBase;
		pAvatarInfo->Status.iStatClose += Status.iStatCloseBase;

		pAvatarInfo->Skill.iaTraining[0] = Status.iStatRunBase;
		pAvatarInfo->Skill.iaTraining[1] = Status.iStatJumpBase;
		pAvatarInfo->Skill.iaTraining[2] = Status.iStatStrBase;
		pAvatarInfo->Skill.iaTraining[3] = Status.iStatPassBase;
		pAvatarInfo->Skill.iaTraining[4] = Status.iStatDribbleBase;
		pAvatarInfo->Skill.iaTraining[5] = Status.iStatReboundBase;
		pAvatarInfo->Skill.iaTraining[6] = Status.iStatBlockBase;
		pAvatarInfo->Skill.iaTraining[7] = Status.iStatStealBase;
		pAvatarInfo->Skill.iaTraining[8] = Status.iStat2PointBase;
		pAvatarInfo->Skill.iaTraining[9] = Status.iStat3PointBase;
		pAvatarInfo->Skill.iaTraining[10] = Status.iStatDriveBase;
		pAvatarInfo->Skill.iaTraining[11] = Status.iStatCloseBase;
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatAllSkill(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	
	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	int iaSkill[MAX_SKILL_STORAGE_COUNT] = {0,};

	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatSkill(pAvatarInfo->iGameIDIndex, iaSkill ) )
		iSuccess = 1;
	else
		iSuccess = 0;
	
	PacketComposer.Add( &iCode);
	PacketComposer.Add( &iSuccess);
	pClient->Send(&PacketComposer);

	if(iSuccess == 1)
	{
		memcpy(pAvatarInfo->Skill.iaSkill, iaSkill, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
		memcpy(pAvatarInfo->Skill.iaSkillStock, iaSkill, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

		for(int i = 0; i < MAX_SKILL_SLOT; i++)
		{
			pAvatarInfo->Skill.iaUseSkill[i]=-1;
			pAvatarInfo->Skill.iaUseSkillType[i]=-1;	
		}
		sprintf_s(pAvatarInfo->Skill.szUseSkill,"");
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatAllFreeStyle(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	
	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	int iaFreeStyle[MAX_SKILL_STORAGE_COUNT] = {0,};

	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatFreeStyle(pAvatarInfo->iGameIDIndex, iaFreeStyle) )
		iSuccess = 1;
	else
		iSuccess = 0;
	
	PacketComposer.Add( &iCode);
	PacketComposer.Add( &iSuccess);
	pClient->Send(&PacketComposer);

	if(iSuccess == 1)
	{
		memcpy(pAvatarInfo->Skill.iaFreestyle, iaFreeStyle, sizeof(int)*MAX_SKILL_STORAGE_COUNT);
		memcpy(pAvatarInfo->Skill.iaFreestyleStock, iaFreeStyle, sizeof(int)*MAX_SKILL_STORAGE_COUNT);

		for(int i = 0; i < MAX_SKILL_SLOT; i++)
		{
			pAvatarInfo->Skill.iaUseFreestyle[i]=-1;
			pAvatarInfo->Skill.iaUseFreestyleType[i]=-1;	
		}
		sprintf_s(pAvatarInfo->Skill.szUseFreestyle,"");
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatSkillSlot(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	int iSlotAddCount=0;
	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	SAvatarInfo * pAvatarInfo = pUser->GetCurUsedAvatar();
	if( NULL == pAvatarInfo ) return;
	
	CPacketComposer PacketComposer(S2C_CHEAT_RES);

	SAvatarStatus Status;
	if( ODBC_RETURN_SUCCESS == pGameODBC->spCheatSkillSlot(pUser->GetUserIDIndex(), pAvatarInfo->iGameIDIndex, iSlotAddCount) )
		iSuccess = 1;
	else
		iSuccess = 0;
	
	PacketComposer.Add( &iCode );
	PacketComposer.Add( &iSuccess );
	pClient->Send(&PacketComposer);

	

	if(iSuccess == 1)
	{
		pAvatarInfo->Skill.iMaxSkillSlotNum += (iSlotAddCount * 5);
		pUser->SendUserStat();
	}
}

void CGameCheat::CheatOneManTeam(CFSGameClient * pClient, CFSGameODBC* pGameODBC)
{
	if(m_IsCheatInit == FALSE) return;

	int iCode, iRequest1, iRequest2, iSuccess;
	
	pClient->GetCheatCode(iCode, iRequest1, iRequest2);

	CFSGameUser* pUser = (CFSGameUser*)pClient->GetUser();
	if( NULL == pUser) return;

	

	CPacketComposer PacketComposer(S2C_CHEAT_RES);
	iSuccess = 1;
	
	if(iRequest1 == 0)
		SetOneManTeamOff();
	else if(iRequest1 == 1)
		SetOneManTeamOn();
	else
		iSuccess = 0;
		
	PacketComposer.Add( &iCode );
	PacketComposer.Add( &iSuccess );
	PacketComposer.Add( &iRequest1 );
	pClient->Send( &PacketComposer );
}