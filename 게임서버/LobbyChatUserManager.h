qpragma once
qinclude "Singleton.h"
qinclude "Lock.h"

class CFSGameUser;

typedef map<int/*iUserIDIndex*/, CFSGameUser*> GAMEUSERMAP;

class CLobbyChatChannel
{
public:
	CLobbyChatChannel(void) {};
	~CLobbyChatChannel(void) {};

	void Add(CFSGameUser* pUser, eCHAT_CHANNEL_TYPE eType);
	void Remove(CFSGameUser* pUser, eCHAT_CHANNEL_TYPE eType);
	void Clear();
	void ClearFaction();
	void Broadcast(CPacketComposer& Packet);
	void BroadcastFaction(CPacketComposer& Packet, int iFactionIndex);

private:
	LOCK_RESOURCE(m_csUserMap);
	GAMEUSERMAP	m_mapUser;
};

class CLobbyChatUserManager : public Singleton<CLobbyChatUserManager>
{
public:
	CLobbyChatUserManager(void);
	virtual ~CLobbyChatUserManager(void);

	void AddUser(CFSGameUser* pUser, int iRoomType, int iRoomIndex);
	void AddFactionUser(CFSGameUser* pUser);
	void RemoveFactionUser();

	void ChatServerDisconnected();

	void Broadcast(CPacketComposer& Packet, int iRoomType, int iRoomIndex);
	void BroadcastFaction(CPacketComposer& Packet, int iFactionIndex);

private:
	CLobbyChatChannel m_ChatChannel[CHAT_LOBBYROOM_TYPE_MAX][MAX_LOBBY_CHAT_ROOM_COUNT+1];
	CLobbyChatChannel m_ChatFactionChannel[MAX_FACTION_INDEX_COUNT];
};

#define LOBBYCHAT CLobbyChatUserManager::GetInstance() 
