// CFSGameService.cpp: implementation of the CFSGameService class.
//
//////////////////////////////////////////////////////////////////////

qinclude "stdafx.h"
qinclude "CFSGameService.h"

qinclude "CFSGameServer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFSGameService::CFSGameService(CIOCP* pIOCP):CIOCPService(pIOCP)
{

}

CFSGameService::~CFSGameService()
{

}

CServer * CFSGameService::CreateServer(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv)
{
	//int iServerKind = PCK_NONE;
	unsigned short usPort = (unsigned short) -1;
	UINT uiServerIndex = 0;

	if(NULL != ppGlobalArgv)
	{
		for(int iIndex = 0; iIndex < iGlobalArgc; iIndex++)
		{
			if( (TEXT('/') == ppGlobalArgv[iIndex][0]) || (TEXT('-') == ppGlobalArgv[iIndex][0]) )
			{
				if(0 == ::lstrcmpi(&(ppGlobalArgv[iIndex][1]), TEXT("Port")))
				{
					if(iIndex + 1 < iGlobalArgc)
					{
						iIndex++;
						usPort = (unsigned short) atoi(ppGlobalArgv[iIndex]);
					}
				}
			}
		}
	}

	for(DWORD dwIndex = 0; dwIndex < dwArgc; dwIndex++)
	{
		if( (TEXT('/') == pszArgv[dwIndex][0]) || (TEXT('-') == pszArgv[dwIndex][0]) )
		{
			if(0 == ::lstrcmpi(&(pszArgv[dwIndex][1]), TEXT("Port")))
			{
				if(dwIndex + 1 < dwArgc)
				{
					dwIndex++;
					usPort = (unsigned short) atoi(pszArgv[dwIndex]);
				}
			}
		}
	}

#ifdef _DEBUG
	unsigned short usProcessID = -1;

	{
		for(DWORD dwIndex = 0; dwIndex < dwArgc; dwIndex++)
		{
			if( (TEXT('/') == pszArgv[dwIndex][0]) || (TEXT('-') == pszArgv[dwIndex][0]) )
			{
				if(0 == ::lstrcmpi(&(pszArgv[dwIndex][1]), TEXT("PID")))
				{
					if(dwIndex + 1 < dwArgc)
					{
						dwIndex++;
						usProcessID = (unsigned short) atoi(pszArgv[dwIndex]);
					}
				}
			}
		}
	}
#endif

	CFSGameServer * pServer = NULL;

	pServer = new CFSGameServer(GetIOCP(), "FSGameServer", INFINITE);

	if( usPort == (unsigned short) -1 )
	{
		TCHAR * temp;
		if( temp = strchr(pszArgv[0], '_') )
		{
			pServer->SetServerPort( atoi(temp+1) );
		}

		TCHAR t = *temp;
		*temp = 0;
		int nProcessID = atoi(temp - 3);
		*temp = t;

		pServer->SetProcessID(nProcessID);
	}
	else
	{

#ifdef _DEBUG
		pServer->SetProcessID(usProcessID);
#endif
		pServer->SetServerPort(usPort);

	}

	return pServer;
}

