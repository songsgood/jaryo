// TTFSGameSvr.cpp : Defines the entry point for the console application.
//

qinclude "stdafx.h"
qinclude "TTFSGameSvr.h"

qinclude "WinSock.h"

qinclude "CIOCPServiceServerManager.h"
qinclude "MessageBox.h"

qinclude "CFSGameService.h"
qinclude "CFSGameThread.h"

qinclude "CFSGameDBCfg.h"
qinclude "CFSFileLog.h"

qinclude "EHModuleManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The one and only application object
extern CLogManager g_LogManager; 
CFSFileLog		   g_FSFileLog;

using namespace std;

CIOCPServiceServerManager<CFSGameService, CFSGameThread>*	g_pIOCPServiceServerManager	= NULL;

CFSGameDBCfg			*	g_pFSDBCfg			= NULL;

//구동을 위한 필수 - public
void CleanupObjects();

BOOL ParseCommandLineAndDoWork( int iArgc, LPTSTR * ppArgv );
//iServerMode 0 -> Game, iServerMode 1 -> Login
BOOL AllocateObjects(BOOL bDebugMode = FALSE);
BOOL DoService(BOOL bDebugMode = FALSE, int iArgc = 0, LPTSTR * ppArgv = NULL );
//구동을 위한 필수 - End

BOOL AllocateObjects(BOOL bDebugMode)
{
	AUTH_TYPE eAuthType = AUTH_TYPE_NONE;//bDebugMode ? AUTH_TYPE_DEBUG : AUTH_TYPE_RELEASE;
	WRITE_LOG_NEW(LOG_TYPE_SYSTEM, ALLOCATION, START, "AllocateObjects");

	g_pFSDBCfg	 = new CFSGameDBCfg();

	g_FSFileLog.SetAppName("FSGameServer");

	if( NULL == g_pFSDBCfg )	return FALSE;

	g_pIOCPServiceServerManager = new CIOCPServiceServerManager<CFSGameService, CFSGameThread>;
	if(false == g_pIOCPServiceServerManager->Initialize(eAuthType))
	{
		return FALSE;
	}

	return TRUE;
}


void CleanUpObjects()
{
	if(NULL != g_pIOCPServiceServerManager)
	{
		delete g_pIOCPServiceServerManager;
		g_pIOCPServiceServerManager = NULL;
	}
	
	if( NULL != g_pFSDBCfg )
	{
		delete g_pFSDBCfg;
		g_pFSDBCfg = NULL;
	}
	
}


BOOL DoService(BOOL bDebugMode, int iArgc, LPTSTR* ppArgv)
{
	BOOL bSuccessful = FALSE;
	
	if(FALSE != ::InitializeWinSock())
	{
		if(FALSE == ::AllocateObjects(bDebugMode))
		{
			::ShowMessageBox(TEXT("Object Allocation Error"));
			
		}
		else
		{
			SERVICE_TABLE_ENTRY ServiceTable[] = {
				{ TEXT("NEXUS_GAME"), CService::ServiceMain },
				{ TEXT("NEXUS_TEST"), CService::ServiceMain },
				{ NULL, NULL }
			};
			
			if(FALSE == g_pIOCPServiceServerManager->Start(ServiceTable, bDebugMode, iArgc, ppArgv))
			{
				::ShowMessageBox(TEXT("Service Start Failed"));
			}
			else
			{
				bSuccessful = TRUE;
			}
		}
		
		::CleanUpObjects();
		::CleanupWinSock();
	}
	return bSuccessful;
}


BOOL ParseCommandLineAndDoWork(int iArgc, LPTSTR* ppArgv)
{
	CService::SetGlobalParameter(iArgc, ppArgv);
	BOOL bSuccessfulWork = FALSE;
	
	if(iArgc >= 1)
	{
		enum SERVICEKIND { SK_NORMAL, SK_DEBUG, SK_HELP } ServiceKind = SK_NORMAL;
		
		for(int iIndex = 1; iIndex < iArgc; iIndex++)
		{
			if( (TEXT('/') == ppArgv[iIndex][0]) || (TEXT('-') == ppArgv[iIndex][0]) )
			{
				if(0 == ::lstrcmpi(&(ppArgv[iIndex][1]), TEXT("debug")))
				{
					ServiceKind = SK_DEBUG;
				}
				else if(0 == ::lstrcmpi(&(ppArgv[iIndex][1]), TEXT("help")))
				{
					ServiceKind = SK_HELP;
					break;
				}
			}
		}
		
		switch(ServiceKind)
		{
		case SK_NORMAL:
			bSuccessfulWork = ::DoService(FALSE, 0, NULL);
			break;
			
		case SK_DEBUG:
			bSuccessfulWork = ::DoService(TRUE, iArgc, ppArgv);
			break;
			
		case SK_HELP:
			::ShowMessageBox(TEXT("Options :\n\n")
				TEXT("\t/help\t\nShows this message")
				TEXT("\t/debug\tRuns the service as a normal process for debugging"));
			break;
		}
	}
	return bSuccessfulWork;
}




int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;
	
	CEHModuleManager ModuleManger;
	if(FALSE == ModuleManger.Install(DL_MY_HANDLER_STACKOVERFLOW, EH_BREAKPAD))
	{
		WRITE_LOG_NEW(LOG_TYPE_SYSTEM, MODULEMANAGER_INSTALL, NONE, "Kind:10752790", EH_BREAKPAD);
	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		nRetCode = 1;
	}
	else
	{
		if(FALSE == ::ParseCommandLineAndDoWork(argc, argv))
		{
			ASSERT(0);
		}
	}
	
	return nRetCode;
}


