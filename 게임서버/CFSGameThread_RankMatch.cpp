qinclude "stdafx.h"
qinclude "CFSGameThread.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameClient.h"
qinclude "MatchSvrProxy.h"
qinclude "RankMatchManager.h"
qinclude "ODBCSvrProxy.h"
qinclude "LeagueSvrProxy.h"

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_AVOID_TEAM_INFO_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	pUser->GetUserRankMatch()->SendPacketAvoidTeamInfo();
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_AVOID_TEAM_SAVE_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SC2S_RANKMATCH_AVOID_TEAM_SAVE_REQ	info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_RANKMATCH_AVOID_TEAM_SAVE_REQ));

	SS2C_RANKMATCH_AVOID_TEAM_SAVE_RES res;
	res.btResult = RESULT_RANKMATCH_AVOID_TEAM_SAVE_LIMIT_EXCEEDED;
	if( FALSE == pUser->GetUserRankMatch()->CheckAvoidTeamLimit(info.btCheckIndexCount))
	{
		pUser->Send(S2C_RANKMATCH_AVOID_TEAM_SAVE_RES, &res, sizeof(SS2C_RANKMATCH_AVOID_TEAM_SAVE_RES));
		return;
	}

	list<SRANKMATCH_AVOID_TEAM>	listSaveAvoidInfo;
	for( int i = 0 ; i < info.btCheckIndexCount ; ++i )
	{
		SRANKMATCH_AVOID_TEAM	sAvoidInfo;
		m_ReceivePacketBuffer.Read((PBYTE)&sAvoidInfo, sizeof(SRANKMATCH_AVOID_TEAM	));
		listSaveAvoidInfo.push_back(sAvoidInfo);
	}

	pUser->GetUserRankMatch()->ReqAvoidTeamSave(listSaveAvoidInfo, res.btResult);
	pUser->Send(S2C_RANKMATCH_AVOID_TEAM_SAVE_RES, &res, sizeof(SS2C_RANKMATCH_AVOID_TEAM_SAVE_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_READY_TIME_CHECK_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchSvrProxy* pMatch = (CMatchSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SC2S_RANKMATCH_READY_TIME_CHECK_REQ	info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_RANKMATCH_READY_TIME_CHECK_REQ));

	//ToDo RankMatch
	//pMatch->Send
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_ENTER_RANKMATCH_LYAER_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_ENTER_RANKMATCH_LYAER_RES res;
	if( pUser->GetCurUsedAvtarLv() < RANKMATCH.GetConfigValue(LEAGUE_CONFIG_PageRankPositionLimitLevel))
		res.btResult = RESULT_MOVE_RANKMATCH_LAYER_ENOUGHT_LV;
	else if(FALSE == pFSClient->SetState(FS_RANK_MATCH))
		res.btResult = RESULT_MOVE_RANKMATCH_LAYER_FAIL;
	else 
		res.btResult = RESULT_MOVE_RANKMATCH_LAYER_SUCCESS;

	if( res.btResult == RESULT_MOVE_RANKMATCH_LAYER_SUCCESS && pUser->GetUserRankMatch()->CheckPlacementTest() )
		res.btResult = RESULT_MOVE_RANKMATCH_LAYER_SUCCESS_PLACEMENT_TEST;
	else if( res.btResult == RESULT_MOVE_RANKMATCH_LAYER_SUCCESS && RANKMATCH.CheckRankPageEnterCondition(res.iOpenDate))
		res.btResult = RESULT_MOVE_RANKMATCH_LAYER_NOT_OPEN_YET;

	pUser->Send(S2C_ENTER_RANKMATCH_LYAER_RES, &res, sizeof(SS2C_ENTER_RANKMATCH_LYAER_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_EXIT_RANKMATCH_LYAER_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	SS2C_EXIT_RANKMATCH_LYAER_RES res;
	if(FALSE == pFSClient->SetState(NEXUS_LOBBY))
		res.btResult = RESULT_MOVE_RANKMATCH_LAYER_FAIL;
	else 
		res.btResult = RESULT_MOVE_RANKMATCH_LAYER_SUCCESS;

	pUser->Send(S2C_EXIT_RANKMATCH_LYAER_RES, &res, sizeof(SS2C_EXIT_RANKMATCH_LYAER_RES));
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_RANK_LIST_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

 	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
 	CHECK_NULL_POINTER_VOID(pUser);
 
 	CODBCSvrProxy*	pODBCProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
 	CHECK_NULL_POINTER_VOID(pODBCProxy);
 
 	SC2S_RANKMATCH_RANK_LIST_REQ	info;
 	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_RANKMATCH_RANK_LIST_REQ));
 	
	SS2O_RANKMATCH_RANK_LIST_REQ	req;
	req.btMyRank = info.btMyRank;

	if( TRUE == pUser->GetUserRankMatch()->GetLeagueInfo(req.btTier, req.btLeagueRating, req.iGroup ))
	{// 배치고사중이 아니라면. 들어올수 있다.
		if( TRUE == req.btMyRank )
		{ /* do nothing */	}
		else
		{
			req.btLeagueRating = info.btLeagueRating; // 보기로 한 페이지를 봄.
		}

		char* szGroupName = RANKMATCH.GetGroupName(req.iGroup);
		if( NULL == szGroupName )
		{
			ZeroMemory(req.szGroupName, sizeof(char) * MAX_RANKMATCH_GROUPNAME_LENGTH);
		}
		else
		{
			memcpy(req.szGroupName, szGroupName, MAX_RANKMATCH_GROUPNAME_LENGTH+1);
			req.szGroupName[MAX_RANKMATCH_GROUPNAME_LENGTH] = 0;
		}

		CPacketComposer Packet(S2O_RANKMATCH_RANK_LIST_REQ);
		Packet.Add((int)pUser->GetGameIDIndex());
		Packet.Add((PBYTE)&req, sizeof(SS2O_RANKMATCH_RANK_LIST_REQ));

		pODBCProxy->Send(&Packet);
	}
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_RANK_SEASON_RECORD_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CODBCSvrProxy*	pODBCProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pODBCProxy);
	
	SC2S_RANKMATCH_RANK_SEASON_RECORD_REQ	info;
	m_ReceivePacketBuffer.Read((PBYTE)&info, sizeof(SC2S_RANKMATCH_RANK_SEASON_RECORD_REQ));

	CPacketComposer	Packet(S2O_RANKMATCH_RANK_SEASON_RECORD_REQ);
	Packet.Add((int)pUser->GetGameIDIndex());
	Packet.Add((PBYTE)&info, sizeof(SC2S_RANKMATCH_RANK_SEASON_RECORD_REQ));

	pODBCProxy->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_GAMEEND_DETAIL_RECORD_NOT)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CODBCSvrProxy*	pODBCProxy = (CODBCSvrProxy*)GAMEPROXY.GetProxy(FS_ODBC_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pODBCProxy);

	BYTE btServerIndex = CFSGameServer::GetInstance()->GetServerIndex();
	CPacketComposer	Packet(S2O_RANKMATCH_GAMEEND_DETAIL_RECORD_NOT);
	Packet.Add((int)pUser->GetGameIDIndex());
	Packet.Add(btServerIndex);
	Packet.Add(m_ReceivePacketBuffer.GetRemainedData(), m_ReceivePacketBuffer.GetRemainedDataSize());
	pODBCProxy->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_ROOM_READY_NOT)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	// todo ready
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_MATCHING_COMPLETED_RES)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);

	CLeagueSvrProxy* pLeagueProxy = (CLeagueSvrProxy*)GAMEPROXY.GetProxy(FS_LEAGUE_SERVER_PROXY);
	CHECK_NULL_POINTER_VOID(pLeagueProxy);

	SC2S_RANKMATCH_MATCHING_COMPLETED_RES rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_RANKMATCH_MATCHING_COMPLETED_RES));

	if(rs.btResult == RESULT_RANKMATCH_MATCHING_COMPLETED_ACCEPT)
		pUser->GetUserRankMatch()->ClearLimitMatchingInfo();
	else
		pUser->GetUserRankMatch()->UpdateLimitMatchingInfo();

	SG2L_RANKMATCH_MATCHING_COMPLETED_RES	info;

	info.btResult = rs.btResult;
	info.btServerIndex = CFSGameServer::GetInstance()->GetServerIndex();
	info.iGameIDIndex = pUser->GetGameIDIndex();

	CPacketComposer	Packet(G2L_RANKMATCH_MATCHING_COMPLATED_RES);
	Packet.Add((PBYTE)&info,sizeof(SG2L_RANKMATCH_MATCHING_COMPLETED_RES) );
	pLeagueProxy->Send(&Packet);
}

DEFINE_GAMETHREAD_PROC_FUNC(C2S_RANKMATCH_ROOM_READY_REQ)
{
	CHECK_CONDITION_RETURN_VOID( FALSE == RANKMATCH.GetConfigEnable(LEAGUE_CONFIG_Switch) );

	CFSGameUser* pUser = (CFSGameUser*) pFSClient->GetUser();
	CHECK_NULL_POINTER_VOID(pUser);
	
	CMatchBaseSvrProxy* pMatch = (CMatchBaseSvrProxy*)GAMEPROXY.FindProxy(pUser->GetMatchLocation());
	CHECK_NULL_POINTER_VOID(pMatch);

	SC2S_RANKMATCH_ROOM_READY_REQ rs;
	m_ReceivePacketBuffer.Read((PBYTE)&rs, sizeof(SC2S_RANKMATCH_ROOM_READY_REQ));

	SG2L_RANKMATCH_ROOM_READY_REQ	info;

	info.iGameIDIndex = pUser->GetGameIDIndex();
	info.btServerIndex = CFSGameServer::GetInstance()->GetServerIndex();
	info.btReady = rs.btReady;

	CPacketComposer	Packet(G2L_RANKMATCH_ROOM_READY_REQ);
	Packet.Add((PBYTE)&info, sizeof(SG2L_RANKMATCH_ROOM_READY_REQ));
	pMatch->Send(&Packet);
}