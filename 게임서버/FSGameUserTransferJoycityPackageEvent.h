qpragma once

qinclude "CFSODBCBase.h"

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserTransferJoycityPackageEvent
{
public:
	CFSGameUserTransferJoycityPackageEvent(CFSGameUser* pUser);
	~CFSGameUserTransferJoycityPackageEvent(void);

	BOOL	Load();
	
	void SendEventInfo();
	RESULT_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD GetRewardReq(BYTE btRewardType, SS2C_EVENT_TRANSFERJOYCITY_PACKAGE_GET_REWARD_RES& rs);

private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	BOOL m_bIsTransferUser;
	BOOL m_bIsGetCash; 
	BOOL m_bIsGetPackage;
};

