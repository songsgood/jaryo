qinclude "stdafx.h"
qinclude "CFSGameUserCharacterCollection.h"
qinclude "CFSGameUser.h"
qinclude "CharacterCollectionConfigManager.h"

CGameUserCharacterCollection::CGameUserCharacterCollection(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
{
	memset(m_iaSentenceSlot, -1, sizeof(int)*MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT);
}

CGameUserCharacterCollection::~CGameUserCharacterCollection(void)
{
	RemoveAll();
}

void CGameUserCharacterCollection::RemoveAll()
{
	CHARACTER_COLLECTION_USER_BOOK_LIST::iterator bookItr = m_listUserBook.begin();
	CHARACTER_COLLECTION_USER_BOOK_LIST::iterator bookItrEnd = m_listUserBook.end();
	for( ; bookItr != bookItrEnd; ++bookItr)
	{
		SAFE_DELETE(*bookItr);
	}
	m_listUserBook.clear();

	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator sentenceItr = m_listUserSentence.begin();
	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator sentenceItrEnd = m_listUserSentence.end();
	for( ; sentenceItr != sentenceItrEnd; ++sentenceItr)
	{
		SAFE_DELETE(*sentenceItr);
	}
	m_listUserSentence.clear();
}

BOOL CGameUserCharacterCollection::Load()
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	vector<SCharacterCollectionUserBook> vUserAvatar;
	if (ODBC_RETURN_SUCCESS != pODBC->CHARACTER_COLLECTION_GetUserSpecialAvatarList(m_pUser->GetUserIDIndex(), vUserAvatar)) 
	{
		WRITE_LOG_NEW(LOG_TYPE_CHARACTERCOLLECTION, INITIALIZE_DATA, FAIL, "CHARACTER_COLLECTION_GetUserSpecialAvatarList error");
		return FALSE;		
	}
	AddUserAvatar(vUserAvatar);

	vector<SCharacterCollectionUserSentenceSlot> vUserSentenceSlot;
	if (ODBC_RETURN_SUCCESS != pODBC->CHARACTER_COLLECTION_GetUserSentenceSlot(m_pUser->GetUserIDIndex(), vUserSentenceSlot))
	{
		WRITE_LOG_NEW(LOG_TYPE_CHARACTERCOLLECTION, INITIALIZE_DATA, FAIL, "CHARACTER_COLLECTION_GetUserSentence error");
		return FALSE;		
	}
	AddUserSentenceSlot(vUserSentenceSlot);

	MakeBook();
	MakeSentenceList();

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CGameUserCharacterCollection::AddUserAvatar(vector<SCharacterCollectionUserBook> vUserAvatar)
{
	for(int i = 0; i < vUserAvatar.size(); ++i)
	{
		CHARACTER_COLLECTION_USER_BOOK_MAP::iterator itr = m_mapUserBook.find(vUserAvatar[i].iSpecialAvatarIndex);
		if(itr != m_mapUserBook.end())
		{
			SCharacterCollectionUserBook* pAvatar = itr->second;
			CHECK_NULL_POINTER_VOID(pAvatar);

			if(vUserAvatar[i].iLv > pAvatar->iLv ||
				(vUserAvatar[i].iLv == pAvatar->iLv &&
				vUserAvatar[i].tOpdate < pAvatar->tOpdate))
			{
				pAvatar->iGameIDIndex = vUserAvatar[i].iGameIDIndex;
				strncpy_s(pAvatar->szGameID, _countof(pAvatar->szGameID), vUserAvatar[i].szGameID, _countof(vUserAvatar[i].szGameID)-1);
				pAvatar->iTeamIndex = vUserAvatar[i].iTeamIndex;
				pAvatar->iSpecialAvatarIndex = vUserAvatar[i].iSpecialAvatarIndex;
				pAvatar->iFace = vUserAvatar[i].iFace;
				pAvatar->iGamePosition = vUserAvatar[i].iGamePosition;
				pAvatar->iLv = vUserAvatar[i].iLv;
			}
		}
		else
		{
			SCharacterCollectionUserBook* pUserAvatar = new SCharacterCollectionUserBook;
			CHECK_NULL_POINTER_VOID(pUserAvatar);

			memcpy(pUserAvatar, &vUserAvatar[i], sizeof(SCharacterCollectionUserBook));
			m_mapUserBook.insert(CHARACTER_COLLECTION_USER_BOOK_MAP::value_type(vUserAvatar[i].iSpecialAvatarIndex, pUserAvatar));
		}
	}
}

void CGameUserCharacterCollection::AddUserSentenceSlot(vector<SCharacterCollectionUserSentenceSlot> vUserSentenceSlot)
{
	for(int i = 0; i <vUserSentenceSlot.size(); ++i)
	{
		CHECK_CONDITION_RETURN_VOID(vUserSentenceSlot[i].iSlotIndex < 0 || vUserSentenceSlot[i].iSlotIndex >= MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT);
		m_iaSentenceSlot[vUserSentenceSlot[i].iSlotIndex] = vUserSentenceSlot[i].iSentenceIndex;
	}
}

void CGameUserCharacterCollection::MakeBook()
{
	vector<SCharacterCollectionBookConfig> vBookConfig;
	CCHARACTERCOLLECTIONCONFIG.GetBookConfigList(vBookConfig);

	for(int i = 0; i < vBookConfig.size(); ++i)
	{
		SCharacterCollectionUserBook* pUserBook = NULL;
		CHARACTER_COLLECTION_USER_BOOK_MAP::iterator itr = m_mapUserBook.find(vBookConfig[i].iSpecialAvatarIndex);
		if(itr != m_mapUserBook.end())
		{
			pUserBook = itr->second;
			CHECK_NULL_POINTER_VOID(pUserBook);

			pUserBook->btStatus = CHARACTER_COLLECTION_CHAR_SLOT_STATUS_HAVE;
			pUserBook->iTeamIndex = vBookConfig[i].iTeamIndex;
			pUserBook->iFace = vBookConfig[i].iFace;
		}
		else
		{
			pUserBook = new SCharacterCollectionUserBook;
			CHECK_NULL_POINTER_VOID(pUserBook);

			pUserBook->btStatus = CHARACTER_COLLECTION_CHAR_SLOT_STATUS_NO_HAVE;
			pUserBook->iGameIDIndex = 0;
			ZeroMemory(pUserBook->szGameID, sizeof(pUserBook->szGameID));
			pUserBook->iTeamIndex = vBookConfig[i].iTeamIndex;
			pUserBook->iSpecialAvatarIndex = vBookConfig[i].iSpecialAvatarIndex;
			pUserBook->iFace = vBookConfig[i].iFace;
			pUserBook->iGamePosition = 0;
			pUserBook->iLv = 0;

			m_mapUserBook.insert(CHARACTER_COLLECTION_USER_BOOK_MAP::value_type(pUserBook->iSpecialAvatarIndex, pUserBook));
		}

		m_listUserBook.push_back(pUserBook);
	}

	m_listUserBook.sort(SCharacterCollectionUserBook::SortByBook);
}

BYTE CGameUserCharacterCollection::GetAllSentenceConditionStatus(int iSentenceIndex)
{
	SentenceConditionConfigVector vSentenceConditionConfig;
	CCHARACTERCOLLECTIONCONFIG.GetSentenceConditionConfigList(iSentenceIndex, vSentenceConditionConfig);

	BYTE btStatus = CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE;
	for(int i = 0; i < vSentenceConditionConfig.size(); ++i)
	{
		btStatus = GetSentenceConditionStatus(vSentenceConditionConfig[i].iSpecialAvatarIndex, vSentenceConditionConfig[i].iConditionLv);
		CHECK_CONDITION_RETURN(btStatus != CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE, btStatus);
	}

	return CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE;
}

BYTE CGameUserCharacterCollection::GetSentenceConditionStatus(int iSpecialAvatarIndex, int iConditionLv)
{
	CHARACTER_COLLECTION_USER_BOOK_MAP::iterator itr = m_mapUserBook.find(iSpecialAvatarIndex);
	CHECK_CONDITION_RETURN(itr == m_mapUserBook.end(), CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NEED_CHAR);
	CHECK_CONDITION_RETURN(itr->second == NULL, CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NEED_CHAR);
	CHECK_CONDITION_RETURN(itr->second->iGameIDIndex == 0, CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NEED_CHAR);
	CHECK_CONDITION_RETURN(itr->second->iLv < iConditionLv, CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_ENOUGHT_LV);

	return CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE;
}

void CGameUserCharacterCollection::MakeSentenceList()
{
	vector<int> vSentenceIndex;
	CCHARACTERCOLLECTIONCONFIG.GetSentenceConfigList(vSentenceIndex);

	BYTE btStatus = CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE;
	for(int i = 0; i < vSentenceIndex.size(); ++i)
	{
		SCharacterCollectionUserSentence* pUserSentence = new SCharacterCollectionUserSentence;
		CHECK_NULL_POINTER_VOID(pUserSentence);

		pUserSentence->iSentenceIndex = vSentenceIndex[i];
		pUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_OFF;

		btStatus = GetAllSentenceConditionStatus(pUserSentence->iSentenceIndex);
		if(btStatus == CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE)
			pUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_ON;

		for(int iSlotIndex = 0; iSlotIndex < MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT; ++iSlotIndex)
		{
			if(m_iaSentenceSlot[iSlotIndex] > -1 &&
				m_iaSentenceSlot[iSlotIndex] == pUserSentence->iSentenceIndex &&
				btStatus == CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE)
			{
				pUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_WEAR;
			}
		}

		m_mapUserSentence.insert(CHARACTER_COLLECTION_USER_SENTENCE_MAP::value_type(pUserSentence->iSentenceIndex, pUserSentence));
		m_listUserSentence.push_back(pUserSentence);
	}

	m_listUserSentence.sort(SCharacterCollectionUserSentence::SortBySentence);

	CheckWearSlot();
}

void CGameUserCharacterCollection::SendCharList(int iPageNum)
{
	CHECK_CONDITION_RETURN_VOID(m_bDataLoaded == FALSE);
	CHECK_CONDITION_RETURN_VOID(iPageNum == 0);
	int iMaxPageNum = ceil((float)m_listUserBook.size() / MAX_CHARACTER_COLLECTION_BOOK_SLOT_COUNT_PER_PAGE);
	CHECK_CONDITION_RETURN_VOID(iPageNum > iMaxPageNum);

	const int iListStartNum = (iPageNum - 1) * MAX_CHARACTER_COLLECTION_BOOK_SLOT_COUNT_PER_PAGE + 1;
	CPacketComposer Packet(S2C_CHARACTER_COLLECTION_CHAR_LIST_RES);
	SS2C_CHARACTER_COLLECTION_CHAR_LIST_RES rs;
	rs.iMaxPageNum = iMaxPageNum;
	rs.iPageNum = iPageNum;
	rs.iSlotCount = 0;
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CHARACTER_COLLECTION_CHAR_LIST_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	if(m_listUserBook.empty())
	{
		MakeBook();
	}

	CHARACTER_COLLECTION_CHAR_INFO info;
	CHARACTER_COLLECTION_USER_BOOK_LIST::iterator itr = m_listUserBook.begin();
	CHARACTER_COLLECTION_USER_BOOK_LIST::iterator itrEnd = m_listUserBook.end();
	for(int i = 1 ; itr != itrEnd; ++itr)
	{
		SCharacterCollectionUserBook* pUserBook = *itr;
		CHECK_NULL_POINTER_VOID(pUserBook);

		if(iListStartNum <= i++)
		{
			++rs.iSlotCount;

			info.btStatus = pUserBook->btStatus;
			info.iTeamIndex = pUserBook->iTeamIndex;
			info.iFace = pUserBook->iFace;
			strncpy_s(info.szGameID, _countof(info.szGameID), pUserBook->szGameID, _countof(info.szGameID)-1);
			info.iGamePosition = pUserBook->iGamePosition;
			info.iLv = pUserBook->iLv;
			Packet.Add((PBYTE)&info, sizeof(CHARACTER_COLLECTION_CHAR_INFO));

			if(MAX_CHARACTER_COLLECTION_BOOK_SLOT_COUNT_PER_PAGE <= rs.iSlotCount)
				break;
		}
	}

	memcpy(pCountLocation, &rs.iSlotCount, sizeof(int));
	m_pUser->Send(&Packet);
}

void CGameUserCharacterCollection::SendWearSentenceList()
{
	CPacketComposer Packet(S2C_CHARACTER_COLLECTION_WEAR_SENTENCE_RES);
	SS2C_CHARACTER_COLLECTION_WEAR_SENTENCE_RES rs;

	for(int iSlotIndex = 0; iSlotIndex < MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT; ++iSlotIndex)
	{
		rs.SentenceInfo[iSlotIndex].iSlotIndex = iSlotIndex;
		rs.SentenceInfo[iSlotIndex].iSentenceIndex = m_iaSentenceSlot[iSlotIndex];
		memset(rs.SentenceInfo[iSlotIndex].RewardInfo, -1, sizeof(CHARACTER_COLLECTION_SENTENCE_REWARD_INFO)*MAX_CHARACTER_COLLECTION_REWARD_COUNT);

		if(m_iaSentenceSlot[iSlotIndex] > -1)
		{
			SentenceRewardConfigVector vRewardConfig;
			CCHARACTERCOLLECTIONCONFIG.GetRewardConfigList(m_iaSentenceSlot[iSlotIndex], vRewardConfig);

			for(int iRewardIndex = 0; iRewardIndex < vRewardConfig.size(); ++iRewardIndex)
			{
				rs.SentenceInfo[iSlotIndex].RewardInfo[iRewardIndex].iRewardType = vRewardConfig[iRewardIndex].iRewardType;
				rs.SentenceInfo[iSlotIndex].RewardInfo[iRewardIndex].fValue = vRewardConfig[iRewardIndex].fNoticeValue;
			}
		}	
	}

	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	rs.iTotalRewardCount = vReward.size();
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CHARACTER_COLLECTION_WEAR_SENTENCE_RES));

	CHARACTER_COLLECTION_SENTENCE_REWARD_INFO RewardInfo;
	for(int i = 0; i < vReward.size(); ++i)
	{
		RewardInfo.iRewardType = vReward[i].iRewardType;
		RewardInfo.fValue = vReward[i].fNoticeValue;
		Packet.Add((PBYTE)&RewardInfo, sizeof(CHARACTER_COLLECTION_SENTENCE_REWARD_INFO));
	}

	m_pUser->Send(&Packet);
}

void CGameUserCharacterCollection::GetWearRewardList(vector<SCharacterCollectionSentenceRewardConfig>& vReward)
{
	int iSlotIndex_1 = 0, iSlotIndex_2 = 1;
	if(m_iaSentenceSlot[iSlotIndex_1] > -1)
	{
		SentenceRewardConfigVector vRewardConfig;
		CCHARACTERCOLLECTIONCONFIG.GetRewardConfigList(m_iaSentenceSlot[iSlotIndex_1], vRewardConfig);

		SCharacterCollectionSentenceRewardConfig RewardConfig;
		for(int iRewardIndex = 0; iRewardIndex < vRewardConfig.size(); ++iRewardIndex)
		{
			RewardConfig.iRewardType = vRewardConfig[iRewardIndex].iRewardType;
			RewardConfig.fNoticeValue = vRewardConfig[iRewardIndex].fNoticeValue;
			RewardConfig.fValue = vRewardConfig[iRewardIndex].fValue;
			vReward.push_back(RewardConfig);
		}
	}

	BOOL bExistRewardType = FALSE;
	if(m_iaSentenceSlot[iSlotIndex_2] > -1)
	{
		SentenceRewardConfigVector vRewardConfig;
		CCHARACTERCOLLECTIONCONFIG.GetRewardConfigList(m_iaSentenceSlot[iSlotIndex_2], vRewardConfig);

		SCharacterCollectionSentenceRewardConfig RewardConfig;
		for(int iRewardIndex = 0; iRewardIndex < vRewardConfig.size(); ++iRewardIndex)
		{
			RewardConfig.iRewardType = vRewardConfig[iRewardIndex].iRewardType;
			RewardConfig.fNoticeValue = vRewardConfig[iRewardIndex].fNoticeValue;
			RewardConfig.fValue = vRewardConfig[iRewardIndex].fValue;

			for(int i = 0; i < vReward.size(); ++i)
			{
				if(RewardConfig.iRewardType == vReward[i].iRewardType)
				{
					bExistRewardType = TRUE;
					if(RewardConfig.fNoticeValue > vReward[i].fNoticeValue)
					{
						vReward[i].fNoticeValue = RewardConfig.fNoticeValue;
						vReward[i].fValue = RewardConfig.fValue;
					}
				}
				else if(RewardConfig.iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_ALL_ABILITY &&
					(vReward[i].iRewardType >= CHARACTER_COLLECTION_REWARD_TYPE_RUN &&
					vReward[i].iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE))
				{
					if(RewardConfig.fNoticeValue < vReward[i].fNoticeValue)
					{
						vReward[i].fNoticeValue -= RewardConfig.fNoticeValue;
						vReward[i].fValue -= RewardConfig.fValue;
					}
					else
					{
						vReward.erase(vReward.begin() + i--);
					}
				}
				else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_ALL_ABILITY &&
					(RewardConfig.iRewardType >= CHARACTER_COLLECTION_REWARD_TYPE_RUN &&
					RewardConfig.iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE))
				{

					if(vReward[i].fNoticeValue < RewardConfig.fNoticeValue)
					{
						RewardConfig.fNoticeValue -= vReward[i].fNoticeValue;
						RewardConfig.fValue -= vReward[i].fValue;
					}
					else
					{
						bExistRewardType = TRUE;
					}
				}
			}

			if(bExistRewardType == FALSE)
				vReward.push_back(RewardConfig);

			bExistRewardType = FALSE;
		}
	}
}

void CGameUserCharacterCollection::SendSentenceList(int iPageNum)
{
	CHECK_CONDITION_RETURN_VOID(m_bDataLoaded == FALSE);
	CHECK_CONDITION_RETURN_VOID(iPageNum == 0);
	int iMaxPageNum = ceil((float)m_listUserSentence.size() / MAX_CHARACTER_COLLECTION_SENTENCE_SLOT_COUNT_PER_PAGE);
	CHECK_CONDITION_RETURN_VOID(iPageNum > iMaxPageNum);

	const int iListStartNum = (iPageNum - 1) * MAX_CHARACTER_COLLECTION_SENTENCE_SLOT_COUNT_PER_PAGE + 1;
	CPacketComposer Packet(S2C_CHARACTER_COLLECTION_SENTENCE_LIST_RES);
	SS2C_CHARACTER_COLLECTION_SENTENCE_LIST_RES rs;
	rs.iMaxPageNum = iMaxPageNum;
	rs.iPageNum = iPageNum;
	rs.iSentenceCount = 0;
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CHARACTER_COLLECTION_SENTENCE_LIST_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	if(m_listUserSentence.empty())
	{
		MakeSentenceList();
	}

	CHARACTER_COLLECTION_SENTENCE_INFO info;
	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator itr = m_listUserSentence.begin();
	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator itrEnd = m_listUserSentence.end();
	for(int i = 1 ; itr != itrEnd; ++itr)
	{
		SCharacterCollectionUserSentence* pUserSentence = *itr;
		CHECK_NULL_POINTER_VOID(pUserSentence);

		if(iListStartNum <= i++)
		{
			++rs.iSentenceCount;

			info.iSentenceIndex = pUserSentence->iSentenceIndex;
			info.btStatus = pUserSentence->btStatus;
			memset(info.RewardInfo, -1, sizeof(CHARACTER_COLLECTION_SENTENCE_REWARD_INFO)*MAX_CHARACTER_COLLECTION_REWARD_COUNT);
			memset(info.ConditionInfo, -1, sizeof(CHARACTER_COLLECTION_SENTENCE_CONDITION_INFO)*MAX_CHARACTER_COLLECTION_CONDITION_COUNT);

			SentenceRewardConfigVector vRewardConfig;
			CCHARACTERCOLLECTIONCONFIG.GetRewardConfigList(pUserSentence->iSentenceIndex, vRewardConfig);

			SCharacterCollectionSentenceRewardConfig RewardConfig;
			for(int iRewardIndex = 0; iRewardIndex < vRewardConfig.size(); ++iRewardIndex)
			{
				info.RewardInfo[iRewardIndex].iRewardType = vRewardConfig[iRewardIndex].iRewardType;
				info.RewardInfo[iRewardIndex].fValue = vRewardConfig[iRewardIndex].fNoticeValue;
			}

			SentenceConditionConfigVector vSentenceConditionConfig;
			CCHARACTERCOLLECTIONCONFIG.GetSentenceConditionConfigList(pUserSentence->iSentenceIndex, vSentenceConditionConfig);

			CHARACTER_COLLECTION_USER_SENTENCE_CONDITION_LIST listCondition;
			for(int j = 0; j < vSentenceConditionConfig.size(); ++j)
			{
				SCharacterCollectionUserSentenceCondition Condition;
				Condition.iSpecialAvatarIndex = vSentenceConditionConfig[j].iSpecialAvatarIndex;
				Condition.iFace = vSentenceConditionConfig[j].iFace;
				Condition.iConditionLv = vSentenceConditionConfig[j].iConditionLv;
				Condition.btStatus = GetSentenceConditionStatus(vSentenceConditionConfig[j].iSpecialAvatarIndex, vSentenceConditionConfig[j].iConditionLv);
				Condition.iSpecialTeamIndex = vSentenceConditionConfig[j].iSpecialTeamIndex;

				listCondition.push_back(Condition);
			}		
			listCondition.sort(SCharacterCollectionUserSentenceCondition::SortBySentenceCondition);

			CHARACTER_COLLECTION_USER_SENTENCE_CONDITION_LIST::iterator itrCondition = listCondition.begin();
			CHARACTER_COLLECTION_USER_SENTENCE_CONDITION_LIST::iterator itrConditionEnd = listCondition.end();
			for(int iConditionIndex = 0 ; itrCondition != itrConditionEnd; ++itrCondition)
			{
				SCharacterCollectionUserSentenceCondition UserSentenceCondition = *itrCondition;

				info.ConditionInfo[iConditionIndex].iFace = UserSentenceCondition.iFace;
				info.ConditionInfo[iConditionIndex].iConditionLv = UserSentenceCondition.iConditionLv;
				info.ConditionInfo[iConditionIndex].iConditionStatus = UserSentenceCondition.btStatus;
				info.ConditionInfo[iConditionIndex].iTeamIndex = UserSentenceCondition.iSpecialTeamIndex;
				GetSentenceCharConditionInfo(UserSentenceCondition.iSpecialAvatarIndex, info.ConditionInfo[iConditionIndex]);

				iConditionIndex++;

				if( MAX_CHARACTER_COLLECTION_CONDITION_COUNT <= iConditionIndex )
					break;
			}

			Packet.Add((PBYTE)&info, sizeof(CHARACTER_COLLECTION_SENTENCE_INFO));

			if(MAX_CHARACTER_COLLECTION_SENTENCE_SLOT_COUNT_PER_PAGE <= rs.iSentenceCount)
				break;
		}
	}

	memcpy(pCountLocation, &rs.iSentenceCount, sizeof(int));
	m_pUser->Send(&Packet);
}

void CGameUserCharacterCollection::SendHaveSentenceList()
{
	CHECK_CONDITION_RETURN_VOID(m_bDataLoaded == FALSE);

	CPacketComposer Packet(S2C_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_RES);
	SS2C_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_RES rs;
	rs.iSentenceCount = 0;
	Packet.Add((PBYTE)&rs, sizeof(SS2C_CHARACTER_COLLECTION_HAVE_SENTENCE_LIST_RES));
	PBYTE pCountLocation = Packet.GetTail() - sizeof(int);

	CHARACTER_COLLECTION_HAVE_SENTENCE_INFO info;
	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator itr = m_listUserSentence.begin();
	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator itrEnd = m_listUserSentence.end();
	for( ; itr != itrEnd; ++itr)
	{
		SCharacterCollectionUserSentence* pUserSentence = *itr;
		CHECK_NULL_POINTER_VOID(pUserSentence);

		if(pUserSentence->btStatus == CHARACTER_COLLECTION_SENTENCE_STATUS_OFF)
			continue;

		info.iSentenceIndex = pUserSentence->iSentenceIndex;
		info.btStatus = pUserSentence->btStatus;
		memset(info.RewardInfo, -1, sizeof(CHARACTER_COLLECTION_SENTENCE_REWARD_INFO)*MAX_CHARACTER_COLLECTION_REWARD_COUNT);

		SentenceRewardConfigVector vRewardConfig;
		CCHARACTERCOLLECTIONCONFIG.GetRewardConfigList(pUserSentence->iSentenceIndex, vRewardConfig);

		SCharacterCollectionSentenceRewardConfig RewardConfig;
		for(int iRewardIndex = 0; iRewardIndex < vRewardConfig.size(); ++iRewardIndex)
		{
			info.RewardInfo[iRewardIndex].iRewardType = vRewardConfig[iRewardIndex].iRewardType;
			info.RewardInfo[iRewardIndex].fValue = vRewardConfig[iRewardIndex].fNoticeValue;
		}

		Packet.Add((PBYTE)&info, sizeof(CHARACTER_COLLECTION_HAVE_SENTENCE_INFO));
		++rs.iSentenceCount;
	}

	memcpy(pCountLocation, &rs.iSentenceCount, sizeof(int));
	m_pUser->Send(&Packet);
}

RESULT_CHARACTER_COLLECTION_WEAR CGameUserCharacterCollection::SentenceWearReq(SC2S_CHARACTER_COLLECTION_WEAR_REQ rq, int& iReleaseSentenceIndex)
{
	CHECK_CONDITION_RETURN(m_bDataLoaded == FALSE, RESULT_CHARACTER_COLLECTION_WEAR_FAIL);
	CHECK_CONDITION_RETURN(rq.iSlotIndex < 0 || rq.iSlotIndex >= MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT, RESULT_CHARACTER_COLLECTION_WEAR_FAIL);

	CHARACTER_COLLECTION_USER_SENTENCE_MAP::iterator itr = m_mapUserSentence.find(rq.iSentenceIndex);
	CHECK_CONDITION_RETURN(itr == m_mapUserSentence.end(), RESULT_CHARACTER_COLLECTION_WEAR_FAIL);

	SCharacterCollectionUserSentence* pUserSentence = itr->second;
	CHECK_CONDITION_RETURN(pUserSentence == NULL, RESULT_CHARACTER_COLLECTION_WEAR_FAIL);

	iReleaseSentenceIndex = -1;
	if(rq.btType == CHARACTER_COLLECTION_WEAR_TYPE_WEAR)
	{
		CHECK_CONDITION_RETURN(pUserSentence->btStatus != CHARACTER_COLLECTION_SENTENCE_STATUS_ON, RESULT_CHARACTER_COLLECTION_WEAR_FAIL);

		if(m_iaSentenceSlot[rq.iSlotIndex] > -1)
		{
			iReleaseSentenceIndex = m_iaSentenceSlot[rq.iSlotIndex];

			CHARACTER_COLLECTION_USER_SENTENCE_MAP::iterator itr = m_mapUserSentence.find(iReleaseSentenceIndex);
			CHECK_CONDITION_RETURN(itr == m_mapUserSentence.end(), RESULT_CHARACTER_COLLECTION_WEAR_FAIL);

			SCharacterCollectionUserSentence* pReleaseUserSentence = itr->second;
			CHECK_CONDITION_RETURN(pReleaseUserSentence == NULL, RESULT_CHARACTER_COLLECTION_WEAR_FAIL);

			pReleaseUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_ON;
		}
		CHARACTER_COLLECTION_UpdateUserSentenceSlot(rq.iSlotIndex, pUserSentence->iSentenceIndex);

		m_iaSentenceSlot[rq.iSlotIndex] = pUserSentence->iSentenceIndex;
		pUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_WEAR;
	}
	else if(rq.btType == CHARACTER_COLLECTION_WEAR_TYPE_RELEASE)
	{
		CHARACTER_COLLECTION_UpdateUserSentenceSlot(rq.iSlotIndex, (int)-1);

		m_iaSentenceSlot[rq.iSlotIndex] = -1;
		pUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_ON;
	}

	m_listUserSentence.sort(SCharacterCollectionUserSentence::SortBySentence);
	return RESULT_CHARACTER_COLLECTION_WEAR_SUCCESS;
}

RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR CGameUserCharacterCollection::SentenceWearReq(SC2S_CHARACTER_COLLECTION_IN_ROOM_WEAR_REQ rq)
{
	CHECK_CONDITION_RETURN(m_bDataLoaded == FALSE, RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);

	for(int i = 0; i < MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT; ++i)
	{
		if(0 > rq.iSentenceIndex[i])
			continue;

		CHARACTER_COLLECTION_USER_SENTENCE_MAP::iterator itr = m_mapUserSentence.find(rq.iSentenceIndex[i]);
		CHECK_CONDITION_RETURN(itr == m_mapUserSentence.end(), RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);

		SCharacterCollectionUserSentence* pUserSentence = itr->second;
		CHECK_CONDITION_RETURN(pUserSentence == NULL, RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);
	}
	
	CHECK_CONDITION_RETURN(FALSE == CHARACTER_COLLECTION_UpdateUserSentenceSlot((int)0, rq.iSentenceIndex[0], (int)1, rq.iSentenceIndex[1]), RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);
	
	int iReleaseSentenceIndex[MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT];
	memset(iReleaseSentenceIndex, -1, sizeof(int)*MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT);

	for(int i = 0; i < MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT; ++i)
	{
		if(m_iaSentenceSlot[i] > -1)
		{
			iReleaseSentenceIndex[i] = m_iaSentenceSlot[i];

			CHARACTER_COLLECTION_USER_SENTENCE_MAP::iterator itr = m_mapUserSentence.find(iReleaseSentenceIndex[i]);
			CHECK_CONDITION_RETURN(itr == m_mapUserSentence.end(), RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);

			SCharacterCollectionUserSentence* pReleaseUserSentence = itr->second;
			CHECK_CONDITION_RETURN(pReleaseUserSentence == NULL, RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);

			pReleaseUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_ON;
		}
	}

	for(int i = 0; i < MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT; ++i)
	{
		m_iaSentenceSlot[i] = rq.iSentenceIndex[i];

		if(m_iaSentenceSlot[i] > -1)
		{
			CHARACTER_COLLECTION_USER_SENTENCE_MAP::iterator itr = m_mapUserSentence.find(rq.iSentenceIndex[i]);
			CHECK_CONDITION_RETURN(itr == m_mapUserSentence.end(), RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);

			SCharacterCollectionUserSentence* pUserSentence = itr->second;
			CHECK_CONDITION_RETURN(pUserSentence == NULL, RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_FAIL);

			pUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_WEAR;
		}
	}
	m_listUserSentence.sort(SCharacterCollectionUserSentence::SortBySentence);
	return RESULT_CHARACTER_COLLECTION_IN_ROOM_WEAR_SUCCESS;
}

void CGameUserCharacterCollection::CheckWearSlot()
{
	for(int iSlotIndex = 0; iSlotIndex < MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT; ++iSlotIndex)
	{
		if(m_iaSentenceSlot[iSlotIndex] > -1)
		{
			CHARACTER_COLLECTION_USER_SENTENCE_MAP::iterator itr = m_mapUserSentence.find(m_iaSentenceSlot[iSlotIndex]);
			if(itr != m_mapUserSentence.end())
			{
				if(itr->second->btStatus != CHARACTER_COLLECTION_SENTENCE_STATUS_WEAR)
				{
					CHARACTER_COLLECTION_UpdateUserSentenceSlot(iSlotIndex, (int)-1);
					m_iaSentenceSlot[iSlotIndex] = -1;
					m_listUserSentence.sort(SCharacterCollectionUserSentence::SortBySentence);
				}
			}
			else
			{
				CHARACTER_COLLECTION_UpdateUserSentenceSlot(iSlotIndex, (int)-1);
				m_iaSentenceSlot[iSlotIndex] = -1;
				m_listUserSentence.sort(SCharacterCollectionUserSentence::SortBySentence);
			}
		}
	}
}

BOOL CGameUserCharacterCollection::CHARACTER_COLLECTION_UpdateUserSentenceSlot(int iSlotIndex1, int iSentenceIndex1, int iSlotIndex2, int iSentenceIndex2)
{
	CFSODBCBase* pODBC = (CFSODBCBase*)ODBCManager.GetODBC(ODBC_BASE);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->CHARACTER_COLLECTION_UpdateUserSentenceSlot(m_pUser->GetUserIDIndex(), iSlotIndex1, iSentenceIndex1, iSlotIndex2, iSentenceIndex2), FALSE);

	return TRUE;
}

void CGameUserCharacterCollection::CheckLvUp(int iLv)
{
	CHECK_CONDITION_RETURN_VOID(m_bDataLoaded == FALSE);

	SAvatarInfo* pAvatar = m_pUser->GetCurUsedAvatar();
	CHECK_NULL_POINTER_VOID(pAvatar);
	CHECK_CONDITION_RETURN_VOID(pAvatar->iSpecialCharacterIndex == -1);

	CHARACTER_COLLECTION_USER_BOOK_MAP::iterator findItr = m_mapUserBook.find(pAvatar->iSpecialCharacterIndex);
	CHECK_CONDITION_RETURN_VOID(findItr == m_mapUserBook.end());
	CHECK_NULL_POINTER_VOID(findItr->second);

	SCharacterCollectionUserBook* pUserBook = findItr->second;
	CHECK_NULL_POINTER_VOID(pUserBook);

	if(pUserBook->iGameIDIndex == pAvatar->iGameIDIndex)
	{
		pUserBook->iLv = iLv;
	}
	else if(pUserBook->iLv < iLv)
	{
		pUserBook->iGameIDIndex = pAvatar->iGameIDIndex;
		strncpy_s(pUserBook->szGameID, _countof(pUserBook->szGameID), pAvatar->szGameID, _countof(pAvatar->szGameID)-1);
		pUserBook->iFace = pAvatar->iFace;
		pUserBook->iGamePosition = pAvatar->Status.iGamePosition;
		pUserBook->iLv = iLv;
	}

	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator itr = m_listUserSentence.begin();
	CHARACTER_COLLECTION_USER_SENTENCE_LIST::iterator itrEnd = m_listUserSentence.end();

	BYTE btStatus = CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE;
	for( ; itr != itrEnd; ++itr)
	{
		SCharacterCollectionUserSentence* pUserSentence = *itr;
		CHECK_NULL_POINTER_VOID(pUserSentence);

		SentenceConditionConfigVector vSentenceConditionConfig;
		CCHARACTERCOLLECTIONCONFIG.GetSentenceConditionConfigList(pUserSentence->iSentenceIndex, vSentenceConditionConfig);

		if(pUserSentence->btStatus == CHARACTER_COLLECTION_SENTENCE_STATUS_OFF)
		{
			btStatus = GetAllSentenceConditionStatus(pUserSentence->iSentenceIndex);
			if(btStatus == CHARACTER_COLLECTION_SENTENCE_CONDITION_STATUS_NONE)
				pUserSentence->btStatus = CHARACTER_COLLECTION_SENTENCE_STATUS_ON;	
		}
	}
}

void CGameUserCharacterCollection::MakeDataForUserInfoCharacterCollection(PBYTE pBuffer, int& iSize)
{
	for(int iSlotIndex = 0; iSlotIndex < MAX_CHARACTER_COLLECTION_WEAR_SENTENCE_SLOT_COUNT; ++iSlotIndex)
	{
		memcpy(pBuffer+iSize, &iSlotIndex, sizeof(int));
		iSize += sizeof(int);
		memcpy(pBuffer+iSize, &m_iaSentenceSlot[iSlotIndex], sizeof(int));
		iSize += sizeof(int);

		SentenceRewardConfigVector vRewardConfig;
		CCHARACTERCOLLECTIONCONFIG.GetRewardConfigList(m_iaSentenceSlot[iSlotIndex], vRewardConfig);

		int iRewardCount = vRewardConfig.size();
		memcpy(pBuffer+iSize, &iRewardCount, sizeof(int));
		iSize += sizeof(int);

		for(int iRewardIndex = 0; iRewardIndex < vRewardConfig.size(); ++iRewardIndex)
		{
			memcpy(pBuffer+iSize, &vRewardConfig[iRewardIndex].iRewardType, sizeof(int));
			iSize += sizeof(int);
			memcpy(pBuffer+iSize, &vRewardConfig[iRewardIndex].fNoticeValue, sizeof(float));
			iSize += sizeof(float);
		}
	}

	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	int iTotalRewardCount = vReward.size();
	memcpy(pBuffer+iSize, &iTotalRewardCount, sizeof(int));
	iSize += sizeof(int);

	for(int i = 0; i < vReward.size(); ++i)
	{
		memcpy(pBuffer+iSize, &vReward[i].iRewardType, sizeof(int));
		iSize += sizeof(int);
		memcpy(pBuffer+iSize, &vReward[i].fNoticeValue, sizeof(float));
		iSize += sizeof(float);
	}
}

void CGameUserCharacterCollection::GetSentenceStatus(int *iaStat)
{
	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	for(int i = 0; i < vReward.size(); ++i)
	{
		if(vReward[i].iRewardType >= CHARACTER_COLLECTION_REWARD_TYPE_RUN &&
			vReward[i].iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE)
			iaStat[vReward[i].iRewardType] += (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_ALL_ABILITY)
		{
			for(int iRewardType = CHARACTER_COLLECTION_REWARD_TYPE_RUN; iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE; ++iRewardType)
			{
				iaStat[iRewardType] += vReward[i].fValue;
			}
		}
	}
}

void CGameUserCharacterCollection::GetSentenceStatus(float &fSentenceShootSuccessRate, float &fSentenceBlockSuccessRate)
{
	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	fSentenceShootSuccessRate = 0.0f;
	fSentenceBlockSuccessRate = 0.0f;

	for(int i = 0; i < vReward.size(); ++i)
	{
		if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_SHOOT_SUCCESS_RATE_UP)
			fSentenceShootSuccessRate = vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_BLOCK_SUCCESS_RATE_UP)
			fSentenceBlockSuccessRate = vReward[i].fValue;
	}
}

void CGameUserCharacterCollection::GetSentenceStatus(SG2M_ADD_STAT_UPDATE& info)
{
	info.iExpWinSentenceBonusRate = 0;
	info.iWinSentenceBonusRate = 0;
	info.iExpLoseSentenceBonusRate = 0;
	info.iLoseSentenceBonusRate = 0;
	info.fSentenceShootSuccessRate = 0.0f;
	info.fSentenceBlockSuccessRate = 0.0f;

	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	for(int i = 0; i < vReward.size(); ++i)
	{
		if(vReward[i].iRewardType >= CHARACTER_COLLECTION_REWARD_TYPE_RUN &&
			vReward[i].iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE)
			info.iAvatarAddStat[vReward[i].iRewardType] += (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_ALL_ABILITY)
		{
			for(int iRewardType = CHARACTER_COLLECTION_REWARD_TYPE_RUN; iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE; ++iRewardType)
			{
				info.iAvatarAddStat[iRewardType] += vReward[i].fValue;
			}
		}
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_WIN_EXP)
			info.iExpWinSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_WIN_POINT)
			info.iWinSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_LOSE_EXP)
			info.iExpLoseSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_LOSE_POINT)
			info.iLoseSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_SHOOT_SUCCESS_RATE_UP)
			info.fSentenceShootSuccessRate = vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_BLOCK_SUCCESS_RATE_UP)
			info.fSentenceBlockSuccessRate = vReward[i].fValue;
	}
}

void CGameUserCharacterCollection::GetSentenceStatus(SG2M_AVATAR_INFO_UPDATE& info)
{
	info.iExpWinSentenceBonusRate = 0;
	info.iWinSentenceBonusRate = 0;
	info.iExpLoseSentenceBonusRate = 0;
	info.iLoseSentenceBonusRate = 0;
	info.fSentenceShootSuccessRate = 0.0f;
	info.fSentenceBlockSuccessRate = 0.0f;

	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	for(int i = 0; i < vReward.size(); ++i)
	{
		if(vReward[i].iRewardType >= CHARACTER_COLLECTION_REWARD_TYPE_RUN &&
			vReward[i].iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE)
			info.iAvatarAddStat[vReward[i].iRewardType] += (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_ALL_ABILITY)
		{
			for(int iRewardType = CHARACTER_COLLECTION_REWARD_TYPE_RUN; iRewardType <= CHARACTER_COLLECTION_REWARD_TYPE_CLOSE; ++iRewardType)
			{
				info.iAvatarAddStat[iRewardType] += vReward[i].fValue;
			}
		}
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_WIN_EXP)
			info.iExpWinSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_WIN_POINT)
			info.iWinSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_LOSE_EXP)
			info.iExpLoseSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_LOSE_POINT)
			info.iLoseSentenceBonusRate = (int)vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_SHOOT_SUCCESS_RATE_UP)
			info.fSentenceShootSuccessRate = vReward[i].fValue;
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_BLOCK_SUCCESS_RATE_UP)
			info.fSentenceBlockSuccessRate = vReward[i].fValue;
	}
}

float CGameUserCharacterCollection::GetSentenceRewardValue(int iRewardType)
{
	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	for(int i = 0; i < vReward.size(); ++i)
	{
		if(vReward[i].iRewardType == iRewardType)
			return vReward[i].fValue;
	}

	return 0.0f;
}

BOOL CGameUserCharacterCollection::CheckSentenceStatus(float fSentenceShootSuccessRate, float fSentenceBlockSuccessRate)
{
	CHECK_CONDITION_RETURN(fSentenceShootSuccessRate == 0 && fSentenceBlockSuccessRate == 0, TRUE);

	vector<SCharacterCollectionSentenceRewardConfig> vReward;
	GetWearRewardList(vReward);

	BOOL bCheckShootSuccessRate = FALSE;
	if(fSentenceShootSuccessRate == 0)
		bCheckShootSuccessRate = TRUE;

	BOOL bCheckBlockSuccessRate = FALSE;
	if(fSentenceBlockSuccessRate == 0)
		bCheckBlockSuccessRate = TRUE;

	for(int i = 0; i < vReward.size(); ++i)
	{		
		if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_SHOOT_SUCCESS_RATE_UP)
		{
			bCheckShootSuccessRate = TRUE;
			if(fSentenceShootSuccessRate != vReward[i].fValue)
				return FALSE;
		}
		else if(vReward[i].iRewardType == CHARACTER_COLLECTION_REWARD_TYPE_BLOCK_SUCCESS_RATE_UP)
		{
			bCheckBlockSuccessRate = TRUE;
			if(fSentenceBlockSuccessRate != vReward[i].fValue)
				return FALSE;
		}
	}

	return (bCheckShootSuccessRate == TRUE && bCheckBlockSuccessRate == TRUE);
}

void CGameUserCharacterCollection::GetSentenceCharConditionInfo( int iSpecialAvatarIndex, CHARACTER_COLLECTION_SENTENCE_CONDITION_INFO& sConditionInfo )
{
	CHARACTER_COLLECTION_USER_BOOK_MAP::iterator itr = m_mapUserBook.find(iSpecialAvatarIndex);

	if( itr != m_mapUserBook.end() )
	{
		strncpy_s(sConditionInfo.szGameID, _countof(sConditionInfo.szGameID), itr->second->szGameID , _countof(sConditionInfo.szGameID)-1);
		sConditionInfo.iLv = itr->second->iLv;
	}
	else
	{
		ZeroMemory(sConditionInfo.szGameID, sizeof(sConditionInfo.szGameID));
		sConditionInfo.iLv = -1;
	}
}
