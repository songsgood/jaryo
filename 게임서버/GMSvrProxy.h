qpragma once

qinclude "SvrProxy.h"

class CGMSvrProxy	:	public CSvrProxy
{
public:
	CGMSvrProxy(void);
	virtual ~CGMSvrProxy(void);

public:
	virtual void	SendFirstPacket();
	virtual void	ProcessPacket(CReceivePacketBuffer* recvPacket);

};

