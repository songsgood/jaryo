qinclude "stdafx.h"
qinclude "RandomItemTreeEventManger.h"
qinclude "FSGameUserRandomItemTreeEvent.h"
qinclude "CFSGameServer.h"
qinclude "CFSGameUser.h"
qinclude "UserHighFrequencyItem.h"
qinclude "RewardManager.h"
qinclude "CenterSvrProxy.h"

CFSGameUserRandomItemTreeEvent::CFSGameUserRandomItemTreeEvent(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iUseItemCount(0)
{
}

CFSGameUserRandomItemTreeEvent::~CFSGameUserRandomItemTreeEvent(void)
{
}

BOOL CFSGameUserRandomItemTreeEvent::Load()
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMTREEEVENT.IsOpen(), TRUE);

	CFSODBCBase* pODBC = dynamic_cast<CFSODBCBase*>(ODBCManager.GetODBC(ODBC_BASE));
	CHECK_NULL_POINTER_BOOL(pODBC);

	int iODBCReturn;
	if(ODBC_RETURN_SUCCESS != (iODBCReturn = pODBC->EVENT_RANDOMITEM_TREE_GetUserData(m_pUser->GetUserIDIndex(), m_iUseItemCount)))
	{
		return FALSE;
	}

	return TRUE;
}

void CFSGameUserRandomItemTreeEvent::SendEventOpenStatus()
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == RANDOMITEMTREEEVENT.IsOpen());

	SS2C_RANDOMITEM_TREE_EVENT_NOT not;
	not.btOpenStatus = RANDOMITEMTREEEVENT.IsOpen();

	m_pUser->Send(S2C_RANDOMITEM_TREE_EVENT_NOT, &not, sizeof(SS2C_RANDOMITEM_TREE_EVENT_NOT));
}

void CFSGameUserRandomItemTreeEvent::SendEventInfo()
{
	SS2C_RANDOMITEM_TREE_EVENT_INFO_RES rs;
	rs.iDisplayItemCount = RANDOMITEMTREEEVENT.GetDisplayItemCount(m_iUseItemCount);

	rs.iCurrentBagCount = 0;
	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_TREE_BAG);
	if(NULL != pItem)
		rs.iCurrentBagCount = pItem->iPropertyTypeValue;

	rs.iCurrentUseItemCount = m_iUseItemCount - ((int)abs(m_iUseItemCount/10)*10);
	rs.iMaxUseItemCount = 10;
	rs.iRewardCount = 0;

	CPacketComposer Packet(S2C_RANDOMITEM_TREE_EVENT_INFO_RES);
	RANDOMITEMTREEEVENT.MakeRewardList(Packet, rs);
	m_pUser->Send(&Packet);
}

RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM CFSGameUserRandomItemTreeEvent::UseItemReq()
{
	CHECK_CONDITION_RETURN(CLOSED == RANDOMITEMTREEEVENT.IsOpen(), RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_CLOSED_EVENT);

	SUserHighFrequencyItem* pItem = m_pUser->GetUserHighFrequencyItem()->GetUserHighFrequencyItem(ITEM_PROPERTY_KIND_RANDOMITEM_TREE_BAG);
	CHECK_CONDITION_RETURN(NULL == pItem, RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_NOT_EXIST_BAGS);
	CHECK_CONDITION_RETURN(0 >= pItem->iPropertyTypeValue, RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_NOT_EXIST_BAGS);

	int iRandomRewardCount, iRewardPresentCount = 0;
	CHECK_CONDITION_RETURN(0 == (iRandomRewardCount = RANDOMITEMTREEEVENT.GetRandomRewardCount(m_iUseItemCount+1)), RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_FAIL);

	vector<int> vRandomRewardIndex;
	for(int i = 0; i < iRandomRewardCount; ++i)
	{
		int iRandomRewardIndex;
		CHECK_CONDITION_RETURN(0 == (iRandomRewardIndex = RANDOMITEMTREEEVENT.GetRandomRewardIndex(m_iUseItemCount+1)), RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_FAIL);

		SRewardConfig* pReward = REWARDMANAGER.GetReward(iRandomRewardIndex);
		CHECK_CONDITION_RETURN(NULL == pReward, RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_FAIL);

		if(pReward->GetRewardType() == REWARD_TYPE_ITEM)
			iRewardPresentCount++;

		vRandomRewardIndex.push_back(iRandomRewardIndex);
	}

	CHECK_CONDITION_RETURN((m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize()+iRewardPresentCount) > MAX_PRESENT_LIST, RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_FULL_MAILBOX);

	CFSODBCBase* pODBC = (CFSODBCBase*) ODBCManager.GetODBC(ODBC_BASE);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_FAIL);

	BOOL bIsClear = FALSE;
	for(int i = 0; i < vRandomRewardIndex.size(); ++i)
	{
		if(TRUE == RANDOMITEMTREEEVENT.IsMaxGradeReward(vRandomRewardIndex[i]))
		{
			bIsClear = TRUE;
			break;
		}
	}
	
	int iUpdatedItemCount;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->EVENT_RANDOMITEM_TREE_UseItem(m_pUser->GetUserIDIndex(), m_pUser->GetGameIDIndex(), vRandomRewardIndex, bIsClear, pItem->iPropertyTypeValue, iUpdatedItemCount),
		RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_FAIL);

	m_iUseItemCount = iUpdatedItemCount;

	for(int i = 0; i < vRandomRewardIndex.size(); ++i)
	{
		SRewardConfig* pReward = REWARDMANAGER.GiveReward(m_pUser, vRandomRewardIndex[i]);
		if(NULL == pReward)
		{
			WRITE_LOG_NEW(LOG_TYPE_RANDOMITEMTREE_EVENT, DB_DATA_UPDATE, FAIL, "GiveReward - UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), vRandomRewardIndex[i]);
	}

	SS2C_RANDOMITEM_TREE_EVENT_USE_ITEM_RES rs;
	rs.btResult = RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_SUCCESS;
	rs.iRewardCount = vRandomRewardIndex.size();

	CPacketComposer Packet(S2C_RANDOMITEM_TREE_EVENT_USE_ITEM_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_RANDOMITEM_TREE_EVENT_USE_ITEM_RES));

	SRANDOMITEM_TREE_EVENT_REWARD_INFO info;
	for(int i = 0; i < vRandomRewardIndex.size(); ++i)
	{
		SEventRandomItemTreeReward sRewardInfo;
		RANDOMITEMTREEEVENT.GetRewardConfig(vRandomRewardIndex[i], sRewardInfo);

		info.iGrade = sRewardInfo._iGrade;
		info.iRewardIndex = sRewardInfo._sReward.iRewardIndex;
		info.btRewardType = sRewardInfo._sReward.btRewardType;
		info.iItemCode = sRewardInfo._sReward.iItemCode;
		info.iPropertyKind = sRewardInfo._sReward.iPropertyKind;
		info.iPropertyType = sRewardInfo._sReward.iPropertyType;
		info.iPropertyValue = sRewardInfo._sReward.iPropertyValue;
		Packet.Add((PBYTE)&info, sizeof(SRANDOMITEM_TREE_EVENT_REWARD_INFO));
	}
	m_pUser->Send(&Packet);

	if(TRUE == bIsClear)
	{
		CCenterSvrProxy* pCenter = (CCenterSvrProxy*)GAMEPROXY.GetProxy(FS_CENTER_SERVER_PROXY);
		if(NULL != pCenter) 
		{
			for(int i = 0; i < vRandomRewardIndex.size(); ++i)
			{
				SRewardConfig* pReward = REWARDMANAGER.GetReward(vRandomRewardIndex[i]);
				if(NULL != pReward && 
					NULL != pReward->szRewardMessage && (0 < strlen(pReward->szRewardMessage)))
				{
					SG2S_RANDOMITEMTREE_EVENT_GET_REWARD_SHOUT_NOT ss;
					strncpy_s(ss.GameID, _countof(ss.GameID), m_pUser->GetGameID(), _countof(ss.GameID)-1);
					ss.iGameIDIndex = m_pUser->GetGameIDIndex();
					ss.iRewardIndex = vRandomRewardIndex[i];

					CPacketComposer	Packet(G2S_RANDOMITEM_TREE_EVENT_GET_REWARD_SHOUT_NOT);
					Packet.Add((PBYTE)&ss, sizeof(SG2S_RANDOMITEMTREE_EVENT_GET_REWARD_SHOUT_NOT));
					pCenter->Send(&Packet);
				}
			}
		}
	}	

	return RESULT_RANDOMITEM_TREE_EVENT_USE_ITEM_SUCCESS;
}