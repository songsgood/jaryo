qpragma once

qinclude "Lock.h"

class CFSODBCBase;
class CFSGameUser;
class CEVENT_FriendInviteKingUserInfo
{
	friend class CFSGameUserFriendInviteList;

private:
	SFRIENDINVITE_KING_INFO		m_MyInfo;
	BOOL						m_bLoad;
	CFSGameUser*				m_pUser;

public:
	BOOL LoadFriendInviteKingInfo();
	void UpdateFriendInviteKingExp(IN int iFriendUserIDIndex, IN int iExp , IN char* szGameID);
	void UpdateFriendInviteKingExp(IN int iExp);
public:
	inline int GetTotalExp(void) {
		return m_MyInfo.iTotalExp;
	}

public:
	CEVENT_FriendInviteKingUserInfo(CFSGameUser* pUser);
	~CEVENT_FriendInviteKingUserInfo(void);
};

class CFSGameUserFriendInviteList
{
public:
	BOOL LoadFriendInviteMyInfo(BOOL bLastConnectTimeNotice = FALSE);
	BOOL LoadFriendList();
	BOOL LoadFriendMissionList();
	BOOL LoadMyMissionList();

	void SendFriendInviteRegisterButtonNot();
	void SendFriendInviteMyInfo();

	void SendFriendMissionInfo(const SC2S_FRIEND_INVITE_MISSION_INFO_REQ& rq);
	void UpdateFriendComment(const SC2S_FRIEND_INVITE_UPDATE_COMMENT_REQ& rq);
	void GiveFriendInviteReward(const SC2S_FRIEND_INVITE_REWARD_RECEIVING_REQ& rq);
	void UpdateFriendStatus(const SC2S_FRIEND_INVITE_UPDATE_FRIEND_STATUS_REQ& rq);
	void UpdateFriendStatus(BYTE btIndex, int iStatus);
	void SendFriendInviteRewardList(void);
	void SendMyMissionInfo(void);
	int DeleteFriend(const SFRIENDINVITE_UpdateFriendStatus& sUpdate, IN const BYTE& btIndex);
	void SendFriendInviteMessageNot(const SS2C_FRIEND_INVITE_MESSAGE_NOT& not);
	void SendFriendInviteMessageNot(FRIEND_INVITE_MESSAGE_INDEX index, char* szGameID = nullptr);
	void AddInviteFriend(const SC2S_FRIEND_INVITE_ADD_FRIEND_REQ& rq);
	void CheckAndSendFriendInviteCode(const SC2S_FRIEND_INVITE_CHECK_INVITE_CODE_REQ& rq);
	int GetFriendUserIDIndex(void);
	void CheckAndGiveFriendBenefit(IN int iType);
	void UpdateFriendInviteMission(IN int iMissionIndex, int iValue = 1);
	void UpdateAllMissionUpdate(CFSODBCBase* pODBCBase = nullptr);
	void UpdateFriendInviteKingExp(int iExp);
	void CheckAndSendDeleteStatus();
	void CheckAndGiveBenefit();

public:
	// center svr receive
	void RefusalUser(IN int iRefusalUserIDIndex,IN int iUserIDIndex);
	void UpdateInviteStatus(IN int iUserIDIndex, IN int iFriendUserIDIndex, IN int iStatus, IN char* szGameID); 
	void UpdateFriendMission(IN const SS2G_FRIEND_INVITE_UPDATE_FRIEND_MISSION_RES& rs);
	void UpdateFriendListStatus(IN const SS2G_FRIEND_INVITE_UPDATE_FRIENDLIST_STATUS_RES& rs);
	void UpdateGaveBenefitReward(IN const SS2G_FRIEND_INVITE_GIVE_BENEFIT_RES& rs);
	void UpdateFriendInviteKingExp(IN const SS2G_FRIEND_KING_EVENT_UPDATE_EXP_RES& rs);
	void UpdateFriendInviteAddFriend(IN const SS2G_FRIEND_INVITE_ADD_FRIEND_RES& rs);
	void UpdateFriendInvteRemoveFriendList(IN const SS2G_FRIEND_INVITE_REMOVE_FRIENDLIST_RES& rs);
	void UpdateLastConnectTimeFriend(IN const SS2G_FRIEND_INVITE_LASTCONNECT_RES& rs);
	void UpdateMissionSuccessFriend(IN const SS2G_FRIEND_INVITE_MISSION_SUCCESS_RES& rs);
public:
	// center svr send
	void SendCenterSvrUpdateInviteStatus(const SFRIENDINVITE_UpdateFriendStatus& sUpdate);
	void SendCenterSvrUpdateFriendListStatus(const SFRIENDINVITE_UpdateFriendStatus& sUpdate);
	void SendCenterSvrRefusal(int iUserIDIndex, int iRefusalUserIDIndex);
	void SendCenterSvrFriendMission();
	void SendCenterSvrGaveBenefitReward(int iType, int iPresentIndex, int iUserIDIndex);
	void SendCenterSvrAddFriend(const SFRIENDINVITE_AddFriend& sAdd);
	void SendCenterSvrRemoveFriendList(const SFRIENDINVITE_AddFriend& sAdd);
	void SendCenterSvrLastConnectTimeNoticeForFriend(BOOL bLogin = FALSE);
	void SendCenterSvrMissionSuccessFriend();
	void SendCenterSvrMyFriendListMissionReq();

public:
	inline CEVENT_FriendInviteKingUserInfo* GetEventInviteKing() { return &m_EventInviteKingInfo; }
	

private:
	void UpdateGaveReward(const SFRIENDINVITE_GiveReward& give, IN const BYTE& btFriendIndex);

private:
	CFSGameUser*			m_pUser;
	SFRIENDINVITE_MyInfo	m_myInfo;
	SFRIENDINVITE_Mission	m_myMission;

	volatile LONG 	m_lBool[MAX_FRIEND_INVITE_UPDATE];

	inline BOOL SetUserUpdate(int index)	{	return ( 0 == ::InterlockedCompareExchange(&m_lBool[index], 1, 0));	}
	inline BOOL UnSetUserUpdate(int index)	{	return ( 1 == ::InterlockedCompareExchange(&m_lBool[index], 0, 1));	}
	inline BOOL GetUserUpdate(int index)	{	return ( 0 != m_lBool[index] );	}

private:
	LOCK_RESOURCE(m_csFriendlist);
	map<BYTE, SFRIENDINVITE_Friend> m_mapFriend;

	LOCK_RESOURCE(m_csFriendMission);
	map<int/*useridindex*/, SFRIENDINVITE_Mission> m_mapFriendMission;

	list<SFRIEND_INVITE_UserBenefit>	m_listBenefit; // 예약된 아이템

	CEVENT_FriendInviteKingUserInfo		m_EventInviteKingInfo;
	volatile LONG 	m_lMissionSave;
	inline BOOL SetMissionSave(void) { return (0==::InterlockedCompareExchange(&m_lMissionSave, 1, 0)); }
	inline BOOL UnSetMissionSave(void) { return (1==::InterlockedCompareExchange(&m_lMissionSave, 0, 1)); }
	inline BOOL GetMissionSave(void)  { return ( 0 != m_lMissionSave); }

	volatile LONG  m_lMissionUpdateCount;
	inline void IncreaseMyMissionUpdateCount()
	{
		InterlockedIncrement(&m_lMissionUpdateCount);
	}
	inline void DecreaseMyMissionUpdateCount()
	{
		InterlockedDecrement(&m_lMissionUpdateCount);
	}
	inline int GetMyMissionUpdateCount() const { return static_cast<int>(m_lMissionUpdateCount); }

protected:
	enum FRIEND_INVITE_FRIENDCOUNT_INDEX : int
	{
		FIFI_IN = 0,
		FIFI_NOT_IN = 1,
	};

	inline int GetFriendCount(IN const BYTE& btStatus, IN const FRIEND_INVITE_FRIENDCOUNT_INDEX& btCountIndex)
	{
		int iCount = 0;
		CLock lock(&m_csFriendlist);

		map<BYTE, SFRIENDINVITE_Friend>::iterator iter = m_mapFriend.begin();

		while(iter != m_mapFriend.end())
		{
			if(FIFI_IN == btCountIndex)
			{
				if(btStatus == iter->second.btStatus) 
					++iCount;		
			}
			else if(FIFI_NOT_IN == btCountIndex)
			{
				if(btStatus != iter->second.btStatus) 
					++iCount;
			}

			++iter;
		}

		return iCount;
	}

public:
	CFSGameUserFriendInviteList(CFSGameUser* pUser);
	~CFSGameUserFriendInviteList(void);
};
