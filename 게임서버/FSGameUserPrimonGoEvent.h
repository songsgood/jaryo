qpragma once

class CFSGameUser;
class CFSODBCBase;

class CFSGameUserPrimonGoEvent
{
public:
	CFSGameUserPrimonGoEvent(CFSGameUser* pUser);
	~CFSGameUserPrimonGoEvent(void);

	BOOL Load();
	void SetUserSpeicalRewardIndex(vector<int>& vRewardIndex);

	void SendEventInfo();
	RESULT_EVENT_PRIMONGO_OPEN_PRIMONBAL OpenPrimonBallReq(BYTE& btRewardType, SREWARD_INFO& sReward);
	RESULT_EVENT_PRIMONGO_GET_FREE_REWARD GetFreeRewardReq();
	RESULT_EVENT_PRIMONGO_INITIALIZE_TRY_TYPE InitializeTryTypeReq();

private:
	BYTE m_btTryType;	// SEVENT_PRIMONGO_TRY_TYPE
	BYTE m_btFreeRewardStatus;	// SEVENT_PRIMONGO_FREE_REWARD_STATUS
	int m_iSpeicalRewardIndex[MAX_PRIMONGO_SPECIAL_ITEM_COUNT];
	int m_iSpeicalRewardCount;
	BYTE m_btRateType;

	CFSGameUser* m_pUser;
	BOOL m_bDataLoaded;

};

