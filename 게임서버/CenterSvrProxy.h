// CenterSvrProxy.h: interface for the CCenterSvrProxy class.
//
// Center Server와의 통신 클래스
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CENTERSVRPROXY_H__1C8936C9_A072_43D0_8A40_257A8024DDBA__INCLUDED_)
#define AFX_CENTERSVRPROXY_H__1C8936C9_A072_43D0_8A40_257A8024DDBA__INCLUDED_

#if _MSC_VER > 1000
qpragma once
#endif // _MSC_VER > 1000

qinclude "svrproxy.h"
qinclude "ThreadODBCManager.h"

#ifndef DECLARE_CENTERPROXY_PROC_FUNC(x)
#define DECLARE_CENTERPROXY_PROC_FUNC(x) void Proc_##x(CReceivePacketBuffer*);
#endif

class CCenterSvrProxy : public CSvrProxy
{	
public:
	CCenterSvrProxy();
	virtual ~CCenterSvrProxy();

	void			SendFirstPacket();

	BOOL			SendMsgToAllFriend(char* gameID, BYTE* msg, int msglen);
	BOOL			SendMsgToFriend(char* gameID, BYTE* msg, int msglen, char* recver);
	BOOL			SendUserStatusUpdate(char* gameID, int status, int iLocation);
	BOOL			SendAddFriend(int iGameIDIndex, char* gameID, char* friendID, int OpCode, int res, int position, int iLevel, int iFameLevel, int iEquippedAchievementTitle );
	BOOL			SendDelFriend(int iGameIDIndex, char* gameID, char* friendID);
	BOOL			SendUserNotify(char* recver, int index, int iMailType);
	BOOL			SendUserSInfo(char* gameID, char* recvID, int type, int iRecordType, int iSeason, int iLinkItemAddStat[MAX_STAT_NUM], int iCoachCardProductIndex = 0, int iCoachCardFront = 0);
	BOOL			SendWhisper(char* gameID, char* recvID, char* szText);
	BOOL			SendAddInterestUserReq(int iGameIDIndex, char* pGameID, char* pInterestID );
	BOOL			SendAddBannedUserReq(int iGameIDIndex, char* pGameID, char* pBannedID );
	BOOL			SendDelInterestUserReq(int iGameIDIndex, char* gameID, char* friendID);
	BOOL			SendDelBannedUserReq(int iGameIDIndex, char* gameID, char* friendID);
	void			SendShoutMessageReq(char* szGameID, int iGameIDIndex, int iItemIdx, int iItemCode, char* szMessage);
	static void		SendSystemShoutMessageReqToThisServer(int iMessageColor,int iShowTime,int iSystemTextIndex,CPacketComposer* params = NULL);
	BOOL			SendShout( char* gameID, char* szText, int iShoutItemCode);
	BOOL			SendShout( char* gameID, char* szText, size_t iSize, int iShoutItemMode );
	void			SendRequestGameIDChange(int iUserIDIndex, int iGameIDIndex , char* szGameID , char* szGameIDNew, int iFactionIndex, BOOL bLogin = TRUE);
	BOOL			SendEventShoutMsgReq( int iGameIDIndex, char* UserID, int iItemCode );
	void			SendRequestKickOut( char* szGameID );
	void			SendClubJoinReq(int iClubSN, BYTE btKind, char* szGameID);
	void			SendClubWithDrawReq(int iClubSN, char* szGameID);
	void			SendClubUpdateAdminMemberReq(char* szGameID, BYTE btClubPosition);
	void			SendClubUpdateKeyPlayerReq(char* szGameID, BYTE btUniformNum);
	void			SendClubTransferReq(char* szGameID);

	void			ProcessPacket(CReceivePacketBuffer* recvPacket);
	void			Process_FriendList(CReceivePacketBuffer* recvPacket);
	void			Process_MsgAtFriend(CReceivePacketBuffer* recvPacket);
	void			Process_MsgToFriendFail(CReceivePacketBuffer* recvPacket);	
	void			Process_AddFriendReq(CReceivePacketBuffer* recvPacket);	
	void			Process_DelFriend(CReceivePacketBuffer* recvPacket);
	void			Process_UserStatusUpdate(CReceivePacketBuffer* recvPacket);
	void			Process_UserNotify(CReceivePacketBuffer* recvPacket);
	void			Process_UserSInfo(CReceivePacketBuffer* recvPacket);
	void			Process_AddBannedUserReq(CReceivePacketBuffer* recvPacket);
	void			Process_DelBannedUser(CReceivePacketBuffer* recvPacket);
	void			Process_AddInterestUserReq(CReceivePacketBuffer* recvPacket);
	void			Process_DelInterestUser(CReceivePacketBuffer* recvPacket);
	void			Process_ShoutMessage(CReceivePacketBuffer* recvPacket);
	void			Process_SystemShoutMessage(CReceivePacketBuffer* recvPacket);
	void			Process_ShoutMessageResult(CReceivePacketBuffer* recvPacket);
	void			Process_UserShout(CReceivePacketBuffer* recvPacket);
	void			Process_FriendIDChangeNotify(CReceivePacketBuffer* recvPacket);
	void			Process_ChangeUserName(CReceivePacketBuffer* recvPacket);
	void			Process_EventShoutMsg(CReceivePacketBuffer* recvPacket);
	void			Process_GameManagerKickOut( CReceivePacketBuffer* recvPacket );		// 20090520 add function for expand GM kick out function.
	void			Process_FSAnnounce(CReceivePacketBuffer* recvPacket);
	void			Process_WhisperResult(CReceivePacketBuffer* recvPacket);

	void			Process_UseChannelBuffItem(CReceivePacketBuffer* recvPacket);
	void			Process_LimitedEditionItemCount(CReceivePacketBuffer* recvPacket);	
	void			Process_ClubJoinRes(CReceivePacketBuffer* recvPacket);	
	void			Process_ClubWithDrawRes(CReceivePacketBuffer* recvPacket);	
	void			Process_ClubUpdateAdminMemberRes(CReceivePacketBuffer* recvPacket);	
	void			Process_ClubUpdateKeyPlayerRes(CReceivePacketBuffer* recvPacket);	
	void			Process_ClubTransferRes(CReceivePacketBuffer* recvPacket);
	void			Process_FollowFriendRes(CReceivePacketBuffer* recvPacket);
	void			Process_FollowFriendResponseRes(CReceivePacketBuffer* recvPacket);
	void			Process_InviteUserRes(CReceivePacketBuffer* recvPacket);
	void			Process_InviteUserResponseRes(CReceivePacketBuffer* recvPacket);
	void			Process_AnnounceCompleteAchievementNot(CReceivePacketBuffer* recvPacket);
	void			Process_UpdateServerState(CReceivePacketBuffer* recvPacket);

	// todo pcroom old code
	void			Process_ExpirePremiumPCRoomNot(CReceivePacketBuffer* recvPacket);

	DECLARE_CENTERPROXY_PROC_FUNC(S2G_UPDATE_INTENSIVEPRACTICE_RANK_REQ)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_UPDATE_INTENSIVEPRACTICE_FRIEND_RANK_REQ)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_UPDATE_MATCHINGPOOLCARE_TARGETUSER_REQ)
	DECLARE_CENTERPROXY_PROC_FUNC(S2A_FACTION_RACE_STATUS_NOT)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FACTION_GIVE_REWARD_REQ)
	DECLARE_CENTERPROXY_PROC_FUNC(S2A_FACTION_USERCOUNT_NOT)
	DECLARE_CENTERPROXY_PROC_FUNC(S2A_FACTION_DISTRICT_SCORE_NOT)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FACTION_NEWSBOARD_NOT)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FACTION_USE_BUFFITEM_REQ)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FACTION_ALL_DISTRICT_USE_BUFFITEM_REQ)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_HONEYWALLET_SHOUT_RES)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_CUSTOMIZE_MAKE_FINISH_RES)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_HOTGIRLTIME_SHOUT_NOT)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_HOTGIRLTIME_EVENT_OPEN_SHOUT_NOT)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_MATCHINGCARD_CASH_NOT)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_MATCHINGCARD_GIVE_EVENTCASH_RES)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_LUCKYSTAR_OPEN_AND_GIVE_REWARD_REQ)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_LUCKYSTAR_SESSION_INFO_RES)
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_RANDOMITEM_TREE_EVENT_GET_REWARD_SHOUT_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_DELETE_WAIT_FRIEND_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EXCHANGE_GAMEID_NOTICE);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_USER_KICK_WITHWEB_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_RANDOMARROW_EVENT_S_REWARD_SHOUT_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_OPENSTATUS_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_UPDATE_LIMITED_SALEITEM_DAY_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_BUY_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOPPINGFASTIVAL_LIMITED_SALEITEM_UPDATE_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_HALFPRICE_SHOUT_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_GAMEOFDICE_GIVE_RANK_REWARD_REQ);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_SHOUT_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_STATUS_FRIEND_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_REFUSAL_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_FRIEND_MISSION_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_UPDATE_FRIENDLIST_STATUS_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_GIVE_BENEFIT_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_KING_EVENT_UPDATE_EXP_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_ADD_FRIEND_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_REMOVE_FRIENDLIST_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_LASTCONNECT_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_MISSION_SUCCESS_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_INVITE_FRIEND_MISSION_LIST_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_HIPPOCAMPUS_GIVE_RANK_REWARD_REQ);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_MISSIONCASHLIMIT_GIVECASH_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_MISSIONCASHLIMIT_DECREASE_CASH_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_LIST_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_ADD_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_DELETE_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_ACCOUNTID_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_SECRETAVATAR_OPT_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_FRIEND_INFO_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_FRIEND_ACCOUNT_UPDATE_FAVORITE_OPT_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_PVE_SCHEDULER_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_PVE_DAILY_MISSION_SCHEDULER_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_ENTER_PCROOM_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EXPIRE_PREMIUM_PCROOM_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_ANNOUNCE_ACHIEVEMENT_COMPLETE_REQ);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_PREMIUMPASS_MISSION_SCHEDULER_NOT);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_INFO_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_TRY_WIN_RES);
	DECLARE_CENTERPROXY_PROC_FUNC(S2G_EVENT_TRANSFERJOYCITY_100DREAM_WRITE_WORDS_RES);
};

#endif // !defined(AFX_CENTERSVRPROXY_H__1C8936C9_A072_43D0_8A40_257A8024DDBA__INCLUDED_)
