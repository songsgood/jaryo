qpragma once

qinclude "FSEventODBC.h"

class CFSGameUser;
class CFSODBCBase;

struct SUserSummerCandyEvent
{
	int _IBuyCandyCount;
	int _iDailyGamePlayCount;
	time_t _tLastGamePlayDate;
};

class CFSGameUserSummerCandyEvent
{
public:
	CFSGameUserSummerCandyEvent(CFSGameUser* pUser);
	~CFSGameUserSummerCandyEvent(void);

	BOOL	Load();
	
	void	SendUserInfo();
	void	SendUserCandyInfo();
	void	BuyCandy(int iPropertyValue);
	BOOL	CheckGiveRewardCandy();
	void	GiveRewardCandy(int iGiveCandyCount = 1);
	void	IncreaseDailyGamePlayCount();
	RESULT_EVENT_SUMMER_CANDY_GET_REWARD GetRewardReq(int iRewardIndex);
	void	SaveUserInfo(CFSEventODBC* pODBC = NULL);

protected:
	void	ResetPlayCount();
	
private:
	CFSGameUser*		m_pUser;
	BOOL				m_bDataLoaded;

	SUserSummerCandyEvent m_UserSummerCandyEvent;
};

