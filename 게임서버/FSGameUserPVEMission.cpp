qinclude "stdafx.h"
qinclude "FSGameUserPVEMission.h"
qinclude "PVEManager.h"
qinclude "CFSGameUser.h"
qinclude "RewardManager.h"

CFSGameUserPVEMission::CFSGameUserPVEMission(CFSGameUser* pUser)
	: m_pUser(pUser)
	, m_bDataLoaded(FALSE)
	, m_iTotalPoint(0)
	, m_iRankGameIDIndex(-1)
{
	ZeroMemory(m_UserPiece, sizeof(SPVEUserPiece)*MAX_PVE_PIECE_TYPE_COUNT);
	ZeroMemory(m_iPoint, sizeof(int)*MAX_PVE_TYPE_COUNT);
	ZeroMemory(m_iaCompletedMissionCount, sizeof(int)*MAX_PVE_TYPE_COUNT);
	ZeroMemory(m_szRankGameID, sizeof(char)*MAX_GAMEID_LENGTH+1);
}

CFSGameUserPVEMission::~CFSGameUserPVEMission(void)
{
}

BOOL CFSGameUserPVEMission::Load()
{
	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_NULL_POINTER_BOOL(pODBC);

	CHECK_CONDITION_RETURN(FALSE == m_pUser->LoadRepresentInfo(), FALSE);

	const SREPRESENT_INFO* pRepresent = m_pUser->GetRepresentInfo();
	CHECK_NULL_POINTER_BOOL(pRepresent);

	m_iRankGameIDIndex = pRepresent->iGameIDIndex;
	strncpy_s(m_szRankGameID, _countof(m_szRankGameID), pRepresent->szGameID, MAX_GAMEID_LENGTH+1);

	vector<SPVEUserMission> vPVEMission;
	vector<SPVEUserPiece> vUserPiece;
	if (ODBC_RETURN_SUCCESS != pODBC->PVE_GetUserMission(m_pUser->GetUserIDIndex(), vPVEMission) ||
		ODBC_RETURN_SUCCESS != pODBC->PVE_GetUserDailyMission(m_pUser->GetUserIDIndex(), m_UserDailyMission) ||
		ODBC_RETURN_SUCCESS != pODBC->PVE_GetUserSpecialAvatarPiece(m_pUser->GetUserIDIndex(), vUserPiece))
	{
		WRITE_LOG_NEW(LOG_TYPE_PVE, INITIALIZE_DATA, FAIL, "PVE_GetUserMission error");
		return FALSE;
	}

	SetMission(vPVEMission);
	m_listMission.sort(SPVEUserMission::SortByMission);

	SetTotalPoint();
	SetPiece(vUserPiece);

	m_bDataLoaded = TRUE;

	return TRUE;
}

void CFSGameUserPVEMission::SetMission(vector<SPVEUserMission>& vPVEMission)
{
	for(int i = 0; i < vPVEMission.size(); ++i)
	{
		m_mapUserMission[vPVEMission[i]._iMissionIndex] = vPVEMission[i];
		m_listMission.push_back(&m_mapUserMission[vPVEMission[i]._iMissionIndex]);

		if(PVE_MISSION_STATUS_COMPETED == vPVEMission[i]._btStatus ||
			PVE_MISSION_STATUS_COMPETED_GET_REWARD == vPVEMission[i]._btStatus)
		{
			BYTE btPVEType = PVEMANAGER.GetMissionPVEType(vPVEMission[i]._iMissionIndex);
			CHECK_CONDITION_RETURN_VOID(MAX_PVE_TYPE_COUNT <= btPVEType);

			m_iaCompletedMissionCount[btPVEType]++;
		}
	}
}

void CFSGameUserPVEMission::SetTotalPoint()
{
	PVE_USER_MISSION_LIST::iterator iter = m_listMission.begin();
	for( ; iter != m_listMission.end(); ++iter)
	{
		if(PVE_MISSION_STATUS_NONE == (*iter)->_btStatus)
			continue;

		int iPoint = PVEMANAGER.GetMissionPoint((*iter)->_iMissionIndex);
		m_iTotalPoint += iPoint;

		BYTE btPVEType = PVEMANAGER.GetMissionPVEType((*iter)->_iMissionIndex);
		if(MAX_PVE_TYPE_COUNT > btPVEType)
			m_iPoint[btPVEType] += iPoint;
	}
}

void CFSGameUserPVEMission::SetPiece(vector<SPVEUserPiece>& vUserPiece)
{	
	for(int i = 0; i < vUserPiece.size(); ++i)
	{
		CHECK_CONDITION_RETURN_VOID(MAX_PVE_PIECE_TYPE_COUNT <= vUserPiece[i]._btPieceType);

		m_UserPiece[vUserPiece[i]._btPieceType] = vUserPiece[i];
	}
}

BYTE CFSGameUserPVEMission::GetPVEType(BYTE btMatchType)
{
	CHECK_CONDITION_RETURN(MATCH_TYPE_PVE > btMatchType || MATCH_TYPE_PVE_AFOOTBALL < btMatchType, PVE_TYPE_NONE);

	BYTE btPVEType = PVE_TYPE_NONE;
	switch(btMatchType)
	{
	case MATCH_TYPE_PVE:
		btPVEType = PVE_TYPE_NONE;
		break;
	case MATCH_TYPE_PVE_3CENTER: 
		btPVEType = PVE_TYPE_3CENTER; 
		break;
	case MATCH_TYPE_PVE_CHANGE_GRADE: 
		btPVEType = PVE_TYPE_CHANGE_GRADE; 
		break;
	case MATCH_TYPE_PVE_AFOOTBALL: 
		btPVEType = PVE_TYPE_AFOOTBALL; 
		break;
	}

	return btPVEType;
}

void CFSGameUserPVEMission::SendMissionInfo()
{
	ResetDailyMisson();

	SS2C_PVE_MISSION_INFO_RES rs;
	rs.iTotalPoint = m_iTotalPoint;
	rs.iTotalMaxCount = PVEMANAGER.GetTotalMissionCount();
	rs.iTotalCurrentCompletedCount = 0;
	ZeroMemory(rs.sLastMission, sizeof(SPVE_LAST_COMPLETED_MISSION)*MAX_LAST_COMPLETED_MISSION_COUNT);

	PVE_USER_MISSION_LIST::iterator iter = m_listMission.begin();
	for(int iIndex = 0; iter != m_listMission.end() && iIndex < MAX_LAST_COMPLETED_MISSION_COUNT; ++iter)
	{
		SPVEMission sMission;
		CHECK_CONDITION_RETURN_VOID(FALSE == PVEMANAGER.GetMissionConfig((*iter)->_iMissionIndex, sMission));

		if(PVE_MISSION_STATUS_NONE == (*iter)->_btStatus)
			continue;

		rs.sLastMission[iIndex].iMissionIndex = sMission._iMissionIndex;
		rs.sLastMission[iIndex].btPVEType = sMission._btPVEType;
		rs.sLastMission[iIndex].btLOD = sMission._btLOD;
		rs.sLastMission[iIndex].btMissionMode = sMission._btMissionMode;
		rs.sLastMission[iIndex].iMissionType = sMission._iMissionType;
		rs.sLastMission[iIndex].iConditionValue = sMission._iConditionValue;
		rs.sLastMission[iIndex].iPoint = sMission._iPoint;
		rs.sLastMission[iIndex].tUpdateDate = (*iter)->_tUpdateDate;
		++iIndex;
	}

	for(BYTE btPVEType = 0; btPVEType < MAX_PVE_TYPE_COUNT; ++btPVEType)
	{
		rs.iaMaxCount[btPVEType] = PVEMANAGER.GetMissionCount(btPVEType);
		rs.iaCurrentCompletedCount[btPVEType] = m_iaCompletedMissionCount[btPVEType];
		rs.iTotalCurrentCompletedCount += m_iaCompletedMissionCount[btPVEType];
	}
	
	rs.bIsDailyMissionOpen = PVEMANAGER.IsDailyMissionOpen();

	SPVEDailyMission sDailyMission;
	PVEMANAGER.GetDailyMissionConfig(sDailyMission);

	rs.sDailyMission.iMissionIndex = sDailyMission._iMissionIndex;
	rs.sDailyMission.btPVEType = sDailyMission._btPVEType;
	rs.sDailyMission.btLOD = sDailyMission._btLOD;
	rs.sDailyMission.btMissionMode = sDailyMission._btMissionMode;
	rs.sDailyMission.iMissionType = sDailyMission._iMissionType;
	rs.sDailyMission.iConditionValue = sDailyMission._iConditionValue;
	rs.sDailyMission.iCurrentValue = m_UserDailyMission._iCurrentValue;
	rs.sDailyMission.btStatus = m_UserDailyMission._btStatus;
	rs.sDailyMission.sReward.btRewardType = sDailyMission._sReward.btRewardType;
	rs.sDailyMission.sReward.iItemCode = sDailyMission._sReward.iItemCode;
	rs.sDailyMission.sReward.iPropertyType = sDailyMission._sReward.iPropertyType;
	rs.sDailyMission.sReward.iPropertyValue = sDailyMission._sReward.iPropertyValue;

	m_pUser->Send(S2C_PVE_MISSION_INFO_RES, &rs, sizeof(SS2C_PVE_MISSION_INFO_RES));
}

void CFSGameUserPVEMission::SendMissionList(BYTE btPVEType)
{
	SS2C_PVE_MISSION_LIST_RES rs;
	rs.btPVEType = btPVEType;
	rs.iMissionCnt = 0;

	vector<SPVEMission> vMission;
	CHECK_CONDITION_RETURN_VOID(FALSE == PVEMANAGER.GetMissionConfigList(btPVEType, vMission));

	sort(vMission.begin(), vMission.end(), SPVEMission::SortByDisplayPriority);

	vector<SPVEMission> vClearMission;
	vector<SPVEMission> vGetRewardMission;
	for(int i = 0; i < vMission.size(); ++i)
	{
		PVE_USER_MISSION_MAP::iterator iter = m_mapUserMission.find(vMission[i]._iMissionIndex);
		if(iter != m_mapUserMission.end() &&
			PVE_MISSION_STATUS_COMPETED <= iter->second._btStatus)
		{
			if(PVE_MISSION_STATUS_COMPETED == iter->second._btStatus)
				vClearMission.push_back(vMission[i]);
			else if(PVE_MISSION_STATUS_COMPETED_GET_REWARD == iter->second._btStatus)
				vGetRewardMission.push_back(vMission[i]);

			vMission[i]._iMissionIndex = -1;
		}
	}

	rs.iMissionCnt = vMission.size();
	CPacketComposer Packet(S2C_PVE_MISSION_LIST_RES);
	Packet.Add((PBYTE)&rs, sizeof(SS2C_PVE_MISSION_LIST_RES));

	SPVE_MISSION sMission;
	for(int i = 0; i < vClearMission.size(); ++i)
	{
		sMission.iMissionIndex = vClearMission[i]._iMissionIndex;
		sMission.btPVEType = vClearMission[i]._btPVEType;
		sMission.btLOD = vClearMission[i]._btLOD;
		sMission.btMissionMode = vClearMission[i]._btMissionMode;
		sMission.iMissionType = vClearMission[i]._iMissionType;
		sMission.iConditionValue = vClearMission[i]._iConditionValue;
		sMission.iCurrentValue = 0;
		sMission.btStatus = PVE_MISSION_STATUS_NONE;
		sMission.iPoint = vClearMission[i]._iPoint;
		sMission.sReward.btRewardType = vClearMission[i]._sReward.btRewardType;
		sMission.sReward.iItemCode = vClearMission[i]._sReward.iItemCode;
		sMission.sReward.iPropertyType = vClearMission[i]._sReward.iPropertyType;
		sMission.sReward.iPropertyValue = vClearMission[i]._sReward.iPropertyValue;
		sMission.bDisplayGauge = vClearMission[i]._bDisplayGauge;
		sMission.iDisplayPriority = vClearMission[i]._iDisplayPriority;
		sMission.tUpdateDate = -1;

		PVE_USER_MISSION_MAP::iterator iter = m_mapUserMission.find(sMission.iMissionIndex);
		if(iter != m_mapUserMission.end())
		{
			sMission.iCurrentValue = iter->second._iCurrentValue;
			sMission.btStatus = iter->second._btStatus;
			sMission.tUpdateDate = iter->second._tUpdateDate;
		}
		Packet.Add((PBYTE)&sMission, sizeof(SPVE_MISSION));
	}

	for(int i = 0; i < vMission.size(); ++i)
	{
		if(-1 == vMission[i]._iMissionIndex)
			continue;;

		sMission.iMissionIndex = vMission[i]._iMissionIndex;
		sMission.btPVEType = vMission[i]._btPVEType;
		sMission.btLOD = vMission[i]._btLOD;
		sMission.btMissionMode = vMission[i]._btMissionMode;
		sMission.iMissionType = vMission[i]._iMissionType;
		sMission.iConditionValue = vMission[i]._iConditionValue;
		sMission.iCurrentValue = 0;
		sMission.btStatus = PVE_MISSION_STATUS_NONE;
		sMission.iPoint = vMission[i]._iPoint;
		sMission.sReward.btRewardType = vMission[i]._sReward.btRewardType;
		sMission.sReward.iItemCode = vMission[i]._sReward.iItemCode;
		sMission.sReward.iPropertyType = vMission[i]._sReward.iPropertyType;
		sMission.sReward.iPropertyValue = vMission[i]._sReward.iPropertyValue;
		sMission.bDisplayGauge = vMission[i]._bDisplayGauge;
		sMission.iDisplayPriority = vMission[i]._iDisplayPriority;
		sMission.tUpdateDate = -1;

		PVE_USER_MISSION_MAP::iterator iter = m_mapUserMission.find(sMission.iMissionIndex);
		if(iter != m_mapUserMission.end())
		{
			sMission.iCurrentValue = iter->second._iCurrentValue;
			sMission.btStatus = iter->second._btStatus;
			sMission.tUpdateDate = iter->second._tUpdateDate;
		}
		Packet.Add((PBYTE)&sMission, sizeof(SPVE_MISSION));
	}

	for(int i = 0; i < vGetRewardMission.size(); ++i)
	{
		sMission.iMissionIndex = vGetRewardMission[i]._iMissionIndex;
		sMission.btPVEType = vGetRewardMission[i]._btPVEType;
		sMission.btLOD = vGetRewardMission[i]._btLOD;
		sMission.btMissionMode = vGetRewardMission[i]._btMissionMode;
		sMission.iMissionType = vGetRewardMission[i]._iMissionType;
		sMission.iConditionValue = vGetRewardMission[i]._iConditionValue;
		sMission.iCurrentValue = 0;
		sMission.btStatus = PVE_MISSION_STATUS_NONE;
		sMission.iPoint = vGetRewardMission[i]._iPoint;
		sMission.sReward.btRewardType = vGetRewardMission[i]._sReward.btRewardType;
		sMission.sReward.iItemCode = vGetRewardMission[i]._sReward.iItemCode;
		sMission.sReward.iPropertyType = vGetRewardMission[i]._sReward.iPropertyType;
		sMission.sReward.iPropertyValue = vGetRewardMission[i]._sReward.iPropertyValue;
		sMission.bDisplayGauge = vGetRewardMission[i]._bDisplayGauge;
		sMission.iDisplayPriority = vGetRewardMission[i]._iDisplayPriority;
		sMission.tUpdateDate = -1;

		PVE_USER_MISSION_MAP::iterator iter = m_mapUserMission.find(sMission.iMissionIndex);
		if(iter != m_mapUserMission.end())
		{
			sMission.iCurrentValue = iter->second._iCurrentValue;
			sMission.btStatus = iter->second._btStatus;
			sMission.tUpdateDate = iter->second._tUpdateDate;
		}
		Packet.Add((PBYTE)&sMission, sizeof(SPVE_MISSION));
	}

	m_pUser->Send(&Packet);
}

RESULT_PVE_MISSION_GET_REWARD CFSGameUserPVEMission::GetRewardReq(int iMissionIndex, SREWARD_INFO& sReward)
{
	ZeroMemory(&sReward, sizeof(SREWARD_INFO));
	PVE_USER_MISSION_MAP::iterator iter = m_mapUserMission.find(iMissionIndex);
	CHECK_CONDITION_RETURN(iter == m_mapUserMission.end(), RESULT_PVE_MISSION_GET_REWARD_FAIL);
	CHECK_CONDITION_RETURN(PVE_MISSION_STATUS_COMPETED != iter->second._btStatus, RESULT_PVE_MISSION_GET_REWARD_FAIL);
//	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_PVE_MISSION_GET_REWARD_FAIL_MAILBOX_FULL);

	int iRewardIndex = PVEMANAGER.GetMissionRewardIndex(iMissionIndex);
	CHECK_CONDITION_RETURN(0 == iRewardIndex, RESULT_PVE_MISSION_GET_REWARD_FAIL);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_PVE_MISSION_GET_REWARD_FAIL);
	
	CHECK_CONDITION_RETURN(REWARD_TYPE_ACHIEVEMENT == pReward->GetRewardType(), RESULT_PVE_MISSION_GET_REWARD_FAIL);

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_PVE_MISSION_GET_REWARD_FAIL);

	SPVEUserMission sUserMission;
	memcpy(&sUserMission, &iter->second, sizeof(SPVEUserMission));
	sUserMission._btStatus = PVE_MISSION_STATUS_COMPETED_GET_REWARD;

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateUserMission(m_pUser->GetUserIDIndex(), sUserMission, 0, 0, m_iRankGameIDIndex, m_szRankGameID), RESULT_PVE_MISSION_GET_REWARD_FAIL);

	BOOL bPiece = FALSE;

	sReward.btRewardType = pReward->GetRewardType();
	sReward.iPropertyValue = pReward->GetRewardValue();

	if(REWARD_TYPE_ITEM == pReward->GetRewardType())
	{
		SRewardConfigItem* pRewardItem = (SRewardConfigItem*)pReward;
		CHECK_CONDITION_RETURN(NULL == pRewardItem, RESULT_PVE_MISSION_GET_REWARD_FAIL);

		sReward.iItemCode = pRewardItem->iItemCode;
		sReward.iPropertyType = pRewardItem->GetPropertyType();
		sReward.iPropertyValue = pRewardItem->GetPropertyValue();

		if(PVE_RANDOM_SPECIAL_AVATAR_PIECE_ITEMCODE == pRewardItem->iItemCode)
		{
			bPiece = TRUE;
			sReward.btRewardType = REWARD_TYPE_PVE_SPECIAL_AVATAR_PIECE;
			sReward.iItemCode = GiveRandomSpecialAvatarPieceType(pRewardItem->GetPropertyValue());
			if(MAX_PVE_PIECE_TYPE_COUNT == sReward.iItemCode)
			{
				WRITE_LOG_NEW(LOG_TYPE_PVE, SET_DATA, FAIL, "CFSGameUserPVEMission::GetRewardReq - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
er->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
			}
		}
	}

	if(FALSE == bPiece)
	{
		pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
		if(NULL == pReward)
		{
			WRITE_LOG_NEW(LOG_TYPE_PVE, SET_DATA, FAIL, "CFSGameUserPVEMission::GetRewardReq - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
r->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
		}
	}
	
	iter->second._btStatus = PVE_MISSION_STATUS_COMPETED_GET_REWARD;

	return RESULT_PVE_MISSION_GET_REWARD_SUCCESS;
}

RESULT_PVE_DAILY_MISSION_GET_REWARD CFSGameUserPVEMission::GetDailyRewardReq(SREWARD_INFO& sReward)
{
	CHECK_CONDITION_RETURN(CLOSED == PVEMANAGER.IsDailyMissionOpen(), RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);

	ZeroMemory(&sReward, sizeof(SREWARD_INFO));
	CHECK_CONDITION_RETURN(TRUE == ResetDailyMisson(), RESULT_PVE_DAILY_MISSION_GET_REWARD_RESET_MISSION);

	CHECK_CONDITION_RETURN(PVE_MISSION_STATUS_COMPETED != m_UserDailyMission._btStatus, RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);
//	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL_MAILBOX_FULL);

	SPVEDailyMission sDailyMission;
	PVEMANAGER.GetDailyMissionConfig(sDailyMission);
	CHECK_CONDITION_RETURN(sDailyMission._iMissionType != m_UserDailyMission._iMissionType, RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(sDailyMission._sReward.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);

	CHECK_CONDITION_RETURN(REWARD_TYPE_ACHIEVEMENT == pReward->GetRewardType(), RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);

	SPVEUserDailyMission sUserMission;
	memcpy(&sUserMission, &m_UserDailyMission, sizeof(SPVEUserDailyMission));
	sUserMission._btStatus = PVE_MISSION_STATUS_COMPETED_GET_REWARD;

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateUserDailyMission(m_pUser->GetUserIDIndex(), sUserMission), RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);

	BOOL bPiece = FALSE;

	sReward.btRewardType = pReward->GetRewardType();
	sReward.iPropertyValue = pReward->GetRewardValue();

	if(REWARD_TYPE_ITEM == pReward->GetRewardType())
	{
		SRewardConfigItem* pRewardItem = (SRewardConfigItem*)pReward;
		CHECK_CONDITION_RETURN(NULL == pRewardItem, RESULT_PVE_DAILY_MISSION_GET_REWARD_FAIL);

		sReward.iItemCode = pRewardItem->iItemCode;
		sReward.iPropertyType = pRewardItem->GetPropertyType();
		sReward.iPropertyValue = pRewardItem->GetPropertyValue();

		if(PVE_RANDOM_SPECIAL_AVATAR_PIECE_ITEMCODE == pRewardItem->iItemCode)
		{
			bPiece = TRUE;
			sReward.btRewardType = REWARD_TYPE_PVE_SPECIAL_AVATAR_PIECE;
			sReward.iItemCode = GiveRandomSpecialAvatarPieceType(pRewardItem->GetPropertyValue());
			if(MAX_PVE_PIECE_TYPE_COUNT == sReward.iItemCode)
			{
				WRITE_LOG_NEW(LOG_TYPE_PVE, SET_DATA, FAIL, "CFSGameUserPVEMission::GetDailyRewardReq - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
er->GetUserIDIndex(), m_pUser->GetGameIDIndex(), sDailyMission._sReward.iRewardIndex);
			}
		}
	}

	if(FALSE == bPiece)
	{
		pReward = REWARDMANAGER.GiveReward(m_pUser, sDailyMission._sReward.iRewardIndex);
		if(NULL == pReward)
		{
			WRITE_LOG_NEW(LOG_TYPE_PVE, SET_DATA, FAIL, "CFSGameUserPVEMission::GetDailyRewardReq - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
r->GetUserIDIndex(), m_pUser->GetGameIDIndex(), sDailyMission._sReward.iRewardIndex);
		}
	}

	m_UserDailyMission._btStatus = PVE_MISSION_STATUS_COMPETED_GET_REWARD;

	return RESULT_PVE_DAILY_MISSION_GET_REWARD_SUCCESS;
}

BYTE CFSGameUserPVEMission::GiveRandomSpecialAvatarPieceType(int iPieceCount)
{
	BYTE btPieceType = rand() MAX_PVE_PIECE_TYPE_COUNT;

	
	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_CONDITION_RETURN(NULL == pODBC, MAX_PVE_PIECE_TYPE_COUNT);

	int iUpdated_PieceCount = 0;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateUserSpecialAvatarPiece(m_pUser->GetUserIDIndex(), PVE_SPECIAL_AVATAR_PIECE_UPDATETYPE_GET, btPieceType, iPieceCount, PVE_SPECIAL_AVATAR_PIECE_LOGTYPE_GET_PIECE, iUpdated_PieceCount), MAX_PVE_PIECE_TYPE_COUNT);

	m_UserPiece[btPieceType]._btPieceType = btPieceType;
	m_UserPiece[btPieceType]._iCurrentPieceCount = iUpdated_PieceCount;

	return btPieceType;
}

void CFSGameUserPVEMission::CheckAndUpdateMission(BYTE btMatchType, BYTE btPVEEOD, BYTE btMissionType, int iValue1, int iValue2, int iValue3)
{
	BYTE btPVEType = GetPVEType(btMatchType);
	CHECK_CONDITION_RETURN_VOID(PVE_TYPE_NONE == btPVEType);

	vector<SPVEMission> vMission;
	CHECK_CONDITION_RETURN_VOID(FALSE == PVEMANAGER.GetMissionConfigList(btPVEType, btPVEEOD, btMissionType, vMission));

	int iValue = 0;
	for(int i = 0; i < vMission.size(); ++i)
	{
		SPVEMission sConfig = vMission[i];

		if(TRUE == sConfig._bAccumulateValue)
			continue;

		if(sConfig._iConditionValue > iValue1 ||
			sConfig._iConditionValue > iValue2 ||
			sConfig._iConditionValue > iValue3)
			continue;

		iValue = sConfig._iConditionValue;

		CheckAndUpdateMission(btMatchType, btPVEEOD, btMissionType, iValue);
	}
}

void CFSGameUserPVEMission::CheckAndUpdateMission(BYTE btMatchType, BYTE btPVEEOD, BYTE btMissionType, int iAddValue/* = 1*/)
{
	BYTE btPVEType = GetPVEType(btMatchType);
	CHECK_CONDITION_RETURN_VOID(PVE_TYPE_NONE == btPVEType);

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_NULL_POINTER_VOID(pODBC);

	vector<SPVEMission> vMission;
	CHECK_CONDITION_RETURN_VOID(FALSE == PVEMANAGER.GetMissionConfigList(btPVEType, btPVEEOD, btMissionType, vMission));

	BOOL bSort = FALSE;
	for(int i = 0; i < vMission.size(); ++i)
	{
		SPVEMission sConfig = vMission[i];

		SPVEUserMission sMission;
		sMission._iMissionIndex = sConfig._iMissionIndex;
		sMission._iCurrentValue = iAddValue;

		PVE_USER_MISSION_MAP::iterator iter = m_mapUserMission.find(sConfig._iMissionIndex);
		if(iter != m_mapUserMission.end())
		{
			if(PVE_MISSION_STATUS_NONE < iter->second._btStatus)
				continue;

			if(TRUE == sConfig._bAccumulateValue)
				sMission._iCurrentValue = iter->second._iCurrentValue + iAddValue;
		}

		if(PVE_MISSION_TYPE_WIN_PLAY_TOTAL_POINT_LESS_THAN_A_SOME_POINT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_AWAY_LESS_THAN_A_SOME_REBOUND_COUNT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_AWAY_LESS_THAN_A_SOME_BLOCK_COUNT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEP1_WIN_PLAY_LESS_A_SOME_TOTAL_POINT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_CHANGE_GRADE_MODE_STEM2_WIN_PLAY_LESS_A_SOME_POINT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_3POINT_SHOOT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_DUNK_SHOOT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_MIDDLE_SHOOT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_GOAL_SHOOT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_ONLY_USE_RAYUP == sConfig._iMissionType)
		{
			if(sConfig._iConditionValue < sMission._iCurrentValue)
				continue;
		}
		else if(PVE_MISSION_TYPE_WIN_PLAY_GAP_A_SOME_POINT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_GAME_START_A_SOME_TIME_NOT_LOSE_A_POINT == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_COUNT_1_USER == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_COUNT_3_USER == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_NOT_FALL_DOWN == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_NOT_DROP_BALL == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_NOT_WEAR_PROPERTY_ITEM == sConfig._iMissionType ||
			PVE_MISSION_TYPE_WIN_PLAY_NOT_SUFFER_STEAL == sConfig._iMissionType)
		{
			if(sConfig._iConditionValue != sMission._iCurrentValue)
				continue;
		}
		else if(PVE_MISSION_TYPE_MISSION_COMPLETED_COUNT == sConfig._iMissionType)
		{
			if(0 == m_iaCompletedMissionCount[btPVEType])
				continue;

			sMission._iCurrentValue = m_iaCompletedMissionCount[btPVEType];
		}
		else 
		{
			if(FALSE == sConfig._bAccumulateValue &&
				sConfig._iConditionValue > sMission._iCurrentValue)
				continue;
		}

		int iRewardIndex = PVEMANAGER.GetMissionRewardIndex(sConfig._iMissionIndex);
		CHECK_CONDITION_RETURN_VOID(0 == iRewardIndex);

		SRewardConfig* pReward = REWARDMANAGER.GetReward(iRewardIndex);
		CHECK_NULL_POINTER_VOID(pReward);

		if((FALSE == sConfig._bAccumulateValue && PVE_MISSION_TYPE_MISSION_COMPLETED_COUNT != sConfig._iMissionType) ||
			(FALSE == sConfig._bAccumulateValue && PVE_MISSION_TYPE_MISSION_COMPLETED_COUNT == sConfig._iMissionType && sConfig._iConditionValue <= sMission._iCurrentValue) ||
			(TRUE == sConfig._bAccumulateValue && sConfig._iConditionValue <= sMission._iCurrentValue))
		{
			sMission._iCurrentValue = sConfig._iConditionValue;
			sMission._btStatus = PVE_MISSION_STATUS_COMPETED;
			if(REWARD_TYPE_ACHIEVEMENT == pReward->GetRewardType())
				sMission._btStatus = PVE_MISSION_STATUS_COMPETED_GET_REWARD;
		}

		int iPoint = m_iPoint[btPVEType];;
		if(PVE_MISSION_STATUS_COMPETED <= sMission._btStatus)
			iPoint += sConfig._iPoint;

		sMission._tUpdateDate = _time64(NULL);
		if(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateUserMission(m_pUser->GetUserIDIndex(), sMission, btPVEType, iPoint, m_iRankGameIDIndex, m_szRankGameID))
			continue;

		m_mapUserMission[sMission._iMissionIndex] = sMission;

		if(iter == m_mapUserMission.end())
		{
			m_listMission.push_back(&m_mapUserMission[sMission._iMissionIndex]);
			bSort = TRUE;
		}

		if(PVE_MISSION_STATUS_COMPETED <= sMission._btStatus)
		{
			m_iPoint[btPVEType] = iPoint;
			m_iTotalPoint += sConfig._iPoint;
			m_iaCompletedMissionCount[btPVEType] += 1;
		}
		
		if(PVE_MISSION_STATUS_COMPETED_GET_REWARD == sMission._btStatus)
		{
			pReward = REWARDMANAGER.GiveReward(m_pUser, iRewardIndex);
			if(NULL == pReward)
			{
				WRITE_LOG_NEW(LOG_TYPE_PVE, SET_DATA, FAIL, "CFSGameUserPVEMission::CheckAndUpdateMission - UserIDIndex:10752790, GameIDIndex:0, RewardIndex:7106560", 
er->GetUserIDIndex(), m_pUser->GetGameIDIndex(), iRewardIndex);
			}
		}
	}

	if(TRUE == bSort)
		m_listMission.sort(SPVEUserMission::SortByMission);
}

BOOL CFSGameUserPVEMission::ResetDailyMisson()
{
	time_t tCurrentDate = _time64(NULL);
	TIMESTAMP_STRUCT CurrentDate;
	TimetToTimeStruct(tCurrentDate, CurrentDate);

	time_t tYesterDate = tCurrentDate - (60*60*24);

	int iYesterYYMMDD = TimetToYYYYMMDD(tYesterDate);
	int iYYMMDD = TimetToYYYYMMDD(tCurrentDate);

	int iHour = CurrentDate.hour;
	const int iResetHour = 7;

	if(0 < m_UserDailyMission._iUpdateDate)
	{
		CHECK_CONDITION_RETURN(iHour < iResetHour && ((m_UserDailyMission._iUpdateDate == iYesterYYMMDD && m_UserDailyMission._iUpdateHour >= iResetHour) || m_UserDailyMission._iUpdateDate >= iYYMMDD), FALSE);
		CHECK_CONDITION_RETURN(iHour >= iResetHour && ((m_UserDailyMission._iUpdateDate == iYYMMDD && m_UserDailyMission._iUpdateHour >= iResetHour) || m_UserDailyMission._iUpdateDate > iYYMMDD), FALSE);
	}

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_CONDITION_RETURN(NULL == pODBC, FALSE);
	
	SPVEDailyMission sDailyMission;
	PVEMANAGER.GetDailyMissionConfig(sDailyMission);

	SPVEUserDailyMission sUserMission;
	sUserMission._iMissionType = sDailyMission._iMissionType;
	sUserMission._iCurrentValue = 0;
	sUserMission._btStatus = PVE_MISSION_STATUS_NONE;
	sUserMission._iUpdateDate = iYYMMDD;
	sUserMission._iUpdateHour = iHour;

	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateUserDailyMission(m_pUser->GetUserIDIndex(), sUserMission), FALSE);

	memcpy(&m_UserDailyMission, &sUserMission, sizeof(SPVEUserDailyMission));

	return TRUE;
}

void CFSGameUserPVEMission::CheckAndUpdateDailyMission(BYTE btMatchType, BYTE btPVEEOD, BYTE btMissionType, int iAddValue/* = 1*/)
{
	CHECK_CONDITION_RETURN_VOID(CLOSED == PVEMANAGER.IsDailyMissionOpen());

	BYTE btPVEType = GetPVEType(btMatchType);
	CHECK_CONDITION_RETURN_VOID(PVE_TYPE_NONE == btPVEType);

	SPVEDailyMission sDailyMission;
	PVEMANAGER.GetDailyMissionConfig(sDailyMission);
	CHECK_CONDITION_RETURN_VOID(btPVEType != sDailyMission._btPVEType && PVE_TYPE_NONE != sDailyMission._btPVEType);
	CHECK_CONDITION_RETURN_VOID(btPVEEOD != sDailyMission._btLOD && INDEX_LEVEL_OF_DIFFICULTY_ALL != sDailyMission._btLOD);
	CHECK_CONDITION_RETURN_VOID(btMissionType != sDailyMission._iMissionType);

	ResetDailyMisson();

	CHECK_CONDITION_RETURN_VOID(PVE_MISSION_STATUS_NONE != m_UserDailyMission._btStatus);

	time_t tCurrentDate = _time64(NULL);
	TIMESTAMP_STRUCT CurrentDate;
	TimetToTimeStruct(tCurrentDate, CurrentDate);

	int iPreValue = 0;

	SPVEUserDailyMission sUserMission;
	memcpy(&sUserMission, &m_UserDailyMission, sizeof(SPVEUserDailyMission));

	iPreValue = sUserMission._iCurrentValue;
	sUserMission._iCurrentValue += iAddValue;
	if(FALSE == sDailyMission._bAccumulateValue)
		sUserMission._iCurrentValue = iAddValue;
	
	sUserMission._iUpdateDate = TimetToYYYYMMDD(tCurrentDate);
	sUserMission._iUpdateHour = CurrentDate.hour;

	if(sDailyMission._iConditionValue <= sUserMission._iCurrentValue)
		sUserMission._btStatus = PVE_MISSION_STATUS_COMPETED;

	CHECK_CONDITION_RETURN_VOID(FALSE == sDailyMission._bAccumulateValue &&
		PVE_MISSION_STATUS_COMPETED != sUserMission._btStatus &&
		iPreValue == sUserMission._iCurrentValue);

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_NULL_POINTER_VOID(pODBC);

	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateUserDailyMission(m_pUser->GetUserIDIndex(), sUserMission));

	memcpy(&m_UserDailyMission, &sUserMission, sizeof(SPVEUserDailyMission));
}

void CFSGameUserPVEMission::SendAvatarPieceInfo()
{
	SS2C_PVE_SPECIAL_AVATAR_PIECE_INFO_RES rs;

	for(BYTE btPiceType = 0; btPiceType < MAX_PVE_PIECE_TYPE_COUNT; ++btPiceType)
	{
		rs.sPieceInfo[btPiceType].btPieceType = btPiceType;
		rs.sPieceInfo[btPiceType].iNeedPieceCount = PVEMANAGER.GetNeedPieceCount(btPiceType);
		rs.sPieceInfo[btPiceType].iCurrentPieceCount = m_UserPiece[btPiceType]._iCurrentPieceCount;
	}

	m_pUser->Send(S2C_PVE_SPECIAL_AVATAR_PIECE_INFO_RES, &rs, sizeof(SS2C_PVE_SPECIAL_AVATAR_PIECE_INFO_RES));
}

RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE CFSGameUserPVEMission::ExchangePieceReq(SC2S_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_REQ& rq)
{
	CHECK_CONDITION_RETURN(MAX_PVE_PIECE_TYPE_COUNT <= rq.btUsePieceType || MAX_PVE_PIECE_TYPE_COUNT <= rq.btGetPieceType, RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_FAIL);
	CHECK_CONDITION_RETURN(rq.btUsePieceType == rq.btGetPieceType, RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_FAIL_SAME_PIECE);

	CHECK_CONDITION_RETURN((rq.iUsePieceCount / 3) != rq.iGetPieceCount, RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_FAIL);
	CHECK_CONDITION_RETURN(m_UserPiece[rq.btUsePieceType]._iCurrentPieceCount < rq.iUsePieceCount, RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_FAIL_NOT_EXIST_PICE);

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_FAIL);

	int iUpdated_UsePieceCount = 0, iUpdated_GetPieceCount = 0;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->PVE_ExchangeUserSpecialAvatarPiece(m_pUser->GetUserIDIndex(), rq.btUsePieceType, rq.iUsePieceCount, rq.btGetPieceType, rq.iGetPieceCount, PVE_SPECIAL_AVATAR_PIECE_LOGTYPE_EXCHANGE_PIECE, iUpdated_UsePieceCount, iUpdated_GetPieceCount), RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_FAIL);

	m_UserPiece[rq.btUsePieceType]._btPieceType = rq.btUsePieceType;
	m_UserPiece[rq.btUsePieceType]._iCurrentPieceCount = iUpdated_UsePieceCount;

	m_UserPiece[rq.btGetPieceType]._btPieceType = rq.btGetPieceType;
	m_UserPiece[rq.btGetPieceType]._iCurrentPieceCount = iUpdated_GetPieceCount;

	return RESULT_PVE_SPECIAL_AVATAR_PIECE_EXCHANGE_SUCCESS;
}

RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD CFSGameUserPVEMission::GetRewardPiece(BYTE btPieceType, SREWARD_INFO& sReward)
{
	ZeroMemory(&sReward, sizeof(SREWARD_INFO));
	CHECK_CONDITION_RETURN(MAX_PVE_PIECE_TYPE_COUNT <= btPieceType, RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_FAIL);
	CHECK_CONDITION_RETURN(m_pUser->GetPresentList(MAIL_PRESENT_ON_ACCOUNT)->GetSize() >= MAX_PRESENT_LIST, RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_FAIL_MAILBOX_FULL);

	int iNeedPieceCount = PVEMANAGER.GetNeedPieceCount(btPieceType);
	CHECK_CONDITION_RETURN(m_UserPiece[btPieceType]._iCurrentPieceCount < iNeedPieceCount, RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_FAIL_NOT_EXIST_PICE);

	SRewardInfo sRewardConfig;
	PVEMANAGER.GetPieceReward(btPieceType, sRewardConfig);

	SRewardConfig* pReward = REWARDMANAGER.GetReward(sRewardConfig.iRewardIndex);
	CHECK_CONDITION_RETURN(NULL == pReward, RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_FAIL);

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_CONDITION_RETURN(NULL == pODBC, RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_FAIL);

	int iUpdated_PieceCount = 0;
	CHECK_CONDITION_RETURN(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateUserSpecialAvatarPiece(m_pUser->GetUserIDIndex(), PVE_SPECIAL_AVATAR_PIECE_UPDATETYPE_USE, btPieceType, iNeedPieceCount, PVE_SPECIAL_AVATAR_PIECE_LOGTYPE_USE_PIECE, iUpdated_PieceCount), RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_FAIL);
	
	m_UserPiece[btPieceType]._btPieceType = btPieceType;
	m_UserPiece[btPieceType]._iCurrentPieceCount = iUpdated_PieceCount;

	pReward = REWARDMANAGER.GiveReward(m_pUser, sRewardConfig.iRewardIndex);
	if(NULL == pReward)
	{
		WRITE_LOG_NEW(LOG_TYPE_PVE, DB_DATA_UPDATE, FAIL, "CFSGameUserPVEMission::GetRewardPiece - UserIDIndex:10752790, RewardIndex:0", m_pUser->GetUserIDIndex(), sRewardConfig.iRewardIndex);

sReward.btRewardType = sRewardConfig.btRewardType;
	sReward.iItemCode = sRewardConfig.iItemCode;
	sReward.iPropertyType = sRewardConfig.iPropertyType;
	sReward.iPropertyValue = sRewardConfig.iPropertyValue;

	return RESULT_PVE_SPECIAL_AVATAR_PIECE_GET_REWARD_SUCCESS;
}

void CFSGameUserPVEMission::SendRankList(BYTE btPVEType)
{
	CHECK_CONDITION_RETURN_VOID(MAX_PVE_TYPE_COUNT <= btPVEType);

	int iPoint = 0;
	if(PVE_TYPE_NONE == btPVEType)
		iPoint = m_iTotalPoint;
	else
		iPoint = m_iPoint[btPVEType];

	PVEMANAGER.SendRankList(btPVEType, m_pUser, m_szRankGameID, iPoint);
}

void CFSGameUserPVEMission::MakeLastMissionList(BYTE btPVEType, SS2C_PVE_RANK_USER_INFO_RES& rs)
{
	CHECK_CONDITION_RETURN_VOID(MAX_PVE_TYPE_COUNT <= btPVEType);

	if(PVE_TYPE_NONE == btPVEType)
	{
		PVE_USER_MISSION_LIST::iterator iter = m_listMission.begin();
		for(int iIndex = 0; iter != m_listMission.end() && iIndex < MAX_LAST_COMPLETED_MISSION_COUNT; ++iter)
		{
			SPVEMission sMission;
			CHECK_CONDITION_RETURN_VOID(FALSE == PVEMANAGER.GetMissionConfig((*iter)->_iMissionIndex, sMission));

			if(PVE_MISSION_STATUS_NONE == (*iter)->_btStatus)
				continue;

			rs.sLastMission[iIndex].iMissionIndex = sMission._iMissionIndex;
			rs.sLastMission[iIndex].btPVEType = sMission._btPVEType;
			rs.sLastMission[iIndex].btLOD = sMission._btLOD;
			rs.sLastMission[iIndex].btMissionMode = sMission._btMissionMode;
			rs.sLastMission[iIndex].iMissionType = sMission._iMissionType;
			rs.sLastMission[iIndex].iConditionValue = sMission._iConditionValue;
			rs.sLastMission[iIndex].iPoint = sMission._iPoint;
			rs.sLastMission[iIndex].tUpdateDate = (*iter)->_tUpdateDate;
			++iIndex;
		}
	}
	else 
	{
		PVE_USER_MISSION_LIST listMission;
		PVE_USER_MISSION_LIST::iterator iter = m_listMission.begin();
		for( ; iter != m_listMission.end(); ++iter)
		{
			SPVEMission sMission;
			CHECK_CONDITION_RETURN_VOID(FALSE == PVEMANAGER.GetMissionConfig((*iter)->_iMissionIndex, sMission));

			if(PVE_MISSION_STATUS_NONE == (*iter)->_btStatus)
				continue;

			if(btPVEType == sMission._btPVEType)
			{
				listMission.push_back(*iter);
			}
		}

		iter = listMission.begin();
		for(int iIndex = 0; iter != listMission.end() && iIndex < MAX_LAST_COMPLETED_MISSION_COUNT; ++iter)
		{
			SPVEMission sMission;
			CHECK_CONDITION_RETURN_VOID(FALSE == PVEMANAGER.GetMissionConfig((*iter)->_iMissionIndex, sMission));

			rs.sLastMission[iIndex].iMissionIndex = sMission._iMissionIndex;
			rs.sLastMission[iIndex].btPVEType = sMission._btPVEType;
			rs.sLastMission[iIndex].btLOD = sMission._btLOD;
			rs.sLastMission[iIndex].btMissionMode = sMission._btMissionMode;
			rs.sLastMission[iIndex].iMissionType = sMission._iMissionType;
			rs.sLastMission[iIndex].iConditionValue = sMission._iConditionValue;
			rs.sLastMission[iIndex].iPoint = sMission._iPoint;
			rs.sLastMission[iIndex].tUpdateDate = (*iter)->_tUpdateDate;

			++iIndex;
		}
	}
}

void CFSGameUserPVEMission::UpdateRankGameID(int iGameIDIndex, char* szGameID)
{
	CHECK_CONDITION_RETURN_VOID(iGameIDIndex != m_iRankGameIDIndex);

	CFSRankODBC* pODBC = (CFSRankODBC*)ODBCManager.GetODBC(ODBC_RANK);
	CHECK_NULL_POINTER_VOID(pODBC);

	CHECK_CONDITION_RETURN_VOID(ODBC_RETURN_SUCCESS != pODBC->PVE_UpdateRankGameID(m_pUser->GetUserIDIndex(), iGameIDIndex, szGameID));

	strncpy_s(m_szRankGameID, _countof(m_szRankGameID), szGameID, MAX_GAMEID_LENGTH+1);
}