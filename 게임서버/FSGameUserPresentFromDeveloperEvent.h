#pragma once

class CUser;
class CFSODBCBase;

class CFSGameUserPresentFromDeveloperEvent
{
public:
	CFSGameUserPresentFromDeveloperEvent(CUser* pUser);
	~CFSGameUserPresentFromDeveloperEvent(void);

	BOOL Load();

	void SendEventInfo();
	void SendPresentInfo();
	RESULT_EVENT_PRESENT_FROM_DEVELOPER_GET_PRESENT GetPresentReq(BYTE btEventRewardType);
	void UpdateUserEventStatus(BOOL bSave = FALSE);
	BOOL CheckEventUser();

private:
	BOOL CheckUpdateTime(time_t tCurrentDate);

private:
	CUser* m_pUser;
	BOOL m_bDataLoaded;

	SUserDeveloperEvent m_UserDeveloperEvent;
	time_t m_tMissionCheckTime;
};

