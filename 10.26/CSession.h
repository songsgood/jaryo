#pragma once

#include "CSocket.h"

#include "RingBuffer.h"
#include "SerializationBuffer.h"
#include "ListQueue.h"

#define SEND_WSABUF_COUNT 10

class CSession : public CSocket
{
public:
	CSession();
	virtual ~CSession();

	
	void InitSession();

	bool SendPost();
	bool RecvPost();
	void Disconnect();

	long IncrementIO();
	long DecrementIO();
		
	void SetIO(long Count);

	
	OVERLAPPED* GetSendOverlapped();
	OVERLAPPED* GetRecvOverlapped();
	OVERLAPPED* GetSendPostOverlapped();
	
	
	void RecvBufferAddRear(DWORD Size);
	void RecvBufferRemoveData(DWORD Size);
	


private:

	CSocket m_Sock;

	CRingBuffer				   m_RecvBuffer;
	CListQueue<CPacketBuffer*> m_SendQ;
	CListQueue<CPacketBuffer*> m_SendPacketKeepQ;

	
	
	OVERLAPPED m_SendOverlapped;
	OVERLAPPED m_RecvOverlapped;
	OVERLAPPED m_SendPostOverlapped;

	
	
	
	long m_lIOCount;

	bool m_bReleaseFlag;

	long m_bSendCount;
	
	



};