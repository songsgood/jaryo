#include "stdafx.h"
#include "RingBuffer.h"
#include <malloc.h>



CRingBuffer::CRingBuffer(int iBufferSize)
{
    _iBufferSize = iBufferSize;

    _pBuffer = (char*)malloc(_iBufferSize); 

    _iRear = 0;
    _iFront = 0;
  
}

/////////////////////////////////////////////////////////////////////////
// 소멸자
/////////////////////////////////////////////////////////////////////////
CRingBuffer::~CRingBuffer()
{
    free(_pBuffer);
	
}

/////////////////////////////////////////////////////////////////////////
// 현재 사용중인 용량 얻기.
//
// Parameters: 없음.
// Return: (int)사용중인 용량.
/////////////////////////////////////////////////////////////////////////
int CRingBuffer::GetUseSize(void)
{
    int iSize;
    int iRear = _iRear;
    int iFront = _iFront;

    if (iRear >= iFront)
    {
        iSize = iRear - iFront;
    }
    else
    {
        iSize = _iBufferSize - iFront + iRear;
    }

    return iSize;
}

/////////////////////////////////////////////////////////////////////////
// 현재 버퍼에 남은 용량 얻기.
//
// Parameters: 없음.
// Return: (int)남은용량.
/////////////////////////////////////////////////////////////////////////
int CRingBuffer::GetFreeSize(void)
{
    int iSize;
    int iRear = _iRear;
    int iFront = _iFront;

    if (iRear >= iFront)
    {
        iSize = _iBufferSize - iRear + iFront - 1;
    }
    else
    {
        iSize = iFront - iRear - 1;
    }

    return iSize;
}

/////////////////////////////////////////////////////////////////////////
// 현재 버퍼에 남은 용량 얻기.
//
// Parameters: 없음.
// Return: (int)총 버퍼 크기.
/////////////////////////////////////////////////////////////////////////
int CRingBuffer::GetBufferSize(void)
{
    return _iBufferSize;
}
/////////////////////////////////////////////////////////////////////////
// 버퍼 포인터로 외부에서 한방에 쓸 수 있는 길이.
// (끊기지 않은 길이)
//
// Parameters: 없음.
// Return: (int)사용가능 용량.
////////////////////////////////////////////////////////////////////////
int CRingBuffer::GetNotBrokenPutSize(void)
{
    int iSize;
    int iBufferSize = _iBufferSize;
    int iRear = _iRear;
    int iFront = _iFront;

	//이거 고치자
    int iBlank = (iFront + iBufferSize - 1)  3085078BufferSize;
 if (iBlank > iRear)
    {
        iSize = iBlank - iRear;
    }
    else if (iBlank <= iRear)
    {
        iSize = iBufferSize - iRear;
    }

    return iSize;
}

/////////////////////////////////////////////////////////////////////////
// 버퍼 포인터로 외부에서 한방에 읽을수 있는 길이.
// (끊기지 않은 길이)
//
// Parameters: 없음.
// Return: (int)사용가능 용량.
////////////////////////////////////////////////////////////////////////
int CRingBuffer::GetNotBrokenGetSize(void)
{
    int iSize;
    int iBufferSize = _iBufferSize;
    int iRear = _iRear;
    int iFront = _iFront;

    if (iRear >= iFront)
    {
        iSize = iRear - iFront;
    }
    else
    {
        iSize = iBufferSize - iFront;
    }

    return iSize;
}

/////////////////////////////////////////////////////////////////////////
// RearPos 에 데이타 넣음.
//
// Parameters: (char *)데이타 포인터. (int)크기. RearPos 이동.
// Return: (int)넣은 크기.
/////////////////////////////////////////////////////////////////////////
int CRingBuffer::Enqueue(char * chpData, int iSize)
{
    int iBufferSize = _iBufferSize;

    int iFreeSize = GetFreeSize();
    int iSafeSize = GetNotBrokenPutSize();

    int iRear = _iRear;
    int iFront = _iFront;

    int iIndex;    

    if (iFreeSize < iSize)
    {
        iSize = iFreeSize;
        int* p = 0;
        *p = NULL;
    }
    if (iSafeSize >= iSize)
    {
        memcpy(_pBuffer + iRear, chpData, iSize);
        iIndex = (iRear + iSize)  3085078BufferSize;

    else
    {
        memcpy(_pBuffer + iRear, chpData, iSafeSize);
        memcpy(_pBuffer, chpData + iSafeSize, iSize - iSafeSize);
        iIndex = iSize - iSafeSize;       
    }

    _iRear = iIndex;
   
    return iSize;
}

/////////////////////////////////////////////////////////////////////////
// FrontPos 에서 데이타 가져옴. FrontPos 이동.
//
// Parameters: (char *)데이타 포인터. (int)크기.
// Return: (int)가져온 크기.
/////////////////////////////////////////////////////////////////////////
int CRingBuffer::Dequeue(char * chpDest, int iSize)
{
    int iBufferSize = _iBufferSize;

    int iUseSize = GetUseSize();
    int iSafeSize = GetNotBrokenGetSize();

    int iRear = _iRear;
    int iFront = _iFront;
    int iIndex;
    
    if (iUseSize < iSize)
    {
        iSize = iUseSize;
		return 0;
       /* int* p = NULL;
        *p = 0;*/
    }
    if (iSafeSize >= iSize)
    {
        memcpy(chpDest, _pBuffer + iFront, iSize);
        iIndex = (iFront + iSize) _iBufferSize;       

    }
    else
    {
        memcpy(chpDest, _pBuffer + iFront, iSafeSize);
        memcpy(chpDest + iSafeSize, _pBuffer, iSize - iSafeSize);
        iIndex = iSize - iSafeSize;        
    }

    _iFront = iIndex;

    return iSize;
}

/////////////////////////////////////////////////////////////////////////
// FrontPos 에서 데이타 읽어옴. FrontPos 고정.
//
// Parameters: (char *)데이타 포인터. (int)크기.
// Return: (int)가져온 크기.
/////////////////////////////////////////////////////////////////////////
int CRingBuffer::Peek(char * chpDest, int iSize)
{  
    int iUseSize = GetUseSize();
    int iSafeSize = GetNotBrokenGetSize();

    int iBufferSize = _iBufferSize;

    int iRear = _iRear;
    int iFront = _iFront;   

    if (iUseSize < iSize)
        iSize = iUseSize;

    if (iSafeSize >= iSize)
    {
        memcpy(chpDest, _pBuffer + iFront, iSize);

    }
    else
    {
        memcpy(chpDest, _pBuffer + iFront, iSafeSize);
        memcpy(chpDest + iSafeSize, _pBuffer, iSize - iSafeSize);
    }

    return iSize;
}

/////////////////////////////////////////////////////////////////////////
// 버퍼의 포인터 얻음.
//
// Parameters: 없음.
// Return: (char *) 버퍼 포인터.
/////////////////////////////////////////////////////////////////////////
char* CRingBuffer::GetBufferPtr(void)
{
    return _pBuffer;
}

/////////////////////////////////////////////////////////////////////////
// 버퍼의 RearPos 포인터 얻음.
//
// Parameters: 없음.
// Return: (char *) 버퍼 포인터.
/////////////////////////////////////////////////////////////////////////
char* CRingBuffer::GetWriteBufferPtr(void)
{
    return _pBuffer + _iRear;
}

/////////////////////////////////////////////////////////////////////////
// 버퍼의 FrontPos 포인터 얻음.
//
// Parameters: 없음.
// Return: (char *) 버퍼 포인터.
/////////////////////////////////////////////////////////////////////////
char* CRingBuffer::GetReadBufferPtr(void)
{
    return _pBuffer + _iFront;
}

/////////////////////////////////////////////////////////////////////////
// 버퍼의 모든 데이타 삭제.
//
// Parameters: 없음.
// Return: 없음.
/////////////////////////////////////////////////////////////////////////
void CRingBuffer::ClearBuffer(void)
{
    _iRear = 0;
    _iFront = 0;
}

/////////////////////////////////////////////////////////////////////////
// 원하는 길이만큼 읽은 위치에서 사이즈만큼 삭제 / 쓰기 위치 에서 사이즈만큼 이동
//
// Parameters: 없음.
// Return: 없음.
/////////////////////////////////////////////////////////////////////////
void CRingBuffer::RemoveData(int iSize)
{
    int iIndex;
    int iSafeSize = GetNotBrokenGetSize();
    int iFront = _iFront;
    int iBufferSize = _iBufferSize;

	if(GetUseSize() < iSize)
	{
		// 에러 처리

	}

    if (iSafeSize >= iSize)
    {
        iIndex = (iFront + iSize)  3085078BufferSize;

    else
    {
        iIndex = iSize - iSafeSize;
    }

    _iFront = iIndex;
}

/////////////////////////////////////////////////////////////////////////
// 원하는 길이만큼 읽은 위치에서 사이즈만큼 삭제 / 쓰기 위치 에서 사이즈만큼 이동
//
// Parameters: 없음.
// Return: 없음.
/////////////////////////////////////////////////////////////////////////
void CRingBuffer::MoveWritePos(int iSize)
{
    int iIndex;
    int iSafeSize = GetNotBrokenPutSize();
    int iRear = _iRear;
    int iBufferSize = _iBufferSize;

	if(GetFreeSize() < iSize)
	{
		//에러 처리

	}

    if (iSafeSize >= iSize)
    {
        iIndex = (iRear + iSize)  3085078BufferSize;

    else
    {
        iIndex = iSize - iSafeSize;
    }

    _iRear = iIndex;
}
