#include "StdAfx.h"
#include "CSession.h"

CSession::CSession(): m_lIOCount(0)
{
	InitSession();
}


void CSession::InitSession()
{	
	m_bSendCount = 0;
	
	m_bReleaseFlag = false;
}


long CSession::IncrementIO()
{
	return InterlockedIncrement(&m_lIOCount);
}

long CSession::DecrementIO()
{
	return InterlockedDecrement(&m_lIOCount);
}

void CSession::SetIO(long lCount)
{
	m_lIOCount = lCount;
}

bool CSession::SendPost()
{
	//센드 1회 제한
	if(InterlockedExchange(&m_bSendCount,1) == 1)
		return false;

	if(m_SendQ.GetSize() <= 0)
		return false;

	WSABUF wsaBuf[SEND_WSABUF_COUNT];
	
	int iWSABufCount = 0;
	CPacketBuffer* pPacket;
	while(m_SendQ.Dequeue(pPacket))
	{
		wsaBuf[iWSABufCount].buf = pPacket->GetBufferPtr();
		wsaBuf[iWSABufCount].len = pPacket->GetPacketSize();

		m_SendPacketKeepQ.Enqueue(pPacket);

		if(++iWSABufCount == SEND_WSABUF_COUNT)
			break;

	}

	ZeroMemory(&m_RecvOverlapped,sizeof(OVERLAPPED));

	IncrementIO();
	
	if(!m_Sock.Send(wsaBuf,iWSABufCount,&m_RecvOverlapped))
	{
		if(DecrementIO() == 0)
			return false;
	}	

	return true;
	
}

bool CSession::RecvPost()
{

	WSABUF wsaBuf[2];

	int iWSABufCount = 0;

	wsaBuf[iWSABufCount].len = m_RecvBuffer.GetNotBrokenPutSize();
	wsaBuf[iWSABufCount++].buf = m_RecvBuffer.GetWriteBufferPtr();

	INT iLen = m_RecvBuffer.GetFreeSize() - m_RecvBuffer.GetNotBrokenPutSize();
	if(iLen > 0)
	{
		wsaBuf[iWSABufCount].len = iLen;
		wsaBuf[iWSABufCount++].buf = m_RecvBuffer.GetBufferPtr();
	}

	ZeroMemory(&m_RecvOverlapped,sizeof(OVERLAPPED));

	IncrementIO();

	if(!m_Sock.Recv(wsaBuf,iWSABufCount,&m_RecvOverlapped))
	{
		if(DecrementIO() == 0)
			return false;
	}	

	return true;
}
void CSession::RecvBufferAddRear(DWORD Size)
{
	m_RecvBuffer.MoveWritePos(Size);

}
void CSession::RecvBufferRemoveData(DWORD Size)
{
	m_RecvBuffer.RemoveData(Size);

}

OVERLAPPED* CSession::GetSendOverlapped()
{
	return &m_SendOverlapped;
}

OVERLAPPED* CSession::GetRecvOverlapped()
{
	return &m_RecvOverlapped;
}

OVERLAPPED* CSession::GetSendPostOverlapped()
{
	return &m_SendPostOverlapped;
}


void CSession::Disconnect()
{
	m_Sock.Disconnect();

}
