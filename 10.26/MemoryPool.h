#pragma once

#include <malloc.h>
#include <new.h>
#include <assert.h>
#include <process.h>
#include <Windows.h>

//---------------------------------------
// 메모리 풀 ( Pool )
//
// 특정 데이터 (구조체,클레스,변수)를 일정량 할당후 나눠 쓴다.
//
//---------------------------------------
#define df_SAFE_CODE_MEMORYPOOL         0x1234567812345678
template<typename DATA>
class CMemoryPool
{
private:
    struct SBLOCK_NODE
    {
        DATA                Data;
        UINT64			    SafeCode;
        SBLOCK_NODE*		pNextBlock;

        SBLOCK_NODE()
        {
            SafeCode = df_SAFE_CODE_MEMORYPOOL;
            pNextBlock = NULL;
        }        
    };

   
	union TOP_NODE
	{
		LONG64 Aligment;
		struct
		{
			SBLOCK_NODE *pTopNode;
			UINT32 iUniqueNum;

		};

	};

private:
    TOP_NODE*			 m_pTopNode;
    LONG                 m_lAllocCount;
    LONG                 m_lUseCount;
    bool                 m_bPlaceMentNew;

public:
    //---------------------------------------
    // 실제 데이터 메모리 할당.
    //
    //  Parameters: 없음.
    //  Return: (DATA*) 데이터 블럭 포인터 
    //---------------------------------------
    DATA*      Alloc(void);

    //---------------------------------------
    // 사용중인 블럭을 해제한다.
    //
    //  Parameters: (DATA*) 데이터 블럭 포인터 
    //  Return: (bool) 사용중인 블럭을 해제했는지(TRUE), 못했는지 (FALSE)
    //---------------------------------------
    bool	   Free(DATA* pData);

public:
    //---------------------------------------
    // 현재 사용중인 할당 전체 갯수 얻기.
    //
    // Parameters: 없음.
    // Return: (LONG)현재 사용중인 할당 전체 갯수 얻기.
    //---------------------------------------
    LONG		GetAllocCount(void);

    //---------------------------------------
    // 현재 사용중인 할당 갯수 얻기.
    //
    // Parameters: 없음.
    // Return: (LONG)현재 사용중인 Alloc 갯수
    //---------------------------------------
    LONG		GetUseCount(void);

public:
    //---------------------------------------
    // 생성자
    //
    // Parameters: (int) 최대 블럭 갯수 ,(bool) 생성자 호출 여부  
    // Return: (int)사용중인 용량.
    //---------------------------------------
    CMemoryPool(int iBlockNum, bool bPlacementNew = false);

    //---------------------------------------
    // 파괴자
    //
    // Parameters: 없음.  
    // Return: 없음.
    //---------------------------------------
    ~CMemoryPool();
};


//---------------------------------------
// 실제 데이터 메모리 할당. 
//---------------------------------------
template<typename DATA>
inline DATA* CMemoryPool<DATA>::Alloc(void)
{
    DATA* pData;

    LONG   AllocCount = _iAllocCount;
    LONG   UseCountResult = InterlockedIncrement(&_iUseCount);

    //---------------------------------------
    // 할당할 노드가 없는경우 
    //---------------------------------------
    if (UseCountResult > AllocCount)
    { 
        SBLOCK_NODE* pNode = new SBLOCK_NODE;

        pData = (DATA*)pNode;

        InterlockedIncrement(&_iAllocCount);

        return pData;
    }

    //---------------------------------------
    // 할당할 노드가 있는경우만 처리함.
    //---------------------------------------

	TOP_NODE CurTop;
	TOP_NODE NewTop;

    do
    {     



        TopNode.pTopNode  = m_pTopNode->pTopNode;
        TopNode.iUniqueNum = _pTopNode->iUniqueNum;

    }while(!InterlockedCompareExchange128((LONG64*)_pTopNode,_pTopNode->iUniqueNum + 1,(LONG64)TopNode.pTopNode->pNextBlock, (LONG64*)&TopNode));  //_pTopNode->pTopNode = _pTopNode->pTopNode->pNextBlock;
    
    pData = (DATA*)TopNode.pTopNode;

    if (_bPlaceMentNew)
    {
        new(pData)DATA;
    }

    return pData;
}

//---------------------------------------
// 사용중인 블럭을 해제한다.
//---------------------------------------
template<typename DATA>
inline bool CMemoryPool<DATA>::Free(DATA * pData)
{
    SBLOCK_NODE* pNode = (SBLOCK_NODE*)pData;

    if (pNode->SafeCode != df_SAFE_CODE_MEMORYPOOL)
    {
        assert(false);
        return false;
    }

    st_TOP_NODE     TopNode;

    do
    {


		CurTop.Aligment = pTop->Aligment;


		NewTop.iUniqueNum = CurTop.iUniqueNum+1;		
		NewTop.pNode = pNewNode;

		pNewNode->pNext = pTop->pNode;
		


        TopNode.pTopNode = _pTopNode->pTopNode;
        TopNode.iUniqueNum = _pTopNode->iUniqueNum;

        pNode->pNextBlock = TopNode.pTopNode;

    }while (!InterlockedCompareExchange128((LONG64*)_pTopNode,_pTopNode->iUniqueNum + 1, (LONG64)pNode, (LONG64*)&TopNode)); // TopNode.pTopNode = pNode;

    if (_bPlaceMentNew)
    {
        pData->~DATA();
    }

    InterlockedDecrement(&_iUseCount);

    return true;
}

//---------------------------------------
// 현재 사용중인 할당 전체 갯수 얻기.
//
// Parameters: 없음.
// Return: (int)현재 사용중인 Alloc 전체 갯수 얻기.
//---------------------------------------
template<typename DATA>
inline LONG CMemoryPool<DATA>::GetAllocCount(void)
{
    return _iAllocCount;
}

//---------------------------------------
// 현재 사용중인 할당 갯수 얻기.
//
// Parameters: 없음.
// Return: (int)현재 사용중인 Alloc 갯수
//---------------------------------------
template<typename DATA>
inline LONG CMemoryPool<DATA>::GetUseCount(void)
{
    return _iUseCount;
}

//---------------------------------------
// 생성자
//
// Parameters: (int) 최대 블럭 갯수 ,(bool) 생성자 호출 여부  
// Return: (int)사용중인 용량.
//---------------------------------------
template<typename DATA>
inline CMemoryPool<DATA>::CMemoryPool(int iBlockNum, bool bPlacementNew = false)
    :_iUseCount(0)
{
    // 생성자 호출 여부.
    _bPlaceMentNew = bPlacementNew;

    // 총 할당 갯수.
    _iAllocCount = iBlockNum;

    // Top노드 메모리 할당 및 초기값 셋팅.
    _pTopNode = (st_TOP_NODE*)_aligned_malloc(sizeof(st_TOP_NODE), 16);
    _pTopNode->pTopNode = NULL;
    _pTopNode->iUniqueNum = 0;
}

//---------------------------------------
// 파괴자
//
// Parameters: 없음.  
// Return: 없음.
//---------------------------------------
template<typename DATA>
inline CMemoryPool<DATA>::~CMemoryPool()
{  
    if (_pTopNode == NULL)
        return;

    SBLOCK_NODE* pNode = _pTopNode->pTopNode;
    SBLOCK_NODE* pDeleteNode;
    DATA*       pData = (DATA*)((char*)pNode + sizeof(SBLOCK_NODE));

    while (pNode != NULL)
    {
        // 소멸자 호출 여부 
        if (!_bPlaceMentNew)
        {
            pData->~DATA();
        }

        pDeleteNode = pNode;
        pNode = pNode->pNextBlock;
        free(pDeleteNode);
        pData = (DATA*)((char*)pNode + sizeof(SBLOCK_NODE));
    }
}

#define         df_CHUNK_SIZE               200

//---------------------------------------
// 메모리 풀 TLS ( Pool ) 
//
// Chunk를 다루는 메모리풀.
// 쓰레드마다 할당을 해주되 통합관리가 필요함. 
//---------------------------------------
template<typename DATA>
class CMemoryPoolTLS
{
    //---------------------------------------
    // 우리가 다룰 데이터를 관리하는 용도.
    //---------------------------------------   
    class CChunKBlock
    {
    public:
        struct st_DATA_BLOCK
        {
            DATA                 Data;
            CChunKBlock*         pChunkBlock;
            st_DATA_BLOCK*       pNextBlock;

            st_DATA_BLOCK()
            {
                pNextBlock = NULL;
            }
        };

    public:
        st_DATA_BLOCK*                   _pTopNode;
        st_DATA_BLOCK*                   _pTempTop;
        LONG                             _AllocCount;
        LONG                             _FreeCount;

    public:
        //---------------------------------------
        // 실제 데이터 메모리 할당.
        //
        //  Parameters: 없음.
        //  Return: (DATA*) 데이터 블럭 포인터 
        //---------------------------------------
        DATA*      Alloc(void)
        {  
            if (--_AllocCount == 0)
            {
                TlsSetValue(_dwTLSIndex, NULL);
            }

            st_DATA_BLOCK* pTopNode = _pTopNode;            

            DATA* pData = &pTopNode->Data;

            _pTopNode = pTopNode->pNextBlock;     

            return pData;
        }       

        void      Clear(void)
        {
            _pTopNode = _pTempTop;
            _AllocCount = df_CHUNK_SIZE;
            _FreeCount = df_CHUNK_SIZE;
        }

    public:
        //---------------------------------------
        // Chunk 생성자       
        //---------------------------------------
        CChunKBlock()
        {
            _pTopNode = NULL;
            _AllocCount = df_CHUNK_SIZE;
            _FreeCount = df_CHUNK_SIZE;

            // 처음 Chunk 할당.            
            for (int iCnt = 0; iCnt < df_CHUNK_SIZE; ++iCnt)
            {
                st_DATA_BLOCK* pTopNode = _pTopNode;
                _pTempTop = _pTopNode;

                st_DATA_BLOCK* pNode = new st_DATA_BLOCK;
                pNode->pChunkBlock = this;
                pNode->pNextBlock = pTopNode;

                _pTopNode = pNode;
            }
            _pTempTop = _pTopNode;
        }

        //---------------------------------------
        // Chunk 파괴자        
        //---------------------------------------
        ~CChunKBlock()
        {

        }
    };

public:
    static DWORD                                   _dwTLSIndex;                 // TLS Index.
    CMemoryPool<CChunKBlock>*                      _pChunkPool;                 // ChunkPool.    

public:
    //---------------------------------------
    // 실제 데이터 메모리 할당.
    //
    //  Parameters: 없음.
    //  Return: (DATA*) 데이터 블럭 포인터 
    //---------------------------------------
    DATA*      Alloc(void);

    //---------------------------------------
    // 사용중인 블럭을 해제한다.
    //
    //  Parameters: (DATA*) 데이터 블럭 포인터 
    //  Return: (bool) 사용중인 블럭을 해제했는지(TRUE), 못했는지 (FALSE)
    //---------------------------------------
    bool	   Free(DATA* pData);

public:
    //---------------------------------------
    // 현재 사용중인 할당 전체 갯수 얻기.
    //
    // Parameters: 없음.
    // Return: (LONG)현재 사용중인 할당 전체 갯수 얻기.
    //---------------------------------------
    LONG		GetAllocCount(void);

    //---------------------------------------
    // 현재 사용중인 할당 갯수 얻기.
    //
    // Parameters: 없음.
    // Return: (LONG)현재 사용중인 Alloc 갯수
    //---------------------------------------
    LONG		GetUseCount(void);

public:
    //---------------------------------------
    // 생성자
    //
    // Parameters: (int) 최대 블럭 갯수 ,(bool) 생성자 호출 여부  
    // Return: (int)사용중인 용량.
    //---------------------------------------
    CMemoryPoolTLS(int iBlockNum, bool bPlacementNew = false);

    //---------------------------------------
    // 파괴자
    //
    // Parameters: 없음.  
    // Return: 없음.
    //---------------------------------------
    ~CMemoryPoolTLS();
};
template<typename DATA>
DWORD CMemoryPoolTLS<DATA>::_dwTLSIndex = 0;

//---------------------------------------
// MemoryPoolTLS 생성자
//
// Parameters: (int) 최대 블럭 갯수 ,(bool) 생성자 호출 여부  
// Return: (int)사용중인 용량.
//---------------------------------------
template<typename DATA>
inline CMemoryPoolTLS<DATA>::CMemoryPoolTLS(int iBlockNum, bool bPlacementNew = false)
{
    //---------------------------------------
    // TLS Index 구하기.
    //---------------------------------------
    _dwTLSIndex = TlsAlloc();

    //---------------------------------------
    // 메모리풀 생성.
    //---------------------------------------
    _pChunkPool = new CMemoryPool<CChunKBlock>(0, false);
}

//---------------------------------------
// MemoryPoolTLS 파괴자
//
// Parameters: 없음.  
// Return: 없음.
//---------------------------------------
template<typename DATA>
inline CMemoryPoolTLS<DATA>::~CMemoryPoolTLS()
{

}

//---------------------------------------
// Chunk Block 메모리 할당.
//---------------------------------------
template<typename DATA>
inline DATA* CMemoryPoolTLS<DATA>::Alloc(void)
{
    CChunKBlock* pChunk = (CChunKBlock*)TlsGetValue(_dwTLSIndex);

    if (pChunk == NULL)
    {
        pChunk = _pChunkPool->Alloc();

        TlsSetValue(_dwTLSIndex, pChunk);
    }

    DATA* pData = pChunk->Alloc();

    return pData;
}

//---------------------------------------
// Chunk Block 메모리 해제.
//---------------------------------------
template<typename DATA>
inline bool CMemoryPoolTLS<DATA>::Free(DATA* pData)
{    
    CChunKBlock* pChunk = ((CChunKBlock::st_DATA_BLOCK*)pData)->pChunkBlock;

    if (InterlockedDecrement(&pChunk->_FreeCount) == 0)
    {
        pChunk->Clear();

        _pChunkPool->Free(pChunk);
    }

    return TRUE;
}

template<typename DATA>
inline LONG CMemoryPoolTLS<DATA>::GetAllocCount(void)
{
    return _pChunkPool->GetAllocCount();
}

template<typename DATA>
inline LONG CMemoryPoolTLS<DATA>::GetUseCount(void)
{
    return _pChunkPool->GetUseCount();
}