#ifndef SERIALIZATIONBUFFER_H_
#define SERIALIZATIONBUFFER_H_




#include "MemoryPoolLF.h"
//#include "MemoryPoolTLS.h"
//#include "MemoryPoolSRW.h"
#include <time.h>
//#include "MemoryPoolSRWTLS.h"

//#include "profiler.h"

#define df_HEAD_SIZE 5

///사용법 
//1. 메모리풀 전역 선언
//2. 페킷 해더 사이즈 설정
//3. Alloc 사용
//4. putHead 해드 넣기
//5. free
//6. 필요시 addref로 레퍼런스 카운트 증가
//7. 헤드를 빼내올때는 5바이트 공간은 쓰지않는다 그뒤부터 그래서 그냥 인큐로 뽑으면 헤드가 나온다
class CPacketBuffer
{
public:

	// 일단 200되 있음
	enum en_PACKET
	{
		eBUFFER_DEFAULT = 1000,		// 패킷의 기본 버퍼 사이즈.
		ePACKET_MAX_SIZE = 1000,		// 패킷의 최대 사이즈
	};


	CPacketBuffer();
	CPacketBuffer(int iBufferSize);
	//CPacketBuffer(const CPacketBuffer &clSrcPacket);



	virtual ~CPacketBuffer();

protected:

	//////////////////////////////////////////////////////////////////////////
	// 패킷 초기화.
	//
	// 메모리 할당을 여기서 하므로, 함부로 호출하면 안된다. 
	//
	// Parameters: (int)BufferSize.
	// Return: 없음.
	//////////////////////////////////////////////////////////////////////////
	void	Initial(int iBufferSize);
	//////////////////////////////////////////////////////////////////////////
	// 패킷  파괴.
	//
	// Parameters: 없음.
	// Return: 없음.
	//////////////////////////////////////////////////////////////////////////
	void	Release(void);



public:



	//////////////////////////////////////////////////////////////////////////
	// 패킷 청소.
	//
	// Parameters: 없음.
	// Return: 없음.
	//////////////////////////////////////////////////////////////////////////
	void	Clear(void);

	//헤더 넣어줄 사이즈만큼 냅두고 초기화 //2017.12.19추가 iocp다시 만들며 추가
	//void	Clear(int Size);

	//////////////////////////////////////////////////////////////////////////
	// 버퍼 사이즈 얻기.
	//
	// Parameters: 없음.
	// Return: (int)패킷 버퍼 사이즈 얻기.
	//////////////////////////////////////////////////////////////////////////
	//int		GetBufferSize(void);
	//////////////////////////////////////////////////////////////////////////
	// 패킷 실제 총 사이즈
	//////////////////////////////////////////////////////////////////////////
	int		GetPacketSize(void);

	int		GetUseSize(void);

	int	    GetPayLoadSize();

	//내용물 담을 포인터
	//char* GetPayLoadBufferPtr(void);

	//페이로드의 시작 포인터 주소 
	char* GetPayLoadBufferStartPtr();
	//해터의 포인터
//	char* GetHeadBufferPtr(void);

	//char* GetCurrentBufferPtr(void);

	//////////////////////////////////////////////////////////////////////////
	// 버퍼 포인터 얻기.
	//
	// 패킷의 가장 앞에는 헤더 영역이 존재하며, 이는 일반 버퍼로 사용불가.
	//
	// Parameters: 없음.
	// Return: (char *)버퍼 포인터.
	//////////////////////////////////////////////////////////////////////////
	char	*GetBufferPtr(void);
	//////////////////////////////////////////////////////////////////////////
	// 버퍼 Pos 이동. (음수이동은 안됨)
	// GetBufferPtr 함수를 이용하여 외부에서 강제로 버퍼 내용을 수정할 경우 사용. 
	//
	// Parameters: (int) 이동 사이즈.
	// Return: (int) 이동된 사이즈.
	//////////////////////////////////////////////////////////////////////////
	int		MoveWritePos(int iSize);
	int		MoveReadPos(int iSize);
	int		MoveHeadPos(int iSize);

	//////////////////////////////////////////////////////////////////////////
	// 데이타 얻기.
	//
	// Parameters: (char *)Dest 포인터. (int)Size.
	// Return: (int)복사한 사이즈.
	//////////////////////////////////////////////////////////////////////////
	int		GetData(char *chpDest, int iSize);
	int		GetData(WCHAR *chpDest, int iSize);

	//////////////////////////////////////////////////////////////////////////
	// 데이타 삽입.
	//
	// Parameters: (char *)Src 포인터. (int)SrcSize.
	// Return: (int)복사한 사이즈.
	//////////////////////////////////////////////////////////////////////////
	int	PutData(char *chpSrc, int iSrcSize);
	int	PutData(WCHAR *chpSrc, int iSrcSize);


private:

	//헤더 삽입
	void PutHeader(char *chpSrc, int size);
public:
	void PutHeaderShort(int Size);
	//헤더 뽑기
	//void GetHeader(char *chpSrc, int iSrcSize);

	
	static void SetEncodeVar(BYTE PackCode, BYTE Code1, BYTE Code2)
	{
		_PacketCode = PackCode;
		_EncodeCode1 = Code1;
		_EncodeCode2 = Code2;

		srand((unsigned)time(NULL));
	}

	////암호화 - 2018.01.22
	void	Encode();

	//복호화
	bool	Decode();

private:
	//체크섬 구하기
	BYTE GetCheckSum(BYTE* pBuffer, int size);

	void GetByteXOR(BYTE Code, BYTE *pTarget, int size);

	//Code(1byte) - Len(2byte) - Rand XOR Code(1byte) - CheckSum(1byte) - Payload(Len byte)
#pragma pack(push, 1)
	struct st_PackHead
	{
		BYTE PackCode;
		short Len;
		BYTE RandXORCode;
		BYTE CheckSum;

	};
#pragma pack(pop)

public:



	CPacketBuffer& operator<<(char data);
	CPacketBuffer& operator<<(unsigned char data);
	CPacketBuffer& operator<<(short data);
	CPacketBuffer& operator<<(int data);
	CPacketBuffer& operator<<(long data);
	CPacketBuffer& operator<<(unsigned short data);
	CPacketBuffer& operator<<(unsigned int data);
	CPacketBuffer& operator<<(unsigned long data);
	CPacketBuffer& operator<<(float data);
	CPacketBuffer& operator<<(double data);
	CPacketBuffer& operator<<(unsigned __int64 data);
	CPacketBuffer& operator<<(__int64 data);


	CPacketBuffer& operator>>(char &data);
	CPacketBuffer& operator>>(unsigned char &data);
	CPacketBuffer& operator>>(short &data);
	CPacketBuffer& operator>>(int &data);
	CPacketBuffer& operator>>(long &data);
	CPacketBuffer& operator>>(unsigned short &data);
	CPacketBuffer& operator>>(unsigned int &data);
	CPacketBuffer& operator>>(unsigned long &data);
	CPacketBuffer& operator>>(float &data);
	CPacketBuffer& operator>>(double &data);
	CPacketBuffer& operator>>(unsigned __int64 &data);
	CPacketBuffer& operator>>(__int64 &data);

	//-----------------------------------
	// 보내고 끊기 설정 (0 : 끊음 , 1 : 일반
	//-----------------------------------
	void SetEndPacket(int End);
	int  GetEndPacket();





	////////////////////////
	//패킷풀
	////////////////////////

	//안됨 호출이 해결못함
	//static CMemoryPool<CPacketBuffer> *_pMemoryPool;
	//static void MemoryPool(int Size)
	//{ 
	//	_pMemoryPool = new CMemoryPool<CPacketBuffer>(Size);
	//}

	/////////////////////////////사용법//////////////////////
	//1.사용할곳 전역에 CMemoryPool<CPacketBuffer> CPacketBuffer::_pMemoryPool; 선언한다	
	//
	//CPacketBuffer *Packet = CPacketBuffer::Alloc();
	//*Packet << 2;
	//Packet->Free();
	//레퍼런스 추가시 Packet->AddRef();
	//똑같이 Free해주면됨
	/////////////////////////////////////////////////////////////


	//static CMemoryPoolSRW<CPacketBuffer> _pMemoryPool;
	//static CMemoryPool<CPacketBuffer> _pMemoryPool;
	static CMemoryPool<CPacketBuffer> _pMemoryPool;
	//static CMemoryPoolSRWTLS<CPacketBuffer> _pMemoryPool;

	//	static void SetHeadSize(int size) {  }
		//HeadSize = size;

	static LONG AllocCount;


	void Pick(int Offset, char* data, int Size);


	static CPacketBuffer *Alloc()
	{
		
		CPacketBuffer * packet = _pMemoryPool.Alloc();
		

		packet->Clear();
		packet->AddRef();

		InterlockedIncrement(&AllocCount);

		return packet;
	}


	void Free(void)
	{


		if (InterlockedDecrement(&_RefCount) == 0)
		{
		
			InterlockedDecrement(&AllocCount);
		//	delete this;
			
			_pMemoryPool.Free(this);
			

			return;


		}

	
	}



	void AddRef(void)
	{

		InterlockedIncrement(&_RefCount);
	}

	static int GetMemoryPoolUseCount() { return _pMemoryPool.GetUseCount(); }
	static int GetMemoryPoolAllocCount() { return _pMemoryPool.GetAllocCount(); }



private:

	//헤드 세팅 했는가
	BYTE _byIsHead;

	int EndPacket;

public:

	int _iFront;
	int _iRear;

	char *_Buffer;

	

	//레퍼런스 카운트
	long _RefCount;



	//	int _HeadSize;

	static BYTE _PacketCode;
	static BYTE _EncodeCode1;
	static BYTE _EncodeCode2;



};


#endif // !SERIALIZATIONBUFFER_H_

