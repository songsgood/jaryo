#ifndef _QUEUELF_H_
#define _QUEUELF_H_

#include "MemoryPoolLF.h"


template<typename T>
class CQueueLF
{
private:

	struct st_Node
	{
		T Data;
		st_Node *pNext;
	};

	struct st_Head_Node
	{
		st_Node *pNode;
		INT64 iUniqueNum;

	};

	//struct st_Debug
	//{
	//	//int s;
	//	char Name[15];
	//	st_Node *pHead;
	//	st_Node *pTail;

	//	st_Node *pTailNext;
	//	st_Node *pHeadNext;

	//	st_Node *pCurHead;
	//	st_Node *pCurTail;

	//	st_Node *pCurTailNext;
	//	st_Node *pCurHeadNext;
	//	st_Node *NewNode;
	//	st_Node *NewNodeNext;

	//};

private:




	st_Head_Node *pHead;
	st_Head_Node *pTail;


	//long iMaxSize;
	long iCurrentSize;


	//st_Debug arrDebug[5000000];

	//long count;


	//CRITICAL_SECTION cs1;


	//long unique;



public:
	CQueueLF()
	{	
		pool = new CMemoryPool<st_Node>();
		st_Node *pNewNode = pool->Alloc();

		pNewNode->Data = NULL;
		pNewNode->pNext = NULL;

		pHead = (st_Head_Node*)_aligned_malloc(sizeof(st_Head_Node), 16);
		pTail = (st_Head_Node*)_aligned_malloc(sizeof(st_Head_Node), 16);

		pHead->pNode = pNewNode;
		pHead->iUniqueNum = 0;

		pTail->pNode = pNewNode;
		pTail->iUniqueNum = 0;

		iCurrentSize = 0;

	}


	~CQueueLF()
	{
		free(pHead);
		free(pTail);
	}

	//int debugsize();


	bool Enqueue(T t);
	bool Dequeue(T &t);
	int Size();
	void Clear();

	bool Pick(T &t, long index);



	//void Lock();
	//void UnLock();

	//테스트용으로 public
private:

	CMemoryPool<st_Node> *pool;




};
//template<typename T>
//int CQueueLF<T>::debugsize()
//{
//	int count = 0;
//	st_Node *cur = pHead->pNode;
//	while (true)
//	{
//		cur = cur->pNext;
//		count++;
//
//		if (cur->pNext == NULL)
//			return count;
//	}
//}

template<typename T>
bool CQueueLF<T>::Enqueue(T t)
{

	st_Node *pNewNode = pool->Alloc();

	pNewNode->Data = t;
	pNewNode->pNext = NULL;

	st_Head_Node curTail;

	//strcpy(arrDebug[count].Name, "alloc");
	//arrDebug[count].pHead = pHead->pNode;
	//arrDebug[count].pTail = pTail->pNode;
	//arrDebug[count].pHeadNext = pHead->pNode->pNext;
	//arrDebug[count].pTailNext = pTail->pNode->pNext;

	//arrDebug[count].pCurHead = NULL;
	//arrDebug[count].pCurHeadNext = NULL;
	//arrDebug[count].pCurTail = NULL;
	//arrDebug[count].pCurTailNext = NULL;

	//arrDebug[count].NewNode = pNewNode;
	//arrDebug[count].NewNodeNext = pNewNode->pNext;
	//count++;


	while (true)
	{

		curTail.pNode = pTail->pNode;
		curTail.iUniqueNum = pTail->iUniqueNum;

		//tail이 tail을 찌르는 상황
		//curTaill.pNext->pNode 

		if (NULL == InterlockedCompareExchange64((LONG64*)&pTail->pNode->pNext, (LONG64)pNewNode, NULL))
		{
			/*	strcpy(arrDebug[count].Name, "push");
				arrDebug[count].pHead = pHead->pNode;
				arrDebug[count].pTail = pTail->pNode;
				arrDebug[count].pHeadNext = pHead->pNode->pNext;
				arrDebug[count].pTailNext = pTail->pNode->pNext;

				arrDebug[count].pCurHead = NULL;
				arrDebug[count].pCurHeadNext = NULL;
				arrDebug[count].pCurTail = curTail.pNode;
				arrDebug[count].pCurTailNext = curTail.pNode->pNext;*/

				/*		arrDebug[count].NewNode = pNewNode;
						arrDebug[count].NewNodeNext = pNewNode->pNext;
						count++;*/


						////같을경우 tail을 옮긴다 성공 여부와 관계없이 그냥 넘어감			
			InterlockedCompareExchange128((LONG64*)pTail, pTail->iUniqueNum + 1, (LONG64)pNewNode, (LONG64*)&curTail);

			break;
		}
		//다음 주자가 tail을 옮긴다
		else
		{


			/////////////////////////////////////////////////////////////
			//Taill ABA 현상 tail 이 NULL이나 자기자신을 찌른다
			//1. 한쓰레드가 curTail을받고 여기까지 넘어온다
			//2. 그리고 다른 스레드가 값을 빼고 넣고하여 curTaill과 같은 메모리의 값을 할당받은뒤 pNext를 NULL로 초기화한다
			//3. 그후 처음 스레드가 아래의 CAS를 검사후 tail의 넥스트를 찌르게되면 NULL값을 찌르거나 curTail의 넥스트를 찌르게되면 자기 자신을 찌르게된다
			/////////////////////////////////////////////////////////////
			//EnterCriticalSection(&cs1);
			if (0 != InterlockedCompareExchange128((LONG64*)pTail, pTail->iUniqueNum + 1, (LONG64)pTail->pNode->pNext, (LONG64*)&curTail))
			{
				/*	strcpy(arrDebug[count].Name, "pushTail");-
					arrDebug[count].pHead = pHead->pNode;
					arrDebug[count].pTail = pTail->pNode;
					arrDebug[count].pHeadNext = pHead->pNode->pNext;
					arrDebug[count].pTailNext = pTail->pNode->pNext;

					arrDebug[count].pCurHead = NULL;
					arrDebug[count].pCurHeadNext = NULL;
					arrDebug[count].pCurTail = curTail.pNode;
					arrDebug[count].pCurTailNext = curTail.pNode->pNext;


					arrDebug[count].NewNode = pNewNode;
					arrDebug[count].NewNodeNext = pNewNode->pNext;
					count++;*/
			}

		}

	}

	InterlockedIncrement(&iCurrentSize);


	return true;
}

template<typename T>
bool CQueueLF<T>::Dequeue(T &t)
{

	if (iCurrentSize == 0)
		return false;

	//int Size = InterlockedDecrement(&iCurrentSize);
	//if (Size <= 0)
	//{
	//	InterlockedIncrement(&iCurrentSize);
	///*	if (0 < InterlockedIncrement(&iCurrentSize))
	//		return Pop(t);*/

	//	return false;
	//}



	st_Head_Node curHead;

	st_Head_Node curTail;


	while (true)
	{

		curHead.pNode = pHead->pNode;
		curHead.iUniqueNum = pHead->iUniqueNum;


		curTail.pNode = pTail->pNode;
		curTail.iUniqueNum = pTail->iUniqueNum;

		//////////////////////////////////////////////////////////////////////////////////////
		//NextHead가 null이 되는 상황
		//1. 여기서 한 쓰레드가 멈춰있다
		//2. 두번째 스레드가 curHead같은 값을 가지고 프리까지 끝내버린후 다시 푸시로 메모리풀에서 나온다
		//3. 그값이 next를 null로 찌르면 curhead의 Next는 NUll이된다
		//4. 그후 처음의 쓰레드가 nextHead에 값을 넣을려고하면 이미 curHead의 넥스트는 NULL을 찌르고있다.
		///////////////////////////////////////////////////////////////////////////////////////
		st_Node *NextHead = curHead.pNode->pNext;


		if (NextHead == NULL)
			continue; //성능테스트


		if (curHead.pNode == NULL)
		{
			int *p = NULL;
			*p = 0;
			return false;
		}

		//헤드와 테일이 같을때
		if (curHead.pNode == curTail.pNode)
		{
			//뺄것이 없음		
			if (NextHead == NULL)
				return false;

			if (0 != InterlockedCompareExchange128((LONG64*)pTail, pTail->iUniqueNum + 1, (LONG64)pTail->pNode->pNext, (LONG64*)&curTail))
			{
				/*strcpy(arrDebug[count].Name, "PopTailpush");
				arrDebug[count].pHead = pHead->pNode;
				arrDebug[count].pTail = pTail->pNode;
				arrDebug[count].pHeadNext = pHead->pNode->pNext;
				arrDebug[count].pTailNext = pTail->pNode->pNext;

				arrDebug[count].pCurHead = curHead.pNode;
				arrDebug[count].pCurHeadNext = curHead.pNode->pNext;
				arrDebug[count].pCurTail = curTail.pNode;
				arrDebug[count].pCurTailNext = curTail.pNode->pNext;


				arrDebug[count].NewNode = NULL;
				arrDebug[count].NewNodeNext = NULL;
				count++;*/

			}
		}
		else
		{
			//백업
			///////////////////////////////////////////////////////////////
			//1.curHead가 받아진 상태에서 다른쓰레드에서 curHead를 이용하여 반환후
			//2.다시 curhead를 재할당 받아 curhead에 다른 값을 담을수있다
			///////////////////////////////////////////////////////////////
			t = NextHead->Data;

			if (InterlockedCompareExchange128((LONG64*)pHead, pHead->iUniqueNum + 1, (LONG64)NextHead, (LONG64*)&curHead) != 0)
			{
				/*	strcpy(arrDebug[count].Name, "Pop");
					arrDebug[count].pHead = pHead->pNode;
					arrDebug[count].pTail = pTail->pNode;
					arrDebug[count].pHeadNext = pHead->pNode->pNext;
					arrDebug[count].pTailNext = pTail->pNode->pNext;

					arrDebug[count].pCurHead = curHead.pNode;
					arrDebug[count].pCurHeadNext = curHead.pNode->pNext;
					arrDebug[count].pCurTail = curTail.pNode;
					arrDebug[count].pCurTailNext = curTail.pNode->pNext;


					arrDebug[count].NewNode = NULL;
					arrDebug[count].NewNodeNext = NULL;
					count++;*/

				InterlockedDecrement(&iCurrentSize);

				pool->Free(curHead.pNode);

				return true;
			}
		}
	}

	return false;

}



template<typename T>
int CQueueLF<T>::Size()
{
	return iCurrentSize;
}

template<typename T>
void CQueueLF<T>::Clear()
{
	iCurrentSize = 0;

}

template<typename T>
bool CQueueLF<T>::Pick(T &t, long index)
{
	int PickPos = 0;
	st_Node *pNode = pHead->pNode;
	while (true)
	{
		if (pNode->pNext == NULL)
			return false;
		else if (index == PickPos)
		{
			t = pNode->pNext->Data;
			break;
		}


		pNode = pNode->pNext;
		PickPos++;

	}

	return true;
}

#endif 