#ifndef _LISTQUEUE_H_
#define _LISTQUEUE_H_

#include "MemoryPool.h"

template <class DATA>
class CListQueue
{
public:
	CListQueue();
	~CListQueue();

	void Enqueue(DATA data);
	bool Dequeue(DATA &data);

	int GetSize(){return iNodeCount};

private:

	struct Node
	{
		DATA Data;
		Node *pNext;

	};

	CMemoryPool<Node> pNodePool;
	
	Node *pFront;
	Node *pRear;

	int iNodeCount;
};

template <class DATA>
CListQueue::CListQueue()
{
	Node *Dummy = pNodePool.Alloc();
	Dummy->Data = NULL;

	pFront = NULL;
	pRear  = NULL;

	iNodeCount = 0;

}

template <class DATA>
CListQueue::~CListQueue()
{
}

template <class DATA>
void CListQueue::Enqueue(DATA data)
{
	Node *pNode = pNodePool.Alloc();

	pNode->Data  =  data;
	

	if(iNodeCount == 0)
	{
		pNode->pNext = NULL;

		
		pRear  = pNode;
		pFront = pNode;
	
	}
	else
	{
		pNode->pNext = pRear;
		pRear = pNode;
	}

	++iNodeCount;
}

template <class DATA>
bool CListQueue::Dequeue(DATA &data)
{
	if (pFront == pRear)
		return false;

	Node *pNode = pFront;

	data = pFront->Data;
	pFront = pFront->pNext;

	pNodePool.Free(pNode);

//	ire

}

#endif // !_LISTQUEUE_H_
