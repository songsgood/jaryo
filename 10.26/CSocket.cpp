#include "StdAfx.h"

#include "CSocket.h"


SOCKET CSocket::GetSocket()
{
	return m_hSocket;
}

void CSocket::SetPort(unsigned short usPort)
{
	m_usPort = usPort;
}


unsigned short CSocket::GetPort()
{
	return m_usPort;
}

bool CSocket::Connect()
{



	ZeroMemory(&m_SocketAddr, sizeof(SOCKADDR_IN));

	m_SocketAddr.sin_family = AF_INET;
	m_SocketAddr.sin_addr.s_addr = inet_addr(m_sAddr);
	m_SocketAddr.sin_port = htons((u_short)m_usPort);
	

	if(connect(m_hSocket, (SOCKADDR*)&m_SocketAddr, sizeof(SOCKADDR_IN)) == SOCKET_ERROR)
	{
		DWORD dwErr = WSAGetLastError();
		//에러 처리 로그
		return FALSE;
	}

	return TRUE;
}


bool CSocket::Send(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped)
{
	DWORD dwFlags = 0;
	DWORD dwNumberOfBytesRecvd = 0;

	if(!WSASend(m_hSocket,buffer,Size,&dwNumberOfBytesRecvd,dwFlags,pOverlapped,NULL))
	{
		if(WSAGetLastError() != ERROR_IO_PENDING)
		{
			// 로그

		}

		return false;		

	}

	return true;

}
bool CSocket::Recv(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped)
{
	DWORD dwFlags = 0;
	DWORD dwNumberOfBytesRecvd = 0;

	if(!WSARecv(m_hSocket,buffer,Size,&dwNumberOfBytesRecvd,&dwFlags,pOverlapped,NULL))
	{
		if(WSAGetLastError() != ERROR_IO_PENDING)
		{
			// 로그
		
		}

		return false;		

	}
	
	return true;
}


void CSocket::Disconnect()
{
	shutdown(m_hSocket,SD_BOTH);
}