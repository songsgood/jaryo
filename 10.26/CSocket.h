#pragma once


#include <Winsock2.h>

class CSocket
{
public:
	CSocket();
	virtual ~CSocket();

	SOCKET GetSocket();
	
	bool Connect();
	void Disconnect();

	bool Send(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped);
	bool Recv(WSABUF *buffer,int Size,OVERLAPPED* pOverlapped);

	void SetPort(unsigned short usPort);
	unsigned short GetPort();

private:	

	//---------------------------
	// ���� ����
	//---------------------------
	SOCKADDR_IN		m_SocketAddr;

	char			m_sAddr[16];
	unsigned short  m_usPort;

	
	SOCKET			m_hSocket;

}; 