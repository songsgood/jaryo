include "StdAfx.h"

include "CSocket.h"


SOCKET CSocket::GetSocket()
{
	return m_hSocket;
}

void CSocket::SetPort(unsigned short usPort)
{
	m_usPort = usPort;
}


unsigned short CSocket::GetPort()
{
	return m_usPort;
}

bool CSocket::Connect()
{



	ZeroMemory(&m_SocketAddr, sizeof(SOCKADDR_IN));

	m_SocketAddr.sin_family = AF_INET;
	m_SocketAddr.sin_addr.s_addr = inet_addr(m_sAddr);
	m_SocketAddr.sin_port = htons((u_short)m_usPort);
	

	if(connect(m_hSocket, (SOCKADDR*)&m_SocketAddr, sizeof(SOCKADDR_IN)) == SOCKET_ERROR)
	{
		DWORD dwErr = WSAGetLastError();
		return FALSE;
	}

	return TRUE;
}

void CSocket::Disconnect()
{
	shutdown(m_hSocket,SD_BOTH);
}