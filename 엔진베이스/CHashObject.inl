/******************************************************************************
	CHashObject.inl
******************************************************************************/


inline void CHashObject::SetHashIndex(UINT uiHashIndex)
{
	m_uiHashIndex = uiHashIndex;
}


inline UINT volatile CHashObject::GetHashIndex()
{
	// 2010.01.11
	// Modify for Migration 2005
	// ktKim
	return (*(UINT volatile*) &m_uiHashIndex);
	// End
}


///////////////////////////////////////////////////////////////////////////////


inline void CHashObject::SetOwner(void* pOwner)
{
	m_pOwner = pOwner;
}


inline void* CHashObject::GetOwner()
{
	return m_pOwner;
}


///////////////////////////////// End of File /////////////////////////////////
