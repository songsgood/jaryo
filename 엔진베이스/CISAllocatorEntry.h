/******************************************************************************
	CISAllocatorEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CInterlockedStack.h"
qinclude "CInterlockedBool.h"


///////////////////////////////////////////////////////////////////////////////


class CISAllocatorEntry;


///////////////////////////////////////////////////////////////////////////////


struct IS_ALLOCATOR_ENTRY : public INTERLOCKED_STACK_ENTRY {
	IS_ALLOCATOR_ENTRY();
	CISAllocatorEntry* This;
};


typedef IS_ALLOCATOR_ENTRY* PIS_ALLOCATOR_ENTRY;


///////////////////////////////////////////////////////////////////////////////


class CISAllocatorEntry  
{
public:
	CISAllocatorEntry();
	virtual ~CISAllocatorEntry() { }

	virtual CISAllocatorEntry* AllocSingleEntry() = 0;

	PINTERLOCKED_STACK_ENTRY GetStackEntry();

	BOOL SetAllocated();
	BOOL SetFreed();
	BOOL IsFreed();

	virtual void InitializeInstance() { }
	virtual void CleanupInstance() { }

	static void DeleteAllEntry();


protected:
	void AddToAllList();


protected:
	IS_ALLOCATOR_ENTRY m_AllocatorEntry;
	CInterlockedBool m_ibFreed;
	static PINTERLOCKED_STACK_ENTRY m_pAllListFirst;
	IS_ALLOCATOR_ENTRY m_AllListNext;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CISAllocatorEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
