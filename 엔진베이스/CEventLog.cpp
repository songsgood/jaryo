/******************************************************************************
	CEventLog.cpp
******************************************************************************/


//////////////////////////////////////////////////////////////////////////////


qinclude "stdafx.h"
qinclude "CEventLog.h"


//////////////////////////////////////////////////////////////////////////////


BOOL CEventLog::Install(DWORD dwTypesSupported/* = EVENTLOG_INFORMATION_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_ERROR_TYPE*/, 
						PCTSTR pszEventMsgFilePaths/* = NULL*/, PCTSTR pszParameterMsgFilePaths/* = NULL*/, 
						DWORD dwCategoryCount/* = 0*/, PCTSTR pszCategoryMsgFilePaths/* = NULL*/) 
{
	if (0 == (dwTypesSupported & (EVENTLOG_INFORMATION_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_ERROR_TYPE))) return FALSE;

	BOOL fOk = TRUE;
	TCHAR szSubKey[_MAX_PATH + 1];
	wsprintf(szSubKey, TEXT("System\\CurrentControlSet\\Services\\EventLog\\Application\\�4"), m_szAppName);

HKEY hkey = NULL;
	__try 
	{
		LONG lRet;
		DWORD dwDisposition;
		
		lRet = ::RegCreateKeyEx(HKEY_LOCAL_MACHINE, szSubKey, 0, NULL, 
			REG_OPTION_NON_VOLATILE, KEY_SET_VALUE, NULL, &hkey, &dwDisposition);
		if (lRet != NO_ERROR) __leave;

		lRet = ::RegSetValueEx(hkey, TEXT("TypesSupported"), 0, REG_DWORD, 
			(PBYTE) &dwTypesSupported, sizeof(dwTypesSupported));
		if (lRet != NO_ERROR) __leave;

		TCHAR szModulePathname[MAX_PATH];
		::GetModuleFileName(NULL, szModulePathname, sizeof(szModulePathname)/sizeof(szModulePathname[0]));

		if (pszEventMsgFilePaths == NULL) 
			pszEventMsgFilePaths = szModulePathname;
		lRet = ::RegSetValueEx(hkey, TEXT("EventMessageFile"), 0, REG_EXPAND_SZ, 
			(PBYTE) pszEventMsgFilePaths, (lstrlen(pszEventMsgFilePaths) + 1) * sizeof(TCHAR));
		if (lRet != NO_ERROR) __leave;

		if (pszParameterMsgFilePaths == NULL) 
			pszParameterMsgFilePaths = szModulePathname;
		lRet = ::RegSetValueEx(hkey, TEXT("ParameterMessageFile"), 0, REG_EXPAND_SZ,  
			(PBYTE) pszParameterMsgFilePaths, 
			(lstrlen(pszParameterMsgFilePaths) + 1) * sizeof(TCHAR));
		if (lRet != NO_ERROR) __leave;

		if (dwCategoryCount > 0) 
		{
			if (pszCategoryMsgFilePaths == NULL) 
				pszCategoryMsgFilePaths = szModulePathname;
			lRet = ::RegSetValueEx(hkey, TEXT("CategoryMessageFile"), 0, 
				REG_EXPAND_SZ,  (PBYTE) pszCategoryMsgFilePaths, 
				(lstrlen(pszCategoryMsgFilePaths) + 1) * sizeof(TCHAR));
			if (lRet != NO_ERROR) __leave;

			lRet = ::RegSetValueEx(hkey, TEXT("CategoryCount"), 0, REG_DWORD, 
				(PBYTE) &dwCategoryCount, sizeof(dwCategoryCount));
			if (lRet != NO_ERROR) __leave;
		}
		fOk = TRUE;
	}
	__finally 
	{
		if (hkey != NULL) ::RegCloseKey(hkey);
	}
	return(fOk);
}


BOOL CEventLog::Uninstall() 
{
	TCHAR szSubKey[_MAX_PATH + 1];
	wsprintf(szSubKey, TEXT("System\\CurrentControlSet\\Services\\EventLog\\Application\\�4"), m_szAppName);
return(NO_ERROR == ::RegDeleteKey(HKEY_LOCAL_MACHINE, szSubKey));
}


//////////////////////////////////////////////////////////////////////////////


BOOL CEventLog::ReportEvent(LPCTSTR lpString, WORD wType/* = EVENTLOG_ERROR_TYPE*/)
{
	TCHAR szLogMessage[2048];
	
	wsprintf(szLogMessage, "�4", lpString);

return (ReportEvent(wType, 0, EL_MSG_ERROR, CEventLog::REUSER_NOTAPPLICABLE, 1, (PCTSTR *)&szLogMessage));
}


BOOL CEventLog::ReportEvent(WORD wType, WORD wCategory/* = 0*/, DWORD dwEventID/* = EL_MSG_ERROR*/, 
	REPORTEVENTUSER reu/* = REUSER_NOTAPPLICABLE*/, WORD wNumStrings/* = 0*/, PCTSTR* pStrings/* = NULL*/, 
	DWORD dwDataSize/* = 0*/, PVOID pvRawData/* = NULL*/) 
{
	BOOL fOk = TRUE;
	DWORD dwErr;

	if (m_hEventLog == NULL) 
	{
		m_hEventLog = ::RegisterEventSource(NULL, m_szAppName);
	}
	if (m_hEventLog != NULL) 
	{
		PSID psidUser = NULL;
		if (reu != REUSER_NOTAPPLICABLE) {
			HANDLE hToken;
			if (REUSER_SERVICE == reu)
				fOk = ::OpenProcessToken(::GetCurrentProcess(), TOKEN_QUERY, &hToken);
			else 
				fOk = ::OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, TRUE, 
					&hToken);
			if (fOk) 
			{
				BYTE bTokenUser[1024];
				PTOKEN_USER ptuUserSID = (PTOKEN_USER) bTokenUser;
				DWORD dwReturnLength;
				::GetTokenInformation(hToken, TokenUser, ptuUserSID, sizeof(bTokenUser), &dwReturnLength);  
				::CloseHandle(hToken);
				psidUser = ptuUserSID->User.Sid;
			}
		}

		fOk = fOk && ::ReportEvent(m_hEventLog, wType, wCategory, dwEventID, 
			psidUser, wNumStrings, dwDataSize, pStrings, pvRawData);
		if (!fOk) dwErr = GetLastError();
	}
	return(fOk);
}


///////////////////////////////// End of File /////////////////////////////////