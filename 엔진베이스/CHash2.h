/******************************************************************************
	CHash2.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CDoublyLinkedList2.h"


///////////////////////////////////////////////////////////////////////////////


struct HASH_ENTRY_2 : public DOUBLY_LIST_ENTRY_2
{
};


///////////////////////////////////////////////////////////////////////////////


class CHash2  
{
public:
	typedef u_int_32 HASH_SIZE;
	typedef HASH_SIZE HASH_INDEX;


public:
	CHash2();
	CHash2(HASH_SIZE HashSize);
	virtual ~CHash2();


public:
	void Initialize(HASH_SIZE HashSize);

	HASH_SIZE GetHashSize();

	void Add(HASH_INDEX HashIndex, HASH_ENTRY_2* pHashEntry);
	void Remove(HASH_INDEX HashIndex, HASH_ENTRY_2* pHashEntry);

	HASH_ENTRY_2* GetPartFirst(HASH_INDEX HashIndex);
	HASH_ENTRY_2* GetPartNext(HASH_INDEX HashIndex, HASH_ENTRY_2* pHashEntry);

	HASH_ENTRY_2* GetFirst(HASH_INDEX* pHashIndex);
	HASH_ENTRY_2* GetNext(HASH_INDEX* pHashIndex, HASH_ENTRY_2* pHashEntry);

	bool IsPartEmpty(HASH_INDEX HashIndex);
	bool IsEmpty();


protected:
	CDoublyLinkedList2* m_pHashObjectList;
	HASH_SIZE m_HashSize;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CHash2.inl"


///////////////////////////////// End of File /////////////////////////////////
