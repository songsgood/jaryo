/******************************************************************************
	CIOCPClientSocket.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CClientSocket.h"
qinclude "CIOCPSocketReceiveBuffer.h"
qinclude "CIOCPSocketSendBufferQueue.h"
qinclude "CSocketIOCPOverlapped.h"
qinclude "CAcceptExSockAddrBuffer.h"
qinclude "CInterlockedBool.h"


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketClient;
class CPacketCoder;
class CIOCPSocketSendBuffer;
class CIOCPSocketReceiveBuffer;


///////////////////////////////////////////////////////////////////////////////


class CIOCPClientSocket : public CClientSocket  
{
protected:
	typedef void (CSocketIOCPOverlapped::*FP_SET_OPCODE)(OPCODE);
	typedef BOOL (CWSASocket::*FP_WSASEND)(LPWSABUF, DWORD, LPDWORD, DWORD, LPWSAOVERLAPPED, LPWSAOVERLAPPED_COMPLETION_ROUTINE);
	typedef UINT (CQueueBuffer::*FP_GET_FREESIZE)();
	typedef void (CQueueBuffer::*FP_MOVE_DATA_TOBEGIN)();
	typedef PBYTE (CQueueBuffer::*FP_GET_TAIL)();
	typedef BOOL (CWSASocket::*FP_WSARECV)(LPWSABUF, DWORD, LPDWORD, LPDWORD, LPWSAOVERLAPPED, LPWSAOVERLAPPED_COMPLETION_ROUTINE);


public:
	CIOCPClientSocket(CIOCPSocketClient* pIOCPSocketClient, CPacketCoder* pPacketCoder);
	virtual ~CIOCPClientSocket() { }

	void InitializeInstance();
	void CleanupInstance();

	CAcceptExSockAddrBuffer* GetSockAddrBuffer();

	DWORD GetLastSuccessfulIOTick();

	void AddToSendQueue(PBYTE pData, UINT uiDataSize);
	void AddToSendQueue(CIOCPSocketSendBuffer* pBeginBuffer, CIOCPSocketSendBuffer* pEndBuffer);
	CIOCPSocketReceiveBuffer* GetReceiveBuffer();

	BOOL PostAcceptEx();
	BOOL PostWSASend();
	BOOL PostWSAReceive();
	BOOL DisconnectClient();

	virtual void OnReceived(DWORD dwReceivedDataSize);
	virtual void OnSent(DWORD dwSentDataSize);

	BOOL GetRemoteAddress(BYTE pucRemoteIP[4]);
	BOOL GetRemotePort(unsigned short& usPort);


protected:
	BOOL PostWSASend(FP_SET_OPCODE pSetOpCode, FP_WSASEND pWSASend, WSABUF* pWSABuf, DWORD dwWSABufCount);
	BOOL PostWSAReceive(FP_GET_FREESIZE pGetFreeSize, FP_MOVE_DATA_TOBEGIN pMoveDataToBegin, FP_GET_TAIL pGetTail, FP_SET_OPCODE pSetOpCode, FP_WSARECV pWSARecv);


protected:
	CIOCPSocketReceiveBuffer m_ReceiveBuffer;
	CIOCPSocketSendBufferQueue m_SendBufferQueue;

	CSocketIOCPOverlapped m_SendOverlapped, m_ReceiveOverlapped;

	CAcceptExSockAddrBuffer m_SockAddrBuffer;

	CInterlockedBool m_ibSending, m_ibClosing;

	const UINT m_uiMaxSendDataSize, m_uiMinReceiveBufferFreeSize;

	DWORD m_dwLastSuccessfulIOTick;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPClientSocket.inl"


///////////////////////////////// End of File /////////////////////////////////
