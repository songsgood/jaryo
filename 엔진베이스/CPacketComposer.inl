/******************************************************************************
	CPacketComposer.inl
******************************************************************************/


inline CPacketComposer::CPacketComposer(unsigned short usCommand) :
	m_usPacketSize(8)
{
	Initialize(usCommand);
}


///////////////////////////////////////////////////////////////////////////////


inline unsigned short CPacketComposer::GetCommand()
{
	return m_usCommand;
}


///////////////////////////////////////////////////////////////////////////////


inline PBYTE CPacketComposer::GetTail()
{
	return (GetPacketBuffer() + m_usPacketSize);
}


inline void CPacketComposer::DataAdded(UINT uiDataSize)
{
	m_usPacketSize = (unsigned short) (m_usPacketSize + uiDataSize);
}


///////////////////////////////////////////////////////////////////////////////


inline void CPacketComposer::AddByPointer(PBYTE pData, UINT uiDataSize)
{
	Add(pData, uiDataSize);
}


///////////////////////////////////////////////////////////////////////////////

inline void CPacketComposer::Finalize()
{
	*((unsigned short*) (m_PacketBuffer + 6)) = (unsigned short) (m_usPacketSize - 8);
}


///////////////////////////////////////////////////////////////////////////////


inline PBYTE CPacketComposer::GetPacketBuffer()
{
	return m_PacketBuffer;
}


inline PBYTE CPacketComposer::GetBodyBuffer()
{
	return (m_PacketBuffer + 8);
}


inline UINT CPacketComposer::GetPacketSize()
{
	return m_usPacketSize;
}


inline UINT CPacketComposer::GetBodySize()
{
	return (GetPacketSize() - 8);
}


inline UINT CPacketComposer::GetPacketBufferSize()
{
	return (sizeof(m_PacketBuffer));
}


inline UINT CPacketComposer::GetFreeSize()
{
	return (sizeof(m_PacketBuffer) - m_usPacketSize);
}


///////////////////////////////// End of File /////////////////////////////////
