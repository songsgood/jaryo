/******************************************************************************
	CISLAllocatorEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CInterlockedSList.h"


///////////////////////////////////////////////////////////////////////////////


class CISLAllocatorEntry : public INTERLOCKED_SLIST_ENTRY  
{
public:
	CISLAllocatorEntry() { }
	virtual ~CISLAllocatorEntry() { }

	virtual void InitializeInstance() { }
	virtual void CleanupInstance() { }
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CISLAllocatorEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
