/******************************************************************************
	CReceivePacketBufferBase.inl
******************************************************************************/


inline void CReceivePacketBufferBase::Clear()
{
	Empty();
	SeekToBegin();
}


///////////////////////////////////////////////////////////////////////////////

inline void CReceivePacketBufferBase::SetCommand(unsigned short usCommand)
{
	m_usCommand = usCommand;
}


inline UINT CReceivePacketBufferBase::GetSequenceNumber()
{
	return m_uiSequenceNumber;
}


inline void CReceivePacketBufferBase::SetSequenceNumber(UINT uiSequenceNumber)
{
	m_uiSequenceNumber = uiSequenceNumber;
}


///////////////////////////////////////////////////////////////////////////////


inline void CReceivePacketBufferBase::SeekToBegin()
{
	m_uiCurrentReadPosition = m_uiHead;
}


///////////////////////////////////////////////////////////////////////////////


inline void CReceivePacketBufferBase::ReadNTString(TCHAR* pBuffer, UINT uiBufferSize)
{
	m_uiCurrentReadPosition += GetString(m_uiCurrentReadPosition, pBuffer, uiBufferSize);
}


///////////////////////////////////////////////////////////////////////////////


inline PBYTE CReceivePacketBufferBase::GetRemainedData()
{
	return (GetHead() + m_uiCurrentReadPosition);
}


inline unsigned long CReceivePacketBufferBase::GetRemainedDataSize()
{
	return SUBTRACT_BASEZERO(GetDataSize(), m_uiCurrentReadPosition);
}


///////////////////////////////////////////////////////////////////////////////


inline void CReceivePacketBufferBase::Skip(UINT uiDataSize)
{
	m_uiCurrentReadPosition += uiDataSize;
}


///////////////////////////////// End of File /////////////////////////////////
