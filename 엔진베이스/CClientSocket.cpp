/******************************************************************************
	CClientSocket.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CClientSocket.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


BOOL CClientSocket::CreateConnect(const char* pAddress, unsigned short usPort, int af/* = AF_INET*/,
	int type/* = SOCK_STREAM*/, int protocol/* = IPPROTO_IP*/, LPWSAPROTOCOL_INFO lpProtocolInfo/* = NULL*/,
	GROUP g/* = 0*/, DWORD dwFlags/* = WSA_FLAG_OVERLAPPED*/)
{
	if(FALSE == CWSASocket::Create(af, type, protocol, lpProtocolInfo, g, dwFlags))
	{
		return FALSE;
	}

	return CWSASocket::Connect(pAddress, usPort);
}


///////////////////////////////// End of File /////////////////////////////////
