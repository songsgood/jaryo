qinclude "stdafx.h"
qinclude "SocketIOCPTBThread.h"


FP_SOCKETIOFUNCTION* CSocketIOCPTBThread::m_fpSocketIO = NULL;
FP_TBUFFERPACKETPROCESSFUNCTION** CSocketIOCPTBThread::m_ppTBufferPacketProcessFunction = NULL;

BOOL CSocketIOCPTBThread::ProcessCompletedIO_TBuffer( ULONG_PTR /*pulCompletionKey*/, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped )
{
	CSocketIOCPOverlapped* pSocketIOCPOverlapped = (CSocketIOCPOverlapped*) pOverlapped;
	CIOCPSocketClient* pIOCPSocketClient = pSocketIOCPOverlapped->GetClient();

	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient_TBuffer, sizeof(CIOCPSocketClient))"));
		return TRUE;
	}

	if(FALSE == pIOCPSocketClient->CheckCIOCPSocketClient())
	{
		VERIFY(!TEXT("FALSE == pIOCPSocketClient->CheckCIOCPSocketClient() T_Buffer"));
		return TRUE;
	}

	OPCODE eOpCode = pSocketIOCPOverlapped->GetOpCode();
	if( (0 == dwNumberOfBytesTransferred) && 
		( (OP_SEND == eOpCode) || (OP_RECV == eOpCode) ) )
	{
		OnPeerDisconnected(pIOCPSocketClient);
	}
	else
	{
		if( eOpCode >= OP_ACCEPT && eOpCode <= OP_RECV ){
			(this->*m_fpSocketIO[eOpCode])(pIOCPSocketClient, dwNumberOfBytesTransferred);
		}
	}

	pIOCPSocketClient->ReleaseRef();

	return TRUE;
}

void CSocketIOCPTBThread::InitializeTBufferIOFunction()
{
	HANDLE hProcessHeap = NULL;
	VERIFY(NULL != (hProcessHeap = ::GetProcessHeap()));


	VERIFY(NULL != (m_fpSocketIO = (FP_SOCKETIOFUNCTION*) ::HeapAlloc( hProcessHeap, HEAP_ZERO_MEMORY,
		OP_MAX * sizeof(FP_SOCKETIOFUNCTION)) )) ;

	m_fpSocketIO[OP_ACCEPT] = &CSocketIOCPTBThread::OnAccepted_TBuffer;
	m_fpSocketIO[OP_RECV] = &CSocketIOCPTBThread::OnReceived_TBuffer;
	m_fpSocketIO[OP_SEND] = &CSocketIOCPTBThread::OnSent_TBuffer;
}

void CSocketIOCPTBThread::OnReceived_TBuffer( CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred )
{
	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient_TBuffer, sizeof(CIOCPSocketClient)"));
		return;
	}
	if(FALSE == pIOCPSocketClient->OnReceived(dwNumberOfBytesTransferred)) 
	{ 
		return; 
	}

	OnReceive_TBuffer(pIOCPSocketClient);

	if(FALSE == pIOCPSocketClient->PostWSAReceive())
	{
		OnPeerDisconnected(pIOCPSocketClient);
	}
}

void CSocketIOCPTBThread::OnSent_TBuffer( CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred )
{
	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient) TBuffer"));
		return;
	}
	pIOCPSocketClient->OnSent(dwNumberOfBytesTransferred);
	pIOCPSocketClient->PostWSASend();
}

void CSocketIOCPTBThread::OnAccepted_TBuffer( CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred )
{
	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)"));
		return;
	}
	PostAnotherAcceptEx(pIOCPSocketClient->GetServer());
	pIOCPSocketClient->OnAccepted();

	if(dwNumberOfBytesTransferred > 0)
	{
		OnReceived_TBuffer(pIOCPSocketClient, dwNumberOfBytesTransferred);
	}
	else
	{
		if(FALSE == pIOCPSocketClient->PostWSAReceive())
		{
			OnPeerDisconnected(pIOCPSocketClient);
		}
	}
}


void CSocketIOCPTBThread::OnReceive_TBuffer( CIOCPSocketClient* pIOCPSocketClient )
{
	CQueueBuffer* pFromBuffer = (CQueueBuffer*)pIOCPSocketClient->GetClientSocket()->GetReceiveBuffer();

	unsigned short uscommand = 0;
	DWORD dwStart = 0, dwCal = 0;

	while( 1 /*0 < pIOCPSocketClient->GetPacket(&m_ReceivePacketBuffer)*/ )
	{
		if( pFromBuffer->GetDataSize() < 8 )
			break;

		unsigned short usPacketLength = *((unsigned short*) (pFromBuffer->GetHead() + 6));

		if(usPacketLength > pFromBuffer->GetDataSize() - 8)
			break;

		uscommand = *((unsigned short*) (pFromBuffer->GetHead() + 4));

		dwStart = GetTickCount();
		if(usPacketLength > 0)
		{
			//TB::Buffer buffer(pFromBuffer->GetHead()+8);
			//m_ReceivePacketBuffer.DataAdded(usPacketLength);

			ProcessPacketTBuffer(pIOCPSocketClient, pFromBuffer->GetHead()+8, uscommand, usPacketLength);
		}
		else
		{
			//TB::Buffer buffer;
			ProcessPacketTBuffer(pIOCPSocketClient, nullptr, uscommand, 0);
		}
		dwCal = GetTickCount() - dwStart;
		if(dwCal > 1000) // 1sec - league server
		{
			g_LogManager.WriteLogToFile("[Profile] ProcessPacket TBufferThread : Time:[10752790], Command:[0]", dwCal, uscommand);

		pFromBuffer->DataRemoved(usPacketLength + 8);

		//m_ReceivePacketBuffer.Clear();
	}

}

void CSocketIOCPTBThread::ProcessPacketTBuffer( CIOCPSocketClient* /*pIOCPSocketClient*/
	, PBYTE /*pBuffer*/
	, unsigned short /*usCommand */
	, unsigned short /*usPacketLength*/)
{
	
}

void CSocketIOCPTBThread::ProcessPacket( CIOCPSocketClient* /*pIOCPSocketClient*/ )
{
	
}

void CSocketIOCPTBThread::ReleaseTBufferIOFunction()
{
	if( NULL != m_fpSocketIO )
	{
		HANDLE hProcessHeap = NULL;
		VERIFY(NULL != (hProcessHeap = ::GetProcessHeap()));

		::HeapFree(hProcessHeap, 0, m_fpSocketIO);
		m_fpSocketIO = NULL;
	}
}

BOOL CSocketIOCPTBThread::ProcessFailedIO( ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped )
{
	return CSocketIOCPThread::ProcessFailedIO(pulCompletionKey, dwNumberOfBytesTransferred, pOverlapped);
}

BOOL CSocketIOCPTBThread::ProcessCompletedIO( ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped )
{
	return ProcessCompletedIO_TBuffer(pulCompletionKey, dwNumberOfBytesTransferred, pOverlapped);
}

void CSocketIOCPTBThread::InitializeTBufferPacketProcessFunction()
{
	HANDLE hProcessHeap = NULL;
	VERIFY(NULL != (hProcessHeap = ::GetProcessHeap()));
	VERIFY(NULL != (m_ppTBufferPacketProcessFunction = (FP_TBUFFERPACKETPROCESSFUNCTION**) ::HeapAlloc(hProcessHeap
		, HEAP_ZERO_MEMORY,
		sizeof(FP_TBUFFERPACKETPROCESSFUNCTION*))));

	VERIFY(NULL != (m_ppTBufferPacketProcessFunction[0] = (FP_TBUFFERPACKETPROCESSFUNCTION*) ::HeapAlloc(hProcessHeap
		, HEAP_ZERO_MEMORY, PACKETPROCESSFUNCTION_COUNT * sizeof(FP_TBUFFERPACKETPROCESSFUNCTION))));
	
	for(UINT uiIndex = 0; uiIndex < PACKETPROCESSFUNCTION_COUNT; uiIndex++)
	{
		SetTBufferPacketProcessFunction(0, uiIndex, &CSocketIOCPTBThread::ProcessUnknownTbufferPacket);
	}
}

void CSocketIOCPTBThread::CleanupTBufferPacketProcessFunction()
{
	if(NULL != m_ppTBufferPacketProcessFunction)
	{
		HANDLE hProcessHeap = NULL;
		VERIFY(NULL != (hProcessHeap = ::GetProcessHeap()));

		if(NULL != m_ppTBufferPacketProcessFunction[0])
		{
			::HeapFree(hProcessHeap, 0, m_ppTBufferPacketProcessFunction[0]);
			m_ppTBufferPacketProcessFunction[0] = NULL;
		}

		::HeapFree(hProcessHeap, 0, m_ppTBufferPacketProcessFunction);
		m_ppTBufferPacketProcessFunction = NULL;
	}
}

void CSocketIOCPTBThread::SetTBufferPacketProcessFunction( UINT uiPacketProcessFunctionState, UINT uiCommand, FP_TBUFFERPACKETPROCESSFUNCTION pFunction )
{
	m_ppTBufferPacketProcessFunction[uiPacketProcessFunctionState][uiCommand] = pFunction;

}

void CSocketIOCPTBThread::ProcessUnknownTbufferPacket( CIOCPSocketClient* /*pIOCPSocketClient*/
	, PBYTE /*pBuffer*/
	, unsigned short /*usCommand*/ 
	, unsigned short /*usPacketLength*/)
{
	/* do nothing */
}
