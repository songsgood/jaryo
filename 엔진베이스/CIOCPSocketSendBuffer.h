/******************************************************************************
	CIOCPSocketSendBuffer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CQueueBuffer.h"
qinclude "CISLAllocatorEntry.h"
qinclude "CSRMAQueue.h"


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketSendBuffer : public CQueueBuffer, public CISLAllocatorEntry, public SRMA_QUEUE_ENTRY  
{
public:
	CIOCPSocketSendBuffer(BYTE ucAllocatorIndex, UINT uiBufferSize);
	virtual ~CIOCPSocketSendBuffer();

	CIOCPSocketSendBuffer* AllocateNewObject();

	virtual void InitializeInstance();

	BOOL CheckCIOCPSocketSendBufferCheckCode() { return (23 == m_ucCIOCPSocketSendBufferCheckCode); }

	static UINT GetTotalCount();
	static UINT GetTotalSize();

	BYTE GetAllocatorIndex();

	void SetNextPart();

	bool IsEncrypted();
	void SetEncrypted();


protected:
	BYTE m_ucCIOCPSocketSendBufferCheckCode;
	PBYTE m_pSendBuffer;
	bool m_bEncrypted;
	bool m_bFirstPart;
	BYTE m_ucAllocatorIndex;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketSendBuffer.inl"


///////////////////////////////// End of File /////////////////////////////////
