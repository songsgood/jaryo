/******************************************************************************
	CISAllocatorEntry.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CISAllocatorEntry.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


PINTERLOCKED_STACK_ENTRY CISAllocatorEntry::m_pAllListFirst = NULL;


///////////////////////////////////////////////////////////////////////////////


void CISAllocatorEntry::DeleteAllEntry()
{
	PINTERLOCKED_STACK_ENTRY pEntry = NULL;

	while(NULL != (pEntry = m_pAllListFirst))
	{
		m_pAllListFirst = m_pAllListFirst->Next;
		CISAllocatorEntry* pTemp = ((PIS_ALLOCATOR_ENTRY) pEntry)->This;
		delete pTemp;
	}
}


///////////////////////////////////////////////////////////////////////////////


void CISAllocatorEntry::AddToAllList()
{
	PINTERLOCKED_STACK_ENTRY pInitialAllListFirst = NULL;
	do {
		pInitialAllListFirst = m_pAllListFirst;
		m_AllListNext.Next = pInitialAllListFirst;
	} while(pInitialAllListFirst != ::InterlockedCompareExchangePointer((PVOID volatile *) &m_pAllListFirst,
		(PINTERLOCKED_STACK_ENTRY) &m_AllListNext, pInitialAllListFirst));
}


///////////////////////////////// End of File /////////////////////////////////
