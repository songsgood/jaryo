/******************************************************************************
	CPrototypeAllocator.inl
******************************************************************************/


template <class ObjectType>
inline CPrototypeAllocator<ObjectType>::CPrototypeAllocator(ObjectType* pPrototype/* = NULL*/) : m_pPrototype(pPrototype)
{
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType>
inline void CPrototypeAllocator<ObjectType>::SetPrototype(ObjectType* pPrototype)
{
	m_pPrototype = pPrototype;
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType>
inline ObjectType* CPrototypeAllocator<ObjectType>::New()
{
	return m_pPrototype->AllocateNewObject();
}


template <class ObjectType>
inline void CPrototypeAllocator<ObjectType>::Delete(ObjectType* pObject)
{
	delete pObject;
}


///////////////////////////////// End of File /////////////////////////////////
