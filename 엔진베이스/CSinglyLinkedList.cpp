/******************************************************************************
	CSinglyLinkedList.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CSinglyLinkedList.h"
qinclude "CSinglyListEntry.h"


///////////////////////////////////////////////////////////////////////////////


void CSinglyLinkedList::Push(CSinglyListEntry* pEntry)
{
	pEntry->SetNextSListEntry(m_pFirstEntry);
	m_pFirstEntry = pEntry;
}


CSinglyListEntry* CSinglyLinkedList::Pop()
{
	if(NULL == m_pFirstEntry)
	{
		return NULL;
	}

	CSinglyListEntry* pReturnEntry = m_pFirstEntry;
	m_pFirstEntry = m_pFirstEntry->GetNextSListEntry();
	return pReturnEntry;
}


///////////////////////////////////////////////////////////////////////////////


CSinglyListEntry* CSinglyLinkedList::GetFirst()
{
	return m_pFirstEntry;
}


CSinglyListEntry* CSinglyLinkedList::GetNext(CSinglyListEntry* pEntry)
{
	return pEntry->GetNextSListEntry();
}


///////////////////////////////// End of File /////////////////////////////////
