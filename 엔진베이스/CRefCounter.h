/******************************************************************************
	CRefCounter.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CRefCounter  
{
public:
	CRefCounter();
	virtual ~CRefCounter() { }

	void Initialize();
	UINT GetRefCounter();

	LONG AddRef();
	LONG ReleaseRef();

	BOOL AddFirstRef();
	BOOL AddNextRef();


protected:
	LONG m_lRefCounter;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CRefCounter.inl"


//////////////////////////////// End of File //////////////////////////////////
