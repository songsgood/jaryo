/******************************************************************************
	CSinglyListEntry.inl
******************************************************************************/


inline CSinglyListEntry::CSinglyListEntry() :
	m_pNextSListEntry(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


inline void CSinglyListEntry::SetNextSListEntry(CSinglyListEntry* pNextSListEntry)
{
	m_pNextSListEntry = pNextSListEntry;
}


inline CSinglyListEntry* CSinglyListEntry::GetNextSListEntry()
{
	return m_pNextSListEntry;
}


///////////////////////////////////////////////////////////////////////////////


inline void CSinglyListEntry::SetOwner(void* pOwner)
{
	m_pOwner = pOwner;
}


inline void* CSinglyListEntry::GetOwner()
{
	return m_pOwner;
}


///////////////////////////////// End of File /////////////////////////////////
