/******************************************************************************
	CDoublyListEntry.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CDoublyListEntry.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CDoublyListEntry::CDoublyListEntry() :
	m_pPrev(NULL),
	m_pNext(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


void CDoublyListEntry::InitializeInstance()
{
	m_pPrev = NULL;
	m_pNext = NULL;
}


///////////////////////////////// End of File /////////////////////////////////
