//==============================================================================
//	CClientPoolIOCPSocketServer.inl
//==============================================================================


template <class IOCPSocketClient>
inline CClientPoolIOCPSocketServer<IOCPSocketClient>::CClientPoolIOCPSocketServer(CIOCP* pIOCP,
	DWORD dwServerTimerInterval/* = INFINITE*/) :
	CIOCPSocketServer(pIOCP, dwServerTimerInterval)
{
}


////////////////////////////////////////////////////////////////////////////////


template <class IOCPSocketClient>
inline void CClientPoolIOCPSocketServer<IOCPSocketClient>::FreeClient(CSocketClient* pSocketClient)
{
	m_IOCPSocketClientAllocator.Delete((IOCPSocketClient*) pSocketClient);
}


////////////////////////////////////////////////////////////////////////////////


template <class IOCPSocketClient>
inline CSocketClient* CClientPoolIOCPSocketServer<IOCPSocketClient>::IAllocateClient()
{
	return m_IOCPSocketClientAllocator.New();
}


////////////////////////////////// End of File /////////////////////////////////
