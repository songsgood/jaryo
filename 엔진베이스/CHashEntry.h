/******************************************************************************
	CHashEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


template <class DLinkedListEntry>
class CHashEntry : public DLinkedListEntry  
{
public:
	CHashEntry();
	virtual ~CHashEntry() { }

	void SetHashIndex(UINT uiHashIndex);
	UINT GetHashIndex();


private:
	UINT m_uiHashIndex;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CHashEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
