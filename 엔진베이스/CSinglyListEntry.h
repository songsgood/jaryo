/******************************************************************************
	CSinglyListEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CSinglyListEntry  
{
public:
	CSinglyListEntry();
	virtual ~CSinglyListEntry() { }

	void SetNextSListEntry(CSinglyListEntry* pNextSListEntry);
	CSinglyListEntry* GetNextSListEntry();

	void SetOwner(void* pOwner);
	void* GetOwner();


private:
	CSinglyListEntry* m_pNextSListEntry;
	void* m_pOwner;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSinglyListEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
