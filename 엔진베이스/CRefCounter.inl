/******************************************************************************
	CRefCounter.inl
******************************************************************************/


inline CRefCounter::CRefCounter() : m_lRefCounter(0)
{
}


///////////////////////////////////////////////////////////////////////////////


inline void CRefCounter::Initialize()
{
	m_lRefCounter = 0;
}


inline UINT CRefCounter::GetRefCounter()
{
	return m_lRefCounter;
}


///////////////////////////////////////////////////////////////////////////////


inline LONG CRefCounter::AddRef()
{
	return ::InterlockedIncrement(&m_lRefCounter);
}


inline LONG CRefCounter::ReleaseRef()
{
	return ::InterlockedDecrement(&m_lRefCounter);
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CRefCounter::AddFirstRef()
{
	return (0 == ::InterlockedCompareExchange(&m_lRefCounter, 1, 0));
}


//////////////////////////////// End of File //////////////////////////////////
