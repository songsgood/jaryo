/******************************************************************************
	Debug.cpp
******************************************************************************/


qinclude "StdAfx.h"
qinclude "Debug.h"
qinclude "MessageBox.h"
qinclude "Path.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


void WriteOutLogMessage(LPCTSTR pErrorMessage)
{
	DECLARE_INIT_TCHAR_ARRAY(LogFilePath, _MAX_PATH + 1);

	FILE* pFile = NULL;
	::fopen_s(&pFile, ::MakeLogFullPath(LogFilePath, _countof(LogFilePath), "Log\\System\\"), TEXT("a+"));
	if(NULL == pFile)
	{
		return;
	}

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);

	fprintf(pFile, "[1643286/00/4972544 3435973836:3435973836:3435973836]\t", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay,
stemTime.wMinute, SystemTime.wSecond);
	fprintf(pFile, pErrorMessage);
	fprintf(pFile, "\r\n");

	::fclose(pFile);
}


void AssertFailed(LPCTSTR pErrorMessage)
{
//#ifdef _DEBUG
//	::ShowMessageBox(pErrorMessage);
//#else	// _DEBUG
	WriteOutLogMessage(pErrorMessage);
//#endif	// _DEBUG
}


void AssertFailed(LPCTSTR lpFileName, int iLineNumber, LPCTSTR pExpression, DWORD dwErrorCode/* = ERROR_SUCCESS*/,
	LPCTSTR pErrorMessage/* = NULL*/)
{
	// TCHAR szErrorMessage[1025];
	// ::lstrcpy(szErrorMessage, TEXT(""));
	DECLARE_INIT_TCHAR_ARRAY(szErrorMessage, 1025);

	if(ERROR_SUCCESS != dwErrorCode)
	{
		::wsprintf(szErrorMessage, TEXT("File �4, Line 0\n\n[0xCCCCCCCC]"), lpFileName, iLineNumber, pExpression, dwErrorCode);
	else
	{
		::wsprintf(szErrorMessage, TEXT("File �4, Line 0\n"), lpFileName, iLineNumber, pExpression);

	}

	if(NULL != pErrorMessage)
	{
		if(DIMOF(szErrorMessage) > ::lstrlen(szErrorMessage) + ::lstrlen(TEXT("\n")) + ::lstrlen(pErrorMessage))
		{
			::lstrcat(szErrorMessage, TEXT("\n"));
			::lstrcat(szErrorMessage, pErrorMessage);
		}
	}

	AssertFailed(szErrorMessage);
}


///////////////////////////////////////////////////////////////////////////////


void GetErrorMessageString(HLOCAL* phMessageBuffer, DWORD dwErrorCode)
{
	DWORD dwMessageLength = 0;

	dwMessageLength = ::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, NULL, dwErrorCode,
		MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), (LPTSTR) phMessageBuffer, 0, NULL);

	if(0 == dwMessageLength)
	{
		HMODULE hNetMsgDll = ::LoadLibraryEx(TEXT("netmsg.dll"), NULL, DONT_RESOLVE_DLL_REFERENCES);

		if(NULL != hNetMsgDll)
		{
			dwMessageLength = ::FormatMessage(FORMAT_MESSAGE_FROM_HMODULE | FORMAT_MESSAGE_ALLOCATE_BUFFER, hNetMsgDll, dwErrorCode,
				MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), (LPTSTR) phMessageBuffer, 0, NULL);
			::FreeLibrary(hNetMsgDll);
		}
	}

	if(0 == dwMessageLength)
	{
		*phMessageBuffer = NULL;
	}
}


///////////////////////////////// End of File /////////////////////////////////
