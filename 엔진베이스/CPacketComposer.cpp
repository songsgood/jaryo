/******************************************************************************
	CPacketComposer.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CPacketComposer.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CPacketComposer::CPacketComposer() :
	m_usPacketSize(0),
	m_usCommand(0)
{
	Initialize(0);
}


CPacketComposer::CPacketComposer(PBYTE pData, UINT uiDataSize) :
	m_usPacketSize(8)
{
	VERIFY(uiDataSize >= 8);
	Initialize(*((unsigned short*) (pData + 4)));
	Add(pData + 8, uiDataSize - 8);
}


///////////////////////////////////////////////////////////////////////////////


void CPacketComposer::Initialize(unsigned short usCommand)
{
	m_usCommand = usCommand;

	m_usPacketSize = 8;

	*((UINT*) m_PacketBuffer) = 0;
	*((unsigned short*) (m_PacketBuffer + 4)) = usCommand;
	*((unsigned short*) (m_PacketBuffer + 6)) = 0;
}

//////////////////////////////////////////////////////////////////////////

void CPacketComposer::SetCommand(unsigned short usCommand)
{
	m_usCommand = usCommand;

	*((unsigned short*) (m_PacketBuffer + 4)) = usCommand;
}

///////////////////////////////////////////////////////////////////////////////
// ktKim
// Temp
/*
void CPacketComposer::Add( const std::string & strData )
{
	unsigned short usStrLength = (unsigned short) strData.length();
	ASSERT(GetFreeSize() >= sizeof(unsigned short));
	*((unsigned short*) (GetPacketBuffer() + m_usPacketSize)) = ++usStrLength;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(unsigned short));

	PBYTE pData = (unsigned char *) strData.c_str();
	ASSERT(GetFreeSize() >= usStrLength);
	::CopyMemory(GetPacketBuffer() + m_usPacketSize, pData, usStrLength);
	m_usPacketSize = (unsigned short) (m_usPacketSize + usStrLength);
}
*/

void CPacketComposer::Add(PBYTE pData, UINT uiDataSize)
{
	ASSERT(GetFreeSize() >= uiDataSize);
	::CopyMemory(GetPacketBuffer() + m_usPacketSize, pData, uiDataSize);
	m_usPacketSize = (unsigned short) (m_usPacketSize + uiDataSize);
}


void CPacketComposer::Add(unsigned short* pusData)
{
	ASSERT(GetFreeSize() >= sizeof(unsigned short));
	*((unsigned short*) (GetPacketBuffer() + m_usPacketSize)) = *pusData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(unsigned short));
}


void CPacketComposer::Add(unsigned short usData)
{
	ASSERT(GetFreeSize() >= sizeof(unsigned short));
	*((unsigned short*) (GetPacketBuffer() + m_usPacketSize)) = usData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(unsigned short));
}


void CPacketComposer::Add(short sData)
{
	ASSERT(GetFreeSize() >= sizeof(short));
	*((short*) (GetPacketBuffer() + m_usPacketSize)) = sData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(short));
}


void CPacketComposer::Add(PBYTE pucData)
{
	ASSERT(GetFreeSize() >= 1);
	*(GetPacketBuffer() + m_usPacketSize) = *pucData;
	m_usPacketSize++;
}


void CPacketComposer::Add(BYTE ucData)
{
	ASSERT(GetFreeSize() >= 1);
	*(GetPacketBuffer() + m_usPacketSize) = ucData;
	m_usPacketSize++;
}


void CPacketComposer::Add(int* piData)
{
	ASSERT(GetFreeSize() >= sizeof(int));
	*((int*) (GetPacketBuffer() + m_usPacketSize)) = *piData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(int));
}


void CPacketComposer::Add(UINT* puiData)
{
	ASSERT(GetFreeSize() >= sizeof(UINT));
	*((UINT*) (GetPacketBuffer() + m_usPacketSize)) = *puiData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(UINT));
}


void CPacketComposer::Add(UINT uiData)
{
	ASSERT(GetFreeSize() >= sizeof(UINT));
	*((UINT*) (GetPacketBuffer() + m_usPacketSize)) = uiData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(UINT));
}


void CPacketComposer::Add(int iData)
{
	ASSERT(GetFreeSize() >= sizeof(int));
	*((int*) (GetPacketBuffer() + m_usPacketSize)) = iData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(int));
}


void CPacketComposer::Add(ULONG* pulData)
{
	ASSERT(GetFreeSize() >= sizeof(ULONG));
	*((ULONG*) (GetPacketBuffer() + m_usPacketSize)) = *pulData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(ULONG));
}


void CPacketComposer::Add(ULONG ulData)
{
	ASSERT(GetFreeSize() >= sizeof(ULONG));
	*((ULONG*) (GetPacketBuffer() + m_usPacketSize)) = ulData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(ULONG));
}


void CPacketComposer::Add(float* pfData)
{
	ASSERT(GetFreeSize() >= sizeof(float));
	*((float*) (GetPacketBuffer() + m_usPacketSize)) = *pfData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(float));
}


void CPacketComposer::Add(float fData)
{
	ASSERT(GetFreeSize() >= sizeof(float));
	*((float*) (GetPacketBuffer() + m_usPacketSize)) = fData;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(float));
}


void CPacketComposer::Add(SYSTEMTIME* ptimeDate)
{
	ASSERT(GetFreeSize() >= sizeof(SYSTEMTIME));
	*((SYSTEMTIME*) (GetPacketBuffer() + m_usPacketSize)) = *ptimeDate;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(SYSTEMTIME));
}


void CPacketComposer::Add(__int64* pi64Data)
{
	ASSERT(GetFreeSize() >= sizeof(__int64));
	*((__int64*) (GetPacketBuffer() + m_usPacketSize)) = *pi64Data;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(__int64));
}


void CPacketComposer::Add(unsigned __int64* pui64Data)
{
	ASSERT(GetFreeSize() >= sizeof(unsigned __int64));
	*((unsigned __int64*) (GetPacketBuffer() + m_usPacketSize)) = *pui64Data;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(unsigned __int64));
}


void CPacketComposer::Add(__int64 i64Data)
{
	ASSERT(GetFreeSize() >= sizeof(__int64));
	*((__int64*) (GetPacketBuffer() + m_usPacketSize)) = i64Data;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(__int64));
}


void CPacketComposer::Add(unsigned __int64 ui64Data)
{
	ASSERT(GetFreeSize() >= sizeof(unsigned __int64));
	*((unsigned __int64*) (GetPacketBuffer() + m_usPacketSize)) = ui64Data;
	m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(unsigned __int64));
}


void CPacketComposer::AddNTString(TCHAR* pString, UINT uiMaxStringBufSize)
{
	ASSERT(GetFreeSize() > 0);

	if(NULL == ::lstrcpyn((TCHAR*) (GetPacketBuffer() + m_usPacketSize), pString, MIN(GetFreeSize(), uiMaxStringBufSize)))
	{
		return;
	}

	m_usPacketSize = (unsigned short) (m_usPacketSize + (::lstrlen((TCHAR*) (GetPacketBuffer() + m_usPacketSize)) + 1));
}


///////////////////////////////// End of File /////////////////////////////////
