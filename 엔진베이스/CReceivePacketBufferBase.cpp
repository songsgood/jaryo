/******************************************************************************
	CReceivePacketBufferBase.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CReceivePacketBufferBase.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CReceivePacketBufferBase::CReceivePacketBufferBase(PBYTE pucBuffer, UINT uiBufferSize) :
	CQueueBuffer(pucBuffer, uiBufferSize),
	m_usCommand(0), m_uiSequenceNumber(0), m_uiCurrentReadPosition(0)
{
}


///////////////////////////////////////////////////////////////////////////////


void CReceivePacketBufferBase::Read(PBYTE pBuffer, UINT uiDataSize)
{
	Get(m_uiCurrentReadPosition, pBuffer, uiDataSize);
	m_uiCurrentReadPosition += uiDataSize;
}


///////////////////////////////////////////////////////////////////////////////
// ktKim
// Temp
/*void CReceivePacketBufferBase::Read(std::string & strData)
{
	unsigned short usStrLength = 0;
	Get(m_uiCurrentReadPosition, &usStrLength);
	m_uiCurrentReadPosition += sizeof(unsigned short);

	TCHAR buffer[16*1024];
	m_uiCurrentReadPosition += GetString(m_uiCurrentReadPosition, buffer, usStrLength);
	buffer[usStrLength] = '\0';

	strData = buffer;
}*/

//////////////////////////////////////////////////////////////////////////

unsigned short CReceivePacketBufferBase::GetCommand()
{
	if(m_usCommand == 50141)
	{
		int ia;
		BYTE bta;
		ia = bta = 0;
		Get(m_uiCurrentReadPosition, &ia);
		Get(m_uiCurrentReadPosition+sizeof(int), &bta);

		if(2091325837 == ia && bta == 171)
		{
			m_usCommand = 0;
			memset(GetHead(), 0x00, GetDataSize());

			_asm
			{
				int 3;
			}	
		}
	}

	return m_usCommand;
}

BOOL CReceivePacketBufferBase::GetPacket(CQueueBuffer* pReceivedBuffer)
{
	if(pReceivedBuffer->GetDataSize() < 8)
	{
		return FALSE;
	}

	unsigned short usPacketLength = *((unsigned short*) (pReceivedBuffer->GetHead() + 6));

	if(usPacketLength > pReceivedBuffer->GetDataSize() - 8)
	{
		return FALSE;
	}

	if(usPacketLength > 0)
	{
		Add(pReceivedBuffer->GetHead() + 8, usPacketLength);
	}

	m_usCommand = *((unsigned short*) (pReceivedBuffer->GetHead() + 4));  
	m_uiSequenceNumber = *((UINT*) pReceivedBuffer->GetHead());


	pReceivedBuffer->DataRemoved(usPacketLength + 8);

	return TRUE;
}


///////////////////////////////// End of File /////////////////////////////////
