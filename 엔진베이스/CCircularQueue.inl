/******************************************************************************
	CCircularQueue.inl
******************************************************************************/


template <class ObjectType, int iQueueSize>
inline CCircularQueue<ObjectType, iQueueSize>::CCircularQueue() :
	m_iHead(0),
	m_bFull(false),
	m_iItemCount(0)
{
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, int iQueueSize>
inline void CCircularQueue<ObjectType, iQueueSize>::Empty()
{
	m_iHead = 0;
	m_bFull = false;
	m_iItemCount = 0;
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, int iQueueSize>
inline void CCircularQueue<ObjectType, iQueueSize>::Add(ObjectType Object)
{
	m_ObjectList[m_iHead] = Object;
	SetNextIndex();

	m_iItemCount++;
	if(m_iItemCount > iQueueSize)
	{
		m_iItemCount = iQueueSize;
	}
}


template <class ObjectType, int iQueueSize>
inline ObjectType* CCircularQueue<ObjectType, iQueueSize>::Pop()
{
	if (m_iItemCount <= 0) return NULL;

	m_iItemCount--;

	m_bFull = false;

	return &m_ObjectList[m_iHead];
}


template <class ObjectType, int iQueueSize>
inline ObjectType CCircularQueue<ObjectType, iQueueSize>::ReadTail()
{
	int iTail;
	if(m_iHead >= m_iItemCount)
	{
		iTail = m_iHead - m_iItemCount;
	}
	else
	{
		iTail = m_iHead + iQueueSize - m_iItemCount;
	}

	return m_ObjectList[iTail];
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, int iQueueSize>
inline bool CCircularQueue<ObjectType, iQueueSize>::IsFull()
{
	return m_bFull;
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, int iQueueSize>
inline int CCircularQueue<ObjectType, iQueueSize>::GetItemCount()
{
	return m_iItemCount;
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, int iQueueSize>
inline void CCircularQueue<ObjectType, iQueueSize>::SetNextIndex()
{
	m_iHead = (m_iHead + 1) % iQueueSize;
	if( (!m_bFull) && (0 == m_iHead) )	m_bFull = true;
}


///////////////////////////////// End of File /////////////////////////////////
