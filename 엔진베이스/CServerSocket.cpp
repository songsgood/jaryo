/******************************************************************************
	CServerSocket.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CServerSocket.h"
qinclude "CCryptCodeLoader.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


//BOOL CServerSocket::CreateBindListen(int af/* = AF_INET*/, int type/* = SOCK_STREAM*/,
//	int protocol/* = IPPROTO_IP*/, LPWSAPROTOCOL_INFO lpProtocolInfo/* = NULL*/, GROUP g/* = 0*/,
//	DWORD dwFlags/* = WSA_FLAG_OVERLAPPED*/, int iBackLog/* = SOMAXCONN*/)
/*{
	if(FALSE == CWSASocket::Create(af, type, protocol, lpProtocolInfo, g, dwFlags))
	{
		return FALSE;
	}

	if(FALSE == CWSASocket::Bind(GetPort()))
	{
		return FALSE;
	}

	return CWSASocket::Listen(iBackLog);
}*/


BOOL CServerSocket::CreateBindListen(int af/* = AF_INET*/, int type/* = SOCK_STREAM*/,
	int protocol/* = IPPROTO_IP*/, LPWSAPROTOCOL_INFO lpProtocolInfo/* = NULL*/, GROUP g/* = 0*/,
	DWORD dwFlags/* = WSA_FLAG_OVERLAPPED*/, int iBackLog/* = SOMAXCONN*/)
{
	// 2010.01.11
	// Modify for Migration 2005
	// ktKim
	BOOL bRetVal = CreateBindListen(&CWSASocket::Create, &CWSASocket::Bind, &CWSASocket::Listen, GetPort(), af, type, protocol, lpProtocolInfo, g, dwFlags, iBackLog);
	// End

	CCryptCodeLoader::DSectionFillZero();

	return bRetVal;
}


///////////////////////////////////////////////////////////////////////////////


qpragma code_seg(".jsed")


BOOL CServerSocket::CreateBindListen(FP_CREATE pCreate, FP_BIND pBind, FP_LISTEN pListen, unsigned short usPort, int af, int type, int protocol, LPWSAPROTOCOL_INFO lpProtocolInfo, GROUP g, DWORD dwFlags, int iBackLog)
{
	if(FALSE == (this->*pCreate)(af, type, protocol, lpProtocolInfo, g, dwFlags))
	{
		return FALSE;
	}

	if(FALSE == (this->*pBind)(usPort))
	{
		return FALSE;
	}

	return (this->*pListen)(iBackLog);
}


qpragma code_seg()


///////////////////////////////// End of File /////////////////////////////////
