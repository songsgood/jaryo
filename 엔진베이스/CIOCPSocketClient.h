/******************************************************************************
	CIOCPSocketClient.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketClient.h"
qinclude "CPacketCoder.h"
qinclude "CRefCounter.h"


///////////////////////////////////////////////////////////////////////////////


class CPacketComposer;
class CIOCPClientSocket;
class CIOCPSocketServer;


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketClient : public CSocketClient, public CPacketCoder  
{
public:
	CIOCPSocketClient() : m_ucCheckCode(75) { }
	virtual ~CIOCPSocketClient() { }

	void AddRef();
	void ReleaseRef();

	virtual void InitializeInstance();
	virtual void CleanupInstance();

	virtual void AllocateClientSocket();

	BOOL CheckCIOCPSocketClient() { return (75 == m_ucCheckCode); }

	CIOCPSocketServer* GetServer();

	BOOL WaitForClientConnection();

	/*virtual */BOOL OnReceived(DWORD dwReceivedDataSize);
	/*virtual */void OnSent(DWORD dwSentDataSize);
	virtual void OnAccepted();

	BOOL Connect(const char* pAddress, unsigned short usPort);
	BOOL Connect();

	void PostWSASend();
	BOOL PostWSAReceive();

	void DisconnectClient();

	BOOL AddToSendQueue(CPacketComposer* pPacketComposer);
	void Send(CPacketComposer* pPacketComposer, BOOL bSend = TRUE);
	void Send(PBYTE pData, UINT uiDataSize , BOOL bSend = TRUE);
	void Send();

	virtual int GetPacket(CReceivePacketBuffer* pReceivePacketBuffer);

	CIOCPClientSocket* GetClientSocket();


protected:
	BOOL PostAcceptEx();

	/*virtual */void ProcessPacket();
	void ProcessUnknownPacket();


protected:
	BYTE m_ucCheckCode;
	CRefCounter m_RefCounter;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketClient.inl"


///////////////////////////////// End of File /////////////////////////////////
