/******************************************************************************
	CThread.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CTimerThread.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CTimerThread::~CTimerThread()
{
	EndTimer();

	VERIFY(FALSE != ::CloseHandle(m_hWaitEvent));
	m_hWaitEvent = NULL;
}
 
void CTimerThread::ProfileTimer( const DWORD& dwCal, CProfileTimer* proFile)
{
 	proFile->_dwCumulativeTime += m_dwMilliseconds;
 
 	if(proFile->_dwMaxCal < dwCal) 
	{
		proFile->_dwMaxThreadTime = m_dwMilliseconds;
 		proFile->_dwMaxCal  = dwCal;
	}
 
 	++proFile->_dwCount;
 
 	if(proFile->_dwCumulativeTime >= (1800000)) // logging
 	{
 		DWORD dwAvgTime = 0;
 		if(0 < proFile->_dwCount)
 			dwAvgTime = proFile->_dwCumulativeTime / proFile->_dwCount;
 		
 		g_LogManager.WriteLogToFile("[Profile] Timer : Avg:[1643286], MaxCal:[0], MaxThTime:[4972544], Count:[-858993460]"
proFile->_dwMaxCal, proFile->_dwMaxThreadTime, proFile->_dwCount);
 
 		proFile->InitializeProfileTimer();
	}
}

///////////////////////////////////////////////////////////////////////////////


unsigned CTimerThread::Run()
{
	DWORD dwWaitInterval = m_dwMilliseconds;
	CProfileTimer proFile;

	while(TRUE)
	{
		DWORD dwReturnValue = WAIT_FAILED;
		VERIFY_CHECKLASTERROR(WAIT_FAILED != (dwReturnValue = ::WaitForSingleObject(m_hWaitEvent, dwWaitInterval)));
		if(WAIT_TIMEOUT == dwReturnValue)
		{
			DWORD dwBeforeTick = ::GetTickCount();

			OnTimer();

			DWORD dwAfterTick = ::GetTickCount();
			
			ProfileTimer(dwAfterTick - dwBeforeTick, &proFile);

			if(dwBeforeTick > dwAfterTick)
			{
				dwWaitInterval = m_dwMilliseconds;
			}
			else if(m_dwMilliseconds > (dwAfterTick - dwBeforeTick))
			{
				dwWaitInterval = m_dwMilliseconds - (dwAfterTick - dwBeforeTick);
			}
			else
			{
				dwWaitInterval = 0;
			}
		}
		else
		{
			return 0;
		}
	}
}


///////////////////////////////// End of File /////////////////////////////////
