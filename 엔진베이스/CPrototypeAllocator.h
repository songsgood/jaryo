/******************************************************************************
	CPrototypeAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType>
class CPrototypeAllocator  
{
public:
	CPrototypeAllocator(ObjectType* pPrototype = NULL);
	/*virtual */~CPrototypeAllocator() { }

	void SetPrototype(ObjectType* pPrototype);

	ObjectType* New();
	void Delete(ObjectType* pObject);


private:
	ObjectType* m_pPrototype;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CPrototypeAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
