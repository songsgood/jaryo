/******************************************************************************
	CDLinkedListEntry.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CDLinkedListEntry.h"


///////////////////////////////////////////////////////////////////////////////


CDLinkedListEntry::CDLinkedListEntry() :
	m_pOwner(NULL),
	m_pNext(NULL), m_pPrev(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


void CDLinkedListEntry::SetRemoved()
{
	m_pNext = NULL;
	m_pPrev = NULL;
}


///////////////////////////////// End of File /////////////////////////////////
