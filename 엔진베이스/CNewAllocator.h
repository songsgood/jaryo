/******************************************************************************
	CNewAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType>
class CNewAllocator  
{
public:
	CNewAllocator() { }
	/*virtual */~CNewAllocator() { }

	ObjectType* New();
	void Delete(ObjectType* pObject);
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CNewAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
