/******************************************************************************
	CClientTickManager.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CClientTickManager.h"


///////////////////////////////////////////////////////////////////////////////


CClientTickManager::CClientTickManager() :
	m_dwFirstClientTick(0), m_dwFirstClientTickUpdateTick(0),
	m_dwThreshold(6000)
{
}


///////////////////////////////////////////////////////////////////////////////


void CClientTickManager::Initialize()
{
	m_dwFirstClientTick = 0;
	m_dwFirstClientTickUpdateTick = 0;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CClientTickManager::IsValidClientTick(DWORD dwCurrentClientTick)
{
	DWORD dwCurrentTick = ::GetTickCount();

	if( (0 == m_dwFirstClientTick) && (0 == m_dwFirstClientTickUpdateTick) )
	{
		m_dwFirstClientTick = dwCurrentClientTick;
		m_dwFirstClientTickUpdateTick = dwCurrentTick;
		return TRUE;
	}

	if( (m_dwFirstClientTick > dwCurrentClientTick) || (m_dwFirstClientTickUpdateTick > dwCurrentTick) )
	{
		VERIFY(!TEXT("(m_dwFirstClientTick > dwCurrentClientTick) || (m_dwFirstClientTickUpdateTick > dwCurrentTick)"));
		m_dwFirstClientTick = dwCurrentClientTick;
		m_dwFirstClientTickUpdateTick = dwCurrentTick;
		return TRUE;
	}

	if( (dwCurrentClientTick - m_dwFirstClientTick) > (dwCurrentTick - m_dwFirstClientTickUpdateTick + m_dwThreshold) )
	{
		return FALSE;
	}

	return TRUE;
}


///////////////////////////////// End of File /////////////////////////////////
