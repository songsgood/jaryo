/******************************************************************************
	CSystemInfo.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CSystemInfo.h"


///////////////////////////////////////////////////////////////////////////////


DWORD CSystemInfo::GetNumberOfProcessors()
{
	SYSTEM_INFO SystemInfo;
	UpdateSystemInfo(&SystemInfo);
	return SystemInfo.dwNumberOfProcessors;
}


///////////////////////////////// End of File /////////////////////////////////
