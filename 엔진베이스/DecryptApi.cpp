// DecryptApi.cpp: implementation of the CDecryptApi class.
//
//////////////////////////////////////////////////////////////////////

qinclude "StdAfx.h"
qinclude "DecryptApi.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDecryptApi::CDecryptApi(LPCTSTR, DWORD, ALG_ID)
{

}

CDecryptApi::~CDecryptApi()
{

}
void CDecryptApi::SetParameter(LPCTSTR szProvider, DWORD dwProvType, ALG_ID AlgID)
{
	memset(m_szProvider, 0x00, MAX_PROVIDER_LENGTH+1);
	memcpy(m_szProvider, szProvider, min(strlen(szProvider), MAX_PROVIDER_LENGTH+1));

	m_dwProvType	= dwProvType;
	m_AlgId			= AlgID;
}
qinclude "MessageBox.h"
int CDecryptApi::Decrypt(PBYTE pPassword, int iPwSize, PBYTE pSourceData, PBYTE pDestData, int iDataSize, int iDestDataBufferSize)
{
	if ( pPassword == NULL )		return -1;

	if ( iDataSize > iDestDataBufferSize +8 )	return -2;

    HCRYPTPROV hProv   = 0;
    HCRYPTKEY hKey     = 0;
    HCRYPTHASH hHash   = 0;

    PBYTE pbKeyBlob = NULL;

    DWORD dwBlockLen;
    DWORD dwBufferLen;
    DWORD dwCount;

	PBYTE pSourcePointer	= pSourceData;
	PBYTE pDestPointer		= pDestData;

	int iDecryptedDataSize = 0;

	int iDestinationDataSize =0;


    // Get handle to the default provider.
    if(!CryptAcquireContext(&hProv, NULL, (const char*)m_szProvider, m_dwProvType, CRYPT_VERIFYCONTEXT))
	{
		return -3;
    }

	// Decrypt the file with a session key derived from a password.

	// Create a hash object.
	if(!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash)) 
	{
		return -4;
	}

	// Hash in the password data.
	if(!CryptHashData(hHash, (const unsigned char*)pPassword, iPwSize, 0)) 
	{

		// Release provider handle.
		if(hProv) CryptReleaseContext(hProv, 0);

		return -5;
	}

	// Derive a session key from the hash object.
	if(!CryptDeriveKey(hProv, m_AlgId, hHash, 0, &hKey)) 
	{
		// Destroy hash object.
		if(hHash) CryptDestroyHash(hHash);

		// Release provider handle.
		if(hProv) CryptReleaseContext(hProv, 0);

		return -6;
	}

	// Destroy the hash object.
	CryptDestroyHash(hHash);
	hHash = 0;

    // Determine number of BYTEs to decrypt at a time. This must be a multiple
    // of ENCRYPT_BLOCK_SIZE.
    dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE;
    dwBufferLen = dwBlockLen;

	BOOL bFinal;

	while ( iDataSize - iDecryptedDataSize > 0 )
    {
		if ( iDataSize - iDecryptedDataSize > (int)dwBlockLen )
		{
			dwCount = dwBlockLen;
			bFinal = FALSE;
		}
		else
		{
			dwCount = iDataSize - iDecryptedDataSize;
			bFinal = TRUE;
		}

		memcpy(pDestPointer, pSourcePointer, dwCount);

		pSourcePointer		= pSourcePointer + dwCount;
		iDecryptedDataSize	+= dwCount;

        // Decrypt data
        if(!CryptDecrypt(hKey, 0, bFinal, 0, pDestPointer, &dwCount)) 
		{
			DWORD dwError = GetLastError();
			dwError;
			return -7;
        }

		pDestPointer = pDestPointer + dwCount;
		iDestinationDataSize += dwCount;

		if ( bFinal )
		{
			memset(pDestPointer, 0x00, iDataSize - iDestinationDataSize);
		}
    } 

    // Free memory.
    if(pbKeyBlob) free(pbKeyBlob);

    // Destroy session key.
    if(hKey) CryptDestroyKey(hKey);

    // Destroy hash object.
    if(hHash) CryptDestroyHash(hHash);

    // Release provider handle.
    if(hProv) CryptReleaseContext(hProv, 0);

	return iDestinationDataSize;
}
