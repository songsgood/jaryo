/******************************************************************************
	CThread.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CThread  
{
public:
	CThread();
	virtual ~CThread();

	BOOL CreateThread(LPSECURITY_ATTRIBUTES lpThreadAttributes = NULL, DWORD dwStackSize = 0, DWORD dwCreationFlags = 0,
		LPDWORD lpThreadId = NULL);
	void EndThread();

	HANDLE GetHandle();


protected:
	static unsigned __stdcall _Run(void* pParameter);
	virtual unsigned Run() = 0;

	virtual BOOL OnStartup();


protected:
	HANDLE m_hThread;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CThread.inl"


///////////////////////////////// End of File /////////////////////////////////
