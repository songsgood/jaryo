/******************************************************************************
	CIOCPSocketServer.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPSocketServer.h"


///////////////////////////////////////////////////////////////////////////////


CIOCPSocketServer::CIOCPSocketServer(CIOCP* pIOCP, DWORD dwServerTimerInterval/* = INFINITE*/) :
	CSocketServer(dwServerTimerInterval),
	m_pIOCP(pIOCP)
{
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPSocketServer::PostAccept(UINT uiInitialAcceptCount)
{
	for(UINT uiPostAcceptIndex = 0; uiPostAcceptIndex < uiInitialAcceptCount; uiPostAcceptIndex++)
	{
		CIOCPSocketClient* pClient = (CIOCPSocketClient*) AllocateClient();

		if(FALSE == pClient->WaitForClientConnection())
		{
			return FALSE;
		}
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPSocketServer::OnInitialize()
{
	if(FALSE == CSocketServer::OnInitialize())
	{
		return FALSE;
	}

	if(FALSE == PostAccept(10))
	{
		return FALSE;
	}

	return m_pIOCP->AssociateDevice((HANDLE) GetServerSocket()->GetHandle(), (ULONG_PTR) GetServerSocket());
}


///////////////////////////////// End of File /////////////////////////////////
