/******************************************************************************
	CSListAllocator.inl
******************************************************************************/


template <class ObjectType, class BaseAllocator>
inline CSListAllocator<ObjectType, BaseAllocator>::CSListAllocator() :
	m_pFirstEntry(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, class BaseAllocator>
inline ObjectType* CSListAllocator<ObjectType, BaseAllocator>::New()
{
	ObjectType* pObject = m_pFirstEntry;

	if(NULL == pObject)
	{
		VERIFY(NULL != (pObject = m_BaseAllocator.New()));
	}
	else
	{
		m_pFirstEntry = (ObjectType*) pObject->GetNextAllocatorEntry();
	}
	/*ObjectType* pObject = NULL;
	VERIFY(NULL != (pObject = m_BaseAllocator.New()));*/

	pObject->InitializeInstance();	// Constructor

	return pObject;
}


template <class ObjectType, class BaseAllocator>
inline void CSListAllocator<ObjectType, BaseAllocator>::Delete(ObjectType* pObject)
{
	pObject->CleanupInstance();	// Destructor

	pObject->SetNextAllocatorEntry(m_pFirstEntry);
	m_pFirstEntry = pObject;
	//m_BaseAllocator.Delete(pObject);
}


///////////////////////////////// End of File /////////////////////////////////
