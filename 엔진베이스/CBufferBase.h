/******************************************************************************
	CBufferBase.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CBufferBase  
{
public:
	CBufferBase(BYTE* pucBuffer = NULL, UINT uiBufferSize = 0);
	virtual ~CBufferBase() { }

	BYTE* GetBuffer();
	UINT GetBufferSize();

	operator BYTE*();
	operator VOID*();

	BYTE& operator[](UINT uiIndex);

	void FillZeroMemory();


protected:
	void SetBuffer(PBYTE pucBuffer, UINT uiBufferSize);


protected:
	BYTE* m_pucBuffer;
	UINT m_uiBufferSize;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CBufferBase.inl"


///////////////////////////////// End of File /////////////////////////////////
