///=============================================================================
///	\file	File2.h
///	\author	S. H. Hwang (lwittgen@naver.com)
///=============================================================================


#pragma once


////////////////////////////////////////////////////////////////////////////////


/**
 *	File2 Class
 */
class CFile2  
{
public:
	CFile2();
	virtual ~CFile2();


public:
	bool Create(LPCTSTR lpFileName, DWORD dwDesiredAccess = GENERIC_READ, DWORD dwShareMode = FILE_SHARE_READ, LPSECURITY_ATTRIBUTES lpSecurityAttributes = NULL, DWORD dwCreationDisposition = OPEN_EXISTING, DWORD dwFlagsAndAttributes = FILE_ATTRIBUTE_NORMAL, HANDLE hTemplateFile = NULL);
	bool Close();
	void SetFile(HANDLE hFile);
	bool Read(LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, LPOVERLAPPED lpOverlapped = NULL);
	bool Write(LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, LPOVERLAPPED lpOverlapped = NULL);
	DWORD SetFilePointer(LONG lDistanceToMove, PLONG lpDistanceToMoveHigh = NULL, DWORD dwMoveMethod = FILE_BEGIN);
	DWORD GetFileSize();
	BOOL GetFileTime(LPFILETIME lpCreationTime, LPFILETIME lpLastAccessTime, LPFILETIME lpLastWriteTime);

	bool IsValidHandle();


protected:
	HANDLE m_hFile;	///<	파일 핸들
};


////////////////////////////////////////////////////////////////////////////////


/**
 *	Constructor
 */
inline CFile2::CFile2() :
	m_hFile(INVALID_HANDLE_VALUE)
{
}


////////////////////////////////////////////////////////////////////////////////


/**
 *	이미 열린 파일의 핸들 값으로 세팅
 */
inline void CFile2::SetFile(HANDLE hFile)
{
	m_hFile = hFile;
}


////////////////////////////////////////////////////////////////////////////////


/**
 *	Determines If The CFile Object Has A Valid Handle
 */
inline bool CFile2::IsValidHandle()
{
	return (INVALID_HANDLE_VALUE != m_hFile);
}


////////////////////////////////// End of File /////////////////////////////////
