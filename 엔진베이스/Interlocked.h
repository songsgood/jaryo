/******************************************************************************
	Interlocked.h
******************************************************************************/


#pragma once


///////////////////////////////////////////////////////////////////////////////


#ifdef _WIN64

#else	// _WIN64

#ifdef _WIN32

#ifdef _M_IX86

ULONGLONG InterlockedCompareExchange64b(ULONGLONG* volatile pullDestination, ULONGLONG ullExchange,
	ULONGLONG ullComperand);

#endif	// _M_IX86

#endif	// _WIN32

#endif	// _WIN64


///////////////////////////////// End of File /////////////////////////////////
