//==============================================================================
//	BaseTypes.inl
//==============================================================================


template <class BASE_TYPE>
inline TypeStruct<BASE_TYPE>::TypeStruct()
{
}


template <class BASE_TYPE>
inline TypeStruct<BASE_TYPE>::TypeStruct(BASE_TYPE InitValue) :
	Value(InitValue)
{
}


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE>
inline BASE_TYPE TypeStruct<BASE_TYPE>::Get()
{
	return Value;
}


template <class BASE_TYPE>
inline void TypeStruct<BASE_TYPE>::Set(BASE_TYPE NewValue)
{
	Value = NewValue;
}


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE>
inline TypeStruct<BASE_TYPE>::operator BASE_TYPE()
{
	return Get();
}


template <class BASE_TYPE>
inline BASE_TYPE* TypeStruct<BASE_TYPE>::operator & ()
{
	return (&Value);
}


template <class BASE_TYPE>
inline BASE_TYPE TypeStruct<BASE_TYPE>::operator = (BASE_TYPE NewValue)
{
	Set(NewValue);
	return Get();
}


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline TypeStructIV<BASE_TYPE, InvalidValue>::TypeStructIV() :
	TypeStruct<BASE_TYPE>(InvalidValue)
{
}


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline TypeStructIV<BASE_TYPE, InvalidValue>::TypeStructIV(BASE_TYPE InitValue) :
	TypeStruct<BASE_TYPE>(InitValue)
{
}


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline BASE_TYPE TypeStructIV<BASE_TYPE, InvalidValue>::GetInvalidValue()
{
	return InvalidValue;
}


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline bool TypeStructIV<BASE_TYPE, InvalidValue>::IsValid(BASE_TYPE Value)
{
	return !IsInvalid(Value);
}


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline bool TypeStructIV<BASE_TYPE, InvalidValue>::IsInvalid(BASE_TYPE Value)
{
	return (InvalidValue == Value);
}


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline void TypeStructIV<BASE_TYPE, InvalidValue>::SetInvalidValue()
{
	Set(InvalidValue);
}


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline bool TypeStructIV<BASE_TYPE, InvalidValue>::IsValid()
{
	return IsValid(Get());
}


template <class BASE_TYPE, BASE_TYPE InvalidValue>
inline bool TypeStructIV<BASE_TYPE, InvalidValue>::IsInvalid()
{
	return IsInvalid(Get());
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline TypeWithRange<BASE_TYPE, MinValue, MaxValue>::TypeWithRange(BASE_TYPE Value)
{
	Set(Value);
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline void TypeWithRange<BASE_TYPE, MinValue, MaxValue>::Set(BASE_TYPE Value)
{
	TypeStruct<BASE_TYPE>::Set(GetInRangeValue(Value));
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline bool TypeWithRange<BASE_TYPE, MinValue, MaxValue>::IsInRange(BASE_TYPE Value)
{
	return ::IsBetweenII(Value, MinValue, MaxValue);
}


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline bool TypeWithRange<BASE_TYPE, MinValue, MaxValue>::IsInRange()
{
	return IsInRange(Get());
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline BASE_TYPE TypeWithRange<BASE_TYPE, MinValue, MaxValue>::GetMaxValue()
{
	return MaxValue;
}


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline BASE_TYPE TypeWithRange<BASE_TYPE, MinValue, MaxValue>::GetMinValue()
{
	return MinValue;
}


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline BASE_TYPE TypeWithRange<BASE_TYPE, MinValue, MaxValue>::GetInRangeValue(BASE_TYPE Value)
{
	return ::GetBetween(Value, MinValue, MaxValue);
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline bool TypeWithRange<BASE_TYPE, MinValue, MaxValue>::FixInRange(BASE_TYPE* pValue)
{
	if(IsInRange(*pValue))
	{
		return true;
	}
	*pValue = GetInRangeValue(*pValue);
	return false;
}


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
inline bool TypeWithRange<BASE_TYPE, MinValue, MaxValue>::FixInRange()
{
	if(IsInRange())
	{
		return true;
	}
	Set(Get());
	return false;
}


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
inline FixedString<MaxCharacterCount>::FixedString()
{
	SetEmptyString();
}


template <u_int_32 MaxCharacterCount>
inline FixedString<MaxCharacterCount>::FixedString(TCHAR* pInitString)
{
	SetString(pInitString);
}


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
inline FixedString<MaxCharacterCount>::operator TCHAR*()
{
	return GetString();
}


template <u_int_32 MaxCharacterCount>
inline TCHAR* FixedString<MaxCharacterCount>::operator = (TCHAR* pString)
{
	SetString(pString);
	return GetString();
}


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
inline void FixedString<MaxCharacterCount>::SetEmptyString()
{
	INIT_TCHAR_ARRAY(String);
}


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
inline TCHAR* FixedString<MaxCharacterCount>::GetString()
{
	return String;
}


template <u_int_32 MaxCharacterCount>
inline void FixedString<MaxCharacterCount>::SetString(TCHAR* pString)
{
	LSTRCPYNTOARRAY(String, pString);
}


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
inline bool FixedString<MaxCharacterCount>::IsEmpty()
{
	return (GetCharacterCount() <= 0);
}


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
inline u_int_32 FixedString<MaxCharacterCount>::GetMaxCharacterCount()
{
	return MaxCharacterCount;
}


template <u_int_32 MaxCharacterCount>
inline u_int_32 FixedString<MaxCharacterCount>::GetBufferSize()
{
	return ((MaxCharacterCount + 1) * sizeof(TCHAR));
}


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
inline u_int_32 FixedString<MaxCharacterCount>::GetCharacterCount()
{
	int iCharacterCount = ::lstrlen(String);
	return MAX(iCharacterCount, 0);
}


////////////////////////////////////////////////////////////////////////////////


inline BIT_ARRAY::BIT_ARRAY() :
	pArray(NULL)
{
}


inline BIT_ARRAY::~BIT_ARRAY()
{
	Free();
}


////////////////////////////////////////////////////////////////////////////////


inline void BIT_ARRAY::Allocate(BIT_ARRAY_INDEX MaxArrayIndex)
{
	VERIFY(NULL != (pArray = new u_int_8[(MaxArrayIndex + 8) / 8]));
	::ZeroMemory(pArray, (MaxArrayIndex + 8) / 8);
}


inline void BIT_ARRAY::Set(BIT_ARRAY_INDEX Index)
{
	pArray[Index / 8] |= 0x01 << (Index % 8);
}


inline void BIT_ARRAY::Unset(BIT_ARRAY_INDEX Index)
{
	pArray[Index / 8] &= ~(0x01 << (Index % 8));
}


inline bool BIT_ARRAY::IsSet(BIT_ARRAY_INDEX Index)
{
	return ( 0 != (pArray[Index / 8] & (0x01 << (Index % 8))) );
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE>
inline BIT_FLAG<BASE_TYPE>::BIT_FLAG()
{
	Set(0);
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE>
inline void BIT_FLAG<BASE_TYPE>::Initialize()
{
	Set(0);
}


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE>
inline void BIT_FLAG<BASE_TYPE>::SetFlag(BASE_TYPE Flag)
{
	Value |= Flag;
}


template<class BASE_TYPE>
inline void BIT_FLAG<BASE_TYPE>::UnsetFlag(BASE_TYPE Flag)
{
	Value &= ~Flag;
}


template<class BASE_TYPE>
inline bool BIT_FLAG<BASE_TYPE>::IsSet(BASE_TYPE Flag)
{
	return (0 != (Value & Flag));
}


////////////////////////////////// End of File /////////////////////////////////
