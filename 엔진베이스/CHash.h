/******************************************************************************
	CHash.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


template <class DLinkedListEntry> class CDLinkedList;


///////////////////////////////////////////////////////////////////////////////


template <class HashEntry>
class CHash  
{
public:
	CHash();
	virtual ~CHash();

	BOOL Initialize(UINT uiHashSize);

	UINT GetHashSize();

	UINT GetHashIndex(LPCTSTR lpStringKey);

	void Add(HashEntry* pEntry);
	void Remove(HashEntry* pEntry);

	HashEntry* GetPartFirst(UINT uiHashIndex);
	HashEntry* GetPartNext(HashEntry* pEntry);

	HashEntry* GetFirst();
	HashEntry* GetNext(HashEntry* pEntry);

	HashEntry* Find(LPCTSTR lpStringKey);
	HashEntry* FindI(LPCTSTR lpStringKey);


private:
	CDLinkedList<HashEntry>* m_pHashEntryList;
	UINT m_uiHashSize;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CHash.inl"


///////////////////////////////// End of File /////////////////////////////////
