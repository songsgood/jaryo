/******************************************************************************
	CSRMAQueue.cpp
	Single Remover (& Reader) / Multiple Adder Queue
******************************************************************************/


qinclude "stdafx.h"
qinclude "CSRMAQueue.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


void CSRMAQueue::AddTail(PSRMA_QUEUE_ENTRY pBeginEntry, PSRMA_QUEUE_ENTRY pEndEntry)
{
	pEndEntry->pSRMAQEntryNext = &m_Head;
	PSRMA_QUEUE_ENTRY pInitialTail = NULL;
	do
	{
		pInitialTail = m_pTail;
	} while(::InterlockedCompareExchangePointer((PVOID volatile *) &m_pTail, pEndEntry, pInitialTail) != pInitialTail);
	pInitialTail->pSRMAQEntryNext = pBeginEntry;
}


PSRMA_QUEUE_ENTRY CSRMAQueue::RemoveHead()
{
	if(&m_Head == m_Head.pSRMAQEntryNext)
	{
		return NULL;
	}

	if(FALSE != ::IsBadReadPtr(m_Head.pSRMAQEntryNext, sizeof(SRMA_QUEUE_ENTRY)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(m_Head.pSRMAQEntryNext, sizeof(SRMA_QUEUE_ENTRY))"));
		m_Head.pSRMAQEntryNext = &m_Head;
		return NULL;
	}

	PSRMA_QUEUE_ENTRY pHead = NULL;

	while(NULL == pHead)
	{
		pHead = m_Head.pSRMAQEntryNext;
		if(&m_Head == pHead->pSRMAQEntryNext)
		{
			m_Head.pSRMAQEntryNext = &m_Head;
			if(::InterlockedCompareExchangePointer((PVOID volatile *) &m_pTail, &m_Head, pHead) != pHead)
			{
				m_Head.pSRMAQEntryNext = pHead;
				pHead = NULL;
			}
		}
		else
		{
			m_Head.pSRMAQEntryNext = pHead->pSRMAQEntryNext;
		}
	}

	return pHead;
}


///////////////////////////////// End of File /////////////////////////////////
