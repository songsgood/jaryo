/******************************************************************************
	CIOCP.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CIOCP  
{
public:
	CIOCP() : m_hIOCP(NULL) { }
	virtual ~CIOCP();

	BOOL Create(DWORD dwNumberOfConcurrentThreads = 0);

	BOOL AssociateDevice(HANDLE hDevice, ULONG_PTR pulCompletionKey);

	BOOL PostStatus(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred = 0, LPOVERLAPPED pOverlapped = NULL);
	BOOL GetStatus(PULONG_PTR ppulCompletionKey, LPDWORD pdwNumberOfBytes, LPOVERLAPPED* ppOverlapped,
		DWORD dwMilliseconds = INFINITE);


protected:
	HANDLE m_hIOCP;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCP.inl"


///////////////////////////////// End of File /////////////////////////////////
