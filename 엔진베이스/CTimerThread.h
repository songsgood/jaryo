/******************************************************************************
	CThread.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CThread.h"


///////////////////////////////////////////////////////////////////////////////

 class CProfileTimer
 {
 public:
 	CProfileTimer()
 		:_dwCumulativeTime(0)
 		,_dwMaxCal(0)
 		,_dwCount(0)
		,_dwMaxThreadTime(0)
 	{}
 	~CProfileTimer() {}
 
 	void InitializeProfileTimer()
 	{
 		_dwCumulativeTime = 0;
 		_dwMaxCal = 0;
 		_dwCount = 0;
		_dwMaxThreadTime = 0;
 	}
 
 public:
 	DWORD _dwCumulativeTime;
 	DWORD _dwMaxCal;
	DWORD _dwMaxThreadTime;
 	DWORD _dwCount;
 };

class CTimerThread : public CThread  
{
public:
	CTimerThread(DWORD dwMilliseconds = 0);
	virtual ~CTimerThread();

	void SetTimerInterval(DWORD dwMilliseconds);
	DWORD GetTimerInterval();

	void BeginTimer();
	void EndTimer();

private:
 	void ProfileTimer(const DWORD& dwCal, CProfileTimer* proFile);

protected:
	virtual unsigned Run();

	virtual void OnTimer() = 0;


protected:
	HANDLE m_hWaitEvent;

	DWORD m_dwMilliseconds;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CTimerThread.inl"


///////////////////////////////// End of File /////////////////////////////////
