/******************************************************************************
	CTimerThread.inl
******************************************************************************/


inline CTimerThread::CTimerThread(DWORD dwMilliseconds/* = 0*/) 
	: m_hWaitEvent(NULL)
	, m_dwMilliseconds(dwMilliseconds)
{
	VERIFY(NULL != (m_hWaitEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL)));
}


///////////////////////////////////////////////////////////////////////////////


inline void CTimerThread::SetTimerInterval(DWORD dwMilliseconds)
{
	m_dwMilliseconds = dwMilliseconds;
}


inline DWORD CTimerThread::GetTimerInterval()
{
	return m_dwMilliseconds;
}


///////////////////////////////////////////////////////////////////////////////


inline void CTimerThread::BeginTimer()
{
	CreateThread();
}


inline void CTimerThread::EndTimer()
{
	VERIFY_CHECKLASTERROR(FALSE != ::SetEvent(m_hWaitEvent));

	EndThread();
}


///////////////////////////////// End of File /////////////////////////////////
