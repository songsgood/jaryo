/******************************************************************************
	CIOCPThreadManager.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPThreadManager.h"
qinclude "CIOCPThread.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CIOCPThreadManager::CIOCPThreadManager(CIOCPThread* pIOCPThreadAllocator)
	: m_pIOCPThreadAllocator(pIOCPThreadAllocator), m_pFirstIOCPThread(NULL), m_uiIOCPThreadCount(0)
{
}


///////////////////////////////////////////////////////////////////////////////


CIOCPThreadManager::~CIOCPThreadManager()
{
	CIOCPThread* pThread = m_pFirstIOCPThread, * pNextThread = NULL;

	while(NULL != pThread)
	{
		pNextThread = pThread->GetNext();
		pThread->SendEndMessage();
		pThread = pNextThread;
	}

	pThread = m_pFirstIOCPThread;
	while(NULL != pThread)
	{
		pNextThread = pThread->GetNext();
		delete pThread;
		pThread = pNextThread;
	}

	m_pFirstIOCPThread = NULL;

	if(NULL != m_pIOCPThreadAllocator)
	{
		delete m_pIOCPThreadAllocator;
		m_pIOCPThreadAllocator = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


UINT CIOCPThreadManager::AddIOCPThread(UINT uiAdditionalThreadCount/* = 1*/)
{
	UINT uiAllocatedThreadCount = 0;
	CIOCPThread* pNewThread = NULL;

	for(uiAllocatedThreadCount = 0; uiAdditionalThreadCount > uiAllocatedThreadCount; uiAllocatedThreadCount++)
	{
		VERIFY(NULL != (pNewThread = m_pIOCPThreadAllocator->AllocSingleThread()));

		if(NULL == pNewThread)
		{
			break;
		}

		pNewThread->CreateThread();

		pNewThread->SetNext(m_pFirstIOCPThread);
		m_pFirstIOCPThread = pNewThread;
	}

	m_uiIOCPThreadCount += uiAllocatedThreadCount;

	return uiAllocatedThreadCount;
}


///////////////////////////////// End of File /////////////////////////////////
