/******************************************************************************
	CNetAuth.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CPacketComposer;
class CClientSocket;


///////////////////////////////////////////////////////////////////////////////

class CNetAuth  
{
protected:
//	typedef void (*FP_LOAD)(PBYTE, DWORD, int);


public:
	CNetAuth() { }
	virtual ~CNetAuth() { }


public:
//	static bool Auth(TCHAR* pTeamName, TCHAR* pProjectName);
	static bool Auth(AUTH_TYPE eAuthType);


protected:
//	static void Load(FP_LOAD pLoad, PBYTE pCodeData, DWORD dwCodeDataSize);
//	static bool AddIPAddress(CPacketComposer* pPacketComposer);
//	static bool Connect(CClientSocket* pClientSocket, TCHAR* pAddressKeyName);
	static bool GetInfoAuth(TCHAR* szAddr, size_t sizeOfAddr, int& iPort, TCHAR* szPWD, size_t sizeOfPWD, int& iGameID, TCHAR* szClientDN, size_t sizeOfClientDN);
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CNetAuth.inl"


///////////////////////////////// End of File /////////////////////////////////
