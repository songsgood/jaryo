/******************************************************************************
	CHash2PartLock.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CHash2.h"


///////////////////////////////////////////////////////////////////////////////


class CHash2PartLock  
{
public:
	CHash2PartLock();
	CHash2PartLock(CHash2::HASH_SIZE HashSize);
	CHash2PartLock(CHash2::HASH_SIZE HashSize, DWORD dwSpinCount);
	virtual ~CHash2PartLock();


public:
	void Initialize(CHash2::HASH_SIZE HashSize);
	bool Initialize(CHash2::HASH_SIZE HashSize, DWORD dwSpinCount);

	void Lock(CHash2::HASH_INDEX HashIndex);
	void Unlock(CHash2::HASH_INDEX HashIndex);


protected:
	void Allocate(CHash2::HASH_SIZE HashSize);


private:
	CRITICAL_SECTION* m_pCriticalSection;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CHash2PartLock.inl"


///////////////////////////////// End of File /////////////////////////////////
