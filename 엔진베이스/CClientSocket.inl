/******************************************************************************
	CClientSocket.inl
******************************************************************************/


inline CClientSocket::CClientSocket() : m_pServerSocket(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


inline void CClientSocket::SetServerSocket(CServerSocket* pServerSocket)
{
	m_pServerSocket = pServerSocket;
}


inline CServerSocket* CClientSocket::GetServerSocket()
{
	return m_pServerSocket;
}


///////////////////////////////// End of File /////////////////////////////////
