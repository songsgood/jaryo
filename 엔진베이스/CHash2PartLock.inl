/******************************************************************************
	CHash2PartLock.inl
******************************************************************************/


inline CHash2PartLock::CHash2PartLock() :
	m_pCriticalSection(NULL)
{
}


inline CHash2PartLock::CHash2PartLock(CHash2::HASH_SIZE HashSize) :
	m_pCriticalSection(NULL)
{
	Initialize(HashSize);
}


inline CHash2PartLock::CHash2PartLock(CHash2::HASH_SIZE HashSize, DWORD dwSpinCount) :
	m_pCriticalSection(NULL)
{
	Initialize(HashSize, dwSpinCount);
}


///////////////////////////////////////////////////////////////////////////////


inline void CHash2PartLock::Lock(CHash2::HASH_INDEX HashIndex)
{
	::EnterCriticalSection(&(m_pCriticalSection[HashIndex]));
}


inline void CHash2PartLock::Unlock(CHash2::HASH_INDEX HashIndex)
{
	::LeaveCriticalSection(&(m_pCriticalSection[HashIndex]));
}


///////////////////////////////////////////////////////////////////////////////


inline void CHash2PartLock::Allocate(CHash2::HASH_SIZE HashSize)
{
	VERIFY(NULL != (m_pCriticalSection = new CRITICAL_SECTION[HashSize]));
}


///////////////////////////////// End of File /////////////////////////////////
