/******************************************************************************
	TimeFunc.inl
******************************************************************************/


inline DWORD GetTicks()
{
#ifdef WIN32
	return GetTickCount();
#else
	//clock_t LinuxTick = 0;
	//LinuxTick = ::times(NULL);
   	//return (LinuxTick*10);
	return (::times(NULL)*10);
#endif
}


///////////////////////////////// End of File /////////////////////////////////
