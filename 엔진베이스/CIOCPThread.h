/******************************************************************************
	CIOCPThread.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CThread.h"


///////////////////////////////////////////////////////////////////////////////


class CIOCP;


///////////////////////////////////////////////////////////////////////////////


class CIOCPThread : public CThread  
{
public:
	CIOCPThread(CIOCP* pIOCP);
	virtual ~CIOCPThread();

	virtual CIOCPThread* AllocSingleThread();
	static CIOCPThread* Allocate(CIOCP* pIOCP);

	void SetNext(CIOCPThread* pNext);
	CIOCPThread* GetNext();

	virtual BOOL SendEndMessage();


protected:
	virtual unsigned Run();

	/*virtual */BOOL IsControlMessage(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred,
		LPOVERLAPPED pOverlapped, BOOL bIOSucceeded);
	virtual BOOL IsEndMessage(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred,
		LPOVERLAPPED pOverlapped, BOOL bIOSucceeded);

	virtual BOOL ProcessControlMessage(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred,
		LPOVERLAPPED pOverlapped, BOOL bIOSucceeded);
	virtual BOOL ProcessIOMessage(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred,
		LPOVERLAPPED pOverlapped, BOOL bIOSucceeded);

	virtual BOOL ProcessFailedIO(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped);
	virtual BOOL ProcessCompletedIO(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped);


protected:
	CIOCP* m_pIOCP;
	CIOCPThread* m_pNext;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPThread.inl"


///////////////////////////////// End of File /////////////////////////////////
