/******************************************************************************
	CIOCPClientSocketSendState.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


enum IOCP_CLIENTSOCKET_SENDSTATE { ICSSS_NONE = 0, ICSSS_SENDING = 1, ICSSS_SENDINGMODIFIED = 2 };


///////////////////////////////////////////////////////////////////////////////


class CIOCPClientSocketSendState  
{
public:
	CIOCPClientSocketSendState() { }
	virtual ~CIOCPClientSocketSendState() { }


protected:
	LONG m_lIOCPClientSocketSendState;
};


///////////////////////////////// End of File /////////////////////////////////
