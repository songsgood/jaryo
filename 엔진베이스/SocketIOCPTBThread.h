qpragma once

qinclude "CSocketIOCPThread.h"

class CSocketIOCPTBThread;
class CIOCPSocketClient;
typedef void (CSocketIOCPTBThread::*FP_SOCKETIOFUNCTION)(CIOCPSocketClient*, DWORD);
typedef void (CSocketIOCPTBThread::*FP_TBUFFERPACKETPROCESSFUNCTION)(CIOCPSocketClient*, PBYTE, USHORT, USHORT);

class CSocketIOCPTBThread
	: public CSocketIOCPThread  
{
public:
	CSocketIOCPTBThread(CIOCP* pIOCP);
	virtual ~CSocketIOCPTBThread(void);

public:
	static void InitializeTBufferPacketProcessFunction();
	static void CleanupTBufferPacketProcessFunction();
	static void InitializeTBufferIOFunction();
	static void ReleaseTBufferIOFunction();
	static void SetTBufferPacketProcessFunction(UINT uiPacketProcessFunctionState, UINT uiCommand,
		FP_TBUFFERPACKETPROCESSFUNCTION pFunction);

	virtual BOOL ProcessFailedIO(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped);
	virtual BOOL ProcessCompletedIO(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped);

	BOOL ProcessCompletedIO_TBuffer( ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped );

	void OnReceived_TBuffer(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred);
	void OnSent_TBuffer(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred);
	void OnAccepted_TBuffer(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred);
	void OnReceive_TBuffer(CIOCPSocketClient* pIOCPSocketClient);

	virtual void ProcessPacketTBuffer(CIOCPSocketClient* pIOCPSocketClient, PBYTE pBuffer, USHORT usCommand, USHORT usPacketLength);
	virtual void ProcessPacket(CIOCPSocketClient* pIOCPSocketClient);
	void ProcessUnknownTbufferPacket(CIOCPSocketClient* pIOCPSocketClient, PBYTE pBuffer, USHORT usCommand, USHORT usPacketLength);

protected:
	static FP_SOCKETIOFUNCTION* m_fpSocketIO;
	static FP_TBUFFERPACKETPROCESSFUNCTION** m_ppTBufferPacketProcessFunction;
};

///////////////////////////////////////

qinclude "SocketIOCPTBThread.inl"