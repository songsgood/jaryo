/******************************************************************************
	CCircularQueue.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, int iQueueSize>
class CCircularQueue  
{
public:
	CCircularQueue();
	/*virtual */~CCircularQueue() { }

	void Empty();

	void Add(ObjectType Object);
	ObjectType* Pop();
	ObjectType ReadTail();

	bool IsFull();

	int GetItemCount();


protected:
	void SetNextIndex();


private:
	ObjectType m_ObjectList[iQueueSize];
	int m_iHead;
	bool m_bFull;
	int m_iItemCount;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CCircularQueue.inl"


///////////////////////////////// End of File /////////////////////////////////
