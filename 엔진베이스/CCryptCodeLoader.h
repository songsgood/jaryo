/******************************************************************************
	CCryptCodeLoader.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CExeFileAnalyzer;


///////////////////////////////////////////////////////////////////////////////


class CCryptCodeLoader  
{
public:
	CCryptCodeLoader() { }
	virtual ~CCryptCodeLoader() { }


public:
	static bool Load();
	static void Load(PBYTE pCodeData, DWORD dwCodeDataSize, int iDecryptKind);

	static void DSectionFillZero();


protected:
	static bool Load(TCHAR* pCryptCodeFilePath);
	static bool Load(CExeFileAnalyzer* pFileAnalyzer, TCHAR* pSectionName, PBYTE pSectionData, DWORD dwSectionDataSize);


private:
	static PBYTE m_pDSectionAddress;
	static DWORD m_dwDSectionSize;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CCryptCodeLoader.inl"


///////////////////////////////// End of File /////////////////////////////////
