//
// Variables prefixed with t_ are to fake hackers.
// Modify initial values appropriately, noting comments of each variable, if any.
//


#ifdef VARDECL

#define DECLBEGIN
#define UL1(var)		unsigned long var;
#define UL2(var, val)	unsigned long var;
#define US1(var)		unsigned short var;
#define US2(var, val)	unsigned short var;
#define UC1(var)		unsigned char var;
#define UC2(var, val)	unsigned char var;
#define DECLEND			unsigned char t_end;

#else

#ifdef VARSET

#define DECLBEGIN
#define UL1(var)
#define UL2(var, val)	var = val;
#define US1(var)
#define US2(var, val)	var = val;
#define UC1(var)
#define UC2(var, val)	var = val;
#define DECLEND			t_end = 0;

#else

#define DECLBEGIN
#define UL1(var)
#define UL2(var, val)	var(val),
#define US1(var)
#define US2(var, val)	var(val),
#define UC1(var)
#define UC2(var, val)	var(val),
#define DECLEND			t_end(0)

#endif

#endif


DECLBEGIN

#if 0
UL1(t_a)
UL2(t_b, 1726171)
UL2(t_c, 947603)
UL1(key)
UL2(t_T, 556267)
UL1(t_U)
UL1(tag)
UL2(t_o, 17)
UL2(err, 0)
UL2(t_d, 887267)
UL1(c_key)
UL1(t_v)
UL1(s_key)
UL2(t_V, 23297)
UL2(t_W, 331)
UL1(t_X)
UL1(t_Y)
UL1(t_Z)
UL1(cs_key)
UL2(t_e, 740951)
UC1(c1_key)
US1(c2_key)
UL2(t_u, 0)
UC1(c3_key)
UL1(c4_key)
UL2(t_f, 224947)
UL1(t_p)
UC1(s1_key)
US1(s2_key)
UC1(s3_key)
UL2(t_I, 51169)
UL2(t_J, 1999993)
UL1(s4_key)
UL2(t_g, 397)
UL1(c_tag)
UL1(s_tag)
UL2(t_h, 1229)
UL2(s1, 1942215) // 20061107 Encription Key Change  
//UL2(s1, 1823489)		// 20061107 Original
UL2(s3, 0xffffff80)		// 1's followed by 0's
UL1(t_q)
UL2(s3n, 25)			// number of nonzero bits in s3
UL2(s4, 0x0000007f)		// 0's followed by 1's (s3 | s4 == 0xffffffff)
UL2(t_R, 0xffffff00)
UL2(s4n,  7)			// number of nonzero bits in s4 (s3n + s4n == 32)
UL2(t_i, 40543)
UL2(c1, 975991)			//
UL1(t_w)
UL2(c2, 153)			//
UL2(c3, 0xfffff800)		// 1's followed by 0's
UL2(c3n, 21)			// number of nonzero bits in c3
UL2(t_r, 23)
UL2(c4, 0x000007ff)		// 0's followed by 1's (s3 | s4 == 0xffffffff)
UL1(t_K)
UL2(c4n, 11)			// number of nonzero bits in c4 (c3n + c4n == 32)
UL1(t_j)
UL2(t_k, 31)
UL1(t_y)
UL2(t_A, 9241)
UL2(t_B, 9011)
UL2(t_C, 123)
UL2(t_D, 198)
UC2(c1_inc, 5)			//
US2(c2_inc, 1)
UC2(c3_inc, 9)
UL2(t_F, 8)
UL2(t_G, 24)
UL2(c4_inc, 1023)
UC2(s1_inc, 31)			//
US2(s2_inc, 12)
UL1(t_z)
UC2(s3_inc, 3)
UL2(s4_inc, 11)
UL2(t_E, 1)
UL2(t_H, 2)
UL2(c_shift1, 5)		// 0 ~ 24
UL2(c_shift2, 9)		// 0 ~ 16
UL2(c_shift3, 17)		// 0 ~ 24
UL2(t_s, 31)
UL2(c_shift4, 11)		// 0 ~ 32
UL2(c_shift5, 21)		// 0 ~ 32 (c_shift4 + c_shift5 == 32)
UL2(t_l, 73)
UL1(t_M)
UL2(t_N, 4)
UL2(t_O, 9)
UL2(t_P, 0xffff0000)
UL1(t_Q)
UL2(t_S, 0x000000ff)
UL2(s_shift1, 3)		// 0 ~ 24
UL1(t_x)
UL2(s_shift2, 7)		// 0 ~ 16
UL2(s_shift3, 2)		// 0 ~ 24
UL2(t_t, 527419)
UL2(s_shift4, 27)		// 0 ~ 32
UL2(s_shift5, 5)		// 0 ~ 32 (s_shift4 + s_shift5 == 32)
UL2(t_m, 487)
UL2(t_n, 5)
UL1(t_L)
#endif

UL1(key)
UL1(tag)
UL1(c_key)
UL1(s_key)
UL1(cs_key)
UC1(c1_key)
US1(c2_key)
UC1(c3_key)
UL1(c4_key)
UC1(s1_key)
US1(s2_key)
UC1(s3_key)
UL1(s4_key)
UL1(c_tag)
UL1(s_tag)
UL2(s1, 1942215) // 20061107 Encription Key Change  
//UL2(s1, 1823489) // Original
UL2(s3n, 25)
UL2(s4n,  7)
UC2(c1_inc, 5)
US2(c2_inc, 1)
UC2(c3_inc, 9)
UL2(c4_inc, 1023)
UC2(s1_inc, 31)
US2(s2_inc, 12)
UC2(s3_inc, 3)
UL2(s4_inc, 11)
UL2(c_shift1, 5)
UL2(c_shift2, 9)
UL2(c_shift3, 17)
UL2(c_shift4, 11)
UL2(c_shift5, 21)
UL2(s_shift1, 3)
UL2(s_shift2, 7)
UL2(s_shift3, 2)
UL2(s_shift4, 27)
UL2(s_shift5, 5)

DECLEND


#undef DECLBEGIN
#undef UL1
#undef UL2
#undef US1
#undef US2
#undef UC1
#undef UC2
#undef DECLEND
