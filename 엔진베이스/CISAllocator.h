/******************************************************************************
	CISAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CInterlockedStack.h"


///////////////////////////////////////////////////////////////////////////////


class CISAllocatorEntry;


///////////////////////////////////////////////////////////////////////////////


class CISAllocator  
{
public:
	CISAllocator(CISAllocatorEntry* pAllocatorEntry = NULL);
	virtual ~CISAllocator() { }

	static CISAllocator* AllocAllocator(CISAllocatorEntry* pAllocatorEntry, UINT uiPreAllocCount = 0);

	CISAllocatorEntry* Allocate();
	void Free(CISAllocatorEntry* pEntry);

	UINT PreAlloc(UINT uiInitialCount);


protected:
	CInterlockedStack m_Stack;
	CISAllocatorEntry* m_pAllocatorEntry;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CISAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
