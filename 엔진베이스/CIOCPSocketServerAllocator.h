/******************************************************************************
	CIOCPSocketServerAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketServerAllocator.h"


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketServerAllocator : public CSocketServerAllocator  
{
public:
	CIOCPSocketServerAllocator(CIOCP* pIOCP);
	virtual ~CIOCPSocketServerAllocator() { }

	virtual CServiceStatus* AllocServiceStatus(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv);

	CIOCP* GetIOCP();


private:
	CIOCP* m_pIOCP;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketServerAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
