/******************************************************************************
	CInterlockedBool.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CInterlockedBool  
{
public:
	CInterlockedBool(BOOL bInitValue = FALSE);
	virtual ~CInterlockedBool() { }

	BOOL Set();
	BOOL UnSet();

	BOOL Get();

	operator BOOL();


private:
	LONG m_lBool;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CInterlockedBool.inl"


//////////////////////////////// End of File //////////////////////////////////
