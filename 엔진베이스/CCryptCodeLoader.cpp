/******************************************************************************
	CCryptCodeLoader.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CCryptCodeLoader.h"
qinclude "ExeFileAnalyzer.h"
qinclude "SectionHeaderIterator.h"
qinclude "DecryptRijndael.h"
qinclude "DecryptBlowFish.h"
qinclude "DecryptApi.h"
qinclude "SystemDataFile.h"


///////////////////////////////////////////////////////////////////////////////


PBYTE CCryptCodeLoader::m_pDSectionAddress = NULL;
DWORD CCryptCodeLoader::m_dwDSectionSize = 0;


///////////////////////////////////////////////////////////////////////////////


bool CCryptCodeLoader::Load()
{	// return true;
	TCHAR szPath[_MAX_PATH];
	memset(szPath, 0, _MAX_PATH);

	if(0 == ::GetWindowsDirectory(szPath, _MAX_PATH))
	{
		return false;
	}

	CString strPath = szPath;

	if('\\' != strPath[strPath.GetLength() - 1])
	{
		strPath += "\\";
	}

	strPath += "js";
	strPath += "cc.";
	strPath += "dl";
	strPath += "l";

	return Load((TCHAR*) (LPCTSTR) strPath);
}


bool CCryptCodeLoader::Load(TCHAR* pCryptCodeFilePath)
{
	CFile2 CryptCodeFile;

	if(!CryptCodeFile.Create(pCryptCodeFilePath))
	{
		return false;
	}

	DWORD dwCodeDataSize = CryptCodeFile.GetFileSize();
	if(INVALID_FILE_SIZE == dwCodeDataSize)
	{
		return false;
	}

	PBYTE pCodeData = new BYTE[dwCodeDataSize];

	DWORD dwNumberOfBytesRead = 0;
	if(CryptCodeFile.Read(pCodeData, dwCodeDataSize, &dwNumberOfBytesRead))
	{
		if(dwCodeDataSize == dwNumberOfBytesRead)
		{
			Load(pCodeData, dwCodeDataSize, 1);
		}
	}

	delete []pCodeData;

	return true;
}


void CCryptCodeLoader::Load(PBYTE pCodeData, DWORD dwCodeDataSize, int iDecryptKind)
{
	TCHAR FilePath[MAX_PATH + 1] = TEXT("");
	if(::GetModuleFileName(NULL, FilePath, MAX_PATH + 1) <= 0)
	{
		return;
	}

	CExeFileAnalyzer FileAnalyzer;
	if(!FileAnalyzer.Open(FilePath))
	{
		return;
	}

	if(dwCodeDataSize < sizeof(int))
	{
		return;
	}

	int iVersion = *((int*) pCodeData);
	iVersion;
	pCodeData += sizeof(int);
	dwCodeDataSize -= sizeof(int);

	BYTE Key[32] = { 0x00, 0xB0, 0xE5, 0x86, 0x21, 0x98, 0x68, 0x2C,
		0x65, 0x1A, 0x49, 0x74, 0x18, 0xE0, 0x2D, 0x26,
		0xA9, 0x08, 0x10, 0xEC, 0x5D, 0x0A, 0x41, 0x54,
		0x50, 0x30, 0xB5, 0xA6, 0x31, 0xD8, 0xD8, 0xCB };

	switch(iDecryptKind)
	{
	case 1:
		if(!::GetHWAuthKey(Key))
		{
			return;
		}
		break;

	case 2:
		break;

	default:
		return;
	}

	PBYTE pTempBuf = new BYTE[dwCodeDataSize];

	CDecryptApi DecryptApi;
	DecryptApi.SetParameter(MS_DEF_RSA_SCHANNEL_PROV, PROV_RSA_SCHANNEL, CALG_DES);
	int iRetVal = DecryptApi.Decrypt(Key, sizeof(Key), pCodeData, pTempBuf, dwCodeDataSize, dwCodeDataSize);
	if(iRetVal <= 0)
	{
		return;
	}

	dwCodeDataSize = iRetVal;

	switch(iDecryptKind)
	{
	case 1:
		{
			CDecryptRijndael DecryptRijndael;
			DecryptRijndael.Initialize((char*) Key, sizeof(Key));
			DecryptRijndael.Decrypt((char*) pTempBuf, (char*) pCodeData, dwCodeDataSize);
		}
		break;

	case 2:
		{
			CDecryptBlowFish DecryptBlowFish;
			DecryptBlowFish.Initialize((char*) Key, sizeof(Key));
			DecryptBlowFish.Decrypt((char*) pTempBuf, (char*) pCodeData, dwCodeDataSize);
		}
		break;

	default:
		return;
	}

	while(dwCodeDataSize >= IMAGE_SIZEOF_SHORT_NAME + sizeof(DWORD))
	{
		PBYTE pSectionName = pCodeData;
		pCodeData += IMAGE_SIZEOF_SHORT_NAME;
		dwCodeDataSize -= IMAGE_SIZEOF_SHORT_NAME;

		DWORD dwSectionDataSize = *((DWORD*) pCodeData);
		pCodeData += sizeof(DWORD);
		dwCodeDataSize -= sizeof(DWORD);

		if(dwSectionDataSize > 0)
		{
			if(dwCodeDataSize < dwSectionDataSize)
			{
				return;
			}

			PBYTE pSectionData = pCodeData;
			pCodeData += dwSectionDataSize;
			dwCodeDataSize -= dwSectionDataSize;

			if(!Load(&FileAnalyzer, (TCHAR*) pSectionName, pSectionData, dwSectionDataSize))
			{
				return;
			}
		}
	}

	return;
}


///////////////////////////////////////////////////////////////////////////////


bool CCryptCodeLoader::Load(CExeFileAnalyzer* pFileAnalyzer, TCHAR* pSectionName, PBYTE pSectionData, DWORD dwSectionDataSize)
{
	CSectionHeaderIterator SectionHeaderIterator(pFileAnalyzer);

	PIMAGE_SECTION_HEADER pSectionHeader = SectionHeaderIterator.FindSectionHeader(pSectionName);
	if(NULL == pSectionHeader)
	{
		return false;
	}

	PBYTE pBaseAddress = (pFileAnalyzer->GetNTHeader()->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC) ? (PBYTE) (pFileAnalyzer->GetNTHeader()->OptionalHeader.ImageBase) : (PBYTE) (pFileAnalyzer->GetNTHeader64()->OptionalHeader.ImageBase);
	DWORD dwNumberOfBytesWritten = 0;
	if(!::WriteProcessMemory(::GetCurrentProcess(), pBaseAddress + pSectionHeader->PointerToRawData, pSectionData, ::GetMin(dwSectionDataSize, pSectionHeader->SizeOfRawData), &dwNumberOfBytesWritten))
	{
		return false;
	}

	if(0 == ::lstrcmpi(pSectionName, ".jsed"))
	{
		m_pDSectionAddress = pBaseAddress + pSectionHeader->PointerToRawData;
		m_dwDSectionSize = ::GetMin(dwSectionDataSize, pSectionHeader->SizeOfRawData);
	}

	return true;
}


void CCryptCodeLoader::DSectionFillZero()
{
	if( (NULL != m_pDSectionAddress) && (m_dwDSectionSize > 0) )
	{
		PBYTE pBlankBuf = new BYTE[m_dwDSectionSize];
		::ZeroMemory(pBlankBuf, m_dwDSectionSize);

		DWORD dwNumberOfBytesWritten = 0;
		::WriteProcessMemory(::GetCurrentProcess(), m_pDSectionAddress, pBlankBuf, m_dwDSectionSize, &dwNumberOfBytesWritten);

		delete []pBlankBuf;
	}
}


///////////////////////////////// End of File /////////////////////////////////
