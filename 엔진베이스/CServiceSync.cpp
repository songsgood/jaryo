/******************************************************************************
	CServiceSync.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CServiceSync.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CServiceSync::~CServiceSync()
{
	if(NULL != m_hEvent)
	{
		VERIFY_CHECKLASTERROR(FALSE != (::CloseHandle(m_hEvent)));
		m_hEvent = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


int CServiceSync::Sleep(DWORD dwInterval)
{
	if(WAIT_TIMEOUT == ::WaitForSingleObjectEx(m_hEvent, dwInterval, FALSE))
	{
		return 1;
	}

	if(m_bWakeUp)
	{
		m_bWakeUp = false;
		return 0;
	}

	return 2;
}


///////////////////////////////// End of File /////////////////////////////////
