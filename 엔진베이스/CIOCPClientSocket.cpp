/******************************************************************************
	CIOCPClientSocket.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPClientSocket.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CIOCPClientSocket::CIOCPClientSocket(CIOCPSocketClient* pIOCPSocketClient, CPacketCoder* pPacketCoder) :
	m_SendBufferQueue(pPacketCoder), m_SendOverlapped(pIOCPSocketClient), m_ReceiveOverlapped(pIOCPSocketClient),
	m_ibSending(FALSE), m_ibClosing(FALSE), m_uiMaxSendDataSize(1024), m_uiMinReceiveBufferFreeSize(1024)
{
	m_dwLastSuccessfulIOTick = GetTickCount();
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPClientSocket::PostAcceptEx()
{
	DWORD dwBytesReceived = 0;
	m_ReceiveOverlapped.SetOpCode(OP_ACCEPT);
	return CWSASocket::AcceptEx(GetServerSocket()->GetHandle(), m_hSocket, (BYTE*) m_SockAddrBuffer, 0,
		sizeof(SOCKADDR_IN) + 16, sizeof(SOCKADDR_IN) + 16, &dwBytesReceived, (LPOVERLAPPED) &m_ReceiveOverlapped);
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPClientSocket::OnReceived(DWORD dwReceivedDataSize)
{
	m_ReceiveBuffer.DataAdded(dwReceivedDataSize);

	m_dwLastSuccessfulIOTick = GetTickCount();
}


void CIOCPClientSocket::OnSent(DWORD dwSentDataSize)
{
	m_SendBufferQueue.Removed(dwSentDataSize);
	m_ibSending.UnSet();

	m_dwLastSuccessfulIOTick = GetTickCount();
}


///////////////////////////////////////////////////////////////////////////////


/*BOOL CIOCPClientSocket::PostWSASend()
{
	do
	{
		if(FALSE == m_ibSending.Set())
		{
			return FALSE;
		}

		WSABUF WSABuf[50];
		::ZeroMemory(WSABuf, sizeof(WSABuf));

		DWORD dwWSABufCount = m_SendBufferQueue.GetWSABuf(WSABuf, DIMOF(WSABuf), m_uiMaxSendDataSize);

		if(dwWSABufCount > 0)
		{
			DWORD dwNumberOfBytesSent = 0;
			m_SendOverlapped.SetOpCode(OP_SEND);

			if(FALSE == CWSASocket::WSASend(WSABuf, dwWSABufCount, &dwNumberOfBytesSent, 0, (LPOVERLAPPED) &m_SendOverlapped,
				NULL))
			{
				return FALSE;
			}

			return TRUE;
		}

		m_ibSending.UnSet();
	} while(FALSE != m_SendBufferQueue.IsDirty());

	return FALSE;
}*/


BOOL CIOCPClientSocket::PostWSASend()
{
	do
	{
		if(FALSE == m_ibSending.Set())
		{
			return FALSE;
		}

		WSABUF WSABuf[50];
		::ZeroMemory(WSABuf, sizeof(WSABuf));

		DWORD dwWSABufCount = m_SendBufferQueue.GetWSABuf(WSABuf, DIMOF(WSABuf), m_uiMaxSendDataSize);

		if(dwWSABufCount > 0)
		{
			// 2010.01.11
			// Modify for Migration 2005
			// ktKim
			return PostWSASend(&CSocketIOCPOverlapped::SetOpCode, &CWSASocket::WSASend, WSABuf, dwWSABufCount);
			// End
		}

		m_ibSending.UnSet();
	} while(FALSE != m_SendBufferQueue.IsDirty());

	return FALSE;
}


qpragma code_seg(".jsenw2")


BOOL CIOCPClientSocket::PostWSASend(FP_SET_OPCODE pSetOpCode, FP_WSASEND pWSASend, WSABUF* pWSABuf, DWORD dwWSABufCount)
{
	DWORD dwNumberOfBytesSent = 0;
	(m_SendOverlapped.*pSetOpCode)(OP_SEND);

	return (this->*pWSASend)(pWSABuf, dwWSABufCount, &dwNumberOfBytesSent, 0, (LPOVERLAPPED) &m_SendOverlapped,
		NULL);
}


qpragma code_seg()


/*BOOL CIOCPClientSocket::PostWSAReceive()
{
	if(m_ReceiveBuffer.GetFreeSize() < m_uiMinReceiveBufferFreeSize)
	{
		m_ReceiveBuffer.MoveDataToBegin();
		if(0 == m_ReceiveBuffer.GetFreeSize())
		{
			return FALSE;
		}
	}

	WSABUF WSABuf;
	WSABuf.buf = (char*) m_ReceiveBuffer.GetTail();
	WSABuf.len = m_ReceiveBuffer.GetFreeSize();

	DWORD dwFlags = 0, dwNumberOfBytesRecvd = 0;
	m_ReceiveOverlapped.SetOpCode(OP_RECV);

	return CWSASocket::WSARecv(&WSABuf, 1, &dwNumberOfBytesRecvd, &dwFlags, (LPOVERLAPPED) &m_ReceiveOverlapped, NULL);
}*/


BOOL CIOCPClientSocket::PostWSAReceive()
{
	// 2010.01.11
	// Modify for Migration 2005
	// ktKim
	return PostWSAReceive(&CQueueBuffer::GetFreeSize, &CQueueBuffer::MoveDataToBegin, &CQueueBuffer::GetTail, &CSocketIOCPOverlapped::SetOpCode, &CWSASocket::WSARecv);
	// End
}


qpragma code_seg(".jsenw3")


BOOL CIOCPClientSocket::PostWSAReceive(FP_GET_FREESIZE pGetFreeSize, FP_MOVE_DATA_TOBEGIN pMoveDataToBegin, FP_GET_TAIL pGetTail, FP_SET_OPCODE pSetOpCode, FP_WSARECV pWSARecv)
{
	if((m_ReceiveBuffer.*pGetFreeSize)() < m_uiMinReceiveBufferFreeSize)
	{
		(m_ReceiveBuffer.*pMoveDataToBegin)();
		if(0 == (m_ReceiveBuffer.*pGetFreeSize)())
		{
			return FALSE;
		}
	}

	WSABUF WSABuf;
	WSABuf.buf = (char*) (m_ReceiveBuffer.*pGetTail)();
	WSABuf.len = (m_ReceiveBuffer.*pGetFreeSize)();

	DWORD dwFlags = 0, dwNumberOfBytesRecvd = 0;
	(m_ReceiveOverlapped.*pSetOpCode)(OP_RECV);

	return (this->*pWSARecv)(&WSABuf, 1, &dwNumberOfBytesRecvd, &dwFlags, (LPOVERLAPPED) &m_ReceiveOverlapped, NULL);
}


qpragma code_seg()


///////////////////////////////////////////////////////////////////////////////


void CIOCPClientSocket::InitializeInstance()
{
	m_ReceiveBuffer.Empty();
	// m_SendBufferQueue.Empty();
	m_SendOverlapped.Initialize();
	m_ReceiveOverlapped.Initialize();
	m_SockAddrBuffer.InitializeInstance();
	m_ibSending.UnSet();
	m_ibClosing.UnSet();

	m_dwLastSuccessfulIOTick = GetTickCount();
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPClientSocket::DisconnectClient()
{
	if(FALSE == m_ibClosing.Set())
	{
		return FALSE;
	}
	ShutDown();
	CloseSocket();
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPClientSocket::GetRemoteAddress(BYTE pucRemoteIP[4])
{
	SOCKADDR_IN* pRemoteAddress = m_SockAddrBuffer.GetRemoteSockAddr();
	if(NULL == pRemoteAddress)
	{
		return FALSE;
	}
	pucRemoteIP[0] = pRemoteAddress->sin_addr.S_un.S_un_b.s_b1;
	pucRemoteIP[1] = pRemoteAddress->sin_addr.S_un.S_un_b.s_b2;
	pucRemoteIP[2] = pRemoteAddress->sin_addr.S_un.S_un_b.s_b3;
	pucRemoteIP[3] = pRemoteAddress->sin_addr.S_un.S_un_b.s_b4;
	return TRUE;
}

BOOL CIOCPClientSocket::GetRemotePort( unsigned short& usPort )
{
	SOCKADDR_IN* pRemoteAddress = m_SockAddrBuffer.GetRemoteSockAddr();
	if(nullptr == pRemoteAddress)
	{
		return FALSE;
	}

	usPort = pRemoteAddress->sin_port;

	return TRUE;
}


///////////////////////////////// End of File /////////////////////////////////
