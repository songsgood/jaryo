/******************************************************************************
	CService.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CService.h"
qinclude "Debug.h"
qinclude "MessageBox.h"
qinclude "CServer.h"

extern CLogManager g_LogManager;

///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CService* CService::m_pThis = NULL;

int CService::m_iGlobalArgc = 0;
LPTSTR* CService::m_ppGlobalArgv = NULL;


///////////////////////////////////////////////////////////////////////////////


CService::CService()
{
	if(NULL == m_pThis)
	{
		m_pThis = this;
	}
}


///////////////////////////////////////////////////////////////////////////////


BOOL CService::StartServiceCtrlDispatcher(LPSERVICE_TABLE_ENTRY lpServiceTable/* = NULL*/, BOOL bDebugMode/* = FALSE*/,
	DWORD dwArgc/* = 0*/, LPTSTR* pszArgv/* = NULL*/)
{
	SERVICE_TABLE_ENTRY ServiceTable[] = {
		{ TEXT("Wittgen Service"), CService::ServiceMain },
		{ NULL, NULL }
	};

	if(NULL == lpServiceTable)
	{
		lpServiceTable = ServiceTable;
	}

	BOOL bRetVal = FALSE;

	if(FALSE == bDebugMode)
	{
		VERIFY_CHECKLASTERROR(FALSE != (bRetVal = ::StartServiceCtrlDispatcher(lpServiceTable)));
	}
	else
	{
		bRetVal = TRUE;
		dwArgc; pszArgv;
		IServiceMain(m_iGlobalArgc, m_ppGlobalArgv, m_iGlobalArgc, m_ppGlobalArgv, bDebugMode);
	}

	return bRetVal;
}


void CService::SetGlobalParameter(int iGlobalArgc, LPTSTR* ppGlobalArgv)
{
	m_iGlobalArgc = iGlobalArgc;
	m_ppGlobalArgv = ppGlobalArgv;
}


///////////////////////////////////////////////////////////////////////////////


void WINAPI CService::ServiceMain(DWORD dwArgc, LPTSTR* pszArgv)
{
	m_pThis->IServiceMain(m_iGlobalArgc, m_ppGlobalArgv, dwArgc, pszArgv);
}


///////////////////////////////////////////////////////////////////////////////


void CService::IServiceMain(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv,
	BOOL bDebugMode/* = FALSE*/)
{
	iGlobalArgc;
	ppGlobalArgv;
	dwArgc;
	pszArgv;

	BOOL bServiceStartPended = FALSE, bServiceFailed = TRUE;

	CServiceStatus ServiceStatus(pszArgv[0]);

	ServiceStatus.SetDebugMode(bDebugMode);
	ServiceStatus.SetService(this);

	CServer* pServer = NULL;
	if(NULL != (pServer = CreateServer(iGlobalArgc, ppGlobalArgv, dwArgc, pszArgv)))
	{
		if(FALSE != ServiceStatus.RegisterServiceCtrlHandlerEx(HandlerEx))
		{
			if(FALSE != (bServiceStartPended = ServiceStatus.BeginStateTransition(SERVICE_RUNNING)))	// Try to start.
			{
				bServiceFailed = !Main(pServer, &ServiceStatus);
			}
		}
	}

	if(FALSE != bServiceFailed)
	{
#ifdef _DEBUG
		::ShowMessageBox(TEXT("Service Failed"));
#else
		g_LogManager.WriteLog("Service Failed");
#endif
	}

	if(FALSE != bServiceStartPended)
	{
		ServiceStatus.StateTransitionToStop();
	}
}


BOOL CService::Main(CServer* pServer, CServiceStatus* pServiceStatus)
{
	if(FALSE != pServer->OnInitialize())
	{
		__try
		{
			pServer->InitializeServiceSync(pServiceStatus->GetServiceSync());

			if(FALSE != pServiceStatus->EndStateTransition())	// SERVICE_RUNNING
			{
				return Run(pServiceStatus, pServer);
			}
		}
		__finally
		{
			g_LogManager.WriteLogToFile("Service Closed");
			pServer->OnClose();
		}
	}

	return FALSE;
}


///////////////////////////////////////////////////////////////////////////////


DWORD CService::IHandlerEx(DWORD dwControl, DWORD dwEventType, LPVOID pEventData, LPVOID pContext)
{
	DWORD dwReturn = ERROR_CALL_NOT_IMPLEMENTED;

	switch(dwControl)
	{
	case SERVICE_CONTROL_CONTINUE:
		dwReturn = OnContinue((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_INTERROGATE:
		dwReturn = OnInterrogate((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_NETBINDADD:
		dwReturn = OnNetBindAdd((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_NETBINDDISABLE:
		dwReturn = OnNetBindDisable((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_NETBINDENABLE:
		dwReturn = OnNetBindEnable((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_NETBINDREMOVE:
		dwReturn = OnNetBindRemove((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_PARAMCHANGE:
		dwReturn = OnParamChange((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_PAUSE:
		dwReturn = OnPause((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_SHUTDOWN:
		dwReturn = OnShutdown((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_STOP:
		dwReturn = OnStop((CServiceStatus*) pContext);
		break;

	case SERVICE_CONTROL_DEVICEEVENT:
		dwReturn = OnDeviceEvent((CServiceStatus*) pContext, dwEventType, pEventData);
		break;

	case SERVICE_CONTROL_HARDWAREPROFILECHANGE:
		dwReturn = OnHardwareProfileChange((CServiceStatus*) pContext, dwEventType);
		break;

	case SERVICE_CONTROL_POWEREVENT:
		dwReturn = OnPowerEvent((CServiceStatus*) pContext, dwEventType, pEventData);
		break;

	default:
		if( (dwControl >= 128) && (dwControl <= 255) )
		{
			dwReturn = OnCustomHandlerEx((CServiceStatus*) pContext, dwControl);
		}
		break;
	}

	return dwReturn;
}


///////////////////////////////////////////////////////////////////////////////


DWORD CService::OnStop(CServiceStatus* pServiceStatus)
{
	if(FALSE == pServiceStatus->BeginStateTransition(SERVICE_STOPPED))
	{
		return ERROR_CALL_NOT_IMPLEMENTED;
	}

	pServiceStatus->GetServiceSync()->WakeUp();

	return NO_ERROR;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CService::Run(CServiceStatus* pServiceStatus, CServer* pServer)
{
	int iSleepResult = 0;

	while((iSleepResult = pServiceStatus->GetServiceSync()->Sleep(pServer->GetServerTimerInterval())) > 0)
	{
		if(2 == iSleepResult)
		{
			pServer->OnEventSignaled();
		}

		pServer->OnServerTimer();
	}

	return TRUE;
}


///////////////////////////////// End of File /////////////////////////////////
