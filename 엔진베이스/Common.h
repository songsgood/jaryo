/******************************************************************************
	Common.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


#define MAX_IPADDRESS_LENGTH	15


///////////////////////////////////////////////////////////////////////////////


#define DIMOF(Array)	(sizeof(Array) / sizeof((Array)[0]))


///////////////////////////////////////////////////////////////////////////////


#define MAX(x, y)	(((x) >= (y)) ? (x) : (y))


#define MIN(x, y)	(((x) >= (y)) ? (y) : (x))


#define BETWEEN(Value, From, To) \
	( \
		((Value) <= (From)) ? (From) : \
		( \
			((Value) >= (To)) ? (To) : \
			(Value) \
		) \
	)


template <class DATA_TYPE>
inline DATA_TYPE GetMax(DATA_TYPE X, DATA_TYPE Y)
{
	return (((X) >= (Y)) ? (X) : (Y));
}


template <class DATA_TYPE>
inline DATA_TYPE GetMin(DATA_TYPE X, DATA_TYPE Y)
{
	return (((X) >= (Y)) ? (Y) : (X));
}


template <class DATA_TYPE>
inline DATA_TYPE GetBetween(DATA_TYPE Value, DATA_TYPE Min, DATA_TYPE Max)
{
	return ((Value <= Min) ? Min : ((Value >= Max) ? Max : Value));
}


///////////////////////////////////////////////////////////////////////////////


#define GET_LOWVALUE_PERCENTAGE(Value, Max)	((100 * (Value)) / (Max) )
#define GET_LOWVALUE_PERCENTAGEVALUE(Value, Percentage)	(((Value) * (Percentage)) / (100))
#define GET_PERCENTAGEVALUE(Value, Percentage)	((((double) (Value)) * (Percentage)) / (100))


#define IS_RANDOM_PERCENTAGE_TRUE(Percentage)	( ((DWORD) (Percentage)) > (::GetRandomNumber15() ) )
) )


///////////////////////////////////////////////////////////////////////////////


#define GET_RAND30RANGE_VALUE(Min, Max)	((Min) + (::GetRandomNumber30() ((Max) - (Min) + 1)) )



///////////////////////////////////////////////////////////////////////////////


// I : Including
// E : Excluding
#define IS_BETWEEN_II(Value, From, To)	( ((Value) >= (From)) && ((Value) <= (To)) )
#define IS_BETWEEN_EI(Value, From, To)	( ((Value) > (From)) && ((Value) <= (To)) )
#define IS_BETWEEN_IE(Value, From, To)	( ((Value) >= (From)) && ((Value) < (To)) )
#define IS_BETWEEN_EE(Value, From, To)	( ((Value) > (From)) && ((Value) < (To)) )


#define IS_OUTER_II(Value, From, To)	( ((Value) <= (From)) || ((Value) >= (To)) )
#define IS_OUTER_EI(Value, From, To)	( ((Value) < (From)) || ((Value) >= (To)) )
#define IS_OUTER_IE(Value, From, To)	( ((Value) <= (From)) || ((Value) > (To)) )
#define IS_OUTER_EE(Value, From, To)	( ((Value) < (From)) || ((Value) > (To)) )


template <class DATA_TYPE>
inline bool IsBetweenII(DATA_TYPE Value, DATA_TYPE Min, DATA_TYPE Max)
{
	return ( ((Value) >= (Min)) && ((Value) <= (Max)) );
}


template <class DATA_TYPE>
inline bool IsBetweenEI(DATA_TYPE Value, DATA_TYPE Min, DATA_TYPE Max)
{
	return ( ((Value) > (Min)) && ((Value) <= (Max)) );
}


template <class DATA_TYPE>
inline bool IsBetweenIE(DATA_TYPE Value, DATA_TYPE Min, DATA_TYPE Max)
{
	return ( ((Value) >= (Min)) && ((Value) < (Max)) );
}


template <class DATA_TYPE>
inline bool IsBetweenEE(DATA_TYPE Value, DATA_TYPE Min, DATA_TYPE Max)
{
	return ( ((Value) > (Min)) && ((Value) < (Max)) );
}


///////////////////////////////////////////////////////////////////////////////


#define IS_BIGGER_RECT_I(Left1, Top1, Right1, Bottom1, Left2, Top2, Right2, Bottom2) \
	( ((Left2) >= (Left1)) && ((Top2) >= (Top1)) && ((Right1) >= (Right2)) && ((Bottom1) >= (Bottom2)) )


///////////////////////////////////////////////////////////////////////////////


#define SUBTRACT_BASEZERO(Big, Small)	( ((Big) > (Small)) ? ((Big) - (Small)) : 0 )
#define SUBTRACE_BIGSUBSMALL(Num1, Num2)	( ((Num1) >= (Num2)) ? ((Num1) - (Num2)) : ((Num2) - (Num1)) )


///////////////////////////////////////////////////////////////////////////////


#define INIT_TCHAR_ARRAY(Name)	Name[0] = TEXT('\0');


#define DECLARE_INIT_TCHAR_ARRAY(Name, Count)	TCHAR Name[Count] = {TEXT('\0')};


#define LSTRCPYN(pDestination, pSource, uiDestinationBufferSize) \
	{ \
		UINT uiTempSourceLength = ::lstrlen(pSource); \
		VERIFY(NULL != ::lstrcpyn((pDestination), (pSource), MIN((uiDestinationBufferSize), uiTempSourceLength + 1))); \
	}


#define LSTRCPYNTOARRAY(pDestination, pSource) \
	{ \
		UINT uiTempSourceLength = ::lstrlen(pSource); \
		VERIFY(NULL != ::lstrcpyn((pDestination), (pSource), MIN(DIMOF(pDestination), uiTempSourceLength + 1))); \
	}


///////////////////////////////////////////////////////////////////////////////


inline DWORD GetRandomNumber12()
{
	return (::rand() & 0x0FFF);
}


inline DWORD GetRandomNumber15()
{
	return ::rand();
}


inline DWORD GetRandomNumber24()
{
	return ((::GetRandomNumber12() << 12) + ::GetRandomNumber12());
}


inline DWORD GetRandomNumber30()
{
	return ((::rand() << 15) + rand());
}


inline DWORD GetRandPercentage()
{
	return (GetRandomNumber15() );
0);
}


///////////////////////////////////////////////////////////////////////////////
enum AUTH_TYPE
{
	AUTH_TYPE_NONE,
	AUTH_TYPE_DEBUG,
	AUTH_TYPE_RELEASE,
};


qinclude "CLogManager.h"
qinclude "BaseTypes.h"


///////////////////////////////// End of File /////////////////////////////////
