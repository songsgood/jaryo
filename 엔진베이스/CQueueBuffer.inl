/******************************************************************************
	CQueueBuffer.inl
******************************************************************************/


inline PBYTE CQueueBuffer::GetHead()
{
	return (GetBuffer() + m_uiHead);
}


inline PBYTE CQueueBuffer::GetTail()
{
	return (GetBuffer() + m_uiTail);
}


///////////////////////////////////////////////////////////////////////////////


inline UINT CQueueBuffer::GetDataSize()
{
	return (SUBTRACT_BASEZERO(m_uiTail, m_uiHead));
}


inline UINT CQueueBuffer::GetFreeSize()
{
	return (SUBTRACT_BASEZERO(GetBufferSize(), m_uiTail));
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CQueueBuffer::IsEmpty()
{
	return (m_uiHead >= m_uiTail);
}


///////////////////////////////////////////////////////////////////////////////


inline void CQueueBuffer::DataAdded(UINT uiAddedDataSize)
{
	m_uiTail += uiAddedDataSize;
}


inline void CQueueBuffer::DataRemoved(UINT uiRemovedDataSize)
{
	m_uiHead += uiRemovedDataSize;
}


///////////////////////////////////////////////////////////////////////////////


inline void CQueueBuffer::Set(UINT uiOffset, PBYTE pData, UINT uiDataSize)
{
	::CopyMemory(GetHead() + uiOffset, pData, uiDataSize);
}


inline void CQueueBuffer::Get(UINT uiOffset, PBYTE pBuffer, UINT uiBufferSize)
{
	if( (m_uiHead + uiOffset) <= GetBufferSize() )
		::CopyMemory(pBuffer, GetHead() + uiOffset, uiBufferSize);
}


///////////////////////////////// End of File /////////////////////////////////
