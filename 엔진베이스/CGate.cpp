/******************************************************************************
	CGate.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CGate.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CGate::~CGate()
{
	if(NULL != m_hGate)
	{
		VERIFY_CHECKLASTERROR(FALSE != (::CloseHandle(m_hGate)));
		m_hGate = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


DWORD CGate::WaitToEnterGate(DWORD dwMilliseconds/* = INFINITE*/, BOOL bAlertable/* = FALSE*/)
{
	DWORD dwRetVal;
	VERIFY_CHECKLASTERROR(WAIT_FAILED != (dwRetVal = ::WaitForSingleObjectEx(m_hGate, dwMilliseconds, bAlertable)));
	return dwRetVal;
}


///////////////////////////////// End of File /////////////////////////////////
