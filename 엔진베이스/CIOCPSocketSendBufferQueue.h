/******************************************************************************
	CIOCPSocketSendBufferQueue.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude <Winsock2.h>
qinclude "CSRMAQueue.h"


///////////////////////////////////////////////////////////////////////////////


class CPacketCoder;
class CIOCPSocketSendBuffer;
class CIOCPSocketSendBufferAllocator;


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketSendBufferQueue  
{
public:
	CIOCPSocketSendBufferQueue(CPacketCoder* pPacketCoder);
	virtual ~CIOCPSocketSendBufferQueue() { }

	static void CreateIOCPSocketSendBufferAllocator(UINT* puiBufferSize, UINT* puiPreAllocSize, UINT uiAllocatorCount);
	static CIOCPSocketSendBufferAllocator* GetIOCPSocketSendBufferAllocator();
	static void DeleteIOCPSocketSendBufferAllocator();

	void Add(PBYTE pData, UINT uiDataSize);
	void Add(CIOCPSocketSendBuffer* pBeginBuffer, CIOCPSocketSendBuffer* pEndBuffer);

	DWORD GetWSABuf(LPWSABUF pWSABuf, DWORD dwWSABufCount, UINT uiMaxDataSize);

	void Removed(UINT uiDataSize);
	CIOCPSocketSendBuffer* RemoveHead();
	void Empty();

	void SetDirty();
	void UnSetDirty();
	BOOL IsDirty();


protected:
	CSRMAQueue m_Queue;
	BOOL m_bDirty;
	UINT m_uiSequenceNumber;
	CPacketCoder* m_pPacketCoder;
	static CIOCPSocketSendBufferAllocator* m_pIOCPSocketSendBufferAllocator;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketSendBufferQueue.inl"


///////////////////////////////// End of File /////////////////////////////////
