/******************************************************************************
	CHash2.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CHash2.h"


///////////////////////////////////////////////////////////////////////////////


CHash2::~CHash2()
{
	if(NULL != m_pHashObjectList)
	{
		delete []m_pHashObjectList;
		m_pHashObjectList = NULL;
	}

	m_HashSize = 0;
}


///////////////////////////////////////////////////////////////////////////////


void CHash2::Initialize(HASH_SIZE HashSize)
{
	m_HashSize = HashSize;
	VERIFY(NULL != (m_pHashObjectList = new CDoublyLinkedList2[HashSize]));
}


///////////////////////////////////////////////////////////////////////////////


HASH_ENTRY_2* CHash2::GetFirst(HASH_INDEX* pHashIndex)
{
	HASH_ENTRY_2* pFirstEntry = NULL;

	for(HASH_INDEX HashIndex = 0; HashIndex < GetHashSize(); HashIndex++)
	{
		if(NULL != (pFirstEntry = GetPartFirst(HashIndex)))
		{
			*pHashIndex = HashIndex;
			return pFirstEntry;
		}
	}

	return NULL;
}


HASH_ENTRY_2* CHash2::GetNext(HASH_INDEX* pHashIndex, HASH_ENTRY_2* pHashEntry)
{
	HASH_ENTRY_2* pNextEntry = GetPartNext(*pHashIndex, pHashEntry);

	if(NULL != pNextEntry)
	{
		return pNextEntry;
	}

	(*pHashIndex)++;

	for(; (*pHashIndex) < GetHashSize(); (*pHashIndex)++)
	{
		if(NULL != (pNextEntry = GetPartFirst(*pHashIndex)))
		{
			return pNextEntry;
		}
	}

	return NULL;
}


///////////////////////////////////////////////////////////////////////////////


bool CHash2::IsEmpty()
{
	for(HASH_INDEX HashIndex = 0; HashIndex < GetHashSize(); HashIndex++)
	{
		if(!IsPartEmpty(HashIndex))
		{
			return false;
		}
	}

	return true;
}


///////////////////////////////// End of File /////////////////////////////////
