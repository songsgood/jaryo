/******************************************************************************
	CNetAuth.cpp
******************************************************************************/


qinclude "stdafx.h"
//qinclude <Iphlpapi.h>
qinclude "CNetAuth.h"
//qinclude "CClientSocket.h"
//qinclude "CPacketComposer.h"
//qinclude "CQueueBuffer.h"
//qinclude "CPacketCoder.h"
//qinclude "CReceivePacketBuffer.h"
//qinclude "CCryptCodeLoader.h"
//qinclude "ExeFileAnalyzer.h"
qinclude "Winsock2.h"
qinclude "JCAuthClient.h"
qinclude "JCAuthClient_Error.h"

///////////////////////////////////////////////////////////////////////////////

/*
#define AUTH_SERVER_PORT		13912


///////////////////////////////////////////////////////////////////////////////

bool CNetAuth::Connect(CClientSocket* pClientSocket, TCHAR* pAddressKeyName)
{
	DECLARE_INIT_TCHAR_ARRAY(InitFilePath, _MAX_PATH + 1);

	VERIFY(NULL != ::MakeFullPath(InitFilePath, _countof(InitFilePath), TEXT("Config\\NetAuth.ini")));

	DWORD dwError = 0;
	TCHAR AuthServerAddress[512] = TEXT("");
	VERIFY((dwError = ::GetPrivateProfileString(TEXT("AuthServer"), pAddressKeyName, TEXT(""), AuthServerAddress, sizeof(AuthServerAddress), InitFilePath)) > 0);
	if(dwError <= 0)
	{
		return false;
	}

	return (FALSE != pClientSocket->CreateConnect(AuthServerAddress, AUTH_SERVER_PORT, AF_INET, SOCK_STREAM, IPPROTO_IP, NULL, 0, 0));
}

bool CNetAuth::Auth(TCHAR* pTeamName, TCHAR* pProjectName)
{	// return true;
	// 연결
	CClientSocket ClientSocket;
	TCHAR AddressKeyName[3][12] = {"AuthServer1", "AuthServer2", "AuthServer3"};
	for(int i = 0; i < 3; i++)
	{
		if(Connect(&ClientSocket, AddressKeyName[i]))
		{
			break;
		}
	}

	// 패킷 구성
	typedef FixedString<10> TEAM_NAME, PROJECT_NAME;
	TEAM_NAME TeamName;
	PROJECT_NAME ProjectName;
	TeamName.SetString(pTeamName);
	ProjectName.SetString(pProjectName);

	TCHAR FilePath[MAX_PATH + 1] = TEXT("");
	if(::GetModuleFileName(NULL, FilePath, MAX_PATH + 1) <= 0)
	{
		return false;
	}

	CExeFileAnalyzer FileAnalyzer;
	if(!FileAnalyzer.Open(FilePath))
	{
		return false;
	}

	CPacketComposer PacketComposer(1000);
	PacketComposer.AddValue((int) 1);
	PacketComposer.AddByPointer((PBYTE) TeamName.GetString(), TEAM_NAME::GetBufferSize());
	PacketComposer.AddByPointer((PBYTE) ProjectName.GetString(), PROJECT_NAME::GetBufferSize());
	// 2010.01.11
	// Modify for Migration 2005
	// ktKim
	PacketComposer.AddValue((DWORD) (FileAnalyzer.GetImageFileHeader()->TimeDateStamp));
	// End
	AddIPAddress(&PacketComposer);
	PacketComposer.Finalize();

	// 패킷 인크립션
	CQueueBuffer QueueBuffer(PacketComposer.GetPacketBuffer(), PacketComposer.GetPacketBufferSize());
	QueueBuffer.DataAdded(PacketComposer.GetPacketSize());
	CPacketCoder PacketCoder;
	PacketCoder.EncodePacket(&QueueBuffer);

	// 송신
	WSABUF WSABuf;
	WSABuf.buf = (char*) QueueBuffer.GetHead();
	WSABuf.len = QueueBuffer.GetDataSize();
	DWORD dwBytesTemp = 0;
	if(!ClientSocket.WSASend(&WSABuf, 1, &dwBytesTemp, 0, NULL, NULL))
	{
		return false;
	}

	QueueBuffer.Empty();

	// 수신
	BYTE TempBuf[1024];
	WSABuf.buf = (char*) TempBuf;
	WSABuf.len = sizeof(TempBuf);
	CReceivePacketBuffer ReceivePacketBuffer;
	while(true)
	{
		DWORD dwFlags = 0;
		if(!ClientSocket.WSARecv(&WSABuf, 1, &dwBytesTemp, &dwFlags, NULL, NULL))
		{
			return false;
		}

		if(dwBytesTemp > 0)
		{
			QueueBuffer.Add(TempBuf, dwBytesTemp);

			int iRecvCode = PacketCoder.DecodePacket(&ReceivePacketBuffer, &QueueBuffer);
			// Error
			if(iRecvCode < 0)
			{
				return false;
			}

			// Success
			if(iRecvCode > 0)
			{
				if(1001 == ReceivePacketBuffer.GetCommand())
				{
					break;
				}
				else if(1003 == ReceivePacketBuffer.GetCommand())
				{
					return false;
				}
			}
		}
	}

	// CCryptCodeLoader::Load(ReceivePacketBuffer.GetRemainedData(), ReceivePacketBuffer.GetRemainedDataSize(), 2);
	Load(CCryptCodeLoader::Load, ReceivePacketBuffer.GetRemainedData(), ReceivePacketBuffer.GetRemainedDataSize());

	return true;
}
*/

// 2010.01.14
// New Auth Flow
// ktKim
bool CNetAuth::GetInfoAuth(TCHAR* szAddr, size_t sizeOfAddr, int& iPort, TCHAR* szPWD, size_t sizeOfPWD, int& iGameID, TCHAR* szClientDN, size_t sizeOfClientDN)
{
	DECLARE_INIT_TCHAR_ARRAY(InitFilePath, _MAX_PATH + 1);

	VERIFY(NULL != ::MakeFullPath(InitFilePath, _countof(InitFilePath), TEXT("Config\\NetAuth.ini")));

	if(::GetPrivateProfileString(TEXT("AuthServer"), TEXT("IP"), TEXT(""), szAddr, sizeOfAddr, InitFilePath) <= 0) return false;
	if((iPort = ::GetPrivateProfileInt(TEXT("AuthServer"), TEXT("Port"), 0, InitFilePath)) <= 0) return false;
	if(::GetPrivateProfileString(TEXT("AuthServer"), TEXT("Pwd"), TEXT(""), szPWD, sizeOfPWD, InitFilePath) <= 0) return false;
	if((iGameID = ::GetPrivateProfileInt(TEXT("AuthServer"), TEXT("GameID"), 0, InitFilePath)) <= 0) return false;
	if(::GetPrivateProfileString(TEXT("AuthServer"), TEXT("ClientDN"), TEXT(""), szClientDN, sizeOfClientDN, InitFilePath) <= 0) return false;
	
	return true;
}

bool CNetAuth::Auth(AUTH_TYPE eAuthType)
{	
	if (AUTH_TYPE_NONE == eAuthType)
	{
		return true;
	}

	AuthContext *pContext = NULL;
	TCHAR szServerAddr[128] = {0,};
	TCHAR szLocalAddr[128] = {0,};
	TCHAR szPWD[32] = {0,};
	TCHAR szClientDN[128] = {0,};
	INT iPort = 0;
	INT	iGameID = 0;
	INT ret = JCAUTH_SUCCESS;
	PHOSTENT pHostEnt;

	if(false == GetInfoAuth(szServerAddr, _countof(szServerAddr), iPort, szPWD, _countof(szPWD), iGameID, szClientDN, _countof(szClientDN))) return false;
	if(0 != gethostname(szLocalAddr, sizeof(szLocalAddr))) return false;
	else if(NULL == (pHostEnt = gethostbyname(szLocalAddr))) return false;
	memcpy(szLocalAddr, inet_ntoa(*(struct in_addr *)pHostEnt->h_addr), MAX_IPADDRESS_LENGTH);

	if(JCAUTH_SUCCESS != (ret = JCAuthClient_InitContext(szClientDN, &pContext))) return false;
	if(JCAUTH_SUCCESS != (ret = JCAuthClient_SetAddress(pContext, szLocalAddr))) goto CLEAN;
	if(JCAUTH_SUCCESS != (ret = JCAuthClient_ConnectToServer(pContext, szServerAddr, iPort))) goto CLEAN;
	if(JCAUTH_SUCCESS != (ret = JCAuthClient_CertifyClient(pContext, iGameID, szPWD, strlen(szPWD)))) goto CLEAN;
	if(AUTH_TYPE_RELEASE == eAuthType)
	{
		if(JCAUTH_SUCCESS != (ret = JCAuthClient_LoadCodeBlock(pContext))) goto CLEAN;
	}

CLEAN :

	if (pContext) 
	{
		if(pContext->State & CLIENTSTATE_CONNECTED) 
		{
			JCAuthClient_CloseConnection(pContext);
		}

		if(AUTH_TYPE_RELEASE == eAuthType)
		{
			JCAuthClient_ReleaseContext(pContext);
		}
	}

	if(JCAUTH_SUCCESS != ret) 
	{
		g_LogManager.WriteLogToFile("Auth Error Code[10752790]", ret);
rn false;
	}
	else return true;
}
// End
/*
qpragma code_seg(".jsenw4")


void CNetAuth::Load(FP_LOAD pLoad, PBYTE pCodeData, DWORD dwCodeDataSize)
{
	(*pLoad)(pCodeData, dwCodeDataSize, 2);
}


qpragma code_seg()


bool CNetAuth::AddIPAddress(CPacketComposer* pPacketComposer)
{
	ULONG BufLen = sizeof(IP_ADAPTER_INFO);
	PIP_ADAPTER_INFO pAdapterInfo = NULL;

	while(true)
	{
		VERIFY(NULL != (pAdapterInfo = (PIP_ADAPTER_INFO) new BYTE[BufLen]));

		DWORD dwRetVal = ::GetAdaptersInfo(pAdapterInfo, &BufLen);

		if(ERROR_SUCCESS == dwRetVal)
		{
			break;
		}
		else if(ERROR_BUFFER_OVERFLOW == dwRetVal)
		{
			delete []pAdapterInfo;
		}
		else
		{
			g_LogManager.WriteLogToFile(TEXT("GetAdaptersInfo Failed (10752790)"), dwRetVal);
urn false;
		}
	}

	pPacketComposer->AddByPointer((PBYTE) pAdapterInfo->IpAddressList.IpAddress.String, 16);

	return true;
}
*/

///////////////////////////////// End of File /////////////////////////////////
