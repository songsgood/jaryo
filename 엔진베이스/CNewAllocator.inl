/******************************************************************************
	CNewAllocator.inl
******************************************************************************/


template <class ObjectType>
inline ObjectType* CNewAllocator<ObjectType>::New()
{
	return new ObjectType;
}


template <class ObjectType>
inline void CNewAllocator<ObjectType>::Delete(ObjectType* pObject)
{
	delete pObject;
}


///////////////////////////////// End of File /////////////////////////////////
