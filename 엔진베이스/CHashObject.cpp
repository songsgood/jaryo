/******************************************************************************
	CHashObject.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CHashObject.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CHashObject::CHashObject() :
	m_uiHashIndex(INVALID_HASHINDEX),
	m_pOwner(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


void CHashObject::InitializeInstance()
{
	CDoublyListEntry::InitializeInstance();

	m_uiHashIndex = INVALID_HASHINDEX;
}


///////////////////////////////// End of File /////////////////////////////////
