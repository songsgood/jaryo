//==============================================================================
//	CClientPoolIOCPSocketServer.h
//==============================================================================


qpragma once


////////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketServer.h"
qinclude "CISLAllocator.h"
qinclude "CNewAllocator.h"


////////////////////////////////////////////////////////////////////////////////


template <class IOCPSocketClient>
class CClientPoolIOCPSocketServer : public CIOCPSocketServer  
{
public:
	CClientPoolIOCPSocketServer(CIOCP* pIOCP, DWORD dwServerTimerInterval = INFINITE);
	virtual ~CClientPoolIOCPSocketServer() { }

	virtual void FreeClient(CSocketClient* pSocketClient);


protected:
	virtual CSocketClient* IAllocateClient();


private:
	CISLAllocator< IOCPSocketClient, CNewAllocator<IOCPSocketClient> > m_IOCPSocketClientAllocator;
};


////////////////////////////////////////////////////////////////////////////////


qinclude "CClientPoolIOCPSocketServer.inl"


////////////////////////////////// End of File /////////////////////////////////
