//==============================================================================
//	CIOCPThreadManager2.h
//==============================================================================


qpragma once


////////////////////////////////////////////////////////////////////////////////


class CIOCP;


////////////////////////////////////////////////////////////////////////////////


template <class IOCPThread>
class CIOCPThreadManager2  
{
public:
	CIOCPThreadManager2(CIOCP* pIOCP);
	virtual ~CIOCPThreadManager2();

	UINT AddIOCPThread(UINT uiAdditionalThreadCount = 1);

	void DestroyAllThread();


private:
	CIOCP* m_pIOCP;
	IOCPThread* m_pFirstIOCPThread;
	UINT m_uiIOCPThreadCount;
};


////////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPThreadManager2.inl"


////////////////////////////////// End of File /////////////////////////////////
