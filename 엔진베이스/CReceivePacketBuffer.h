/******************************************************************************
	CReceivePacketBuffer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CReceivePacketBufferBase.h"


///////////////////////////////////////////////////////////////////////////////


class CReceivePacketBuffer : public CReceivePacketBufferBase  
{
public:
	CReceivePacketBuffer();
	virtual ~CReceivePacketBuffer() { }


protected:
	BYTE m_pPacketBuffer[8192];
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CReceivePacketBuffer.inl"


///////////////////////////////// End of File /////////////////////////////////
