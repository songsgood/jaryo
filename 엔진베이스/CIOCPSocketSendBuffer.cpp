/******************************************************************************
	CIOCPSocketSendBuffer.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPSocketSendBuffer.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CIOCPSocketSendBuffer::CIOCPSocketSendBuffer(BYTE ucAllocatorIndex, UINT uiBufferSize) :
	m_ucAllocatorIndex(ucAllocatorIndex),
	m_pSendBuffer(NULL),
	m_bEncrypted(false),
	m_bFirstPart(true)
{
	VERIFY(NULL != (m_pSendBuffer = new BYTE[uiBufferSize]));
	SetBuffer(m_pSendBuffer, uiBufferSize);
}


CIOCPSocketSendBuffer::~CIOCPSocketSendBuffer()
{
	if(NULL != m_pSendBuffer)
	{
		delete []m_pSendBuffer;
		m_pSendBuffer = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


CIOCPSocketSendBuffer* CIOCPSocketSendBuffer::AllocateNewObject()
{
	CIOCPSocketSendBuffer* pIOCPSocketSendBuffer = NULL;
	VERIFY(NULL != (pIOCPSocketSendBuffer = new CIOCPSocketSendBuffer(m_ucAllocatorIndex, GetBufferSize())));
	return pIOCPSocketSendBuffer;
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketSendBuffer::InitializeInstance()
{
	Empty();
	m_bEncrypted = false;
	m_bFirstPart = true;
}


///////////////////////////////// End of File /////////////////////////////////
