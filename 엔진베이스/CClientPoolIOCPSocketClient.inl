//==============================================================================
//	CClientPoolIOCPSocketClient.inl
//==============================================================================


inline void CClientPoolIOCPSocketClient::InitializeInstance()
{
	CIOCPSocketClient::InitializeInstance();
}


inline void CClientPoolIOCPSocketClient::CleanupInstance()
{
	CIOCPSocketClient::CleanupInstance();
}


////////////////////////////////// End of File /////////////////////////////////
