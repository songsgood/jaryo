/******************************************************************************
	CEventLog.h
******************************************************************************/


///////////////////////////////////////////////////////////////////////////////


qinclude "EventLogMsgs.h"
qinclude <windows.h>


///////////////////////////////////////////////////////////////////////////////


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CEventLog 
{
public:
	CEventLog();
	~CEventLog();
	void SetAppName(LPCTSTR lpAppName);
	BOOL Install(DWORD dwTypesSupported = EVENTLOG_INFORMATION_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_ERROR_TYPE, 
		PCTSTR pszEventMsgFilePaths = NULL, PCTSTR pszParameterMsgFilePaths = NULL, 
		DWORD dwCategoryCount = 0, PCTSTR pszCategoryMsgFilePaths = NULL);
	BOOL Uninstall();

	// Records an event into the event log
	enum REPORTEVENTUSER { REUSER_NOTAPPLICABLE, REUSER_SERVICE, REUSER_CLIENT };
	BOOL ReportEvent(LPCTSTR lpString, WORD wType = EVENTLOG_ERROR_TYPE);
	BOOL ReportEvent(WORD wType, WORD wCategory = 0, DWORD dwEventID = EL_MSG_ERROR, 
		REPORTEVENTUSER reu = REUSER_NOTAPPLICABLE, 
		WORD wNumStrings = 0, PCTSTR *pStrings = NULL, DWORD dwDataSize = 0, PVOID pvRawData = NULL);

private:
	TCHAR m_szAppName[128];
	HANDLE m_hEventLog;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CEventLog.inl"


///////////////////////////////// End of File /////////////////////////////////