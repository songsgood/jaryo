/******************************************************************************
	CSocketClient.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CSocketClient.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CSocketClient::CSocketClient() :
	m_pClientSocket(NULL),
	m_pServer(NULL),
	m_usPeerPort(0)
{
	INIT_TCHAR_ARRAY(m_szPeerIPAddress);
}


CSocketClient::~CSocketClient()
{
	if(NULL != m_pClientSocket)
	{
		delete m_pClientSocket;
		m_pClientSocket = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


void CSocketClient::SetServer(CSocketServer* pServer)
{
	m_pServer = pServer;
	m_pClientSocket->SetServerSocket(pServer->GetServerSocket());
}


///////////////////////////////////////////////////////////////////////////////


BOOL CSocketClient::Connect(const char* pAddress, unsigned short usPort)
{
	LSTRCPYNTOARRAY(m_szPeerIPAddress, pAddress);
	m_usPeerPort = usPort;
	return m_pClientSocket->CreateConnect(pAddress, usPort);
}


///////////////////////////////////////////////////////////////////////////////


BOOL CSocketClient::Connect()
{
	if(NULL == m_pClientSocket)
	{
		return FALSE;
	}
	if(FALSE == m_pClientSocket->IsValidSocket())
	{
		return m_pClientSocket->CreateConnect(m_szPeerIPAddress, m_usPeerPort);
	}
	else
	{
		return m_pClientSocket->Connect(m_szPeerIPAddress, m_usPeerPort);
	}
}


///////////////////////////////// End of File /////////////////////////////////
