/******************************************************************************
	CWSASocket.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CWSASocket.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CWSASocket::CWSASocket() :
	m_hSocket(INVALID_SOCKET)
{
}


CWSASocket::~CWSASocket()
{
	if(INVALID_SOCKET != m_hSocket)
	{
		ShutDown();
		CloseSocket();
	}
}


///////////////////////////////////////////////////////////////////////////////


BOOL CWSASocket::CloseSocket()
{
	if(0 != ::closesocket(m_hSocket))
	{
		return FALSE;
	}
	m_hSocket = INVALID_SOCKET;
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CWSASocket::EventSelect(WSAEVENT hEventObject, long lNetworkEvents)
{
	BOOL bRetVal = FALSE;
	VERIFY_CHECKWSALASTERROR(FALSE != (bRetVal = (0 == ::WSAEventSelect(m_hSocket, hEventObject, lNetworkEvents))));
	return bRetVal;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CWSASocket::AcceptEx(SOCKET sListenSocket, SOCKET sAcceptSocket, PVOID lpOutputBuffer,
	DWORD dwReceiveDataLength, DWORD dwLocalAddressLength, DWORD dwRemoteAddressLength, LPDWORD lpdwBytesReceived,
	LPOVERLAPPED lpOverlapped)
{
	if(FALSE == ::AcceptEx(sListenSocket, sAcceptSocket, lpOutputBuffer, dwReceiveDataLength, dwLocalAddressLength,
		dwRemoteAddressLength, lpdwBytesReceived, lpOverlapped))
	{
		if(WSA_IO_PENDING != ::WSAGetLastError())
		{
			VERIFY(!TEXT("AcceptEx Failed"));
			return FALSE;
		}
	}
	return TRUE;
}


BOOL CWSASocket::WSASend(LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesSent, DWORD dwFlags,
	LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine)
{
	if(0 != ::WSASend(m_hSocket, lpBuffers, dwBufferCount, lpNumberOfBytesSent, dwFlags, lpOverlapped,
		lpCompletionRoutine))
	{
		if(WSA_IO_PENDING != ::WSAGetLastError())
		{
			return FALSE;
		}
	}
	return TRUE;
}


BOOL CWSASocket::WSARecv(LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesRecvd, LPDWORD lpFlags,
	LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine)
{
	if(0 != ::WSARecv(m_hSocket, lpBuffers, dwBufferCount, lpNumberOfBytesRecvd, lpFlags, lpOverlapped,
		lpCompletionRoutine))
	{
		if(WSA_IO_PENDING != ::WSAGetLastError())
		{
			return FALSE;
		}

		/*switch(::WSAGetLastError())
		{
		case WSANOTINITIALISED:
			return FALSE;
		case WSAENETDOWN:
			return FALSE;
		case WSAENOTCONN:
			return FALSE;
		case WSAEINTR:
			return FALSE;
		case WSAEINPROGRESS:
			return FALSE;
		case WSAENETRESET:
			return FALSE;
		case WSAENOTSOCK:
			return FALSE;
		case WSAEFAULT:
			return FALSE;
		case WSAEOPNOTSUPP:
			return FALSE;
		case WSAESHUTDOWN:
			return FALSE;
		case WSAEWOULDBLOCK:
			return FALSE;
		case WSAEMSGSIZE:
			return FALSE;
		case WSAEINVAL:
			return FALSE;
		case WSAECONNABORTED:
			return FALSE;
		case WSAECONNRESET:
			return FALSE;
		case WSAEDISCON:
			return FALSE;
		case WSA_IO_PENDING:
			break;
		case WSA_OPERATION_ABORTED:
			return FALSE;
		}*/
	}
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CWSASocket::Create(int af/* = AF_INET*/, int type/* = SOCK_STREAM*/, int protocol/* = IPPROTO_IP*/,
	LPWSAPROTOCOL_INFO lpProtocolInfo/* = NULL*/, GROUP g/* = 0*/, DWORD dwFlags/* = WSA_FLAG_OVERLAPPED*/)
{
	VERIFY_CHECKWSALASTERROR(INVALID_SOCKET != (m_hSocket = ::WSASocket(af, type, protocol, lpProtocolInfo, g, dwFlags)));
	return (INVALID_SOCKET != m_hSocket);
}


BOOL CWSASocket::Bind(unsigned short usSocketPort)
{
	BOOL bBindRetVal = FALSE;

	SOCKADDR_IN addr;
	::ZeroMemory(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = ::htonl(INADDR_ANY);
	addr.sin_port = ::htons(usSocketPort);

	VERIFY_CHECKWSALASTERROR(FALSE != (bBindRetVal = (0 == ::bind(m_hSocket, (sockaddr*) &addr, sizeof(addr)))));

	return bBindRetVal;
}


BOOL CWSASocket::Listen(int iBackLog/* = SOMAXCONN*/)
{
	BOOL bListenRetVal = FALSE;
	VERIFY_CHECKWSALASTERROR(FALSE != (bListenRetVal = (0 == ::listen(m_hSocket, iBackLog))));
	return bListenRetVal;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CWSASocket::Connect(const char* pAddress, unsigned short usPort)
{
	SOCKADDR_IN ServerAddress;
	ServerAddress.sin_family = AF_INET;
	ServerAddress.sin_port = ::htons(usPort);
	ServerAddress.sin_addr.s_addr = InetAddr(pAddress);

	if(SOCKET_ERROR == ::connect(m_hSocket, (SOCKADDR*) &ServerAddress, sizeof(ServerAddress)))
	{
		if(WSAEWOULDBLOCK != ::WSAGetLastError())
		{
			// #####
			// VERIFY(!TEXT("Connect Failed"));
			return FALSE;
		}
	}

	return TRUE;
}


unsigned long CWSASocket::InetAddr(const char* pAddress)
{
	unsigned long ulInetAddr = ::inet_addr(pAddress);

	if(INADDR_NONE != ulInetAddr)
	{
		return ulInetAddr;
	}

	struct hostent* host = ::gethostbyname(pAddress);
	if(NULL != host)
	{
		return *((unsigned long*) host->h_addr);
	}

	return INADDR_NONE;
}


///////////////////////////////// End of File /////////////////////////////////
