/******************************************************************************
	CServiceSync.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CServiceSync  
{
public:
	CServiceSync();
	virtual ~CServiceSync();

	HANDLE GetEvent();

	int Sleep(DWORD dwInterval);
	void WakeUp();


protected:
	HANDLE m_hEvent;
	bool m_bWakeUp;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CServiceSync.inl"


///////////////////////////////// End of File /////////////////////////////////
