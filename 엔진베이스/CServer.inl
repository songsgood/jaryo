/******************************************************************************
	CServer.inl
******************************************************************************/


inline CServer::CServer(DWORD dwServerTimerInterval/* = INFINITE*/) :
	m_dwServerTimerInterval(dwServerTimerInterval)
{
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CServer::OnInitialize()
{
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


inline DWORD CServer::GetServerTimerInterval()
{
	return m_dwServerTimerInterval;
}


///////////////////////////////// End of File /////////////////////////////////
