/******************************************************************************
	CThread.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CThread.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


BOOL CThread::CreateThread(LPSECURITY_ATTRIBUTES lpThreadAttributes/* = NULL*/, DWORD dwStackSize/* = 0*/, DWORD dwCreationFlags/* = 0*/,
	LPDWORD lpThreadId/* = NULL*/)
{
	VERIFY(((HANDLE) 0) != (m_hThread = (HANDLE) _beginthreadex((void*) lpThreadAttributes, (unsigned) dwStackSize, _Run, (void*) this,
		(unsigned) dwCreationFlags, (unsigned*) lpThreadId)));
	return (((HANDLE) 0) != m_hThread);
}


void CThread::EndThread()
{
	if(((HANDLE) 0) == m_hThread)
	{
		return;
	}

	BOOL bRetVal = FALSE;

	VERIFY_CHECKLASTERROR(FALSE != (bRetVal = (WAIT_FAILED != ::WaitForSingleObjectEx(m_hThread, INFINITE, FALSE))));
	if(FALSE == bRetVal)
	{
		return;
	}

	VERIFY_CHECKLASTERROR(FALSE != ::CloseHandle(m_hThread));
	m_hThread = (HANDLE) 0;
}


///////////////////////////////////////////////////////////////////////////////


unsigned __stdcall CThread::_Run(void* pParameter)
{
	srand((unsigned) time(NULL));

	if(FALSE == ((CThread*) pParameter)->OnStartup())
	{
		return 1;
	}

	return ((CThread*) pParameter)->Run();
}


///////////////////////////////// End of File /////////////////////////////////
