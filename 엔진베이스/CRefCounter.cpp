/******************************************************************************
	CRefCounter.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CRefCounter.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


BOOL CRefCounter::AddNextRef()
{
	LONG lOrigRefCounter = 0;

	do {
		lOrigRefCounter = m_lRefCounter;

		if(0 == lOrigRefCounter)
		{
			return FALSE;
		}
	} while(::InterlockedCompareExchange(&m_lRefCounter, lOrigRefCounter + 1, lOrigRefCounter) != lOrigRefCounter);

	return TRUE;
}


//////////////////////////////// End of File //////////////////////////////////
