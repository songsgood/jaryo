/******************************************************************************
	CInterlockedBool.inl
******************************************************************************/


inline CInterlockedBool::CInterlockedBool(BOOL bInitValue/* = FALSE*/)
{
	m_lBool = (FALSE == bInitValue) ? 0 : 1;
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CInterlockedBool::Set()
{
	return (0 == ::InterlockedCompareExchange(&m_lBool, 1, 0));
}


inline BOOL CInterlockedBool::UnSet()
{
	return (1 == ::InterlockedCompareExchange(&m_lBool, 0, 1));
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CInterlockedBool::Get()
{
	return (0 != m_lBool);
}


///////////////////////////////////////////////////////////////////////////////


inline CInterlockedBool::operator BOOL()
{
	return (0 != m_lBool);
}


//////////////////////////////// End of File //////////////////////////////////
