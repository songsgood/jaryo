/******************************************************************************
	CServerSocket.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CWSASocket.h"


///////////////////////////////////////////////////////////////////////////////


#define INVALID_SOCKETPORT_NUMBER	0


///////////////////////////////////////////////////////////////////////////////


class CServerSocket : public CWSASocket  
{
protected:
	typedef BOOL (CWSASocket::*FP_CREATE)(int, int, int, LPWSAPROTOCOL_INFO, GROUP, DWORD);
	typedef BOOL (CWSASocket::*FP_BIND)(unsigned short);
	typedef BOOL (CWSASocket::*FP_LISTEN)(int);


public:
	CServerSocket(unsigned short usPort = 2002);
	virtual ~CServerSocket() { }

	void SetPort(unsigned short usPort);
	unsigned short GetPort();

	BOOL CreateBindListen(int af = AF_INET, int type = SOCK_STREAM,
		int protocol = IPPROTO_IP, LPWSAPROTOCOL_INFO lpProtocolInfo = NULL, GROUP g = 0,
		DWORD dwFlags = WSA_FLAG_OVERLAPPED, int iBackLog = SOMAXCONN);


protected:
	BOOL CreateBindListen(FP_CREATE pCreate, FP_BIND pBind, FP_LISTEN pListen, unsigned short usPort, int af = AF_INET, int type = SOCK_STREAM,
		int protocol = IPPROTO_IP, LPWSAPROTOCOL_INFO lpProtocolInfo = NULL, GROUP g = 0,
		DWORD dwFlags = WSA_FLAG_OVERLAPPED, int iBackLog = SOMAXCONN);


private:
	unsigned short m_usPort;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CServerSocket.inl"


///////////////////////////////// End of File /////////////////////////////////
