/******************************************************************************
	CIOCP.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCP.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CIOCP::~CIOCP()
{
	if(NULL != m_hIOCP)
	{
		VERIFY_CHECKLASTERROR(FALSE != ::CloseHandle(m_hIOCP));
		m_hIOCP = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCP::Create(DWORD dwNumberOfConcurrentThreads/* = 0*/)
{
	VERIFY_CHECKLASTERROR(NULL != (m_hIOCP = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, dwNumberOfConcurrentThreads)));
	return (NULL != m_hIOCP);
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCP::AssociateDevice(HANDLE hDevice, ULONG_PTR pulCompletionKey)
{
	BOOL bRetVal = FALSE;
	VERIFY_CHECKLASTERROR(FALSE != (bRetVal = (::CreateIoCompletionPort(hDevice, m_hIOCP, pulCompletionKey, 0) == m_hIOCP)));
	return bRetVal;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCP::PostStatus(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred/* = 0*/, LPOVERLAPPED pOverlapped/* = NULL*/)
{
	BOOL bRetVal = FALSE;
	VERIFY_CHECKLASTERROR(FALSE != (bRetVal = ::PostQueuedCompletionStatus(m_hIOCP, dwNumberOfBytesTransferred, pulCompletionKey, pOverlapped)));
	return bRetVal;
}


BOOL CIOCP::GetStatus(PULONG_PTR ppulCompletionKey, LPDWORD pdwNumberOfBytes, LPOVERLAPPED* ppOverlapped, DWORD dwMilliseconds/* = INFINITE*/)
{
	//BOOL bRetVal = FALSE;
	//VERIFY_CHECKLASTERROR(FALSE != (bRetVal = ::GetQueuedCompletionStatus(m_hIOCP, pdwNumberOfBytes, ppulCompletionKey, ppOverlapped,
	//	dwMilliseconds)));
	//return bRetVal;
	return ::GetQueuedCompletionStatus(m_hIOCP, pdwNumberOfBytes, ppulCompletionKey, ppOverlapped, dwMilliseconds);
}


///////////////////////////////// End of File /////////////////////////////////
