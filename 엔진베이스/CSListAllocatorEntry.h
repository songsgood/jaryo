/******************************************************************************
	CSListAllocatorEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CSinglyListEntry.h"


///////////////////////////////////////////////////////////////////////////////


class CSListAllocatorEntry  
{
public:
	CSListAllocatorEntry();
	virtual ~CSListAllocatorEntry() { }

	virtual void InitializeInstance() { }
	virtual void CleanupInstance() { }

	void SetNextAllocatorEntry(CSListAllocatorEntry* pNextAllocatorEntry);
	CSListAllocatorEntry* GetNextAllocatorEntry();


private:
	CSListAllocatorEntry* m_pNextAllocatorEntry;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSListAllocatorEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
