/******************************************************************************
	CSocketIOCPThread.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CSocketIOCPThread.h"
qinclude "CIOCPSocketClient.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


FP_PACKETPROCESSFUNCTION** CSocketIOCPThread::m_ppPacketProcessFunction = NULL;


///////////////////////////////////////////////////////////////////////////////


void CSocketIOCPThread::InitializePacketProcessFunction(UINT uiPacketProcessFunctionState)
{
	HANDLE hProcessHeap = NULL;
	VERIFY(NULL != (hProcessHeap = ::GetProcessHeap()));
	VERIFY(NULL != (m_ppPacketProcessFunction = (FP_PACKETPROCESSFUNCTION**) ::HeapAlloc(hProcessHeap, HEAP_ZERO_MEMORY,
		uiPacketProcessFunctionState * sizeof(FP_PACKETPROCESSFUNCTION*))));

	for(UINT uiIndex = 0; uiIndex < uiPacketProcessFunctionState; uiIndex++)
	{
		VERIFY(NULL != (m_ppPacketProcessFunction[uiIndex] = (FP_PACKETPROCESSFUNCTION*) ::HeapAlloc(hProcessHeap,
			HEAP_ZERO_MEMORY, PACKETPROCESSFUNCTION_COUNT * sizeof(FP_PACKETPROCESSFUNCTION))));
	}

	for(UINT uiIndex = 0; uiIndex < uiPacketProcessFunctionState; uiIndex++)
	{
		for(UINT uiIndex2 = 0; uiIndex2 < PACKETPROCESSFUNCTION_COUNT; uiIndex2++)
		{
			// 2010.01.11
			// Modify for Migration 2005
			// ktKim
			SetPacketProcessFunction(uiIndex, uiIndex2, &CSocketIOCPThread::ProcessUnknownPacket);
			// End
		}
	}
}


void CSocketIOCPThread::CleanupPacketProcessFunction(UINT uiPacketProcessFunctionState)
{
	if(NULL != m_ppPacketProcessFunction)
	{
		HANDLE hProcessHeap = NULL;
		VERIFY(NULL != (hProcessHeap = ::GetProcessHeap()));

		for(UINT uiIndex = 0; uiIndex < uiPacketProcessFunctionState; uiIndex++)
		{
			if(NULL != m_ppPacketProcessFunction[uiIndex])
			{
				::HeapFree(hProcessHeap, 0, m_ppPacketProcessFunction[uiIndex]);
				m_ppPacketProcessFunction[uiIndex] = NULL;
			}
		}

		::HeapFree(hProcessHeap, 0, m_ppPacketProcessFunction);
		m_ppPacketProcessFunction = NULL;
	}
}


void CSocketIOCPThread::SetPacketProcessFunction(UINT uiPacketProcessFunctionState, UINT uiCommand,
	FP_PACKETPROCESSFUNCTION pFunction)
{
	m_ppPacketProcessFunction[uiPacketProcessFunctionState][uiCommand] = pFunction;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CSocketIOCPThread::ProcessFailedIO(ULONG_PTR/* pulCompletionKey*/, DWORD/* dwNumberOfBytesTransferred*/,
	LPOVERLAPPED pOverlapped)
{
	CSocketIOCPOverlapped* pSocketIOCPOverlapped = (CSocketIOCPOverlapped*) pOverlapped;
	CIOCPSocketClient* pIOCPSocketClient = pSocketIOCPOverlapped->GetClient();

	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient))"));
		return TRUE;
	}

	if(FALSE == pIOCPSocketClient->CheckCIOCPSocketClient())
	{
		VERIFY(!TEXT("FALSE == pIOCPSocketClient->CheckCIOCPSocketClient()"));
		return TRUE;
	}

	if(OP_ACCEPT == pSocketIOCPOverlapped->GetOpCode())
	{
	}
	else
	{
		OnPeerDisconnected(pIOCPSocketClient);
	}

	pIOCPSocketClient->ReleaseRef();

	return TRUE;
}


//BOOL CSocketIOCPThread::ProcessCompletedIO(ULONG_PTR/* pulCompletionKey*/, DWORD dwNumberOfBytesTransferred,
/*	LPOVERLAPPED pOverlapped)
{
	CSocketIOCPOverlapped* pSocketIOCPOverlapped = (CSocketIOCPOverlapped*) pOverlapped;
	CIOCPSocketClient* pIOCPSocketClient = pSocketIOCPOverlapped->GetClient();

	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient))"));
		return TRUE;
	}

	if(FALSE == pIOCPSocketClient->CheckCIOCPSocketClient())
	{
		VERIFY(!TEXT("FALSE == pIOCPSocketClient->CheckCIOCPSocketClient()"));
		return TRUE;
	}

	if( (0 == dwNumberOfBytesTransferred) && ( (OP_SEND == pSocketIOCPOverlapped->GetOpCode()) ||
		(OP_RECV == pSocketIOCPOverlapped->GetOpCode()) ) )
	{
		OnPeerDisconnected(pIOCPSocketClient);
	}
	else
	{
		switch(pSocketIOCPOverlapped->GetOpCode())
		{
		case OP_RECV:
			OnReceived(pIOCPSocketClient, dwNumberOfBytesTransferred);
			break;

		case OP_SEND:
			OnSent(pIOCPSocketClient, dwNumberOfBytesTransferred);
			break;

		case OP_ACCEPT:
			OnAccepted(pIOCPSocketClient, dwNumberOfBytesTransferred);
			break;

		default:
			VERIFY( (OP_ACCEPT == pSocketIOCPOverlapped->GetOpCode()) || (OP_SEND == pSocketIOCPOverlapped->GetOpCode()) ||
				(OP_RECV == pSocketIOCPOverlapped->GetOpCode()) );
			break;
		}
	}

	pIOCPSocketClient->ReleaseRef();

	return TRUE;
}*/


BOOL CSocketIOCPThread::ProcessCompletedIO(ULONG_PTR/* pulCompletionKey*/, DWORD dwNumberOfBytesTransferred,
	LPOVERLAPPED pOverlapped)
{
	CSocketIOCPOverlapped* pSocketIOCPOverlapped = (CSocketIOCPOverlapped*) pOverlapped;
	CIOCPSocketClient* pIOCPSocketClient = pSocketIOCPOverlapped->GetClient();

	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient))"));
		return TRUE;
	}

	if(FALSE == pIOCPSocketClient->CheckCIOCPSocketClient())
	{
		VERIFY(!TEXT("FALSE == pIOCPSocketClient->CheckCIOCPSocketClient()"));
		return TRUE;
	}

	// 2010.01.11
	// Modify for Migration 2005
	// ktKim
	ProcessCompletedIO(&CSocketIOCPOverlapped::GetOpCode, &CSocketIOCPThread::OnPeerDisconnected, &CSocketIOCPThread::OnReceived, &CSocketIOCPThread::OnSent, &CSocketIOCPThread::OnAccepted, pSocketIOCPOverlapped, pIOCPSocketClient, dwNumberOfBytesTransferred);
	// End

	pIOCPSocketClient->ReleaseRef();

	return TRUE;
}


qpragma code_seg(".JCAuth")


void CSocketIOCPThread::ProcessCompletedIO(FP_GET_OPCODE pGetOpCode, FP_ON_PEER_DISCONNECTED pOnPeerDisconnected, FP_SOCKET_IO pOnReceived, FP_SOCKET_IO pOnSent, FP_SOCKET_IO pOnAccepted, CSocketIOCPOverlapped* pSocketIOCPOverlapped, CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred)
{
	if( (0 == dwNumberOfBytesTransferred) && ( (OP_SEND == (pSocketIOCPOverlapped->*pGetOpCode)()) ||
		(OP_RECV == (pSocketIOCPOverlapped->*pGetOpCode)()) ) )
	{
		(this->*pOnPeerDisconnected)(pIOCPSocketClient);
	}
	else
	{
		switch((pSocketIOCPOverlapped->*pGetOpCode)())
		{
		case OP_RECV:
			(this->*pOnReceived)(pIOCPSocketClient, dwNumberOfBytesTransferred);
			break;

		case OP_SEND:
			(this->*pOnSent)(pIOCPSocketClient, dwNumberOfBytesTransferred);
			break;

		case OP_ACCEPT:
			(this->*pOnAccepted)(pIOCPSocketClient, dwNumberOfBytesTransferred);
			break;
		}
	}
}


qpragma code_seg()


///////////////////////////////////////////////////////////////////////////////


void CSocketIOCPThread::ProcessPacket(CIOCPSocketClient* /*pIOCPSocketClient*/)
{
	/*(this->*(m_ppPacketProcessFunction[pIOCPSocketClient->GetProcessPacketState()][m_ReceivePacketBuffer.GetCommand()]))(
		pIOCPSocketClient);*/
}


void CSocketIOCPThread::ProcessUnknownPacket(CIOCPSocketClient* /*pIOCPSocketClient*/)
{
}


///////////////////////////////////////////////////////////////////////////////


void CSocketIOCPThread::OnReceived(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred)
{
	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)"));
		return;
	}
	if(FALSE == pIOCPSocketClient->OnReceived(dwNumberOfBytesTransferred)) { return; }
	OnReceive(pIOCPSocketClient);
	if(FALSE == pIOCPSocketClient->PostWSAReceive())
	{
		OnPeerDisconnected(pIOCPSocketClient);
	}
}


void CSocketIOCPThread::OnSent(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred)
{
	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)"));
		return;
	}
	pIOCPSocketClient->OnSent(dwNumberOfBytesTransferred);
	pIOCPSocketClient->PostWSASend();
}


void CSocketIOCPThread::OnAccepted(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred)
{
	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)"));
		return;
	}
	PostAnotherAcceptEx(pIOCPSocketClient->GetServer());
	pIOCPSocketClient->OnAccepted();
	if(dwNumberOfBytesTransferred > 0)
	{
		OnReceived(pIOCPSocketClient, dwNumberOfBytesTransferred);
	}
	else
	{
		if(FALSE == pIOCPSocketClient->PostWSAReceive())
		{
			OnPeerDisconnected(pIOCPSocketClient);
		}
	}
}


void CSocketIOCPThread::OnPeerDisconnected(CIOCPSocketClient* pIOCPSocketClient)
{
	if(FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pIOCPSocketClient, sizeof(CIOCPSocketClient)"));
		return;
	}
	pIOCPSocketClient->DisconnectClient();
}


///////////////////////////////////////////////////////////////////////////////


void CSocketIOCPThread::OnReceive(CIOCPSocketClient* pIOCPSocketClient)
{
	int iRetVal = 0;
	DWORD dwStart = 0, dwCal = 0;

	while((iRetVal = pIOCPSocketClient->GetPacket(&m_ReceivePacketBuffer)) > 0)
	{
		dwStart = GetTickCount();

		ProcessPacket(pIOCPSocketClient);

		dwCal = GetTickCount() - dwStart;
		if(dwCal > 2000 && 1003 != m_ReceivePacketBuffer.GetCommand()) // 2sec, no login packet
		{
			g_LogManager.WriteLogToFile("[Profile] ProcessPacket Thread : Time:[10752790], Command:[0]", dwCal, m_ReceivePacketBuffer.GetCommand());

		m_ReceivePacketBuffer.Clear();
	}

	/*BYTE szTemp[1024];
	CPacketComposer PacketComposer(10000);
	PacketComposer.Add(szTemp, 1024);
	pIOCPSocketClient->Send(&PacketComposer);*/

	if(iRetVal < 0)
	{
		OnPeerDisconnected(pIOCPSocketClient);
	}
}


///////////////////////////////////////////////////////////////////////////////


void CSocketIOCPThread::PostAnotherAcceptEx(CSocketServer* pServer)
{
	if(FALSE != ::IsBadReadPtr(pServer, sizeof(CSocketServer)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pServer, sizeof(CSocketServer)"));
		return;
	}
	CIOCPSocketClient* pAnotherClient = (CIOCPSocketClient*) pServer->AllocateClient();
	pAnotherClient->WaitForClientConnection();
}


///////////////////////////////// End of File /////////////////////////////////