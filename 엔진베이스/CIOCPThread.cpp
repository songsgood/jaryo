/******************************************************************************
	CIOCPThread.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPThread.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CIOCPThread::CIOCPThread(CIOCP* pIOCP) : m_pIOCP(pIOCP), m_pNext(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPThread::IsControlMessage(ULONG_PTR pulCompletionKey, DWORD/* dwNumberOfBytesTransferred*/,
	LPOVERLAPPED/* pOverlapped*/, BOOL bIOSucceeded)
{
	if( (NULL == pulCompletionKey) && (FALSE != bIOSucceeded) )
	{
		return TRUE;
	}

	return FALSE;
}


BOOL CIOCPThread::IsEndMessage(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred,
	LPOVERLAPPED/* pOverlapped*/, BOOL bIOSucceeded)
{
	if( (NULL == pulCompletionKey) && (SERVICE_STOP_PENDING == dwNumberOfBytesTransferred) && (FALSE != bIOSucceeded) )
	{
		return TRUE;
	}

	return FALSE;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPThread::ProcessIOMessage(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred,
	LPOVERLAPPED pOverlapped, BOOL bIOSucceeded)
{
	if(NULL != pOverlapped)
	{
		if(FALSE == bIOSucceeded)
		{
			return ProcessFailedIO(pulCompletionKey, dwNumberOfBytesTransferred, pOverlapped);
		}
		else
		{
			return ProcessCompletedIO(pulCompletionKey, dwNumberOfBytesTransferred, pOverlapped);
		}
	}
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


unsigned CIOCPThread::Run()
{
	ULONG_PTR pulCompletionKey;
	DWORD dwNumberOfBytesTransferred;
	LPOVERLAPPED pOverlapped;

	BOOL bEndThread = FALSE;

	while(FALSE == bEndThread)
	{
		BOOL bIOSucceeded = m_pIOCP->GetStatus(&pulCompletionKey, &dwNumberOfBytesTransferred, &pOverlapped);

		if(FALSE != IsControlMessage(pulCompletionKey, dwNumberOfBytesTransferred,
			pOverlapped, bIOSucceeded))
		{
			bEndThread = IsEndMessage(pulCompletionKey, dwNumberOfBytesTransferred,
				pOverlapped, bIOSucceeded);
			if(FALSE == bEndThread)
			{
				bEndThread = (FALSE == ProcessControlMessage(pulCompletionKey, dwNumberOfBytesTransferred, pOverlapped,
					bIOSucceeded));
			}
		}
		else
		{
			bEndThread = (FALSE == ProcessIOMessage(pulCompletionKey, dwNumberOfBytesTransferred, pOverlapped,
				bIOSucceeded));
		}
	}

	g_LogManager.WriteLogToFile("Thread Loop break!!");

	return 0;
}


///////////////////////////////// End of File /////////////////////////////////
