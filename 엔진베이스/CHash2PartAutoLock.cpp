/******************************************************************************
	CHash2PartAutoLock.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CHash2PartAutoLock.h"


///////////////////////////////////////////////////////////////////////////////


CHash2PartAutoLock::CHash2PartAutoLock(CHash2PartLock* pHashPartLock, CHash2::HASH_INDEX HashIndex,
	bool bInitialLock/* = false*/) :
	m_pHashPartLock(pHashPartLock),
	m_HashIndex(HashIndex),
	m_bLocked(false)
{
	if(bInitialLock)
	{
		Lock();
	}
}


CHash2PartAutoLock::~CHash2PartAutoLock()
{
	if(IsLocked())
	{
		Unlock();
	}
}


///////////////////////////////////////////////////////////////////////////////


void CHash2PartAutoLock::Lock()
{
	m_bLocked = true;
	m_pHashPartLock->Lock(m_HashIndex);
}


void CHash2PartAutoLock::Unlock()
{
	m_pHashPartLock->Unlock(m_HashIndex);
	m_bLocked = false;
}


///////////////////////////////// End of File /////////////////////////////////
