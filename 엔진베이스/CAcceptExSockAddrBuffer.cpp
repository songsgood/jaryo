/******************************************************************************
	CAcceptExSockAddrBuffer.h
******************************************************************************/


qinclude "stdafx.h"
qinclude "CAcceptExSockAddrBuffer.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CAcceptExSockAddrBuffer::CAcceptExSockAddrBuffer() : CBufferBase(m_AddressBuffer, sizeof(m_AddressBuffer)),
	m_pLocalSockAddr(NULL), m_pRemoteSockAddr(NULL)
{
	FillZeroMemory();
}


///////////////////////////////////////////////////////////////////////////////


void CAcceptExSockAddrBuffer::InitializeInstance()
{
	FillZeroMemory();
	m_pLocalSockAddr = NULL;
	m_pRemoteSockAddr = NULL;
}


///////////////////////////////////////////////////////////////////////////////


void CAcceptExSockAddrBuffer::SetAcceptExSockaddrs()
{
	int iLocalSockAddrLength = 0, iRemoteSockAddrLength = 0;

	::GetAcceptExSockaddrs(m_AddressBuffer, 0, sizeof(SOCKADDR_IN) + 16, sizeof(SOCKADDR_IN) + 16, (LPSOCKADDR*) &m_pLocalSockAddr, &iLocalSockAddrLength,
		(LPSOCKADDR*) &m_pRemoteSockAddr, &iRemoteSockAddrLength);
}


//////////////////////////////// End of File //////////////////////////////////
