/******************************************************************************
	CPacketComposer.h
******************************************************************************/


qpragma once

// 2010.01.11
// Modify for Migration 2005
// 불필요 할것 같은데...
// ktKim
// Temp
//qpragma warning (push, 3)
//qinclude <string>
//qpragma warning (pop)
// End


///////////////////////////////////////////////////////////////////////////////


class CPacketComposer  
{
public:
	CPacketComposer();
	CPacketComposer(unsigned short usCommand);
	CPacketComposer(PBYTE pData, UINT uiDataSize);
	// virtual ~CPacketComposer() { }

	void Initialize(unsigned short usCommand);

	unsigned short GetCommand();
	void SetCommand(unsigned short usCommand);

	PBYTE GetTail();
	void DataAdded(UINT uiDataSize);

	//
	// 이전 호환성 유지용
	//
	// Temp
	// ktKim
	//void Add(const std::string & strData);
	void Add(PBYTE pData, UINT uiDataSize);
	void Add(PBYTE pucData);
	void Add(BYTE ucData);
	void Add(unsigned short* pusData);
	void Add(unsigned short usData);
	void Add(short sData);
	void Add(int* piData);
	void Add(UINT* puiData);
	void Add(UINT uiData);
	void Add(int iData);
	void Add(ULONG* pulData);
	void Add(ULONG ulData);
	void Add(float* pfData);
	void Add(float fData);
	void Add(SYSTEMTIME* ptimeDate);
	void Add(__int64* pi64Data);
	void Add(unsigned __int64* pui64Data);
	void Add(__int64 i64Data);
	void Add(unsigned __int64 ui64Data);
	template <class DATA_TYPE> void AddData(DATA_TYPE* pData)
	{
		ASSERT(GetFreeSize() >= sizeof(DATA_TYPE));
		*((DATA_TYPE*) (GetPacketBuffer() + m_usPacketSize)) = *pData;
		m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(DATA_TYPE));
	}
	//

	template <class DATA_TYPE> void AddValue(DATA_TYPE Data)
	{
		ASSERT(GetFreeSize() >= sizeof(DATA_TYPE));
		*((DATA_TYPE*) (GetPacketBuffer() + m_usPacketSize)) = Data;
		m_usPacketSize = (unsigned short) (m_usPacketSize + sizeof(DATA_TYPE));
	}
	template <class DATA_TYPE> void AddByPointer(DATA_TYPE* pData)
	{
		AddData(pData);
	}
	void AddByPointer(PBYTE pData, UINT uiDataSize);
	void AddNTString(TCHAR* pString, UINT uiMaxStringBufSize);
	template <class DATA_TYPE> CPacketComposer& operator<<(DATA_TYPE Data)
	{
		AddValue(Data);
		return *this;
	}

	void Finalize();

	PBYTE GetPacketBuffer();
	PBYTE GetBodyBuffer();
	UINT GetPacketSize();
	UINT GetBodySize();
	UINT GetPacketBufferSize();
	UINT GetFreeSize();


protected:
	BYTE m_PacketBuffer[16384];
	unsigned short m_usPacketSize;
	unsigned short m_usCommand;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CPacketComposer.inl"


///////////////////////////////// End of File /////////////////////////////////
