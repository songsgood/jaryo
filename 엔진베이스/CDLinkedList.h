/******************************************************************************
	CDLinkedList.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


template <class DLinkedListEntry>
class CDLinkedList  
{
public:
	CDLinkedList() { }
	virtual ~CDLinkedList() { }

	void Add(DLinkedListEntry* pEntry);
	void Remove(DLinkedListEntry* pEntry);

	void Empty();

	DLinkedListEntry* GetFirst();
	DLinkedListEntry* GetNext(DLinkedListEntry* pEntry);

	DLinkedListEntry* Find(LPCTSTR lpStringKey);	// Case Sensitive
	DLinkedListEntry* FindI(LPCTSTR lpStringKey);	// Case Insensitive


private:
	DLinkedListEntry m_FirstEntry;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CDLinkedList.inl"


///////////////////////////////// End of File /////////////////////////////////
