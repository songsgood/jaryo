/******************************************************************************
	CSListAllocatorEntry.inl
******************************************************************************/


inline CSListAllocatorEntry::CSListAllocatorEntry() :
	m_pNextAllocatorEntry(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


inline void CSListAllocatorEntry::SetNextAllocatorEntry(CSListAllocatorEntry* pNextAllocatorEntry)
{
	m_pNextAllocatorEntry = pNextAllocatorEntry;
}


inline CSListAllocatorEntry* CSListAllocatorEntry::GetNextAllocatorEntry()
{
	return m_pNextAllocatorEntry;
}


///////////////////////////////// End of File /////////////////////////////////
