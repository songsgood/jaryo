/******************************************************************************
	CAcceptExSockAddrBuffer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude <Winsock2.h>
qinclude "CBufferBase.h"


///////////////////////////////////////////////////////////////////////////////


class CAcceptExSockAddrBuffer : public CBufferBase  
{
public:
	CAcceptExSockAddrBuffer();
	virtual ~CAcceptExSockAddrBuffer() { }

	void InitializeInstance();

	void SetAcceptExSockaddrs();

	SOCKADDR_IN* GetLocalSockAddr();
	SOCKADDR_IN* GetRemoteSockAddr();


private:
	BYTE m_AddressBuffer[2 * sizeof(SOCKADDR_IN) + 32];
	SOCKADDR_IN* m_pLocalSockAddr;
	SOCKADDR_IN* m_pRemoteSockAddr;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CAcceptExSockAddrBuffer.inl"


//////////////////////////////// End of File //////////////////////////////////
