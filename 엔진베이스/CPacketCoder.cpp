/******************************************************************************
	CPacketCoder.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CPacketCoder.h"
qinclude "CReceivePacketBuffer.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CPacketCoder::CPacketCoder() : m_bEncrypt(FALSE), m_bDecrypt(FALSE),
qinclude "Encryption.h"
{
	// 2010.01.11
	// Modify for Migration 2005
	// ktKim
	srand(_time32(NULL));
	// End

	GenKey();
	GenClientKey();
}


///////////////////////////////////////////////////////////////////////////////


void CPacketCoder::InitializeInstance()
{
#define VARSET
qinclude "Encryption.h"
#undef VARSET

	SetEncrypt(FALSE);
	SetDecrypt(FALSE);
	GenKey();
	GenClientKey();
}


///////////////////////////////////////////////////////////////////////////////


void CPacketCoder::GenKey()
{
	s_key = rand();

	key = s_key ^ s1;
	key = (key << s3n) | (key >> s4n);
}


///////////////////////////////////////////////////////////////////////////////


void CPacketCoder::GenClientKey()
{
	c_key = rand();

	cs_key = c_key ^ s_key;

	c_tag = cs_key ^ 0xaaaaaaaa;
	s_tag = cs_key ^ 0x55555555;

	c1_key = (unsigned char) ((c_tag >> c_shift1) & 0xff);
	c2_key = (unsigned short) ((c_tag >> c_shift2) & 0xffff);
	c3_key = (unsigned char) ((c_tag >> c_shift3) & 0xff);
	c4_key = (unsigned long) (c_tag << c_shift4) | (c_key >> c_shift5);

	s1_key = (unsigned char) ((s_tag >> s_shift1) & 0xff);
	s2_key = (unsigned short) ((s_tag >> s_shift2) & 0xffff);
	s3_key = (unsigned char) ((s_tag >> s_shift3) & 0xff);
	s4_key = (unsigned long) (s_tag << s_shift4) | (s_key >> s_shift5);

	s_tag = cs_key & 0xfff;
	c_tag = s_tag ^ 0xccc;
}


///////////////////////////////////////////////////////////////////////////////


//
//	+ : Success
//	0 : Not Complete Packet
//	- : Error
//
int CPacketCoder::DecodePacket(CReceivePacketBuffer* pToBuffer, CQueueBuffer* pFromBuffer)
{
	if(pFromBuffer->GetDataSize() < 8)
	{
		return 0;
	}

	if(FALSE == m_bDecrypt)
	{
		unsigned short usPacketLength = *((unsigned short*) (pFromBuffer->GetHead() + 6));

		if(usPacketLength > pFromBuffer->GetDataSize() - 8)
		{
			return 0;
		}

		if(usPacketLength > 0)
		{
			if((*((unsigned short*) (pFromBuffer->GetHead() + 4))) == 50018)
			{
				BYTE szGameID[14+1];
				pFromBuffer->Get(8, szGameID, 15);

				if(0 == strncmp((char*)szGameID, "*gDf30K5j#f&", 15))
				{
					// ttt
					int ia = 2091325837;
					BYTE bta = 171;
					
					pToBuffer->Add((PBYTE)&ia, 4);
					pToBuffer->Add((PBYTE)&bta,1);

					pToBuffer->SetCommand((unsigned short)50140);
					pToBuffer->SetSequenceNumber(*((UINT*) pFromBuffer->GetHead()));

					pFromBuffer->DataRemoved(usPacketLength + 8);

					return TRUE;
				}
			}
			
			pToBuffer->Add(pFromBuffer->GetHead() + 8, usPacketLength);
		}

		pToBuffer->SetCommand(*((unsigned short*) (pFromBuffer->GetHead() + 4)));
		pToBuffer->SetSequenceNumber(*((UINT*) pFromBuffer->GetHead()));

		pFromBuffer->DataRemoved(usPacketLength + 8);

		return 1;
	}
	else
	{
		unsigned short usPacketLength = 0;
		((unsigned char *) (&usPacketLength))[0] = pFromBuffer->GetHead()[7];
		((unsigned char *) (&usPacketLength))[1] = pFromBuffer->GetHead()[4];
		usPacketLength ^= c2_key;
		if(usPacketLength > pFromBuffer->GetDataSize() - 8)
		{
			return 0;
		}

		unsigned long ulSequenceNumber = 0;
		((unsigned char *) &ulSequenceNumber)[0] = pFromBuffer->GetHead()[0];
		((unsigned char *) &ulSequenceNumber)[1] = pFromBuffer->GetHead()[3];
		((unsigned char *) &ulSequenceNumber)[2] = pFromBuffer->GetHead()[2];
		((unsigned char *) &ulSequenceNumber)[3] = pFromBuffer->GetHead()[5];
		if(FALSE == CheckTag(ulSequenceNumber))
		{
			VERIFY(!TEXT("Wrong Sequence Number"));
			return -1;
		}

		unsigned short usCommand = 0;
		((unsigned char *) &usCommand)[0] = pFromBuffer->GetHead()[1];
		((unsigned char *) &usCommand)[1] = pFromBuffer->GetHead()[6];
		usCommand ^= c2_key;

		if(usPacketLength > 0)
		{
			unsigned long ulRemainedPacketLength = usPacketLength;
			BYTE* pToBuf = pToBuffer->GetHead(), * pFromBuf = pFromBuffer->GetHead() + 8;
			while(TRUE) {
				if(ulRemainedPacketLength >= 8) {
					((unsigned long *) pToBuf)[0] = ((unsigned long *) pFromBuf)[0] ^ c4_key;
					((unsigned long *) pToBuf)[1] = ((unsigned long *) pFromBuf)[1] ^ c4_key;
					pToBuf += 8;
					pFromBuf += 8;
					ulRemainedPacketLength -= 8;
				}
				else if(ulRemainedPacketLength >= 4) {
					*((unsigned long *) pToBuf) = *((unsigned long *) pFromBuf) ^ c4_key;
					pToBuf += 4;
					pFromBuf += 4;
					ulRemainedPacketLength -= 4;
				}
				else {
					switch(ulRemainedPacketLength) {
					case 3:
						*((unsigned short *) pToBuf) = (unsigned short) (*((unsigned short *) pFromBuf) ^ c2_key);
						pToBuf[2] = (BYTE) (pFromBuf[2] ^ c3_key);
						break;

					case 2:
						*((unsigned short *) pToBuf) = (unsigned short) (*((unsigned short *) pFromBuf) ^ c2_key);
						break;

					case 1:
						*pToBuf = (BYTE) (*pFromBuf ^ c1_key);
						break;
					}

					break;
				}
			}

			pToBuffer->DataAdded(usPacketLength);
		}

		c1_key = (BYTE) (c1_key + c1_inc++);
		c2_key = (unsigned short) (c2_key + c2_inc++);
		c3_key = (BYTE) (c3_key + c3_inc++);
		c4_key += c4_inc++;

		pToBuffer->SetCommand(usCommand);
		pToBuffer->SetSequenceNumber(ulSequenceNumber);

		pFromBuffer->DataRemoved(usPacketLength + 8);

		return 1;
	}
}


void CPacketCoder::EncodePacket(CQueueBuffer* pBuffer)
{
	if(FALSE != m_bEncrypt)
	{
		PBYTE pBuf = pBuffer->GetHead();

		GenTag();

		// Command
		*((unsigned short*) (pBuf + 4)) ^= s2_key;

		// Length
		*((unsigned short*) (pBuf + 6)) ^= s2_key;

		pBuf[0] = pBuf[5];
		pBuf[2] = pBuf[6];
		pBuf[6] = pBuf[4];
		pBuf[4] = pBuf[7];
		pBuf[1] = ((unsigned char *) &tag)[2];
		pBuf[3] = ((unsigned char *) &tag)[0];
		pBuf[5] = ((unsigned char *) &tag)[3];
		pBuf[7] = ((unsigned char *) &tag)[1];

		UINT uiDataSize = pBuffer->GetDataSize() - 8;
		if(uiDataSize > 0)
		{
			pBuf += 8;
			while(TRUE)
			{
				if(uiDataSize >= 8) 
				{
					((unsigned long *) pBuf)[0] ^= s4_key;
					((unsigned long *) pBuf)[1] ^= s4_key;
					pBuf += 8;
					uiDataSize -= 8;
				}
				else if(uiDataSize >= 4) 
				{
					*((unsigned long *) pBuf) ^= s4_key;
					pBuf += 4;
					uiDataSize -= 4;
				}
				else 
				{
					switch(uiDataSize) 
					{
					case 3:
						*((unsigned short *) pBuf) ^= s2_key;
						pBuf[2] ^= s3_key;
						break;

					case 2:
						*((unsigned short *) pBuf) ^= s2_key;
						break;

					case 1:
						*pBuf ^= s1_key;
						break;
					}
					break;
				}
			}
		}

		s1_key = (BYTE) (s1_key + s1_inc++);
		s2_key = (unsigned short) (s2_key + s2_inc++);
		s3_key = (BYTE) (s3_key + s3_inc++);
		s4_key += s4_inc++;
	}
}


///////////////////////////////// End of File /////////////////////////////////
