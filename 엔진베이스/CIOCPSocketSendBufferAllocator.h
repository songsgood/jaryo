/******************************************************************************
	CIOCPSocketSendBufferAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CISLAllocator.h"
qinclude "CIOCPSocketSendBuffer.h"
qinclude "CPrototypeAllocator.h"


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketSendBufferAllocator  
{
public:
	CIOCPSocketSendBufferAllocator();
	/*virtual */~CIOCPSocketSendBufferAllocator();

	void AllocAllocator(UINT* puiBufferSize, UINT* puiPreAllocSize, UINT uiAllocatorCount);
	void FreeAllocator();

	CIOCPSocketSendBuffer* Allocate(UINT uiDataSize);
	void Free(CIOCPSocketSendBuffer* pBuffer);


private:
	CISLAllocator< CIOCPSocketSendBuffer, CPrototypeAllocator<CIOCPSocketSendBuffer> >* m_pAllocator;
	UINT m_uiAllocatorCount;
	UINT* m_puiBufferSize;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketSendBufferAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
