/******************************************************************************
	CServiceStatus.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude <Winsvc.h>
qinclude "CGate.h"
qinclude "CServiceSync.h"


///////////////////////////////////////////////////////////////////////////////


class CService;


///////////////////////////////////////////////////////////////////////////////


#define MAX_SERVICENAME_LENGTH	256


///////////////////////////////////////////////////////////////////////////////


class CServiceStatus  
{
public:
	CServiceStatus(LPCTSTR pServiceName, DWORD dwServiceType = SERVICE_WIN32_SHARE_PROCESS,
		DWORD dwControlsAccepted = SERVICE_ACCEPT_STOP);
	virtual ~CServiceStatus();

	virtual CServiceStatus* AllocSingleEntry(LPCTSTR szServiceName);
	static CServiceStatus* Allocate(LPCTSTR szServiceName);

	DWORD GetCurrentState();

	LPCTSTR GetServiceName();

	CServiceSync* GetServiceSync();

	void SetDebugMode(BOOL bDebugMode);

	void SetService(CService* pService);
	CService* GetService();

	BOOL RegisterServiceCtrlHandlerEx(LPHANDLER_FUNCTION_EX lpHandlerProc);

	BOOL BeginStateTransition(DWORD dwUltimateState, DWORD dwWaitHint = 50000);
	BOOL AdvanceStateTransition(DWORD dwWaitHint, DWORD dwCheckPoint = 0);
	BOOL EndStateTransition();

	void StateTransitionToStop();

	virtual BOOL ProcessCommandLine(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv);
	virtual BOOL OnInitService();
	virtual void OnCloseService() { }


protected:
	BOOL SetServiceStatus();


protected:
	TCHAR m_szServiceName[MAX_SERVICENAME_LENGTH + 1];
	SERVICE_STATUS m_ServiceStatus;
	SERVICE_STATUS_HANDLE m_hServiceStatus;
	BOOL m_bDebugMode;

	CService* m_pService;

	CGate m_Gate;

	CServiceSync m_ServiceSync;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CServiceStatus.inl"


///////////////////////////////// End of File /////////////////////////////////
