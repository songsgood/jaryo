/******************************************************************************
	CSinglyLinkedList.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CSinglyListEntry;


///////////////////////////////////////////////////////////////////////////////


class CSinglyLinkedList  
{
public:
	CSinglyLinkedList();
	virtual ~CSinglyLinkedList() { }

	void Push(CSinglyListEntry* pEntry);
	CSinglyListEntry* Pop();

	CSinglyListEntry* GetFirst();
	CSinglyListEntry* GetNext(CSinglyListEntry* pEntry);


private:
	CSinglyListEntry* m_pFirstEntry;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSinglyLinkedList.inl"


///////////////////////////////// End of File /////////////////////////////////
