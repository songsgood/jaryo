/******************************************************************************
	CSocketIOCPOverlapped.inl
******************************************************************************/


inline void CSocketIOCPOverlapped::Initialize()
{
	::ZeroMemory((OVERLAPPED*) this, sizeof(OVERLAPPED));
}


///////////////////////////////////////////////////////////////////////////////


inline void CSocketIOCPOverlapped::SetClient(CIOCPSocketClient* pIOCPSocketClient)
{
	m_pIOCPSocketClient = pIOCPSocketClient;
}


inline CIOCPSocketClient* CSocketIOCPOverlapped::GetClient()
{
	return m_pIOCPSocketClient;
}


///////////////////////////////////////////////////////////////////////////////


inline void CSocketIOCPOverlapped::SetOpCode(OPCODE OpCode)
{
	m_OpCode = OpCode;
}


inline OPCODE CSocketIOCPOverlapped::GetOpCode()
{
	return m_OpCode;
}


///////////////////////////////// End of File /////////////////////////////////
