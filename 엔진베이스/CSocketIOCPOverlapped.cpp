/******************************************************************************
	CSocketIOCPOverlapped.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CSocketIOCPOverlapped.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CSocketIOCPOverlapped::CSocketIOCPOverlapped(CIOCPSocketClient* pIOCPSocketClient/* = NULL*/)
	: m_OpCode(OP_NONE), m_pIOCPSocketClient(pIOCPSocketClient)
{
	Initialize();
}


///////////////////////////////// End of File /////////////////////////////////
