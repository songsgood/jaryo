/******************************************************************************
	CIOCPSocketSendBufferAllocator.inl
******************************************************************************/


inline CIOCPSocketSendBufferAllocator::~CIOCPSocketSendBufferAllocator()
{
	FreeAllocator();
}


///////////////////////////////////////////////////////////////////////////////


inline void CIOCPSocketSendBufferAllocator::Free(CIOCPSocketSendBuffer* pBuffer)
{
	m_pAllocator[pBuffer->GetAllocatorIndex()].Delete(pBuffer);
}


///////////////////////////////// End of File /////////////////////////////////
