/******************************************************************************
	CSocketServer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CServer.h"
qinclude "CISLAllocator.h"
qinclude "CPrototypeAllocator.h"


///////////////////////////////////////////////////////////////////////////////


class CSocketClient;
class CServerSocket;


///////////////////////////////////////////////////////////////////////////////


class CSocketServer : public CServer  
{
public:
	CSocketServer(DWORD dwServerTimerInterval = INFINITE);
	virtual ~CSocketServer();

	virtual BOOL OnInitialize();
	virtual void OnClose();

	CSocketClient* AllocateClient();
	virtual void FreeClient(CSocketClient* pSocketClient);

	void SetServerPort(unsigned short usPort);

	CServerSocket* GetServerSocket();


protected:
	virtual CServerSocket* AllocateServerSocket();

	virtual CSocketClient* IAllocateClient();

	BOOL CreateServerSocket();
	void DestroyServerSocket();


private:
	CServerSocket* m_pServerSocket;
	unsigned short m_usPort;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketServer.inl"


///////////////////////////////// End of File /////////////////////////////////
