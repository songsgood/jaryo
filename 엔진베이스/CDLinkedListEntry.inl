/******************************************************************************
	CDLinkedListEntry.inl
******************************************************************************/


inline void CDLinkedListEntry::InitializeInstance()
{
	SetRemoved();
}


inline BOOL CDLinkedListEntry::IsAdded()
{
	return (NULL != m_pPrev);
}


///////////////////////////////////////////////////////////////////////////////


inline void CDLinkedListEntry::SetOwner(void* pOwner)
{
	m_pOwner = pOwner;
}


inline void* CDLinkedListEntry::GetOwner()
{
	return m_pOwner;
}


///////////////////////////////////////////////////////////////////////////////


inline void CDLinkedListEntry::SetNext(CDLinkedListEntry* pNext)
{
	m_pNext = pNext;
}


inline CDLinkedListEntry* CDLinkedListEntry::GetNext()
{
	return m_pNext;
}


inline void CDLinkedListEntry::SetPrev(CDLinkedListEntry* pPrev)
{
	m_pPrev = pPrev;
}


inline CDLinkedListEntry* CDLinkedListEntry::GetPrev()
{
	return m_pPrev;
}


///////////////////////////////// End of File /////////////////////////////////
