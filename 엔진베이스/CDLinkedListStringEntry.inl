/******************************************************************************
	CDLinkedListStringEntry.inl
******************************************************************************/


inline CDLinkedListStringEntry::CDLinkedListStringEntry() :
	m_pStringKeyBuffer(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


inline void CDLinkedListStringEntry::SetStringKeyBuffer(TCHAR* pStringKeyBuffer)
{
	m_pStringKeyBuffer = pStringKeyBuffer;
}


///////////////////////////////////////////////////////////////////////////////


inline LPCTSTR CDLinkedListStringEntry::GetKey()
{
	return m_pStringKeyBuffer;
}


///////////////////////////////////////////////////////////////////////////////


// Case Sensitive
inline BOOL CDLinkedListStringEntry::IsSame(LPCTSTR lpStringKey)
{
	return (0 == ::lstrcmp(GetKey(), lpStringKey));
}


// Case Insensitive
inline BOOL CDLinkedListStringEntry::IsSameI(LPCTSTR lpStringKey)
{
	return (0 == ::lstrcmpi(GetKey(), lpStringKey));
}


///////////////////////////////// End of File /////////////////////////////////
