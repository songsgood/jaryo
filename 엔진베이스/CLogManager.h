/******************************************************************************
	CLogManager.h
******************************************************************************/


qinclude "CEventLog.h"
qinclude "CFileLog.h"


///////////////////////////////////////////////////////////////////////////////


qpragma once


///////////////////////////////////////////////////////////////////////////////

class CLogManager
{
public:
	CLogManager() { }
	CLogManager(LPCTSTR lpAppName);
	~CLogManager() {};

	void SetAppName(LPCTSTR lpAppName);

	virtual BOOL WriteLogToFile(LPCTSTR lpFormat, ...);
	virtual BOOL WriteLogToFileNew(LPCTSTR lpLogType, LPCTSTR lpLogAction, LPCTSTR lpLogActionResult, LPCTSTR szLogString = "");
	virtual BOOL WriteLogToFileHour(LPCTSTR lpLogType, LPCTSTR lpLogAction, LPCTSTR lpLogActionResult, LPCTSTR szLogString = "");

	BOOL ReportEvent(LPCTSTR lpFormat, ...);
	BOOL ReportEvent(WORD wType, LPCTSTR lpFormat, ...);

	BOOL WriteLog(WORD wType, LPCTSTR lpFormat, ...);
	BOOL WriteLog(LPCTSTR lpFormat, ...);

private:
	CEventLog m_EventLog;
	CFileLog m_FileLog;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CLogManager.inl"


///////////////////////////////////////////////////////////////////////////////


extern CLogManager g_LogManager;


///////////////////////////////// End of File /////////////////////////////////