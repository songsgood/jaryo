/******************************************************************************
	CClientTickManager.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CClientTickManager  
{
public:
	CClientTickManager();
	/*virtual */~CClientTickManager() { }

	void Initialize();

	BOOL IsValidClientTick(DWORD dwCurrentClientTick);


private:
	DWORD m_dwFirstClientTick, m_dwFirstClientTickUpdateTick;
	DWORD m_dwThreshold;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CClientTickManager.inl"


///////////////////////////////// End of File /////////////////////////////////
