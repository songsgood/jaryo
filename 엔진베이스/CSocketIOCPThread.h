/******************************************************************************
	CSocketIOCPThread.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPThread.h"
qinclude "CReceivePacketBuffer.h"
qinclude "CSocketIOCPOverlapped.h"


///////////////////////////////////////////////////////////////////////////////


class CSocketIOCPThread;
class CIOCPSocketClient;
class CSocketServer;


///////////////////////////////////////////////////////////////////////////////


typedef void (CSocketIOCPThread::*FP_PACKETPROCESSFUNCTION)(CIOCPSocketClient*);


///////////////////////////////////////////////////////////////////////////////


class CSocketIOCPThread : public CIOCPThread  
{
protected:
	typedef OPCODE (CSocketIOCPOverlapped::*FP_GET_OPCODE)();
	typedef void (CSocketIOCPThread::*FP_ON_PEER_DISCONNECTED)(CIOCPSocketClient*);
	typedef void (CSocketIOCPThread::*FP_SOCKET_IO)(CIOCPSocketClient*, DWORD);


public:
	CSocketIOCPThread(CIOCP* pIOCP);
	virtual ~CSocketIOCPThread();

	virtual CIOCPThread* AllocSingleThread();
	static CSocketIOCPThread* Allocate(CIOCP* pIOCP);

	static void InitializePacketProcessFunction();
	static void CleanupPacketProcessFunction();
	static void InitializePacketProcessFunction(UINT uiPacketProcessFunctionState);
	static void CleanupPacketProcessFunction(UINT uiPacketProcessFunctionState);
	static void SetPacketProcessFunction(UINT uiPacketProcessFunctionState, UINT uiCommand,
		FP_PACKETPROCESSFUNCTION pFunction);


protected:
	virtual BOOL ProcessFailedIO(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped);
	virtual BOOL ProcessCompletedIO(ULONG_PTR pulCompletionKey, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED pOverlapped);
	void ProcessCompletedIO(FP_GET_OPCODE pGetOpCode, FP_ON_PEER_DISCONNECTED pOnPeerDisconnected, FP_SOCKET_IO pOnReceived, FP_SOCKET_IO pOnSent, FP_SOCKET_IO pOnAccepted, CSocketIOCPOverlapped* pSocketIOCPOverlapped, CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred);

	void OnReceived(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred);
	void OnSent(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred);
	void OnAccepted(CIOCPSocketClient* pIOCPSocketClient, DWORD dwNumberOfBytesTransferred);
	
	virtual void OnPeerDisconnected(CIOCPSocketClient* pIOCPSocketClient);

	/*virtual */void OnReceive(CIOCPSocketClient* pIOCPSocketClient);
	

	virtual void ProcessPacket(CIOCPSocketClient* pIOCPSocketClient);
	void ProcessUnknownPacket(CIOCPSocketClient* pIOCPSocketClient);

	void PostAnotherAcceptEx(CSocketServer* pServer);


protected:
	static FP_PACKETPROCESSFUNCTION** m_ppPacketProcessFunction;

	CReceivePacketBuffer m_ReceivePacketBuffer;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketIOCPThread.inl"


///////////////////////////////// End of File /////////////////////////////////
