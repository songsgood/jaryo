/******************************************************************************
	CIOCPSocketClient.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPSocketClient.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketClient::OnAccepted()
{
	GetClientSocket()->GetSockAddrBuffer()->SetAcceptExSockaddrs();
	GetServer()->GetIOCP()->AssociateDevice((HANDLE) GetClientSocket()->GetHandle(), (ULONG_PTR) this);
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketClient::DisconnectClient()
{
	if(FALSE != GetClientSocket()->DisconnectClient())
	{
		ReleaseRef();
	}
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketClient::ReleaseRef()
{
	if(0 == m_RefCounter.ReleaseRef())
	{
		if(NULL == GetServer())
		{
			VERIFY(!"NULL == GetServer()");
		}
		else
		{
			GetServer()->FreeClient(this);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketClient::InitializeInstance()
{
	CPacketCoder::InitializeInstance();

	m_RefCounter.Initialize();
//	GetClientSocket()->InitializeInstance();
	AllocateClientSocket();
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketClient::AllocateClientSocket()
{
	if(NULL == GetClientSocket())
	{
		CClientSocket* pClientSocket = NULL;
		VERIFY(NULL != (pClientSocket = new CIOCPClientSocket(this, this)));
		SetClientSocket(pClientSocket);
	}
	else
	{
		GetClientSocket()->InitializeInstance();
	}
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPSocketClient::WaitForClientConnection()
{
	AddRef();
	if(FALSE == GetClientSocket()->Create())
	{
		return FALSE;
	}
	return PostAcceptEx();
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPSocketClient::OnReceived(DWORD dwReceivedDataSize)
{
	if(FALSE != ::IsBadReadPtr(GetClientSocket(), sizeof(CIOCPClientSocket)))
	{
		VERIFY(!"FALSE != ::IsBadReadPtr(GetClientSocket(), sizeof(CIOCPClientSocket))");
		return FALSE;
	}
	GetClientSocket()->OnReceived(dwReceivedDataSize);
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPSocketClient::Connect(const char* pAddress, unsigned short usPort)
{
	AddRef();
	if(FALSE == CSocketClient::Connect(pAddress, usPort))
	{
		ReleaseRef();
		return FALSE;
	}
	// PostWSAReceive();
	return TRUE;
}


BOOL CIOCPSocketClient::Connect()
{
	AddRef();
	if(FALSE == CSocketClient::Connect())
	{
		ReleaseRef();
		return FALSE;
	}
	// PostWSAReceive();
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketClient::PostWSASend()
{
	AddRef();
	if(FALSE == GetClientSocket()->PostWSASend())
	{
		ReleaseRef();
	}
}


BOOL CIOCPSocketClient::PostWSAReceive()
{
	AddRef();
	if(FALSE == GetClientSocket()->PostWSAReceive())
	{
		ReleaseRef();
		return FALSE;
	}
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPSocketClient::AddToSendQueue(CPacketComposer* pPacketComposer)
{
	pPacketComposer->Finalize();
	if(FALSE != ::IsBadReadPtr(GetClientSocket(), sizeof(CIOCPClientSocket)))
	{
		g_LogManager.WriteLogToFile(TEXT("A41316 == GetClientSocket()"), GetClientSocket());
turn FALSE;
	}
	GetClientSocket()->AddToSendQueue(pPacketComposer->GetPacketBuffer(), pPacketComposer->GetPacketSize());
	return TRUE;
}


void CIOCPSocketClient::Send(CPacketComposer* pPacketComposer, BOOL bSend/* = TRUE*/)
{
	// Write Packet Log
	/*g_LogManager.WriteLogToFile("Send\t10752790\t0", *((unsigned short*) (pPacketComposer->GetPacketBuffer() + 4)),
cketComposer->GetPacketSize());*/
	//

	if(FALSE == AddToSendQueue(pPacketComposer))
	{
		return;
	}

	if(FALSE != bSend)
	{
		PostWSASend();
	}
}

void CIOCPSocketClient::Send( PBYTE pData, UINT uiDataSize , BOOL bSend /*= TRUE*/ )
{
	if(FALSE != ::IsBadReadPtr(GetClientSocket(), sizeof(CIOCPClientSocket)))
	{
		g_LogManager.WriteLogToFile(TEXT("A41316 == BYTE GetClientSocket()"), GetClientSocket());
turn;
	}
	
	GetClientSocket()->AddToSendQueue(pData, uiDataSize);

	if(FALSE != bSend)
	{
		PostWSASend();
	}
}


///////////////////////////////////////////////////////////////////////////////


BOOL CIOCPSocketClient::PostAcceptEx()
{
	AddRef();
	if(FALSE != GetClientSocket()->PostAcceptEx())
	{
		return TRUE;
	}
	ReleaseRef();
	VERIFY(!"PostAcceptEx() Failed");
	return FALSE;
}


///////////////////////////////// End of File /////////////////////////////////
