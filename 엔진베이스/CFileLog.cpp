/******************************************************************************
	CFileLog.cpp
******************************************************************************/


//////////////////////////////////////////////////////////////////////////////



qinclude "stdafx.h"
qinclude "CFileLog.h"

qinclude <share.h>



//////////////////////////////////////////////////////////////////////////////

CFileLog::CFileLog()
:m_pFile(NULL)
{
	//TCHAR LogFilePath[_MAX_PATH + 1] = {TEXT('\0')};

	//m_pFile = ::_fsopen( ::MakeLogFullPath(LogFilePath, _countof(LogFilePath), "Log\\System\\"), "ac", _SH_DENYNO );
	//if(NULL == m_pFile)
	//{
	//	//
	//}

}


CFileLog::~CFileLog()
{
	//if ( NULL != m_pFile)
	//{
	//	fclose(m_pFile);
	//}

}

BOOL CFileLog::WriteLog(LPCTSTR lpLogMessage)
{

	TCHAR LogFilePath[_MAX_PATH + 1] = {TEXT('\0')};
	FILE* pFile = NULL;

	pFile = ::_fsopen( ::MakeLogFullPath(LogFilePath, _countof(LogFilePath), "Log\\System\\", m_szAppName), "a+", _SH_DENYNO );

	if ( NULL == pFile)
	{
		return FALSE;
	}

	TCHAR szLogMessage[2048];

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);

	wsprintf(szLogMessage, "[%04u/%02u/%02u %02u:%02u:%02u]\t%s\t[%x]\t%s\r\n", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay,
		SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, m_szAppName, ::GetCurrentThreadId(), lpLogMessage);

	fprintf(pFile, "%s\r\n", szLogMessage);

	fclose(pFile);
	
	return TRUE;
}

BOOL CFileLog::WriteLog(LPCTSTR lpLogType, LPCTSTR lpLogAction, LPCTSTR lpLogActionResult, LPCTSTR lpLogMessage)
{

	TCHAR LogFilePath[_MAX_PATH + 1] = {TEXT('\0')};
	FILE* pFile = NULL;

	pFile = ::_fsopen( ::MakeLogFullPath(LogFilePath, _countof(LogFilePath), "Log\\System\\", m_szAppName), "a+", _SH_DENYNO );

	if ( NULL == pFile)
	{
		return FALSE;
	}

	TCHAR szLogMessage[2048];

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);

	wsprintf(szLogMessage, "[%04u/%02u/%02u %02u:%02u:%02u]\t%s\t[%x]\t[%s][%s:%s]\t%s\r\n", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay,
		SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, m_szAppName, ::GetCurrentThreadId(), lpLogType, lpLogAction, lpLogActionResult, lpLogMessage);

	fprintf(pFile, "%s\r\n", szLogMessage);

	fclose(pFile);

	return TRUE;
}

BOOL CFileLog::WriteLogHour(LPCTSTR lpLogType, LPCTSTR lpLogAction, LPCTSTR lpLogActionResult, LPCTSTR lpLogMessage)
{
	TCHAR LogFilePath[_MAX_PATH + 1] = {TEXT('\0')};
	FILE* pFile = NULL;

	pFile = ::_fsopen( ::MakeLogFullPathHour(LogFilePath, _countof(LogFilePath), "Log\\System\\", m_szAppName), "a+", _SH_DENYNO );

	if ( NULL == pFile)
	{
		return FALSE;
	}

	TCHAR szLogMessage[2048];

	SYSTEMTIME SystemTime;
	::GetLocalTime(&SystemTime);

	wsprintf(szLogMessage, "[%04u/%02u/%02u %02u:%02u:%02u]\t%s\t[%x]\t[%s][%s:%s]\t%s\r\n", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay,
		SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond, m_szAppName, ::GetCurrentThreadId(), lpLogType, lpLogAction, lpLogActionResult, lpLogMessage);

	fprintf(pFile, "%s\r\n", szLogMessage);

	fclose(pFile);

	return TRUE;
}

///////////////////////////////// End of File /////////////////////////////////
