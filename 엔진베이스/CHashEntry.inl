/******************************************************************************
	CHashEntry.inl
******************************************************************************/


template <class DLinkedListEntry>
inline CHashEntry<DLinkedListEntry>::CHashEntry() :
	m_uiHashIndex(0)
{
}


///////////////////////////////////////////////////////////////////////////////


template <class DLinkedListEntry>
inline void CHashEntry<DLinkedListEntry>::SetHashIndex(UINT uiHashIndex)
{
	m_uiHashIndex = uiHashIndex;
}


template <class DLinkedListEntry>
inline UINT CHashEntry<DLinkedListEntry>::GetHashIndex()
{
	return m_uiHashIndex;
}


///////////////////////////////// End of File /////////////////////////////////
