/******************************************************************************
	Char.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "Debug.h"


///////////////////////////////////////////////////////////////////////////////


inline BOOL AtoU(CHAR *pszAnsiString, WCHAR *pszUnicodeString, INT cSizeUnicodeBuffer)
{
	int iRetVal = 0;
	VERIFY_CHECKLASTERROR(0 != (iRetVal = ::MultiByteToWideChar(CP_ACP, MB_USEGLYPHCHARS, pszAnsiString, -1,
		pszUnicodeString, cSizeUnicodeBuffer)));
	return (0 != iRetVal);
}


inline BOOL UtoA(WCHAR *pszUnicodeString, CHAR *pszAnsiString, INT cSizeAsciiBuffer)
{
	int iRetVal = 0;
	VERIFY_CHECKLASTERROR(0 != (iRetVal = ::WideCharToMultiByte(CP_ACP, 0, pszUnicodeString, -1, pszAnsiString,
		cSizeAsciiBuffer, NULL, NULL)));
	return (0 != iRetVal);
}


///////////////////////////////// End of File /////////////////////////////////
