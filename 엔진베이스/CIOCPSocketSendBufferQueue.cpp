/******************************************************************************
	CIOCPSocketSendBufferQueue.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPSocketSendBufferQueue.h"
qinclude "CPacketCoder.h"
qinclude "CIOCPSocketSendBufferAllocator.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CIOCPSocketSendBufferAllocator* CIOCPSocketSendBufferQueue::m_pIOCPSocketSendBufferAllocator = NULL;


///////////////////////////////////////////////////////////////////////////////


CIOCPSocketSendBufferQueue::CIOCPSocketSendBufferQueue(CPacketCoder* pPacketCoder) :
	m_bDirty(FALSE), m_uiSequenceNumber(0), m_pPacketCoder(pPacketCoder)
{
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketSendBufferQueue::Add(CIOCPSocketSendBuffer* pBeginBuffer, CIOCPSocketSendBuffer* pEndBuffer)
{
	m_Queue.AddTail(pBeginBuffer, pEndBuffer);
	SetDirty();
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketSendBufferQueue::CreateIOCPSocketSendBufferAllocator(UINT* puiBufferSize, UINT* puiPreAllocSize,
	UINT uiAllocatorCount)
{
	VERIFY(NULL != (m_pIOCPSocketSendBufferAllocator = new CIOCPSocketSendBufferAllocator));
	m_pIOCPSocketSendBufferAllocator->AllocAllocator(puiBufferSize, puiPreAllocSize, uiAllocatorCount);
}


CIOCPSocketSendBufferAllocator* CIOCPSocketSendBufferQueue::GetIOCPSocketSendBufferAllocator()
{
	return m_pIOCPSocketSendBufferAllocator;
}


void CIOCPSocketSendBufferQueue::DeleteIOCPSocketSendBufferAllocator()
{
	if(NULL != m_pIOCPSocketSendBufferAllocator)
	{
		delete m_pIOCPSocketSendBufferAllocator;
		m_pIOCPSocketSendBufferAllocator = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketSendBufferQueue::Add(PBYTE pData, UINT uiDataSize)
{
	CIOCPSocketSendBuffer* pBuffer = (CIOCPSocketSendBuffer*) m_pIOCPSocketSendBufferAllocator->Allocate(uiDataSize);
	pBuffer->Add(pData, uiDataSize);
	m_Queue.AddTail(pBuffer);
	SetDirty();
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketSendBufferQueue::Empty()
{
	CIOCPSocketSendBuffer* pBuffer = NULL;
	while(NULL != (pBuffer = (CIOCPSocketSendBuffer*) m_Queue.RemoveHead()))
	{
		if(FALSE != ::IsBadReadPtr(m_pIOCPSocketSendBufferAllocator, sizeof(CIOCPSocketSendBufferAllocator)))
		{
			g_LogManager.WriteLogToFile("FALSE != ::IsBadReadPtr(m_pIOCPSocketSendBufferAllocator, sizeof(CIOCPSocketSendBufferAllocator) [A41316]",
m_pIOCPSocketSendBufferAllocator);
			return;
		}
		m_pIOCPSocketSendBufferAllocator->Free(pBuffer);
	}
}


///////////////////////////////////////////////////////////////////////////////


DWORD CIOCPSocketSendBufferQueue::GetWSABuf(LPWSABUF pWSABuf, DWORD dwWSABufCount, UINT uiMaxDataSize)
{
	UnSetDirty();

	CIOCPSocketSendBuffer* pSendBuffer = (CIOCPSocketSendBuffer*) m_Queue.GetFirstEntry();

	DWORD dwFilledWSABufCount = 0;
	UINT uiFilledDataSize = 0;

	while( (NULL != pSendBuffer) && (dwWSABufCount > dwFilledWSABufCount) && (uiMaxDataSize > uiFilledDataSize) )
	{
		if(pSendBuffer->GetDataSize() > 0)
		{
			if( (pSendBuffer->GetDataSize() >= uiMaxDataSize) && (dwFilledWSABufCount > 0) )
			{
				break;
			}

			if(false == pSendBuffer->IsEncrypted())
			{
				m_pPacketCoder->EncodePacket(pSendBuffer);
				pSendBuffer->SetEncrypted();
				// pSendBuffer->Encrypt(&m_uiSequenceNumber);
			}

			pWSABuf[dwFilledWSABufCount].buf = (char*) pSendBuffer->GetHead();
			// pWSABuf[dwFilledWSABufCount].len = MIN((uiMaxDataSize - uiFilledDataSize), pSendBuffer->GetDataSize());
			pWSABuf[dwFilledWSABufCount].len = pSendBuffer->GetDataSize();

			dwFilledWSABufCount++;
			uiFilledDataSize += pWSABuf[dwFilledWSABufCount].len;
		}
		pSendBuffer = (CIOCPSocketSendBuffer*) m_Queue.GetNextEntry(pSendBuffer);
	}

	return dwFilledWSABufCount;
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketSendBufferQueue::Removed(UINT uiDataSize)
{
	UINT uiDataRemoved = 0;
	CIOCPSocketSendBuffer* pSendBuffer = NULL;

	while(uiDataSize > 0)
	{
		pSendBuffer = (CIOCPSocketSendBuffer*) m_Queue.GetFirstEntry();
		if(NULL == pSendBuffer)
		{
			break;
		}
		if(0 == pSendBuffer->GetDataSize())
		{
			VERIFY(!TEXT("CIOCPSocketSendBufferQueue::Removed() 0 == pSendBuffer->GetDataSize()"));
			VERIFY(pSendBuffer == m_Queue.RemoveHead());
			m_pIOCPSocketSendBufferAllocator->Free(pSendBuffer);
		}
		else
		{
			if(uiDataSize >= pSendBuffer->GetDataSize())
			{
				uiDataRemoved = pSendBuffer->GetDataSize();
				VERIFY(pSendBuffer == m_Queue.RemoveHead());
				m_pIOCPSocketSendBufferAllocator->Free(pSendBuffer);
				uiDataSize -= uiDataRemoved;
			}
			else
			{
				uiDataRemoved = uiDataSize;
				pSendBuffer->DataRemoved(uiDataRemoved);
				uiDataSize = 0;
			}
		}
	}
}


///////////////////////////////// End of File /////////////////////////////////
