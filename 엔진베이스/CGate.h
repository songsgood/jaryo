/******************************************************************************
	CGate.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CGate  
{
public:
	CGate(BOOL bInitialState = TRUE, LPCTSTR lpName = NULL);
	virtual ~CGate();

	DWORD WaitToEnterGate(DWORD dwMilliseconds = INFINITE, BOOL bAlertable = FALSE);
	void LiftGate();


protected:
	HANDLE m_hGate;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CGate.inl"


///////////////////////////////// End of File /////////////////////////////////
