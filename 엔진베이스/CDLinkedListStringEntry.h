/******************************************************************************
	CDLinkedListStringEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CDLinkedListEntry.h"


///////////////////////////////////////////////////////////////////////////////


class CDLinkedListStringEntry : public CDLinkedListEntry  
{
public:
	CDLinkedListStringEntry();
	virtual ~CDLinkedListStringEntry() { }

	void SetStringKeyBuffer(TCHAR* pStringKeyBuffer);

	LPCTSTR GetKey();

	BOOL IsSame(LPCTSTR lpStringKey);	// Case Sensitive
	BOOL IsSameI(LPCTSTR lpStringKey);	// Case Insensitive


private:
	TCHAR* m_pStringKeyBuffer;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CDLinkedListStringEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
