/******************************************************************************
	CSocketIOCPOverlapped.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


enum OPCODE {OP_NONE, OP_CONNECT, OP_ACCEPT, OP_SEND, OP_RECV, OP_MAX};


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketClient;


///////////////////////////////////////////////////////////////////////////////


class CSocketIOCPOverlapped : public OVERLAPPED  
{
public:
	CSocketIOCPOverlapped(CIOCPSocketClient* pIOCPSocketClient = NULL);
	virtual ~CSocketIOCPOverlapped() { }

	void Initialize();

	void SetClient(CIOCPSocketClient* pIOCPSocketClient);
	CIOCPSocketClient* GetClient();

	void SetOpCode(OPCODE OpCode);
	OPCODE GetOpCode();


protected:
	OPCODE m_OpCode;
	CIOCPSocketClient* m_pIOCPSocketClient;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketIOCPOverlapped.inl"


///////////////////////////////// End of File /////////////////////////////////
