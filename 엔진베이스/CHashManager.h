/******************************************************************************
	CHashManager.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CHashObject;
class CDoublyLinkedList;


///////////////////////////////////////////////////////////////////////////////


class CHashManager  
{
public:
	CHashManager();
	virtual ~CHashManager();

	void Initialize(UINT uiHashSize);

	UINT GetHashSize();

	void Add(CHashObject* pObject);
	void Add(UINT uiHashIndex, CHashObject* pObject);

	void Remove(CHashObject* pObject);
	void Remove(UINT uiHashIndex, CHashObject* pObject);

	CHashObject* GetPartFirst(UINT uiHashIndex);
	CHashObject* GetPartNext(CHashObject* pObject);

	CHashObject* GetFirst();
	CHashObject* GetNext(CHashObject* pObject);

	void* GetPartFirstObject(UINT uiHashIndex);
	void* GetPartNextObject(CHashObject* pObject);

	BOOL IsCellEmpty(UINT uiHashIndex);
	BOOL IsEmpty();

	void* GetFirstObject();
	void* GetNextObject(CHashObject* pObject);

	void Empty();


protected:
	CDoublyLinkedList* m_pHashObjectList;
	UINT m_uiHashSize;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CHashManager.inl"


///////////////////////////////// End of File /////////////////////////////////
