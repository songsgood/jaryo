/******************************************************************************
	CDLinkedList.inl
******************************************************************************/


template <class DLinkedListEntry>
inline void CDLinkedList<DLinkedListEntry>::Add(DLinkedListEntry* pEntry)
{
	pEntry->SetNext(GetFirst());
	pEntry->SetPrev(&m_FirstEntry);
	if(NULL != GetFirst())
	{
		GetFirst()->SetPrev(pEntry);
	}
	m_FirstEntry.SetNext(pEntry);
}


template <class DLinkedListEntry>
inline void CDLinkedList<DLinkedListEntry>::Remove(DLinkedListEntry* pEntry)
{
	if(FALSE == pEntry->IsAdded())
	{
		return;
	}
	pEntry->GetPrev()->SetNext(pEntry->GetNext());
	if(NULL != pEntry->GetNext())
	{
		pEntry->GetNext()->SetPrev(pEntry->GetPrev());
	}
	pEntry->SetRemoved();
}


///////////////////////////////////////////////////////////////////////////////


template <class DLinkedListEntry>
inline void CDLinkedList<DLinkedListEntry>::Empty()
{
	while(NULL != GetFirst())
	{
		Remove(GetFirst());
	}
}


///////////////////////////////////////////////////////////////////////////////


template <class DLinkedListEntry>
inline DLinkedListEntry* CDLinkedList<DLinkedListEntry>::GetFirst()
{
	return ((DLinkedListEntry*) m_FirstEntry.GetNext());
}


template <class DLinkedListEntry>
inline DLinkedListEntry* CDLinkedList<DLinkedListEntry>::GetNext(DLinkedListEntry* pEntry)
{
	return ((DLinkedListEntry*) pEntry->GetNext());
}


///////////////////////////////////////////////////////////////////////////////


template <class DLinkedListEntry>
inline DLinkedListEntry* CDLinkedList<DLinkedListEntry>::Find(LPCTSTR lpStringKey)
{
	DLinkedListEntry* pEntry = GetFirst();
	while(NULL != pEntry)
	{
		if(FALSE != pEntry->IsSame(lpStringKey))
		{
			return pEntry;
		}
		pEntry = GetNext(pEntry);
	}
	return NULL;
}


template <class DLinkedListEntry>
inline DLinkedListEntry* CDLinkedList<DLinkedListEntry>::FindI(LPCTSTR lpStringKey)
{
	DLinkedListEntry* pEntry = GetFirst();
	while(NULL != pEntry)
	{
		if(FALSE != pEntry->IsSameI(lpStringKey))
		{
			return pEntry;
		}
		pEntry = GetNext(pEntry);
	}
	return NULL;
}


///////////////////////////////// End of File /////////////////////////////////
