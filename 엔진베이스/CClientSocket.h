/******************************************************************************
	CClientSocket.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CWSASocket.h"


///////////////////////////////////////////////////////////////////////////////


class CServerSocket;


///////////////////////////////////////////////////////////////////////////////


class CClientSocket : public CWSASocket  
{
public:
	CClientSocket();
	virtual ~CClientSocket() { }

	void SetServerSocket(CServerSocket* pServerSocket);
	CServerSocket* GetServerSocket();

	BOOL CreateConnect(const char* pAddress, unsigned short usPort, int af = AF_INET, int type = SOCK_STREAM,
		int protocol = IPPROTO_IP, LPWSAPROTOCOL_INFO lpProtocolInfo = NULL, GROUP g = 0,
		DWORD dwFlags = WSA_FLAG_OVERLAPPED);


protected:
	CServerSocket* m_pServerSocket;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CClientSocket.inl"


///////////////////////////////// End of File /////////////////////////////////
