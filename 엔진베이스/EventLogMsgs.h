/**************************************************************
Module:  EventLogMsgs.mc
**************************************************************/
//******************** CATEGORY SECTION ***********************
// Category IDs are 16-bit values
//
//  Values are 32 bit values layed out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: EL_CAT_APPEXECSTATUS
//
// MessageText:
//
//  Execution status
//
#define EL_CAT_APPEXECSTATUS             ((WORD)0x00000001L)

//
// MessageId: EL_CAT_APPEVENT
//
// MessageText:
//
//  Event
//
#define EL_CAT_APPEVENT                  ((WORD)0x00000002L)

//********************* MESSAGE SECTION ***********************
// Event IDs are 32-bit values
//
// MessageId: EL_MSG_ERROR
//
// MessageText:
//
//  %1
//
#define EL_MSG_ERROR                     ((DWORD)0x00000000L)

//
// MessageId: EL_MSG_APPSTART
//
// MessageText:
//
//  Application started.
//
#define EL_MSG_APPSTART                  ((DWORD)0x00000064L)

//
// MessageId: EL_MSG_APPSTOP
//
// MessageText:
//
//  Application stopped.
//
#define EL_MSG_APPSTOP                   ((DWORD)0x00000065L)

//
// MessageId: EL_MSG_ERRORCODE
//
// MessageText:
//
//  Application generated error code %1: "%%%1"
//
#define EL_MSG_ERRORCODE                 ((DWORD)0x00000066L)

//************************ END OF FILE ************************