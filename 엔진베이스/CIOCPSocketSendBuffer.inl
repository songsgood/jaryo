/******************************************************************************
	CIOCPSocketSendBuffer.inl
******************************************************************************/


inline BYTE CIOCPSocketSendBuffer::GetAllocatorIndex()
{
	return m_ucAllocatorIndex;
}


///////////////////////////////////////////////////////////////////////////////


inline void CIOCPSocketSendBuffer::SetNextPart()
{
	m_bFirstPart = true;
}


///////////////////////////////////////////////////////////////////////////////


inline bool CIOCPSocketSendBuffer::IsEncrypted()
{
	return m_bEncrypted;
}


inline void CIOCPSocketSendBuffer::SetEncrypted()
{
	m_bEncrypted = true;
}


///////////////////////////////// End of File /////////////////////////////////
