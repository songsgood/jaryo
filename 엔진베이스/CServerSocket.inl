/******************************************************************************
	CServerSocket.inl
******************************************************************************/


inline CServerSocket::CServerSocket(unsigned short usPort/* = 2002*/) :
	m_usPort(usPort)
{
}


///////////////////////////////////////////////////////////////////////////////


inline void CServerSocket::SetPort(unsigned short usPort)
{
	m_usPort = usPort;
}


inline unsigned short CServerSocket::GetPort()
{
	return m_usPort;
}


///////////////////////////////// End of File /////////////////////////////////
