//==============================================================================
//	CIOCPThreadManager2.inl
//==============================================================================


template <class IOCPThread>
inline CIOCPThreadManager2<IOCPThread>::CIOCPThreadManager2(CIOCP* pIOCP) :
	m_pIOCP(pIOCP),
	m_pFirstIOCPThread(NULL),
	m_uiIOCPThreadCount(0)
{
}


template <class IOCPThread>
inline CIOCPThreadManager2<IOCPThread>::~CIOCPThreadManager2()
{
	DestroyAllThread();
}


////////////////////////////////////////////////////////////////////////////////


template <class IOCPThread>
inline UINT CIOCPThreadManager2<IOCPThread>::AddIOCPThread(UINT uiAdditionalThreadCount/* = 1*/)
{
	UINT uiAllocatedThreadCount = 0;
	IOCPThread* pNewThread = NULL;

	for(uiAllocatedThreadCount = 0; uiAdditionalThreadCount > uiAllocatedThreadCount; uiAllocatedThreadCount++)
	{
		VERIFY(NULL != (pNewThread = new IOCPThread(m_pIOCP)));

		if(NULL == pNewThread)
		{
			break;
		}

		pNewThread->CreateThread();

		pNewThread->SetNext(m_pFirstIOCPThread);
		m_pFirstIOCPThread = pNewThread;
	}

	m_uiIOCPThreadCount += uiAllocatedThreadCount;

	return uiAllocatedThreadCount;
}


////////////////////////////////////////////////////////////////////////////////


template <class IOCPThread>
inline void CIOCPThreadManager2<IOCPThread>::DestroyAllThread()
{
	CIOCPThread* pThread = m_pFirstIOCPThread, * pNextThread = NULL;

	while(NULL != pThread)
	{
		pNextThread = pThread->GetNext();
		pThread->SendEndMessage();
		pThread = pNextThread;
	}

	pThread = m_pFirstIOCPThread;
	while(NULL != pThread)
	{
		pNextThread = pThread->GetNext();
		delete pThread;
		pThread = pNextThread;
	}

	m_uiIOCPThreadCount = 0;
	m_pFirstIOCPThread = NULL;
}


////////////////////////////////// End of File /////////////////////////////////
