/******************************************************************************
	CQueueBuffer.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CQueueBuffer.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CQueueBuffer::CQueueBuffer(PBYTE pucBuffer/* = NULL*/, UINT uiBufferSize/* = 0*/)
	: CBufferBase(pucBuffer, uiBufferSize), m_uiHead(0), m_uiTail(0)
{
}


///////////////////////////////////////////////////////////////////////////////


void CQueueBuffer::Empty()
{
	m_uiHead = 0;
	m_uiTail = 0;
}


///////////////////////////////////////////////////////////////////////////////


void CQueueBuffer::Add(PBYTE pData, UINT uiNewDataSize)
{
	::CopyMemory(GetTail(), pData, uiNewDataSize);
	DataAdded(uiNewDataSize);
}


void CQueueBuffer::Remove(PBYTE pBuffer, UINT uiBufferSize)
{
	::CopyMemory(pBuffer, GetHead(), uiBufferSize);
	DataRemoved(uiBufferSize);
}


///////////////////////////////////////////////////////////////////////////////


UINT CQueueBuffer::GetString(UINT uiOffset, TCHAR* pBuffer, UINT uiBufferSize)
{
	if(uiOffset > GetDataSize())
	{
		return 0;
	}

	if(NULL == ::lstrcpyn(pBuffer, (LPCTSTR) (GetHead() + uiOffset), ::GetMin(GetDataSize() - uiOffset + 1, uiBufferSize)))
	{
		return 0;
	}

	UINT uiStringLength = ::lstrlen(pBuffer);

	if(uiOffset + uiStringLength >= GetDataSize())
	{
		return uiStringLength;
	}

	if(TEXT('\0') == *(GetHead() + uiOffset + uiStringLength))
	{
		return uiStringLength + 1;
	}

	return uiStringLength;
}


///////////////////////////////////////////////////////////////////////////////


void CQueueBuffer::MoveDataToBegin()
{
	if(m_uiHead > 0)
	{
		if(FALSE != IsEmpty())
		{
			m_uiHead = 0;
			m_uiTail = 0;
		}
		else
		{
			if(FALSE != ::IsBadWritePtr(GetBuffer(), GetDataSize()))
			{
				VERIFY(!TEXT("FALSE != ::IsBadWritePtr(GetBuffer(), GetDataSize())"));
				m_uiHead = 0;
				m_uiTail = 0;
				return;
			}

			if(FALSE != ::IsBadReadPtr(GetHead(), GetDataSize()))
			{
				VERIFY(!TEXT("FALSE != ::IsBadReadPtr(GetHead(), GetDataSize())"));
				m_uiHead = 0;
				m_uiTail = 0;
				return;
			}

			::MoveMemory(GetBuffer(), GetHead(), GetDataSize());

			m_uiTail -= m_uiHead;
			m_uiHead = 0;
		}
	}
}


///////////////////////////////// End of File /////////////////////////////////
