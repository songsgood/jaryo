/******************************************************************************
	CDoublyLinkedList2.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CDoublyLinkedList2.h"


///////////////////////////////////////////////////////////////////////////////


DOUBLY_LIST_ENTRY_2::DOUBLY_LIST_ENTRY_2() :
	pPrevDoublyListEntry(NULL), pNextDoublyListEntry(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////
	
	
void CDoublyLinkedList2::Add(DOUBLY_LIST_ENTRY_2* pListEntry)
{
	if(NULL != m_FirstEntry.pNextDoublyListEntry)
	{
		m_FirstEntry.pNextDoublyListEntry->pPrevDoublyListEntry = pListEntry;
	}

	pListEntry->pNextDoublyListEntry = m_FirstEntry.pNextDoublyListEntry;

	m_FirstEntry.pNextDoublyListEntry = pListEntry;

	pListEntry->pPrevDoublyListEntry = &m_FirstEntry;
}


void CDoublyLinkedList2::Remove(DOUBLY_LIST_ENTRY_2* pListEntry)
{
	if(NULL != pListEntry->pNextDoublyListEntry)
	{
		pListEntry->pNextDoublyListEntry->pPrevDoublyListEntry = pListEntry->pPrevDoublyListEntry;
	}

	pListEntry->pPrevDoublyListEntry->pNextDoublyListEntry = pListEntry->pNextDoublyListEntry;
}


///////////////////////////////// End of File /////////////////////////////////
