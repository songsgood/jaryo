/******************************************************************************
	CIOCPThreadManager.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CIOCPThread;


///////////////////////////////////////////////////////////////////////////////


class CIOCPThreadManager  
{
public:
	CIOCPThreadManager(CIOCPThread* pIOCPThreadAllocator);
	virtual ~CIOCPThreadManager();

	UINT AddIOCPThread(UINT uiAdditionalThreadCount = 1);


protected:
	CIOCPThread* m_pIOCPThreadAllocator;
	CIOCPThread* m_pFirstIOCPThread;
	UINT m_uiIOCPThreadCount;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPThreadManager.inl"


///////////////////////////////// End of File /////////////////////////////////
