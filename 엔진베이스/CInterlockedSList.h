/******************************************************************************
	CInterlockedSList.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


struct INTERLOCKED_SLIST_ENTRY {
	INTERLOCKED_SLIST_ENTRY();
	INTERLOCKED_SLIST_ENTRY* pISLEntryNext;
};


template <class ObjectType>
union INTERLOCKED_SLIST_HEADER {
	INTERLOCKED_SLIST_HEADER();
    ULONGLONG Alignment;
    struct {
        ObjectType* pNext;
        WORD wDepth;
        WORD wSequence;
    };
};


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType>
class CInterlockedSList  
{
public:
	CInterlockedSList() { }
	virtual ~CInterlockedSList() { }

	void Push(ObjectType* pEntry);
	ObjectType* Pop();


private:
	INTERLOCKED_SLIST_HEADER<ObjectType> m_Header;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CInterlockedSList.inl"


//////////////////////////////// End of File //////////////////////////////////
