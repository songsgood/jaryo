/******************************************************************************
	CServiceStatus.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CServiceStatus.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CServiceStatus::CServiceStatus(LPCTSTR pServiceName, DWORD dwServiceType/* = SERVICE_WIN32_SHARE_PROCESS*/,
	DWORD dwControlsAccepted/* = SERVICE_ACCEPT_STOP*/) : m_hServiceStatus(0), m_bDebugMode(FALSE),
	m_pService(NULL)
{
	INIT_TCHAR_ARRAY(m_szServiceName);

	if(NULL != pServiceName)
	{
		LSTRCPYN(m_szServiceName, pServiceName, MAX_SERVICENAME_LENGTH + 1);
	}

	m_ServiceStatus.dwServiceType = dwServiceType;
	m_ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
	m_ServiceStatus.dwControlsAccepted = dwControlsAccepted;
	m_ServiceStatus.dwWin32ExitCode = NO_ERROR;
	m_ServiceStatus.dwServiceSpecificExitCode = 0;
	m_ServiceStatus.dwCheckPoint = 0;
	m_ServiceStatus.dwWaitHint = 0;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CServiceStatus::RegisterServiceCtrlHandlerEx(LPHANDLER_FUNCTION_EX lpHandlerProc)
{
	if(FALSE != m_bDebugMode)
	{
		return TRUE;
	}
	else
	{
		VERIFY_CHECKLASTERROR(0 != (m_hServiceStatus = ::RegisterServiceCtrlHandlerEx(m_szServiceName, lpHandlerProc,
			this)));
		return (0 != m_hServiceStatus);
	}
}


///////////////////////////////////////////////////////////////////////////////


BOOL CServiceStatus::SetServiceStatus()
{
	if(FALSE != m_bDebugMode)
	{
		return TRUE;
	}
	else
	{
		BOOL bRetVal = FALSE;
		VERIFY_CHECKLASTERROR(FALSE != (bRetVal = ::SetServiceStatus(m_hServiceStatus, &m_ServiceStatus)));
		return bRetVal;
	}
}


///////////////////////////////////////////////////////////////////////////////


BOOL CServiceStatus::BeginStateTransition(DWORD dwUltimateState, DWORD dwWaitHint/* = 5000*/)
{
	m_Gate.WaitToEnterGate();

	DWORD dwOriginalState = m_ServiceStatus.dwCurrentState;

	switch(dwUltimateState)
	{
	case SERVICE_PAUSED:
		m_ServiceStatus.dwCurrentState = SERVICE_PAUSE_PENDING;
		break;

	case SERVICE_RUNNING:
		m_ServiceStatus.dwCurrentState = (SERVICE_PAUSED == m_ServiceStatus.dwCurrentState) ? SERVICE_CONTINUE_PENDING : SERVICE_START_PENDING;
		break;

	case SERVICE_STOPPED:
		m_ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		break;

	default:
		VERIFY( (SERVICE_PAUSED == dwUltimateState) || (SERVICE_RUNNING == dwUltimateState) || (SERVICE_STOPPED == dwUltimateState) );
		m_Gate.LiftGate();
		return FALSE;
	}

	m_ServiceStatus.dwWin32ExitCode = NO_ERROR;
	m_ServiceStatus.dwServiceSpecificExitCode = 0;
	m_ServiceStatus.dwCheckPoint = 1;
	m_ServiceStatus.dwWaitHint = dwWaitHint;

	if(FALSE == SetServiceStatus())
	{
		m_ServiceStatus.dwCurrentState = dwOriginalState;
		m_Gate.LiftGate();
		return FALSE;
	}

	return TRUE;
}


BOOL CServiceStatus::AdvanceStateTransition(DWORD dwWaitHint, DWORD dwCheckPoint/* = 0*/)
{
	m_ServiceStatus.dwCheckPoint = (0 == dwCheckPoint) ? (m_ServiceStatus.dwCheckPoint + 1) : dwCheckPoint;
	m_ServiceStatus.dwWaitHint = dwWaitHint;

	m_ServiceStatus.dwWin32ExitCode = NO_ERROR;
	m_ServiceStatus.dwServiceSpecificExitCode = 0;

	return SetServiceStatus();
}


BOOL CServiceStatus::EndStateTransition()
{
	switch(m_ServiceStatus.dwCurrentState)
	{
	case SERVICE_PAUSE_PENDING:
		m_ServiceStatus.dwCurrentState = SERVICE_PAUSED;
		break;

	case SERVICE_CONTINUE_PENDING:
	case SERVICE_START_PENDING:
		m_ServiceStatus.dwCurrentState = SERVICE_RUNNING;
		break;

	case SERVICE_STOP_PENDING:
		m_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
		break;

	default:
		VERIFY( (SERVICE_PAUSE_PENDING == m_ServiceStatus.dwCurrentState) || (SERVICE_CONTINUE_PENDING == m_ServiceStatus.dwCurrentState) ||
			(SERVICE_START_PENDING == m_ServiceStatus.dwCurrentState) || (SERVICE_STOP_PENDING == m_ServiceStatus.dwCurrentState) );
		m_Gate.LiftGate();
		return FALSE;
	}

	m_ServiceStatus.dwWin32ExitCode = NO_ERROR;
	m_ServiceStatus.dwServiceSpecificExitCode = 0;
	m_ServiceStatus.dwCheckPoint = 0;
	m_ServiceStatus.dwWaitHint = 0;

	BOOL bRetVal = SetServiceStatus();
	m_Gate.LiftGate();
	return bRetVal;
}


///////////////////////////////////////////////////////////////////////////////


void CServiceStatus::StateTransitionToStop()
{
	switch(GetCurrentState())
	{
	case SERVICE_CONTINUE_PENDING:
	case SERVICE_PAUSE_PENDING:
	case SERVICE_START_PENDING:
		EndStateTransition();
		BeginStateTransition(SERVICE_STOPPED);
		EndStateTransition();
		break;

	case SERVICE_PAUSED:
	case SERVICE_RUNNING:
		BeginStateTransition(SERVICE_STOPPED);
		EndStateTransition();
		break;

	case SERVICE_STOP_PENDING:
		EndStateTransition();
		break;
	}
}


///////////////////////////////// End of File /////////////////////////////////
