/******************************************************************************
	CSocketServerAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CServiceStatusAllocator.h"


///////////////////////////////////////////////////////////////////////////////


class CSocketServerAllocator : public CServiceStatusAllocator  
{
public:
	CSocketServerAllocator() { }
	virtual ~CSocketServerAllocator() { }

	virtual CServiceStatus* AllocServiceStatus(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv);
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketServerAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
