/******************************************************************************
	CHashManager.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CHashManager.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CHashManager::CHashManager() :
	m_pHashObjectList(NULL),
	m_uiHashSize(0)
{
}


CHashManager::~CHashManager()
{
	if(NULL != m_pHashObjectList)
	{
		delete []m_pHashObjectList;
		m_pHashObjectList = NULL;
		m_uiHashSize = 0;
	}
}


///////////////////////////////////////////////////////////////////////////////


void CHashManager::Initialize(UINT uiHashSize)
{
	m_uiHashSize = uiHashSize;
	VERIFY(NULL != (m_pHashObjectList = new CDoublyLinkedList[uiHashSize]));
}


///////////////////////////////////////////////////////////////////////////////


CHashObject* CHashManager::GetFirst()
{
	CHashObject* pFoundObject = NULL;
	for(UINT uiHashIndex = 0; uiHashIndex < m_uiHashSize; uiHashIndex++)
	{
		if(NULL != (pFoundObject = GetPartFirst(uiHashIndex)))
		{
			return pFoundObject;
		}
	}
	return NULL;
}


CHashObject* CHashManager::GetNext(CHashObject* pObject)
{
	CHashObject* pFoundObject = GetPartNext(pObject);
	if(NULL != pFoundObject)
	{
		return pFoundObject;
	}
	for(UINT uiHashIndex = pObject->GetHashIndex() + 1; uiHashIndex < m_uiHashSize; uiHashIndex++)
	{
		if(NULL != (pFoundObject = GetPartFirst(uiHashIndex)))
		{
			return pFoundObject;
		}
	}
	return NULL;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CHashManager::IsEmpty()
{
	for(UINT uiHashIndex = 0; uiHashIndex < m_uiHashSize; uiHashIndex++)
	{
		if(FALSE == IsCellEmpty(uiHashIndex))
		{
			return FALSE;
		}
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////


void CHashManager::Remove(UINT uiHashIndex, CHashObject* pObject)
{
	if(FALSE != pObject->IsAdded())
	{
		ASSERT(uiHashIndex < GetHashSize());
		m_pHashObjectList[uiHashIndex].Remove(pObject);
	}
}


///////////////////////////////////////////////////////////////////////////////


void* CHashManager::GetPartFirstObject(UINT uiHashIndex)
{
	CHashObject* pFoundObject = GetPartFirst(uiHashIndex);
	return ((NULL == pFoundObject) ? (NULL) : (pFoundObject->GetOwner()));
}


void* CHashManager::GetPartNextObject(CHashObject* pObject)
{
	CHashObject* pFoundObject = GetPartNext(pObject);
	return ((NULL == pFoundObject) ? (NULL) : (pFoundObject->GetOwner()));
}


///////////////////////////////////////////////////////////////////////////////


void* CHashManager::GetFirstObject()
{
	CHashObject* pFoundObject = GetFirst();
	return ((NULL == pFoundObject) ? (NULL) : (pFoundObject->GetOwner()));
}


void* CHashManager::GetNextObject(CHashObject* pObject)
{
	CHashObject* pFoundObject = GetNext(pObject);
	return ((NULL == pFoundObject) ? (NULL) : (pFoundObject->GetOwner()));
}


///////////////////////////////////////////////////////////////////////////////


void CHashManager::Empty()
{
	for(UINT uiHashIndex = 0; uiHashIndex < m_uiHashSize; uiHashIndex++)
	{
		m_pHashObjectList[uiHashIndex].Empty();
	}
}


///////////////////////////////// End of File /////////////////////////////////
