/******************************************************************************
	CService.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude <Winsvc.h>


///////////////////////////////////////////////////////////////////////////////


class CServiceStatus;
class CServer;


///////////////////////////////////////////////////////////////////////////////


class CService  
{
public:
	CService();
	virtual ~CService() { }

	BOOL StartServiceCtrlDispatcher(LPSERVICE_TABLE_ENTRY lpServiceTable = NULL, BOOL bDebugMode = FALSE, DWORD dwArgc = 0,
		 LPTSTR* pszArgv = NULL);

	static void SetGlobalParameter(int iGlobalArgc, LPTSTR* ppGlobalArgv);

	// 매개변수 사용을 위해 추가
	static void GetGlobalParameter(int & globalArgc, LPTSTR* & globalArgv)
	{
		globalArgc = m_iGlobalArgc;
		globalArgv = m_ppGlobalArgv;
	}

	static void WINAPI ServiceMain(DWORD dwArgc, LPTSTR* pszArgv);


protected:
	virtual CServer* CreateServer(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv);

	void IServiceMain(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv, BOOL bDebugMode = FALSE);
	BOOL Main(CServer* pServer, CServiceStatus* pServiceStatus);

	static DWORD WINAPI HandlerEx(DWORD dwControl, DWORD dwEventType, LPVOID pEventData, LPVOID pContext);
	DWORD IHandlerEx(DWORD dwControl, DWORD dwEventType, LPVOID pEventData, LPVOID pContext);

	virtual DWORD OnContinue(CServiceStatus* pServiceStatus);
	virtual DWORD OnInterrogate(CServiceStatus* pServiceStatus);
	virtual DWORD OnNetBindAdd(CServiceStatus* pServiceStatus);
	virtual DWORD OnNetBindDisable(CServiceStatus* pServiceStatus);
	virtual DWORD OnNetBindEnable(CServiceStatus* pServiceStatus);
	virtual DWORD OnNetBindRemove(CServiceStatus* pServiceStatus);
	virtual DWORD OnParamChange(CServiceStatus* pServiceStatus);
	virtual DWORD OnPause(CServiceStatus* pServiceStatus);
	virtual DWORD OnShutdown(CServiceStatus* pServiceStatus);
	virtual DWORD OnStop(CServiceStatus* pServiceStatus);
	virtual DWORD OnDeviceEvent(CServiceStatus* pServiceStatus, DWORD dwEventType, LPVOID lpEventData);
	virtual DWORD OnHardwareProfileChange(CServiceStatus* pServiceStatus, DWORD dwEventType);
	virtual DWORD OnPowerEvent(CServiceStatus* pServiceStatus, DWORD dwEventType, LPVOID lpEventData);
	virtual DWORD OnCustomHandlerEx(CServiceStatus* pServiceStatus, DWORD dwControl);

	BOOL Run(CServiceStatus* pServiceStatus, CServer* pServer);


protected:
	static CService* m_pThis;

	static int m_iGlobalArgc;
	static LPTSTR* m_ppGlobalArgv;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CService.inl"


///////////////////////////////// End of File /////////////////////////////////
