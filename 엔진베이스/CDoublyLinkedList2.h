/******************************************************************************
	CDoublyLinkedList2.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


struct DOUBLY_LIST_ENTRY_2
{
	DOUBLY_LIST_ENTRY_2();

	DOUBLY_LIST_ENTRY_2* pPrevDoublyListEntry, * pNextDoublyListEntry;
};


///////////////////////////////////////////////////////////////////////////////


class CDoublyLinkedList2  
{
public:
	CDoublyLinkedList2() { }
	virtual ~CDoublyLinkedList2() { }


public:
	void Add(DOUBLY_LIST_ENTRY_2* pListEntry);
	void Remove(DOUBLY_LIST_ENTRY_2* pListEntry);

	DOUBLY_LIST_ENTRY_2* GetFirst();
	DOUBLY_LIST_ENTRY_2* GetNext(DOUBLY_LIST_ENTRY_2* pListEntry);

	bool IsEmpty();


private:
	DOUBLY_LIST_ENTRY_2 m_FirstEntry;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CDoublyLinkedList2.inl"


///////////////////////////////// End of File /////////////////////////////////
