//==============================================================================
//	CClientPoolIOCPSocketClient.h
//==============================================================================


qpragma once


////////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketClient.h"
qinclude "CISLAllocatorEntry.h"


////////////////////////////////////////////////////////////////////////////////


class CClientPoolIOCPSocketClient : public CIOCPSocketClient, public CISLAllocatorEntry  
{
public:
	CClientPoolIOCPSocketClient() { }
	virtual ~CClientPoolIOCPSocketClient() { }

	virtual void InitializeInstance();
	virtual void CleanupInstance();
};


////////////////////////////////////////////////////////////////////////////////


qinclude "CClientPoolIOCPSocketClient.inl"


////////////////////////////////// End of File /////////////////////////////////
