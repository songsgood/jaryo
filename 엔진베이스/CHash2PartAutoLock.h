/******************************************************************************
	CHash2PartAutoLock.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CHash2PartLock.h"


///////////////////////////////////////////////////////////////////////////////


class CHash2PartAutoLock  
{
public:
	CHash2PartAutoLock(CHash2PartLock* pHashPartLock, CHash2::HASH_INDEX HashIndex, bool bInitialLock = false);
	virtual ~CHash2PartAutoLock();


public:
	void Lock();
	void Unlock();

	bool IsLocked();


private:
	CHash2PartLock* m_pHashPartLock;
	CHash2::HASH_INDEX m_HashIndex;

	bool m_bLocked;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CHash2PartAutoLock.inl"


///////////////////////////////// End of File /////////////////////////////////
