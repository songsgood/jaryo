/******************************************************************************
	Debug.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


#ifdef ASSERT
#undef ASSERT
#endif	// ASSERT


#ifdef VERIFY
#undef VERIFY
#endif	// VERIFY


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#define DbgBreak()
//#define DbgBreak	_CrtDbgBreak
//#define DbgBreak()
#endif	// _DEBUG


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#define ASSERT(x) \
	if(FALSE == (x)) { \
		::AssertFailed(TEXT(__FILE__), __LINE__, TEXT(#x)); \
		DbgBreak(); \
	}
#else	// _DEBUG
#define ASSERT(x)
#endif	// _DEBUG


#ifdef _DEBUG
#define VERIFY(x)	ASSERT(x)
#else	// _DEBUG
#define VERIFY(x) \
	if(FALSE == (x)) { \
		::AssertFailed(TEXT(__FILE__), __LINE__, TEXT(#x)); \
	}
#endif	// _DEBUG


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#define ASSERTFAILED(x, ErrorCode) \
	{ \
		HLOCAL hAssertErrorMessageBuffer = NULL; \
		GetErrorMessageString(&hAssertErrorMessageBuffer, (ErrorCode)); \
		if(NULL == hAssertErrorMessageBuffer) { \
			AssertFailed(TEXT(__FILE__), __LINE__, (x), (ErrorCode)); \
		} \
		else { \
			AssertFailed(TEXT(__FILE__), __LINE__, (x), (ErrorCode), (LPCTSTR) LocalLock(hAssertErrorMessageBuffer)); \
			LocalFree(hAssertErrorMessageBuffer); \
		} \
		DbgBreak(); \
	}
#else	// _DEBUG
#define ASSERTFAILED(x, ErrorCode) \
	{ \
		HLOCAL hAssertErrorMessageBuffer = NULL; \
		GetErrorMessageString(&hAssertErrorMessageBuffer, (ErrorCode)); \
		if(NULL == hAssertErrorMessageBuffer) { \
			AssertFailed(TEXT(__FILE__), __LINE__, (x), (ErrorCode)); \
		} \
		else { \
			AssertFailed(TEXT(__FILE__), __LINE__, (x), (ErrorCode), (LPCTSTR) LocalLock(hAssertErrorMessageBuffer)); \
			LocalFree(hAssertErrorMessageBuffer); \
		} \
	}
#endif	// _DEBUG


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#define ASSERT_CHECKLASTERROR(x) \
	if(FALSE == (x)) { \
		ASSERTFAILED(TEXT(#x), GetLastError()); \
	}
#else	// _DEBUG
#define ASSERT_CHECKLASTERROR(x)
#endif	// _DEBUG


#ifdef _DEBUG
#define VERIFY_CHECKLASTERROR(x) ASSERT_CHECKLASTERROR(x)
#else	// _DEBUG
#define VERIFY_CHECKLASTERROR(x) \
	if(FALSE == (x)) { \
		ASSERTFAILED(TEXT(#x), GetLastError()); \
	}
#endif	// _DEBUG


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#define ASSERT_CHECKWSALASTERROR(x) \
	if(FALSE == (x)) { \
		ASSERTFAILED(TEXT(#x), WSAGetLastError()); \
	}
#else	// _DEBUG
#define ASSERT_CHECKWSALASTERROR(x)
#endif	// _DEBUG


#ifdef _DEBUG
#define VERIFY_CHECKWSALASTERROR(x) ASSERT_CHECKWSALASTERROR(x)
#else	// _DEBUG
#define VERIFY_CHECKWSALASTERROR(x) \
	if(FALSE == (x)) { \
		ASSERTFAILED(TEXT(#x), WSAGetLastError()); \
	}
#endif	// _DEBUG


///////////////////////////////////////////////////////////////////////////////


#define FILELOG { g_LogManager.SetFileLogHeader("File �4, Line 0", TEXT(__FILE__), __LINE__); }


#define EVENTLOG { g_LogManager.SetEventLogHeader("File �4, Line 0", TEXT(__FILE__), __LINE__); }


#define LOG { g_LogManager.SetLogHeader("File �4, Line 0", TEXT(__FILE__), __LINE__); }


#define WRITE_LOG	g_LogManager.WriteLogToFile


///////////////////////////////////////////////////////////////////////////////


void WriteOutLogMessage(LPCTSTR pErrorMessage);


void AssertFailed(LPCTSTR pErrorMessage);


void AssertFailed(LPCTSTR lpFileName, int iLineNumber, LPCTSTR lpExpression, DWORD dwErrorCode = ERROR_SUCCESS, LPCTSTR lpErrorMessage = NULL);


void GetErrorMessageString(HLOCAL* phMessageBuffer, DWORD dwErrorCode);


///////////////////////////////////////////////////////////////////////////////


#define VERIFYREADPTR(Pointer, Type)	VERIFY(FALSE == ::IsBadReadPtr((Pointer), sizeof(Type));


///////////////////////////////////////////////////////////////////////////////


#ifdef _HEAVY_READPTRCHECK
#define HEAVY_VERIFREADPTR(Pointer, Type)	VERIFYREADPTR((Pointer), (Type))
#else	// _HEAVY_READPTRCHECK
#define HEAVY_VERIFREADPTR(Pointer, Type)
#endif	// _HEAVY_READPTRCHECK


///////////////////////////////// End of File /////////////////////////////////
