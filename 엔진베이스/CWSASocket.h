/******************************************************************************
	CWSASocket.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude <Winsock2.h>


///////////////////////////////////////////////////////////////////////////////


class CWSASocket  
{
public:
	CWSASocket();
	virtual ~CWSASocket();

	SOCKET GetHandle();

	BOOL IsValidSocket();

	BOOL Create(int af = AF_INET, int type = SOCK_STREAM, int protocol = IPPROTO_IP,
		LPWSAPROTOCOL_INFO lpProtocolInfo = NULL, GROUP g = 0, DWORD dwFlags = WSA_FLAG_OVERLAPPED);

	BOOL ShutDown(int how = SD_BOTH);
	BOOL CloseSocket();

	BOOL Bind(unsigned short usSocketPort);
	BOOL Listen(int iBackLog = SOMAXCONN);

	BOOL Connect(const char* pAddress, unsigned short usPort);

	BOOL AcceptEx(SOCKET sListenSocket, SOCKET sAcceptSocket, PVOID lpOutputBuffer, DWORD dwReceiveDataLength, DWORD dwLocalAddressLength,
		DWORD dwRemoteAddressLength, LPDWORD lpdwBytesReceived, LPOVERLAPPED lpOverlapped);
	BOOL WSASend(LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesSent, DWORD dwFlags,
		LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine);
	BOOL WSARecv(LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesRecvd, LPDWORD lpFlags,
		LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine);

	BOOL EventSelect(WSAEVENT hEventObject, long lNetworkEvents);


protected:
	static unsigned long InetAddr(const char* pAddress);


protected:
	SOCKET m_hSocket;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CWSASocket.inl"


///////////////////////////////// End of File /////////////////////////////////
