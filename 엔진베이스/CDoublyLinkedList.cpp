/******************************************************************************
	CDoublyLinkedList.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CDoublyLinkedList.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


void CDoublyLinkedList::Add(CDoublyListEntry* pListEntry)
{
	if(FALSE != ::IsBadReadPtr(pListEntry, sizeof(CDoublyListEntry)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pListEntry, sizeof(CDoublyListEntry))"));
		return;
	}

	pListEntry->SetNext(m_FirstEntry.GetNext());
	pListEntry->SetPrev(&m_FirstEntry);
	if(NULL != m_FirstEntry.GetNext())
	{
		if(FALSE != ::IsBadReadPtr(m_FirstEntry.GetNext(), sizeof(CDoublyListEntry)))
		{
			VERIFY(!TEXT("FALSE != ::IsBadReadPtr(m_FirstEntry.GetNext(), sizeof(CDoublyListEntry))"));
			m_FirstEntry.SetNext(NULL);
		}
		else
		{
			m_FirstEntry.GetNext()->SetPrev(pListEntry);
		}
	}
	m_FirstEntry.SetNext(pListEntry);
}


void CDoublyLinkedList::Remove(CDoublyListEntry* pListEntry)
{
	if(FALSE != ::IsBadReadPtr(pListEntry, sizeof(CDoublyListEntry)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pListEntry, sizeof(CDoublyListEntry))"));
		return;
	}

	if(FALSE == pListEntry->IsAdded())
	{
		return;
	}

	if(NULL != pListEntry->GetNext())
	{
		pListEntry->GetNext()->SetPrev(pListEntry->GetPrev());
	}
	pListEntry->GetPrev()->SetNext(pListEntry->GetNext());
	pListEntry->SetNext(NULL);
	pListEntry->SetPrev(NULL);
}


///////////////////////////////////////////////////////////////////////////////


CDoublyListEntry* CDoublyLinkedList::GetFirst()
{
	CDoublyListEntry* pEntry = m_FirstEntry.GetNext();
	if(NULL != pEntry)
	{
		if(FALSE != ::IsBadReadPtr(pEntry, sizeof(CDoublyListEntry)))
		{
			VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pEntry, sizeof(CDoublyListEntry))"));
			m_FirstEntry.SetNext(NULL);
			return NULL;
		}
	}
	return pEntry;
}


CDoublyListEntry* CDoublyLinkedList::GetNext(CDoublyListEntry* pListEntry)
{
	CDoublyListEntry* pEntry = pListEntry->GetNext();
	if(NULL != pEntry)
	{
		if(FALSE != ::IsBadReadPtr(pEntry, sizeof(CDoublyListEntry)))
		{
			VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pEntry, sizeof(CDoublyListEntry))"));
			pListEntry->SetNext(NULL);
			return NULL;
		}
	}
	return pEntry;
}


///////////////////////////////// End of File /////////////////////////////////
