/******************************************************************************
	CSystemInfo.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CSystemInfo  
{
public:
	CSystemInfo() { }
	/*virtual */~CSystemInfo() { }


public:
	static void UpdateSystemInfo(SYSTEM_INFO* pSystemInfo);

	static DWORD GetNumberOfProcessors();
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSystemInfo.inl"


///////////////////////////////// End of File /////////////////////////////////
