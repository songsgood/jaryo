/******************************************************************************
	CDLinkedListEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CDLinkedListEntry  
{
public:
	CDLinkedListEntry();
	virtual ~CDLinkedListEntry() { }

	void InitializeInstance();
	void SetRemoved();
	BOOL IsAdded();

	void SetOwner(void* pOwner);
	void* GetOwner();

	void SetNext(CDLinkedListEntry* pNext);
	CDLinkedListEntry* GetNext();
	void SetPrev(CDLinkedListEntry* pPrev);
	CDLinkedListEntry* GetPrev();


private:
	void* m_pOwner;
	CDLinkedListEntry* m_pNext, * m_pPrev;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CDLinkedListEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
