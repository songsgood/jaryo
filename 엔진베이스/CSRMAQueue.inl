/******************************************************************************
	CSRMAQueue.inl
	Single Remover (& Reader) / Multiple Adder Queue
******************************************************************************/


inline SRMA_QUEUE_ENTRY::SRMA_QUEUE_ENTRY() : pSRMAQEntryNext(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


inline CSRMAQueue::CSRMAQueue() : m_pTail(&m_Head)
{
	m_Head.pSRMAQEntryNext = &m_Head;
}


///////////////////////////////////////////////////////////////////////////////


inline void CSRMAQueue::AddTail(PSRMA_QUEUE_ENTRY pEntry)
{
	AddTail(pEntry, pEntry);
}


///////////////////////////////////////////////////////////////////////////////


inline PSRMA_QUEUE_ENTRY CSRMAQueue::GetFirstEntry()
{
	return ((&m_Head == m_Head.pSRMAQEntryNext) ? (NULL) : (m_Head.pSRMAQEntryNext));
}


inline PSRMA_QUEUE_ENTRY CSRMAQueue::GetNextEntry(PSRMA_QUEUE_ENTRY pEntry)
{
	return ((&m_Head == pEntry->pSRMAQEntryNext) ? (NULL) : (pEntry->pSRMAQEntryNext));
}


///////////////////////////////// End of File /////////////////////////////////
