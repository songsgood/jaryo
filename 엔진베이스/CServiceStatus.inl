/******************************************************************************
	CServiceStatus.inl
******************************************************************************/


inline CServiceStatus::~CServiceStatus()
{
	m_Gate.WaitToEnterGate();
}


///////////////////////////////////////////////////////////////////////////////


inline CServiceStatus* CServiceStatus::AllocSingleEntry(LPCTSTR szServiceName)
{
	return CServiceStatus::Allocate(szServiceName);
}


inline CServiceStatus* CServiceStatus::Allocate(LPCTSTR szServiceName)
{
	return (new CServiceStatus(szServiceName));
}


///////////////////////////////////////////////////////////////////////////////


inline DWORD CServiceStatus::GetCurrentState()
{
	return (m_ServiceStatus.dwCurrentState);
}


///////////////////////////////////////////////////////////////////////////////


inline LPCTSTR CServiceStatus::GetServiceName()
{
	return m_szServiceName;
}


///////////////////////////////////////////////////////////////////////////////


inline CServiceSync* CServiceStatus::GetServiceSync()
{
	return &m_ServiceSync;
}


///////////////////////////////////////////////////////////////////////////////


inline void CServiceStatus::SetDebugMode(BOOL bDebugMode)
{
	m_bDebugMode = bDebugMode;
}


///////////////////////////////////////////////////////////////////////////////


inline void CServiceStatus::SetService(CService* pService)
{
	m_pService = pService;
}


inline CService* CServiceStatus::GetService()
{
	return m_pService;
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CServiceStatus::ProcessCommandLine(int/* iGlobalArgc*/, LPTSTR* /*ppGlobalArgv*/, DWORD /*dwArgc*/,
	LPTSTR* /*pszArgv*/)
{
	return TRUE;
}


inline BOOL CServiceStatus::OnInitService()
{
	return TRUE;
}


///////////////////////////////// End of File /////////////////////////////////
