/******************************************************************************
	CQueueBuffer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CBufferBase.h"


///////////////////////////////////////////////////////////////////////////////


class CQueueBuffer : public CBufferBase  
{
public:
	CQueueBuffer(PBYTE pucBuffer = NULL, UINT uiBufferSize = 0);
	virtual ~CQueueBuffer() { }

	PBYTE GetHead();
	PBYTE GetTail();

	UINT GetDataSize();
	UINT GetFreeSize();

	BOOL IsEmpty();
	void Empty();

	void DataAdded(UINT uiAddedDataSize);
	void DataRemoved(UINT uiRemovedDataSize);

	void Add(PBYTE pData, UINT uiNewDataSize);
	template <class DATA_TYPE> void Add(DATA_TYPE* pData)
	{
		*((DATA_TYPE*) GetTail()) = *pData;
		DataAdded(sizeof(DATA_TYPE));
	}

	void Remove(PBYTE pBuffer, UINT uiBufferSize);
	template <class DATA_TYPE> void Remove(DATA_TYPE* pBuffer)
	{
		*pBuffer = *((DATA_TYPE*) GetHead());
		DataRemoved(sizeof(DATA_TYPE));
	}

	void Set(UINT uiOffset, PBYTE pData, UINT uiDataSize);
	template <class DATA_TYPE> void Set(UINT uiOffset, DATA_TYPE* pData)
	{
		*((DATA_TYPE*) (GetHead() + uiOffset)) = *pData;
	}

	void Get(UINT uiOffset, PBYTE pBuffer, UINT uiBufferSize);
	template <class DATA_TYPE> void Get(UINT uiOffset, DATA_TYPE* pData)
	{
		if( (m_uiHead + uiOffset) <= GetBufferSize() )
			*pData = *((DATA_TYPE*) (GetHead() + uiOffset));
	}
	UINT GetString(UINT uiOffset, TCHAR* pBuffer, UINT uiBufferSize);

	void MoveDataToBegin();


protected:
	UINT m_uiHead, m_uiTail;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CQueueBuffer.inl"


///////////////////////////////// End of File /////////////////////////////////
