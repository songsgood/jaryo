/******************************************************************************
	CDoublyListEntry.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CDoublyListEntry  
{
public:
	CDoublyListEntry();
	virtual ~CDoublyListEntry() { }

	void InitializeInstance();

	void SetPrev(CDoublyListEntry* pPrev);
	CDoublyListEntry* GetPrev();
	void SetNext(CDoublyListEntry* pNext);
	CDoublyListEntry* GetNext();

	BOOL IsAdded();


protected:
	CDoublyListEntry* m_pPrev;
	CDoublyListEntry* m_pNext;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CDoublyListEntry.inl"


///////////////////////////////// End of File /////////////////////////////////
