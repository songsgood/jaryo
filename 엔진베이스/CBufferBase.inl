/******************************************************************************
	CBufferBase.inl
******************************************************************************/


#pragma once


///////////////////////////////////////////////////////////////////////////////


inline BYTE* CBufferBase::GetBuffer()
{
	return m_pucBuffer;
}


inline UINT CBufferBase::GetBufferSize()
{
	return m_uiBufferSize;
}


///////////////////////////////////////////////////////////////////////////////


inline CBufferBase::operator BYTE*()
{
	return m_pucBuffer;
}


inline CBufferBase::operator VOID*()
{
	return ((VOID*) m_pucBuffer);
}


///////////////////////////////////////////////////////////////////////////////


inline BYTE& CBufferBase::operator [](UINT uiIndex)
{
	return m_pucBuffer[uiIndex];
}


///////////////////////////////////////////////////////////////////////////////


inline void CBufferBase::FillZeroMemory()
{
	::ZeroMemory(m_pucBuffer, m_uiBufferSize);
}


///////////////////////////////// End of File /////////////////////////////////
