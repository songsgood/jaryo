/******************************************************************************
	CLogManager.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CLogManager.h"


//////////////////////////////////////////////////////////////////////////////


CLogManager g_LogManager;


//////////////////////////////////////////////////////////////////////////////


CLogManager::CLogManager(LPCTSTR lpAppName)
{
	m_EventLog.SetAppName(lpAppName);
	m_FileLog.SetAppName(lpAppName);
	m_EventLog.Install();
}


//////////////////////////////////////////////////////////////////////////////


void CLogManager::SetAppName(LPCTSTR lpAppName)
{
	m_EventLog.SetAppName(lpAppName);
	m_FileLog.SetAppName(lpAppName);
	m_EventLog.Install();
}


//////////////////////////////////////////////////////////////////////////////


BOOL CLogManager::WriteLogToFile(LPCTSTR lpFormat, ...)
{
	va_list argList;
	BOOL bRet;

	DECLARE_INIT_TCHAR_ARRAY(szLogString, 1025);

	va_start(argList, lpFormat);
	wvsprintf(szLogString, lpFormat, argList);
	bRet = m_FileLog.WriteLog(szLogString);

	va_end(argList);
	return (bRet);
}

BOOL CLogManager::WriteLogToFileNew(LPCTSTR lpLogType, LPCTSTR lpLogAction, LPCTSTR lpLogActionResult, LPCTSTR szLogString)
{
	return m_FileLog.WriteLog(lpLogType,lpLogAction,lpLogActionResult,szLogString);
}

BOOL CLogManager::WriteLogToFileHour(LPCTSTR lpLogType, LPCTSTR lpLogAction, LPCTSTR lpLogActionResult, LPCTSTR szLogString)
{
	return m_FileLog.WriteLogHour(lpLogType,lpLogAction,lpLogActionResult,szLogString);
}

//////////////////////////////////////////////////////////////////////////////


BOOL CLogManager::ReportEvent(LPCTSTR lpFormat, ...)
{
	va_list argList;
	BOOL bRet;

	DECLARE_INIT_TCHAR_ARRAY(szLogString, 1025);

	va_start(argList, lpFormat);
	wvsprintf(szLogString, lpFormat, argList);
	bRet = m_EventLog.ReportEvent(szLogString);

	va_end(argList);
	return (bRet);
}


BOOL CLogManager::ReportEvent(WORD wType, LPCTSTR lpFormat, ...)
{
	va_list argList;
	BOOL bRet;

	DECLARE_INIT_TCHAR_ARRAY(szLogString, 1025);

	va_start(argList, lpFormat);
	wvsprintf(szLogString, lpFormat, argList);
	bRet = m_EventLog.ReportEvent(szLogString, wType);

	va_end(argList);
	return (bRet);
}


//////////////////////////////////////////////////////////////////////////////


BOOL CLogManager::WriteLog(WORD wType, LPCTSTR lpFormat, ...)
{
	va_list argList;
	BOOL bRet;

	DECLARE_INIT_TCHAR_ARRAY(szLogString, 1025);

	va_start(argList, lpFormat);
	wvsprintf(szLogString, lpFormat, argList);
	bRet = (m_FileLog.WriteLog(szLogString) && m_EventLog.ReportEvent(szLogString, wType));

	va_end(argList);
	return (bRet);
}


BOOL CLogManager::WriteLog(LPCTSTR lpFormat, ...)
{
	va_list argList;
	BOOL bRet;

	DECLARE_INIT_TCHAR_ARRAY(szLogString, 1025);

	va_start(argList, lpFormat);
	wvsprintf(szLogString, lpFormat, argList);
	bRet = (m_FileLog.WriteLog(szLogString) && m_EventLog.ReportEvent(szLogString));

	va_end(argList);
	return (bRet);
}


//////////////////////////////////////////////////////////////////////////////