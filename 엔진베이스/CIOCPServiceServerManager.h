//==============================================================================
//	CIOCPServiceServerManager.h
//==============================================================================


qpragma once


////////////////////////////////////////////////////////////////////////////////


qinclude "CIOCP.h"
qinclude "CIOCPService.h"
qinclude "CIOCPThreadManager2.h"


////////////////////////////////////////////////////////////////////////////////


class CIOCPThread;


////////////////////////////////////////////////////////////////////////////////

template <class IOCPService, class IOCPThread>
class CIOCPServiceServerManager  
{
public:
	CIOCPServiceServerManager();
	virtual ~CIOCPServiceServerManager();


public:
//	bool Initialize(TCHAR* pTeamName, TCHAR* pProjectName, UINT uiThreadCount = 0, UINT* puiSendBufferSize = NULL, UINT* puiSendBufferPreAllocSize = NULL,
//		UINT uiSendBufferAllocatorCount = 0);
	bool Initialize(AUTH_TYPE eAuthType = AUTH_TYPE_RELEASE, UINT uiThreadCount = 0, UINT* puiSendBufferSize = NULL, UINT* puiSendBufferPreAllocSize = NULL,
		UINT uiSendBufferAllocatorCount = 0);
	bool Start(LPSERVICE_TABLE_ENTRY lpServiceTable = NULL, BOOL bDebugMode = FALSE, DWORD dwArgc = 0,
		LPTSTR* pszArgv = NULL);


private:
	CIOCP m_IOCP;
	IOCPService m_IOCPService;
	CIOCPThreadManager2<IOCPThread> m_IOCPThreadManager;
};


////////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPServiceServerManager.inl"


////////////////////////////////// End of File /////////////////////////////////
