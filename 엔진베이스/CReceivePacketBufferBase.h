/******************************************************************************
	CReceivePacketBufferBase.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CQueueBuffer.h"
// 2010.01.11
// Modify for Migration 2005
// 불필요 할것 같은데...
// ktKim
// Temp
//qpragma warning (push, 3)
//qinclude <string>
//qpragma warning (pop)
// End


///////////////////////////////////////////////////////////////////////////////


class CReceivePacketBufferBase : public CQueueBuffer  
{
public:
	CReceivePacketBufferBase(PBYTE pucBuffer, UINT uiBufferSize);
	//CReceivePacketBufferBase() {}
	virtual ~CReceivePacketBufferBase() { }

	void Clear();

	BOOL GetPacket(CQueueBuffer* pReceivedBuffer);

	unsigned short GetCommand();
	void SetCommand(unsigned short usCommand);
	UINT GetSequenceNumber();
	void SetSequenceNumber(UINT uiSequenceNumber);
	
	void SeekToBegin();
	// ktKim
	// Temp
//	void Read(std::string & strData);
	void Read(PBYTE pBuffer, UINT uiDataSize);
	//template <class DATA_TYPE> void Read(DATA_TYPE & pData)
	//{
	//	Get(m_uiCurrentReadPosition, &pData);
	//	m_uiCurrentReadPosition += sizeof(DATA_TYPE);
	//}
	template <class DATA_TYPE> void Read(DATA_TYPE* pData)
	{
		Get(m_uiCurrentReadPosition, pData);
		m_uiCurrentReadPosition += sizeof(DATA_TYPE);
	}
	void ReadNTString(TCHAR* pBuffer, UINT uiBufferSize);
	template <class DATA_TYPE> CReceivePacketBufferBase& operator>>(DATA_TYPE& Data)
	{
		Read(&Data);
		return *this;
	}

	PBYTE GetRemainedData();
	unsigned long GetRemainedDataSize();

	void Skip(UINT uiDataSize);

protected:
	unsigned short m_usCommand;
	UINT m_uiSequenceNumber;
	UINT m_uiCurrentReadPosition;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CReceivePacketBufferBase.inl"


///////////////////////////////// End of File /////////////////////////////////
