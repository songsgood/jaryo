/******************************************************************************
	CDoublyLinkedList2.inl
******************************************************************************/


inline DOUBLY_LIST_ENTRY_2* CDoublyLinkedList2::GetFirst()
{
	return m_FirstEntry.pNextDoublyListEntry;
}


inline DOUBLY_LIST_ENTRY_2* CDoublyLinkedList2::GetNext(DOUBLY_LIST_ENTRY_2* pListEntry)
{
	return pListEntry->pNextDoublyListEntry;
}


///////////////////////////////////////////////////////////////////////////////


inline bool CDoublyLinkedList2::IsEmpty()
{
	return (NULL == GetFirst());
}


///////////////////////////////// End of File /////////////////////////////////
