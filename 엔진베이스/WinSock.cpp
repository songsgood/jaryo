/******************************************************************************
	WinSock.cpp
******************************************************************************/


qinclude "stdafx.h"

qinclude <Winsock2.h>

qinclude "WinSock.h"
qinclude "MessageBox.h"
qinclude "Debug.h"


///////////////////////////////////////////////////////////////////////////////


BOOL InitializeWinSock()
{
	WSADATA WsaData = {0};
	int iWSAStartupRetVal = 0;

	VERIFY(0 == (iWSAStartupRetVal = ::WSAStartup(MAKEWORD(2, 2), &WsaData)));

	switch(iWSAStartupRetVal) {
	case 0:	// Success
		break;

	case WSASYSNOTREADY:
		::ShowMessageBox(TEXT("WinSock Startup Error (WSASYSNOTREADY)"));
		break;

	case WSAVERNOTSUPPORTED:
		::ShowMessageBox(TEXT("WinSock Startup Error (WSAVERNOTSUPPORTED)"));
		break;

	case WSAEINPROGRESS:
		::ShowMessageBox(TEXT("WinSock Startup Error (WSAEINPROGRESS)"));
		break;

	case WSAEPROCLIM:
		::ShowMessageBox(TEXT("WinSock Startup Error (WSAEPROCLIM)"));
		break;

	case WSAEFAULT:
		::ShowMessageBox(TEXT("WinSock Startup Error (WSAEFAULT)"));
		break;

	default:
		::ShowMessageBox(TEXT("WinSock Startup Error (10752790)"), iWSAStartupRetVal);
k;
	}

	return (0 == iWSAStartupRetVal);
}


BOOL CleanupWinSock()
{
	int iWSACleanupRetVal = 0;
	VERIFY_CHECKWSALASTERROR(0 == (iWSACleanupRetVal = ::WSACleanup()));

	if(0 != iWSACleanupRetVal) {
		::ShowMessageBox(TEXT("WinSock Cleanup Error"));
	}

	return (0 == iWSACleanupRetVal);
}


///////////////////////////////// End of File /////////////////////////////////
