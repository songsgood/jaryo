/******************************************************************************
	CHash2PartLock.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CHash2PartLock.h"


///////////////////////////////////////////////////////////////////////////////


CHash2PartLock::~CHash2PartLock()
{
	if(NULL != m_pCriticalSection)
	{
		delete []m_pCriticalSection;
		m_pCriticalSection = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


void CHash2PartLock::Initialize(CHash2::HASH_SIZE HashSize)
{
	Allocate(HashSize);

	for(CHash2::HASH_SIZE i = 0; i < HashSize; i++)
	{
		::InitializeCriticalSection(&(m_pCriticalSection[i]));
	}
}


bool CHash2PartLock::Initialize(CHash2::HASH_SIZE HashSize, DWORD dwSpinCount)
{
	Allocate(HashSize);

	for(CHash2::HASH_SIZE i = 0; i < HashSize; i++)
	{
		if(FALSE == ::InitializeCriticalSectionAndSpinCount(&(m_pCriticalSection[i]), dwSpinCount))
		{
			return false;
		}
	}

	return true;
}


///////////////////////////////// End of File /////////////////////////////////
