/******************************************************************************
	CIOCPSocketReceiveBuffer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CQueueBuffer.h"


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketReceiveBuffer : public CQueueBuffer  
{
public:
	CIOCPSocketReceiveBuffer();
	virtual ~CIOCPSocketReceiveBuffer() { }


protected:
	BYTE m_ReceiveBuffer[8192];
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketReceiveBuffer.inl"


///////////////////////////////// End of File /////////////////////////////////
