/******************************************************************************
	CServer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CServiceSync;


///////////////////////////////////////////////////////////////////////////////


class CServer  
{
public:
	CServer(DWORD dwServerTimerInterval = INFINITE);
	virtual ~CServer() { }

	virtual BOOL OnInitialize();
	virtual void OnClose() { }

	virtual void OnEventSignaled() { }
	virtual void OnServerTimer() { }

	virtual void InitializeServiceSync(CServiceSync* /*pServiceSync*/) { }

	DWORD GetServerTimerInterval();


private:
	DWORD m_dwServerTimerInterval;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CServer.inl"


///////////////////////////////// End of File /////////////////////////////////
