/******************************************************************************
	CISLAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CInterlockedSList.h"


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, class BaseAllocator>
class CISLAllocator  
{
public:
	CISLAllocator() { }
	/*virtual */~CISLAllocator();

	ObjectType* New();
	void Delete(ObjectType* pObject);

	void SetPrototype(ObjectType* pObject);


private:
	CInterlockedSList<ObjectType> m_Stack, m_AllStack;
	BaseAllocator m_BaseAllocator;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CISLAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
