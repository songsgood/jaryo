/******************************************************************************
	Coordinate.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


// 제곱했을 때에 float 의 한계를 벗어나지 않아야 한다.
#define USER_SIGHT_MAX		50


#define LEAVE_SIGHT_MARGIN	20


#define NORMAL_CHAT_DISTANCE	50


#define NPC_EVENT_RANGE	3


///////////////////////////////////////////////////////////////////////////////


typedef float COORDINATE;
typedef int DIRECTION;
typedef UINT MAPNUMBER;


struct POINT3D {
	POINT3D();
	POINT3D(POINT3D* pPoint);

	void Set(COORDINATE iX, COORDINATE iY, COORDINATE iZ);
	BOOL IsSame(POINT3D* pPoint);

	COORDINATE GetDistance(POINT3D* pCoordinate);
	COORDINATE GetDistance(COORDINATE X1, COORDINATE Y1, COORDINATE Z1);

	COORDINATE GetDistance2(POINT3D* pCoordinate);
	COORDINATE GetDistance2(COORDINATE X1, COORDINATE Y1, COORDINATE Z1);

	BOOL IsInDistance(POINT3D* pCoordinate, COORDINATE Distance);
	BOOL IsInDistance(COORDINATE X1, COORDINATE Y1, COORDINATE Z1, COORDINATE Distance);

	BOOL IsInDistance2(POINT3D* pCoordinate, COORDINATE Distance2);
	BOOL IsInDistance2(COORDINATE X1, COORDINATE Y1, COORDINATE Z1, COORDINATE Distance2);

	COORDINATE X;
	COORDINATE Y;
	COORDINATE Z;
};


typedef POINT3D* PPOINT3D;


///////////////////////////////////////////////////////////////////////////////


BOOL IsInRect(COORDINATE X, COORDINATE Y, COORDINATE Left, COORDINATE Top, COORDINATE Right, COORDINATE Bottom);
BOOL IsInRect(PPOINT3D pCoordinate1, PPOINT3D pCoordinate2, COORDINATE Range);
BOOL IsInRect(UINT X, UINT Y, UINT Left, UINT Top, UINT Right, UINT Bottom);


///////////////////////////////////////////////////////////////////////////////


COORDINATE Get2DDistance(COORDINATE X1, COORDINATE Z1, COORDINATE X2, COORDINATE Z2);
COORDINATE Get2DDistance(PPOINT3D pCoordinate1, PPOINT3D pCoordinate2);
COORDINATE Get2DDistance2(COORDINATE X1, COORDINATE Z1, COORDINATE X2, COORDINATE Z2);
COORDINATE Get2DDistance2(PPOINT3D pCoordinate1, PPOINT3D pCoordinate2);
COORDINATE GetDistance(COORDINATE X1, COORDINATE Y1, COORDINATE Z1, COORDINATE X2, COORDINATE Y2, COORDINATE Z2);
COORDINATE GetDistance(PPOINT3D pCoordinate1, PPOINT3D pCoordinate2);
COORDINATE GetDistance2(COORDINATE X1, COORDINATE Y1, COORDINATE Z1, COORDINATE X2, COORDINATE Y2, COORDINATE Z2);
COORDINATE GetDistance2(PPOINT3D pCoordinate1, PPOINT3D pCoordinate2);


///////////////////////////////////////////////////////////////////////////////


qinclude "Coordinate.inl"


///////////////////////////////// End of File /////////////////////////////////
