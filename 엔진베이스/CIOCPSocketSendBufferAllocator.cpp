/******************************************************************************
	CIOCPSocketSendBufferAllocator.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CIOCPSocketSendBufferAllocator.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CIOCPSocketSendBufferAllocator::CIOCPSocketSendBufferAllocator() :
	m_pAllocator(NULL),
	m_uiAllocatorCount(0),
	m_puiBufferSize(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


void CIOCPSocketSendBufferAllocator::AllocAllocator(UINT* puiBufferSize, UINT* puiPreAllocSize,
	UINT uiAllocatorCount)
{
	puiPreAllocSize;

	m_uiAllocatorCount = uiAllocatorCount;
	VERIFY(NULL != (m_pAllocator =
		new CISLAllocator< CIOCPSocketSendBuffer, CPrototypeAllocator<CIOCPSocketSendBuffer> >[uiAllocatorCount]));

	VERIFY(NULL != (m_puiBufferSize = new UINT[uiAllocatorCount]));
	::CopyMemory(m_puiBufferSize, puiBufferSize, uiAllocatorCount * sizeof(UINT));

	for(UINT uiIndex = 0; uiIndex < uiAllocatorCount; uiIndex++)
	{
		CIOCPSocketSendBuffer* pIOCPSocketSendBuffer = NULL;
		VERIFY(NULL != (pIOCPSocketSendBuffer = new CIOCPSocketSendBuffer((BYTE) uiIndex, puiBufferSize[uiIndex])));

		m_pAllocator[uiIndex].SetPrototype(pIOCPSocketSendBuffer);
	}
}


void CIOCPSocketSendBufferAllocator::FreeAllocator()
{
	if(NULL != m_puiBufferSize)
	{
		delete []m_puiBufferSize;
		m_puiBufferSize = NULL;
	}

	if(NULL != m_pAllocator)
	{
		delete []m_pAllocator;
		m_pAllocator = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////


CIOCPSocketSendBuffer* CIOCPSocketSendBufferAllocator::Allocate(UINT uiDataSize)
{
	for(UINT uiIndex = 0; uiIndex < m_uiAllocatorCount; uiIndex++)
	{
		if(m_puiBufferSize[uiIndex] >= uiDataSize)
		{
			return ((CIOCPSocketSendBuffer*) m_pAllocator[uiIndex].New());
		}
	}

	VERIFY(!TEXT("Too Big Packet"));

	return NULL;
}


///////////////////////////////// End of File /////////////////////////////////
