/******************************************************************************
	CSListAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, class BaseAllocator>
class CSListAllocator  
{
public:
	CSListAllocator();
	virtual ~CSListAllocator() { }

	ObjectType* New();
	void Delete(ObjectType* pObject);


private:
	ObjectType* m_pFirstEntry;
	BaseAllocator m_BaseAllocator;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSListAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
