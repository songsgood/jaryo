/******************************************************************************
	CSRMAQueue.h
	Single Remover (& Reader) / Multiple Adder Queue
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CInterlockedSList.h"


///////////////////////////////////////////////////////////////////////////////


struct SRMA_QUEUE_ENTRY {
	SRMA_QUEUE_ENTRY();
	SRMA_QUEUE_ENTRY* pSRMAQEntryNext;
};


typedef SRMA_QUEUE_ENTRY* PSRMA_QUEUE_ENTRY;


///////////////////////////////////////////////////////////////////////////////


class CSRMAQueue  
{
public:
	CSRMAQueue();
	virtual ~CSRMAQueue() { }

	void AddTail(PSRMA_QUEUE_ENTRY pEntry);
	void AddTail(PSRMA_QUEUE_ENTRY pBeginEntry, PSRMA_QUEUE_ENTRY pEndEntry);
	PSRMA_QUEUE_ENTRY RemoveHead();

	PSRMA_QUEUE_ENTRY GetFirstEntry();
	PSRMA_QUEUE_ENTRY GetNextEntry(PSRMA_QUEUE_ENTRY pEntry);


protected:
	SRMA_QUEUE_ENTRY m_Head;
	PSRMA_QUEUE_ENTRY m_pTail;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSRMAQueue.inl"


///////////////////////////////// End of File /////////////////////////////////
