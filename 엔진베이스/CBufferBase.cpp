/******************************************************************************
	CBufferBase.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CBufferBase.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CBufferBase::CBufferBase(BYTE* pucBuffer/* = NULL*/, UINT uiBufferSize/* = 0*/) : m_pucBuffer(pucBuffer),
	m_uiBufferSize(uiBufferSize)
{
}


///////////////////////////////////////////////////////////////////////////////


void CBufferBase::SetBuffer(PBYTE pucBuffer, UINT uiBufferSize)
{
	m_pucBuffer = pucBuffer;
	m_uiBufferSize = uiBufferSize;
}


///////////////////////////////// End of File /////////////////////////////////
