/******************************************************************************
	CDoublyLinkedList.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CDoublyListEntry.h"


///////////////////////////////////////////////////////////////////////////////


class CDoublyLinkedList  
{
public:
	CDoublyLinkedList() { }
	virtual ~CDoublyLinkedList() { }

	void Add(CDoublyListEntry* pListEntry);
	void Remove(CDoublyListEntry* pListEntry);

	CDoublyListEntry* GetFirst();
	CDoublyListEntry* GetNext(CDoublyListEntry* pListEntry);

	void Empty();


protected:
	CDoublyListEntry m_FirstEntry;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CDoublyLinkedList.inl"


///////////////////////////////// End of File /////////////////////////////////
