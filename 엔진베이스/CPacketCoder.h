/******************************************************************************
	CPacketCoder.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CQueueBuffer;
class CReceivePacketBuffer;


///////////////////////////////////////////////////////////////////////////////


class CPacketCoder  
{
public:
	CPacketCoder();
	virtual ~CPacketCoder();

	void InitializeInstance();

	void SetEncrypt(BOOL bEncrypt = TRUE);
	void SetDecrypt(BOOL bDecrypt = TRUE);

	int DecodePacket(CReceivePacketBuffer* pToBuffer, CQueueBuffer* pFromBuffer);
	void EncodePacket(CQueueBuffer* pBuffer);


protected:
	void GenKey();
	void GenClientKey();

	void GenTag();
	BOOL CheckTag(unsigned long Tag);


protected:
	BOOL m_bEncrypt;
	BOOL m_bDecrypt;


protected:
#	define VARDECL
#qinclude "Encryption.h"				// Key variable declaration
#	undef VARDECL
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CPacketCoder.inl"


///////////////////////////////// End of File /////////////////////////////////
