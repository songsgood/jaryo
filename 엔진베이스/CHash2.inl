/******************************************************************************
	CHash2.inl
******************************************************************************/


inline CHash2::CHash2() :
	m_pHashObjectList(NULL),
	m_HashSize(0)
{
}


inline CHash2::CHash2(HASH_SIZE HashSize)
{
	Initialize(HashSize);
}


///////////////////////////////////////////////////////////////////////////////


inline CHash2::HASH_SIZE CHash2::GetHashSize()
{
	return m_HashSize;
}


///////////////////////////////////////////////////////////////////////////////


inline void CHash2::Add(HASH_INDEX HashIndex, HASH_ENTRY_2* pHashEntry)
{
	m_pHashObjectList[HashIndex].Add(pHashEntry);
}


inline void CHash2::Remove(HASH_INDEX HashIndex, HASH_ENTRY_2* pHashEntry)
{
	m_pHashObjectList[HashIndex].Remove(pHashEntry);
}


///////////////////////////////////////////////////////////////////////////////


inline HASH_ENTRY_2* CHash2::GetPartFirst(HASH_INDEX HashIndex)
{
	return ((HASH_ENTRY_2*) m_pHashObjectList[HashIndex].GetFirst());
}


inline HASH_ENTRY_2* CHash2::GetPartNext(HASH_INDEX HashIndex, HASH_ENTRY_2* pHashEntry)
{
	return ((HASH_ENTRY_2*) m_pHashObjectList[HashIndex].GetNext(pHashEntry));
}


///////////////////////////////////////////////////////////////////////////////


inline bool CHash2::IsPartEmpty(HASH_INDEX HashIndex)
{
	return m_pHashObjectList[HashIndex].IsEmpty();
}


///////////////////////////////// End of File /////////////////////////////////
