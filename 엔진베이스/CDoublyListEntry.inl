/******************************************************************************
	CDoublyListEntry.inl
******************************************************************************/


#pragma once


///////////////////////////////////////////////////////////////////////////////


inline void CDoublyListEntry::SetPrev(CDoublyListEntry* pPrev)
{
	m_pPrev = pPrev;
}


inline CDoublyListEntry* CDoublyListEntry::GetPrev()
{
	return m_pPrev;
}


inline void CDoublyListEntry::SetNext(CDoublyListEntry* pNext)
{
	m_pNext = pNext;
}


inline CDoublyListEntry* CDoublyListEntry::GetNext()
{
	return m_pNext;
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CDoublyListEntry::IsAdded()
{
	return (NULL != m_pPrev);
}


///////////////////////////////// End of File /////////////////////////////////
