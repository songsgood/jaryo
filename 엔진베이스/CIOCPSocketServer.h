/******************************************************************************
	CIOCPSocketServer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketServer.h"


///////////////////////////////////////////////////////////////////////////////


class CIOCP;


///////////////////////////////////////////////////////////////////////////////


class CIOCPSocketServer : public CSocketServer  
{
public:
	CIOCPSocketServer(CIOCP* pIOCP, DWORD dwServerTimerInterval = INFINITE);
	virtual ~CIOCPSocketServer();

	virtual BOOL OnInitialize();
	virtual void OnClose();

	virtual void OnEventSignaled();

	virtual void FreeClient(CSocketClient* pSocketClient);

	virtual void InitializeServiceSync(CServiceSync* pServiceSync);

	CIOCP* GetIOCP();


protected:
	virtual CSocketClient* IAllocateClient();


protected:
	BOOL PostAccept(UINT uiInitialAcceptCount);


private:
	CIOCP* m_pIOCP;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CIOCPSocketServer.inl"


///////////////////////////////// End of File /////////////////////////////////
