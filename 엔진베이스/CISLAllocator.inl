/******************************************************************************
	CISLAllocator.inl
******************************************************************************/


template <class ObjectType, class BaseAllocator>
inline CISLAllocator<ObjectType, BaseAllocator>::~CISLAllocator()
{
	ObjectType* pEntry = NULL;
	while(NULL != (pEntry = m_AllStack.Pop()))
	{
		m_BaseAllocator.Delete(pEntry);
	}
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, class BaseAllocator>
inline ObjectType* CISLAllocator<ObjectType, BaseAllocator>::New()
{
	ObjectType* pEntry = m_Stack.Pop();

	if(NULL == pEntry)
	{
		VERIFY(NULL != (pEntry = m_BaseAllocator.New()));
		m_AllStack.Push(pEntry);
	}

	/*if(FALSE != ::IsBadReadPtr(pEntry, sizeof(ObjectType)))
	{
		VERIFY(!TEXT("FALSE != ::IsBadReadPtr(pEntry, sizeof(ObjectType)"));
		VERIFY(NULL != (pEntry = m_BaseAllocator.New()));
		m_AllStack.Push(pEntry);
	}*/
	/*ObjectType* pEntry = NULL;
	VERIFY(NULL != (pEntry = m_BaseAllocator.New()));*/

	pEntry->InitializeInstance();	// Constructor

	return pEntry;
}


template <class ObjectType, class BaseAllocator>
inline void CISLAllocator<ObjectType, BaseAllocator>::Delete(ObjectType* pEntry)
{
	pEntry->CleanupInstance();	// Destructor
	//m_BaseAllocator.Delete(pEntry);
	m_Stack.Push(pEntry);
}


///////////////////////////////////////////////////////////////////////////////


template <class ObjectType, class BaseAllocator>
inline void CISLAllocator<ObjectType, BaseAllocator>::SetPrototype(ObjectType* pObject)
{
	m_BaseAllocator.SetPrototype(pObject);
}


///////////////////////////////// End of File /////////////////////////////////
