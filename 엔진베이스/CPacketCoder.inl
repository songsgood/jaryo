/******************************************************************************
	CPacketCoder.inl
******************************************************************************/


inline CPacketCoder::~CPacketCoder()
{
}


///////////////////////////////////////////////////////////////////////////////


inline void CPacketCoder::SetEncrypt(BOOL bEncrypt/* = TRUE*/)
{
	m_bEncrypt = bEncrypt;
}


inline void CPacketCoder::SetDecrypt(BOOL bDecrypt/* = TRUE*/)
{
	m_bDecrypt = bDecrypt;
}


///////////////////////////////////////////////////////////////////////////////


inline void CPacketCoder::GenTag()
{
	tag = cs_key ^ (++s_tag);
}


inline BOOL CPacketCoder::CheckTag(unsigned long Tag)
{
	return (++c_tag == (Tag ^ cs_key));
}


///////////////////////////////// End of File /////////////////////////////////
