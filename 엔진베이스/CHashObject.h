/******************************************************************************
	CHashObject.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CDoublyListEntry.h"


///////////////////////////////////////////////////////////////////////////////


#define INVALID_HASHINDEX	((UINT) -1)


///////////////////////////////////////////////////////////////////////////////


class CHashObject : public CDoublyListEntry  
{
public:
	CHashObject();
	virtual ~CHashObject() { }

	void InitializeInstance();

	void SetHashIndex(UINT uiHashIndex);
	UINT volatile GetHashIndex();

	void SetOwner(void* pOwner);
	void* GetOwner();


protected:
	UINT m_uiHashIndex;
	void* m_pOwner;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CHashObject.inl"


///////////////////////////////// End of File /////////////////////////////////
