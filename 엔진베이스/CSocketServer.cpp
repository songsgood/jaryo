/******************************************************************************
	CSocketServer.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CSocketServer.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


CSocketServer::CSocketServer(DWORD dwServerTimerInterval/* = INFINITE*/) :
	CServer(dwServerTimerInterval),
	m_pServerSocket(NULL),
	m_usPort(2002)
{
}


///////////////////////////////////////////////////////////////////////////////


CSocketClient* CSocketServer::AllocateClient()
{
	CSocketClient* pSocketClient = IAllocateClient();
	// pSocketClient->AllocateClientSocket();
	pSocketClient->SetServer(this);
	return pSocketClient;
}


///////////////////////////////////////////////////////////////////////////////


BOOL CSocketServer::CreateServerSocket()
{
	VERIFY(NULL != (m_pServerSocket = AllocateServerSocket()));

	return m_pServerSocket->CreateBindListen();
}


void CSocketServer::DestroyServerSocket()
{
	if(NULL != m_pServerSocket)
	{
		delete m_pServerSocket;
		m_pServerSocket = NULL;
	}
}


///////////////////////////////// End of File /////////////////////////////////
