/******************************************************************************
	CServiceStatusAllocator.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CServiceStatus;


///////////////////////////////////////////////////////////////////////////////


class CServiceStatusAllocator  
{
public:
	CServiceStatusAllocator() { }
	virtual ~CServiceStatusAllocator() { }

	virtual CServiceStatus* AllocServiceStatus(int iGlobalArgc, LPTSTR* ppGlobalArgv, DWORD dwArgc, LPTSTR* pszArgv);
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CServiceStatusAllocator.inl"


///////////////////////////////// End of File /////////////////////////////////
