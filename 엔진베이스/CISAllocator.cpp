/******************************************************************************
	CISAllocator.cpp
******************************************************************************/


qinclude "stdafx.h"
qinclude "CISAllocator.h"


///////////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


///////////////////////////////////////////////////////////////////////////////


UINT CISAllocator::PreAlloc(UINT uiInitialCount)
{
	CISAllocatorEntry* pEntry = NULL;
	UINT uiAllocatedCount = 0;

	for(uiAllocatedCount = 0; uiAllocatedCount < uiInitialCount; uiAllocatedCount++)
	{
		if(NULL == (pEntry = m_pAllocatorEntry->AllocSingleEntry()))
		{
			break;
		}
		Free(pEntry);
	}

	return uiAllocatedCount;
}


///////////////////////////////// End of File /////////////////////////////////
