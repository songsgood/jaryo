//==============================================================================
//	BaseTypes.h
//==============================================================================


qpragma once


////////////////////////////////////////////////////////////////////////////////


typedef char int_8;
typedef signed char s_int_8;
typedef unsigned char u_int_8;
typedef short int_16;
typedef signed short s_int_16;
typedef unsigned short u_int_16;
typedef int int_32;
typedef signed int s_int_32;
typedef unsigned int u_int_32;
typedef __int64 int_64;
typedef signed __int64 s_int_64;
typedef unsigned __int64 u_int_64;


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE>
struct TypeStruct
{
	TypeStruct();
	TypeStruct(BASE_TYPE InitValue);

	typedef BASE_TYPE BASE_TYPE;

	BASE_TYPE Get();
	void Set(BASE_TYPE NewValue);

	operator BASE_TYPE();
	BASE_TYPE* operator & ();
	BASE_TYPE operator = (BASE_TYPE NewValue);

	BASE_TYPE Value;
};


////////////////////////////////////////////////////////////////////////////////


template <class BASE_TYPE, BASE_TYPE InvalidValue>
struct TypeStructIV : public TypeStruct<BASE_TYPE>
{
	TypeStructIV();
	TypeStructIV(BASE_TYPE InitValue);

	static BASE_TYPE GetInvalidValue();

	static bool IsValid(BASE_TYPE Value);
	static bool IsInvalid(BASE_TYPE Value);

	void SetInvalidValue();

	bool IsValid();
	bool IsInvalid();
};


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE, BASE_TYPE MinValue, BASE_TYPE MaxValue>
struct TypeWithRange : TypeStruct<BASE_TYPE>
{
	TypeWithRange() { }
	TypeWithRange(BASE_TYPE Value);

	void Set(BASE_TYPE Value);

	static BASE_TYPE GetMaxValue();
	static BASE_TYPE GetMinValue();
	static BASE_TYPE GetInRangeValue(BASE_TYPE Value);

	static bool IsInRange(BASE_TYPE Value);
	bool IsInRange();

	static bool FixInRange(BASE_TYPE* pValue);
	bool FixInRange();
};


////////////////////////////////////////////////////////////////////////////////


template <u_int_32 MaxCharacterCount>
struct FixedString
{
	FixedString();
	FixedString(TCHAR* pInitString);

	typedef TCHAR BASE_TYPE[MaxCharacterCount + 1];

	operator TCHAR*();
	TCHAR* operator = (TCHAR* pString);

	void SetEmptyString();

	TCHAR* GetString();
	void SetString(TCHAR* pString);

	bool IsEmpty();

	static u_int_32 GetMaxCharacterCount();
	static u_int_32 GetBufferSize();

	u_int_32 GetCharacterCount();

	BASE_TYPE String;
};


////////////////////////////////////////////////////////////////////////////////


struct BIT_ARRAY
{
	typedef u_int_32 BIT_ARRAY_INDEX;

	BIT_ARRAY();
	virtual ~BIT_ARRAY();

	void Allocate(BIT_ARRAY_INDEX MaxArrayIndex);
	void Free();
	void Set(BIT_ARRAY_INDEX Index);
	void Unset(BIT_ARRAY_INDEX Index);
	bool IsSet(BIT_ARRAY_INDEX Index);

	u_int_8* pArray;
};


////////////////////////////////////////////////////////////////////////////////


template<class BASE_TYPE>
struct BIT_FLAG : TypeStruct<BASE_TYPE>
{
	BIT_FLAG();

	void Initialize();

	void SetFlag(BASE_TYPE Flag);
	void UnsetFlag(BASE_TYPE Flag);
	bool IsSet(BASE_TYPE Flag);
};


////////////////////////////////////////////////////////////////////////////////


qinclude "BaseTypes.inl"


////////////////////////////////// End of File /////////////////////////////////
