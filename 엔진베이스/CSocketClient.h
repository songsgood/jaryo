/******************************************************************************
	CSocketClient.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


class CClientSocket;
class CSocketServer;


///////////////////////////////////////////////////////////////////////////////


class CSocketClient  
{
public:
	CSocketClient();
	virtual ~CSocketClient();

	virtual void SetServer(CSocketServer* pServer);
	CSocketServer* GetServer();

	virtual void AllocateClientSocket();

	BOOL Connect(const char* pAddress, unsigned short usPort);
	BOOL Connect();


protected:
	void SetClientSocket(CClientSocket* pClientSocket);
	CClientSocket* GetClientSocket();


private:
	CClientSocket* m_pClientSocket;
	CSocketServer* m_pServer;
	TCHAR m_szPeerIPAddress[MAX_IPADDRESS_LENGTH + 1];
	unsigned short m_usPeerPort;
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CSocketClient.inl"


///////////////////////////////// End of File /////////////////////////////////
