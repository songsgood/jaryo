;/**************************************************************
;Module:  EventLogMsgs.mc
;**************************************************************/


;//******************** CATEGORY SECTION ***********************
;// Category IDs are 16-bit values
MessageIdTypedef=WORD

MessageId=1
SymbolicName=EL_CAT_APPEXECSTATUS
Language=English
Execution status
.

MessageId=2
SymbolicName=EL_CAT_APPEVENT
Language=English
Event
.


;//********************* MESSAGE SECTION ***********************
;// Event IDs are 32-bit values
MessageIdTypedef=DWORD

MessageId=0
SymbolicName=EL_MSG_ERROR
Language=English
%1
.

MessageId=100
SymbolicName=EL_MSG_APPSTART
Language=English
Application started.
.

MessageId=
SymbolicName=EL_MSG_APPSTOP
Language=English
Application stopped.
.

MessageId=
SymbolicName=EL_MSG_ERRORCODE
Language=English
Application generated error code %1: "%%%1"
.


;//************************ END OF FILE ************************