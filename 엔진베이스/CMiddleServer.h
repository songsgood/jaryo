/******************************************************************************
	CMiddleServer.h
******************************************************************************/


qpragma once


///////////////////////////////////////////////////////////////////////////////


qinclude "CServer.h"


///////////////////////////////////////////////////////////////////////////////


class CMiddleServer : public CServer  
{
public:
	CMiddleServer();
	virtual ~CMiddleServer();
};


///////////////////////////////////////////////////////////////////////////////


qinclude "CMiddleServer.inl"


///////////////////////////////// End of File /////////////////////////////////
