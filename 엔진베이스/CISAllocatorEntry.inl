/******************************************************************************
	CISAllocatorEntry.inl
******************************************************************************/


inline IS_ALLOCATOR_ENTRY::IS_ALLOCATOR_ENTRY() : This(NULL)
{
}


///////////////////////////////////////////////////////////////////////////////


inline CISAllocatorEntry::CISAllocatorEntry() : m_ibFreed(FALSE)
{
	m_AllocatorEntry.This = this;
	m_AllListNext.This = this;
	AddToAllList();
}


///////////////////////////////////////////////////////////////////////////////


inline PINTERLOCKED_STACK_ENTRY CISAllocatorEntry::GetStackEntry()
{
	return ((PINTERLOCKED_STACK_ENTRY) (&m_AllocatorEntry));
}


///////////////////////////////////////////////////////////////////////////////


inline BOOL CISAllocatorEntry::SetAllocated()
{
	return m_ibFreed.UnSet();
}


inline BOOL CISAllocatorEntry::SetFreed()
{
	return m_ibFreed.Set();
}


inline BOOL CISAllocatorEntry::IsFreed()
{
	return m_ibFreed;
}


///////////////////////////////// End of File /////////////////////////////////
