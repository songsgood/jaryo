/******************************************************************************
	CBufferBase.inl
******************************************************************************/


#pragma once


//////////////////////////////////////////////////////////////////////////////


inline CEventLog::CEventLog() : m_hEventLog(NULL)
{
}


inline CEventLog::~CEventLog() 
{
	if (m_hEventLog != NULL) ::DeregisterEventSource(m_hEventLog);
}


//////////////////////////////////////////////////////////////////////////////


inline void CEventLog::SetAppName(LPCTSTR lpAppName)
{
	LSTRCPYNTOARRAY(m_szAppName, lpAppName);
}


///////////////////////////////// End of File /////////////////////////////////
