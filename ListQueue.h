#ifndef _LISTQUEUE_H_
#define _LISTQUEUE_H_

#include "MemoryPool.h"

template <class DATA>
class CListQueue
{
public:
	CListQueue();
	~CListQueue();

	void Enqueue(DATA data);
	void Dequeue(DATA &data);

private:

	struct Node
	{
		DATA Data;
		Node *pNext;

	};

	CMemoryPool<Node> pNodePool;



	Node *pHead;
	Node *pRear;
};

template <class DATA>
CListQueue::CListQueue()
{
	Node *Dummy = pNodePool.Alloc();
	Dummy->Data = NULL;

	pHead = Dummy;
	pRear = Dummy;

}

template <class DATA>
CListQueue::~CListQueue()
{
}

template <class DATA>
void CListQueue::Enqueue(DATA data)
{
	Node *pNode = pNodePool.Alloc();

	pNode->Data = data;
	pNode->pNext = NULL;// Dummy;

	//pHead = pNode;

	iRear->pNext = pNode;
	iRear = pNode;
}

template <class DATA>
void CListQueue::Dequeue(DATA &data)
{
	if (pHead == pRear)
		data = NULL;

//	ire

}

#endif // !_LISTQUEUE_H_
