#pragma once

#include <Windows.h>
#include <Strsafe.h>
#include <time.h>
#include <stdio.h>
#include <locale.h>


#include <direct.h>		//mkdir
#include <errno.h>		//errno


#define FILEWRITE_LOG(szType,fmt,...) CFileLog::GetInstance()->System_Log(__FILE__,__LINE__,szType,fmt,##__VA_ARGS__)


class CFileLog
{
public:
	
	void System_Log(const char* file, int Line, const char* Type, const char* fmt, ...);
		

	static CFileLog *GetInstance()
	{
		static CFileLog Log;

		return &Log;

	}

public:
	CFileLog()
	{
		Number = 0;
	}
	~CFileLog() {}
	

private:
	UINT64 Number;


	

};