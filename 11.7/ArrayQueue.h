#pragma once



template<typename T>
class CArrayQueue
{

public :
	
	CArrayQueue(int Count = 100)
	{
		MaxCount = Count;
		queue = new T[Count];
		front = rear = 0;
		Size = 0;
	}

	~CArrayQueue()
	{
		front = rear = 0;
		delete[] queue;
	}
private:

	int MaxCount;
	T *queue;
	int front, rear;
	int Size;
	
public:

	int GetSize(void)		
	{
		return Size;
	}

	void clear_queue(void)
	{
		front = rear;
	}

	bool Enqueue(T k)
	{
		// 큐가 꽉차있는지 확인
		int _rear = rear;
		int _front = front;

		if ((_rear + 1) MaxCount == _front) {

			//printf("\n   Queue overflow.");
			return false;
		}

		*(queue+_rear) = k;
		rear = (_rear + 1) MaxCount;


		++Size;

		return true;
	}

	bool Dequeue(T &k)
	{

		int _rear = rear;
		int _front = front;

		if (_front == _rear) {
			//printf("\n  Queue underflow.");
			return false;
		}

		k = *(queue+_front);
		front = (_front + 1) MaxCount;


		--Size;

		return true;
	}

	bool IsClear()
	{
		if (front == rear)
			return true;
		else
			return false;
	}

};