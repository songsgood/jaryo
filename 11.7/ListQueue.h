#ifndef _LISTQUEUE_H_
#define _LISTQUEUE_H_

#include "MemoryPoolLF.h"

template <class DATA>
class CListQueue
{
public:
	CListQueue();
	~CListQueue();

	void Enqueue(DATA data);
	bool Dequeue(DATA &data);

	int GetSize(){return iNodeCount;};
	long iNodeCount;
private:

	struct Node
	{
		DATA Data;
		Node *pNext;

	};

	CMemoryPool<Node> pNodePool;
	
	Node *pFront;
	Node *pRear;
	Node *Dummy;

	
};

template <class DATA>
CListQueue<DATA>::CListQueue()
{
	Dummy = pNodePool.Alloc();
	Dummy->Data = NULL;

	pFront = NULL;//Dummy;
	pRear  = NULL;//Dummy;

	iNodeCount = 0;

}

template <class DATA>
CListQueue<DATA>::~CListQueue()
{

}

template <class DATA>
void CListQueue<DATA>::Enqueue(DATA data)
{
	Node *pNode = pNodePool.Alloc();

	pNode->Data  =  data;
	pNode->pNext =  NULL;

	if(iNodeCount == 0)
	{
		pFront = pNode;//pNode->pNext = Dummy;
		if(pFront == NULL)
		{
			int b = 0;
		}
		//Dummy->pNext = pNode;
		
		//pRear  = pNode;
		//pFront = pNode;
	
	}
	else
	{
		//pNode->pNext = pRear;
		pRear->pNext = pNode;
		//pRear = pNode;
	}
	pRear = pNode;
	//InterlockedIncrement(&iNodeCount);
	++iNodeCount;
}

template <class DATA>
bool CListQueue<DATA>::Dequeue(DATA &data)
{


	if (iNodeCount == 0)
		return false;
	
	Node *pNode = pFront;//->pNext;

	data = pNode->Data;
	 pFront = pNode->pNext;
	 if(pFront == NULL)
	 {
		 int aa = 0;


	 }
	 if(iNodeCount == 2)
	 {
		 int b =0;
	 }

	pNodePool.Free(pNode);
	//InterlockedDecrement(&iNodeCount);
	--iNodeCount;

	return true;
//	ire

}

#endif // !_LISTQUEUE_H_
