// SerializationBuffer.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "SerializationBuffer.h"
//#include <Windows.h>

//CMemoryPoolSRW<CPacketBuffer> CPacketBuffer::_pMemoryPool;
CMemoryPool<CPacketBuffer> CPacketBuffer::_pMemoryPool;
//CMemoryPoolTLS<CPacketBuffer> CPacketBuffer::_pMemoryPool;
//CMemoryPoolSRWTLS<CPacketBuffer> CPacketBuffer::_pMemoryPool;
BYTE CPacketBuffer::_PacketCode;
BYTE CPacketBuffer::_EncodeCode1;
BYTE CPacketBuffer::_EncodeCode2;



CPacketBuffer::CPacketBuffer()
{
	Initial(eBUFFER_DEFAULT);
}

CPacketBuffer::CPacketBuffer(int iBufferSize)
{
	Initial(iBufferSize);


}


//CPacketBuffer::CPacketBuffer(const CPacketBuffer &clSrcPacket)
//{
//
//}


CPacketBuffer::~CPacketBuffer()
{
	//delete[] _Buffer;
	//if (_Buffer)
//	memset(_Buffer, 0, eBUFFER_DEFAULT);
	free(_Buffer);
}


void CPacketBuffer::Initial(int iBufferSize)
{

	_iFront = 0;
	_iRear = df_HEAD_SIZE;
	//_iBufferSize = iBufferSize;
	//_Buffer = new char[iBufferSize];
	//_Buffer = NULL;
	//if (!_Buffer)

	_Buffer = (char*)malloc(iBufferSize);

	memset(_Buffer, 0, iBufferSize);

	_RefCount = 0;

	EndPacket = 1;


}

void CPacketBuffer::Release(void)
{
	//delete[] _Buffer;
	//if (_Buffer)
	free(_Buffer);
}
//void CPacketBuffer::Clear(void)
//{
//	//_iFront = _HeadSize;
//	//_iRear = _HeadSize;
//	_byIsHead = 1;
//	_iFront = 0;
//	_iRear = 0;
//
//}

void CPacketBuffer::Clear()
{
	_byIsHead = 1;

	_iFront = 0;
	_iRear = df_HEAD_SIZE;
}

//char* CPacketBuffer::GetHeadBufferPtr(void)
//{
//	return _Buffer + _iFront;
//}


//int	CPacketBuffer::GetBufferSize(void)
//{
//	return _iBufferSize;
//}

int	CPacketBuffer::GetPacketSize(void)
{
	return _iRear - _iFront;
}
//
//int	CPacketBuffer::GetPacketPayLoadSize(void)
//{
//	return _iRear - _HeadSize;
//}


char* CPacketBuffer::GetBufferPtr(void)
{
	return _Buffer + _iFront;
}




//char* CPacketBuffer::GetPayLoadBufferPtr(void)
//{
//	return _Buffer + _iRear;
//}

char* CPacketBuffer::GetPayLoadBufferStartPtr()
{
	return _Buffer + df_HEAD_SIZE;
}


//char* CPacketBuffer::GetCurrentBufferPtr(void)
//{
//	return _Buffer + _iFront;
//}


int	CPacketBuffer::MoveWritePos(int iSize)
{
	_iRear += iSize;
	return iSize;

}
int	CPacketBuffer::MoveReadPos(int iSize)
{
	_iFront += iSize;
	return iSize;
}

int	CPacketBuffer::MoveHeadPos(int iSize)
{
	_iFront += df_HEAD_SIZE - iSize;
	return iSize;
}


CPacketBuffer& CPacketBuffer::operator<<(char data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(char));
	_iRear += sizeof(char);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(unsigned char data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(unsigned char));
	_iRear += sizeof(unsigned char);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(short data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(short));
	_iRear += sizeof(short);

	return *this;
}
CPacketBuffer &CPacketBuffer::operator<<(int data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(int));
	_iRear += sizeof(int);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(long data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(long));
	_iRear += sizeof(long);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(unsigned short data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(unsigned short));
	_iRear += sizeof(unsigned short);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(unsigned int data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(unsigned int));
	_iRear += sizeof(unsigned int);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(unsigned long data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(unsigned long));
	_iRear += sizeof(unsigned long);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(float data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(float));
	_iRear += sizeof(float);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator<<(double data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(double));
	_iRear += sizeof(double);

	return *this;
}

CPacketBuffer& CPacketBuffer::operator<<(unsigned __int64 data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(unsigned __int64));
	_iRear += sizeof(unsigned __int64);

	return *this;
}

CPacketBuffer& CPacketBuffer::operator<<(__int64 data)
{
	memcpy(_Buffer + _iRear, &data, sizeof(__int64));
	_iRear += sizeof(__int64);

	return *this;
}


///////////////////////////////////////////////////////////////////////////////////

CPacketBuffer& CPacketBuffer::operator>>(char &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(char));
	_iFront += sizeof(char);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(unsigned char &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(unsigned char));
	_iFront += sizeof(unsigned char);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(short &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(short));
	_iFront += sizeof(short);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(int &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(int));
	_iFront += sizeof(int);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(long &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(long));
	_iFront += sizeof(long);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(unsigned short &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(unsigned short));
	_iFront += sizeof(unsigned short);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(unsigned int &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(unsigned int));
	_iFront += sizeof(unsigned int);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(unsigned long &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(unsigned long));
	_iFront += sizeof(unsigned long);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(float &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(float));
	_iFront += sizeof(float);

	return *this;
}
CPacketBuffer& CPacketBuffer::operator>>(double &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(double));
	_iFront += sizeof(double);

	return *this;
}

CPacketBuffer& CPacketBuffer::operator>>(unsigned __int64 &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(unsigned __int64));
	_iFront += sizeof(unsigned __int64);

	return *this;
}


CPacketBuffer& CPacketBuffer::operator>>(__int64 &data)
{
	memcpy(&data, _Buffer + _iFront, sizeof(__int64));
	_iFront += sizeof(__int64);

	return *this;
}


/////////////////////////////////////////////////////////////////////

int	CPacketBuffer::GetData(char *chpDest, int iSize)
{
	memcpy(chpDest, _Buffer + _iFront, iSize);
	_iFront += iSize;

	return iSize;
}


int	CPacketBuffer::GetData(WCHAR *chpDest, int iSize)
{
	memcpy(chpDest, _Buffer + _iFront, iSize * 2);
	_iFront += (iSize * 2);

	return iSize;
}


//////////////////////////////////////////////////////////////////////////
// 데이타 삽입.
//
// Parameters: (char *)Src 포인터. (int)SrcSize.
// Return: (int)복사한 사이즈.
//////////////////////////////////////////////////////////////////////////
int	CPacketBuffer::PutData(char *chpSrc, int iSrcSize)
{
	memcpy(_Buffer + _iRear, chpSrc, iSrcSize);
	_iRear += iSrcSize;

	return iSrcSize;
}


int	CPacketBuffer::PutData(WCHAR *chpSrc, int iSrcSize)
{
	memcpy(_Buffer + _iRear, chpSrc, iSrcSize * 2);
	_iRear += (iSrcSize* 2);

	return iSrcSize;
}


void CPacketBuffer::PutHeader(char *chpSrc, int size)
{
	if (_byIsHead == 0)
		return;

	_byIsHead--;

	_iFront = df_HEAD_SIZE - size;
	memcpy(_Buffer + _iFront, chpSrc, size);

}

void CPacketBuffer::PutHeaderShort(int chpSrc)
{
	if (_byIsHead == 0)
		return;

	_byIsHead--;

	_iFront = 3;
	memcpy(_Buffer + _iFront, &chpSrc, 2);

}



int	CPacketBuffer::GetUseSize()
{

	return _iRear - _iFront;
}

int	CPacketBuffer::GetPayLoadSize()
{
	return _iRear - df_HEAD_SIZE;
}


BYTE CPacketBuffer::GetCheckSum(BYTE* pBuffer, int size)
{
	int CheckSum = 0;
	BYTE *pPtr = pBuffer;



	for (int i = 0; i < size; i++)
	{
		CheckSum += *pPtr;
		pPtr++;
	}

	return (BYTE)CheckSum ;
56;
}

void CPacketBuffer::GetByteXOR(BYTE Code, BYTE *pTarget, int size)
{
	for (int i = 0; i < size; i++)
		pTarget[i] ^= Code;
}

void CPacketBuffer::SetPacketCommand(WORD Command)
{
		
	memset(_Buffer, 0x00, 8);
	memcpy(_Buffer+sizeof(UINT), &Command, sizeof(short));

}
////////////////////////////////////////////////////////////////////////
//1. Rand XOR Code 생성
//2. Payload 의 checksum 계산
//3. Rand XOR Code 로[CheckSum, Payload] 바이트 단위 xor
//4. 고정 XOR Code 1 로[Rand XOR Code, CheckSum, Payload] 를 XOR
//5. 고정 XOR Code 2 로 [ Rand XOR Code , CheckSum , Payload ] 를 XOR
////////////////////////////////////////////////////////////////////////
void CPacketBuffer::Encode()
{
	//USHORT usPacketNum = 21000;
	USHORT sBufLen = GetPayLoadSize();
	
	memcpy(_Buffer+sizeof(UINT)+sizeof(short), &sBufLen, sizeof(short));	

	return;

	if (_byIsHead == 0)
		return;

	_byIsHead--;

	//이미 헤드가 들어있다면 나온다 (중복 암호화 방지)
	//if (_iFront != 5)
	//	return;

	//1. Rand XOR Code 생성
	BYTE RandCode = rand() ;


	//2. Payload 의 checksum 계산
	BYTE* pPayLoad = (BYTE*)GetPayLoadBufferStartPtr();
	BYTE CheckSum = GetCheckSum(pPayLoad, GetPayLoadSize());


	//3. Rand XOR Code 로[CheckSum, Payload] 바이트 단위 xor
	GetByteXOR(RandCode, &CheckSum, sizeof(BYTE));
	GetByteXOR(RandCode, pPayLoad, GetPayLoadSize());

	//4. 고정 XOR Code 1 로[Rand XOR Code, CheckSum, Payload] 를 XOR
	GetByteXOR(_EncodeCode1, &RandCode, sizeof(BYTE));
	GetByteXOR(_EncodeCode1, &CheckSum, sizeof(BYTE));
	GetByteXOR(_EncodeCode1, pPayLoad, GetPayLoadSize());

	//5. 고정 XOR Code 2 로 [ Rand XOR Code , CheckSum , Payload ] 를 XOR
	GetByteXOR(_EncodeCode2, &RandCode, sizeof(BYTE));
	GetByteXOR(_EncodeCode2, &CheckSum, sizeof(BYTE));
	GetByteXOR(_EncodeCode2, pPayLoad, GetPayLoadSize());


	//Code(1byte) - Len(2byte) - Rand XOR Code(1byte) - CheckSum(1byte) - Payload(Len byte)

	st_PackHead stHead;
	stHead.PackCode = _PacketCode;
	stHead.Len = GetPayLoadSize();
	stHead.RandXORCode = RandCode;
	stHead.CheckSum = CheckSum;




	//_iFront = df_HEAD_SIZE - size;
	memcpy(_Buffer, &stHead, sizeof(st_PackHead));

	//PutHeader((char*)&stHead, sizeof(st_PackHead));

}

//////////////////////////////////////////////////////////////////////
//1. 고정 XOR Code 2 로[Rand XOR Code, CheckSum, Payload] 를 XOR
//2. 고정 XOR Code 1 로[Rand XOR Code, CheckSum, Payload] 를 XOR
//3. Rand XOR Code 를 파악.
//4. Rand XOR Code 로[CheckSum - Payload] 바이트 단위 xor
//5. Payload 를 checksum 공식으로 계산 후 패킷의 checksum 과 비교
/////////////////////////////////////////////////////////////////////

//복호화
bool CPacketBuffer::Decode()
{

	//(WORD)(_Buffer + 4)

	////뺄때는 앞의 5바이트 버린다
	//FSPacketHeader stHead;

	////헤더 뽑기
	//GetData((char*)&stHead, sizeof(FSPacketHeader));//_HeadSize

	//memcpy(_Buffer+sizeof(FSPacketHeader),&stHead.usCommandNum,2);

	//(USHORT*)(_Buffer+sizeof(FSPacketHeader)) = stHead.usCommandNum;
	char temp[8];
	GetData(temp,8);

	return true;

	BYTE* pPayLoad = (BYTE*)GetPayLoadBufferStartPtr();



	////1. 고정 XOR Code 2 로 [ Rand XOR Code , CheckSum , Payload ] 를 XOR
	//GetByteXOR(_EncodeCode2, &stHead.RandXORCode, sizeof(BYTE));
	//GetByteXOR(_EncodeCode2, &stHead.CheckSum, sizeof(BYTE));
	//GetByteXOR(_EncodeCode2, pPayLoad, stHead.Len);


	////2. 고정 XOR Code 1 로[Rand XOR Code, CheckSum, Payload] 를 XOR
	//GetByteXOR(_EncodeCode1, &stHead.RandXORCode, sizeof(BYTE));
	//GetByteXOR(_EncodeCode1, &stHead.CheckSum, sizeof(BYTE));
	//GetByteXOR(_EncodeCode1, pPayLoad, stHead.Len);


	////4. Rand XOR Code 로[CheckSum - Payload] 바이트 단위 xor
	//GetByteXOR(stHead.RandXORCode, &stHead.CheckSum, sizeof(BYTE));
	//GetByteXOR(stHead.RandXORCode, pPayLoad, stHead.Len);

	//int CheckSum = GetCheckSum(pPayLoad, stHead.Len);

	////체크섬 확인 방어
	//if (CheckSum != stHead.CheckSum)
	//	return false;

	////페이로드 길이와 헤더에 명시된 길이 비교
	//if (GetUseSize() != stHead.Len)
	//	return false;


	return true;


}

void CPacketBuffer::SetEndPacket(int End)
{
	EndPacket = End;
}

int CPacketBuffer::GetEndPacket()
{
	return EndPacket;
}

void CPacketBuffer::Pick(int Offset, char* data, int Size)
{
	memcpy(data, GetBufferPtr() + Offset, Size);
	
}