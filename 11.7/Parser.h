#pragma once

#include <Windows.h>
#include <string>
#include <unordered_map>

using namespace std;




struct SPacket
{	
	char Name[30];
	WORD wPacketCommand;
	list<int> listValue;

	SPacket()
	{
		memset(Name,0,30);
	}
};



enum
{
	EINT = 0,
	ESTRING,
	EINT64
	

};

class CParser
{
private:
    char*          _pwchBuffer;
    int             _iFileSize;
    
    int             _iBufferCurrent;
    int             _iBufferEnd;
    int             _iBufferStart;

    bool            _bProvideAreaMode;

	typedef unordered_map<string,SPacket*> MapPackType;//  ;

public:
    static CParser* GetInstance(void)
    {
        static CParser Parser;
        return &Parser;
    }

public:
    void FileLoad(char* pFileName);
    void Initialize(void);
    bool SkipNoneCommand(void);
    MapPackType* SetPacketList();

	bool SetPacket();
	bool FindValueType(SPacket* pPacket, char* pchType,int iLen);

    bool GetNextWord(char** pchBuffer, int* iLength);
    bool GetStringWord(char** pwchBuffer, int* iLength);
    bool GetValue(char* pchName, WORD* wpValue);
    bool GetValue(char* pchName, LONG* lpValue);
    bool GetValue(char* pchName, int* ipValue);
    bool GetValue(char* pchName, float* fpValue);
    bool GetValue(char* pchName, BYTE* chValue);
	bool GetValue(char * pcnName, char * pchValue);

	bool UTF16toUTF8(WCHAR *wValue,char* Value);
	bool UTF16toUTF8_NotNull(WCHAR *wValue, char* Value);

public:
    CParser();
    ~CParser();

private:


	//SPacket *PacketInfoType;
	
	MapPackType *MapPacket;
	
};