#pragma once
//#include <Windows.h>

#define     df_BufferSize       5000

class CRingBuffer
{
private:
    char*    _pBuffer; // Buffer Ptr
    int     _iBufferSize; // Buffer size 
    int     _iRear;  
    int     _iFront;

public:    
 
    int	GetUseSize(void);  
    int	GetFreeSize(void);

    int	GetBufferSize(void);

public:
  
    int	GetNotBrokenPutSize(void);
    int	GetNotBrokenGetSize(void);

public:
   
    int	Enqueue(char *chpData, int iSize);
    int	Dequeue(char *chpDest, int iSize);

    int	Peek(char *chpDest, int iSize);

public:
 
    char*   GetBufferPtr(void);

  
    char*   GetWriteBufferPtr(void); 
    char*   GetReadBufferPtr(void);

public:
   
    void	ClearBuffer(void);


public:
   
    void RemoveData(int iSize);
    void MoveWritePos(int iSize);

public:

      CRingBuffer(int iBufferSize = df_BufferSize);
    ~CRingBuffer();
};