#pragma once

#define _WINSOCKAPI_
#include <Windows.h>
//#include <iostream>
#include <new.h>

#include <process.h>


#define dfCHECKCODE 0x99118822


template <class DATA>
class CMemoryPool
{
private:

	/* **************************************************************** */
	// 각 블럭 앞에 사용될 노드 구조체.
	/* **************************************************************** */
	struct st_BLOCK_NODE
	{
		DATA data;
		int CheckCode;
		st_BLOCK_NODE *stpNextBlock;
		
		st_BLOCK_NODE()
		{
			stpNextBlock = NULL;
			CheckCode = dfCHECKCODE;

		}

	};


	union TOP_NODE
	{
		LONG64 Aligment;
		struct
		{
			st_BLOCK_NODE *pTopNode;
			UINT32 iUniqueNum;

		};

	};



private:

	//DATA * _Data;

	bool _bPlacementNew;

	CRITICAL_SECTION	_csMemoryPool;



public:

	//////////////////////////////////////////////////////////////////////////
	// 생성자, 파괴자.
	//
	// Parameters:	(int) 최대 블럭 개수.
	//				(bool) 생성자 호출 여부.
	// Return:
	//////////////////////////////////////////////////////////////////////////
	CMemoryPool(int iBlockNum, bool bPlacementNew = false);
	CMemoryPool();
	virtual	~CMemoryPool()
	{
		_aligned_free(_pTop);
	}


	//////////////////////////////////////////////////////////////////////////
	// 블럭 하나를 할당받는다.
	//
	// Parameters: 없음.
	// Return: (DATA *) 데이타 블럭 포인터.
	//////////////////////////////////////////////////////////////////////////
	DATA	*Alloc(void);

	//////////////////////////////////////////////////////////////////////////
	// 사용중이던 블럭을 해제한다.
	//
	// Parameters: (DATA *) 블럭 포인터.
	// Return: (BOOL) TRUE, FALSE.
	//////////////////////////////////////////////////////////////////////////
	bool	Free(DATA *pData);


	//////////////////////////////////////////////////////////////////////////
	// 현재 확보 된 블럭 개수를 얻는다. (메모리풀 내부의 전체 개수)
	//
	// Parameters: 없음.
	// Return: (int) 메모리 풀 내부 전체 개수
	//////////////////////////////////////////////////////////////////////////
	int		GetAllocCount(void) { return _iAllocCount; }

	//////////////////////////////////////////////////////////////////////////
	// 현재 사용중인 블럭 개수를 얻는다.
	//
	// Parameters: 없음.
	// Return: (int) 사용중인 블럭 개수.
	//////////////////////////////////////////////////////////////////////////
	int		GetUseCount(void) { return _iUseCount; }

private:

	long _iAllocCount;
	long _iUseCount;
	//long _iUsableCount;


	TOP_NODE *_pTop;
	//	char* _pTop_Start;

	st_BLOCK_NODE *pEmptyNode;

	//UINT64 _iUniqueNumber;

	//st_BLOCK_NODE *_pTop->pTopNode;

	bool _bMemoryFree;

	int _iMaxNodeNum;



};


template <class DATA>
CMemoryPool<DATA>::CMemoryPool()
{

	//InitializeCriticalSection(&_csMemoryPool);

	_bPlacementNew = false;
	_iMaxNodeNum = 0;

	_bMemoryFree = true;

	_iAllocCount = 0;
	_iUseCount = 0;


	_pTop = (TOP_NODE*)_aligned_malloc(sizeof(TOP_NODE), 16);

	pEmptyNode = new st_BLOCK_NODE();


	_pTop->iUniqueNum = 0;
	_pTop->pTopNode = pEmptyNode;




}

template <class DATA>
DATA* CMemoryPool<DATA>::Alloc(void)
{

	DATA* pData;
	//사용가능 카운터
	long AllocCount = _iAllocCount;
	long iUseCountResult = InterlockedIncrement(&_iUseCount);

	//메모리 프리 -> 부족하면 생성	
	if (iUseCountResult > AllocCount)
	{
		st_BLOCK_NODE *pNode = new st_BLOCK_NODE;

		pData = (DATA*)pNode;

		InterlockedIncrement(&_iAllocCount);

		return pData;
	}





	////노드 있을시 


	TOP_NODE BeforeTop;
	TOP_NODE NewTop;


	do {

		BeforeTop.Aligment = _pTop->Aligment;

		NewTop.pTopNode = BeforeTop.pTopNode->stpNextBlock;
		NewTop.iUniqueNum = BeforeTop.iUniqueNum + 1;


		//_pTop->pTopNode = ((st_BLOCK_NODE*)_pTop->pTopNode)->stpNextBlock;
	} while (InterlockedCompareExchange64(&_pTop->Aligment, NewTop.Aligment, BeforeTop.Aligment) != BeforeTop.Aligment);

	//InterlockedIncrement64((LONG64*)&_pTop->iUniqueNum);

	pData = (DATA*)BeforeTop.pTopNode;

	//((st_BLOCK_NODE*)temp.pTopNode) += 1;//sizeof(st_BLOCK_NODE); // st_BLOCK_NODE만큼 늘거나 줄기에 1만 더해준다

	if (_bPlacementNew)
		new (pData) DATA;//생성자 호출



	return pData;


}

//////////////////////////////////////////////////////////////////////////
// 사용중이던 블럭을 해제한다.
//
// Parameters: (DATA *) 블럭 포인터.
// Return: (BOOL) TRUE, FALSE.
//////////////////////////////////////////////////////////////////////////
template <class DATA>
bool CMemoryPool<DATA>::Free(DATA *pData)
{
	st_BLOCK_NODE *pNode = (st_BLOCK_NODE*)pData;


	if (pNode->CheckCode != dfCHECKCODE)
	{
		wprintf(L"실패");
		int*p = NULL;
		*p = 0;
		
		return false;
	}

	if (_bPlacementNew)
		pData->~DATA();


	TOP_NODE BeforeTop;
	TOP_NODE NewTop;

	do {

		BeforeTop.Aligment = _pTop->Aligment;

		NewTop.pTopNode = pNode;
		NewTop.iUniqueNum = _pTop->iUniqueNum + 1;



		pNode->stpNextBlock = BeforeTop.pTopNode;//e->stpNextBlock.Aligment = BeforeTop.Aligment;

		
	


	} while (InterlockedCompareExchange64(&_pTop->Aligment, NewTop.Aligment, BeforeTop.Aligment) != BeforeTop.Aligment);
		
	InterlockedDecrement(&_iUseCount);


	/*_pTop->pTopNode = (st_BLOCK_NODE *)data;*/


	//LeaveCriticalSection(&_csMemoryPool);

	return true;

}

