#include "stdafx.h"
#include "Parser.h"

CParser::CParser()
{
	Initialize();
}

CParser::~CParser()
{

}
//bool CParser::UTF16toUTF8_NotNull(char *wValue, char* Value)
//{
//	//마스터 토큰
//	int len = WideCharToMultiByte(CP_ACP, 0, wValue, -1, NULL, 0, NULL, NULL);
//	WideCharToMultiByte(CP_ACP, 0, wValue, -1, Value, len, NULL, NULL);
//	return true;
//}
//bool CParser::UTF16toUTF8(char *wValue, char* Value)
//{
//	//마스터 토큰
//	int len = WideCharToMultiByte(CP_ACP, 0, wValue, -1, NULL, 0, NULL, NULL);
//	WideCharToMultiByte(CP_ACP, 0, wValue, -1, Value, len, NULL, NULL);
//	return true;
//}

void CParser::FileLoad(char * pFileName)
{
	FILE* pFile;


	fopen_s(&pFile, pFileName, "rb");

	if (pFile == NULL)
		return;

	fseek(pFile, 0, SEEK_END);
	_iFileSize = ftell(pFile) * sizeof(char);
	fseek(pFile, 0, SEEK_SET);   

	_pwchBuffer = (char*)malloc(_iFileSize);
	memset(_pwchBuffer, 0, _iFileSize);
	fread(_pwchBuffer, _iFileSize, 1, pFile);

	fclose(pFile);
}

void CParser::Initialize(void)
{
	_iBufferCurrent = 1;
	_iBufferStart = -1;
	_iBufferEnd = -1;

	_bProvideAreaMode = false;
}

bool CParser::SkipNoneCommand(void)
{
	char*      pwchBuffer;

	pwchBuffer = _pwchBuffer + _iBufferCurrent;

	while (1)
	{
		if (_iFileSize < _iBufferCurrent)
			return false;

		if (*pwchBuffer == 0x20 || *pwchBuffer == 0x08 || *pwchBuffer == 0x09 || *pwchBuffer == 0x0a || *pwchBuffer == 0x0d)
		{
			++pwchBuffer;
			_iBufferCurrent += 1;
		}
		else if (*pwchBuffer == L'/' && *(pwchBuffer+1) == L'/')
		{
			while (*pwchBuffer != 0x0d)
			{
				if (_iFileSize < _iBufferCurrent)
					return false;

				++pwchBuffer;
				_iBufferCurrent += 1;
			}
		}
		else
		{
			break;
		}
	}

	return true;
}
//bool CParser::SetPacketList()
//{
//	char* pchwBuff, wchBuff[256];
//	int iLength;
//
//	_iBufferCurrent = 0;
//	_iBufferStart = -1;
//	_iBufferEnd = -1;
//
//	if (!SkipNoneCommand())
//		return false;
//
//	pchwBuff = _pwchBuffer + _iBufferCurrent;
//
//	while (GetNextWord(&pchwBuff, &iLength))
//	{
//		memset(wchBuff, 0, sizeof(char) * 256);
//		memcpy(wchBuff, pchwBuff, sizeof(char) * iLength);
//
//		if (wchBuff[0] == L':')
//		{
//
//			if (!strcmp(wchBuff + 1, "PACKET_LIST"))
//			{
//				if (GetNextWord(&pchwBuff, &iLength))
//				{
//					memset(wchBuff, 0, sizeof(char) * 256);
//					memcpy(wchBuff, pchwBuff, sizeof(char) * iLength);
//
//					if (wchBuff[0] == L'{')
//					{
//						// 공백 제거 !! 
//						if (!SkipNoneCommand())
//							return false;
//
//						_bProvideAreaMode = true;
//						_iBufferStart = _iBufferCurrent;
//					}
//				}
//				else
//				{
//					return false;
//				}
//			}
//		}
//		else if (_bProvideAreaMode && wchBuff[0] == L'}')
//		{
//			_iBufferEnd = _iBufferCurrent;
//			_bProvideAreaMode = false;
//			return true;
//		}
//	}
//
//	return false;
//}
CParser::MapPackType* CParser::SetPacketList()
{
	/*if(Packet == nullptr)
		return false;*/

	//PacketInfoType = new SPacket();//Packet;
	MapPacket = new MapPackType();
	
	char* pchwBuff, wchBuff[256];
	int iLength;

	_iBufferCurrent = 0;
	_iBufferStart = -1;
	_iBufferEnd = -1;

	if (!SkipNoneCommand())
		return false;

	pchwBuff = _pwchBuffer + _iBufferCurrent;

	while (GetNextWord(&pchwBuff, &iLength))
	{
		//memset(wchBuff, 0, sizeof(char) * 256);
		//memcpy(wchBuff, pchwBuff, sizeof(char) * iLength);

		if (pchwBuff[0] == L'#')
		{			
			if (!memcmp(pchwBuff + 1, "PACKET_LIST",iLength-1))
			{
				if (GetNextWord(&pchwBuff, &iLength))
				{
					//	memset(wchBuff, 0, sizeof(char) * 256);
					//	memcpy(wchBuff, pchwBuff, sizeof(char) * iLength);

					if (pchwBuff[0] == L'[')
					{
						// 공백 제거 !! 
						if (!SkipNoneCommand())
							return nullptr;
						/*
						_bProvideAreaMode = true;
						_iBufferStart = _iBufferCurrent;*/

						if(!SetPacket())
							return nullptr;

					}
				}
				else
				{
					return nullptr;
				}

			}
		}
		/*else if (_bProvideAreaMode && pchwBuff[0] == L']')
		{
			_iBufferEnd = _iBufferCurrent;
			_bProvideAreaMode = false;
			return true;
		}*/
	}

	return MapPacket;
}

//단어 길이 측정
bool CParser::GetNextWord(char ** pwchBuffer, int * piLength)
{
	char* pwchBufferTemp;

	if (!SkipNoneCommand())
		return false;

	*pwchBuffer = _pwchBuffer + _iBufferCurrent;

	pwchBufferTemp = *pwchBuffer;

	*piLength = 0;

	// 문자열인지 아닌지 체크
	if (**pwchBuffer == '"')
	{
		if (GetStringWord(pwchBuffer, piLength))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	while (1)
	{
		if (_iBufferCurrent > _iFileSize)
			return false;

		//0x09 탭, 0x20 스페이스, 0x0a 0x0d 줄바꿈
		if (**pwchBuffer == 0x09 || **pwchBuffer == 0x20 || **pwchBuffer == 0x0a || **pwchBuffer == 0x0d) //**pwchBuffer == 0x08 0x08 백스페이스, 
		{
			break;
		}

		if (**pwchBuffer == '}' || **pwchBuffer == ',')
		{
			++_iBufferCurrent;
			++(*piLength);
			return true;
		}

		++_iBufferCurrent;
		++(*pwchBuffer);
		++(*piLength);


	}

	*pwchBuffer = pwchBufferTemp;

	return true;
}

bool CParser::GetStringWord(char** pwchBuffer, int* iLength)
{

	char* pwchBufferTemp;

	//if (!SkipNoneCommand())
	//    return false;

	//*pwchBuffer = _pwchBuffer + _iBufferCurrent;
	pwchBufferTemp =  *pwchBuffer;

	*iLength = 0;

	/*  if (**pwchBuffer != '"')
	{
	return false;
	}*/

	// 첫 따옴표 넘기기 
	++_iBufferCurrent;
	++(*pwchBuffer);
	++pwchBufferTemp;


	while (1)
	{
		if (_iBufferCurrent > _iFileSize)
			return false;

		if (**pwchBuffer == 0x09 || **pwchBuffer == 0x20 || **pwchBuffer == 0x0a || **pwchBuffer == 0x0d || **pwchBuffer == '"')
		{
			break;
		}

		++_iBufferCurrent;
		++(*pwchBuffer);
		++(*iLength);
	}
	*pwchBuffer = pwchBufferTemp;
	return true;
}

bool CParser::SetPacket()
{
	char* pwchBuffTemp, pwchBuff[256];
	int     iLength;

	// 현재 구역의 시작점
	//_iBufferCurrent = _iBufferStart;
	SPacket *pPacket = nullptr;

	while (GetNextWord(&pwchBuffTemp, &iLength))
	{
		//패킷 이름
		memset(pwchBuff, 0, sizeof(char) * 256);
		memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

		


		if (0 == strcmp(pwchBuff, ":"))
		{
			//패킷번호		
			if (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				pPacket->wPacketCommand = atoi(pwchBuff);

			}
			
		}
		//변수 타입
		else if (0 == strcmp(pwchBuff, "(")) 
		{
			while (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				if(0 == strcmp(pwchBuff, ","))
				{
					++pwchBuffTemp;
					++_iBufferCurrent;
				}
				else if (0 == strcmp(pwchBuff, ")"))
				{
					MapPackType::iterator iter = MapPacket->find(pPacket->Name);
					if(iter != MapPacket->end())
						return false;
					

				
					MapPacket->insert(pair<string,SPacket*>(pPacket->Name,pPacket));

					break;
				}
				else
				//타입 찾기
				if(!FindValueType(pPacket,pwchBuffTemp,iLength))
					return false;

			}

		}
		else if(0 == strcmp(pwchBuff, "]"))
		{
			return true;
		}
		else
		{
			pPacket = new SPacket();
			//패킷 이름
			memcpy(pPacket->Name,pwchBuff,iLength);
		}


		

	}
	return false;
}


bool CParser::FindValueType(SPacket *pPacket,char* pchType,int iLen)
{
	if (!SkipNoneCommand())
		return false;

	switch(iLen)
	{
	case 3 :
		{			
			if(0 == memcmp(pchType,"int",iLen))
			{
				pPacket->listValue.push_back(EINT);
			}
		}
		break;
	case 5:
		{
			if(0 == memcmp(pchType,"INT64",iLen))
			{
				pPacket->listValue.push_back(EINT64);
			}
		}
		break;
	case 6:
		{
			if(0 == memcmp(pchType,"string",iLen))
			{
				pPacket->listValue.push_back(ESTRING);
			}

		}		
		break;
	default:
		return false;
		
	}

	return true;


}

bool CParser::GetValue(char * pwchName, WORD* wpValue)
{
	char* pwchBuffTemp, pwchBuff[256];
	int     iLength;

	// 현재 구역의 시작점
	_iBufferCurrent = _iBufferStart;

	while (GetNextWord(&pwchBuffTemp, &iLength))
	{
		memset(pwchBuff, 0, sizeof(char) * 256);
		memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

		if (0 == strcmp(pwchBuff, pwchName))
		{
			if (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				if (0 == strcmp(pwchBuff, "="))
				{
					if (GetNextWord(&pwchBuffTemp, &iLength))
					{
						memset(pwchBuff, 0, sizeof(char) * 256);
						memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

						*wpValue = atoi(pwchBuff);
						return true;
					}
					return false;
				}
				// 다른 부등호가 있으면 여기에 추가할것.
			}
			return false;
		}
	}
	return false;
}

bool CParser::GetValue(char * pwchName, LONG * lpValue)
{
	char* pwchBuffTemp, pwchBuff[256];
	int     iLength;

	// 현재 구역의 시작점
	_iBufferCurrent = _iBufferStart;

	while (GetNextWord(&pwchBuffTemp, &iLength))
	{
		memset(pwchBuff, 0, sizeof(char) * 256);
		memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

		if (0 == strcmp(pwchBuff, pwchName))
		{
			if (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				if (0 == strcmp(pwchBuff, "="))
				{
					if (GetNextWord(&pwchBuffTemp, &iLength))
					{
						memset(pwchBuff, 0, sizeof(char) * 256);
						memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

						*lpValue = atoi(pwchBuff);
						return true;
					}
					return false;
				}
				// 다른 부등호가 있으면 여기에 추가할것.
			}
			return false;
		}

	}
	return false;
}

bool CParser::GetValue(char * pwchName, int * ipValue)
{
	char* pwchBuffTemp, pwchBuff[256];
	int     iLength;

	// 현재 구역의 시작점
	_iBufferCurrent = _iBufferStart;

	while (GetNextWord(&pwchBuffTemp, &iLength))
	{
		memset(pwchBuff, 0, sizeof(char) * 256);
		memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

		if (0 == strcmp(pwchBuff, pwchName))
		{
			if (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				if (0 == strcmp(pwchBuff, "="))
				{
					if (GetNextWord(&pwchBuffTemp, &iLength))
					{
						memset(pwchBuff, 0, sizeof(char) * 256);
						memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

						*ipValue = atoi(pwchBuff);
						return true;
					}
					return false;
				}
				// 다른 부등호가 있으면 여기에 추가할것.
			}
			return false;
		}

	}
	return false;
}

bool CParser::GetValue(char * pwchName, float * fpValue)
{
	char* pwchBuffTemp, pwchBuff[256];
	int     iLength;

	// 현재 구역의 시작점
	_iBufferCurrent = _iBufferStart;

	while (GetNextWord(&pwchBuffTemp, &iLength))
	{
		memset(pwchBuff, 0, sizeof(char) * 256);
		memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

		if (0 == strcmp(pwchBuff, pwchName))
		{
			if (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				if (0 == strcmp(pwchBuff, "="))
				{
					if (GetNextWord(&pwchBuffTemp, &iLength))
					{
						memset(pwchBuff, 0, sizeof(char) * 256);
						memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

						*fpValue = float(atof(pwchBuff));
						return true;
					}
					return false;
				}
				// 다른 부등호가 있으면 여기에 추가할것.
			}
			return false;
		}

	}
	return false;
}

bool CParser::GetValue(char * pwchName, BYTE* chValue)
{
	char* pwchBuffTemp, pwchBuff[256];
	int     iLength;

	// 현재 구역의 시작점
	_iBufferCurrent = _iBufferStart;

	while (GetNextWord(&pwchBuffTemp, &iLength))
	{
		memset(pwchBuff, 0, sizeof(char) * 256);
		memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

		if (0 == strcmp(pwchBuff, pwchName))
		{
			if (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				if (0 == strcmp(pwchBuff, "="))
				{
					if (GetNextWord(&pwchBuffTemp, &iLength))
					{
						memset(pwchBuff, 0, sizeof(char) * 256);
						memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

						*chValue = BYTE(atof(pwchBuff));
						return true;
					}
					return false;
				}
				// 다른 부등호가 있으면 여기에 추가할것.
			}
			return false;
		}

	}
	return false;
}

bool CParser::GetValue(char * pwcnName, char * pwchValue)
{
	char* pwchBuffTemp, pwchBuff[256];
	int     iLength;

	// 현재 구역의 시작점
	_iBufferCurrent = _iBufferStart;

	while (GetNextWord(&pwchBuffTemp, &iLength))
	{
		memset(pwchBuff, 0, sizeof(char) * 256);
		memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

		if (0 == strcmp(pwchBuff, pwcnName))
		{
			if (GetNextWord(&pwchBuffTemp, &iLength))
			{
				memset(pwchBuff, 0, sizeof(char) * 256);
				memcpy(pwchBuff, pwchBuffTemp, sizeof(char) * iLength);

				if (0 == strcmp(pwchBuff, "="))
				{
					if (GetNextWord(&pwchBuffTemp, &iLength))
					{
						//memset(pwchValue, 0,  sizeof(char)* iLength);
						memcpy(pwchValue, pwchBuffTemp, sizeof(char) * iLength);
						pwchValue[iLength] = '\0';
						// *iBufferSize = iLength;
						return true;
					}
					return false;
				}
				// 다른 부등호가 있으면 여기에 추가할것.
			}
			return false;
		}
	}

	return false;
}