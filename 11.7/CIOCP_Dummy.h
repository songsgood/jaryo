#pragma once

//#include <WinSock2.h>
//#include <WS2tcpip.h>


#include <process.h>
#include <conio.h>

//라이브러리

#include "CSession.h"
#include "StackLockFree.h"

#define MAX_USER			20
#define WORK_THREAD_COUNT	4
#define UPDATE_THREAD_COUNT 5

#define UPDATE_SLEEP 100

#define IP   "192.168.50.36"

#define PORT 10021

//class CPlayer;

class CIOCP_Dummy
{

public:
	CIOCP_Dummy();
	~CIOCP_Dummy();

	static CIOCP_Dummy Instance;

	static CIOCP_Dummy& GetInstance()
	{
		return Instance;
	}

	bool Start();
	void Stop();

	static unsigned WINAPI ConnectThread(LPVOID arg);
	static unsigned WINAPI UpdateThread(LPVOID arg);
	static unsigned WINAPI WorkThread(LPVOID arg);
	static unsigned WINAPI LogThread(LPVOID arg);

	void OnConnectThread();
	void OnUpdateThread();
	void OnWorkThread();
	void OnLogThread();

	long m_lLoginSuccess;

	UINT64 m_uiTotalDelay;
	UINT64 TotalRecvPacket;
	//long m_lAvgDelay;
	long m_lMaxDelay;

	long m_lSlowClient;

	
private:

	void InitSession();
	//CSession* NewSession();
	


	


	void CreateThreads();
	
	//-------------------
	// 패킷 처리
	//-------------------
	void PacketProcedure(CSession *pSession);

	//CPacketBuffer* HeadCheck(CSession *pSession);

	void Disconnect(CSession *pSession);
	//void ReleaseSession(CSession *pSession);


	//cSession* SessionIO_Lock()

private:

	long UpdateThreadNumber;

	CSession **m_ArraySession;
	//CStackLockFree<CSession*> m_StackSession;


	int m_MaxUser;

	HANDLE m_hIO;

	int m_iWorkThreadCount;


	
	HANDLE m_hConnectThread;
	HANDLE *m_hWorkThread;
	HANDLE *m_hUpdateThread;
	HANDLE m_hLogThread;

	long *m_Mointor_SlowClientThread;
	long *m_Mointor_Stop;


	long m_lMonitor_Connect;
	long m_lMonitor_ConnectTotal;

	long m_lMonitor_SendTPS;
	long m_lMonitor_RecvTPS;

	
	

};
