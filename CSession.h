pragma once

include "CSocket.h"

include "RingBuffer.h"

class CSession : public CSocket
{
public:
	CSession();
	virtual ~CSession();

	
	void InitSession();

	bool SendPost();
	void RecvPost();

	long IncrementIO();
	long DecrementIO();
		
	void SetIO(long Count);

	
	OVERLAPPED* GetSendOverlapped();
	OVERLAPPED* GetRecvOverlapped();
	OVERLAPPED* GetSendPostOverlapped();
	
	
	void Disconnect();


private:

	CSocket m_Sock;

	CRingBuffer m_RecvBuffer;
	
	
	
	
	OVERLAPPED m_SendOverlapped;
	OVERLAPPED m_RecvOverlapped;
	OVERLAPPED m_SendPostOverlapped;

	
	
	
	long m_lIOCount;

	bool m_bReleaseFlag;

	bool m_bSendFlag;
	bool m_bRecvFlag;
	



};