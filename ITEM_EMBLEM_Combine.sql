USE [FreeStyle]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ITEM_EMBLEM_Combine]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ITEM_EMBLEM_Combine]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ITEM_EMBLEM_Combine]
	@UserIDIndex INT,
	@GameIDIndex INT,
	@UserEmblemIndex1 INT,
	@UserEmblemIndex2 INT,
	@UserEmblemIndex3 INT,

	@EmblemKind INT,
	@EmblemGrade INT,
	@SpecialCardType INT,
	@EmblemAbilityIndex1 INT,
	@AbilityGrade1 INT,

	@EmblemAbilityIndex2 INT,
	@AbilityGrade2 INT,
	@EmblemAbilityIndex3 INT,
	@AbilityGrade3 INT,
	@EmblemAbilityIndex4 INT,

	@AbilityGrade4 INT,
	@EmblemAbilityIndex5 INT,
	@AbilityGrade5 INT,
	@EmblemAbilityIndex6 INT,
	@AbilityGrade6 INT,

	@SpecialType1 INT,
	@SpecialSubType1 INT,
	@SpecialValue1 INT,
	@SpecialType2 INT,
	@SpecialSubType2 INT,

	@SpecialValue2 INT,
	@SpecialType3 INT,
	@SpecialSubType3 INT,
	@SpecialValue3 INT,
	@CostType INT,

	@Cost INT,
	@PrevCash INT,
	@PostCash INT,
	@outUpdatedPoint INT OUTPUT,
	@outCombinedUserEmblemIndex INT OUTPUT,
	@CombienIndex INT OUTPUT,

	@UserType INT
AS
SET XACT_ABORT ON
SET NOCOUNT ON

DECLARE @Point INT
,		@Lv	INT
,		@Position INT
,		@IPAddress VARCHAR(15)
,		@Port INT
,		@MacAddress VARCHAR(12)

SET @outUpdatedPoint = 0
SET @outCombinedUserEmblemIndex = 0
SET @CombienIndex = -1

BEGIN

	BEGIN TRY

		IF (@SpecialType1 NOT IN (0,1)) OR (@SpecialType2 NOT IN (0,1)) OR (@SpecialType3 NOT IN (0,1))
			RETURN 1
	
		IF @EmblemAbilityIndex1 < 0
			RETURN -9991

		IF @CostType = 2 -- Cost Point
		BEGIN
			SELECT @Point = Point FROM tblUserPoint (NOLOCK) WHERE UserIDIndex = @UserIDIndex
			IF @@ROWCOUNT = 0
				RETURN -11
				
			IF @Point < @Cost
				RETURN -1
		END

		SELECT @IpAddress = IpAddress, @Port = Port, @MacAddress = MacAddress FROM USER_IPInfo WITH(NOLOCK) WHERE UserIDIndex = @UserIDIndex
		IF @@ROWCOUNT = 0
		BEGIN
			RETURN -1
		END

		SELECT @Lv = L.Lv, @Position = G.GamePosition FROM tblUserGame G (NOLOCK) 
			JOIN tblUserGameLvExp L (NOLOCK) ON G.GameIDIndex = L.GameIDIndex
			WHERE G.GameIDIndex = @GameIDIndex		 

		BEGIN TRAN
		
			DELETE ITEM_EMBLEM_UserInventory WHERE GameIDIndex = @GameIDIndex AND UserEmblemIndex IN (@UserEmblemIndex1, @UserEmblemIndex2, @UserEmblemIndex3)
			IF @@ROWCOUNT <> 3
			BEGIN
				ROLLBACK TRAN
				RETURN -11
			END

			DELETE ITEM_EMBLEM_UserEmblemAbility WHERE GameIDIndex = @GameIDIndex AND UserEmblemIndex IN (@UserEmblemIndex1, @UserEmblemIndex2, @UserEmblemIndex3)
			DELETE ITEM_EMBLEM_UserEmblemSpecial WHERE GameIDIndex = @GameIDIndex AND UserEmblemIndex IN (@UserEmblemIndex1, @UserEmblemIndex2, @UserEmblemIndex3)

			-- Insert
			SELECT @outCombinedUserEmblemIndex = ISNULL(MAX(UserEmblemIndex), 0) + 1 FROM ITEM_EMBLEM_UserInventory WHERE GameIDIndex = @GameIDIndex
					
			INSERT INTO ITEM_EMBLEM_UserInventory VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @EmblemKind, @EmblemGrade, @SpecialCardType)
		
			IF @SpecialType1 > 0
				INSERT INTO ITEM_EMBLEM_UserEmblemSpecial VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @SpecialType1, @SpecialSubType1, @SpecialValue1)
		
			IF @SpecialType2 > 0
				INSERT INTO ITEM_EMBLEM_UserEmblemSpecial VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @SpecialType2, @SpecialSubType2, @SpecialValue2)
				
			IF @SpecialType3 > 0
				INSERT INTO ITEM_EMBLEM_UserEmblemSpecial VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @SpecialType3, @SpecialSubType3, @SpecialValue3)
				
			IF @EmblemAbilityIndex1 >= 0
				INSERT INTO ITEM_EMBLEM_UserEmblemAbility VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @EmblemAbilityIndex1, @AbilityGrade1)
			IF @EmblemAbilityIndex2 >= 0
				INSERT INTO ITEM_EMBLEM_UserEmblemAbility VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @EmblemAbilityIndex2, @AbilityGrade2)
			IF @EmblemAbilityIndex3 >= 0
				INSERT INTO ITEM_EMBLEM_UserEmblemAbility VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @EmblemAbilityIndex3, @AbilityGrade3)
			IF @EmblemAbilityIndex4 >= 0
				INSERT INTO ITEM_EMBLEM_UserEmblemAbility VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @EmblemAbilityIndex4, @AbilityGrade4)
			IF @EmblemAbilityIndex5 >= 0
				INSERT INTO ITEM_EMBLEM_UserEmblemAbility VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @EmblemAbilityIndex5, @AbilityGrade5)
			IF @EmblemAbilityIndex6 >= 0
				INSERT INTO ITEM_EMBLEM_UserEmblemAbility VALUES (@GameIDIndex, @outCombinedUserEmblemIndex, @EmblemAbilityIndex6, @AbilityGrade6)
		
			IF @CostType = 1	-- Cash
			BEGIN
				INSERT INTO tblItemLogCoin  ( UserIDIndex , GameIDIndex , OppositeIDIndex , ItemCode , [Event] , OpDate , PrevMoney , PostMoney, OID, IsBuyWeb, BonusPriceType, BonusPrice, BonusPriceType2, BonusPrice2, ProductCode, Lv, Position, Price, IPAddress, Port, MacAddress, UserType )
						 VALUES ( @UserIDIndex , @GameIDIndex , 0 , 0  ,  33 , GETDATE() ,@PrevCash, @PostCash, '', 0, 0, 0, 0 , 0 , '', @Lv, @Position, @Cost , @IPAddress, @Port, @MacAddress, @UserType)
			END
			ELSE IF @CostType = 2  -- Point
			BEGIN
				UPDATE tblUserPoint SET Point = Point - @Cost, @outUpdatedPoint = Point - @Cost WHERE UserIDIndex = @UserIDIndex
				IF @outUpdatedPoint < 0
				BEGIN
					ROLLBACK TRAN
					RETURN -1
				END
				INSERT INTO tblUserPointLog (UserIDIndex, Point, ChangePoint, PointLogType)
					VALUES (@UserIDIndex, @Point, -@Cost, 4100)

				INSERT INTO tblItemLogPoint  ( UserIDIndex , GameIDIndex , OppositeIDIndex , ItemCode , [Event] , OpDate , PrevPoint , PostPoint, IsBuyWeb, BonusPriceType, BonusPrice, Lv, Position, Price )
						 VALUES ( @UserIDIndex , @GameIDIndex , 0 , 0  ,  33 ,  GETDATE() ,  @Point , @Point - @Cost, 0, 0, 0, @Lv, @Position, @Cost  )
			END
			
			--합성 관련 정보 로그 저장
			INSERT INTO [ITEM_EMBLEM_Combine_Gain_Log]
			VALUES ( @UserIDIndex, @GameIDIndex ,@outCombinedUserEmblemIndex, @EmblemKind, @EmblemGrade, @SpecialCardType,
					 @EmblemAbilityIndex1, @AbilityGrade1, @EmblemAbilityIndex2, @AbilityGrade2, @EmblemAbilityIndex3, @AbilityGrade3,
					 @EmblemAbilityIndex4, @AbilityGrade4, @EmblemAbilityIndex5, @AbilityGrade5, @EmblemAbilityIndex6, @AbilityGrade6,
					 @SpecialType1, @SpecialSubType1, @SpecialValue1, @SpecialType2, @SpecialSubType2, @SpecialValue2,
					 @SpecialType3, @SpecialSubType3, @SpecialValue3, @CostType, @Cost, GETDATE())
			
			SET @CombienIndex = @@IDENTITY
			
		COMMIT TRAN

	END TRY

	BEGIN CATCH
	 
		IF @@TRANCOUNT <> 0
			ROLLBACK TRAN

		RETURN ERROR_NUMBER()

	END CATCH

	RETURN 0
END
SET NOCOUNT OFF
GO
